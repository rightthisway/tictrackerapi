package com.rtf.ecomerce.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtf.ecomerce.utils.EcommUtil;

@Entity
@Table(name = "rtf_order_refund")
public class RtfOrderRefund  implements Serializable{

	private Integer id;
	private Integer orderId;
	private Integer orderProdId;
	private Double refundAmount;
	private String transactionId;
	private String refundId;
	private String status;
	private String remarks;
	private Integer refundLoyaltyPoints;
	private Integer deductOrderEarnedPoints;
	private String refundType;
	private Date refundDate;
	
	private Integer orderQty;
	private Double finalSellPrice;
	private Integer usedLoyaltyPoints;
	private Date updDate;
	private String updBy;
	
	@Id
	@GeneratedValue
	@Column(name="ord_rfd_id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="order_id")
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	@Column(name="order_products_id")
	public Integer getOrderProdId() {
		return orderProdId;
	}
	public void setOrderProdId(Integer orderProdId) {
		this.orderProdId = orderProdId;
	}
	
	@Column(name="refund_amt")
	public Double getRefundAmount() {
		try{
			if(refundAmount != null){
				return EcommUtil.getRoundedValue(refundAmount);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return refundAmount;
	}
	public void setRefundAmount(Double refundAmount) {
		this.refundAmount = refundAmount;
	}
	
	@Column(name="strp_charge_id")
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	@Column(name="strp_refund_id")
	public String getRefundId() {
		return refundId;
	}
	public void setRefundId(String refundId) {
		this.refundId = refundId;
	}
	
	@Column(name="refund_status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Column(name="refund_loyalpnts")
	public Integer getRefundLoyaltyPoints() {
		return refundLoyaltyPoints;
	}
	public void setRefundLoyaltyPoints(Integer refundLoyaltyPoints) {
		this.refundLoyaltyPoints = refundLoyaltyPoints;
	}
	
	@Column(name = "refund_type")
	public String getRefundType() {
		return refundType;
	}
	public void setRefundType(String refundType) {
		this.refundType = refundType;
	}
	@Column(name = "refund_remarks")
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	@Column(name = "ord_qty")
	public Integer getOrderQty() {
		return orderQty;
	}
	public void setOrderQty(Integer orderQty) {
		this.orderQty = orderQty;
	}
	@Column(name = "ord_final_sellprice")
	public Double getFinalSellPrice() {
		return finalSellPrice;
	}
	public void setFinalSellPrice(Double finalSellPrice) {
		this.finalSellPrice = finalSellPrice;
	}
	@Column(name = "ord_used_loyalpnts")
	public Integer getUsedLoyaltyPoints() {
		return usedLoyaltyPoints;
	}
	public void setUsedLoyaltyPoints(Integer usedLoyaltyPoints) {
		this.usedLoyaltyPoints = usedLoyaltyPoints;
	}
	@Column(name = "lupd_date")
	public Date getUpdDate() {
		return updDate;
	}
	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
	@Column(name = "lupd_by")
	public String getUpdBy() {
		return updBy;
	}
	public void setUpdBy(String updBy) {
		this.updBy = updBy;
	}
	@Column(name = "deduct_order_earned_points")
	public Integer getDeductOrderEarnedPoints() {
		return deductOrderEarnedPoints;
	}
	public void setDeductOrderEarnedPoints(Integer deductOrderEarnedPoints) {
		this.deductOrderEarnedPoints = deductOrderEarnedPoints;
	}
	@Column(name = "strp_refund_date")
	public Date getRefundDate() {
		return refundDate;
	}
	public void setRefundDate(Date refundDate) {
		this.refundDate = refundDate;
	}
	
	
}
