package com.rtf.ecomerce.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;

@Entity
@Table(name = "rtf_coupon_code_mstr")
public class CouponCodeMst implements Serializable {	
	
	
	private Integer ccId;	
	private String coucde;
	private String couname;	
	private String coudesc;
	private Double coudiscount;
	private String coustatus;
	private Date updDate;
	private String updBy;	
	private String discounttype;
	private String coutype;
	private Date exprydate;	
	private String updDateStr ;
	private String exprydateStr;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ccID")
	public Integer getCcId() {
		return ccId;
	}
	public void setCcId(Integer ccId) {
		this.ccId = ccId;
	}
	@Column(name = "cou_code")
	public String getCoucde() {
		return coucde;
	}
	public void setCoucde(String coucde) {
		this.coucde = coucde;
	}
	@Column(name = "cou_name")
	public String getCouname() {
		return couname;
	}
	public void setCouname(String couname) {
		this.couname = couname;
	}
	@Column(name = "cou_desc")
	public String getCoudesc() {
		return coudesc;
	}
	public void setCoudesc(String coudesc) {
		this.coudesc = coudesc;
	}
	@Column(name = "cou_discount")
	public Double getCoudiscount() {
		return coudiscount;
	}
	public void setCoudiscount(Double coudiscount) {
		this.coudiscount = coudiscount;
	}
	
	@Column(name = "cou_status")
	public String getCoustatus() {
		return coustatus;
	}
	public void setCoustatus(String coustatus) {
		this.coustatus = coustatus;
	}
	
	@Column(name = "lupd_date")
	public Date getUpdDate() {
		return updDate;
	}
	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
	
	@Column(name = "lupd_by")
	public String getUpdBy() {
		return updBy;
	}
	public void setUpdBy(String updBy) {
		this.updBy = updBy;
	}
	
	@Column(name = "discountype")
	public String getDiscounttype() {
		return discounttype;
	}
	public void setDiscounttype(String discounttype) {
		this.discounttype = discounttype;
	}
	
	@Column(name = "cou_type")
	public String getCoutype() {
		return coutype;
	}
	public void setCoutype(String coutype) {
		this.coutype = coutype;
	}
	
	@Column(name = "exp_date")
	public Date getExprydate() {
		return exprydate;
	}
	public void setExprydate(Date exprydate) {
		this.exprydate = exprydate;
	}
	
	@Transient
	public String getUpdDateStr() {		
		if(updDate!=null){
			this.updDateStr = Util.formatDateToMonthDateYear(updDate);
		}
		return updDateStr;
	}
	public void setUpdDateStr(String updDateStr) {
		this.updDateStr = updDateStr;
	}
	@Transient
	public String getExprydateStr() {		
		if(exprydate!=null){
			this.exprydateStr = Util.formatDateToMonthDateYear(exprydate);
		}	
		return exprydateStr;
	}
	public void setExprydateStr(String exprydateStr) {
		this.exprydateStr = exprydateStr;
	}	
	
	
	
	
	
		
}