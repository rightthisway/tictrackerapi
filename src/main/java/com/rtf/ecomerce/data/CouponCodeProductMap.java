package com.rtf.ecomerce.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "rtf_coupon_product_map")
public class CouponCodeProductMap implements Serializable {	
	
	private Integer cpmId;	
	private Integer ccId;
	private Integer slrpitmsId;	
	private String couCode;	
	
	private String pname;
	private Double pselMinPrc;
	private Double pregMinPrc;
	private String pimg;
	private String isMapped;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "cpmID")	
	public Integer getCpmId() {
		return cpmId;
	}
	public void setCpmId(Integer cpmId) {
		this.cpmId = cpmId;
	}
	
	@Column(name = "ccID")
	public Integer getCcId() {
		return ccId;
	}
	public void setCcId(Integer ccId) {
		this.ccId = ccId;
	}
	
	@Column(name = "slrpitms_id")
	public Integer getSlrpitmsId() {
		return slrpitmsId;
	}
	public void setSlrpitmsId(Integer slrpitmsId) {
		this.slrpitmsId = slrpitmsId;
	}
	
	@Column(name = "cou_code")
	public String getCouCode() {
		return couCode;
	}
	public void setCouCode(String couCode) {
		this.couCode = couCode;
	}
	
	
	
	@Transient
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	@Transient
	public Double getPselMinPrc() {
		return pselMinPrc;
	}
	public void setPselMinPrc(Double pselMinPrc) {
		this.pselMinPrc = pselMinPrc;
	}
	@Transient
	public Double getPregMinPrc() {
		return pregMinPrc;
	}
	public void setPregMinPrc(Double pregMinPrc) {
		this.pregMinPrc = pregMinPrc;
	}
	@Transient
	public String getPimg() {
		return pimg;
	}
	public void setPimg(String pimg) {
		this.pimg = pimg;
	}
	@Transient
	public String getIsMapped() {
		return isMapped;
	}
	public void setIsMapped(String isMapped) {
		this.isMapped = isMapped;
	}
	@Override
	public String toString() {
		return "CouponCodeProductMap [cpmId=" + cpmId + ", ccId=" + ccId + ", slrpitmsId=" + slrpitmsId + ", couCode="
				+ couCode + ", pname=" + pname + ", pselMinPrc=" + pselMinPrc + ", pregMinPrc=" + pregMinPrc + ", pimg="
				+ pimg + ", isMapped=" + isMapped + "]";
	}
	
	
	
	
	
		
}