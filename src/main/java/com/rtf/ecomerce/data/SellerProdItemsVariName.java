package com.rtf.ecomerce.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "rtf_seller_product_items_vari_opt_name")
public class SellerProdItemsVariName implements Serializable{
	
	private Integer slrpiVarId;//seller product item Variation Id
	private Integer spiId;//seller product item id
	private Integer sellerId;
	private String vOptName;//Variation OPtion Name
	private Date updDate;
	private String updBy;
	
	
	
	private List<SellerProdItemsVariValue> values;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "slrpivar_id")
	public Integer getSlrpiVarId() {
		return slrpiVarId;
	}
	public void setSlrpiVarId(Integer slrpiVarId) {
		this.slrpiVarId = slrpiVarId;
	}
	
	@Column(name = "slrpitms_id")
	public Integer getSpiId() {
		return spiId;
	}
	public void setSpiId(Integer spiId) {
		this.spiId = spiId;
	}
	@Column(name = "sler_id")
	public Integer getSellerId() {
		return sellerId;
	}
	public void setSellerId(Integer sellerId) {
		this.sellerId = sellerId;
	}
	@Column(name = "vopt_name")
	public String getvOptName() {
		return vOptName;
	}
	public void setvOptName(String vOptName) {
		this.vOptName = vOptName;
	}
	@Column(name = "lupd_date")
	public Date getUpdDate() {
		return updDate;
	}
	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
	@Column(name = "lupd_by")
	public String getUpdBy() {
		return updBy;
	}
	public void setUpdBy(String updBy) {
		this.updBy = updBy;
	}
	
	@Transient
	public List<SellerProdItemsVariValue> getValues() {
		return values;
	}
	public void setValues(List<SellerProdItemsVariValue> values) {
		this.values = values;
	}
			
}