package com.rtf.ecomerce.data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtf.ecomerce.utils.EcommUtil;
import com.rtw.tmat.utils.Util;

@Entity
@Table(name = "rtf_seller_product_items_mstr")
public class SellerProductsItems implements Serializable{
	
	private Integer spiId;//seller product item id
	private Integer sellerId;
	private Integer dispOrder;
	private String pBrand;//Product Brand
	private String pName;//Product Name
	private String pDesc;//Product Description
	private String pLongDesc;//Product Long Desc
	private Double psMinPrice;//Product sell minimum proce
	private Double pRegMinPrice;//Product 
	private String pImgUrl;//Product image Url
	private String status;
	private Date pExpDate;//Prodct Expiry Date
	private Date updDate;
	private String updBy;
	private String psku;
	private Boolean isVarient;
	private Integer maxCustQty;
	private String prodType;
	private Boolean isRwdPointProd;
	private Integer reqRwdPoint;
	private Boolean isForLiveGame;
	private Boolean dispPerctageOff;
	private Boolean isDisplayRegularPrice;
	
	
	private String discPerc;
	private String pExpDateStr;
	private String updDateStr;
	
	List<SellerProdItemsVariName> variNamesList;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "slrpitms_id")
	public Integer getSpiId() {
		return spiId;
	}
	public void setSpiId(Integer spiId) {
		this.spiId = spiId;
	}
	@Column(name = "sler_id")
	public Integer getSellerId() {
		return sellerId;
	}
	public void setSellerId(Integer sellerId) {
		this.sellerId = sellerId;
	}
	
	@Column(name = "displ_order")
	public Integer getDispOrder() {
		return dispOrder;
	}
	public void setDispOrder(Integer dispOrder) {
		this.dispOrder = dispOrder;
	}
	
	@Column(name = "pbrand")
	public String getpBrand() {
		return pBrand;
	}
	public void setpBrand(String pBrand) {
		this.pBrand = pBrand;
	}
	@Column(name = "pname")
	public String getpName() {
		return pName;
	}
	public void setpName(String pName) {
		this.pName = pName;
	}
	@Column(name = "pdesc")
	public String getpDesc() {
		return pDesc;
	}
	public void setpDesc(String pDesc) {
		this.pDesc = pDesc;
	}
	@Column(name = "plong_desc")
	public String getpLongDesc() {
		return pLongDesc;
	}
	public void setpLongDesc(String pLongDesc) {
		this.pLongDesc = pLongDesc;
	}
	@Column(name = "psel_min_prc")
	public Double getPsMinPrice() {
		if(psMinPrice!=null){
			try {
				psMinPrice = EcommUtil.getNormalRoundedValue(psMinPrice);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return psMinPrice;
	}
	public void setPsMinPrice(Double psMinPrice) {
		this.psMinPrice = psMinPrice;
	}
	@Column(name = "preg_min_prc")
	public Double getpRegMinPrice() {
		if(pRegMinPrice!=null){
			try {
				pRegMinPrice = EcommUtil.getNormalRoundedValue(pRegMinPrice);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return pRegMinPrice;
	}
	public void setpRegMinPrice(Double pRegMinPrice) {
		this.pRegMinPrice = pRegMinPrice;
	}
	@Column(name = "pimg")
	public String getpImgUrl() {
		return pImgUrl;
	}
	public void setpImgUrl(String pImgUrl) {
		this.pImgUrl = pImgUrl;
	}
	@Column(name = "pstatus")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Column(name = "pexp_date")
	public Date getpExpDate() {
		return pExpDate;
	}
	public void setpExpDate(Date pExpDate) {
		this.pExpDate = pExpDate;
	}
	@Column(name = "lupd_date")
	public Date getUpdDate() {
		return updDate;
	}
	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
	@Column(name = "lupd_by")
	public String getUpdBy() {
		return updBy;
	}
	public void setUpdBy(String updBy) {
		this.updBy = updBy;
	}
	@Column(name = "psku")
	public String getPsku() {
		return psku;
	}
	public void setPsku(String psku) {
		this.psku = psku;
	}
	@Column(name = "max_cust_qty")
	public Integer getMaxCustQty() {
		return maxCustQty;
	}
	public void setMaxCustQty(Integer maxCustQty) {
		this.maxCustQty = maxCustQty;
	}
	@Column(name = "is_variant")
	public Boolean getIsVarient() {
		return isVarient;
	}
	public void setIsVarient(Boolean isVarient) {
		this.isVarient = isVarient;
	}
	
	@Column(name = "prod_type")
	public String getProdType() {
		return prodType;
	}
	public void setProdType(String prodType) {
		this.prodType = prodType;
	}
	
	@Column(name = "only_rwd_prod")
	public Boolean getIsRwdPointProd() {
		return isRwdPointProd;
	}
	public void setIsRwdPointProd(Boolean isRwdPointProd) {
		this.isRwdPointProd = isRwdPointProd;
	}
	
	@Column(name = "only_req_rwd_pnts_prod")
	public Integer getReqRwdPoint() {
		return reqRwdPoint;
	}
	public void setReqRwdPoint(Integer reqRwdPoint) {
		this.reqRwdPoint = reqRwdPoint;
	}
	
	
	
	@Transient
	public String getDiscPerc() {
		try {
			Double diff = pRegMinPrice - psMinPrice;
			discPerc = 	EcommUtil.getNormalRoundedValue((diff*100)/pRegMinPrice)+"% Off";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return discPerc;
	}
	public void setDiscPerc(String discPerc) {
		this.discPerc = discPerc;
	}
	
	@Transient
	public String getpExpDateStr() {
		if(pExpDate!=null){
			pExpDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(pExpDate);
		}
		return pExpDateStr;
	}
	public void setpExpDateStr(String pExpDateStr) {
		this.pExpDateStr = pExpDateStr;
	}
	
	@Transient
	public String getUpdDateStr() {
		if(updDate!=null){
			updDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(updDate);
		}
		return updDateStr;
	}
	public void setUpdDateStr(String updDateStr) {
		this.updDateStr = updDateStr;
	}
	
	@Transient
	public List<SellerProdItemsVariName> getVariNamesList() {
		return variNamesList;
	}
	public void setVariNamesList(List<SellerProdItemsVariName> variNamesList) {
		this.variNamesList = variNamesList;
	}
	@Column(name = "is_for_live_game")
	public Boolean getIsForLiveGame() {
		return isForLiveGame;
	}
	public void setIsForLiveGame(Boolean isForLiveGame) {
		this.isForLiveGame = isForLiveGame;
	}
	@Column(name = "disp_perc_off")
	public Boolean getDispPerctageOff() {
		return dispPerctageOff;
	}
	public void setDispPerctageOff(Boolean dispPerctageOff) {
		this.dispPerctageOff = dispPerctageOff;
	}
	@Column(name = "disp_reg_price")
	public Boolean getIsDisplayRegularPrice() {
		return isDisplayRegularPrice;
	}
	public void setIsDisplayRegularPrice(Boolean isDisplayRegularPrice) {
		this.isDisplayRegularPrice = isDisplayRegularPrice;
	}
	
	
	
	
	
	
		
}