package com.rtf.ecomerce.data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "rtf_seller_product_excel_upload")
public class SellerProductUpload implements Serializable {
	
	
    private Integer id;
	private String slerCompName;
	private String pname;
	private String pbrand;
	private String prodType;
	private String pdesc;
	private String plongDesc;
	
	private Double  pregMinPprc;
	private Double  pselMinPrc ;
	private Double  discOnSelPrice;
	private Integer avlQty;
	
	private String vonameOne;
	private String vonameTwo;
	private String vonameThree;	
	private String vonamefour;
	
	private String vovalueOne;
	private String vovalueTwo;
	private String vovalueThree;
	private String vovalueFour;
	
	
	private String pimg;
	private String pvarImg;
	private String excelFileName;
	private String upldStatus;
	
	private Integer maxCustQty;
	private Boolean isForLive;
	private Integer displayOrder;
	private String productType;
	
	private Boolean onlyRewardPointsProd;
	private Integer reqdLoyaltyPoints;
	private Double priceAfterLoyaltyPoints;
	private Boolean isPercentageOff;
	private Boolean isDsplayRegularPrice;
	private String prodWebsiteUrl;
	private String lupdBy;
	private Date lupdDate;
	
	private Boolean isVariantProduct;
	
	  private Integer prodId;
	  private List<String> varNames;
	  private Map<String,Integer> varNameIdMap;
	  private Map<String ,List<String>> varNameOptValueLstMap;
	  private List<SellerProdItemsVariName> sellerProdItemsVariNameLst;
	
	    private Map<String,Set> varNameSet;
		private Map<String ,Integer> variantNameIdMap;
		private Set<String> prodvarnameset;
		private Map<String,Set<String>> pnameVarOptValueSetMap;
	
		private List<SellerProductUpload> inventoryList;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name = "sler_comp_name")
	public String getSlerCompName() {
		return slerCompName;
	}
	public void setSlerCompName(String slerCompName) {
		this.slerCompName = slerCompName;
	}
	@Column(name = "pname")
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	@Column(name = "pbrand")
	public String getPbrand() {
		return pbrand;
	}
	public void setPbrand(String pbrand) {
		this.pbrand = pbrand;
	}
	
	@Column(name = "prod_type")
	public String getProdType() {
		return prodType;
	}
	public void setProdType(String prodType) {
		this.prodType = prodType;
	}
	@Column(name = "pdesc")
	public String getPdesc() {
		return pdesc;
	}
	public void setPdesc(String pdesc) {
		this.pdesc = pdesc;
	}
	@Column(name = "plong_desc")
	public String getPlongDesc() {
		return plongDesc;
	}
	public void setPlongDesc(String plongDesc) {
		this.plongDesc = plongDesc;
	}
	@Column(name = "preg_min_prc")
	public Double getPregMinPprc() {
		return pregMinPprc;
	}
	public void setPregMinPprc(Double pregMinPprc) {
		this.pregMinPprc = pregMinPprc;
	}
	@Column(name = "psel_min_prc")
	public Double getPselMinPrc() {
		return pselMinPrc;
	}
	public void setPselMinPrc(Double pselMinPrc) {
		this.pselMinPrc = pselMinPrc;
	}
	@Column(name = "avl_qty")
	public Integer getAvlQty() {
		return avlQty;
	}
	public void setAvlQty(Integer avlQty) {
		this.avlQty = avlQty;
	}	
	
	@Column(name = "vo_name_one")
	public String getVonameOne() {
		return vonameOne;
	}
	public void setVonameOne(String vonameOne) {
		this.vonameOne = vonameOne;
	}
	@Column(name = "vo_name_two")
	public String getVonameTwo() {
		return vonameTwo;
	}
	
	public void setVonameTwo(String vonameTwo) {
		this.vonameTwo = vonameTwo;
	}
	@Column(name = "vo_name_three")
	public String getVonameThree() {
		return vonameThree;
	}
	public void setVonameThree(String vonameThree) {
		this.vonameThree = vonameThree;
	}
	@Column(name = "vo_name_four")
	public String getVonamefour() {
		return vonamefour;
	}
	
	public void setVonamefour(String vonamefour) {
		this.vonamefour = vonamefour;
	}
	@Column(name = "vo_value_one")
	public String getVovalueOne() {
		return vovalueOne;
	}
	public void setVovalueOne(String vovalueOne) {
		this.vovalueOne = vovalueOne;
	}
	@Column(name = "vo_value_two")
	public String getVovalueTwo() {
		return vovalueTwo;
	}
	public void setVovalueTwo(String vovalueTwo) {
		this.vovalueTwo = vovalueTwo;
	}
	@Column(name = "vo_value_three")
	public String getVovalueThree() {
		return vovalueThree;
	}
	public void setVovalueThree(String vovalueThree) {
		this.vovalueThree = vovalueThree;
	}
	@Column(name = "vo_value_four")
	public String getVovalueFour() {
		return vovalueFour;
	}
	public void setVovalueFour(String vovalueFour) {
		this.vovalueFour = vovalueFour;
	}
	
	@Column(name = "pimg")
	public String getPimg() {
		return pimg;
	}
	public void setPimg(String pimg) {
		this.pimg = pimg;
	}
	@Column(name = "pvar_img")
	public String getPvarImg() {
		return pvarImg;
	}
	public void setPvarImg(String pvarImg) {
		this.pvarImg = pvarImg;
	}
	@Column(name = "excel_file_name")
	public String getExcelFileName() {
		return excelFileName;
	}
	public void setExcelFileName(String excelFileName) {
		this.excelFileName = excelFileName;
	}
	@Column(name = "upld_status")
	public String getUpldStatus() {
		return upldStatus;
	}
	public void setUpldStatus(String upldStatus) {
		this.upldStatus = upldStatus;
	}
	@Column(name = "lupd_by")
	public String getLupdBy() {
		return lupdBy;
	}
	public void setLupdBy(String lupdBy) {
		this.lupdBy = lupdBy;
	}
	@Column(name = "lupd_date")
	public Date getLupdDate() {
		return lupdDate;
	}
	public void setLupdDate(Date lupdDate) {
		this.lupdDate = lupdDate;
	}
	public Integer getProdId() {
		return prodId;
	}
	public void setProdId(Integer prodId) {
		this.prodId = prodId;
	}
	public List<String> getVarNames() {
		return varNames;
	}
	public void setVarNames(List<String> varNames) {
		this.varNames = varNames;
	}
	public Map<String, Integer> getVarNameIdMap() {
		return varNameIdMap;
	}
	public void setVarNameIdMap(Map<String, Integer> varNameIdMap) {
		this.varNameIdMap = varNameIdMap;
	}
	public Map<String, List<String>> getVarNameOptValueLstMap() {
		return varNameOptValueLstMap;
	}
	public void setVarNameOptValueLstMap(Map<String, List<String>> varNameOptValueLstMap) {
		this.varNameOptValueLstMap = varNameOptValueLstMap;
	}
	public List<SellerProdItemsVariName> getSellerProdItemsVariNameLst() {
		return sellerProdItemsVariNameLst;
	}
	public void setSellerProdItemsVariNameLst(List<SellerProdItemsVariName> sellerProdItemsVariNameLst) {
		this.sellerProdItemsVariNameLst = sellerProdItemsVariNameLst;
	}
	@Override
	public String toString() {
		return "SellerProductUpload [id=" + id + ", slerCompName=" + slerCompName + ", pname=" + pname + ", pbrand="
				+ pbrand + ", pdesc=" + pdesc + ", plongDesc=" + plongDesc + ", pregMinPprc=" + pregMinPprc
				+ ", pselMinPrc=" + pselMinPrc + ", avlQty=" + avlQty + ", vonameOne=" + vonameOne + ", vonameTwo="
				+ vonameTwo + ", vonameThree=" + vonameThree + ", vonamefour=" + vonamefour + ", vovalueOne="
				+ vovalueOne + ", vovalueTwo=" + vovalueTwo + ", vovalueThree=" + vovalueThree + ", vovalueFour="
				+ vovalueFour + ", pimg=" + pimg + ", pvarImg=" + pvarImg + ", excelFileName=" + excelFileName
				+ ", upldStatus=" + upldStatus + ", lupdBy=" + lupdBy + ", lupdDate=" + lupdDate + ", prodId=" + prodId
				+ ", varNames=" + varNames + ", varNameIdMap=" + varNameIdMap + ", varNameOptValueLstMap="
				+ varNameOptValueLstMap + ", sellerProdItemsVariNameLst=" + sellerProdItemsVariNameLst + ", varNameSet="
				+ varNameSet + ", variantNameIdMap=" + variantNameIdMap + ", prodvarnameset=" + prodvarnameset
				+ ", pnameVarOptValueSetMap=" + pnameVarOptValueSetMap + "]";
	}
	public Map<String, Set> getVarNameSet() {
		return varNameSet;
	}
	public void setVarNameSet(Map<String, Set> varNameSet) {
		this.varNameSet = varNameSet;
	}
	public Map<String, Integer> getVariantNameIdMap() {
		return variantNameIdMap;
	}
	public void setVariantNameIdMap(Map<String, Integer> variantNameIdMap) {
		this.variantNameIdMap = variantNameIdMap;
	}
	public Set<String> getProdvarnameset() {
		return prodvarnameset;
	}
	public void setProdvarnameset(Set<String> prodvarnameset) {
		this.prodvarnameset = prodvarnameset;
	}
	public Map<String, Set<String>> getPnameVarOptValueSetMap() {
		return pnameVarOptValueSetMap;
	}
	public void setPnameVarOptValueSetMap(Map<String, Set<String>> pnameVarOptValueSetMap) {
		this.pnameVarOptValueSetMap = pnameVarOptValueSetMap;
	}
	public Integer getMaxCustQty() {
		return maxCustQty;
	}
	public void setMaxCustQty(Integer maxCustQty) {
		this.maxCustQty = maxCustQty;
	}
	public Boolean getIsForLive() {
		return isForLive;
	}
	public void setIsForLive(Boolean isForLive) {
		this.isForLive = isForLive;
	}
	public Integer getDisplayOrder() {
		return displayOrder;
	}
	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public Boolean getOnlyRewardPointsProd() {
		return onlyRewardPointsProd;
	}
	public void setOnlyRewardPointsProd(Boolean onlyRewardPointsProd) {
		this.onlyRewardPointsProd = onlyRewardPointsProd;
	}
	public Integer getReqdLoyaltyPoints() {
		return reqdLoyaltyPoints;
	}
	public void setReqdLoyaltyPoints(Integer reqdLoyaltyPoints) {
		this.reqdLoyaltyPoints = reqdLoyaltyPoints;
	}
	
	public Double getPriceAfterLoyaltyPoints() {
		return priceAfterLoyaltyPoints;
	}
	public void setPriceAfterLoyaltyPoints(Double priceAfterLoyaltyPoints) {
		this.priceAfterLoyaltyPoints = priceAfterLoyaltyPoints;
	}
	public Boolean getIsPercentageOff() {
		return isPercentageOff;
	}
	public void setIsPercentageOff(Boolean isPercentageOff) {
		this.isPercentageOff = isPercentageOff;
	}
	public Boolean getIsDsplayRegularPrice() {
		return isDsplayRegularPrice;
	}
	public void setIsDsplayRegularPrice(Boolean isDsplayRegularPrice) {
		this.isDsplayRegularPrice = isDsplayRegularPrice;
	}
	public String getProdWebsiteUrl() {
		return prodWebsiteUrl;
	}
	public void setProdWebsiteUrl(String prodWebsiteUrl) {
		this.prodWebsiteUrl = prodWebsiteUrl;
	}
	public List<SellerProductUpload> getInventoryList() {
		return inventoryList;
	}
	public void setInventoryList(List<SellerProductUpload> inventoryList) {
		this.inventoryList = inventoryList;
	}
	public Double getDiscOnSelPrice() {
		return discOnSelPrice;
	}
	public void setDiscOnSelPrice(Double discOnSelPrice) {
		this.discOnSelPrice = discOnSelPrice;
	}
	public Boolean getIsVariantProduct() {
		return isVariantProduct;
	}
	public void setIsVariantProduct(Boolean isVariantProduct) {
		this.isVariantProduct = isVariantProduct;
	}
	
		
}