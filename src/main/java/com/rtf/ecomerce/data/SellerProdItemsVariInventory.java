package com.rtf.ecomerce.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "rtf_seller_product_items_variants_inventory")
public class SellerProdItemsVariInventory implements Serializable{
	
	private Integer spivInvId;//seller produt item variation inventory id
	private Integer spiVarComId;//seller Product Variation comp id
	private Integer setUniqId;
	private Integer spiId;//seller product item id
	private Integer sellerId;
	private String pBrand;//Product Brand
	private String vOptNameValCom;//
	private String pName;//Product Name
	private Double sellPrice;
	private Double regPrice;
	private Integer reqLPnts;
	private Double priceAftLPnts;
	private Integer avlQty;
	private Integer soldQty;
	private String dheight;
	private String dwidth;
	private String dlength;
	private String dUnitTyp;
	private String size;
	private String sUntTyp;//size Unit Type
	private String weight;
	private String wUntTyp;//Weight Unit Type
	private String volume;
	private String vUntTyp;//Volume Unit Type
	private String status;
	private String pImg;
	private Integer catId;
	private Integer sCatId;
	private Date expDate;
	private String vn1;//Variant name One
	private String vv1;//Variant Value One
	private String vn2;//Variant name Two
	private String vv2;//Variant Value Two
	private String vn3;//Variant name Three
	private String vv3;//Variant Value Three
	private String vn4;//Variant name Four
	private String vv4;//Variant Value Four
	private Integer vnId1;//Variant Name Id One
	private Integer vvId1;//Variant Value Id One
	private Integer vnId2;//Variant Name Id Two
	private Integer vvId2;//Variant Value Id Two
	private Integer vnId3;//Variant Name Id Three
	private Integer vvId3;//Variant Value Id Three
	private Integer vnId4;//Variant Name Id Four
	private Integer vvId4;//Variant Value Id Four
	private Date updDate; 
	private String updBy;
	
	
	
	private Double finalPrice;
	private Double lytpntDisc;
	private Double custCodeDisc=0.00;
	private Double codeDisc=0.00;
	
	private String prodType;
	private Boolean isRwdPointProd;
	private Boolean dispPerctageOff;
	private Boolean isDisplayRegularPrice;
	private String prodWsUrl;
	//only_rwd_prod,prod_type,disp_perc_off,disp_reg_price,prod_ws_url
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "slrinv_id")
	public Integer getSpivInvId() {
		return spivInvId;
	}
	public void setSpivInvId(Integer spivInvId) {
		this.spivInvId = spivInvId;
	}
	@Column(name = "slrpivarcom_id")
	public Integer getSpiVarComId() {
		return spiVarComId;
	}
	public void setSpiVarComId(Integer spiVarComId) {
		this.spiVarComId = spiVarComId;
	}
	@Column(name = "set_uniq_id")
	public Integer getSetUniqId() {
		return setUniqId;
	}
	public void setSetUniqId(Integer setUniqId) {
		this.setUniqId = setUniqId;
	}
	@Column(name = "slrpitms_id")
	public Integer getSpiId() {
		return spiId;
	}
	public void setSpiId(Integer spiId) {
		this.spiId = spiId;
	}
	@Column(name = "sler_id")
	public Integer getSellerId() {
		return sellerId;
	}
	public void setSellerId(Integer sellerId) {
		this.sellerId = sellerId;
	}
	@Column(name = "pbrand")
	public String getpBrand() {
		return pBrand;
	}
	public void setpBrand(String pBrand) {
		this.pBrand = pBrand;
	}
	@Column(name = "vopt_name_val_com")
	public String getvOptNameValCom() {
		return vOptNameValCom;
	}
	public void setvOptNameValCom(String vOptNameValCom) {
		this.vOptNameValCom = vOptNameValCom;
	}
	@Column(name = "pname")
	public String getpName() {
		return pName;
	}
	public void setpName(String pName) {
		this.pName = pName;
	}
	@Column(name = "psel_prc")
	public Double getSellPrice() {
		return sellPrice;
	}
	public void setSellPrice(Double sellPrice) {
		this.sellPrice = sellPrice;
	}
	@Column(name = "preg_prc")
	public Double getRegPrice() {
		return regPrice;
	}
	public void setRegPrice(Double regPrice) {
		this.regPrice = regPrice;
	}
	@Column(name = "req_lty_pnts")
	public Integer getReqLPnts() {
		return reqLPnts;
	}
	public void setReqLPnts(Integer reqLPnts) {
		this.reqLPnts = reqLPnts;
	}
	@Column(name = "pprc_aft_lty_pnts")
	public Double getPriceAftLPnts() {
		return priceAftLPnts;
	}
	public void setPriceAftLPnts(Double priceAftLPnts) {
		this.priceAftLPnts = priceAftLPnts;
	}
	@Column(name = "avl_qty")
	public Integer getAvlQty() {
		return avlQty;
	}
	public void setAvlQty(Integer avlQty) {
		this.avlQty = avlQty;
	}
	@Column(name = "sld_qty")
	public Integer getSoldQty() {
		return soldQty;
	}
	public void setSoldQty(Integer soldQty) {
		this.soldQty = soldQty;
	}
	@Column(name = "dheight")
	public String getDheight() {
		return dheight;
	}
	public void setDheight(String dheight) {
		this.dheight = dheight;
	}
	@Column(name = "dwidth")
	public String getDwidth() {
		return dwidth;
	}
	public void setDwidth(String dwidth) {
		this.dwidth = dwidth;
	}
	@Column(name = "dlength")
	public String getDlength() {
		return dlength;
	}
	public void setDlength(String dlength) {
		this.dlength = dlength;
	}
	@Column(name = "dunit_type")
	public String getdUnitTyp() {
		return dUnitTyp;
	}
	public void setdUnitTyp(String dUnitTyp) {
		this.dUnitTyp = dUnitTyp;
	}
	@Column(name = "size")
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	@Column(name = "size_unit_type")
	public String getsUntTyp() {
		return sUntTyp;
	}
	public void setsUntTyp(String sUntTyp) {
		this.sUntTyp = sUntTyp;
	}
	@Column(name = "weight")
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	@Column(name = "wgt_unit_type")
	public String getwUntTyp() {
		return wUntTyp;
	}
	public void setwUntTyp(String wUntTyp) {
		this.wUntTyp = wUntTyp;
	}
	@Column(name = "volume")
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
	@Column(name = "vlm_unit_type")
	public String getvUntTyp() {
		return vUntTyp;
	}
	public void setvUntTyp(String vUntTyp) {
		this.vUntTyp = vUntTyp;
	}
	@Column(name = "prd_status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Column(name = "pimg")
	public String getpImg() {
		return pImg;
	}
	public void setpImg(String pImg) {
		this.pImg = pImg;
	}
	
	@Column(name = "cat_id")
	public Integer getCatId() {
		return catId;
	}
	
	public void setCatId(Integer catId) {
		this.catId = catId;
	}
	@Column(name = "subcat_id")
	public Integer getsCatId() {
		return sCatId;
	}
	public void setsCatId(Integer sCatId) {
		this.sCatId = sCatId;
	}
	@Column(name = "exp_date")
	public Date getExpDate() {
		return expDate;
	}
	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}
	@Column(name = "vo_name_one")
	public String getVn1() {
		return vn1;
	}
	public void setVn1(String vn1) {
		this.vn1 = vn1;
	}
	@Column(name = "vo_value_one")
	public String getVv1() {
		return vv1;
	}
	public void setVv1(String vv1) {
		this.vv1 = vv1;
	}
	@Column(name = "vo_name_two")
	public String getVn2() {
		return vn2;
	}
	public void setVn2(String vn2) {
		this.vn2 = vn2;
	}
	@Column(name = "vo_value_two")
	public String getVv2() {
		return vv2;
	}
	public void setVv2(String vv2) {
		this.vv2 = vv2;
	}
	@Column(name = "vo_name_three")
	public String getVn3() {
		return vn3;
	}
	public void setVn3(String vn3) {
		this.vn3 = vn3;
	}
	@Column(name = "vo_value_three")
	public String getVv3() {
		return vv3;
	}
	public void setVv3(String vv3) {
		this.vv3 = vv3;
	}
	@Column(name = "vo_name_four")
	public String getVn4() {
		return vn4;
	}
	public void setVn4(String vn4) {
		this.vn4 = vn4;
	}
	@Column(name = "vo_value_four")
	public String getVv4() {
		return vv4;
	}
	public void setVv4(String vv4) {
		this.vv4 = vv4;
	}
	@Column(name = "slrpivar_id_one")
	public Integer getVnId1() {
		return vnId1;
	}
	public void setVnId1(Integer vnId1) {
		this.vnId1 = vnId1;
	}
	@Column(name = "varoval_id_one")
	public Integer getVvId1() {
		return vvId1;
	}
	public void setVvId1(Integer vvId1) {
		this.vvId1 = vvId1;
	}
	@Column(name = "slrpivar_id_two")
	public Integer getVnId2() {
		return vnId2;
	}
	public void setVnId2(Integer vnId2) {
		this.vnId2 = vnId2;
	}
	@Column(name = "varoval_id_two")
	public Integer getVvId2() {
		return vvId2;
	}
	public void setVvId2(Integer vvId2) {
		this.vvId2 = vvId2;
	}
	@Column(name = "slrpivar_id_three")
	public Integer getVnId3() {
		return vnId3;
	}
	public void setVnId3(Integer vnId3) {
		this.vnId3 = vnId3;
	}
	@Column(name = "varoval_id_three")
	public Integer getVvId3() {
		return vvId3;
	}
	public void setVvId3(Integer vvId3) {
		this.vvId3 = vvId3;
	}
	@Column(name = "slrpivar_id_four")
	public Integer getVnId4() {
		return vnId4;
	}
	public void setVnId4(Integer vnId4) {
		this.vnId4 = vnId4;
	}
	@Column(name = "varoval_id_four")
	public Integer getVvId4() {
		return vvId4;
	}
	public void setVvId4(Integer vvId4) {
		this.vvId4 = vvId4;
	}
	@Column(name = "lupd_date")
	public Date getUpdDate() {
		return updDate;
	}
	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
	@Column(name = "lupd_by")
	public String getUpdBy() {
		return updBy;
	}
	public void setUpdBy(String updBy) {
		this.updBy = updBy;
	}
	
	@Transient
	public Double getFinalPrice() {
		if(finalPrice == null){
			finalPrice = this.sellPrice;
		}
		return finalPrice;
	}
	public void setFinalPrice(Double finalPrice) {
		this.finalPrice = finalPrice;
	}
	@Transient
	public Double getCustCodeDisc() {
		return custCodeDisc;
	}
	public void setCustCodeDisc(Double custCodeDisc) {
		this.custCodeDisc = custCodeDisc;
	}
	@Transient
	public Double getCodeDisc() {
		return codeDisc;
	}
	public void setCodeDisc(Double codeDisc) {
		this.codeDisc = codeDisc;
	}
	
	@Transient
	public Double getLytpntDisc() {
		lytpntDisc = this.sellPrice-this.priceAftLPnts;
		return lytpntDisc;
	}
	public void setLytpntDisc(Double lytpntDisc) {
		this.lytpntDisc = lytpntDisc;
	}
	@Column(name = "prod_type")
	public String getProdType() {
		return prodType;
	}
	public void setProdType(String prodType) {
		this.prodType = prodType;
	}
	@Column(name = "only_rwd_prod")
	public Boolean getIsRwdPointProd() {
		return isRwdPointProd;
	}
	public void setIsRwdPointProd(Boolean isRwdPointProd) {
		this.isRwdPointProd = isRwdPointProd;
	}
	@Column(name = "disp_perc_off")
	public Boolean getDispPerctageOff() {
		return dispPerctageOff;
	}
	public void setDispPerctageOff(Boolean dispPerctageOff) {
		this.dispPerctageOff = dispPerctageOff;
	}
	@Column(name = "disp_reg_price")
	public Boolean getIsDisplayRegularPrice() {
		return isDisplayRegularPrice;
	}
	public void setIsDisplayRegularPrice(Boolean isDisplayRegularPrice) {
		this.isDisplayRegularPrice = isDisplayRegularPrice;
	}
	@Column(name = "prod_ws_url")
	public String getProdWsUrl() {
		return prodWsUrl;
	}
	public void setProdWsUrl(String prodWsUrl) {
		this.prodWsUrl = prodWsUrl;
	}
	
	
}