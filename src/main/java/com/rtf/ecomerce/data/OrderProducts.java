package com.rtf.ecomerce.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtf.ecomerce.utils.EcommUtil;


@Entity
@Table(name = "rtf_order_products")
public class OrderProducts implements Serializable{
	
	private Integer id;	
	private Integer custId;
	private Integer orderId;
	private Integer sellerId;	
	private Integer spiId;//seller product item id
	private String pName;//Product Name
	private String pDesc;//Product Description
	private Integer qty;
	private Double selPrice;
	private String qtyType;//Quantity Type	
	private String pImgUrl;//Product image Url
	private Double fSelPrice;//Primary Payment Amount
	private String shClass;//Shipping Class
	private Integer usedLPnts;//Used Loyalty Points
	private Double priceWithLPnts;//Price With Loyalty Points
	private String pLongDesc;//Product Long Desc
	private String pCpnCode;//Product Coupen Code
	private Double cpnCodeDisc;//Coupen Code Discount
	private Integer spivInvId;//seller produt item variation inventory id
	private Date expDelDate;//Expected Delivery Date
	private String shMethod;//Shipping Method
	private String shTrackId;//Shipping Track id
	private Double shCharges;
	private String custCpnCode;//Product Coupen Code
	private Double custcpnCodeDisc;//Coupen Code Discount
	private Double tax;
	private String shippingStatus;
	private String delNote;
	private Double regPrice;
	private String status;
	private String deliveredImgUrl;
	private String prodType;
	private Boolean isRwdPointProd;
	private Boolean isDelEmailSent;
	
	
	private String selPriceStr;
	private String fSelPriceStr;
	private String priceWithLPntsStr;
	private String cpnCodeDiscStr;
	private String custcpnCodeDiscStr;
	private String shChargesStr;
	private String taxStr;
	private String expDelDateStr;
	private String regPriceStr;
	private String vOptNameValCom;
	
	private Double refundAmt;
	private Boolean isTrackingIdUpdated;
	
	private String custFirstName;
	private String custLastName;
	private String custEmail;
	private String custPhone;
	private String shAddr1;
	private String shAddr2;
	private String shCity;
	private String shState;
	private String shCountry;
	private String shZip;
	private String shFName;
	private String shLName;
	private String shPhone;
	private String shEmail;
	private String productUrl;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name = "cust_id")
	public Integer getCustId() {
		return custId;
	}
	public void setCustId(Integer custId) {
		this.custId = custId;
	}
	@Column(name = "order_id")
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	@Column(name = "sler_id")
	public Integer getSellerId() {
		return sellerId;
	}
	public void setSellerId(Integer sellerId) {
		this.sellerId = sellerId;
	}
	@Column(name = "slrpitms_id")
	public Integer getSpiId() {
		return spiId;
	}
	public void setSpiId(Integer spiId) {
		this.spiId = spiId;
	}
	@Column(name = "prod_name")
	public String getpName() {
		return pName;
	}
	public void setpName(String pName) {
		this.pName = pName;
	}
	@Column(name = "prod_desc")
	public String getpDesc() {
		return pDesc;
	}
	public void setpDesc(String pDesc) {
		this.pDesc = pDesc;
	}
	@Column(name = "qty")
	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	@Column(name = "sell_price")
	public Double getSelPrice() {
		if(selPrice==null){
			selPrice = 0.00;
		}
		return selPrice;
	}
	public void setSelPrice(Double selPrice) {
		this.selPrice = selPrice;
	}
	@Column(name = "qty_type")
	public String getQtyType() {
		return qtyType;
	}
	public void setQtyType(String qtyType) {
		this.qtyType = qtyType;
	}
	@Column(name = "prod_img_url")
	public String getpImgUrl() {
		return pImgUrl;
	}
	public void setpImgUrl(String pImgUrl) {
		this.pImgUrl = pImgUrl;
	}
	@Column(name = "final_sellprice")
	public Double getfSelPrice() {
		if(fSelPrice==null){
			fSelPrice = 0.00;
		}
		return fSelPrice;
	}
	public void setfSelPrice(Double fSelPrice) {
		this.fSelPrice = fSelPrice;
	}
	@Column(name = "shippingclass")
	public String getShClass() {
		return shClass;
	}
	public void setShClass(String shClass) {
		this.shClass = shClass;
	}
	@Column(name = "used_loyalpnts")
	public Integer getUsedLPnts() {
		return usedLPnts;
	}
	public void setUsedLPnts(Integer usedLPnts) {
		this.usedLPnts = usedLPnts;
	}
	@Column(name = "price_with_loyalpnts")
	public Double getPriceWithLPnts() {
		return priceWithLPnts;
	}
	public void setPriceWithLPnts(Double priceWithLPnts) {
		this.priceWithLPnts = priceWithLPnts;
	}
	@Column(name = "prod_long_desc")
	public String getpLongDesc() {
		return pLongDesc;
	}
	public void setpLongDesc(String pLongDesc) {
		this.pLongDesc = pLongDesc;
	}
	@Column(name = "prod_cou_code")
	public String getpCpnCode() {
		return pCpnCode;
	}
	public void setpCpnCode(String pCpnCode) {
		this.pCpnCode = pCpnCode;
	}
	@Column(name = "cou_code_discount")
	public Double getCpnCodeDisc() {
		return cpnCodeDisc;
	}
	public void setCpnCodeDisc(Double cpnCodeDisc) {
		this.cpnCodeDisc = cpnCodeDisc;
	}
	@Column(name = "slrinv_id")
	public Integer getSpivInvId() {
		return spivInvId;
	}
	public void setSpivInvId(Integer spivInvId) {
		this.spivInvId = spivInvId;
	}
	@Column(name = "exp_delivery_date")
	public Date getExpDelDate() {
		return expDelDate;
	}
	public void setExpDelDate(Date expDelDate) {
		this.expDelDate = expDelDate;
	}
	@Column(name = "shipping_method")
	public String getShMethod() {
		if(shMethod==null){
			//shMethod="Not assigned yet";
		}
		return shMethod;
	}
	public void setShMethod(String shMethod) {
		this.shMethod = shMethod;
	}
	@Column(name = "shipping_trackID")
	public String getShTrackId() {
		if(shTrackId==null || shTrackId.isEmpty()){
			//shTrackId = "Not assigned yet";
		}
		return shTrackId;
	}
	public void setShTrackId(String shTrackId) {
		this.shTrackId = shTrackId;
	}
	@Column(name = "shipping_charges")
	public Double getShCharges() {
		if(shCharges==null){
			shCharges = 0.00;
		}
		return shCharges;
	}
	public void setShCharges(Double shCharges) {
		this.shCharges = shCharges;
	}
	@Column(name = "cust_cou_code")
	public String getCustCpnCode() {
		return custCpnCode;
	}
	public void setCustCpnCode(String custCpnCode) {
		this.custCpnCode = custCpnCode;
	}
	@Column(name = "cust_cou_code_discount")
	public Double getCustcpnCodeDisc() {
		return custcpnCodeDisc;
	}
	public void setCustcpnCodeDisc(Double custcpnCodeDisc) {
		this.custcpnCodeDisc = custcpnCodeDisc;
	}
	
	@Column(name = "delivery_note")
	public String getDelNote() {
		return delNote;
	}
	public void setDelNote(String delNote) {
		this.delNote = delNote;
	}

	@Column(name = "order_status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Column(name = "reg_price")
	public Double getRegPrice() {
		return regPrice;
	}
	public void setRegPrice(Double regPrice) {
		this.regPrice = regPrice;
	}
	
	
	@Transient
	public Double getTax() {
		if(tax == null){
			tax = 0.00;
		}
		return tax;
	}
	public void setTax(Double tax) {
		this.tax = tax;
	}
	
	@Column(name = "shipping_status")
	public String getShippingStatus() {
		if(shippingStatus == null){
			//shippingStatus = "Shipment Pending";
		}
		return shippingStatus;
	}
	public void setShippingStatus(String shippingStatus) {
		this.shippingStatus = shippingStatus;
	}
	@Column(name = "is_track_upd")
	public Boolean getIsTrackingIdUpdated() {
		return isTrackingIdUpdated;
	}
	public void setIsTrackingIdUpdated(Boolean isTrackingIdUpdated) {
		this.isTrackingIdUpdated = isTrackingIdUpdated;
	}
	@Column(name = "refund_amt")
	public Double getRefundAmt() {
		return refundAmt;
	}
	public void setRefundAmt(Double refundAmt) {
		this.refundAmt = refundAmt;
	}
	
	@Column(name = "delivered_image_file")
	public String getDeliveredImgUrl() {
		return deliveredImgUrl;
	}
	public void setDeliveredImgUrl(String deliveredImgUrl) {
		this.deliveredImgUrl = deliveredImgUrl;
	}
	
	@Column(name = "prod_type")
	public String getProdType() {
		return prodType;
	}
	public void setProdType(String prodType) {
		this.prodType = prodType;
	}
	
	@Column(name = "only_rwd_prod")
	public Boolean getIsRwdPointProd() {
		return isRwdPointProd;
	}
	public void setIsRwdPointProd(Boolean isRwdPointProd) {
		this.isRwdPointProd = isRwdPointProd;
	}
	@Column(name = "is_email_sent")
	public Boolean getIsDelEmailSent() {
		return isDelEmailSent;
	}
	public void setIsDelEmailSent(Boolean isDelEmailSent) {
		this.isDelEmailSent = isDelEmailSent;
	}
	@Transient
	public String getSelPriceStr() {
		try {
			selPriceStr = EcommUtil.getRoundedValueString(selPrice);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return selPriceStr;
	}
	public void setSelPriceStr(String selPriceStr) {
		this.selPriceStr = selPriceStr;
	}
	
	@Transient
	public String getfSelPriceStr() {
		if(fSelPrice==null){
			fSelPrice = 0.00;
		}
		try {
			fSelPriceStr = EcommUtil.getRoundedValueString(fSelPrice);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fSelPriceStr;
	}
	public void setfSelPriceStr(String fSelPriceStr) {
		this.fSelPriceStr = fSelPriceStr;
	}
	
	@Transient
	public String getPriceWithLPntsStr() {
		if(priceWithLPnts==null){
			priceWithLPnts = 0.00;
		}
		try {
			priceWithLPntsStr = EcommUtil.getRoundedValueString(priceWithLPnts);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return priceWithLPntsStr;
	}
	public void setPriceWithLPntsStr(String priceWithLPntsStr) {
		this.priceWithLPntsStr = priceWithLPntsStr;
	}
	
	@Transient
	public String getCpnCodeDiscStr() {
		if(cpnCodeDisc==null){
			cpnCodeDisc = 0.00;
		}
		try {
			cpnCodeDiscStr = EcommUtil.getRoundedValueString(cpnCodeDisc);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cpnCodeDiscStr;
	}
	public void setCpnCodeDiscStr(String cpnCodeDiscStr) {
		this.cpnCodeDiscStr = cpnCodeDiscStr;
	}
	
	@Transient
	public String getCustcpnCodeDiscStr() {
		if(custcpnCodeDisc==null){
			custcpnCodeDisc = 0.00;
		}
		try {
			custcpnCodeDiscStr = EcommUtil.getRoundedValueString(custcpnCodeDisc);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return custcpnCodeDiscStr;
	}
	public void setCustcpnCodeDiscStr(String custcpnCodeDiscStr) {
		this.custcpnCodeDiscStr = custcpnCodeDiscStr;
	}
	
	@Transient
	public String getShChargesStr() {
		try {
			shChargesStr = EcommUtil.getRoundedValueString(shCharges);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return shChargesStr;
	}
	public void setShChargesStr(String shChargesStr) {
		this.shChargesStr = shChargesStr;
	}
	
	@Transient
	public String getTaxStr() {
		return taxStr;
	}
	public void setTaxStr(String taxStr) {
		try {
			taxStr = EcommUtil.getRoundedValueString(tax);
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.taxStr = taxStr;
	}
	
	@Transient
	public String getExpDelDateStr() {
		if(expDelDate!=null){
			expDelDateStr = EcommUtil.formatDateTpMMddyyyy(expDelDate);
		}else{
			//expDelDateStr = "Not assigned yet";
		}
		return expDelDateStr;
	}
	public void setExpDelDateStr(String expDelDateStr) {
		this.expDelDateStr = expDelDateStr;
	}
	
	@Transient
	public String getRegPriceStr() {
		try {
			regPriceStr = EcommUtil.getRoundedValueString(regPrice);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return regPriceStr;
	}
	public void setRegPriceStr(String regPriceStr) {
		this.regPriceStr = regPriceStr;
	}
	@Transient
	public String getvOptNameValCom() {
		return vOptNameValCom;
	}
	public void setvOptNameValCom(String vOptNameValCom) {
		this.vOptNameValCom = vOptNameValCom;
	}
	@Transient
	public String getCustFirstName() {
		return custFirstName;
	}
	public void setCustFirstName(String custFirstName) {
		this.custFirstName = custFirstName;
	}
	@Transient
	public String getCustLastName() {
		return custLastName;
	}
	public void setCustLastName(String custLastName) {
		this.custLastName = custLastName;
	}
	@Transient
	public String getCustEmail() {
		return custEmail;
	}
	public void setCustEmail(String custEmail) {
		this.custEmail = custEmail;
	}
	@Transient
	public String getCustPhone() {
		return custPhone;
	}
	public void setCustPhone(String custPhone) {
		this.custPhone = custPhone;
	}
	@Transient
	public String getShAddr1() {
		return shAddr1;
	}
	public void setShAddr1(String shAddr1) {
		this.shAddr1 = shAddr1;
	}
	@Transient
	public String getShAddr2() {
		return shAddr2;
	}
	public void setShAddr2(String shAddr2) {
		this.shAddr2 = shAddr2;
	}
	@Transient
	public String getShCity() {
		return shCity;
	}
	public void setShCity(String shCity) {
		this.shCity = shCity;
	}
	@Transient
	public String getShState() {
		return shState;
	}
	public void setShState(String shState) {
		this.shState = shState;
	}
	@Transient
	public String getShCountry() {
		return shCountry;
	}
	public void setShCountry(String shCountry) {
		this.shCountry = shCountry;
	}
	@Transient
	public String getShZip() {
		return shZip;
	}
	public void setShZip(String shZip) {
		this.shZip = shZip;
	}
	@Transient
	public String getShFName() {
		return shFName;
	}
	public void setShFName(String shFName) {
		this.shFName = shFName;
	}
	@Transient
	public String getShLName() {
		return shLName;
	}
	public void setShLName(String shLName) {
		this.shLName = shLName;
	}
	@Transient
	public String getShPhone() {
		return shPhone;
	}
	public void setShPhone(String shPhone) {
		this.shPhone = shPhone;
	}
	@Transient
	public String getShEmail() {
		return shEmail;
	}
	public void setShEmail(String shEmail) {
		this.shEmail = shEmail;
	}
	@Transient
	public String getProductUrl() {
		return productUrl;
	}
	public void setProductUrl(String productUrl) {
		this.productUrl = productUrl;
	}
	
	
	
	
	
}