package com.rtf.ecomerce.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtf.ecomerce.dao.impl.EcommDAORegistry;
import com.rtf.ecomerce.data.CouponCodeMst;
import com.rtf.ecomerce.data.CouponCodeProductMap;
import com.rtf.ecomerce.data.CustomerOrder;
import com.rtf.ecomerce.data.OrderProducts;
import com.rtf.ecomerce.data.RTFSellerDetails;
import com.rtf.ecomerce.data.RtfOrderRefund;
import com.rtf.ecomerce.data.SellerProdItemsVariInventory;
import com.rtf.ecomerce.data.SellerProdItemsVariName;
import com.rtf.ecomerce.data.SellerProdItemsVariValue;
import com.rtf.ecomerce.data.SellerProductUpload;
import com.rtf.ecomerce.data.SellerProductsItems;
import com.rtf.ecomerce.pojo.CouponCodeMstDTO;
import com.rtf.ecomerce.pojo.CouponCodeProductsMapDTO;
import com.rtf.ecomerce.pojo.FileUploadGenericDTO;
import com.rtf.ecomerce.pojo.OrderResp;
import com.rtf.ecomerce.pojo.RTFSellerlDTO;
import com.rtf.ecomerce.pojo.RtfOrderRefundResponse;
import com.rtf.ecomerce.pojo.SellerProdItmlDTO;
import com.rtf.ecomerce.pojo.SellerProductUploadTO;
import com.rtf.ecomerce.utils.ASWS3ImageUploaderUtil;
import com.rtf.ecomerce.utils.EcommUtil;
import com.rtf.ecomerce.utils.GridHeaderFilters;
import com.rtf.ecomerce.utils.GridHeaderFiltersUtil;
import com.rtf.ecomerce.utils.MultiFiIesUploadUtil;
import com.rtf.ecomerce.utils.ProductInventoryXLSUploadUtil;
import com.rtf.ecomerce.utils.RTFEcommAppCache;
import com.rtw.tmat.utils.Constants;
import com.rtw.tmat.utils.Error;
import com.rtw.tmat.utils.Util;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.aws.AWSFileService;
import com.rtw.tracker.aws.AwsS3Response;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.pojos.GenericResponseDTO;
import com.rtw.tracker.utils.FileUploadUtil;
import com.rtw.tracker.utils.PaginationUtil;

@Controller
public class RTFEcommController {
	
	private SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	
	SharedProperty sharedProperty;
	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}

	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}
	
	@RequestMapping(value="/GetRtfSellerDtls")
	public RTFSellerlDTO getRtfSellerDtls(HttpServletRequest request, HttpServletResponse response){
		RTFSellerlDTO responseDTO = new RTFSellerlDTO();
		Error error = new Error();
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String status = request.getParameter("status");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getRtfSellerFilter(headerFilter);
			if(status==null || status.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please send valid status.");
				responseDTO.setError(error);
				return responseDTO;
			}
			List<RTFSellerDetails> levels = EcommDAORegistry.getrTFSellerDetailsDAO().getAllRTFSellerDetails(filter,status);
			responseDTO.setRtfSellers(levels);
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(levels.size()));
			responseDTO.setStatus(1);
			if(levels.isEmpty()){
				responseDTO.setStatus(1);
				responseDTO.setMessage("No sellers found.");
				return responseDTO;
			}
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while fetching Sellers.");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}
	
	
	@RequestMapping(value="/UpdateRtfSetllerDtl")
	public RTFSellerlDTO updateSuperFanLevelConf(HttpServletRequest request, HttpServletResponse response){
		RTFSellerlDTO responseDTO = new RTFSellerlDTO();
		Error error = new Error();
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String action = request.getParameter("action");
			String sellerIdStr = request.getParameter("sellerId");
			String status = request.getParameter("status");
			String userName = request.getParameter("userName"); 
			String msg = "";
			GridHeaderFilters filter = GridHeaderFiltersUtil.getRtfSellerFilter(headerFilter);
			if(action==null || action.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Could not able to identify action to be perform.");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(action.equalsIgnoreCase("EDIT")){
				if(sellerIdStr==null || sellerIdStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Seller id not found.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer sellerId = 0;
				try {
					sellerId = Integer.parseInt(sellerIdStr);
				} catch (Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Seller id is invalid.");
					responseDTO.setError(error);
					return responseDTO;
				}
				RTFSellerDetails sellerDtl = EcommDAORegistry.getrTFSellerDetailsDAO().get(sellerId);
				if(sellerDtl == null){
					responseDTO.setStatus(0);
					error.setDescription("Seller not found with given identifier.");
					responseDTO.setError(error);
					return responseDTO;
				}
				responseDTO.setStatus(1);
				responseDTO.setRtfSeller(sellerDtl);
				return responseDTO;
			}else if(action.equalsIgnoreCase("SAVE") || action.equalsIgnoreCase("UPDATE")){
				
				String fName = request.getParameter("fName");
				String lName = request.getParameter("lName");
				String compName = request.getParameter("compName");
				String email = request.getParameter("email");
				String phone = request.getParameter("phone");
				String addLine1 = request.getParameter("addLine1");
				String addLine2 = request.getParameter("addLine2");
				String city = request.getParameter("city");
				String state = request.getParameter("state");
				String country = request.getParameter("country");
				String pincode = request.getParameter("pincode");
				
				if(status==null || status.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Invalid seller status.");
					responseDTO.setError(error);
					return responseDTO;
				}
				
				if(fName==null || fName.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("First Name is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(lName==null || lName.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Last Name is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(compName==null || compName.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Company Name is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(email==null || email.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Email is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(phone==null || phone.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Phone is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(addLine1==null || addLine1.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Address Line1 is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(city==null || city.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("City is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(state==null || state.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("State is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(country==null || country.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Country is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(pincode==null || pincode.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Pincode is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				RTFSellerDetails sellerDtl = null;
				if(action.equalsIgnoreCase("UPDATE")){
					if(sellerIdStr==null || sellerIdStr.isEmpty()){
						responseDTO.setStatus(0);
						error.setDescription("Seller id not found.");
						responseDTO.setError(error);
						return responseDTO;
					}
					Integer sellerId = 0;
					try {
						sellerId = Integer.parseInt(sellerIdStr);
					} catch (Exception e) {
						responseDTO.setStatus(0);
						error.setDescription("Seller id is invalid.");
						responseDTO.setError(error);
						return responseDTO;
					}
					sellerDtl = EcommDAORegistry.getrTFSellerDetailsDAO().get(sellerId);
					if(sellerDtl == null){
						responseDTO.setStatus(0);
						error.setDescription("Seller not found with given identifier.");
						responseDTO.setError(error);
						return responseDTO;
					}
					sellerDtl.setUpdBy(userName);
					sellerDtl.setUpdDate(new Date());
					msg = "Selected Seller updated successfully.";
				}else if(action.equalsIgnoreCase("SAVE")){
					/*SuperFanLevels oldLevel = QuizDAORegistry.getSuperFanLevelsDAO().getSuperFanLevelByLeveNo(levelNo);
					if(oldLevel !=null){
						responseDTO.setStatus(0);
						error.setDescription("Provided super fan level no is laready configured, Cannot have multiple records with same level no.");
						responseDTO.setError(error);
						return responseDTO;
					}*/
					sellerDtl = new RTFSellerDetails();
					sellerDtl.setUpdBy(userName);
					sellerDtl.setUpdDate(new Date());
					sellerDtl.setStatus("ACTIVE");
					msg = "Selected Seller saved successfully.";
				}
				
				sellerDtl.setfName(fName);
				sellerDtl.setlName(lName);
				sellerDtl.setCompName(compName);
				sellerDtl.setEmail(email);
				sellerDtl.setPhone(phone);
				sellerDtl.setAddLine1(addLine1);
				sellerDtl.setAddLine2(addLine2);
				sellerDtl.setCity(city);
				sellerDtl.setState(state);
				sellerDtl.setCountry(country);
				sellerDtl.setPincode(pincode);
				EcommDAORegistry.getrTFSellerDetailsDAO().saveOrUpdate(sellerDtl);
				
				List<RTFSellerDetails> sellers = EcommDAORegistry.getrTFSellerDetailsDAO().getAllRTFSellerDetails(filter,status);
				responseDTO.setRtfSellers(sellers);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(sellers.size()));
				responseDTO.setStatus(1);
				responseDTO.setMessage(msg);
				return responseDTO;
				
			}else if(action.equalsIgnoreCase("DELETE") || action.equalsIgnoreCase("INACTIVE") || action.equalsIgnoreCase("ACTIVE")){
				if(sellerIdStr==null || sellerIdStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Seller id not found.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(status==null || status.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Invalid seller status.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer sellerId = 0;
				try {
					sellerId = Integer.parseInt(sellerIdStr);
				} catch (Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Seller id is invalid.");
					responseDTO.setError(error);
					return responseDTO;
				}
				RTFSellerDetails sellerDtl = EcommDAORegistry.getrTFSellerDetailsDAO().get(sellerId);
				if(sellerDtl == null){
					responseDTO.setStatus(0);
					error.setDescription("Seller not found with given identifier.");
					responseDTO.setError(error);
					return responseDTO;
				}
				sellerDtl.setUpdBy(userName);
				sellerDtl.setUpdDate(new Date());
				if(action.equalsIgnoreCase("DELETE")){
					sellerDtl.setStatus("DELETED");
					msg = "Seller deleted successfully.";
				}else if(action.equalsIgnoreCase("ACTIVE")){
					sellerDtl.setStatus(action);
					msg = "Seller Activated successfully.";
				}else if(action.equalsIgnoreCase("INACTIVE")){
					sellerDtl.setStatus(action);
					msg = "Seller InActivated successfully.";
				}
				EcommDAORegistry.getrTFSellerDetailsDAO().saveOrUpdate(sellerDtl);
				
				List<RTFSellerDetails> sellers = EcommDAORegistry.getrTFSellerDetailsDAO().getAllRTFSellerDetails(filter,status);
				responseDTO.setRtfSellers(sellers);
				int size = 0;
				if(sellers != null) {
					size = sellers.size();
				}
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(size));
				responseDTO.setStatus(1);
				responseDTO.setMessage(msg);
				return responseDTO;
			}
			responseDTO.setStatus(0);
			responseDTO.setMessage("Could not able to identify action.");
			return responseDTO;
			
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while fetching Seller Details.");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}
	
	@RequestMapping(value="/GetProdOrderManage")
	public OrderResp getProdOrderManagement(HttpServletRequest request, HttpServletResponse response){
		OrderResp responseDTO = new OrderResp();
		Error error = new Error();
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String status = request.getParameter("status");
			String userIdStr = request.getParameter("userId");
			String isSellerStr = request.getParameter("isSeller");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getRtfProdOrdersFilter(headerFilter);
			if(status==null || status.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please send valid status.");
				responseDTO.setError(error);
				return responseDTO;
			}
			Boolean isSeller = false;
			if(isSellerStr != null && !isSellerStr.isEmpty()) {
				try {
					isSeller = Boolean.parseBoolean(isSellerStr);
					
				} catch(Exception e) {
					
				}
			}
			Integer trUserId= null;
			if(isSeller) {
				try {
					trUserId = Integer.parseInt(userIdStr);
				}catch (Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Invalid TFF User Id");
					responseDTO.setError(error);
					return responseDTO;
				}
			}
			
			List<CustomerOrder> orders = EcommDAORegistry.getCustomerProdOrderDAO().getAllRTFProdOrderDetails(filter, status, trUserId);
			responseDTO.setOrders(orders);
			int size = 0;
			if(orders != null) {
				size = orders.size();
			}
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(size));
			responseDTO.setStatus(1);
			if(orders.isEmpty()){
				responseDTO.setStatus(1);
				responseDTO.setMessage("No Orders found.");
				return responseDTO;
			}
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while fetching seller orders.");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}
	@RequestMapping(value="/GetCustOrderProducts")
	public OrderResp getCustOrderProducts(HttpServletRequest request, HttpServletResponse response){
		OrderResp responseDTO = new OrderResp();
		Error error = new Error();
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String status = request.getParameter("status");
			String sellerIdStr = request.getParameter("sellerId");
			String orderIdStr = request.getParameter("orderId");
			String userIdStr = request.getParameter("userId");
			String isSellerStr = request.getParameter("isSeller");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getRtfSellerFilter(headerFilter);
			if(status==null || status.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please send valid status.");
				responseDTO.setError(error);
				return responseDTO;
			}
			Boolean isSeller = false;
			if(isSellerStr != null && !isSellerStr.isEmpty()) {
				try {
					isSeller = Boolean.parseBoolean(isSellerStr);
					
				} catch(Exception e) {
					
				}
			}
			Integer trUserId= null;
			if(isSeller) {
				try {
					trUserId = Integer.parseInt(userIdStr);
				}catch (Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Invalid TFF User Id");
					responseDTO.setError(error);
					return responseDTO;
				}
			}
			Integer orderId= null;
			try {
				orderId = Integer.parseInt(orderIdStr);
			}catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Invalid Order Id");
				responseDTO.setError(error);
				return responseDTO;
			}
			
			List<OrderProducts> prodsList = EcommDAORegistry.getOrderProductSetDAO().getAllRtfOrderProductDetails(filter, status, trUserId, orderId);
			responseDTO.setProds(prodsList);
			int size = 0;
			if(prodsList != null) {
				size = prodsList.size();
			}
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(size));
			responseDTO.setStatus(1);
			if(prodsList != null && prodsList.isEmpty()){
				responseDTO.setStatus(1);
				responseDTO.setMessage("No Products found.");
				return responseDTO;
			}
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while fetching seller order products.");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}
	@RequestMapping(value="/GetOrderProduct")
	public OrderResp getOrderProduct(HttpServletRequest request, HttpServletResponse response){
		OrderResp responseDTO = new OrderResp();
		Error error = new Error();
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String status = request.getParameter("status");
			String sellerIdStr = request.getParameter("sellerId");
			String orderProdIdStr = request.getParameter("orderProdId");
			String userIdStr = request.getParameter("userId");
			String isSellerStr = request.getParameter("isSeller");
			
			if(status==null || status.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please send valid status.");
				responseDTO.setError(error);
				return responseDTO;
			}
			Boolean isSeller = false;
			if(isSellerStr != null && !isSellerStr.isEmpty()) {
				try {
					isSeller = Boolean.parseBoolean(isSellerStr);
					
				} catch(Exception e) {
					
				}
			}
			Integer trUserId= null;
			if(isSeller) {
				try {
					trUserId = Integer.parseInt(userIdStr);
				}catch (Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Invalid TFF User Id");
					responseDTO.setError(error);
					return responseDTO;
				}
			}
			Integer orderProdId= null;
			try {
				orderProdId = Integer.parseInt(orderProdIdStr);
			}catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Invalid Order Product Id");
				responseDTO.setError(error);
				return responseDTO;
			}
			OrderProducts orderProduct = EcommDAORegistry.getOrderProductSetDAO().get(orderProdId);
			if(orderProduct == null) {
				responseDTO.setStatus(0);
				error.setDescription("Invalid Order Product Id");
				responseDTO.setError(error);
				return responseDTO;
			}
			CustomerOrder custOrder = EcommDAORegistry.getCustomerProdOrderDAO().getACustomerOrderByOrderId(orderProduct.getOrderId());
			SellerProdItemsVariInventory inventory = EcommDAORegistry.getSellerProdItemsVariInventoryDAO().getInventoryByInventoryId(orderProduct.getSpiId());
			if(inventory != null) {
				orderProduct.setvOptNameValCom(inventory.getvOptNameValCom());
			}
			responseDTO.setOrderProduct(orderProduct);
			responseDTO.setInventory(inventory);
			responseDTO.setOrder(custOrder);
			responseDTO.setStatus(1);
			
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while fetching order product.");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}
	
	@RequestMapping(value="/UpdateOrderProductShpipping")
	public OrderResp updateOrderProductShpipping(HttpServletRequest request, HttpServletResponse response){
		OrderResp responseDTO = new OrderResp();
		Error error = new Error();
		try {
			String orderProdIdStr = request.getParameter("orderProdId");
			String delNote = request.getParameter("delNote");
			String shMethod = request.getParameter("shMethod");
			String shClass = request.getParameter("shClass");
			String shTrackId = request.getParameter("shTrackId");
			String expDelDateStr = request.getParameter("expDelDate");
			String orderIDStr = request.getParameter("orderId");
			String status = request.getParameter("status");
			String userIdStr = request.getParameter("userId");
			String isSellerStr = request.getParameter("isSeller");
			//String prodDelivered = request.getParameter("prodDelivered");
			
			Integer orderProdId= null;
			try {
				orderProdId = Integer.parseInt(orderProdIdStr);
			}catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Invalid Order Product Id");
				responseDTO.setError(error);
				return responseDTO;
			}
			Integer orderId= null;
			try {
				orderId = Integer.parseInt(orderIDStr);
			}catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Invalid Order Id");
				responseDTO.setError(error);
				return responseDTO;
			}
			Boolean isSeller = false;
			if(isSellerStr != null && !isSellerStr.isEmpty()) {
				try {
					isSeller = Boolean.parseBoolean(isSellerStr);
					
				} catch(Exception e) {
					
				}
			}
			Integer trUserId= null;
			if(isSeller) {
				try {
					trUserId = Integer.parseInt(userIdStr);
				}catch (Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Invalid TFF User Id");
					responseDTO.setError(error);
					return responseDTO;
				}
			}
			OrderProducts orderProduct = EcommDAORegistry.getOrderProductSetDAO().get(orderProdId);
			if(orderProduct == null) {
				responseDTO.setStatus(0);
				error.setDescription("Invalid Order Product Id");
				responseDTO.setError(error);
				return responseDTO;
			}
			Date expDelDate = null;
			if(expDelDateStr!=null && !expDelDateStr.isEmpty()){
				try {
					expDelDate = dateFormat.parse(expDelDateStr);
				} catch( Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Invalid Expected Delivery Date");
					responseDTO.setError(error);
					return responseDTO;
				}
			}
			if(shMethod != null && !shMethod.isEmpty() ) {
				orderProduct.setShMethod(shMethod);
			}
			if(shClass != null && !shClass.isEmpty() ) {
				orderProduct.setShClass(shClass);
			}
			if(shTrackId != null && !shTrackId.isEmpty() ) {
				if(orderProduct.getShTrackId() == null || !orderProduct.getShTrackId().equals(shTrackId)) {
					orderProduct.setIsTrackingIdUpdated(true);	
				}
				orderProduct.setShTrackId(shTrackId);
				
			}
			if(delNote != null && !delNote.isEmpty() ) {
				orderProduct.setDelNote(delNote);
			}
			/*if(prodDelivered != null && !prodDelivered.isEmpty() ) {
				orderProduct.setShippingStatus("DELIVERED");
				orderProduct.setStatus("FULFILLED");
			}*/
			if(expDelDate != null) {
				orderProduct.setExpDelDate(expDelDate);
			}
			EcommDAORegistry.getOrderProductSetDAO().update(orderProduct);
			
			/*List<OrderProducts> orderProducts = EcommDAORegistry.getOrderProductSetDAO().getOrderProductByOrderId(orderId);
			String orderStatus = EcommUtil.computeOrderProductStatus(orderProducts);
			if(!orderStatus.equals("ACTIVE")) {
				CustomerOrder orderObj = EcommDAORegistry.getCustomerProdOrderDAO().getACustomerOrderByOrderId(orderId);
				if(!orderObj.getStatus().equalsIgnoreCase(orderStatus)) {
					orderObj.setStatus(orderStatus);
					EcommDAORegistry.getCustomerProdOrderDAO().update(orderObj);
				}
			}*/
			
			List<OrderProducts> prodsList = EcommDAORegistry.getOrderProductSetDAO().getAllRtfOrderProductDetails(null, status, trUserId, orderId);
			responseDTO.setProds(prodsList);
			int size = 0;
			if(prodsList != null) {
				size = prodsList.size();
			}
			responseDTO.setProds(prodsList);
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(size));
			
			responseDTO.setStatus(1);
			responseDTO.setMessage("Shipping Details Updated Successfully.");
			
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while updating order product shipping.");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}
	
	@RequestMapping(value="/UpdateOrderProductToDelivered")
	public OrderResp updatepdDelivered(HttpServletRequest request, HttpServletResponse response){
		OrderResp responseDTO = new OrderResp();
		Error error = new Error();
		try {
			
			String orderProdIdStr = request.getParameter("orderProdId");
			String orderIDStr = request.getParameter("orderId");
			String delDate = request.getParameter("delDate");
			String status = request.getParameter("status");
			String userIdStr = request.getParameter("userId");
			String isSellerStr = request.getParameter("isSeller");
			String action = request.getParameter("action");
			String isimgupd =  request.getParameter("isimgupd");//TRUE if image changed
			
			Integer orderProdId= null;
			try {
				orderProdId = Integer.parseInt(orderProdIdStr);
			}catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Invalid Order Product Id");
				responseDTO.setError(error);
				return responseDTO;
			}
			
			Date expDelDate = null;
			try {
				expDelDate = dateFormat.parse(delDate);
			} catch( Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Invalid Delivery Date");
				responseDTO.setError(error);
				return responseDTO;
			}
			
			Integer orderId= null;
			try {
				orderId = Integer.parseInt(orderIDStr);
			}catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Invalid Order Id");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(action == null || action.isEmpty()) {
				responseDTO.setStatus(0);
				error.setDescription("Action Type is Mandatory.");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(!action.equals("ACCEPTED") && !action.equals("REJECTED") && !action.equals("FULFILLED")) {
				responseDTO.setStatus(0);
				error.setDescription("Invalid Action Type.");
				responseDTO.setError(error);
				return responseDTO;
			}
			Boolean isSeller = false;
			if(isSellerStr != null && !isSellerStr.isEmpty()) {
				try {
					isSeller = Boolean.parseBoolean(isSellerStr);
					
				} catch(Exception e) {
					
				}
			}
			Integer trUserId= null;
			if(isSeller) {
				try {
					trUserId = Integer.parseInt(userIdStr);
				}catch (Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Invalid TFF User Id");
					responseDTO.setError(error);
					return responseDTO;
				}
			}
			OrderProducts orderProduct = EcommDAORegistry.getOrderProductSetDAO().get(orderProdId);
			if(orderProduct == null) {
				responseDTO.setStatus(0);
				error.setDescription("Invalid Order Product Id");
				responseDTO.setError(error);
				return responseDTO;
			}
			
			//FileUploadGenericDTO upldto = new FileUploadGenericDTO();
			if("TRUE".equals(isimgupd)) {
				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
				MultipartFile file = multipartRequest.getFile("deliveryImage");
				if(file==null || file.getSize() <= 0){
					responseDTO.setStatus(0);
					error.setDescription("Selected file is invalid or empty.");
					responseDTO.setError(error);
					return responseDTO;
				}
				
				File newFile = null;
				String ext = FilenameUtils.getExtension(file.getOriginalFilename());
				String fullFileName =  orderProduct.getProdType().trim()+"_"+orderProduct.getId()+"."+ext;
				newFile = new File(System.getProperty("java.io.tmpdir")+"/"+fullFileName);
				if(!newFile.exists()){
					newFile.createNewFile();
				}
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(newFile));
		        FileCopyUtils.copy(file.getInputStream(), stream);
				stream.close();
				System.out.println("UPLOAD FILE SIZE : "+newFile.length());
				
				GenericResponseDTO genResponseDTO = FileUploadUtil.uploadGCAssets( "PGCOD" ,  newFile ,  Constants.BASE_URL) ;
				if(genResponseDTO.getStatus() != 1) {
					error.setDescription(genResponseDTO.getMessage());
					responseDTO.setStatus(0);
					return responseDTO;						
				}
				
				AwsS3Response awsRsponse = AWSFileService.upLoadFileToS3(AWSFileService.BUCKET_RTF_ASSETS, fullFileName, AWSFileService.RTFPROD_FOLDER, newFile );
				if(null != awsRsponse && awsRsponse.getStatus() != 1){
					responseDTO.setStatus(0);
					error.setDescription("Error occured while uploading file to S3.");
					responseDTO.setError(error);
					return responseDTO;
				}
				orderProduct.setDeliveredImgUrl(fullFileName);
				
				/*String s3bucket = RTFEcommAppCache.s3staticbucket;
				String s3moduleFolder = RTFEcommAppCache.s3orddeliveredfolder;
				upldto =  ASWS3ImageUploaderUtil.uploadMultipartFileToS3(null , request, upldto ,s3bucket ,s3moduleFolder );
				//upldto =  ASWS3ImageUploaderUtil.uploadFileToS3(null , request, upldto ,s3bucket ,s3moduleFolder );		
				if(upldto.getSts() != 1)  {
					//resMsg = "Error Uploading Image  :"+upldto.getMsg();
					responseDTO.setStatus(0);
					error.setDescription("Error Uploading Image.");
					responseDTO.setError(error);
					return responseDTO;			
				}
				if(EcommUtil.isNullOrEmpty(upldto.getAwsfinalUrl())){
					//resMsg = "Error Uploading Image  :"+upldto.getMsg();
					responseDTO.setStatus(0);
					error.setDescription("Error Uploading Image.");
					responseDTO.setError(error);
					return responseDTO;
				}*/
				
			}
			if(orderProduct.getShMethod() ==   null|| orderProduct.getShMethod().isEmpty() || orderProduct.getShMethod().equalsIgnoreCase("Not assigned yet")){
				orderProduct.setShMethod("Fulfilled");
			}
			if(orderProduct.getShTrackId() ==   null|| orderProduct.getShTrackId().isEmpty() || orderProduct.getShTrackId().equalsIgnoreCase("Not assigned yet")){
				orderProduct.setShTrackId("Fulfilled");
			}
			orderProduct.setShippingStatus("DELIVERED");
			orderProduct.setShClass("DELIVERED");
			orderProduct.setExpDelDate(expDelDate);
			orderProduct.setStatus(action);
			EcommDAORegistry.getOrderProductSetDAO().update(orderProduct);
			
			/*List<OrderProducts> orderProducts = EcommDAORegistry.getOrderProductSetDAO().getOrderProductByOrderId(orderId);
			String orderStatus = EcommUtil.computeOrderProductStatus(orderProducts);
			if(!orderStatus.equals("ACTIVE")) {
				CustomerOrder orderObj = EcommDAORegistry.getCustomerProdOrderDAO().getACustomerOrderByOrderId(orderId);
				if(!orderObj.getStatus().equalsIgnoreCase(orderStatus)) {
					orderObj.setStatus(orderStatus);
					EcommDAORegistry.getCustomerProdOrderDAO().update(orderObj);
				}
			}*/
			
			List<OrderProducts> prodsList = EcommDAORegistry.getOrderProductSetDAO().getAllRtfOrderProductDetails(null, status, trUserId, orderId);
			responseDTO.setProds(prodsList);
			int size = 0;
			if(prodsList != null) {
				size = prodsList.size();
			}
			responseDTO.setProds(prodsList);
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(size));
			
			responseDTO.setMessage("Product Updated As Delivered Successfully.");
			responseDTO.setStatus(1);
			return responseDTO;
			
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while updating product to delivered.");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}
	
	@RequestMapping(value="/UpdateOrderProductStatus")
	public OrderResp updateOrderProductStatus(HttpServletRequest request, HttpServletResponse response){
		OrderResp responseDTO = new OrderResp();
		Error error = new Error();
		try {
			String orderProdIdStr = request.getParameter("orderProdId");
			String orderIDStr = request.getParameter("orderId");
			String status = request.getParameter("status");
			String userIdStr = request.getParameter("userId");
			String isSellerStr = request.getParameter("isSeller");
			String action = request.getParameter("action");
			
			Integer orderProdId= null;
			try {
				orderProdId = Integer.parseInt(orderProdIdStr);
			}catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Invalid Order Product Id");
				responseDTO.setError(error);
				return responseDTO;
			}
			Integer orderId= null;
			try {
				orderId = Integer.parseInt(orderIDStr);
			}catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Invalid Order Id");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(action == null || action.isEmpty()) {
				responseDTO.setStatus(0);
				error.setDescription("Action Type is Mandatory.");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(!action.equals("ACCEPTED") && !action.equals("REJECTED") && !action.equals("FULFILLED") 
					&& !action.equals("FULFILL_TO_ACCEPT") && !action.equals("ACCEPT_TO_ACTIVE")) {
				responseDTO.setStatus(0);
				error.setDescription("Invalid Action Type.");
				responseDTO.setError(error);
				return responseDTO;
			}
			Boolean isSeller = false;
			if(isSellerStr != null && !isSellerStr.isEmpty()) {
				try {
					isSeller = Boolean.parseBoolean(isSellerStr);
					
				} catch(Exception e) {
					
				}
			}
			Integer trUserId= null;
			if(isSeller) {
				try {
					trUserId = Integer.parseInt(userIdStr);
				}catch (Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Invalid TFF User Id");
					responseDTO.setError(error);
					return responseDTO;
				}
			}
			CustomerOrder order = EcommDAORegistry.getCustomerProdOrderDAO().get(orderId);
			if(order == null) {
				responseDTO.setStatus(0);
				error.setDescription("Invalid Order Id");
				responseDTO.setError(error);
				return responseDTO;
			}
			OrderProducts orderProduct = EcommDAORegistry.getOrderProductSetDAO().get(orderProdId);
			if(orderProduct == null) {
				responseDTO.setStatus(0);
				error.setDescription("Invalid Order Product Id");
				responseDTO.setError(error);
				return responseDTO;
			}
			String respMsg = "";
			if(action.equals("ACCEPTED")) {
				
				orderProduct.setStatus("ACCEPTED");
				respMsg = "Product Accepted Successfully.";
				
			} else if(action.equals("REJECTED")) {
				
				orderProduct.setStatus("REJECTED");
				respMsg = "Product Rejected Successfully.";
				
			} else if(action.equals("FULFILLED")) {
				
				orderProduct.setStatus("FULFILLED");
				orderProduct.setShippingStatus("DELIVERED");
				respMsg = "Product Updated As Delivered Successfully.";
				
			} else if(action.equals("FULFILL_TO_ACCEPT")) {
				if(orderProduct.getStatus() == null || !orderProduct.getStatus().equals("FULFILLED")) {
					responseDTO.setStatus(0);
					error.setDescription("Fulfilled Products only can be moved back to Open Orders.");
					responseDTO.setError(error);
					return responseDTO;
				}
				orderProduct.setStatus("ACCEPTED");
				orderProduct.setIsDelEmailSent(false);
				orderProduct.setShippingStatus(EcommUtil.calculateShippingStatus(order.getCrDate()));
				
				respMsg = "Product Moved back to Open Orders from Fulfilled Orders Successfully.";
				
			} else if(action.equals("ACCEPT_TO_ACTIVE")) {
				if(orderProduct.getStatus() == null || !orderProduct.getStatus().equals("ACCEPTED")) {
					responseDTO.setStatus(0);
					error.setDescription("Accepted Products only can be moved back to New Orders.");
					responseDTO.setError(error);
					return responseDTO;
				}
				
				orderProduct.setStatus("ACTIVE");
				respMsg = "Product Moved back to New Orders from Open Orders Successfully.";
			} 
			//orderProduct.setStatus(action);
			EcommDAORegistry.getOrderProductSetDAO().update(orderProduct);
			
			/*List<OrderProducts> orderProducts = EcommDAORegistry.getOrderProductSetDAO().getOrderProductByOrderId(orderId);
			String orderStatus = EcommUtil.computeOrderProductStatus(orderProducts);
			if(!orderStatus.equals("ACTIVE")) {
				CustomerOrder orderObj = EcommDAORegistry.getCustomerProdOrderDAO().getACustomerOrderByOrderId(orderId);
				if(!orderObj.getStatus().equalsIgnoreCase(orderStatus)) {
					orderObj.setStatus(orderStatus);
					EcommDAORegistry.getCustomerProdOrderDAO().update(orderObj);
				}
			}*/
			List<OrderProducts> prodsList = EcommDAORegistry.getOrderProductSetDAO().getAllRtfOrderProductDetails(null, status, trUserId, orderId);
			responseDTO.setProds(prodsList);
			int size = 0;
			if(prodsList != null) {
				size = prodsList.size();
			}
			responseDTO.setProds(prodsList);
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(size));
			responseDTO.setMessage(respMsg);
			responseDTO.setStatus(1);
			
			
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while updating order product shipping.");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}
	@RequestMapping(value="/VoidOrderProduct")
	public OrderResp voidOrderProduct(HttpServletRequest request, HttpServletResponse response){
		OrderResp responseDTO = new OrderResp();
		Error error = new Error();
		try {
			String orderProdIdStr = request.getParameter("orderProdId");
			String orderIDStr = request.getParameter("orderId");
			String status = request.getParameter("status");
			String userIdStr = request.getParameter("userId");
			String isSellerStr = request.getParameter("isSeller");
			String refundAmtStr = request.getParameter("refundAmt");
			String userName = request.getParameter("userName");
			
			Integer orderProdId= null;
			try {
				orderProdId = Integer.parseInt(orderProdIdStr);
			}catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Invalid Order Product Id");
				responseDTO.setError(error);
				return responseDTO;
			}
			Integer orderId= null;
			try {
				orderId = Integer.parseInt(orderIDStr);
			}catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Invalid Order Id");
				responseDTO.setError(error);
				return responseDTO;
			}
			Double refundAmt= null;
			try {
				refundAmt = Double.valueOf(refundAmtStr);
			}catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Invalid Refund Amount");
				responseDTO.setError(error);
				return responseDTO;
			}
			Boolean isSeller = false;
			if(isSellerStr != null && !isSellerStr.isEmpty()) {
				try {
					isSeller = Boolean.parseBoolean(isSellerStr);
					
				} catch(Exception e) {
					
				}
			}
			Integer trUserId= null;
			if(isSeller) {
				responseDTO.setStatus(0);
				error.setDescription("You Dont have access to void product.");
				responseDTO.setError(error);
				return responseDTO;
				/*try {
					trUserId = Integer.parseInt(userIdStr);
				}catch (Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Invalid TFF User Id");
					responseDTO.setError(error);
					return responseDTO;
				}*/
			}
			CustomerOrder order = EcommDAORegistry.getCustomerProdOrderDAO().getACustomerOrderByOrderId(orderId);
			if(order == null) {
				responseDTO.setStatus(0);
				error.setDescription("Invalid Order Id");
				responseDTO.setError(error);
				return responseDTO;
			}
			
			OrderProducts orderProduct = EcommDAORegistry.getOrderProductSetDAO().get(orderProdId);
			if(orderProduct == null || (orderProduct.getStatus() != null && !orderProduct.getStatus().equals("ACCEPTED") && !orderProduct.getStatus().equals("REJECTED"))) {
				responseDTO.setStatus(0);
				error.setDescription("Invalid Order Product Id");
				responseDTO.setError(error);
				return responseDTO;
			}
			
			String errorMsg = "";
			if(refundAmt > 0 ) {
				Map<String, String> paramMap = Util.getParameterMap(request);
				paramMap.put("orderId", String.valueOf(orderId));
				paramMap.put("orderProdId", String.valueOf(orderId));
				paramMap.put("refundType", order.getpPayType());
				paramMap.put("refundAmount",String.valueOf(Util.formatDoubleToTwoDecimalPoint(refundAmt)));
				
				String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+Constants.RTF_ORDER_PROD_REFUND);
				Gson gson = Util.getGsonBuilder().create();	
				JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
				RtfOrderRefundResponse rtfOrderRefundResponse = gson.fromJson(((JsonObject)jsonObject.get("rtfOrderRefundResponse")), RtfOrderRefundResponse.class);
				
				RtfOrderRefund rtfOrderRefund = null;
				if(rtfOrderRefundResponse == null){
					System.err.println("ORder Refund API not available.");
					error.setDescription("ORder Refund API not available.");
					responseDTO.setError(error);
					responseDTO.setStatus(0);
					return responseDTO;
				}else if(rtfOrderRefundResponse.getStatus()==1){
					rtfOrderRefund = rtfOrderRefundResponse.getRtfORderRefund();
					if(rtfOrderRefund != null){
						rtfOrderRefund.setUpdBy(userName);
					}
				}else{
					errorMsg = rtfOrderRefundResponse.getError().getMessage();
				}
				if(rtfOrderRefund == null) {
					error.setDescription("Error Occured While refunding Amount.");
					responseDTO.setError(error);
					responseDTO.setStatus(0);
					return responseDTO;
				}
				rtfOrderRefund.setDeductOrderEarnedPoints(0);
				rtfOrderRefund.setRefundLoyaltyPoints(orderProduct.getUsedLPnts());
				
				if(orderProduct.getUsedLPnts() != null && orderProduct.getUsedLPnts() > 0){
//					CustomerLoyaltyPointTranx loyaltyPointTrax = new CustomerLoyaltyPointTranx();
//					loyaltyPointTrax.setCrBy(cust.getUserId());
//					loyaltyPointTrax.setCrDate(new Date());
//					loyaltyPointTrax.setCustId(custId);
//					loyaltyPointTrax.setEarnPoints(0);
//					loyaltyPointTrax.setSpentPoints(loyaltyPoints);
//					loyaltyPointTrax.setBeforePoints(cust.getLoyaltyPoints());
//					loyaltyPointTrax.setAfterPoints(cust.getLoyaltyPoints()- loyaltyPoints);
//					loyaltyPointTrax.setStatus("PENDING");
//					loyaltyPointTrax.setOrderId(order.getId());
//					loyaltyPointTrax.setTranxType("PROD_ORDER");
//					
//					EcommDAORegistry.getCustomerLoyaltyPointTranxDAO().save(loyaltyPointTrax);
				}
			}
			orderProduct.setShippingStatus("VOIDED");
			orderProduct.setStatus("VOIDED");
			orderProduct.setRefundAmt(refundAmt);
			EcommDAORegistry.getOrderProductSetDAO().update(orderProduct);
			
			/*List<OrderProducts> orderProducts = EcommDAORegistry.getOrderProductSetDAO().getOrderProductByOrderId(orderId);
			String orderStatus = EcommUtil.computeOrderProductStatus(orderProducts);
			if(!orderStatus.equals("ACTIVE")) {
				CustomerOrder orderObj = EcommDAORegistry.getCustomerProdOrderDAO().getACustomerOrderByOrderId(orderId);
				if(!orderObj.getStatus().equalsIgnoreCase(orderStatus)) {
					orderObj.setStatus(orderStatus);
					EcommDAORegistry.getCustomerProdOrderDAO().update(orderObj);
				}
			}*/
			List<OrderProducts> prodsList = EcommDAORegistry.getOrderProductSetDAO().getAllRtfOrderProductDetails(null, status, trUserId, orderId);
			responseDTO.setProds(prodsList);
			int size = 0;
			if(prodsList != null) {
				size = prodsList.size();
			}
			responseDTO.setProds(prodsList);
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(size));
			
			responseDTO.setStatus(1);
			responseDTO.setMessage("Product Voided Successfully.");
			
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while updating order product shipping.");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}
	
	@RequestMapping(value="/getpditemaster")
	public SellerProdItmlDTO getPdItemMaster(HttpServletRequest request, HttpServletResponse response){
		SellerProdItmlDTO responseDTO = new SellerProdItmlDTO();
		Error error = new Error();
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String status = request.getParameter("status");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getRtfProductItemFilter(headerFilter);
			if(status==null || status.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please send valid status.");
				responseDTO.setError(error);
				return responseDTO;
			}
			List<SellerProductsItems> selProdItms = EcommDAORegistry.getSellerProductsItemsDAO().getAllRTFSellerDetails(filter,status);
			responseDTO.setList(selProdItms);
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(selProdItms.size()));
			responseDTO.setStatus(1);
			if(selProdItms.isEmpty()){
				responseDTO.setStatus(1);
				responseDTO.setMessage("No Products found.");
				return responseDTO;
			}
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while fetching Seller Products.");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}
	@RequestMapping(value="/updatepditemaster")
	public SellerProdItmlDTO updatePdIteMaster(HttpServletRequest request, HttpServletResponse response){
		SellerProdItmlDTO responseDTO = new SellerProdItmlDTO();
		Error error = new Error();
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String sellerprodidStr = request.getParameter("slrpitmsId");
			String action = request.getParameter("action");
			String userName = request.getParameter("userName"); 
			String status = request.getParameter("status");	
			String msg = "";
			String resMsg = "";
			GridHeaderFilters filter = GridHeaderFiltersUtil.getRtfSellerFilter(headerFilter);
			if(action==null || action.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Could not able to identify action to be perform.");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(action.equalsIgnoreCase("EDIT")){
				if(sellerprodidStr==null || sellerprodidStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Seller Product id is invalid.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer sellerProdId = 0;
				try {
					sellerProdId = Integer.parseInt(sellerprodidStr);
				} catch (Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Seller Product id is invalid.");
					responseDTO.setError(error);
					return responseDTO;
				}
				SellerProductsItems sellerPRodDtl = EcommDAORegistry.getSellerProductsItemsDAO().get(sellerProdId);
				if(sellerPRodDtl == null){
					responseDTO.setStatus(0);
					error.setDescription("Seller Product not found with given identifier.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Boolean isInventoryExists = EcommDAORegistry.getSellerProdItemsVariInventoryDAO().isProductItemHasInventory(sellerProdId);
				if(isInventoryExists) {
					responseDTO.setStatus(0);
					error.setDescription("Selcted product inventory already generated, Product is not allowed to Edit.");
					responseDTO.setError(error);
					return responseDTO;
				}
				responseDTO.setStatus(1);
				responseDTO.setSellerProductsItem(sellerPRodDtl);
				return responseDTO;
			}else if(action.equalsIgnoreCase("SAVE") || action.equalsIgnoreCase("UPDATE")){
				
				//String sellerprodidStr = request.getParameter("slrpitmsId");
				String sellerIdStr = request.getParameter("slerId");
				String pbrand = request.getParameter("pbrand");
				String pname = request.getParameter("pname");
				String pdesc = request.getParameter("pdesc");
				String plongdesc = request.getParameter("plongdesc");
				String dispOrderStr = request.getParameter("dispOrder");
				
				String prodType = request.getParameter("prodType");
				String isRwdPointProdStr = request.getParameter("isRwdPointProd");	
				String reqRwdPointStr = request.getParameter("reqRwdPoint");		
				
				String pselMinPrcstr = request.getParameter("pselMinPrc");
				String pregMinPrcstr = request.getParameter("pregMinPrc");	
				String maxcustqtystr = request.getParameter("maxcustqty");
				//String dispstartdatestr=request.getParameter("dispstartdate");
				String pexpdatestr = request.getParameter("pexpdate");
				String isimgupd =  request.getParameter("isimgupd");//TRUE if image changed
				
				String isVariantStr = request.getParameter("isVariant");
				String dispRegPriceStr = request.getParameter("dispRegPrice");
				String dispPerOffStr = request.getParameter("dispPerOff");
				
				if(EcommUtil.isNullOrEmpty(sellerIdStr)){
					resMsg = "Invalid Seller Id:"+sellerIdStr;
					responseDTO.setStatus(0);
					error.setDescription("Invalid Seller Id");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(EcommUtil.isNullOrEmpty(status)){
					resMsg = "Invalid status:"+sellerIdStr;
					responseDTO.setStatus(0);
					error.setDescription("Invalid status");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(EcommUtil.isNullOrEmpty(pname)){
					resMsg = "Invalid Product Name :"+pname;
					responseDTO.setStatus(0);
					error.setDescription("Invalid Product Name");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(EcommUtil.isNullOrEmpty(dispOrderStr)){
					resMsg = "Invalid Product display order :"+dispOrderStr;
					responseDTO.setStatus(0);
					error.setDescription("Invalid display order");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(EcommUtil.isNullOrEmpty(pdesc)){
					resMsg = "Invalid Product Description :"+pdesc;
					responseDTO.setStatus(0);
					error.setDescription("Invalid Product Description");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(EcommUtil.isNullOrEmpty(pselMinPrcstr)){
					resMsg = "Invalid Selling Price :"+pselMinPrcstr;
					responseDTO.setStatus(0);
					error.setDescription("Invalid Selling Price");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(EcommUtil.isNullOrEmpty(pregMinPrcstr)){
					resMsg = "Invalid Regular Selling Price :"+pregMinPrcstr;
					responseDTO.setStatus(0);
					error.setDescription("Invalid Regular Selling Price");
					responseDTO.setError(error);
					return responseDTO;
				}
				/*if(EcommUtil.isNullOrEmpty(psku)){
					resMsg = "Invalid Product sku :"+psku;
					responseDTO.setStatus(0);
					error.setDescription("Invalid Product sku");
					responseDTO.setError(error);
					return responseDTO;
				}*/
				if(EcommUtil.isNullOrEmpty(pexpdatestr)){
					resMsg = "Invalid Product Expiry Date :"+pexpdatestr;
					responseDTO.setStatus(0);
					error.setDescription("Invalid Product Expiry Date");
					responseDTO.setError(error);
					return responseDTO;
				}
				/*if(EcommUtil.isNullOrEmpty(isvariantstr)){
					resMsg = "Invalid Product Variant enabled  Flag :"+isvariantstr;
					responseDTO.setStatus(0);
					error.setDescription("Invalid Product Variant enabled  Flag");
					responseDTO.setError(error);
					return responseDTO;
				}*/
				
				if(EcommUtil.isNullOrEmpty(maxcustqtystr)){
					resMsg = "Invalid Max Cust Quantity :"+maxcustqtystr;
					responseDTO.setStatus(0);
					error.setDescription("Invalid Max Cust Quantity");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(EcommUtil.isNullOrEmpty(prodType)){
					resMsg = "Product type is mendatory"+prodType;
					responseDTO.setStatus(0);
					error.setDescription("Product type is mendatory");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(EcommUtil.isNullOrEmpty(isRwdPointProdStr)){
					resMsg = "Product pricing type is mendatory"+isRwdPointProdStr;
					responseDTO.setStatus(0);
					error.setDescription("Product pricing type is mendatory");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(EcommUtil.isNullOrEmpty(isVariantStr)){
					resMsg = "Product Variant value is mendatory"+isVariantStr;
					responseDTO.setStatus(0);
					error.setDescription("Product Variant value is mendatory");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(EcommUtil.isNullOrEmpty(dispRegPriceStr)){
					resMsg = "Display Regular price? is mendatory."+dispRegPriceStr;
					responseDTO.setStatus(0);
					error.setDescription("Display Regular price? is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(EcommUtil.isNullOrEmpty(dispPerOffStr)){
					resMsg = "Display Percentage Off? is mendatory."+dispPerOffStr;
					responseDTO.setStatus(0);
					error.setDescription("Display Percentage Off? is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(EcommUtil.isNullOrEmpty(reqRwdPointStr)){
					reqRwdPointStr = "0";
				}

				/*if(EcommUtil.isNullOrEmpty(dispstartdatestr)){
					resMsg = "Invalid Display Start Date :"+dispstartdatestr;
					responseDTO.setStatus(0);
					error.setDescription("Invalid Display Start Date");
					responseDTO.setError(error);
					return responseDTO;
				}*/
				Integer sellerId=null;
				Integer maxcustqty=0;
				Double pselMinPrc=0.0,pregMinPrc=0.0;
				Date dispstartdate = null,pexpdate=null;
				Boolean isVariant = false;
				Boolean isDispPerOff = false;
				Boolean isDispRegPrice = false;
				
				Integer dispOrder = 0;
				Boolean isRwdProd = false;
				Integer reqRwdPoint = 0;
				try {
					sellerId = new Integer(sellerIdStr);
				}catch(Exception ex) {
					resMsg = "Invalid Seller Id"+sellerIdStr;
					responseDTO.setStatus(0);
					error.setDescription("Invalid Seller Id");
					responseDTO.setError(error);
					return responseDTO;				
				}
				
				try {
					dispOrder = new Integer(dispOrderStr);
				}catch(Exception ex) {
					resMsg = "Invalid product display order"+dispOrder;
					responseDTO.setStatus(0);
					error.setDescription("Invalid product display order");
					responseDTO.setError(error);
					return responseDTO;				
				}
				
				try {
					isRwdProd = new Boolean(isRwdPointProdStr);
				}catch(Exception ex) {
					resMsg = "Invalid product pricing type"+isRwdPointProdStr;
					responseDTO.setStatus(0);
					error.setDescription("Invalid product pricing type");
					responseDTO.setError(error);
					return responseDTO;				
				}
				
				try {
					reqRwdPoint = new Integer(reqRwdPointStr);
				}catch(Exception ex) {
					resMsg = "Invalid product required reward points"+reqRwdPointStr;
					responseDTO.setStatus(0);
					error.setDescription("Invalid product required reward points");
					responseDTO.setError(error);
					return responseDTO;				
				}
				
				try {
					maxcustqty = new Integer(maxcustqtystr);
				}catch(Exception ex) {
					resMsg = "Invalid Max Cust Quantity :"+maxcustqtystr;
					responseDTO.setStatus(0);
					error.setDescription("Invalid Max Cust Quantity");
					responseDTO.setError(error);
					return responseDTO;				
				}
				
				try {
					pselMinPrc = new Double(pselMinPrcstr);
				}catch(Exception ex) {
					resMsg = "Invalid selling priice :"+pselMinPrcstr;
					responseDTO.setStatus(0);
					error.setDescription("Invalid selling priice");
					responseDTO.setError(error);
					return responseDTO;					
				}
				try {
					pregMinPrc = new Double(pregMinPrcstr);
				}catch(Exception ex) {
					resMsg = "Invalid Regular selling priice :"+pregMinPrcstr;
					responseDTO.setStatus(0);
					error.setDescription("Invalid Regular selling priice");
					responseDTO.setError(error);
					return responseDTO;				
				}
				DateFormat formatDate = new SimpleDateFormat("MM/dd/yyyy");
				
				/*try {
					 dispstartdate = formatDate.parse(dispstartdatestr);
				}catch(Exception ex) {
					resMsg = "Invalid Display Start Date :"+dispstartdatestr;
					responseDTO.setStatus(0);
					error.setDescription("Invalid Display Start Date");
					responseDTO.setError(error);
					return responseDTO;					
				}*/
				try {
					 pexpdate = formatDate.parse(pexpdatestr);				
				}catch(Exception ex) {
					resMsg = "Invalid Product Expirty Date :"+pregMinPrcstr;
					responseDTO.setStatus(0);
					error.setDescription("Invalid Product Expirty Date");
					responseDTO.setError(error);
					return responseDTO;				
				}
				
				try {
					isDispPerOff = Boolean.parseBoolean(dispPerOffStr);
					isDispRegPrice = Boolean.parseBoolean(dispRegPriceStr);
				}catch(Exception ex) {
					resMsg = "Display Regular price/Percentage off? is mendatory"+pregMinPrcstr;
					responseDTO.setStatus(0);
					error.setDescription("Display Regular price/Percentage off? is mendatory");
					responseDTO.setError(error);
					return responseDTO;				
				}
				
				try {
					isVariant = Boolean.valueOf(isVariantStr);				
				}catch(Exception ex) {
					resMsg = "Invalid product variant value:"+isVariantStr;
					responseDTO.setStatus(0);
					error.setDescription("Invalid product variant value.");
					responseDTO.setError(error);
					return responseDTO;					
				}
				
				if(!isRwdProd){
					reqRwdPoint = 0;
				}
				if(isRwdProd && (pselMinPrc > 0 || pregMinPrc > 0)){
					resMsg = "When product pricing type is reward point regular price and sell price should be 0."+isRwdProd;
					responseDTO.setStatus(0);
					error.setDescription("When product pricing type is reward point regular price and sell price should be 0.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(isRwdProd && reqRwdPoint <= 0){
					resMsg = "When product pricing type is reward point than required reward points should be greater than 0."+isRwdProd;
					responseDTO.setStatus(0);
					error.setDescription("When product pricing type is reward point than required reward points should be greater than 0.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(!isRwdProd && pselMinPrc <= 0){
					resMsg = "When product pricing type is regular product sell price should be greater than 0."+isRwdProd;
					responseDTO.setStatus(0);
					error.setDescription("When product pricing type is regular product sell price should be greater than 0.");
					responseDTO.setError(error);
					return responseDTO;
				}
				
				
				SellerProductsItems sellerPRodDtl = null;
				if(action.equalsIgnoreCase("UPDATE")){
					if(EcommUtil.isNullOrEmpty(sellerprodidStr)){
						responseDTO.setStatus(0);
						error.setDescription("Invalid Seller Product Id");
						responseDTO.setError(error);
						return responseDTO;
					}
					Integer sellerprodid = 0;
					try {
						sellerprodid = Integer.parseInt(sellerprodidStr);
					} catch (Exception e) {
						responseDTO.setStatus(0);
						error.setDescription("Seller Product id is invalid.");
						responseDTO.setError(error);
						return responseDTO;
					}
					sellerPRodDtl = EcommDAORegistry.getSellerProductsItemsDAO().get(sellerprodid);
					if(sellerPRodDtl == null){
						responseDTO.setStatus(0);
						error.setDescription("Seller Product not found with given identifier.");
						responseDTO.setError(error);
						return responseDTO;
					}
					Boolean isInventoryExists = EcommDAORegistry.getSellerProdItemsVariInventoryDAO().isProductItemHasInventory(sellerprodid);
					if(isInventoryExists) {
						responseDTO.setStatus(0);
						error.setDescription("Selcted product inventory already generated, Product is not allowed to update.");
						responseDTO.setError(error);
						return responseDTO;
					}
					sellerPRodDtl.setUpdBy(userName);
					sellerPRodDtl.setUpdDate(new Date());
					msg = "Selected Seller Product updated successfully.";
				}else if(action.equalsIgnoreCase("SAVE")){
					/*SuperFanLevels oldLevel = QuizDAORegistry.getSuperFanLevelsDAO().getSuperFanLevelByLeveNo(levelNo);
					if(oldLevel !=null){
						responseDTO.setStatus(0);
						error.setDescription("Provided super fan level no is laready configured, Cannot have multiple records with same level no.");
						responseDTO.setError(error);
						return responseDTO;
					}*/
					sellerPRodDtl = new SellerProductsItems();
					sellerPRodDtl.setUpdBy(userName);
					sellerPRodDtl.setUpdDate(new Date());
					sellerPRodDtl.setStatus("ACTIVE");
					msg = "Selected Seller Product saved successfully.";
				}
				
				FileUploadGenericDTO upldto = new FileUploadGenericDTO();
				if("TRUE".equals(isimgupd)) {
					String s3bucket = RTFEcommAppCache.s3staticbucket;
					String s3moduleFolder = RTFEcommAppCache.s3inventoryoductsfolder;
					upldto =  ASWS3ImageUploaderUtil.uploadMultipartFileToS3(null , request, upldto ,s3bucket ,s3moduleFolder );
					//upldto =  ASWS3ImageUploaderUtil.uploadFileToS3(null , request, upldto ,s3bucket ,s3moduleFolder );		
					if(upldto.getSts() != 1)  {
						resMsg = "Error Uploading Image  :"+upldto.getMsg();
						responseDTO.setStatus(0);
						error.setDescription("Error Uploading Image.");
						responseDTO.setError(error);
						return responseDTO;			
					}
					if(EcommUtil.isNullOrEmpty(upldto.getAwsfinalUrl())){
						resMsg = "Error Uploading Image  :"+upldto.getMsg();
						responseDTO.setStatus(0);
						error.setDescription("Error Uploading Image.");
						responseDTO.setError(error);
						return responseDTO;
					}
				}
				
				
				
				
				
				//sellerPRodDtl.setSlrpitmsId(sellerprodid);
				sellerPRodDtl.setSellerId(sellerId);
				sellerPRodDtl.setpBrand(pbrand);
				sellerPRodDtl.setpName(pname);
				sellerPRodDtl.setpDesc(pdesc);
				sellerPRodDtl.setpLongDesc(plongdesc);
				sellerPRodDtl.setDispOrder(dispOrder);
				sellerPRodDtl.setpImgUrl(upldto.getAwsfinalUrl());
				//sellerPRodDtl.setPsku(psku);
				sellerPRodDtl.setStatus("PENDING");
				sellerPRodDtl.setProdType(prodType);
				sellerPRodDtl.setIsRwdPointProd(isRwdProd);
				sellerPRodDtl.setReqRwdPoint(reqRwdPoint);
				sellerPRodDtl.setIsVarient(isVariant);
				sellerPRodDtl.setPsMinPrice(pselMinPrc);
				sellerPRodDtl.setpRegMinPrice(pregMinPrc);
				sellerPRodDtl.setMaxCustQty(maxcustqty);
				//sellerPRodDtl.setDispStartdate(dispstartdate.getTime());
				sellerPRodDtl.setpExpDate(pexpdate);
				sellerPRodDtl.setIsDisplayRegularPrice(isDispRegPrice);
				sellerPRodDtl.setDispPerctageOff(isDispPerOff);
				
				EcommDAORegistry.getSellerProductsItemsDAO().saveOrUpdate(sellerPRodDtl);
				
				List<SellerProductsItems> selProdItms = EcommDAORegistry.getSellerProductsItemsDAO().getAllRTFSellerDetails(filter,status);
				responseDTO.setList(selProdItms);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(selProdItms.size()));
				responseDTO.setStatus(1);
				responseDTO.setMessage(msg);
				return responseDTO;
				
			}else if(action.equalsIgnoreCase("ACTIVE") || action.equalsIgnoreCase("EXPIRED")){
				if(EcommUtil.isNullOrEmpty(sellerprodidStr)){
					responseDTO.setStatus(0);
					error.setDescription("Invalid Seller Product Id");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(EcommUtil.isNullOrEmpty(status)){
					responseDTO.setStatus(0);
					error.setDescription("Invalid status");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer sellerprodid = 0;
				try {
					sellerprodid = Integer.parseInt(sellerprodidStr);
				} catch (Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Seller Product id is invalid.");
					responseDTO.setError(error);
					return responseDTO;
				}
				SellerProductsItems sellerPRodDtl = EcommDAORegistry.getSellerProductsItemsDAO().get(sellerprodid);
				if(sellerPRodDtl == null){
					responseDTO.setStatus(0);
					error.setDescription("Seller Product not found with given identifier.");
					responseDTO.setError(error);
					return responseDTO;
				}
				sellerPRodDtl.setUpdBy(userName);
				sellerPRodDtl.setUpdDate(new Date());
				sellerPRodDtl.setStatus(action);
				EcommDAORegistry.getSellerProductsItemsDAO().saveOrUpdate(sellerPRodDtl);
				
				if(action.equalsIgnoreCase("ACTIVE")){
					if(sellerPRodDtl.getIsVarient()==null || !sellerPRodDtl.getIsVarient()){
						responseDTO.setStatus(0);
						error.setDescription("Product does not have either variant added or inventory generated please check once.");
						responseDTO.setError(error);
						return responseDTO;
					}
					msg = "Product activated successfully.";
				}else if(action.equalsIgnoreCase("EXPIRED")){
					msg = "Product Expired successfully.";
				}
				
				List<SellerProductsItems> selProdItms = EcommDAORegistry.getSellerProductsItemsDAO().getAllRTFSellerDetails(filter,status);
				responseDTO.setList(selProdItms);
				int size = 0;
				if(selProdItms != null) {
					size = selProdItms.size();
				}
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(size));
				responseDTO.setStatus(1);
				responseDTO.setMessage(msg);
				return responseDTO;
			}else if(action.equalsIgnoreCase("DELETE")){
				if(EcommUtil.isNullOrEmpty(sellerprodidStr)){
					responseDTO.setStatus(0);
					error.setDescription("Invalid Seller Product Id");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(EcommUtil.isNullOrEmpty(status)){
					responseDTO.setStatus(0);
					error.setDescription("Invalid status");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer sellerprodid = 0;
				try {
					sellerprodid = Integer.parseInt(sellerprodidStr);
				} catch (Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Seller Product id is invalid.");
					responseDTO.setError(error);
					return responseDTO;
				}
				SellerProductsItems sellerPRodDtl = EcommDAORegistry.getSellerProductsItemsDAO().get(sellerprodid);
				if(sellerPRodDtl == null){
					responseDTO.setStatus(0);
					error.setDescription("Seller Product not found with given identifier.");
					responseDTO.setError(error);
					return responseDTO;
				}
				sellerPRodDtl.setUpdBy(userName);
				sellerPRodDtl.setUpdDate(new Date());
				sellerPRodDtl.setStatus("DELETED");
				EcommDAORegistry.getSellerProductsItemsDAO().saveOrUpdate(sellerPRodDtl);
				
				msg = "Seller deleted successfully.";
				
				List<SellerProductsItems> selProdItms = EcommDAORegistry.getSellerProductsItemsDAO().getAllRTFSellerDetails(filter,status);
				responseDTO.setList(selProdItms);
				int size = 0;
				if(selProdItms != null) {
					size = selProdItms.size();
				}
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(size));
				responseDTO.setStatus(1);
				responseDTO.setMessage(msg);
				return responseDTO;
			}
			responseDTO.setStatus(0);
			responseDTO.setMessage("Could not able to identify action.");
			return responseDTO;
			
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while fetching Seller Product Details.");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}

	@RequestMapping(value="/getproditmvaroptns")
	public SellerProdItmlDTO getproditmvaroptns(HttpServletRequest request, HttpServletResponse response) {
		SellerProdItmlDTO responseDTO = new SellerProdItmlDTO();
		Error error = new Error();
		try {
			String sellerprodidStr = request.getParameter("slrpitmsId");
			//String sellerIdStr = request.getParameter("slerId");
			
			Integer sellerProdId = 0;
			try {
				sellerProdId = Integer.parseInt(sellerprodidStr);
			} catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Seller Product id is invalid.");
				responseDTO.setError(error);
				return responseDTO;
			}
			SellerProductsItems sellerPRodDtl = EcommDAORegistry.getSellerProductsItemsDAO().get(sellerProdId);
			if(sellerPRodDtl == null){
				responseDTO.setStatus(0);
				error.setDescription("Seller Product not found with given identifier.");
				responseDTO.setError(error);
				return responseDTO;
			}
			List<SellerProdItemsVariName> variantNames = EcommDAORegistry.getSellerProdItemsVariNameDAO().getAllProductVariants(sellerProdId);
			for (SellerProdItemsVariName varNameObj : variantNames) {
				List<SellerProdItemsVariValue> varValues = EcommDAORegistry.getSellerProdItemsVariValueDAO().getOptionValues(varNameObj.getSlrpiVarId());
				varNameObj.setValues(varValues);
			}
			responseDTO.setVariantsList(variantNames);
			
			responseDTO.setStatus(1);
			if(responseDTO.getVariantsList() == null || responseDTO.getVariantsList().isEmpty()){
				responseDTO.setStatus(1);
				responseDTO.setMessage("No Variant options found.");
				return responseDTO;
			}
			List<SellerProdItemsVariInventory> inventoryList = EcommDAORegistry.getSellerProdItemsVariInventoryDAO().getInventoryByProductId(sellerProdId);
			if(inventoryList != null) {
				responseDTO.setHasInventory(true);
			}else {
				responseDTO.setHasInventory(false);
			}
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while fetching Products Variant options.");
			responseDTO.setError(error);
			return responseDTO;
		}
	}
	@RequestMapping(value="/genProdInventory")
	public SellerProdItmlDTO genProdInventory(HttpServletRequest request, HttpServletResponse response) {
		SellerProdItmlDTO responseDTO = new SellerProdItmlDTO();
		Error error = new Error();
		try {
			String sellerprodidStr = request.getParameter("slrpitmsId");
			String sellerIdStr = request.getParameter("slerId");
			String userName = request.getParameter("userName");
			
			Integer sellerProdId = 0;
			try {
				sellerProdId = Integer.parseInt(sellerprodidStr);
			} catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Seller Product id is invalid.");
				responseDTO.setError(error);
				return responseDTO;
			}
			Integer sellerId = 0;
			try {
				sellerId = Integer.parseInt(sellerIdStr);
			} catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Seller id is invalid.");
				responseDTO.setError(error);
				return responseDTO;
			}
			RTFSellerDetails seller = EcommDAORegistry.getrTFSellerDetailsDAO().get(sellerId);
			if(seller == null){
				responseDTO.setStatus(0);
				error.setDescription("Seller not found with given identifier.");
				responseDTO.setError(error);
				return responseDTO;
			}
			SellerProductsItems sellerPRodDtl = EcommDAORegistry.getSellerProductsItemsDAO().get(sellerProdId);
			if(sellerPRodDtl == null){
				responseDTO.setStatus(0);
				error.setDescription("Seller Product not found with given identifier.");
				responseDTO.setError(error);
				return responseDTO;
			}
			List<SellerProdItemsVariName> variantNames = EcommDAORegistry.getSellerProdItemsVariNameDAO().getAllProductVariants(sellerProdId);
			if(variantNames == null || variantNames.isEmpty()) {
				responseDTO.setStatus(0);
				error.setDescription("No Option Variants available.");
				responseDTO.setError(error);
				return responseDTO;
			}
			for (SellerProdItemsVariName varNameObj : variantNames) {
				List<SellerProdItemsVariValue> varValues = EcommDAORegistry.getSellerProdItemsVariValueDAO().getOptionValues(varNameObj.getSlrpiVarId());
				if(varValues == null || varValues.isEmpty()) {
					responseDTO.setStatus(0);
					error.setDescription("Product variant option should have atleast one value.");
					responseDTO.setError(error);
					return responseDTO;
				}
			}
			Boolean isInventoryExists = EcommDAORegistry.getSellerProdItemsVariInventoryDAO().isProductItemHasInventory(sellerProdId);
			if(isInventoryExists) {
				responseDTO.setStatus(0);
				error.setDescription("Inventory Already Exists for this Product.");
				responseDTO.setError(error);
				return responseDTO;
			}
			
			
			Integer responseCode = EcommDAORegistry.getSellerProdItemsVariInventoryDAO().generateSellerPRoductVariantMAtrix(sellerId, sellerProdId, userName);
			if(responseCode != 0) {
				responseDTO.setStatus(0);
				error.setDescription("Error occured in Products Inventory creation. ");
				responseDTO.setError(error);
				return responseDTO;
			}
			responseDTO.setStatus(1);
			responseDTO.setMessage("Product Inventroy generated successfully.");
			
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while generating Products Inventory.");
			responseDTO.setError(error);
			return responseDTO;
		}
	}
	@RequestMapping(value="/getProdInventories")
	public SellerProdItmlDTO getprodInventories(HttpServletRequest request, HttpServletResponse response) {
		SellerProdItmlDTO responseDTO = new SellerProdItmlDTO();
		Error error = new Error();
		try {
			String sellerprodidStr = request.getParameter("slrpitmsId");
			String sellerIdStr = request.getParameter("slerId");
			String userName = request.getParameter("userName");
			String headerFilter  = request.getParameter("headerFilter");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getRtfProductInventoryFilter(headerFilter);
			Integer sellerProdId = 0;
			try {
				sellerProdId = Integer.parseInt(sellerprodidStr);
			} catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Seller Product id is invalid.");
				responseDTO.setError(error);
				return responseDTO;
			}
			Integer sellerId = 0;
			try {
				sellerId = Integer.parseInt(sellerIdStr);
			} catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Seller id is invalid.");
				responseDTO.setError(error);
				return responseDTO;
			}
			RTFSellerDetails seller = EcommDAORegistry.getrTFSellerDetailsDAO().get(sellerId);
			if(seller == null){
				responseDTO.setStatus(0);
				error.setDescription("Seller not found with given identifier.");
				responseDTO.setError(error);
				return responseDTO;
			}
			SellerProductsItems sellerPRodDtl = EcommDAORegistry.getSellerProductsItemsDAO().get(sellerProdId);
			if(sellerPRodDtl == null){
				responseDTO.setStatus(0);
				error.setDescription("Seller Product not found with given identifier.");
				responseDTO.setError(error);
				return responseDTO;
			}
			List<SellerProdItemsVariInventory> inventoryList = EcommDAORegistry.getSellerProdItemsVariInventoryDAO().getAllInventoryByProductId(filter,sellerProdId);
			responseDTO.setInventoryList(inventoryList);
			int size = 0;
			if(inventoryList != null) {
				size = inventoryList.size();
			}
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(size));
			responseDTO.setStatus(1);
			//responseDTO.setMessage();
			
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while generating Products Inventory.");
			responseDTO.setError(error);
			return responseDTO;
		}
	}
	
	@RequestMapping(value="/updateProdInventories")
	public SellerProdItmlDTO updateProdInventories(HttpServletRequest request, HttpServletResponse response) {
		SellerProdItmlDTO responseDTO = new SellerProdItmlDTO();
		Error error = new Error();
		try {
			String sellerprodidStr = request.getParameter("slrpitmsId");
			String sellerIdStr = request.getParameter("slerId");
			String userName = request.getParameter("userName");
			
			Integer sellerProdId = 0;
			try {
				sellerProdId = Integer.parseInt(sellerprodidStr);
			} catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Seller Product id is invalid.");
				responseDTO.setError(error);
				return responseDTO;
			}
			Integer sellerId = 0;
			try {
				sellerId = Integer.parseInt(sellerIdStr);
			} catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Seller id is invalid.");
				responseDTO.setError(error);
				return responseDTO;
			}
			RTFSellerDetails seller = EcommDAORegistry.getrTFSellerDetailsDAO().get(sellerId);
			if(seller == null){
				responseDTO.setStatus(0);
				error.setDescription("Seller not found with given identifier.");
				responseDTO.setError(error);
				return responseDTO;
			}
			SellerProductsItems sellerPRodDtl = EcommDAORegistry.getSellerProductsItemsDAO().get(sellerProdId);
			if(sellerPRodDtl == null){
				responseDTO.setStatus(0);
				error.setDescription("Seller Product not found with given identifier.");
				responseDTO.setError(error);
				return responseDTO;
			}
			List<SellerProdItemsVariInventory> inventoryList = new ArrayList<SellerProdItemsVariInventory>();
			SellerProdItemsVariInventory inventoryObj = null;
			
			Map<String, String[]> requestParams = request.getParameterMap();
			for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
				String key = entry.getKey();
				if(key.contains("inv_")){
					
					Integer inventoryId = null;
					try {
						try {
							inventoryId = Integer.parseInt(key.split("_")[1]);
						} catch(Exception e) {
							responseDTO.setStatus(0);
							error.setDescription("Invalid Inventory Id.");
							responseDTO.setError(error);
							return responseDTO;
						}
						String[] dataArr = entry.getValue()[0].split("_");
						Integer avlQty = Integer.parseInt(dataArr[0]);
						Double regPrice = Double.parseDouble(dataArr[1]);
						Double selPrice = Double.parseDouble(dataArr[2]);
						Integer reqLPoints = Integer.parseInt(dataArr[3]);
						Double priceAftLPnts = Double.parseDouble(dataArr[4]);
						
						inventoryObj = EcommDAORegistry.getSellerProdItemsVariInventoryDAO().get(inventoryId);
						if(inventoryObj == null || !inventoryObj.getSpiId().equals(sellerPRodDtl.getSpiId()) ||
								!inventoryObj.getSellerId().equals(seller.getSellerId())) {
							responseDTO.setStatus(0);
							error.setDescription("Inventory Not Exists.");
							responseDTO.setError(error);
							return responseDTO;
						}
						inventoryObj.setRegPrice(regPrice);
						inventoryObj.setAvlQty(avlQty);
						inventoryObj.setSellPrice(selPrice);
						inventoryObj.setReqLPnts(reqLPoints);
						inventoryObj.setPriceAftLPnts(priceAftLPnts);
						inventoryObj.setUpdDate(new Date());
						inventoryObj.setUpdBy(userName);
						inventoryList.add(inventoryObj);
						
						
					} catch(Exception e) {
						responseDTO.setStatus(0);
						error.setDescription("Invalid Inventory Data to Update.");
						responseDTO.setError(error);
						return responseDTO;
					}
				}
			}
			
			/*String[] invIdsArr = invIdsStr.split(",");
			for(int i=0;i<invIdsArr.length;i++) {
				String idStr = invIdsArr[i];
				Integer invId = null;
				try {
					invId = Integer.parseInt(idStr);
				} catch(Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Invalid Inventory Id");
					responseDTO.setError(error);
					return responseDTO;
				}
				String regPriceStr = request.getParameter("regPrice_"+invId);
				String selPriceStr = request.getParameter("selPrice_"+invId);
				String avlQtyStr = request.getParameter("avlQty_"+invId);
				String reqLPointsStr = request.getParameter("reqLPoints_"+invId);
				String priceAftLPntsStr = request.getParameter("priceAftLPnts_"+invId);
				
				Double regPrice =null;
				try {
					regPrice = Double.parseDouble(regPriceStr);
				}catch(Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Invalid Regular Price.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Double selPrice =null;
				try {
					selPrice = Double.parseDouble(selPriceStr);
				}catch(Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Invalid Selling Price.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer avlQty =null;
				try {
					avlQty = Integer.parseInt(avlQtyStr);
				}catch(Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Invalid Avl Quantity.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer reqLPoints =null;
				try {
					reqLPoints = Integer.parseInt(reqLPointsStr);
				}catch(Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Invalid Required Loyalty Points.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Double priceAftLPnts =null;
				try {
					priceAftLPnts = Double.parseDouble(priceAftLPntsStr);
				}catch(Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Invalid Price with Loyalty Points.");
					responseDTO.setError(error);
					return responseDTO;
				}
				SellerProdItemsVariInventory inventory = EcommDAORegistry.getSellerProdItemsVariInventoryDAO().get(invId);
				if(inventory == null) {
					responseDTO.setStatus(0);
					error.setDescription("Inventory Not Exists.");
					responseDTO.setError(error);
					return responseDTO;
				}
				inventory.setRegPrice(regPrice);
				inventory.setAvlQty(avlQty);
				inventory.setSellPrice(selPrice);
				inventory.setReqLPnts(reqLPoints);
				inventory.setPriceAftLPnts(priceAftLPnts);
				inventory.setUpdDate(new Date());
				inventory.setUpdBy(userName);
				inventoryList.add(inventory);
			}*/
			EcommDAORegistry.getSellerProdItemsVariInventoryDAO().saveOrUpdateAll(inventoryList);
			
			List<SellerProdItemsVariInventory> inventoryListTemp = EcommDAORegistry.getSellerProdItemsVariInventoryDAO().getInventoryByProductId(sellerProdId);
			responseDTO.setInventoryList(inventoryListTemp);
			responseDTO.setStatus(1);
			responseDTO.setMessage("Inventory Details Updated Successfully.");
			
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while updating Products Inventory.");
			responseDTO.setError(error);
			return responseDTO;
		}
	}
	@RequestMapping(value="/addproditmvaroptns")
	public SellerProdItmlDTO addProdIrmVarOptns(HttpServletRequest request, HttpServletResponse response){
		SellerProdItmlDTO responseDTO = new SellerProdItmlDTO();
		Error error = new Error();
		try {
			String sellerprodidStr = request.getParameter("slrpitmsId");
			String sellerIdStr = request.getParameter("slerId");
			String action = request.getParameter("action");
			String userName = request.getParameter("userName");
			
			
			Integer sellerProdId = 0;
			try {
				sellerProdId = Integer.parseInt(sellerprodidStr);
			} catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Seller Product id is invalid.");
				responseDTO.setError(error);
				return responseDTO;
			}
			SellerProductsItems sellerPRodDtl = EcommDAORegistry.getSellerProductsItemsDAO().get(sellerProdId);
			if(sellerPRodDtl == null){
				responseDTO.setStatus(0);
				error.setDescription("Seller Product not found with given identifier.");
				responseDTO.setError(error);
				return responseDTO;
			}
			Integer sellerId = 0;
			try {
				sellerId = Integer.parseInt(sellerIdStr);
			} catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Seller id is invalid.");
				responseDTO.setError(error);
				return responseDTO;
			}
			RTFSellerDetails seller = EcommDAORegistry.getrTFSellerDetailsDAO().get(sellerId);
			if(seller == null){
				responseDTO.setStatus(0);
				error.setDescription("Seller not found with given identifier.");
				responseDTO.setError(error);
				return responseDTO;
			}
			List<SellerProdItemsVariInventory> inventoryList = EcommDAORegistry.getSellerProdItemsVariInventoryDAO().getInventoryByProductId(sellerProdId);
			if(inventoryList != null && !inventoryList.isEmpty()) {
				responseDTO.setStatus(0);
				error.setDescription("Inventory already exists you cant edit variant options and values.");
				responseDTO.setError(error);
				return responseDTO;
			}
			
			List<SellerProdItemsVariName> varientNames = new ArrayList<SellerProdItemsVariName>();
			SellerProdItemsVariName varientNameObj = null;
			for (int i=1;i<=4;i++) {
				String optNameValueStr = request.getParameter("optVal"+i);
				try {
					if(!EcommUtil.isNullOrEmpty(optNameValueStr)){
						String[] optNameValue1Str = optNameValueStr.split(":");
						if(optNameValue1Str.length == 2) {
							varientNameObj = new SellerProdItemsVariName();
							varientNameObj.setSellerId(sellerId);
							varientNameObj.setSpiId(sellerProdId);
							String varientOptName = optNameValue1Str[0];
							varientNameObj.setvOptName(varientOptName);
							String[] varOptValuesArr = optNameValue1Str[1].split(",");
							
							List<SellerProdItemsVariValue> varientOptValues = new ArrayList<SellerProdItemsVariValue>();
							SellerProdItemsVariValue varOptValObj = null;
							for (String optValues : varOptValuesArr) {
								varOptValObj = new SellerProdItemsVariValue();
								varOptValObj.setVarVal(optValues);
								varientOptValues.add(varOptValObj);
							}
							if(varientOptValues.isEmpty()) {
								responseDTO.setStatus(0);
								error.setDescription("Variant Option Values is mandatory for Variant "+varientOptName);
								responseDTO.setError(error);
								return responseDTO;
							}
							varientNameObj.setValues(varientOptValues);
							varientNames.add(varientNameObj);
						}
					}	
				} catch(Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Invalid Variant Option Values for variant "+i);
					responseDTO.setError(error);
					return responseDTO;
				}
			}
			if(varientNames.isEmpty()) {
				responseDTO.setStatus(0);
				error.setDescription("Atleast One Variant option is mandatory");
				responseDTO.setError(error);
				return responseDTO;
			}
			Map<String,Map<String,String>> variantMap = new HashMap<String, Map<String,String>>();
			
			for (SellerProdItemsVariName variantNameObj : varientNames) {
				Map<String,String> valueMap = new HashMap<String, String>();
				for (SellerProdItemsVariValue variantValueObj : variantNameObj.getValues()) {
					valueMap.put(variantValueObj.getVarVal(), null);
				}
				variantMap.put(variantNameObj.getvOptName(), valueMap);
			}
			MultiFiIesUploadUtil.uploadMultipleFilesTest(request, variantMap,varientNames);
			
			List<SellerProdItemsVariName> VariantNameListDb = EcommDAORegistry.getSellerProdItemsVariNameDAO().getAllProductVariants(sellerProdId);
			if(VariantNameListDb != null && !VariantNameListDb.isEmpty()) {
				for (SellerProdItemsVariName variantNameObj : VariantNameListDb) {
					List<SellerProdItemsVariValue> valuesList = EcommDAORegistry.getSellerProdItemsVariValueDAO().getOptionValues(variantNameObj.getSlrpiVarId());
					if(valuesList != null && !valuesList.isEmpty()) {
						for (SellerProdItemsVariValue variantValueObj : valuesList) {
							if(variantValueObj.getVarValImg() != null && !variantValueObj.getVarValImg().isEmpty()) {
								MultiFiIesUploadUtil.deleteFileFromS3(variantValueObj.getVarValImg());
							}
						}
						EcommDAORegistry.getSellerProdItemsVariValueDAO().deleteAll(valuesList);
					}
					EcommDAORegistry.getSellerProdItemsVariNameDAO().delete(variantNameObj);
				}
			}
			
			for (SellerProdItemsVariName variantNameObj : varientNames) {
				variantNameObj.setUpdDate(new Date());
				variantNameObj.setUpdBy(userName);
				Integer variantNameId = EcommDAORegistry.getSellerProdItemsVariNameDAO().save(varientNameObj);
				
				for (SellerProdItemsVariValue variantValueObj : variantNameObj.getValues()) {
					//variantValueObj.setSlrpiVarId(variantNameObj.getSlrpiVarId());
					variantValueObj.setSlrpiVarId(variantNameId);
					variantValueObj.setUpdDate(new Date());
					variantValueObj.setUpdBy(userName);
				}
				EcommDAORegistry.getSellerProdItemsVariValueDAO().saveAll(variantNameObj.getValues());
			}
			
			List<SellerProdItemsVariName> variantNames = EcommDAORegistry.getSellerProdItemsVariNameDAO().getAllProductVariants(sellerProdId);
			for (SellerProdItemsVariName varNameObj : variantNames) {
				List<SellerProdItemsVariValue> varValues = EcommDAORegistry.getSellerProdItemsVariValueDAO().getOptionValues(varNameObj.getSlrpiVarId());
				varNameObj.setValues(varValues);
			}
			responseDTO.setVariantsList(variantNames);
			
			
			responseDTO.setStatus(1);
			responseDTO.setMessage("Variant option values added successfully.");
			return responseDTO;
			
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while fetching Seller Product Details.");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}
	
	@RequestMapping(value="/GetRtfCouponCodes")
	public CouponCodeMstDTO getRtfCouponCodes(HttpServletRequest request, HttpServletResponse response){
		CouponCodeMstDTO responseDTO = new CouponCodeMstDTO();
		Error error = new Error();
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String status = request.getParameter("ccStatus");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getRtfSellerFilter(headerFilter);
			if(status==null || status.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please send valid status.");
				responseDTO.setError(error);
				return responseDTO;
			}
			List<CouponCodeMst> lst = EcommDAORegistry.getCouponCodeMstrDAO().getAllCouponCodes(filter,status);
			responseDTO.setCoupCodeMstrList(lst);
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(lst.size()));
			responseDTO.setStatus(1);
			if(lst.isEmpty()){
				responseDTO.setStatus(1);
				responseDTO.setMessage("No Coupon Codes Found in System.");
				return responseDTO;
			}
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while Coupon Codes");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}
	
	@RequestMapping(value="/updateditcoupcodemaster")
	public CouponCodeMstDTO updateEditCouponCodeMaster(HttpServletRequest request, HttpServletResponse response){
		CouponCodeMstDTO responseDTO = new CouponCodeMstDTO();
		Error error = new Error();
		String action = null;
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String ccIdStr = request.getParameter("ccId");
			action = request.getParameter("action");
			String userName = request.getParameter("userName");
			String coustatus = request.getParameter("ccStatus");
			String msg = "";
			String resMsg = "";
			GridHeaderFilters filter = GridHeaderFiltersUtil.getRtfSellerFilter(headerFilter);
			if(action==null || action.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Could not able to identify action to be perform.");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(action.equalsIgnoreCase("EDIT")){
				if(ccIdStr==null || ccIdStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Coupon Code id is invalid.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer ccId = 0;
				try {
					ccId = Integer.parseInt(ccIdStr);
				} catch (Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Coupon Code id is invalid.");
					responseDTO.setError(error);
					return responseDTO;
				}
				CouponCodeMst couponCodeMstr = EcommDAORegistry.getCouponCodeMstrDAO().get(ccId);
				if(couponCodeMstr == null){
					responseDTO.setStatus(0);
					error.setDescription("CouponCode not found with given identifier.");
					responseDTO.setError(error);
					return responseDTO;
				}
				responseDTO.setStatus(1);
				responseDTO.setCoupCodeMstr(couponCodeMstr);
				return responseDTO;
			}else if(action.equalsIgnoreCase("SAVE") || action.equalsIgnoreCase("UPDATE")){							
				
				
				String coucde = request.getParameter("coucde");
				String couname = request.getParameter("couname");
				String coudesc = request.getParameter("coudesc");
				String coudiscountStr = request.getParameter("coudiscount");
				
				String discounttype = request.getParameter("discounttype");	
				String coutype = request.getParameter("coutype");		
				String cau = request.getParameter("cau");	
				String exprydateStr = request.getParameter("exprydate");				
				
				if(EcommUtil.isNullOrEmpty(coucde)){
					resMsg = "Invalid Coupon Code  :"+coucde;
					responseDTO.setStatus(0);
					error.setDescription(resMsg);
					responseDTO.setError(error);
					return responseDTO;
				}
				if(EcommUtil.isNullOrEmpty(couname)){
					resMsg = "Invalid Coupon Name :"+couname;
					responseDTO.setStatus(0);
					error.setDescription(resMsg);
					responseDTO.setError(error);
					return responseDTO;
				}
				if(EcommUtil.isNullOrEmpty(coudesc)){
					resMsg = "Invalid Coupon Description:"+coudesc;
					responseDTO.setStatus(0);
					error.setDescription(resMsg);
					responseDTO.setError(error);
					return responseDTO;
				}
				if(EcommUtil.isNullOrEmpty(coudiscountStr)){
					resMsg = "Invalid Coupon Discount :"+coudiscountStr;
					responseDTO.setStatus(0);
					error.setDescription(resMsg);
					responseDTO.setError(error);
					return responseDTO;
				}
				if(EcommUtil.isNullOrEmpty(coustatus)){
					resMsg = "Invalid Coupon Status :"+coustatus;
					responseDTO.setStatus(0);
					error.setDescription(resMsg);
					responseDTO.setError(error);
					return responseDTO;
				}
				if(EcommUtil.isNullOrEmpty(discounttype)){
					resMsg = "Invalid Coupon Discount Type :"+discounttype;
					responseDTO.setStatus(0);
					error.setDescription(resMsg);
					responseDTO.setError(error);
					return responseDTO;
				}
				if(EcommUtil.isNullOrEmpty(coutype)){
					resMsg = "Invalid Coupon Type : "+coutype;
					responseDTO.setStatus(0);
					error.setDescription(resMsg);
					responseDTO.setError(error);
					return responseDTO;
				}
				
				if(EcommUtil.isNullOrEmpty(exprydateStr)){
					resMsg = "Invalid Expiry Date :"+exprydateStr;
					responseDTO.setStatus(0);
					error.setDescription(resMsg);
					responseDTO.setError(error);
					return responseDTO;
				}
				
				Integer ccId=null;				
				Double coudiscount=0.0;				
				
				try {
					coudiscount = new Double(coudiscountStr);
				}catch(Exception ex) {
					resMsg = "Invalid Coupon Discount :"+coudiscountStr;
					responseDTO.setStatus(0);
					error.setDescription(resMsg);
					responseDTO.setError(error);
					return responseDTO;					
				}
				
				exprydateStr = exprydateStr + " 23:59:59";
				Date expyDate = null;
				try {
					expyDate = Util.getDateWithTwentyFourHourFormat1(exprydateStr);
				} catch (Exception e) {
					responseDTO.setStatus(0);
					resMsg = "Invalid Expiry Date OR Date Format :"+exprydateStr;
					error.setDescription(resMsg);
					responseDTO.setError(error);
					return responseDTO;
				}
				
				 if (expyDate.before(new Date())) {					 
				    responseDTO.setStatus(0);
					resMsg = "Expiry Date Should be Current Date or Future Date :"+exprydateStr;
					error.setDescription(resMsg);
					responseDTO.setError(error);
					return responseDTO;					 
				 }
				
				CouponCodeMst couponCodeMst = null;
				if(action.equalsIgnoreCase("UPDATE")){
					if(EcommUtil.isNullOrEmpty(ccIdStr)){
						resMsg = "Coupon Code id is invalid.:"+ccIdStr;
						responseDTO.setStatus(0);
						error.setDescription(resMsg);
						responseDTO.setError(error);
						return responseDTO;
					}
					try {
						ccId = new Integer(ccIdStr);
					}catch(Exception ex) {
						resMsg = "Invalid Coupon Code Id "+ccIdStr;
						responseDTO.setStatus(0);
						error.setDescription(resMsg);
						responseDTO.setError(error);
						return responseDTO;				
					}	
					couponCodeMst = EcommDAORegistry.getCouponCodeMstrDAO().get(ccId);
					if(couponCodeMst == null){
						responseDTO.setStatus(0);
						error.setDescription("Coupon Code not found with given identifier.");
						responseDTO.setError(error);
						return responseDTO;
					}
					 if (expyDate.before(new Date())) {					 
						    responseDTO.setStatus(0);
							resMsg = "Expiry Date Should be Current Date or Future Date :"+exprydateStr;
							error.setDescription(resMsg);
							responseDTO.setError(error);
							return responseDTO;					 
					 }
					 
					 if(couponCodeMst.getCoucde() !=null ) {
						 if(!couponCodeMst.getCoucde().equalsIgnoreCase(coucde)) {
							    responseDTO.setStatus(0);
								resMsg = "Coupon Code cannot be edited/Renamed, As it may  be referred in previous reports. :["+coucde + "] ";
								error.setDescription(resMsg);
								responseDTO.setError(error);
								return responseDTO;	
							 
						 }
					 }
					 
					 
					 
					 
					couponCodeMst.setUpdBy(userName);
					couponCodeMst.setUpdDate(new Date());
					msg = "Coupon Code updated successfully.";
				}else if(action.equalsIgnoreCase("SAVE")){
					
					Boolean isCoupCodeNameExistdb = EcommDAORegistry.getCouponCodeMstrDAO().getCouponCodeMstrByCouponName(coucde);
					if(isCoupCodeNameExistdb) {						
						responseDTO.setStatus(0);
						error.setDescription("Coupon Code [ " + coucde  + "]  Exists in our Records. Please use a different Code");
						responseDTO.setError(error);
						return responseDTO;
						
					}
				
					couponCodeMst = new CouponCodeMst();
					couponCodeMst.setUpdBy(userName);
					couponCodeMst.setUpdDate(new Date());
					couponCodeMst.setCoustatus("ACTIVE");
					msg = "Coupon Code saved successfully.";
				}
				
				couponCodeMst.setCoucde(coucde);
				couponCodeMst.setCouname(couname);
				couponCodeMst.setCoudesc(coudesc);
				couponCodeMst.setCoudiscount(coudiscount);				
				couponCodeMst.setCoustatus(coustatus);
				couponCodeMst.setDiscounttype(discounttype);
				couponCodeMst.setCoutype(coutype);
				couponCodeMst.setExprydate(expyDate);				
				
				EcommDAORegistry.getCouponCodeMstrDAO().saveOrUpdate(couponCodeMst);
				
				List<CouponCodeMst> CouponCodeMstlst = EcommDAORegistry.getCouponCodeMstrDAO().getAllCouponCodes(filter,coustatus);
				responseDTO.setCoupCodeMstrList(CouponCodeMstlst);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(CouponCodeMstlst.size()));
				responseDTO.setStatus(1);
				responseDTO.setMessage(msg);
				return responseDTO;
				
			}else if(action.equalsIgnoreCase("DELETE")){
				Integer ccId=null;
				if(EcommUtil.isNullOrEmpty(ccIdStr)){
					resMsg = "Coupon Code id is invalid.:"+ccIdStr;
					responseDTO.setStatus(0);
					error.setDescription(resMsg);
					responseDTO.setError(error);
					return responseDTO;
				}
				try {
					ccId = new Integer(ccIdStr);
				}catch(Exception ex) {
					resMsg = "Invalid Coupon Code Id "+ccIdStr;
					responseDTO.setStatus(0);
					error.setDescription(resMsg);
					responseDTO.setError(error);
					return responseDTO;				
				}	
				CouponCodeMst couponCodeMst = EcommDAORegistry.getCouponCodeMstrDAO().get(ccId);
				if(couponCodeMst == null){
					responseDTO.setStatus(0);
					error.setDescription("Coupon Code not found with given identifier.");
					responseDTO.setError(error);
					return responseDTO;
				}
				couponCodeMst.setUpdBy(userName);
				couponCodeMst.setUpdDate(new Date());
				couponCodeMst.setCoustatus("DELETED");
				EcommDAORegistry.getCouponCodeMstrDAO().saveOrUpdate(couponCodeMst);
				
				msg = "Coupon Code Record deleted successfully.";
				
				List<CouponCodeMst> coupCodeLst = EcommDAORegistry.getCouponCodeMstrDAO().getAllCouponCodes(filter,coustatus);
				responseDTO.setCoupCodeMstrList(coupCodeLst);
				int size = 0;
				if(coupCodeLst != null) {
					size = coupCodeLst.size();
				}
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(size));
				responseDTO.setStatus(1);
				responseDTO.setMessage(msg);
				return responseDTO;
			}else if(action.equalsIgnoreCase("EXPIRE")){

				Integer ccId=null;
				if(EcommUtil.isNullOrEmpty(ccIdStr)){
					resMsg = "Coupon Code id is invalid.:"+ccIdStr;
					responseDTO.setStatus(0);
					error.setDescription(resMsg);
					responseDTO.setError(error);
					return responseDTO;
				}
				try {
					ccId = new Integer(ccIdStr);
				}catch(Exception ex) {
					resMsg = "Invalid Coupon Code Id "+ccIdStr;
					responseDTO.setStatus(0);
					error.setDescription(resMsg);
					responseDTO.setError(error);
					return responseDTO;				
				}	
				CouponCodeMst couponCodeMst = EcommDAORegistry.getCouponCodeMstrDAO().get(ccId);
				if(couponCodeMst == null){
					responseDTO.setStatus(0);
					error.setDescription("Coupon Code not found with given identifier.");
					responseDTO.setError(error);
					return responseDTO;
				}
				couponCodeMst.setUpdBy(userName);
				couponCodeMst.setUpdDate(new Date());
				couponCodeMst.setCoustatus("EXPIRED");
				EcommDAORegistry.getCouponCodeMstrDAO().saveOrUpdate(couponCodeMst);
				
				msg = "Coupon Code Expired successfully.";
				
				List<CouponCodeMst> coupCodeLst = EcommDAORegistry.getCouponCodeMstrDAO().getAllCouponCodes(filter,coustatus);
				responseDTO.setCoupCodeMstrList(coupCodeLst);
				int size = 0;
				if(coupCodeLst != null) {
					size = coupCodeLst.size();
				}
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(size));
				responseDTO.setStatus(1);
				responseDTO.setMessage(msg);
				return responseDTO;
			
				
			}
			responseDTO.setStatus(0);
			responseDTO.setMessage("Could not able to identify action.");
			return responseDTO;
			
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured on Action [ " + action + " ] For Coupon Code."  + e.getLocalizedMessage());
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}
	
	
	
	@RequestMapping(value="/GetRtfCouponCodeProductsMap")
	public CouponCodeProductsMapDTO getRtfCouponCodeProductsMap(HttpServletRequest request, HttpServletResponse response){
		CouponCodeProductsMapDTO responseDTO = new CouponCodeProductsMapDTO();
		Error error = new Error();
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String ccIdstr = request.getParameter("ccId");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getRtfSellerFilter(headerFilter);
			if(ccIdstr==null || ccIdstr.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please send valid Coupon Code Id.");
				responseDTO.setError(error);
				return responseDTO;
			}
			Integer ccId = null;
			try {
				ccId = Integer.parseInt(ccIdstr);
			}catch(Exception ex) {
				responseDTO.setStatus(0);
				error.setDescription("Please send valid Coupon Code Id.");
				responseDTO.setError(error);
				return responseDTO;
			}
			
			List<CouponCodeProductMap> lst = EcommDAORegistry.getCouponCodeProductMapDAO().getAllUnMappedProductsForCouponCodes(filter, ccId);
				if(lst != null ) {				
					for (CouponCodeProductMap mpdvo : lst ) {
						mpdvo.setIsMapped("false");					
					}
				}
				
			System.out.println(" Unapped List " + lst );
			
			List<CouponCodeProductMap> mappedLst = EcommDAORegistry.getCouponCodeProductMapDAO().getAllMappedProductsForCouponCodes(filter, ccId);			
			
			if(mappedLst != null ) {				
				for (CouponCodeProductMap mpdvo : mappedLst ) {
					mpdvo.setIsMapped("true");					
				}
			}
			
			System.out.println("******** Mapped List " + mappedLst );
			
			lst.addAll(mappedLst);
			
			responseDTO.setCoupCodeProdMapList(lst);
			responseDTO.setCoupCodeProdUExistMapList(mappedLst);
			
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(lst.size()));
			responseDTO.setStatus(1);
			if(lst.isEmpty()){
				responseDTO.setStatus(1);
				responseDTO.setMessage("No Coupon Codes / Products Mapped in System.");
				return responseDTO;
			}			
			CouponCodeMst ccMst = EcommDAORegistry.getCouponCodeMstrDAO().get(ccId);
			responseDTO.setCcMstr(ccMst);
			
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while Fetching Coupon Code Product Mapping ");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}
	

	@RequestMapping(value="/SaveRtfCouponCodeProductsMap")
	public CouponCodeProductsMapDTO saveRtfCouponCodeProductsMap(HttpServletRequest request, HttpServletResponse response){
		CouponCodeProductsMapDTO responseDTO = new CouponCodeProductsMapDTO();
		Error error = new Error();
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String ccIdstr = request.getParameter("ccId");
			String prodItmsIdStr = request.getParameter("slrpitmsId");
			
			System.out.println(" PRODUCTS ID'S RECIEVED  " + prodItmsIdStr);
			GridHeaderFilters filter = GridHeaderFiltersUtil.getRtfSellerFilter(headerFilter);
			if(ccIdstr==null || ccIdstr.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please Select Valid Coupon Code Id." );
				responseDTO.setError(error);
				return responseDTO;
			}			
			if(prodItmsIdStr==null || prodItmsIdStr.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please Select Product Id's to Map.");
				responseDTO.setError(error);
				return responseDTO;
				
			}
			Integer ccId = null;
			try {
				ccId = Integer.parseInt(ccIdstr);
			}catch(Exception ex) {
				responseDTO.setStatus(0);
				error.setDescription("Please Select Valid Coupon Code Id." );
				responseDTO.setError(error);
				return responseDTO;
			}
			List<String> prodItmsIdArry = new ArrayList<String>();
			List<Integer> prodIdIntArry = new ArrayList<Integer>();
			try {
				prodItmsIdArry = Arrays.asList(prodItmsIdStr.split(","));
				//prodItmsIdArry = Arrays.asList(prodItmsIdStr);
				prodIdIntArry = new ArrayList<Integer>();
				if(prodItmsIdArry != null && prodItmsIdArry.size() >0) {				
					for (String prodItmsId : prodItmsIdArry) {
						System.out.println("[---prodItmsId-------] "  + prodItmsId);
						prodIdIntArry.add(Integer.parseInt(prodItmsId.trim()));
					}
				}
			}catch(Exception ex) {
				ex.printStackTrace();
				responseDTO.setStatus(0);
				error.setDescription("Please send valid Product Id's for Mapping." + prodItmsIdArry);
				responseDTO.setError(error);
				return responseDTO;
			}			
			CouponCodeMst ccdeMstr = EcommDAORegistry.getCouponCodeMstrDAO().get(ccId);
			if(ccdeMstr == null || ccdeMstr.getCcId() == null) {
				responseDTO.setStatus(0);
				error.setDescription("Coupon Code Id Not available in System Records");
				responseDTO.setError(error);
				return responseDTO;
				
			}
			String couponCode = ccdeMstr.getCoucde();			
			//System.out.println("Coupon Code from DB  " + couponCode);			
			Collection<CouponCodeProductMap> ccprodmaplst = new ArrayList<CouponCodeProductMap>();
			for(Integer prodItemdId: prodIdIntArry) {
				if(prodItemdId == -1) continue;
				CouponCodeProductMap ccprdmap = new CouponCodeProductMap();
				ccprdmap.setCcId(ccId);
				ccprdmap.setCouCode(couponCode);
				ccprdmap.setSlrpitmsId(prodItemdId);
				ccprodmaplst.add(ccprdmap);
			}	
			
			//System.out.println("[COUPON CODES MAP NEW LIST  FOR SAVING " + ccprodmaplst);
			//EcommDAORegistry.getCouponCodeProductMapDAO().deleteCoupCodeMappinByCoupId(ccId);			
			//System.out.println(" Existing Coupon Code Mappings Removed from from DB for " + ccId);
			
			if(ccprodmaplst != null) {
				EcommDAORegistry.getCouponCodeProductMapDAO().saveOrUpdateAll(ccprodmaplst);
				System.out.println(" New Coupon Code Mappings Added to DB for  " + ccId);
			}						
			List<CouponCodeProductMap> lst = EcommDAORegistry.getCouponCodeProductMapDAO().getAllUnMappedProductsForCouponCodes(filter, ccId);
			if(lst == null) lst  = new ArrayList<CouponCodeProductMap>();
			if(lst != null ) {				
					for (CouponCodeProductMap mpdvo : lst ) {
						mpdvo.setIsMapped("NO");					
					}
				}				
			//System.out.println(" Unapped List " + lst );			
			List<CouponCodeProductMap> mappedLst = EcommDAORegistry.getCouponCodeProductMapDAO().getAllMappedProductsForCouponCodes(filter, ccId);			
			if(mappedLst != null ) {				
				for (CouponCodeProductMap mpdvo : mappedLst ) {
					mpdvo.setIsMapped("YES");					
				}
			}else {
				mappedLst = new ArrayList<CouponCodeProductMap>();
			}
			//System.out.println("******** Mapped List " + mappedLst );			
			lst.addAll(mappedLst);	
			//System.out.println("******** FINAL  List " + mappedLst );	
			responseDTO.setCoupCodeProdMapList(lst);
			responseDTO.setCoupCodeProdUExistMapList(mappedLst);			
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(lst.size()));
			responseDTO.setStatus(1);
			if(lst.isEmpty()){
				responseDTO.setStatus(1);
				responseDTO.setMessage("No Coupon Codes / Products Mapped in System.");
				return responseDTO;
			}			
			CouponCodeMst ccMst = EcommDAORegistry.getCouponCodeMstrDAO().get(ccId);
			responseDTO.setCcMstr(ccMst);			
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while Updating Coupon Code Product Mapping");
			responseDTO.setError(error);
			return responseDTO;
		}	
	}
	
	@RequestMapping(value="/RemoveRtfCouponCodeProductsMap")
	public CouponCodeProductsMapDTO removeRtfCouponCodeProductsMap(HttpServletRequest request, HttpServletResponse response){
		
		CouponCodeProductsMapDTO responseDTO = new CouponCodeProductsMapDTO();
		Error error = new Error();
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String ccIdstr = request.getParameter("ccId");
			String prodItmsIdStr = request.getParameter("slrpitmsId");
			
			System.out.println("[DELETE COUPON CODE MAPPING  FOR PRODUCTS ITEMS ] " +  prodItmsIdStr );	
			GridHeaderFilters filter = GridHeaderFiltersUtil.getRtfSellerFilter(headerFilter);
			if(ccIdstr==null || ccIdstr.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please Select Valid Coupon Code Id." );
				responseDTO.setError(error);
				return responseDTO;
			}			
			if(prodItmsIdStr==null || prodItmsIdStr.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please Select Product Id's to Map.");
				responseDTO.setError(error);
				return responseDTO;
				
			}
			Integer ccId = null;
			try {
				ccId = Integer.parseInt(ccIdstr);
			}catch(Exception ex) {
				responseDTO.setStatus(0);
				error.setDescription("Please Select Valid Coupon Code Id." );
				responseDTO.setError(error);
				return responseDTO;
			}
			List<String> prodItmsIdArry = new ArrayList<String>();
			List<Integer> prodIdIntArry = new ArrayList<Integer>();
			try {
				prodItmsIdArry = Arrays.asList(prodItmsIdStr.split(","));			
				prodIdIntArry = new ArrayList<Integer>();
				if(prodItmsIdArry != null && prodItmsIdArry.size() >0) {				
					for (String prodItmsId : prodItmsIdArry) {						
						prodIdIntArry.add(Integer.parseInt(prodItmsId.trim()));
					}
				}
			}catch(Exception ex) {
				ex.printStackTrace();
				responseDTO.setStatus(0);
				error.setDescription("Please send valid Product Id's for Mapping." + prodItmsIdArry);
				responseDTO.setError(error);
				return responseDTO;
			}			
			CouponCodeMst ccdeMstr = EcommDAORegistry.getCouponCodeMstrDAO().get(ccId);
			if(ccdeMstr == null || ccdeMstr.getCcId() == null) {
				responseDTO.setStatus(0);
				error.setDescription("Coupon Code Id Not available in System Records");
				responseDTO.setError(error);
				return responseDTO;
				
			}
			String couponCode = ccdeMstr.getCoucde();			
			System.out.println("Coupon Code from DB  " + couponCode);			
			Collection<CouponCodeProductMap> ccprodmaplst = new ArrayList<CouponCodeProductMap>();
			Integer result = 0;
			for(Integer prodItemdId: prodIdIntArry) {
				if(prodItemdId == -1) continue;
				CouponCodeProductMap ccprdmap = new CouponCodeProductMap();
				ccprdmap.setCcId(ccId);
				ccprdmap.setCouCode(couponCode);
				ccprdmap.setSlrpitmsId(prodItemdId);
				ccprodmaplst.add(ccprdmap);
				Integer delcnt = EcommDAORegistry.getCouponCodeProductMapDAO(). deleteCoupCodeMappinByCoupIdAndProId(ccId,prodItemdId);
				//System.out.println("[ COUPON CODES MAPpings  DELETED IS  " + ccId + " , " + prodItemdId );	
				if(delcnt != null) result = result + delcnt;
			}	
			
			//System.out.println("[Total COUPON CODES MAPpings  DELETED IS  " + result);				
						
			List<CouponCodeProductMap> lst = EcommDAORegistry.getCouponCodeProductMapDAO().getAllUnMappedProductsForCouponCodes(filter, ccId);
			if(lst == null) lst  = new ArrayList<CouponCodeProductMap>();
			if(lst != null ) {				
					for (CouponCodeProductMap mpdvo : lst ) {
						mpdvo.setIsMapped("false");					
					}
				}				
			//System.out.println("[********* Unapped List ] " + lst );			
			List<CouponCodeProductMap> mappedLst = EcommDAORegistry.getCouponCodeProductMapDAO().getAllMappedProductsForCouponCodes(filter, ccId);			
			if(mappedLst != null ) {				
				for (CouponCodeProductMap mpdvo : mappedLst ) {
					mpdvo.setIsMapped("true");					
				}
			}else {
				mappedLst = new ArrayList<CouponCodeProductMap>();
			}
			//System.out.println("[******** Mapped List ]" + mappedLst );			
			lst.addAll(mappedLst);			
			responseDTO.setCoupCodeProdMapList(lst);
			responseDTO.setCoupCodeProdUExistMapList(mappedLst);			
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(lst.size()));
			responseDTO.setStatus(1);
			if(lst.isEmpty()){
				responseDTO.setStatus(1);
				responseDTO.setMessage("No Coupon Codes / Products Mapped in System.");
				return responseDTO;
			}			
			CouponCodeMst ccMst = EcommDAORegistry.getCouponCodeMstrDAO().get(ccId);
			responseDTO.setCcMstr(ccMst);			
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while Updating Coupon Code Product Mapping");
			responseDTO.setError(error);
			return responseDTO;
		}
	}

	@RequestMapping(value = "/UploadRTFInventory")
	public SellerProductUploadTO uploadRTFInventory(HttpServletRequest request, HttpServletResponse response) {
		SellerProductUploadTO responseDTO = new SellerProductUploadTO();
		responseDTO.setStatus(0);
		Error error = new Error();
		try {
			MultipartFile file = null;

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			file = multipartRequest.getFile("imageFile");

			File upldXlsFile = ProductInventoryXLSUploadUtil.multipartToFile(file, file.getOriginalFilename());
			if (file == null || upldXlsFile == null) {

				responseDTO.setStatus(0);
				error.setDescription("Please Upload Excel File with Inventory Details");
				responseDTO.setError(error);
				return responseDTO;

			}

			String userName = request.getParameter("userName");
			responseDTO.setUserName(userName);
			responseDTO = ProductInventoryXLSUploadUtil.parseUploadInventory(responseDTO, upldXlsFile);
			
			if(responseDTO.getStatus() == 0 ) {			
				return responseDTO;
			}
			List<SellerProductUpload> upldList = responseDTO.getProdlst();

			if (upldList == null || upldList.size() == 0) {
				responseDTO.setStatus(0);
				error.setDescription("Error occured while Uploading Inventory ddata");
				responseDTO.setError(error);
				return responseDTO;
			}
			RTFSellerDetails sellerDtl = null;
			try {

				String sellerName = upldList.get(0).getSlerCompName();
				sellerDtl = EcommDAORegistry.getrTFSellerDetailsDAO().findSellerByName(sellerName);
				if (sellerDtl == null || sellerDtl.getSellerId() == null) {
					responseDTO.setStatus(0);
					error.setDescription(
							"Seller Details Not Recorded in System. Please Create Seller detail with Name [ "
									+ sellerName + " ] before Uploading Inventory");
					responseDTO.setError(error);
					return responseDTO;

				}

				Map<String, SellerProductUpload> processedDVOMap = ProductInventoryXLSUploadUtil
						.processInventoryUpload(upldList);
				
				SellerProductUploadTO tempResp = ProductInventoryXLSUploadUtil.validateInventoryFileUpload(processedDVOMap,sellerDtl, responseDTO);
				if(tempResp != null) {
					return tempResp;
				}
				
				ProductInventoryXLSUploadUtil.saveInventoryDetailsNew(processedDVOMap, sellerDtl.getSellerId(),userName,responseDTO);
				//ProductInventoryXLSUploadUtil.saveInventoryDetails(processedDVOMap, sellerDtl.getSellerId(),responseDTO);
			} catch (Exception ex) {
				responseDTO.setStatus(0);
				if(responseDTO.getError() != null && responseDTO.getError().getDescription() != null) {
					error.setDescription(responseDTO.getError().getDescription());
				} else {
					error.setDescription("Error Occured While Uploading products.");
				}
				responseDTO.setError(error);
				return responseDTO;

			}		
			responseDTO.setStatus(1);
			return responseDTO;

		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription(responseDTO.getError().getDescription());
			responseDTO.setError(error);
			return responseDTO;
		} finally {
			responseDTO.setProdlst(null);
		}

	}
	@RequestMapping(value="/ExportRTFProductsForSaaS")
	public SellerProdItmlDTO exportRTFProductsForSaaS(HttpServletRequest request, HttpServletResponse response){
		SellerProdItmlDTO responseDTO = new SellerProdItmlDTO();
		Error error = new Error();
		try {
			String prodItmsIdStr = request.getParameter("slrpitmsId");			
			System.out.println(" PRODUCTS ID'S RECIEVED  " + prodItmsIdStr);		
			if(prodItmsIdStr==null || prodItmsIdStr.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please Select Product Id's to Map.");
				responseDTO.setError(error);
				return responseDTO;				
			}		
			List<String> prodItmsIdArry = new ArrayList<String>();
			List<Integer> prodIdIntArry = new ArrayList<Integer>();
			try {
				prodItmsIdArry = Arrays.asList(prodItmsIdStr.split(","));
				//prodItmsIdArry = Arrays.asList(prodItmsIdStr);
				prodIdIntArry = new ArrayList<Integer>();
				if(prodItmsIdArry != null && prodItmsIdArry.size() >0) {				
					for (String prodItmsId : prodItmsIdArry) {
						System.out.println("[---prodItmsId-------] "  + prodItmsId);
						prodIdIntArry.add(Integer.parseInt(prodItmsId.trim()));
					}
				}
			}catch(Exception ex) {
				ex.printStackTrace();
				responseDTO.setStatus(0);
				error.setDescription("Please send valid Product Id's for Exporting." + prodItmsIdArry);
				responseDTO.setError(error);
				return responseDTO;
			}
		
			List<SellerProductsItems> prodItms = EcommDAORegistry.getSellerProductsItemsDAO().getProductsById(prodItmsIdStr);
			if(prodItms == null || prodItms.isEmpty()) {
				responseDTO.setStatus(0);
				error.setDescription("No Records available for Exporting to Excel for Prod Ids [ " + prodItmsIdStr + " ] ");
				responseDTO.setError(error);
				responseDTO.setMessage("No Products To Export for roduct Id[" + prodItmsIdStr + " ]");	
				return responseDTO;				
			}
			
			System.out.println("EXPORT INVENTORY Product Count " + prodItms.size());			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			ProductInventoryXLSUploadUtil.generateExcelForExport(prodItms , workbook);	
			Date date = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy") ;
            String curDate =dateFormat.format(date);
            String fileName = "RTFInventory_"+ curDate + ".xlsx";
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+fileName); // 
			workbook.write(response.getOutputStream()); 
			responseDTO.setStatus(1);
			responseDTO.setMessage("RTF Inventory Excel Generated Successfully");	
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while Exporting RTF Inventory " + e.getLocalizedMessage());
			responseDTO.setError(error);
			return responseDTO;
		}	
	}
	
}
