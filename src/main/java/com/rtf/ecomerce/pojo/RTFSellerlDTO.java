package com.rtf.ecomerce.pojo;

import java.util.List;

import com.rtf.ecomerce.data.RTFSellerDetails;
import com.rtw.tmat.utils.Error;
import com.rtw.tracker.pojos.PaginationDTO;

public class RTFSellerlDTO {

	private List<RTFSellerDetails> rtfSellers;
	private RTFSellerDetails rtfSeller;
	private Error error = new Error(); 
	private String message = new String();
	private Integer status;
	private PaginationDTO pagination;
	
	
	public List<RTFSellerDetails> getRtfSellers() {
		return rtfSellers;
	}
	public void setRtfSellers(List<RTFSellerDetails> rtfSellers) {
		this.rtfSellers = rtfSellers;
	}
	public RTFSellerDetails getRtfSeller() {
		return rtfSeller;
	}
	public void setRtfSeller(RTFSellerDetails rtfSeller) {
		this.rtfSeller = rtfSeller;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public PaginationDTO getPagination() {
		return pagination;
	}
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}
	
	
}
