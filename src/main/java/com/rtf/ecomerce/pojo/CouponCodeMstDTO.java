package com.rtf.ecomerce.pojo;

import java.util.List;

import com.rtf.ecomerce.data.CouponCodeMst;
import com.rtw.tmat.utils.Error;
import com.rtw.tracker.pojos.PaginationDTO;

public class CouponCodeMstDTO {
	
	private List<CouponCodeMst> coupCodeMstrList;
	private CouponCodeMst coupCodeMstr;
	
	
	private Error error = new Error(); 
	private String message = new String();
	private Integer status;
	private PaginationDTO pagination;
	public List<CouponCodeMst> getCoupCodeMstrList() {
		return coupCodeMstrList;
	}
	public void setCoupCodeMstrList(List<CouponCodeMst> coupCodeMstrList) {
		this.coupCodeMstrList = coupCodeMstrList;
	}
	public CouponCodeMst getCoupCodeMstr() {
		return coupCodeMstr;
	}
	public void setCoupCodeMstr(CouponCodeMst coupCodeMstr) {
		this.coupCodeMstr = coupCodeMstr;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public PaginationDTO getPagination() {
		return pagination;
	}
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}
	@Override
	public String toString() {
		return "CouponCodeMstDTO [coupCodeMstrList=" + coupCodeMstrList + ", coupCodeMstr=" + coupCodeMstr + ", error="
				+ error + ", message=" + message + ", status=" + status + ", pagination=" + pagination + "]";
	}

}
