package com.rtf.ecomerce.pojo;

import com.rtf.ecomerce.data.RtfOrderRefund;

public class RtfOrderRefundResponse {

	private Integer status;
	private Error error; 
	private String message;
	private RtfOrderRefund rtfORderRefund;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public RtfOrderRefund getRtfORderRefund() {
		return rtfORderRefund;
	}
	public void setRtfORderRefund(RtfOrderRefund rtfORderRefund) {
		this.rtfORderRefund = rtfORderRefund;
	}
	
}
