package com.rtf.ecomerce.pojo;

import java.util.List;

import com.rtf.ecomerce.data.CouponCodeMst;
import com.rtf.ecomerce.data.CouponCodeProductMap;
import com.rtw.tmat.utils.Error;
import com.rtw.tracker.pojos.PaginationDTO;

public class CouponCodeProductsMapDTO {
	
	private List<CouponCodeProductMap> coupCodeProdMapList;
	private Integer ccId;
	
	
	private Error error = new Error(); 
	private String message = new String();
	private Integer status;
	private PaginationDTO pagination;
	private CouponCodeMst ccMstr;
	
	private List<CouponCodeProductMap> coupCodeProdUExistMapList;
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public PaginationDTO getPagination() {
		return pagination;
	}
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}
	@Override
	public String toString() {
		return "CouponCodeProductsMapDTO [coupCodeProdMapList=" + coupCodeProdMapList + ", ccId=" + ccId + ", error="
				+ error + ", message=" + message + ", status=" + status + ", pagination=" + pagination + ", ccMstr="
				+ ccMstr + ", coupCodeProdUExistMapList=" + coupCodeProdUExistMapList + "]";
	}
	public List<CouponCodeProductMap> getCoupCodeProdMapList() {
		return coupCodeProdMapList;
	}
	public void setCoupCodeProdMapList(List<CouponCodeProductMap> coupCodeProdMapList) {
		this.coupCodeProdMapList = coupCodeProdMapList;
	}
	public Integer getCcId() {
		return ccId;
	}
	public void setCcId(Integer ccId) {
		this.ccId = ccId;
	}
	public CouponCodeMst getCcMstr() {
		return ccMstr;
	}
	public void setCcMstr(CouponCodeMst ccMstr) {
		this.ccMstr = ccMstr;
	}
	public List<CouponCodeProductMap> getCoupCodeProdUExistMapList() {
		return coupCodeProdUExistMapList;
	}
	public void setCoupCodeProdUExistMapList(List<CouponCodeProductMap> coupCodeProdUExistMapList) {
		this.coupCodeProdUExistMapList = coupCodeProdUExistMapList;
	}

}
