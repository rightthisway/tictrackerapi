package com.rtf.ecomerce.pojo;

import java.util.List;

import com.rtf.ecomerce.data.CustomerOrder;
import com.rtf.ecomerce.data.OrderProducts;
import com.rtf.ecomerce.data.SellerProdItemsVariInventory;
import com.rtw.tmat.utils.Error;
import com.rtw.tracker.pojos.PaginationDTO;

public class OrderResp {

	
	private Integer status;
	private Error error = new Error();
	private String message;
	private CustomerOrder order;
	private List<CustomerOrder> orders;
	private List<OrderProducts> prods;
	private PaginationDTO pagination;
	
	private OrderProducts orderProduct;
	private SellerProdItemsVariInventory inventory;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public CustomerOrder getOrder() {
		return order;
	}
	public void setOrder(CustomerOrder order) {
		this.order = order;
	}
	public List<CustomerOrder> getOrders() {
		return orders;
	}
	public void setOrders(List<CustomerOrder> orders) {
		this.orders = orders;
	}
	public List<OrderProducts> getProds() {
		return prods;
	}
	public void setProds(List<OrderProducts> prods) {
		this.prods = prods;
	}
	public PaginationDTO getPagination() {
		return pagination;
	}
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}
	public OrderProducts getOrderProduct() {
		return orderProduct;
	}
	public void setOrderProduct(OrderProducts orderProduct) {
		this.orderProduct = orderProduct;
	}
	public SellerProdItemsVariInventory getInventory() {
		return inventory;
	}
	public void setInventory(SellerProdItemsVariInventory inventory) {
		this.inventory = inventory;
	}
	
}
