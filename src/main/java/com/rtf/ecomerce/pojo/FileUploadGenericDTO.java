package com.rtf.ecomerce.pojo;

public class FileUploadGenericDTO {
	
	private Integer sts;
	private String msg;
	private String awsfinalUrl ;
	public Integer getSts() {
		return sts;
	}
	public void setSts(Integer sts) {
		this.sts = sts;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getAwsfinalUrl() {
		return awsfinalUrl;
	}
	public void setAwsfinalUrl(String awsfinalUrl) {
		this.awsfinalUrl = awsfinalUrl;
	}

}
