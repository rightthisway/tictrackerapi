package com.rtf.ecomerce.pojo;

import java.util.List;

import com.rtf.ecomerce.data.SellerProductUpload;
import com.rtw.tmat.utils.Error;
import com.rtw.tracker.pojos.PaginationDTO;

public class SellerProductUploadTO {
	
	private List<SellerProductUpload> prodlst;	
	private Error error = new Error(); 
	private String message = new String();
	private Integer status;
	private PaginationDTO pagination;
	
	private String userName;
	
	public List<SellerProductUpload> getProdlst() {
		return prodlst;
	}
	public void setProdlst(List<SellerProductUpload> prodlst) {
		this.prodlst = prodlst;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public PaginationDTO getPagination() {
		return pagination;
	}
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}	

}
