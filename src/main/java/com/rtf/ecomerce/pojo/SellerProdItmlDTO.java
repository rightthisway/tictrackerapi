package com.rtf.ecomerce.pojo;

import java.util.List;

import com.rtf.ecomerce.data.RTFSellerDetails;
import com.rtf.ecomerce.data.SellerProdItemsVariInventory;
import com.rtf.ecomerce.data.SellerProdItemsVariName;
import com.rtf.ecomerce.data.SellerProductsItems;
import com.rtw.tmat.utils.Error;
import com.rtw.tracker.pojos.PaginationDTO;

public class SellerProdItmlDTO {

	private List<SellerProductsItems> list;
	private SellerProductsItems sellerProductsItem;
	private Error error = new Error(); 
	private String message = new String();
	private Integer status;
	private PaginationDTO pagination;
	private Boolean hasInventory;
	
	private List<SellerProdItemsVariName> variantsList;
	private List<SellerProdItemsVariInventory> inventoryList;
	
	
	public List<SellerProductsItems> getList() {
		return list;
	}
	public void setList(List<SellerProductsItems> list) {
		this.list = list;
	}
	public SellerProductsItems getSellerProductsItem() {
		return sellerProductsItem;
	}
	public void setSellerProductsItem(SellerProductsItems sellerProductsItem) {
		this.sellerProductsItem = sellerProductsItem;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public PaginationDTO getPagination() {
		return pagination;
	}
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}
	public List<SellerProdItemsVariName> getVariantsList() {
		return variantsList;
	}
	public void setVariantsList(List<SellerProdItemsVariName> variantsList) {
		this.variantsList = variantsList;
	}
	public List<SellerProdItemsVariInventory> getInventoryList() {
		return inventoryList;
	}
	public void setInventoryList(List<SellerProdItemsVariInventory> inventoryList) {
		this.inventoryList = inventoryList;
	}
	public Boolean getHasInventory() {
		return hasInventory;
	}
	public void setHasInventory(Boolean hasInventory) {
		this.hasInventory = hasInventory;
	}
	
	
}
