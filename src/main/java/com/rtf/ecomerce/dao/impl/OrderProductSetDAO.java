package com.rtf.ecomerce.dao.impl;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtf.ecomerce.data.CustomerOrder;
import com.rtf.ecomerce.data.OrderProducts;
import com.rtf.ecomerce.utils.GridHeaderFilters;
import com.rtw.tmat.utils.Constants;
import com.rtw.tracker.dao.implementation.HibernateDAO;


public class OrderProductSetDAO extends HibernateDAO<Integer, OrderProducts> implements com.rtf.ecomerce.dao.service.OrderProductSetDAO{

	@Override
	public List<OrderProducts> getOrderProductByOrderId(Integer orderId) {
		return find("FROM OrderProducts WHERE orderId=?",new Object[]{orderId});
	}

	@SuppressWarnings("unchecked")
	public List<OrderProducts> getAllRtfOrderProductDetails(GridHeaderFilters filter,String status,Integer userId,Integer orderId){
		List<OrderProducts> prodsList = null;
		Session session = null;
		try{
			
			String sqlQuery = "select pr.id as id,order_id as orderId,pr.cust_id as custId,prod_name as pName,prod_desc as pDesc,qty as qty,qty_type as qtyType,"
					+ " prod_img_url as pImgUrl,final_sellprice as fSelPrice,used_loyalpnts as usedLPnts," +
					" price_with_loyalpnts as priceWithLPnts,inv.vopt_name_val_com as vOptNameValCom," + 
					" pr.tax_chares as tax,pr.prod_cou_code as pCpnCode,pr.cust_cou_code as custCpnCode,pr.shipping_method as shMethod,"
					+ " pr.shippingclass as shClass,pr.shipping_trackID as shTrackId," + 
					" pr.delivery_note as delNote,pr.exp_delivery_date as expDelDate,pr.shipping_status as shippingStatus,pr.order_status as status," +
					" c.cust_name as custFirstName,c.last_name as custLastName,c.email as custEmail,c.phone as custPhone,"
					+ " co.sh_addr1 as shAddr1,co.sh_addr2 as shAddr2,co.sh_city as shCity,co.sh_state as shState,co.sh_country as shCountry,co.sh_zip as shZip,"
					+ " co.sh_fname as shFName,co.sh_lname as shLName, co.sh_phone as shPhone,co.sh_email as shEmail,inv.prod_ws_url as productUrl "
					+ "  from rtf_order_products pr " +
					" inner join rtf_customer_order as co on co.id=pr.order_id" +
					" inner join "+Constants.RTFECOM_TO_ZONES_DB_LINK_SERVER+"customer c on c.id=co.cust_id" +
					" inner join rtf_seller_product_items_variants_inventory as inv on inv.slrinv_id=pr.slrinv_id" + 
					" where co.id="+orderId;//+" and co.status='"+status+"' ";
			if(userId != null) {
				sqlQuery += " and pr.sler_id in (select sler_id from rtf_seller_dtls where sler_login_id="+userId+")";
			}
			if(status.equals("ACTIVE")) {
				sqlQuery += " and (pr.order_status is null or pr.order_status='ACTIVE' )";
			} else if(status.equals("ALL")) {
				sqlQuery += " and (pr.order_status is null or pr.order_status in ('ACTIVE','ACCEPTED','FULFILLED') )";
			} else  {
				sqlQuery += " and pr.order_status='"+status+"'";
			} 
			
			session = getSession();
			SQLQuery query = session.createSQLQuery(sqlQuery);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("orderId", Hibernate.INTEGER);
			query.addScalar("custId", Hibernate.INTEGER);
			query.addScalar("pName", Hibernate.STRING);
			query.addScalar("pDesc", Hibernate.STRING);
			query.addScalar("qty", Hibernate.INTEGER);
			query.addScalar("qtyType", Hibernate.STRING);						
			query.addScalar("pImgUrl", Hibernate.STRING);
			query.addScalar("fSelPrice", Hibernate.DOUBLE);
			query.addScalar("usedLPnts", Hibernate.INTEGER);
			query.addScalar("priceWithLPnts", Hibernate.DOUBLE);
			query.addScalar("vOptNameValCom", Hibernate.STRING);
			query.addScalar("tax", Hibernate.DOUBLE);
			query.addScalar("pCpnCode", Hibernate.STRING);
			query.addScalar("custCpnCode", Hibernate.STRING);
			query.addScalar("shMethod", Hibernate.STRING);
			query.addScalar("shClass", Hibernate.STRING);
			query.addScalar("shTrackId", Hibernate.STRING);
			query.addScalar("delNote", Hibernate.STRING);
			query.addScalar("expDelDate", Hibernate.TIMESTAMP);
			query.addScalar("shippingStatus", Hibernate.STRING);
			query.addScalar("status", Hibernate.STRING);
			
			query.addScalar("custFirstName", Hibernate.STRING);
			query.addScalar("custLastName", Hibernate.STRING);
			query.addScalar("custEmail", Hibernate.STRING);
			query.addScalar("custPhone", Hibernate.STRING);
			
			query.addScalar("shAddr1", Hibernate.STRING);
			query.addScalar("shAddr2", Hibernate.STRING);
			query.addScalar("shCity", Hibernate.STRING);
			query.addScalar("shState", Hibernate.STRING);
			query.addScalar("shCountry", Hibernate.STRING);
			query.addScalar("shZip", Hibernate.STRING);
			query.addScalar("shFName", Hibernate.STRING);
			query.addScalar("shLName", Hibernate.STRING);
			query.addScalar("shPhone", Hibernate.STRING);
			query.addScalar("shEmail", Hibernate.STRING);
			query.addScalar("productUrl", Hibernate.STRING);
			
			prodsList = query.setResultTransformer(Transformers.aliasToBean(OrderProducts.class)).list();
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return prodsList;
	}
}
