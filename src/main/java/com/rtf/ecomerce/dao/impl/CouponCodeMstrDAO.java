package com.rtf.ecomerce.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.rtf.ecomerce.data.CouponCodeMst;
import com.rtf.ecomerce.utils.GridHeaderFilters;
import com.rtw.tmat.utils.Util;
import com.rtw.tracker.dao.implementation.HibernateDAO;

public class CouponCodeMstrDAO extends HibernateDAO<Integer, CouponCodeMst>
		implements com.rtf.ecomerce.dao.service.CouponCodeMstrDAO {

	public CouponCodeMst getCouponCodeMstr(Integer ccId) {
		return findSingle("from CouponCodeMst where ccId=? ", new Object[] { ccId });
	}

	@SuppressWarnings("unchecked")
	public List<CouponCodeMst> getAllCouponCodes(GridHeaderFilters filter,String couponStatus) {
		List<CouponCodeMst> rewardList = new ArrayList<CouponCodeMst>();
		try {
			String sqlQuery = "From CouponCodeMst where coustatus = '" + couponStatus + "'" ;

			if (filter.getCoucde() != null) {
				sqlQuery += " AND coucde like '%" + filter.getCoucde() + "%' ";
			}
			if (filter.getCouname() != null) {
				sqlQuery += " AND couname like '%" + filter.getCouname() + "%' ";
			}
			if (filter.getCoudesc() != null) {
				sqlQuery += " AND coudesc like '%" + filter.getCoudesc() + "%' ";
			}
			if (filter.getCoudiscount() != null) {
				sqlQuery += " AND coudiscount like % " + filter.getCoudiscount() + "% ";
			}
			if (filter.getCoustatus() != null) {
				sqlQuery += " AND coustatus like '% " + filter.getCoustatus() + "%' ";
			}
			if (filter.getDiscounttype() != null) {
				sqlQuery += " AND discounttype like '% " + filter.getDiscounttype() + "%' ";
			}
			if (filter.getCoutype() != null) {
				sqlQuery += " AND coutype like '% " + filter.getCoutype() + "%' ";
			}
			if (filter.getLastUpdatedBy() != null) {
				sqlQuery += " AND updBy like '% " + filter.getLastUpdatedBy() + "%' ";
			}
			if (filter.getLastUpdatedDateStr() != null) {
				String dateTimeStr = filter.getLastUpdatedDateStr();
				if (dateTimeStr.contains(" ")) {
					String dateTimeArr[] = dateTimeStr.split(" ");
					if (Util.extractDateElement(dateTimeArr[0], "DAY") > 0) {
						sqlQuery += " AND DATEPART(day,updDate) = " + Util.extractDateElement(dateTimeArr[0], "DAY")
								+ " ";
					}
					if (Util.extractDateElement(dateTimeArr[0], "MONTH") > 0) {
						sqlQuery += " AND DATEPART(month,updDate) = " + Util.extractDateElement(dateTimeArr[0], "MONTH")
								+ " ";
					}
					if (Util.extractDateElement(dateTimeArr[0], "YEAR") > 0) {
						sqlQuery += " AND DATEPART(year,updDate) = " + Util.extractDateElement(dateTimeArr[0], "YEAR")
								+ " ";
					}
					if (dateTimeArr.length > 1) {
						String dateTime = dateTimeArr[1];
						if (dateTimeArr.length > 2) {
							if (dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()) {
								dateTime += " " + dateTimeArr[2];
							}
						}
						if (Util.extractDateElement(dateTime, "HOUR") > 0) {
							sqlQuery += " AND DATEPART(hour,updDate) = " + Util.extractDateElement(dateTime, "HOUR")
									+ " ";
						}
						if (Util.extractDateElement(dateTime, "MINUTE") > 0) {
							sqlQuery += " AND DATEPART(minute,updDate) = " + Util.extractDateElement(dateTime, "MINUTE")
									+ " ";
						}
					}
				} else {
					if (Util.extractDateElement(filter.getLastUpdatedDateStr(), "DAY") > 0) {
						sqlQuery += " AND DATEPART(day,updDate) = "
								+ Util.extractDateElement(filter.getLastUpdatedDateStr(), "DAY") + " ";
					}
					if (Util.extractDateElement(filter.getLastUpdatedDateStr(), "MONTH") > 0) {
						sqlQuery += " AND DATEPART(month,updDate) = "
								+ Util.extractDateElement(filter.getLastUpdatedDateStr(), "MONTH") + " ";
					}
					if (Util.extractDateElement(filter.getLastUpdatedDateStr(), "YEAR") > 0) {
						sqlQuery += " AND DATEPART(year,updDate) = "
								+ Util.extractDateElement(filter.getLastUpdatedDateStr(), "YEAR") + " ";
					}
				}
			}
			if (filter.getExprydateStr() != null) {
				String dateTimeStr = filter.getExprydateStr();
				if (dateTimeStr.contains(" ")) {
					String dateTimeArr[] = dateTimeStr.split(" ");
					if (Util.extractDateElement(dateTimeArr[0], "DAY") > 0) {
						sqlQuery += " AND DATEPART(day,exprydate) = " + Util.extractDateElement(dateTimeArr[0], "DAY")
								+ " ";
					}
					if (Util.extractDateElement(dateTimeArr[0], "MONTH") > 0) {
						sqlQuery += " AND DATEPART(month,exprydate) = "
								+ Util.extractDateElement(dateTimeArr[0], "MONTH") + " ";
					}
					if (Util.extractDateElement(dateTimeArr[0], "YEAR") > 0) {
						sqlQuery += " AND DATEPART(year,exprydate) = " + Util.extractDateElement(dateTimeArr[0], "YEAR")
								+ " ";
					}
					if (dateTimeArr.length > 1) {
						String dateTime = dateTimeArr[1];
						if (dateTimeArr.length > 2) {
							if (dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()) {
								dateTime += " " + dateTimeArr[2];
							}
						}
						if (Util.extractDateElement(dateTime, "HOUR") > 0) {
							sqlQuery += " AND DATEPART(hour,exprydate) = " + Util.extractDateElement(dateTime, "HOUR")
									+ " ";
						}
						if (Util.extractDateElement(dateTime, "MINUTE") > 0) {
							sqlQuery += " AND DATEPART(minute,exprydate) = "
									+ Util.extractDateElement(dateTime, "MINUTE") + " ";
						}
					}
				} else {
					if (Util.extractDateElement(filter.getExprydateStr(), "DAY") > 0) {
						sqlQuery += " AND DATEPART(day,exprydate) = "
								+ Util.extractDateElement(filter.getExprydateStr(), "DAY") + " ";
					}
					if (Util.extractDateElement(filter.getExprydateStr(), "MONTH") > 0) {
						sqlQuery += " AND DATEPART(month,exprydate) = "
								+ Util.extractDateElement(filter.getExprydateStr(), "MONTH") + " ";
					}
					if (Util.extractDateElement(filter.getExprydateStr(), "YEAR") > 0) {
						sqlQuery += " AND DATEPART(year,exprydate) = "
								+ Util.extractDateElement(filter.getExprydateStr(), "YEAR") + " ";
					}
				}
			}
			System.out.println("GET ALL COUPON CODE" + sqlQuery);
			return find(sqlQuery);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rewardList;
	}

	public Integer getAllCouponCodesCount(GridHeaderFilters filter) {
		Integer count = 0;
		Session session = null;
		try {
			String sqlQuery = "select count(*) as cnt from CouponCodeMst with(nolock) where 1 = 1";

			if (filter.getCoucde() != null) {
				sqlQuery += " AND coucde like '%" + filter.getCoucde() + "%' ";
			}
			if (filter.getCouname() != null) {
				sqlQuery += " AND couname like '%" + filter.getCouname() + "%' ";
			}
			if (filter.getCoudesc() != null) {
				sqlQuery += " AND coudesc like '%" + filter.getCoudesc() + "%' ";
			}
			if (filter.getCoudiscount() != null) {
				sqlQuery += " AND coudiscount like % " + filter.getCoudiscount() + "% ";
			}
			if (filter.getCoustatus() != null) {
				sqlQuery += " AND coustatus like '% " + filter.getCoustatus() + "%' ";
			}
			if (filter.getDiscounttype() != null) {
				sqlQuery += " AND discounttype like '% " + filter.getDiscounttype() + "%' ";
			}
			if (filter.getCoutype() != null) {
				sqlQuery += " AND coutype like '% " + filter.getCoutype() + "%' ";
			}
			if (filter.getLastUpdatedBy() != null) {
				sqlQuery += " AND updBy like '% " + filter.getLastUpdatedBy() + "%' ";
			}

			if (filter.getLastUpdatedDateStr() != null) {
				String dateTimeStr = filter.getLastUpdatedDateStr();
				if (dateTimeStr.contains(" ")) {
					String dateTimeArr[] = dateTimeStr.split(" ");
					if (Util.extractDateElement(dateTimeArr[0], "DAY") > 0) {
						sqlQuery += " AND DATEPART(day,lupd_date) = " + Util.extractDateElement(dateTimeArr[0], "DAY")
								+ " ";
					}
					if (Util.extractDateElement(dateTimeArr[0], "MONTH") > 0) {
						sqlQuery += " AND DATEPART(month,lupd_date) = "
								+ Util.extractDateElement(dateTimeArr[0], "MONTH") + " ";
					}
					if (Util.extractDateElement(dateTimeArr[0], "YEAR") > 0) {
						sqlQuery += " AND DATEPART(year,lupd_date) = " + Util.extractDateElement(dateTimeArr[0], "YEAR")
								+ " ";
					}
					if (dateTimeArr.length > 1) {
						String dateTime = dateTimeArr[1];
						if (dateTimeArr.length > 2) {
							if (dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()) {
								dateTime += " " + dateTimeArr[2];
							}
						}
						if (Util.extractDateElement(dateTime, "HOUR") > 0) {
							sqlQuery += " AND DATEPART(hour,lupd_date) = " + Util.extractDateElement(dateTime, "HOUR")
									+ " ";
						}
						if (Util.extractDateElement(dateTime, "MINUTE") > 0) {
							sqlQuery += " AND DATEPART(minute,lupd_date) = "
									+ Util.extractDateElement(dateTime, "MINUTE") + " ";
						}
					}
				} else {
					if (Util.extractDateElement(filter.getLastUpdatedDateStr(), "DAY") > 0) {
						sqlQuery += " AND DATEPART(day,lupd_date) = "
								+ Util.extractDateElement(filter.getLastUpdatedDateStr(), "DAY") + " ";
					}
					if (Util.extractDateElement(filter.getLastUpdatedDateStr(), "MONTH") > 0) {
						sqlQuery += " AND DATEPART(month,lupd_date) = "
								+ Util.extractDateElement(filter.getLastUpdatedDateStr(), "MONTH") + " ";
					}
					if (Util.extractDateElement(filter.getLastUpdatedDateStr(), "YEAR") > 0) {
						sqlQuery += " AND DATEPART(year,lupd_date) = "
								+ Util.extractDateElement(filter.getLastUpdatedDateStr(), "YEAR") + " ";
					}
				}
			}
			if (filter.getExprydateStr() != null) {
				String dateTimeStr = filter.getExprydateStr();
				if (dateTimeStr.contains(" ")) {
					String dateTimeArr[] = dateTimeStr.split(" ");
					if (Util.extractDateElement(dateTimeArr[0], "DAY") > 0) {
						sqlQuery += " AND DATEPART(day,exprydate) = " + Util.extractDateElement(dateTimeArr[0], "DAY")
								+ " ";
					}
					if (Util.extractDateElement(dateTimeArr[0], "MONTH") > 0) {
						sqlQuery += " AND DATEPART(month,exprydate) = "
								+ Util.extractDateElement(dateTimeArr[0], "MONTH") + " ";
					}
					if (Util.extractDateElement(dateTimeArr[0], "YEAR") > 0) {
						sqlQuery += " AND DATEPART(year,exprydate) = " + Util.extractDateElement(dateTimeArr[0], "YEAR")
								+ " ";
					}
					if (dateTimeArr.length > 1) {
						String dateTime = dateTimeArr[1];
						if (dateTimeArr.length > 2) {
							if (dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()) {
								dateTime += " " + dateTimeArr[2];
							}
						}
						if (Util.extractDateElement(dateTime, "HOUR") > 0) {
							sqlQuery += " AND DATEPART(hour,exprydate) = " + Util.extractDateElement(dateTime, "HOUR")
									+ " ";
						}
						if (Util.extractDateElement(dateTime, "MINUTE") > 0) {
							sqlQuery += " AND DATEPART(minute,exprydate) = "
									+ Util.extractDateElement(dateTime, "MINUTE") + " ";
						}
					}
				} else {
					if (Util.extractDateElement(filter.getExprydateStr(), "DAY") > 0) {
						sqlQuery += " AND DATEPART(day,exprydate) = "
								+ Util.extractDateElement(filter.getExprydateStr(), "DAY") + " ";
					}
					if (Util.extractDateElement(filter.getExprydateStr(), "MONTH") > 0) {
						sqlQuery += " AND DATEPART(month,exprydate) = "
								+ Util.extractDateElement(filter.getExprydateStr(), "MONTH") + " ";
					}
					if (Util.extractDateElement(filter.getExprydateStr(), "YEAR") > 0) {
						sqlQuery += " AND DATEPART(year,exprydate) = "
								+ Util.extractDateElement(filter.getExprydateStr(), "YEAR") + " ";
					}
				}
			}
			session = getSession();
			SQLQuery query = session.createSQLQuery(sqlQuery);
			count = Integer.parseInt(String.valueOf(query.uniqueResult()));

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return count;
	}
	
	public Boolean getCouponCodeMstrByCouponName(String couponName) {
		Session session = null;
		Boolean isExists= false;
		try {
		session = getSession();
		SQLQuery query = session.createSQLQuery("select cou_code FROM rtf_coupon_product_map where cou_code='" +couponName + "'");	
		List<?> resultSet = query.list();
		if(resultSet== null || resultSet.size() == 0 ) {
			isExists = false;
		}else {
			isExists = true;
			System.out.println("[[ COUPON CODE MASTER SAME NAME EXISTS FOR COUPON CODE ] " );
		}
	
	
	}catch(Exception e){
		e.printStackTrace();
	}
	finally {
		if (session != null && session.isOpen()) {
			session.close();
		}
	}		
			return isExists;
	}
		
	

}
