package com.rtf.ecomerce.dao.impl;

import com.rtf.ecomerce.dao.service.CouponCodeMstrDAO;
import com.rtf.ecomerce.dao.service.CouponCodeProductMapDAO;
import com.rtf.ecomerce.dao.service.CustomerOrderDAO;
import com.rtf.ecomerce.dao.service.OrderProductSetDAO;
import com.rtf.ecomerce.dao.service.RTFSellerDetailsDAO;
import com.rtf.ecomerce.dao.service.RtfOrderRefundDAO;
import com.rtf.ecomerce.dao.service.SellerProdItemsVariInventoryDAO;
import com.rtf.ecomerce.dao.service.SellerProdItemsVariNameDAO;
import com.rtf.ecomerce.dao.service.SellerProdItemsVariValueDAO;

public class EcommDAORegistry {
	
	private static RTFSellerDetailsDAO rTFSellerDetailsDAO;
	public static SellerProductsItemsDAO sellerProductsItemsDAO;
	public static SellerProdItemsVariNameDAO sellerProdItemsVariNameDAO;
	public static SellerProdItemsVariValueDAO sellerProdItemsVariValueDAO;
	public static SellerProdItemsVariInventoryDAO sellerProdItemsVariInventoryDAO;
	public static CustomerOrderDAO customerProdOrderDAO;
	public static OrderProductSetDAO orderProductSetDAO;
	public static CouponCodeMstrDAO couponCodeMstrDAO;
	public static CouponCodeProductMapDAO couponCodeProductMapDAO;
	public static RtfOrderRefundDAO rtfOrderRefundDAO;
	
	
	public static RTFSellerDetailsDAO getrTFSellerDetailsDAO() {
		return rTFSellerDetailsDAO;
	}

	public final void setrTFSellerDetailsDAO(RTFSellerDetailsDAO rTFSellerDetailsDAO) {
		EcommDAORegistry.rTFSellerDetailsDAO = rTFSellerDetailsDAO;
	}

	public static SellerProdItemsVariNameDAO getSellerProdItemsVariNameDAO() {
		return sellerProdItemsVariNameDAO;
	}

	public final void setSellerProdItemsVariNameDAO(SellerProdItemsVariNameDAO sellerProdItemsVariNameDAO) {
		EcommDAORegistry.sellerProdItemsVariNameDAO = sellerProdItemsVariNameDAO;
	}

	public static SellerProdItemsVariValueDAO getSellerProdItemsVariValueDAO() {
		return sellerProdItemsVariValueDAO;
	}

	public final void setSellerProdItemsVariValueDAO(SellerProdItemsVariValueDAO sellerProdItemsVariValueDAO) {
		EcommDAORegistry.sellerProdItemsVariValueDAO = sellerProdItemsVariValueDAO;
	}

	public static SellerProdItemsVariInventoryDAO getSellerProdItemsVariInventoryDAO() {
		return sellerProdItemsVariInventoryDAO;
	}

	public final void setSellerProdItemsVariInventoryDAO(SellerProdItemsVariInventoryDAO sellerProdItemsVariInventoryDAO) {
		EcommDAORegistry.sellerProdItemsVariInventoryDAO = sellerProdItemsVariInventoryDAO;
	}

	public static SellerProductsItemsDAO getSellerProductsItemsDAO() {
		return sellerProductsItemsDAO;
	}

	public final void setSellerProductsItemsDAO(SellerProductsItemsDAO sellerProductsItemsDAO) {
		EcommDAORegistry.sellerProductsItemsDAO = sellerProductsItemsDAO;
	}

	public static CustomerOrderDAO getCustomerProdOrderDAO() {
		return customerProdOrderDAO;
	}

	public final void setCustomerProdOrderDAO(CustomerOrderDAO customerProdOrderDAO) {
		EcommDAORegistry.customerProdOrderDAO = customerProdOrderDAO;
	}

	public static OrderProductSetDAO getOrderProductSetDAO() {
		return orderProductSetDAO;
	}

	public final void setOrderProductSetDAO(OrderProductSetDAO orderProductSetDAO) {
		EcommDAORegistry.orderProductSetDAO = orderProductSetDAO;
	}
	public static CouponCodeMstrDAO getCouponCodeMstrDAO() {
		return couponCodeMstrDAO;
	}

	public static void setCouponCodeMstrDAO(CouponCodeMstrDAO couponCodeMstrDAO) {
		EcommDAORegistry.couponCodeMstrDAO = couponCodeMstrDAO;
	}

	public static CouponCodeProductMapDAO getCouponCodeProductMapDAO() {
		return couponCodeProductMapDAO;
	}

	public final void setCouponCodeProductMapDAO(CouponCodeProductMapDAO couponCodeProductMapDAO) {
		EcommDAORegistry.couponCodeProductMapDAO = couponCodeProductMapDAO;
	}

	public static RtfOrderRefundDAO getRtfOrderRefundDAO() {
		return rtfOrderRefundDAO;
	}

	public final void setRtfOrderRefundDAO(RtfOrderRefundDAO rtfOrderRefundDAO) {
		EcommDAORegistry.rtfOrderRefundDAO = rtfOrderRefundDAO;
	}
	
	
	
	
}