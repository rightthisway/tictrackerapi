package com.rtf.ecomerce.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtf.ecomerce.data.CustomerOrder;
import com.rtf.ecomerce.data.RTFSellerDetails;
import com.rtf.ecomerce.utils.GridHeaderFilters;
import com.rtw.tmat.utils.Constants;
import com.rtw.tmat.utils.Util;
import com.rtw.tracker.dao.implementation.HibernateDAO;
import com.rtw.tracker.datas.FanClub;

public class CustomerOrderDAO extends HibernateDAO<Integer, CustomerOrder> implements com.rtf.ecomerce.dao.service.CustomerOrderDAO{

	
	public List<CustomerOrder> getAllCustomerOrdersByCustId(Integer custId) throws Exception {
		return find("FROM CustomerOrder WHERE custId=? order by crDate desc",new Object[]{custId});
	}
	public CustomerOrder getACustomerOrderByOrderId(Integer orderId) throws Exception {
		return findSingle("FROM CustomerOrder WHERE id=? ",new Object[]{orderId});
	}
	public CustomerOrder getCustomerOrderByOrderIdAndCustomerId(Integer orderId,Integer custId) throws Exception {
		return findSingle("FROM CustomerOrder WHERE id=? and custId=? ",new Object[]{orderId,custId});
	}
	
	@SuppressWarnings("unchecked")
	public List<CustomerOrder> getAllRTFProdOrderDetails(GridHeaderFilters filter,String status,Integer userId){
		List<CustomerOrder> orderList = null;
		Session session = null;
		try{
			
			
			String sqlQuery = " select o.id as id,o.cust_id as custId,o.net_total as netTot,o.discount as disc,prm_pay_method as pPayType,prm_pay_amt as pPayAmt," + 
					" o.sec_pay_method as sPayType,o.sec_pay_amt as sPayAmt,o.shipping_charges as shCharges,o.tax_chares as tax,o.cr_date as crDate," + 
					" o.order_type as orderType,o.status as status," + 
					" c.id as custId, c.user_id as custUserId,c.cust_name as custFirstName,c.last_name as custLastName,c.email as custEmail,c.phone as custPhone,"
					+ " o.sh_addr1 as shAddr1,o.sh_addr2 as shAddr2,o.sh_city as shCity,o.sh_state as shState,o.sh_country as shCountry,o.sh_zip as shZip,"
					+ " o.sh_fname as shFName,o.sh_lname as shLName, o.sh_phone as shPhone,o.sh_email as shEmail,"
					+ " count(op.id) as noOfItems" + 
					" from rtf_customer_order o" + 
					" inner join "+Constants.RTFECOM_TO_ZONES_DB_LINK_SERVER+"customer c on c.id=o.cust_id" + 
					" inner join rtf_order_products op on op.order_id=o.id" + 
					" where o.status='ACTIVE' ";
			if(userId != null) {
				sqlQuery += " and op.sler_id in (select sel.sler_id from rtf_seller_dtls sel where sel.sler_login_id="+userId+")";
			}
			if(status.equals("ACTIVE")) {
				sqlQuery += " and (op.order_status is null or op.order_status='ACTIVE' )";
			} else if(status.equals("ALL")) {
				sqlQuery += " and (op.order_status is null or op.order_status in ('ACTIVE','ACCEPTED','FULFILLED') )";
			} else  {
				sqlQuery += " and op.order_status='"+status+"'";
			} 
			
			if(filter.getOrderId() != null){
				sqlQuery += " AND o.id like "+filter.getOrderId()+" ";
			}
			if(filter.getNetTot() != null){
				sqlQuery += " AND o.net_total like '%"+filter.getNetTot()+"%' ";
			}
			/*if(filter.getNoOfItems() != null){
				sqlQuery += " AND comp_name like '%"+filter.getNoOfItems()+"%' ";	
			}*/
			if(filter.getStatus() != null){
				sqlQuery += " AND op.order_status like '%"+filter.getStatus()+"%' ";	
			}
			if(filter.getpPayType() != null){
				sqlQuery += " AND o.prm_pay_method like '%"+filter.getpPayType()+"%' ";
			}
			if(filter.getpPayAmt() != null){
				sqlQuery += " AND o.prm_pay_amt like '%"+filter.getpPayAmt()+"%' ";
			}
			if(filter.getsPayType() != null){
				sqlQuery += " AND o.sec_pay_method like '%"+filter.getsPayType()+"%' ";	
			}
			if(filter.getsPayAmt() != null){
				sqlQuery += " AND o.sec_pay_amt like '%"+filter.getsPayAmt()+"%' ";
			}
			if(filter.getShCharges() != null){
				sqlQuery += " AND o.shipping_charges like '%"+filter.getShCharges()+"%' ";
			}
			if(filter.getTax() != null){
				sqlQuery += " AND o.tax_chares like '%"+filter.getTax()+"%' ";	
			}
			if(filter.getfName() != null){
				sqlQuery += " AND c.cust_name like '%"+filter.getfName()+"%' ";
			}
			if(filter.getlName() != null){
				sqlQuery += " AND c.last_name like '%"+filter.getlName()+"%' ";
			}
			if(filter.getCustEmail() != null){
				sqlQuery += " AND c.email like '%"+filter.getCustEmail()+"%' ";
			}
			if(filter.getCustPhone() != null){
				sqlQuery += " AND c.phone like '%"+filter.getCustPhone()+"%' ";
			}
			if(filter.getShAddr1() != null){
				sqlQuery += " AND o.sh_addr1  like '%"+filter.getShAddr1()+"%' ";
			}
			if(filter.getShAddr2() != null){
				sqlQuery += " AND o.sh_addr2 like '%"+filter.getShAddr2()+"%' ";
			}
			if(filter.getShCity() != null){
				sqlQuery += " AND o.sh_city like '%"+filter.getShCity()+"%' ";
			}
			if(filter.getShState() != null){
				sqlQuery += " AND o.sh_state like '%"+filter.getShState()+"%' ";
			}
			if(filter.getShCountry() != null){
				sqlQuery += " AND o.sh_country like '%"+filter.getShCountry()+"%' ";
			}
			if(filter.getShZip() != null){
				sqlQuery += " AND o.sh_zip like '%"+filter.getShZip()+"%' ";
			}
			if(filter.getShFName() != null){
				sqlQuery += " AND o.sh_fname like '%"+filter.getShFName()+"%' ";
			}
			if(filter.getShLName() != null){
				sqlQuery += " AND o.sh_lname like '%"+filter.getShLName()+"%' ";
			}
			if(filter.getShPhone() != null){
				sqlQuery += " AND o.sh_phone like '%"+filter.getShPhone()+"%' ";
			}
			if(filter.getShEmail() != null){
				sqlQuery += " AND o.sh_email like '%"+filter.getShEmail()+"%' ";
			}
			
			
			if(filter.getCrDateStr() != null){
				String dateTimeStr = filter.getCrDateStr();
				if(dateTimeStr.contains(" ")){
					String dateTimeArr[] = dateTimeStr.split(" ");
					if(Util.extractDateElement(dateTimeArr[0],"DAY") > 0){
						sqlQuery += " AND DATEPART(day,o.cr_date) = "+Util.extractDateElement(dateTimeArr[0],"DAY")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,o.cr_date) = "+Util.extractDateElement(dateTimeArr[0],"MONTH")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,o.cr_date) = "+Util.extractDateElement(dateTimeArr[0],"YEAR")+" ";
					}
					if(dateTimeArr.length>1){
						String dateTime = dateTimeArr[1];
						if(dateTimeArr.length > 2){
							if(dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()){
								dateTime += " "+dateTimeArr[2];
							}
						}
						if(Util.extractDateElement(dateTime,"HOUR") > 0){
							sqlQuery += " AND DATEPART(hour,o.cr_date) = "+Util.extractDateElement(dateTime,"HOUR")+" ";
						}
						if(Util.extractDateElement(dateTime,"MINUTE") > 0){
							sqlQuery += " AND DATEPART(minute,o.cr_date) = "+Util.extractDateElement(dateTime,"MINUTE")+" ";
						}
					}
				}else{
					if(Util.extractDateElement(filter.getCrDateStr(),"DAY") > 0){
						sqlQuery += " AND DATEPART(day,o.cr_date) = "+Util.extractDateElement(filter.getCrDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getCrDateStr(),"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,o.cr_date) = "+Util.extractDateElement(filter.getCrDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getCrDateStr(),"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,o.cr_date) = "+Util.extractDateElement(filter.getCrDateStr(),"YEAR")+" ";
					}
				}
			}
			sqlQuery += " group by o.id ,o.cust_id ,o.net_total ,o.discount ,prm_pay_method,prm_pay_amt," + 
					"o.sec_pay_method,o.sec_pay_amt ,o.shipping_charges ,o.tax_chares ,o.cr_date ," + 
					"o.order_type ,o.status," + 
					"c.id , c.user_id,c.cust_name,c.last_name,c.email,c.phone,"
					+ " o.sh_addr1,o.sh_addr2,o.sh_city,o.sh_state,o.sh_country,o.sh_zip, o.sh_fname,o.sh_lname, o.sh_phone,o.sh_email";
			
			if(filter.getNoOfItems() != null){
				sqlQuery += " having count(op.id)="+filter.getNoOfItems();	
			}
			
			session = getSession();
			SQLQuery query = session.createSQLQuery(sqlQuery);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("custId", Hibernate.INTEGER);
			query.addScalar("netTot", Hibernate.DOUBLE);
			query.addScalar("disc", Hibernate.DOUBLE);
			query.addScalar("pPayType", Hibernate.STRING);
			query.addScalar("pPayAmt", Hibernate.DOUBLE);						
			query.addScalar("sPayType", Hibernate.STRING);
			query.addScalar("sPayAmt", Hibernate.DOUBLE);
			query.addScalar("shCharges", Hibernate.DOUBLE);
			query.addScalar("tax", Hibernate.DOUBLE);
			query.addScalar("orderType", Hibernate.STRING);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("custId", Hibernate.INTEGER);
			query.addScalar("custUserId", Hibernate.STRING);
			query.addScalar("custFirstName", Hibernate.STRING);
			query.addScalar("custLastName", Hibernate.STRING);
			query.addScalar("custEmail", Hibernate.STRING);
			query.addScalar("custPhone", Hibernate.STRING);
			query.addScalar("noOfItems", Hibernate.INTEGER);
			
			query.addScalar("shAddr1", Hibernate.STRING);
			query.addScalar("shAddr2", Hibernate.STRING);
			query.addScalar("shCity", Hibernate.STRING);
			query.addScalar("shState", Hibernate.STRING);
			query.addScalar("shCountry", Hibernate.STRING);
			query.addScalar("shZip", Hibernate.STRING);
			query.addScalar("shFName", Hibernate.STRING);
			query.addScalar("shLName", Hibernate.STRING);
			query.addScalar("shPhone", Hibernate.STRING);
			query.addScalar("shEmail", Hibernate.STRING);
			
			
			query.addScalar("status", Hibernate.STRING);			
			query.addScalar("crDate", Hibernate.TIMESTAMP);
			orderList = query.setResultTransformer(Transformers.aliasToBean(CustomerOrder.class)).list();
		}catch(Exception e){
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return orderList;
	}
	
	public Integer getAllRTFSellerDetailsCount(GridHeaderFilters filter,String status){
		Integer count = 0;
		Session session = null;
		try{
			String sqlQuery = "select count(*) as cnt from rtf_seller_dtls with(nolock) where sler_status='"+status+"' ";

			if(filter.getfName() != null){
				sqlQuery += " AND first_name like '%"+filter.getfName()+"%' ";
			}
			if(filter.getlName() != null){
				sqlQuery += " AND last_name like '%"+filter.getlName()+"%' ";
			}
			if(filter.getCompanyName() != null){
				sqlQuery += " AND comp_name like '%"+filter.getCompanyName()+"%' ";	
			}
			if(filter.getEmail() != null){
				sqlQuery += " AND email like '%"+filter.getEmail()+"%' ";
			}
			if(filter.getPhone() != null){
				sqlQuery += " AND phone like '%"+filter.getPhone()+"%' ";
			}
			if(filter.getAddressLine1() != null){
				sqlQuery += " AND addr_line_one like '%"+filter.getAddressLine1()+"%' ";	
			}
			if(filter.getAddressLine2() != null){
				sqlQuery += " AND addr_line_two like '%"+filter.getAddressLine2()+"%' ";
			}
			if(filter.getCity() != null){
				sqlQuery += " AND city like '%"+filter.getCity()+"%' ";
			}
			if(filter.getState() != null){
				sqlQuery += " AND state like '%"+filter.getState()+"%' ";	
			}
			if(filter.getCountry() != null){
				sqlQuery += " AND country like '%"+filter.getCountry()+"%' ";	
			}
			if(filter.getZipCode() != null){
				sqlQuery += " AND pincode like '%"+filter.getZipCode()+"%' ";	
			}
			if(filter.getStatus() != null){
				sqlQuery += " AND sler_status like '%"+filter.getStatus()+"%' ";	
			}
			if(filter.getLastUpdatedBy() != null){
				sqlQuery += " AND lupd_by like '%"+filter.getLastUpdatedBy()+"%' ";	
			}
			if(filter.getCrDateStr() != null){
				String dateTimeStr = filter.getCrDateStr();
				if(dateTimeStr.contains(" ")){
					String dateTimeArr[] = dateTimeStr.split(" ");
					if(Util.extractDateElement(dateTimeArr[0],"DAY") > 0){
						sqlQuery += " AND DATEPART(day,o.cr_date) = "+Util.extractDateElement(dateTimeArr[0],"DAY")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,o.cr_date) = "+Util.extractDateElement(dateTimeArr[0],"MONTH")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,o.cr_date) = "+Util.extractDateElement(dateTimeArr[0],"YEAR")+" ";
					}
					if(dateTimeArr.length>1){
						String dateTime = dateTimeArr[1];
						if(dateTimeArr.length > 2){
							if(dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()){
								dateTime += " "+dateTimeArr[2];
							}
						}
						if(Util.extractDateElement(dateTime,"HOUR") > 0){
							sqlQuery += " AND DATEPART(hour,o.cr_date) = "+Util.extractDateElement(dateTime,"HOUR")+" ";
						}
						if(Util.extractDateElement(dateTime,"MINUTE") > 0){
							sqlQuery += " AND DATEPART(minute,o.cr_date) = "+Util.extractDateElement(dateTime,"MINUTE")+" ";
						}
					}
				}else{
					if(Util.extractDateElement(filter.getCrDateStr(),"DAY") > 0){
						sqlQuery += " AND DATEPART(day,o.cr_date) = "+Util.extractDateElement(filter.getCrDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getCrDateStr(),"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,o.cr_date) = "+Util.extractDateElement(filter.getCrDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getCrDateStr(),"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,o.cr_date) = "+Util.extractDateElement(filter.getCrDateStr(),"YEAR")+" ";
					}
				}
			}
			
			session = getSession();
			SQLQuery query = session.createSQLQuery(sqlQuery);
			count = Integer.parseInt(String.valueOf(query.uniqueResult()));
		}catch(Exception e){
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return count;
	}
}
