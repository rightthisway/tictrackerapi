package com.rtf.ecomerce.dao.impl;

import java.util.ArrayList;
import java.util.List;

import com.rtf.ecomerce.data.RTFSellerDetails;
import com.rtf.ecomerce.data.SellerProductsItems;
import com.rtf.ecomerce.utils.GridHeaderFilters;
import com.rtw.tmat.utils.Util;
import com.rtw.tracker.dao.implementation.HibernateDAO;

public class SellerProductsItemsDAO extends HibernateDAO<Integer, SellerProductsItems> implements com.rtf.ecomerce.dao.service.SellerProductsItemsDAO{

	@Override
	public List<SellerProductsItems> getProductsByStatus(String status) {
		return find("FROM SellerProductsItems WHERE status=?  Order by dispOrder",new Object[]{status});
	}

	@Override
	public SellerProductsItems getProductsById(Integer prodId) {
		return findSingle("FROM SellerProductsItems WHERE spiId=? AND status=?" , new Object[]{prodId,"ACTIVE"});
	}
	
	@SuppressWarnings("unchecked")
	public List<SellerProductsItems> getAllRTFSellerDetails(GridHeaderFilters filter,String status){
		List<SellerProductsItems> rewardList = new ArrayList<SellerProductsItems>();
		try{
//			var loadProdColumns = ["prodId","pBrand", "pName","pDesc", "pRegMinPrice", "psMinPrice","maxCustQty","pExpDate",
			  //                         "status","updatedDate", "updatedBy","viewImg","editProd"];
			
			String sqlQuery = "From SellerProductsItems where status='"+status+"' ";
			
			if(filter.getSpiId() != null){
				sqlQuery += " AND spiId = "+filter.getSpiId()+" ";
			}
			if(filter.getpBrand() != null){
				sqlQuery += " AND pBrand like '%"+filter.getpBrand()+"%' ";
			}
			if(filter.getPname() != null){
				sqlQuery += " AND pName like '%"+filter.getPname()+"%' ";	
			}
			if(filter.getpDesc() != null){
				sqlQuery += " AND pDesc like '% "+filter.getpDesc()+"%' ";
			}
			if(filter.getpRegMinPrice() != null){
				sqlQuery += " AND pRegMinPrice =  "+filter.getpRegMinPrice()+"";
			}
			if(filter.getPsMinPrice() != null){
				sqlQuery += " AND psMinPrice = "+filter.getPsMinPrice()+"";	
			}
			if(filter.getMaxCustQty() != null){
				sqlQuery += " AND maxCustQty = "+filter.getMaxCustQty()+" ";
			}
			if(filter.getCount()!= null){
				sqlQuery += " AND dispOrder = "+filter.getCount()+" ";
			}
			if(filter.getStatus() != null){
				sqlQuery += " AND status like '% "+filter.getStatus()+"%' ";	
			}
			if(filter.getOrderType() != null){
				sqlQuery += " AND prodType like '% "+filter.getOrderType()+"%' ";	
			}
			if(filter.getReqLPnts()!= null){
				sqlQuery += " AND reqRwdPoint = "+filter.getReqLPnts()+" ";
			}
			if(filter.getText1() != null){
				if(filter.getText1().equalsIgnoreCase("YES")){
					sqlQuery += " AND isRwdPointProd =true ";	
				}else if(filter.getText1().equalsIgnoreCase("NO")){
					sqlQuery += " AND isRwdPointProd =false ";	
				}
			}
			if(filter.getLastUpdatedBy() != null){
				sqlQuery += " AND updBy like '% "+filter.getLastUpdatedBy()+"%' ";	
			}
			if(filter.getDelvDateStr() != null){
				String dateTimeStr = filter.getDelvDateStr();
				if(dateTimeStr.contains(" ")){
					String dateTimeArr[] = dateTimeStr.split(" ");
					if(Util.extractDateElement(dateTimeArr[0],"DAY") > 0){
						sqlQuery += " AND DATEPART(day,pExpDate) = "+Util.extractDateElement(dateTimeArr[0],"DAY")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,pExpDate) = "+Util.extractDateElement(dateTimeArr[0],"MONTH")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,pExpDate) = "+Util.extractDateElement(dateTimeArr[0],"YEAR")+" ";
					}
					if(dateTimeArr.length>1){
						String dateTime = dateTimeArr[1];
						if(dateTimeArr.length > 2){
							if(dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()){
								dateTime += " "+dateTimeArr[2];
							}
						}
						if(Util.extractDateElement(dateTime,"HOUR") > 0){
							sqlQuery += " AND DATEPART(hour,pExpDate) = "+Util.extractDateElement(dateTime,"HOUR")+" ";
						}
						if(Util.extractDateElement(dateTime,"MINUTE") > 0){
							sqlQuery += " AND DATEPART(minute,pExpDate) = "+Util.extractDateElement(dateTime,"MINUTE")+" ";
						}
					}
				}else{
					if(Util.extractDateElement(filter.getDelvDateStr(),"DAY") > 0){
						sqlQuery += " AND DATEPART(day,pExpDate) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,pExpDate) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,pExpDate) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
					}
				}
			}
			if(filter.getLastUpdatedDateStr() != null){
				String dateTimeStr = filter.getLastUpdatedDateStr();
				if(dateTimeStr.contains(" ")){
					String dateTimeArr[] = dateTimeStr.split(" ");
					if(Util.extractDateElement(dateTimeArr[0],"DAY") > 0){
						sqlQuery += " AND DATEPART(day,updDate) = "+Util.extractDateElement(dateTimeArr[0],"DAY")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,updDate) = "+Util.extractDateElement(dateTimeArr[0],"MONTH")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,updDate) = "+Util.extractDateElement(dateTimeArr[0],"YEAR")+" ";
					}
					if(dateTimeArr.length>1){
						String dateTime = dateTimeArr[1];
						if(dateTimeArr.length > 2){
							if(dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()){
								dateTime += " "+dateTimeArr[2];
							}
						}
						if(Util.extractDateElement(dateTime,"HOUR") > 0){
							sqlQuery += " AND DATEPART(hour,updDate) = "+Util.extractDateElement(dateTime,"HOUR")+" ";
						}
						if(Util.extractDateElement(dateTime,"MINUTE") > 0){
							sqlQuery += " AND DATEPART(minute,updDate) = "+Util.extractDateElement(dateTime,"MINUTE")+" ";
						}
					}
				}else{
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
						sqlQuery += " AND DATEPART(day,updDate) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,updDate) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,updDate) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
					}
				}
			}
			
			sqlQuery += " Order by dispOrder";
			
			return find(sqlQuery);
		}catch(Exception e){
			e.printStackTrace();
		}
		return rewardList;
	}
	
	@Override
	public List<SellerProductsItems> getProductsById(String prodIds) {	
		return find(" FROM SellerProductsItems WHERE spiId in (" + prodIds +")");
	
	}	

}
