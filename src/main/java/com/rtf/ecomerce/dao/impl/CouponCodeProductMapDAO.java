package com.rtf.ecomerce.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtf.ecomerce.data.CouponCodeProductMap;
import com.rtf.ecomerce.data.SellerProdItemsVariInventory;
import com.rtf.ecomerce.utils.GridHeaderFilters;
import com.rtw.tracker.dao.implementation.HibernateDAO;


public class CouponCodeProductMapDAO extends HibernateDAO<Integer, CouponCodeProductMap> implements  com.rtf.ecomerce.dao.service.CouponCodeProductMapDAO {
		
	@SuppressWarnings("unchecked")
	public List<CouponCodeProductMap> getAllCouponCodesProductMap(GridHeaderFilters filter,Integer ccId){
		
		List<CouponCodeProductMap> lst = new ArrayList<CouponCodeProductMap>();		
		Session session = null;
		try{			
			String sqlQuery = "	 select * from ( " 
				+ "	  SELECT a.pname as pname,a.slrpitms_id as slrpitmsId,a.psel_min_prc as pselMinPrc "
				+ "	  ,a.preg_min_prc as pregMinPrc , a.pimg  as pimg, b.ccID as ccId "
				+ "	  ,b.cou_code as couCode ,b.cpmID as cpmId from rtf_seller_product_items_mstr a with(nolock) "
				+ "	  right  join rtf_coupon_product_map b on  a.slrpitms_id=b.slrpitms_id where b.ccId=" + ccId + " and a.pstatus='ACTIVE'  union "
				+ "	  select a.pname as pname ,a.slrpitms_id as slrpitmsId,a.psel_min_prc as pselMinPrc "
				+ "   ,a.preg_min_prc as pregMinPrc ,a.pimg as pimg ,null as  ccId,null as couCode ,"
				+ "   null as cpmId from rtf_seller_product_items_mstr a "
				+ "	  where  a.pstatus='ACTIVE' AND  a.slrpitms_id not in  "
				+ "	  (select c.slrpitms_id from rtf_coupon_product_map c where c.ccId=" + ccId + "  )  ) as u where 1=1 " ;
				
			if(filter.getCoucde() != null){
				sqlQuery += " AND u.couCode like '%"+filter.getCoucde()+"%' ";
			}
			
			if(filter.getPname() != null){
				sqlQuery += " AND u.pname like '%"+filter.getPname()+"%' ";	
			}
			if(filter.getSellPrc() != null){
				sqlQuery += " AND u.pselMinPrc like % "+filter.getSellPrc()+"% ";
			}
			if(filter.getCoustatus() != null){
				sqlQuery += " AND u.pregMinPrc like '% "+filter.getRegPrc()+"%' ";
			}
			//System.out.println("GET ALL COUPON CODE Map " + sqlQuery);			
			session = getSession();
			SQLQuery query = (org.hibernate.SQLQuery) session.createSQLQuery(sqlQuery);
			
			query.addScalar("pname", Hibernate.STRING);
			query.addScalar("slrpitmsId", Hibernate.INTEGER);
			query.addScalar("pselMinPrc", Hibernate.DOUBLE);
			query.addScalar("pregMinPrc", Hibernate.DOUBLE);
			query.addScalar("pimg", Hibernate.STRING);
			query.addScalar("ccId", Hibernate.INTEGER);
			query.addScalar("couCode", Hibernate.STRING);
			query.addScalar("cpmId", Hibernate.INTEGER);
			lst = query.setResultTransformer(Transformers.aliasToBean(CouponCodeProductMap.class)).list();
			return lst;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return lst;
	}
	
	public Integer getAllCouponCodesProductMapCount(GridHeaderFilters filter,Integer ccId){
		Integer count = 0;
		List<CouponCodeProductMap> lst = new ArrayList<CouponCodeProductMap>();		
		Session session = null;
		try{
			
			String sqlQuery = "	 select * from ( " 
				+ "	  SELECT a.pname as pname,a.slrpitms_id as slrpitmsId,a.psel_min_prc as pselMinPrc "
				+ "	  ,a.preg_min_prc as pregMinPrc , a.pimg  as pimg, b.ccID as ccId "
				+ "	  ,b.cou_code as couCode ,b.cpmID as cpmId from rtf_seller_product_items_mstr a with(nolock) "
				+ "	  right  join rtf_coupon_product_map b on  a.slrpitms_id=b.slrpitms_id where b.ccId=" + ccId + " and a.pstatus='ACTIVE'  union "
				+ "	  select a.pname as pname ,a.slrpitms_id as slrpitmsId,a.psel_min_prc as pselMinPrc "
				+ "   ,a.preg_min_prc as pregMinPrc ,a.pimg as pimg ,null as  ccId,null as couCode ,"
				+ "   null as cpmId from rtf_seller_product_items_mstr a "
				+ "	  where  a.pstatus='ACTIVE' AND  a.slrpitms_id not in  "
				+ "	  (select c.slrpitms_id from rtf_coupon_product_map c where c.ccId=" + ccId + "  )  ) as u where 1=1 " ;
				
			if(filter.getCoucde() != null){
				sqlQuery += " AND u.couCode like '%"+filter.getCoucde()+"%' ";
			}
			
			if(filter.getPname() != null){
				sqlQuery += " AND u.pname like '%"+filter.getPname()+"%' ";	
			}
			if(filter.getSellPrc() != null){
				sqlQuery += " AND u.pselMinPrc like % "+filter.getSellPrc()+"% ";
			}
			if(filter.getCoustatus() != null){
				sqlQuery += " AND u.pregMinPrc like '% "+filter.getRegPrc()+"%' ";
			}
			//System.out.println("GET ALL COUPON CODE Map " + sqlQuery);
			
			session = getSession();
			SQLQuery query = session.createSQLQuery(sqlQuery);
			lst=query.list();
			if(lst != null)	count = lst.size();				
		}catch(Exception e){
			e.printStackTrace();
		}
		finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return count;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<CouponCodeProductMap> getAllUnMappedProductsForCouponCodes(GridHeaderFilters filter,Integer ccId){
		
		List<CouponCodeProductMap> lst = new ArrayList<CouponCodeProductMap>();		
		Session session = null;
		try{			
			String sqlQuery = "" 
				+ "	  SELECT a.pname as pname,a.slrpitms_id as slrpitmsId,a.psel_min_prc as pselMinPrc "
				+ "	  ,a.preg_min_prc as pregMinPrc , a.pimg  as pimg  from rtf_seller_product_items_mstr a with(nolock) "
				+ "	  where   a.slrpitms_id   not in ( select b.slrpitms_id  from  rtf_coupon_product_map b "
				+ "   where b.ccId=" + ccId + " )  AND  a.pstatus='ACTIVE'  " ;
			
			if(filter.getPname() != null){
				sqlQuery += " AND u.pname like '%"+filter.getPname()+"%' ";	
			}
			if(filter.getSellPrc() != null){
				sqlQuery += " AND a.pselMinPrc like % "+filter.getSellPrc()+"% ";
			}
			if(filter.getCoustatus() != null){
				sqlQuery += " AND a.pregMinPrc like '% "+filter.getRegPrc()+"%' ";
			}
			//System.out.println("GET ALL COUPON CODE Map " + sqlQuery);			
			session = getSession();
			SQLQuery query = (org.hibernate.SQLQuery) session.createSQLQuery(sqlQuery);
			
			query.addScalar("pname", Hibernate.STRING);
			query.addScalar("slrpitmsId", Hibernate.INTEGER);
			query.addScalar("pselMinPrc", Hibernate.DOUBLE);
			query.addScalar("pregMinPrc", Hibernate.DOUBLE);
			query.addScalar("pimg", Hibernate.STRING);
			
			/*
			 * query.addScalar("ccId", Hibernate.INTEGER);
			 * query.addScalar("couCode", Hibernate.STRING);
			 * query.addScalar("cpmId", Hibernate.INTEGER);
			 */
			lst = query.setResultTransformer(Transformers.aliasToBean(CouponCodeProductMap.class)).list();
			return lst;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return lst;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<CouponCodeProductMap> getAllMappedProductsForCouponCodes(GridHeaderFilters filter,Integer ccId){
		
		List<CouponCodeProductMap> lst = new ArrayList<CouponCodeProductMap>();		
		Session session = null;
		try{			
			String sqlQuery = "" 
				+ "	  SELECT a.pname as pname,a.slrpitms_id as slrpitmsId,a.psel_min_prc as pselMinPrc "
				+ "	  ,a.preg_min_prc as pregMinPrc , a.pimg  as pimg  from rtf_seller_product_items_mstr a with(nolock) "
				+ "	  where   a.slrpitms_id  in ( select b.slrpitms_id  from  rtf_coupon_product_map b "
				+ "   where b.ccId=" + ccId + " )  AND  a.pstatus='ACTIVE'  " ;
			
			if(filter.getPname() != null){
				sqlQuery += " AND u.pname like '%"+filter.getPname()+"%' ";	
			}
			if(filter.getSellPrc() != null){
				sqlQuery += " AND a.pselMinPrc like % "+filter.getSellPrc()+"% ";
			}
			if(filter.getCoustatus() != null){
				sqlQuery += " AND a.pregMinPrc like '% "+filter.getRegPrc()+"%' ";
			}
			//System.out.println("GET ALL COUPON CODE Map " + sqlQuery);			
			session = getSession();
			SQLQuery query = (org.hibernate.SQLQuery) session.createSQLQuery(sqlQuery);
			
			query.addScalar("pname", Hibernate.STRING);
			query.addScalar("slrpitmsId", Hibernate.INTEGER);
			query.addScalar("pselMinPrc", Hibernate.DOUBLE);
			query.addScalar("pregMinPrc", Hibernate.DOUBLE);
			query.addScalar("pimg", Hibernate.STRING);
			
			/*
			 * query.addScalar("ccId", Hibernate.INTEGER);
			 * query.addScalar("couCode", Hibernate.STRING);
			 * query.addScalar("cpmId", Hibernate.INTEGER);
			 */
			lst = query.setResultTransformer(Transformers.aliasToBean(CouponCodeProductMap.class)).list();
			return lst;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return lst;
	}
	
		
	@Override
	public void deleteCoupCodeMappinByCoupId(Integer ccId) {		
		bulkUpdate("DELETE FROM CouponCodeProductMap WHERE ccId=?", new Object[]{ccId});
	}
	

	@Override
	public Integer deleteCoupCodeMappinByCoupIdAndProId(Integer ccId,Integer prodId) {	
		Session session = null;
		int result =0;
		try {
		session = getSession();
		SQLQuery query = session.createSQLQuery("delete FROM rtf_coupon_product_map where ccId=" +ccId + " and slrpitms_id=" +prodId);	
		result = query.executeUpdate();		 
		if (result > 0) {
		    System.out.println("product mapping is removed");
		}
	
	}catch(Exception e){
		e.printStackTrace();
	}
	finally {
		if (session != null && session.isOpen()) {
			session.close();
		}
	}		
			return result;
	}
	
}
