package com.rtf.ecomerce.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.rtf.ecomerce.data.SellerProdItemsVariInventory;
import com.rtf.ecomerce.data.SellerProductsItems;
import com.rtf.ecomerce.utils.GridHeaderFilters;
import com.rtw.tmat.utils.Util;
import com.rtw.tracker.dao.implementation.HibernateDAO;

public class SellerProdItemsVariInventoryDAO extends HibernateDAO<Integer, SellerProdItemsVariInventory> implements com.rtf.ecomerce.dao.service.SellerProdItemsVariInventoryDAO{

	@Override
	public SellerProdItemsVariInventory getInventoryByInventoryId(Integer inventoryId) {
		return findSingle(" from SellerProdItemsVariInventory WHERE spivInvId=?",new Object[]{inventoryId});
	}
	@Override
	public SellerProdItemsVariInventory getInventoryByOptionValue(Integer opValId) {
		return findSingle(" FROM SellerProdItemsVariInventory WHERE vvId1=?",new Object[]{opValId});
	}

	@Override
	public SellerProdItemsVariInventory getInventoryByOptionValue(Integer opValId1, Integer opValId2) {
		return findSingle(" FROM SellerProdItemsVariInventory WHERE vvId1=? AND vvId2=?",new Object[]{opValId1,opValId2});
	}

	@Override
	public SellerProdItemsVariInventory getInventoryByOptionValue(Integer opValId1, Integer opValId2,Integer opValId3) {
		return findSingle(" FROM SellerProdItemsVariInventory WHERE vvId1=? AND vvId2=? AND AND vvId3=?",new Object[]{opValId1,opValId2,opValId3});
	}
	
	@Override
	public List<SellerProdItemsVariInventory> getInventoryByProductId(Integer productItmId) {
		return find(" FROM SellerProdItemsVariInventory WHERE spiId=? ",new Object[]{productItmId});
	}
	
	public void deleteInventoryByProductId(Integer productItmId) {
		bulkUpdate(" DELETE FROM SellerProdItemsVariInventory WHERE spiId=? ",new Object[]{productItmId});
	}
	
	public List<SellerProdItemsVariInventory> getAllInventoryByProductId(GridHeaderFilters filter,Integer productItmId){
		List<SellerProdItemsVariInventory> rewardList = new ArrayList<SellerProdItemsVariInventory>();
		try{
			
			String sqlQuery = "From SellerProdItemsVariInventory where spiId="+productItmId+" ";
			
			if(filter.getSpiId() != null){
				sqlQuery += " AND spiId = "+filter.getSpiId()+" ";
			}
			if(filter.getSellerId() != null){
				sqlQuery += " AND sellerId ="+filter.getSellerId()+" ";
			}
			if(filter.getvOptNameValCom() != null){
				sqlQuery += " AND vOptNameValCom like '%"+filter.getvOptNameValCom()+"%' ";	
			}
			if(filter.getSoldQty() != null){
				sqlQuery += " AND soldQty ="+filter.getSoldQty()+" ";
			}
			if(filter.getAvlQty() != null){
				sqlQuery += " AND avlQty =  "+filter.getAvlQty()+"";
			}
			if(filter.getRegPrc() != null){
				sqlQuery += " AND regPrice = "+filter.getRegPrc()+"";	
			}
			if(filter.getSellPrc() != null){
				sqlQuery += " AND sellPrice = "+filter.getSellPrc()+" ";
			}
			if(filter.getReqLPnts() != null){
				sqlQuery += " AND reqLPnts = "+filter.getReqLPnts()+" ";
			}
			if(filter.getPriceAftLPnts() != null){
				sqlQuery += " AND priceAftLPnts = "+filter.getPriceAftLPnts()+" ";
			}
			if(filter.getStatus() != null){
				sqlQuery += " AND status like '% "+filter.getStatus()+"%' ";	
			}
			if(filter.getLastUpdatedBy() != null){
				sqlQuery += " AND updBy like '% "+filter.getLastUpdatedBy()+"%' ";	
			}
			if(filter.getDelvDateStr() != null){
				String dateTimeStr = filter.getDelvDateStr();
				if(dateTimeStr.contains(" ")){
					String dateTimeArr[] = dateTimeStr.split(" ");
					if(Util.extractDateElement(dateTimeArr[0],"DAY") > 0){
						sqlQuery += " AND DATEPART(day,pExpDate) = "+Util.extractDateElement(dateTimeArr[0],"DAY")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,pExpDate) = "+Util.extractDateElement(dateTimeArr[0],"MONTH")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,pExpDate) = "+Util.extractDateElement(dateTimeArr[0],"YEAR")+" ";
					}
					if(dateTimeArr.length>1){
						String dateTime = dateTimeArr[1];
						if(dateTimeArr.length > 2){
							if(dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()){
								dateTime += " "+dateTimeArr[2];
							}
						}
						if(Util.extractDateElement(dateTime,"HOUR") > 0){
							sqlQuery += " AND DATEPART(hour,pExpDate) = "+Util.extractDateElement(dateTime,"HOUR")+" ";
						}
						if(Util.extractDateElement(dateTime,"MINUTE") > 0){
							sqlQuery += " AND DATEPART(minute,pExpDate) = "+Util.extractDateElement(dateTime,"MINUTE")+" ";
						}
					}
				}else{
					if(Util.extractDateElement(filter.getDelvDateStr(),"DAY") > 0){
						sqlQuery += " AND DATEPART(day,pExpDate) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,pExpDate) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,pExpDate) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
					}
				}
			}
			if(filter.getLastUpdatedDateStr() != null){
				String dateTimeStr = filter.getLastUpdatedDateStr();
				if(dateTimeStr.contains(" ")){
					String dateTimeArr[] = dateTimeStr.split(" ");
					if(Util.extractDateElement(dateTimeArr[0],"DAY") > 0){
						sqlQuery += " AND DATEPART(day,updDate) = "+Util.extractDateElement(dateTimeArr[0],"DAY")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,updDate) = "+Util.extractDateElement(dateTimeArr[0],"MONTH")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,updDate) = "+Util.extractDateElement(dateTimeArr[0],"YEAR")+" ";
					}
					if(dateTimeArr.length>1){
						String dateTime = dateTimeArr[1];
						if(dateTimeArr.length > 2){
							if(dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()){
								dateTime += " "+dateTimeArr[2];
							}
						}
						if(Util.extractDateElement(dateTime,"HOUR") > 0){
							sqlQuery += " AND DATEPART(hour,updDate) = "+Util.extractDateElement(dateTime,"HOUR")+" ";
						}
						if(Util.extractDateElement(dateTime,"MINUTE") > 0){
							sqlQuery += " AND DATEPART(minute,updDate) = "+Util.extractDateElement(dateTime,"MINUTE")+" ";
						}
					}
				}else{
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
						sqlQuery += " AND DATEPART(day,updDate) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,updDate) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,updDate) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
					}
				}
			}
			
			return find(sqlQuery);
		}catch(Exception e){
			e.printStackTrace();
		}
		return rewardList;
	}

	@Override
	public SellerProdItemsVariInventory getInventoryByOptionValue(Integer opValId1, Integer opValId2, Integer opValId3,Integer opValId4) {
		Criteria cri = getSession().createCriteria(SellerProdItemsVariInventory.class);
		cri.add(Restrictions.eq("vvId1", opValId1));
		cri.add(Restrictions.eq("status", "ACTIVE"));
		if(opValId2!=null && opValId2 > 0){
			cri.add(Restrictions.eq("vvId2", opValId2));
		}
		if(opValId3!=null && opValId3 > 0){
			cri.add(Restrictions.eq("vvId3", opValId3));
		}
		if(opValId4!=null && opValId4 > 0){
			cri.add(Restrictions.eq("vvId4", opValId4));
		}
		return (SellerProdItemsVariInventory) cri.uniqueResult();
		//return findSingle("SellerProdItemsVariInventory WHERE vvId1=? AND vvId2=? AND AND vvId3=? AND vvId4=?",new Object[]{opValId1,opValId2,opValId3,opValId4});
	}
	
	public Boolean isProductItemHasInventory(Integer productItemId) {
		SellerProdItemsVariInventory obj =  findSingle(" FROM SellerProdItemsVariInventory WHERE spiId=?",new Object[]{productItemId});
		if(obj != null) {
			return true;
		}
		return false;
	}
	
	
	@Override
	public List<SellerProdItemsVariInventory> getInventoryByIds(List<Integer> ids) {
		Criteria cri = getSession().createCriteria(SellerProdItemsVariInventory.class);
		if(ids.isEmpty()){
			cri.add(Restrictions.eq("vvId1", -1111));
		}else{
			cri.add(Restrictions.in("vvId1", ids));
		}
		cri.add(Restrictions.eq("status", "ACTIVE"));
		return cri.list();
	}

public Integer generateSellerPRoductVariantMAtrix(Integer sellerId,Integer prodItmId,String userName) throws Exception{
		
		SQLQuery query = null;
		Session session = null;
		Integer responseCode = null;
		try {
			String queryStr = "exec spGenSlerProdVariMatrix @sler_id='"+sellerId+"',@slrpitms_id='"+prodItmId+"',@lupd_by='"+userName+"'";
			System.out.println("  query : "+queryStr);
			
			session = getSession();
			query = session.createSQLQuery(queryStr);
			List<Object[]> result =  query.list();
			if(result != null && result.size() > 0) {
				responseCode = Integer.valueOf(result.get(0)[0].toString());
			}
			return responseCode;
		}catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(session != null && session.isOpen()) {
				session.close();
			}
		}
		return responseCode;
		
	}
}
