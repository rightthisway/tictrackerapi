package com.rtf.ecomerce.dao.impl;

import java.util.List;

import com.rtf.ecomerce.data.SellerProdItemsVariValue;
import com.rtw.tracker.dao.implementation.HibernateDAO;

public class SellerProdItemsVariValueDAO extends HibernateDAO<Integer, SellerProdItemsVariValue> implements com.rtf.ecomerce.dao.service.SellerProdItemsVariValueDAO{

	public List<SellerProdItemsVariValue> getOptionValues(Integer optionId){
		return find("FROM SellerProdItemsVariValue WHERE slrpiVarId=?",new Object[]{optionId});
	}
	
}
