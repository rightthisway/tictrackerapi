package com.rtf.ecomerce.dao.impl;

import java.util.List;

import com.rtf.ecomerce.data.SellerProdItemsVariName;
import com.rtw.tracker.dao.implementation.HibernateDAO;

public class SellerProdItemsVariNameDAO extends HibernateDAO<Integer, SellerProdItemsVariName> implements com.rtf.ecomerce.dao.service.SellerProdItemsVariNameDAO{

	@Override
	public List<SellerProdItemsVariName> getAllProductVariants(Integer prodId) {
		return find("FROM SellerProdItemsVariName WHERE spiId=?",new Object[]{prodId});
	}

}
