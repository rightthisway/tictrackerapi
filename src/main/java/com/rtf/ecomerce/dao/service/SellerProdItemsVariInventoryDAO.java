package com.rtf.ecomerce.dao.service;

import java.util.List;

import com.rtf.ecomerce.data.SellerProdItemsVariInventory;
import com.rtf.ecomerce.data.SellerProductsItems;
import com.rtf.ecomerce.utils.GridHeaderFilters;
import com.rtw.tracker.dao.services.RootDAO;

public interface SellerProdItemsVariInventoryDAO extends RootDAO<Integer, SellerProdItemsVariInventory>{

	
	public SellerProdItemsVariInventory getInventoryByInventoryId(Integer inventoryId);
	public SellerProdItemsVariInventory getInventoryByOptionValue(Integer opValId);
	public SellerProdItemsVariInventory getInventoryByOptionValue(Integer opValId1,Integer opValId2);
	public SellerProdItemsVariInventory getInventoryByOptionValue(Integer opValId1,Integer opValId2,Integer opValId3);
	public SellerProdItemsVariInventory getInventoryByOptionValue(Integer opValId1,Integer opValId2,Integer opValId3,Integer opValId4);
	public List<SellerProdItemsVariInventory> getInventoryByIds(List<Integer> ids);
	public Boolean isProductItemHasInventory(Integer productItemId);
	public List<SellerProdItemsVariInventory> getInventoryByProductId(Integer productItmId);
	public Integer generateSellerPRoductVariantMAtrix(Integer sellerId,Integer prodItmId,String userName) throws Exception;
	public List<SellerProdItemsVariInventory> getAllInventoryByProductId(GridHeaderFilters filter,Integer productItmId);
	public void deleteInventoryByProductId(Integer productItmId);
}
