package com.rtf.ecomerce.dao.service;

import com.rtf.ecomerce.data.RtfOrderRefund;
import com.rtw.tracker.dao.services.RootDAO;

public interface RtfOrderRefundDAO extends RootDAO<Integer, RtfOrderRefund>{

}
