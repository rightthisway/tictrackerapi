package com.rtf.ecomerce.dao.service;

import java.util.List;

import com.rtf.ecomerce.data.CustomerOrder;
import com.rtf.ecomerce.data.OrderProducts;
import com.rtf.ecomerce.utils.GridHeaderFilters;
import com.rtw.tracker.dao.services.RootDAO;

public interface OrderProductSetDAO extends RootDAO<Integer, OrderProducts>{

	public List<OrderProducts> getOrderProductByOrderId(Integer orderId); 
	public List<OrderProducts> getAllRtfOrderProductDetails(GridHeaderFilters filter,String status,Integer userId,Integer orderId);
}
