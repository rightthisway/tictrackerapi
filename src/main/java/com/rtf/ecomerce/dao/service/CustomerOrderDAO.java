package com.rtf.ecomerce.dao.service;

import java.util.List;

import com.rtf.ecomerce.data.CustomerOrder;
import com.rtf.ecomerce.utils.GridHeaderFilters;
import com.rtw.tracker.dao.services.RootDAO;

public interface CustomerOrderDAO extends RootDAO<Integer, CustomerOrder>{

	public List<CustomerOrder> getAllCustomerOrdersByCustId(Integer custId) throws Exception;
	public CustomerOrder getACustomerOrderByOrderId(Integer orderId) throws Exception;
	public CustomerOrder getCustomerOrderByOrderIdAndCustomerId(Integer orderId,Integer custId) throws Exception;
	public List<CustomerOrder> getAllRTFProdOrderDetails(GridHeaderFilters filter,String status,Integer userId);
}
