package com.rtf.ecomerce.dao.service;

import java.util.List;

import com.rtf.ecomerce.data.CouponCodeProductMap;
import com.rtf.ecomerce.utils.GridHeaderFilters;
import com.rtw.tracker.dao.services.RootDAO;

public interface CouponCodeProductMapDAO extends RootDAO<Integer, CouponCodeProductMap>{	
	
	public List<CouponCodeProductMap> getAllCouponCodesProductMap(GridHeaderFilters filter,Integer ccId);
	public Integer getAllCouponCodesProductMapCount(GridHeaderFilters filter,Integer ccId);
	public List<CouponCodeProductMap> getAllUnMappedProductsForCouponCodes(GridHeaderFilters filter,Integer ccId);
	public List<CouponCodeProductMap> getAllMappedProductsForCouponCodes(GridHeaderFilters filter,Integer ccId);
	public void deleteCoupCodeMappinByCoupId(Integer ccId);
	public Integer deleteCoupCodeMappinByCoupIdAndProId(Integer ccId, Integer prodId);
}
