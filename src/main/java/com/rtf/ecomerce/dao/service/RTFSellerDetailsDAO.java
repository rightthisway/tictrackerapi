package com.rtf.ecomerce.dao.service;

import java.util.List;

import com.rtf.ecomerce.data.RTFSellerDetails;
import com.rtf.ecomerce.utils.GridHeaderFilters;
import com.rtw.tracker.dao.services.RootDAO;

public interface RTFSellerDetailsDAO extends RootDAO<Integer, RTFSellerDetails>{
	
	public RTFSellerDetails getRTFSellerDetailsBySellerId(Integer sellerId);
	public List<RTFSellerDetails> getAllRTFSellerDetails(GridHeaderFilters filter,String status);
	public Integer getAllRTFSellerDetailsCount(GridHeaderFilters filter,String statusSS);
	public RTFSellerDetails findSellerByName(String compName);
	public RTFSellerDetails findSellerByLoginId(Integer loginId);
}
