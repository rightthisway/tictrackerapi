package com.rtf.ecomerce.dao.service;

import java.util.List;

import com.rtf.ecomerce.data.SellerProductsItems;
import com.rtf.ecomerce.utils.GridHeaderFilters;
import com.rtw.tracker.dao.services.RootDAO;

public interface SellerProductsItemsDAO extends RootDAO<Integer, SellerProductsItems>{
	
	public List<SellerProductsItems> getProductsByStatus(String status);
	public SellerProductsItems getProductsById(Integer prodId);
	public List<SellerProductsItems> getAllRTFSellerDetails(GridHeaderFilters filter,String status);
	public List<SellerProductsItems> getProductsById(String prodIds);
}
