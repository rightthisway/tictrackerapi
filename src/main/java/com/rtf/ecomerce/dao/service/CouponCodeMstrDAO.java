package com.rtf.ecomerce.dao.service;

import java.util.List;

import com.rtf.ecomerce.data.CouponCodeMst;
import com.rtf.ecomerce.utils.GridHeaderFilters;
import com.rtw.tracker.dao.services.RootDAO;

public interface CouponCodeMstrDAO extends RootDAO<Integer, CouponCodeMst>{
	
	public CouponCodeMst getCouponCodeMstr(Integer ccId);
	public List<CouponCodeMst> getAllCouponCodes(GridHeaderFilters filter,String couponStatus);
	public Integer getAllCouponCodesCount(GridHeaderFilters filter);
	public Boolean getCouponCodeMstrByCouponName(String couponName);
	
}
