package com.rtf.ecomerce.dao.service;

import java.util.List;

import com.rtf.ecomerce.data.SellerProdItemsVariValue;
import com.rtw.tracker.dao.services.RootDAO;

public interface SellerProdItemsVariValueDAO extends RootDAO<Integer, SellerProdItemsVariValue>{
	
	public List<SellerProdItemsVariValue> getOptionValues(Integer optionId);

}
