package com.rtf.ecomerce.dao.service;

import java.util.List;

import com.rtf.ecomerce.data.SellerProdItemsVariName;
import com.rtw.tracker.dao.services.RootDAO;

public interface SellerProdItemsVariNameDAO extends RootDAO<Integer, SellerProdItemsVariName>{

	
	public List<SellerProdItemsVariName> getAllProductVariants(Integer prodId);
}
