package com.rtf.ecomerce.utils;

import java.sql.Time;
import java.util.Date;
import java.util.List;

import com.rtf.ecomerce.data.OrderProducts;

public class GridHeaderFilters {

	private String sortingColumn;
	private String sortingOrder;
	
	private String fName;
	private String lName;
	private String Email;
	private String phone;
	private String companyName;
	private String addressLine1;
	private String addressLine2; //Invoice Creation Customer Grid
	private String city;
	private String state;
	private String country;
	private String zipCode;

	private Date createdDate; //createdDate
	private String status;
	private Date lastUpdatedDate;
	private String createdBy;
	private String createdDateStr;
	private String lastUpdatedDateStr;
	
	private String lastUpdatedBy;
	private String actionType;
	
	private String text1;
	private String text2;
	private String text3;
	private String text4;
	private String text5;
	private String text6;
	private String text7;
	private String text8;
	private String text9;
	private String text10;
	private String text11;
	private Integer count;
	
	
	private String coucde;
	private String couname;	
	private String coudesc;
	private Double coudiscount;
	private String coustatus;	
	private String discounttype;
	private String coutype;
	private Date exprydate;
	private String exprydateStr;
	
	
	private String pname;
	private Double regPrc;
	private Double sellPrc;
	
	private Integer orderId;
	private Double netTot;	
	private Double disc;
	private Double grosTot;
	private String pPayType;//Primary Payment Type	
	private String sPayType;//Secondary Payment Type
	private Double pPayAmt;//Primary Payment Amount
	private Double sPayAmt;//Secondary Payment Amount
	private String pTrnxId;//Primary Transaction Id
	private String sTrnxId;//Secondary Transaction Id
	private String orderType;
	private String shAddr1;
	private String shAddr2;
	private String shCity;
	private String shState;
	private String shCountry;
	private String shZip;
	private String shFName;
	private String shLName;
	private String shPhone;
	private String shEmail;
	private Double shCharges;
	private Double tax;
	
	
	private String crDateStr;
	private String delvStatus;
	private String delvDateStr;
	private String pImgUrl;
	private Double orderSubTotal;
	private Integer noOfItems;
	
	private String custUserId;
	private String custFirstName;
	private String custLastName;
	private String custEmail;
	private String custPhone;
	
	
	private Integer spiId;//seller product item id
	private String pBrand;//Product Brand
	private String pDesc;//Product Description
	private Double psMinPrice;//Product sell minimum proce
	private Double pRegMinPrice;//Product 
	private Integer maxCustQty;
	
	private Integer sellerId;
	private String vOptNameValCom;//
	private Integer reqLPnts;
	private Double priceAftLPnts;
	private Integer avlQty;
	private Integer soldQty;
	
	public String getSortingColumn() {
		return sortingColumn;
	}
	public void setSortingColumn(String sortingColumn) {
		this.sortingColumn = sortingColumn;
	}
	public String getSortingOrder() {
		return sortingOrder;
	}
	public void setSortingOrder(String sortingOrder) {
		this.sortingOrder = sortingOrder;
	}
	public String getfName() {
		return fName;
	}
	public void setfName(String fName) {
		this.fName = fName;
	}
	public String getlName() {
		return lName;
	}
	public void setlName(String lName) {
		this.lName = lName;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedDateStr() {
		return createdDateStr;
	}
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	public String getLastUpdatedDateStr() {
		return lastUpdatedDateStr;
	}
	public void setLastUpdatedDateStr(String lastUpdatedDateStr) {
		this.lastUpdatedDateStr = lastUpdatedDateStr;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public String getActionType() {
		return actionType;
	}
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}
	public String getText1() {
		return text1;
	}
	public void setText1(String text1) {
		this.text1 = text1;
	}
	public String getText2() {
		return text2;
	}
	public void setText2(String text2) {
		this.text2 = text2;
	}
	public String getText3() {
		return text3;
	}
	public void setText3(String text3) {
		this.text3 = text3;
	}
	public String getText4() {
		return text4;
	}
	public void setText4(String text4) {
		this.text4 = text4;
	}
	public String getText5() {
		return text5;
	}
	public void setText5(String text5) {
		this.text5 = text5;
	}
	public String getText6() {
		return text6;
	}
	public void setText6(String text6) {
		this.text6 = text6;
	}
	public String getText7() {
		return text7;
	}
	public void setText7(String text7) {
		this.text7 = text7;
	}
	public String getText8() {
		return text8;
	}
	public void setText8(String text8) {
		this.text8 = text8;
	}
	public String getText9() {
		return text9;
	}
	public void setText9(String text9) {
		this.text9 = text9;
	}
	public String getText10() {
		return text10;
	}
	public void setText10(String text10) {
		this.text10 = text10;
	}
	public String getText11() {
		return text11;
	}
	public void setText11(String text11) {
		this.text11 = text11;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public String getCoucde() {
		return coucde;
	}
	public void setCoucde(String coucde) {
		this.coucde = coucde;
	}
	public String getCouname() {
		return couname;
	}
	public void setCouname(String couname) {
		this.couname = couname;
	}
	public String getCoudesc() {
		return coudesc;
	}
	public void setCoudesc(String coudesc) {
		this.coudesc = coudesc;
	}
	public Double getCoudiscount() {
		return coudiscount;
	}
	public void setCoudiscount(Double coudiscount) {
		this.coudiscount = coudiscount;
	}
	public String getCoustatus() {
		return coustatus;
	}
	public void setCoustatus(String coustatus) {
		this.coustatus = coustatus;
	}
	public String getDiscounttype() {
		return discounttype;
	}
	public void setDiscounttype(String discounttype) {
		this.discounttype = discounttype;
	}
	public String getCoutype() {
		return coutype;
	}
	public void setCoutype(String coutype) {
		this.coutype = coutype;
	}
	public Date getExprydate() {
		return exprydate;
	}
	public void setExprydate(Date exprydate) {
		this.exprydate = exprydate;
	}
	public String getExprydateStr() {
		return exprydateStr;
	}
	public void setExprydateStr(String exprydateStr) {
		this.exprydateStr = exprydateStr;
	}
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	public Double getRegPrc() {
		return regPrc;
	}
	public void setRegPrc(Double regPrc) {
		this.regPrc = regPrc;
	}
	public Double getSellPrc() {
		return sellPrc;
	}
	public void setSellPrc(Double sellPrc) {
		this.sellPrc = sellPrc;
	}
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	public Double getNetTot() {
		return netTot;
	}
	public void setNetTot(Double netTot) {
		this.netTot = netTot;
	}
	public Double getDisc() {
		return disc;
	}
	public void setDisc(Double disc) {
		this.disc = disc;
	}
	public Double getGrosTot() {
		return grosTot;
	}
	public void setGrosTot(Double grosTot) {
		this.grosTot = grosTot;
	}
	public String getpPayType() {
		return pPayType;
	}
	public void setpPayType(String pPayType) {
		this.pPayType = pPayType;
	}
	public String getsPayType() {
		return sPayType;
	}
	public void setsPayType(String sPayType) {
		this.sPayType = sPayType;
	}
	public Double getpPayAmt() {
		return pPayAmt;
	}
	public void setpPayAmt(Double pPayAmt) {
		this.pPayAmt = pPayAmt;
	}
	public Double getsPayAmt() {
		return sPayAmt;
	}
	public void setsPayAmt(Double sPayAmt) {
		this.sPayAmt = sPayAmt;
	}
	public String getpTrnxId() {
		return pTrnxId;
	}
	public void setpTrnxId(String pTrnxId) {
		this.pTrnxId = pTrnxId;
	}
	public String getsTrnxId() {
		return sTrnxId;
	}
	public void setsTrnxId(String sTrnxId) {
		this.sTrnxId = sTrnxId;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getShAddr1() {
		return shAddr1;
	}
	public void setShAddr1(String shAddr1) {
		this.shAddr1 = shAddr1;
	}
	public String getShAddr2() {
		return shAddr2;
	}
	public void setShAddr2(String shAddr2) {
		this.shAddr2 = shAddr2;
	}
	public String getShCity() {
		return shCity;
	}
	public void setShCity(String shCity) {
		this.shCity = shCity;
	}
	public String getShState() {
		return shState;
	}
	public void setShState(String shState) {
		this.shState = shState;
	}
	public String getShCountry() {
		return shCountry;
	}
	public void setShCountry(String shCountry) {
		this.shCountry = shCountry;
	}
	public String getShZip() {
		return shZip;
	}
	public void setShZip(String shZip) {
		this.shZip = shZip;
	}
	public String getShFName() {
		return shFName;
	}
	public void setShFName(String shFName) {
		this.shFName = shFName;
	}
	public String getShLName() {
		return shLName;
	}
	public void setShLName(String shLName) {
		this.shLName = shLName;
	}
	public String getShPhone() {
		return shPhone;
	}
	public void setShPhone(String shPhone) {
		this.shPhone = shPhone;
	}
	public String getShEmail() {
		return shEmail;
	}
	public void setShEmail(String shEmail) {
		this.shEmail = shEmail;
	}
	public Double getShCharges() {
		return shCharges;
	}
	public void setShCharges(Double shCharges) {
		this.shCharges = shCharges;
	}
	public Double getTax() {
		return tax;
	}
	public void setTax(Double tax) {
		this.tax = tax;
	}
	public String getCrDateStr() {
		return crDateStr;
	}
	public void setCrDateStr(String crDateStr) {
		this.crDateStr = crDateStr;
	}
	public String getDelvStatus() {
		return delvStatus;
	}
	public void setDelvStatus(String delvStatus) {
		this.delvStatus = delvStatus;
	}
	public String getDelvDateStr() {
		return delvDateStr;
	}
	public void setDelvDateStr(String delvDateStr) {
		this.delvDateStr = delvDateStr;
	}
	public String getpImgUrl() {
		return pImgUrl;
	}
	public void setpImgUrl(String pImgUrl) {
		this.pImgUrl = pImgUrl;
	}
	public Double getOrderSubTotal() {
		return orderSubTotal;
	}
	public void setOrderSubTotal(Double orderSubTotal) {
		this.orderSubTotal = orderSubTotal;
	}
	public Integer getNoOfItems() {
		return noOfItems;
	}
	public void setNoOfItems(Integer noOfItems) {
		this.noOfItems = noOfItems;
	}
	public String getCustUserId() {
		return custUserId;
	}
	public void setCustUserId(String custUserId) {
		this.custUserId = custUserId;
	}
	public String getCustFirstName() {
		return custFirstName;
	}
	public void setCustFirstName(String custFirstName) {
		this.custFirstName = custFirstName;
	}
	public String getCustLastName() {
		return custLastName;
	}
	public void setCustLastName(String custLastName) {
		this.custLastName = custLastName;
	}
	public String getCustEmail() {
		return custEmail;
	}
	public void setCustEmail(String custEmail) {
		this.custEmail = custEmail;
	}
	public String getCustPhone() {
		return custPhone;
	}
	public void setCustPhone(String custPhone) {
		this.custPhone = custPhone;
	}
	public Integer getSpiId() {
		return spiId;
	}
	public void setSpiId(Integer spiId) {
		this.spiId = spiId;
	}
	public String getpBrand() {
		return pBrand;
	}
	public void setpBrand(String pBrand) {
		this.pBrand = pBrand;
	}
	public String getpDesc() {
		return pDesc;
	}
	public void setpDesc(String pDesc) {
		this.pDesc = pDesc;
	}
	public Double getPsMinPrice() {
		return psMinPrice;
	}
	public void setPsMinPrice(Double psMinPrice) {
		this.psMinPrice = psMinPrice;
	}
	public Double getpRegMinPrice() {
		return pRegMinPrice;
	}
	public void setpRegMinPrice(Double pRegMinPrice) {
		this.pRegMinPrice = pRegMinPrice;
	}
	public Integer getMaxCustQty() {
		return maxCustQty;
	}
	public void setMaxCustQty(Integer maxCustQty) {
		this.maxCustQty = maxCustQty;
	}
	public Integer getSellerId() {
		return sellerId;
	}
	public void setSellerId(Integer sellerId) {
		this.sellerId = sellerId;
	}
	public String getvOptNameValCom() {
		return vOptNameValCom;
	}
	public void setvOptNameValCom(String vOptNameValCom) {
		this.vOptNameValCom = vOptNameValCom;
	}
	public Integer getReqLPnts() {
		return reqLPnts;
	}
	public void setReqLPnts(Integer reqLPnts) {
		this.reqLPnts = reqLPnts;
	}
	public Double getPriceAftLPnts() {
		return priceAftLPnts;
	}
	public void setPriceAftLPnts(Double priceAftLPnts) {
		this.priceAftLPnts = priceAftLPnts;
	}
	public Integer getAvlQty() {
		return avlQty;
	}
	public void setAvlQty(Integer avlQty) {
		this.avlQty = avlQty;
	}
	public Integer getSoldQty() {
		return soldQty;
	}
	public void setSoldQty(Integer soldQty) {
		this.soldQty = soldQty;
	}
	

}
