package com.rtf.ecomerce.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import java.util.Set;

import javax.management.RuntimeErrorException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import com.rtf.ecomerce.dao.impl.EcommDAORegistry;
import com.rtf.ecomerce.data.RTFSellerDetails;
import com.rtf.ecomerce.data.SellerProdItemsVariInventory;
import com.rtf.ecomerce.data.SellerProdItemsVariName;
import com.rtf.ecomerce.data.SellerProdItemsVariValue;
import com.rtf.ecomerce.data.SellerProductUpload;
import com.rtf.ecomerce.data.SellerProductsItems;
import com.rtf.ecomerce.pojo.SellerProductUploadTO;
import com.rtw.tmat.utils.Error;
import com.rtw.tmat.utils.TicketUtil;
import com.rtw.tmat.utils.Util;
import com.rtw.tracker.utils.ExcelUtil;
public class ProductInventoryXLSUploadUtil {
	
	
	
	public static  SellerProductUploadTO parseUploadInventory(SellerProductUploadTO responseDTO , File uploadedFile) throws Exception {
	  
	   Error error = new Error();	
	   responseDTO = parseExcelFile(responseDTO,uploadedFile);	
	   
	   if(responseDTO.getStatus() == 0 ) {
		   return responseDTO;  
	   }		
		return responseDTO;
		
	}

	
	  private  static SellerProductUploadTO parseExcelFile(SellerProductUploadTO responseDTO, File uploadedFile) throws Exception{
		  
		  try {				
				FileInputStream fis = new FileInputStream(uploadedFile);
				XSSFWorkbook wb = new XSSFWorkbook(fis);
				XSSFSheet sheet = wb.getSheetAt(0);
		        Iterator<Row> itr = sheet.iterator();		        
                List<SellerProductUpload> pilist = new ArrayList<SellerProductUpload>();
					while (itr.hasNext()) {
						Row row = itr.next();
						if (row.getRowNum() == 0 )
							continue; // Header
						SellerProductUpload dvo = populateProductItemsVO(row, responseDTO);
						pilist.add(dvo);
					}
				responseDTO.setProdlst(pilist);				
				System.out.println(" Fetched Product Item Master List Size " + responseDTO.getProdlst());
				responseDTO.setStatus(1);
				
			} catch (Exception e) {
				Error err = new Error();
				responseDTO.setStatus(0);
				e.printStackTrace();
				responseDTO.getError().getDescription();
				//throw new Exception(e.getLocalizedMessage());
			}
			System.out.println( "[ IN CONTROLLER ] " + responseDTO.getError().getDescription());
			return responseDTO;  
		
	  }


		private static SellerProductUpload populateProductItemsVO(Row row, SellerProductUploadTO  dto) throws Exception {
			/*
			 * Seller Name - 1
			 * Product Name - 2
			 * Product Brand - 3
			 * Product Short Description - 4
			 * Product Full Description - 5
			 * Regular Price/Strikeout Price -6
			 * Selling Price -7
			 * Percentage Off On Selling Price - 8
			 * Reward Points Discount - 9
			 * Product Display Order - 10
			 * Available Quantity - 11
			 * Variant Name (A) -12
			 * Variant Value (A) -13
			 * Variant Name (B) -14
			 * Variant Value (B) -15
			 * Variant Name (C) -16
			 * Variant Value (C) -17
			 * Variant Name (D) -18
			 * Variant Value (D) -19
			 * Product Image File Name - 20
			 * Variant Image File Name - 21
			 * Product Website URL - 22
			 * Product Type - 23
			 * Max Customer Quantity - 24
			 * Display in LIVE Game  - 25
			 * Product Sold Using Reward Points Only - 26
			 * Display Percentage Off - 27
			 * Display Strikeout Price - 28

			 * Length -29
			 * Width -30
			 * Height -31
			 * Measurement Unit -32
			 * Weight -33
			 * Measurement Unit -34
			 * Volume -35
			 * Measurement Unit -36
			 * Product Image File Name -37
			 * Variant Image File Name -38
			
			 * New Added  02-08 :: ========================
			 * Max Customer Quantity - 28
			 * Display in LIVE Game   - 29 
			 * Product Display Order     -30 
			 * product type	     -31 
			 * Product Sold Using Reward Points Only - 32 
			 * Reward Points Discount    - 33
			 * Display Percentage Off	       -34
			 * Display Strikeout Price       -35	
			 * Product Website URL         -36 
			 * 
			 */

			SellerProductUpload dvo = new SellerProductUpload();
			Integer integerval = null;
			Double douibleVal = null;
			String stringVal = null;
			Boolean boolValue = null;
			System.out.println("[PARSING ROW NUM ] " + row.getRowNum());
			stringVal = fetchMandatoryStringVallue(row, String.valueOf((Object) row.getCell(0)), dto, 1); // (Object) TO
																											 // AVOID NPE..
			dvo.setSlerCompName(stringVal);
			
			stringVal = fetchMandatoryStringVallue(row, String.valueOf((Object) row.getCell(1)), dto, 2); 
			dvo.setPname(stringVal);
			
			stringVal = fetchEmptyyStringIfNull(row, String.valueOf((Object) row.getCell(2)), dto, 3); 
			dvo.setPbrand(stringVal);
			
			stringVal = fetchEmptyyStringIfNull(row, String.valueOf((Object) row.getCell(3)), dto, 4); 
			dvo.setPdesc(stringVal);
			
			stringVal = fetchEmptyyStringIfNull(row, String.valueOf((Object) row.getCell(4)), dto, 5); 
			dvo.setPlongDesc(stringVal);
			
			douibleVal = fetchDoubleValue(row, String.valueOf((Object) row.getCell(5)), dto, 6);
			dvo.setPregMinPprc(douibleVal);
			
			douibleVal = fetchDoubleValue(row, String.valueOf((Object) row.getCell(6)), dto, 7);
			dvo.setPselMinPrc(douibleVal);
			
			douibleVal = fetchDoubleValue(row, String.valueOf((Object) row.getCell(7)), dto, 8);
			dvo.setDiscOnSelPrice(douibleVal);
			
			integerval = fetchIntegerValue(row, String.valueOf((Object) row.getCell(8)), dto, 9);
			dvo.setReqdLoyaltyPoints(integerval);
			
			integerval = fetchIntegerValue(row, String.valueOf((Object) row.getCell(9)), dto, 10);
			dvo.setDisplayOrder(integerval);
			
			integerval = fetchIntegerValue(row, String.valueOf((Object) row.getCell(10)), dto, 11);
			dvo.setAvlQty(integerval);
			
			// Variant Names and Values ...
			
			stringVal = fetchEmptyyStringIfNull(row, String.valueOf((Object) row.getCell(11)), dto, 12); 
			dvo.setVonameOne(stringVal);
			stringVal = fetchEmptyyStringIfNull(row, String.valueOf((Object) row.getCell(12)), dto, 13); 
			dvo.setVovalueOne(stringVal);
			
			stringVal = fetchEmptyyStringIfNull(row, String.valueOf((Object) row.getCell(13)), dto, 14); 
			dvo.setVonameTwo(stringVal);
			stringVal = fetchEmptyyStringIfNull(row, String.valueOf((Object) row.getCell(14)), dto, 15); 
			dvo.setVovalueTwo(stringVal);
			
			stringVal = fetchEmptyyStringIfNull(row, String.valueOf((Object) row.getCell(15)), dto, 16); 
			dvo.setVonameThree(stringVal);
			stringVal = fetchEmptyyStringIfNull(row, String.valueOf((Object) row.getCell(16)), dto, 17); 
			dvo.setVovalueThree(stringVal);
			
			stringVal = fetchEmptyyStringIfNull(row, String.valueOf((Object) row.getCell(17)), dto, 18); 
			dvo.setVonamefour(stringVal);
			stringVal = fetchEmptyyStringIfNull(row, String.valueOf((Object) row.getCell(18)), dto, 19); 
			dvo.setVovalueFour(stringVal);
			
			
			stringVal = GenUtil.getEmptyStringIfNull(String.valueOf((Object) row.getCell(19)));
			dvo.setPimg(stringVal);
			
			stringVal = GenUtil.getEmptyStringIfNull(String.valueOf((Object) row.getCell(20)));
			dvo.setPvarImg(stringVal);
			
			stringVal = GenUtil.getEmptyStringIfNull(String.valueOf((Object) row.getCell(21)));
			dvo.setProdWebsiteUrl(stringVal);
			
			stringVal= fetchMandatoryStringVallue(row, String.valueOf((Object) row.getCell(22)), dto, 23);
			dvo.setProdType(stringVal);
			
			/*stringVal = fetchMandatoryStringVallue(row, String.valueOf((Object) row.getCell(26)), dto, 26); 
			dvo.setProdType(stringVal);*/
			
			
			
			/**
			 * max Customer Quantity - 27
			 * is for LIVE game  - 28 
			 * Display order     -29 
			 * product type	     -30 
			 * only Reward points Product - 31 
			 * required Loyalty POINTS    - 32
			 * Price After Loyalty Points  -33	
			 * Show Percentage Off	       -34
			 * Display Regular Price       -35	
			 * product website URL         -36 
			 */
			// Max Cust Quanntity
			integerval = fetchIntegerValue(row, String.valueOf((Object) row.getCell(23)), dto, 24);
			dvo.setMaxCustQty(integerval);
			
			//is for LIVE game
			boolValue = fetchBooleanValueFromString(row, String.valueOf((Object) row.getCell(24)), dto, 25);
			dvo.setIsForLive(boolValue);
			
			boolValue = fetchBooleanValueFromString(row, String.valueOf((Object) row.getCell(25)), dto, 26);
			dvo.setOnlyRewardPointsProd(boolValue);
			
			
			/*douibleVal = fetchDoubleValue(row, String.valueOf((Object) row.getCell(32)), dto, 33);
			dvo.setPriceAfterLoyaltyPoints(douibleVal);*/
			
			
			boolValue = fetchBooleanValueFromString(row, String.valueOf((Object) row.getCell(26)), dto, 27);
			dvo.setIsPercentageOff(boolValue);
			boolValue = fetchBooleanValueFromString(row, String.valueOf((Object) row.getCell(27)), dto, 28);
			dvo.setIsDsplayRegularPrice(boolValue);
			
			
			dvo.setLupdBy(dto.getUserName());
			
			
			dvo.setIsVariantProduct(true);
			if((dvo.getVonameOne() == null || dvo.getVonameOne().isEmpty()) && (dvo.getVovalueOne() == null || dvo.getVovalueOne().isEmpty()) &&
					(dvo.getVonameTwo() == null || dvo.getVonameTwo().isEmpty()) && (dvo.getVovalueTwo() == null || dvo.getVovalueTwo().isEmpty()) &&
					(dvo.getVonameThree() == null || dvo.getVonameThree().isEmpty()) && (dvo.getVovalueThree() == null || dvo.getVovalueThree().isEmpty()) &&
					(dvo.getVonamefour() == null || dvo.getVonamefour().isEmpty()) && (dvo.getVovalueFour() == null || dvo.getVovalueFour().isEmpty())
					) {
				dvo.setIsVariantProduct(false);
				dvo.setVonameOne("N/A");
				dvo.setVovalueOne("N/A");
			} 
			
			double selPrice = dvo.getPselMinPrc();
			if(dvo.getDiscOnSelPrice() > 0) {
				selPrice = EcommUtil.getNormalRoundedValue(selPrice - EcommUtil.getNormalRoundedValue(selPrice*dvo.getDiscOnSelPrice()/100));
			}
			dvo.setPselMinPrc(selPrice);
			double discAmt = EcommUtil.getNormalRoundedValue(dvo.getReqdLoyaltyPoints()/100.0);
			if(selPrice >= discAmt) {
				double afterLoyaltyPoints = EcommUtil.getNormalRoundedValue(selPrice - discAmt);
				dvo.setPriceAfterLoyaltyPoints(afterLoyaltyPoints);	
			} else {
				dvo.setPriceAfterLoyaltyPoints(0.0);
			}
			if(dvo.getOnlyRewardPointsProd()) {
				dvo.setPriceAfterLoyaltyPoints(0.0);
			}
			
			System.out.println(dvo);
			return dvo;

		}


	private static SellerProductsItems populateItmdvo(SellerProductsItems itmdvo,SellerProductUpload uplddvo, Integer sellerId ) {
		  itmdvo.setSellerId(sellerId);
		  itmdvo.setpName(uplddvo.getPname());
		  itmdvo.setpBrand(uplddvo.getPbrand());
		  itmdvo.setpDesc(uplddvo.getPdesc());
		  itmdvo.setpLongDesc(uplddvo.getPlongDesc());
		  itmdvo.setpRegMinPrice(uplddvo.getPregMinPprc());
		  itmdvo.setPsMinPrice(uplddvo.getPselMinPrc());
		  itmdvo.setUpdBy(uplddvo.getLupdBy());
		  itmdvo.setUpdDate(new Date());
		  itmdvo.setpImgUrl(uplddvo.getPimg());	
		  itmdvo.setStatus("PENDING");		 
		  itmdvo.setpExpDate( new Date( new Date().getTime() + (1000 * 60 * 60 * 24)));
		  itmdvo.setIsVarient(uplddvo.getIsVariantProduct());
		  
		  itmdvo.setMaxCustQty(uplddvo.getMaxCustQty());
		  itmdvo.setIsForLiveGame(uplddvo.getIsForLive());
		  itmdvo.setDispOrder(uplddvo.getDisplayOrder());
		  itmdvo.setProdType(uplddvo.getProdType());
		  itmdvo.setIsRwdPointProd(uplddvo.getOnlyRewardPointsProd());
		  itmdvo.setDispPerctageOff(uplddvo.getIsPercentageOff());
		  itmdvo.setIsDisplayRegularPrice(uplddvo.getIsDsplayRegularPrice());
		
		  if(uplddvo.getOnlyRewardPointsProd()) {
			  itmdvo.setReqRwdPoint(uplddvo.getReqdLoyaltyPoints());			  
		  } else {
			  itmdvo.setReqRwdPoint(0);
		  }
			/**
			 * max Customer Quantity - 27
			 * is for LIVE game  - 28 
			 * Display order     -29 
			 * product type	     -30 
			 * only Reward points Product - 31 
			 * required Loyalty POINTS    - 32
			 * Price After Loyalty Points  -33	--- Only Inventory 
			 * Show Percentage Off	       -34
			 * Display Regular Price       -35	
			 * product website URL         -36 
			 */
		  return itmdvo;
	  }
	  
	  
	  
	  
	  public static void main1(String[] args)  throws Exception{
		 
		//List<SellerProductUpload >  upldList = getUploadList();
		//Map<String,SellerProductUpload>	processedDVOMap = processInventoryUpload(upldList) ;
		//saveInventoryDetails(processedDVOMap ,1);
		  Set<String> optvalueSet =  new HashSet<String>();
		  optvalueSet.add("2");
		  optvalueSet.add("1");
		  optvalueSet.add("3");
		  
		  
		  Optional<String> firstString = optvalueSet.stream().findFirst();
		  String first = null;
		  if(firstString.isPresent()){
		      first = firstString.get();
		      System.out.println(" first  Opt var value " + first   );
		  }
		    
			for(String optvalue:optvalueSet) {				
				SellerProdItemsVariValue  varvalue = new SellerProdItemsVariValue();
				if(first.equals(varvalue))  {
					
				}
				varvalue.setVarVal(optvalue);;
				System.out.println(" Save d Opt var value " + optvalue );
			}
		  
	 }

	public static SellerProductUploadTO validateInventoryFileUpload(Map<String, SellerProductUpload> uploadProdsMap,RTFSellerDetails sellerDtl,
			SellerProductUploadTO dto) throws Exception {

		String validateMsg = null;
		Error error = new Error();
		dto.setStatus(0);
		try {
			for (SellerProductUpload prodObj : uploadProdsMap.values()) {
	
				for (SellerProductUpload invObj : prodObj.getInventoryList()) {
					
					if(!invObj.getSlerCompName().equalsIgnoreCase(sellerDtl.getCompName())) {
						error.setDescription("Seller Name Should be same for all records.");
						dto.setError(error);
						return dto;
					}
	
					validateMsg = validateInventoryDetailsLength(invObj);
					if(validateMsg != null) {
						error.setDescription(validateMsg);
						dto.setError(error);
						return dto;
					}
					
					validateMsg = validateInventoryRecord(prodObj, invObj);
					if(validateMsg != null) {
						error.setDescription(validateMsg);
						dto.setError(error);
						return dto;
					}
				}
				Set<String> prodvarnameset = prodObj.getProdvarnameset();
				/*if (prodvarnameset == null || prodvarnameset.isEmpty()) {
					validateMsg = "Variant Option can't Empty for Product " + prodObj.getPname();
					error.setDescription(validateMsg);
					dto.setError(error);
					return dto;
				}*/
				if(prodvarnameset != null && !prodvarnameset.isEmpty()) {
					for (String varname : prodvarnameset) {
						Set<String> optvalueSet = prodObj.getPnameVarOptValueSetMap().get(varname);
						if (optvalueSet == null || optvalueSet.isEmpty()) {
							validateMsg = "Variant Option Value can't Empty for Product " + prodObj.getPname()
									+ " and variant " + varname;
							error.setDescription(validateMsg);
							dto.setError(error);
							return dto;
						}
					}
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public static String validateInventoryRecord(SellerProductUpload prodObj,SellerProductUpload invObj) throws Exception {
		
		if (!prodObj.getProdType().equals(invObj.getProdType())) {
			return "Product Type - Should be same for all rows of product " + prodObj.getPname();
		} 
		if (!prodObj.getIsForLive().equals(invObj.getIsForLive())) {
			return "Is for LIVE game - Should be same for all rows of product " + prodObj.getPname();
		}
		if (!prodObj.getIsDsplayRegularPrice().equals(invObj.getIsDsplayRegularPrice())) {
			return "Display Regular Price - Should be same for all rows of product " + prodObj.getPname();
		}
		if (!prodObj.getOnlyRewardPointsProd().equals(invObj.getOnlyRewardPointsProd())) {
			return "only Reward points Product - Should be same for all rows of product " + prodObj.getPname();
		} 
		if (!prodObj.getIsPercentageOff().equals(invObj.getIsPercentageOff())) {
			return "Show Percentage Off - Should be same for all rows of product " + prodObj.getPname();
		}
		if (!prodObj.getDisplayOrder().equals(invObj.getDisplayOrder())) {
			return "Display Order - Should be same for all rows of product " + prodObj.getPname();
		}
		if (!prodObj.getPbrand().equals(invObj.getPbrand())) {
			return "Product Brand Name - Should be same for all rows of product " + prodObj.getPname();
		}
		if (!prodObj.getSlerCompName().equals(invObj.getSlerCompName())) {
			return "Seller Name - Should be same for all rows of product " + prodObj.getPname();
		} 
		
		if(!invObj.getProdType().equalsIgnoreCase("PRODUCT") && !invObj.getProdType().equalsIgnoreCase("DIGITALPRODUCT") &&
				!invObj.getProdType().equalsIgnoreCase("GIFTCARD")) {
			return "Product Type - Should be PRODUCT or DIGITALPRODUCT or GIFTCARD for product" + prodObj.getPname();
		}

		if(invObj.getOnlyRewardPointsProd()) {
			if(invObj.getReqdLoyaltyPoints() <= 0) {
				return "Loyalty Points should be grater than 0 for Product Sold Using Reward Points Only  " + prodObj.getPname();
			}
			if(invObj.getPselMinPrc() > 0) {
				return "Selling price should be 0 for Product Sold Using Reward Points Only  " + prodObj.getPname();
			}
			/*if(invObj.getPriceAfterLoyaltyPoints() > 0) {
				return "Price After Loyalty Points - should be 0 for Only Reward Points Product  " + prodObj.getPname();
			}*/
		} else {
			if(invObj.getPselMinPrc() <= 0) {
				return "Selling price should be greater than 0 forRegular Product  " + prodObj.getPname();
			}
		}
		return null;
	}
	
public static String validateInventoryDetailsLength(SellerProductUpload invObj) throws Exception {
		
		if (invObj.getPname().length() > 120) {
			return "Product Name length should be within 120 characters for product " + invObj.getPname();
		}
		
		if (invObj.getPdesc().length() > 250) {
			return "Product Short Description length should be within 250 characters for product " + invObj.getPname();
		}
		
		if (invObj.getPlongDesc().length() > 3500) {
			return "Product Full Description length should be within 3500 characters for product " + invObj.getPname();
		}
		if (invObj.getPbrand().length() > 80) {
			return "Product Brand length should be within 80 characters for product " + invObj.getPname();
		}
		
		if (invObj.getPimg() != null && invObj.getPimg().length() > 1000) {
			return "Product Image File Name length should be within 1000 characters for product " + invObj.getPname();
		}
		if (invObj.getPvarImg() != null && invObj.getPvarImg().length() > 1000) {
			return "Variant Image File Name length should be within 1000 characters for product " + invObj.getPname();
		}
		if (invObj.getProdWebsiteUrl() != null && invObj.getProdWebsiteUrl().length() > 1000) {
			return "product website URL length should be within 1000 characters for product " + invObj.getPname();
		}
		
		if (invObj.getVonameOne() != null && invObj.getVonameOne().length() > 80) {
			return "Variant Name (A) length should be within 80 characters for product " + invObj.getPname();
		}
		if (invObj.getVovalueOne() != null && invObj.getVovalueOne().length() > 80) {
			return "Variant Value (A) length should be within 80 characters for product " + invObj.getPname();
		}
		
		if (invObj.getVonameTwo() != null && invObj.getVonameTwo().length() > 80) {
			return "Variant Name (B) length should be within 80 characters for product " + invObj.getPname();
		}
		if (invObj.getVovalueTwo() != null && invObj.getVovalueTwo().length() > 80) {
			return "Variant Value (B) length should be within 80 characters for product " + invObj.getPname();
		}
		
		if (invObj.getVonameThree() != null && invObj.getVonameThree().length() > 80) {
			return "Variant Name (C) length should be within 80 characters for product " + invObj.getPname();
		}
		if (invObj.getVovalueThree() != null && invObj.getVovalueThree().length() > 80) {
			return "Variant Value (C) length should be within 80 characters for product " + invObj.getPname();
		}
		
		if (invObj.getVonamefour() != null && invObj.getVonamefour().length() > 80) {
			return "Variant Name (D) length should be within 80 characters for product " + invObj.getPname();
		}
		if (invObj.getVovalueFour() != null && invObj.getVovalueFour().length() > 80) {
			return "Variant Value (D) length should be within 80 characters for product " + invObj.getPname();
		}
		
		return null;
	}


	public static Map<String,SellerProductUpload> processInventoryUpload(List<SellerProductUpload> upldList) {
		    Map<String ,List<SellerProductUpload>> allprodMasterListMap = new LinkedHashMap<String ,List<SellerProductUpload>>(); // By Pname List
			Map<String,SellerProductUpload> uniqueProdMasterMapByProdName = new LinkedHashMap<String ,SellerProductUpload>(); // Unique
		
			for(SellerProductUpload dvo : upldList) {			
				String pname = dvo.getPname();
				List<SellerProductUpload> ptmlist = null;
				if(allprodMasterListMap.containsKey(pname)) {
					ptmlist = allprodMasterListMap.get(pname);
				}else {
					ptmlist = new ArrayList<SellerProductUpload>();
				}
				ptmlist.add(dvo);
				allprodMasterListMap.put(pname, ptmlist);
				
				SellerProductUpload prodUpload = uniqueProdMasterMapByProdName.get(pname);
				if(prodUpload != null && prodUpload.getPselMinPrc() <= dvo.getPselMinPrc()) {
					continue;
				}
				uniqueProdMasterMapByProdName.put(pname, dvo);
			}
		 
		 for (String pnameKey : uniqueProdMasterMapByProdName.keySet())  
		 {
			
			 SellerProductUpload uniqueSellerProductUpload = uniqueProdMasterMapByProdName.get(pnameKey);
			 Set<String> uniqueVarnameSet = uniqueSellerProductUpload.getProdvarnameset();
			 if(uniqueVarnameSet == null ) {
				 uniqueVarnameSet = new LinkedHashSet<String>();
				 uniqueSellerProductUpload.setProdvarnameset(uniqueVarnameSet);
			 }
			 Set<String> optvalSet = new LinkedHashSet<String>();
			 Map<String,Set<String>> pnameOptValueSetMap = new LinkedHashMap<String,Set<String>>();
			 uniqueSellerProductUpload.setPnameVarOptValueSetMap(pnameOptValueSetMap);
				
				if(!GenUtil.isEmptyString(uniqueSellerProductUpload.getVonameOne())) {
					uniqueSellerProductUpload.getProdvarnameset().add(uniqueSellerProductUpload.getVonameOne());
					Set<String> optval1Set = new HashSet<String>();
					uniqueSellerProductUpload.getPnameVarOptValueSetMap().put(uniqueSellerProductUpload.getVonameOne(), optval1Set);
					
				}
				if(!GenUtil.isEmptyString(uniqueSellerProductUpload.getVonameTwo())) {
					uniqueSellerProductUpload.getProdvarnameset().add(uniqueSellerProductUpload.getVonameTwo());
					Set<String> optval2Set = new LinkedHashSet<String>();
					uniqueSellerProductUpload.getPnameVarOptValueSetMap().put(uniqueSellerProductUpload.getVonameTwo(), optval2Set);
				}		
				if(!GenUtil.isEmptyString(uniqueSellerProductUpload.getVonameThree())) {
					uniqueSellerProductUpload.getProdvarnameset().add(uniqueSellerProductUpload.getVonameThree());
					Set<String> optval3Set = new LinkedHashSet<String>();
					uniqueSellerProductUpload.getPnameVarOptValueSetMap().put(uniqueSellerProductUpload.getVonameThree(), optval3Set);
				}		
				if(!GenUtil.isEmptyString(uniqueSellerProductUpload.getVonamefour())) {
					uniqueSellerProductUpload.getProdvarnameset().add(uniqueSellerProductUpload.getVonamefour());
					Set<String> optval4Set = new LinkedHashSet<String>();
					uniqueSellerProductUpload.getPnameVarOptValueSetMap().put(uniqueSellerProductUpload.getVonamefour(), optval4Set);
				}
			 
		 }
	
		 
		 for (String pnameKey : uniqueProdMasterMapByProdName.keySet())  
		 {			
			    SellerProductUpload uniqueSellerProductUpload = uniqueProdMasterMapByProdName.get(pnameKey);			    
			    List<SellerProductUpload> commprodsdvolst = allprodMasterListMap.get(pnameKey);			    
			    for(SellerProductUpload commdvo:commprodsdvolst) {	
			    	String vnameOne = commdvo.getVonameOne();
			    	Set<String> optvalSet1 = uniqueSellerProductUpload.getPnameVarOptValueSetMap().get(vnameOne);
			    	if(optvalSet1 != null) {
			    		optvalSet1.add(commdvo.getVovalueOne());
			    		uniqueSellerProductUpload.getPnameVarOptValueSetMap().put(vnameOne,optvalSet1);
			    	}
			    	String vnameTwo = commdvo.getVonameTwo();
			    	Set<String> optvalSet2 = uniqueSellerProductUpload.getPnameVarOptValueSetMap().get(vnameTwo);
			    	if(optvalSet2 != null) {
			    		optvalSet2.add(commdvo.getVovalueTwo());
			    		uniqueSellerProductUpload.getPnameVarOptValueSetMap().put(vnameTwo,optvalSet2);
			    	}
			    	String vnameThree = commdvo.getVonameThree();
			    	Set<String> optvalSet3 = uniqueSellerProductUpload.getPnameVarOptValueSetMap().get(vnameThree);
			    	if(optvalSet3 != null) {
			    		optvalSet3.add(commdvo.getVovalueThree());
			    		uniqueSellerProductUpload.getPnameVarOptValueSetMap().put(vnameThree,optvalSet3);
			    	}
			    	String vnameFour = commdvo.getVonamefour();
			    	Set<String> optvalSet4 = uniqueSellerProductUpload.getPnameVarOptValueSetMap().get(vnameFour);
			    	if(optvalSet4 != null) {
			    		optvalSet4.add(commdvo.getVovalueFour());
			    		uniqueSellerProductUpload.getPnameVarOptValueSetMap().put(vnameFour,optvalSet4);
			    	}			    	
			    }
		 }	
		 
		for (String pnameKey : uniqueProdMasterMapByProdName.keySet()) {
			SellerProductUpload uniqueSellerProductUpload = uniqueProdMasterMapByProdName.get(pnameKey);
			List<SellerProductUpload> commprodsdvolst = allprodMasterListMap.get(pnameKey);
			uniqueSellerProductUpload.setInventoryList(commprodsdvolst);
		}
		 
		 System.out.println("Unique Variant names Set added to unique POJO " );
		 System.out.println("[All prod List ] " + allprodMasterListMap.size());
		 System.out.println("[Unique  prod List ] " + uniqueProdMasterMapByProdName.size());
		 
		 System.out.println(uniqueProdMasterMapByProdName);		  
		 return uniqueProdMasterMapByProdName;
	}
	
	

	public static void saveInventoryDetails(Map<String, SellerProductUpload> processedDVOMap,Integer sellerId,SellerProductUploadTO dto ) {
		
		try {		
		System.out.println("Save All Product Master Details ....");
		for (String pnameKey : processedDVOMap.keySet())  
		 {
			   SellerProductUpload uniqueSellerProductUpload = processedDVOMap.get(pnameKey);
			   SellerProductsItems itmdvo = new SellerProductsItems();
			   itmdvo=  populateItmdvo(itmdvo,uniqueSellerProductUpload , sellerId);
			   Integer id=EcommDAORegistry.getSellerProductsItemsDAO().save(itmdvo);
			   uniqueSellerProductUpload.setProdId(id);
			   System.out.println("SavedProduct Master Details .... with Id "  + id);			 
		 }
		 System.out.println("Save All Product Variant Name Details ....");
		 for (String pnameKey : processedDVOMap.keySet())  
		 {
			   SellerProductUpload uniqueSellerProductUpload = processedDVOMap.get(pnameKey);
			   Set<String>  prodvarnameset =  uniqueSellerProductUpload.getProdvarnameset();
			   for(String varname:prodvarnameset) {
				     SellerProdItemsVariName varnamedvo = new SellerProdItemsVariName();
					 varnamedvo.setSellerId(sellerId);
					 varnamedvo.setSpiId(uniqueSellerProductUpload.getProdId());
					 varnamedvo.setvOptName(varname);
					 varnamedvo.setUpdBy(uniqueSellerProductUpload.getLupdBy());
					 varnamedvo.setUpdDate(new Date()); 					 
					 Integer varNameId = EcommDAORegistry.getSellerProdItemsVariNameDAO().save(varnamedvo);
					 System.out.println("[ Variany Name  ] " + varname   + " [ variant ID  ]" +  varNameId );
					 Map<String,Integer> varNameIdMap =  uniqueSellerProductUpload.getVariantNameIdMap();
					 if(varNameIdMap == null) {
						varNameIdMap = new HashMap<String,Integer>();
					 	uniqueSellerProductUpload.setVariantNameIdMap(varNameIdMap);
					 }
					 
					 uniqueSellerProductUpload.getVariantNameIdMap().put(varname, varNameId);				   
			   }			 
		 }
		 
		 System.out.println("Saved All Product Option Value Details....");
		 
		 for (String pnameKey : processedDVOMap.keySet())  
		 {
			   SellerProductUpload uniqueSellerProductUpload = processedDVOMap.get(pnameKey);
			   Set<String>  prodvarnameset =  uniqueSellerProductUpload.getProdvarnameset();
			     
			   for(String varname:prodvarnameset) {
				   Integer varNameId =  uniqueSellerProductUpload.getVariantNameIdMap().get(varname);
				   Set<String> optvalueSet = uniqueSellerProductUpload.getPnameVarOptValueSetMap().get(varname);
				   
				
				   Optional<String> firstOptVal = optvalueSet.stream().findFirst();
					  String firstvalue = "";					
					  if(firstOptVal.isPresent()){
						  firstvalue = firstOptVal.get();
					      System.out.println(" first  Opt var value " + firstvalue   );
					  }
				   
				   for(String optvalue:optvalueSet) {
					   SellerProdItemsVariValue  varvalue = new SellerProdItemsVariValue();
					   varvalue.setSlrpiVarId(varNameId);
					   varvalue.setVarVal(optvalue);
					   varvalue.setUpdBy(uniqueSellerProductUpload.getLupdBy());
					   varvalue.setUpdDate(new Date());
					   //System.out.println(" Updating Variant Option  Value " + optvalue + "  with " + uniqueSellerProductUpload.getPvarImg());
					   if(firstvalue.equals(optvalue)) {
						   varvalue.setVarValImg(uniqueSellerProductUpload.getPvarImg());
						   System.out.println("Saving Variant Image for  optvalue [  " + optvalue  + " , " + uniqueSellerProductUpload.getPvarImg() );
					   }else {
						   System.out.println("NOT Saving Variant Image for  optvalue [  "  + optvalue +  " ] " ) ;  
					   }
					   Integer varoptValueId = EcommDAORegistry.getSellerProdItemsVariValueDAO().save(varvalue);					   
					   System.out.println(" Saved Opt var value " + varoptValueId);					   
				   }
				   	   
			   }			 
		 } 
		}catch(Exception ex) {
			throw ex;
		}
	}
	public static void main(String[] args) {
		Scanner scr = new Scanner(System.in);
		String str ="";
		while(!str.equals("EXIT")) {
			System.out.println("Enter text : ");
			str = scr.nextLine();
			System.out.println("str : "+str +" : "+getTitleCaseString(str));
		}
	}
	
	public static String getTitleCaseString(String value) {
		value = WordUtils.capitalizeFully(value);
		return value;
	}
	
public static void saveInventoryDetailsNew(Map<String, SellerProductUpload> processedDVOMap,Integer sellerId,String userName,SellerProductUploadTO dto ) throws Exception {
		
	List<SellerProductsItems> prodItmsList = new ArrayList<SellerProductsItems>();
		try {		
		System.out.println("Save All Product Master Details ....");
		
		for (String pnameKey : processedDVOMap.keySet())  
		 {
			   SellerProductUpload uniqueSellerProductUpload = processedDVOMap.get(pnameKey);
			   SellerProductsItems itmdvo = new SellerProductsItems();
			   itmdvo =  populateItmdvo(itmdvo,uniqueSellerProductUpload , sellerId);
			   Integer id=EcommDAORegistry.getSellerProductsItemsDAO().save(itmdvo);
			   uniqueSellerProductUpload.setProdId(id);
			   System.out.println("SavedProduct Master Details .... with Id "  + id);
			   
			   List<SellerProdItemsVariName> variNameList = new ArrayList<SellerProdItemsVariName>();
			   
			   System.out.println("Save All Product Variant Name Details ....");
			   Set<String>  prodvarnameset =  uniqueSellerProductUpload.getProdvarnameset();
			   for(String varname:prodvarnameset) {
				     SellerProdItemsVariName varnamedvo = new SellerProdItemsVariName();
					 varnamedvo.setSellerId(sellerId);
					 varnamedvo.setSpiId(uniqueSellerProductUpload.getProdId());
					 varnamedvo.setvOptName(varname);
					 varnamedvo.setUpdBy(uniqueSellerProductUpload.getLupdBy());
					 varnamedvo.setUpdDate(new Date()); 					 
					 Integer varNameId = EcommDAORegistry.getSellerProdItemsVariNameDAO().save(varnamedvo);
					 System.out.println("[ Variany Name  ] " + varname   + " [ variant ID  ]" +  varNameId );
					 Map<String,Integer> varNameIdMap =  uniqueSellerProductUpload.getVariantNameIdMap();
					 if(varNameIdMap == null) {
						varNameIdMap = new HashMap<String,Integer>();
					 	uniqueSellerProductUpload.setVariantNameIdMap(varNameIdMap);
					 }
					 
					 uniqueSellerProductUpload.getVariantNameIdMap().put(varname, varNameId);
					 
					 System.out.println("Saved All Product Option Value Details....");
					 
					 Set<String> optvalueSet = uniqueSellerProductUpload.getPnameVarOptValueSetMap().get(varname);
					   Optional<String> firstOptVal = optvalueSet.stream().findFirst();
						  String firstvalue = "";					
						  if(firstOptVal.isPresent()){
							  firstvalue = firstOptVal.get();
						      System.out.println(" first  Opt var value " + firstvalue   );
						  }
					   
						List<SellerProdItemsVariValue> variValList = new ArrayList<SellerProdItemsVariValue>();
					   for(String optvalue:optvalueSet) {
						   SellerProdItemsVariValue  varvalue = new SellerProdItemsVariValue();
						   varvalue.setSlrpiVarId(varNameId);
						   varvalue.setVarVal(optvalue);
						   varvalue.setUpdBy(uniqueSellerProductUpload.getLupdBy());
						   varvalue.setUpdDate(new Date());
						   //System.out.println(" Updating Variant Option  Value " + optvalue + "  with " + uniqueSellerProductUpload.getPvarImg());
						   if(firstvalue.equals(optvalue)) {
							   varvalue.setVarValImg(uniqueSellerProductUpload.getPvarImg());
							   System.out.println("Saving Variant Image for  optvalue [  " + optvalue  + " , " + uniqueSellerProductUpload.getPvarImg() );
						   }else {
							   System.out.println("NOT Saving Variant Image for  optvalue [  "  + optvalue +  " ] " ) ;  
						   }
						   Integer varoptValueId = EcommDAORegistry.getSellerProdItemsVariValueDAO().save(varvalue);					   
						   System.out.println(" Saved Opt var value " + varoptValueId);
						   variValList.add(varvalue);
					   }
					   varnamedvo.setValues(variValList);
					   variNameList.add(varnamedvo);
			   }
			   prodItmsList.add(itmdvo);
			   
			   Integer responseCode = EcommDAORegistry.getSellerProdItemsVariInventoryDAO().generateSellerPRoductVariantMAtrix(sellerId, itmdvo.getSpiId(),userName);
				if(responseCode != 0) {
					 System.out.println("Error occured in Products Inventory creation for Product : "+uniqueSellerProductUpload.getPname());
					 Error error = new  Error();
					  dto.setStatus(0);
					  error.setDescription("Error occured in Products Inventory creation for Product : "+uniqueSellerProductUpload.getPname());
					  dto.setError(error);	
					 throw new RuntimeException();
				}
				
				List<SellerProdItemsVariInventory> invnotryList = EcommDAORegistry.getSellerProdItemsVariInventoryDAO().getInventoryByProductId(itmdvo.getSpiId());
				if(invnotryList == null) {
					 System.out.println("Error occured in Products Inventory creation for Product : "+uniqueSellerProductUpload.getPname());
					 Error error = new  Error();
					  dto.setStatus(0);
					  error.setDescription("Error occured in Products Inventory creation for Product : "+uniqueSellerProductUpload.getPname());
					  dto.setError(error);	
					 throw new RuntimeException();
				}
				Map<String,SellerProdItemsVariInventory> inventroyMap = new HashMap<String,SellerProdItemsVariInventory>();
				for (SellerProdItemsVariInventory inventory : invnotryList) {
					String key = getEmptyOrStringValue(inventory.getVn1())+":"+getEmptyOrStringValue(inventory.getVv1())+":"+getEmptyOrStringValue(inventory.getVn2())
					+":"+getEmptyOrStringValue(inventory.getVv2())+":"+getEmptyOrStringValue(inventory.getVn3())+":"+getEmptyOrStringValue(inventory.getVv3())+":"+
					getEmptyOrStringValue(inventory.getVn4())+":"+getEmptyOrStringValue(inventory.getVv4());
					inventroyMap.put(key, inventory);
				}
				
				invnotryList = new ArrayList<SellerProdItemsVariInventory>();
				List<SellerProductUpload> invList = uniqueSellerProductUpload.getInventoryList();
				for (SellerProductUpload obj : invList) {
					String key = obj.getVonameOne()+":"+obj.getVovalueOne()+":"+obj.getVonameTwo()+":"+obj.getVovalueTwo()+":"+obj.getVonameThree()+":"+obj.getVovalueThree()+":"+
							obj.getVonamefour()+":"+obj.getVovalueFour();
					SellerProdItemsVariInventory inventory = inventroyMap.get(key);
					if(inventory == null) {
						 System.out.println("Error occured in Products Inventory Details Update for Product : "+uniqueSellerProductUpload.getPname()+" and variant : "+key);
						 Error error = new  Error();
						  dto.setStatus(0);
						  error.setDescription("Error occured in Products Inventory Details Update for Product : "+uniqueSellerProductUpload.getPname());
						  dto.setError(error);	
						 throw new RuntimeException();
					}
					inventory.setAvlQty(obj.getAvlQty());
					inventory.setRegPrice(obj.getPregMinPprc());
					inventory.setSellPrice(obj.getPselMinPrc());
					inventory.setReqLPnts(obj.getReqdLoyaltyPoints());
					inventory.setPriceAftLPnts(obj.getPriceAfterLoyaltyPoints());
					
					inventory.setProdType(obj.getProdType());
					inventory.setIsRwdPointProd(obj.getOnlyRewardPointsProd());
					inventory.setIsDisplayRegularPrice(obj.getIsDsplayRegularPrice());
					inventory.setDispPerctageOff(obj.getIsPercentageOff());
					inventory.setProdWsUrl(obj.getProdWebsiteUrl());
					inventory.setpImg(obj.getPvarImg());;
					
					invnotryList.add(inventory);
				}
				EcommDAORegistry.getSellerProdItemsVariInventoryDAO().saveOrUpdateAll(invnotryList);
				
		 }
		 
		}catch(Exception ex) {
			
			deleteAllProudctInventoryDetailsData(prodItmsList);
						
			throw ex;
		}
	}
	private static String getEmptyOrStringValue(String value) {
		if(value == null) {
			return "";
		}
		return value;
	}
	private static void deleteAllProudctInventoryDetailsData(List<SellerProductsItems> prodItmsList) throws Exception {
			
		for (SellerProductsItems sellerProductsItems : prodItmsList) {
			EcommDAORegistry.getSellerProdItemsVariInventoryDAO().deleteInventoryByProductId(sellerProductsItems.getSpiId());
			
			for (SellerProdItemsVariName varNameObj : sellerProductsItems.getVariNamesList()) {
				if(varNameObj.getValues() != null) {
					EcommDAORegistry.getSellerProdItemsVariValueDAO().deleteAll(varNameObj.getValues());
				}
			}
			EcommDAORegistry.getSellerProdItemsVariNameDAO().deleteAll(sellerProductsItems.getVariNamesList());
			EcommDAORegistry.getSellerProductsItemsDAO().delete(sellerProductsItems);
			
		}
	}
	
	private static String fetchMandatoryStringVallue(Row row, String cellval, SellerProductUploadTO dto, Integer cellNo)
			throws Exception {

		try {
			if (GenUtil.isEmptyString(cellval) || "null".equalsIgnoreCase(cellval)) {
				throw new Exception();
			}
			return cellval;
		} catch (Exception ex) {
			  Error error = new  Error();
			  dto.setStatus(0);
			  error.setDescription(" Empty / Invalid  String/Text Value [ " + cellval + " ]  AT ROW NUMBER [ " + row.getRowNum()
				+ " ] CellNo [ " + cellNo + " ]");
			  dto.setError(error);	
			  throw ex;
		}
	}
	
	
	private static String fetchEmptyyStringIfNull(Row row, String cellval, SellerProductUploadTO dto, Integer cellNo)
			throws Exception {
		String retVal = "";
		try {
			if (GenUtil.isEmptyString(cellval) || "null".equalsIgnoreCase(cellval)) {
				return "";
			}
		
		} catch (Exception ex) {
			 
		}
		return cellval;
	}
	
	
	
	

	private static Integer fetchIntegerValue(Row row, String cellval, SellerProductUploadTO dto, Integer cellNo)
			throws Exception {
		Integer integerval;
		System.out.println(" Expecting Integer Recieved Value as " + cellval);
		try {
			integerval = Integer.valueOf(cellval);
		} catch (Exception ex) {			
			try {
				int i = (int) Double.parseDouble(cellval);
				integerval = new Integer(i);
				return integerval;
			}catch(Exception ex1) {
				
				  Error error = new  Error();
				  dto.setStatus(0);
				  error.setDescription(" Invalid Integer Value [ " + cellval + " ]  AT ROW NUMBER [ " + row.getRowNum() + " ] CellNo [ "
							+ cellNo + " ]");
				  dto.setError(error);	
				  throw ex;
				
			}
			
		}
		return integerval;
	}

	private static Integer fetchIntegerValueIfNotNull(Row row, String cellval, SellerProductUploadTO dto, Integer cellNo)
			throws Exception {
		Integer integerval;
		if (GenUtil.isEmptyString(cellval) || "null".equalsIgnoreCase(cellval)) {
			return null;
		}
		try {
			integerval = Integer.valueOf(cellval);
		} catch (Exception ex) {
			
			  Error error = new  Error();
			  dto.setStatus(0);
			  error.setDescription(" Invalid Integer Value [ " + cellval + " ]  AT ROW NUMBER [ " + row.getRowNum() + " ] CellNo [ "
						+ cellNo + " ]");
			  dto.setError(error);	
			  throw ex;		
		}	
		return integerval;
	}

	private static Double fetchDoubleValue(Row row, String cellval, SellerProductUploadTO dto, Integer cellNo)
			throws Exception {
		Double doubleval;
		try {
			doubleval = Double.valueOf(cellval);
		} catch (Exception ex) {
			Error error = new  Error();
			dto.setStatus(0);
			error.setDescription(" Invalid Double Value [ " + cellval + " ]  AT ROW NUMBER [ " + row.getRowNum() + " ] CellNo [ "
						+ cellNo + " ]");
			dto.setError(error);			
			throw ex;
		}
		return doubleval;
	}

	private static Boolean fetchBooleanValueFromString(Row row, String cellval, SellerProductUploadTO dto, Integer cellNo)
			throws Exception {
		Boolean boolval = false;
		try {
			if (GenUtil.isEmptyString(cellval)) {
				throw new Exception();
			}
			
			if (! (cellval.equalsIgnoreCase("Yes") || cellval.equalsIgnoreCase("No") )) {
				throw new Exception();
			}
			
			if (cellval.equalsIgnoreCase("Yes"))
				return true;
		} catch (Exception ex) {
			
			Error error = new  Error();
			dto.setStatus(0);
			error.setDescription(" Invalid Boolean (Yes/No) Value [ " + cellval + " ]  AT ROW NUMBER [ " + row.getRowNum() + " ] CellNo [ "
					+ cellNo + " ]");
			dto.setError(error);			
			throw ex;
		}
		return boolval;
	}
	private static Boolean fetchBooleanValue(Row row, String cellval, SellerProductUploadTO dto, Integer cellNo)
			throws Exception {
		Boolean boolval = false;
		try {
			if (GenUtil.isEmptyString(cellval)) {
				throw new Exception();
			}
			
			if (! (cellval.equals("0") || cellval.equals("1") ) && !(cellval.equals("0.0") || cellval.equals("1.0"))) {
				throw new Exception();
			}
			
			if (cellval.equals("1") || cellval.equals("1.0"))
				return true;
		} catch (Exception ex) {
			
			Error error = new  Error();
			dto.setStatus(0);
			error.setDescription(" Invalid Boolean Value [ " + cellval + " ]  AT ROW NUMBER [ " + row.getRowNum() + " ] CellNo [ "
					+ cellNo + " ]");
			dto.setError(error);			
			throw ex;
		}
		return boolval;
	}
	
	public  static File multipartToFile(MultipartFile multipart, String fileName) throws IllegalStateException, IOException {
	    File convFile = new File(System.getProperty("java.io.tmpdir")+"/"+fileName);
	    multipart.transferTo(convFile);
	    return convFile;
	}
	

	public static List<SellerProductUpload> getUploadList() {
		List<SellerProductUpload>  lst = new ArrayList<SellerProductUpload>();
		SellerProductUpload dvo = new SellerProductUpload();
		dvo.setPname("Men's Golf Polo Shirt");
		dvo.setPbrand("Adidas");
		dvo.setVonameOne("Size");
		dvo.setVonameTwo("Color");
		dvo.setVovalueOne("5");
		dvo.setVovalueTwo("Red");		
		lst.add(dvo);
		
		dvo = new SellerProductUpload();
		dvo.setPname("Men's Golf Polo Shirt");
		dvo.setPbrand("Adidas");
		dvo.setVonameOne("Size");
		dvo.setVonameTwo("Color");
		dvo.setVovalueOne("L");
		dvo.setVovalueTwo("Navy");
	
		lst.add(dvo);
		
		
		  dvo = new SellerProductUpload();
		  dvo.setPname("Reserved Footwear Men's Clint Sneaker");
		  dvo.setPbrand("Reserved");
		  dvo.setVonameOne("Size");
		  dvo.setVonameTwo("Color");
		  dvo.setVovalueOne("7.5");
		  dvo.setVovalueTwo("Black");
		  lst.add(dvo);
		  
		  dvo = new SellerProductUpload();
		  dvo.setPname("Reserved Footwear Men's Clint Sneaker");
		  dvo.setPbrand("Reserved");
		  dvo.setVonameOne("Size");
		  dvo.setVonameTwo("Color");
		  dvo.setVovalueOne("7.5");
		  dvo.setVovalueTwo("Brown");
		  lst.add(dvo);
		  
		  dvo = new SellerProductUpload();
		  dvo.setPname("Reserved Footwear Men's Clint Sneaker");
		  dvo.setPbrand("Reserved");
		  dvo.setVonameOne("Size");
		  dvo.setVonameTwo("Color");
		  dvo.setVovalueOne("7.5");
		  dvo.setVovalueTwo("Wheat");
		  lst.add(dvo);
		  
		  dvo = new SellerProductUpload();
		  dvo.setPname("Reserved Footwear Men's Clint Sneaker");
		  dvo.setPbrand("Reserved");
		  dvo.setVonameOne("Size");
		  dvo.setVonameTwo("Color");
		  dvo.setVovalueOne("8");
		  dvo.setVovalueTwo("Black");
		  lst.add(dvo);
		  
		  dvo = new SellerProductUpload();
		  dvo.setPname("Reserved Footwear Men's Clint Sneaker");
		  dvo.setPbrand("Reserved");
		  dvo.setVonameOne("Size");
		  dvo.setVonameTwo("Color");
		  dvo.setVovalueOne("8");
		  dvo.setVovalueTwo("Brown");
		  lst.add(dvo);
		  
		  dvo = new SellerProductUpload();
		  dvo.setPname("Reserved Footwear Men's Clint Sneaker");
		  dvo.setPbrand("Reserved");
		  dvo.setVonameOne("Size");
		  dvo.setVonameTwo("Color");
		  dvo.setVovalueOne("8");
		  dvo.setVovalueTwo("Wheat");
		  lst.add(dvo);
		 
		return lst;
	} 


	public static SXSSFWorkbook generateExcelForExport(List<SellerProductsItems> prodItmsList , SXSSFWorkbook workbook) {	
		
		try{			
			int j = 0; int rowCount = 1;		
			List<String> headerList = new ArrayList<String>();
			headerList.add("slrpitms_id");
			headerList.add("sler_id");
			headerList.add("pbrand");
			headerList.add("pname");
			headerList.add("pdesc");
			headerList.add("plong_desc");
			
			headerList.add("psel_min_prc");
			headerList.add("preg_min_prc");
			headerList.add("pimg");
			headerList.add("psku");
			headerList.add("pstatus");
			headerList.add("pexp_date");
			
			headerList.add("lupd_date");
			headerList.add("lupd_by");
			headerList.add("is_variant");
			headerList.add("max_cust_qty");
			headerList.add("disp_start_date");
			headerList.add("is_for_live_game");
			Sheet ssSheet2 = workbook.createSheet("RTFPRODUCTS");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet2);	
			j = 0; rowCount = 1;
			for(SellerProductsItems obj : prodItmsList){
				Row rowhead =   ssSheet2.createRow((int)rowCount);
				rowCount++;
				j = 0;
				Util.getExcelStringCell(obj.getSpiId(), j, rowhead);j++;			
				Util.getExcelStringCell("0", j, rowhead);j++;
				Util.getExcelStringCell(obj.getpBrand(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getpName(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getpDesc(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getpLongDesc(), j, rowhead);j++;
				
				Util.getExcelStringCell(obj.getPsMinPrice(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getpRegMinPrice(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getpImgUrl(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getPsku(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getStatus(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getpExpDate(), j, rowhead);j++;
				
				Util.getExcelStringCell(obj.getUpdDate(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getUpdBy(), j, rowhead);j++;
				Util.getExcelStringCell("1", j, rowhead);j++;  // Is Variant 
				Util.getExcelStringCell("1", j, rowhead);j++;  // Max Cust Qty
				Util.getExcelStringCell("", j, rowhead);j++;   // Display Start Date 
				Util.getExcelStringCell("1", j, rowhead);j++;  // is for Live Game
			}
		} catch (Exception e) {
			e.printStackTrace();
	
		}
		return workbook;	
	}	 

}
