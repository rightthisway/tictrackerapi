package com.rtf.ecomerce.utils;

public class RTFEcommAppCache {

	public static String envfoldertype = "sa";
	public static String s3staticbucket = "saasadminassetz";
	public static String s3rewardsfolder = "rwd";
	public static String s3shopproductsfolder ="rtfshp";
	public static String cfstaticurl =  "http://d9kn0fqut3lki.cloudfront.net";
	public static String TMP_FOLDER= System.getProperty("java.io.tmpdir");
	public static String s3inventoryoductsfolder = "invty";
	public static String s3orddeliveredfolder = "orddel";

	public static void loadApplicationValues() {
		
		envfoldertype = "sa";
		s3staticbucket = "saasadminassetz";
		s3rewardsfolder = "rwd";
		cfstaticurl = "http://d9kn0fqut3lki.cloudfront.net";
		s3shopproductsfolder = "rtfshp";
		s3inventoryoductsfolder ="invty";
		s3orddeliveredfolder = "orddel";
		
		/*ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		envfoldertype = resourceBundle.getString("s3.env.foldertype");
		s3staticbucket = resourceBundle.getString("s3.staticassets.bucketurl");
		s3rewardsfolder = resourceBundle.getString("s3.rewards.folder");
		cfstaticurl = resourceBundle.getString("cf.staticassets.baserurl");
		s3shopproductsfolder = resourceBundle.getString("s3.shopproducts.folder");
		s3inventoryoductsfolder = resourceBundle.getString("s3.inventoryproducts.folder");*/
		
		System.out.println("******* SaaSAdminAppCache-START  APPLICATION CACHE **********************");
		System.out.println("APPLICATION CACHE : s3.env.foldertype : " + envfoldertype);
		System.out.println("APPLICATION CACHE : s3.staticassets.bucketurl : " + s3staticbucket);
		System.out.println("APPLICATION CACHE : s3.rewards.folder : " + s3rewardsfolder);
		System.out.println("APPLICATION CACHE : cf.staticassets.baserurl : " + cfstaticurl);
		System.out.println("APPLICATION CACHE : s3.shopproducts.folder : " + s3shopproductsfolder);
		
	
		TMP_FOLDER = System.getProperty("java.io.tmpdir");
		System.out.println("APPLICATION CACHE : TMP FOLDER  : " + TMP_FOLDER);
		
		System.out.println("******* SaaSAdminAppCache-END  APPLICATION CACHE ************************");
	}
}
