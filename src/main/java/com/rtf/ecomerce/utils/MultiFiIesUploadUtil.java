package com.rtf.ecomerce.utils;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.rtf.ecomerce.data.SellerProdItemsVariName;
import com.rtf.ecomerce.data.SellerProdItemsVariValue;
import com.rtw.tracker.aws.AWSFileService;
import com.rtw.tracker.aws.AwsS3Response;

public class MultiFiIesUploadUtil {

	public static Map<String, Map<String, String>> uploadMultipleFiles(HttpServletRequest request, String clId,
			Map<String, Map<String, String>> varoptmap) throws Exception {

		String TMP_DIR = RTFEcommAppCache.TMP_FOLDER;
		TMP_DIR = TMP_DIR + File.separator + clId + File.separator;
		System.out.println("******** TEMP DIRECTORY TOMCAT " + TMP_DIR);
		File fileSaveDir = new File(TMP_DIR);
		if (!fileSaveDir.exists()) {
			fileSaveDir.mkdirs();
		}
		System.out.println("******** Upload File Directory=" + fileSaveDir.getAbsolutePath());
		String origFileName = null;
		String optname_value = null;
		try {
			for (Part part : request.getParts()) {
				Map<String, String> optnameFileMap = FileUtil.getOptValueFileNameMap(part);
				if (optnameFileMap != null) {
						Map.Entry<String, String> entry = optnameFileMap.entrySet().iterator().next();
						optname_value = entry.getKey();
						origFileName = entry.getValue();
						String[] opt_value = optname_value.split("_");
						String variantName = opt_value[0];
						String optionvalue = opt_value[1];
	
						System.out.println(" optionvalue " + optionvalue + "= variant name=" + variantName);
						Map<String, String> initMap = varoptmap.get(variantName);
							if (initMap.containsKey(optionvalue)) {
								System.out.println(" optionvalue==   " + optionvalue + "  = variant name==  " + variantName + "origFileName ==  " + origFileName);
								
								
								String EXTENSION = FileUtil.getFileExtension(origFileName);
								String fileName = clId + "_" + System.currentTimeMillis() + "." + EXTENSION;
								part.write(TMP_DIR + File.separator + fileName);
								System.out.println("******* file uploaded path  " + TMP_DIR + File.separator + fileName);
								String s3bucket = RTFEcommAppCache.s3staticbucket;
								String s3moduleFolder = RTFEcommAppCache.s3inventoryoductsfolder;	
								String awsfolder= RTFEcommAppCache.envfoldertype + "/" + s3moduleFolder +"/" + clId ;
								File tmpfile= new File(TMP_DIR + File.separator + fileName);
								AwsS3Response awsRsponse = AWSFileService.upLoadFileToS3(s3bucket,fileName, awsfolder, tmpfile );
								System.out.println("S3 UPLOADED FILE " + awsRsponse.getFullFilePath());
								System.out.println("S3 UPLOADED STATUS " + awsRsponse.getStatus());
								String serverLinkPath = RTFEcommAppCache.cfstaticurl + "/" + awsfolder + "/"  + fileName;
								System.out.println("S3 UPLOADED FILE URL  " + serverLinkPath);
								initMap.put(optionvalue, serverLinkPath);
								if(awsRsponse.getStatus() != 1) throw new Exception("Error Uploading file " + origFileName + " to S3 . Error is " +awsRsponse.getDesc() );
								varoptmap.put(variantName, initMap);
							}
							
				} else {
					System.out.println("No File Name Skipping Upload");
				}				
			}
			return varoptmap;
		} catch (Exception ex) {
				ex.printStackTrace();
		}
		return null;
	}

	public static Map<String, String> uploadMultipleFiles1(HttpServletRequest request, String clId) {

		String TMP_DIR = RTFEcommAppCache.TMP_FOLDER;
		TMP_DIR = TMP_DIR + File.separator + clId + File.separator;
		System.out.println("******** TEMP DIRECTORY TOMCAT " + TMP_DIR);
		File fileSaveDir = new File(TMP_DIR);
		if (!fileSaveDir.exists()) {
			fileSaveDir.mkdirs();
		}
		System.out.println("******** Upload File Directory=" + fileSaveDir.getAbsolutePath());
		String origFileName = null;
		try {
			/*for (Part part : request.getParts()) {
				origFileName = FileUtil.getFileName(part);
				System.out.println("******** origFileName=" + origFileName);
			}*/
		} catch (Exception ex) {

		}

		String EXTENSION = FileUtil.getFileExtension(origFileName);
		String fileName = clId + "_" + System.currentTimeMillis() + "." + EXTENSION;
		try {
			/*for (Part part : request.getParts()) {
				part.write(TMP_DIR + File.separator + fileName);
				System.out.println("******* file uploaded path  " + TMP_DIR + File.separator + fileName);
			}*/
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}
	
	public static Map<String, Map<String, String>> uploadMultipleFilesOne(HttpServletRequest request,
			Map<String, Map<String, String>> varoptmap) throws Exception {

		String TMP_DIR = RTFEcommAppCache.TMP_FOLDER;
		String clId = "RTF";
		TMP_DIR = TMP_DIR + File.separator + clId + File.separator;
		System.out.println("******** TEMP DIRECTORY TOMCAT " + TMP_DIR);
		File fileSaveDir = new File(TMP_DIR);
		if (!fileSaveDir.exists()) {
			fileSaveDir.mkdirs();
		}
		System.out.println("******** Upload File Directory=" + fileSaveDir.getAbsolutePath());
		String origFileName = null;
		String optname_value = null;
		try {
			for (Part part : request.getParts()) {
				Map<String, String> optnameFileMap = FileUtil.getOptValueFileNameMap(part);
				if (optnameFileMap != null) {
						Map.Entry<String, String> entry = optnameFileMap.entrySet().iterator().next();
						optname_value = entry.getKey();
						origFileName = entry.getValue();
						String[] opt_value = optname_value.split("_");
						String variantName = opt_value[0];
						String optionvalue = opt_value[1];
	
						System.out.println(" optionvalue " + optionvalue + "= variant name=" + variantName);
						Map<String, String> initMap = varoptmap.get(variantName);
							if (initMap.containsKey(optionvalue)) {
								System.out.println(" optionvalue==   " + optionvalue + "  = variant name==  " + variantName + "origFileName ==  " + origFileName);
								
								
								String EXTENSION = FileUtil.getFileExtension(origFileName);
								String fileName = clId + "_" + System.currentTimeMillis() + "." + EXTENSION;
								part.write(TMP_DIR + File.separator + fileName);
								System.out.println("******* file uploaded path  " + TMP_DIR + File.separator + fileName);
								String s3bucket = RTFEcommAppCache.s3staticbucket;
								String s3moduleFolder = RTFEcommAppCache.s3inventoryoductsfolder;	
								String awsfolder= RTFEcommAppCache.envfoldertype + "/" + s3moduleFolder +"/" + clId ;
								File tmpfile= new File(TMP_DIR + File.separator + fileName);
								AwsS3Response awsRsponse = AWSFileService.upLoadFileToS3(s3bucket,fileName, awsfolder, tmpfile );
								System.out.println("S3 UPLOADED FILE " + awsRsponse.getFullFilePath());
								System.out.println("S3 UPLOADED STATUS " + awsRsponse.getStatus());
								String serverLinkPath = RTFEcommAppCache.cfstaticurl + "/" + awsfolder + "/"  + fileName;
								System.out.println("S3 UPLOADED FILE URL  " + serverLinkPath);
								initMap.put(optionvalue, serverLinkPath);
								if(awsRsponse.getStatus() != 1) throw new Exception("Error Uploading file " + origFileName + " to S3 . Error is " +awsRsponse.getDesc() );
								varoptmap.put(variantName, initMap);
							}
							
				} else {
					System.out.println("No File Name Skipping Upload");
				}				
			}
			return varoptmap;
		} catch (Exception ex) {
				ex.printStackTrace();
		}
		return null;
	}
	
	public static Map<String, Map<String, String>> uploadMultipleFilesTest(HttpServletRequest request,
			Map<String, Map<String, String>> varoptmap,List<SellerProdItemsVariName> varientNames) throws Exception {

		String TMP_DIR = RTFEcommAppCache.TMP_FOLDER;
		String clId = "RTF";
		TMP_DIR = TMP_DIR + File.separator + clId + File.separator;
		System.out.println("******** TEMP DIRECTORY TOMCAT " + TMP_DIR);
		File fileSaveDir = new File(TMP_DIR);
		if (!fileSaveDir.exists()) {
			fileSaveDir.mkdirs();
		}
		System.out.println("******** Upload File Directory=" + fileSaveDir.getAbsolutePath());
		String origFileName = null;
		
		try {
			MultipartFile file = null;
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			
			for (SellerProdItemsVariName variantNameObj : varientNames) {
				for (SellerProdItemsVariValue variantValueObj : variantNameObj.getValues()) {
					
					String key = "file_"+variantNameObj.getvOptName()+"_"+variantValueObj.getVarVal();
					//String key = "file"+"_"+variantValueObj.getVarVal();
					file = multipartRequest.getFile(key);
					if(file != null && !file.isEmpty()) {
						origFileName = file.getOriginalFilename();
						String EXTENSION = FileUtil.getFileExtension(origFileName);
						String fileName = clId + "_" + System.currentTimeMillis() + "." + EXTENSION;
						File tmpfile = ProductInventoryXLSUploadUtil.multipartToFile(file, fileName);
						
						//File tmpfile= new File(TMP_DIR + File.separator + fileName);
						
						//part.write(TMP_DIR + File.separator + fileName);
						System.out.println("******* file uploaded path  " + TMP_DIR + File.separator + fileName);
						String s3bucket = RTFEcommAppCache.s3staticbucket;
						String s3moduleFolder = RTFEcommAppCache.s3inventoryoductsfolder;	
						String awsfolder= RTFEcommAppCache.envfoldertype + "/" + s3moduleFolder +"/" + clId ;
						
						AwsS3Response awsRsponse = AWSFileService.upLoadFileToS3(s3bucket,fileName, awsfolder, tmpfile );
						System.out.println("S3 UPLOADED FILE " + awsRsponse.getFullFilePath());
						System.out.println("S3 UPLOADED STATUS " + awsRsponse.getStatus());
						String serverLinkPath = RTFEcommAppCache.cfstaticurl + "/" + awsfolder + "/"  + fileName;
						System.out.println("S3 UPLOADED FILE URL  " + serverLinkPath);
						if(awsRsponse.getStatus() != 1) throw new Exception("Error Uploading file " + origFileName + " to S3 . Error is " +awsRsponse.getDesc() );
						variantValueObj.setVarValImg(serverLinkPath);
					}
				}
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public static String deleteFileFromS3(String fileUrl) throws Exception {
		if(fileUrl == null || fileUrl.isEmpty()) {
			return null;
		}
		URL url = new URL(fileUrl);
		String fileName = FilenameUtils.getName(url.getPath());
        //System.out.println(FilenameUtils.getBaseName(url.getPath())); // -> file
        //System.out.println(FilenameUtils.getExtension(url.getPath())); // -> xml
        System.out.println(FilenameUtils.getName(url.getPath())); // -> file.xml
        
        String s3bucket = RTFEcommAppCache.s3staticbucket;
		String s3moduleFolder = RTFEcommAppCache.s3inventoryoductsfolder;	
		String awsfolder= RTFEcommAppCache.envfoldertype + "/" + s3moduleFolder +"/" + "RTF" ;
		
		AwsS3Response awsResponse = AWSFileService.deleteFilesFromS3(s3bucket, fileName, awsfolder);
		System.out.println(awsResponse);

		return null;
	}

}
