package com.rtf.ecomerce.utils;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Part;


public class FileUtil {
	
	 public static String getFileExtension(String f) {
		    String ext = "";
		    int i = f.lastIndexOf('.');
		    if (i > 0 &&  i < f.length() - 1) {
		      ext = f.substring(i + 1);
		    }
		    return ext;
		  }
	 
		public static  String getFileName(Part part) {
			String contentDisp = part.getHeader("content-disposition");
			System.out.println("content-disposition header= " + contentDisp);
			String[] tokens = contentDisp.split(";");
			for (String token : tokens) {
				if (token.trim().startsWith("filename")) {
					return token.substring(token.indexOf("=") + 2, token.length() - 1);
				}
			}
			return "";
		}
		public static  Map<String,String> getOptValueFileNameMap(Part part) {
			String contentDisp = part.getHeader("content-disposition");
			System.out.println("content-disposition header= " + contentDisp);
			Map <String,String> fileNameOptionvalueMap = new HashMap<String,String> ();
			String[] tokens = contentDisp.split(";");
			String fileName= null;
			String optvalue=null;
			for (String token : tokens) {
				if (token.trim().startsWith("file_")) {
					 fileName =  token.substring(token.indexOf("=") + 2, token.length() - 1);
				}
				if (token.trim().startsWith("name")) {
					optvalue =  token.substring(token.indexOf("=") + 2, token.length() - 1);
				}				
			}
			if(!GenUtil.isEmptyString(fileName)) {
				fileNameOptionvalueMap.put(optvalue, fileName);
				System.out.println(" Initialized Opt value and File Name Map " + fileNameOptionvalueMap);
			}else {
				fileNameOptionvalueMap = null;
			}
			return fileNameOptionvalueMap;
		}
		
		

}
