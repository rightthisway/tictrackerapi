package com.rtf.ecomerce.utils;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.rtf.ecomerce.pojo.FileUploadGenericDTO;
import com.rtw.tracker.aws.AWSFileService;
import com.rtw.tracker.aws.AwsS3Response;

public class ASWS3ImageUploaderUtil {
	
	public static FileUploadGenericDTO uploadFileToS3(String clId , HttpServletRequest request,FileUploadGenericDTO dto , String s3bucket,String s3modulefolder  ) {
		dto.setSts(0);
		String TMP_DIR = RTFEcommAppCache.TMP_FOLDER;
		TMP_DIR = TMP_DIR + File.separator + clId + File.separator;
		System.out.println("******** TEMP DIRECTORY TOMCAT " + TMP_DIR);
		File fileSaveDir = new File(TMP_DIR);
		if (!fileSaveDir.exists()) {
			fileSaveDir.mkdirs();
		}
		System.out.println("******** Upload File Directory=" + fileSaveDir.getAbsolutePath());
		String origFileName = null;
		try {
			for (Part  part : request.getParts()) {
				origFileName = FileUtil.getFileName(part);			
			}
		}catch(Exception ex) {
			dto.setSts(0);
			dto.setMsg(Messages.PRODUCT_IMAGE_FILE_UPLOAD_MANDATORY);			
			return dto;
		}
		if(EcommUtil.isNullOrEmpty(origFileName)) {
			dto.setSts(0);
			dto.setMsg(Messages.PRODUCT_IMAGE_FILE_UPLOAD_MANDATORY);
			return dto;
		}
		
		String EXTENSION = FileUtil.getFileExtension(origFileName);			
		String fileName = clId + "_" + System.currentTimeMillis() + "." + EXTENSION;
		try {
			for (Part part : request.getParts()) {				
				part.write(TMP_DIR + File.separator + fileName);
			}
		}catch(Exception ex) {
			dto.setSts(0);
			dto.setMsg(ex.getLocalizedMessage());
			return dto;
		}
		System.out.println("******* file uploaded path  " + TMP_DIR + File.separator + fileName);			
		
		//String s3bucket = RTFEcommAppCache.s3staticbucket;
		//String awsfolder= RTFEcommAppCache.envfoldertype + "/" + RTFEcommAppCache.s3inventoryoductsfolder +"/" + clId ;
		String awsfolder= RTFEcommAppCache.envfoldertype + "/" + s3modulefolder +"/" + clId ;
		
		System.out.println("******* S3 FOLDER PATH IS : " + awsfolder);
		File tmpfile= new File(TMP_DIR + File.separator + fileName);
		AwsS3Response awsRsponse = AWSFileService.upLoadFileToS3(s3bucket,fileName, awsfolder, tmpfile );
		
		if(awsRsponse == null) {
			dto.setSts(0);
			dto.setMsg(" Error Uploading to S3 ");
			return dto;
		}
		
		if(null != awsRsponse && awsRsponse.getStatus() != 1){
			dto.setSts(0);
			dto.setMsg(" Error Uploading to S3 " + awsRsponse.getDesc());
			return dto;			
		}						
		String serverLinkPath = RTFEcommAppCache.cfstaticurl + "/" + awsfolder + "/"  + fileName;
		System.out.println("**** CLOUD FRONT URL  - " + serverLinkPath);
		dto.setSts(1);
		dto.setAwsfinalUrl(serverLinkPath);
		return dto;
	}
	
	public  static File multipartToFile(MultipartFile multipart, String fileName) throws IllegalStateException, IOException {
	    File convFile = new File(System.getProperty("java.io.tmpdir")+"/"+fileName);
	    multipart.transferTo(convFile);
	    return convFile;
	}
	
	public static FileUploadGenericDTO uploadMultipartFileToS3(String clId , HttpServletRequest request,FileUploadGenericDTO dto , String s3bucket,String s3modulefolder  ) throws Exception {
		clId = "RTF";
		dto.setSts(0);
		String TMP_DIR = RTFEcommAppCache.TMP_FOLDER;
		TMP_DIR = TMP_DIR + File.separator + clId + File.separator;
		System.out.println("******** TEMP DIRECTORY TOMCAT " + TMP_DIR);
		File fileSaveDir = new File(TMP_DIR);
		if (!fileSaveDir.exists()) {
			fileSaveDir.mkdirs();
		}
		System.out.println("******** Upload File Directory=" + fileSaveDir.getAbsolutePath());
		String origFileName = null;
		String serverLinkPath = null;
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		//file = multipartRequest.getFile(key);
		Map<String,MultipartFile> multiparTFileMAp = multipartRequest.getFileMap();
		for (MultipartFile file : multiparTFileMAp.values()) {
			
			if(file != null && !file.isEmpty()) {
				origFileName = file.getOriginalFilename();
				String EXTENSION = FileUtil.getFileExtension(origFileName);
				String fileName = clId + "_" + System.currentTimeMillis() + "." + EXTENSION;
				File tmpfile = multipartToFile(file, fileName);
				//File tmpfile= new File(TMP_DIR + File.separator + fileName);
				//part.write(TMP_DIR + File.separator + fileName);
				System.out.println("******* file uploaded path  " + TMP_DIR + File.separator + fileName);
				//String s3bucket = RTFEcommAppCache.s3staticbucket;
				//String s3moduleFolder = RTFEcommAppCache.s3inventoryoductsfolder;	
				String awsfolder= RTFEcommAppCache.envfoldertype + "/" + s3modulefolder +"/" + clId ;
				
				AwsS3Response awsRsponse = AWSFileService.upLoadFileToS3(s3bucket,fileName, awsfolder, tmpfile );
				System.out.println("S3 UPLOADED FILE " + awsRsponse.getFullFilePath());
				System.out.println("S3 UPLOADED STATUS " + awsRsponse.getStatus());
				serverLinkPath = RTFEcommAppCache.cfstaticurl + "/" + awsfolder + "/"  + fileName;
				System.out.println("S3 UPLOADED FILE URL  " + serverLinkPath);
				if(awsRsponse == null || awsRsponse.getStatus() != 1) throw new Exception("Error Uploading file " + origFileName + " to S3 . Error is " +awsRsponse.getDesc() );
				
				dto.setSts(1);
				dto.setAwsfinalUrl(serverLinkPath);
				return dto;
			}
		}
		return dto;
		
	}
	
	
	
	

}
