package com.rtf.ecomerce.utils;

import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.rtf.ecomerce.dao.impl.EcommDAORegistry;
import com.rtf.ecomerce.data.CustomerOrder;
import com.rtf.ecomerce.data.OrderProducts;

public class EcommUtil {

	private static DecimalFormat df2 = new DecimalFormat(".##");
	
	public static boolean isNullOrEmpty(String s) {
		if (s == null || s.isEmpty() || isNullOrWhiteSpace(s)) {
			return true;
		}
		return false;
	}
	public static boolean isNullOrWhiteSpace(String s) {
		if (s == null || s.trim().isEmpty()) {
			return true;
		}
		return false;
	}
	
	public static Double getNormalRoundedValue(Double value) throws Exception {
		return Double.valueOf(df2.format(value));
	}
	
	public static String getRoundedValueString(Double value) throws Exception {
		return String.format( "%.2f", value);
	}
	
	public static Integer getRoundedValueIntger(Double value) throws Exception {
		value = Math.floor(value);
		return value.intValue();
	}
	
	public static Double getRoundedValue(Double value) throws Exception {
		df2.setRoundingMode(RoundingMode.DOWN);
		return Double.valueOf(df2.format(value));
	}
	
	public static Double getRoundedUpValue(Double value) throws Exception {
		df2.setRoundingMode(RoundingMode.UP);
		return Double.valueOf(df2.format(value));
	}
	
	
	
	
	public static List<String> getOfferList(String type, Double disc,Double price,String code){
		List<String> offerList = new ArrayList<String>();
		try {
			if(type.equalsIgnoreCase("PERCENTAGE")){
				if(code!=null && !code.isEmpty()){
					offerList.add("Use "+(disc.equals(Math.ceil(disc))?disc.intValue():disc)+"%  discount code("+code+").");
				}else{
					offerList.add("Use "+(disc.equals(Math.ceil(disc))?disc.intValue():disc)+"%  discount code earned from the game.");
				}
				
			}else{
				if(code!=null && !code.isEmpty()){
					offerList.add("Use $"+(disc.equals(Math.ceil(disc))?disc.intValue():disc)+" flat off discount code("+code+").");
				}else{
					offerList.add("Use $"+(disc.equals(Math.ceil(disc))?disc.intValue():disc)+" flat off discount code earned from the game.");
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return offerList;
	}
	
	public static String formatDateTpMMddyyyyHHmmAA(Date date){
		String result = null;
		try {
			DateFormat formatDateTime = new SimpleDateFormat("MM/dd/yyyy HH:mm aa");
			result = formatDateTime.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
	public static String formatDateTpMMddyyyy(Date date){
		String result = null;
		try {
			DateFormat formatDateTime = new SimpleDateFormat("MM/dd/yyyy");
			result = formatDateTime.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	public static String computeOrderProductStatus(List<OrderProducts> orderProducts) throws Exception {
		int activeCount=0,voidedCount=0,fulfiledCount=0,accepted=0,rejected=0;
		int totalCount = orderProducts.size();
		for (OrderProducts obj : orderProducts) {
			if(obj.getStatus() == null || obj.getStatus().equalsIgnoreCase("ACTIVE")) {
				activeCount++;
			} else if (obj.getStatus().equalsIgnoreCase("FULFILLED")) {
				fulfiledCount++;
			}else if (obj.getStatus().equalsIgnoreCase("VOIDED")) {
				voidedCount++;
			}else if (obj.getStatus().equalsIgnoreCase("ACCEPTED")) {
				accepted++;
			}else if (obj.getStatus().equalsIgnoreCase("REJECTED")) {
				rejected++;
			}
		}
		String orderStatus="ACTIVE";
		if(totalCount > 0) {
			if(activeCount > 0) {
				orderStatus="ACTIVE";
			} else if(accepted > 0) {
				orderStatus="ACCEPTED";
			} else if((fulfiledCount + voidedCount + rejected) == totalCount){
				if(fulfiledCount > 0) {
					orderStatus="FULFILLED";
				} else if(rejected > 0) {
					orderStatus="REJECTED";
				} else {
					orderStatus = "VOIDED";
				}
			} 
		}
		return orderStatus;
	}
	public static String calculateShippingStatus(Date date){
		String shipping = "Will be shipped b/w ";
		try {
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			cal.add(Calendar.DAY_OF_MONTH, 15);
			shipping = shipping + cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH);
			Integer day = cal.get(Calendar.DATE);
			shipping  = shipping + " "+day;
			shipping  = shipping +(day>1?"th":"st");
			
			shipping  = shipping + " to ";
			cal.add(Calendar.DAY_OF_MONTH, 15);
			day = cal.get(Calendar.DATE);
			shipping = shipping + cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH);
			shipping  = shipping + " "+day;
			shipping  = shipping +(day>1?"th":"st");
			return shipping;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
