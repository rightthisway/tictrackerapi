package com.rtf.ecomerce.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ResourceBundle;

public class GenUtil {

	public static String getExceptionAsString(Exception ex) {
		StringWriter sw = new StringWriter();
		ex.printStackTrace(new PrintWriter(sw));
		return sw.toString();
	}

	public static long currentTimeMillis() {
		return System.currentTimeMillis();
	}

	public static boolean isEmptyString(String string) {
		return string == null || string.length() == 0;
	}

	public static boolean isNullOrEmpty(String s) {
		if (s == null || s.isEmpty() || isNullOrWhiteSpace(s)) {
			return true;
		}
		return false;
	}

	public static boolean isNullOrWhiteSpace(String s) {
		if (s == null || s.trim().isEmpty()) {
			return true;
		}
		return false;
	}
	public static String getResourceKey(String key) {
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		//node =resourceBundle.getString("rtf.cassandra.db.host.ip");		
		return resourceBundle.getString(key);
	}
	public static Boolean getActiveBitFlag(String activeStr) {
		Boolean activeFlag= Boolean.FALSE;
		if(isNullOrEmpty(activeStr)) {
			return activeFlag;			
		}
		if("TRUE".equalsIgnoreCase(activeStr)) {
			return Boolean.TRUE;
		}
		if("ACTIVE".equalsIgnoreCase(activeStr)) {
			return Boolean.TRUE;
		}
		if("1".equals(activeStr)) {
			return Boolean.TRUE;
		}
		return activeFlag;		
	}
	public static java.sql.Timestamp getCurrentTimeStamp() {
		java.util.Date today = new java.util.Date();
		return new java.sql.Timestamp(today.getTime());
	}

	public static String getEmptyStringIfNull(String string) {
		if(isNullOrEmpty(string) || "null".equalsIgnoreCase(string)) {
			return "";			
		}		
		return string;
	}	

}
