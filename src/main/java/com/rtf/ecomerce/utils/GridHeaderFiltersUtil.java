package com.rtf.ecomerce.utils;


public class GridHeaderFiltersUtil {

	public static GridHeaderFilters getRtfSellerFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("fName")){
							filter.setfName(value);
						}else if(name.equalsIgnoreCase("lName")){
							filter.setlName(value);
						}else if(name.equalsIgnoreCase("compName")){
							filter.setCompanyName(value);
						}else if(name.equalsIgnoreCase("email")){
							filter.setEmail(value);
						}else if(name.equalsIgnoreCase("phone")){
							filter.setPhone(value);
						}else if(name.equalsIgnoreCase("addLine1")){
							filter.setAddressLine1(value);
						}else if(name.equalsIgnoreCase("addLine2")){
							filter.setAddressLine2(value);
						}else if(name.equalsIgnoreCase("city")){
							filter.setCity(value);
						}else if(name.equalsIgnoreCase("state")){
							filter.setState(value);
						}else if(name.equalsIgnoreCase("country")){
							filter.setCountry(value);
						}else if(name.equalsIgnoreCase("pincode")){
							filter.setZipCode(value);
						}else if(name.equalsIgnoreCase("updDate")){
							filter.setLastUpdatedDateStr(value);
						}else if(name.equalsIgnoreCase("updBy")){
							filter.setLastUpdatedBy(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						} 
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	public static GridHeaderFilters getRtfProductItemFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
					//	var loadProdColumns = ["prodId","pBrand", "pName","pDesc", "pRegMinPrice", "psMinPrice","maxCustQty","pExpDate",
					  //                         "status","updatedDate", "updatedBy","viewImg","editProd"];
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("prodId")){
							filter.setSpiId(Integer.valueOf(value));
						}else if(name.equalsIgnoreCase("pBrand")){
							filter.setpBrand(value);
						}else if(name.equalsIgnoreCase("pName")){
							filter.setPname(value);
						}else if(name.equalsIgnoreCase("pDesc")){
							filter.setpDesc(value);
						}else if(name.equalsIgnoreCase("pRegMinPrice")){
							filter.setpRegMinPrice(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("psMinPrice")){
							filter.setPsMinPrice(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("maxCustQty")){
							filter.setMaxCustQty(Integer.valueOf(value));
						}else if(name.equalsIgnoreCase("pExpDate")){
							filter.setDelvDateStr(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setState(value);
						}else if(name.equalsIgnoreCase("dispOrder")){
							filter.setCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("prodType")){
							filter.setOrderType(value);
						}else if(name.equalsIgnoreCase("isRwdPointProd")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("reqRwdPoint")){
							filter.setReqLPnts(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setLastUpdatedDateStr(value);
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setLastUpdatedBy(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						} 
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getRtfProductInventoryFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
					//	var loadProdColumns = ["prodId","pBrand", "pName","pDesc", "pRegMinPrice", "psMinPrice","maxCustQty","pExpDate",
					  //                         "status","updatedDate", "updatedBy","viewImg","editProd"];
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("spiId")){
							filter.setSpiId(Integer.valueOf(value));
						}else if(name.equalsIgnoreCase("sellerId")){
							filter.setSellerId(Integer.valueOf(value));
						}else if(name.equalsIgnoreCase("vOptNameValCom")){
							filter.setvOptNameValCom(value);
						}else if(name.equalsIgnoreCase("soldQty")){
							filter.setSoldQty(Integer.valueOf(value));
						}else if(name.equalsIgnoreCase("avlQty")){
							filter.setAvlQty(Integer.valueOf(value));
						}else if(name.equalsIgnoreCase("regPrice")){
							filter.setRegPrc(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("sellPrice")){
							filter.setSellPrc(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("reqLPnts")){
							filter.setReqLPnts(Integer.valueOf(value));
						}else if(name.equalsIgnoreCase("priceAftLPnts")){
							filter.setPriceAftLPnts(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("status")){
							filter.setState(value);
						}else if(name.equalsIgnoreCase("updDate")){
							filter.setLastUpdatedDateStr(value);
						}else if(name.equalsIgnoreCase("updBy")){
							filter.setLastUpdatedBy(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						} 
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	public static GridHeaderFilters getRtfProdOrdersFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						
						if(name.equalsIgnoreCase("orderId")){
							filter.setOrderId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("orderTotal")){
							filter.setNetTot(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("orderDate")){
							filter.setCrDateStr(value);
						}else if(name.equalsIgnoreCase("noOfItems")){
							filter.setNoOfItems(Integer.valueOf(value));
						}else if(name.equalsIgnoreCase("pPayType")){
							filter.setpPayType(value);
						}else if(name.equalsIgnoreCase("pPayAmt")){
							filter.setpPayAmt(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("sPayType")){
							filter.setsPayType(value);
						}else if(name.equalsIgnoreCase("sPayAmt")){
							filter.setsPayAmt(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("shCharges")){
							filter.setShCharges(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("tax")){
							filter.setTax(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("firstName")){
							filter.setCustFirstName(value);
						}else if(name.equalsIgnoreCase("lastName")){
							filter.setCustLastName(value);
						}else if(name.equalsIgnoreCase("email")){
							filter.setCustEmail(value);
						}else if(name.equalsIgnoreCase("phone")){
							filter.setCustPhone(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}
						else if(name.equalsIgnoreCase("shAddr1")){
							filter.setShAddr1(value);
						}else if(name.equalsIgnoreCase("shAddr2")){
							filter.setShAddr2(value);
						}else if(name.equalsIgnoreCase("shCity")){
							filter.setShCity(value);
						}else if(name.equalsIgnoreCase("shState")){
							filter.setShState(value);
						}else if(name.equalsIgnoreCase("shCountry")){
							filter.setShCountry(value);
						}else if(name.equalsIgnoreCase("shZip")){
							filter.setShZip(value);
						}else if(name.equalsIgnoreCase("shFName")){
							filter.setShFName(value);
						}else if(name.equalsIgnoreCase("shLName")){
							filter.setShLName(value);
						}else if(name.equalsIgnoreCase("shPhone")){
							filter.setShPhone(value);
						}else if(name.equalsIgnoreCase("shEmail")){
							filter.setShEmail(value);
						}
						
						else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						} 
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	
	public static GridHeaderFilters getRtfCouponCodeFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						
						/**
						 *    ccID
						      ,cou_code
						      ,cou_name
						      ,cou_desc
						      ,cou_discount
						      ,cou_status
						      ,lupd_date
						      ,lupd_by
						      ,discountype
						      ,cou_type
						      ,exp_date
						 */
						
						
						
						
						
						
						if(name.equalsIgnoreCase("fName")){
							filter.setfName(value);
						}else if(name.equalsIgnoreCase("lName")){
							filter.setlName(value);
						}else if(name.equalsIgnoreCase("compName")){
							filter.setCompanyName(value);
						}else if(name.equalsIgnoreCase("email")){
							filter.setEmail(value);
						}else if(name.equalsIgnoreCase("phone")){
							filter.setPhone(value);
						}else if(name.equalsIgnoreCase("addLine1")){
							filter.setAddressLine1(value);
						}else if(name.equalsIgnoreCase("addLine2")){
							filter.setAddressLine2(value);
						}else if(name.equalsIgnoreCase("city")){
							filter.setCity(value);
						}else if(name.equalsIgnoreCase("state")){
							filter.setState(value);
						}else if(name.equalsIgnoreCase("country")){
							filter.setCountry(value);
						}else if(name.equalsIgnoreCase("pincode")){
							filter.setZipCode(value);
						}else if(name.equalsIgnoreCase("updDate")){
							filter.setLastUpdatedDateStr(value);
						}else if(name.equalsIgnoreCase("updBy")){
							filter.setLastUpdatedBy(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						} 
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	
	
	

	private static boolean validateParamValues(String name,String value){
		if(name==null || name.isEmpty() || name.equalsIgnoreCase("undefined") ||
				value==null || value.isEmpty() || value.equalsIgnoreCase("undefined")){
			return false;
		}
		return true;	
	}
}



