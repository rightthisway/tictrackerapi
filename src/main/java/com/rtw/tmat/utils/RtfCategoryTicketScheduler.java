package com.rtw.tmat.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtw.tmat.dao.implementation.TMATDAORegistry;
import com.rtw.tmat.data.AutoPricingError;
import com.rtw.tmat.data.AutopricingProduct;
import com.rtw.tmat.data.Event;
import com.rtw.tmat.data.RewardthefanCategoryTicket;
import com.rtw.tmat.data.RewardthefanCatsExchangeEvent;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.RtfCatsTicket;
import com.rtw.tracker.datas.TicketStatus;
import com.rtw.tracker.mail.MailManager;
import com.rtw.tracker.utils.Util;


public class RtfCategoryTicketScheduler  extends QuartzJobBean implements StatefulJob {
	private static Logger log = LoggerFactory.getLogger(RtfCategoryTicketScheduler.class);
	
	 public static Date lastUpdateTime;
	 public static Date nextRunTime;
	 private static Boolean running=false;
	 private static Boolean stopped;
	 public static DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	 
	 public static MailManager mailManager;
		public static MailManager getMailManager() {
			return mailManager;
		}

		public void setMailManager(MailManager mailManager) {
			RTFApiTracker.mailManager = mailManager;
		}
		
	 public static void processRtfCatssProcessorTickets() throws Exception{
		Calendar cal =  Calendar.getInstance();
		Date now = cal.getTime();
		cal.add(Calendar.MINUTE,5);
		setNextRunTime(cal.getTime());
		if(isStopped() || isRunning()){
			System.out.println("Stopped");
			//return ;
		}
		setRunning(true);
		
		
		Date lastRunTime = getLastUpdateTime();
		Long minute = 0l;
		if(lastRunTime!=null){
			minute = (now.getTime()-lastRunTime.getTime())/(1000* 60); 
		}else{
			minute = 2880l; // 2 Days in minute
		}
		AutopricingProduct autopricingProduct = TMATDAORegistry.getAutopricingProductDAO().getAutopricingProductByName("RTF Cats");
		if(autopricingProduct==null){
			autopricingProduct = new AutopricingProduct();
			autopricingProduct.setName("RTF Cats");
			autopricingProduct.setStatus("ACTIVE");
			autopricingProduct.setStopped(false);
		}
		if(autopricingProduct.getStopped()){
			setStopped(true);
			setRunning(false);
			return;
		}
		
		setLastUpdateTime(now);
		autopricingProduct.setLastRunTime(now);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		/*ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String toAddress = resourceBundle.getString("emailNotificationTo");
		String ccAddress= resourceBundle.getString("emailNotificationCCTo");*/
		
		AutoPricingError error= null;
		List<AutoPricingError> errorList = new ArrayList<AutoPricingError>();
		boolean isErrorOccured = false;
//		Date jobStartTime = new Date();
		int tInsert=0,tUpdate=0,tremovet=0,tposCount=0;
		
		 try{
				
			 Map<Integer, Event> eventMap = new HashMap<Integer, Event>();
			 Map<Integer, Event> eventMapByExchangeEventId = new HashMap<Integer, Event>();
			 Collection<Event> eventList = null;
			 Collection<RewardthefanCatsExchangeEvent> rewardthefanCatsExchangeEvents = null;
			 
			 try {
				 
				 //Get all events that is updated in tmat since last run time. 
				 rewardthefanCatsExchangeEvents = TMATDAORegistry.getRewardthefanCatsExchangeEventDAO().getAllRewardthefanCatsExchangeEventsEligibleForUpdate(minute,autopricingProduct);
				 System.out.println("RtfCats Event Size : "+rewardthefanCatsExchangeEvents.size());
				 
				 eventList = TMATDAORegistry.getEventDAO().getAllActiveEvents();
				 for(Event event:eventList) {
					eventMap.put(event.getId(), event);
					eventMapByExchangeEventId.put(event.getAdmitoneId(), event);
				 }
				 System.out.println("RtfCats : TMAT event size : "+eventList.size());
				 
			 } catch (Exception e) {
				 isErrorOccured = true;
				 error = new AutoPricingError();
				 error.setProductId(autopricingProduct.getId());
				 error.setMessage("Error while Loading Events.");
				 error.setExample(""+e.fillInStackTrace());
				 error.setProcess("Event Loading.");
				 error.setEventId(0);
				 error.setTimeStamp(new Date());
				 errorList.add(error);
				 log.error("RtfCats 1 : Error while Loading Events.");
				 System.err.println("RtfCats 1 : Error while Loading Events.");
				 e.printStackTrace();
			 }
			 
			 
			 List<com.rtw.tracker.datas.Event> rtfEvents = new ArrayList<com.rtw.tracker.datas.Event>();
			 Map<Integer,com.rtw.tracker.datas.Event>rtfEventsMap = new HashMap<Integer, com.rtw.tracker.datas.Event>();
			
			 int eventSize = rewardthefanCatsExchangeEvents.size();
			 Integer i=0;
			 Integer commonExcludeHoursBeforeEvent = 0;
			 //Integer minimamExcludeEventDays = DAORegistry.getAutopricingSettingsDAO().getMinimumExcludeEventDaysByProductId(autopricingProduct.getId());

			for (RewardthefanCatsExchangeEvent exEvent : rewardthefanCatsExchangeEvents) {
				 
				/* if(isStopped()){
					 break;
				 }*/
				 i++;
				 Integer eventId = exEvent.getEventId();
//				 eventId = 1000117766; 
				 Event event = eventMap.get(eventId);
				 if(event==null ){
					 System.out.println(i+".TMAT Event Not Exisit :" + eventId);
//					 log.info(i+".TMAT Event Not Exisit :" + eventId);
					 continue;
				 }
				 
				 //Integer exBrokerId = exEvent.getRtfCatsBrokerId();
				 Date currentTime = new Date();
				 
				Date startDate = new Date();	
				long preProcessTime = 0,eventProcessTime=0,postProcessTime=0,posUpdateTime=0,tmatUpdateTime=0,totalTime=0,tmatSelectTime=0;
				System.out.println("RtfCats Even:" + i+"/"+eventSize+" .Event: " + event.getId()+ ":" + startDate);

				Date date = null;
				if(event.getLocalDateTime()!=null){
					date = event.getLocalDateTime();	
				}
				//if(event.getLocalDate()!=null){
				//	date = df.parse(df.format(event.getLocalDate()));	
				//}
				//now = df.parse(df.format(now));
				Integer excludeHoursBeforeEvent=null;//excludingEventDays = null,
				excludeHoursBeforeEvent = exEvent.getExcludeHoursBeforeEvent();
				if(excludeHoursBeforeEvent == null){
					excludeHoursBeforeEvent = commonExcludeHoursBeforeEvent ;
				}
				com.rtw.tracker.datas.Event rtfEvent = null;
				if(rtfEventsMap == null || rtfEventsMap.isEmpty()) {
					rtfEvents = DAORegistry.getEventDAO().getAllActiveEvents();
					if(rtfEvents != null) {
						for(com.rtw.tracker.datas.Event rtfEventObj : rtfEvents) {
							rtfEventsMap.put(rtfEventObj.getEventId(), rtfEventObj);
						}
					}
				}
				rtfEvent = rtfEventsMap.get(exEvent.getEventId());
				
				String message = null;
				if(date!=null && ((date.getTime()-now.getTime()) <= excludeHoursBeforeEvent * 60 * 60 * 1000)) {//2 days events
					message = "Event within exclude eventdays";
				} else if(rtfEvent == null) {
					message = "Event not Exist in RTF Events";
				} /*else if(event.getVenueCategoryId() == null) {
					message = "Event donot have venue category";
				}*/
				
				if(message != null) {
					try {
						try {
							DAORegistry.getRtfCatsTicketDAO().deleteAllActiveRtfCatsListingsByEventId(eventId);
							 
						 } catch (Exception e) {
							 e.printStackTrace();
							 isErrorOccured = true;
							 error = new AutoPricingError();
							 error.setProductId(autopricingProduct.getId());
							 error.setMessage("Event : "+eventId);
							 error.setExample("" + e.fillInStackTrace());
							 error.setProcess("Error while deleting "+message);
							 error.setEventId(eventId);
							 error.setTimeStamp(new Date());
							 errorList.add(error);
							 log.error("RtfCats 2 : Error while deleting zone listings of "+message+" : Event:" + eventId + "):Msg:" + e.fillInStackTrace());
							 System.err.println("RtfCats 2 : Error while deleting zone listings of "+message+" : Event:" + eventId + "):Msg:" + e.fillInStackTrace());
						 }
						 
						Integer count =  TMATDAORegistry.getRewardthefanCategoryTicketDAO().deleteAllActiveRtfCategoryticketsByEventId(exEvent.getId(), message);
						tremovet = tremovet + count;
						System.out.println("DELETING Event "+message+" : Event: " + event.getId() + ", Tix: ");
						
					} catch (Exception e) {
						isErrorOccured = true;
						error = new AutoPricingError();
						 error.setProductId(autopricingProduct.getId());
						 error.setMessage("Error while Deleting Event Listings for "+message+":"+eventId);
						 error.setExample(""+e.fillInStackTrace());
						 error.setProcess("Deleting Event Listings.");
						 error.setEventId(eventId);
						 error.setTimeStamp(new Date());
						 errorList.add(error);
						 log.error("RtfCats 7 : Error while Deleting Event Listings for Event within Few Days."+eventId);
						 System.err.println("RtfCats 7 : Error while Deleting Event Listings for Event within Few Days."+eventId);
						 e.printStackTrace();
					}
					continue;
				}
				
				preProcessTime = new Date().getTime()-startDate.getTime();
				// If event is valid event add , update or delete event tickets based on latest tmat tickets.
				Map<String,RewardthefanCategoryTicket> catTixMap = new HashMap<String, RewardthefanCategoryTicket>();
				Map<String,RewardthefanCategoryTicket> catTixFromDB = new HashMap<String, RewardthefanCategoryTicket>();
				List<RewardthefanCategoryTicket> removeTixList = new ArrayList<RewardthefanCategoryTicket>();
				try {
					
					 List<RewardthefanCategoryTicket> catTixList = CategoryGroupManager.computeCategoryTickets(event, exEvent);
					 for (RewardthefanCategoryTicket catTixObj : catTixList) { // Add generated tickets in map
						RewardthefanCategoryTicket catTix = (RewardthefanCategoryTicket) catTixObj;

						String key = catTix.getQuantity()+"";
						catTixMap.put(key, catTix);
					 }
					 eventProcessTime = new Date().getTime()-(startDate.getTime()+preProcessTime);
					 List<RewardthefanCategoryTicket> miniTickets = TMATDAORegistry.getRewardthefanCategoryTicketDAO().getAllActiveRtfCategoryTicketsByEventId(event.getId());
					 for (RewardthefanCategoryTicket dbCatTix : miniTickets) {// Add Existing tickets in map.]
						 String key = dbCatTix.getQuantity()+"";
						 catTixFromDB.put(key, dbCatTix);
					 }
					 
				} catch (Exception e) {
					isErrorOccured = true;
					error = new AutoPricingError();
					 error.setProductId(autopricingProduct.getId());
					 error.setMessage("Error while Computing Category Tickets."+eventId);
					 error.setExample(""+e.fillInStackTrace());
					 error.setProcess("Category Ticket Computation.");
					 error.setEventId(eventId);
					 error.setTimeStamp(new Date());
					 errorList.add(error);
					 log.error("RtfCats 8 :Error while Computing Category Tickets."+eventId);
					 System.err.println("RtfCats 8 : Error while Computing Category Tickets."+eventId);
					 e.printStackTrace();
					 
					 continue;
				}
				
				List<RewardthefanCategoryTicket> newTixList = new ArrayList<RewardthefanCategoryTicket>();
				//Map<String,RtfCategoryTicket> exFinalCatTixMap = new HashMap<String, RtfCategoryTicket>();
				 try {
					 String priceHistory="";
					 String ticketIdHistory= "";
					 String baseTicketOneHistory= "";
					 String baseTicketTwoHistory="";
					 String baseTicketThreeHistory="";
					 
					 String ticketHDateStr = dateTimeFormat.format(new Date());
					String priceHDateStr = dateTimeFormat.format(new Date());
					List<String> keys = new ArrayList<String>(catTixFromDB.keySet());
					Integer baseTicket1,baseTicket2,baseTicket3;
					Boolean tempTicketIdFlag = false,isPriceChangeFlag = false;
					
					for(String key:keys) {
						RewardthefanCategoryTicket dbTix =catTixFromDB.remove(key);
						RewardthefanCategoryTicket tix = catTixMap.remove(key);
						isPriceChangeFlag = false;

						if(tix != null) { // if generated tickets is already existing..
							if(!dbTix.getActualPrice().equals(tix.getActualPrice())) {
								isPriceChangeFlag = true;
								priceHistory = priceHDateStr+"-"+tix.getActualPrice();
								if(dbTix.getPriceHistory() != null) {
									priceHistory = dbTix.getPriceHistory() +","+priceHistory;
									if(priceHistory.length()>=500) {
										priceHistory = priceHistory.substring(priceHistory.indexOf(",")+1);	
									}
								}
								dbTix.setPriceHistory(priceHistory);
								dbTix.setIsEdited(true);
								
							} else if(!dbTix.getTicketId().equals(tix.getTicketId()) ||
									!dbTix.getRtfPrice().equals(tix.getRtfPrice()) || 
									!dbTix.getTmatPrice().equals(tix.getTmatPrice())) { 
								
								dbTix.setIsEdited(true);
								
							} else {
								baseTicket1 = 0;
								baseTicket2 = 0;
								
								if(dbTix.getBaseTicketOne() != null) {
									baseTicket1 = dbTix.getBaseTicketOne();
								}
								if(dbTix.getBaseTicketTwo() != null) {
									baseTicket2 = dbTix.getBaseTicketTwo();
								}
								if(!tix.getBaseTicketOne().equals(baseTicket1) || !tix.getBaseTicketTwo().equals(baseTicket2)) {
									dbTix.setIsEdited(true);
									
								}
							}
							
							//add the pricehistory of basetickets prices here for all tickets
							
							tempTicketIdFlag = false;
							if(isPriceChangeFlag || !dbTix.getTicketId().equals(tix.getTicketId())) {
								ticketIdHistory = ticketHDateStr +"/"+ tix.getTicketId() +"/"+ tix.getPurPrice();
								
								if(dbTix.getTicketIdHistory() != null) {
									ticketIdHistory = dbTix.getTicketIdHistory()+","+ ticketIdHistory;
									if(ticketIdHistory.length()>= 500) {
										ticketIdHistory = ticketIdHistory.substring(ticketIdHistory.indexOf(",")+1);	
									}
								}
								dbTix.setTicketIdHistory(ticketIdHistory);
								dbTix.setIsEdited(true);
							} 
							
							// For Base Ticket 1
							tempTicketIdFlag = false;
							if (dbTix.getBaseTicketOne() != null && dbTix.getBaseTicketOne() != 0) {
								if(isPriceChangeFlag || !dbTix.getBaseTicketOne().equals(tix.getBaseTicketOne())) {
									tempTicketIdFlag = true;
								}
							} else if(tix.getBaseTicketOne() != null && tix.getBaseTicketOne() != 0) {
								tempTicketIdFlag = true;
							}
							
							if(tempTicketIdFlag) {
								baseTicketOneHistory = ticketHDateStr +"/"+ tix.getBaseTicketOne() +"/"+ tix.getBaseTicketOnePurPrice();
								
								if(dbTix.getBaseTicketOneHistory() != null) {
									baseTicketOneHistory = dbTix.getBaseTicketOneHistory()+","+ baseTicketOneHistory;
															
									if(baseTicketOneHistory.length()>= 500) {
										baseTicketOneHistory = baseTicketOneHistory.substring(baseTicketOneHistory.indexOf(",")+1);	
									}
								}
								dbTix.setBaseTicketOneHistory(baseTicketOneHistory);
								dbTix.setIsEdited(true);
							}
							
							// For Base Ticket 2
							tempTicketIdFlag = false;
							if (dbTix.getBaseTicketTwo() != null && dbTix.getBaseTicketTwo() != 0) {
								if(isPriceChangeFlag || !dbTix.getBaseTicketTwo().equals(tix.getBaseTicketTwo())){
									tempTicketIdFlag = true;
								}
							} else if(tix.getBaseTicketTwo() != null && tix.getBaseTicketTwo() != 0) {
								tempTicketIdFlag = true;
							}
							
							if(tempTicketIdFlag) {
								baseTicketTwoHistory = ticketHDateStr +"/"+ tix.getBaseTicketTwo() +"/"+ tix.getBaseTicketTwoPurPrice();
								if(dbTix.getBaseTicketTwoHistory() != null) {
									baseTicketTwoHistory = dbTix.getBaseTicketTwoHistory()+","+ baseTicketTwoHistory;
															
									if(baseTicketTwoHistory.length()>= 500) {
										baseTicketTwoHistory = baseTicketTwoHistory.substring(baseTicketTwoHistory.indexOf(",")+1);	
									}
								}
								dbTix.setBaseTicketTwoHistory(baseTicketTwoHistory);
								dbTix.setIsEdited(true);
							}
							
							if(dbTix.getRtfCatsId() == null ) { // Add tickets as new tickets if it is not exist in POS (i.e. tn group id is not present)
								dbTix.setIsEdited(true);
								dbTix.setRtfCatsId(tix.getRtfCatsId());
								
							}
							
							if(dbTix.getIsEdited()) {
								dbTix.setBaseTicketOne(tix.getBaseTicketOne());
								dbTix.setBaseTicketTwo(tix.getBaseTicketTwo());
								dbTix.setTicketId(tix.getTicketId());
								dbTix.setItemId(tix.getItemId());
								dbTix.setActualPrice(tix.getActualPrice());
								dbTix.setRtfPrice(tix.getRtfPrice());
								dbTix.setLastUpdated(currentTime);
								dbTix.setTmatPrice(tix.getTmatPrice());
								
								newTixList.add(dbTix);
							}
								
						} else { // else delete existing ticket as it does not exist in newly generated tickets..
							
							dbTix.setStatus("DELETED");
							dbTix.setLastUpdated(currentTime);
							dbTix.setReason("Ticket not exist in TMAT");
							//exFinalCatTixMap.put(key, dbTix);
							removeTixList.add(dbTix);
						}
					}
				
					for(String key:catTixMap.keySet()) {  // Add remaining newly generated tickets to db.
						RewardthefanCategoryTicket newTix = catTixMap.get(key);
						priceHistory = priceHDateStr+"-"+newTix.getActualPrice();
						newTix.setPriceHistory(priceHistory);
						
						ticketIdHistory = ticketHDateStr +"/"+ newTix.getTicketId() +"/"+ newTix.getPurPrice();
						newTix.setTicketIdHistory(ticketIdHistory);
						
						if (newTix.getBaseTicketOne() != 0) {
							baseTicketOneHistory = ticketHDateStr +"/"+ newTix.getBaseTicketOne() +"/"+ newTix.getBaseTicketOnePurPrice();
							newTix.setBaseTicketOneHistory(baseTicketOneHistory);
						}
						
						// For Base Ticket 2
						if (newTix.getBaseTicketTwo() != 0) {
							baseTicketTwoHistory = ticketHDateStr +"/"+ newTix.getBaseTicketTwo() +"/"+ newTix.getBaseTicketTwoPurPrice();
							newTix.setBaseTicketTwoHistory(baseTicketTwoHistory);
						}
						
						newTix.setLastUpdated(currentTime);
						newTix.setCreatedDate(currentTime);
						
						//exFinalCatTixMap.put(key, newTix);
						newTixList.add(newTix);
					}
					postProcessTime = new Date().getTime()-(startDate.getTime()+preProcessTime+eventProcessTime);
					
					for (RewardthefanCategoryTicket rewardthefanCategoryTicket : newTixList) {
						
						try {
							if(rewardthefanCategoryTicket.getRtfCatsId() == null) {
								RtfCatsTicket rtfCats = new RtfCatsTicket(rewardthefanCategoryTicket);
								rtfCats.setStatus(TicketStatus.ACTIVE);
								rtfCats.setBroadcast(Boolean.TRUE);
								rtfCats.setCreatedDate(new Date());
								rtfCats.setLastUpdatedDate(new Date());
								DAORegistry.getRtfCatsTicketDAO().saveOrUpdate(rtfCats);
								rewardthefanCategoryTicket.setRtfCatsId(rtfCats.getId());
								tInsert++;
							} else {
								DAORegistry.getRtfCatsTicketDAO().updateRtfCatsTicketByrtfCatsId(rewardthefanCategoryTicket);
								tUpdate++;
							}
						} catch(Exception e) {
							e.printStackTrace();
							isErrorOccured = true;
							error = new AutoPricingError();
							error.setProductId(autopricingProduct.getId());
							error.setMessage("Zone tickets Update zoneTicketGroupId:"+rewardthefanCategoryTicket.getRtfCatsId()); 
							error.setExample("" + e.fillInStackTrace());
							error.setProcess("Error While zones Update.");
							error.setEventId(eventId);
							error.setTimeStamp(new Date());
							errorList.add(error);
							log.error("RtfCats 13 : Error While zones Update :"+"Event:" + eventId+ ":" +"Id:(" + rewardthefanCategoryTicket.getQuantity() +"):Msg:" + e.fillInStackTrace());
							System.out.println("RtfCats 13 : Error While zones Update :"+"Event:" + eventId+ ":" +"Id:(" + rewardthefanCategoryTicket.getQuantity() +"):Msg:" + e.fillInStackTrace());
						}
					}
					
					try {
						DAORegistry.getRtfCatsTicketDAO().deleteRtfCatsTicketByrtfCats(new ArrayList<RewardthefanCategoryTicket>(removeTixList));
						if(removeTixList != null) {
							tremovet = tremovet + removeTixList.size();
						}
					} catch(Exception e) {
						e.printStackTrace();
						isErrorOccured = true;
						error = new AutoPricingError();
						 error.setProductId(autopricingProduct.getId());
						 error.setMessage("While deleting zoneticketgroups event :"+eventId);
						error.setExample("" + e.fillInStackTrace());
						error.setProcess("Error While zones delete.");
						error.setEventId(eventId);
						error.setTimeStamp(new Date());
						errorList.add(error);
						log.error("RtfCats 13 : Error While zones Update :"+"Event:" + eventId + " :Msg:" + e.fillInStackTrace());
						System.out.println("RtfCats 13 : Error While zones Update :"+"Event:" + eventId + " :Msg:" + e.fillInStackTrace());
					}
					posUpdateTime = new Date().getTime()-(startDate.getTime()+preProcessTime+eventProcessTime+postProcessTime);
					
					 TMATDAORegistry.getRewardthefanCategoryTicketDAO().saveOrUpdateAll(newTixList);
					 TMATDAORegistry.getRewardthefanCategoryTicketDAO().updateAll(removeTixList);
				
					 tmatUpdateTime = new Date().getTime()-(startDate.getTime()+preProcessTime+eventProcessTime+postProcessTime+posUpdateTime);
					 totalTime = new Date().getTime()-startDate.getTime();
					 System.out.println("RtfCats  : Total...: Insert :"+tInsert+" Updat : "+tUpdate+" : tremovet :"+tremovet+" : New PO Count : "+tposCount);
					 //System.out.println("RtfCats  : totalTime : "+totalTime+"prePros :"+preProcessTime+" EvenPos : "+eventProcessTime+" : postPros :"+postProcessTime+" : posUpd : "+posUpdateTime+" : tmatUpd : "+tmatUpdateTime);
					 
					 
				 } catch (Exception e) {
					 isErrorOccured = true;
					 error = new AutoPricingError();
					 error.setProductId(autopricingProduct.getId());
					 error.setMessage("Error while Updating TMAT Larry Ticket."+eventId);
					 error.setExample(""+e.fillInStackTrace());
					 error.setProcess("Larry Ticket Updation..");
					 error.setEventId(eventId);
					 error.setTimeStamp(new Date());
					 errorList.add(error);
					 log.error("RtfCats 15 : Error while Updating TMAT Larry Ticket."+eventId);
					 System.out.println("RtfCats 15 : Error while Updating TMAT Larry Ticket."+eventId);
					 e.printStackTrace();
				}
			}
			
			TMATDAORegistry.getAutopricingProductDAO().saveOrUpdate(autopricingProduct);
			System.out.println("RtfCats Final...: Insert :"+tInsert+" Updat : "+tUpdate+" : tremovet :"+tremovet+" : New PO Count : "+tposCount);
			 
			running = false; 
		 }catch(Exception e){
			 running = false;
			 isErrorOccured = true;
			 error = new AutoPricingError();
			 error.setProductId(autopricingProduct.getId());
			 error.setMessage("Error while Loading Common Properties.");
			 error.setExample(""+e.fillInStackTrace());
			 error.setProcess("Loading common Properties..");
			 error.setEventId(0);
			 error.setTimeStamp(new Date());
			 errorList.add(error);
			 log.error("RtfCats 16 :Error while Loading Common Properties.");
			 System.out.println("RtfCats 16 : Error while Loading Common Properties.");
			 e.printStackTrace();
		 }
		 
		 String subject,fileName;
		 Map<String, Object> map;
			
		 TMATDAORegistry.getAutoPricingErrorDAO().saveAll(errorList);
		 log.info("CT error size......"+errorList.size());
		 
			if(isErrorOccured){
				map = new HashMap<String, Object>();
				map.put("error", error);
				map.put("errors", errorList);
				try {
					
					subject = "RTF Cats Scheduler job failed :";
					fileName = "templates/rtf-cats-job-failure-message.txt";
					String emailTo = "AODev@rightthisway.com";
					//String emailCC = "AODev@rightthisway.com";
					//subject = subject + dateTimeFormat.format(jobStartTime)+" to "+dateTimeFormat.format(new Date());
					
					//EmailManager.sendEmail(toAddress, ccAddress, subject, fileName, map);
					
					mailManager.sendMailUsingGMAILSMTP("smtp.gmail.com",465,true,"rightthiswayllc@gmail.com","ticket#1441","infortw10018@gmail.com",null,emailTo,null,null, 
							subject,		fileName,map, "text/html", null,null,null);
					
				} catch (Exception e) {
					log.error("RtfCats 17 : Error while Inserting Error Listings in TMAT.");
					 System.out.println("RtfCats 17 :Error while Inserting Error Listings in TMAT.");
					e.printStackTrace();
				}
			}
	 }
	 
	 	 
	 @Override
		protected void executeInternal(JobExecutionContext context)
				throws JobExecutionException {
//			if(!running){
//				running=true;
		 		if(Util.isContestRunning){
		 			System.out.println("RETURN");
		 			return;
		 		}
		 		System.out.println("RUNNNNN");
				System.out.println("RTF Cats Job Called.." + new Date() + ": " + running);
				log.info("RTF Cats Job Called.." + new Date() + ": " + running);
				try{
					
					processRtfCatssProcessorTickets();
					System.out.println("RTF Cats Scheduler Job finished @ " + new Date());
				}catch(Exception e){
					e.printStackTrace();
				}
//				running = false;
//			}
	}


	public static Boolean isStopped() {
		if(stopped==null){
			stopped =  false;
		}
		return stopped;
	}


	public static void setStopped(Boolean stopped) {
		RtfCategoryTicketScheduler.stopped = stopped;
	}


	public static Boolean isRunning() {
		return running;
	}


	public static void setRunning(Boolean running) {
		RtfCategoryTicketScheduler.running = running;
	}


	public static Date getLastUpdateTime() {
		if(lastUpdateTime==null){
			lastUpdateTime = TMATDAORegistry.getAutopricingProductDAO().getAutopricingProductByName("RTF Cats").getLastRunTime();
		}
		return lastUpdateTime;
	}


	public static void setLastUpdateTime(Date lastUpdateTime) {
		RtfCategoryTicketScheduler.lastUpdateTime = lastUpdateTime;
	}


	public static Date getNextRunTime() {
		return nextRunTime;
	}


	public static void setNextRunTime(Date nextRunTime) {
		RtfCategoryTicketScheduler.nextRunTime = nextRunTime;
	}

}