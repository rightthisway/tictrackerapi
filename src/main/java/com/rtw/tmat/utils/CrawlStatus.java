package com.rtw.tmat.utils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class CrawlStatus implements Serializable{
	private List<LocalDestination> destination;
	private List<String> postBackUrls;
	private String eventId;
	private Integer totalCrawl;
	private Integer executedCrawl;
	private Date lastTimeCrawl;
	
	public String getEventId() {
		return eventId;
	}
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	public Integer getTotalCrawl() {
		return totalCrawl;
	}
	public void setTotalCrawl(Integer totalCrawl) {
		this.totalCrawl = totalCrawl;
	}
	public Integer getExecutedCrawl() {
		return executedCrawl;
	}
	public void setExecutedCrawl(Integer executedCrawl) {
		this.executedCrawl = executedCrawl;
	}
	public Date getLastTimeCrawl() {
		return lastTimeCrawl;
	}
	public void setLastTimeCrawl(Date lastTimeCrawl) {
		this.lastTimeCrawl = lastTimeCrawl;
	}
	public List<LocalDestination> getDestination() {
		return destination;
	}
	public void setDestination(List<LocalDestination> destination) {
		this.destination = destination;
	}
	public List<String> getPostBackUrls() {
		return postBackUrls;
	}
	public void setPostBackUrls(List<String> postBackUrls) {
		this.postBackUrls = postBackUrls;
	}
	
}
