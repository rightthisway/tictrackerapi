package com.rtw.tmat.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtw.tmat.dao.implementation.TMATDAORegistry;
import com.rtw.tmat.data.Event;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.SoldTicketDetail;
import com.rtw.tracker.utils.Util;



public class RTWOpenOrdersAutomaticCrawlUpdater extends QuartzJobBean implements StatefulJob{

	public static final int DEFAULT_TIMEOUT = 30000;
	public static Date lastRunTime = new Date();
	public static DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
	private static SharedProperty sharedProperty;
	
	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}

	public static void setSharedProperty(SharedProperty sharedProperty) {
		RTWOpenOrdersAutomaticCrawlUpdater.sharedProperty = sharedProperty;
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		if(Util.isContestRunning){
			System.out.println("RETURN");
			return;
		}
		System.out.println("RUNNNNN");
		System.out.println("RTWOOCU : RTW Open Order Automcatic Crawl Updater: Uploader Job Started....."+new Date());
		try{
			getRTWOpenOrdersCrawlUpdater();
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("RTWOOCU : RTW Open Order Automcatic Crawl Updater: Uploader Job Finished....."+new Date());
	}
	
	public void getRTWOpenOrdersCrawlUpdater() throws Exception {
		
		Collection<SoldTicketDetail> ticketDetail =  new ArrayList<SoldTicketDetail>();
		try{
			 ticketDetail = DAORegistry.getSoldTicketDetailDAO().getAllSoldUnfilledTicketDetails();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		if(null != ticketDetail && !ticketDetail.isEmpty()){
			System.out.println("RTWCU : Total No Of open Orders : "+ticketDetail.size());
			String eventIDs = "";
			Set<Integer> eventIdSet = new HashSet<Integer>(); 
			int count =0;
			for (SoldTicketDetail soldTicketDetail : ticketDetail) {
				Event event = TMATDAORegistry.getEventDAO().getEventById((soldTicketDetail.getTmatEventId()));
				if(null == event){
					continue;
				}
				if(!eventIdSet.add(event.getId())) {
					continue;
				}
				eventIDs = eventIDs +","+event.getId();
				count++;
				
				if(count%100 == 0) {
					eventIDs= eventIDs.substring(1);
					forceCrawlEventAndSendMail(eventIDs);
					eventIDs = "";
				}
			}
			if(!eventIDs.isEmpty()) {
				eventIDs= eventIDs.substring(1);
				forceCrawlEventAndSendMail(eventIDs);
			}
			System.out.println("RTWCU : RTW CRAWL UPDATER BEGINS-"+new Date());
			/*CrawlerDwr crawlerDwr = new CrawlerDwr();
			crawlerDwr.rtwOpenOrderforceCrawlerByEvent(eventIDs);*/
			System.out.println("RTWCU : RTW CRAWL UPDATER ENDS-"+new Date());
		}
	}
	
	// to force crawl an event and send fullfillment details for zonetickets order.
	public static void forceCrawlEventAndSendMail(String eventIds) throws Exception {
		try{
			Date now = new Date();
			String url = sharedProperty.getBrowseUrl()+"WSForceEvents";		
			HttpClient hc = new DefaultHttpClient();
			HttpPost hp = new HttpPost(url);
			NameValuePair nameValuePair = new BasicNameValuePair("eventIds", "" + eventIds);
			//NameValuePair nameValuePair1 = new BasicNameValuePair("postBackUrl", null);
			NameValuePair nameValuePair2 = new BasicNameValuePair("priority", "high");
			List<NameValuePair> parameters = new ArrayList<NameValuePair>();
			parameters.add(nameValuePair);
			//parameters.add(nameValuePair1);
			parameters.add(nameValuePair2);
			UrlEncodedFormEntity entity = new UrlEncodedFormEntity(parameters);
			hp.setEntity(entity);
			HttpResponse res = hc.execute(hp);
			String content = EntityUtils.toString(res.getEntity());
			
			Thread.sleep(5000);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}
