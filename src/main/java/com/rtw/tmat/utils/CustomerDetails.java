package com.rtw.tmat.utils;

import java.util.List;


public class CustomerDetails {
	
	private Integer status;
	private Error error; 
	private String customerProfilePicWebView;
	private String message;
	private List<CustomerCardInfo> cardList;
	
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public String getCustomerProfilePicWebView() {
		return customerProfilePicWebView;
	}
	public void setCustomerProfilePicWebView(String customerProfilePicWebView) {
		this.customerProfilePicWebView = customerProfilePicWebView;
	}
	public List<CustomerCardInfo> getCardList() {
		return cardList;
	}
	public void setCardList(List<CustomerCardInfo> cardList) {
		this.cardList = cardList;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
		
}
