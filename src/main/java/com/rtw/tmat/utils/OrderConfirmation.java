package com.rtw.tmat.utils;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("OrderConfirmation")
public class OrderConfirmation {

	private Integer orderId;
	private Integer status;
	private Boolean redirectTicketListing;
	private Error error; 
	private String message;	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Boolean getRedirectTicketListing() {
		if(null == redirectTicketListing){
			redirectTicketListing = false;
		}
		return redirectTicketListing;
	}
	public void setRedirectTicketListing(Boolean redirectTicketListing) {
		this.redirectTicketListing = redirectTicketListing;
	}
	
	
}
