package com.rtw.tmat.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.InitializingBean;

import com.rtw.tmat.dao.implementation.TMATDAORegistry;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.CrownJewelLeagues;
import com.rtw.tracker.datas.CrownJewelTeamZones;
import com.rtw.tracker.datas.Event;

public class CrownJewelTeamZonesScheduler extends TimerTask implements InitializingBean{

	@Override
	public void afterPropertiesSet() throws Exception {
		TimerTask timerTask = new CrownJewelTeamZonesScheduler();
		Calendar today = Calendar.getInstance();
		today.set(Calendar.HOUR, 23);
		today.set(Calendar.MINUTE, 59);
		today.set(Calendar.SECOND, 59);
		
		
		Timer timer = new Timer();
		//timer.scheduleAtFixedRate(timerTask,new Date(), 15*60*1000);
		timer.schedule(timerTask, today.getTime());
		
	}

	@Override
	public void run() {
		try {
			Date today = new Date();
			List<CrownJewelLeagues> leagues = DAORegistry.getCrownJewelLeaguesDAO().getAllActiveLeagues();
			List<CrownJewelTeamZones> activeZones = null;
			List<CrownJewelTeamZones> soldTicketsZones = null;
			List<CrownJewelTeamZones> zonesToDelete = null;
			for(CrownJewelLeagues league : leagues){
				zonesToDelete = new ArrayList<CrownJewelTeamZones>();
				if(league.getEventId()==null){
					continue;
				}
				Event event = DAORegistry.getEventDAO().get(league.getEventId());
				List<String> zoneList = TMATDAORegistry.getCategoryDAO().getCategorySymbolsByVenueCategoryId(event.getVenueCategoryId());
				
				//Get all sold tix zones of league and create map.
				soldTicketsZones = DAORegistry.getQueryManagerDAO().getCrownJewelTeamZonesByLeagueId(league.getId()); //Sold tickets zones
				Map<String,CrownJewelTeamZones> soldTixZonesMap = new HashMap<String, CrownJewelTeamZones>();
				for(CrownJewelTeamZones zoneZoldTix : soldTicketsZones) {
					String key = zoneZoldTix.getZone().toUpperCase();
					soldTixZonesMap.put(key, zoneZoldTix);
				}
				
				//Get All active zones of league and create map
				activeZones = DAORegistry.getCrownJewelTeamZonesDAO().getAllActiveTeamZonesByLeagueId(league.getId()); // all Active zones
				Map<String,CrownJewelTeamZones> activeZonesMap = new HashMap<String, CrownJewelTeamZones>();
				for(CrownJewelTeamZones cjTeamZones : activeZones) {
					String key = cjTeamZones.getZone().toUpperCase();
					activeZonesMap.put(key, cjTeamZones);
				}
				
				for(String zone : zoneList) {
					String key = zone.toUpperCase();
					activeZonesMap.remove(key);
				}
				
				for(CrownJewelTeamZones rZone : activeZonesMap.values()){
					String key = rZone.getZone().toUpperCase();
					if(soldTixZonesMap.get(key) == null){
						rZone.setStatus("DELETED");
						rZone.setLastUpdated(today);
						rZone.setLastUpdateddBy("Auto");
						zonesToDelete.add(rZone);
					}
				}
				
				if(!zonesToDelete.isEmpty()){
					DAORegistry.getCrownJewelTeamZonesDAO().updateAll(zonesToDelete);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
