package com.rtw.tmat.utils;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.InitializingBean;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.CustomerOrder;
import com.rtw.tracker.datas.Invoice;
import com.rtw.tracker.datas.InvoiceRefund;
import com.rtw.tracker.datas.InvoiceRefundResponse;
import com.rtw.tracker.enums.InvoiceStatus;
import com.rtw.tracker.enums.PartialPaymentMethod;
import com.rtw.tracker.enums.PaymentMethod;
import com.rtw.tracker.utils.Util;

public class StripeRefundScheduler extends TimerTask implements InitializingBean{

	Map<String, String> map = new HashMap<String, String>();
	private static final int limit = 100;
	
	@Override
	public void afterPropertiesSet() throws Exception {
		TimerTask timerTask = new StripeRefundScheduler();
		Calendar today = Calendar.getInstance();

		Timer timer = new Timer();
		//timer.schedule(timerTask, today.getTime(), TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS)); // 60*60*24*100 = 8640000ms
		timer.scheduleAtFixedRate(timerTask, today.getTime(), 60*60*1000);
		
	}

	@Override
	public void run() {
		try {
			if(Util.isContestRunning){
				System.out.println("RETURN");
				return;
			}
			System.out.println("RUNNNNN");
			map.put("configId", Constants.CONFIGID);
			map.put("productType", Constants.PRODUCTTYPE);
			map.put("platForm", Constants.PLATFORM);
			map.put("refundType",PaymentMethod.CREDITCARD.toString());
			map.put("limit", String.valueOf(limit));
			Map<String, Integer> refundMap = new HashMap<String, Integer>();
			String data = com.rtw.tmat.utils.Util.getObject(map,Constants.BASE_URL+Constants.GET_INVOICE_REFUNDS_LIST);
			Gson gson = com.rtw.tmat.utils.Util.getGsonBuilder().create();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			InvoiceRefundResponse stripeRefundResp = gson.fromJson(((JsonObject)jsonObject.get("invoiceRefundResponse")), InvoiceRefundResponse.class);
			if(stripeRefundResp!=null && stripeRefundResp.getStatus()==1){
				List<InvoiceRefund> refunds = stripeRefundResp.getStripeRefunds();
				Collection<InvoiceRefund> CustomerStripeRefund = DAORegistry.getInvoiceRefundDAO().getAllStripeRefunds();
				for(InvoiceRefund ref : CustomerStripeRefund){
					refundMap.put(ref.getRefundId(), ref.getOrderId());
				}
				if(refunds!=null && !refunds.isEmpty()){
					for(InvoiceRefund refund : refunds){
						if(refundMap.get(refund.getRefundId())!=null){
							continue;
						}
						InvoiceRefund dbRefund = DAORegistry.getInvoiceRefundDAO().getStripeRefundByIdandTrasactionId(refund.getRefundId(), refund.getTransactionId());
						if(dbRefund!=null){
							continue;
						}
						CustomerOrder customerOrder = DAORegistry.getCustomerOrderDAO().getCustomerOrderByTransactionId(refund.getTransactionId());
						if(customerOrder==null){
							continue;
						}
						Invoice invoice=  DAORegistry.getInvoiceDAO().getInvoiceByOrderId(customerOrder.getId());
						
						Double refundAmount =0.0;
						if (customerOrder != null && (customerOrder.getPrimaryPaymentMethod().equals(PaymentMethod.CREDITCARD)
								|| customerOrder.getPrimaryPaymentMethod().equals(PaymentMethod.GOOGLEPAY) 
								|| customerOrder.getPrimaryPaymentMethod().equals(PaymentMethod.IPAY))){
							refundAmount = customerOrder.getPrimaryPayAmt();
							if(!invoice.getStatus().equals(InvoiceStatus.Voided)){
								if(refundAmount.equals(refund.getAmount())){
									com.rtw.tmat.utils.Util.voidInvoice(invoice, customerOrder, refund, null);
								}
							}
							customerOrder.setPrimaryRefundAmount(customerOrder.getPrimaryRefundAmount()!=null?customerOrder.getPrimaryRefundAmount()+ refundAmount:refundAmount);
							//customerOrder.setOrderTotal(customerOrder.getOrderTotal()-refundAmount);
							
							invoice.setInvoiceTotal(invoice.getInvoiceTotal()-refundAmount);
							invoice.setLastUpdated(new Date());
							invoice.setLastUpdatedBy("AUTO");
							invoice.setRefundAmount(invoice.getRefundAmount()!=null?invoice.getRefundAmount()+refundAmount:refundAmount);
							if(refund!=null && refund.getId()==null){
								refund.setOrderId(customerOrder.getId());
								refund.setRefundedBy("AUTO");
								DAORegistry.getInvoiceRefundDAO().save(refund);
							}
							DAORegistry.getCustomerOrderDAO().update(customerOrder);
							DAORegistry.getInvoiceDAO().update(invoice);
						}else if (customerOrder.getSecondaryPaymentMethod()!=null && 
								(customerOrder.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
										|| customerOrder.getSecondaryPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY) 
										|| customerOrder.getSecondaryPaymentMethod().equals(PartialPaymentMethod.IPAY))) {
							refundAmount = customerOrder.getSecondaryPayAmt();
							
							customerOrder.setSecondaryRefundAmount(customerOrder.getSecondaryRefundAmount()!=null?customerOrder.getSecondaryRefundAmount()+ refundAmount:refundAmount);
							//customerOrder.setOrderTotal(customerOrder.getOrderTotal()-refundAmount);
							
							invoice.setInvoiceTotal(invoice.getInvoiceTotal()-refundAmount);
							invoice.setLastUpdated(new Date());
							invoice.setLastUpdatedBy("Auto");
							invoice.setRefundAmount(invoice.getRefundAmount()!=null?invoice.getRefundAmount()+refundAmount:refundAmount);
							if(refund!=null){
								refund.setOrderId(customerOrder.getId());
								refund.setRefundedBy("Auto");
								DAORegistry.getInvoiceRefundDAO().save(refund);
							}
							DAORegistry.getCustomerOrderDAO().update(customerOrder);
							DAORegistry.getInvoiceDAO().update(invoice);
							
						}else if(customerOrder.getThirdPaymentMethod()!=null && 
								(customerOrder.getThirdPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
										|| customerOrder.getThirdPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY) 
										|| customerOrder.getThirdPaymentMethod().equals(PartialPaymentMethod.IPAY))){
							refundAmount = customerOrder.getThirdPayAmt();
							
							customerOrder.setThirdRefundAmount(customerOrder.getThirdRefundAmount()!=null?customerOrder.getThirdRefundAmount()+ refundAmount:refundAmount);
							//customerOrder.setOrderTotal(customerOrder.getOrderTotal()-refundAmount);
							
							invoice.setInvoiceTotal(invoice.getInvoiceTotal()-refundAmount);
							invoice.setLastUpdated(new Date());
							invoice.setLastUpdatedBy("Auto");
							invoice.setRefundAmount(invoice.getRefundAmount()!=null?invoice.getRefundAmount()+refundAmount:refundAmount);
							if(refund!=null){
								refund.setOrderId(customerOrder.getId());
								refund.setRefundedBy("Auto");
								DAORegistry.getInvoiceRefundDAO().save(refund);
							}
							DAORegistry.getCustomerOrderDAO().update(customerOrder);
							DAORegistry.getInvoiceDAO().update(invoice);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
