package com.rtw.tmat.utils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtw.tmat.dao.implementation.TMATDAORegistry;
import com.rtw.tmat.data.Category;
import com.rtw.tmat.data.CategoryMapping;
import com.rtw.tmat.data.CategorySynonym;
import com.rtw.tmat.data.Event;
import com.rtw.tmat.data.Ticket;
import com.rtw.tmat.data.VenueCategory;



public class Categorizer extends QuartzJobBean implements StatefulJob{
	private static Pattern numberOrLetterBlock = Pattern.compile("([^\\d]+)|(\\d+)");
	private static Logger logger = org.slf4j.LoggerFactory.getLogger(Categorizer.class);
	
	private static Map<Integer, Collection<CategoryMapping>> categoryMappingMap = new ConcurrentHashMap<Integer, Collection<CategoryMapping>>();
	private static Map<Integer, Collection<Category>> categoryMap = new ConcurrentHashMap<Integer, Collection<Category>>();
	private static Map<String, String> validSection = new HashMap<String, String>();
	private static Map<Integer,Map<Long, Map<String, List<CategoryMapping>>>> mapTime = new HashMap<Integer, Map<Long,Map<String,List<CategoryMapping>>>>();
	private static Map<Integer, Collection<CategoryMapping>> realTimeMappingMap = new HashMap<Integer, Collection<CategoryMapping>>();
	private static Map<Integer, Long> mappingTimeMap = new HashMap<Integer, Long>();
	
	public static void init() {
		Long startTime = System.currentTimeMillis();
		Collection<CategoryMapping> categoryMappings = TMATDAORegistry.getCategoryMappingDAO().getAll();
		Collection<Category> categories = TMATDAORegistry.getCategoryDAO().getAll();
		Map<Integer, Collection<CategoryMapping>> tempCategoryMappingMap = new ConcurrentHashMap<Integer, Collection<CategoryMapping>>();
		Map<Integer, Collection<Category>> tempCategoryMap = new ConcurrentHashMap<Integer, Collection<Category>>();
		for (CategoryMapping mapping: categoryMappings) {
			
			Collection<CategoryMapping> list = tempCategoryMappingMap.get(mapping.getCategoryId());
			if (list == null) {
				list = new ArrayList<CategoryMapping>();
				tempCategoryMappingMap.put(mapping.getCategoryId(), list);
			}
			list.add(mapping);
		}
		categoryMappingMap.clear();
		categoryMappingMap = null;
		categoryMappingMap= new HashMap<Integer, Collection<CategoryMapping>>(tempCategoryMappingMap);
		tempCategoryMappingMap.clear();
		tempCategoryMappingMap =null;
		Map<String,Boolean> map = new HashMap<String, Boolean>();
		for (Category category: categories) {
//			Collection<CategoryMapping> mapping = categoryMappingMap.get(category.getId());
//			if (mapping== null){
//				
//			}
			Collection<CategoryMapping> list = categoryMappingMap.get(category.getId());
			if(list==null){
				list = new ArrayList<CategoryMapping>();
			}
			/* normalized zone should be consider to categorized tickets,
			 * 
			 * */
			String sectionZone = category.getNormalizedZone();
			String rowZone = "*";
			if(sectionZone!= null && sectionZone.contains("%")){
				String temp[] = sectionZone.split("%");
				sectionZone = temp[0];
				if(temp.length==2){
					rowZone = temp[1];
				}
			}
			Boolean isProcessed = map.get(sectionZone.replaceAll("\\s+", " ").toLowerCase());
			if(isProcessed==null || !isProcessed){
			
				CategoryMapping catMapping = new CategoryMapping();
				catMapping.setStartSection(sectionZone);
				catMapping.setEndSection(sectionZone);
				catMapping.setStartRow(rowZone);
				catMapping.setEndRow(rowZone);
				catMapping.setStartSeat("*");
				catMapping.setEndSeat("*");
				catMapping.setCategoryId(category.getId());
				list.add(catMapping);
				map.put(catMapping.getStartSection().toLowerCase(), true);
			}
			Collection<Category> tempCategoryList = tempCategoryMap.get(category.getVenueCategoryId());
			if(tempCategoryList==null){
				tempCategoryList = new ArrayList<Category>();
			}
			tempCategoryList.add(category);
			tempCategoryMap.put(category.getVenueCategoryId(), tempCategoryList);
		
		}
		categoryMap.clear();
		categoryMap = new ConcurrentHashMap<Integer, Collection<Category>>(tempCategoryMap);
		tempCategoryMap.clear();
		tempCategoryMap=null;
		System.out.println("TIME TO LAST CATEGORY MAPPING=" + (System.currentTimeMillis() - startTime) / 1000);

	}
	
	public static Collection<Category> getCategoryByVenueCategoryId(Integer venueCategoryId){
		return TMATDAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(venueCategoryId);
	}
	
	
	public static Collection<Ticket> computeCategoryForTickets(Collection<Ticket> tickets, Map<Integer, Category> catMap) {
//		return computeCategory(ticket.getEventId(), ticket.getNormalizedSection(), ticket.getNormalizedRow(), ticket.getNormalizedSeat(),groupName);
		if(tickets==null || tickets.isEmpty()){
			return tickets;
		}
		Map<String, List<CategoryMapping>> map = new HashMap<String, List<CategoryMapping>>();
/*		Map<Long, Map<Integer, List<CategoryMapping>>> catMap = mapTime.get(event.getVenueCategory().getId());
		if(catMap!=null && !catMap.isEmpty()){
			Long time = catMap.keySet().iterator().next();
			if(new Date().getTime() - time <= 1000*60*5){
				map = catMap.get(time);
			}
		}else{
			catMap = new HashMap<Long, Map<Integer,List<CategoryMapping>>>();
		}*/
//		if(map==null){
		/*VenueCategory venueCategory = null;
		if(event.getVenueCategory()!=null && event.getVenueCategory().getCategoryGroup().equalsIgnoreCase(groupName)){
			venueCategory = event.getVenueCategory();
		}else{
			venueCategory = DAORegistry.getVenueCategoryDAO().getVenueCategoryByVenueAndCategoryGroup(event.getVenueId(), groupName);
		}
			
		Collection<Category> catList = categoryMap.get(venueCategory.getId());
		*/
		
		Collection<Category> catList = catMap.values();
		for(Category category:catList){
			
			Collection<CategoryMapping> catMappingList = categoryMappingMap.remove(category.getId());
			catMappingList = TMATDAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByCategoryId(category.getId());
			if(catMappingList!=null && !catMappingList.isEmpty()){
				categoryMappingMap.put(category.getId(),catMappingList);
			}else{
				catMappingList = new ArrayList<CategoryMapping>();
			}
			
			/* Old Code commented by Ulaganathan
			Collection<CategoryMapping> catMappingList = categoryMappingMap.get(category.getId());
			if(catMappingList==null || catMappingList.isEmpty()){
				catMappingList = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByCategoryId(category.getId());
				if(catMappingList!=null && !catMappingList.isEmpty()){
					categoryMappingMap.put(category.getId(),catMappingList);
				}
			}*/
			for(CategoryMapping mapping:catMappingList){
				List<CategoryMapping> mappingList = map.get(mapping.getStartSection().replaceAll("\\s+", " ").toLowerCase());
				if(mappingList==null){
					mappingList = new ArrayList<CategoryMapping>();
				}
				mappingList.add(mapping);
				map.put(mapping.getStartSection().replaceAll("\\s+", " ").toLowerCase(), mappingList);
				if(mapping.getStartSection().equals(mapping.getEndSection())){
					if(mapping.getStartSection().contains("-")){
						mappingList = map.get("-");
						if(mappingList==null){
							mappingList = new ArrayList<CategoryMapping>();
						}
						mappingList.add(mapping);
						map.put("-", mappingList);
					}
					continue;
				}
				boolean isInt=false;
				Integer intStartSection =0;
				String startSection ="";
				String endSection  ="";
				Integer intEndSection =0; 
				String prefix="";  
				// Consider only integer section range..
				try{
					startSection = mapping.getStartSection();
					endSection = mapping.getEndSection();
					intStartSection= Integer.parseInt(startSection);
					intEndSection= Integer.parseInt(endSection);
					isInt=true;
				}catch (Exception ex) {
					
				}
				if(isInt){
					for(int start=intStartSection;start<=intEndSection;start++){
						String sec = prefix + start;
						if(sec.equals(startSection)){
							continue;
						}
						mappingList = map.get(sec.toLowerCase());
						if(mappingList==null){
							mappingList = new ArrayList<CategoryMapping>();
						}
						mappingList.add(mapping);
						map.put(sec.toLowerCase(), mappingList);
					}
					
				}
			}	
		}
			
//			map = new HashMap<String, List<CategoryMapping>>();
			/*if(mappings == null){
				return tickets;
			}
			
			catMap.clear();
			catMap.put(new Date().getTime(), map);
			mapTime.put(event.getVenueCategory().getId(),catMap);*/
//		}
//		Map<Integer,Category> catMap = new HashMap<Integer, Category>();
		for(Ticket ticket:tickets){
			ticket.setUncategorized(true);
//			List<CategoryMapping> mappingList = map.get(ticket.getNormalizedSection().replaceAll("\\s+", " ").toLowerCase());
			List<CategoryMapping> mappingList = null;
//			System.out.println(ticket.getId());
			if(ticket.getNormalizedSection().contains("-")){
				mappingList = map.get("-");
			}else{
				mappingList = map.get(ticket.getNormalizedSection().replaceAll("\\s+", " ").toLowerCase());
			}
			if(mappingList==null){
				mappingList = map.get("*");
				if(mappingList!=null && !mappingList.isEmpty()){
					CategoryMapping mapping = mappingList.get(0);
					ticket.setCategory(catMap.get(mapping.getCategoryId()));
					ticket.setCategoryMappingId(mapping.getId());
					ticket.setUncategorized(false);
				}
			}else if(mappingList!=null){
				for(CategoryMapping mapping:mappingList){
					Integer catId = computeCategory(ticket.getEventId(), ticket.getNormalizedSection(), ticket.getNormalizedRow(),ticket.getNormalizedSeat(), mapping);
					if(catId!=null){
						ticket.setCategory(catMap.get(catId));
						ticket.setCategoryMappingId(mapping.getId());
						ticket.setUncategorized(false);
						break;
					}
				}	
			}
		}
		return tickets;
	}
	
	public static Integer computeCategory(Ticket ticket,VenueCategory venueCategory, String groupName,Map<Integer,Category> catMap,Map<Integer,List<CategoryMapping>> catMappingMap) {
		Category cat = computeCategory(venueCategory, ticket.getNormalizedSection(), ticket.getNormalizedRow(),ticket.getNormalizedSeat(),catMap,catMappingMap);
		if(cat!=null){
			return cat.getId();
		}
		return null;
	}
	
	/*public static Collection<CategoryMapping> getMappingMap(String mapId){
		return mappingMap.get(mapId);
	}*/
	/**
	 * computeCategory
	 * 
	 * Will return a category id if there exists a category mapping for this
	 * section, row, and seat. If not it will check for a category id for
	 * this section and row.
	 * @param eventId
	 * @param section
	 * @param row
	 * @param startSeat
	 * @return
	 */
public static Category computeCategory(VenueCategory venueCategory, String section, String row, String seatRange,Map<Integer,Category> catMap,Map<Integer,List<CategoryMapping>> catMappingMap){
		
	
		if(venueCategory==null){
			return null;
		}
		String startSeat = null;
		String endSeat = null;
		
		if(seatRange != null && !seatRange.isEmpty() ) {
			 String[] tokens = seatRange.split("-", 2);
			 if(tokens.length == 2) {
				 startSeat = tokens[0];
				 endSeat = tokens[1];
			 } else {
				 startSeat = seatRange;
				 endSeat = seatRange;
			 }
		}	
//		Collection<Category> categoryList = categoryMap.get(venueCategory.getId());
//		if(categoryList==null){
//			categoryList = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(venueCategory.getId());
//			categoryMap.put(venueCategory.getId(),categoryList);
//		}
		for(Category category:catMap.values()){
			
//			Collection<CategoryMapping> list = categoryMappingMap.remove(category.getId());
//			
//			list = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByCategoryId(category.getId());
			Collection<CategoryMapping> list = catMappingMap.get(category.getId());
			if(list!=null && !list.isEmpty()){
				categoryMappingMap.put(category.getId(),list);
			}else{
				list = new ArrayList<CategoryMapping>();
			}
			
			// francois - FIXME: looks like inefficient code (should sort before putting in map)
			// -- begin --
			
			List<CategoryMapping> catList = new ArrayList<CategoryMapping>();
			catList.addAll(list);
			
			//order list for "Worst First"
			orderCatMaps(catList);
			
			// -- end --
			
			for (CategoryMapping mapping: catList) {
				Integer cmp1 = compare(mapping.getStartSection(), section);
				if (cmp1 == null) {
					continue;
				}
				
				Integer cmp2 = compare(section, mapping.getEndSection());
				if (cmp2 == null) {
					continue;
				}

				Integer cmp3 = compare(mapping.getStartRow(), row);
				if (cmp3 == null) {
					continue;
				}

				Integer cmp4 = compare(row, mapping.getEndRow());
				if (cmp4 == null) {
					continue;
				}

				
				if(startSeat == null || endSeat == null) {
					boolean flag = false;
					if((mapping.getStartSeat()!= null && (mapping.getStartSeat().equals("^") || mapping.getStartSeat().equals("*"))) ||( mapping.getEndSeat()!=null && (mapping.getEndSeat().equals("^") ||mapping.getEndSeat().equals("*"))))
					{
						flag= true;
					}
					if (cmp1 <= 0 && cmp2 <= 0 && cmp3 <= 0 && cmp4 <= 0 && (flag))  {
						return category;
					}
				}else if(!(mapping.getStartSeat().equals("^"))){
					Integer cmp5 = compareSeat(mapping.getStartSeat(), startSeat);
					if (cmp5 == null) {
						continue;
					}
					
					Integer cmp6 = compareSeat(startSeat, mapping.getEndSeat());
					if (cmp6 == null) {
						continue;
					}
		
					
					Integer cmp7 = compareSeat(mapping.getStartSeat(), endSeat);
					if (cmp7 == null) {
						continue;
					}
					
					Integer cmp8 = compareSeat(endSeat, mapping.getEndSeat());
					if (cmp8 == null) {
						continue;
					}
		
					// if one of the seats matches, it should be the lowest category
					//Changes for including seat number in categorization
					if (cmp1 <= 0 && cmp2 <= 0 && cmp3 <= 0 && cmp4 <= 0 && ((cmp5 <= 0 && cmp6 <=0 && cmp7 <=0 &&  cmp8 <=0)||(cmp5 >= 0 && cmp6 <=0 && cmp7 <=0 &&  cmp8 <=0)))  {
						return category;
					}
				}
			}	
		}
		
		return null;
	}

public static Integer computeCategory(Integer eventId, String section, String row, String seatRange, CategoryMapping mapping){
	
	String startSeat = null;
	String endSeat = null;
	
	if(seatRange != null && !seatRange.isEmpty() ) {
		 String[] tokens = seatRange.split("-", 2);
		 if(tokens.length == 2) {
			 startSeat = tokens[0];
			 endSeat = tokens[1];
		 } else {
			 startSeat = seatRange;
			 endSeat = seatRange;
		 }
	}	
	/*
	Collection<CategoryMapping> list = mappingMap.get(eventId + groupName);

	if(list == null) {
		return computeCategory(eventId, section, row, groupName);
	}
	
	// francois - FIXME: looks like inefficient code (should sort before putting in map)
	// -- begin --
	
	List<CategoryMapping> catList = new ArrayList<CategoryMapping>();
	catList.addAll(list);
	
	//order list for "Worst First"
	orderCatMaps(catList);
	
	// -- end --
	*/
//	for (CategoryMapping mapping: catList) {
	Integer cmp1 = null;
	Integer cmp2 = null;
	boolean isSectionRange = false;
	boolean isRange = false;
	if(section.contains("-")){
		try{
			String sections[] = section.split("-");
			String startSection = sections[0];
			String endSection = sections[1];
			try{
				Integer.parseInt(startSection);
				Integer.parseInt(endSection);
				isRange= true;
			}catch (Exception e) {
				startSection = section;
				endSection = section;
			}
			String mappingStartSection = mapping.getStartSection();
			String mappingEndSection = mapping.getEndSection();
			if(mappingStartSection.equalsIgnoreCase(mappingEndSection)){
				if(mappingStartSection.contains("-")){
					String tempStartSection = "";
					String tempEndSection = "";
					if(isRange){
						tempStartSection = mappingStartSection.split("-")[0];
						tempEndSection = mappingStartSection.split("-")[1];
					}else{
						tempStartSection = mappingStartSection;
						tempEndSection = mappingStartSection;
					}
					
					Integer tempStartSecton1 = compare(tempStartSection, startSection +"");
					Integer tempStartSecton2 = compare(tempEndSection, startSection +"");
					Integer tempEndSecton1 = compare(tempStartSection, endSection +"");
					Integer tempEndSecton2 = compare(tempEndSection, endSection +"");
					if(tempStartSecton1==null || tempStartSecton2 ==null || tempEndSecton1==null || tempEndSecton2 ==null  
							|| tempStartSecton1>0 || tempStartSecton2<0 || tempEndSecton1>0 || tempEndSecton2<0){
						return null;
					}else{
						cmp1 = -1;
						cmp2 = -1;
						isSectionRange = true;
					}
				}
				
			}
			
		}catch (Exception e) {
			isSectionRange = false;
		}
	}
	if(!isSectionRange){
		cmp1 = compare(mapping.getStartSection(), section);
		if (cmp1 == null) {
			return null;
		}
	
		cmp2 = compare(section, mapping.getEndSection());
		if (cmp2 == null) {
			return null;
		}
	}
		if(row == null || row.isEmpty()) {
			if((null !=mapping.getStartRow() && mapping.getStartRow().equals("^")) || 
					(null !=mapping.getEndRow() && mapping.getEndRow().equals("^"))){
				
				if (cmp1 <= 0 && cmp2 <= 0 )  {
					return mapping.getCategoryId();
				}
			}
		}

		Integer cmp3 = compare(mapping.getStartRow(), row);
		if (cmp3 == null) {
			return null;
		}

		Integer cmp4 = compare(row, mapping.getEndRow());
		if (cmp4 == null) {
			return null;
		}

		
		if(startSeat == null || endSeat == null) {
			boolean flag = false;
			if((mapping.getStartSeat()!= null && (mapping.getStartSeat().equals("^") || mapping.getStartSeat().equals("*"))) ||( mapping.getEndSeat()!=null && (mapping.getEndSeat().equals("^") ||mapping.getEndSeat().equals("*")))){
				flag= true;
			}
			if (cmp1 <= 0 && cmp2 <= 0 && cmp3 <= 0 && cmp4 <= 0 && (flag))  {
				return mapping.getCategoryId();
			}
		}else if(!(mapping.getStartSeat().equals("^"))){
			Integer cmp5 = compareSeat(mapping.getStartSeat(), startSeat);
			if (cmp5 == null) {
				return null;
			}
			
			Integer cmp6 = compareSeat(startSeat, mapping.getEndSeat());
			if (cmp6 == null) {
				return null;
			}

			
			Integer cmp7 = compareSeat(mapping.getStartSeat(), endSeat);
			if (cmp7 == null) {
				return null;
			}
			
			Integer cmp8 = compareSeat(endSeat, mapping.getEndSeat());
			if (cmp8 == null) {
				return null;
			}

			// if one of the seats matches, it should be the lowest category
			//Changes for including seat number in categorization
			if (cmp1 <= 0 && cmp2 <= 0 && cmp3 <= 0 && cmp4 <= 0 && ((cmp5 <= 0 && cmp6 <=0 && cmp7 <=0 &&  cmp8 <=0)||(cmp5 >= 0 && cmp6 <=0 && cmp7 <=0 &&  cmp8 <=0)))  {
				return mapping.getCategoryId();
			}
		}
//	}
	return null;
}
	
	public static Integer computeCategory1(VenueCategory venueCategory, String section, String row, String groupName,Map<Integer,Category> catMap,Map<Integer,List<CategoryMapping>> catMappingMap) {
//		Collection<CategoryMapping> list = mappingMap.get(event.getVenueCategory().getId());
		if(venueCategory == null){
			return null;
		}
		/*Collection<Category> list = categoryMap.get(venueCategory.getId());
		if(list == null){
			list = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(venueCategory.getId());
			categoryMap.put(venueCategory.getId(),list);
		}*/
		for(Category category:catMap.values()){
			/*Collection<CategoryMapping> catMapColleciton = categoryMappingMap.get(category.getId());
			if(catMapColleciton ==null || catMapColleciton.isEmpty()){
				continue;
			}*/
			String catzone = category.getNormalizedZone();
			String catSec = "";
			String catRow = "";
			if(catzone != null && catzone.contains("%")){
				catSec = catzone.split("%")[0];
				catRow = catzone.split("%")[1];
			}else{
				catSec = catzone;
				catRow = "";
			}
			if(catSec.replaceAll("\\s+", " ").equalsIgnoreCase(section.replaceAll("\\s+", " "))){
				if(catRow.isEmpty()){
					return category.getId();
				}else if(catRow.replaceAll("\\s+", " ").equalsIgnoreCase(row.replaceAll("\\s+", " "))){
					return category.getId();
				}
			}
		}
		for(Category category:catMap.values()){
			Collection<CategoryMapping> catMapColleciton = catMappingMap.get(category.getId());
			/*catMapColleciton = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByCategoryId(category.getId());
			if(null != catMapColleciton && !catMapColleciton.isEmpty()){
				categoryMappingMap.put(category.getId(), catMapColleciton);
			}*/
			/*
			Collection<CategoryMapping> catMapColleciton = categoryMappingMap.get(category.getId());
			if(catMapColleciton ==null || catMapColleciton.isEmpty()){
				catMapColleciton = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByCategoryId(category.getId());
				categoryMappingMap.put(category.getId(), catMapColleciton);
			}*/
			if (catMapColleciton==null || catMapColleciton.isEmpty()) {
				continue;
			}
			
			for (CategoryMapping mapping: catMapColleciton) {
				Integer cmp1 = compare(mapping.getStartSection(), section);
				if (cmp1 == null) {
					continue;
				}

				Integer cmp2 = compare(section, mapping.getEndSection());
				if (cmp2 == null) {
					continue;
				}

				Integer cmp3 = compare(mapping.getStartRow(), row);
				if (cmp3 == null) {
					continue;
				}
				
				Integer cmp4 = compare(row, mapping.getEndRow());
				if (cmp4 == null) {
					continue;
				}
				
				if (cmp1 <= 0 && cmp2 <= 0 && cmp3 <= 0 && cmp4 <= 0) {
					return mapping.getCategoryId();
				}
				
			}
		}
		
		return null;
	}
		
	public static Integer computeCategory(List<CategoryMapping> catList, String section, String row) {

		if (section == null) {
			section = "";
		}
		
		if (row == null) {
			row = "";
		}
		
		if (catList == null) {
			return null;
		}
				
		if (catList.isEmpty()) {
			return null;
		}
		
		orderCatMaps(catList);
			for (CategoryMapping mapping: catList) {
				Integer cmp1 = compare(mapping.getStartSection(), section);
				if (cmp1 == null) {
					continue;
				}
				Integer cmp2 = compare(section, mapping.getEndSection());
				if (cmp2 == null) {
					continue;
				}
				Integer cmp3 = compare(mapping.getStartRow(), row);
				if (cmp3 == null) {
					continue;
				}
				Integer cmp4 = compare(row, mapping.getEndRow());
				if (cmp4 == null) {
					continue;
				}
				
				if (cmp1 <= 0 && cmp2 <= 0 && cmp3 <= 0 && cmp4 <= 0) {
					return mapping.getCategoryId();
				}
				
			}
		//logger.info("returning null w/ Mappings");
		return null;
	}
	/*
	 * Compare by letter blocks and number blocks
	 * (e.g., ab56 > ab8 even though alphabetically it's not the case)
	 * we compare block per block so in this case first "ab" and "ab" then 56 and 8
	 * However if we have to compare things like: 20C and 19, then these are not comparable
	 * 
	 * So the idea is when we compare 20C and 19
	 * is first to compare that potentially 19 is less than 20. Then keep going and see that these 2 are not
	 * comparable
	 * 
	 * Same thing for 20C and 21BT, these 2 are not comparable
	 * However 20C and 21B are comparable
	 * 
	 * so we parse block per block (of letters of numbers) and whenever we
	 * find which is one is greater than the others, we keep going to check
	 * that the 2 symbols are comparable.
	 * 
	 * New addition: now, we have one more restriction: 12C and 14D are not comparable
	 * However 12C and 14C are.
	 * 
	 * They are comparable if:
	 * - they have the same structure: same alternance of number and letter blocks
	 * - letters block which are compared are of the same size (cannot compare C and CD for instance)
	 */
	
	public static Integer compare(String symbol1, String symbol2) {
		if (symbol1 == null || symbol2 == null) {
			return null;
		}
		
		symbol1 = symbol1.replaceAll(" ", "").toLowerCase();
		symbol2 = symbol2.replaceAll(" ", "").toLowerCase();

		if (symbol1.equals("*")) {
			return 0;
		}

		if (symbol2.equals("*")) {
			return 0;
		}
		
		if (symbol1.length() == 0 || symbol2.length() == 0) {
			return null;
		}
		
		if (symbol1.charAt(0) == '>') {
			for (String token: symbol1.split(">")) {
				
				token = token.trim();
				if (token.isEmpty()) {
					continue;
				}
				
				if (symbol2.indexOf(token) >= 0) {
					return 0;
				}
			}
			
			return null;
		}

		if (symbol2.charAt(0) == '>') {
			for (String token: symbol2.split(">")) {
				token = token.trim();
				if (token.isEmpty()) {
					continue;
				}
				if (symbol1.indexOf(token) >= 0) {
					return 0;
				}
			}
			
			return null;
		}

		Matcher matcher1 = numberOrLetterBlock.matcher(symbol1);
		Matcher matcher2 = numberOrLetterBlock.matcher(symbol2);
		Integer offset1 = 0;
		Integer offset2 = 0;
		
		Integer result = 0;
		while (true) {
			Boolean match1 = matcher1.find(offset1);
			Boolean match2 = matcher2.find(offset2);
			
//			System.out.println("MATCH " + match1 + " " + match2);
			if (match1 == false && match2 == false) {
				return result;
			}

			// if one has a next block and not the other => not comparable
			if (match1 == false || match2 == false) {
				return null;
			}

			String letters1 = matcher1.group(1);
			String letters2 = matcher2.group(1);

			// block of numbers to compare
			if (letters1 == null && letters2 == null) {
				String numberStr1 = matcher1.group(2);
				String numberStr2 = matcher2.group(2);

				try {
					/*
					 * getting numberFormatException for numbers greater then 10 digits so used Long instead of Integer
					 * In future we may change long to anyother datatype or some other logic if number 'll be greater then 19 digits					 * 
					 * Integer number1 = Integer.parseInt(numberStr1);
					 * Integer number2 = Integer.parseInt(numberStr2);
					*/
					Long number1 = Long.parseLong(numberStr1);
					Long number2 = Long.parseLong(numberStr2);
					if (number1 < number2) {
						// result already found
						if (result == 0) {
							result = -1;
						} else { // not comparable
							return null;
						}
					} else if (number1 > number2) {
						// result already found
						if (result == 0) {
							result = 1;
						} else { // not comparable
							return null;
						}
					}
					offset1 += numberStr1.length();
					offset2 += numberStr2.length();
				} catch (NumberFormatException nfe) {
					logger.info("Number format exception while parsing" +nfe.getMessage());
					return null;
					}
				catch (Exception e)
				{
					logger.info("Exception while parsing" +e.getMessage());
					return null;
				}
								
			} else if (letters1 != null & letters2 != null) {
				// block of letters to compare
				// must have the same length othewise not comparable
				if (letters1.length() != letters2.length()) {
					return null;
				}
				
				Integer cmp = letters1.compareToIgnoreCase(letters2);
				if (cmp < 0 || cmp > 0) {
					// result already found
					if (result == 0) {
						result = cmp;
					} else { // not comparable
						return null;						
					}
				}
				
				offset1 += letters1.length();
				offset2 += letters2.length();
			} else {
				// letter vs. number
				return null;				
			}
		}
		
	}

	public static Integer compareSeat(String symbol1, String symbol2) {
		if (symbol1 == null || symbol2 == null) {
			return null;
		}
		
		symbol1 = symbol1.replaceAll(" ", "").toLowerCase();
		symbol2 = symbol2.replaceAll(" ", "").toLowerCase();

		if (symbol1.equals("*")|| symbol1.equals("^")) {
			return 0;
		}
	
		if (symbol2.equals("*") || symbol2.equals("^")) {
			return 0;
		}
		
		if (symbol1.length() == 0 || symbol2.length() == 0) {
			return null;
		}
		
		if (symbol1.charAt(0) == '>') {
			for (String token: symbol1.split(">")) {
				token = token.trim();
				if (token.isEmpty()) {
					continue;
				}
				if(org.apache.commons.lang.StringUtils.isNumeric(token) && org.apache.commons.lang.StringUtils.isNumeric(symbol2))
				{
					if (new Integer(symbol2) > new Integer(token)) {
						return 0;
					}
				}
			}
			
			return null;
		}

		if (symbol2.charAt(0) == '>') {
			for (String token: symbol2.split(">")) {
				token = token.trim();
				if (token.isEmpty()) {
					continue;
				}
				if(org.apache.commons.lang.StringUtils.isNumeric(token) && org.apache.commons.lang.StringUtils.isNumeric(symbol1))
				{
					if (new Integer(symbol1) > new Integer(token)) {
						return 0;
					}
				}
			}
			
			return null;
		}

		Matcher matcher1 = numberOrLetterBlock.matcher(symbol1);
		Matcher matcher2 = numberOrLetterBlock.matcher(symbol2);
		Integer offset1 = 0;
		Integer offset2 = 0;
		
		Integer result = 0;
		while (true) {
			Boolean match1 = matcher1.find(offset1);
			Boolean match2 = matcher2.find(offset2);
			
//			System.out.println("MATCH " + match1 + " " + match2);
			if (match1 == false && match2 == false) {
				return result;
			}

			// if one has a next block and not the other => not comparable
			if (match1 == false || match2 == false) {
				return null;
			}

			String letters1 = matcher1.group(1);
			String letters2 = matcher2.group(1);

			// block of numbers to compare
			if (letters1 == null && letters2 == null) {
				String numberStr1 = matcher1.group(2);
				String numberStr2 = matcher2.group(2);

				try {
					Integer number1 = Integer.parseInt(numberStr1);
					Integer number2 = Integer.parseInt(numberStr2);
					if (number1 < number2) {
						// result already found
						if (result == 0) {
							result = -1;
						} else { // not comparable
							return null;
						}
					} else if (number1 > number2) {
						// result already found
						if (result == 0) {
							result = 1;
						} else { // not comparable
							return null;
						}
					}
					offset1 += numberStr1.length();
					offset2 += numberStr2.length();
				} catch (NumberFormatException nfe) {
					logger.info("Number format exception while parsing" +nfe.getMessage());
					return null;
					}
				catch (Exception e)
				{
					logger.info("Exception while parsing" +e.getMessage());
					return null;
				}
								
			} else if (letters1 != null & letters2 != null) {
				// block of letters to compare
				// must have the same length otherwise not comparable
				if (letters1.length() != letters2.length()) {
					return null;
				}
				
				Integer cmp = letters1.compareToIgnoreCase(letters2);
				if (cmp < 0 || cmp > 0) {
					// result already found
					if (result == 0) {
						result = cmp;
					} else { // not comparable
						return null;						
					}
				}
				
				offset1 += letters1.length();
				offset2 += letters2.length();
			} else {
				// letter vs. number
				return null;				
			}
		}
		
	}

	public static List<Integer> getCategoriesByEvent(VenueCategory venueCategory) {
		List<Integer> categoryIds = new ArrayList<Integer>();
		if(venueCategory == null ){
			return categoryIds;
		}
		Collection<Category> catList = categoryMap.get(venueCategory.getId());
		if(catList==null){
			catList = TMATDAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(venueCategory.getId());
			categoryMap.put(venueCategory.getId(),catList);
		}
//		Map<Integer,Collection<CategoryMapping>> map = mappingMap.get(event.getVenueCategory().getId());;
		
		for(Category category:catList){
			if(!categoryIds.contains(category.getId())){
				categoryIds.add(category.getId());
			}
			/*Collection<CategoryMapping> catMaps = map.get(key);
			if(catMaps != null) {
				if(!catMaps.isEmpty()) {
					for(CategoryMapping mapping : catMaps){
						if(!categoryIds.contains(mapping.getCategoryId())){
							categoryIds.add(mapping.getCategoryId());
						}
					}
				}
			}*/
		}
		return categoryIds;
	}
	public static List<String> getCategoryGroupsByVenueId(Integer venueId) {
		return TMATDAORegistry.getVenueCategoryDAO().getCategoryGroupByVenueId(venueId);
	}
	public static Category getCategoryByCatSection(String section, String row, VenueCategory venueCategory) {	

		Category category = null;
		try{
//			VenueCategory venueCategory = DAORegistry.getVenueCategoryDAO().getVenueCategoryByVenueAndCategoryGroup(event.getVenueId(), groupName);
			if(venueCategory==null){
				return null;
			}
			Collection<CategorySynonym> synonyms = TMATDAORegistry.getCategorySynonymDAO().getTourCategorySynonyms(venueCategory.getId());
	
			if(synonyms == null) {
				logger.error("Invalid category. "
						+ "The category for the symbol '"
						+ section + "' cannot be found. There are no "
						+ "synonyms for this category");
				return null;
			}
//			Event event = DAORegistry.getEventDAO().get(eventId);
			
			List<Integer> categoryIds = getCategoriesByEvent(venueCategory);
			
			Iterator<CategorySynonym> syns = synonyms.iterator();
			
//			Collection<CategoryMapping> list = mappingMap.get(event.getVenueCategory().getId());
			Collection<Category> list = categoryMap.get(venueCategory.getId());;
			for(Category cat:list){
				Collection<CategoryMapping> mappingList = categoryMappingMap.get(cat.getId()); 
				if (mappingList == null) {
					continue;
				}
				while(syns.hasNext() && category == null) {
					
					CategorySynonym syn = syns.next();
					category = syn.getCategory();
					
					//System.out.println("Checking Category: " + category.getDescription());	
					//System.out.println("Section: " + section);
					//System.out.println("range: " + syn.getStartSynonym() + "-" + syn.getEndSynonym());
		
					if(syn.getStartSynonym().equals(section)){
						for(CategoryMapping catMap : mappingList){
							if(catMap.getCategoryId().equals(category.getId())) {
								Integer cmp1 = compare(catMap.getStartRow(), row);
								if (cmp1 == null) {
									continue;
								}
								Integer cmp2 = compare(row, catMap.getEndRow());
								if (cmp2 == null) {
									continue;
								}
								if(cmp1 <= 0 && cmp2 <= 0) {
									return category;
								}
							}
						}
					} else if(syn.getEndSynonym() != null && !syn.getEndSynonym().equals("")){
						Integer cmp1 = Categorizer.compare(section, syn.getStartSynonym());
						if (cmp1 == null) {
							continue;
						}
						Integer cmp2 = Categorizer.compare(syn.getEndSynonym(), section);
						if (cmp2 == null) {
							continue;
						}
						// System.out.println("cmp1: " + cmp1 + " cmp2: " + cmp2);
						if ((cmp1 > 0 && cmp2 >= 0) || (cmp1 < 0 && cmp2 <= 0)) {
							//if category is in this event's category mapping
							//if it is not that means it is for  different venue in this tour
							if(categoryIds.contains(category.getId())) {
								//System.out.println("Returning Category: " + category.getDescription());
								for(CategoryMapping catMap : mappingList){
									if(catMap.getCategoryId().equals(category.getId())) {
										Integer cmp3 = compare(catMap.getStartRow(), row);
										Integer cmp4 = compare(row, catMap.getEndRow());
										if(cmp3 <= 0 && cmp4 <= 0) {
											return category;
										}
									}
								}
							}
						}
					}
					category = null;
				}
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * orderCatMaps - THIS METHOD MUTATES THE PARAMETER catMaps
	 * 
	 * This method will mutate the parameter List statuses and 
	 * order it by:
	 * a.) Worse(newer/lower numbered) category ids before better category ids
	 *
	 * @param catMaps - List of CatMaps to be ordered
	 * @return void 
	 */
	public static void orderCatMaps(List<CategoryMapping> catMaps){
		Collections.sort(catMaps, new Comparator<CategoryMapping>() {
			
			public int compare(CategoryMapping catMap1, CategoryMapping catMap2) {
				if(catMap1.getCategoryId() < catMap2.getCategoryId()) {
					return 1;
				} else if(catMap1.getCategoryId() > catMap2.getCategoryId()) {
					return -1;
				}
				return 0;
			}
		});
		// sort = "external";
	}
	
	public  static void fillValidSection(){
		String[] sectionGroup = "A:B,B:C,C:D,D:E,E:F,F:G,G:H,H:I,I:J,J:K,K:L,L:M,M:N,N:O,O:P,P:Q,Q:R,R:S,S:T,T:U,U:V,V:W,W:X,X:Y,Y:Z,Z:--,AA:BB,BB:CC,CC:DD,DD:EE,EE:FF,FF:GG,GG:HH,HH:II,II:JJ,JJ:KK,KK:LL,LL:MM,MM:NN,NN:OO,OO:PP,PP:QQ,QQ:RR,RR:SS,SS:TT,TT:UU,UU:VV,VV:WW,WW:XX,XX:YY,YY:ZZ,ZZ,--".split(",");
		for(String section:sectionGroup){
			if(section!=null && !section.isEmpty()){
				String[] keyValue = section.split(",");
				if(keyValue.length==2){
					validSection.put(keyValue[0], keyValue[1]);
				}
			}
		}
	}
	
	public static void fillCategoryFilterMap(Event event,String catScheme,String categorySymbol,Map<String,Object> map,String tabName,String username){
		// "" = ALL
		// "CAT" = ALL categorized ticket
		// "UNCAT" = uncategorized ticket
		
		/*if(catScheme == null){
			catScheme = Categorizer.getCategoryGroupsByEvent(event.getId()).get(0);
			
		}*/
		
		/*Collection<String> catGroups = Categorizer.getCategoryGroupsByEvent(event.getId());
		if(catGroups != null){
			if(!catGroups.contains(catScheme)){
				catScheme = Categorizer.getCategoryGroupsByEvent(event.getId()).get(0);
			}
		} else {
			catScheme = "";
		}*/
		if((catScheme == null || catScheme.isEmpty()) && event.getVenueCategory()!=null){
			catScheme = event.getVenueCategory().getCategoryGroup();
		}
		
		List<Integer> categoryIds = new ArrayList<Integer>();
		Map<String, Boolean> catFilters = new HashMap<String, Boolean>();
		String categorySymbols = "";
		Collection<Category> cats = TMATDAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(event.getVenueId(), catScheme);	
		catFilters.put("ALL", new Boolean(false));
		catFilters.put("CAT", new Boolean(false));
		catFilters.put("UNCAT", new Boolean(false));
		if(cats != null){
			
			for(Category cat: cats){
				catFilters.put(cat.getSymbol(), new Boolean(false));
				categorySymbols = categorySymbols + cat.getSymbol() + ",";
			}
			categorySymbols = categorySymbols.substring(0, categorySymbols.length());
		}
		if (categorySymbol == null) {
			categorySymbol = ""; 
		}
		if (categorySymbol.equals("")) {
			categoryIds.add(Category.ALL_ID);
			catFilters.put("ALL", new Boolean(true));
		} else if(categorySymbol.length() >= 6) {
			if (categorySymbol.substring(0,5).equals("UNCAT")) {
				categoryIds.add(Category.UNCATEGORIZED_ID);		
				catFilters.put("UNCAT", new Boolean(true));
			} 
		} else if(categorySymbol.length() >= 4) {
			if (categorySymbol.substring(0,3).equals("ALL")) {
				categoryIds.add(Category.ALL_ID);
				catFilters.put("ALL", new Boolean(true));
			} else if (categorySymbol.substring(0,3).equals("CAT")) {
				categoryIds.add(Category.CATEGORIZED_ID);	
				catFilters.put("CAT", new Boolean(true));
			}
		} 
		if(!categorySymbol.equals("")) {
			StringTokenizer tokenizer = new StringTokenizer(categorySymbol);
			while(tokenizer.hasMoreTokens()){
				String cat = tokenizer.nextToken(",").trim();
				if(!cat.equals("ALL") && !cat.equals("CAT") && !cat.equals("UNCAT") && !cat.equals("")){
					catFilters.put(cat, new Boolean(true));
					Category category = TMATDAORegistry.getCategoryDAO().getCategoryByVenueCategoryIdAndCategorySymbol(event.getVenueCategory().getId(),cat);
					if(category != null){
						categoryIds.add(category.getId());
					}
				}
			}
		}
		
		
		if(categoryIds.isEmpty()){
			categoryIds.add(Category.ALL_ID);
		}
		
		if(tabName.equals("tickets")){
			
			map.put("catScheme", catScheme);
			map.put("categorySymbol", categorySymbol);
			map.put("categorySymbols", categorySymbols);
			map.put("categoryIds", categoryIds);
			map.put("catFilters", catFilters);
		}
		if(tabName.equals("history")){
			
			map.put("historyCatScheme", catScheme);
			map.put("historyCategorySymbol", categorySymbol);
			map.put("historyCategorySymbols", categorySymbols);
			map.put("historyCategoryIds", categoryIds);
			map.put("historyCatFilters", catFilters);
		}
		if(tabName.equals("liquidity")){
			
			map.put("liquidityCatScheme", catScheme);
			map.put("liquidityCategorySymbol", categorySymbol);
			map.put("liquidityCategorySymbols", categorySymbols);
			map.put("liquidityCategoryIds", categoryIds);
			map.put("liquidityCatFilters", catFilters);
		}
		if(tabName.equals("stats")){
			
			map.put("statsCatScheme", catScheme);
			map.put("statsCategorySymbol", categorySymbol);
			map.put("statsCategorySymbols", categorySymbols);
			map.put("statsCategoryIds", categoryIds);
			map.put("statsCatFilters", catFilters);
		}
		
	}
	
	public static Collection<Ticket> unCategorizedTickets(Collection<Ticket> tickets,Map<Integer, Category> categoryMap,Event event) throws SQLException {
		Collection<Ticket> result = new ArrayList<Ticket>();
		if(tickets==null || tickets.isEmpty()){
			return tickets;
		}
//		Integer eventId= tickets.iterator().next().getEventId();
		Map<String, List<CategoryMapping>> map = null;
		Map<Long, Map<String, List<CategoryMapping>>> catMap = mapTime.get(event.getVenueCategoryId());
		if(catMap!=null && !catMap.isEmpty()){
			Long time = catMap.keySet().iterator().next();
			if(new Date().getTime() - time <= 1000*60*5){
				map = catMap.get(time);
			}
		}else{
			catMap = new HashMap<Long, Map<String,List<CategoryMapping>>>();
		}
		if(map==null){
			Collection<CategoryMapping> list = null;
			if(mappingTimeMap.get(event.getVenueCategoryId())==null || (new Date().getTime()-mappingTimeMap.get(event.getVenueCategoryId())>=1000*60*5)){
				list = TMATDAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(event.getVenueCategoryId());
				realTimeMappingMap.put(event.getVenueCategoryId(), list);
				mappingTimeMap.put(event.getVenueCategoryId(), new Date().getTime());
			}else{
				list = realTimeMappingMap.get(event.getVenueCategoryId());
			}
			map = new HashMap<String, List<CategoryMapping>>();
			for(CategoryMapping mapping:list){
				List<CategoryMapping> mappingList = map.get(mapping.getStartSection().replaceAll("\\s+", " ").toLowerCase());
				if(mappingList==null){
					mappingList = new ArrayList<CategoryMapping>();
				}
				mappingList.add(mapping);
				map.put(mapping.getStartSection().replaceAll("\\s+", " ").toLowerCase(), mappingList);
				
				if(mapping.getStartSection().equals(mapping.getEndSection())){
					continue;
				}
				boolean isInt=false;
				Integer intStartSection =0;
				String startSection ="";
				String endSection  ="";
				Integer intEndSection =0; 
				String prefix="";  
				// Consider only integer section range..
				try{
					startSection = mapping.getStartSection();
					endSection = mapping.getEndSection();
					intStartSection= Integer.parseInt(startSection);
					intEndSection= Integer.parseInt(endSection);
					isInt=true;
				}catch (Exception ex) {
					
				}
				
				if(isInt){
					for(int start=intStartSection;start<=intEndSection;start++){
						String sec = prefix + start;
						if(sec.equals(startSection)){
							continue;
						}
						mappingList = map.get(sec.toLowerCase());
						if(mappingList==null){
							mappingList = new ArrayList<CategoryMapping>();
						}
						mappingList.add(mapping);
						map.put(sec.toLowerCase(), mappingList);
					}
					
				}
			}
			catMap.clear();
			catMap.put(new Date().getTime(), map);
			mapTime.put(event.getVenueCategoryId(),catMap);
		}
		List<Ticket> uncategoprizedTickets = new ArrayList<Ticket>();
		for(Ticket ticket:tickets){
			
			boolean iscategorized =false;
			
			List<CategoryMapping> mappingList = map.get(ticket.getNormalizedSection().replaceAll("\\s+", " ").toLowerCase());
			if(mappingList!=null){
				for(CategoryMapping mapping:mappingList){
					Integer catId = computeCategory(ticket.getEventId(), ticket.getNormalizedSection(), ticket.getRow(),ticket.getSeat(),mapping);
					if(catId!=null){
						iscategorized =true;
						break;
					}
				}
				
			}
			if(!iscategorized){
				uncategoprizedTickets.add(ticket);
			}
			
		}
		return uncategoprizedTickets;
	}

	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		init();
		
	}
	
	public static Collection<Ticket> computeCategoryForTickets(Collection<Ticket> tickets,Map<Integer, Category> categoryMap,Event event) throws Exception {
//		return computeCategory(ticket.getEventId(), ticket.getNormalizedSection(), ticket.getNormalizedRow(), ticket.getNormalizedSeat(),groupName);
		Collection<Ticket> result = new ArrayList<Ticket>();
		
		if(tickets==null || tickets.isEmpty()){
			return tickets;
		}
		
		Map<String, List<CategoryMapping>> map = getCategoryMappingByVenueCategoryId(event);
		
		for(Ticket ticket:tickets){
			//List<CategoryMapping> mappingList = map.get(ticket.getNormalizedSection().replaceAll("\\+", " ").toLowerCase());
			List<CategoryMapping> mappingList = null;
			if(ticket.getNormalizedSection().contains("-")){
				mappingList = map.get("-");
			}else{
				mappingList = map.get(ticket.getNormalizedSection().replaceAll("\\s+", " ").toLowerCase());
			}
		   
			if(mappingList==null){
				mappingList = map.get("*");
				if(mappingList!=null && !mappingList.isEmpty()){
					CategoryMapping mapping = mappingList.get(0);
					ticket.setCategory(categoryMap.get(mapping.getCategoryId()));
					ticket.setCategoryMappingId(mapping.getId());
					ticket.setUncategorized(false);
					result.add(ticket);
				}
			}else if(mappingList!=null){
				for(CategoryMapping mapping:mappingList){
					Integer catId = computeCategory(ticket.getEventId(), ticket.getNormalizedSection(), ticket.getNormalizedRow(),ticket.getNormalizedSeat(), mapping);
					if(catId!=null){
						ticket.setCategory(categoryMap.get(catId));
						ticket.setCategoryMappingId(mapping.getId());
						ticket.setUncategorized(false);
						result.add(ticket);
						break;
					}
				}	
			}
			/*if(mappingList!=null){
				for(CategoryMapping mapping:mappingList){
					Integer catId = computeCategory(ticket.getEventId(), ticket.getNormalizedSection(), ticket.getRow(),ticket.getSeat(),mapping);
					if(catId!=null){
						ticket.setCategory(categoryMap.get(catId));
						ticket.setCategoryMapping(mapping);
						result.add(ticket);
						break;
					}
				}	
			}*/ 
			
		}
		return result;
	}
	
	public static Map<String, List<CategoryMapping>> getCategoryMappingByVenueCategoryId(Event event) throws Exception {
		
		Map<String, List<CategoryMapping>> finalMap = new HashMap<String, List<CategoryMapping>>();
		Map<String, List<CategoryMapping>> map = null;
		Map<Long, Map<String, List<CategoryMapping>>> catMap = mapTime.get(event.getVenueCategoryId());
		if(catMap!=null && !catMap.isEmpty()){
			Long time = catMap.keySet().iterator().next();
			if(new Date().getTime() - time <= 1000*60*5){
				map = catMap.get(time);
				//catMappingList = realTimeMappingMap.get(event.getVenueCategoryId());
			}
		}else{
			catMap = new HashMap<Long, Map<String,List<CategoryMapping>>>();
		}
		if(map==null){
			Collection<CategoryMapping> list = null;
			if(mappingTimeMap.get(event.getVenueCategoryId())==null || (new Date().getTime()-mappingTimeMap.get(event.getVenueCategoryId())>=1000*60*5)){
				list = TMATDAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(event.getVenueCategoryId());//Records are in descending order.
				realTimeMappingMap.put(event.getVenueCategoryId(), list);
				mappingTimeMap.put(event.getVenueCategoryId(), new Date().getTime());
			}else{
				list = realTimeMappingMap.get(event.getVenueCategoryId());
			}
			//catMappingList = list;
			map = new HashMap<String, List<CategoryMapping>>();
			for(CategoryMapping mapping:list){
				List<CategoryMapping> mappingList = map.get(mapping.getStartSection().replaceAll("\\s+", " ").toLowerCase());
				if(mappingList==null){
					mappingList = new ArrayList<CategoryMapping>();
				}
				mappingList.add(mapping);
				map.put(mapping.getStartSection().replaceAll("\\s+", " ").toLowerCase(), mappingList);
				
				/*if(mapping.getStartSection().equals(mapping.getEndSection())){
					continue;
				}*/
				if(mapping.getStartSection().equals(mapping.getEndSection())){
				     if(mapping.getStartSection().contains("-")){
				      mappingList = map.get("-");
				      if(mappingList==null){
				       mappingList = new ArrayList<CategoryMapping>();
				      }
				      mappingList.add(mapping);
				      map.put("-", mappingList);
				     }
				     continue;
				}
				
				boolean isInt=false;
				Integer intStartSection =0;
				String startSection ="";
				String endSection  ="";
				Integer intEndSection =0; 
				String prefix="";  
				// Consider only integer section range..
				try{
					startSection = mapping.getStartSection();
					endSection = mapping.getEndSection();
					intStartSection= Integer.parseInt(startSection);
					intEndSection= Integer.parseInt(endSection);
					isInt=true;
				}catch (Exception ex) {
					
				}
				
				if(isInt){
					for(int start=intStartSection;start<=intEndSection;start++){
						String sec = prefix + start;
						if(sec.equals(startSection)){
							continue;
						}
						mappingList = map.get(mapping.getStartSection().toLowerCase());
						if(mappingList==null){
							mappingList = new ArrayList<CategoryMapping>();
						}
						mappingList.add(mapping);
						map.put(sec.toLowerCase(), mappingList);
					}
					
				}
			}
			catMap.clear();
			catMap.put(new Date().getTime(), map);
			mapTime.put(event.getVenueCategoryId(),catMap);
		}
		if(map != null) {
			finalMap = new HashMap<String, List<CategoryMapping>>(map);	
		}
		return finalMap;
	}
	
	public static Map<String, CategoryMapping> computeRowRangeForSectionsByVenueCategoryIdOne(Event event) throws Exception {
		
		Map<String, CategoryMapping> map = new HashMap<String, CategoryMapping>();
		Map<String,List<CategoryMapping>> csvCatMap = Categorizer.getCategoryMappingByVenueCategoryId(event);
		List<String> keyList = new ArrayList<String>(csvCatMap.keySet());
		
		for (String key : keyList) {
			
			if(key.equals("-")) {
				continue;
			}
			List<CategoryMapping> catmappingList = csvCatMap.get(key);
		
			for (CategoryMapping mapping : catmappingList) {
				if(mapping.getRowRange() != null && mapping.getRowRange().trim().length() > 0 ) {
					map.put(key,mapping);
				}
			}
		}
		return map;
	}

	public static Map<String,CategoryMapping> getLastRowMinicatsSectionBasedEligibleTMATTicketRowsFromLastRow(Event event) throws Exception {
		
		String intRegex = "[0-9]+";
		String strAlphabetRegex = "[a-z]+";
		
		Map<String,CategoryMapping> sectionMap = new HashMap<String, CategoryMapping>();
		Map<String,List<CategoryMapping>> catmappingMap = Categorizer.getCategoryMappingByVenueCategoryId(event);
		List<String> keyList = new ArrayList<String>(catmappingMap.keySet());
		
		for (String key : keyList) {
			
			if(key.equals("-")) {
				continue;
			}
			
			List<CategoryMapping> catmappingList = catmappingMap.get(key);
		
			for (CategoryMapping catmapping : catmappingList) {
				if(catmapping.getLastRow() != null && !catmapping.getLastRow().equals("")) {
					String lastRow = catmapping.getLastRow().toLowerCase();
					
					try {
						Set<String> eligibleRowList = new HashSet<String>();
						String miniLastRow = null;
			
						if(lastRow.matches(intRegex)) {
							Integer miniLastRowInt = Integer.valueOf(lastRow);
							miniLastRowInt = miniLastRowInt - 3;//miniLastRow should be less than 3 rows from lastRow 
							int startRowInt = 10;//miniLastRowInt - 4;//Rows should be within 4 row from miniLastRow 
							
							if(miniLastRowInt >= 10) {//Rows should not less than 1. 
								for (int i = miniLastRowInt; i >= startRowInt; i--) {
									eligibleRowList.add("" + i);
								}
								miniLastRow = "" + miniLastRowInt;
								//catMapping.setMiniLastRow(miniLastRow.toUpperCase());
							}
							
						} else if(lastRow.matches(strAlphabetRegex)) {
							String charRepeatregex = "^" + lastRow.charAt(0) + "+$";
							if(lastRow.matches(charRepeatregex)) {
								
								int stringLength = lastRow.length();
								int miniLastRowInt = (int) lastRow.toLowerCase().charAt(0);
								miniLastRowInt = miniLastRowInt - 3;
								int startRowInt = 107;//miniLastRowInt - 4;
								
								if(miniLastRowInt >= 107) {//Rows should not less than a. 
									for (int i = miniLastRowInt; i >= startRowInt; i--) {
										char tempChar = (char)i;
										String str="";
										
										for(int j=0; j < stringLength; j++) {
											str = str+tempChar;
										}
										eligibleRowList.add("" + str);
										
										if(i == miniLastRowInt) {
											miniLastRow = str;
											//catMapping.setMiniLastRow(miniLastRow.toUpperCase());
										}
									}
								}
							}
						}
						
						if(!eligibleRowList.isEmpty()) {
							CategoryMapping tempCatMapping = new CategoryMapping(catmapping);
							tempCatMapping.setMiniLastRow(miniLastRow.toUpperCase());
							tempCatMapping.setMiniLastEligibleRows(eligibleRowList);
							sectionMap.put(key, tempCatMapping);
							break;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		
		return sectionMap;
	
	}
}
