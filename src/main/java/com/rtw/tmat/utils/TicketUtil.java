package com.rtw.tmat.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rtw.tmat.dao.implementation.TMATDAORegistry;
import com.rtw.tmat.data.AdmitoneInventory;
import com.rtw.tmat.data.BaseTicket;
import com.rtw.tmat.data.Category;
import com.rtw.tmat.data.DefaultPurchasePrice;
import com.rtw.tmat.data.DuplicateTicketMap;
import com.rtw.tmat.data.Event;
import com.rtw.tmat.data.EventPriceAdjustment;
import com.rtw.tmat.data.ManagePurchasePrice;
import com.rtw.tmat.data.RewardthefanCategoryTicket;
import com.rtw.tmat.data.RewardthefanCatsExchangeEvent;
import com.rtw.tmat.data.Site;
import com.rtw.tmat.data.Ticket;
import com.rtw.tmat.enums.PriceRounding;
import com.rtw.tmat.enums.RemoveDuplicatePolicy;
import com.sun.syndication.fetcher.FetcherException;
import com.sun.syndication.io.FeedException;






/**
 * Ticket Util Class.
 */
public final class TicketUtil {
	public static final Integer MAX_QTY = new Integer(11);
	private static final Logger logger = LoggerFactory.getLogger(TicketUtil.class);
	private static final Double SIMILARITY_PRICE_THRESHOLD_PERCENT = 0.80; // not used for smart remove
//	private static final Double SIMILARITY_PRICE_DIFF_THRESHOLD_PERCENT = 0.20;   // Old markup % 
	private static final Double SIMILARITY_PRICE_DIFF_THRESHOLD_PERCENT = 0.25;   // Chagned from 18 to 25 by Chirag on Joe's Request
	
	// site ranking when comparing tickets
	private static Map<String, Integer> siteRank = new HashMap<String, Integer>();
	private static CurrencyRSSFetcher gbpFetcher = null;
	private static CurrencyRSSFetcher eurFetcher = null;
	// eventId-siteId => price adjustment
	private static Map<String, Double> percentAdjustmentMap = new HashMap<String, Double>();
	private static Map<String, PriceRounding> priceRoundingMap = new HashMap<String, PriceRounding>();
	private static String[] rankedSiteIds = {Site.AOP, Site.EIBOX, Site.EI_MARKETPLACE, Site.TICKET_NETWORK, Site.TICKET_NETWORK_DIRECT,
							Site.STUB_HUB, Site.STUBHUB_FEED, Site.TICKET_SOLUTIONS, Site.WS_TICKETS,Site.TICKET_EVOLUTION,
							Site.EBAY, Site.VIAGOGO, Site.SEATWAVE, Site.GET_ME_IN,
							Site.RAZOR_GATOR, Site.TICKET_MASTER, Site.EVENT_INVENTORY, Site.TICKETS_NOW,Site.VIVIDSEAT,Site.FLASH_SEATS, Site.TICK_PICK, Site.TICKET_CITY,Site.TICKET_LIQUIDATOR,Site.STUB_HUB_API,Site.FAN_SNAP , Site.SEATGEEK
	};

	private static String[] reversedRankedSiteIds;
	private static Pattern seatRangePattern = Pattern.compile("[0]*([0-9])+\\s+-\\s+([0]*[0-9])+");
	private static Pattern singleSeatPattern = Pattern.compile("([0]*[0-9])+");
	private static final int CREATE_GROUP_TIME_DURATION = 1000*60*5; 
	private static final int DEFAULT_MARK_UP = 0; 
	private static final int DEFAULT_SELL_QTY = 10; 
	private static final double DEFAULT_RPT_FACTOR = 20;
	private static final double DEFAULT_SHIPPING_CHARG = 4.0;
	private static final int DEFAULT_EXPOSOR = 2;
	private static Set<String> larryQuantitySeatTickets = new HashSet<String>();
	
	static {
		// 1- IF EIMP and ANY other sites….THEN scrub all dupes and display ONLY
		// EIMP
		// 2- IF no EIMP, BUT Ticket Network and ANY other sites….THEN scrub all
		// dupes and display ONLY Ticket Network
		// 3- IF no EIMP and Ticket Network, BUT Stubhub and ANY other
		// sites…then scrub all dupes and display ONLY StubHub
		// 4- IF no EIMP and Ticket Network, and StubHub, BUT Ticketsnow and ANY
		// other sites…then scrub all dupes and display ONLY Ticketsnow
		// 5- IF no EIMP and TicketNetwork, StubHub, TicketsNow, BUT Event
		// Inventory and ANY other sites….then scrub all dupes and display only
		// Event Inventory
		// 6- IF no EIMP and Ticket Network and StubHub and Ticketsnow, BUT
		// Razorgator and ANY other sites…then scrub all dupes and display ONLY
		// Razorgator
		// 7- IF no EIMP, Ticket Network, StubHub, Ticketsnow, Razorgator, BUT
		// Ticketsolutions and ANY other sites…then scrub all dupes and display
		// ONLY Ticketsolutions
		// 8- IF no EIMP, TicketNetwork, StubHub, Ticketsnow, Razorgator,
		// Ticketsolutions, BUT Western States and ANY other sites…then scrub
		// all dupes and display only Western States
		// 9- IF no EIMP, Ticket Network, Stub Hub, Ticketsnow, RazorGator,
		// Ticketsoultions, Western States, but Viagogo and ANY other site…then
		// scrub all dupes and display ONLY Viagogo
		// 10- IF no EIMP, TicketNetwork, StubHub, TicketsNow, Rzrogator,
		// Ticketsolutions, Western State, Viagogo, but Seatwave and ANY other
		// site…then scrub all dupes and display ONLY Seatwave
		// 11 – IF no EIMP, TicketNetwork, StubHub, Ticketsnow, Razorgator,
		// Ticketsolutions, Western State, Viagogo, Seatwave, but GetMeIn and
		// ANY other site…then scrub all dupes and display ONLY GetMeIn
		// 12- IF no EIMP, TicektNetwork, StubHub, Ticektsnow, Razorgator,
		// Ticketsolutions, Western States, Viagogo, Seatwave, GetMeIn, but EBay
		// and ANY other site, then scrub all dupes and display ONLY Ebay.

		// orders: EIMP > Ticket Network > StubHub > TicketsNow > RazorGator >
		// TicketsSolutions > Western States > Viagogo > SeatWave > GetMeIn >
		// eBay > FanSnap > TicketMaster
		
		int rank = 1;
		for (String siteId : rankedSiteIds) {
			siteRank.put(siteId, rank);
			rank++;
		}	

		reversedRankedSiteIds = new String[rankedSiteIds.length];
		for (int i = 0; i < rankedSiteIds.length; i++) {
			reversedRankedSiteIds[rankedSiteIds.length - 1 - i] = rankedSiteIds[i];
		}	
		
		try{
			gbpFetcher = new CurrencyRSSFetcher("http://coinmill.com/rss/GBP_USD.xml");
			eurFetcher = new CurrencyRSSFetcher("http://coinmill.com/rss/EUR_USD.xml");
		} catch (FeedException feed) {
			System.out.println("Caught FeedException");
			feed.printStackTrace();
		} catch (FetcherException fete) {
			System.out.println("Caught FetcherException");
			fete.printStackTrace();
		} catch (Exception e) {
			System.out.println("Caught Exception getting currency fetcher: " + e.getMessage());
			e.printStackTrace();
		}	
		
		larryQuantitySeatTickets.add("1-1001");
		larryQuantitySeatTickets.add("2-2001-2002");
		larryQuantitySeatTickets.add("3-3001-3003");
		larryQuantitySeatTickets.add("4-4001-4004");
		larryQuantitySeatTickets.add("5-5001-5005");
		larryQuantitySeatTickets.add("6-6001-6006");
		larryQuantitySeatTickets.add("7-7001-7007");
		larryQuantitySeatTickets.add("8-8001-8008");
		larryQuantitySeatTickets.add("9-9001-9009");
	}

	/**
	 * Get ticket duplicate key. (allows to identify a unique ticket)
	 * @param tickets
	 * @param removeDuplicatePolicy
	 * @return
	 */
	private static MessageDigest md5MessageDigest = null;	
	static {
		try {
			md5MessageDigest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}

	
	/**
	 * Compute adjusted price using the price adjustment for the event and the merchant site and
	 * the rounding policy for the merchant site.
	 * @param percentAdjustment
	 * @param priceRounding
	 * @param price
	 * @param siteId
	 * @return
	 */
	public static double computeAdjustedPrice(BaseTicket ticket) {
		Double percentAdjustment = percentAdjustmentMap.get(getAdjustKey(ticket));
		if(percentAdjustment == null){
			EventPriceAdjustment priceAdjustment = TMATDAORegistry.getEventPriceAdjustmentDAO().get(ticket.getEventId(), ticket.getSiteId());
			if (priceAdjustment != null) {
				percentAdjustment = priceAdjustment.getPercentAdjustment();
				percentAdjustmentMap.put(getAdjustKey(ticket), percentAdjustment);
			}
		}
		PriceRounding priceRounding = priceRoundingMap.get(ticket.getSiteId());
		if (priceRounding == null) {
			Site site = TMATDAORegistry.getSiteDAO().get(ticket.getSiteId());
			if (site != null) {
				priceRounding = site.getPriceRounding();
				priceRoundingMap.put(ticket.getSiteId(), priceRounding);
			}		
		}
		return computeAdjustedPrice(percentAdjustment, priceRounding, 
				ticket.getCurrentPrice(), ticket.getSiteId());
	}
	/**
	 * Compute adjusted price using the price adjustment for the event and the merchant site and
	 * the rounding policy for the merchant site.
	 * @param percentAdjustment
	 * @param priceRounding
	 * @param price
	 * @param siteId
	 * @return
	 */
	public static double computeAdjustedPrice(Double percentAdjustment,
			PriceRounding priceRounding, double price, String siteId) {
		double adjustedPrice = price;
		if (percentAdjustment != null) {
			adjustedPrice = adjustedPrice * (1.0 + percentAdjustment / 100.0);
		}
		
		// note: removed SEATWAVE for this because the seatwave API
		// gives prices in USD now
		
		if(siteId.equals(Site.GET_ME_IN)){
			
			Double gbpConversion = null;
			try {
				gbpConversion = gbpFetcher.getCurrency();
			} catch (FeedException feed) {
				System.out.println("Caught FeedException");
				feed.printStackTrace();
			} catch (FetcherException fete) {
				System.out.println("Caught FetcherException");
				fete.printStackTrace();
			} catch (Exception e) {
				System.out.println("Caught Exception getting curremcy fetcher: " + e.getMessage());
				e.printStackTrace();
			}
			if(gbpConversion != null) {
				adjustedPrice = adjustedPrice * gbpConversion;
			}
		}
		
		if (priceRounding == null || priceRounding.equals(PriceRounding.NONE)) {
			// no change
		} else if (priceRounding.equals(PriceRounding.UP_TO_NEAREST_DOLLAR)) {
			adjustedPrice = Math.ceil(adjustedPrice);
		} else if (priceRounding.equals(PriceRounding.DOWN_TO_NEAREST_DOLLAR)) {
			adjustedPrice = Math.floor(adjustedPrice);
		}else if (priceRounding.equals(PriceRounding.ROUND_TO_NEAREST_DOLLAR)) {			
			if((adjustedPrice-Math.floor(adjustedPrice))<0.50){
				adjustedPrice = Math.floor(adjustedPrice);
			}else{
				adjustedPrice = Math.ceil(adjustedPrice);	
			}
		}

		return adjustedPrice;
	}
	
	private static String getAdjustKey(BaseTicket ticket) {
		return ticket.getEventId() + "-" + ticket.getSiteId();
	}
	
	/**
	 * Remove duplicate tickets.
	 * @param tickets
	 * @return
	 */
	public static List<Ticket> removeDuplicateTickets(Collection<Ticket> tickets) {
		return removeDuplicateTickets(tickets, RemoveDuplicatePolicy.SUPER_SMART);
	}
	
	
	public static List<Ticket> removeDuplicateTickets(Collection<Ticket> tickets, RemoveDuplicatePolicy removeDuplicatePolicy) {
		
		if (removeDuplicatePolicy.equals(RemoveDuplicatePolicy.NONE)) {
			return new ArrayList<Ticket>(tickets);
		}	
		
		if (removeDuplicatePolicy.equals(RemoveDuplicatePolicy.SMART)) {
			return smartRemoveDuplicateTickets(tickets, RemoveDuplicatePolicy.REMOVE_NON_WORD_CHARACTERS);
		}

		if (removeDuplicatePolicy.equals(RemoveDuplicatePolicy.SUPER_SMART)) {
			return superSmartRemoveDuplicateTickets(tickets, RemoveDuplicatePolicy.SUPER_SMART);
		}

		if (removeDuplicatePolicy.equals(RemoveDuplicatePolicy.MANUAL)) {
			return manualRemoveDuplicateTickets(tickets, RemoveDuplicatePolicy.SMART);
		}
		
		if (tickets == null) {
			return new ArrayList<Ticket>();
		}
		
		if (tickets.isEmpty()) {
			return new ArrayList<Ticket>();
		}		

		List<Ticket> resultTickets = new ArrayList<Ticket>();

		Map<String, ArrayList<Ticket>> scannedTickets = new HashMap<String, ArrayList<Ticket>>();
		for (Ticket ticket: tickets) {
			String key = getTicketDuplicateKey(ticket, removeDuplicatePolicy); 

			ArrayList<Ticket> duplicateTickets = scannedTickets.get(key);
			if (duplicateTickets != null) {
				// in case of duplicate, keep preferentially:
				// 1) cheapest ticket
				// 2) best merchant
				boolean foundDup = false;
				for(Ticket duplicateTicket : duplicateTickets){
					if((duplicateTicket.getSeat() == null)
							|| (ticket.getSeat() == null)
							|| duplicateTicket.getSeat().isEmpty()
							|| ticket.getSeat().isEmpty()
							|| duplicateTicket.getSeat().equals("*-*")
							|| ticket.getSeat().equals("*-*")
							|| duplicateTicket.getSeat().equals(ticket.getSeat())) {
						
						foundDup = true;
						if (isBetterTicket(ticket, duplicateTicket)) {
							// removed the duplicate ticket from the list
							resultTickets.remove(duplicateTicket);
							scannedTickets.get(key).remove(duplicateTicket);
							scannedTickets.get(key).add(ticket);
							resultTickets.add(ticket);
							break;
						}
					}
				}
				if(!foundDup) {
						scannedTickets.get(key).add(ticket);
						resultTickets.add(ticket);
				}
				// else we skip the current ticket
			} else {
				ArrayList<Ticket> ticketList = new ArrayList<Ticket>();
				ticketList.add(ticket);
				scannedTickets.put(key, ticketList);
				resultTickets.add(ticket);
			}
		}
		return resultTickets;
	}

	public static List<Ticket> smartRemoveDuplicateTickets(Collection<Ticket> tickets, RemoveDuplicatePolicy removeDuplicatePolicy) {
		Map<String, Collection<Ticket>> ticketMap = new HashMap<String, Collection<Ticket>>();
				
		for (String siteId: reversedRankedSiteIds) {
			for(Ticket ticket: filterTicketsBySite(siteId, tickets)) {
				String key = getTicketDuplicateKey(ticket, removeDuplicatePolicy);
				Collection<Ticket> ticketList = ticketMap.get(key);
				if (ticketList == null) {
					ticketList = new ArrayList<Ticket>();
					ticketMap.put(key, ticketList);
					ticketList.add(ticket);
				} else {
					Ticket sameTicket = getTicketWithSameSeat(ticket, ticketList);
					if (sameTicket != null) {
						if (isBetterTicket(ticket, sameTicket)) {
							ticketList.remove(sameTicket);
							ticketList.add(ticket);
						}
						continue;
					}
					
					Ticket closestTicket = getTicketWithClosestPrice(ticket, ticketList);
					if (closestTicket == null) {
						ticketList.add(ticket);
					} else if (ticket.getCurrentPrice() > closestTicket.getCurrentPrice()) {
						if (closestTicket.getCurrentPrice() / ticket.getCurrentPrice() < SIMILARITY_PRICE_THRESHOLD_PERCENT) {
							ticketList.add(ticket);							
						}
					} else {
						if (ticket.getCurrentPrice() / closestTicket.getCurrentPrice() > SIMILARITY_PRICE_THRESHOLD_PERCENT) {
							ticketList.remove(closestTicket);
							ticketList.add(ticket);	
						} else{
							ticketList.add(ticket);		
						}
					}
				}
			}
		}

		List<Ticket> resultTickets = new ArrayList<Ticket>();
		for (Collection<Ticket> list: ticketMap.values()) {
			resultTickets.addAll(list);
		}
		return resultTickets;
	}
	
	
	/**
	 * Implement the duplicate ticket removal logic:
	 * 1) remove dups based on section, row and wholesale
	 * 2) identify AO tickets
	 * 3) scrub other non-AO tickets
	 * @param tickets
	 * @param removeDuplicatePolicy
	 * @return
	 */
	public static List<Ticket> superSmartRemoveDuplicateTickets(Collection<Ticket> tickets, RemoveDuplicatePolicy removeDuplicatePolicy) {
		List<Ticket> processedMarkedDupTickets = scrubExplicitlyMarkedDuplicateTickets(tickets);
		List<Ticket> result = new ArrayList<Ticket>();
		
		// group tickets by qty, section, row, price
		// siteId => (key => {tickets})
		Map<String, Map<String, Collection<Ticket>>> sameQtySecRowPriceBySiteMap
				= createMapByKeyAndSite(processedMarkedDupTickets, new TicketKeyCallBack() {
					
					public String compute(Ticket ticket) {
						return  getTicketDuplicateByQuantitySectionRowPriceKey(ticket);
					}
				});
		
		// remove same seats with lower ranking
		removeSameSeatWithLowerRank(sameQtySecRowPriceBySiteMap);
		
		// remove most likely tickets based on seats (e.g., when comparing EI: 1 with SH * and TN *)
		//removeMostLikelySameTickets(sameQtySecRowPriceBySiteMap);
		removeDuplicateBySeactionRowPrice(sameQtySecRowPriceBySiteMap);
		final Collection<Ticket> nonAOTicketContainer = new ArrayList<Ticket>();
		
		Map<String, Collection<Ticket>> ticketResultMap = mergeTicketMap(sameQtySecRowPriceBySiteMap, new TicketMergeCallBack() {
			
			public void merge(Collection<Ticket> merged, Collection<Ticket> ticketRow) {
				// simple merge
				// FIXME improve later in the case there are different seats for the same row
				// Changes by cs as added new case of different seats with same row
				/*Collection<Ticket> row = new ArrayList<Ticket>();
				if (!merged.isEmpty()) {
					List<Ticket> toMerged = new ArrayList<Ticket>(merged);
					int diff = ticketRow.size() - merged.size();
					if (diff > 0) {
						for (Ticket ticket: ticketRow) {
							if (diff-- < 0) {
								ticket.setDupRefTicketId(toMerged.get(0).getId());
							} 
							row.add(ticket);
						}
					} else {
						for (Ticket ticket: ticketRow) {
							ticket.setDupRefTicketId(toMerged.get(0).getId());
							row.add(ticket);
						}
					}
				} else {
					row.addAll(ticketRow);
				}
				
				merged.addAll(row);
				*/
				merged.addAll(ticketRow);

			}
		});
		/* cs code starts here*/
		for (Collection<Ticket> list: ticketResultMap.values()) {
				for (Ticket ticket: list) {
					if (ticket.getDupRefTicketId() == null) {
						result.add(ticket);
					}
				}
			
		}
	

		return result;
	}
	


	
	public static List<Ticket> manualRemoveDuplicateTickets(Collection<Ticket> tickets, RemoveDuplicatePolicy initialRemoveDuplicatePolicy) {
		if (tickets == null || tickets.isEmpty()) {
			return new ArrayList<Ticket>();
		}
		
		if (!initialRemoveDuplicatePolicy.equals(RemoveDuplicatePolicy.NONE)) {
			tickets = removeDuplicateTickets(tickets, initialRemoveDuplicatePolicy);
		}
		
		Integer eventId = tickets.iterator().next().getEventId();
		Map<Integer, Long> duplicateTicketMap = new HashMap<Integer, Long>();

		//This is to make sure we don't remove more than one duplicate tickets from the the same Site 
		Map<Long, Collection<String>> keyToSiteMap = new HashMap<Long, Collection<String>>();
		
		for (DuplicateTicketMap duplicateTicket: TMATDAORegistry.getDuplicateTicketMapDAO().getDuplicateTicketMap(eventId)) {
			duplicateTicketMap.put(duplicateTicket.getTicketId(), duplicateTicket.getGroupId());
		}

		List<Ticket> resultTickets = new ArrayList<Ticket>();
		
		Map<Long, Ticket> scannedTickets = new HashMap<Long, Ticket>();
		for (Ticket ticket: tickets) {
			Long key = duplicateTicketMap.get(ticket.getId()); 

			if (key == null) {
				scannedTickets.put(key, ticket);
				resultTickets.add(ticket);	
				continue;
			} else {
				Ticket duplicateTicket = scannedTickets.get(key);
				if (duplicateTicket != null && (keyToSiteMap.get(key) == null || !keyToSiteMap.get(key).contains(ticket.getSiteId()))) {
					// in case of duplicate, keep preferentially:
					// 1) cheapest ticket
					// 2) best merchant
					
					if (isBetterTicket(ticket, duplicateTicket)) {
						// removed the duplicate ticket from the list
						resultTickets.remove(duplicateTicket);
						scannedTickets.put(key, ticket);
						resultTickets.add(ticket);
						if(keyToSiteMap.get(key) == null) {
							keyToSiteMap.put(key, new ArrayList<String>());
						}
						keyToSiteMap.get(key).add(duplicateTicket.getSiteId());
					}
					// else we skip the current ticket
					if(keyToSiteMap.get(key) == null) {
						keyToSiteMap.put(key, new ArrayList<String>());
					}
					keyToSiteMap.get(key).add(ticket.getSiteId());
				} else {
                    			scannedTickets.put(key, ticket);
                    			resultTickets.add(ticket);
				}
			}
		}
		return resultTickets;
	}
	
	/**
	 * Generate the ticket duplicate key from the fields of the ticket.
	 * @return duplicate key.
	 */

	public static String getTicketDuplicateKey(Ticket ticket, RemoveDuplicatePolicy removeDuplicatePolicy) { 	
		String section;
		if (ticket.getNormalizedSection() == null) {
			section = "";
		} else {
			section = TextUtil.removeNonWordCharacters(ticket.getNormalizedSection());
		}
	
		String row;
		if (ticket.getNormalizedRow() == null) {
			row = "";
		} else {
			row = TextUtil.removeNonWordCharacters(ticket.getNormalizedRow());
		}
	
		String admitoneTicket = "";
		if(ticket.isAdmitOneTicket()){
			admitoneTicket = "ADMT1";
		}
		String key = "" + admitoneTicket
			 	+ ticket.getTicketStatus()
		 		+ ticket.getRemainingQuantity() + "," + section + ","
				+ row;
		key=key.toLowerCase();
		
		String duplicateKeyHash;
		
		synchronized (md5MessageDigest) {			
			duplicateKeyHash = new String(Hex.encodeHex(md5MessageDigest.digest(key.getBytes())));
		}

		return duplicateKeyHash;
	}
	
	
	/**
	 * Return true if ticket1 is better than ticket2 by using the following rules:
	 * - cheaper price
	 * - if price are equals compare the site ranking
	 * @param ticket1
	 * @param ticket2
	 * @return
	 */
	public static boolean isBetterTicket(Ticket ticket1, Ticket ticket2) {
/*
		boolean isAdm1 = isAdmitOneTicket(ticket1);
		boolean isAdm2 = isAdmitOneTicket(ticket2);
		
		if (isAdm1 && !isAdm2) {
			return true;
		}

		if (!isAdm1 && isAdm2) {
			return false;
		}
*/
		return (ticket1.getCurrentPrice() < ticket2.getCurrentPrice()
				|| (ticket1.getCurrentPrice().equals(ticket2.getCurrentPrice()) 
						&& siteRank.get(ticket1.getSiteId()) < siteRank.get(ticket2.getSiteId())));
	}



	private static Collection<Ticket> filterTicketsBySite(String siteId, Collection<Ticket> tickets) {
		Collection<Ticket> result = new ArrayList<Ticket>();
		for (Ticket ticket: tickets) {
			if (ticket.getSiteId().equals(siteId)) {
				result.add(ticket);
			}
		}
		
		return result;
	}
	
	
	public static Ticket getTicketWithSameSeat(Ticket srcTicket, Collection<Ticket> tickets) {
		String[] seatTokens = getStartEndSeatFromSeat(srcTicket.getSeat());
		
		if (seatTokens == null) {
			return null;
		}
		
		String srcStartSeat = seatTokens[0];
		String srcEndSeat = seatTokens[1];
		
		for (Ticket ticket: tickets) {
			seatTokens = getStartEndSeatFromSeat(ticket.getSeat());
			
			if (seatTokens == null) {
				continue;
			}
			
			String startSeat = seatTokens[0];
			String endSeat = seatTokens[1];
			
			if (srcStartSeat.equals(startSeat) && srcEndSeat.equals(endSeat)) {
				return ticket;
			}
		}
		
		return null;
	}

	
	private static Ticket getTicketWithClosestPrice(Ticket srcTicket, Collection<Ticket> tickets) {
		if (tickets == null || tickets.isEmpty()) {
			return null;
		}
		
		Ticket closestTicket = null;
		for (Ticket ticket: tickets) {
			if (ticket.getSiteId().equals(srcTicket.getSiteId())) {
				continue;
			}
			
			Double price = srcTicket.getCurrentPrice();
			if (closestTicket == null || Math.abs(ticket.getCurrentPrice() - price) < Math.abs(closestTicket.getCurrentPrice() - price)) {
				closestTicket = ticket;
			}
		}
		
		return closestTicket;
	}
	
	public static List<Ticket> scrubExplicitlyMarkedDuplicateTickets(Collection<Ticket> tickets) {
		if (tickets == null || tickets.isEmpty()) {
			return new ArrayList<Ticket>();
		}
		
		Map<Integer, Ticket> ticketMap = new HashMap<Integer, Ticket>();
		for (Ticket ticket: tickets) {
			ticketMap.put(ticket.getId(), ticket);
		}
		
		List<Ticket> result = new ArrayList<Ticket>();
		Map<Integer, Long> groupIdByTicketId = new HashMap<Integer, Long>();
		Map<Long, List<Ticket>> ticketListByGroupId = new HashMap<Long, List<Ticket>>();
		
		// get duplicate marked tickets
		for (DuplicateTicketMap map: TMATDAORegistry.getDuplicateTicketMapDAO().getDuplicateTicketMap(tickets.iterator().next().getEventId())) {
			groupIdByTicketId.put(map.getTicketId(), map.getGroupId());
			List<Ticket> ticketList = ticketListByGroupId.get(map.getGroupId());
			if (ticketList == null) {
				ticketList = new ArrayList<Ticket>();
				ticketListByGroupId.put(map.getGroupId(), ticketList);
			}
			Ticket ticket = ticketMap.get(map.getTicketId());
			if (ticket != null) {
				ticketList.add(ticket);
			}
		}
		
		// delete based on price and exchanges
		
		for (Ticket ticket: tickets) {
			Long groupId = groupIdByTicketId.get(ticket.getId());
			if (groupId != null) {
				List<Ticket> ticketList = ticketListByGroupId.get(groupId);
				if (ticketList != null) {
					Collections.sort(ticketList, new Comparator<Ticket>() {
	
						public int compare(Ticket t1, Ticket t2) {
							int cmp = t1.getAdjustedCurrentPrice().compareTo(t2.getAdjustedCurrentPrice());
							
							if (cmp != 0) {
								return cmp;
							}
							
							return siteRank.get(t1.getSiteId()).compareTo(siteRank.get(t1.getSiteId()));
						}
					});
					ticketListByGroupId.remove(groupId);
					// mark other tix as dupe of the best one
					int i = 0;
					Ticket bestTicket = null;
					for (Ticket dupTicket: ticketList) {
						if (i++ == 0) {
							bestTicket = dupTicket;
							result.add(bestTicket);
						} else {
							dupTicket.setDupRefTicketId(bestTicket.getId());
						}
					}
				}
			} else {
				result.add(ticket);
			}
		}
		
		return result;
	}
	
	private static Map<String, Map<String, Collection<Ticket>>> createMapByKeyAndSite(Collection<Ticket> tickets, TicketKeyCallBack callback) {
		Map<String, Map<String, Collection<Ticket>>> map = new HashMap<String, Map<String, Collection<Ticket>>>();
		
		for (String siteId: rankedSiteIds) {
			Collection<Ticket> filteredTickets = filterTicketsBySite(siteId, tickets);
			if (filteredTickets == null || filteredTickets.isEmpty()) {
				continue;
			}
			Map<String, Collection<Ticket>> ticketMap = new HashMap<String, Collection<Ticket>>();
			map.put(siteId, ticketMap);
			Collection<Ticket> sameKeyTickets =null;
			for(Ticket ticket: filteredTickets) {
				String key = callback.compute(ticket);
				sameKeyTickets = ticketMap.get(key);
				if (sameKeyTickets == null) {
					sameKeyTickets = new ArrayList<Ticket>();
					ticketMap.put(key, sameKeyTickets);
				}
				
				sameKeyTickets.add(ticket);
			}
			if(sameKeyTickets!=null && !sameKeyTickets.isEmpty()){
				Collections.sort((List<Ticket>)(sameKeyTickets), new Comparator<Ticket>() {
					
					public int compare(Ticket t1, Ticket t2) {
						int cmp = t1.getAdjustedCurrentPrice().compareTo(t2.getAdjustedCurrentPrice());
						
						if (cmp != 0) {
							return cmp;
						}
						
						return siteRank.get(t1.getSiteId()).compareTo(siteRank.get(t1.getSiteId()));
					}
				});
			}
		}
		return map;
	}

	public static List<Ticket> removeAdmitoneTickets(Collection<Ticket> nonAOTickets,Event event,Integer ticketNetworkExchagneId,boolean allowSectionRange) throws Exception{
		Collection<AdmitoneInventory> admitoneTickets = null;
		try {
			admitoneTickets = TMATDAORegistry.getAdmitoneInventoryDAO().getAllInventoryByAdmitOneEventId(ticketNetworkExchagneId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}
		Map<String,AdmitoneInventory> admitoneTicketMap = new HashMap<String,AdmitoneInventory>();
		if(admitoneTickets!=null){
			for(AdmitoneInventory admitoneTicket:admitoneTickets){
//				String seat = admitoneTicket.getSeat()==null?"":admitoneTicket.getSeat();
				//String section = admitoneTicket.getSection()==null?"":admitoneTicket.getSection().substring(0,admitoneTicket.getSection().length()-1).toLowerCase();
				String section = admitoneTicket.getSection()==null?"":admitoneTicket.getSection().toLowerCase();
				//String row = admitoneTicket.getRow()==null?"":admitoneTicket.getRow().substring(0,admitoneTicket.getRow().length()-1).toLowerCase();
				String row = admitoneTicket.getRow()==null?"":admitoneTicket.getRow().toLowerCase();
				String key = admitoneTicket.getQuantity() + "-" + section + "-" + row;
				admitoneTicketMap.put(key,admitoneTicket);
				
			}
		}
		List<Ticket> result= new ArrayList<Ticket>();
		for(Ticket ticket:nonAOTickets){
			String seat = ticket.getSeat()==null?"":ticket.getSeat();
			String section = ticket.getSection()==null?"":ticket.getSection().toLowerCase();
			String row = ticket.getRow()==null?"":ticket.getRow().toLowerCase();
			String key = ticket.getQuantity() + "-" + section + "-" + row;
			String qtySeatKey = ticket.getQuantity() + "-" + seat;
			//Tamil : Do not consider section range tickets for ticketnetwork
			if((!allowSectionRange && ( row.contains("-") || row.toLowerCase().contains(" or "))) ||
					section.contains("-") || section.toLowerCase().contains(" or ") ||
					larryQuantitySeatTickets.contains(qtySeatKey)) {
				
				continue;
			}
			
			AdmitoneInventory admitoneTicket = admitoneTicketMap.get(key);
			if(admitoneTicket !=null){
				String admitoneSeat = admitoneTicket.getSeat();
				if(seat!=null && admitoneSeat!=null && seat.equals(admitoneSeat)){
					continue;
				}else if(admitoneTicket.getCurrentPrice().equals(ticket.getCurrentPrice())){
					continue;
				}
			}
			result.add(ticket);
		}
		return result;
	}
	
	private interface TicketKeyCallBack {
		String compute(Ticket ticket);
	}

	
	
	public static String getTicketDuplicateByQuantitySectionRowPriceKey(Ticket ticket) { 	
		String section;
		if (ticket.getNormalizedSection() == null) {
			section = "";
		} else {
			section = TextUtil.removeNonWordCharacters(ticket.getNormalizedSection());
		}
	
		String row;
		if (ticket.getNormalizedRow() == null) {
			row = "";
		} else {
			row = TextUtil.removeNonWordCharacters(ticket.getNormalizedRow());
		}
	
		String key = "" + 
		 		+ ticket.getRemainingQuantity() + "," + section + ","
				+ row;// + "," + ticket.getAdjustedCurrentPrice();

		String duplicateKeyHash;
		
		synchronized (md5MessageDigest) {			
			duplicateKeyHash = new String(Hex.encodeHex(md5MessageDigest.digest(key.getBytes())));
		}

		return duplicateKeyHash;
	}


	
	
	private static void removeSameSeatWithLowerRank(Map<String, Map<String, Collection<Ticket>>> ticketMap) {
//		Set<String> seats = new HashSet<String>();
		Map<String, Ticket> seatMap = new HashMap<String, Ticket>();
		for (String siteId: rankedSiteIds) {
			Map<String, Collection<Ticket>> map = ticketMap.get(siteId);
			if (map == null) {
				continue;
			}
			
			for (Entry<String, Collection<Ticket>> entry: map.entrySet()) {
				Collection<Ticket> tickets = entry.getValue();
				for (Ticket ticket: tickets) {
					if (isSeatNull(ticket.getSeat())) {
						continue;
					}
					
					String seatKey = getTicketDuplicateKeyBySectionRowSeatAndQuantity(ticket);
					Ticket potentialDup = seatMap.get(seatKey); 
					if (potentialDup != null) {
						if (ticket.getCurrentPrice() >= potentialDup.getCurrentPrice()) {
							ticket.setDupRefTicketId(potentialDup.getId());
//							seatMap.put(seatKey, ticket);
						} else {
							potentialDup.setDupRefTicketId(ticket.getId());
							seatMap.put(seatKey, ticket);
						}
					} else {
						seatMap.put(seatKey, ticket);
					}
				}
			}
		}
	}
	
	private static void removeDuplicateBySeactionRowPrice(Map<String, Map<String, Collection<Ticket>>> ticketMap) {
//		Set<String> seats = new HashSet<String>();
		Map<String, List<Ticket>> seatMap = new HashMap<String, List<Ticket>>();
		List<Integer> ticketRestoreMap= new ArrayList<Integer>();
		for (String siteId: rankedSiteIds) {
			Map<String, Collection<Ticket>> map = ticketMap.get(siteId);
			if (map == null) {
				continue;
			}
			
			
			for (Entry<String, Collection<Ticket>> entry: map.entrySet()) {
				Collection<Ticket> tickets = entry.getValue();
				for (Ticket ticket: tickets) {
					if(ticket.getDupRefTicketId()!=null){
						continue;
					}
					String seatKey = getTicketDuplicateKeyBySectionRowAndQuantity(ticket);
					List<Ticket> potentialDupList = seatMap.get(seatKey); 
					if (potentialDupList != null && !potentialDupList.isEmpty()) {
						boolean flag=true;
						for(Ticket potentialDupTicket:potentialDupList){
							if(!potentialDupTicket.getSiteId().equals(ticket.getSiteId())){
								if(ticketRestoreMap.contains(potentialDupTicket.getId())){
									continue;
								}
								if(isSeatNull(potentialDupTicket.getSeat()) || isSeatNull(ticket.getSeat())){
//									System.out.println(potentialDupTicket.getCurrentPrice()+ ":" + ticket.getCurrentPrice());
//									System.out.println("Kem:" + (potentialDupTicket.getCurrentPrice()* (1 + SIMILARITY_PRICE_DIFF_THRESHOLD_PERCENT))+ ":" + ticket.getCurrentPrice());
//									System.out.println("Kem-1" + (potentialDupTicket.getCurrentPrice() + ":" + (ticket.getCurrentPrice()* (1 + SIMILARITY_PRICE_DIFF_THRESHOLD_PERCENT))));
//									System.out.println(potentialDupTicket.getCurrentPrice()<ticket.getCurrentPrice());
//									System.out.println(((potentialDupTicket.getCurrentPrice()* (1 + SIMILARITY_PRICE_DIFF_THRESHOLD_PERCENT)) >= ticket.getCurrentPrice()));
									if((potentialDupTicket.getPurchasePrice().equals(ticket.getPurchasePrice())) ||
									(potentialDupTicket.getPurchasePrice()<ticket.getPurchasePrice() && 
									((potentialDupTicket.getPurchasePrice()* (1 + SIMILARITY_PRICE_DIFF_THRESHOLD_PERCENT)) >= ticket.getPurchasePrice()))){
										ticket.setDupRefTicketId(potentialDupTicket.getId());
										ticketRestoreMap.add(potentialDupTicket.getId());
										flag=false;
										break;
									}else if(potentialDupTicket.getPurchasePrice()>ticket.getPurchasePrice() && 
											(potentialDupTicket.getPurchasePrice() <= (ticket.getPurchasePrice()* (1 + SIMILARITY_PRICE_DIFF_THRESHOLD_PERCENT)))){
										potentialDupTicket.setDupRefTicketId(ticket.getId());
										potentialDupList.remove(potentialDupTicket);
										addInList(potentialDupList,ticket);
										flag=false;
										break;
									}
								
								}else if (potentialDupTicket.getSeat().equals(ticket.getSeat())){
									if(potentialDupTicket.getPurchasePrice()>ticket.getPurchasePrice()){
										potentialDupTicket.setDupRefTicketId(ticket.getId());
										potentialDupList.remove(potentialDupTicket);
										addInList(potentialDupList,ticket);
										flag=false;
										break;
//										seatMap.put(seatKey, ticket);
									}else{
										ticket.setDupRefTicketId(potentialDupTicket.getId());
									}
								}
							}else{
								addInList(potentialDupList,ticket);
								flag=false;
								break;
							}
						}
						if(flag){
							addInList(potentialDupList,ticket);
						}
					} else {
						potentialDupList=new ArrayList<Ticket>();
						addInList(potentialDupList,ticket);
						seatMap.put(seatKey, potentialDupList);
					}
				}
			}
			ticketRestoreMap.clear();
		}
	}
	
	
	private static Map<String, Collection<Ticket>> mergeTicketMap(Map<String, Map<String, Collection<Ticket>>> ticketMap, TicketMergeCallBack callback) {
		Map<String, Collection<Ticket>> result = new HashMap<String, Collection<Ticket>>();
		for (String siteId: rankedSiteIds) {
			if (ticketMap.get(siteId) == null) {
				continue;
			}
			
			for(Entry<String, Collection<Ticket>> entry: ticketMap.get(siteId).entrySet()) {
				Collection<Ticket> groupMapTickets = result.get(entry.getKey());
				
				if (groupMapTickets == null) {
					groupMapTickets = new ArrayList<Ticket>();
					result.put(entry.getKey(), groupMapTickets);
				}
				
				callback.merge(groupMapTickets, entry.getValue());
			}
		}
		return result;
	}
	
	private interface TicketMergeCallBack {
		void merge(Collection<Ticket> merged, Collection<Ticket> ticketRow);
	}
	
	
	private static String[] getStartEndSeatFromSeat(String seat) {
		if (seat == null || seat.trim().isEmpty()) {
			return null;
		}

		Matcher matcher = singleSeatPattern.matcher(seat);
		if (matcher.find()) {
			return new String[]{matcher.group(1), matcher.group(1)};
		} else {
			matcher = seatRangePattern.matcher(seat);
			if (matcher.find()) {
				return new String[]{matcher.group(1), matcher.group(2)};
			}
		}
		return null;
	}
	
	
	
	public static boolean isSeatNull(String seat) {
		if (seat == null) {
			return true;
		}
		
		seat = seat.trim().toUpperCase();
		
		if (seat.isEmpty()
			|| seat.equals("*")
			|| seat.equals("-")
			|| seat.equals("*-*")
			|| seat.contains("N/A")) {
			return true;
		}
		
		return false;
	}
	
	
	public static String getTicketDuplicateKeyBySectionRowSeatAndQuantity(Ticket ticket) {
		String section;
		if (ticket.getNormalizedSection() == null) {
			section = "";
		} else {
			section = TextUtil.removeNonWordCharacters(ticket.getNormalizedSection()).trim();
		}
	
		String row;
		if (ticket.getNormalizedRow() == null) {
			row = "";
		} else {
			row = TextUtil.removeNonWordCharacters(ticket.getNormalizedRow()).trim();
		}

		String key = ""
			 	+ ticket.getTicketStatus()
		 		+ "," + ticket.getRemainingQuantity() + "," + section + "," + row + "," + normalizeSeat(ticket.getSeat()) ;

		String duplicateKeyHash;
		
		synchronized (md5MessageDigest) {			
			duplicateKeyHash = new String(Hex.encodeHex(md5MessageDigest.digest(key.getBytes())));
		}

		return duplicateKeyHash;
	}
	
	public static String normalizeSeat(String seat) {
		if (isSeatNull(seat)) {
			return "";
		}
		if(!seat.contains("-")){
			Pattern pattern = Pattern.compile("^\\d+$");
			Matcher matcher = pattern.matcher(seat);
			if(matcher.find()){
				seat=seat+"-" + seat;
			}
		}
		return seat.replaceAll("0*(\\d+)-0*(\\d+)", "$1-$2").replaceAll("^0*(\\d+)$", "$1");
	}
	
	public static void addInList(List<Ticket> list,Ticket ticket){
		list.add(ticket);
		Collections.sort(list, new Comparator<Ticket>() {
			
			public int compare(Ticket t1, Ticket t2) {
				int cmp = t1.getAdjustedCurrentPrice().compareTo(t2.getAdjustedCurrentPrice());
				
				if (cmp != 0) {
					return cmp;
				}
				
				return siteRank.get(t1.getSiteId()).compareTo(siteRank.get(t1.getSiteId()));
			}
		});
	}
	
	
	public static String getTicketDuplicateKeyBySectionRowAndQuantity(Ticket ticket) {
		String section;
		if (ticket.getNormalizedSection() == null) {
			section = "";
		} else {
			section = TextUtil.removeNonWordCharacters(ticket.getNormalizedSection()).trim();
		}
	
		String row;
		if (ticket.getNormalizedRow() == null) {
			row = "";
		} else {
			row = TextUtil.removeNonWordCharacters(ticket.getNormalizedRow()).trim();
		}

		String key = ""
			 	+ ticket.getTicketStatus()
		 		+ "," + ticket.getRemainingQuantity() + "," + section + "," + row ;

		String duplicateKeyHash;
		
		synchronized (md5MessageDigest) {			
			duplicateKeyHash = new String(Hex.encodeHex(md5MessageDigest.digest(key.getBytes())));
		}

		return duplicateKeyHash;
	}
	
	
	
	/**
	 * Assign to a set of tickets their categories
	 * @param tickets
	 * @param categories
	 * @param scheme
	 */
	public static void preAssignCategoriesToTickets(Collection<Ticket> tickets, Collection<Category> categories) {
		Map<Integer, Category> categoryMap = new HashMap<Integer, Category>();
		
		for (Category category: categories) {
			categoryMap.put(category.getId(), category);
		}
//		for (Ticket ticket: tickets) {
			tickets = Categorizer.computeCategoryForTickets(tickets, categoryMap);
//		}
	}
	
	public static Collection<Ticket> preAssignCategoriesToTickets(Collection<Ticket> tickets, Collection<Category> categories,Event event) throws Exception {
		Map<Integer, Category> categoryMap = new HashMap<Integer, Category>();
		
		for (Category category: categories) {
			categoryMap.put(category.getId(), category);
		}
//		for (Ticket ticket: tickets) {
			return Categorizer.computeCategoryForTickets(tickets, categoryMap,event);
//		}
	}
	
	public static void getPurchasePrice(Ticket t,Map<String, DefaultPurchasePrice> defaultPurchasePriceMap,Integer lotSize,Map<String, ManagePurchasePrice> tourPriceMap){
		Double tempPrice = t.getCurrentPrice();
		if (lotSize==null || lotSize==0){
			lotSize = t.getQuantity();
		}
//		Event event = t.getEvent();
//		Map<String, ManageTourPrice> tourPriceMap = new HashMap<String, ManageTourPrice>();
//		Long time = manageTourPriceTimeMap.get(tour.getTmatTourId());
//		if(tourPriceMap==null){
//			tourPriceMap= new HashMap<String, ManageTourPrice>();
//		}
		String mapKey = t.getSiteId()+ "-" + (t.getTicketDeliveryType()==null?"REGULAR":t.getTicketDeliveryType());
//		Long now =new Date().getTime();
//		if(tourPriceMap.get(mapKey)==null || now-time>60*60*1000){
//			System.out.println("DB called for manage price stated @ " + new Date());
//			for(ManageTourPrice tourPrice:manageTourPricelist){
//				tourPriceMap.put(tourPrice.getExchange()+"-" + tourPrice.getTicketType(),tourPrice);
//			}
//			System.out.println("DB called for manage price ended @ " + new Date());
//			manageTourPriceMap.put(tour.getTmatTourId(),tourPriceMap);
//			manageTourPriceTimeMap.put(tour.getTmatTourId(),now);
//		}
		ManagePurchasePrice managePurchasePrice=tourPriceMap.get(mapKey);
		
		if(managePurchasePrice!=null){
			double serviceFee=managePurchasePrice.getServiceFee();
			double  shippingFee=managePurchasePrice.getShipping();
			int currencyType=managePurchasePrice.getCurrencyType();
			tempPrice=(currencyType==1?((lotSize * tempPrice*(1+serviceFee/100)) + shippingFee)/lotSize:((lotSize * tempPrice) + serviceFee + shippingFee)/lotSize);
		}else{
			DefaultPurchasePrice defaultPurchasePrice = defaultPurchasePriceMap.get(mapKey);
			if(defaultPurchasePrice!=null){
				double serviceFee=defaultPurchasePrice.getServiceFees();
				double  shippingFee=defaultPurchasePrice.getShipping();
				int currencyType=defaultPurchasePrice.getCurrencyType();
				tempPrice=(currencyType==1?((lotSize * tempPrice*(1+serviceFee/100)) + shippingFee)/lotSize:((lotSize * tempPrice) + serviceFee + shippingFee)/lotSize);
			}
		}
		t.setPurchasePrice(tempPrice);
	}
	
	public static void getCheapestTicketByQty(Set<Ticket> ticketsList,RewardthefanCatsExchangeEvent exEvent,
			int quantity,int sectionEligibleMinEntries,RewardthefanCategoryTicket rewardthefanCategoryTicket,String exposure,Double rptFactor) throws Exception {
		
		
		Set<Ticket> sectionTicketsForQty = new TreeSet<Ticket>(new Comparator<Ticket>() {
			public int compare(Ticket ticket1, Ticket ticket2) {
				int cmp = ticket1.getPurchasePrice().compareTo(
						ticket2.getPurchasePrice());
				if (cmp < 0) {
					return -1;
				}
				if (cmp > 0) {
					return 1;
				}
				// if same price get the one with less quantity first
				int cmp2 = ticket1.getQuantity().compareTo(
						ticket2.getQuantity());
				if (cmp2 < 0) {
					return -1;
				}

				if (cmp2 > 0) {
					return 1;
				}
				return ticket1.getId().compareTo(ticket2.getId());
			}	

		});
		
		
		for (Ticket ticket : ticketsList) {
			for(int qty : TicketUtil.getPartition(ticket.getQuantity())){
				if(quantity==qty){
					sectionTicketsForQty.add(ticket);
					break;
				}
			}
		}
		
		if(!sectionTicketsForQty.isEmpty()) {
			/*if(product.getName().equals("RewardTheFan Listings")) {
				Iterator<Ticket> iterator = sectionTicketsForQty.iterator();
				Ticket ticket = iterator.next();
				if(ticket.isZoneCategoryticket()) {
					categoryTicket.createCategoryTicket(ticket);
					categoryTicket.setBaseTicketOne(0);
					categoryTicket.setBaseTicketTwo(0);
					categoryTicket.setBaseTicketThree(0);
					categoryTicket.setBaseTicketOnePurPrice(0.0);
					categoryTicket.setBaseTicketTwoPurPrice(0.0);
					categoryTicket.setBaseTicketThreePurPrice(0.0);
					
				} else {
					if(sectionTicketsForQty.size() >= sectionEligibleMinEntries) {
				          getSafeCheapestTicket(sectionTicketsForQty, exEvent,categoryTicket);
				    }
				}
			
			} else {*/
				if(sectionTicketsForQty.size() >= sectionEligibleMinEntries) {
			          getSafeCheapestTicket(sectionTicketsForQty, exEvent,rewardthefanCategoryTicket,exposure,rptFactor);
			    }
			//}
		}
		
	}
	
	private static void getSafeCheapestTicket(Set<Ticket> sortedTickets,RewardthefanCatsExchangeEvent rtfExchangeEvent,
			RewardthefanCategoryTicket rewardthefanCategoryTicket,String exposure,Double rptFactor) throws Exception{
		
		Integer baseTicketOne,baseTicketTwo;
		Double baseTicketOnePurPrice,baseTicketTwoPurPrice;
//		LastRowMiniCatsCategoryTicket categoryTicket = null;
		
		Integer exposureAmt = Integer.parseInt(exposure.split("-")[0]);
		
		//Exposure hot coded to 1-OXP for allow section range events section range sections.
		/*if(isAllowRangeSection) {
			exposureAmt = 1;
		}*/
//		String exposuretype = exposure.split("-")[1];

		Iterator<Ticket> iterator = sortedTickets.iterator();
		Iterator<Ticket> currentTicketIterator = sortedTickets.iterator();
		if(!iterator.hasNext()){
			return ;
		}
		Ticket currentTicket=null;
		Ticket nextTicket=null;
			currentTicket= iterator.next();
			currentTicketIterator.next();
		int count=0;
		for(Ticket ticket=currentTicket;;ticket=currentTicketIterator.next()){
			try{
				baseTicketOne = 0;
				baseTicketTwo = 0;
				baseTicketOnePurPrice = 0.0;
				baseTicketTwoPurPrice = 0.0;
				
				Map<Integer, Ticket> map = new HashMap<Integer, Ticket>();
				count++;
				int ii=0;
				Iterator<Ticket> nextTicketIterator = sortedTickets.iterator();
				while(nextTicketIterator.hasNext() && ii<count){
					nextTicketIterator.next();
					ii++;
				}
				if(nextTicketIterator.hasNext()){
					nextTicket=nextTicketIterator.next();
				}else if(1!=exposureAmt){
					return ;
				}
				
				boolean flag=false;
				if(1==exposureAmt){
					rewardthefanCategoryTicket.createCategoryTicket(ticket);
					rewardthefanCategoryTicket.setBaseTicketOne(baseTicketOne);
					rewardthefanCategoryTicket.setBaseTicketTwo(baseTicketTwo);
					rewardthefanCategoryTicket.setBaseTicketOnePurPrice(baseTicketOnePurPrice);
					rewardthefanCategoryTicket.setBaseTicketTwoPurPrice(baseTicketTwoPurPrice);
					break;
				}else{
					int j=1;
					for(int i=exposureAmt ;i>1  && iterator != null;i--) {
						if(ticket.getPurchasePrice()*(1+rptFactor/100)>=(nextTicket.getPurchasePrice())){
							if(j==1) {
								baseTicketOne = nextTicket.getId();
								baseTicketOnePurPrice = nextTicket.getPurchasePrice();
							} else if(j==2) {
								baseTicketTwo = nextTicket.getId();
								baseTicketTwoPurPrice = nextTicket.getPurchasePrice();
							}
							j++;
							map.put(1 + exposureAmt-i, ticket);
							
							if(j==exposureAmt){
								break;
							}
							if(nextTicketIterator.hasNext() ){
								nextTicket=nextTicketIterator.next();
							}else{
								flag=true;
								break;
							}
						}else{
							flag=true;
							break;
						}
					}
					if(!flag){
						rewardthefanCategoryTicket.createCategoryTicket(map.get(1));
						rewardthefanCategoryTicket.setBaseTicketOne(baseTicketOne);
						rewardthefanCategoryTicket.setBaseTicketTwo(baseTicketTwo);
						rewardthefanCategoryTicket.setBaseTicketOnePurPrice(baseTicketOnePurPrice);
						rewardthefanCategoryTicket.setBaseTicketTwoPurPrice(baseTicketTwoPurPrice);
						break;
					}
				}
			}catch (Exception e) {
				System.out.println( rtfExchangeEvent.getEventId() + ":" +  e.fillInStackTrace());
				throw e;
			}
			if(!currentTicketIterator.hasNext()){
				break;
			}
		}
//		return null;
	}
	
	/**
	 * Tell if a quantity can be a partition of a bigger quantity.
	 * For instance a ticket of quantity 5 can be subdivided in 1, 2, 3, 5
	 * @param quantity
	 * @param totalQuantity
	 * @return
	 */
	public static Integer[] getPartition(int quantity) {
		
		if(quantity == 1){
			return new Integer[]{1};
		}else if(quantity == 2){
			return new Integer[]{2};
		}else if(quantity == 3) {
			return new Integer[]{1,3};
		}else if(quantity == 4) {
			return new Integer[]{2,4};
		}else if(quantity == 5) {
			return new Integer[]{1,2,3,5};
		}else if(quantity == 6) {
			return new Integer[]{1,2,4,6};
		}else if(quantity == 7) {
			return new Integer[]{1,2,3,4,5,7};
		}else if(quantity == 8) {
			return new Integer[]{1,2,3,4,5,6,8};
		}else if(quantity == 9) {
			return new Integer[]{1,2,3,4,5,6,7,9};
		}else if(quantity == 10) {
			return new Integer[]{1,2,3,4,5,6,7,8,10};
		}else if(quantity == 11) {
			return new Integer[]{1,2,3,4,5,6,7,8,9,11};
		}else{
			return new Integer[]{1,2,3,4,5,6,7,8,9,10,12};
		}
	}
	
}


	