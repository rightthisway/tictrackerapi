package com.rtw.tmat.utils;

import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.LoyaltySettings;




public class RewardConversionUtil {
	
	
	
	public static Double getRewardDoller(Double activeRewardPoints){
		Double activeRewardDollers = 0.00;
		if(null != activeRewardPoints){
			LoyaltySettings loyaltySettings = DAORegistry.getLoyaltySettingsDAO().getActivetLoyaltySettings();
			Double temp = activeRewardPoints * loyaltySettings.getDollerConversion();
			activeRewardDollers = Math.floor(temp);
		}
		return activeRewardDollers;
	}
	
	public static Integer getRewardPoints(Double dollers){
		Double rewardPoints = 0.00;
		LoyaltySettings loyaltySettings = DAORegistry.getLoyaltySettingsDAO().getActivetLoyaltySettings();
		rewardPoints = dollers * loyaltySettings.getRewardConversion();
		rewardPoints = Math.floor(rewardPoints);
		return rewardPoints.intValue();
	}
	
	public static Double getRewardDoller(Integer activeRewardPoints,Double dollerConverstion){
		Double rewardDollers = 0.00;
		if(null != activeRewardPoints){
			Double temp = activeRewardPoints * dollerConverstion;
			rewardDollers = Math.floor(temp);
		}
		return rewardDollers;
	}
	
	/*public static Double getBalanceRewardPoints(Double orderTotal,Double dollerConverstion){
		Double rewardDollers = 0.00;
		if(null != activeRewardPoints){
			Double temp = activeRewardPoints * dollerConverstion;
			rewardDollers = Math.floor(temp);
		}
		return rewardDollers;
	}*/
	
	
	public static Integer getRewardPoints(Double dollers,Double rewardConverstion){
		Double rewardPoints = 0.00;
		rewardPoints = dollers * rewardConverstion;
		rewardPoints = Math.floor(rewardPoints);
		return rewardPoints.intValue();
	}
	
	
	
	public static void main(String[] args) {
		
		Double activeRewardPoints = 500.00; 
		Double dollerConverstion = 0.1;
		
		System.out.println(getRewardPoints(activeRewardPoints, dollerConverstion));
		
	}
	
}
