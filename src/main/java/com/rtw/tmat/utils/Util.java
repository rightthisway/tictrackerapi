package com.rtw.tmat.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.math.RoundingMode;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.rtw.tmat.dao.implementation.TMATDAORegistry;
import com.rtw.tmat.data.Category;
import com.rtw.tmat.data.CategoryMapping;
import com.rtw.tmat.data.Event;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.AffiliateCashReward;
import com.rtw.tracker.datas.AffiliateCashRewardHistory;
import com.rtw.tracker.datas.CategoryTicket;
import com.rtw.tracker.datas.CategoryTicketGroup;
import com.rtw.tracker.datas.Customer;
import com.rtw.tracker.datas.CustomerLoyalty;
import com.rtw.tracker.datas.CustomerLoyaltyHistory;
import com.rtw.tracker.datas.CustomerOrder;
import com.rtw.tracker.datas.EventDetails;
import com.rtw.tracker.datas.Invoice;
import com.rtw.tracker.datas.InvoiceAudit;
import com.rtw.tracker.datas.InvoiceRefund;
import com.rtw.tracker.datas.InvoiceTicketAttachment;
import com.rtw.tracker.datas.OpenOrderStatus;
import com.rtw.tracker.datas.OrderTicketGroupDetails;
import com.rtw.tracker.datas.RTFPromotionalOfferTracking;
import com.rtw.tracker.datas.RealTicketSectionDetails;
import com.rtw.tracker.datas.Role;
import com.rtw.tracker.datas.Ticket;
import com.rtw.tracker.datas.TicketGroup;
import com.rtw.tracker.datas.TicketStatus;
import com.rtw.tracker.datas.TrackerBrokers;
import com.rtw.tracker.datas.TrackerUser;
import com.rtw.tracker.datas.UserAction;
import com.rtw.tracker.enums.InvoiceAuditAction;
import com.rtw.tracker.enums.InvoiceStatus;
import com.rtw.tracker.enums.RewardStatus;
import com.rtw.tracker.pojos.CategoryAndLongTickets;

public class Util {

	private static DecimalFormat df2 = new DecimalFormat(".##");
	private static Double loyalFanPercentage = 0.0;
	private static final String CHARACTER_SET = "0123456789abcdefghijkmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ";
	private static Random rnd = new Random();
	private static Integer maxLength = 8;
	
	public static GsonBuilder getGsonBuilder(){
		GsonBuilder builder = new GsonBuilder(); 

		// Register an adapter to manage the date types as long values 
		builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() { 
		   public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		      return new Date(json.getAsJsonPrimitive().getAsLong()); 
		   } 
		});
		return builder;
	}
	
	public static String generateEncryptedKey(String keyToHash) {
        String encryptedKey = null;
        try {
            String uniqueString = UUID.randomUUID().toString();
    		String sub = uniqueString.substring(0, 6);
    		String key = doBase64Encryption(sub + keyToHash);
    		key = key.replaceAll("=", "");
    		encryptedKey = key.substring(0, 6);
            
            String twoChracter = "";
            keyToHash = keyToHash.replaceAll(" ","");
            if(keyToHash.length() > 2){
            	twoChracter = keyToHash.substring(0,2);
            }
            encryptedKey = twoChracter + encryptedKey ;
        } 
        
        catch (Exception e) {
            e.printStackTrace();
        }
        return encryptedKey.toUpperCase();
    }
	
	public static Map<String, String> getParameterMap(HttpServletRequest request){
		Map<String, String> map = new HashMap<String, String>();
		map.put("configId", Constants.CONFIGID);
		map.put("productType", Constants.PRODUCTTYPE);
		map.put("platForm", Constants.PLATFORM);
		try {
			if(request!=null  && request.getSession()!=null){
				HttpSession session = request.getSession();
				if(session.getAttribute("trackerUser")!=null){
					TrackerUser trackerUser = (TrackerUser)session.getAttribute("trackerUser");
					if(trackerUser != null){
						map.put("trackerUserName", trackerUser.getUserName());
						map.put("trackerUserId", trackerUser.getId().toString());
					}else{
						System.out.println("TRACKERUSER1 NULL");
					}
				}else{
					System.out.println("TRACKERUSER2 NULL");
				}
				map.put("deviceId", session.getId());
				map.put("clientIPAddress", request.getRemoteAddr());
			}else{
				map.put("clientIPAddress","1.1.1.1");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	public static Map<String, String> getParameterMapCass(HttpServletRequest request){
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("configId", Constants.CONFIGID);
		map.put("productType", Constants.PRODUCTTYPE);
		map.put("pfm", Constants.PLATFORM);
		map.put("dType", Constants.PLATFORM);
		try {
			if(request!=null  && request.getSession()!=null){
				HttpSession session = request.getSession();
				if(session.getAttribute("trackerUser")!=null){
					TrackerUser trackerUser = (TrackerUser)session.getAttribute("trackerUser");
					if(trackerUser != null){
						map.put("trackerUserName", trackerUser.getUserName());
						map.put("trackerUserId", trackerUser.getId().toString());
					}else{
						System.out.println("TRACKERUSER1 NULL");
					}
				}else{
					System.out.println("TRACKERUSER2 NULL");
				}
				map.put("deviceId", session.getId());
				map.put("clientIPAddress", request.getRemoteAddr());
			}else{
				map.put("clientIPAddress","1.1.1.1");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	public static String getFileNameFromPath(String filePath) throws Exception {
		String fileName = "";
		if(filePath!=null && !filePath.isEmpty()){
			fileName = filePath.substring(filePath.lastIndexOf("/")+1, filePath.length());
		}
		return fileName;
	}
	
	public static String generatePassword(String fisrtName, String prefix) throws Exception{
		if(fisrtName.length() > 3){
			fisrtName = fisrtName.substring(0,3);
		}
		String uniqueString = UUID.randomUUID().toString();
		String sub = uniqueString.substring(0,4);
		String key = doBase64Encryption(sub+fisrtName);
		key =key.replaceAll("=", "");
		String code = "RTF"+key;
		return code;
	}
	
	public static String doBase64Encryption(String originalText) {
		byte[] encodedBytes = Base64.encodeBase64(originalText.getBytes());
		String encryptedText = new String(encodedBytes);
		return encryptedText;
	}
	
	public static String generateEncrptedPassword(String clearPassword) {
     MessageDigest algorithm;
	  try {
	   algorithm = MessageDigest.getInstance("SHA-1");
	  } catch (NoSuchAlgorithmException e) {
	   throw new RuntimeException("SHA-1 algorithm not found!");
	  }
	     byte [] digest = algorithm.digest(clearPassword.getBytes());
	     String encrptedPw = new String(Hex.encodeHex(digest));
	     return encrptedPw;
	 }

	public static String generateCustomerReferalCodeOld(Long referrerNumber,
			String prefix) throws Exception {
		String uniqueString = UUID.randomUUID().toString();
		String sub = uniqueString.substring(0, 4);
		String key = doBase64Encryption(sub + referrerNumber);
		key = key.replaceAll("=", "");
		String code = prefix + key;
		return code;
	}
	
	public static String generateCustomerReferalCode() throws Exception{
		StringBuilder builder = new StringBuilder();
		String tempCharSet = new String(CHARACTER_SET);
	    for(int i = 0; i < maxLength; i++){
	        builder.append(tempCharSet.charAt(rnd.nextInt(tempCharSet.length())));
	    }
	    return builder.toString().toUpperCase();
	}
	
	public static Double getLoyalFanPriceAsDouble(Double price) throws Exception {
		if(loyalFanPercentage==0)
			loyalFanPercentage = Double.valueOf(DAORegistry.getPropertyDAO().get("rtf.loyalfan.discount.percentage").getValue());
		
		Double loyalFanPrice = 0.00;
		price = price - ( (price * loyalFanPercentage)/100);
		loyalFanPrice = getRoundedValue(price);
		return loyalFanPrice;
	}
	
	public static Double getRoundedValue(Double value) throws Exception {
		df2.setRoundingMode(RoundingMode.DOWN);
		return Double.valueOf(df2.format(value));
	}
	public static String getRoundedValueString(Double value) throws Exception {
		return String.format( "%.2f", value);
	}
	
	
	public static String getObject(Map<String, String> dataMap,String url) throws Exception{
		try{
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost post = new HttpPost(url);

			// add header
			post.setHeader("x-sign", Constants.X_SIGN_HEADER);
			post.setHeader("x-token", Constants.X_TOKEN_HEADER);
			post.setHeader("x-platform", Constants.X_PLATFORM_HEADER);			
			post.setHeader("Accept","application/json");
			
			List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
			
			for(Map.Entry<String, String> entry : dataMap.entrySet()){
				urlParameters.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
			}
			
			post.setEntity(new UrlEncodedFormEntity(urlParameters));

			HttpResponse response = client.execute(post);

			BufferedReader responseData = new BufferedReader(
			        new InputStreamReader(response.getEntity().getContent()));		
			
			String result = IOUtils.toString(responseData);
			System.out.println(result);
			return result;

		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String getObjectWithFile(Map<String, String> dataMap,String url,List<String> files,List<String> allFiles) throws Exception{
		try{
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost post = new HttpPost(url);

			// add header
			post.setHeader("x-sign", Constants.X_SIGN_HEADER);
			post.setHeader("x-token", Constants.X_TOKEN_HEADER);
			post.setHeader("x-platform", Constants.X_PLATFORM_HEADER);			
			post.setHeader("Accept","application/json");
			
			MultipartEntity multiEntity = new MultipartEntity();
			for(Map.Entry<String, String> entry : dataMap.entrySet()){
				multiEntity.addPart(entry.getKey(),new StringBody(entry.getValue()));
			}
			
			if(files!=null && !files.isEmpty()){
				int i = 1;
				for(String file : files){
					multiEntity.addPart("file_"+i,new FileBody(new File(file)));
					i++;
				}
				multiEntity.addPart("fileSize",new StringBody(String.valueOf(i-1)));
			}
			
			if(allFiles!=null && !allFiles.isEmpty()){
				int i = 1;
				for(String file : allFiles){
					File file1 = new File(file);
					multiEntity.addPart("upload_"+i,new FileBody(file1));
					i++;
				}
				multiEntity.addPart("uploadSize",new StringBody(String.valueOf(i-1)));
			}
			post.setEntity(multiEntity);
			HttpResponse response = client.execute(post);

			BufferedReader responseData = new BufferedReader(
			        new InputStreamReader(response.getEntity().getContent()));		
			
			String result = IOUtils.toString(responseData);
			System.out.println(result);
			return result;

		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String formatDoubleToTwoDecimalPoint(Double value){
		String result="";
		NumberFormat formatter = new DecimalFormat("#0.00");     
		try {
			if(value!=null){
				result = formatter.format(value);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public static String formatDateToMonthDateYear(Date date){
		String result = "";
		DateFormat formatDate = new SimpleDateFormat("MM/dd/yyyy");
		try{
			if(date != null){
				result = formatDate.format(date);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	public static String formatTimeToHourMinutes(Time time){
		String result = "";
		DateFormat formatTime = new SimpleDateFormat("hh:mm a");
		try{
			if(time != null){
				result = formatTime.format(time);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	public static String formatTimeToHourMinutes(Date time){
		String result = "";
		DateFormat formatTime = new SimpleDateFormat("hh:mm a");
		try{
			if(time != null){
				result = formatTime.format(time);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	public static String formatDateTimeToMonthDateYearAndHourMinute(Date datetime){
		String result = "";
		DateFormat formatDateTime = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		try{
			if(datetime != null){
				result = formatDateTime.format(datetime);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	public static String getDateWithTwentyFourHourFormat(Date datetime){
		String result = "";
		DateFormat formatDateTime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		try{
			if(datetime != null){
				result = formatDateTime.format(datetime);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	
	public static String getTimeStamp(Date datetime){
		String result = "";
		DateFormat formatDateTime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
		try{
			if(datetime != null){
				result = formatDateTime.format(datetime);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	public static Date getDateWithTwentyFourHourFormat(String datetime){
		Date result =null;
		DateFormat formatDateTime = new SimpleDateFormat("yyyy/MM/dd/ HH:mm:ss");
		try{
			if(datetime != null){
				result = formatDateTime.parse(datetime);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	public static Date getDateWithTwentyFourHourFormat1(String datetime){
		Date result =null;
		DateFormat formatDateTime = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		try{
			if(datetime != null){
				result = formatDateTime.parse(datetime);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	public static String getParseDateWithTwelvwFourHourFormat(String datetime){
		String result =null;
		DateFormat formatDateTime = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
		try{
			if(datetime != null){
				result = formatDateTime.format(formatDateTime.parse(datetime));
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	
	public static String formatDateTimeToMonthDateYearWithTwelveHourFormat(Date datetime){
		String result = "";
		DateFormat formatDateTime_12Hour = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
		try{
			if(datetime != null){
				result = formatDateTime_12Hour.format(datetime);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	
	public static String getFilePath(HttpServletRequest request,String fileName,String folderPath){
		String path = null;
		try {
			path = request.getSession().getServletContext().getRealPath(folderPath+fileName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return path;
	}
	
	public static boolean voidInvoice(Invoice invoice, CustomerOrder customerOrder,InvoiceRefund invoiceRefund, String authUserName){
		try {
			String userName = "AUTO";
			//Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if(authUserName!=null){
				userName = authUserName;
			}
			CustomerLoyaltyHistory customerLoyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyByOrderIdAndStatus(customerOrder.getId());
			CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customerOrder.getCustomerId());
			
			/*if(customerOrder.getPrimaryPaymentMethod().equals(PaymentMethod.FULL_REWARDS)){
				Double rewardPoints = customerOrder.getPrimaryPayAmt();
				customerLoyalty.setActivePoints(customerLoyalty.getActivePoints()+rewardPoints);
				customerLoyalty.setRevertedSpentPoints(customerLoyalty.getRevertedSpentPoints() + rewardPoints);
				if(customerLoyaltyHistory!=null){
					customerLoyaltyHistory.setRewardStatus(RewardStatus.VOIDED);
					DAORegistry.getCustomerLoyaltyHistoryDAO().update(customerLoyaltyHistory);
				}
				DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
			}else if(customerOrder.getPrimaryPaymentMethod().equals(PaymentMethod.PARTIAL_REWARDS) ){
				Double rewardPoints = customerOrder.getPrimaryPayAmt();
				customerLoyalty.setActivePoints(customerLoyalty.getActivePoints()+rewardPoints);
				customerLoyalty.setRevertedSpentPoints(customerLoyalty.getRevertedSpentPoints() + rewardPoints);
				if(invoiceRefund != null)
					invoiceRefund.setRevertedUsedRewardPoints(rewardPoints);
			}*/
			
			if(customerLoyaltyHistory!=null){
				if(customerLoyaltyHistory.getRewardStatus().equals(RewardStatus.ACTIVE)){
					customerLoyalty.setActivePoints(customerLoyalty.getActivePoints() - customerLoyaltyHistory.getPointsEarned());
					customerLoyalty.setVoidedRewardPoints(customerLoyalty.getVoidedRewardPoints() + customerLoyaltyHistory.getPointsEarned());
					if(invoiceRefund != null)
						invoiceRefund.setRevertedEarnedRewardPoints(customerLoyaltyHistory.getPointsEarned());
				}else{
					customerLoyalty.setPendingPoints(customerLoyalty.getPendingPoints() - customerLoyaltyHistory.getPointsEarned());
					customerLoyalty.setVoidedRewardPoints(customerLoyalty.getVoidedRewardPoints() + customerLoyaltyHistory.getPointsEarned());
					if(invoiceRefund!=null)
						invoiceRefund.setRevertedEarnedRewardPoints(customerLoyaltyHistory.getPointsEarned());
				}
				customerLoyaltyHistory.setRewardStatus(RewardStatus.VOIDED);
				DAORegistry.getCustomerLoyaltyHistoryDAO().update(customerLoyaltyHistory);
				DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
			}
			
			//revert ticket uploads if any
			if(invoice.getIsRealTixUploaded()!=null && invoice.getIsRealTixUploaded().equalsIgnoreCase("Yes")){
				List<InvoiceTicketAttachment> invoiceAttachments = DAORegistry.getInvoiceTicketAttachmentDAO().getTicketAttachmentByInvoiceId(invoice.getId());
				 if(invoiceAttachments!=null && !invoiceAttachments.isEmpty()){
					 File file = null;
					 for(InvoiceTicketAttachment attachment:invoiceAttachments){
						 file = new File(attachment.getFilePath());
						 if(file.exists()){
							 file.delete();
						 }
					 }
					 DAORegistry.getInvoiceTicketAttachmentDAO().deleteAll(invoiceAttachments);
				 }
			}
			
			//revert PO mapping is any
			if(invoice.getRealTixMap()!=null && invoice.getRealTixMap().equalsIgnoreCase("Yes")){
				List<CategoryTicket> oldMappedCategoryTicket = DAORegistry.getCategoryTicketDAO().getMappedCategoryTicketByInvoiceId(invoice.getId());
				List<Ticket> oldMappedTicket = DAORegistry.getTicketDAO().getMappedTicketsByInvoiceId(invoice.getId());
				 if(oldMappedCategoryTicket!=null){
					 for(CategoryTicket catTicket:oldMappedCategoryTicket) {
						catTicket.setTicketId(null);
					}
					DAORegistry.getCategoryTicketDAO().updateAll(oldMappedCategoryTicket);
				 }
				 if(oldMappedTicket!=null){
					 for(Ticket ticket:oldMappedTicket) {
						 ticket.setInvoiceId(null);
						 ticket.setTicketStatus(TicketStatus.ACTIVE);
					}
					DAORegistry.getTicketDAO().updateAll(oldMappedTicket);
				 }
			}
			
			//Void Affiliate Cash Reward & Affiliate Cash Reward History
			AffiliateCashRewardHistory affilaiteCashRewardHistory = DAORegistry.getAffiliateCashRewardHistoryDAO().getHistoryByOrderId(customerOrder.getId()); 
			if(affilaiteCashRewardHistory != null){
				Double creditedCash = affilaiteCashRewardHistory.getCreditedCash();
				
				AffiliateCashReward affiliateCashReward = DAORegistry.getAffiliateCashRewardDAO().getAffiliateByUserId(affilaiteCashRewardHistory.getUserId());
				if(affiliateCashReward != null){
					
					if(affilaiteCashRewardHistory.getRewardStatus().equals(RewardStatus.ACTIVE) || affilaiteCashRewardHistory.getRewardStatus().equals(RewardStatus.PENDING)){			
						
						//Update Affiliate Cash Reward
						if(affilaiteCashRewardHistory.getRewardStatus().equals(RewardStatus.ACTIVE)){
							Double activeCash = affiliateCashReward.getActiveCash();
							Double totalCreditedCash = affiliateCashReward.getTotalCreditedCash();
							affiliateCashReward.setActiveCash(activeCash - creditedCash);
							affiliateCashReward.setTotalCreditedCash(totalCreditedCash - creditedCash);
						}
						if(affilaiteCashRewardHistory.getRewardStatus().equals(RewardStatus.PENDING)){
							Double pendingCash = affiliateCashReward.getPendingCash();
							affiliateCashReward.setPendingCash(pendingCash - creditedCash);
						}						
						Double totalVoidedCash = affiliateCashReward.getTotalVoidedCash();
						
						affiliateCashReward.setLastVoidCash(creditedCash);
						affiliateCashReward.setTotalVoidedCash(totalVoidedCash + creditedCash);
						affiliateCashReward.setLastUpdate(new Date());
												
						DAORegistry.getAffiliateCashRewardDAO().update(affiliateCashReward);
					
						//Update Affiliate Cash Reward History
						affilaiteCashRewardHistory.setCreditedCash(0.00);
						affilaiteCashRewardHistory.setRewardStatus(RewardStatus.VOIDED);
						affilaiteCashRewardHistory.setUpdatedDate(new Date());
						DAORegistry.getAffiliateCashRewardHistoryDAO().update(affilaiteCashRewardHistory);
					}
					if(affilaiteCashRewardHistory.getRewardStatus().equals(RewardStatus.PAID)){
						//Update Affiliate Cash Reward
						Double totalDebitedCash = affiliateCashReward.getTotalDebitedCash();
						affiliateCashReward.setLastDebitedCash(creditedCash);
						affiliateCashReward.setTotalDebitedCash(totalDebitedCash + creditedCash);
						affiliateCashReward.setLastUpdate(new Date());
						
						DAORegistry.getAffiliateCashRewardDAO().update(affiliateCashReward);
					}
				}
			}
			
			//Void - RTF Promotional Order Tracking New Table
			RTFPromotionalOfferTracking promotionalOfferTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().getPromoTrackingByOrderId(customerOrder.getId());
			if(promotionalOfferTracking != null){
				promotionalOfferTracking.setStatus("VOIDED");
				promotionalOfferTracking.setUpdatedDate(new Date());
				DAORegistry.getRtfPromotionalOfferTrackingDAO().update(promotionalOfferTracking);
			}
						
			//Delete - tix_open_order_status table
			List<OpenOrderStatus> openOrderStatusList = DAORegistry.getOpenOrderStatusDao().getRTFActiveOrderByInvoiceId(invoice.getId(), "REWARDTHEFAN");
			if(openOrderStatusList != null){
				for(OpenOrderStatus openOrderStatus : openOrderStatusList){
					openOrderStatus.setStatus("DELETED");
					openOrderStatus.setLastUpdated(new Date());
				}
				DAORegistry.getOpenOrderStatusDao().updateAll(openOrderStatusList);
			}
			
			invoice.setStatus(InvoiceStatus.Voided);
			invoice.setLastUpdated(new Date());
			invoice.setRealTixMap("No");
			invoice.setIsRealTixUploaded("No");
			invoice.setVoidDate(new Date());
			
			 customerOrder.setActualSection(null);
			 customerOrder.setActualSeat(null);
			 customerOrder.setRow(null);
			 
			
			if(invoiceRefund!=null){
				invoiceRefund.setOrderId(customerOrder.getId());
				invoiceRefund.setRefundedBy(userName);
				DAORegistry.getInvoiceRefundDAO().save(invoiceRefund);
			}
				
			List<OrderTicketGroupDetails> oldOrderTicketGroupDetails = DAORegistry.getOrderTicketGroupDetailsDAO().getOrderTicketGroupDetailsByOrderId(customerOrder.getId());
			DAORegistry.getOrderTicketGroupDetailsDAO().deleteAll(oldOrderTicketGroupDetails);
			DAORegistry.getInvoiceDAO().update(invoice);
			DAORegistry.getCustomerOrderDAO().update(customerOrder);
			
			InvoiceAudit audit = new InvoiceAudit(invoice);
			audit.setAction(InvoiceAuditAction.INVOICE_VOIDED);
			audit.setNote("Invoice is voided, All Real Ticket mappings are released and Uploaded Ticket files are deleted");
			audit.setCreatedDate(new Date());
			audit.setCreateBy(userName);
			DAORegistry.getInvoiceAuditDAO().save(audit);
			
			Map<String, String> map = getParameterMap(null);
			map.put("customerId",String.valueOf(invoice.getCustomerId()));
			map.put("orderType",customerOrder.getOrderType().toString());
			map.put("orderNo",String.valueOf(customerOrder.getId()));
			String data = getObject(map, Constants.BASE_URL + Constants.CANCEL_ORDER);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static Integer extractDateElement(String input,String format){
		try {
			if(input==null || input.isEmpty() || format==null || format.isEmpty()){
				return -1;
			}
			if(format.equals("DAY")){
				if(input.contains("/")){
					String arr[] = input.split("/");
					if(arr.length>1){
						return Integer.parseInt(arr[1].isEmpty()?"-1":arr[1]);
					}
				}
				return -1;
			}else if(format.equals("MONTH")){
				if(input.contains("/")){
					String arr[] = input.split("/");
					if(arr.length>0){
						return Integer.parseInt(arr[0].isEmpty()?"-1":arr[0]);
					}
				}else{
					return Integer.parseInt(input);
				}
				return -1;
			}else if(format.equals("YEAR")){
				if(input.contains("/")){
					String arr[] = input.split("/");
					if(arr.length>2){
						return Integer.parseInt(arr[2].isEmpty()?"-1":arr[2]);
					}
				}
				return -1;
			}else if(format.equals("HOUR")){
				String amPm="";
				if(input.contains(":")){
					String arr[];
					if(input.contains(" ")){
						arr = input.split(" ");
						amPm = arr[1];
					}
					arr = input.split(":");
					if(arr.length>0){
						if(amPm.equalsIgnoreCase("pm")){
							return Integer.parseInt(arr[0].isEmpty()?"-1":arr[0])+12;
						}
						return Integer.parseInt(arr[0].isEmpty()?"-1":arr[0]);
					}
				}else{
					return Integer.parseInt(input);
				}
				return -1;
			}else if(format.equals("MINUTE")){
				if(input.contains(":")){
					if(input.contains(" ")){
						input= input.substring(0,input.indexOf(" "));
					}
					String []arr = input.split(":");
					if(arr.length>1){
						return Integer.parseInt(arr[1].isEmpty()?"-1":arr[1]);
					}
				}
				return -1;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	public static Double roundOffDouble(Double input){
		Double roundedValue = 0.00;
		DecimalFormat df = new DecimalFormat("#.##");
		try{
			if(input != null && input > 0){		
				roundedValue = input;		
				roundedValue = Double.valueOf(df.format(roundedValue));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return roundedValue;
	}
	public static String roundOffStr(Double input){
		String roundedValue = "0.00";
		DecimalFormat df = new DecimalFormat("#.##");
		try{
			if(input != null && input > 0){		
				roundedValue = df.format(input);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return roundedValue;
	}
	public static String formatTime(String eventTimeStr){
		String formattedTime = null;
		try {
			if(eventTimeStr != null){
				if(eventTimeStr.equals("TBD")){
					formattedTime = "TBD";
				}else{
					SimpleDateFormat sdf = new SimpleDateFormat("hh:mmaa");
					DateFormat tf = new SimpleDateFormat("hh:mm aa");
					
					Date date = (Date)sdf.parse(eventTimeStr);
					formattedTime = tf.format(date);
				}
			}
			return formattedTime;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Date getDateAndTime(Date date,Time time){
		if(date==null && time==null){
			return null;
		}
		Calendar aDate = Calendar.getInstance();
		Calendar aTime = Calendar.getInstance();
		Calendar aDateTime = Calendar.getInstance();
		if(date!=null){
		    aDate.setTime(date);
		    aDateTime.set(Calendar.DAY_OF_MONTH, aDate.get(Calendar.DAY_OF_MONTH));
		    aDateTime.set(Calendar.MONTH, aDate.get(Calendar.MONTH));
		    aDateTime.set(Calendar.YEAR, aDate.get(Calendar.YEAR));
		    aDateTime.set(Calendar.HOUR_OF_DAY, 0);
		    aDateTime.set(Calendar.MINUTE, 0);
		    aDateTime.set(Calendar.MILLISECOND, 0);
		    aDateTime.set(Calendar.SECOND, 0);
		}
		if(time!=null){
		    aTime.setTime(time);
		    aDateTime.set(Calendar.HOUR_OF_DAY, aTime.get(Calendar.HOUR));
		    aDateTime.set(Calendar.MINUTE, aTime.get(Calendar.MINUTE));
		    aDateTime.set(Calendar.MILLISECOND, 0);
		    aDateTime.set(Calendar.SECOND, 0);
		}
	    return aDateTime.getTime();
	}
	
	public static boolean isEmptyOrNull(String str){
		if(str==null || str.trim().isEmpty()){
			return true;
		}
		return false;
	}
	
	 public static String getRealTicketSectionDescription(List<RealTicketSectionDetails> list,Integer qty){
			if(list==null || list.isEmpty()) {
				return "";
			}
			String description = "<font color=#124597>ZONE "+list.get(0).getSection()+" tickets will be <b>seated</b> in ";
			if(list.get(0).getSection()!=null && !list.get(0).getSection().isEmpty()){
				description += "Section "+list.get(0).getSection();
				if(list.get(0).getRow()!=null && !list.get(0).getRow().isEmpty()){
					description += " and Row "+list.get(0).getRow();
				}
			}
			if(qty==1){
				description += ", Seat "+list.get(0).getSeatLow();
			}else{
				boolean isFirst = true;
				for(RealTicketSectionDetails section : list){
					if(isFirst){
						isFirst = false;
						description += ", Seat "+section.getSeatLow()+" to "+section.getSeatHigh();
					}else{
						description += ", and "+section.getSeatLow()+" to "+section.getSeatHigh();
					}
				}
			}
			description += "</font>";
			return description;
		 }
	 
	public static String computZoneForTickets(Integer eventId,String section) {
		String zone="";
		try {
			Event event = TMATDAORegistry.getEventDAO().getEventById(eventId);
			if (event == null) {
				return zone;
			}
			/*Collection<com.rtw.tmat.data.Ticket> tickets = TMATDAORegistry.getTicketDAO().getAllActiveTicketsByEvent(event.getId());
			if (tickets.isEmpty()) {
				return zone;
			}*/
			Collection<Category> categories = null;
			if (event.getVenueCategory() != null) {
				categories = TMATDAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategory().getId());
			} else {
				List<String> venueCategories = TMATDAORegistry.getVenueCategoryDAO().getCategoryGroupByVenueId(event.getVenueId());
				categories = TMATDAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(event.getVenueId(), venueCategories.get(0));
			}
			//tickets = TicketUtil.preAssignCategoriesToTickets(tickets,categories, event);
			
			Map<String,List<CategoryMapping>> catMappingMap = Categorizer.getCategoryMappingByVenueCategoryId(event);
			Map<Integer,Category> categoryMap = new HashMap<Integer, Category>();
			if(categories != null) {
				for (Category category : categories) {
					categoryMap.put(category.getId(), category);
				}
			}
			
			section = section.toLowerCase();
			List<CategoryMapping> categoryMappingList = catMappingMap.get(section);
			if(categoryMappingList != null) {
				for (CategoryMapping catMapping : categoryMappingList) {
					Category category = categoryMap.get(catMapping.getCategoryId());
					if(category != null){
						zone = category.getSymbol();
						break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return zone;
	}
	
	public static void setActualTicketDetails(List<RealTicketSectionDetails> list,CustomerOrder order){
		try {
			if(list==null || list.isEmpty()){
				return;
			}
			if(list.size()==1){
				order.setActualSection(list.get(0).getSection());
				order.setRow(list.get(0).getRow());
				if(list.get(0).getSeatLow().equalsIgnoreCase(list.get(0).getSeatHigh())){
					order.setActualSeat(list.get(0).getSeatLow());
				}else{
					order.setActualSeat(list.get(0).getSeatLow()+"-"+list.get(0).getSeatHigh());
				}
			}else{
				String rowString="";
				String seatString="";
				for(RealTicketSectionDetails sec : list){
					order.setActualSection(sec.getSection());
					if(rowString.isEmpty()){
						rowString += sec.getRow();
					}else if(rowString.equalsIgnoreCase(sec.getRow())){
						
					}else{
						rowString +=" AND "+sec.getRow();
					}
					if(seatString.isEmpty()){
						if(sec.getSeatLow().equalsIgnoreCase(sec.getSeatHigh())){
							seatString += sec.getSeatLow();
						}else{
							seatString += sec.getSeatLow()+"-"+sec.getSeatHigh();
						}
					}else{
						if(sec.getSeatLow().equalsIgnoreCase(sec.getSeatHigh())){
							seatString += " AND "+sec.getSeatLow();
						}else{
							seatString += " AND "+ sec.getSeatLow()+"-"+sec.getSeatHigh();
						}
					}
				}
				order.setRow(rowString);
				order.setActualSeat(seatString);
			}
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static Integer getBrokerId(HttpSession session){
		Boolean roleFlag = false;
		Integer brokerId = 0;
		TrackerUser trackerUserBroker = null;
		TrackerUser trackerUser = (TrackerUser)session.getAttribute("trackerUser");
		try{
			for(Role role : trackerUser.getRoles()){
				if(role.getName().equals("ROLE_SUPER_ADMIN")){
					roleFlag = true;
				}
				if(role.getName().equals("ROLE_BROKER")){
					trackerUserBroker = DAORegistry.getTrackerUserDAO().get(trackerUser.getId());
				}
			}
			if(roleFlag){
				Integer broker = (Integer) session.getAttribute("brokerId");
				if(broker==null){
					brokerId = Constants.BROKERID;
				}else{
					brokerId = broker;
				}
			}else{
				if(trackerUserBroker != null){
					brokerId = trackerUserBroker.getBroker().getId();
				}else{
					brokerId = Constants.BROKERID;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return brokerId;
	}
	
	public static String getCommaSeperatedStringFromCollection(Collection<Object[]> list) throws Exception{
		StringBuilder csvString = new StringBuilder();
		for(Object[] object : list){
			csvString.append(StringUtils.arrayToCommaDelimitedString(object)+com.rtw.tracker.utils.Constants.NEW_LINE_DELIMITER);
		}
		return csvString.toString();
	}
	
	
	public static Double getBrokerServiceFees(Integer brokerId, Double price, Integer qty, Double tax){
		  Double brokerFees = 0.00;
		  if(brokerId.equals(1001) ){
			  if(tax != null && tax > 0);{
				  //brokerFees = (double) Math.ceil(((price * qty)*(tax/100)) * 100 / 100);
				  brokerFees = tax;
			  }
		  }else{
			  TrackerBrokers broker = DAORegistry.getTrackerBrokersDAO().get(brokerId);
			  brokerFees = (double) Math.ceil(((price * qty)*(broker.getServiceFees()/100)) * 100 / 100);
		  }
		  return brokerFees;
	}
	
	public static Collection<CategoryTicketGroup> addEventsInEachCategoryTicketGroup(Collection<CategoryTicketGroup> categoryTicketGroups,EventDetails eventDetails,Map<Integer, String> shippingMap){
		try{
			
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return categoryTicketGroups;
	}
	
	public static Collection<TicketGroup> addEventsInEachTicketGroup(Collection<TicketGroup> ticketGroups ,EventDetails eventDetails, Map<Integer, String> shippingMap){
		try{
			
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return ticketGroups;
	}
	
	public static Collection<CategoryAndLongTickets> mergeCategoryTicketsAndTickets(Collection<CategoryTicketGroup> categoryTicketGroups, Collection<TicketGroup> ticketGroups,Map<Integer, String> shippingMap,EventDetails eventDtls){
		Collection<CategoryAndLongTickets> tickets = new ArrayList<CategoryAndLongTickets>(); 
		CategoryAndLongTickets ticket = null;
		try{
			if(!CollectionUtils.isEmpty(ticketGroups)){
				for (TicketGroup tx : ticketGroups) {
					ticket = new CategoryAndLongTickets();
					ticket.setId(tx.getId());
					ticket.setEventId(tx.getEventId());
					ticket.setSection(tx.getSection());
					ticket.setRow(tx.getRow());
					ticket.setQuantity(tx.getQuantity());
					ticket.setPrice(String.format("%.2f", tx.getPrice()));
					ticket.setTaxAmount(String.format("%.2f", 0.0));
					ticket.setSectionRange("");
					ticket.setRowRange("");
					ticket.setShippingMethod(shippingMap.get(tx.getShippingMethodId()));
					ticket.setProductType("REWARDTHEFAN");
					ticket.setRetailPrice(String.format("%.2f", tx.getPrice()));//tx.getPrice());
					ticket.setWholeSalePrice(String.format("%.2f", tx.getPrice()));//tx.getWholeSalePrice());
					ticket.setFacePrice(String.format("%.2f", 0.0));// tx.getFacePrice());
					ticket.setCost(String.format("%.2f", 0.0));//tx.getCost());
					ticket.setBroadcast(tx.getBroadcast()!=null?tx.getBroadcast():false);//tx.getBroadcast());
					ticket.setInternalNotes("ZTP");// tx.getInternalNotes());
					ticket.setExternalNotes("");
					ticket.setMarketPlaceNotes("");
					ticket.setMaxShowing("");
					ticket.setSeatLow(tx.getSeatLow());
					ticket.setSeatHigh(tx.getSeatHigh());
					ticket.setNearTermDisplayOption("");
					if(eventDtls!=null){
						ticket.setEventName(eventDtls.getEventName());
						ticket.setEventDate(eventDtls.getEventDateStr());
						ticket.setEventTime(eventDtls.getEventTimeStr());
						ticket.setVenue(eventDtls.getBuilding());
					}
					ticket.setBrokerId(tx.getBrokerId());
					ticket.setTicketType("Real");
					tickets.add(ticket);
				}
			}
			
			if(!CollectionUtils.isEmpty(categoryTicketGroups)){
				for (CategoryTicketGroup tx : categoryTicketGroups) {
					ticket = new CategoryAndLongTickets();
					ticket.setId(tx.getId());
					ticket.setEventId(tx.getEventId());
					ticket.setSection(tx.getSection());
					ticket.setRow(tx.getRow());
					ticket.setQuantity(tx.getQuantity());
					ticket.setPrice(String.format("%.2f", tx.getPrice()));
					ticket.setTaxAmount(String.format("%.2f", tx.getTaxAmount()));
					ticket.setSectionRange(tx.getSectionRange());
					ticket.setRowRange(tx.getRowRange());
					ticket.setShippingMethod(shippingMap.get(tx.getShippingMethodId()));
					ticket.setProductType(tx.getProducttype().name());
					ticket.setRetailPrice(String.format("%.2f", tx.getPrice()));//tx.getPrice());
					ticket.setWholeSalePrice(String.format("%.2f", tx.getPrice()));//tx.getWholeSalePrice());
					ticket.setFacePrice(String.format("%.2f", 0.0));// tx.getFacePrice());
					ticket.setCost(String.format("%.2f", 0.0));//tx.getCost());
					ticket.setBroadcast(tx.getBroadcast()!=null?tx.getBroadcast():false);//tx.getBroadcast());
					ticket.setInternalNotes("ZTP");// tx.getInternalNotes());
					ticket.setExternalNotes(tx.getExternalNotes());
					ticket.setMarketPlaceNotes(tx.getMarketPlaceNotes());
					ticket.setMaxShowing(tx.getMaxShowing() != null ? String.valueOf(tx.getMaxShowing()) : "");
					ticket.setSeatLow(tx.getSeatLow());
					ticket.setSeatHigh(tx.getSeatHigh());
					ticket.setNearTermDisplayOption(tx.getNearTermDisplayOption());
					if(eventDtls!=null){
						ticket.setEventName(eventDtls.getEventName());
						ticket.setEventDate(eventDtls.getEventDateStr());
						ticket.setEventTime(eventDtls.getEventTimeStr());
						ticket.setVenue(eventDtls.getBuilding());
					}
					ticket.setBrokerId(tx.getBrokerId());
					ticket.setTicketType("Category");
					tickets.add(ticket);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return tickets;
	}
	
	public static String generatedCustomerUserId(String fName,String lName){
		String userId="";
		try {
			if(fName == null){
				fName="";
			}
			if(lName == null){
				lName="";
			}
			
			fName = fName.trim().replaceAll("'","");
			fName = fName.trim().replaceAll("-","");
			fName = fName.trim().replaceAll("_","");
			fName = fName.trim().replaceAll("\\.","");
			fName = fName.trim().replaceAll(" ","");
			
			lName = lName.trim().replaceAll("'","");
			lName = lName.trim().replaceAll("-","");
			lName = lName.trim().replaceAll("_","");
			lName = lName.trim().replaceAll("\\.","");
			lName = lName.trim().replaceAll(" ","");
			
			if(!fName.trim().isEmpty() && !lName.trim().isEmpty()){
				userId = String.valueOf(fName.trim().charAt(0));
				userId += lName.trim();
			}else if(!fName.trim().isEmpty() && lName.trim().isEmpty()){
				userId = fName.trim();
			}else if(fName.trim().isEmpty() && !lName.trim().isEmpty()){
				userId = lName.trim();
			}
			
			if(userId.length() < 5){
				int len = 5-userId.length();
				for(int i=1;i<=len;i++){
					userId += rnd.nextInt(10);
				}
			}else if(userId.length() > 12){
				userId = userId.substring(0,12);
			}
			
			List<Customer> list = DAORegistry.getCustomerDAO().getCustomerByUserId(userId);
			
			if(list.size()> 0){
				if(userId.length() <= 10){
					userId += rnd.nextInt(10); 
					userId += rnd.nextInt(10);
					list = DAORegistry.getCustomerDAO().getCustomerByUserId(userId);
				}
				if(list.size() > 0){
					if(!fName.trim().isEmpty()){
						userId = fName;
					}else if(!lName.trim().isEmpty()){
						userId = lName;
					}
					if(userId.length() < 5){
						int len = 6-userId.length();
						for(int i=1;i<=len;i++){
							userId += rnd.nextInt(10);
						}
					}else if(userId.length() > 12){
						userId = userId.substring(0,10);
						userId += rnd.nextInt(10); 
						userId += rnd.nextInt(10);
					}else if(userId.length() < 9){
						userId += rnd.nextInt(10); 
						userId += rnd.nextInt(10);
						userId += rnd.nextInt(10);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			userId = "";
		}
		return userId.toLowerCase();
	}
	
	public static void userActionAudit(HttpServletRequest request, Integer dataId, String message){
		UserAction userActionAudit = new UserAction();
		
		try{
			String userName = request.getParameter("trackerUserName");
			String userId = request.getParameter("trackerUserId");
			String clientIPAddress = request.getParameter("clientIPAddress");
			String actionURI = request.getRequestURI();
			
			userActionAudit.setUserName(userName);
			userActionAudit.setUserId(Integer.parseInt(userId));
			userActionAudit.setAction(actionURI);
			userActionAudit.setIpAddress(clientIPAddress);
			userActionAudit.setTimeStamp(new Date());
			userActionAudit.setMessage(message);
			if(dataId != null){
				userActionAudit.setDataId(dataId);
			}
			
			DAORegistry.getUserActionDAO().save(userActionAudit);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	public static void getExcelStringCell(Object o,int j,Row rowhead){
		try {
			if(o!=null){
				rowhead.createCell((int) j).setCellValue(o.toString());
			}else{
				rowhead.createCell((int) j).setCellValue("");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void getExcelStringCellRed(Object o,int j,Row rowhead,CellStyle style){
		try {
			if(o!=null){
				Cell cell = rowhead.createCell((int) j);
				cell.setCellValue(o.toString());
				cell.setCellStyle(style);
				//rowhead.createCell((int) j).setCellValue(o.toString());
			}else{
				Cell cell = rowhead.createCell((int) j);
				cell.setCellValue("");
				cell.setCellStyle(style);
				//rowhead.createCell((int) j).setCellValue("");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void getExcelDateCell(Object o,int j,Row rowhead,CellStyle style){
		try {
			
			if(o!=null){
				Cell cell = rowhead.createCell((int) j);
				cell.setCellValue(o.toString());
				cell.setCellStyle(style);
			}else{
				rowhead.createCell((int) j).setCellValue("");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void getExcelDecimalCell(Object o,int j,Row rowhead){
		try {
			if(o!=null){
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(Double.parseDouble(o.toString().trim())));
			}else{
				rowhead.createCell((int) j).setCellValue("");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void getExcelDecimalCellRed(Object o,int j,Row rowhead,CellStyle style){
		try {
			if(o!=null){
				Cell cell = rowhead.createCell((int) j);
				cell.setCellValue(Util.getRoundedValue(Double.parseDouble(o.toString().trim())));
				cell.setCellStyle(style);
				//rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(Double.parseDouble(o.toString().trim())));
			}else{
				Cell cell = rowhead.createCell((int) j);
				cell.setCellValue("");
				cell.setCellStyle(style);
				//rowhead.createCell((int) j).setCellValue("");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void getExcelHeaderCellWithBG(Object o,int j,Row rowhead,CellStyle style){
		try {
			Cell cell = rowhead.createCell((int) j);
			cell.setCellValue(o.toString());
			cell.setCellStyle(style);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void getExcelIntegerCell(Object o,int j,Row rowhead){
		try {
			if(o!=null){
				rowhead.createCell((int) j).setCellValue(Integer.parseInt(o.toString().trim()));
			}else{
				rowhead.createCell((int) j).setCellValue("");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static CellRangeAddress getMergedRegion(Cell c){
		try {
			Sheet sheet = c.getSheet();
			for(CellRangeAddress range: sheet.getMergedRegions()){
				if(range.isInRange(c.getRowIndex(), c.getColumnIndex())){
					return range;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public static String getHourFromDate(Date date){
		try {
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			int hour = cal.get(Calendar.HOUR_OF_DAY);
			return String.valueOf(hour);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String getMinutesFromDate(Date date){
		try {
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			int minutes = cal.get(Calendar.MINUTE);
			return String.valueOf(minutes);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	/*public static SmbFile getSmbFile(SharedProperty property,String fileUrl) {
		try {
			NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(null, property.getSharedFileUserName(),property.getSharedFilePassword());
			SmbFile file = new SmbFile(fileUrl,auth);
			return file;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
		
	}
	
	
	public static String getTicketFileDirectory(SharedProperty property){
		String path = "";
		try {
			path = property.getShredFilePathURL()+"SvgMaps//Tickets//";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return path;
	}
	
	public static String getSMBFilePath(SharedProperty property,String filePath){
		String path = "";
		try {
			path = property.getShredFilePathURL()+filePath;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return path;
	}*/
	
	private static List<String> getRandomAnswerList() {
		List<String> randomAnswerList = new ArrayList<String>();	
		randomAnswerList.add("A");
		randomAnswerList.add("B");
		randomAnswerList.add("C");
		return randomAnswerList;
	}


	public static String getRandomWrongAnswer(String correctAns) {
		if(StringUtils.isEmpty(correctAns)) return null;
		List<String> randomAnswerList = getRandomAnswerList();
		Collections.shuffle(randomAnswerList);
		randomAnswerList.remove(correctAns);
		return randomAnswerList.get(0);
	}
	
}