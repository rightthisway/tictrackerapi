package com.rtw.tmat.utils;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("StripeTransaction")
public class StripeTransaction {
	private Integer status;
	private Error error;
	private String transactionId;
	private String transactionStatus;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getTransactionStatus() {
		return transactionStatus;
	}
	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}
}
