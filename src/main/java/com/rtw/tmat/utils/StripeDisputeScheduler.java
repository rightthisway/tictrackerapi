package com.rtw.tmat.utils;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.InitializingBean;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.CustomerOrder;
import com.rtw.tracker.datas.Invoice;
import com.rtw.tracker.datas.InvoiceRefund;
import com.rtw.tracker.datas.InvoiceRefundResponse;
import com.rtw.tracker.datas.StripeDispute;
import com.rtw.tracker.datas.StripeDisputeResponse;
import com.rtw.tracker.enums.InvoiceStatus;
import com.rtw.tracker.enums.PartialPaymentMethod;
import com.rtw.tracker.enums.PaymentMethod;

public class StripeDisputeScheduler extends TimerTask implements InitializingBean{

	Map<String, String> map = new HashMap<String, String>();
	private static final int limit = 100;
	
	@Override
	public void afterPropertiesSet() throws Exception {
		TimerTask timerTask = new StripeDisputeScheduler();
		Calendar today = Calendar.getInstance();
		today.set(Calendar.HOUR, 23);
		today.set(Calendar.MINUTE, 59);
		today.set(Calendar.SECOND, 59);
		
		
		Timer timer = new Timer();
		//timer.scheduleAtFixedRate(timerTask,new Date(), 15*60*1000);
		timer.schedule(timerTask, today.getTime());
	}

	@Override
	public void run() {

		try {
			map.put("configId", Constants.CONFIGID);
			map.put("productType", Constants.PRODUCTTYPE);
			map.put("platForm", Constants.PLATFORM);
			map.put("limit", String.valueOf(limit));
			String data = Util.getObject(map,Constants.BASE_URL+Constants.GET_INVOICE_DISPUTE_LIST);
			Gson gson = Util.getGsonBuilder().create();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			StripeDisputeResponse stripeDisputeResp = gson.fromJson(((JsonObject)jsonObject.get("stripeDisputeResponse")), StripeDisputeResponse.class);
			if(stripeDisputeResp!=null && stripeDisputeResp.getStatus()==1){
				List<StripeDispute> disputes = stripeDisputeResp.getDisputes();
				List<StripeDispute> dbList = DAORegistry.getStripeDisputeDAO().getAllDisputeList();
				Map<String, StripeDispute> disputeMap = new HashMap<String, StripeDispute>();
				for(StripeDispute d : dbList){
					disputeMap.put(d.getDisputeId(),d);
				}
				if(disputes!=null && !disputes.isEmpty()){
					for(StripeDispute dispute : disputes){
						if(disputeMap.get(dispute.getDisputeId()) != null){
							continue;
						}
						CustomerOrder order = DAORegistry.getCustomerOrderDAO().getCustomerOrderByTransactionId(dispute.getCharge());
						if(order==null){
							continue;
						}
						Invoice invoice = DAORegistry.getInvoiceDAO().getInvoiceByOrderId(order.getId());
						if(invoice==null){
							continue;
						}
						invoice.setStatus(InvoiceStatus.Disputed);
						invoice.setLastUpdated(new Date());
						DAORegistry.getInvoiceDAO().update(invoice);
						dispute.setOrderId(order.getId());
						DAORegistry.getStripeDisputeDAO().save(dispute);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	
		
	}

}
