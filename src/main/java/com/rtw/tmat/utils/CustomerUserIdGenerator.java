package com.rtw.tmat.utils;

import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.InitializingBean;

import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.Customer;

public class CustomerUserIdGenerator extends TimerTask implements InitializingBean{

	@Override
	public void afterPropertiesSet() throws Exception {
		//TimerTask timerTask = new CustomerUserIdGenerator();
		//Calendar cal = Calendar.getInstance();
		//Timer timer = new Timer(true);
		//timer.scheduleAtFixedRate(timerTask, cal.getTime(), 2*60*1000);
		
	}

	@Override
	public void run() {
		try {
			String userId = "";
			List<Customer> c1 = null;
			Random rn  = new Random();
			List<Customer> customers = DAORegistry.getCustomerDAO().getAllRTFCustomer();
			for(Customer c : customers){
				userId = "";
				if(c.getUserId() == null || c.getUserId().trim().isEmpty()){
					if(c.getCustomerName()!= null && !c.getCustomerName().isEmpty()
							&& c.getLastName()!=null && !c.getLastName().isEmpty()){
						userId = String.valueOf(c.getCustomerName().trim().charAt(0));
						if(c.getLastName().trim().contains(" ")){
							userId  += c.getLastName().trim().split("\\s+")[0];
						}else{
							userId += c.getLastName().trim();
						}
						
						if(userId.length() < 5){
							int len = 5-userId.length();
							for(int i=1;i<=len;i++){
								userId += rn.nextInt(10);
							}
						}
						
						c1 = DAORegistry.getCustomerDAO().getCustomerByUserId(userId);
						if(c1.size()> 0){
							for(int i=1;i<=9;i++){
								userId  += rn.nextInt(10);
								c1 = DAORegistry.getCustomerDAO().getCustomerByUserId(userId);
								if(c1.size()==0){
									break;
								}
							}
						}
					}else if((c.getCustomerName()== null || c.getCustomerName().isEmpty())
							&& c.getLastName()!=null && !c.getLastName().isEmpty()){
						if(c.getLastName().trim().contains(" ")){
							userId  += c.getLastName().trim().split("\\s+")[0];
						}else{
							userId += c.getLastName().trim();
						}
						
						if(userId.length() < 5){
							int len = 5-userId.length();
							for(int i=1;i<=len;i++){
								userId += rn.nextInt(10);
							}
						}
						
						c1 = DAORegistry.getCustomerDAO().getCustomerByUserId(userId);
						if(c1.size()> 0){
							for(int i=1;i<=9;i++){
								userId  += rn.nextInt(10);
								c1 = DAORegistry.getCustomerDAO().getCustomerByUserId(userId);
								if(c1.size()==0){
									break;
								}
							}
						}
					}else if(c.getCustomerName()!= null && !c.getCustomerName().isEmpty()
							&&(c.getLastName()==null || c.getLastName().isEmpty())){
						if(c.getCustomerName().trim().contains(" ")){
							userId  += c.getCustomerName().trim().split("\\s+")[0];
						}else{
							userId += c.getCustomerName().trim();
						}
						
						if(userId.length() < 5){
							int len = 5-userId.length();
							for(int i=1;i<=len;i++){
								userId += rn.nextInt(10);
							}
						}
						
						c1 = DAORegistry.getCustomerDAO().getCustomerByUserId(userId);
						if(c1.size()> 0){
							for(int i=1;i<=9;i++){
								userId += rn.nextInt(10);
								c1 = DAORegistry.getCustomerDAO().getCustomerByUserId(userId);
								if(c1.size()==0){
									break;
								}
							}
						}
						
					}
					userId = userId.trim().replaceAll("'","");
					userId = userId.trim().replaceAll("-","");
					userId = userId.trim().replaceAll("_","");
					userId = userId.trim().replaceAll("\\.","");
					if(userId.length() > 12){
						userId = userId.trim().substring(0,9);
						userId += rn.nextInt(10);
						userId += rn.nextInt(10);
						userId += rn.nextInt(10);
					}
					c.setUserId(userId.toLowerCase());
					DAORegistry.getCustomerDAO().update(c);
					System.out.println(userId);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
