package com.rtw.tmat.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.InitializingBean;

import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.SoldTicketDetail;
import com.rtw.tracker.utils.Util;

public class OpenOrderScheduler extends TimerTask implements InitializingBean{

	@Override
	public void afterPropertiesSet() throws Exception {
		TimerTask timerTask = new OpenOrderScheduler();
		Calendar cal = Calendar.getInstance();
		//cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE)+1);
		Timer timer = new Timer(true);
		timer.scheduleAtFixedRate(timerTask, cal.getTime(), 2*60*1000);
	}

	@Override
	public void run() {
		try{
			if(Util.isContestRunning){
				System.out.println("RETURN");
				return;
			}
			System.out.println("RUNNNNN");
			Collection<SoldTicketDetail> ticketDetail =  new ArrayList<SoldTicketDetail>();
			ticketDetail = DAORegistry.getQueryManagerDAO().getAllSoldUnfilledNewTickets();
			//System.out.println("OPENORDERSCHEDULER / SOLDTICKET:"+ticketDetail.size());
			OpenOrderUtil.addNewOpenOrders(ticketDetail);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}
