package com.rtw.tmat.utils;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtw.tracker.dao.implementation.QuizDAORegistry;
import com.rtw.tracker.datas.Contests;
import com.rtw.tracker.pojos.QuizJoinContestInfo;
import com.rtw.tracker.utils.GsonCustomConfig;

public class ContestTotalUserCountSchedulerUtil implements Runnable{
	
	Contests contest = null;
	public ContestTotalUserCountSchedulerUtil(Contests con){
		contest = con;
	}

	@Override
	public void run() {
		Map<String, String> map = Util.getParameterMap(null);
		map.put("contestId",contest.getId().toString());
		//int count = 100;
		try {
			while(contest.getStatus().equalsIgnoreCase("STARTED")){
				String data = Util.getObject(map,"http://54.172.179.185/sandbox/GetQuizContestCustomersCount.json" /*Constants.BASE_URL+Constants.TICTRACKER_API_UPDATE_USERCOUNT*/);
				Gson gson = GsonCustomConfig.getGsonBuilder();		
				JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
				QuizJoinContestInfo countInfo = gson.fromJson(((JsonObject)jsonObject.get("quizJoinContestInfo")), QuizJoinContestInfo.class);
				if(countInfo == null || countInfo.getStatus()==0){
					System.out.println("TOTALUSERCOUNT IS NULL FROM API.");
					continue;
				}
				Map<String, String> reqMap = new HashMap<String, String>();
				reqMap.put("id","1");
				reqMap.put("apiName","TOTALUSERCOUNT");
				reqMap.put("totalCount",countInfo.getTotalUsersCount().toString());
				//reqMap.put("totalCount",String.valueOf(count));
				/*String respOK = Util.getObject(reqMap,Constants.NODEJS_API_URL);
				if(respOK==null || !respOK.equalsIgnoreCase("OK")){
					System.out.println("ERROR WHILE UPDATING TOTALUSERCOUNT ON AWS.");
				}*/
				Thread.sleep(10000);
				//count++;
				contest = QuizDAORegistry.getContestsDAO().get(contest.getId()) ;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
