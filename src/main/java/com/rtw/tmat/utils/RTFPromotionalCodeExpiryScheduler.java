package com.rtw.tmat.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.InitializingBean;

import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.RTFPromoOffers;
import com.rtw.tracker.utils.Util;

public class RTFPromotionalCodeExpiryScheduler extends TimerTask implements InitializingBean{

	@Override
	public void afterPropertiesSet() throws Exception {
		TimerTask timerTask = new RTFPromotionalCodeExpiryScheduler();
		Calendar cal = Calendar.getInstance();
		//cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE)+2);
		Timer timer = new Timer(true);
		timer.scheduleAtFixedRate(timerTask, cal.getTime(), 60*60*1000);
	}

	@Override
	public void run() {
		try {
			if(Util.isContestRunning){
				System.out.println("RETURN");
				return;
			}
			System.out.println("RUNNNNN");
			List<RTFPromoOffers> updateList = new ArrayList<RTFPromoOffers>();
			Date today = new Date();
			List<RTFPromoOffers> offers = DAORegistry.getRtfPromoOffersDAO().getAllActiveOffers();
			if(!offers.isEmpty()){
				for(RTFPromoOffers offer : offers){
					if(offer.getMaxOrders()!= null && offer.getNoOfOrders()!=null
							&& (offer.getMaxOrders() == offer.getNoOfOrders() ||
							offer.getMaxOrders().equals(offer.getNoOfOrders()))){
						offer.setStatus("DISABLED");
						updateList.add(offer);
					}
					
					if(offer.getEndDate().before(today)){
						offer.setStatus("DISABLED");
						updateList.add(offer);
					}
				}
				
				if(!updateList.isEmpty()){
					DAORegistry.getRtfPromoOffersDAO().updateAll(updateList);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	
}
