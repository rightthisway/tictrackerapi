package com.rtw.tmat.utils;

import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.InitializingBean;

import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.utils.Util;

public class ReferralCodeGeneratorScheduler extends TimerTask implements InitializingBean{

	@Override
	public void afterPropertiesSet() throws Exception {
		TimerTask timerTask = new ReferralCodeGeneratorScheduler();
		Calendar today = Calendar.getInstance();
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(timerTask, today.getTime(), 60*60*1000);
		
	}

	@Override
	public void run() {
		if(Util.isContestRunning){
			System.out.println("RETURN");
			return;
		}
		System.out.println("RUNNNNN");
		for(int i=0;i<1200;i++){
			List<Integer> customers = DAORegistry.getQueryManagerDAO().getCustomerIdToGenerateReferralCode(String.valueOf(i));
			if(customers.isEmpty()){
				break;
			}
			StringBuffer query = new StringBuffer();
			for(Integer customerId : customers){
				try {
					String code = com.rtw.tmat.utils.Util.generateCustomerReferalCode();
					query.append("UPDATE CUSTOMER SET referrer_code='"+code+"' WHERE id="+customerId+";");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			DAORegistry.getQueryManagerDAO().updateCustomerReferralCode(query.toString());
		}
		
		
		
	}

}
