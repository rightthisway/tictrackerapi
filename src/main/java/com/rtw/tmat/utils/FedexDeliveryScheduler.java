package com.rtw.tmat.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.InitializingBean;

import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.CustomerOrder;
import com.rtw.tracker.datas.Invoice;
import com.rtw.tracker.enums.InvoiceStatus;
import com.rtw.tracker.enums.PaymentMethod;
import com.rtw.tracker.fedex.track.TrackFedexShippingStatusUtil;
import com.rtw.tracker.utils.Util;

public class FedexDeliveryScheduler extends TimerTask implements InitializingBean{

	TrackFedexShippingStatusUtil util = null;
	
	@Override
	public void afterPropertiesSet() throws Exception {
		util = new TrackFedexShippingStatusUtil();
		TimerTask timerTask = new FedexDeliveryScheduler();
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE)+10);
		Timer timer = new Timer(true);
		timer.scheduleAtFixedRate(timerTask, cal.getTime(), 60*60*60*1000);
	}

	@Override
	public void run() {
		try{
			if(Util.isContestRunning){
				System.out.println("RETURN");
				return;
			}
			System.out.println("RUNNNNN");
			List<Invoice> invoices = DAORegistry.getInvoiceDAO().getFedexUndeliveredInvoices();
			boolean isDelivered = false;
			if(invoices!=null && !invoices.isEmpty()){
				for(Invoice invoice : invoices){
					if(invoice.getTrackingNo()!=null && !invoice.getTrackingNo().isEmpty()){
						isDelivered = util.isShipmentDelivered(invoice.getTrackingNo(), invoice.getId());
						if(isDelivered){
							invoice.setStatus(InvoiceStatus.Completed);
							invoice.setLastUpdated(new Date());
							invoice.setRealTixDelivered(true);
							invoice.setRealTixMap("Yes");
							invoice.setIsRealTixUploaded("Yes");
							if(invoice.getInvoiceType().equalsIgnoreCase(PaymentMethod.ACCOUNT_RECIVABLE.toString())){
								CustomerOrder order = DAORegistry.getCustomerOrderDAO().get(invoice.getCustomerOrderId());
								if(order.getPrimaryPaymentMethod().equals(PaymentMethod.ACCOUNT_RECIVABLE)){
									invoice.setStatus(InvoiceStatus.Outstanding);
								}
							}
							DAORegistry.getInvoiceDAO().update(invoice);
						}
						
					}
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}
