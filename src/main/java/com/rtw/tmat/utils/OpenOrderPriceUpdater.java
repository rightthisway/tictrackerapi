package com.rtw.tmat.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.InitializingBean;

import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.OpenOrderStatus;
import com.rtw.tracker.datas.ProductType;
import com.rtw.tracker.datas.SoldTicketDetail;
import com.rtw.tracker.mail.MailManager;
import com.rtw.tracker.utils.Util;



public class OpenOrderPriceUpdater extends TimerTask implements InitializingBean{

	ProductType[] productTypes={ProductType.REWARDTHEFAN,ProductType.RTW,ProductType.RTW2,ProductType.SEATGEEK}; 

public static MailManager mailManager;
	
	
	
	public static MailManager getMailManager() {
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		OpenOrderPriceUpdater.mailManager = mailManager;
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		TimerTask timerTask = new OpenOrderPriceUpdater();
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE)+5);
		Timer timer = new Timer(true);
		timer.scheduleAtFixedRate(timerTask, cal.getTime(), 30*60*1000);
	}
	
	@Override
	public void run() {
		try {
			if(Util.isContestRunning){
				System.out.println("RETURN");
				return;
			}
			System.out.println("RUNNNNN");
			for(ProductType productType : productTypes){
				Collection<SoldTicketDetail> ticketDetail =  new ArrayList<SoldTicketDetail>();
				List<OpenOrderStatus> rtwOpenOrders  = new ArrayList<OpenOrderStatus>();
				try{
					/*if(brokerId>0){
						 ticketDetail = DAORegistry.getSoldTicketDetailDAO().getAllSoldUnfilledTicketDetailsByBrokerId(brokerId);
						 rtwOpenOrders = DAORegistry.getOpenOrderStatusDao().getAllActiveOpenOrdersByBrokerId(brokerId);
					}else{
						 ticketDetail = DAORegistry.getSoldTicketDetailDAO().getRTFAllSoldUnfilledTicketDetails();
						 rtwOpenOrders = DAORegistry.getOpenOrderStatusDao().getRTFAllActiveOpenOrders();
					}*/
					 ticketDetail = DAORegistry.getSoldTicketDetailDAO().getAllSoldUnfilledTicketDetailsByProduct(productType);
					 rtwOpenOrders = DAORegistry.getOpenOrderStatusDao().getAllActiveOpenOrdersByProduct(productType);
					 //System.out.println("PRICEUPDATER ="+productType.toString()+"/ SOLDTICKET:"+ticketDetail.size()+"/ OPENORDER:"+rtwOpenOrders.size());
					 OpenOrderUtil.processSoldTickets(ticketDetail, rtwOpenOrders);
				}catch(Exception e){
					System.out.println("OPEN ORDER PRICEUPDATER JOB FAILED : "+productType+" : "+ new Date());
					e.printStackTrace();
					throw e;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("OPEN ORDER PRICEUPDATER Job Failed : "+new Date());
			try {
				 Map<String, Object> map = new HashMap<String, Object>();
				 map.put("error", ""+e.fillInStackTrace());
				 map.put("bodyContent", "Following error occured while processing Open Order Price Updater Job : ");

				 String subject = "TicTrac - Open Order Price Updater Job Failed : ";
				 String template = "common-mail-error.html";
				 String fromName  = "tmatmanagers@rightthisway.com";
				 String mimeType = "text/html";
				 String toAddress ="AODev@rightthisway.com";
				 //String ccAddress = "tselvan@rightthisway.com";
					
				 mailManager.sendMail(fromName, toAddress, null, null, subject, template, map, mimeType, null);
			} catch(Exception ex) {
				ex.printStackTrace();
				 System.out.println("OPEN ORDER PRICEUPDATER Job email alert Failed : "+new Date());
			}
			
		}
		
	}
	
}
