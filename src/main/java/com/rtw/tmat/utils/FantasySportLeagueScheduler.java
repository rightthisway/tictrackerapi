package com.rtw.tmat.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.InitializingBean;

import com.rtw.tmat.dao.implementation.TMATDAORegistry;
import com.rtw.tmat.data.CrownJewelEvents;
import com.rtw.tmat.data.Event;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.CrownJewelLeagues;
import com.rtw.tracker.datas.CrownJewelTeamZones;
import com.rtw.tracker.utils.Util;

public class FantasySportLeagueScheduler extends TimerTask implements InitializingBean{

	@Override
	public void afterPropertiesSet() throws Exception {
		TimerTask timerTask = new FantasySportLeagueScheduler();
		Calendar today = Calendar.getInstance();
		today.set(Calendar.HOUR, 02);
		today.set(Calendar.MINUTE, 00);
		today.set(Calendar.SECOND, 00);
		Timer timer = new Timer();
		//timer.scheduleAtFixedRate(timerTask, today.getTime(), 4*60*1000);
		timer.schedule(timerTask, today.getTime());		
	}

	@Override
	public void run() {
		try {
			if(Util.isContestRunning){
				System.out.println("RETURN");
				return;
			}
			System.out.println("RUNNNNN");
			Date today = new Date();
			List<CrownJewelEvents> cjEvents = new ArrayList<CrownJewelEvents>();
			List<CrownJewelLeagues> updateLeagues = new ArrayList<CrownJewelLeagues>();
			List<CrownJewelTeamZones> cjZones = new ArrayList<CrownJewelTeamZones>();
			
			List<CrownJewelLeagues> leagues = DAORegistry.getCrownJewelLeaguesDAO().getAllActiveLeagues();
			for(CrownJewelLeagues league : leagues){
				if(league.getEventId()!=null){
					Event event = TMATDAORegistry.getEventDAO().get(league.getEventId());
					CrownJewelEvents cjEvent = TMATDAORegistry.getCrownJewelEventsDAO().getCrownJewelEventByEventId(league.getEventId());
					if(event== null || (event.getDate()!=null && event.getDate().compareTo(today) <= 0)){
						if(cjEvent!=null){
							cjEvent.setStatus("DELETED");
							cjEvent.setLastUpdatedBy("AUTO");
							cjEvent.setLastUpdatedDate(today);
							cjEvents.add(cjEvent);
						}
						league.setEventId(null);
						league.setLastUpdated(today);
						league.setLastUpdateddBy("AUTO");
						updateLeagues.add(league);
						
					}
				}
			}
			
			if(!updateLeagues.isEmpty()){
				for(CrownJewelLeagues league : updateLeagues){
					List<CrownJewelTeamZones> zones = DAORegistry.getCrownJewelTeamZonesDAO().getAllActiveTeamZonesByLeagueId(league.getId());
					for(CrownJewelTeamZones zone : zones){
						zone.setStatus("DELETED");
						zone.setLastUpdated(today);
						zone.setLastUpdateddBy("AUTO");
						cjZones.add(zone);
					}
				}
				
				DAORegistry.getCrownJewelLeaguesDAO().updateAll(updateLeagues);
				DAORegistry.getCrownJewelTeamZonesDAO().updateAll(cjZones);
				TMATDAORegistry.getCrownJewelEventsDAO().updateAll(cjEvents);
			}
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		
	}

	
}
