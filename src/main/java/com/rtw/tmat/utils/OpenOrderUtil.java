package com.rtw.tmat.utils;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import com.rtw.tmat.dao.implementation.TMATDAORegistry;
import com.rtw.tmat.data.Category;
import com.rtw.tmat.data.CategoryMapping;
import com.rtw.tmat.data.DefaultPurchasePrice;
import com.rtw.tmat.data.Event;
import com.rtw.tmat.data.ManagePurchasePrice;
import com.rtw.tmat.data.Ticket;
import com.rtw.tmat.pojo.EventSectionZoneDetails;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.Invoice;
import com.rtw.tracker.datas.OpenOrderStatus;
import com.rtw.tracker.datas.SoldTicketDetail;
import com.rtw.tracker.enums.InvoiceStatus;

public class OpenOrderUtil {

	public static DateFormat dateformat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
	public static DecimalFormat df = new DecimalFormat("#.##");
	
	public static void processSoldTickets(Collection<SoldTicketDetail> ticketDetail,List<OpenOrderStatus> rtwOpenOrders){
		try{
			Map<Integer, Map<String, Boolean>> exEventMap = new HashMap<Integer, Map<String, Boolean>>();
			Map<Integer, Map<String, SoldTicketDetail>> exEventSoldTixMap = new HashMap<Integer, Map<String, SoldTicketDetail>>();
			
			
			Collection<DefaultPurchasePrice> defaultPurchasePrices = TMATDAORegistry.getDefaultPurchasePriceDAO().getAll();
			Map<String, DefaultPurchasePrice> defaultPurchasePriceMap = new HashMap<String, DefaultPurchasePrice>();
			for(DefaultPurchasePrice defaultPurchasePrice:defaultPurchasePrices){
				defaultPurchasePriceMap.put(defaultPurchasePrice.getExchange() + "-" + defaultPurchasePrice.getTicketType(), defaultPurchasePrice);
			}
			
			//Get all unique sections tmat zone from all active auto pricing products 
			Map<Integer,Map<String,String>> eventSectionZoneMap = new HashMap<Integer, Map<String,String>>();
			List<EventSectionZoneDetails> evnetSectionZoneList = TMATDAORegistry.getQueryManagerDAO().getAllAutopricingProductsSectionAndTmatZone();
			if(evnetSectionZoneList != null && !evnetSectionZoneList.isEmpty()) {
				for (EventSectionZoneDetails eventSectionZone : evnetSectionZoneList) {
					String section = eventSectionZone.getSection().replaceAll("\\s+", " ").toLowerCase();
					
					Map<String,String> sectionZoneMap = eventSectionZoneMap.get(eventSectionZone.getEventId());
					if(null != sectionZoneMap){
						sectionZoneMap.put(section, eventSectionZone.getZone());
						eventSectionZoneMap.put(eventSectionZone.getEventId(),sectionZoneMap);
					}else{
						sectionZoneMap = new HashMap<String, String>();
						sectionZoneMap.put(section,eventSectionZone.getZone());
						eventSectionZoneMap.put(eventSectionZone.getEventId(), sectionZoneMap);
					}
				}
			}
			
			for (SoldTicketDetail soldTixDetail : ticketDetail) {
				
				Map<String, SoldTicketDetail> soldTixMap = exEventSoldTixMap.get(soldTixDetail.getTmatEventId());
				if(null != soldTixMap && !soldTixMap.isEmpty()){
					exEventSoldTixMap.get(soldTixDetail.getTmatEventId()).put(soldTixDetail.getInvoiceId()+":"+soldTixDetail.getId(), soldTixDetail);
				}else{
					soldTixMap = new HashMap<String, SoldTicketDetail>();
					soldTixMap.put(soldTixDetail.getInvoiceId()+":"+soldTixDetail.getId(), soldTixDetail);
					exEventSoldTixMap.put(soldTixDetail.getTmatEventId(), soldTixMap);
				}
				
				Map<String, Boolean> map = exEventMap.get(soldTixDetail.getTmatEventId());
				if(null != map && !map.isEmpty()){
					exEventMap.get(soldTixDetail.getTmatEventId()).put(soldTixDetail.getSection().toLowerCase(), true);
				}else{
					map = new HashMap<String, Boolean>();
					map.put(soldTixDetail.getSection().toLowerCase(), true);
					exEventMap.put(soldTixDetail.getTmatEventId(), map);
				}
				
			}
			
			Map<String, OpenOrderStatus> openOrdersMap = new HashMap<String, OpenOrderStatus>();
			for (OpenOrderStatus opOrderStatus : rtwOpenOrders) {
				openOrdersMap.put(opOrderStatus.getInvoiceNo()+":"+opOrderStatus.getTicketGroupId() , opOrderStatus);
			}
			//System.out.println("OPEN ORDER MAP SIZE:"+openOrdersMap.size());
			Map<String, List<Ticket>> sectionTickets = null;
			Map<String, List<Ticket>> zoneSectionTickets = null;
			Integer eventTicketsQty , sectionTixCount;
			List<OpenOrderStatus> tobeSavedOrUpdatedOrders = null;
			
			String zoneKey = "";
			
			for (Integer exEventId : exEventMap.keySet()) {
				eventTicketsQty = 0;
				Event event = TMATDAORegistry.getEventDAO().getEventById(exEventId);
				if(event==null){
					continue;
				}
				Integer tmatEventId = null != event?event.getId():0;
				Collection<Ticket> tickets = TMATDAORegistry.getTicketDAO().getAllActiveTicketsByEvent(tmatEventId);
				
				
				tobeSavedOrUpdatedOrders = new ArrayList<OpenOrderStatus>();
				
				if(null == event || (null == tickets || tickets.isEmpty())){
					
					Map<String, SoldTicketDetail> soldTixMap = exEventSoldTixMap.get(tmatEventId);
					
					for (SoldTicketDetail soldTicketDetail : soldTixMap.values()) {
						OpenOrderStatus opOrderStatus  = openOrdersMap.remove(soldTicketDetail.getInvoiceId()+":"+soldTicketDetail.getId());
						if(null == opOrderStatus ){
							continue;
						}else{
							opOrderStatus.setProfitAndLoss(0.00);
							opOrderStatus.setSectionTixQty(0);
							opOrderStatus.setEventTixQty(0);
							
							if(opOrderStatus.getLastUpdatedPrice() > 0 || opOrderStatus.getZoneCheapestPrice()>0){
								
								if(opOrderStatus.getLastUpdatedPrice() > 0){
									opOrderStatus.setLastUpdatedPrice(opOrderStatus.getMarketPrice());
									opOrderStatus.setPriceUpdateCount(opOrderStatus.getPriceUpdateCount()+1);
									opOrderStatus.setMarketPrice(0.00);
									opOrderStatus.setOnlinePrice(0.00);
								}
								
								opOrderStatus.setZoneCheapestPrice(0.0);
								opOrderStatus.setZoneTixQty(0);
								//opOrderStatus.setZone("");
								opOrderStatus.setZoneTixGroupCount(0);
								opOrderStatus.setZoneTixGroupCount(0);
								opOrderStatus.setZoneProfitAndLoss(0.0);
								opOrderStatus.setZoneTotalPrice(0.0);
								opOrderStatus.setZoneMargin(0.0);
								opOrderStatus.setSectionMargin(0.0);
								
								tobeSavedOrUpdatedOrders.add(opOrderStatus);
							}
							
						}
					}
					
				}else{
					
					Collection<ManagePurchasePrice> managePurchasePricelist = TMATDAORegistry.getManagePurchasePriceDAO().getAllManagePurchasePriceByEventId(tmatEventId);//tour.getTmatPurchaseId());
					Map<String, ManagePurchasePrice> tourPriceMap = new HashMap<String, ManagePurchasePrice>();
					for(ManagePurchasePrice tourPrice:managePurchasePricelist){
						tourPriceMap.put(tourPrice.getExchange()+"-" + tourPrice.getTicketType(),tourPrice);
					}
					
					Collection<Category> categories = null;
					if(event.getVenueCategory()!=null){
						categories = TMATDAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategory().getId());
					}else{
						List<String> venueCategories = TMATDAORegistry.getVenueCategoryDAO().getCategoryGroupByVenueId(event.getVenueId());
						categories = TMATDAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(event.getVenueId(), venueCategories.get(0));
					}
					Map<Integer,Category> categoryMap = new HashMap<Integer, Category>();
					if(categories != null) {
						for (Category category : categories) {
							categoryMap.put(category.getId(), category);
						}
					}
					tickets = TicketUtil.preAssignCategoriesToTickets(tickets, categories,event); 
					sectionTickets = new HashMap<String, List<Ticket>>();
					zoneSectionTickets = new HashMap<String, List<Ticket>>();
					
					for (Ticket ticket : tickets) {
						
						TicketUtil.getPurchasePrice(ticket, defaultPurchasePriceMap, null, tourPriceMap);
						
						eventTicketsQty += ticket.getRemainingQuantity();
						
						String sectionKey = ticket.getNormalizedSection().replaceAll("\\s+", " ").toLowerCase();
						List<Ticket> sectionTixs = sectionTickets.get(sectionKey);
						if(null != sectionTixs && !sectionTixs.isEmpty()){
							sectionTickets.get(sectionKey).add(ticket);
						}else{
							sectionTixs = new ArrayList<Ticket>();
							sectionTixs.add(ticket);
							sectionTickets.put(sectionKey,sectionTixs);
						}
						
						if(null != ticket.getCategory() && null != ticket.getCategory().getSymbol()){
							zoneKey = ("ZONE "+ticket.getCategory().getSymbol()).toLowerCase();
							List<Ticket> zoneSectionTixs = zoneSectionTickets.get(zoneKey);
							if(null != zoneSectionTixs && !zoneSectionTixs.isEmpty()){
								zoneSectionTickets.get(zoneKey).add(ticket);
								continue;
							}
							zoneSectionTixs = new ArrayList<Ticket>();
							zoneSectionTixs.add(ticket);
							zoneSectionTickets.put(zoneKey,zoneSectionTixs);
						}
					}
					
					Map<String,List<CategoryMapping>> catMappingMap = Categorizer.getCategoryMappingByVenueCategoryId(event);
					
					Map<String, String> tmatAvailableSectionMap = new HashMap<String, String>();
					Map<String, String> tmatAvailableZoneMap = new HashMap<String, String>();
					
					Map<String, SoldTicketDetail> soldTixMap = exEventSoldTixMap.get(exEventId);
					boolean isUpdate = false;
					for (SoldTicketDetail soldTicketDetail : soldTixMap.values()) {
						
						Double oldMarketPrice=0.0 , marketPrice = 0.0,onlinePrice=0.0;
						Double profitandLoss = 0.0,zoneTotalPrice=0.0,zoneProfitandLoss=0.0,sectionMargin =0.0,zoneMargin=0.0;
						Integer zoneTixCount=0,zoneTixGroupCount =0;
						Double zoneCheapestPrice = 0.0,oldZoneCheapestPrice;
						String zone = null;
						
						OpenOrderStatus opOrderStatus  = openOrdersMap.remove(soldTicketDetail.getInvoiceId()+":"+soldTicketDetail.getId());
						String orderSection = soldTicketDetail.getSection().replaceAll("\\s+", " ").toLowerCase();
						orderSection = orderSection.replaceAll("[.]$", "");
						String tmatPriceDetails = tmatAvailableSectionMap.get(orderSection);
						if(null != tmatPriceDetails){
							sectionTixCount =( null != tmatPriceDetails.split(":")[0] && !tmatPriceDetails.split(":")[0].isEmpty()) ? Integer.parseInt(tmatPriceDetails.split(":")[0]):0;
							marketPrice = ( null != tmatPriceDetails.split(":")[1] && !tmatPriceDetails.split(":")[1].isEmpty() ) ? Double.valueOf(tmatPriceDetails.split(":")[1]):0;
							onlinePrice = ( null != tmatPriceDetails.split(":")[2] && !tmatPriceDetails.split(":")[2].isEmpty() ) ? Double.valueOf(tmatPriceDetails.split(":")[2]):0;
						} else {
							
							List<Ticket> tmatSectionTickets = new ArrayList<Ticket>();
							
							if(soldTicketDetail.getProductType() != null &&
									soldTicketDetail.getProductType().equalsIgnoreCase("REWARDTHEFAN")) {
								zoneKey = ("ZONE "+soldTicketDetail.getSection()).toLowerCase();
								zone = soldTicketDetail.getSection();
								tmatSectionTickets =  zoneSectionTickets.get(zoneKey);
							} else if((orderSection.contains("ZONE") || orderSection.contains("zone"))){
								tmatSectionTickets =  zoneSectionTickets.get(orderSection);
							}else{
								tmatSectionTickets =  sectionTickets.get(orderSection);
							}
							
							sectionTixCount = 0;
							if(null != tmatSectionTickets && !tmatSectionTickets.isEmpty() && tmatSectionTickets.size() > 0){
								List<Ticket> sectionQtyTickets = new ArrayList<Ticket>();
								for (Ticket ticket : tmatSectionTickets) {
									if(ticket.getRemainingQuantity() < soldTicketDetail.getQuantity()){
										continue;
									}
									sectionQtyTickets.add(ticket);
									sectionTixCount += ticket.getRemainingQuantity();
								}
								Ticket ticket = null;
								if(sectionQtyTickets.size() >0 ){
									Collections.sort(sectionQtyTickets, OpenOrderUtil.sortingTicketbyPrice);
									ticket = sectionQtyTickets.get(0);
								}
								if(ticket!=null){
									marketPrice = ticket.getPurchasePrice();
									onlinePrice = ticket.getBuyItNowPrice();
									
									if(zone == null && ticket.getCategory() != null) {
										zone = ticket.getCategory().getSymbol();
									}
								}else{
									marketPrice = 0.00;
									onlinePrice = 0.00;
								}
							}else{
								marketPrice = 0.00;
								onlinePrice = 0.00;
							}
							tmatAvailableSectionMap.put(orderSection, sectionTixCount+":"+marketPrice+":"+onlinePrice);
						}
						String zoneDetailStr = tmatAvailableZoneMap.get(orderSection);
						if(zoneDetailStr != null) {
							zone = zoneDetailStr.split(":")[0];
							zoneTixCount = Integer.parseInt(zoneDetailStr.split(":")[1]);
							zoneCheapestPrice = Double.parseDouble(zoneDetailStr.split(":")[2]);
							zoneTixGroupCount = Integer.parseInt(zoneDetailStr.split(":")[3]);
						} else {
							
							if(zone == null) {
								List<CategoryMapping> categoryMappingList = catMappingMap.get(orderSection);
								if(categoryMappingList != null) {
									for (CategoryMapping catMapping : categoryMappingList) {
										Category category = categoryMap.get(catMapping.getCategoryId());
										if(category != null){
											zone = category.getSymbol();
											break;
										}
									}
								}
							}
							if(zone == null) {
								if(soldTicketDetail.getInternalNotes() != null) {
									if(soldTicketDetail.getInternalNotes().equalsIgnoreCase("MINICATS") || soldTicketDetail.getInternalNotes().equalsIgnoreCase("VIPMINICATS") ||
											soldTicketDetail.getInternalNotes().equalsIgnoreCase("LASTROW MINICATS") || soldTicketDetail.getInternalNotes().equalsIgnoreCase("AUTOCAT")) {
										zone = TMATDAORegistry.getQueryManagerDAO().getAutocats96CategoryTicketZoneByTnCategoryTicketGroupId(tmatEventId,soldTicketDetail.getId(),
												soldTicketDetail.getBrokerId());
									}
								} else if (soldTicketDetail.getInternalNotes().equalsIgnoreCase("LarryLast NOSTUB NOTNOW NOVIVID NOTEVO")) {
									zone = TMATDAORegistry.getQueryManagerDAO().getLarryLastCategoryTicketZoneByTnTicketGroupId(tmatEventId,soldTicketDetail.getId(),
											soldTicketDetail.getBrokerId());
								} else if (soldTicketDetail.getInternalNotes().equalsIgnoreCase("ZLR NOSTUB NOTNOW NOVIVID NOTEVO")) {
									zone = TMATDAORegistry.getQueryManagerDAO().getZoneLastRowCategoryTicketZoneByTnTicketGroupId(tmatEventId,soldTicketDetail.getId(),
											soldTicketDetail.getBrokerId());
								}
							}
							if(zone == null) {
								Map<String,String> sectionZoneMap =  eventSectionZoneMap.get(tmatEventId);
								if(sectionZoneMap != null) {
									zone = sectionZoneMap.get(orderSection);
								}
							}
							List<Ticket> tmatZoneTickets = new ArrayList<Ticket>();
							if(zone != null) {
								zoneKey = ("ZONE "+zone).toLowerCase();
								tmatZoneTickets = zoneSectionTickets.get(zoneKey);
								if(tmatZoneTickets != null && tmatZoneTickets.size()>0) {
									
									List<Ticket> zoneQtyTickets = new ArrayList<Ticket>();
									for (Ticket ticket : tmatZoneTickets) {
										if(ticket.getRemainingQuantity() < soldTicketDetail.getQuantity()){
											continue;
										}
										zoneQtyTickets.add(ticket);
										zoneTixCount += ticket.getRemainingQuantity();
									}
									
									if(zoneQtyTickets.size()>0){
										Collections.sort(zoneQtyTickets, OpenOrderUtil.sortingTicketbyPrice);
										zoneCheapestPrice = zoneQtyTickets.get(0).getPurchasePrice();
									}else{
										zoneCheapestPrice = 0.00;
									}
									
									//zoneCheapestPrice = tmatZoneTickets.get(0).getPurchasePrice();
									zoneTixGroupCount = zoneSectionTickets.size();
								}else{
									zoneCheapestPrice = 0.00;
								}
								tmatAvailableZoneMap.put(orderSection, zone.toUpperCase()+":"+zoneTixCount+":"+zoneCheapestPrice+":"+zoneTixGroupCount);
							} else {
								//System.out.println("Missing Zone eventID : "+soldTicketDetail.getTmatEventId()+" : internal notes : "+soldTicketDetail.getInternalNotes()+" : section  : "+soldTicketDetail.getSection());
								zone = "";
								zoneTixCount = 0;
								zoneTixGroupCount = 0;
								zoneCheapestPrice = 0.0;
							}
						}
						isUpdate = false;
						
						if(null == opOrderStatus  ){
							continue;
						}else{
							
							opOrderStatus.computeNetTotalSoldPrice();
							opOrderStatus.computeNetSoldPrice();
							
							marketPrice = Double.valueOf(df.format(marketPrice));
							oldMarketPrice = Double.valueOf(df.format(opOrderStatus.getMarketPrice()));
							
							//System.out.println("New Price :"+marketPrice+"-----Old Price :"+oldMarketPrice);
							
							if(sectionTixCount > 0){
								if(!marketPrice.equals(oldMarketPrice)){

									opOrderStatus.setLastUpdatedPrice(oldMarketPrice);
									profitandLoss = Double.valueOf(df.format(opOrderStatus.getNetTotalSoldPrice() - (marketPrice * opOrderStatus.getSoldQty())));
									sectionMargin = Double.valueOf(df.format(profitandLoss*100/opOrderStatus.getNetTotalSoldPrice()));
									opOrderStatus.setMarketPrice(marketPrice);
									opOrderStatus.setOnlinePrice(onlinePrice);
									opOrderStatus.setProfitAndLoss(profitandLoss);
									opOrderStatus.setSectionMargin(sectionMargin);
									opOrderStatus.setPriceUpdateCount(opOrderStatus.getPriceUpdateCount()+1);
									isUpdate = true;
								}
								if(!sectionTixCount.equals(opOrderStatus.getSectionTixQty())){
									opOrderStatus.setSectionTixQty(sectionTixCount);
									opOrderStatus.setEventTixQty(eventTicketsQty);
									isUpdate = true;
								}
							}else{
								
								if(opOrderStatus.getSectionTixQty() > 0 || opOrderStatus.getProfitAndLoss() != 0 || opOrderStatus.getSectionMargin() != 0){
									opOrderStatus.setLastUpdatedPrice(oldMarketPrice);
									opOrderStatus.setSectionTixQty(sectionTixCount);
									opOrderStatus.setEventTixQty(eventTicketsQty);
									opOrderStatus.setMarketPrice(0.00);
									opOrderStatus.setOnlinePrice(0.00);
									opOrderStatus.setProfitAndLoss(0.00);
									opOrderStatus.setSectionMargin(0.00);
									opOrderStatus.setPriceUpdateCount(opOrderStatus.getPriceUpdateCount()+1);
									isUpdate = true;
								}
								
							}
							
							zoneCheapestPrice = Double.valueOf(df.format(zoneCheapestPrice));
							oldZoneCheapestPrice = Double.valueOf(df.format(opOrderStatus.getZoneCheapestPrice()));
							if(zoneTixCount > 0) {
								if(!zoneCheapestPrice.equals(oldZoneCheapestPrice)){
									zoneTotalPrice = Double.valueOf(df.format(zoneCheapestPrice * opOrderStatus.getSoldQty()));
									zoneProfitandLoss = Double.valueOf(df.format(opOrderStatus.getNetTotalSoldPrice() - zoneTotalPrice));
									zoneMargin = Double.valueOf(df.format(zoneProfitandLoss*100/opOrderStatus.getNetTotalSoldPrice()));
									
									opOrderStatus.setZoneProfitAndLoss(zoneProfitandLoss);
									opOrderStatus.setZoneTotalPrice(zoneTotalPrice);
									opOrderStatus.setZoneMargin(zoneMargin);
									opOrderStatus.setZoneCheapestPrice(zoneCheapestPrice);
									opOrderStatus.setZone(zone);
									isUpdate = true;
								}
								if(!zoneTixCount.equals(opOrderStatus.getZoneTixQty()) || !zoneTixGroupCount.equals(opOrderStatus.getZoneTixGroupCount())){
									opOrderStatus.setZoneTixQty(zoneTixCount);
									opOrderStatus.setZoneTixGroupCount(zoneTixGroupCount);
									isUpdate = true;
								}
								
							} else {
								
								if(opOrderStatus.getZoneTixQty() > 0 || opOrderStatus.getZoneTixGroupCount() > 0 ||
										 opOrderStatus.getZoneTotalPrice() != 0 || opOrderStatus.getZoneProfitAndLoss() != 0 ||
										 opOrderStatus.getZoneMargin() != 0){
									opOrderStatus.setZoneProfitAndLoss(0.0);
									opOrderStatus.setZoneTotalPrice(0.0);
									opOrderStatus.setZoneCheapestPrice(0.0);
									opOrderStatus.setZoneMargin(0.00);
									opOrderStatus.setZoneTixQty(0);
									opOrderStatus.setZoneTixGroupCount(0);
									opOrderStatus.setZone(zone);
									isUpdate = true;
								}
							}
							
							if(isUpdate){
								opOrderStatus.setLastUpdated(new Date());
								tobeSavedOrUpdatedOrders.add(opOrderStatus);
							}
						}
					}
				}
				
				if(tobeSavedOrUpdatedOrders.size() > 0 ){
					//System.out.println("To be updated or saved Orders : "+tobeSavedOrUpdatedOrders.size());
					for(OpenOrderStatus openOrderStatus : tobeSavedOrUpdatedOrders){
						SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
						  if(openOrderStatus.getLastUpdatedPriceDate() != null){
							Date todayDate = dateFormat.parse(dateFormat.format(new Date()));
							Calendar cal = Calendar.getInstance();
							cal.setTime(todayDate);
							int days = -4;
							cal.add(Calendar.DATE, days);
							Date date  = cal.getTime(); 
							
							TimeZone timeZone = TimeZone.getDefault();
							Calendar cal1 = Calendar.getInstance(timeZone);
							dateFormat.setCalendar(cal);
							cal1.setTime(dateFormat.parse(dateFormat.format(openOrderStatus.getLastUpdatedPriceDate())));
							Date lastUpdated = cal1.getTime();
							if(date.compareTo(lastUpdated) >= 0){
								openOrderStatus.setLastUpdatedPriceDate(null);
							}
							
						  }
						  DAORegistry.getOpenOrderStatusDao().saveOrUpdate(openOrderStatus);
					}
				}
			}
		
			if(null !=openOrdersMap && !openOrdersMap.isEmpty()){
				//Date today = new Date();
				for (OpenOrderStatus orderStatus : openOrdersMap.values()) {
					/*Date eventDateTime = Util.getDateAndTime(orderStatus.getEventDate(), orderStatus.getEventTime());
					System.out.println(orderStatus.getInvoiceNo()+" == "+eventDateTime+" == "+today);
					if(eventDateTime.compareTo(today) < 0){
						
						System.out.println("DELETED:"+orderStatus.getProductType()+" : inID : "+orderStatus.getInvoiceNo()+" : date : "+new Date());
					}*/
					if(orderStatus.getProductType().equalsIgnoreCase("REWARDTHEFAN")){
						Invoice invoice = DAORegistry.getInvoiceDAO().get(orderStatus.getInvoiceNo());
						if(invoice!= null && invoice.getStatus().equals(InvoiceStatus.Outstanding)){
							continue;
						}
					}
					orderStatus.setStatus("DELETED");
					orderStatus.setLastUpdated(new Date());
					DAORegistry.getOpenOrderStatusDao().saveOrUpdate(orderStatus);
				}
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	public static void addNewOpenOrders(Collection<SoldTicketDetail> ticketDetail){
		try{
			Map<Integer, Map<String, Boolean>> exEventMap = new HashMap<Integer, Map<String, Boolean>>();
			Map<Integer, Map<String, SoldTicketDetail>> exEventSoldTixMap = new HashMap<Integer, Map<String, SoldTicketDetail>>();
			
			
			Collection<DefaultPurchasePrice> defaultPurchasePrices = TMATDAORegistry.getDefaultPurchasePriceDAO().getAll();
			Map<String, DefaultPurchasePrice> defaultPurchasePriceMap = new HashMap<String, DefaultPurchasePrice>();
			for(DefaultPurchasePrice defaultPurchasePrice:defaultPurchasePrices){
				defaultPurchasePriceMap.put(defaultPurchasePrice.getExchange() + "-" + defaultPurchasePrice.getTicketType(), defaultPurchasePrice);
			}
			
			//Get all unique sections tmat zone from all active auto pricing products 
			Map<Integer,Map<String,String>> eventSectionZoneMap = new HashMap<Integer, Map<String,String>>();
			List<EventSectionZoneDetails> evnetSectionZoneList = TMATDAORegistry.getQueryManagerDAO().getAllAutopricingProductsSectionAndTmatZone();
			if(evnetSectionZoneList != null && !evnetSectionZoneList.isEmpty()) {
				for (EventSectionZoneDetails eventSectionZone : evnetSectionZoneList) {
					String section = eventSectionZone.getSection().replaceAll("\\s+", " ").toLowerCase();
					
					Map<String,String> sectionZoneMap = eventSectionZoneMap.get(eventSectionZone.getEventId());
					if(null != sectionZoneMap){
						sectionZoneMap.put(section, eventSectionZone.getZone());
						eventSectionZoneMap.put(eventSectionZone.getEventId(),sectionZoneMap);
					}else{
						sectionZoneMap = new HashMap<String, String>();
						sectionZoneMap.put(section,eventSectionZone.getZone());
						eventSectionZoneMap.put(eventSectionZone.getEventId(), sectionZoneMap);
					}
				}
			}
			
			List<OpenOrderStatus> rtwOpenOrders = DAORegistry.getOpenOrderStatusDao().getAllActiveOpenOrders();
			Map<String, OpenOrderStatus> openOrdersMap = new HashMap<String, OpenOrderStatus>();
			for (OpenOrderStatus opOrderStatus : rtwOpenOrders) {
				openOrdersMap.put(opOrderStatus.getInvoiceNo()+":"+opOrderStatus.getProductType() , opOrderStatus);
			}
			
			for (SoldTicketDetail soldTixDetail : ticketDetail) {
				OpenOrderStatus opOrderStatus = openOrdersMap.get(soldTixDetail.getInvoiceId()+":"+soldTixDetail.getProductType());
				if(opOrderStatus != null) {
					continue;
				}
				Map<String, SoldTicketDetail> soldTixMap = exEventSoldTixMap.get(soldTixDetail.getTmatEventId());
				if(null != soldTixMap && !soldTixMap.isEmpty()){
					exEventSoldTixMap.get(soldTixDetail.getTmatEventId()).put(soldTixDetail.getInvoiceId()+":"+soldTixDetail.getId(), soldTixDetail);
				}else{
					soldTixMap = new HashMap<String, SoldTicketDetail>();
					soldTixMap.put(soldTixDetail.getInvoiceId()+":"+soldTixDetail.getId(), soldTixDetail);
					exEventSoldTixMap.put(soldTixDetail.getTmatEventId(), soldTixMap);
				}
				
				try {
				Map<String, Boolean> map = exEventMap.get(soldTixDetail.getTmatEventId());
				if(null != map && !map.isEmpty()){
					exEventMap.get(soldTixDetail.getTmatEventId()).put(soldTixDetail.getSection().toLowerCase(), true);
				}else{
					map = new HashMap<String, Boolean>();
					map.put(soldTixDetail.getSection().toLowerCase(), true);
					exEventMap.put(soldTixDetail.getTmatEventId(), map);
				}
				}catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
			Map<String, List<Ticket>> sectionTickets = null;
			Map<String, List<Ticket>> zoneSectionTickets = null;
			Integer eventTicketsQty , sectionTixCount;
			List<OpenOrderStatus> tobeSavedOrUpdatedOrders = null;
			
			String zoneKey = "";
			
			for (Integer exEventId : exEventMap.keySet()) {
				eventTicketsQty = 0;
				Event event = TMATDAORegistry.getEventDAO().getEventById(exEventId);
				if(event==null){
					//System.out.println("Missing TMAT evnet ID : "+exEventId);
					continue;
				}
				Integer tmatEventId = null != event?event.getId():0;
				Collection<Ticket> tickets = TMATDAORegistry.getTicketDAO().getAllActiveTicketsByEvent(tmatEventId);
				
				tobeSavedOrUpdatedOrders = new ArrayList<OpenOrderStatus>();
				
				if(null == event || (null == tickets || tickets.isEmpty())){
					
					Map<String, SoldTicketDetail> soldTixMap = exEventSoldTixMap.get(tmatEventId);
					
					for (SoldTicketDetail soldTicketDetail : soldTixMap.values()) {
						OpenOrderStatus opOrderStatus = new OpenOrderStatus(soldTicketDetail);
						opOrderStatus.setTmatEventId(tmatEventId);
						//opOrderStatus.setExEventId(exEventId);
						opOrderStatus.setCreatedDate(new Date());
						opOrderStatus.setLastUpdated(new Date());
						opOrderStatus.setStatus("ACTIVE");
						opOrderStatus.setVenueId(-1);
						opOrderStatus.setMarketPrice(0.00);
						opOrderStatus.setOnlinePrice(0.00);
						opOrderStatus.setProfitAndLoss(0.00);
						opOrderStatus.setSectionTixQty(0);
						opOrderStatus.setEventTixQty(0);
						opOrderStatus.setLastUpdatedPrice(opOrderStatus.getActualSoldPrice());
						opOrderStatus.setPriceUpdateCount(1);
						opOrderStatus.setZone("");
						opOrderStatus.setZoneCheapestPrice(0.0);
						opOrderStatus.setZoneTixQty(0);
						opOrderStatus.setZoneTixGroupCount(0);
						opOrderStatus.setZoneTotalPrice(0.0);
						opOrderStatus.setZoneProfitAndLoss(0.0);
						opOrderStatus.setSectionMargin(0.0);
						opOrderStatus.setZoneMargin(0.0);
						
						tobeSavedOrUpdatedOrders.add(opOrderStatus);
					}
					
				}else{
					

					Collection<ManagePurchasePrice> managePurchasePricelist = TMATDAORegistry.getManagePurchasePriceDAO().getAllManagePurchasePriceByEventId(tmatEventId);//tour.getTmatPurchaseId());
					Map<String, ManagePurchasePrice> tourPriceMap = new HashMap<String, ManagePurchasePrice>();
					for(ManagePurchasePrice tourPrice:managePurchasePricelist){
						tourPriceMap.put(tourPrice.getExchange()+"-" + tourPrice.getTicketType(),tourPrice);
					}
					Collection<Category> categories = null;
					if(event.getVenueCategory()!=null){
						categories = TMATDAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategory().getId());
					}else{
						List<String> venueCategories = TMATDAORegistry.getVenueCategoryDAO().getCategoryGroupByVenueId(event.getVenueId());
						categories = TMATDAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(event.getVenueId(), venueCategories.get(0));
					}
					Map<Integer,Category> categoryMap = new HashMap<Integer, Category>();
					if(categories != null) {
						for (Category category : categories) {
							categoryMap.put(category.getId(), category);
						}
					}
					tickets = TicketUtil.preAssignCategoriesToTickets(tickets, categories,event); 
					sectionTickets = new HashMap<String, List<Ticket>>();
					zoneSectionTickets = new HashMap<String, List<Ticket>>();
					
					for (Ticket ticket : tickets) {
						
						TicketUtil.getPurchasePrice(ticket, defaultPurchasePriceMap, null, tourPriceMap);
						
						eventTicketsQty += ticket.getRemainingQuantity();
						
						String sectionKey = ticket.getNormalizedSection().replaceAll("\\s+", " ").toLowerCase();
						List<Ticket> sectionTixs = sectionTickets.get(sectionKey);
						if(null != sectionTixs && !sectionTixs.isEmpty()){
							sectionTickets.get(sectionKey).add(ticket);
						}else{
							sectionTixs = new ArrayList<Ticket>();
							sectionTixs.add(ticket);
							sectionTickets.put(sectionKey,sectionTixs);
						}
						
						if(null != ticket.getCategory() && null != ticket.getCategory().getSymbol()){
							zoneKey = ("ZONE "+ticket.getCategory().getSymbol()).toLowerCase();
							List<Ticket> zoneSectionTixs = zoneSectionTickets.get(zoneKey);
							if(null != zoneSectionTixs && !zoneSectionTixs.isEmpty()){
								zoneSectionTickets.get(zoneKey).add(ticket);
								continue;
							}
							zoneSectionTixs = new ArrayList<Ticket>();
							zoneSectionTixs.add(ticket);
							zoneSectionTickets.put(zoneKey,zoneSectionTixs);
						}
					}
					
					Map<String,List<CategoryMapping>> catMappingMap = Categorizer.getCategoryMappingByVenueCategoryId(event);
					
					Map<String, String> tmatAvailableSectionMap = new HashMap<String, String>();
					Map<String, String> tmatAvailableZoneMap = new HashMap<String, String>();
					
					Map<String, SoldTicketDetail> soldTixMap = exEventSoldTixMap.get(exEventId);
					for (SoldTicketDetail soldTicketDetail : soldTixMap.values()) {
						Double oldMarketPrice=0.0 , marketPrice = 0.0,onlinePrice=0.0;
						Double profitandLoss = 0.0,zoneTotalPrice=0.0,zoneProfitandLoss=0.0,sectionMargin =0.0,zoneMargin=0.0;
						int zoneTixCount=0,zoneTixGroupCount =0;
						Double zoneCheapestPrice = 0.0;
						String zone = null;
						
						//OpenOrderStatus opOrderStatus  = openOrdersMap.remove(soldTicketDetail.getInvoiceId()+":"+soldTicketDetail.getId());
						String orderSection = soldTicketDetail.getSection().replaceAll("\\s+", " ").toLowerCase();
						orderSection = orderSection.replaceAll("[.]$", "");
						String tmatPriceDetails = tmatAvailableSectionMap.get(orderSection);
						if(null != tmatPriceDetails){
							sectionTixCount =( null != tmatPriceDetails.split(":")[0] && !tmatPriceDetails.split(":")[0].isEmpty()) ? Integer.parseInt(tmatPriceDetails.split(":")[0]):0;
							marketPrice = ( null != tmatPriceDetails.split(":")[1] && !tmatPriceDetails.split(":")[1].isEmpty() ) ? Double.valueOf(tmatPriceDetails.split(":")[1]):0;
							onlinePrice = ( null != tmatPriceDetails.split(":")[2] && !tmatPriceDetails.split(":")[2].isEmpty() ) ? Double.valueOf(tmatPriceDetails.split(":")[2]):0;
						} else {
							
							List<Ticket> tmatSectionTickets = new ArrayList<Ticket>();
							
							if(soldTicketDetail.getProductType() != null &&
									soldTicketDetail.getProductType().equalsIgnoreCase("REWARDTHEFAN")) {
								zoneKey = ("ZONE "+soldTicketDetail.getSection()).toLowerCase();
								zone = soldTicketDetail.getSection();
								tmatSectionTickets =  zoneSectionTickets.get(zoneKey);
							} else if((orderSection.contains("ZONE") || orderSection.contains("zone"))){
								tmatSectionTickets =  zoneSectionTickets.get(orderSection);
							}else{
								tmatSectionTickets =  sectionTickets.get(orderSection);
							}
							
							sectionTixCount = 0;
							if(null != tmatSectionTickets && !tmatSectionTickets.isEmpty() && tmatSectionTickets.size() > 0){
								Collections.sort(tmatSectionTickets, OpenOrderUtil.sortingTicketbyPrice);
								for (Ticket ticket : tmatSectionTickets) {
									sectionTixCount += ticket.getRemainingQuantity();
								}
								Ticket ticket = tmatSectionTickets.get(0);
								marketPrice = ticket.getPurchasePrice();
								onlinePrice = ticket.getBuyItNowPrice();
								
								if(zone == null && ticket.getCategory() != null) {
									zone = ticket.getCategory().getSymbol();
								}
							}else{
								marketPrice = 0.00;
								onlinePrice = 0.00;
							}
							tmatAvailableSectionMap.put(orderSection, sectionTixCount+":"+marketPrice+":"+onlinePrice);
						}
						String zoneDetailStr = tmatAvailableZoneMap.get(orderSection);
						if(zoneDetailStr != null) {
							zone = zoneDetailStr.split(":")[0];
							zoneTixCount = Integer.parseInt(zoneDetailStr.split(":")[1]);
							zoneCheapestPrice = Double.parseDouble(zoneDetailStr.split(":")[2]);
							zoneTixGroupCount = Integer.parseInt(zoneDetailStr.split(":")[3]);
						} else {
							
							if(zone == null) {
								List<CategoryMapping> categoryMappingList = catMappingMap.get(orderSection);
								if(categoryMappingList != null) {
									for (CategoryMapping catMapping : categoryMappingList) {
										Category category = categoryMap.get(catMapping.getCategoryId());
										if(category != null){
											zone = category.getSymbol();
											break;
										}
									}
								}
							}
							if(zone == null) {
								if(soldTicketDetail.getInternalNotes() != null) {
									if(soldTicketDetail.getInternalNotes().equalsIgnoreCase("MINICATS") || soldTicketDetail.getInternalNotes().equalsIgnoreCase("VIPMINICATS") ||
											soldTicketDetail.getInternalNotes().equalsIgnoreCase("LASTROW MINICATS") || soldTicketDetail.getInternalNotes().equalsIgnoreCase("AUTOCAT")) {
										zone = TMATDAORegistry.getQueryManagerDAO().getAutocats96CategoryTicketZoneByTnCategoryTicketGroupId(tmatEventId,soldTicketDetail.getId(),
												soldTicketDetail.getBrokerId());
									}
								} else if (soldTicketDetail.getInternalNotes().equalsIgnoreCase("LarryLast NOSTUB NOTNOW NOVIVID NOTEVO")) {
									zone = TMATDAORegistry.getQueryManagerDAO().getLarryLastCategoryTicketZoneByTnTicketGroupId(tmatEventId,soldTicketDetail.getId(),
											soldTicketDetail.getBrokerId());
								} else if (soldTicketDetail.getInternalNotes().equalsIgnoreCase("ZLR NOSTUB NOTNOW NOVIVID NOTEVO")) {
									zone = TMATDAORegistry.getQueryManagerDAO().getZoneLastRowCategoryTicketZoneByTnTicketGroupId(tmatEventId,soldTicketDetail.getId(),
											soldTicketDetail.getBrokerId());
								}
							}
							if(zone == null) {
								Map<String,String> sectionZoneMap =  eventSectionZoneMap.get(tmatEventId);
								if(sectionZoneMap != null) {
									zone = sectionZoneMap.get(orderSection);
								}
							}
							List<Ticket> tmatZoneTickets = new ArrayList<Ticket>();
							if(zone != null) {
								zoneKey = ("ZONE "+zone).toLowerCase();
								tmatZoneTickets = zoneSectionTickets.get(zoneKey);
								if(tmatZoneTickets != null && tmatZoneTickets.size()>0) {
									Collections.sort(tmatZoneTickets, OpenOrderUtil.sortingTicketbyPrice);
									for (Ticket ticket : tmatZoneTickets) {
										zoneTixCount += ticket.getRemainingQuantity();
									}
									zoneCheapestPrice = tmatZoneTickets.get(0).getPurchasePrice();
									zoneTixGroupCount = tmatZoneTickets.size();
								}else{
									zoneCheapestPrice = 0.00;
								}
								tmatAvailableZoneMap.put(orderSection, zone.toUpperCase()+":"+zoneTixCount+":"+zoneCheapestPrice+":"+zoneTixGroupCount);
							} else {
								//System.out.println("Missing Zone eventID : "+soldTicketDetail.getTmatEventId()+" : internal notes : "+soldTicketDetail.getInternalNotes()+" : section  : "+soldTicketDetail.getSection());
								zone = "";
								zoneTixCount = 0;
								zoneTixGroupCount = 0;
								zoneCheapestPrice = 0.0;
							}
						}
						try {
							OpenOrderStatus opOrderStatus = new OpenOrderStatus(soldTicketDetail);
							opOrderStatus.setTmatEventId(tmatEventId);
							//opOrderStatus.setExEventId(exEventId);
							opOrderStatus.setCreatedDate(new Date());
							opOrderStatus.setLastUpdated(new Date());
							opOrderStatus.setStatus("ACTIVE");
							opOrderStatus.setVenueId(event.getVenueId());
							
							if(sectionTixCount > 0){
								profitandLoss = Double.valueOf(df.format((opOrderStatus.getNetTotalSoldPrice()) - (marketPrice * opOrderStatus.getSoldQty())));
								sectionMargin = Double.valueOf(df.format(profitandLoss*100/opOrderStatus.getNetTotalSoldPrice()));
								opOrderStatus.setMarketPrice(marketPrice);
								opOrderStatus.setOnlinePrice(onlinePrice);
								opOrderStatus.setProfitAndLoss(profitandLoss);
								opOrderStatus.setSectionMargin(sectionMargin);
							}else{
								opOrderStatus.setMarketPrice(0.00);
								opOrderStatus.setOnlinePrice(0.00);
								opOrderStatus.setProfitAndLoss(0.00);
								opOrderStatus.setSectionMargin(0.00);
							}
							if(zoneTixCount > 0) {
								zoneTotalPrice = Double.valueOf(df.format(zoneCheapestPrice * opOrderStatus.getSoldQty()));
								zoneProfitandLoss = Double.valueOf(df.format(opOrderStatus.getNetTotalSoldPrice() - zoneTotalPrice));
								zoneMargin = Double.valueOf(df.format(zoneProfitandLoss * 100/opOrderStatus.getNetTotalSoldPrice()));
								opOrderStatus.setZoneProfitAndLoss(zoneProfitandLoss);
								opOrderStatus.setZoneTotalPrice(zoneTotalPrice);
								opOrderStatus.setZoneMargin(zoneMargin);
								opOrderStatus.setZoneCheapestPrice(zoneCheapestPrice);
							} else {
								opOrderStatus.setZoneProfitAndLoss(0.0);
								opOrderStatus.setZoneTotalPrice(0.0);
								opOrderStatus.setZoneMargin(0.0);
								opOrderStatus.setZoneCheapestPrice(0.00);
							}
							
							opOrderStatus.setSectionTixQty(sectionTixCount);
							opOrderStatus.setEventTixQty(eventTicketsQty);
							opOrderStatus.setLastUpdatedPrice(opOrderStatus.getActualSoldPrice());
							opOrderStatus.setPriceUpdateCount(1);
							opOrderStatus.setZone(zone);
							opOrderStatus.setZoneTixQty(sectionTixCount);
							opOrderStatus.setZoneTixGroupCount(zoneTixGroupCount);
							
							tobeSavedOrUpdatedOrders.add(opOrderStatus);
						} catch (Exception e) {
							//System.out.println("ERROR WHILE CALCULATING PRICES.");
							e.printStackTrace();
						}
						
						}
					}
				
					if(tobeSavedOrUpdatedOrders.size() > 0 ){
						//System.out.println("SAVED ORDERS: "+tobeSavedOrUpdatedOrders.size());
						DAORegistry.getOpenOrderStatusDao().saveAll(tobeSavedOrUpdatedOrders);
					}
				}
		
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static Comparator<Ticket> sortingTicketbyPrice = new Comparator<Ticket>() {

		public int compare(Ticket ticket1, Ticket ticket2) {
			int cmp= ticket1.getPurchasePrice().compareTo(
					ticket2.getPurchasePrice());
			if (cmp < 0) {
				return -1;
			}
			if (cmp > 0) {
				return 1;
			}
			
			return cmp;
			
	    }
		
	};
}
