package com.rtw.tmat.utils.httpclient;

import java.io.IOException;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

/**
 * This class will get latest proxies list from db every 24 hrs
 * @author cshah
 *
 */
public class ChangeProxy extends TimerTask implements InitializingBean{
//		private HttpClientStore httpClientStore;
		private static Logger logger = LoggerFactory.getLogger(ChangeProxy.class);
		//private static Collection<Proxy> ipPool= new ArrayList<Proxy>();//DAORegistry.getProxyDAO().getAllProxy();
		private static String LUMINATI_USERNAME="lum-customer-rightthisway-zone-gen-country-us";
		private static String LUMINATI_PASSWORD = "bcae35b9f8c9";
		private static final int TIMER_INTERVAL = 24 * 60 * 60 * 1000;  // 24*60  minutes
		//private static Iterator<Proxy> it=ipPool.iterator();
		public ChangeProxy() {

		}
		
		@Override
		public void run() {
			//ipPool= DAORegistry.getProxyDAO().getAllProxy();
			//it=ipPool.iterator();
		}
		public static void changeLuminatiProxy(SimpleHttpClient httpClient , boolean isBestBroxy){
			boolean flag =false;
			String result =null;
			int i = 0;
	    	while(i<5 && !flag && !isBestBroxy){
	    		i++;
	    		SimpleHttpClient client;
				try {
					client = HttpClientStore.createHttpClient();
				
		    		HttpGet request = new HttpGet(String.format(
			                "http://client.luminati.io/api/get_super_proxy?raw=1&user=%s&key=%s",
			                LUMINATI_USERNAME,LUMINATI_PASSWORD)); 
					try {
						HttpResponse response = client.execute(request); 
						result = EntityUtils.toString(response.getEntity());
						
				        Pattern pattern = Pattern.compile("[\\d++.\\d++.\\d++.\\d++]");
				        Matcher matcher = pattern.matcher(result);
				        if(result!=null && matcher.find() && !result.contains("23.227")){
				        	flag = true;
				        }
					} catch (ClientProtocolException e) {
						if(e.getMessage().equals("Unauthorized")){
							break;
						}
					} catch (IOException e) {
						System.err.println("Error while getting Proxy from Luminati : " + e.getMessage());
						System.err.println("Complete Message : " );
	//					e.printStackTrace();
						logger.info("Error while getting Proxy from Luminati : " + e.getMessage());
					}finally{
	//					if(request != null){
	//						request. = null;
	//					}
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
	    	}
	    	if(flag && !isBestBroxy){
	    		httpClient.setUserName(ChangeProxy.getLUMINATI_USERNAME()+"-dns-remote-session-"+Math.random());
	    		httpClient.setPassword(ChangeProxy.getLUMINATI_PASSWORD());
	    		
	    		httpClient.setHost(result);
	    		httpClient.setPort(22225);  // Luminati uses static port
	    		HttpHost proxy = new HttpHost(result, 22225);
	    		CredentialsProvider credsProvider = new BasicCredentialsProvider();
	    		HttpRequestRetryHandler myReTryHandler = new HttpRequestRetryHandler() {
	    			@Override
	    			public boolean retryRequest(IOException exception, int executionCount,
	    					HttpContext context) {
	    				if(exception!=null){
	    					logger.info("IO Exception while executing request : " + exception.fillInStackTrace());
	    				}
	    				return false;
	    			}
	    		};
	    		credsProvider.setCredentials(new AuthScope(proxy),new UsernamePasswordCredentials(httpClient.getUserName(), httpClient.getPassword()));
	            RequestConfig config = RequestConfig.custom().setConnectTimeout(60*1000).setConnectionRequestTimeout(60*1000).build();
	    		httpClient.setCloseableHttpClient(HttpClients.custom().setConnectionManager(new PoolingHttpClientConnectionManager()).setProxy(proxy).setRetryHandler(myReTryHandler).setDefaultCredentialsProvider(credsProvider).setDefaultRequestConfig(config).build());
	    		
	    	}else {
	    		//Proxy rem ;
	    		/*if(it.hasNext()){
	    	
					rem =  it.next();
					String[] temp=rem.getUrl().split(":");
					httpClient.setHost(temp[0]);
					httpClient.setPort(Integer.parseInt(temp[1]));
	//				ipPool.remove(rem);
				}else{
					it=ipPool.iterator();
					rem =  it.next();
					String[] temp=rem.getUrl().split(":");
					httpClient.setHost(temp[0]);
					httpClient.setPort(Integer.parseInt(temp[1]));
				}*/
	    		
	    	}
		}

		
		/*public static String getLuminatiProxy(boolean isBestBroxy){
			boolean flag =false;
			String result =null;
			int i = 0;
	    	while(i<5 && !flag && !isBestBroxy){
	    		i++;
	    		Request request = Request.Get(String.format(
	                "http://client.luminati.io/api/get_super_proxy?raw=1&user=%s&key=%s",
	                LUMINATI_USERNAME,LUMINATI_PASSWORD));
				try {
					result = request.execute().returnContent().asString();
					
			        Pattern pattern = Pattern.compile("[\\d++.\\d++.\\d++.\\d++]");
			        Matcher matcher = pattern.matcher(result);
			        if(result!=null && matcher.find() && !result.contains("23.227")){
			        	flag = true;
			        }
				} catch (ClientProtocolException e) {
					if(e.getMessage().equals("Unauthorized")){
						break;
					}
				} catch (IOException e) {
					System.err.println("Error while getting Proxy from Luminati : " + e.getMessage());
					System.err.println("Complete Message : " );
					logger.info("Error while getting Proxy from Luminati : " + e.getMessage());
				}
	    	}
	    	if(flag && !isBestBroxy){
				return result + ":22225";
	    	}else{
	    		if(it.hasNext()){
			    	
					Proxy rem =  it.next();
					String[] temp=rem.getUrl().split(":");
					HttpClientStore.setProxyIp(temp[0]);
					HttpClientStore.setPort(Integer.parseInt(temp[1]));
					return temp[0] + ":" + Integer.parseInt(temp[1]);
				}else{
					it=ipPool.iterator();
					Proxy rem =  it.next();
					String[] temp=rem.getUrl().split(":");
					return temp[0] + ":" + Integer.parseInt(temp[1]);
				}
	    	}
		}
		
		public static void changeBestProxy(){
			if(it.hasNext()){
		    	
				Proxy rem =  it.next();
				String[] temp=rem.getUrl().split(":");
				HttpClientStore.setProxyIp(temp[0]);
				HttpClientStore.setPort(Integer.parseInt(temp[1]));
//				ipPool.remove(rem);
			}else{
				it=ipPool.iterator();
				Proxy rem =  it.next();
				String[] temp=rem.getUrl().split(":");
				HttpClientStore.setProxyIp(temp[0]);
				HttpClientStore.setPort(Integer.parseInt(temp[1]));
			}
		}*/
		
		public void afterPropertiesSet() throws Exception {
			Timer timer= new Timer();
			timer.scheduleAtFixedRate(new ChangeProxy(),new Date(),TIMER_INTERVAL);
		}

		public static String getLUMINATI_USERNAME() {
			return LUMINATI_USERNAME;
		}

		public static String getLUMINATI_PASSWORD() {
			return LUMINATI_PASSWORD;
		}
	}
