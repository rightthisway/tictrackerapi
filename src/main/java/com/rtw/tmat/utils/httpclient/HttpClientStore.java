package com.rtw.tmat.utils.httpclient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

import org.apache.http.HttpHost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rtw.tmat.data.Site;
import com.rtw.tmat.web.Constants;


/**
 * HttpClient Store to control the number of httpClient object created. 
 */
public class HttpClientStore {
	
	private static Logger logger = LoggerFactory.getLogger(HttpClientStore.class);

	// hold list of httpClients
	private static Map<String, List<SimpleHttpClient>> allocatedHttpClientBySiteId = new ConcurrentHashMap<String, List<SimpleHttpClient>>();
	private static Map<String, Integer> numberOfExecutionForIP = new ConcurrentHashMap<String, Integer>();
	private static Map<String, Long> totalHttpClientLifeTimeBySiteId = new ConcurrentHashMap<String, Long>();
	private static Map<String, Long> totalHttpClientLifeTimeCountBySiteId = new ConcurrentHashMap<String, Long>();
	private static String proxyIp="";//DAORegistry.getProxyDAO().getAllProxy().get(0).getUrl().split(":")[0];
	private static int port=0;//Integer.parseInt(DAORegistry.getProxyDAO().getAllProxy().get(0).getUrl().split(":")[1]);
    private static int errorCount=0;
    private static List<String> regularSiteIds=Arrays.asList(new String[]{Site.TICKET_EVOLUTION,Site.TICKET_NETWORK_DIRECT,Site.STUBHUB_FEED/*,Site.STUB_HUB_API*/, "default"});
	static {
		for(String siteId: Constants.getInstance().getExtendedSiteIds()) {
			allocatedHttpClientBySiteId.put(siteId, new ArrayList<SimpleHttpClient>());
			totalHttpClientLifeTimeBySiteId.put(siteId, 0L);
			totalHttpClientLifeTimeCountBySiteId.put(siteId, 0L);
		}
		allocatedHttpClientBySiteId.put(SimpleHttpClient.DEFAULT, new ArrayList<SimpleHttpClient>());
		totalHttpClientLifeTimeBySiteId.put(SimpleHttpClient.DEFAULT, 0L);
		totalHttpClientLifeTimeCountBySiteId.put(SimpleHttpClient.DEFAULT, 0L);
	}
	
	public static Map<String, Integer> getFreeHttpClientCountBySiteId() {
		Map<String, Integer> freeHttpClientCountBySiteId = new HashMap<String, Integer>();
		for(String siteId: Constants.getInstance().getSiteIds()) {
			int count = 0;
			for (SimpleHttpClient httpClient: allocatedHttpClientBySiteId.get(siteId)) {
				if (!httpClient.isUsed()) {
					count++;
				}
			}
			freeHttpClientCountBySiteId.put(siteId, count);
		}

		int count = 0;
		for (SimpleHttpClient httpClient: allocatedHttpClientBySiteId.get(SimpleHttpClient.DEFAULT)) {
			if (!httpClient.isUsed()) {
				count++;
			}
		}
		freeHttpClientCountBySiteId.put(SimpleHttpClient.DEFAULT, count);
		return freeHttpClientCountBySiteId;
	}

	public static Map<String, Integer> getAllocatedHttpClientCountBySiteId() {
		Map<String, Integer> allocatedHttpClientCountBySiteId = new HashMap<String, Integer>();
		for(String siteId: Constants.getInstance().getSiteIds()) {
			allocatedHttpClientCountBySiteId.put(siteId, allocatedHttpClientBySiteId.get(siteId).size());
		}
		allocatedHttpClientCountBySiteId.put(SimpleHttpClient.DEFAULT, allocatedHttpClientBySiteId.get(SimpleHttpClient.DEFAULT).size());
		return allocatedHttpClientCountBySiteId;
	}

	public static SimpleHttpClient createHttpClient() throws Exception{
		return createHttpClient(SimpleHttpClient.DEFAULT, null, null);		
	}

	public static SimpleHttpClient createHttpClient(String siteId) throws Exception{
		return createHttpClient(siteId, null, null);
	}
	
	public static Map<String, Double> getAverageHttpClientLifeTimeBySiteId() {
		Map<String, Double> averageHttpClientLifeTimeBySiteId = new HashMap<String, Double>();
		for(String siteId: com.rtw.tmat.web.Constants.getInstance().getSiteIds()) {
			averageHttpClientLifeTimeBySiteId.put(siteId, 
					(totalHttpClientLifeTimeCountBySiteId.get(siteId) == 0L)?0.0:(totalHttpClientLifeTimeBySiteId.get(siteId) / totalHttpClientLifeTimeCountBySiteId.get(siteId)));
		}
		averageHttpClientLifeTimeBySiteId.put(SimpleHttpClient.DEFAULT, 
				(totalHttpClientLifeTimeCountBySiteId.get(SimpleHttpClient.DEFAULT) == 0L)?0.0:(totalHttpClientLifeTimeBySiteId.get(SimpleHttpClient.DEFAULT) / totalHttpClientLifeTimeCountBySiteId.get(SimpleHttpClient.DEFAULT)));
		return averageHttpClientLifeTimeBySiteId;
	}

	public static synchronized SimpleHttpClient createHttpClient(String siteId, String username, String password) throws Exception {
		List<SimpleHttpClient> newAllocatedRegularHttpClients = new ArrayList<SimpleHttpClient>();		
		
		SimpleHttpClient freeHttpClient = null;
		
		boolean isBestBroxy = null !=siteId && siteId.equals("stubhubapi")?false:false;
		
		// remove outdated httpClient
		for (SimpleHttpClient hc: allocatedHttpClientBySiteId.get(siteId)) {									
			long httpClientLifeTime = new Date().getTime() - hc.getCreated().getTime();
			if (!hc.isUsed() && (!hc.isValid() || httpClientLifeTime > 5L * 60L * 1000L)) {
				// removed httpClient from the list(do not add it to the new list) and update the stats
				totalHttpClientLifeTimeBySiteId.put(siteId, totalHttpClientLifeTimeBySiteId.get(siteId) + httpClientLifeTime);
				totalHttpClientLifeTimeCountBySiteId.put(siteId, totalHttpClientLifeTimeCountBySiteId.get(siteId) + 1);
				hc.getCloseableHttpClient().close();
			} else { 			
				newAllocatedRegularHttpClients.add(hc);
				
				if (!hc.isUsed()) {
					freeHttpClient = hc;
				}
			}
		}
				
		SimpleHttpClient httpClient = null;
		if (freeHttpClient != null) {
			/*String proxyStr = freeHttpClient.getHost();
			if(proxyStr != null){
				Integer i = numberOfExecutionForIP.get(proxyStr);
				if(i==null){
					i=0;
				}
				if(i>10){
					removeHttpClient(freeHttpClient, siteId);
					ChangeProxy.changeLuminatiProxy(freeHttpClient,isBestBroxy);
					String luminatiProxy = ChangeProxy.getLuminatiProxy(isBestBroxy);
					String ip=luminatiProxy.split(":")[0];
					int portNo=Integer.parseInt(luminatiProxy.split(":")[1]);
					HttpHost proxy = new HttpHost(ip, portNo);
					freeHttpClient = new SimpleHttpClient(siteId,proxy,isBestBroxy);
					
					if(siteId.equals("stubhub") && !isBestBroxy){
						freeHttpClient.setStubhubCookie(StubhubCookieFetcher.getCookies(freeHttpClient,null));
					}
					
					freeHttpClient.setLuminati(true);
					newAllocatedRegularHttpClients.add(freeHttpClient);
					numberOfExecutionForIP.put(proxyStr,0);
				}else{
					i++;
					numberOfExecutionForIP.put(proxyStr, i);
					
				}
			}*/
			httpClient = freeHttpClient;
		} else {	
			if (siteId.equals(Site.EI_MARKETPLACE)) {
				throw new Exception("Wrong path.");
				/*ClientConnectionManager cm = getClientConnectionManager();
				httpClient = new SimpleHttpClient(siteId, cm, new BasicHttpParams());
				newAllocatedRegularHttpClients.add(httpClient);
				if (!EIMPUtil.login(httpClient, username, password)) {
					throw new Exception("Cannot login to EIMP");
				}*/				
			} /*else if(!regularSiteIds.contains(siteId)){
				
				String luminatiProxy = ChangeProxy.getLuminatiProxy(isBestBroxy);
				if(luminatiProxy!=null){
					String ip=luminatiProxy.split(":")[0];
					int portNo=Integer.parseInt(luminatiProxy.split(":")[1]);
					HttpHost proxy = new HttpHost(ip, portNo);
					httpClient = new SimpleHttpClient(siteId,proxy,isBestBroxy);
					httpClient.setLuminati(true);
					if(siteId.equals("stubhub") && !isBestBroxy){
						httpClient.setStubhubCookie(StubhubCookieFetcher.getCookies(httpClient,null));
					}
				}else{
					httpClient = new SimpleHttpClient(siteId);
					httpClient.setLuminati(false);
					if(siteId.equals("stubhub") && !isBestBroxy){
						httpClient.setStubhubCookie(StubhubCookieFetcher.getCookies(httpClient,null));
					}
				}
				String host = httpClient.getHost();
				if(host==null){
					host="";
				}
				Integer i = numberOfExecutionForIP.get(host);
				if(i==null){
					i=0;
				}
				if(i>10){
					ChangeProxy.changeLuminatiProxy(httpClient, isBestBroxy);
					if(siteId.equals("stubhub") && !isBestBroxy){
						httpClient.setStubhubCookie(StubhubCookieFetcher.getCookies(httpClient,null));
					}
					numberOfExecutionForIP.put(httpClient.getHost(),0);
				}else{
					i++;
					numberOfExecutionForIP.put(host, i);
				}
				
				newAllocatedRegularHttpClients.add(httpClient);
			}*/else {
				httpClient = new SimpleHttpClient(siteId);
				newAllocatedRegularHttpClients.add(httpClient);
				
//				httpClient.reset();
				// Added on test bases..
//				allocatedHttpClientBySiteId.put(siteId, newAllocatedRegularHttpClients);
			}
			allocatedHttpClientBySiteId.put(siteId, newAllocatedRegularHttpClients);	
			
		}
		
		
//		httpClient.setRedirectHandler(null);
		httpClient.setUsed(true);
		
		return httpClient;
	}
	public static void removeHttpClient(SimpleHttpClient httpClient,String siteId){
		List<SimpleHttpClient> allocatedHttpClients = new ArrayList<SimpleHttpClient>();
		for (SimpleHttpClient hc: allocatedHttpClientBySiteId.get(siteId)) {
			if(!hc.equals(httpClient)){
				allocatedHttpClients.add(hc);
			}else{
				System.out.println(hc.getHost() +" removed from client list");
				try {
					hc.getCloseableHttpClient().close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		allocatedHttpClientBySiteId.put(siteId,allocatedHttpClients);
	}
	/**
	 * Release HttpClient.
	 * Put the httpClient back in the store.
	 * @param httpClient
	 */
	public static void releaseHttpClient(SimpleHttpClient httpClient) {
		if (httpClient == null) {
			return;
		}
		/*try {
			httpClient.getCloseableHttpClient().close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println(e.fillInStackTrace());
		}*/
		httpClient.setUsed(false);
		
	}
	
	private static ClientConnectionManager getClientConnectionManager(){
		SchemeRegistry schemeRegistry = new SchemeRegistry();
		HttpParams hp = new BasicHttpParams();
		SSLContext sslContext = null;
		
		try {
//			TrustManager[] tm = new TrustManager[]{ new NaiveTrustManager()};
			sslContext = SSLContext.getInstance("TLS");
			sslContext.init(null, new TrustManager[]{new NaiveTrustManager()}, null);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		SSLSocketFactory sslsf = new SSLSocketFactory(sslContext);
		
		schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
		schemeRegistry.register(new Scheme("https", sslsf, 443));

		ClientConnectionManager cm = new ThreadSafeClientConnManager(hp,schemeRegistry);
		return cm;
		
	}
	public String getProxyIp() {
		return proxyIp;
	}

	public static void setProxyIp(String proxyIp) {
		HttpClientStore.proxyIp = proxyIp;
	}
	

	public int getPort() {
		return port;
	}

	public static void setPort(int port) {
		HttpClientStore.port = port;
	}

	public int getErrorCount() {
		return errorCount;
	}

	public static void setErrorCount(int errorCount) {
		HttpClientStore.errorCount = errorCount;
	}
    public static void increamentErrorCount(){
    	HttpClientStore.errorCount++;
    }
    
    
}

	