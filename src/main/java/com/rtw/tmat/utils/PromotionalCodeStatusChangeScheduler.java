package com.rtw.tmat.utils;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.InitializingBean;

import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.AffiliatePromoCodeHistory;
import com.rtw.tracker.datas.ReferralContest;
import com.rtw.tracker.utils.Util;

public class PromotionalCodeStatusChangeScheduler extends TimerTask implements InitializingBean{

	@Override
	public void afterPropertiesSet() throws Exception {
		TimerTask timerTask = new PromotionalCodeStatusChangeScheduler();
		Calendar today = Calendar.getInstance();
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(timerTask, today.getTime(), 60*60*1000);
	}

	@Override
	public void run() {
		try{
			if(Util.isContestRunning){
				System.out.println("RETURN");
				return;
			}
			System.out.println("RUNNNNN");
			Collection<AffiliatePromoCodeHistory> affiliatePromoHistoryList = DAORegistry.getAffiliatePromoCodeHistoryDAO().getAllActivePromoCodes();
			Date currentDate = new Date();
			if(affiliatePromoHistoryList != null){
				//System.out.println("PROMOCODESTATUSCHANGESCHEDULER / AFFILIATEPROMOTIONALCODE:"+affiliatePromoHistoryList.size());
				for(AffiliatePromoCodeHistory affiliatePromoHistory : affiliatePromoHistoryList){
					if(affiliatePromoHistory.getEffectiveToDate() != null){
						if(affiliatePromoHistory.getEffectiveToDate().compareTo(currentDate)<0){
							affiliatePromoHistory.setStatus("DELETED");
							DAORegistry.getAffiliatePromoCodeHistoryDAO().update(affiliatePromoHistory);
						}
					}
				}
			}
			//COMMENTED AS LOTTER SECTION NOT IN USE.
			/*List<ReferralContest> contests = DAORegistry.getReferralContestDAO().getAllActiveContests();
			if(contests!=null){
				for(ReferralContest contest : contests){
					if(contest.getEndDate()!=null){
						if(contest.getEndDate().compareTo(currentDate) < 0){
							contest.setContestStatus("EXPIRED");
							DAORegistry.getReferralContestDAO().update(contest);
						}
					}
				}
			}*/
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
