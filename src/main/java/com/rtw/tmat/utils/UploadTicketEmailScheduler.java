package com.rtw.tmat.utils;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.InitializingBean;

import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.Customer;
import com.rtw.tracker.datas.CustomerOrder;
import com.rtw.tracker.datas.CustomerOrderDetails;
import com.rtw.tracker.datas.EventDetails;
import com.rtw.tracker.datas.Invoice;
import com.rtw.tracker.datas.InvoiceTicketAttachment;
import com.rtw.tracker.enums.InvoiceStatus;
import com.rtw.tracker.mail.MailManager;
import com.rtw.tracker.utils.Util;

public class UploadTicketEmailScheduler extends TimerTask implements
		InitializingBean {

	static MailManager mailManager=null;

	public MailManager getMailManager() {
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		UploadTicketEmailScheduler.mailManager = mailManager;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		TimerTask timerTask = new UploadTicketEmailScheduler();
		Calendar cal = Calendar.getInstance();
		// cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE)+1);
		Timer timer = new Timer(true);
		timer.scheduleAtFixedRate(timerTask, cal.getTime(), 15 * 60 * 1000);
	}

	@Override
	public void run() {
		try {
			if(Util.isContestRunning){
				System.out.println("RETURN");
				return;
			}
			System.out.println("RUNNNNN");
			List<Invoice> invoices = DAORegistry.getInvoiceDAO()
					.getInvoiceByTicketUpload();
			if (invoices != null && !invoices.isEmpty()) {
				for (Invoice invoice : invoices) {
					com.rtw.tracker.mail.MailAttachment[] ticketAttachments = null;
					if (invoice.getUploadToExchnge() != null && invoice.getUploadToExchnge()) {
						List<InvoiceTicketAttachment> attachments = DAORegistry.getInvoiceTicketAttachmentDAO().getTicketAttachmentByInvoiceId(invoice.getId());
						ticketAttachments = new com.rtw.tracker.mail.MailAttachment[attachments.size()];
						int i = 0;
						for (InvoiceTicketAttachment attachment : attachments) {
							ticketAttachments[i] = new com.rtw.tracker.mail.MailAttachment(null, "Application/pdf",com.rtw.tmat.utils.Util.getFileNameFromPath(attachment.getFilePath()),
									attachment.getFilePath());
							i++;
						}
					}
					CustomerOrder order = DAORegistry.getCustomerOrderDAO().get(invoice.getCustomerOrderId());
					EventDetails event = DAORegistry.getEventDetailsDAO().getEventById(order.getEventId());
					String email = "";
				 	CustomerOrderDetails orderDetails = DAORegistry.getCustomerOrderDetailsDAO().getCustomerOrderDetailsByOrderId(invoice.getCustomerOrderId());
				 	Customer customer = DAORegistry.getCustomerDAO().get(invoice.getCustomerId());
					if(orderDetails.getShEmail()!=null && !orderDetails.getShEmail().isEmpty()){
						 email = orderDetails.getShEmail();
					}else if(orderDetails.getBlEmail()!=null && !orderDetails.getBlEmail().isEmpty()){
						 email = orderDetails.getBlEmail();
					}else{
						 email =customer.getEmail();
					}
					//email = "msanghani@rightthisway.com";
					/*Map<String, Object> dataMap = new HashMap<String, Object>();
					dataMap.put("eventDateTimeVenue",event.getNameWithDateandVenue());
					dataMap.put("orderNo",order.getId());
					com.rtw.tracker.mail.MailAttachment[] mailAttachment = new com.rtw.tracker.mail.MailAttachment[1];
					mailAttachment[0] = new com.rtw.tracker.mail.MailAttachment(null, "image/png", "logo.png",DAORegistry.getPropertyDAO().get("rtf.logo.image.path").getValue());
					mailManager.sendMailNow("text/html","sales@rewardthefan.com", email, null, "amit.raut@rightthisway.com,msanghani@rightthisway.com",
							"Reward The Fan: Tickets are available for download for Your Order Number :"
									+ order.getId(), "email-order-download-tickets.html", dataMap,"text/html", ticketAttachments, mailAttachment,null);
					invoice.setIsRealTixUploaded("Yes");
					invoice.setStatus(InvoiceStatus.Completed);
					DAORegistry.getInvoiceDAO().saveOrUpdate(invoice);*/
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
