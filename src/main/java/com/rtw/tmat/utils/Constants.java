package com.rtw.tmat.utils;

public class Constants {
	
	//Local Setup
	//public static final String BASE_URL = "http://34.202.149.190/sandbox/";
	
	//Sandbox Setup
	//public static final String BASE_URL = "http://10.0.1.236/sandbox/";
	
	//Production Setup/
	public static final String BASE_URL = "http://10.0.1.111/";
	
	public static final String RTFECOM_TO_ZONES_DB_LINK_SERVER = "ZonesApiPlatformV2.dbo.";
	//public static final String RTFECOM_TO_ZONES_DB_LINK_SERVER = "sandboxZonesApiPlatformV2.dbo.";
	
	//Production Local setup
	//public static final String BASE_URL = "http://api.rewardthefan.com/";
	
	public static final String X_SIGN_HEADER = "b80ABrBWAGpwtsAC/3sLa0qwFdQBS9SIJOkTf8qNt/Q=";
	public static final Integer BROKERID = 1001;
	public static final String X_TOKEN_HEADER = "DESKTOPRTFRTW";
	public static final String X_PLATFORM_HEADER = "DESKTOP_SITE";
	public static final String CONFIGID = "DESKTOPRTFRTW";
	public static final String PRODUCTTYPE = "REWARDTHEFAN";
	public static final String USERID = "REWARDTHEFAN";
	public static final String PLATFORM = "TICK_TRACKER";
	public static final Integer CONTEST_QUESTION_SIZE = 10;
	public static final String GET_EVENT_DETAILS = "GetEventDetails.json"; 
	public static final String CARD_JSP_API = "GetHomeCards";
	public static final String GRAND_CHILD_CATEGORY_API = "GetGrandChildCategory.json";
	public static final String CHILD_CATEGORY_API = "GetChildCategory.json";
	public static final String CUSTOMER_LOGIN = "CustomerLogin.json";
	public static final String CUSTOMER_REGISTRATION = "CustomerRegistration.json";
	public static final String ORDER_PAYMENT_BY_REWARD = "OrderPaymentByReward.json";
	public static final String GET_COUNTRY_AND_STATE = "GetStateAndCountry.json";
	public static final String AUTO_CATS_LOCK_TICKETGROUP = "AutoCatsLockTicketGroup.json";
	public static final String VALIDATE_PROMOCODE = "ValidatePromoCodeTicTracker.json";
	public static final String VALIDATE_PROMOCODE_REAL_TICKET = "ValidatePromoCodeTicTrackerLongOrder.json";
	public static final String ADD_CUSTOMER_ADDRESS = "AddCustomerAddress.json";
	public static final String CREATE_ORDER = "TickTrackerCreateOrder.json";
	public static final String CREATE_ORDER_INITIATE = "InitiateTicTrackerOrder.json";
	public static final String CREATE_ORDER_CONFIRM = "ConfirmTicTrackerOrder.json";
	public static final String CREATE_ORDER_REAL_TICKET = "ConfirmTicTrackerLongOrder.json";
	public static final String INITIATE_ORDER_REAL_TICKET = "InitiateTicTrackerLongOrderNew.json";
	public static final String FAVOURITE_EVENTS = "FavouriteEvents.json";
	public static final String ADD_FAVOURITE_SUPER_ARTIST = "AddFavoriteOrSuperArtist.json";
	public static final String GET_REFFERERCODE = "GetReffererCode.json";
	public static final String GET_SPORTS_CROWN_JEWEL_TICKETS = "GetSportsCrownJewelTickets.json";
	public static final String GET_CUSTOMER_PROFILE_PICTURE = "GetCustomerProfilePic.json";
	public static final String GET_CUSTOMER_ORDERS = "GetCustomerOrders.json";
	public static final String VIEW_ORDERS = "ViewOrder.json";
	public static final String EVENT_SEARCH_BY_EXPLORE = "EventSearchByExplore.json";
	public static final String GET_ARTIST_BT_GRAND_CHILD = "GetArtistByGrandChildCateoryId.json";
	public static final String GENERALIZED_SEARCH = "GeneralizedSearch.json";
	public static final String GET_CITIES = "GetCities.json";
	public static final String SEARCH_ARTIST = "SearchArtist.json";
	public static final String FAVOURITE_ARTIST_EVENTS_BY_CUSTOMER = "FavoriteArtistEventInfoByCustomer.json";
	public static final String GET_CUSTOMER_REWARDS = "GetCustomerLoyaltyRewards.json";
	public static final String GET_ARTIST_INFO = "GetArtistInfo.json";
	public static final String GET_CUSTOMER_INFO = "GetCustomerInfo.json";
	public static final String UPDATE_CUSTOMER_PHONE_NUMBER = "UpdateCustomerPhoneNumber.json";
	public static final String GET_POPULAR_ARTIST_SUPERFAN = "GetPopularArtistForSuperFan.json";
	public static final String GET_LOYALFAN_DETAILS = "GetLoyalFanDetails.json";
	public static final String ADD_ARTIST_DETAILS = "AddFavoriteOrSuperArtist.json";
	public static final String EMAIL_CUSTOMER_DETAILS = "EmailCustomerRequestInfo.json";
	public static final String GET_CUSTOMER_CARDS = "GetCustomerCardsList.json";
	public static final String REMOVE_CUSTOMER_CARDS = "RemoveSavedCustomerCard.json";
	public static final String GET_CUSTOMER_REWARDS_BREAKUP = "GetCustomerRewardsBreakup.json";
	public static final String GET_PAYPAL_API_CREDENTIALS = "GetPayPalCredentials.json";
	public static final String REMOVE_CUSTOMER_ADDRESS = "RemoveCustomerAddress.json";
	public static final String GET_POPULAR_ARTIST_FAVORITE = "GetPopularArtistForFavorite.json";
	public static final String UNLOCK_TICKET_GROUP = "UnLockTicketGroup.json";
	public static final String CUSTOMER_FEDDBACK_SUPPORT = "CustomerFeedBackAndSupport.json";
	public static final String GET_STRIPE_CREDENTIALS = "GetStripeCredentials.json";
	public static final String GET_INVOICE_REFUNDS_LIST = "GetInvoiceRefundList.json";
	public static final String GET_INVOICE_DISPUTE_LIST = "GetStripeDisputes.json";
	public static final String INVOICE_REFUND = "InvoiceRefund.json";
	public static final String PAYPAL_REFUND_TRANSACTION_LIST = "PaypalRefundTranList.json";
	public static final String STRIPE_TRANSACTION = "StripeTransaction.json";
	public static final String STRIPE_TRANSACTION_REAL_TICKET = "StripeTransactionLongOrder.json";
	public static final String CUSTOMER_WALLET_TRANSACTION = "CustomerWalletTransaction.json";
	public static final String AUTOCOMPLETE_ARTIST = "SearchArtistOrTeamByKey.json";
	public static final String AUTOCOMPLETE_LOYALFAN = "LoyalFanStateAutoSearch.json";
	public static final String GET_LOYALFAN_STATUS = "LoyalFanPageStatus.json";
	public static final String UPDATE_LOYALFAN = "UpdateLoyalFan.json";
	public static final String VALIDATE_LOYALFAN = "TickTrackerLoyalFanValidator.json";
	public static final String GET_FANTASY_EVENTS = "ViewFantasyEvent.json";
	public static final String BLOCK_CUSTOMER_COMMENT = "BlockCustomerComment.json";
	//Crown Jewel API'S
	public static final String GET_CROWN_JEWEL_LEAGUE = "GetSuperFanLeagues.json";
	public static final String GET_CROWN_JEWEL_TEAMS = "GetSuperFanTeams.json";
	public static final String GET_CROWN_JEWEL_EVENTS = "CrownJewelEvents.json";
	public static final String GET_CROWN_JEWEL_TICKETS = "GetOthersCrownJewelTickets.json"; //Except Sports
	public static final String GET_CROWN_JEWEL_ORDER_SUMMARY = "CrownJewelOrderSummary.json";
	public static final String GET_CROWN_JEWEL_FUTURE_ORDER_CONFIRMATION = "CrownJewelFutureOrderConfirmation.json";
	public static final String VIEW_CROWN_JEWEL_ORDERS = "ViewCrownJewelOrder.json";
	public static final String GET_CROWN_JEWEL_ORDERS = "GetCrownJewelOrders.json";
	public static final String GET_RESET_PASSWORD_REQUEST = "ResetPasswordRequest.json";
	public static final String GET_RESET_PASSWORD = "ResetPassword.json";
	public static final String GET_REFERRER_CODE = "GetReffererCode.json";
	public static final String VALIDATE_CUSTOMER_LOYAL_FAN = "ValidateCustomerLoyalFan.json";
	public static final String SEND_CUSTOMER_REFERRAL_CODE = "SendReferralCodeToOthers.json";
	public static final String VALIDATE_REFERRALCODE = "ValidateReferralCode.json";
	public static final String CANCEL_ORDER = "CancelOrder.json";
	public static final String TICKET_DOWNLOAD_NOTIFICATION = "TicketDownloadNotification.json";
	public static final String CREATE_FST_ORDER = "CreateFSTOrder.json";
	public static final String VALIDATE_FST_ORDER = "ValidateFSTPurchase.json";
	public static final String GET_FANTASY_PRODUCT_CATEGORY = "GetFantasyProductCategories.json";
	public static final String CONTEST_UPDATE_WINNER_REWARD = "UpdateContestWinnerRewards.json";
	public static final String RESET_CONTEST = "RemoveContestCacheData.json";
	public static final String RESET_CONTEST_CASS = "RemoveCassContCacheData.json";
	public static final String START_QUIZ_CONTEST = "UpdateStartQuizContest.json";
	public static final String GET_QUIZ_QUESTION_INFO = "GetQuizQuestionInfo.json";
	public static final String CREDIT_CUSTOMER_REWARDS = "UploadMediaTFFRewards.json";
	public static final String END_QUIZ_CONTEST = "UpdateEndQuizContest.json";
	public static final String REFRESH_CONTEST = "RefreshNextContestList.json";
	public static final String REFRESH_CONTEST_CACHE = "RefreshContestCacheData.json";
	public static final String UPDATE_CONTEST_WINNER_REWARDS = "UpdateContestWinnerRewards.json";
	public static final String UPDATE_CONTEST_WINNER_REWARDS_CASS = "CompContestWinnerRewards.json";
	public static final String FORCE_MIGRATION_TO_SQL_FROM_CASS = "ForceMigrationToSqlFromCass.json";
	public static final String TICTRACKER_API_UPDATE_USERCOUNT = "GetQuizContestCustomersCount.json";
	
	
	
	public static final String RTF_ORDER_PROD_REFUND = "OrderProdRefund.json";
	
	
	//generalized error message
	public static final String GENERALIZED_ERROR_MSG = "whoops! Something Went Wrong. Please try again later.";
	
	//Fedex property
	public static final String FEDEX_LABEL_DIRECTORY = "fedex.label.filepath";
	public static final String FEDEX_LABEL_MANUAL_DIRECTORY = "fedex.label.manual.filepath";
	public static final String FEDEX_ACCOUNT_NO = "fedex.accountnumber";
	public static final String FEDEX_METER_NO = "fedex.meternumber";
	public static final String FEDEX_KEY = "fedex.key";
	public static final String FEDEX_PASSWORD = "fedex.password";
	public static final String FEDEX_SHIPPING_URL = "fedex.shiping.url";
	public static final String FEDEX_TRACKING_URL = "fedex.tracking.url";
		
}
