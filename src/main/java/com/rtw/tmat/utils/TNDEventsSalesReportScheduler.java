package com.rtw.tmat.utils;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.InitializingBean;
import com.rtw.tmat.dao.implementation.TMATDAORegistry;
import com.rtw.tmat.pojo.TNDEventSalesReportDetail;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.mail.MailAttachment;
import com.rtw.tracker.mail.MailManager;
import com.rtw.tracker.utils.Util;



public class TNDEventsSalesReportScheduler extends TimerTask implements InitializingBean{

	@Override
	public void afterPropertiesSet() throws Exception {
		TimerTask timerTask = new TNDEventsSalesReportScheduler();
		Calendar cal = Calendar.getInstance();
		
		if(cal.get(Calendar.HOUR_OF_DAY) >= 8) {
			cal.add(Calendar.DATE, 1);
		}
		cal.set(Calendar.HOUR_OF_DAY, 8);
		cal.set(Calendar.MINUTE, 01);
		
		Timer timer = new Timer(true);
		timer.scheduleAtFixedRate(timerTask, cal.getTime(), 24*60*60*1000);
	}
	public static MailManager mailManager;
	
	
	
	public static MailManager getMailManager() {
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		TNDEventsSalesReportScheduler.mailManager = mailManager;
	}

	//@Override
	//protected void executeInternal(JobExecutionContext context)
	//		throws JobExecutionException {
	@Override
	public void run() {
		if(Util.isContestRunning){
			System.out.println("RETURN");
			return;
		}
		System.out.println("RUNNNNN");
		System.out.println("TND Events Sales report Scheduler Job Started....."+new Date());
		try{
			getTNDEventsSalesReportEmail();
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("TND Events Sales report Scheduler Job Finished....."+new Date());
	}
	
	public void getTNDEventsSalesReportEmail(){
		
		try{
			//System.out.println("inside if next 15 days unsent order email..."+new Date());
		    ByteArrayOutputStream bos = new ByteArrayOutputStream();
		    SXSSFWorkbook workbook = getTNDEventSalesReportExcelFile();
		    try {
		        workbook.write(bos);
		    } finally {
		        bos.close();
		    }
		    byte[] bytes = bos.toByteArray();
		    MailAttachment[] mailAttachments = new MailAttachment[1];
		    mailAttachments[0] = new MailAttachment(bytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "EventLast3DaysSales.xlsx");
				
		    String resource = "mail-tnd-events-sales-details.html";
			String subject = "Events Last 3 Days Sales Details";
			String mimeType = "text/html";
			//String bccAddress = "AODev@rightthisway.com";
			//String bccAddress = "AODev@rightthisway.com";
			//String toAddress ="tselvan@rightthisway.com,amit.raut@rightthisway.com";
			String fromName = "tmatmanagers@rightthisway.com";
			String toAddress ="amit.raut@rightthisway.com";
			 String ccAddress = "tselvan@rightthisway.com";
			String bccAddress = null;//"AODev@rightthisway.com";
			System.out.println("Events Last 3 Days Sales Details email..."+new Date());
			Map<String, Object> map = new HashMap<String, Object>();
			
			mailManager.sendMail(fromName, toAddress, ccAddress, bccAddress, subject, resource, map, mimeType, mailAttachments);
			
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Events Last 3 Days Sales Details Job Failed : "+new Date());
			try {
				 Map<String, Object> map = new HashMap<String, Object>();
				 map.put("error", ""+e.fillInStackTrace());

				 String subject = "Events Last 3 Days Sales Details Job Failed : ";
				 String template = "mail-tnd-event-sales-report-error-message.txt";
				 String fromName  = "tmatmanagers@rightthisway.com";
				 String mimeType = "text/html";
				 String toAddress ="amit.raut@rightthisway.com";
				 String ccAddress = "tselvan@rightthisway.com";
					
				 mailManager.sendMail(fromName, toAddress, ccAddress, null, subject, template, map, mimeType, null);
			} catch(Exception ex) {
				ex.printStackTrace();
				 System.out.println("Events Last 3 Days Sales Details Job email alert Failed : "+new Date());
			}

		}
		
	}
	
	public static SXSSFWorkbook getTNDEventSalesReportExcelFile() throws Exception {
		
		try {
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			CreationHelper createHelper = workbook.getCreationHelper();
			//List<TNDEventSalesReportDetail> tndEventSalesReportList = TMATDAORegistry.getQueryManagerDAO().getTNDEventSalesDetailReport();
			List<Object[]> dataList = DAORegistry.getQueryManagerDAO().getTNDEventSalesDetailReport();
			SXSSFSheet ssSheet = (SXSSFSheet) workbook.createSheet();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0;
			 Row rowhead =   ssSheet.createRow((int)0);
			 rowhead.createCell((int) j).setCellValue("EventId");
			 j++;
			 rowhead.createCell((int) j).setCellValue("Event Name");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Event Date");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Event Time");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Venue");
		    j++;
		    rowhead.createCell((int) j).setCellValue("City");
		    j++;
		    rowhead.createCell((int) j).setCellValue("State");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Country");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Sales in Last 3 Days");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Recent Sale Date");
		    j++;
		    
		    int i =1;
		    /*for (TNDEventSalesReportDetail tndEventSalesDetail : tndEventSalesReportList) {
			
		    	int column = 0;
		    	Row row = ssSheet.createRow(i);
				row.createCell((int) column).setCellValue(tndEventSalesDetail.getEventId());
		        column++;
		        row.createCell((int) column).setCellValue(tndEventSalesDetail.getEventName());
		        column++;
		        row.createCell((int) column).setCellValue(tndEventSalesDetail.getEventDateStr());
		        column++;
		        row.createCell((int) column).setCellValue(tndEventSalesDetail.getEventTimeStr());
		        column++;
		        row.createCell((int) column).setCellValue(tndEventSalesDetail.getVenue());
		        column++;
		        row.createCell((int) column).setCellValue(tndEventSalesDetail.getCity());
		        column++;
		        row.createCell((int) column).setCellValue(tndEventSalesDetail.getState());
		        column++;
		        row.createCell((int) column).setCellValue(tndEventSalesDetail.getCountry());
		        column++;
		        row.createCell((int) column).setCellValue(tndEventSalesDetail.getSalesCount());
		        column++;
		        row.createCell((int) column).setCellValue(tndEventSalesDetail.getRecentSaleDateStr());
		        column++;
		        
		        i++;
			}*/
		    for (Object[] dataObj : dataList) {
				
		    	int column = 0;
		    	Row row = ssSheet.createRow(i);
		    	
		    	if(dataObj[0] != null){
		    		row.createCell((int) column).setCellValue(dataObj[0].toString());
				}else{
					row.createCell((int) column).setCellValue("");
				}				
				column++;
				
				if(dataObj[1] != null){
					row.createCell((int) column).setCellValue(dataObj[1].toString());
				}else{
					row.createCell((int) column).setCellValue("");
				}				
				column++;
				
				if(dataObj[2] != null){
					Cell cell = row.createCell((int) column);
					cell.setCellValue(new Date(dataObj[2].toString()));
					cell.setCellStyle(cellStyle);
				}else{
					row.createCell((int) column).setCellValue("");
				}				
				column++;
				
				if(dataObj[3] != null){
					row.createCell((int) column).setCellValue(dataObj[3].toString());
				}else{
					row.createCell((int) column).setCellValue("");
				}				
				column++;
				
				if(dataObj[4] != null){
					row.createCell((int) column).setCellValue(dataObj[4].toString());
				}else{
					row.createCell((int) column).setCellValue("");
				}				
				column++;
				
				if(dataObj[5] != null){
					row.createCell((int) column).setCellValue(dataObj[5].toString());
				}else{
					row.createCell((int) column).setCellValue("");
				}				
				column++;
				
				if(dataObj[6] != null){
					row.createCell((int) column).setCellValue(dataObj[6].toString());
				}else{
					row.createCell((int) column).setCellValue("");
				}				
				column++;
				
				if(dataObj[7] != null){
					row.createCell((int) column).setCellValue(dataObj[7].toString());
				}else{
					row.createCell((int) column).setCellValue("");
				}				
				column++;
				
				if(dataObj[8] != null){
					row.createCell((int) column).setCellValue(dataObj[8].toString());
				}else{
					row.createCell((int) column).setCellValue("");
				}				
				column++;
				
				if(dataObj[9] != null){
					Cell cell = row.createCell((int) column);
					cell.setCellValue(new Date(dataObj[9].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					row.createCell((int) column).setCellValue("");
				}				
				column++;
				
		        i++;
			}
		    return workbook;		    
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
