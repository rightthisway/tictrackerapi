package com.rtw.tmat.utils.common;

public class SharedProperty {
	
	private String adminUrl;
	private String browseUrl;
	private String ticTrackerUrl;
	private String apiUrl;
	private String databasAlias;
	private String baseDirectory;
	private String firebaseDatabaseUrl;
	
	public String getAdminUrl() {
		return adminUrl;
	}
	public void setAdminUrl(String adminUrl) {
		this.adminUrl = adminUrl;
	}
	public String getBrowseUrl() {
		return browseUrl;
	}
	public void setBrowseUrl(String browseUrl) {
		this.browseUrl = browseUrl;
	}
	public String getTicTrackerUrl() {
		return ticTrackerUrl;
	}
	public void setTicTrackerUrl(String ticTrackerUrl) {
		this.ticTrackerUrl = ticTrackerUrl;
	}
	public String getApiUrl() {
		return apiUrl;
	}
	public void setApiUrl(String apiUrl) {
		this.apiUrl = apiUrl;
	}
	public String getDatabasAlias() {
		return databasAlias;
	}
	public void setDatabasAlias(String databasAlias) {
		this.databasAlias = databasAlias;
	}
	
	public String getFirebaseDatabaseUrl() {
		return firebaseDatabaseUrl;
	}
	public void setFirebaseDatabaseUrl(String firebaseDatabaseUrl) {
		this.firebaseDatabaseUrl = firebaseDatabaseUrl;
	}
	public String getBaseDirectory() {
		return baseDirectory;
	}
	public void setBaseDirectory(String baseDirectory) {
		this.baseDirectory = baseDirectory;
	}
	
}
