package com.rtw.tmat.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.InitializingBean;

import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.mail.MailManager;
import com.rtw.tracker.pojos.RTFAPITracker;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class RTFApiTracker extends TimerTask implements InitializingBean {

	public static final String ACCOUNT_SID = "ACc0d5b89953472346ae11d9b6e0e3289e";
	public static final String AUTH_TOKEN = "80b54238355b198fa9abd7be0560c456";
	public static final String TWILIO_NUMBER = "+18665751799";
	
	public static MailManager mailManager;
	public static MailManager getMailManager() {
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		RTFApiTracker.mailManager = mailManager;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		TimerTask timerTask = new RTFApiTracker();
		Calendar cal = Calendar.getInstance();
		Timer timer = new Timer(true);
		timer.scheduleAtFixedRate(timerTask, cal.getTime(), 5*60*1000);
		
	}

	@Override
	public void run() {
		try {
			List<RTFAPITracker> trackers = DAORegistry.getQueryManagerDAO().getRtfApiLastUpdatedDate();
			if (trackers != null && !trackers.isEmpty()) {
				Date today = new Date();
				long diff = today.getTime() - trackers.get(0).getLastUpdated().getTime();
				long diffMinutes = diff / (60 * 1000) % 60;
				if (diffMinutes > 10) {
				List<String> phoneNumbers = new ArrayList<String>();
					phoneNumbers.add("+17327255647");  //Amit
					phoneNumbers.add("+918200534918"); //Mitul
				    phoneNumbers.add("+919884512528"); //Ulaga
				    phoneNumbers.add("+919900023994"); //Shiva
				    phoneNumbers.add("+919632133926"); //Lloyd
				    phoneNumbers.add("+919952666784"); //Tamil
				  //  phoneNumbers.add("+917837781520"); //Mamta
				    phoneNumbers.add("+919814318822"); //Jaspreet
				    
					Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
					String msg = "Dear Team, RewardTheFan API seems down for more than 10 mins. Please look into this immediately";

					for (String phone : phoneNumbers) {
						Message message = Message.creator(new PhoneNumber(phone), new PhoneNumber(TWILIO_NUMBER), msg)
								.create();
						System.out.println("Sent From : " + TWILIO_NUMBER + ", Send To : " + phone + ", SMS Status : "
								+ message.getSid());
					}
					
					String emailTo = "msanghani@rightthisway.com,amit.raut@rightthisway.com,kulaganathan@rightthisway.com,lbernad@rightthisway.com,"
							+ "skumar@rightthisway.com,tselvan@rightthisway.com,"
							+ "ulaganathantoall@gmail.com,tamilselvan.nst@gmail.com,kaundal.mamta@gmail.com,mamta@42works.net,"
							+ "jaspreet@42works.net,rsamit@gmail.com,mitulsanghani.mca@gmail.com";
					
					mailManager.sendMailUsingGMAILSMTP("smtp.gmail.com",465,true,"rightthiswayllc@gmail.com","ticket#1441","infortw10018@gmail.com",null,emailTo,null,null, 
							"REWARDTHEFAN API SEEMS DOWN FROM MORE THAN 10 MINUTES.",
				    		"Email-RTF-API_Tracker.html",new HashMap<String, Object>(), "text/html", null,null,null);
					
					/*mailManager.sendMailUsingGMAILSMTP("text/html","infortw10018@gmail.com","aodev@rightthisway.com", 
							null,"", "RewardTheFan API seems down for morethan 10 mins.",
				    		"Email-RTF-API_Tracker.html",new HashMap<String, Object>(), "text/html", null,null,null);*/
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
