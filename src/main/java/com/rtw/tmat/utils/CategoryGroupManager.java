package com.rtw.tmat.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rtw.tmat.dao.implementation.TMATDAORegistry;
import com.rtw.tmat.data.Category;
import com.rtw.tmat.data.DefaultPurchasePrice;
import com.rtw.tmat.data.Event;
import com.rtw.tmat.data.ManagePurchasePrice;
import com.rtw.tmat.data.RewardthefanCategoryTicket;
import com.rtw.tmat.data.RewardthefanCatsExchangeEvent;
import com.rtw.tmat.data.SoldCategoryTicket;
import com.rtw.tmat.data.Ticket;


public class CategoryGroupManager {
	public static Logger logger = LoggerFactory.getLogger(CategoryGroupManager.class);
	static DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	static DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	static DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
	public static int PURCHASE_PRICE_THRESHHOLD = 20;
	public static int MAX_PUR_PRICE_THRESHOLD_PERCENTAGE = 200;
	
	public static Collection<Ticket> getTMATActiveTicketsFromDB(Event event) throws Exception {
		
		Date now = new Date();
		Collection<Ticket> ticketList = new ArrayList<Ticket>();
		try{
			boolean isTNCrawlOnly = false;
			String testEventName = event.getName().replaceAll("\\s+", "").toLowerCase();
			
			//Tamil : if event name contains any of following key word then  consider ticketnetworkdirect crawl tickets only for that event 
			if(testEventName.contains("2day") || testEventName.contains("twoday") //2day,2days,2 day,2 days,two day,two days
					|| testEventName.contains("3day") || testEventName.contains("threeday")//3day,3days,3 day,3 days,three day,three days
					|| testEventName.contains("4day") || testEventName.contains("fourday")//4day,4days,4 day,4 days,four day,four days
					|| testEventName.contains("multiday") //multi day,multidays 
					|| testEventName.contains("weekly") || testEventName.contains("weekend")//weekly,weekend
					|| testEventName.contains("allsession")
					|| testEventName.contains("allperformances")
					|| testEventName.contains("seasontickets")//Season Tickets
					|| testEventName.contains("fullstriptickets"))//Full Strip Tickets is equal to 4 days pass 
			{
				isTNCrawlOnly = true;
			}

			//Tamil : if events belongs to AT&T Stadiu or AT&T Stadium Parking Lots then skip vividseat crawl tickets
			boolean isSkipVividCrawls = false;
			if(event.getVenueId() != null && 
					( event.getVenueId().equals(8913) || //AT&T Stadium
							event.getVenueId().equals(13919))) {//AT&T Stadium Parking Lots
				isSkipVividCrawls = true;
			}
			
			Collection<Ticket> dbTicketList = new ArrayList<Ticket>();
//			if(product.getName().equalsIgnoreCase("RewardTheFan Listings") || product.getName().equalsIgnoreCase("RTFLastRowCats")) {
				//if event date with in 3 days then load etickets and instant tickets
				//Tamil : consider all tickets even if event is within 3 days
				//if(event.getLocalDate()!=null && ((event.getLocalDate().getTime() - now.getTime())  <= 72 * 60 * 60 * 1000 )){
				//	dbTicketList = DAORegistry.getTicketDAO().getAllActiveInstantTicketForAutoCatByEventId(event.getId(),isTNCrawlOnly,isSkipVividCrawls);
				//} else {
					dbTicketList = TMATDAORegistry.getTicketDAO().getAllActiveTicketForAutoCatsByEventId(event.getId(),isTNCrawlOnly,isSkipVividCrawls);
				//}
//			} else {
//				if(event.getLocalDate()!=null && ((event.getLocalDate().getTime() - now.getTime())  <= 120 * 60 * 60 * 1000 )){
//					dbTicketList = TMATDAORegistry.getTicketDAO().getAllActiveInstantTicketForAutoCatByEventId(event.getId(),isTNCrawlOnly,isSkipVividCrawls);
//				} else {
//					dbTicketList = TMATDAORegistry.getTicketDAO().getAllActiveTicketForAutoCatsByEventId(event.getId(),isTNCrawlOnly,isSkipVividCrawls);
//				}
//			}
			//Tamil : we have to skip each category sold tickets associated TMAT ticket in computation
			Collection<SoldCategoryTicket> soldCatTicketList = TMATDAORegistry.getSoldCategoryTicketDAO().getAllSoldCategoryTicketsByEventId(event.getId());
			Map<Integer, Boolean> soldCatTicketMap = new HashMap<Integer, Boolean>();
			if(soldCatTicketList != null) {
				for (SoldCategoryTicket soldCategoryTicket : soldCatTicketList) {
					soldCatTicketMap.put(soldCategoryTicket.getTicketId(), Boolean.TRUE);
				}
			}
			for (Ticket ticket : dbTicketList) {
				if(soldCatTicketMap.get(ticket.getId()) != null) {
					continue;	
				}
				
				//Tamil 07/17/19 added aces keyword
				//Tamil 07/15/19 added garage and Park key words
				//Tamil 05/14/19 added following key words : pltprk, prmprk, vipprk, lot
				if(ticket.getNormalizedSection() == null || ticket.getNormalizedSection().toLowerCase().contains("park") || //park added so parking removed
						 ticket.getNormalizedSection().toLowerCase().contains("pass") || ticket.getNormalizedSection().toLowerCase().contains("vippk") ||
						 ticket.getNormalizedSection().toLowerCase().contains("vipk") || ticket.getNormalizedSection().toLowerCase().contains("pltprk") ||
						 ticket.getNormalizedSection().toLowerCase().contains("prmprk") || ticket.getNormalizedSection().toLowerCase().contains("vipprk") ||
						 ticket.getNormalizedSection().toLowerCase().contains("lot") || ticket.getNormalizedSection().toLowerCase().contains("garage") ||
						 ticket.getNormalizedSection().toLowerCase().contains("aces")) {
					continue;
				}
				if(ticket.getNormalizedRow() != null && ( ticket.getNormalizedRow().toLowerCase().contains("park") ||
						 ticket.getNormalizedRow().toLowerCase().contains("pass") || ticket.getNormalizedRow().toLowerCase().contains("vippk") ||
						 ticket.getNormalizedRow().toLowerCase().contains("vipk") || ticket.getNormalizedRow().toLowerCase().contains("pltprk") ||
						 ticket.getNormalizedRow().toLowerCase().contains("prmprk") || ticket.getNormalizedRow().toLowerCase().contains("vipprk") ||
						 ticket.getNormalizedRow().toLowerCase().contains("lot") || ticket.getNormalizedRow().toLowerCase().contains("garage") ||
						 ticket.getNormalizedRow().toLowerCase().contains("aces"))) {
					continue;
				}
				if(ticket.getNormalizedSeat() != null && ( ticket.getNormalizedSeat().toLowerCase().contains("park") ||
						 ticket.getNormalizedSeat().toLowerCase().contains("pass") || ticket.getNormalizedSeat().toLowerCase().contains("vippk") ||
						 ticket.getNormalizedSeat().toLowerCase().contains("vipk") || ticket.getNormalizedSeat().toLowerCase().contains("pltprk") ||
						 ticket.getNormalizedSeat().toLowerCase().contains("prmprk") || ticket.getNormalizedSeat().toLowerCase().contains("vipprk") ||
						 ticket.getNormalizedSeat().toLowerCase().contains("lot") || ticket.getNormalizedSeat().toLowerCase().contains("garage") ||
						 ticket.getNormalizedSeat().toLowerCase().contains("aces"))) {
					continue;
				}
				ticketList.add(ticket);
			}
			
			//Tamil : Added to skip tickets with quantity 2 and current price below 100 in auto pricing
			/*for (Ticket ticket : dbTicketList) {
				if(ticket.getQuantity().equals(2) && ticket.getCurrentPrice()< 100) {
					continue;
				}
				ticketList.add(ticket);
			}*/
			
		}catch (Exception e) {
			logger.error("105.NF Event:" + event.getId() + ":-:" + event.getName() +":"+ event.getLocalDate() );
			System.out.println("105.NF Event:" + event.getId() + ":-:" + event.getName() +":"+ event.getLocalDate() );
			e.printStackTrace();
			throw e;
		}
		return ticketList;
	}
	
	public static List<RewardthefanCategoryTicket> computeCategoryTickets(Event event,RewardthefanCatsExchangeEvent exEvent) throws Exception {
		
		
		Set<Ticket> finalTicketList = new HashSet<Ticket>();
		List<Ticket> uncatTixList = null;
		Collection<Category> categoryList = null;
		Collection<Ticket> ticketList = getTMATActiveTicketsFromDB(event);
		try {
			
			categoryList = TMATDAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategoryId());
			ticketList = TicketUtil.removeDuplicateTickets(ticketList);
			ticketList = TicketUtil.removeAdmitoneTickets(ticketList, event, event.getAdmitoneId(),false);
			//ticketList = TicketUtil.preAssignCategoriesToTickets(ticketList, event, categoryList);
			
			if(event.getVenueCategoryId() != null) {
				ticketList = TicketUtil.preAssignCategoriesToTickets(ticketList, categoryList, event);	
			}
			

			//Purchase tour = DAORegistry.getPurchaseDAO().getPurchaseByEventId(event.getId());
			Collection<DefaultPurchasePrice> defaultPurchasePrices = TMATDAORegistry.getDefaultPurchasePriceDAO().getAll();
			Map<String, DefaultPurchasePrice> defaultPurchasePriceMap = new HashMap<String, DefaultPurchasePrice>();
			for(DefaultPurchasePrice defaultPurchasePrice:defaultPurchasePrices){
				defaultPurchasePriceMap.put(defaultPurchasePrice.getExchange() + "-" + defaultPurchasePrice.getTicketType(), defaultPurchasePrice);
			}
			Collection<ManagePurchasePrice> managePurchasePricelist = TMATDAORegistry.getManagePurchasePriceDAO().getAllManagePurchasePriceByEventId(event.getId());//tour.getTmatPurchaseId());
			Map<String, ManagePurchasePrice> tourPriceMap = new HashMap<String, ManagePurchasePrice>();
			for(ManagePurchasePrice tourPrice:managePurchasePricelist){
				tourPriceMap.put(tourPrice.getExchange()+"-" + tourPrice.getTicketType(),tourPrice);
			}
			
			for (Ticket ticket : ticketList) {
				TicketUtil.getPurchasePrice(ticket, defaultPurchasePriceMap, null, tourPriceMap);
				//Tamil : for rewardthefan consider tickets only with purchase price above $20
				if(ticket.getPurchasePrice() < PURCHASE_PRICE_THRESHHOLD) {
					continue;
				}
				finalTicketList.add(ticket);
			}
			
			return getZoneTicketsCategoryTickets(event, exEvent, finalTicketList);
			
			//Removing Fake Tickets by Maximum Purchase Price Threshold
			//finalTicketList = TicketUtil.removeFakeTicketsByCategoryandSection(finalTicketList, MAX_PUR_PRICE_THRESHOLD_PERCENTAGE);
			
		}catch (Exception e) {
			e.printStackTrace();
			logger.error("106.NF Event:" + event.getId() + ":-:" + event.getName() +":"+ event.getLocalDate() );
			//System.out.println("106.NF Event:" + event.getId() + e.fillInStackTrace());
			throw e;
		}
		
	}
	
	public static List<RewardthefanCategoryTicket> getZoneTicketsCategoryTickets(Event event,RewardthefanCatsExchangeEvent exEvent,Set<Ticket> ticketsList) throws Exception {

		//Tamil : 05/29/19 rpt changed to 20 from 40.
		//Tamil : 05/06/19 amit asked to change markup to 50% from 100%.
		String exposure = "2-OXP";
		Double rptFactor = 20.0;
		Double markup = 100.0;
		Integer quantity =2;
		Integer minTixEntry = 2;
		List<RewardthefanCategoryTicket> rtfCatsList = new ArrayList<RewardthefanCategoryTicket>();
		
		RewardthefanCategoryTicket catTicket =  new RewardthefanCategoryTicket();
		TicketUtil.getCheapestTicketByQty(ticketsList, exEvent, quantity, minTixEntry,catTicket,exposure,rptFactor);
		if(catTicket.getPurPrice()!=null) {
			catTicket.setQuantity(2);
			computecategoryTicketPrices(catTicket, exEvent, markup);
			rtfCatsList.add(catTicket);
		}
		return rtfCatsList;
	}
	public static void computecategoryTicketPrices(RewardthefanCategoryTicket catTicket,RewardthefanCatsExchangeEvent exEvent,double markup) throws Exception {
		
		
			double roundedPrice;
			double eventMarkup = markup;
			
			roundedPrice = (double) Math.ceil(((catTicket.getPurPrice()*(1+(eventMarkup)/100))) * 100 / 100);
			catTicket.setActualPrice(roundedPrice);
			
			roundedPrice = (double) Math.ceil(((catTicket.getPurPrice()*(1+(eventMarkup)/100))) * 100 / 100);
			catTicket.setRtfPrice(roundedPrice);
	}

}
