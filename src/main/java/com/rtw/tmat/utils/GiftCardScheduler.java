package com.rtw.tmat.utils;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.RtfGiftCard;
import com.rtw.tracker.datas.RtfGiftCardQuantity;
import com.rtw.tracker.utils.Util;

public class GiftCardScheduler extends QuartzJobBean implements StatefulJob {

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		if(Util.isContestRunning){
			System.out.println("RETURN");
			return;
		}
		System.out.println("RUNNNNN");
		Collection<RtfGiftCard> cards = DAORegistry.getRtfGiftCardDAO().getAllActiveGiftCards();
		Date today = new Date();
		
		
		for(RtfGiftCard gc : cards){
			if(gc.getEndDate()!=null && gc.getEndDate().before(today)){
				List<RtfGiftCardQuantity> list = DAORegistry.getRtfGiftCardQuantityDAO().getAllGiftCardQuantityByCardId(gc.getId());
				for(RtfGiftCardQuantity qty : list){
					qty.setStatus(false);
				}
				gc.setCardStatus("EXPIRED");
				DAORegistry.getRtfGiftCardDAO().update(gc);
				DAORegistry.getRtfGiftCardQuantityDAO().updateAll(list);
			}
		}
		
	}

}
