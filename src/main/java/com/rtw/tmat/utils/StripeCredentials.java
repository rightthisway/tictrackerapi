package com.rtw.tmat.utils;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("StripeCredentials")
public class StripeCredentials {
	private Integer status;
	private Error error; 
	private String publishableKey;
	
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getPublishableKey() {
		return publishableKey;
	}
	public void setPublishableKey(String publishableKey) {
		this.publishableKey = publishableKey;
	}
	
	
	
}
