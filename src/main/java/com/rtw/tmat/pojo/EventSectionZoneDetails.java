package com.rtw.tmat.pojo;

import java.io.Serializable;

public class EventSectionZoneDetails implements Serializable{
	
	private Integer eventId;
	private String section;
	private String zone;
	
	
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	
	

}
