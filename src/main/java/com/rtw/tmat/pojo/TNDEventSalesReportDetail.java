package com.rtw.tmat.pojo;


public class TNDEventSalesReportDetail {
	
	private Integer eventId;
	private String eventName;
	private String eventDateStr;
	private String eventTimeStr;
	private String venue;
	private String city;
	private String state;
	private String country;
	private Integer salesCount;
	private String recentSaleDateStr;
	
	
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getEventDateStr() {
		return eventDateStr;
	}
	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}
	public String getEventTimeStr() {
		return eventTimeStr;
	}
	public void setEventTimeStr(String eventTimeStr) {
		this.eventTimeStr = eventTimeStr;
	}
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Integer getSalesCount() {
		return salesCount;
	}
	public void setSalesCount(Integer salesCount) {
		this.salesCount = salesCount;
	}
	public String getRecentSaleDateStr() {
		return recentSaleDateStr;
	}
	public void setRecentSaleDateStr(String recentSaleDateStr) {
		this.recentSaleDateStr = recentSaleDateStr;
	}
	
	
}
