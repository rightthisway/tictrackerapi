package com.rtw.tmat.enums;

public enum TicketDeliveryType {
	INSTANT, EDELIVERY, MERCURY, MERCURYPOS, ETICKETS
}
