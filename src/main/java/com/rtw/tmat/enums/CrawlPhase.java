package com.rtw.tmat.enums;

public enum CrawlPhase {
	STOPPED,DISPATCHED, EXTRACTION, FLUSH, EXPIRATION, REQUEUING
}
