package com.rtw.tmat.enums;

public enum ArtistStatus {
	ACTIVE, EXPIRED, DELETED
}
