package com.rtw.tmat.enums;

public enum EventStatus {
	ACTIVE, EXPIRED, DELETED,DELETEDEXPIRED
}
