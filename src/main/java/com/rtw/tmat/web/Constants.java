package com.rtw.tmat.web;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;

import com.rtw.tmat.data.Site;

/**
 * Hold list of countries and states
 */
public final class Constants {
	private static String admit1NotePattern;
	
	private static String hostName = null;
	private static String ipAddress = null;
	
	static {		
	    try {
		    InetAddress addr = InetAddress.getLocalHost();
	    	hostName = addr.getHostName();
	    	byte[] b = addr.getAddress();
	    	ipAddress = (b[0] & 0xFF) + "." + (b[1] & 0xFF) + "." + (b[2] & 0xFF) + "." + (b[3] & 0xFF);
			if (hostName == null) {
				hostName = addr.getHostAddress();
			}
	    } catch (Exception e) {};
	}

	
	private static final Country[] COUNTRIES = {
		new Country("AF", "Afghanistan"),
		new Country("AL", "Albania"),
		new Country("DZ", "Algeria"),
		new Country("AS", "American Samoa"),
		new Country("AD", "Andorra"),
		new Country("AO", "Angola"),
		new Country("AI", "Anguilla"),
		new Country("AQ", "Antarctica"),
		new Country("AG", "Antigua and Barbuda"),
		new Country("AR", "Argentina"),
		new Country("AM", "Armenia"),
		new Country("AW", "Aruba"),
		new Country("AU", "Australia"),
		new Country("AT", "Austria"),
		new Country("AZ", "Azerbaidjan"),
		new Country("BS", "Bahamas"),
		new Country("BH", "Bahrain"),
		new Country("BD", "Bangladesh"),
		new Country("BB", "Barbados"),
		new Country("BY", "Belarus"),
		new Country("BE", "Belgium"),
		new Country("BZ", "Belize"),
		new Country("BJ", "Benin"),
		new Country("BM", "Bermuda"),
		new Country("BT", "Bhutan"),
		new Country("BO", "Bolivia"),
		new Country("BA", "Bosnia-Herzegovina"),
		new Country("BW", "Botswana"),
		new Country("BV", "Bouvet Island"),
		new Country("BR", "Brazil"),
		new Country("IO", "British Indian Ocean Territory"),
		new Country("BN", "Brunei Darussalam"),
		new Country("BG", "Bulgaria"),
		new Country("BF", "Burkina Faso"),
		new Country("BI", "Burundi"),
		new Country("KH", "Cambodia"),
		new Country("CM", "Cameroon"),
		new Country("CA", "Canada"),
		new Country("CV", "Cape Verde"),
		new Country("KY", "Cayman Islands"),
		new Country("CF", "Central African Republic"),
		new Country("TD", "Chad"),
		new Country("CL", "Chile"),
		new Country("CN", "China"),
		new Country("CX", "Christmas Island"),
		new Country("CC", "Cocos (Keeling) Islands"),
		new Country("CO", "Colombia"),
		new Country("KM", "Comoros"),
		new Country("CG", "Congo"),
		new Country("CK", "Cook Islands"),
		new Country("CR", "Costa Rica"),
		new Country("HR", "Croatia"),
		new Country("CU", "Cuba"),
		new Country("CY", "Cyprus"),
		new Country("CZ", "Czech Republic"),
		new Country("DK", "Denmark"),
		new Country("DJ", "Djibouti"),
		new Country("DM", "Dominica"),
		new Country("DO", "Dominican Republic"),
		new Country("TP", "East Timor"),
		new Country("EC", "Ecuador"),
		new Country("EG", "Egypt"),
		new Country("SV", "El Salvador"),
		new Country("GQ", "Equatorial Guinea"),
		new Country("ER", "Eritrea"),
		new Country("EE", "Estonia"),
		new Country("ET", "Ethiopia"),
		new Country("FK", "Falkland Islands"),
		new Country("FO", "Faroe Islands"),
		new Country("FJ", "Fiji"),
		new Country("FI", "Finland"),
		new Country("CS", "Former Czechoslovakia"),
		new Country("SU", "Former USSR"),
		new Country("FR", "France"),
		new Country("FX", "France (European Territory)"),
		new Country("GF", "French Guyana"),
		new Country("TF", "French Southern Territories"),
		new Country("GA", "Gabon"),
		new Country("GM", "Gambia"),
		new Country("GE", "Georgia"),
		new Country("DE", "Germany"),
		new Country("GH", "Ghana"),
		new Country("GI", "Gibraltar"),
		new Country("GB", "Great Britain"),
		new Country("GR", "Greece"),
		new Country("GL", "Greenland"),
		new Country("GD", "Grenada"),
		new Country("GP", "Guadeloupe (French)"),
		new Country("GU", "Guam (USA)"),
		new Country("GT", "Guatemala"),
		new Country("GN", "Guinea"),
		new Country("GW", "Guinea Bissau"),
		new Country("GY", "Guyana"),
		new Country("HT", "Haiti"),
		new Country("HM", "Heard and McDonald Islands"),
		new Country("HN", "Honduras"),
		new Country("HK", "Hong Kong"),
		new Country("HU", "Hungary"),
		new Country("IS", "Iceland"),
		new Country("IN", "India"),
		new Country("ID", "Indonesia"),
		new Country("INT", "International"),
		new Country("IR", "Iran"),
		new Country("IQ", "Iraq"),
		new Country("IE", "Ireland"),
		new Country("IL", "Israel"),
		new Country("IT", "Italy"),
		new Country("CI", "Ivory Coast (Cote D&#39;Ivoire)"),
		new Country("JM", "Jamaica"),
		new Country("JP", "Japan"),
		new Country("JO", "Jordan"),
		new Country("KZ", "Kazakhstan"),
		new Country("KE", "Kenya"),
		new Country("KI", "Kiribati"),
		new Country("KW", "Kuwait"),
		new Country("KG", "Kyrgyzstan"),
		new Country("LA", "Laos"),
		new Country("LV", "Latvia"),
		new Country("LB", "Lebanon"),
		new Country("LS", "Lesotho"),
		new Country("LR", "Liberia"),
		new Country("LY", "Libya"),
		new Country("LI", "Liechtenstein"),
		new Country("LT", "Lithuania"),
		new Country("LU", "Luxembourg"),
		new Country("MO", "Macau"),
		new Country("MK", "Macedonia"),
		new Country("MG", "Madagascar"),
		new Country("MW", "Malawi"),
		new Country("MY", "Malaysia"),
		new Country("MV", "Maldives"),
		new Country("ML", "Mali"),
		new Country("MT", "Malta"),
		new Country("MH", "Marshall Islands"),
		new Country("MQ", "Martinique (French)"),
		new Country("MR", "Mauritania"),
		new Country("MU", "Mauritius"),
		new Country("YT", "Mayotte"),
		new Country("MX", "Mexico"),
		new Country("FM", "Micronesia"),
		new Country("MD", "Moldavia"),
		new Country("MC", "Monaco"),
		new Country("MN", "Mongolia"),
		new Country("MS", "Montserrat"),
		new Country("MA", "Morocco"),
		new Country("MZ", "Mozambique"),
		new Country("MM", "Myanmar"),
		new Country("NA", "Namibia"),
		new Country("NR", "Nauru"),
		new Country("NP", "Nepal"),
		new Country("NL", "Netherlands"),
		new Country("AN", "Netherlands Antilles"),
		new Country("NT", "Neutral Zone"),
		new Country("NC", "New Caledonia (French)"),
		new Country("NZ", "New Zealand"),
		new Country("NI", "Nicaragua"),
		new Country("NE", "Niger"),
		new Country("NG", "Nigeria"),
		new Country("NU", "Niue"),
		new Country("NF", "Norfolk Island"),
		new Country("KP", "North Korea"),
		new Country("MP", "Northern Mariana Islands"),
		new Country("NO", "Norway"),
		new Country("OM", "Oman"),
		new Country("PK", "Pakistan"),
		new Country("PW", "Palau"),
		new Country("PA", "Panama"),
		new Country("PG", "Papua New Guinea"),
		new Country("PY", "Paraguay"),
		new Country("PE", "Peru"),
		new Country("PH", "Philippines"),
		new Country("PN", "Pitcairn Island"),
		new Country("PL", "Poland"),
		new Country("PF", "Polynesia (French)"),
		new Country("PT", "Portugal"),
		new Country("PR", "Puerto Rico"),
		new Country("QA", "Qatar"),
		new Country("RE", "Reunion (French)"),
		new Country("RO", "Romania"),
		new Country("RU", "Russian Federation"),
		new Country("RW", "Rwanda"),
		new Country("GS", "S. Georgia & S. Sandwich Isls."),
		new Country("SH", "Saint Helena"),
		new Country("KN", "Saint Kitts & Nevis Anguilla"),
		new Country("LC", "Saint Lucia"),
		new Country("PM", "Saint Pierre and Miquelon"),
		new Country("ST", "Saint Tome (Sao Tome) and Principe"),
		new Country("VC", "Saint Vincent & Grenadines"),
		new Country("WS", "Samoa"),
		new Country("SM", "San Marino"),
		new Country("SA", "Saudi Arabia"),
		new Country("SN", "Senegal"),
		new Country("SC", "Seychelles"),
		new Country("SL", "Sierra Leone"),
		new Country("SG", "Singapore"),
		new Country("SK", "Slovak Republic"),
		new Country("SI", "Slovenia"),
		new Country("SB", "Solomon Islands"),
		new Country("SO", "Somalia"),
		new Country("ZA", "South Africa"),
		new Country("KR", "South Korea"),
		new Country("ES", "Spain"),
		new Country("LK", "Sri Lanka"),
		new Country("SD", "Sudan"),
		new Country("SR", "Suriname"),
		new Country("SJ", "Svalbard and Jan Mayen Islands"),
		new Country("SZ", "Swaziland"),
		new Country("SE", "Sweden"),
		new Country("CH", "Switzerland"),
		new Country("SY", "Syria"),
		new Country("TJ", "Tadjikistan"),
		new Country("TW", "Taiwan"),
		new Country("TZ", "Tanzania"),
		new Country("TH", "Thailand"),
		new Country("TG", "Togo"),
		new Country("TK", "Tokelau"),
		new Country("TO", "Tonga"),
		new Country("TT", "Trinidad and Tobago"),
		new Country("TN", "Tunisia"),
		new Country("TR", "Turkey"),
		new Country("TM", "Turkmenistan"),
		new Country("TC", "Turks and Caicos Islands"),
		new Country("TV", "Tuvalu"),
		new Country("UG", "Uganda"),
		new Country("UA", "Ukraine"),
		new Country("AE", "United Arab Emirates"),
		new Country("GB", "United Kingdom"),
		new Country("UY", "Uruguay"),
		new Country("US", "United States"),
		new Country("MIL", "USA Military"),
		new Country("UM", "USA Minor Outlying Islands"),
		new Country("UZ", "Uzbekistan"),
		new Country("VU", "Vanuatu"),
		new Country("VA", "Vatican City State"),
		new Country("VE", "Venezuela"),
		new Country("VN", "Vietnam"),
		new Country("VG", "Virgin Islands (British)"),
		new Country("VI", "Virgin Islands (USA)"),
		new Country("WF", "Wallis and Futuna Islands"),
		new Country("EH", "Western Sahara"),
		new Country("YE", "Yemen"),
		new Country("YU", "Yugoslavia"),
		new Country("ZR", "Zaire"),
		new Country("ZM", "Zambia"),
		new Country("ZW", "Zimbabwe")
	};		

	private static final State[] STATES = {
		new State("-", "N/A"),
		new State("AK", "ALASKA"),
		new State("AL", "ALABAMA"),
		new State("AR", "ARKANSAS"),
		new State("AZ", "ARIZONA"),
		new State("CA", "CALIFORNIA"),
		new State("CO", "COLORADO"),
		new State("CT", "CONNECTICUT"),
		new State("DC", "District of Columbia"),
		new State("DE", "DELAWARE"),
		new State("FL", "FLORIDA"),
		new State("GA", "GEORGIA"),
		new State("HI", "HAWAII"),
		new State("IA", "IOWA"),
		new State("ID", "IDAHO"),
		new State("IL", "ILLINOIS"),
		new State("IN", "INDIANA"),
		new State("KS", "KANSAS"),
		new State("KY", "KENTUCKY"),
		new State("LA", "LOUISIANA"),
		new State("MA", "MASSACHUSETTS"),
		new State("MD", "MARYLAND"),
		new State("ME", "MAINE"),
		new State("MI", "MICHIGAN"),
		new State("MN", "MINNESOTA"),
		new State("MS", "MISSISSIPPI"),
		new State("MO", "MISSOURI"),
		new State("MT", "MONTANA"),
		new State("NC", "NORTH CAROLINA"),
		new State("ND", "NORTH DAKOTA"),
		new State("NE", "NEBRASKA"),
		new State("NH", "NEW HAMPSHIRE"),
		new State("NJ", "NEW JERSEY"),
		new State("NM", "NEW MEXICO"),
		new State("NV", "NEVADA"),
		new State("NY", "NEW YORK"),
		new State("OH", "OHIO"),
		new State("OK", "OKLAHOMA"),
		new State("OR", "OREGON"),
		new State("PA", "PENNSYLVANIA"),
		new State("RI", "RHODE ISLAND"),
		new State("SC", "SOUTH CAROLINA"),
		new State("SD", "SOUTH DAKOTA"),
		new State("TN", "TENNESSEE"),
		new State("TX", "TEXAS"),
		new State("UT", "UTAH"),
		new State("VA", "VIRGINIA"),
		new State("VI", "VIRGIN ISLANDS"),
		new State("VT", "VERMONT"),
		new State("WA", "WASHINGTON"),
		new State("WI", "WISCONSIN"),
		new State("WV", "WEST VIRGINIA"),
		new State("WY", "WYOMING"),
		new State("AS", "AMERICAN SAMOA"),
		new State("FM", "MICRONESIA"),
		new State("GU", "GUAM"),
		new State("MH", "MARSHALL ISLANDS"),
		new State("MP", "MARIANA ISLANDS"),
		new State("PR", "PUERTO RICO"),
		new State("VI", "VIRGIN ISLANDS, U.S."),
		new State("AA", "ARMED FORCES AA"),
		new State("AE", "ARMED FORCES AE"),
		new State("AP", "ARMED FORCES AP")				
	};
	
	private final static String[] siteIds= new String[] {
		Site.TICKET_NETWORK_DIRECT, Site.STUB_HUB, Site.TICKET_EVOLUTION, Site.TICKETS_NOW, Site.VIVIDSEAT,Site.FLASH_SEATS,Site.TICK_PICK ,Site.TICKET_CITY
		,Site.TICKET_LIQUIDATOR,Site.STUB_HUB_API,Site.LIVE_NATION,Site.TICKET_NETWORK,Site.STUBHUB_FEED, Site.FAN_SNAP, Site.RAZOR_GATOR,  Site.TICKET_MASTER,Site.EI_MARKETPLACE,Site.EVENT_INVENTORY,
		Site.WS_TICKETS, Site.EBAY,Site.AOP, Site.EIBOX, Site.GET_ME_IN, Site.SEATWAVE, Site.TICKET_MASTER,  Site.TICKET_SOLUTIONS, Site.SEATWAVE_FEED, Site.VIAGOGO, Site.SEATGEEK
	};

//	private final static String[] extendedSiteIds= new String[] {
//		Site.TICKET_NETWORK_DIRECT, Site.STUB_HUB, Site.TICKET_EVOLUTION, Site.TICKETS_NOW, Site.VIVIDSEAT 
//		,Site.TICKET_NETWORK,Site.STUBHUB_FEED, Site.FAN_SNAP, Site.RAZOR_GATOR,  Site.TICKET_MASTER,Site.EI_MARKETPLACE,Site.EVENT_INVENTORY,
//		Site.WS_TICKETS, Site.EBAY,Site.AOP, Site.EIBOX, Site.GET_ME_IN, Site.SEATWAVE, Site.TICKET_MASTER,  Site.TICKET_SOLUTIONS, Site.SEATWAVE_FEED, Site.VIAGOGO, Site.SEATGEEK
//		};

	private final static String[] disabledSiteIds = new String[] {
		Site.TICKET_NETWORK,Site.STUBHUB_FEED, Site.FAN_SNAP, Site.RAZOR_GATOR,  Site.TICKET_MASTER,Site.EI_MARKETPLACE,Site.EVENT_INVENTORY,
		Site.WS_TICKETS, Site.EBAY,Site.AOP, Site.EIBOX, Site.GET_ME_IN, Site.SEATWAVE, Site.TICKET_MASTER,  Site.TICKET_SOLUTIONS, Site.SEATWAVE_FEED, Site.VIAGOGO, Site.SEATGEEK,
		Site.STUB_HUB_API
	};
	
	private static TimeZone[] timeZones;
	
	// populate timezones
	static {
		String[] timeZoneIds = TimeZone.getAvailableIDs();
		
		timeZones = new TimeZone[timeZoneIds.length];
		int i = 0;
		for (String timeZoneId: timeZoneIds) {
			timeZones[i++] = TimeZone.getTimeZone(timeZoneId);
		}
		
		// sort timezone by name
		Arrays.sort(timeZones, new Comparator<TimeZone>() {
			public int compare(TimeZone t1, TimeZone t2) {
				return t1.getID().compareTo(t2.getID());
			}
			
		});
	}
	
	private static Constants instance;
	
	public Date getCurrentDate() {
		return new Date();
	}

	public void setAdmit1NotePattern(String notePattern) {
		Constants.admit1NotePattern = notePattern.trim();
	}
	
	public String getAdmit1NotePattern() {
		return Constants.admit1NotePattern;
	}

	public Country[] getCountries() {
		return Constants.COUNTRIES;
	}

	public State[] getStates() {
		return Constants.STATES;
	}
	
	public String[] getSiteIds() {
		return Constants.siteIds;
	}
	
	public String[] getEnabledSiteIds() {
		Collection<String> result = new ArrayList<String>();
		String disabledSiteIdsStr = getDisabledSiteIdsString();
		for (String siteId: siteIds) {
			if (!disabledSiteIdsStr.contains("[" + siteId + "]")) {
				result.add(siteId);
			}
		}
		
		String[] array = new String[result.size()];
		return result.toArray(array);
	}	

	public String[] getExtendedSiteIds() {
		return Constants.siteIds;
	}	

	public String[] getDisabledSiteIds() {
		return disabledSiteIds;
	}
	
	public String getDisabledSiteIdsString() {
		String result = "";
		for (String siteId: disabledSiteIds) {
			result += "[" + siteId + "],";
		}
		
		return result.substring(0, result.length() - 1);
	}
	
	public boolean isDisabled(String site) {
		for (String siteId: disabledSiteIds) {
			if (site.equals(siteId)) {
				return true;
			}
		}
		
		return false;
	}

	public TimeZone[] getTimeZones() {
		return Constants.timeZones;
	}	
	
	public String getHostName() {
		return hostName;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public static Constants getInstance() {
		if (instance == null) {
			instance = new Constants();
		}
		return instance;
	}
	
	public static final int MAX_FAILED_LOGIN_ATTEMPTS = 10;
	
	public static class Country {
		private String symbol;
		private String name;
		
		public Country(String symbol, String name) {
			this.symbol = symbol;
			this.name = name;
		}

		public String getSymbol() {
			return symbol;
		}

		public void setSymbol(String symbol) {
			this.symbol = symbol;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}		
	}

	public static class State {
		private String symbol;
		private String name;
		
		public State(String symbol, String name) {
			this.symbol = symbol;
			this.name = name;
		}

		public String getSymbol() {
			return symbol;
		}

		public void setSymbol(String symbol) {
			this.symbol = symbol;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

	
	}

}
