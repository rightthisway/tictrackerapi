package com.rtw.tmat.dao.implementation;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.rtw.tmat.data.Event;
import com.rtw.tracker.dao.implementation.HibernateDAO;


public class EventDAO extends HibernateDAO<Integer, Event> implements com.rtw.tmat.dao.services.EventDAO {	
	
	public List<Event> getAllActiveEventsByArtistId(Integer artistId) {
		return find("FROM Event WHERE artistId=? AND eventStatus='ACTIVE' ORDER BY event_date, event_time Asc", new Object[]{artistId});
	}
	
	public List<Event> getAllActiveEventsByVenueId(Integer venueId) {
		return find("FROM Event WHERE venueId=? AND eventStatus='ACTIVE' ORDER BY event_date, event_time Asc", new Object[]{venueId});
	}
	public Collection<Event> getAllActiveEvents() {
		return find("FROM Event WHERE eventStatus='ACTIVE'", new Object[]{});
	}
	public List<Event> getAllEventsByVenueCategoryId(Integer venueCategoryId) {
		return find("FROM Event Where venueCategoryId = ?" ,new Object[]{venueCategoryId});
	}
	
	public Event.EventTicketStat getNonDuplicateEventTicketStat(int eventId) {
		
		Session session = getSession();
		
//		session.beginTransaction();
		SQLQuery query = session.createSQLQuery("SELECT COUNT(remaining_quantity), SUM(remaining_quantity) FROM (select remaining_quantity FROM ticket with(nolock) WHERE event_id=:eventId and ticket_status='ACTIVE' GROUP BY ticket_duplicate_key) as o");
		query.setInteger("eventId", eventId);
		List<Object[]> list = query.list();
		releaseSession(session);
		
		if (list == null || list.size() == 0) {
			return null;
		}
		
		int ticketListingCount = 0;
		int ticketCount = 0;
		
		if (list.get(0)[0] != null) {
			ticketListingCount = ((BigInteger)list.get(0)[0]).intValue(); 
		}
		if (list.get(0)[1] != null) {
			ticketCount = ((BigDecimal)list.get(0)[1]).intValue(); 
		}
		
		return new Event.EventTicketStat(ticketListingCount, ticketCount);
	}

	public List<Event> getAllActiveEventsByIds(List<Integer> artistIds) {
		String list = artistIds.toString().replace("[", "").replace("]", "").replaceAll(", ", ",");
		return find("FROM Event WHERE id in (" + list +") ORDER BY event_date, event_time Asc");//, new Object[]{artistId});
	}

	@Override
	public Event getEventById(Integer eventId) {
		return findSingle("FROM Event WHERE id=?", new Object[]{eventId});
	}
	
	@Override
	public Event getEventByAdmitOneId(Integer admitOneId) {
		return findSingle("FROM Event WHERE admitoneId=?", new Object[]{admitOneId});
	}
	
}
