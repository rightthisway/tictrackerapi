package com.rtw.tmat.dao.implementation;

import java.util.Collection;

import com.rtw.tmat.data.Artist;
import com.rtw.tmat.enums.ArtistStatus;
import com.rtw.tracker.dao.implementation.HibernateDAO;



public class ArtistDAO extends HibernateDAO<Integer, Artist> implements com.rtw.tmat.dao.services.ArtistDAO {
	
	public Collection<Artist> filterByName(String pattern) {
		return find("FROM Artist WHERE artistStatus=? AND name LIKE ? ORDER BY name ASC",new Object[]{ArtistStatus.ACTIVE,"%"+pattern+"%"});
		
	}
}


