package com.rtw.tmat.dao.implementation;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.rtw.tmat.data.RewardthefanCategoryTicket;
import com.rtw.tracker.dao.implementation.HibernateDAO;


public class RewardthefanCategoryTicketDAO extends HibernateDAO<Integer, RewardthefanCategoryTicket> implements com.rtw.tmat.dao.services.RewardthefanCategoryTicketDAO {

	public List<RewardthefanCategoryTicket> getAllActiveRtfCategoryTicketsByEventId(Integer eventId) throws Exception {
		return find("FROM RewardthefanCategoryTicket where status=? and eventId=?" ,new Object[]{"ACTIVE",eventId});
	}
	
	public Integer deleteAllActiveRtfCategoryticketsByEventId(Integer eventId,String reason)  throws Exception {
		String sql =" UPDATE tg set tg.status='DELETED',tg.last_updated=GETDATE(),reason='"+reason+"' " +
				" FROM rewardthefan_category_ticket tg  " +
			    " WHERE tg.status='ACTIVE' AND tg.event_id = "+eventId;
				//" AND (le.tn_broker_id > 0 OR le.vivid_broker_id > 0 OR le.scorebig_broker_id > 0)) ";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
}
