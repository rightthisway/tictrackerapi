package com.rtw.tmat.dao.implementation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.rtw.tmat.data.AdmitoneInventory;
import com.rtw.tracker.dao.implementation.HibernateDAO;


public class AdmitoneInventoryDAO extends HibernateDAO<Integer, AdmitoneInventory> implements com.rtw.tmat.dao.services.AdmitoneInventoryDAO {

	@Deprecated
	public Collection<AdmitoneInventory> getAllInventoryByDate(Date eventDate)  {
		return new ArrayList<AdmitoneInventory>();
		//return find("FROM AdmitoneInventory WHERE eventId=?", new Object[]{eventId});
	}
	
	public Collection<AdmitoneInventory> getAllInventoryByAdmitOneEventId(Integer eventId) throws Exception {
		return find("FROM AdmitoneInventory WHERE eventId=?", new Object[]{eventId});
	}

	public Collection<AdmitoneInventory> getAllInventoryByEventId(Integer eventId) throws Exception {
		return find("SELECT ae FROM AdmitoneInventory ae, Event e WHERE e.id=? AND e.admitoneId=ae.eventId", new Object[]{eventId});
	}

	
}
