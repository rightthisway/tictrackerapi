package com.rtw.tmat.dao.implementation;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.SQLQuery;

import com.rtw.tmat.data.CategoryMapping;
import com.rtw.tmat.enums.EventStatus;
import com.rtw.tracker.dao.implementation.HibernateDAO;



public class CategoryMappingDAO extends HibernateDAO<Integer, CategoryMapping> implements com.rtw.tmat.dao.services.CategoryMappingDAO {

	public List<CategoryMapping> getAllCategoryMappingsByVenueCategoryId(Integer venueCategoryId) {
		return find("FROM CategoryMapping WHERE categoryId IN (SELECT id FROM Category WHERE venueCategoryId = ? )", new Object[]{venueCategoryId});
	}

	public List<CategoryMapping> getAllCategoryMappingsByCategoryId(Integer categoryId) {
		return find("FROM CategoryMapping WHERE categoryId = ? ", new Object[]{categoryId}); 
	}
	@Override
	public List<String> getAllCategoryGroupNamesByTourIdByVenueCategoryId(Integer tourId,EventStatus eventStatus) {
		return find("select categoryGroup from VenueCategory WHERE id in (Select distinct venueCategoryId from Event where tourId = ? and eventStatus =? )", 
				new Object[]{tourId,eventStatus});
	}
	
	public List<String> getCategoryByEventIdAndZone(Integer eventId,String zone) {
		List<String> list = new ArrayList<String>();
		try {
			String sql = "select top 1 c.symbol,cm.start_section,cm.end_section,cm.row_range " +
					"from category_mapping_new cm with(nolock) " +
					"inner join category_new c with(nolock) on c.id=cm.category_id " +
					"where c.venue_category_id in (select venue_category_id from event with(nolock) where id="+eventId+") " +
					"and c.symbol like '"+zone+"' ";
			SQLQuery query = getSession().createSQLQuery(sql);
			List<Object[]> objects = query.list();
			if(!objects.isEmpty()){
				for(Object[] obj : objects){
					list.add((String)obj[0]);
					list.add((String)obj[1]);
					list.add((String)obj[2]);
					list.add((String)obj[3]);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	
}
