package com.rtw.tmat.dao.implementation;

import java.util.ArrayList;
import java.util.Collection;

import com.rtw.tmat.data.CategorySynonym;
import com.rtw.tracker.dao.implementation.HibernateDAO;



public class CategorySynonymDAO extends HibernateDAO<Integer, CategorySynonym> implements com.rtw.tmat.dao.services.CategorySynonymDAO {

	public Collection<CategorySynonym> getCategorySynonyms(String name) {
		Collection<CategorySynonym> list = find("FROM CategorySynonym WHERE lower(start_synonym)=? ", new Object[]{name.toLowerCase()});
		if (list.size() == 0) {
			return null;
		}
		return list;
	}

	public Collection<CategorySynonym> getSynonyms(Integer catId) {
		Collection<CategorySynonym> list = find("FROM CategorySynonym WHERE catId=? ", new Object[]{catId});
		if (list.size() == 0) {
			return null;
		}
		return list;
	}

	public Collection<CategorySynonym> getTourCategorySynonyms(Integer venueCategoryId) {
		
		//select start_synonym from category_synonym c, category a where a.tour_id = 4743 and a.id = c.cat_id;
		Collection<Object[]> list = find("SELECT s.id, s.catId, s.startSynonym, s.endSynonym FROM CategorySynonym s, Category c WHERE c.venueCategoryId=? AND s.catId = c.id", new Object[]{venueCategoryId});
		if (list.size() == 0) {
			return null;
		}
		Collection<CategorySynonym> syns = new ArrayList<CategorySynonym>();
		for(Object[] synElements : list){
			CategorySynonym catSyn = new CategorySynonym();
			catSyn.setId((Integer)synElements[0]);
			catSyn.setCatId((Integer)synElements[1]);
			catSyn.setStartSynonym((String)synElements[2]);
			if(synElements[3] != null){
				catSyn.setEndSynonym((String)synElements[3]);
			}
			syns.add(catSyn);
		}
		return syns;
	}
}
