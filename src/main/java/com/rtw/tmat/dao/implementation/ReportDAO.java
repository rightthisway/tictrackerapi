package com.rtw.tmat.dao.implementation;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.rtw.tmat.utils.common.SharedProperty;

public class ReportDAO {
	
	private static SessionFactory sessionFactory;
	private static Session staticSession = null;
	
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	
	public static Session getStaticSession() {
	   if(staticSession==null || !staticSession.isOpen() || !staticSession.isConnected()){
		   staticSession = getSessionFactory().openSession();
		   ReportDAO.setStaticSession(staticSession);
	   }
	   return staticSession;
	}

	public static void setStaticSession(Session staticSession) {
		ReportDAO.staticSession = staticSession;
	}
	
	
	
	public List<Object[]> getMothWiseUserRegistrationCount(String fromDate,String toDate,SharedProperty shared){
		Session session =null;
		List<Object[]> list = null;
		try{
			StringBuilder sb = new StringBuilder();
			sb.append("select DATENAME(MONTH,rc.contest_customer_signup_date) + ' '+ DATENAME(YEAR,rc.contest_customer_signup_date) as Month1 ");
			sb.append(", count(rc.contest_customer_id) as DandR, DATEPART(YEAR,rc.contest_customer_signup_date) ");
			sb.append(", DATEPART(MONTH,rc.contest_customer_signup_date) ");
			sb.append("from "+shared.getDatabasAlias()+"customer c with(nolock) ");
			sb.append("INNER JOIN contest_registered_customer rc with(nolock) on c.id = rc.contest_customer_id ");
			sb.append("where 1=1 ");
			if(fromDate!=null && !fromDate.isEmpty()){
				fromDate = fromDate + " 00:00:00.000";
				sb.append("and rc.contest_customer_signup_date >= '"+fromDate+"' ");
			}
			if(toDate!=null && !toDate.isEmpty()){
				toDate = toDate + " 23:59:00.000";
				sb.append("and rc.contest_customer_signup_date <= '"+toDate+"' ");
			}
			sb.append("GROUP BY DATEPART(YEAR,rc.contest_customer_signup_date) ");
			sb.append(", DATEPART(MONTH,rc.contest_customer_signup_date) ");
			sb.append(", DATENAME(MONTH,rc.contest_customer_signup_date) ");
			sb.append(", DATENAME(YEAR,rc.contest_customer_signup_date) ");
			sb.append("order by 3 desc, 4 desc;");
			
			session  = sessionFactory.openSession();
			Query query = session.createSQLQuery(sb.toString());
			list  = query.list();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return list;
	}
	
	
	public List<Object[]> getAvgTimeTakenPerContest(String fromDate,String toDate){
		Session session =null;
		List<Object[]> list = null;
		try{
			StringBuilder sb = new StringBuilder();
			sb.append(" select contestNo, REPLACE(contestName , '  ', '') as contestName, contestDate ");
			sb.append(" , CONVERT(DECIMAL(6,2), AVG(minsSpentInContest*1.00) ) as OverallAverageMinutes ");
			sb.append(" from rtf_rep_Contest_Reward_Dollar_Report  with(nolock) ");
			sb.append("where 1=1 ");
			sb.append("");
			sb.append("");
			if(fromDate!=null && !fromDate.isEmpty()){
				fromDate = fromDate + " 00:00:00.000";
				sb.append("and contestDate >= '"+fromDate+"' ");
			}
			if(toDate!=null && !toDate.isEmpty()){
				toDate = toDate + " 23:59:00.000";
				sb.append("and contestDate <= '"+toDate+"' ");
			}
			sb.append(" group by contestNo, contestName, contestDate order by contestDate;");
			
			session  = sessionFactory.openSession();
			Query query = session.createSQLQuery(sb.toString());
			list  = query.list();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return list;
	}
	
	
	
	public List<Object[]> getActiveInActiveUserCount(){
		Session session =null;
		List<Object[]> list = null;
		try{
			StringBuilder sb = new StringBuilder();
			sb.append("select month_year as month1,active_user_count as activeUsers");
			sb.append(",inactive_user_count as inActiveUsers,registered_user_count as RegisteredUsers ");
			sb.append("from rtf_rep_Active_InActive_User_Count_Report with(nolock) ");
			sb.append("order by yearNo desc, monthNo Desc;");
			
			session  = sessionFactory.openSession();
			Query query = session.createSQLQuery(sb.toString());
			list  = query.list();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return list;
	}
	
	
	public List<Object[]> getActiveUserCount(){
		Session session =null;
		List<Object[]> list = null;
		try{
			StringBuilder sb = new StringBuilder();
			sb.append("select month_year as month1,active_user_count as activeUsers");
			sb.append(",registered_user_count as registeredUsers ");
			sb.append("from rtf_rep_Active_InActive_User_Count_Report with(nolock)");
			sb.append("order by yearNo desc, monthNo Desc;");
			
			session  = sessionFactory.openSession();
			Query query = session.createSQLQuery(sb.toString());
			list  = query.list();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return list;
	}
	
	
	public List<Object[]> getInActiveUserCount(){
		Session session =null;
		List<Object[]> list = null;
		try{
			StringBuilder sb = new StringBuilder();
			sb.append("select month_year as month1,inactive_user_count as inActiveUsers");
			sb.append(",registered_user_count as registeredUsers ");
			sb.append("from rtf_rep_Active_InActive_User_Count_Report with(nolock)");
			sb.append("order by yearNo desc, monthNo Desc;");
			
			session  = sessionFactory.openSession();
			Query query = session.createSQLQuery(sb.toString());
			list  = query.list();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return list;
	}
	
	
	
	public List<Object[]> getFantasyContestUserCount(String fromDate,String toDate,SharedProperty shared){
		Session session =null;
		List<Object[]> list = null;
		try{
			StringBuilder sb = new StringBuilder();
			sb.append("select contestNo,contestName,contest_start_datetime ");
			sb.append(",reg.regCount,fromReg.fromRegCount,count(distinct customerUserID),tot.totParticipants ");
			sb.append("from ( select vw.customerNo,   vw.customerUserID, vw.customerSignupDate, vw.contestNo, REPLACE( ");
			sb.append("co.contest_name, '  ', '') as contestName ");
			sb.append(",co.contest_start_datetime, vw.contestQuestionsAttempted, vw.contestQuestionsPlayed ");
			sb.append("from rtf_rep_Contest_Reward_Dollar_Report vw with(nolock) ");
			sb.append("inner join contest_registered_customer crc with(nolock) on vw.customerNo = crc.contest_customer_id ");
			sb.append("inner join "+shared.getDatabasAlias()+"customer c with(nolock) on crc.contest_customer_id = c.id ");
			sb.append("inner join contest co with(nolock) on vw.contestNo = co.id ");
			sb.append("where 1=1 ");
			if(fromDate!=null && !fromDate.isEmpty()){
				fromDate = fromDate + " 00:00:00.000";
				sb.append("and contestDate >= convert(datetime, '"+fromDate+"') ");
			}
			if(toDate!=null && !toDate.isEmpty()){
				toDate = toDate + " 23:59:00.000";
				sb.append("and contestDate <= convert(datetime, '"+toDate+"') ");
			}
			sb.append("and co.contest_name like '%fantasy%' and co.status = 'EXPIRED' and co.is_test_contest = 0 ");
			sb.append("and datename(WEEKDAY,co.contest_start_datetime) in ('THURSDAY', 'SUNDAY') ");
			sb.append("and convert(varchar(8),co.contest_start_datetime,108) in ('19:00:00','12:00:00') ");
			sb.append("and convert(date, c.signup_date) >= (select min(convert(date, contest_start_datetime)) ");
			sb.append("from contest where 1=1 ");
			if(fromDate!=null && !fromDate.isEmpty()){
				sb.append("and contest_start_datetime >= convert(datetime, '"+fromDate+"') ");
			}
			if(toDate!=null && !toDate.isEmpty()){
				sb.append("and contest_start_datetime <= convert(datetime, '"+toDate+"') ");
			}
			sb.append("and contest_name like '%fantasy%' ");
			sb.append("and status = 'EXPIRED' and is_test_contest = 0 ");
			sb.append("and datename(WEEKDAY,contest_start_datetime) in ('THURSDAY', 'SUNDAY') ");
			sb.append("and convert(varchar(8),contest_start_datetime,108) in ('19:00:00','12:00:00'))) mst ");
			sb.append("outer apply ( select count(oc.id) as regCount ");
			sb.append("from contest_registered_customer cr with(nolock) ");
			sb.append("inner join "+shared.getDatabasAlias()+"customer oc with(nolock) on cr.contest_customer_id = oc.id ");
			sb.append("where convert(date,  oc.signup_date) = convert(date, mst.contest_start_datetime)) reg ");
			sb.append("outer apply ( select count(distinct oc.id) as fromRegCount ");
			sb.append("from contest_registered_customer cr with(nolock) ");
			sb.append("inner join "+shared.getDatabasAlias()+"customer oc with(nolock) on cr.contest_customer_id = oc.id ");
			sb.append("where convert(date,  oc.signup_date) between (select min(convert(date, contest_start_datetime)) ");
			sb.append("from contest ");
			sb.append("where contest_start_datetime >= convert(datetime,'2018-08-01 00:00:00.000' ) ");
			sb.append("and contest_name like '%fantasy%' ");
			sb.append("and status = 'EXPIRED' and is_test_contest = 0 ");
			sb.append("and datename(WEEKDAY,contest_start_datetime) in ('THURSDAY', 'SUNDAY') ");
			sb.append("and convert(varchar(8),contest_start_datetime,108) in ('19:00:00','12:00:00')) ");
			sb.append("and  convert(date, mst.contest_start_datetime)) fromReg ");
			sb.append("outer apply ( select count(distinct vw.customerNo) as totParticipants ");
			sb.append("from rtf_rep_Contest_Reward_Dollar_Report vw with(nolock) ");
			sb.append("inner join contest_registered_customer crc with(nolock) on vw.customerNo = crc.contest_customer_id ");
			sb.append("inner join "+shared.getDatabasAlias()+"customer c with(nolock) on crc.contest_customer_id = c.id ");
			sb.append("where vw.contestNo = mst.contestNo ) tot ");
			sb.append("group by mst.contestName, mst.contest_start_datetime, reg.regCount, tot.totParticipants,");
			sb.append("fromReg.fromRegCount, mst.contestNo ");
			sb.append("order by mst.contest_start_datetime ;");
			
			
			
			session  = sessionFactory.openSession();
			Query query = session.createSQLQuery(sb.toString());
			list  = query.list();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return list;
	}

}
