package com.rtw.tmat.dao.implementation;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.rtw.tmat.data.TicketListingCrawl;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.dao.implementation.HibernateDAO;

public class TicketListingCrawlDAO extends HibernateDAO<Integer, TicketListingCrawl> implements com.rtw.tmat.dao.services.TicketListingCrawlDAO {
	
	public List<TicketListingCrawl> getTicketListingCrawlByCreator(String username) {
		return find("FROM TicketListingCrawl WHERE creator=?", new Object[]{username});
	}

	public List<TicketListingCrawl> getAllTicketListingCrawlByEvent(Integer eventId) {
		return find("FROM TicketListingCrawl WHERE eventId=?", new Object[]{eventId});
	}
	
	public List<TicketListingCrawl> getAllActiveTicketListingCrawlByEvent(Integer eventId) {
		return find("FROM TicketListingCrawl WHERE eventId=? AND enabled=true", new Object[]{eventId});
	}

	public List<TicketListingCrawl> getAllActiveTicketListingCrawlByArtist(Integer artistId) {
		return find("FROM TicketListingCrawl tc WHERE eventId IN (SELECT id FROM Event WHERE artistId = ?) AND enabled=true", new Object[]{artistId});
	}
	
	public List<TicketListingCrawl> getAllTicketListingCrawlByArtist(Integer artistId) {
		return find("FROM TicketListingCrawl tc WHERE eventId IN (SELECT id FROM Event WHERE artistId = ?) ", new Object[]{artistId});
	}
	
	
	public List<TicketListingCrawl> getAllActiveTicketListingCrawlByVenue(Integer venueId) {
		return find("FROM TicketListingCrawl tc WHERE eventId IN (SELECT id FROM Event WHERE venueId = ?) ", new Object[]{venueId});
	}
	
	public List<TicketListingCrawl> getAllTicketListingCrawlByVenue(Integer venueId) {
		return find("FROM TicketListingCrawl tc WHERE eventId IN (SELECT id FROM Event WHERE venueId = ?) AND enabled=true", new Object[]{venueId});
	}
	public List<TicketListingCrawl> getAllActiveTicketListingCrawlByArtistAndVenue(Integer artistId,Integer venueId) {
		return find("FROM TicketListingCrawl tc WHERE eventId IN (SELECT id FROM Event WHERE artistId=? AND venueId = ?) AND enabled=true", new Object[]{artistId,venueId});
	}
	
	public List<TicketListingCrawl> getAllTicketListingCrawlByArtistAndVenue(Integer artistId,Integer venueId) {
		return find("FROM TicketListingCrawl tc WHERE eventId IN (SELECT id FROM Event WHERE artistId=? AND venueId = ?)", new Object[]{artistId,venueId});
	}
	public List<TicketListingCrawl> getTicketListingCrawlBySite(String siteId) {
		return find("FROM TicketListingCrawl WHERE siteId=? AND enabled=true", new Object[]{siteId});
	}
	
	public List<TicketListingCrawl> getTicketListingCrawlByArtistWithNoEvent(Integer artistId) {
		return find("FROM TicketListingCrawl WHERE eventId IN (SELECT id FROM Event WHERE artistId = ?) AND event_id=NULL", new Object[]{artistId});
	}

	public List<TicketListingCrawl> getTicketListingCrawlByLastUpdater(String username) {
		return find("FROM TicketListingCrawl WHERE lastUpdater=?", new Object[]{username});
	}
	
	public int getTicketListingCrawlCountByArtist(Integer artistId) {
		List list = find("SELECT count(id) FROM TicketListingCrawl WHERE eventId IN (SELECT id FROM Event WHERE artistId = ?) AND enabled=true", new Object[]{artistId});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
	
	public int getTicketListingCrawlCountByEvent(Integer eventId) {
		List list = find("SELECT count(id) FROM TicketListingCrawl WHERE eventId=? AND eventId IS NOT NULL AND enabled=true", new Object[]{eventId});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
	public int getTicketListingCrawlCountByEnabled() {
		List list = find("SELECT count(id) FROM TicketListingCrawl WHERE enabled=1");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
	
	//stubhub
	public int getStubhubSportsCrawlCount() {
		List list = find("SELECT count(*)  FROM TicketListingCrawl t,Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where t.eventId=e.id AND e.artistId = a.id AND a.grandChildTourCategoryId=gc.id AND gc.childTourCategory.id=cc.id AND cc.tourCategory.id=tc.id  AND t.enabled=1 AND tc.id=1 AND t.siteId='stubhub'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
	public int getStubhubConcertCrawlCount() {
		List list = find("SELECT count(*)  FROM TicketListingCrawl t,Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where t.eventId=e.id AND e.artistId = a.id AND a.grandChildTourCategoryId=gc.id AND gc.childTourCategory.id=cc.id AND cc.tourCategory.id=tc.id  AND t.enabled=1 AND tc.id=2 AND t.siteId='stubhub'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
	public int getStubhubDefaultCrawlCount() {
		List list = find("SELECT count(*)  FROM TicketListingCrawl t,Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where t.eventId=e.id AND e.artistId = a.id AND a.grandChildTourCategoryId=gc.id AND gc.childTourCategory.id=cc.id AND cc.tourCategory.id=tc.id  AND t.enabled=1 AND tc.id=3 AND t.siteId='stubhub'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
	public int getStubhubTheaterCrawlCount() {
		List list = find("SELECT count(*)  FROM TicketListingCrawl t,Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where t.eventId=e.id AND e.artistId = a.id AND a.grandChildTourCategoryId=gc.id AND gc.childTourCategory.id=cc.id AND cc.tourCategory.id=tc.id  AND t.enabled=1 AND tc.id=4 AND t.siteId='stubhub'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
	public int getStubhubLasVegasCrawlCount() {
		/*List list = find("SELECT count(*)  FROM TicketListingCrawl a,  Artist b,  GrandChildTourCategory c,  ChildTourCategory d,  TourCategory e where a.artistId=b.id and b.grandChildTourCategory=c.id and c.childTourCategory=d.id and d.tourCategory=e.id  and a.enabled=1 and e.id=5 and a.siteId='stubhub'");
		if (list == null || list.get(0) == null) {
			return 0;
		}*/
		return 0;// ((Long)list.get(0)).intValue();
	}
	public int getStubhubOtherCrawlCount() {
		/*List list = find("SELECT count(*)  FROM TicketListingCrawl a,  Artist b,  GrandChildTourCategory c,  ChildTourCategory d,  TourCategory e where a.artistId=b.id and b.grandChildTourCategory=c.id and c.childTourCategory=d.id and d.tourCategory=e.id  and a.enabled=1 and e.id=3 and a.siteId='stubhub'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();*/
		return getStubhubDefaultCrawlCount();
	}
	
	//TicketNetworkDirect
	public int getTndSportsCrawlCount() {
		List list = find("SELECT count(*)  FROM TicketListingCrawl t,Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where t.eventId=e.id AND e.artistId = a.id AND a.grandChildTourCategoryId=gc.id AND gc.childTourCategory.id=cc.id AND cc.tourCategory.id=tc.id  AND t.enabled=1 AND tc.id=1 AND t.siteId='ticketnetworkdirect'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
	public int getTndConcertCrawlCount() {
		List list = find("SELECT count(*)  FROM TicketListingCrawl t,Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where t.eventId=e.id AND e.artistId = a.id AND a.grandChildTourCategoryId=gc.id AND gc.childTourCategory.id=cc.id AND cc.tourCategory.id=tc.id  AND t.enabled=1 AND tc.id=2 AND t.siteId='ticketnetworkdirect'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
	public int getTndDefaultCrawlCount() {
		List list = find("SELECT count(*)  FROM TicketListingCrawl t,Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where t.eventId=e.id AND e.artistId = a.id AND a.grandChildTourCategoryId=gc.id AND gc.childTourCategory.id=cc.id AND cc.tourCategory.id=tc.id  AND t.enabled=1 AND tc.id=3 AND t.siteId='ticketnetworkdirect'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
	public int getTndTheaterCrawlCount() {
		List list = find("SELECT count(*)  FROM TicketListingCrawl t,Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where t.eventId=e.id AND e.artistId = a.id AND a.grandChildTourCategoryId=gc.id AND gc.childTourCategory.id=cc.id AND cc.tourCategory.id=tc.id  AND t.enabled=1 AND tc.id=4 AND t.siteId='ticketnetworkdirect'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
	public int getTndLasVegasCrawlCount() {
		/*List list = find("SELECT count(*)  FROM TicketListingCrawl a,  Artist b,  GrandChildTourCategory c,  ChildTourCategory d,  TourCategory e where a.artistId=b.id and b.grandChildTourCategory=c.id and c.childTourCategory=d.id and d.tourCategory=e.id  and a.enabled=1 and e.id=5 and a.siteId='ticketnetworkdirect'");
		if (list == null || list.get(0) == null) {
			return 0;
		}*/
		return 0;//((Long)list.get(0)).intValue();
	}
	public int getTndOtherCrawlCount() {
		/*List list = find("SELECT count(*)  FROM TicketListingCrawl a,  Artist b,  GrandChildTourCategory c,  ChildTourCategory d,  TourCategory e where a.artistId=b.id and b.grandChildTourCategory=c.id and c.childTourCategory=d.id and d.tourCategory=e.id  and a.enabled=1 and e.id=3 and a.siteId='ticketnetworkdirect'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();*/
		return getTndDefaultCrawlCount();
	}
	
	//Tnow
	public int getTnowSportsCrawlCount() {
		List list = find("SELECT count(*)  FROM TicketListingCrawl t,Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where t.eventId=e.id AND e.artistId = a.id AND a.grandChildTourCategoryId=gc.id AND gc.childTourCategory.id=cc.id AND cc.tourCategory.id=tc.id  AND t.enabled=1 AND tc.id=1 AND t.siteId='ticketsnow'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
	public int getTnowConcertCrawlCount() {
		List list = find("SELECT count(*)  FROM TicketListingCrawl t,Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where t.eventId=e.id AND e.artistId = a.id AND a.grandChildTourCategoryId=gc.id AND gc.childTourCategory.id=cc.id AND cc.tourCategory.id=tc.id  AND t.enabled=1 AND tc.id=2 AND t.siteId='ticketsnow'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
	public int getTnowDefaultCrawlCount() {
		List list = find("SELECT count(*)  FROM TicketListingCrawl t,Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where t.eventId=e.id AND e.artistId = a.id AND a.grandChildTourCategoryId=gc.id AND gc.childTourCategory.id=cc.id AND cc.tourCategory.id=tc.id  AND t.enabled=1 AND tc.id=3 AND t.siteId='ticketsnow'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
	public int getTnowTheaterCrawlCount() {
		List list = find("SELECT count(*)  FROM TicketListingCrawl t,Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where t.eventId=e.id AND e.artistId = a.id AND a.grandChildTourCategoryId=gc.id AND gc.childTourCategory.id=cc.id AND cc.tourCategory.id=tc.id  AND t.enabled=1 AND tc.id=4 AND t.siteId='ticketsnow'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
	public int getTnowLasVegasCrawlCount() {
		/*List list = find("SELECT count(*)  FROM TicketListingCrawl a,  Artist b,  GrandChildTourCategory c,  ChildTourCategory d,  TourCategory e where a.artistId=b.id and b.grandChildTourCategory=c.id and c.childTourCategory=d.id and d.tourCategory=e.id  and a.enabled=1 and e.id=5 and a.siteId='ticketsnow'");
		if (list == null || list.get(0) == null) {
			return 0;
		}*/
		return 0;// ((Long)list.get(0)).intValue();
	}
	public int getTnowOtherCrawlCount() {
		/*List list = find("SELECT count(*)  FROM TicketListingCrawl a,  Artist b,  GrandChildTourCategory c,  ChildTourCategory d,  TourCategory e where a.artistId=b.id and b.grandChildTourCategory=c.id and c.childTourCategory=d.id and d.tourCategory=e.id  and a.enabled=1 and e.id=3 and a.siteId='ticketsnow'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();*/
		return getTnowDefaultCrawlCount();
	}
	
	//TEvolution
	public int getTeSportsCrawlCount() {
		List list = find("SELECT count(*)  FROM TicketListingCrawl t,Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where t.eventId=e.id AND e.artistId = a.id AND a.grandChildTourCategoryId=gc.id AND gc.childTourCategory.id=cc.id AND cc.tourCategory.id=tc.id  AND t.enabled=1 AND tc.id=1 AND t.siteId='ticketevolution'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
	public int getTeConcertCrawlCount() {
		List list = find("SELECT count(*)  FROM TicketListingCrawl t,Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where t.eventId=e.id AND e.artistId = a.id AND a.grandChildTourCategoryId=gc.id AND gc.childTourCategory.id=cc.id AND cc.tourCategory.id=tc.id  AND t.enabled=1 AND tc.id=2 AND t.siteId='ticketevolution'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
	public int getTeDefaultCrawlCount() {
		List list = find("SELECT count(*)  FROM TicketListingCrawl t,Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where t.eventId=e.id AND e.artistId = a.id AND a.grandChildTourCategoryId=gc.id AND gc.childTourCategory.id=cc.id AND cc.tourCategory.id=tc.id  AND t.enabled=1 AND tc.id=3 AND t.siteId='ticketevolution'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
	public int getTeTheaterCrawlCount() {
		List list = find("SELECT count(*)  FROM TicketListingCrawl t,Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where t.eventId=e.id AND e.artistId = a.id AND a.grandChildTourCategoryId=gc.id AND gc.childTourCategory.id=cc.id AND cc.tourCategory.id=tc.id  AND t.enabled=1 AND tc.id=4 AND t.siteId='ticketevolution'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
	public int getTeLasVegasCrawlCount() {
//		List list = find("SELECT count(*)  FROM TicketListingCrawl a,  Artist b,  GrandChildTourCategory c,  ChildTourCategory d,  TourCategory e where a.artistId=b.id and b.grandChildTourCategory=c.id and c.childTourCategory=d.id and d.tourCategory=e.id  and a.enabled=1 and e.id=5 and a.siteId='ticketevolution'");
//		if (list == null || list.get(0) == null) {
			return 0;
//		}
//		return ((Long)list.get(0)).intValue();
	}
	public int getTeOtherCrawlCount() {
		/*List list = find("SELECT count(*)  FROM TicketListingCrawl a,  Artist b,  GrandChildTourCategory c,  ChildTourCategory d,  TourCategory e where a.artistId=b.id and b.grandChildTourCategory=c.id and c.childTourCategory=d.id and d.tourCategory=e.id  and a.enabled=1 and e.id=3 and a.siteId='ticketevolution'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();*/
		return getTeDefaultCrawlCount();
	}
	
		
	//Flash Seat
	public int getFsSportsCrawlCount() {
		List list = find("SELECT count(*)  FROM TicketListingCrawl t,Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where t.eventId=e.id AND e.artistId = a.id AND a.grandChildTourCategoryId=gc.id AND gc.childTourCategory.id=cc.id AND cc.tourCategory.id=tc.id  AND t.enabled=1 AND tc.id=1 AND t.siteId='flashseats'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
	public int getFsConcertCrawlCount() {
		List list = find("SELECT count(*)  FROM TicketListingCrawl t,Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where t.eventId=e.id AND e.artistId = a.id AND a.grandChildTourCategoryId=gc.id AND gc.childTourCategory.id=cc.id AND cc.tourCategory.id=tc.id  AND t.enabled=1 AND tc.id=2 AND t.siteId='flashseats'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
	public int getFsDefaultCrawlCount() {
		List list = find("SELECT count(*)  FROM TicketListingCrawl t,Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where t.eventId=e.id AND e.artistId = a.id AND a.grandChildTourCategoryId=gc.id AND gc.childTourCategory.id=cc.id AND cc.tourCategory.id=tc.id  AND t.enabled=1 AND tc.id=3 AND t.siteId='flashseats'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
	public int getFsTheaterCrawlCount() {
		List list = find("SELECT count(*)  FROM TicketListingCrawl t,Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where t.eventId=e.id AND e.artistId = a.id AND a.grandChildTourCategoryId=gc.id AND gc.childTourCategory.id=cc.id AND cc.tourCategory.id=tc.id  AND t.enabled=1 AND tc.id=4 AND t.siteId='flashseats'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
	
	public int getFsOtherCrawlCount() {
		/*List list = find("SELECT count(*)  FROM TicketListingCrawl a,  Artist b,  GrandChildTourCategory c,  ChildTourCategory d,  TourCategory e where a.artistId=b.id and b.grandChildTourCategory=c.id and c.childTourCategory=d.id and d.tourCategory=e.id  and a.enabled=1 and e.id=3 and a.siteId='ticketevolution'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();*/
		return getFsDefaultCrawlCount();
	}
	
	//Vivid  Seat
	public int getVsSportsCrawlCount() {
		List list = find("SELECT count(*)  FROM TicketListingCrawl t,Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where t.eventId=e.id AND e.artistId = a.id AND a.grandChildTourCategoryId=gc.id AND gc.childTourCategory.id=cc.id AND cc.tourCategory.id=tc.id  AND t.enabled=1 AND tc.id=1 AND t.siteId='vividseat'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
	public int getVsConcertCrawlCount() {
		List list = find("SELECT count(*)  FROM TicketListingCrawl t,Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where t.eventId=e.id AND e.artistId = a.id AND a.grandChildTourCategoryId=gc.id AND gc.childTourCategory.id=cc.id AND cc.tourCategory.id=tc.id  AND t.enabled=1 AND tc.id=2 AND t.siteId='vividseat'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
	public int getVsDefaultCrawlCount() {
		List list = find("SELECT count(*)  FROM TicketListingCrawl t,Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where t.eventId=e.id AND e.artistId = a.id AND a.grandChildTourCategoryId=gc.id AND gc.childTourCategory.id=cc.id AND cc.tourCategory.id=tc.id  AND t.enabled=1 AND tc.id=3 AND t.siteId='vividseat'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
	public int getVsTheaterCrawlCount() {
		List list = find("SELECT count(*)  FROM TicketListingCrawl t,Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where t.eventId=e.id AND e.artistId = a.id AND a.grandChildTourCategoryId=gc.id AND gc.childTourCategory.id=cc.id AND cc.tourCategory.id=tc.id  AND t.enabled=1 AND tc.id=4 AND t.siteId='vividseat'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
	
	public int getVsOtherCrawlCount() {
		/*List list = find("SELECT count(*)  FROM TicketListingCrawl a,  Artist b,  GrandChildTourCategory c,  ChildTourCategory d,  TourCategory e where a.artistId=b.id and b.grandChildTourCategory=c.id and c.childTourCategory=d.id and d.tourCategory=e.id  and a.enabled=1 and e.id=3 and a.siteId='ticketevolution'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();*/
		return getVsDefaultCrawlCount();
	}

	public boolean isQueryUrlAlreadyExist(String queryUrl,Integer crawlId) {
		
		List list = null;
		if(crawlId == null) {
			list = find("SELECT count(*)  FROM TicketListingCrawl  WHERE enabled=1 AND queryUrl=?",new Object[]{queryUrl});
		} else {
			list = find("SELECT count(*)  FROM TicketListingCrawl  WHERE enabled=1 AND queryUrl=? AND id<>?",new Object[]{queryUrl,crawlId});
		}
		
		if (list == null || list.get(0) == null) {
			return false;
		} else if(((Long)list.get(0)) > 0) {
			return true;
		} else {
			return false;	
		}
		
	}
	
	
	
	
	
	public void removeAllTicketListingCrawlByArtist(Integer artistId) {
		bulkUpdate("DELETE FROM TicketListingCrawl where artistId=?", new Object[]{artistId});
	}
		
	/*public void deleteUnusedTicketListingCrawl() {
		Collection<TicketListingCrawl> list = find("FROM TicketListingCrawl WHERE artistId NOT IN (SELECT id FROM Artist)");
		
		for (TicketListingCrawl crawl: list) {
			SpringUtil.getTicketListingCrawler().removeTicketListingCrawl(crawl);
			delete(crawl);
		}
	}
	
	 public void deleteById(Integer id) {    
		 //delete tickets
		 DAORegistry.getTicketDAO().deleteTicketsByCrawlId(id);
		 
		 super.deleteById(id);
	 }*/
	
	public void resetAllTicketListingCrawlEndCrawl() {
		bulkUpdate("UPDATE TicketListingCrawl SET end_crawl=null");
	}
	
	public List<TicketListingCrawl> getAllEnabled() {
		return find("FROM TicketListingCrawl WHERE enabled=true");
	}
	
	public List<TicketListingCrawl> getAllDisabled() {
		return find("FROM TicketListingCrawl WHERE enabled=false");
	}

	public void deleteByEventId(Integer eventId) {
		bulkUpdate("UPDATE TicketListingCrawl SET enabled = ?,end_crawl=?,lastUpdated=? WHERE eventId = ?",new Object[]{Boolean.FALSE,new Date(),new Date(),eventId});
	}
	public ArrayList<String> getAllCrawlerSiteName(){
		return (ArrayList<String>) find("SELECT DISTINCT(siteId) FROM TicketListingCrawl WHERE enabled = true");
	}
	
	/*
	public int getMaxId() {
		List list = find("SELECT max(id) FROM TicketListingCrawl");
		if (list == null) {
			return 0;
		}
		return (Integer)list.get(0);
	}
	@Override
	public void save(TicketListingCrawl entity) {
		entity.setId(getMaxId() + 1);
		super.save(entity);
	}
	*/
	
	public Collection<TicketListingCrawl> getAllTicketListingCrawlsBasedOnTicketsNow(Date currentDate) {
		DateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd");
		String currentDateStr =newFormat.format(currentDate);
		Collection<TicketListingCrawl> ticketListingCrawls = find("FROM TicketListingCrawl WHERE siteId != 'ticketsnow'");
		Collection<TicketListingCrawl> ticketsNowTicketListingCrawls = find("FROM TicketListingCrawl WHERE siteId = 'ticketsnow'");
		
		if(null != ticketsNowTicketListingCrawls && !ticketsNowTicketListingCrawls.isEmpty()){
			
			for (TicketListingCrawl ticketListingCrawl : ticketsNowTicketListingCrawls) {
				if(null != ticketListingCrawl.getEndCrawl() && !ticketListingCrawl.getEndCrawl().equals("")){
					
					String lastCrawlDateStr = newFormat.format(ticketListingCrawl.getEndCrawl());
					
					if(!lastCrawlDateStr.equalsIgnoreCase(currentDateStr)){
						ticketListingCrawls.add(ticketListingCrawl);
					}
				}else{
					ticketListingCrawls.add(ticketListingCrawl);
				}
			}
		}
		return  ticketListingCrawls;
		
	}

	@Override
	public void deleteUnusedTicketListingCrawl() {
		// TODO Auto-generated method stub
		
	}

}
