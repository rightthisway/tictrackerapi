package com.rtw.tmat.dao.implementation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.data.Category;
import com.rtw.tmat.data.SVGFileMissingZoneData;
import com.rtw.tmat.pojo.EventSectionZoneDetails;
import com.rtw.tmat.pojo.TNDEventSalesReportDetail;


public class QueryManagerDAO {
	private SessionFactory sessionFactory;
	private String tnInduxDbName;
	private String stubJktPos;
	private String tn2RotPos;
	private String tixCityPos;
	private String tixCityNewPos;
	private String manhattanPos;
	private String rtwPos;
	
	public String getManhattanPos() {
		return manhattanPos;
	}

	public void setManhattanPos(String manhattanPos) {
		this.manhattanPos = manhattanPos;
	}

	public String getTixCityNewPos() {
		return tixCityNewPos;
	}

	public void setTixCityNewPos(String tixCityNewPos) {
		this.tixCityNewPos = tixCityNewPos;
	}
	
	public String getRtwPos() {
		return rtwPos;
	}

	public void setRtwPos(String rtwPos) {
		this.rtwPos = rtwPos;
	}

	
	
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public  List<Category> getAllCategoriesByVenueCategoryId(Integer venueCategoryId){
		String sql=" SELECT c.id,c.symbol,c.description,c.group_name as groupName,c.equal_cats as equalCats,c.normalized_zone as normalizedZone,";
		sql += " c.seat_quantity as seatQuantity,c.category_capacity as categoryCapacity,c.venue_category_id as venueCategoryId FROM category_new c "; 
			sql += " LEFT JOIN category_mapping_new cm on cm.category_id = c.id ";
			sql += " LEFT JOIN venue_category vc on vc.id = c.venue_category_id "; 
			sql += " WHERE vc.id = " + venueCategoryId ;
			sql += " GROUP BY c.id,c.symbol,c.description,c.group_name,c.equal_cats,c.normalized_zone, "; 
			sql += " c.seat_quantity,c.category_capacity,c.venue_category_id "; 
			sql += " ORDER BY min(cm.id) "; 

		Session session=null;
		List<Category> list = new ArrayList<Category>();
		try{
			session=sessionFactory.openSession();
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			list = sqlQuery.setResultTransformer(Transformers.aliasToBean(Category.class)).list();
		}catch (Exception e) {
			// TODO: handle exception
			return null;
		}finally{
			session.close();
		}
		return list;
	}

	
	public  List<Category> getAllCategoriesByVenueIdAndCategoryGroup(Integer venueId, String categoryGroup){
		String sql=" SELECT c.id,c.symbol,c.description,c.group_name as groupName,c.equal_cats as equalCats,c.normalized_zone as normalizedZone,";
			sql += " c.seat_quantity as seatQuantity,c.category_capacity as categoryCapacity,c.venue_category_id as venueCategoryId FROM category_new c "; 
			sql += " INNER JOIN category_mapping_new cm on cm.category_id = c.id ";
			sql += " INNER JOIN venue_category vc on vc.id = c.venue_category_id "; 
			sql += " WHERE vc.venue_id = " + venueId ;
			sql += " AND vc.category_group = '" + categoryGroup  + "'";
			sql += " GROUP BY c.id,c.symbol,c.description,c.group_name,c.equal_cats,c.normalized_zone, "; 
			sql += " c.seat_quantity,c.category_capacity,c.venue_category_id "; 
			sql += " ORDER BY min(cm.id) "; 

		Session session=null;
		List<Category> list = new ArrayList<Category>();
		try{
			session=sessionFactory.openSession();
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			list = sqlQuery.setResultTransformer(Transformers.aliasToBean(Category.class)).list();
		}catch (Exception e) {
			// TODO: handle exception
			return null;
		}finally{
			session.close();
		}
		return list;
	}
	public Integer getEventIdFromSeatGeekUploadCatsBySeatGeekTicketId(Integer ticketId) throws Exception {
		String sql = "select event_id from seatgeek_upload_cats with(nolock) where cat_ticket_id="+ticketId;
		Session session=null;
		Integer eventId= null;
		try{
			session=sessionFactory.openSession();
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			List<Object> objList = sqlQuery.list();
			if(objList != null && !objList.isEmpty()) {
				eventId = (Integer)(objList.get(0));
				//System.out.println("..."+objList.get(0)[0]);
				//eventId = (Integer)(resultRow[0]);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return eventId;
	}

	public List<TNDEventSalesReportDetail> getTNDEventSalesDetailReport() throws Exception {
		

		String sql = "select  e.id as eventId,e.name as eventName,CONVERT(VARCHAR(19),e.event_date,101) as eventDateStr," +
				" CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) WHEN e.event_time is null THEN 'TBD' END as eventTimeStr," +
				" v.building as venue,v.city as city,v.state as state,v.country as country,tnd.salesCount as salesCount," +
				" CONVERT(VARCHAR(19),tnd.latest_order_date,101)+' '+LTRIM(RIGHT(CONVERT(VARCHAR(20), tnd.latest_order_date, 100), 7)) as recentSaleDateStr" +
				" from event e with(nolock) " +
				" inner join venue v with(nolock) on v.id=e.venue_id" +
				" inner join (select count(*) as salesCount,event_id,max(order_date) as latest_order_date from tnd_sales_autopricing with(nolock) where" +
				" order_date>convert(date,(DATEADD(dd, -2, getdate()))) and accept_sub_status_desc='Accepted'" +
				" group by event_id) as tnd on tnd.event_id=e.admitone_id" +
				" where e.event_date>getdate()" +
				" order by tnd.latest_order_date desc";
		
		Session session=null;
		List<TNDEventSalesReportDetail> list = new ArrayList<TNDEventSalesReportDetail>();
		try{
			session=sessionFactory.openSession();
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			sqlQuery.addScalar("eventId",Hibernate.INTEGER);
			sqlQuery.addScalar("eventName",Hibernate.STRING);
			sqlQuery.addScalar("eventDateStr",Hibernate.STRING);
			sqlQuery.addScalar("eventTimeStr",Hibernate.STRING);
			sqlQuery.addScalar("venue",Hibernate.STRING);
			sqlQuery.addScalar("city",Hibernate.STRING);
			sqlQuery.addScalar("state",Hibernate.STRING);
			sqlQuery.addScalar("country",Hibernate.STRING);
			sqlQuery.addScalar("salesCount",Hibernate.INTEGER);
			sqlQuery.addScalar("recentSaleDateStr",Hibernate.STRING);
			list = sqlQuery.setResultTransformer(Transformers.aliasToBean(TNDEventSalesReportDetail.class)).list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return list;
	}
	public String getAutocats96CategoryTicketZoneByTnCategoryTicketGroupId(Integer eventId,Integer tnCategoryTicketGroupId,Integer brokerId) throws Exception {
		String sql = "select tmat_zone from autocats96_category_ticket where event_id="+eventId+" and tn_category_ticket_group_id="+tnCategoryTicketGroupId+" and broker_id="+brokerId;
		Session session=null;
		String zone= null;
		try{
			session=sessionFactory.openSession();
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			List<Object> objList = sqlQuery.list();
			if(objList != null && !objList.isEmpty()) {
				zone = (String)(objList.get(0));
				//System.out.println("..."+objList.get(0)[0]);
				//eventId = (Integer)(resultRow[0]);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return zone;
	}
	
	public String getLarryLastCategoryTicketZoneByTnTicketGroupId(Integer eventId,Integer tnTicketGroupId,Integer brokerId) throws Exception {
		String sql = "select tmat_zone from larrylast_category_ticket with(nolock) where event_id="+eventId+" and tn_ticket_group_id="+tnTicketGroupId+" and tn_broker_id="+brokerId;
		Session session=null;
		String zone= null;
		try{
			session=sessionFactory.openSession();
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			List<Object> objList = sqlQuery.list();
			if(objList != null && !objList.isEmpty()) {
				zone = (String)(objList.get(0));
				//System.out.println("..."+objList.get(0)[0]);
				//eventId = (Integer)(resultRow[0]);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return zone;
	}
	public String getZoneLastRowCategoryTicketZoneByTnTicketGroupId(Integer eventId,Integer tnTicketGroupId,Integer brokerId) throws Exception {
		String sql = "select tmat_zone from zone_lastrow_minicat_category_ticket with(nolock) where event_id="+eventId+" and tn_ticket_group_id="+tnTicketGroupId+" and tn_broker_id="+brokerId;
		Session session=null;
		String zone= null;
		try{
			session=sessionFactory.openSession();
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			List<Object> objList = sqlQuery.list();
			if(objList != null && !objList.isEmpty()) {
				zone = (String)(objList.get(0));
				//System.out.println("..."+objList.get(0)[0]);
				//eventId = (Integer)(resultRow[0]);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return zone;
	}
	public List<EventSectionZoneDetails> getAllAutopricingProductsSectionAndTmatZone() throws Exception {
		String sql = " select distinct event_id as eventId,section as section,tmat_zone as zone from autocats96_category_ticket tg with(nolock) where status='ACTIVE'" +
				" union all" +
				" select distinct event_id as eventId,section as section,tmat_zone as zone from larrylast_category_ticket tg with(nolock) where status='ACTIVE'" +
				" union all" +
				" select distinct event_id as eventId,section as section,tmat_zone as zone from zone_lastrow_minicat_category_ticket tg with(nolock) where status='ACTIVE'" +
				" union all" +
				" select distinct event_id as eventId,section as section,tmat_zone as zone from zone_tickets_category_ticket_group tg with(nolock) where status='ACTIVE'";
		Session session=null;
		List<EventSectionZoneDetails> eventSectionZoneList = new ArrayList<EventSectionZoneDetails>();
		try{
			session=sessionFactory.openSession();
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			sqlQuery.addScalar("eventId",Hibernate.INTEGER);
			sqlQuery.addScalar("section",Hibernate.STRING);
			sqlQuery.addScalar("zone",Hibernate.STRING);
			eventSectionZoneList =  sqlQuery.setResultTransformer(Transformers.aliasToBean(EventSectionZoneDetails.class)).list();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return eventSectionZoneList;
	}
	
	public List<String> getAllZoneByVenueCategory(Integer venueCatId) throws Exception{
		
		String sql = "select distinct c.symbol from category_mapping_new cm with(nolock) inner join category_new c with(nolock) on cm.category_id=c.id " +
				"where c.venue_category_id="+venueCatId;
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		Session session=null;
		List<String> objList = null;
		try {
			session=sessionFactory.openSession();
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			objList = sqlQuery.list();
			
			/*while(objList != null){
				for (String zone : objList) {
					map.put(zone.toUpperCase(), true);
				}
			}
			return map;*/
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return objList;
	}
	
	public List<SVGFileMissingZoneData> getAllVenueMapZonesNotInCSV() throws Exception {		
		Session session=null;
		List<SVGFileMissingZoneData> missingVenueMapZonesList = new ArrayList<SVGFileMissingZoneData>();
		try{
			String sql = "select distinct v.id as venueId,v.building as building,v.city as city,v.state as state,v.country as country," +
				" vc.id venueCategoryId,vc.category_group as categoryGroupName,vmz.zone as zone from venue_map_zones vmz with(nolock) " +
				" inner join venue_category vc with(nolock) on vc.id=vmz.venue_category_id" +
				" inner join venue v with(nolock) on v.id=vc.venue_id" +
				" where v.country in ('US','CA')" +
				" and not exists (select * from category_new cc with(nolock) where cc.venue_category_id=vmz.venue_category_id and cc.symbol=vmz.zone)" +
				" order by building,city,state,country,category_group";
			
			session = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			missingVenueMapZonesList =  sqlQuery.setResultTransformer(Transformers.aliasToBean(SVGFileMissingZoneData.class)).list();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return missingVenueMapZonesList;
	}
	
	public List<SVGFileMissingZoneData> getAllCsvZonesNotInVenueMap() throws Exception {		
		Session session=null;
		List<SVGFileMissingZoneData> missingVenueMapZonesList = new ArrayList<SVGFileMissingZoneData>();
		try{
			String sql = "select distinct v.id as venueId,v.building as building,v.city as city,v.state as state,v.country as country," +
				" vc.id venueCategoryId,vc.category_group as categoryGroupName,cc.symbol as zone from category_new cc with(nolock) " +
				" inner join venue_category vc with(nolock) on vc.id=cc.venue_category_id" +
				" inner join venue v with(nolock) on v.id=vc.venue_id" +
				" where v.country in ('US','CA') and vc.venue_map=1" +
				" and not exists (select * from venue_map_zones vmz with(nolock) where cc.venue_category_id=vmz.venue_category_id and cc.symbol=vmz.zone)" +
				" order by building,city,state,country,category_group";
				
			session = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			missingVenueMapZonesList =  sqlQuery.setResultTransformer(Transformers.aliasToBean(SVGFileMissingZoneData.class)).list();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return missingVenueMapZonesList;
	}
	
	public List<Object[]> getAllVenueMapZonesWihoutListingsinRTF(String artistName,String startDateStr,String endDateStr) throws Exception {
		Session session=null;
		List<Object[]> objList = null;
		try{
			String sql = "select distinct e.id as EventId,e.name as Event," +
					" CASE WHEN e.event_time is not null then CONVERT(VARCHAR(19),e.event_date,101) else 'TBD' end as EventDate," +
					" CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7))" +
					" WHEN e.event_time is null THEN 'TBD' END as EventTime,a.name as artistName," +
					" v.building as Venue,v.city as City,v.state as State,v.country as Country,vmz.zone as mapZone," +//vc.category_group as venueCategory,
					" e.event_date,e.event_time" +
					" from event e with(nolock) " +
					" inner join artist a with(nolock) on a.id=e.artist_id" +
					" inner join zone_tickets_processor_exchange_event tn with(nolock) on tn.event_id=e.id" +
					" inner join venue_Category vc with(nolock) on vc.id=e.venue_Category_id" +
					" inner join venue v with(nolock) on v.id=e.venue_id" +
					" inner join venue_map_zones vmz with(nolock) on vmz.venue_category_id=e.venue_category_id" +
					" where e.event_status='ACTIVE' and tn.status='ACTIVE'  and v.country in ('US','CA') and vc.venue_map=1 and" +
					" not exists (select * from zone_tickets_category_ticket_group tg with(nolock) where tg.event_id=e.id and status='ACTIVE' and tg.section=vmz.zone and tg.zone_tickets_ticket_group_id is not null)";
			
			if(startDateStr != null) {
				sql = sql + " and e.event_date >='"+startDateStr+"'";
			}
			if(endDateStr != null) {
				sql = sql + " and e.event_date <='"+endDateStr+"'";
			}
			if(artistName != null) {
				sql = sql + " and (e.name like '%"+artistName+"%' or a.name like '%"+artistName+"%')";
			}
			sql = sql + " order by e.event_date,e.event_time,e.name,e.id,zone";			
			
			session = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			objList = sqlQuery.list();			
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return objList;
	}
}
