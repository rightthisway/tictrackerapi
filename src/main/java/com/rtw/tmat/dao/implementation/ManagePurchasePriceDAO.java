package com.rtw.tmat.dao.implementation;


import java.util.List;

import org.hibernate.Query;

import com.rtw.tmat.data.ManagePurchasePrice;
import com.rtw.tracker.dao.implementation.HibernateDAO;



public class ManagePurchasePriceDAO extends HibernateDAO<Integer, ManagePurchasePrice> implements com.rtw.tmat.dao.services.ManagePurchasePriceDAO {

	
	public List<Integer> getDistinctArtistIds() {
		Query query = getSession().createQuery("select distinct(event.artistId) from ManagePurchasePrice");
		   
		  @SuppressWarnings("unchecked")
		  List<Integer> list= query.list();
		  return list;
	}

	
	public List<ManagePurchasePrice> getAllByArtistIds(List<Integer> ids) {
		Query query = getSession().createQuery("FROM ManagePurchasePrice where event.artistId in (:Ids) order by event.artistId").setParameterList("Ids", ids);
		   
		  @SuppressWarnings("unchecked")
		  List<ManagePurchasePrice> list= query.list();
		  return list;
	}

	
	/*public Collection<Artist> getDistinctArtists() {
		Query query = getSession().createQuery("select distinct(artist) from ManagePurchasePrice");
		   
		  @SuppressWarnings("unchecked")
		  List<Artist> list= query.list();
		  return list;
	}*/

	
	public List<ManagePurchasePrice> getAllByArtistExchangeTicketTypes(
			List<Integer> ids, List<String> exchangeIds,
			List<String> ticketTypeIds) {
		Query query = getSession().createQuery("FROM ManagePurchasePrice where event.id in (:Ids) and exchange in (:exchanges) and ticketType in (:tickettypes)  order by event.artistId").setParameterList("Ids", ids).setParameterList("exchanges", exchangeIds).setParameterList("tickettypes", ticketTypeIds);
		   
		  @SuppressWarnings("unchecked")
		  List<ManagePurchasePrice> list= query.list();
		  return list;
	}

	
	public List<ManagePurchasePrice> getAllByManagePurchasePriceByArtistId(Integer id) {
		return find("FROM ManagePurchasePrice WHERE event.artistId= ? ", new Object[]{id});
	}
	
	public List<ManagePurchasePrice> getAllManagePurchasePriceByEventId(Integer eventId) {
		return find("FROM ManagePurchasePrice WHERE event.id= ? ", new Object[]{eventId});
	}

}
