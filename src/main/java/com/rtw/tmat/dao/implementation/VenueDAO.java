package com.rtw.tmat.dao.implementation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.rtw.tmat.data.Venue;
import com.rtw.tracker.dao.implementation.HibernateDAO;



public class VenueDAO extends HibernateDAO<Integer, Venue> implements com.rtw.tmat.dao.services.VenueDAO {
	public Collection<Venue> getAll() {
		return find("FROM Venue ORDER BY building ASC, id ASC");
	}
	
//	public void deleteById(Integer venueId) {
////		for (Event event: DAORegistry.getEventDAO().getAllEventsByVenue(venueId)) {
////			DAORegistry.getEventDAO().deleteById(event.getId());
////		}
//		super.deleteById(venueId);
//	}
	
	public Collection<Venue> getAllVenuesForUSandCA(Date startDate,Date endDate) {
    	return find("SELECT DISTINCT v FROM Venue v,Event e WHERE e.venueId=v.id and (e.localDate >= ? and e.localDate <= ?) and v.country in ('US','USA','CA') ORDER BY v.building ASC",new Object[]{startDate,endDate});
    }
	
	public Collection<Venue> getVenues(String name) {
		Collection<Venue> list = find("FROM Venue WHERE lower(building)=? ", new Object[]{name.toLowerCase()});
		if (list.size() == 0) {
			return null;
		}
		
		return list;
	}
	
	public Venue getVenue(String name) {
		Collection<Venue> list = find("FROM Venue WHERE lower(building)=? ", new Object[]{name.toLowerCase()});
		if (list.size() == 0) {
			return null;
		}
		
		return list.iterator().next();
	}
	
	public Collection<Venue> getVenueById(Integer id){
		return find("FROM Venue WHERE id=?",new Object[]{id});
	}
	
	public Collection<Venue> filterByVenue(String pattern) {
    	return find("FROM Venue WHERE building LIKE '%" + pattern + "%' ORDER BY building ASC");
    }
	
	public Collection<Venue> filterVenue(String venue,String city, String state, String country, String venueType) {
    	String hql = "FROM Venue WHERE ";
    	boolean flag = false;
    	if(!venue.isEmpty()){
    		hql += " building LIKE '%"+ venue + "%'";
    		flag = true;
    	}
    	if(!city.isEmpty()){
    		if(flag){
    			hql += " AND city LIKE '%"+city+"%'";
    		}else{
    			hql += " city LIKE '%"+city+"%'";
    			flag=true;
    		}
    	}
    	if(!state.isEmpty()){
    		if(flag){
    			hql += " AND state LIKE '%"+state+"%'";
    		}else{
    			hql += " state LIKE '%"+state+"%'";
    			flag=true;
    		}	
    	}
    	if(!country.isEmpty()){
    		if(flag){
    			hql += " AND country LIKE '%"+country+"%'";
    		}else{
    			hql += " country LIKE '%"+country+"%'";
    			flag=true;
    		}	
    	}
    	if(!venueType.isEmpty()){
    		if(venueType.toUpperCase().contains("SPORT") || venueType.toUpperCase().contains("CONCERT")){  //SPORT-CONCERT
    			if(flag){
	    			hql += " AND (venueType LIKE '%SPORT%' OR venueType LIKE '%CONCERT%')";
	    		}else{
	    			hql += " (venueType LIKE '%SPORT%' OR venueType LIKE '%CONCERT%')";
	    		}
    		}else {
    			if(flag){
	    			hql += " AND venueType LIKE '%"+venueType+"%'";
	    		}else{
	    			hql += " venueType LIKE '%"+venueType+"%'";
	    		}
    		}
    	}
		return find(hql);
    }
	
	/**
	 *  Commented by chirag -  due to map issue  
	 *  
	 */

	public void deleteEmptyVenues() {
	/*	for (Venue venue: (Collection<Venue>)find("FROM Venue WHERE id NOT IN (SELECT venueId FROM Event)")) {
			DAORegistry.getVenueDAO().delete(venue);
			System.out.println("deleting venue " + venue.getId() + " " + venue.getBuilding() + " " +venue.getCity());
		}
	}
	public void delete(Venue venue) {
		deleteById(venue.getId());
	 	*/
	}

	public Venue getVenueByStubhubId(Integer stubhubId) {
    	Collection<Venue> venues = find("FROM Venue WHERE stubhubId=?", new Object[]{stubhubId});
    	if (venues.size() == 0) {
    		return null;
    	}
    	
    	return venues.iterator().next();
	}
	
	public Collection<Venue> getVenueByTour(Integer tourId) {
		return find("SELECT DISTINCT v FROM Venue v,Event e WHERE v.id = e.venueId AND e.tourId = ? ",new Object[]{tourId});
	}
	
	public void deleteVenue(Integer venueId){
		
		Session session = getSession();
		
		try{
			Query query = session.createQuery("SELECT id FROM VenueCategory WHERE venue.id = :venueId").setParameter("venueId", venueId);
			List<Integer> venueCategoryIds = query.list();
			if(venueCategoryIds != null && !venueCategoryIds.isEmpty()){
				TMATDAORegistry.getVenueCategoryDAO().deleteVenueCategory(venueCategoryIds);
			}
			query = session.createQuery("DELETE FROM Venue WHERE id = :venueId").setParameter("venueId", venueId);
			query.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		
	}
	
}
