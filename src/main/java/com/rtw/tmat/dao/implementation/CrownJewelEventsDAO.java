package com.rtw.tmat.dao.implementation;

import java.util.List;

import com.rtw.tmat.data.CrownJewelEvents;
import com.rtw.tracker.dao.implementation.HibernateDAO;

public class CrownJewelEventsDAO extends HibernateDAO<Integer, CrownJewelEvents> implements com.rtw.tmat.dao.services.CrownJewelEventsDAO{

	public CrownJewelEvents getCrownJewelEventByEventId(Integer eventId) throws Exception{
		return findSingle("FROM CrownJewelEvents where eventId=?", new Object[] {eventId});
	}
	public List<CrownJewelEvents> getCrownJewelEventByParentType(String parentType) throws Exception{
		return find("FROM CrownJewelEvents where parentType=?", new Object[] {parentType});
	}
	@Override
	public List<CrownJewelEvents> getCrownJewelEventByEventIds(List<Integer> eventIds) throws Exception {
		return getHibernateTemplate().findByNamedParam("FROM CrownJewelEvents where eventId in(:ids)", "ids", eventIds);
	}
}
