package com.rtw.tmat.dao.implementation;

import java.util.List;

import com.rtw.tmat.data.SoldCategoryTicket;
import com.rtw.tracker.dao.implementation.HibernateDAO;


public class SoldCategoryTicketDAO extends HibernateDAO<Integer, SoldCategoryTicket> implements com.rtw.tmat.dao.services.SoldCategoryTicketDAO {

	public List<SoldCategoryTicket> getAllSoldCategoryTicketsByEventId(Integer eventId) throws Exception {
		return find("FROM SoldCategoryTicket where eventId=?" ,new Object[]{eventId});
	}
}
