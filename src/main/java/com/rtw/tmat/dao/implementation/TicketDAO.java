package com.rtw.tmat.dao.implementation;

import java.util.Collection;
import java.util.List;

import com.rtw.tmat.data.Ticket;
import com.rtw.tracker.dao.implementation.HibernateDAO;
import com.rtw.tracker.datas.TicketStatus;



public class TicketDAO extends HibernateDAO<Integer, Ticket> implements com.rtw.tmat.dao.services.TicketDAO {
	
	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllActiveTicketsByEvent(Integer eventId) {
		// return all the tickets which are expired and which were updated before their expired
		return find("FROM Ticket WHERE eventId = ? AND ticketStatus = ?", new Object[] {eventId, TicketStatus.ACTIVE});		
	}
	
	@SuppressWarnings("unchecked")
	public int getTicketCountByEvent(int event_id, TicketStatus status) {
		List list = find("SELECT count(id) FROM Ticket WHERE eventId = ? AND ticketStatus=?", new Object[]{event_id, status});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
	
	@SuppressWarnings("unchecked")
	public int getTicketQuantitySumByEvent(int event_id, TicketStatus status) {
		List list = find("SELECT sum(remainingQuantity) FROM Ticket WHERE eventId = ? AND ticketStatus=?", new Object[]{event_id, status});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
	
	public Collection<Ticket> getAllActiveTicketsByNormalizedSectionAndEventId(Integer eventId, String section){
		return find("FROM Ticket WHERE eventId = ? AND ticketStatus = ? And  normalizedSection = ?" 
				, new Object[]{eventId, TicketStatus.ACTIVE, section});
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllActiveTicketForAutoCatsByEventId(Integer eventId,Boolean isTNCrawlOnly,Boolean isSkipVividCrawls)  throws Exception {
		//Tamil : In autopricing projects we should not consider tickets with price morethan $100000
		String query = "FROM Ticket WHERE eventId = ? AND ticketStatus =? " +//AND id not in (SELECT ticketId FROM SoldCategoryTicket)
					" and ticketType = 'Regular' and currentPrice < 100000 ";
		if(isTNCrawlOnly) {
			query = query + " AND siteId in ('ticketnetworkdirect') ";
		} else {
			if(isSkipVividCrawls) {
				query = query + " AND siteId in ('ticketnetworkdirect','ticketevolution','stubhub','flashseats') ";
			} else {
				query = query + " AND siteId in ('ticketnetworkdirect','ticketevolution','stubhub','flashseats') ";					
			}
			//query = query + " AND ticketListingCrawlId not in (select crawlId from StubHubApiTracking where crawlId is not null and status='ACTIVE')";
		}
		return find(query, new Object[]{eventId,TicketStatus.ACTIVE});
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllActiveInstantTicketForAutoCatByEventId(Integer eventId,Boolean isTNCrawlOnly,Boolean isSkipVividCrawls)  throws Exception {
		//Tamil : In autopricing projects we should not consider tickets with price morethan $100000
		
		String query = "FROM Ticket WHERE eventId = ? AND ticketStatus =? " +//AND id not in (SELECT ticketId FROM SoldCategoryTicket)
		" and ticketType = 'Regular' and currentPrice < 100000 AND ticketDeliveryType is not null and ticketDeliveryType not in('MERCURY') ";
		if(isTNCrawlOnly) {
			query = query + " AND siteId in ('ticketnetworkdirect') ";
		} else {
			if(isSkipVividCrawls) {
				query = query + " AND siteId in ('ticketnetworkdirect','ticketevolution','stubhub','flashseats') ";
			} else {
				query = query + " AND siteId in ('ticketnetworkdirect','ticketevolution','stubhub','flashseats') ";					
			}
			//query = query + " AND ticketListingCrawlId not in (select crawlId from StubHubApiTracking where crawlId is not null and status='ACTIVE')";
		}
		return find(query, new Object[]{eventId,TicketStatus.ACTIVE});
	}
	
}