package com.rtw.tmat.dao.implementation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.rtw.tmat.data.Category;
import com.rtw.tmat.enums.EventStatus;
import com.rtw.tracker.dao.implementation.HibernateDAO;



public class CategoryDAO extends HibernateDAO<Integer, Category> implements com.rtw.tmat.dao.services.CategoryDAO {

	public void deleteByVenueCategoryId(Integer venueCategoryId) {
		bulkUpdate("DELETE FROM CategoryMapping WHERE categoryId in (SELECT id from Category WHERE venueCategoryId = ?)" , new Object[]{venueCategoryId});
//		bulkUpdate("DELETE FROM Category WHERE venueCategoryId = ? " , new Object[]{venueCategoryId});
	}
	
	
	 public Collection<Category> getAllCategoriesByVenueCategoryId(Integer venueCategoryId) {
//		return find("FROM Category WHERE venueCategoryId=?", new Object[]{venueCategoryId});
		return TMATDAORegistry.getQueryManagerDAO().getAllCategoriesByVenueCategoryId(venueCategoryId);
	}
	 
	public Collection<Category> getAllCategoriesByVenueIdAndCategoryGroup(Integer venueId, String categoryGroup){
		
		Session session = getSession();
		Collection<Category> category = null;
		try{
//			Query query = session.createQuery("FROM Category WHERE venueCategoryId in (SELECT id FROM VenueCategory WHERE venue.id = :venueId AND categoryGroup = :categoryGroup)").setParameter("venueId", venueId).setParameter("categoryGroup", categoryGroup);
//			category = query.list();
			return TMATDAORegistry.getQueryManagerDAO().getAllCategoriesByVenueIdAndCategoryGroup(venueId, categoryGroup);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		return category;
	}

	public Category getCategoryByVenueCategoryIdAndCategorySymbol(Integer venueCategoryId, String categorySymbol) {
		return findSingle("FROM Category WHERE venueCategoryId = ? AND symbol = ?)", new Object[]{venueCategoryId,categorySymbol});
	}
	
	public String getCategorieGroupByVenueCategoryId(Integer venueCategoryId) {
		
		Session session = getSession();
		List<String> groupName = null;
		
		try{
			Query query = session.createQuery("SELECT distinct groupName FROM Category WHERE venueCategoryId = :venueCategoryId").setParameter("venueCategoryId", venueCategoryId);
			groupName = query.list();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return (groupName != null && groupName.size() > 0 ? (String) groupName.get(0) : null);
	}
	
	public List<String> getCategorySymbolsByVenueCategoryId(Integer venueCategoryId) throws Exception {
		
		Session session = getSession();
		List<String> groupName = new ArrayList<String>();
		
		try{
			Query query = session.createQuery("SELECT distinct symbol FROM Category WHERE venueCategoryId = :venueCategoryId order by symbol").setParameter("venueCategoryId", venueCategoryId);
			groupName = query.list();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return groupName;
	}
	
	public Collection<Category> getAllCategoriesByVenuCategoryIdsByArtistId(Integer artistId,EventStatus eventSataus) {
		
		return find("FROM Category WHERE venueCategoryId in (Select distinct venueCategoryId from Event eve where artistId = ? and eventStatus =? )", 
				new Object[]{artistId,eventSataus});
	}

	@Override
	public int countByVenueCategoryID(Integer venueCategoryId) {
		List list = find("select count(*) FROM CategoryMapping WHERE categoryId in (SELECT id from Category WHERE venueCategoryId = ?)" , new Object[]{venueCategoryId});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}
}
