package com.rtw.tmat.dao.implementation;


import java.util.List;

import com.rtw.tmat.data.DefaultPurchasePrice;
import com.rtw.tracker.dao.implementation.HibernateDAO;



public class DefaultPurchasePriceDAO extends HibernateDAO<Integer, DefaultPurchasePrice> implements com.rtw.tmat.dao.services.DefaultPurchasePriceDAO {

	public DefaultPurchasePrice getAllDefaultTourPriceByTourId(Integer id) {
		return findSingle("FROM DefaultPurchasePrice WHERE id = ? ", new Object[]{id});
	}

	public List<DefaultPurchasePrice> getAllDefaultTourPrice() {
		return find("FROM DefaultPurchasePrice");
	}

	public DefaultPurchasePrice getDefaultTourPriceByExchangeAndTicketType(String exchange, String ticketType) {
		return findSingle("FROM DefaultPurchasePrice WHERE exchange= ? AND ticketType=?", new Object[]{exchange,ticketType});
	}


}
