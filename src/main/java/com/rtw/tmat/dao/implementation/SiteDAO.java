package com.rtw.tmat.dao.implementation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.rtw.tmat.data.Site;
import com.rtw.tracker.dao.implementation.HibernateDAO;



public class SiteDAO extends HibernateDAO<String, Site> implements com.rtw.tmat.dao.services.SiteDAO {
	private Map<String, Site> siteById = new HashMap<String, Site>();
	private Collection<Site> sites = null;
	
	public SiteDAO() {
		super();
	}
	

	@Override
	protected void initDao() throws Exception {
		super.initDao();
		sites = super.getAll();
		if (sites == null) {
			sites = new ArrayList<Site>();
		}
		for (Site site: sites) {
			siteById.put(site.getId(), site);
		}		
	}

	@Override
	public Collection<Site> getAll() {
		return sites;
	}

	@Override
	public Site get(String id) {
		return siteById.get(id);
	}

	@Override
	public void update(Site site) {
		Site oldSite = siteById.get(site.getId());
		sites.remove(oldSite);
		sites.add(site);
		siteById.put(site.getId(), site);
		super.update(site);
	}

	@Override
	public String save(Site site) {
		sites.add(site);
		siteById.put(site.getId(), site);
		return super.save(site);
	}

	public Map<String, Site> getSiteByIdMap() {
		return siteById;
	}


}
