package com.rtw.tmat.dao.implementation;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;

import com.rtw.tmat.data.Event;
import com.rtw.tmat.data.Venue;
import com.rtw.tmat.data.VenueCategory;
import com.rtw.tracker.dao.implementation.HibernateDAO;



public class VenueCategoryDAO extends HibernateDAO<Integer, VenueCategory> implements com.rtw.tmat.dao.services.VenueCategoryDAO {

	public VenueCategory getVenueCategoryByVenueAndCategoryGroup(Integer venueId, String categroyGroup) {
		return findSingle("FROM VenueCategory WHERE venue.id=? AND categoryGroup=?",new Object[]{venueId,categroyGroup});
	}
	
	public List<String> getCategoryGroupByVenueCategoryId(Integer venuCatergoryGroupId)
	{
		return find("SELECT categoryGroup FROM VenueCategory WHERE id=?",new Object[]{venuCatergoryGroupId});
	}

	public List<VenueCategory> getVenueCategoriesByVenueId(Integer venueId) {
			return find("FROM VenueCategory WHERE venue.id=?",new Object[]{venueId});
	}

	public List<String> getCategoryGroupByVenueId(Integer venueId) {
		return find("SELECT categoryGroup FROM VenueCategory WHERE venue.id = ?",new Object[]{venueId});
	}
	
	@Override
	public void delete(VenueCategory entity) {
		List<Event> events = TMATDAORegistry.getEventDAO().getAllEventsByVenueCategoryId(entity.getId());
		if(events!=null && !events.isEmpty()){
			for(Event event:events){
				event.setVenueCategoryId(null);
				event.setVenueCategory(null);
				TMATDAORegistry.getEventDAO().saveOrUpdate(event);
			}
			
		}
		TMATDAORegistry.getCategoryDAO().deleteByVenueCategoryId(entity.getId());
		super.delete(entity);
	}
	
	public void deleteVenueCategory(List venueCategoryId) {
		
		Session session = getSession();
		
		try{
			Query query = session.createQuery("UPDATE Event SET venueCategoryId = null WHERE venueCategoryId IN (:venueCategoryId)").setParameterList("venueCategoryId", venueCategoryId);
			query.executeUpdate();
			query = session.createQuery("DELETE FROM CategoryMapping WHERE categoryId IN (SELECT id FROM Category WHERE venueCategoryId IN (:venueCategoryId))").setParameterList("venueCategoryId", venueCategoryId);
			query.executeUpdate();
			query = session.createQuery("DELETE FROM Category WHERE venueCategoryId IN (:venueCategoryId)").setParameterList("venueCategoryId", venueCategoryId);
			query.executeUpdate();
			query = session.createQuery("DELETE FROM VenueCategory WHERE id IN (:venueCategoryId)").setParameterList("venueCategoryId", venueCategoryId);
			query.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		
	}
	
	public Set<VenueCategory> getAllVenueCategoriesForUSandCA(Collection<Venue> venues) {
		List<Integer> venueIds = new ArrayList<Integer>();
		Set<VenueCategory> venueCateogries = new HashSet<VenueCategory>();
		int i = 0;
		Session session = null;
		Query query = null;
		try{
			session = getSession();
			if(venues != null && venues.size() <= 2000){
				for(Venue venue : venues){
					i++;
					venueIds.add(venue.getId());
				}
			}else{
				for(Venue venue : venues){
					i++;
					venueIds.add(venue.getId());
					if(i == 2000){
						query = session.createQuery("FROM VenueCategory WHERE venue.id in (:venueIds) and venue.country in ('US','USA','CA')").setParameterList("venueIds", venueIds);
						venueCateogries.addAll(query.list());
						venueIds = new ArrayList<Integer>();
						i = 0;
					}
				}
			}
			
			if(i >= 1){
				query = session.createQuery("FROM VenueCategory WHERE venue.id in (:venueIds) and venue.country in ('US','USA','CA') order by categoryGroup").setParameterList("venueIds", venueIds);
				venueCateogries.addAll(query.list());
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		return venueCateogries;
	}
}
