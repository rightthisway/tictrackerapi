package com.rtw.tmat.dao.implementation;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.rtw.tmat.data.EventPriceAdjustment;
import com.rtw.tmat.data.EventPriceAdjustmentPk;
import com.rtw.tracker.dao.implementation.HibernateDAO;



public class EventPriceAdjustmentDAO extends HibernateDAO<EventPriceAdjustmentPk, EventPriceAdjustment> implements com.rtw.tmat.dao.services.EventPriceAdjustmentDAO {
	
	private Map<Integer, Map<String, EventPriceAdjustment>> priceAdjustmentByEventAndSiteId = null;
	
	private void __updatePriceAdjustment(EventPriceAdjustment priceAdjustment) {
		Map<String, EventPriceAdjustment> priceAdjustmentbySiteId = priceAdjustmentByEventAndSiteId.get(priceAdjustment.getEventId());
		if (priceAdjustmentbySiteId == null) {
			priceAdjustmentbySiteId = new HashMap<String, EventPriceAdjustment>();
			priceAdjustmentByEventAndSiteId.put(priceAdjustment.getEventId(), priceAdjustmentbySiteId);
		}
		priceAdjustmentbySiteId.put(priceAdjustment.getSiteId(), priceAdjustment);		
	}
	
	@Override
	protected void initDao() throws Exception {
		super.initDao();
		priceAdjustmentByEventAndSiteId = new HashMap<Integer, Map<String,EventPriceAdjustment>>();
		Collection<EventPriceAdjustment> priceAdjustments = super.getAll();
		for(EventPriceAdjustment priceAdjustment: priceAdjustments) {
			__updatePriceAdjustment(priceAdjustment);
		}
	}
	
	public EventPriceAdjustment get(int eventId, String siteId) {
		Map<String, EventPriceAdjustment> priceAdjustmentbySiteId = priceAdjustmentByEventAndSiteId.get(eventId);
		if (priceAdjustmentbySiteId == null) {
			return null;
		}
		return priceAdjustmentbySiteId.get(siteId);
	}
	
	public Map<String, EventPriceAdjustment> getPriceAdjustments(int eventId) {
		return priceAdjustmentByEventAndSiteId.get(eventId);
	}
	
	public void deletePriceAdjustments(int eventId) {
		bulkUpdate("DELETE FROM EventPriceAdjustment WHERE eventId=?", new Object[]{eventId});
		priceAdjustmentByEventAndSiteId.remove(eventId);
	}

	@Override
	public EventPriceAdjustmentPk save(EventPriceAdjustment priceAdjustment) {
		EventPriceAdjustmentPk newId = super.save(priceAdjustment);
		__updatePriceAdjustment(priceAdjustment);
		return newId;
	}

	@Override
	public void update(EventPriceAdjustment priceAdjustment) {
		super.update(priceAdjustment);
		__updatePriceAdjustment(priceAdjustment);
	}
	
	
}