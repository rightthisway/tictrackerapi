package com.rtw.tmat.dao.implementation;

import com.rtw.tmat.dao.services.AdmitoneInventoryDAO;
import com.rtw.tmat.dao.services.AutoPricingErrorDAO;
import com.rtw.tmat.dao.services.AutopricingProductDAO;
import com.rtw.tmat.dao.services.EventDAO;
import com.rtw.tmat.dao.services.RewardthefanCategoryTicketDAO;
import com.rtw.tmat.dao.services.RewardthefanCatsExchangeEventAuditDAO;
import com.rtw.tmat.dao.services.RewardthefanCatsExchangeEventDAO;
import com.rtw.tmat.dao.services.SeatGeekWSTrackingDAO;
import com.rtw.tmat.dao.services.SiteDAO;
import com.rtw.tmat.dao.services.SoldCategoryTicketDAO;
import com.rtw.tmat.dao.services.TicketListingCrawlDAO;
import com.rtw.tmat.dao.services.VenueDAO;


public class TMATDAORegistry {
	private static EventDAO eventDAO;
	private static GrandChildTourCategoryDAO grandChildTourCategoryDAO;
	private static ArtistDAO artistDAO;
	private static CategoryMappingDAO categoryMappingDAO;
	private static CategoryDAO categoryDAO;
	private static QueryManagerDAO queryManagerDAO;
	private static VenueCategoryDAO venueCategoryDAO;
	private static CategorySynonymDAO categorySynonymDAO;
	private static EventPriceAdjustmentDAO eventPriceAdjustmentDAO;
	private static SiteDAO siteDAO;
	private static ValuationFactorDAO valuationFactorDAO;
	private static VenueDAO venueDAO;
	private static DuplicateTicketMapDAO duplicateTicketMapDAO;
	private static TicketDAO ticketDAO;
	private static DefaultPurchasePriceDAO defaultPurchasePriceDAO;
	private static ManagePurchasePriceDAO managePurchasePriceDAO;
	private static CrownJewelEventsDAO crownJewelEventsDAO;
	private static CrownJewelEventsAuditDAO crownJewelEventsAuditDAO;
	private static SeatGeekWSTrackingDAO seatGeekWSTrackingDAO;
	private static TicketListingCrawlDAO ticketListingCrawlDAO;
	private static SoldCategoryTicketDAO soldCategoryTicketDAO;
	private static RewardthefanCatsExchangeEventDAO rewardthefanCatsExchangeEventDAO;
	private static RewardthefanCategoryTicketDAO rewardthefanCategoryTicketDAO;
	private static AutopricingProductDAO autopricingProductDAO;
	private static AutoPricingErrorDAO autoPricingErrorDAO;
	private static RewardthefanCatsExchangeEventAuditDAO rewardthefanCatsExchangeEventAuditDAO;
	private static AdmitoneInventoryDAO admitoneInventoryDAO;
	
	
	
	
	
	
	
	public static EventDAO getEventDAO() {
		return eventDAO;
	}
	public final void setEventDAO(EventDAO eventDAO) {
		TMATDAORegistry.eventDAO = eventDAO;
	}

	public static GrandChildTourCategoryDAO getGrandChildTourCategoryDAO() {
		return grandChildTourCategoryDAO;
	}
	public final void setGrandChildTourCategoryDAO(
			GrandChildTourCategoryDAO grandChildTourCategoryDAO) {
		TMATDAORegistry.grandChildTourCategoryDAO = grandChildTourCategoryDAO;
	}

	public static ArtistDAO getArtistDAO() {
		return artistDAO;
	}
	public final void setArtistDAO(ArtistDAO artistDAO) {
		TMATDAORegistry.artistDAO = artistDAO;
	}
	
	public static CategoryMappingDAO getCategoryMappingDAO() {
		return categoryMappingDAO;
	}
	public final void setCategoryMappingDAO(CategoryMappingDAO categoryMappingDAO) {
		TMATDAORegistry.categoryMappingDAO = categoryMappingDAO;
	}
	
	public static CategoryDAO getCategoryDAO() {
		return categoryDAO;
	}
	public final void setCategoryDAO(CategoryDAO categoryDAO) {
		TMATDAORegistry.categoryDAO = categoryDAO;
	}
	public static QueryManagerDAO getQueryManagerDAO() {
		return queryManagerDAO;
	}
	public final void setQueryManagerDAO(QueryManagerDAO queryManagerDAO) {
		TMATDAORegistry.queryManagerDAO = queryManagerDAO;
	}
	
	public static VenueCategoryDAO getVenueCategoryDAO() {
		return venueCategoryDAO;
	}
	public final void setVenueCategoryDAO(VenueCategoryDAO venueCategoryDAO) {
		TMATDAORegistry.venueCategoryDAO = venueCategoryDAO;
	}
	
	public static CategorySynonymDAO getCategorySynonymDAO() {
		return categorySynonymDAO;
	}
	public final void setCategorySynonymDAO(CategorySynonymDAO categorySynonymDAO) {
		TMATDAORegistry.categorySynonymDAO = categorySynonymDAO;
	}
	
	public static EventPriceAdjustmentDAO getEventPriceAdjustmentDAO() {
		return eventPriceAdjustmentDAO;
	}
	public final void setEventPriceAdjustmentDAO(
			EventPriceAdjustmentDAO eventPriceAdjustmentDAO) {
		TMATDAORegistry.eventPriceAdjustmentDAO = eventPriceAdjustmentDAO;
	}
	
	public static SiteDAO getSiteDAO() {
		return siteDAO;
	}
	public final void setSiteDAO(SiteDAO siteDAO) {
		TMATDAORegistry.siteDAO = siteDAO;
	}
	
	public static ValuationFactorDAO getValuationFactorDAO() {
		return valuationFactorDAO;
	}
	public final void setValuationFactorDAO(ValuationFactorDAO valuationFactorDAO) {
		TMATDAORegistry.valuationFactorDAO = valuationFactorDAO;
	}
	
	public static VenueDAO getVenueDAO() {
		return venueDAO;
	}
	public final void setVenueDAO(VenueDAO venueDAO) {
		TMATDAORegistry.venueDAO = venueDAO;
	}
	public static DuplicateTicketMapDAO getDuplicateTicketMapDAO() {
		return duplicateTicketMapDAO;
	}
	public static void setDuplicateTicketMapDAO(
			DuplicateTicketMapDAO duplicateTicketMapDAO) {
		TMATDAORegistry.duplicateTicketMapDAO = duplicateTicketMapDAO;
	}
	public static TicketDAO getTicketDAO() {
		return ticketDAO;
	}
	public final void setTicketDAO(TicketDAO ticketDAO) {
		TMATDAORegistry.ticketDAO = ticketDAO;
	}
	public static DefaultPurchasePriceDAO getDefaultPurchasePriceDAO() {
		return defaultPurchasePriceDAO;
	}
	public final void setDefaultPurchasePriceDAO(
			DefaultPurchasePriceDAO defaultPurchasePriceDAO) {
		TMATDAORegistry.defaultPurchasePriceDAO = defaultPurchasePriceDAO;
	}
	public static ManagePurchasePriceDAO getManagePurchasePriceDAO() {
		return managePurchasePriceDAO;
	}
	public final void setManagePurchasePriceDAO(
			ManagePurchasePriceDAO managePurchasePriceDAO) {
		TMATDAORegistry.managePurchasePriceDAO = managePurchasePriceDAO;
	}
	public static CrownJewelEventsDAO getCrownJewelEventsDAO() {
		return crownJewelEventsDAO;
	}

	public final void setCrownJewelEventsDAO(
			CrownJewelEventsDAO crownJewelEventsDAO) {
		TMATDAORegistry.crownJewelEventsDAO = crownJewelEventsDAO;
	}

	public static CrownJewelEventsAuditDAO getCrownJewelEventsAuditDAO() {
		return crownJewelEventsAuditDAO;
	}

	public final void setCrownJewelEventsAuditDAO(
			CrownJewelEventsAuditDAO crownJewelEventsAuditDAO) {
		TMATDAORegistry.crownJewelEventsAuditDAO = crownJewelEventsAuditDAO;
	}
	
	public static SeatGeekWSTrackingDAO getSeatGeekWSTrackingDAO() {
		return seatGeekWSTrackingDAO;
	}
	public final void setSeatGeekWSTrackingDAO(
			SeatGeekWSTrackingDAO seatGeekWSTrackingDAO) {
		TMATDAORegistry.seatGeekWSTrackingDAO = seatGeekWSTrackingDAO;
	}
	public static TicketListingCrawlDAO getTicketListingCrawlDAO() {
		return ticketListingCrawlDAO;
	}
	public final void setTicketListingCrawlDAO(
			TicketListingCrawlDAO ticketListingCrawlDAO) {
		TMATDAORegistry.ticketListingCrawlDAO = ticketListingCrawlDAO;
	}
	public static SoldCategoryTicketDAO getSoldCategoryTicketDAO() {
		return soldCategoryTicketDAO;
	}
	public final void setSoldCategoryTicketDAO(SoldCategoryTicketDAO soldCategoryTicketDAO) {
		TMATDAORegistry.soldCategoryTicketDAO = soldCategoryTicketDAO;
	}
	public static RewardthefanCatsExchangeEventDAO getRewardthefanCatsExchangeEventDAO() {
		return rewardthefanCatsExchangeEventDAO;
	}
	public final void setRewardthefanCatsExchangeEventDAO(
			RewardthefanCatsExchangeEventDAO rewardthefanCatsExchangeEventDAO) {
		TMATDAORegistry.rewardthefanCatsExchangeEventDAO = rewardthefanCatsExchangeEventDAO;
	}
	public static RewardthefanCategoryTicketDAO getRewardthefanCategoryTicketDAO() {
		return rewardthefanCategoryTicketDAO;
	}
	public final void setRewardthefanCategoryTicketDAO(RewardthefanCategoryTicketDAO rewardthefanCategoryTicketDAO) {
		TMATDAORegistry.rewardthefanCategoryTicketDAO = rewardthefanCategoryTicketDAO;
	}
	public static AutopricingProductDAO getAutopricingProductDAO() {
		return autopricingProductDAO;
	}
	public final void setAutopricingProductDAO(AutopricingProductDAO autopricingProductDAO) {
		TMATDAORegistry.autopricingProductDAO = autopricingProductDAO;
	}
	public static AutoPricingErrorDAO getAutoPricingErrorDAO() {
		return autoPricingErrorDAO;
	}
	public final void setAutoPricingErrorDAO(AutoPricingErrorDAO autoPricingErrorDAO) {
		TMATDAORegistry.autoPricingErrorDAO = autoPricingErrorDAO;
	}
	public static RewardthefanCatsExchangeEventAuditDAO getRewardthefanCatsExchangeEventAuditDAO() {
		return rewardthefanCatsExchangeEventAuditDAO;
	}
	public final void setRewardthefanCatsExchangeEventAuditDAO(
			RewardthefanCatsExchangeEventAuditDAO rewardthefanCatsExchangeEventAuditDAO) {
		TMATDAORegistry.rewardthefanCatsExchangeEventAuditDAO = rewardthefanCatsExchangeEventAuditDAO;
	}
	public static AdmitoneInventoryDAO getAdmitoneInventoryDAO() {
		return admitoneInventoryDAO;
	}
	public final void setAdmitoneInventoryDAO(AdmitoneInventoryDAO admitoneInventoryDAO) {
		TMATDAORegistry.admitoneInventoryDAO = admitoneInventoryDAO;
	}
	
	
}