package com.rtw.tmat.dao.implementation;

import java.util.Collection;

import com.rtw.tmat.data.ValuationFactor;
import com.rtw.tracker.dao.implementation.HibernateDAO;


public class ValuationFactorDAO extends HibernateDAO<Integer, ValuationFactor> implements com.rtw.tmat.dao.services.ValuationFactorDAO {
	public Collection<ValuationFactor> getAllValuationFactorsFromEvent(Integer eventId) {
		return find("FROM ValuationFactor WHERE event_id=?", new Object[]{eventId});
	}
	
	/*public Collection<ValuationFactor> getAllValuationFactorsFromTour(Integer tourId) {
		return find("SELECT v FROM ValuationFactor v, Event e WHERE e.id=v.eventId AND e.tourId=?", new Object[]{tourId});
	}*/
	
	public Collection<ValuationFactor> getAllValuationFactorsFromArtist(Integer artistId) {
		return find("SELECT v FROM ValuationFactor v, Event e WHERE e.id=v.eventId AND e.artistId=?", new Object[]{artistId});
	}
}
