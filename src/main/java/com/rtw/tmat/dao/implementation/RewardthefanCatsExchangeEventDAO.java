package com.rtw.tmat.dao.implementation;

import java.util.Collection;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.data.AutopricingProduct;
import com.rtw.tmat.data.RewardthefanCatsExchangeEvent;
import com.rtw.tmat.utils.Util;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.dao.implementation.HibernateDAO;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.PaginationUtil;


public class RewardthefanCatsExchangeEventDAO extends HibernateDAO<Integer, RewardthefanCatsExchangeEvent> implements com.rtw.tmat.dao.services.RewardthefanCatsExchangeEventDAO {

	public RewardthefanCatsExchangeEvent getRtfCatsExchangeEventByEventId(Integer eventId) throws Exception {
		return findSingle("FROM RewardthefanCatsExchangeEvent where eventId=?" ,new Object[]{eventId});
	}
	
public Collection<RewardthefanCatsExchangeEvent> getAllRewardthefanCatsExchangeEventsEligibleForUpdate(Long minute,AutopricingProduct autopricingProduct)  throws Exception {
		
		String sql =" select distinct le.id as id,le.event_id as eventId,le.exclude_hours_before_event as excludeHoursBeforeEvent " + 
					" from rewardthefan_exchange_event le with(nolock) " + 
					" INNER join ticket_listing_crawl tc with(nolock) on tc.event_id = le.event_id " +
					" INNER JOIN event e with(nolock) on e.id = le.event_id " +
					" LEFT JOIN autopricing_error aer with(nolock) on aer.event_id=e.id" +
					" WHERE le.status = 'ACTIVE' " +
					" AND tc.site_id in ('ticketnetworkdirect','stubhub','ticketevolution','flashseats','vividseat') " +
					" AND (DATEDIFF(MINUTE,tc.end_crawl,GETDATE())<=  " + minute +
					" OR (DATEDIFF(MINUTE,le.last_updated_date,GETDATE())<= " + minute + ")" +
					" OR (DATEDIFF(MINUTE,e.last_update,GETDATE())<= " + minute + ")" +
					" OR (aer.product_id="+autopricingProduct.getId()+" and aer.time_stamp is not null and DATEDIFF(MINUTE,aer.time_stamp,GETDATE())<= " + minute + ")" +
					" ) " ;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			sqlQuery.addScalar("id",Hibernate.INTEGER);
			sqlQuery.addScalar("eventId",Hibernate.INTEGER);
			sqlQuery.addScalar("excludeHoursBeforeEvent",Hibernate.INTEGER);
			List<RewardthefanCatsExchangeEvent> exchangeEvents =  sqlQuery.setResultTransformer(Transformers.aliasToBean(RewardthefanCatsExchangeEvent.class)).list();
			return exchangeEvents;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return null;
	}

public List<RewardthefanCatsExchangeEvent> getAllRtfCatsEvents(GridHeaderFilters filter,Integer artistId,Integer venueId,Integer grandChildId, String pageNo,
		Boolean isExcludeEvents,Boolean isPagination) {
	List<RewardthefanCatsExchangeEvent> eventList = null;
	Integer firstResult = 0;
	Session session=null;
	try {
		
		 
		
		String sql = "select distinct tn.id as id, e.id as eventId, e.name as eventName, e.event_date as eventDate, e.event_time as eventTime, " + 
				" v.id as venueId,v.building as building,tn.last_updated_by as lastUpdatedBy,tn.last_updated_date as lastUpdatedDate," +
				" exclude_hours_before_Event as excludeHoursBeforeEvent,v.city as city,v.state as state,v.country as country  " + 
				" from rewardthefan_exchange_event tn" + 
				" inner join event e on e.id=tn.event_id" + 
				" inner join venue v on v.id=e.venue_id" + 
				" inner join artist a on a.id=e.artist_id" + 
				" inner join grand_Child_tour_Category gc on gc.id=a.grand_child_category_id" + 
				" ";
		
		if(isExcludeEvents) {
			sql += " and tn.status='DELETED'";
		} else {
			sql += " and tn.status='ACTIVE'";
		}
		
		if(artistId!=null){
			sql += " AND a.id ="+artistId;
		}else if(venueId!=null){
			sql += " AND v.id ="+venueId;
		} else if(grandChildId!=null){
			sql += " AND gc.id ="+grandChildId;
		}
		
		if(filter.getEventId()!=null){
			sql += " AND e.id ="+filter.getEventId();
		}
		if(filter.getEventName()!=null && !filter.getEventName().isEmpty()){
			sql += " AND e.name like '%"+filter.getEventName()+"%'";
		}
		if(filter.getVenueId()!=null){
			sql += " AND v.id ="+filter.getVenueId();
		}
		if(filter.getVenueName()!=null && !filter.getVenueName().isEmpty()){
			sql += " AND v.building like '%"+filter.getVenueName()+"%'";
		}
		if(filter.getCity()!=null && !filter.getCity().isEmpty()){
			sql += " AND v.city like '%"+filter.getCity()+"%'";
		}
		if(filter.getState()!=null && !filter.getState().isEmpty()){
			sql += " AND v.state like '%"+filter.getState()+"%'";
		}
		if(filter.getCountry()!=null && !filter.getCountry().isEmpty()){
			sql += " AND v.country like '%"+filter.getCountry()+"%'";
		}
		if(filter.getEventDateStr() != null){
			if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
				sql += " AND DATEPART(day, e.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
			}
			if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
				sql += " AND DATEPART(month, e.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
			}
			if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
				sql += " AND DATEPART(year, e.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
			}
		}
		if(filter.getEventTimeStr() != null){
			if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
				sql += " AND DATEPART(hour, e.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
			}
			if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
				sql += " AND DATEPART(minute, e.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
			}
		}
		sql += " order by e.event_date,e.event_time";
		
		System.out.println(sql);
		
//			session = getSessionFactory().openSession();
		session = getSession();
		SQLQuery query = session.createSQLQuery(sql);
		
		query.addScalar("id", Hibernate.INTEGER);
		query.addScalar("eventId", Hibernate.INTEGER);
		query.addScalar("eventName", Hibernate.STRING);
		query.addScalar("eventDate", Hibernate.DATE);
		query.addScalar("eventTime", Hibernate.TIME);
		query.addScalar("venueId", Hibernate.INTEGER);						
		query.addScalar("building", Hibernate.STRING);
		query.addScalar("lastUpdatedBy", Hibernate.STRING);	
		query.addScalar("lastUpdatedDate", Hibernate.TIMESTAMP);	
		query.addScalar("city", Hibernate.STRING);
		query.addScalar("state", Hibernate.STRING);
		query.addScalar("country", Hibernate.STRING);
		query.addScalar("excludeHoursBeforeEvent", Hibernate.INTEGER);
		
		if(isPagination){
			firstResult = PaginationUtil.getNextPageStatFrom(pageNo);
			eventList = query.setResultTransformer(Transformers.aliasToBean(RewardthefanCatsExchangeEvent.class)).setFirstResult(firstResult).setMaxResults(PaginationUtil.PAGESIZE).list();
		}else{
			eventList = query.setResultTransformer(Transformers.aliasToBean(RewardthefanCatsExchangeEvent.class)).list();
		}
		
	} catch (Exception e) {
		if(session != null && session.isOpen()){
			session.close();
		}
		e.printStackTrace();
	}
	return eventList;
}

public Integer getAllRtfCatsEventsCount(GridHeaderFilters filter,Integer artistId,Integer venueId,Integer grandChildId, String pageNo){
	Integer count =0;	
	Session session=null;
	try {		
		
		String sql = " select count(*) as cnt from " + 
				" rewardthefan_exchange_event tn" + 
				" inner join event e on e.id=tn.event_id" + 
				" inner join venue v on v.id=e.venue_id" + 
				" inner join artist a on a.id=e.artist_id" + 
				" inner join grand_Child_tour_Category gc on gc.id=a.grand_child_category_id" + 
				" where event_date>=getdate() and tn.status='ACTIVE'" + 
				" ";
		
		if(artistId!=null){
			sql += " AND a.id ="+artistId;
		}else if(venueId!=null){
			sql += " AND v.id ="+venueId;
		} else if(grandChildId!=null){
			sql += " AND gc.id ="+grandChildId;
		}
		
		if(filter.getEventId()!=null){
			sql += " AND e.id ="+filter.getEventId();
		}
		if(filter.getEventName()!=null && !filter.getEventName().isEmpty()){
			sql += " AND e.name like '%"+filter.getEventName()+"%'";
		}
		if(filter.getVenueId()!=null){
			sql += " AND v.id ="+filter.getVenueId();
		}
		if(filter.getVenueName()!=null && !filter.getVenueName().isEmpty()){
			sql += " AND v.building like '%"+filter.getVenueName()+"%'";
		}			
		if(filter.getEventDateStr() != null){
			if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
				sql += " AND DATEPART(day, e.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
			}
			if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
				sql += " AND DATEPART(month, e.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
			}
			if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
				sql += " AND DATEPART(year, e.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
			}
		}
		if(filter.getEventTimeStr() != null){
			if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
				sql += " AND DATEPART(hour, e.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
			}
			if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
				sql += " AND DATEPART(minute, e.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
			}
		}
		sql += " ";
		
		
		session = getSession();
		SQLQuery query = session.createSQLQuery(sql);
		
		count = (Integer) query.uniqueResult();
	} catch (Exception e) {
		if(session != null && session.isOpen()){
			session.close();
		}
		e.printStackTrace();
	}
	return count;
}
}
