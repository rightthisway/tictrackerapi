package com.rtw.tmat.dao.services;

import java.util.Collection;

import com.rtw.tmat.data.Ticket;
import com.rtw.tracker.dao.services.RootDAO;
import com.rtw.tracker.datas.TicketStatus;



public interface TicketDAO extends RootDAO<Integer, Ticket> {
	public Collection<Ticket> getAllActiveTicketsByEvent(Integer eventId);
	public int getTicketCountByEvent(int event_id, TicketStatus status);
	public int getTicketQuantitySumByEvent(int event_id, TicketStatus status);
	public Collection<Ticket> getAllActiveTicketsByNormalizedSectionAndEventId(Integer eventId, String section);
	public Collection<Ticket> getAllActiveTicketForAutoCatsByEventId(Integer eventId,Boolean isTNCrawlOnly,Boolean isSkipVividCrawls)  throws Exception;
	public Collection<Ticket> getAllActiveInstantTicketForAutoCatByEventId(Integer eventId,Boolean isTNCrawlOnly,Boolean isSkipVividCrawls)  throws Exception;
	
}