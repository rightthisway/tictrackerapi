package com.rtw.tmat.dao.services;


import java.util.List;

import com.rtw.tmat.data.ManagePurchasePrice;
import com.rtw.tracker.dao.services.RootDAO;


public interface ManagePurchasePriceDAO extends RootDAO<Integer , ManagePurchasePrice> {

	List<Integer> getDistinctArtistIds();

	List<ManagePurchasePrice> getAllByArtistIds(List<Integer> ids);

//	Collection<Artist> getDistinctArtists();

	List<ManagePurchasePrice> getAllByArtistExchangeTicketTypes(List<Integer> ids,List<String> exchangeIds, List<String> ticketTypeIds);
	List<ManagePurchasePrice> getAllByManagePurchasePriceByArtistId(Integer Id);
	List<ManagePurchasePrice> getAllManagePurchasePriceByEventId(Integer Id);

}
