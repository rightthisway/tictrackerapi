package com.rtw.tmat.dao.services;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.rtw.tmat.data.Venue;
import com.rtw.tmat.data.VenueCategory;
import com.rtw.tracker.dao.services.RootDAO;


public interface VenueCategoryDAO extends RootDAO<Integer, VenueCategory> {
	public VenueCategory getVenueCategoryByVenueAndCategoryGroup(Integer venueId,String categroyGroup);
	public List<VenueCategory> getVenueCategoriesByVenueId(Integer venueId);
	public List<String> getCategoryGroupByVenueId(Integer venueId);
	public void deleteVenueCategory(List venueCategoryId);
	public List<String> getCategoryGroupByVenueCategoryId(Integer venuCatergoryGroupId );
	public Set<VenueCategory> getAllVenueCategoriesForUSandCA(Collection<Venue> venues);
}
