package com.rtw.tmat.dao.services;

import java.util.Collection;

import com.rtw.tmat.data.Artist;
import com.rtw.tracker.dao.services.RootDAO;

;

public interface ArtistDAO extends RootDAO<Integer, Artist> {
	
	public Collection<Artist> filterByName(String pattern);
}
