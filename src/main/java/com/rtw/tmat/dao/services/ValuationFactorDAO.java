package com.rtw.tmat.dao.services;

import java.util.Collection;

import com.rtw.tmat.data.ValuationFactor;
import com.rtw.tracker.dao.services.RootDAO;


public interface ValuationFactorDAO extends RootDAO<Integer, ValuationFactor> {
	Collection<ValuationFactor> getAllValuationFactorsFromEvent(Integer eventId);
	//Collection<ValuationFactor> getAllValuationFactorsFromTour(Integer tourId);
	Collection<ValuationFactor> getAllValuationFactorsFromArtist(Integer artistId);
}