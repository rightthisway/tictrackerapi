package com.rtw.tmat.dao.services;

import java.util.Collection;
import java.util.Date;

import com.rtw.tmat.data.AdmitoneInventory;
import com.rtw.tracker.dao.services.RootDAO;


public interface AdmitoneInventoryDAO extends RootDAO<Integer, AdmitoneInventory> {
	// FIXME: not really sure what this method's purpose is. Remove it?
	@Deprecated
	Collection<AdmitoneInventory> getAllInventoryByDate(Date eventDate)throws Exception;
	
	Collection<AdmitoneInventory> getAllInventoryByAdmitOneEventId(Integer eventId)throws Exception;
	Collection<AdmitoneInventory> getAllInventoryByEventId(Integer eventId)throws Exception;
}
