package com.rtw.tmat.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtw.tmat.data.Category;
import com.rtw.tmat.enums.EventStatus;
import com.rtw.tracker.dao.services.RootDAO;



public interface CategoryDAO extends RootDAO<Integer, Category> {
	public void deleteByVenueCategoryId(Integer venueId);
	public Collection<Category> getAllCategoriesByVenueCategoryId(Integer venueCategoryId);
	public Collection<Category> getAllCategoriesByVenueIdAndCategoryGroup(Integer venueId, String categoryGroup);
	public Category getCategoryByVenueCategoryIdAndCategorySymbol(Integer venueCategoryId, String categorySymbol);
	public String getCategorieGroupByVenueCategoryId(Integer venueCategoryId);
	public Collection<Category> getAllCategoriesByVenuCategoryIdsByArtistId(Integer artistId,EventStatus eventSataus);
	public int countByVenueCategoryID(Integer id);
	public List<String> getCategorySymbolsByVenueCategoryId(Integer venueCategoryId) throws Exception;
	
}