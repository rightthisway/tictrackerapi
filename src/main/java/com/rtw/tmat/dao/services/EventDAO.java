package com.rtw.tmat.dao.services;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.rtw.tmat.data.Event;
import com.rtw.tracker.dao.services.RootDAO;



public interface EventDAO extends RootDAO<Integer, Event> {
	List<Event> getAllActiveEventsByArtistId(Integer artistId);
	public List<Event> getAllActiveEventsByVenueId(Integer venueId);
	public List<Event> getAllEventsByVenueCategoryId(Integer venueCategoryId);
	public Event.EventTicketStat getNonDuplicateEventTicketStat(int eventId);
	public List<Event> getAllActiveEventsByIds(List<Integer> ids);
	public Event getEventById(Integer eventId);
	public Event getEventByAdmitOneId(Integer admitOneId);
	public Collection<Event> getAllActiveEvents();
} 
