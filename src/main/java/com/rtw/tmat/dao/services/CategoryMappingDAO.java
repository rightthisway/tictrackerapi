package com.rtw.tmat.dao.services;

import java.util.List;

import com.rtw.tmat.data.CategoryMapping;
import com.rtw.tmat.enums.EventStatus;
import com.rtw.tracker.dao.services.RootDAO;



public interface CategoryMappingDAO extends RootDAO<Integer, CategoryMapping> {
	List<CategoryMapping> getAllCategoryMappingsByVenueCategoryId(Integer venueCategoryId);
	List<CategoryMapping> getAllCategoryMappingsByCategoryId(Integer categoryId);
	List<String> getAllCategoryGroupNamesByTourIdByVenueCategoryId(Integer tourId,EventStatus eventStatus);
	public List<String> getCategoryByEventIdAndZone(Integer eventId,String zone);
	
}