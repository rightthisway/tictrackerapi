package com.rtw.tmat.dao.services;

import java.util.List;

import com.rtw.tmat.data.CrownJewelEvents;
import com.rtw.tracker.dao.services.RootDAO;


public interface CrownJewelEventsDAO extends RootDAO<Integer, CrownJewelEvents> {
	
	public CrownJewelEvents getCrownJewelEventByEventId(Integer eventId) throws Exception;
	public List<CrownJewelEvents> getCrownJewelEventByEventIds(List<Integer> eventIds) throws Exception;
	public List<CrownJewelEvents> getCrownJewelEventByParentType(String parentType) throws Exception;

}
