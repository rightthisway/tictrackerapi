package com.rtw.tmat.dao.services;

import java.util.Collection;
import java.util.Map;

import com.rtw.tmat.data.Site;
import com.rtw.tracker.dao.services.RootDAO;


public interface SiteDAO extends RootDAO<String, Site> {
	public Collection<Site> getAll();
	public Site get(String id);
	public void update(Site site);
	public String save(Site site);
	public Map<String, Site> getSiteByIdMap();
}