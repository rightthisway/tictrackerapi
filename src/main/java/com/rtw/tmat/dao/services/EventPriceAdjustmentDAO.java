package com.rtw.tmat.dao.services;

import java.util.Map;

import com.rtw.tmat.data.EventPriceAdjustment;
import com.rtw.tmat.data.EventPriceAdjustmentPk;
import com.rtw.tracker.dao.services.RootDAO;


public interface EventPriceAdjustmentDAO extends RootDAO<EventPriceAdjustmentPk, EventPriceAdjustment> {
	EventPriceAdjustment get(int eventId, String siteId);
	Map<String, EventPriceAdjustment> getPriceAdjustments(int eventId);
	void deletePriceAdjustments(int eventId);
	EventPriceAdjustmentPk save(EventPriceAdjustment priceAdjustment);
	void update(EventPriceAdjustment priceAdjustment);
}
