package com.rtw.tmat.dao.services;

import java.util.List;

import com.rtw.tmat.data.RewardthefanCategoryTicket;
import com.rtw.tracker.dao.services.RootDAO;


public interface RewardthefanCategoryTicketDAO extends RootDAO<Integer, RewardthefanCategoryTicket>{

	public List<RewardthefanCategoryTicket> getAllActiveRtfCategoryTicketsByEventId(Integer eventId) throws Exception;
	public Integer deleteAllActiveRtfCategoryticketsByEventId(Integer eventId,String reason)  throws Exception;
}
