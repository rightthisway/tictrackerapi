package com.rtw.tmat.dao.services;

import com.rtw.tmat.data.RewardthefanCatsExchangeEventAudit;
import com.rtw.tracker.dao.services.RootDAO;


public interface RewardthefanCatsExchangeEventAuditDAO extends RootDAO<Integer, RewardthefanCatsExchangeEventAudit>{

}
