package com.rtw.tmat.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtw.tmat.data.AutopricingProduct;
import com.rtw.tmat.data.RewardthefanCatsExchangeEvent;
import com.rtw.tracker.dao.services.RootDAO;
import com.rtw.tracker.utils.GridHeaderFilters;


public interface RewardthefanCatsExchangeEventDAO extends RootDAO<Integer, RewardthefanCatsExchangeEvent>{

	public RewardthefanCatsExchangeEvent getRtfCatsExchangeEventByEventId(Integer eventId) throws Exception;
	public Collection<RewardthefanCatsExchangeEvent> getAllRewardthefanCatsExchangeEventsEligibleForUpdate(Long minute,AutopricingProduct autopricingProduct)  throws Exception;
	
	public Integer getAllRtfCatsEventsCount(GridHeaderFilters filter,Integer artistId,Integer venueId,Integer grandChildId, String pageNo);
	public List<RewardthefanCatsExchangeEvent> getAllRtfCatsEvents(GridHeaderFilters filter,Integer artistId,Integer venueId,Integer grandChildId, String pageNo,
			Boolean isExcludeEvents,Boolean isPagination);
}
