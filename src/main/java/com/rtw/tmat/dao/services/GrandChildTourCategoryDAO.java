package com.rtw.tmat.dao.services;

import com.rtw.tmat.data.GrandChildTourCategory;
import com.rtw.tracker.dao.services.RootDAO;


public interface GrandChildTourCategoryDAO extends RootDAO<Integer, GrandChildTourCategory> {}
