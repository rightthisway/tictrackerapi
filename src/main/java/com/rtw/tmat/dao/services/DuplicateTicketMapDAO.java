package com.rtw.tmat.dao.services;

import java.util.Collection;

import com.rtw.tmat.data.DuplicateTicketMap;
import com.rtw.tracker.dao.services.RootDAO;


public interface DuplicateTicketMapDAO extends RootDAO<Integer, DuplicateTicketMap> {
	Collection<DuplicateTicketMap> getDuplicateTicketMap(Integer eventId);
	void deleteByGroupId(Long groupId);
	void deleteGroupsWithOneEntry(Integer eventId);
	void delete(Integer ticketId, boolean deleteGroup);
}