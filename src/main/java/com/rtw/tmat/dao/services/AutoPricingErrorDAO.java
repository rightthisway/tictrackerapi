package com.rtw.tmat.dao.services;

import com.rtw.tmat.data.AutoPricingError;
import com.rtw.tracker.dao.services.RootDAO;


public interface AutoPricingErrorDAO extends RootDAO<Integer, AutoPricingError>{
	
}
