package com.rtw.tmat.dao.services;

import com.rtw.tmat.data.CrownJewelEventsAudit;
import com.rtw.tracker.dao.services.RootDAO;

public interface CrownJewelEventsAuditDAO extends RootDAO<Integer, CrownJewelEventsAudit> {
	

}
