package com.rtw.tmat.dao.services;


import java.util.List;

import com.rtw.tmat.data.DefaultPurchasePrice;
import com.rtw.tracker.dao.services.RootDAO;


public interface DefaultPurchasePriceDAO extends RootDAO<Integer , DefaultPurchasePrice> {
	DefaultPurchasePrice getAllDefaultTourPriceByTourId(Integer Id);
	List<DefaultPurchasePrice> getAllDefaultTourPrice();
	DefaultPurchasePrice getDefaultTourPriceByExchangeAndTicketType(String exchange,String ticketType);
}
