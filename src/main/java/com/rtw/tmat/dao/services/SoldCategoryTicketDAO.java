package com.rtw.tmat.dao.services;

import java.util.List;

import com.rtw.tmat.data.SoldCategoryTicket;
import com.rtw.tracker.dao.services.RootDAO;


public interface SoldCategoryTicketDAO extends RootDAO<Integer, SoldCategoryTicket>{

	public List<SoldCategoryTicket> getAllSoldCategoryTicketsByEventId(Integer eventId) throws Exception;
}
