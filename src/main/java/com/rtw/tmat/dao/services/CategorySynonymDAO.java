package com.rtw.tmat.dao.services;

import java.util.Collection;

import com.rtw.tmat.data.CategorySynonym;
import com.rtw.tracker.dao.services.RootDAO;


public interface CategorySynonymDAO extends RootDAO<Integer, CategorySynonym> {
	Collection<CategorySynonym> getCategorySynonyms(String name);
	Collection<CategorySynonym> getSynonyms(Integer catId);
	Collection<CategorySynonym> getTourCategorySynonyms(Integer tourId);
}
