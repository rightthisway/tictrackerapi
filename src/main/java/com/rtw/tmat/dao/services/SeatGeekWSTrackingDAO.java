package com.rtw.tmat.dao.services;

import com.rtw.tmat.data.SeatGeekWSTracking;
import com.rtw.tracker.dao.services.RootDAO;

public interface SeatGeekWSTrackingDAO extends RootDAO<Integer, SeatGeekWSTracking> {
	
}
