package com.rtw.tmat.data;

import java.io.File;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.rtw.tmat.dao.implementation.TMATDAORegistry;
import com.rtw.tmat.utils.Categorizer;
import com.rtw.tmat.utils.TicketUtil;
import com.rtw.tracker.datas.TicketStatus;

@Entity
@Table(name="event")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public final class Event extends BaseEvent {
	private Venue venue;
	private String marketMakerManager;
//	private Collection<Category> category;
	private String uniqueCategoryGroupName;
	private String zoneCategoryGroupName=null;
	private String formatedDate;
	private String formatedAdmitoneId;
	private String catGroupName;
//	private String secondaryCategoryGroup;
	private Collection<String> enabledCrawlSites;
	private Collection<String> disabledCrawlSites;
	private Boolean autoCorrect;
	private Boolean ignoreTBDTime;
	private VenueCategory venueCategory;
	private Boolean noPrice;
	private String admitoneEventDetails;
	
	private Integer tempArtistId;
	private Integer tempVenueId;
	private String tempEventDateStr;
	private String tempEventTimeStr;
	
	public Event() {
	}
	
	
	@Column(name="venue_id")
	public Integer getVenueId() {
		return venueId;
	}

	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}

	@Column(name="auto_correct")
	public Boolean getAutoCorrect() {
		if(autoCorrect==null){
			return false;
		}
		return autoCorrect;
	}

	public void setAutoCorrect(Boolean autoCorrect) {
		this.autoCorrect = autoCorrect;
	}

	@Column(name="ignore_tbd_time")
	public Boolean getIgnoreTBDTime() {
		if(ignoreTBDTime==null){
			return false;
		}
		return ignoreTBDTime;
	}

	public void setIgnoreTBDTime(Boolean ignoreTBDTime) {
		this.ignoreTBDTime = ignoreTBDTime;
	}
	
	@Transient
	public Venue getVenue() {
		if (venueId == null) {
			return null;
		}
		
		if (venue == null) {
			//DAOCall method - for fetching venue by venue id
			venue = TMATDAORegistry.getVenueDAO().get(venueId); 
		}
		return venue;
	}
	
	@Transient
	public void setVenue(Venue venue) {
		this.venue = venue;
	}
	
	
	@Transient
	public Collection<Category> getCategory(){
		if(this.getVenueCategory()!=null){
			//Call method - for fetching categories by venue category id 
			return Categorizer.getCategoryByVenueCategoryId(this.getVenueCategory().getId());
		}
		return null;
	}	
	
	
	@Transient
	public String getUniqueCategoryGroupName(){
		this.uniqueCategoryGroupName  = "";
		if(this.getVenueCategory()!=null){
				this.uniqueCategoryGroupName = this.getVenueCategory().getCategoryGroup();
		}
		
		return this.uniqueCategoryGroupName;
	}

	
	@Transient
	public Integer getTicketCount() {
		//DAOCall method - for fetching no of active tickets based on event id 
		return TMATDAORegistry.getTicketDAO().getTicketCountByEvent(id, TicketStatus.ACTIVE);
	}
	
	@Transient
	public Integer getTicketQuantitySum() {
		//DAOCall method - for fetching no of ticket quantities based on event id
		return TMATDAORegistry.getTicketDAO().getTicketQuantitySumByEvent(id, TicketStatus.ACTIVE);
	}
	
	@Transient
	public EventTicketStat getNonDuplicateEventTicketStat() {
		return TMATDAORegistry.getEventDAO().getNonDuplicateEventTicketStat(id);
	}

	@Transient
	public Integer getNonDuplicateTicketCount() {
		//DAOCall method - for fetching all active tickets by event id
		Collection<Ticket> tickets = TMATDAORegistry.getTicketDAO().getAllActiveTicketsByEvent(id);
		
		//Call method for removing duplicate tickets 
		return TicketUtil.removeDuplicateTickets(tickets).size();
	}

	@Transient
	public Integer getNonDuplicateTicketQuantitySum() {
		//Call method for removing duplicate tickets 
		Collection<Ticket> tickets = TicketUtil.removeDuplicateTickets(TMATDAORegistry.getTicketDAO().getAllActiveTicketsByEvent(id));		
		int quantitySum = 0;
		for (Ticket ticket:tickets) {
			quantitySum += ticket.getRemainingQuantity();
		}
		return quantitySum;
	}
	/**
	 * Get a well formatted venue description.
	 * @return
	 */
	@Transient
	public String getFormattedVenueDescription() {
		Venue venue = getVenue();
		if (venue == null) {
			return "n/a";
		}
		
		String[] tokens = {
			venue.getBuilding(),
			venue.getCity(),
			venue.getState(),
			venue.getCountry()				
		};
		
		String v = null;
		for (String token: tokens) {
			if (token == null || token.equals("null")) {
				continue;
			}
			
			if (v == null) {
				v = token;
			} else {
				v += ", " + token;
			}
		}
		return v;
	}

	/* COMMENTED
	 * @Transient
	public String getMarketMakerManager() {
		if (marketMakerManager == null) {
			//DAOCall method - for fetching active market manager for particular event by event id 
			MarketMakerEventTracking tracking = TMATDAORegistry.getMarketMakerEventTrackingDAO().getCurrentMarketMakerEventTracking(id);
			if (tracking == null) {
				marketMakerManager = "DESK";
			} else {
				marketMakerManager = tracking.getMarketMakerUsername();
			}
		}
		
		return marketMakerManager;
	}*/
	
	@Transient  //commented as we don't use it ager zones b2b. 
	public String getZoneCategoryGroupName() {
		return zoneCategoryGroupName;
	}

	public void setZoneCategoryGroupName(String zoneCategoryGroupName) {
		this.zoneCategoryGroupName = zoneCategoryGroupName;
	}
	@Transient
	public String getFormatedDate() {
		// TODO Auto-generated method stub	
		if(getDate() == null){
			formatedDate = "TBD";	
		}else{
		formatedDate= dateFormat.format(getDate());
		}
		if(getTime()!=null){
			formatedDate = formatedDate+" "+timeFormat.format(getTime());
		}else{
			formatedDate = formatedDate+" TBD";
		}
		
		return formatedDate;
	}
	
	@Transient
	public String getFormatedAdmitoneId() {
		if(getAdmitoneId()==null){
			formatedAdmitoneId = "False";
		}else{
			formatedAdmitoneId = "True";
		}
		return formatedAdmitoneId;
	}		
	
	@Transient
	public String getCatGroupName() {
		catGroupName = "";
		if(this.getVenueCategory()!=null){
			catGroupName = this.getVenueCategory().getCategoryGroup();
		}
		if(catGroupName.isEmpty()){
			return "No Map";
		}else{
		File file = new File("C:/TMATIMAGESFINAL/" + getVenueId() + "_" +catGroupName+ ".gif");		
		if (!file.exists()) {
			return "No Map";
			}
		}
		return catGroupName;
	}
	
	@Transient
	public String toString() {
		String thisEvent = "Event[EventId:" + id 
		+ ", EventName: " + name 
		+ ", EventDate: " + localDate 
		+ ", Venue: " + this.getFormattedVenueDescription() + "]";
		
		return thisEvent;
	}
	
	@Transient
	public Collection<String> getEnabledCrawlSites() {
		return enabledCrawlSites;
	}

	public void setEnabledCrawlSites(Collection<String> enabledCrawlSites) {
		this.enabledCrawlSites = enabledCrawlSites;
	}

	@Transient
	public Collection<String> getDisabledCrawlSites() {
		return disabledCrawlSites;
	}

	public void setDisabledCrawlSites(Collection<String> disabledCrawlSites) {
		this.disabledCrawlSites = disabledCrawlSites;
	}

	@Transient
	public VenueCategory getVenueCategory() {
		if(this.venueCategoryId!=null && this.venueCategory==null){
			venueCategory = TMATDAORegistry.getVenueCategoryDAO().get(venueCategoryId); 
		}
		return venueCategory;
	}

	public void setVenueCategory(VenueCategory venueCategory) {
		this.venueCategory = venueCategory;
	}
	/*
	@ManyToOne
	@JoinColumn(name="secondary_venue_category_id")
	public VenueCategory getSecondaryVenueCategory() {
		return secondaryVenueCategory;
	}

	public void setSecondaryVenueCategory(VenueCategory secondaryVenueCategory) {
		this.secondaryVenueCategory = secondaryVenueCategory;
	}*/
	@Column(name="no_price")
	public Boolean getNoPrice() {
		return noPrice;
	}

	public void setNoPrice(Boolean noPrice) {
		this.noPrice = noPrice;
	}

	@Transient
	public String getAdmitoneEventDetails() {
		return admitoneEventDetails;
	}

	public void setAdmitoneEventDetails(String admitoneEventDetails) {
		this.admitoneEventDetails = admitoneEventDetails;
	}
	
	
	@Transient
	public Integer getTempArtistId() {
		
		if(tempArtistId == null){
			this.tempArtistId=artistId;
		}
		return tempArtistId;
	}

	public void setTempArtistId(Integer tempArtistId) {
		this.tempArtistId = tempArtistId;
	}

	@Transient
	public Integer getTempVenueId() {
		if(tempVenueId == null){
			this.tempVenueId=venueId;
		}
		return tempVenueId;
	}

	public void setTempVenueId(Integer tempVenueId) {
		this.tempVenueId = tempVenueId;
	}

	
	@Transient
	public String getTempEventDateStr() {
		return tempEventDateStr;
	}

	public void setTempEventDateStr(String tempEventDateStr) {
		this.tempEventDateStr = tempEventDateStr;
	}

	
	@Transient
	public String getTempEventTimeStr() {
		return tempEventTimeStr;
	}

	public void setTempEventTimeStr(String tempEventTimeStr) {
		this.tempEventTimeStr = tempEventTimeStr;
	}
	
	
	
}