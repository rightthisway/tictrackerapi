package com.rtw.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.rtw.tmat.dao.implementation.TMATDAORegistry;

@Entity
@Table(name="category_synonym")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public class CategorySynonym implements Serializable {
	private Integer id;
	private Integer catId;
	private String startSynonym;
	private String endSynonym;

	public CategorySynonym(Integer id, Integer catId, String startSynonym, String endSynonym) {
		this.id = id;
		this.catId = catId;
		this.startSynonym = startSynonym;
		this.endSynonym = endSynonym;
	}

	public CategorySynonym() {	}

	@Id
	@Column(name="id")
    @GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="category_id")
	public Integer getCatId() {
		return catId;
	}
	
	public void setCatId(Integer catId) {
		this.catId = catId;
	}
	
	@Column(name="start_synonym")
	public String getStartSynonym() {
		return startSynonym;
	}
	
	public void setStartSynonym(String startSynonym) {
		this.startSynonym = startSynonym;
	}
	
	@Column(name="end_synonym")
	public String getEndSynonym() {
		return endSynonym;
	}
	
	public void setEndSynonym(String endSynonym) {
		this.endSynonym = endSynonym;
	}

	@Transient
	public Category getCategory() {
		return TMATDAORegistry.getCategoryDAO().get(catId);
	}

	public String toString() {
		
		String thisObject = "CategorySynonym[ "
			+ "id:" + id
			+ " categoryId: " + catId
			+ " start synonym: " + startSynonym 
			+ " end synonym: " + endSynonym + "]";
	
	return thisObject;
	}

}
