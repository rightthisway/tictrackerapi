package com.rtw.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import com.rtw.tmat.dao.implementation.TMATDAORegistry;
import com.rtw.tmat.enums.ArtistStatus;


@MappedSuperclass
public abstract class BaseArtist implements Serializable {
	private Integer id;
	private String name;
//	private Integer stubhubId;
	private ArtistStatus artistStatus;
	private Integer grandChildTourCategoryId;
	private GrandChildTourCategory grandChildTourCategory;
	public BaseArtist() {
		this.artistStatus = ArtistStatus.ACTIVE;
	}
	
	public BaseArtist(String name,Integer grandChildTourCategoryId,ArtistStatus artistStatus) {
		this.artistStatus = artistStatus;
		this.name = name;
		this.grandChildTourCategoryId = grandChildTourCategoryId;
	}
	
	@Id
	@Column(name="id")
    @GeneratedValue(strategy=GenerationType.AUTO)	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="name")
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	/*@Column(name="stubhub_id")
	public Integer getStubhubId() {
		return stubhubId;
	}

	public void setStubhubId(Integer stubhubId) {
		this.stubhubId = stubhubId;
	}*/

	@Column(name="artist_status")
	@Enumerated(EnumType.ORDINAL)
	public ArtistStatus getArtistStatus() {
		return artistStatus;
	}

	public void setArtistStatus(ArtistStatus artistStatus) {
		this.artistStatus = artistStatus;
	}

	
	/*@Transient
	public Collection<Tour> getTours() {
		return DAORegistry.getTourDAO().getAllToursByArtist(id);
	}
	
	@Transient
	public int getTourCount() {
		return DAORegistry.getTourDAO().getTourCount(id);
	}*/

	@Transient
	public GrandChildTourCategory getGrandChildTourCategory() {
		if(grandChildTourCategory!=null){
			return grandChildTourCategory;
		}
		if(grandChildTourCategoryId!=null){
			grandChildTourCategory = TMATDAORegistry.getGrandChildTourCategoryDAO().get(grandChildTourCategoryId);
		}
		return grandChildTourCategory;
		
	}

	public void setGrandChildTourCategory(GrandChildTourCategory grandChildTourCategory) {
		this.grandChildTourCategory = grandChildTourCategory;
	}

	@Column(name="grand_child_category_id")
	public Integer getGrandChildTourCategoryId() {
		return grandChildTourCategoryId;
	}

	public void setGrandChildTourCategoryId(Integer grandChildTourCategoryId) {
		this.grandChildTourCategoryId = grandChildTourCategoryId;
	}
	
}
