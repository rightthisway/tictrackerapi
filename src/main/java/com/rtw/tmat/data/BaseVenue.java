package com.rtw.tmat.data;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import com.rtw.tmat.enums.TourType;


@MappedSuperclass
public abstract class BaseVenue implements Serializable {
	protected Integer id;
	protected String building;
	protected String city;
	protected String state;
	protected String country;
	protected String zipcode;
	protected Integer stubhubId;
	protected Integer tnVenueId;
	protected TourType venueType;
	protected List venueCategoryList;
	@Id
	@Column(name="id")
    @GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="building")
	public String getBuilding() {
		return building;
	}

	public void setBuilding(String building) {
		this.building = building;
	}
	
	@Column(name="city")
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	@Column(name="state")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	@Column(name="country")
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}

	@Column(name="stubhub_id")
	public Integer getStubhubId() {
		return stubhubId;
	}

	public void setStubhubId(Integer stubhubId) {
		this.stubhubId = stubhubId;
	}
	@Transient
	public List getVenueCategoryList() {
		return venueCategoryList;
	}
	public void setVenueCategoryList(List venueCategoryList) {
		this.venueCategoryList = venueCategoryList;
	}
	@Transient
	public String getLocation() {
		String location = "";
		if (building != null) {
			location += building + ", ";
		}
		
		if (city != null) {
			location += city + ", ";
		}

		if (state != null) {
			location += state + ", ";
		}

		if (country != null) {
			location += country + ", ";
		}

		if (!location.isEmpty()) {
			location = location.substring(0, location.length() - 2);
		}
		
		return location;
	}
	
	@Column(name="venue_type")
	@Enumerated(EnumType.STRING)
	public TourType getVenueType() {
		return venueType;
	}
	public void setVenueType(TourType venueType) {
		this.venueType = venueType;
	}
	
	@Column(name="tn_venue_id")
	public Integer getTnVenueId() {
		return tnVenueId;
	}
	public void setTnVenueId(Integer tnVenueId) {
		this.tnVenueId = tnVenueId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((building == null) ? 0 : building.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result
				+ ((stubhubId == null) ? 0 : stubhubId.hashCode());
		result = prime * result
				+ ((tnVenueId == null) ? 0 : tnVenueId.hashCode());
		result = prime * result
				+ ((venueType == null) ? 0 : venueType.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaseVenue other = (BaseVenue) obj;
		if (building == null) {
			if (other.building != null)
				return false;
		} else if (!building.equals(other.building))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (stubhubId == null) {
			if (other.stubhubId != null)
				return false;
		} else if (!stubhubId.equals(other.stubhubId))
			return false;
		if (tnVenueId == null) {
			if (other.tnVenueId != null)
				return false;
		} else if (!tnVenueId.equals(other.tnVenueId))
			return false;
		if (venueType != other.venueType)
			return false;
		return true;
	}
	
	@Column(name="postal_code")
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	
	
}
