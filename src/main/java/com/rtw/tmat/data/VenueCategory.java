package com.rtw.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="venue_category")
public class VenueCategory implements Serializable{

	private Integer id;
	private String categoryGroup;
	private Venue venue;
	private Integer venueCapacity;
	private Date lastUpdatedDate;
	private String lastUpdatedBy;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="category_group")
	public String getCategoryGroup() {
		return categoryGroup;
	}
	public void setCategoryGroup(String categoryGroup) {
		this.categoryGroup = categoryGroup;
	}
	
	@ManyToOne
	@JoinColumn(name="venue_id")
	public Venue getVenue() {
		return venue;
	}
	public void setVenue(Venue venue) {
		this.venue = venue;
	}
	
	@Column(name="venue_capacity")
	public Integer getVenueCapacity() {
		return venueCapacity;
	}
	public void setVenueCapacity(Integer venueCapacity) {
		this.venueCapacity = venueCapacity;
	}
	
	@Column(name="last_updated_date")
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	
	@Column(name="last_updated_by")
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	
}
