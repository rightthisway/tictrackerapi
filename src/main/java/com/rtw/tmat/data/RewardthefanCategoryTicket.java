package com.rtw.tmat.data;



import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.enums.TicketDeliveryType;


@Entity
@Table(name="rewardthefan_category_ticket")
public class RewardthefanCategoryTicket implements Serializable{


	private Integer id;
	private Integer eventId;
	private Integer rtfCatsId;
	private Integer ticketId;
	private String itemId;
	private Integer quantity;
	private Ticket ticket;
	private String status;
	private Double purPrice;
	private Double tmatPrice;
	
	private Double rtfPrice;

	private Integer baseTicketOne;
	private Integer baseTicketTwo;
	private String priceHistory;
	private Double actualPrice;
	private Boolean isEdited=false;
	
	private Date createdDate;
	private Date lastUpdated;
	private String ticketIdHistory;
	private String baseTicketOneHistory;
	private String baseTicketTwoHistory;
	
	private Double baseTicketOnePurPrice;
	private Double baseTicketTwoPurPrice;
	private String reason;
	
	
	
	public RewardthefanCategoryTicket() {
		// TODO Auto-generated constructor stub
	}
	
	public RewardthefanCategoryTicket(RewardthefanCategoryTicket catTicket) {
		this.eventId = catTicket.getEventId();
		this.ticketId = catTicket.getTicketId();
		this.itemId = catTicket.getItemId();
		this.baseTicketOne = catTicket.getBaseTicketOne();
		this.baseTicketTwo = catTicket.getBaseTicketTwo();
		this.actualPrice = catTicket.getActualPrice();
		this.status = "ACTIVE";
		this.baseTicketOnePurPrice = catTicket.getBaseTicketOnePurPrice();
		this.baseTicketTwoPurPrice = catTicket.getBaseTicketTwoPurPrice();
		this.purPrice = catTicket.getPurPrice();
		
		this.ticket = catTicket.getTicket();
		this.rtfCatsId = catTicket.getRtfCatsId();
		this.quantity = catTicket.getQuantity();
		this.tmatPrice = catTicket.getTmatPrice();
	}
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="basetickettwo_history")
	public String getBaseTicketTwoHistory() {
		return baseTicketTwoHistory;
	}
	public void setBaseTicketTwoHistory(String baseTicketTwoHistory) {
		this.baseTicketTwoHistory = baseTicketTwoHistory;
	}
	
	
	@Column(name="baseticketone_history")
	public String getBaseTicketOneHistory() {
		return baseTicketOneHistory;
	}
	public void setBaseTicketOneHistory(String baseTicketOneHistory) {
		this.baseTicketOneHistory = baseTicketOneHistory;
	}
	
	@Column(name="ticketid_history")
	public String getTicketIdHistory() {
		return ticketIdHistory;
	}
	public void setTicketIdHistory(String ticketIdHistory) {
		this.ticketIdHistory = ticketIdHistory;
	}
	@Column(name="actual_price")
	public Double getActualPrice() {
		return actualPrice;
	}
	public void setActualPrice(Double actualPrice) {
		this.actualPrice = actualPrice;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="ticket_id")
	public Integer getTicketId() {
		return ticketId;
	}
	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}

	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	
	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="base_ticket2")
	public Integer getBaseTicketTwo() {
		return baseTicketTwo;
	}
	public void setBaseTicketTwo(Integer baseTicketTwo) {
		this.baseTicketTwo = baseTicketTwo;
	}
	
	@Column(name="price_history")
	public String getPriceHistory() {
		return priceHistory;
	}
	public void setPriceHistory(String priceHistory) {
		this.priceHistory = priceHistory;
	}
	
	
	@Column(name="base_ticket1")
	public Integer getBaseTicketOne() {
		if(baseTicketOne==null){
			baseTicketOne=0;
		}
		return baseTicketOne;
	}
	public void setBaseTicketOne(Integer baseTicketOne) {
		this.baseTicketOne = baseTicketOne;
	}

	
	@Column(name="item_id")
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	

	@Transient
	public Double getPurPrice() {
		return purPrice;
	}
	public void setPurPrice(Double purPrice) {
		this.purPrice = purPrice;
	}
	
	@Transient
	public Boolean getIsEdited() {
		return isEdited;
	}
	public void setIsEdited(Boolean isEdited) {
		this.isEdited = isEdited;
	}
	
	@Transient
	public Double getBaseTicketOnePurPrice() {
		return baseTicketOnePurPrice;
	}
	
	public void setBaseTicketOnePurPrice(Double baseTicketOnePurPrice) {
		this.baseTicketOnePurPrice = baseTicketOnePurPrice;
	}
	
	@Transient
	public Double getBaseTicketTwoPurPrice() {
		return baseTicketTwoPurPrice;
	}
	public void setBaseTicketTwoPurPrice(Double baseTicketTwoPurPrice) {
		this.baseTicketTwoPurPrice = baseTicketTwoPurPrice;
	}
	
	@Column(name="reason")
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Column(name="rtf_cats_id")
	public Integer getRtfCatsId() {
		return rtfCatsId;
	}

	public void setRtfCatsId(Integer rtfCatsId) {
		this.rtfCatsId = rtfCatsId;
	}

	@Column(name="quantity")
	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Column(name="rtf_price")
	public Double getRtfPrice() {
		return rtfPrice;
	}

	public void setRtfPrice(Double rtfPrice) {
		this.rtfPrice = rtfPrice;
	}

	@Column(name="tmat_price")
	public Double getTmatPrice() {
		return tmatPrice;
	}

	public void setTmatPrice(Double tmatPrice) {
		this.tmatPrice = tmatPrice;
	}

	@Transient
	public void createCategoryTicket(Ticket ticket) {
		this.eventId = ticket.getEventId();
		this.ticketId = ticket.getId();
		this.itemId = ticket.getItemId();
//		this.lastRow = ticket.getCategoryMapping().getLastRow().toUpperCase();
//		this.rowRange = ticket.getCategoryMapping().getRowRange().toUpperCase();
		this.purPrice = ticket.getPurchasePrice();	
		this.status = "ACTIVE";
		this.ticket = ticket;
		this.tmatPrice = ticket.getPurchasePrice();
	}
	
	@Transient
	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

}

