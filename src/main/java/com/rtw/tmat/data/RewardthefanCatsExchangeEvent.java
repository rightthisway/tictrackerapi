package com.rtw.tmat.data;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.rtw.tmat.dao.implementation.TMATDAORegistry;
import com.rtw.tmat.utils.Util;
@Entity
@Table(name="rewardthefan_exchange_event")
public class RewardthefanCatsExchangeEvent  implements Serializable{
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		
	private Integer id;
	private Integer eventId;
	@JsonIgnore
	private Event event;
	private String status;
	private String lastUpdatedBy;
	@JsonIgnore
	private Date lastUpdatedDate;
	private Integer excludeHoursBeforeEvent;
	
	private String eventName;
	@JsonIgnore
	private Date eventDate;
	@JsonIgnore
	private Time eventTime;
	private Integer venueId;
	private String building;
	private String eventDateStr;
	private String eventTimeStr;
	private String lastUpdatedDateStr;
	private String city;
	private String state;
	private String country;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)  
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="last_updated_by")
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	
	@Column(name="last_updated_date")
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	
	@Column(name="exclude_hours_before_event")
	public Integer getExcludeHoursBeforeEvent() {
		return excludeHoursBeforeEvent;
	}
	public void setExcludeHoursBeforeEvent(Integer excludeHoursBeforeEvent) {
		this.excludeHoursBeforeEvent = excludeHoursBeforeEvent;
	}
	@Transient
	public Event getEvent() {
		if(event==null){
			if(eventId==null){
				return null;
			}
			event = TMATDAORegistry.getEventDAO().get(eventId);
		}
		return event;
	}
	public void setEvent(Event event) {
		this.event = event;
	}
	@Transient
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	@Transient
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	
	@Transient
	public Time getEventTime() {
		return eventTime;
	}
	public void setEventTime(Time eventTime) {
		this.eventTime = eventTime;
	}
	
	@Transient
	public Integer getVenueId() {
		return venueId;
	}
	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}
	
	@Transient
	public String getBuilding() {
		return building;
	}
	public void setBuilding(String building) {
		this.building = building;
	}
	
	@Transient
	public String getEventDateStr() {
		if(eventDateStr != null){
			return eventDateStr;
		}else if(getEventDate()==null){
			return "TBD";
		}
		return Util.formatDateToMonthDateYear(getEventDate());
	}
	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}
	
	@Transient
	public String getEventTimeStr() {
		if(eventTimeStr != null){
			return eventTimeStr;
		}else if(getEventTime()==null){
			return "TBD";
		}
		return Util.formatTimeToHourMinutes(getEventTime());
	}
	public void setEventTimeStr(String eventTimeStr) {
		this.eventTimeStr = eventTimeStr;
	}
	
	@Transient
	public String getLastUpdatedDateStr() {
		if(lastUpdatedDateStr != null){
			return lastUpdatedDateStr;
		}
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(getLastUpdatedDate());
	}
	public void setLastUpdatedDateStr(String lastUpdatedDateStr) {
		this.lastUpdatedDateStr = lastUpdatedDateStr;
	}
	@Transient
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	@Transient
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@Transient
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	
}
