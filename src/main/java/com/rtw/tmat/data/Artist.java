package com.rtw.tmat.data;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.rtw.tmat.dao.implementation.TMATDAORegistry;



@Entity
@Table(name="artist")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public class Artist extends BaseArtist {
	List<Event> events ;
	public Artist() {
		//this.setArtistStatus(ArtistStatus.ACTIVE);
		super();
	}
	
	
	@Transient
	public List<Event> getEvents(){
		if(events==null || events.isEmpty()){
			events = (List<Event>)TMATDAORegistry.getEventDAO().getAllActiveEventsByArtistId(this.getId());
		}
		return events;
	}
	public void setEvents(List<Event> events) {
		this.events = events;
	}
}
