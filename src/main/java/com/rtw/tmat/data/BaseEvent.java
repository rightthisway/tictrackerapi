package com.rtw.tmat.data;

import java.io.Serializable;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import com.rtw.tmat.dao.implementation.TMATDAORegistry;
import com.rtw.tmat.enums.CreationType;
import com.rtw.tmat.enums.EventStatus;
import com.rtw.tmat.enums.TourType;


@MappedSuperclass
public abstract class BaseEvent implements Serializable {
	protected static DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	protected static DateFormat timeFormat = new SimpleDateFormat("HH:mm zzz");
	
	protected static final long serialVersionUID = -7402732543016575381L;
	protected Integer id;
//	protected Integer tourId;
	protected Date localDate;
	protected Time localTime;
	protected Integer venueId;
	protected Integer venueCategoryId;
	protected String timeZoneId;
	protected String name;
	protected boolean timeTbd = false;
	protected boolean dateTbd = false;
	protected EventStatus eventStatus;
	protected Integer stubhubId;
	protected Integer seatwaveId;
	protected java.util.Date creationDate;
	protected java.util.Date lastUpdate;
	protected CreationType creationType;
	protected Integer admitoneId;
	protected TourType eventType;
//	protected Tour tour;
	protected Artist artist;
	protected Integer artistId;
	
	protected Boolean zoneEvent;
	protected String eventOverridden;
	
	protected EventTicketStat eventTicketStat;
	protected String updatedBy;
	protected Integer brokerId;
	protected String brokerStatus;
	
	public BaseEvent() {
		stubhubId = null;
		seatwaveId = null;
		creationDate = new java.util.Date();
		lastUpdate = new java.util.Date();
		creationType = CreationType.MANUAL;
	}
	
	@Id
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	
	@Column(name="event_type")
	@Enumerated(EnumType.STRING)
	public TourType getEventType() {
		if (eventType != null) {
			return eventType;
		}
		return TourType.OTHER;
	}

	public void setEventType(TourType eventType) {
		if (eventType == null) {
			eventType = TourType.OTHER;
		}
		this.eventType = eventType;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	/*@Column(name="tour_id")
	@Index(name="tourIdIndex")
	public Integer getTourId() {
		return tourId;
	}
	
	public void setTourId(Integer tourId) {
		this.tourId = tourId;
	}*/
	
	// store date in UTC
	@Column(name="event_date", columnDefinition="DATE")
	public Date getLocalDate() {
		return localDate;
	}
	
	public void setLocalDate(Date date) {
		this.localDate = date;
	}

	@Transient
	public java.util.Date getLocalDateTime() {
		if (localDate != null){
			java.util.Date dateTime = new java.util.Date(localDate.getTime());
			if(localTime != null){
				dateTime.setHours(localTime.getHours());
				dateTime.setMinutes(localTime.getMinutes());
				dateTime.setSeconds(localTime.getSeconds());
			}
			return dateTime;
		}else{
			return null;
		}
	}


	@Column(name="timezone")
	public String getTimeZoneId() {
		return timeZoneId;
	}

	public void setTimeZoneId(String timeZoneId) {
		this.timeZoneId = timeZoneId;
	}

	@Column(name="name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Transient
	public TimeZone getTimeZone() {
		if (timeZoneId == null) {
			return TimeZone.getTimeZone("America/New_York");
		}
		return TimeZone.getTimeZone(timeZoneId);
	}
	
	@Transient
	public String getTimeZoneSymbol() {
		return getTimeZone().getDisplayName(false, TimeZone.SHORT);
	}
	
	// return date in UTC
	@Transient
	public java.util.Date getDate() {
		if(localDate != null){
			Calendar calendar = Calendar.getInstance(getTimeZone());
			calendar.setTime(localDate);
			return calendar.getTime();
		}else{
			return null;
		}
	}

	// return time in UTC
	@Transient
	public Time getTime() {
		if (localTime == null) {
			return null;
		}
		Calendar calendar = Calendar.getInstance(getTimeZone());
		calendar.setTime(localTime);
		return new Time(calendar.getTime().getTime());
	}

	@Column(name="event_time")
	public Time getLocalTime() {
		return localTime;
	}

	public void setLocalTime(Time localTime) {
		this.localTime = localTime;
	}

	@Transient
	public boolean getTimeTbd() {
		return timeTbd;
	}

	public void setTimeTbd(boolean timeTbd) {
		this.timeTbd = timeTbd;
	}
	
	@Transient
	public boolean getDateTbd() {
		return dateTbd;
	}

	public void setDateTbd(boolean dateTbd) {
		this.dateTbd = dateTbd;
	}
	
	@Column(name="stubhub_id")
	public Integer getStubhubId() {
		return stubhubId;
	}

	public void setStubhubId(Integer stubhubId) {
		this.stubhubId = stubhubId;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="event_status")
	public EventStatus getEventStatus() {
		return eventStatus;
	}

	public void setEventStatus(EventStatus eventStatus) {
		this.eventStatus = eventStatus;
	}
	
	@Transient
	protected abstract BaseVenue getVenue();
	
	@Transient
	public String getFormattedEventDate() {
		// do this because dateFormat.format is not thread safe
		synchronized(BaseEvent.class) {
			return ((getDate() == null)?"TBD":(dateFormat.format(getDate()))) + " " + ((getTime() == null)?"TBD":(timeFormat.format(getTime())));
		}
	}
	
	/* COMMENTED
	 * @Transient
	public int getCrawlCount() {
		return TMATDAORegistry.getTicketListingCrawlDAO().getTicketListingCrawlCountByEvent(id);
	}*/
	
	@Column(name="creation_date")
	public java.util.Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(java.util.Date creationDate) {
		this.creationDate = creationDate;
	}

	@Column(name="last_update")
	public java.util.Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(java.util.Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	@Column(name="creation_type")
	@Enumerated(EnumType.STRING)
	public CreationType getCreationType() {
		return creationType;
	}

	public void setCreationType(CreationType creationType) {
		this.creationType = creationType;
	}
	
	@Column(name="admitone_id")
	public Integer getAdmitoneId() {
		return admitoneId;
	}

	@Column(name="seatwave_id")
	public Integer getSeatwaveId() {
		return seatwaveId;
	}

	public void setSeatwaveId(Integer seatwaveId) {
		this.seatwaveId = seatwaveId;
	}

	public void setAdmitoneId(Integer admitoneId) {
		this.admitoneId = admitoneId;
	}
	
	/*@Column(name="zone_event")
	public Boolean getZoneEvent() {
		return zoneEvent;
	}

	public void setZoneEvent(Boolean zoneEvent) {
		this.zoneEvent = zoneEvent;
	}*/

	@Column(name="event_over_riden")
	public String getEventOverridden() {
		return eventOverridden;
	}

	public void setEventOverridden(String eventOverridden) {
		this.eventOverridden = eventOverridden;
	}

	@Column(name="venue_category_id")
	public Integer getVenueCategoryId() {
		return venueCategoryId;
	}

	public void setVenueCategoryId(Integer venueCategoryId) {
		this.venueCategoryId = venueCategoryId;
	}
	@Transient
	public EventTicketStat getEventTicketStat() {
		return eventTicketStat;
	}
	
	public void setEventTicketStat(EventTicketStat eventTicketStat) {
		this.eventTicketStat = eventTicketStat;
	}

	/*@Transient
	public Tour getTour() {
		if (tour == null) {
			tour = DAORegistry.getTourDAO().get(tourId);
		}
		return tour;
	}*/

	public static class EventTicketStat {
		private int ticketListingCount;
		private int ticketCount;
		
		public EventTicketStat(int ticketListingCount, int ticketCount) {
			this.ticketListingCount = ticketListingCount;
			this.ticketCount = ticketCount;
		}

		public int getTicketListingCount() {
			return ticketListingCount;
		}

		public int getTicketCount() {
			return ticketCount;
		}				
	}
	@Column(name="updated_by")
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column(name = "broker_id")
	public Integer getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}
	@Column(name = "broker_status")
	public String getBrokerStatus() {
		return brokerStatus;
	}

	public void setBrokerStatus(String brokerStatus) {
		this.brokerStatus = brokerStatus;
	}

	@Transient
	public Artist getArtist() {
		if(artist!=null){
			return artist;
		}
		if(artistId !=null)
			artist = TMATDAORegistry.getArtistDAO().get(artistId);
		return artist;
	}

	public void setArtist(Artist artist) {
		this.artist = artist;
	}

	@Column(name = "artist_id")
	public Integer getArtistId() {
		return artistId;
	}

	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}

}
