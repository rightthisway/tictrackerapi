package com.rtw.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="child_tour_category")
public class ChildTourCategory implements Serializable{
	private Integer id;
	private String name;
	private TourCategory tourCategory;
	
	
	public ChildTourCategory(){
		
	}
	
	@Id
	@Column(name="id")
    @GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@ManyToOne
	@JoinColumn(name="tour_category_id")
	public TourCategory getTourCategory() {
		return tourCategory;
	}
	public void setTourCategory(TourCategory tourCategory) {
		this.tourCategory = tourCategory;
	}

}
