package com.rtw.tmat.data;


import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name="venue")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public class Venue extends BaseVenue {
		
}
