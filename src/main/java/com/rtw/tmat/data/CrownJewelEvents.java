package com.rtw.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.dao.implementation.TMATDAORegistry;

@Entity
@Table(name="crownjewel_events")
public class CrownJewelEvents  implements Serializable {
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private Integer eventId;
	private Event event;
	private String exposure;
	private Double rptFactor;
	private Double priceBreakup;
	private Double lowerMarkup;
	private Double upperMarkup;
	private Double lowerShippingFees;
	private Double upperShippingFees;
	private String status;
	private Integer shippingMethod;
	private Integer nearTermDisplayOption;
	private Boolean allowSectionRange;
	private Integer shippingDays;
	private String lastUpdatedBy;
	private Date lastUpdatedDate;
	private String parentType;
	private Integer zoneTicketBrokerId;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)  
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Transient
	public Event getEvent() {
		if(event==null){
			if(eventId==null){
				return null;
			}
			try {
				event = TMATDAORegistry.getEventDAO().getEventById(eventId);
			}catch (Exception e) {
				
			}
		}
		return event;
	}
	public void setEvent(Event event) {
		this.event = event;
	}
	
	@Column(name="exposure")
	public String getExposure() {
		return exposure;
	}
	public void setExposure(String exposure) {
		this.exposure = exposure;
	}
	
	@Column(name="rpt_factor")
	public Double getRptFactor() {
		return rptFactor;
	}
	public void setRptFactor(Double rptFactor) {
		this.rptFactor = rptFactor;
	}
	
	@Column(name="price_breakup")
	public Double getPriceBreakup() {
		return priceBreakup;
	}
	public void setPriceBreakup(Double priceBreakup) {
		this.priceBreakup = priceBreakup;
	}
	
	@Column(name="lower_markup")
	public Double getLowerMarkup() {
		return lowerMarkup;
	}
	public void setLowerMarkup(Double lowerMarkup) {
		this.lowerMarkup = lowerMarkup;
	}
	
	@Column(name="upper_markup")
	public Double getUpperMarkup() {
		return upperMarkup;
	}
	public void setUpperMarkup(Double upperMarkup) {
		this.upperMarkup = upperMarkup;
	}
	
	@Column(name="lower_shipping_fees")
	public Double getLowerShippingFees() {
		return lowerShippingFees;
	}
	public void setLowerShippingFees(Double lowerShippingFees) {
		this.lowerShippingFees = lowerShippingFees;
	}
	
	@Column(name="upper_shipping_fees")
	public Double getUpperShippingFees() {
		return upperShippingFees;
	}
	public void setUpperShippingFees(Double upperShippingFees) {
		this.upperShippingFees = upperShippingFees;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="shipping_method")
	public Integer getShippingMethod() {
		return shippingMethod;
	}
	public void setShippingMethod(Integer shippingMethod) {
		this.shippingMethod = shippingMethod;
	}
	
	@Column(name="near_term_display_option")
	public Integer getNearTermDisplayOption() {
		return nearTermDisplayOption;
	}
	public void setNearTermDisplayOption(Integer nearTermDisplayOption) {
		this.nearTermDisplayOption = nearTermDisplayOption;
	}
	
	@Column(name="allow_section_range")
	public Boolean getAllowSectionRange() {
		return allowSectionRange;
	}
	public void setAllowSectionRange(Boolean allowSectionRange) {
		this.allowSectionRange = allowSectionRange;
	}
	
	@Column(name="shipping_days")
	public Integer getShippingDays() {
		return shippingDays;
	}
	public void setShippingDays(Integer shippingDays) {
		this.shippingDays = shippingDays;
	}

	@Column(name="last_updated_by")
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	
	@Column(name="last_updated_date")
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	
	@Column(name="parent_type")
	public String getParentType() {
		return parentType;
	}
	public void setParentType(String parentType) {
		this.parentType = parentType;
	}
	
	@Column(name="zoneticket_broker_id")
	public Integer getZoneTicketBrokerId() {
		return zoneTicketBrokerId;
	}
	public void setZoneTicketBrokerId(Integer zoneTicketBrokerId) {
		this.zoneTicketBrokerId = zoneTicketBrokerId;
	}
	
}

