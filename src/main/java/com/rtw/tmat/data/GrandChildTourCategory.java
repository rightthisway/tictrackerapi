package com.rtw.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="grand_child_tour_category")
public class GrandChildTourCategory implements Serializable{
	private Integer id;
	private String name;
	private ChildTourCategory childTourCategory;
	
	public GrandChildTourCategory(){
		
	}
	
	
	@Id
	@Column(name="id")
    @GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@ManyToOne
	@JoinColumn(name="child_category_id")
	public ChildTourCategory getChildTourCategory() {
		return childTourCategory;
	}
	public void setChildTourCategory(ChildTourCategory childTourCategory) {
		this.childTourCategory = childTourCategory;
	}

}
