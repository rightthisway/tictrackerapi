package com.rtw.tracker.enums;

public enum RewardStatus {
	ACTIVE,PENDING,VOIDED,ORDERPENDING,PAID;
}
