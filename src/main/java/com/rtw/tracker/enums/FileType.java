package com.rtw.tracker.enums;

public enum FileType {

	ETICKET,BARCODE,QRCODE,OTHER,EGIFTCARD;
}
