package com.rtw.tracker.enums;

public enum LockedTicketStatus {
	ACTIVE,DELETED,SOLD,ALREADY_SOLD
}
