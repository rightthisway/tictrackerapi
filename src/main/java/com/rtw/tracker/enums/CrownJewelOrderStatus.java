package com.rtw.tracker.enums;

public enum CrownJewelOrderStatus {
	OUTSTANDING,APPROVED,REJECTED,PENDING
}
