package com.rtw.tracker.enums;

public enum PromotionalType {
	ARTIST,GRANDCHILD,CHILD,PARENT,VENUE,ALL,NORMAL_PROMO,CUSTOMER_PROMO,CUSTOMER_REFERAL_DISCOUNT_CODE,AFFILIATE_PROMO_CODE,
	MOBILE_APP_DISCOUNT,FIRST_CHECKOUT_OFFER,EVENT
}
