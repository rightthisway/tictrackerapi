package com.rtw.tracker.enums;

public enum OrderStatus {
	PROCESSED,REJECTED,ACTIVE,PAYMENT_PENDING,PAYMENT_FAILED,VOIDED;
}
