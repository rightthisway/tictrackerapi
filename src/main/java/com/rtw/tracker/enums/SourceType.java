package com.rtw.tracker.enums;

public enum SourceType {
	PROFILE_BASIC, PROFILE_FANDOM, VIDEO_LIKES, VIDEO_SHARES, VIDEO_WATCH, VIDEO_UPLOAD, REFERRAL, LIVE_POOLING,TICKETORDER,GIFTCARDORDER
}
