package com.rtw.tracker.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtw.tmat.utils.Constants;
import com.rtw.tmat.utils.Error;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.aws.AWSFileService;
import com.rtw.tracker.aws.AwsS3Response;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.dao.implementation.QuizDAORegistry;
import com.rtw.tracker.datas.Customer;
import com.rtw.tracker.datas.FanClub;
import com.rtw.tracker.datas.FanClubEvent;
import com.rtw.tracker.datas.FanClubMember;
import com.rtw.tracker.datas.FanClubPost;
import com.rtw.tracker.datas.FanClubPostComments;
import com.rtw.tracker.datas.FanClubVideo;
import com.rtw.tracker.datas.FanclubAbuseReported;
import com.rtw.tracker.datas.PollingVideoCategory;
import com.rtw.tracker.datas.PollingVideoInventory;
import com.rtw.tracker.pojos.CommonRespInfo;
import com.rtw.tracker.pojos.FanClubDTO;
import com.rtw.tracker.utils.FileUploadUtil;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.GridHeaderFiltersUtil;
import com.rtw.tracker.utils.GsonCustomConfig;
import com.rtw.tracker.utils.PaginationUtil;
import com.rtw.tracker.utils.URLUtil;
import com.rtw.tracker.utils.Util;

@Controller
public class FanClubApiController {
	
	SharedProperty sharedProperty;

	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}

	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}
	
	
	
	
	@RequestMapping(value="/GetFanClubs")
	public FanClubDTO getFanClubs(HttpServletRequest request, HttpServletResponse response){
		FanClubDTO responseDTO = new FanClubDTO();
		Error error = new Error();
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String status = request.getParameter("status");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getFanClubFIlter(headerFilter);
			/*if(status==null || status.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please send valid status.");
				responseDTO.setError(error);
				return responseDTO;
			}*/
			List<FanClub> clubs = QuizDAORegistry.getFanClubDAO().getAllFabClubs(filter, status, sharedProperty);
			List<PollingVideoCategory> categories = DAORegistry.getPollingVideoCategoryDAO().getAllPollingVideoCategory(new GridHeaderFilters(), "ACTIVE");
			responseDTO.setClubs(clubs);
			responseDTO.setCategories(categories);
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(clubs.size()));
			responseDTO.setDummyPagination(PaginationUtil.getDummyPaginationParameters(0));
			responseDTO.setStatus(1);
			if(clubs.isEmpty()){
				responseDTO.setStatus(1);
				responseDTO.setMessage("No Fan clubs found.");
				return responseDTO;
			}
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while fetching fan clubs.");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}
	
	
	@RequestMapping(value="/GetFanClubMembers")
	public FanClubDTO getFanClubMembers(HttpServletRequest request, HttpServletResponse response){
		FanClubDTO responseDTO = new FanClubDTO();
		Error error = new Error();
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String status = request.getParameter("status");
			String fanClubIdStr = request.getParameter("fanClubId");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getFanClubFIlter(headerFilter);
			/*if(status==null || status.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please send valid status.");
				responseDTO.setError(error);
				return responseDTO;
			}*/
			if(fanClubIdStr==null || fanClubIdStr.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Can not identify fan club id.");
				responseDTO.setError(error);
				return responseDTO;
			}
			Integer fanClubId = 0;
			try {
				fanClubId = Integer.parseInt(fanClubIdStr);
			} catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Invalid fan club id.");
				responseDTO.setError(error);
				return responseDTO;
			}
			List<FanClubMember> members = QuizDAORegistry.getFanClubMemberDAO().getAllFanClubMembers(filter, status, fanClubId, sharedProperty);
			responseDTO.setMembers(members);
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(members.size()));
			responseDTO.setStatus(1);
			if(members.isEmpty()){
				responseDTO.setStatus(1);
				responseDTO.setMessage("No Fan club members found.");
				return responseDTO;
			}
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while fetching fan club members.");
			responseDTO.setError(error);
			return responseDTO;
		}
	}
	
	
	
	@RequestMapping(value="/GetFanClubPosts")
	public FanClubDTO getFanClubPosts(HttpServletRequest request, HttpServletResponse response){
		FanClubDTO responseDTO = new FanClubDTO();
		Error error = new Error();
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String status = request.getParameter("status");
			String fanClubIdStr = request.getParameter("fanClubId");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getFanClubFIlter(headerFilter);
			/*if(status==null || status.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please send valid status.");
				responseDTO.setError(error);
				return responseDTO;
			}*/
			if(fanClubIdStr==null || fanClubIdStr.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Can not identify fan club id.");
				responseDTO.setError(error);
				return responseDTO;
			}
			Integer fanClubId = 0;
			try {
				fanClubId = Integer.parseInt(fanClubIdStr);
			} catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Invalid fan club id.");
				responseDTO.setError(error);
				return responseDTO;
			}
			List<FanClubPost> posts = QuizDAORegistry.getFanClubPostDAO().getAllFanClubPosts(filter, status, fanClubId, sharedProperty);
			responseDTO.setPosts(posts);
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(posts.size()));
			responseDTO.setStatus(1);
			if(posts.isEmpty()){
				responseDTO.setStatus(1);
				responseDTO.setMessage("No Fan club posts found.");
				return responseDTO;
			}
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while fetching fan club posts.");
			responseDTO.setError(error);
			return responseDTO;
		}
	}
	@RequestMapping(value="/GetFanClubPostComments")
	public FanClubDTO getFanClubPostComments(HttpServletRequest request, HttpServletResponse response){
		FanClubDTO responseDTO = new FanClubDTO();
		Error error = new Error();
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String status = request.getParameter("status");
			String commentTypeIdStr = request.getParameter("commentTypeId");
			String commentType = request.getParameter("commentType");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getFanClubFIlter(headerFilter);
			/*if(status==null || status.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please send valid status.");
				responseDTO.setError(error);
				return responseDTO;
			}*/
			if(commentType==null || commentType.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Comment Type not found.");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(commentTypeIdStr==null || commentTypeIdStr.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Comment Type id not found.");
				responseDTO.setError(error);
				return responseDTO;
			}
			Integer commentTypeId = 0;
			try {
				commentTypeId = Integer.parseInt(commentTypeIdStr);
			} catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Comment Type id is invalid.");
				responseDTO.setError(error);
				return responseDTO;
			}
			List<FanClubPostComments> posts = QuizDAORegistry.getFanClubPostCommentsDAO().getAllFanClubPostComments(filter, status, commentType,commentTypeId, sharedProperty);
			responseDTO.setPostComments(posts);
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(posts.size()));
			responseDTO.setStatus(1);
			if(posts.isEmpty()){
				responseDTO.setStatus(1);
				responseDTO.setMessage("No Fan club "+commentType+" comments found.");
				return responseDTO;
			}
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while fetching fan club post comments.");
			responseDTO.setError(error);
			return responseDTO;
		}
	}
	@RequestMapping(value="/UpdateFanClubPostComments")
	public FanClubDTO updateFanClubPostComments(HttpServletRequest request, HttpServletResponse response){
		FanClubDTO responseDTO = new FanClubDTO();
		Error error = new Error();
		try {
			String action  = request.getParameter("action");
			String commentTypeIdStr = request.getParameter("commentTypeId");
			String commentType = request.getParameter("commentType");
			String commentIdStr = request.getParameter("commentId");
			String userName = request.getParameter("userName"); 
			String msg = "";
			if(action==null || action.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Could not able to identify action to be perform.");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(commentType==null || commentType.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Comment Type not found.");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(commentTypeIdStr==null || commentTypeIdStr.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Comment Type id not found.");
				responseDTO.setError(error);
				return responseDTO;
			}
			Integer commentTypeId = 0;
			try {
				commentTypeId = Integer.parseInt(commentTypeIdStr);
			} catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Comment Type id is invalid.");
				responseDTO.setError(error);
				return responseDTO;
			}
			
			if(action.equalsIgnoreCase("DELETE")){
				if(commentIdStr==null || commentIdStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Comment id not found.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer commentId = 0;
				try {
					commentId = Integer.parseInt(commentIdStr);
				} catch (Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Comment id is invalid.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(commentType.equals("POST")) {
					FanClubPostComments comment = QuizDAORegistry.getFanClubPostCommentsDAO().get(commentId);
					if(comment == null){
						responseDTO.setStatus(0);
						error.setDescription("Fanclub Post Comment not found with given identifier.");
						responseDTO.setError(error);
						return responseDTO;
					}
					comment.setUpdatedBy(userName);
					comment.setUpdatedDate(new Date());
					comment.setCommentStatus("BLOCKED");
					QuizDAORegistry.getFanClubPostCommentsDAO().update(comment);
					
					msg = "Selected Post comment deleted successfully.";
					
					GridHeaderFilters filter = new GridHeaderFilters();
					filter.setStatus("ACTIVE");
					List<FanClubPostComments> comments = QuizDAORegistry.getFanClubPostCommentsDAO().getAllFanClubPostComments(filter, "ACTIVE",commentType, commentTypeId, sharedProperty);
					responseDTO.setPostComments(comments);
					responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(comments.size()));
					responseDTO.setStatus(1);
					return responseDTO;
				} else if (commentType.equals("VIDEO")) {
					FanClubPostComments comment = QuizDAORegistry.getFanClubPostCommentsDAO().get(commentId);
					if(comment == null){
						responseDTO.setStatus(0);
						error.setDescription("Fanclub Post Comment not found with given identifier.");
						responseDTO.setError(error);
						return responseDTO;
					}
					comment.setUpdatedBy(userName);
					comment.setUpdatedDate(new Date());
					comment.setCommentStatus("BLOCKED");
					QuizDAORegistry.getFanClubPostCommentsDAO().update(comment);
					
					msg = "Selected Video comment deleted successfully.";
					
					GridHeaderFilters filter = new GridHeaderFilters();
					filter.setStatus("ACTIVE");
					List<FanClubPostComments> comments = QuizDAORegistry.getFanClubPostCommentsDAO().getAllFanClubPostComments(filter, "ACTIVE",commentType, commentTypeId, sharedProperty);
					responseDTO.setPostComments(comments);
					responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(comments.size()));
					responseDTO.setStatus(1);
					return responseDTO;
				}
				
			}
			responseDTO.setStatus(0);
			responseDTO.setMessage("Could not able to identify action.");
			return responseDTO;
			
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured updating Fanclub post comments.");
			responseDTO.setError(error);
			return responseDTO;
		}
	}
	
	
	@RequestMapping(value="/GetFanClubEvents")
	public FanClubDTO getFanClubEvents(HttpServletRequest request, HttpServletResponse response){
		FanClubDTO responseDTO = new FanClubDTO();
		Error error = new Error();
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String status = request.getParameter("status");
			String fanClubIdStr = request.getParameter("fanClubId");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getFanClubFIlter(headerFilter);
			/*if(status==null || status.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please send valid status.");
				responseDTO.setError(error);
				return responseDTO;
			}*/
			if(fanClubIdStr==null || fanClubIdStr.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Can not identify fan club id.");
				responseDTO.setError(error);
				return responseDTO;
			}
			Integer fanClubId = 0;
			try {
				fanClubId = Integer.parseInt(fanClubIdStr);
			} catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Invalid fan club id.");
				responseDTO.setError(error);
				return responseDTO;
			}
			List<FanClubEvent> events = QuizDAORegistry.getFanClubEventDAO().getAllFanClubEvents(filter, status, fanClubId, sharedProperty);
			responseDTO.setEvents(events);
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(events.size()));
			responseDTO.setStatus(1);
			if(events.isEmpty()){
				responseDTO.setStatus(1);
				responseDTO.setMessage("No Fan club events found.");
				return responseDTO;
			}
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while fetching fan club events.");
			responseDTO.setError(error);
			return responseDTO;
		}
	}
	
	
	
	
	@RequestMapping(value="/GetFanClubVideos")
	public FanClubDTO getFanClubVideos(HttpServletRequest request, HttpServletResponse response){
		FanClubDTO responseDTO = new FanClubDTO();
		Error error = new Error();
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String status = request.getParameter("status");
			String fanClubIdStr = request.getParameter("fanClubId");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getFanClubFIlter(headerFilter);
			/*if(status==null || status.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please send valid status.");
				responseDTO.setError(error);
				return responseDTO;
			}*/
			if(fanClubIdStr==null || fanClubIdStr.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Can not identify fan club id.");
				responseDTO.setError(error);
				return responseDTO;
			}
			Integer fanClubId = 0;
			try {
				fanClubId = Integer.parseInt(fanClubIdStr);
			} catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Invalid fan club id.");
				responseDTO.setError(error);
				return responseDTO;
			}
			List<FanClubVideo> videos = QuizDAORegistry.getFanClubVideoDAO().getAllFanClubVideos(filter, status, fanClubId, sharedProperty);
			responseDTO.setVideos(videos);
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(videos.size()));
			responseDTO.setStatus(1);
			if(videos.isEmpty()){
				responseDTO.setStatus(1);
				responseDTO.setMessage("No Fan club videos found.");
				return responseDTO;
			}
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while fetching fan club videos.");
			responseDTO.setError(error);
			return responseDTO;
		}
	}
	
	
	@RequestMapping(value="/UpdateFanClubs")
	public FanClubDTO updateFanClubs(HttpServletRequest request, HttpServletResponse response){
		FanClubDTO responseDTO = new FanClubDTO();
		Error error = new Error();
		try {
			String action  = request.getParameter("action");
			String fanClubIdStr = request.getParameter("fanClubId");
			String userName = request.getParameter("userName"); 
			String msg = "";
			if(action==null || action.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Could not able to identify action to be perform.");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(action.equalsIgnoreCase("EDIT")){
				if(fanClubIdStr==null || fanClubIdStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Fan Club id not found.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer fabClubId = 0;
				try {
					fabClubId = Integer.parseInt(fanClubIdStr);
				} catch (Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Fan Club id is invalid.");
					responseDTO.setError(error);
					return responseDTO;
				}
				FanClub club = QuizDAORegistry.getFanClubDAO().get(fabClubId);
				if(club == null){
					responseDTO.setStatus(0);
					error.setDescription("Fan club not found with given identifier.");
					responseDTO.setError(error);
					return responseDTO;
				}
				responseDTO.setStatus(1);
				responseDTO.setClub(club);
				return responseDTO;
			}else if(action.equalsIgnoreCase("SAVE") || action.equalsIgnoreCase("UPDATE")){
				String title = request.getParameter("title");
				String description = request.getParameter("description");
				String category = request.getParameter("category");
				
				if(title==null || title.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Fan club title is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(description==null || description.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Fan club description is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(category==null || category.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Fan club category is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer categoryId = 0;
				try {
					categoryId = Integer.parseInt(category);
				} catch (Exception e) {
					e.printStackTrace();
					responseDTO.setStatus(0);
					error.setDescription("Fan club category is invalid.");
					responseDTO.setError(error);
					return responseDTO;
				}
				
				FanClub club = null;
				if(action.equalsIgnoreCase("UPDATE")){
					if(fanClubIdStr==null || fanClubIdStr.isEmpty()){
						responseDTO.setStatus(0);
						error.setDescription("Fan Club id not found.");
						responseDTO.setError(error);
						return responseDTO;
					}
					Integer fabClubId = 0;
					try {
						fabClubId = Integer.parseInt(fanClubIdStr);
					} catch (Exception e) {
						responseDTO.setStatus(0);
						error.setDescription("Fan Club id is invalid.");
						responseDTO.setError(error);
						return responseDTO;
					}
					club = QuizDAORegistry.getFanClubDAO().get(fabClubId);
					if(club == null){
						responseDTO.setStatus(0);
						error.setDescription("Fan club not found with given identifier.");
						responseDTO.setError(error);
						return responseDTO;
					}
					club.setUpdatedBy(userName);
					club.setUpdatedDate(new Date());
					msg = "Selected Fan club updated successfully.";
				}else if(action.equalsIgnoreCase("SAVE")){
					Customer cust = DAORegistry.getCustomerDAO().getCustByUserId(Constants.USERID);
					club = new FanClub();
					club.setCustomerId(cust!=null?cust.getId():-1);
					club.setCreatedBy(userName);
					club.setMemberCount(0);
					club.setCreatedDate(new Date());
					club.setStatus("ACTIVE");
					msg = "Fan club saved successfully.";
				}
				MultipartFile imageFile = null;
				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
				imageFile = multipartRequest.getFile("imageFile");
				if(imageFile == null && action.equalsIgnoreCase("SAVE")){
					responseDTO.setStatus(0);
					error.setDescription("Fan Club Image is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				
				club.setTitle(title);
				club.setDescription(description);
				club.setCategoryId(categoryId);
				QuizDAORegistry.getFanClubDAO().saveOrUpdate(club);
				
				if(imageFile!= null && imageFile.getSize() > 0){
					String fileName = imageFile.getOriginalFilename();
					fileName = FileUploadUtil.getFileNameWithoutSpecialCharaters(fileName);
					String ext =  fileName.substring(fileName.lastIndexOf("."));
					String path = URLUtil.FANCLUB_MEDIA+"FANCLUB_IMAGE_"+club.getId()+ext;
					
					File newFile = new File(path);
					newFile.createNewFile();
					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(newFile));
					FileCopyUtils.copy(imageFile.getInputStream(), stream);
					stream.close();
					//String serverFileName = FileUploadUtil.generateFileName(ext);
					
					AwsS3Response awsRsponse = AWSFileService.upLoadFileToS3(AWSFileService.CUSTOMER_MEDIA_BACKET,"FANCLUB_IMAGE_"+club.getId()+ext, AWSFileService.FANCLUB_IMAGE_FOLDER, newFile );
					if(null != awsRsponse && awsRsponse.getStatus() != 1){
						responseDTO.setStatus(0);
						error.setDescription("Fanclub Details save successfully, Error occured while uploading Fanclub image to S3.");
						responseDTO.setError(error);
						return responseDTO;
					}
					club.setImageUrl(AWSFileService.AWS_FANCLUB_MEDIA_URL+AWSFileService.FANCLUB_IMAGE_FOLDER+"/FANCLUB_IMAGE_"+club.getId()+ext);
					QuizDAORegistry.getFanClubDAO().update(club);
				}
				
				List<FanClub> clubs = QuizDAORegistry.getFanClubDAO().getAllFabClubs(new GridHeaderFilters(), "ACTIVE", sharedProperty);
				responseDTO.setClubs(clubs);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(clubs.size()));
				responseDTO.setStatus(1);
				responseDTO.setMessage(msg);
				return responseDTO;
				
			}else if(action.equalsIgnoreCase("DELETE")){
				if(fanClubIdStr==null || fanClubIdStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Fan Club id not found.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer fabClubId = 0;
				try {
					fabClubId = Integer.parseInt(fanClubIdStr);
				} catch (Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Fan Club id is invalid.");
					responseDTO.setError(error);
					return responseDTO;
				}
				FanClub club = QuizDAORegistry.getFanClubDAO().get(fabClubId);
				if(club == null){
					responseDTO.setStatus(0);
					error.setDescription("Fan club not found with given identifier.");
					responseDTO.setError(error);
					return responseDTO;
				}
				club.setUpdatedBy(userName);
				club.setUpdatedDate(new Date());
				club.setStatus("DELETED");
				QuizDAORegistry.getFanClubDAO().update(club);
				
				msg = "Selected Fan club deleted successfully.";
				
				List<FanClub> clubs = QuizDAORegistry.getFanClubDAO().getAllFabClubs(new GridHeaderFilters(), "ACTIVE", sharedProperty);
				responseDTO.setClubs(clubs);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(clubs.size()));
				responseDTO.setStatus(1);
				responseDTO.setMessage(msg);
				return responseDTO;
			}
			responseDTO.setStatus(0);
			responseDTO.setMessage("Could not able to identify action.");
			return responseDTO;
			
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured updating fan club.");
			responseDTO.setError(error);
			return responseDTO;
		}
	}
	
	
	
	@RequestMapping(value="/UpdateFanClubPosts")
	public FanClubDTO updateFanClubPosts(HttpServletRequest request, HttpServletResponse response){
		FanClubDTO responseDTO = new FanClubDTO();
		Error error = new Error();
		try {
			String action  = request.getParameter("action");
			String postIdStr = request.getParameter("postId");
			String fanClubIdStr = request.getParameter("fanClubId");
			String userName = request.getParameter("userName"); 
			String msg = "";
			if(action==null || action.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Could not able to identify action to be perform.");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(fanClubIdStr==null || fanClubIdStr.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Fan club id not found.");
				responseDTO.setError(error);
				return responseDTO;
			}
			Integer fanClubId = 0;
			try {
				fanClubId = Integer.parseInt(fanClubIdStr);
			} catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Fan club id is invalid.");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(action.equalsIgnoreCase("EDIT")){
				if(postIdStr==null || postIdStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Fanclub Post id not found.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer postId = 0;
				try {
					postId = Integer.parseInt(postIdStr);
				} catch (Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Fanclub Post id is invalid.");
					responseDTO.setError(error);
					return responseDTO;
				}
				FanClubPost post = QuizDAORegistry.getFanClubPostDAO().get(postId);
				if(post == null){
					responseDTO.setStatus(0);
					error.setDescription("Fanclub Post not found with given identifier.");
					responseDTO.setError(error);
					return responseDTO;
				}
				responseDTO.setStatus(1);
				responseDTO.setPost(post);
				return responseDTO;
			}else if(action.equalsIgnoreCase("SAVE") || action.equalsIgnoreCase("UPDATE")){
				String postText = request.getParameter("postText");
				
				if(postText==null || postText.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Fan club title is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				
				FanClubPost post = null;
				if(action.equalsIgnoreCase("UPDATE")){
					if(postIdStr==null || postIdStr.isEmpty()){
						responseDTO.setStatus(0);
						error.setDescription("Fanclub Post id not found.");
						responseDTO.setError(error);
						return responseDTO;
					}
					Integer postId = 0;
					try {
						postId = Integer.parseInt(postIdStr);
					} catch (Exception e) {
						responseDTO.setStatus(0);
						error.setDescription("Fanclub Post id is invalid.");
						responseDTO.setError(error);
						return responseDTO;
					}
					post = QuizDAORegistry.getFanClubPostDAO().get(postId);
					if(post == null){
						responseDTO.setStatus(0);
						error.setDescription("Fanclub Post not found with given identifier.");
						responseDTO.setError(error);
						return responseDTO;
					}
					post.setUpdatedBy(userName);
					post.setUpdatedDate(new Date());
					msg = "Selected Fanclub Post updated successfully.";
				}else if(action.equalsIgnoreCase("SAVE")){
					Customer cust = DAORegistry.getCustomerDAO().getCustByUserId(Constants.USERID);
					post = new FanClubPost();
					post.setCustomerId(cust!=null?cust.getId():-1);
					post.setCreatedBy(userName);
					post.setCreatedDate(new Date());
					post.setStatus("ACTIVE");
					msg = "Fanclub Post saved successfully.";
				}
				
				post.setPostText(postText);
				post.setFanClubId(fanClubId);
				QuizDAORegistry.getFanClubPostDAO().saveOrUpdate(post);
				
				MultipartFile imageFile = null;
				MultipartFile videoFile = null;
				MultipartFile thumbFile = null;
				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
				imageFile = multipartRequest.getFile("imageFile");
				videoFile = multipartRequest.getFile("videoFile");
				thumbFile = multipartRequest.getFile("thumbnailImage");
				if(imageFile!= null && imageFile.getSize() > 0){
					String fileName = imageFile.getOriginalFilename();
					fileName = FileUploadUtil.getFileNameWithoutSpecialCharaters(fileName);
					String ext =  fileName.substring(fileName.lastIndexOf("."));
					String path = URLUtil.FANCLUB_MEDIA+"POST_IMAGE_"+post.getId()+ext;
					
					File newFile = new File(path);
					newFile.createNewFile();
					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(newFile));
			        FileCopyUtils.copy(imageFile.getInputStream(), stream);
					stream.close();
					//String serverFileName = FileUploadUtil.generateFileName(ext);
					
					AwsS3Response awsRsponse = AWSFileService.upLoadFileToS3(AWSFileService.CUSTOMER_MEDIA_BACKET,"POST_IMAGE_"+post.getId()+ext, AWSFileService.FANCLUB_POST_IMAGE_FOLDER, newFile );
					if(null != awsRsponse && awsRsponse.getStatus() != 1){
						responseDTO.setStatus(0);
						error.setDescription("Post Details save successfully, Error occured while uploading post image to S3.");
						responseDTO.setError(error);
						return responseDTO;
					}
					post.setImageUrl(AWSFileService.AWS_FANCLUB_MEDIA_URL+AWSFileService.FANCLUB_POST_IMAGE_FOLDER+"/POST_IMAGE_"+post.getId()+ext);
					if(videoFile == null || videoFile.getSize() <= 0){
						post.setVideoUrl(null);
						post.setThumbnailUrl(null);
					}
					QuizDAORegistry.getFanClubPostDAO().update(post);
				}
				if(videoFile!= null && videoFile.getSize() > 0){
					String fileName = videoFile.getOriginalFilename();
					fileName = FileUploadUtil.getFileNameWithoutSpecialCharaters(fileName);
					String ext =  fileName.substring(fileName.lastIndexOf("."));
					String path = URLUtil.FANCLUB_MEDIA+"POST_VIDEO_"+post.getId()+ext;
					
					File newFile = new File(path);
					newFile.createNewFile();
					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(newFile));
			        FileCopyUtils.copy(videoFile.getInputStream(), stream);
					stream.close();
					//String serverFileName = FileUploadUtil.generateFileName(ext);
					
					AwsS3Response awsRsponse = AWSFileService.upLoadFileToS3(AWSFileService.CUSTOMER_MEDIA_BACKET,"POST_VIDEO_"+post.getId()+ext, AWSFileService.FANCLUB_POST_VIDEO_FOLDER, newFile);
					if(null != awsRsponse && awsRsponse.getStatus() != 1){
						responseDTO.setStatus(0);
						error.setDescription("Post Details save successfully, Error occured while uploading post video to S3.");
						responseDTO.setError(error);
						return responseDTO;
					}
					post.setVideoUrl(AWSFileService.AWS_FANCLUB_MEDIA_URL+AWSFileService.FANCLUB_POST_VIDEO_FOLDER+"/POST_VIDEO_"+post.getId()+ext);
					if(thumbFile!=null && thumbFile.getSize() > 0){
						String thumbFileName = thumbFile.getOriginalFilename();
						thumbFileName = FileUploadUtil.getFileNameWithoutSpecialCharaters(thumbFileName);
						String ext1 =  thumbFileName.substring(thumbFileName.lastIndexOf("."));
						String path1 = URLUtil.FANCLUB_MEDIA+"POST_THUMB_"+post.getId()+ext1;
						
						File newFile1 = new File(path1);
						newFile1.createNewFile();
						BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(newFile1));
				        FileCopyUtils.copy(thumbFile.getInputStream(), stream1);
						stream1.close();
						//serverFileName = FileUploadUtil.generateFileName(ext1);
						
						AwsS3Response awsRsponse1 = AWSFileService.upLoadFileToS3(AWSFileService.CUSTOMER_MEDIA_BACKET,"POST_THUMB_"+post.getId()+ext1, AWSFileService.FANCLUB_POST_POSTER_FOLDER, newFile1);
						if(null != awsRsponse1 && awsRsponse1.getStatus() != 1){
							responseDTO.setStatus(0);
							error.setDescription("Post Details save successfully, Error occured while uploading post video thumbnail image to S3.");
							responseDTO.setError(error);
							return responseDTO;
						}
						post.setThumbnailUrl(AWSFileService.AWS_FANCLUB_MEDIA_URL+AWSFileService.FANCLUB_POST_POSTER_FOLDER+"/POST_THUMB_"+post.getId()+ext1);
					}else{
						String thumbnailImagePath = FileUploadUtil.generateThumbnailImage(newFile,"POST_THUMB_"+post.getId());
						//String thumbnailFilename = FileUploadUtil.generateFileName("jpg");
						File thubnailFile = new File(thumbnailImagePath);
					
						if(thumbnailImagePath != null && !thumbnailImagePath.isEmpty()){
							AwsS3Response awsRsp = AWSFileService.upLoadFileToS3(AWSFileService.CUSTOMER_MEDIA_BACKET,"POST_THUMB_"+post.getId()+".jpg", AWSFileService.FANCLUB_POST_POSTER_FOLDER, thubnailFile);
							if(null != awsRsp && awsRsp.getStatus() != 1){
								responseDTO.setStatus(0);
								error.setDescription("Post Details save successfully, Error occured while uploading post video thumbnail image to S3.");
								responseDTO.setError(error);
								return responseDTO;
							}
							post.setThumbnailUrl(AWSFileService.AWS_FANCLUB_MEDIA_URL+AWSFileService.FANCLUB_POST_POSTER_FOLDER+"/POST_THUMB_"+post.getId()+".jpg");
						}
					}
					if(imageFile == null || imageFile.getSize() <= 0){
						post.setImageUrl(null);
					}
					QuizDAORegistry.getFanClubPostDAO().update(post);
				}
				if(thumbFile!=null && thumbFile.getSize() > 0){
					String thumbFileName = thumbFile.getOriginalFilename();
					thumbFileName = FileUploadUtil.getFileNameWithoutSpecialCharaters(thumbFileName);
					String ext1 =  thumbFileName.substring(thumbFileName.lastIndexOf("."));
					String path1 = URLUtil.FANCLUB_MEDIA+"POST_THUMB_"+post.getId()+ext1;
					
					File newFile1 = new File(path1);
					newFile1.createNewFile();
					BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(newFile1));
			        FileCopyUtils.copy(thumbFile.getInputStream(), stream1);
					stream1.close();
					//String serverFileName = FileUploadUtil.generateFileName(ext1);
					
					AwsS3Response awsRsponse1 = AWSFileService.upLoadFileToS3(AWSFileService.CUSTOMER_MEDIA_BACKET,"POST_THUMB_"+post.getId()+ext1, AWSFileService.FANCLUB_POST_POSTER_FOLDER, newFile1);
					if(null != awsRsponse1 && awsRsponse1.getStatus() != 1){
						responseDTO.setStatus(0);
						error.setDescription("Post Details save successfully, Error occured while uploading post video thumbnail image to S3.");
						responseDTO.setError(error);
						return responseDTO;
					}
					post.setThumbnailUrl(AWSFileService.AWS_FANCLUB_MEDIA_URL+AWSFileService.FANCLUB_POST_POSTER_FOLDER+"/POST_THUMB_"+post.getId()+ext1);
					QuizDAORegistry.getFanClubPostDAO().update(post);
				}
				List<FanClubPost> posts = QuizDAORegistry.getFanClubPostDAO().getAllFanClubPosts(new GridHeaderFilters(), null, fanClubId, sharedProperty);
				responseDTO.setPosts(posts);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(posts.size()));
				responseDTO.setStatus(1);
				responseDTO.setMessage(msg);
				return responseDTO;
				
			}else if(action.equalsIgnoreCase("DELETE")){
				if(postIdStr==null || postIdStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Fanclub Post id not found.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer postId = 0;
				try {
					postId = Integer.parseInt(postIdStr);
				} catch (Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Fanclub Post id is invalid.");
					responseDTO.setError(error);
					return responseDTO;
				}
				FanClubPost post = QuizDAORegistry.getFanClubPostDAO().get(postId);
				if(post == null){
					responseDTO.setStatus(0);
					error.setDescription("Fanclub Post not found with given identifier.");
					responseDTO.setError(error);
					return responseDTO;
				}
				post.setUpdatedBy(userName);
				post.setUpdatedDate(new Date());
				post.setStatus("DELETED");
				QuizDAORegistry.getFanClubPostDAO().update(post);
				
				msg = "Selected Fanclub Post deleted successfully.";
				
				List<FanClubPost> posts = QuizDAORegistry.getFanClubPostDAO().getAllFanClubPosts(new GridHeaderFilters(), null, fanClubId, sharedProperty);
				responseDTO.setPosts(posts);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(posts.size()));
				responseDTO.setStatus(1);
				responseDTO.setMessage(msg);
				return responseDTO;
			}
			responseDTO.setStatus(0);
			responseDTO.setMessage("Could not able to identify action.");
			return responseDTO;
			
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured updating Fanclub post.");
			responseDTO.setError(error);
			return responseDTO;
		}
	}
	
	
	
	@RequestMapping(value="/UpdateFanClubEvents")
	public FanClubDTO updateFanClubEvents(HttpServletRequest request, HttpServletResponse response){
		FanClubDTO responseDTO = new FanClubDTO();
		Error error = new Error();
		try {
			String action  = request.getParameter("action");
			String fanClubIdStr = request.getParameter("fanClubId");
			String eventIdStr = request.getParameter("eventId");
			String userName = request.getParameter("userName"); 
			String msg = "";
			if(action==null || action.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Could not able to identify action to be perform.");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(fanClubIdStr==null || fanClubIdStr.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Fan club id not found.");
				responseDTO.setError(error);
				return responseDTO;
			}
			Integer fanClubId = 0;
			try {
				fanClubId = Integer.parseInt(fanClubIdStr);
			} catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Fan club id is invalid.");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(action.equalsIgnoreCase("EDIT")){
				if(eventIdStr==null || eventIdStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Fanclub event id not found.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer eventId = 0;
				try {
					eventId = Integer.parseInt(eventIdStr);
				} catch (Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Fanclub event id is invalid.");
					responseDTO.setError(error);
					return responseDTO;
				}
				FanClubEvent event = QuizDAORegistry.getFanClubEventDAO().get(eventId);
				if(event == null){
					responseDTO.setStatus(0);
					error.setDescription("Fanclub event not found with given identifier.");
					responseDTO.setError(error);
					return responseDTO;
				}
				responseDTO.setStatus(1);
				responseDTO.setEvent(event);
				return responseDTO;
			}else if(action.equalsIgnoreCase("SAVE") || action.equalsIgnoreCase("UPDATE")){
				String eventName = request.getParameter("eventName");
				String venue = request.getParameter("venue");
				String eventDate = request.getParameter("eventDate");
				String eventHour = request.getParameter("eventHour");
				String eventMinute = request.getParameter("eventMinute");
				
				if(eventName==null || eventName.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Event name is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(venue==null || venue.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Event venue is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(eventDate==null || eventDate.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Event date is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(eventHour==null || eventHour.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Event time hour is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(eventMinute==null || eventMinute.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Event time minutes is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer eHour = 0;
				Integer eMinute = 0;
				try {
					eHour = Integer.parseInt(eventHour);
				} catch (Exception e) {
					e.printStackTrace();
					responseDTO.setStatus(0);
					error.setDescription("Event time hour is invalid.");
					responseDTO.setError(error);
					return responseDTO;
				}
				try {
					eMinute = Integer.parseInt(eventMinute);
				} catch (Exception e) {
					e.printStackTrace();
					responseDTO.setStatus(0);
					error.setDescription("Event time minute is invalid.");
					responseDTO.setError(error);
					return responseDTO;
				}
				
				eventDate = eventDate+" "+eHour+":"+eMinute+":00";
				Date eDate = null;
				try {
					eDate = Util.getDateWithTwentyFourHourFormat1(eventDate);
				} catch (Exception e) {
					error.setDescription("Event date and time is invalid.");
					responseDTO.setError(error);
					responseDTO.setStatus(0);
					return responseDTO;
				}
				
				FanClubEvent event = null;
				if(action.equalsIgnoreCase("UPDATE")){
					if(eventIdStr==null || eventIdStr.isEmpty()){
						responseDTO.setStatus(0);
						error.setDescription("Fanclub event id not found.");
						responseDTO.setError(error);
						return responseDTO;
					}
					Integer eventId = 0;
					try {
						eventId = Integer.parseInt(eventIdStr);
					} catch (Exception e) {
						responseDTO.setStatus(0);
						error.setDescription("Fanclub event id is invalid.");
						responseDTO.setError(error);
						return responseDTO;
					}
					 event = QuizDAORegistry.getFanClubEventDAO().get(eventId);
					if(event == null){
						responseDTO.setStatus(0);
						error.setDescription("Fanclub event not found with given identifier.");
						responseDTO.setError(error);
						return responseDTO;
					}
					event.setUpdatedBy(userName);
					event.setUpdatedDate(new Date());
					msg = "Selected Fanclub event updated successfully.";
				}else if(action.equalsIgnoreCase("SAVE")){
					Customer cust = DAORegistry.getCustomerDAO().getCustByUserId(Constants.USERID);
					event = new FanClubEvent();
					event.setCustomerId(cust!=null?cust.getId():-1);
					event.setCreatedBy(userName);
					event.setCreatedDate(new Date());
					event.setInterestedCount(0);
					event.setStatus("ACTIVE");
					msg = "Fanclub event saved successfully.";
				}
				MultipartFile imageFile = null;
				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
				imageFile = multipartRequest.getFile("imageFile");
				if(action.equalsIgnoreCase("SAVE") && imageFile== null){
					responseDTO.setStatus(0);
					error.setDescription("Fanclub event Image file is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				
				event.setEventName(eventName);
				event.setFanClubId(fanClubId);
				event.setVenue(venue);
				event.setEventDate(eDate);
				QuizDAORegistry.getFanClubEventDAO().saveOrUpdate(event);
				
				if(imageFile!=null && imageFile.getSize() > 0){
					String fileName = imageFile.getOriginalFilename();
					fileName = FileUploadUtil.getFileNameWithoutSpecialCharaters(fileName);
					String ext =  fileName.substring(fileName.lastIndexOf("."));
					String path = URLUtil.FANCLUB_MEDIA+"EVENT_IMAGE_"+event.getId()+ext;

					File newFile = new File(path);
					newFile.createNewFile();
					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(newFile));
					FileCopyUtils.copy(imageFile.getInputStream(), stream);
					stream.close();
					//String serverFileName = FileUploadUtil.generateFileName(ext);

					AwsS3Response awsRsponse = AWSFileService.upLoadFileToS3(AWSFileService.CUSTOMER_MEDIA_BACKET,"EVENT_IMAGE_"+event.getId()+ext, AWSFileService.FANCLUB_EVENT_IMAGE_FOLDER, newFile );
					if(null != awsRsponse && awsRsponse.getStatus() != 1){
						responseDTO.setStatus(0);
						error.setDescription("Event Details save successfully, Error occured while uploading Event image to S3.");
						responseDTO.setError(error);
						return responseDTO;
					}
					event.setEventImage(AWSFileService.AWS_FANCLUB_MEDIA_URL+AWSFileService.FANCLUB_EVENT_IMAGE_FOLDER+"/EVENT_IMAGE_"+event.getId()+ext);
					QuizDAORegistry.getFanClubEventDAO().update(event);
				}
				
				List<FanClubEvent> events = QuizDAORegistry.getFanClubEventDAO().getAllFanClubEvents(new GridHeaderFilters(), null, fanClubId, sharedProperty);
				responseDTO.setEvents(events);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(events.size()));
				responseDTO.setStatus(1);
				responseDTO.setMessage(msg);
				return responseDTO;
				
			}else if(action.equalsIgnoreCase("DELETE")){
				if(eventIdStr==null || eventIdStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Fanclub event id not found.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer eventId = 0;
				try {
					eventId = Integer.parseInt(eventIdStr);
				} catch (Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Fanclub event id is invalid.");
					responseDTO.setError(error);
					return responseDTO;
				}
				FanClubEvent event = QuizDAORegistry.getFanClubEventDAO().get(eventId);
				if(event == null){
					responseDTO.setStatus(0);
					error.setDescription("Fanclub event not found with given identifier.");
					responseDTO.setError(error);
					return responseDTO;
				}
				event.setUpdatedBy(userName);
				event.setUpdatedDate(new Date());
				event.setStatus("DELETED");
				QuizDAORegistry.getFanClubEventDAO().update(event);
				
				msg = "Selected Fanclub Event deleted successfully.";
				
				List<FanClubEvent> events = QuizDAORegistry.getFanClubEventDAO().getAllFanClubEvents(new GridHeaderFilters(), null, fanClubId, sharedProperty);
				responseDTO.setEvents(events);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(events.size()));
				responseDTO.setStatus(1);
				responseDTO.setMessage(msg);
				return responseDTO;
			}
			responseDTO.setStatus(0);
			responseDTO.setMessage("Could not able to identify action.");
			return responseDTO;
			
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured updating Fanclub event.");
			responseDTO.setError(error);
			return responseDTO;
		}
	}
	
	
	
	
	@RequestMapping(value="/UpdateFanClubVideos")
	public FanClubDTO updateFanClubVideos(HttpServletRequest request, HttpServletResponse response){
		FanClubDTO responseDTO = new FanClubDTO();
		Error error = new Error();
		try {
			String action  = request.getParameter("action");
			String fanClubIdStr = request.getParameter("fanClubId");
			String videoIdStr = request.getParameter("videoId");
			String userName = request.getParameter("userName"); 
			String msg = "";
			if(action==null || action.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Could not able to identify action to be perform.");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(fanClubIdStr==null || fanClubIdStr.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Fan club id not found.");
				responseDTO.setError(error);
				return responseDTO;
			}
			Integer fanClubId = 0;
			try {
				fanClubId = Integer.parseInt(fanClubIdStr);
			} catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Fan club id is invalid.");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(action.equalsIgnoreCase("EDIT")){
				if(videoIdStr==null || videoIdStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Video id not found.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer videoId = 0;
				try {
					videoId = Integer.parseInt(videoIdStr);
				} catch (Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Video id is invalid.");
					responseDTO.setError(error);
					return responseDTO;
				}
				FanClubVideo video = QuizDAORegistry.getFanClubVideoDAO().get(videoId);
				if(video == null){
					responseDTO.setStatus(0);
					error.setDescription("Video not found with given identifier.");
					responseDTO.setError(error);
					return responseDTO;
				}
				responseDTO.setStatus(1);
				responseDTO.setVideo(video);
				return responseDTO;
			}else if(action.equalsIgnoreCase("SAVE") || action.equalsIgnoreCase("UPDATE")){
				String title = request.getParameter("title");
				String description = request.getParameter("description");
				String category = request.getParameter("category");
				
				if(title==null || title.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Video title is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(description==null || description.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Video description is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(category==null || category.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Video category is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer categoryId = 0;
				try {
					categoryId = Integer.parseInt(category);
				} catch (Exception e) {
					e.printStackTrace();
					responseDTO.setStatus(0);
					error.setDescription("Video category is invalid.");
					responseDTO.setError(error);
					return responseDTO;
				}
				
				FanClubVideo video = null;
				if(action.equalsIgnoreCase("UPDATE")){
					if(videoIdStr==null || videoIdStr.isEmpty()){
						responseDTO.setStatus(0);
						error.setDescription("Video id not found.");
						responseDTO.setError(error);
						return responseDTO;
					}
					Integer videoId = 0;
					try {
						videoId = Integer.parseInt(videoIdStr);
					} catch (Exception e) {
						responseDTO.setStatus(0);
						error.setDescription("Video id is invalid.");
						responseDTO.setError(error);
						return responseDTO;
					}
					video = QuizDAORegistry.getFanClubVideoDAO().get(videoId);
					if(video == null){
						responseDTO.setStatus(0);
						error.setDescription("Video not found with given identifier.");
						responseDTO.setError(error);
						return responseDTO;
					}
					video.setUpdatedBy(userName);
					video.setUpdatedDate(new Date());
					msg = "Selected Video updated successfully.";
				}else if(action.equalsIgnoreCase("SAVE")){
					Customer cust = DAORegistry.getCustomerDAO().getCustByUserId(Constants.USERID);
					video = new FanClubVideo();
					video.setCustomerId(cust!=null?cust.getId():-1);
					video.setCreatedBy(userName);
					video.setCreatedDate(new Date());
					video.setStatus("ACTIVE");
					msg = "Video saved successfully.";
				}
				MultipartFile videoFile = null;
				MultipartFile thumbFile = null;
				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
				videoFile = multipartRequest.getFile("videoFile");
				thumbFile = multipartRequest.getFile("thumbnailImage");
				if(action.equalsIgnoreCase("SAVE") && videoFile == null){
					responseDTO.setStatus(0);
					error.setDescription("Video file is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				
				video.setTitle(title);
				video.setDescription(description);
				video.setCategoryId(categoryId);
				video.setFanClubId(fanClubId);
				QuizDAORegistry.getFanClubVideoDAO().saveOrUpdate(video);
				
				if(videoFile!= null && videoFile.getSize() > 0){
					String fileName = videoFile.getOriginalFilename();
					fileName = FileUploadUtil.getFileNameWithoutSpecialCharaters(fileName);
					String ext =  fileName.substring(fileName.lastIndexOf("."));
					String path = URLUtil.FANCLUB_MEDIA+"VIDEO_"+video.getId()+ext;

					File newFile = new File(path);
					newFile.createNewFile();
					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(newFile));
					FileCopyUtils.copy(videoFile.getInputStream(), stream);
					stream.close();
					//String serverFileName = FileUploadUtil.generateFileName(ext);

					AwsS3Response awsRsponse = AWSFileService.upLoadFileToS3(AWSFileService.CUSTOMER_MEDIA_BACKET,"VIDEO_"+video.getId()+ext, AWSFileService.FANCLUB_VIDEO_FOLDER, newFile);
					if(null != awsRsponse && awsRsponse.getStatus() != 1){
						responseDTO.setStatus(0);
						error.setDescription("Video Details save successfully, Error occured while uploading video to S3.");
						responseDTO.setError(error);
						return responseDTO;
					}
					video.setVideoUrl(AWSFileService.AWS_FANCLUB_MEDIA_URL+AWSFileService.FANCLUB_VIDEO_FOLDER+"/VIDEO_"+video.getId()+ext);
					if(thumbFile!=null && thumbFile.getSize() > 0){
						String thumbFileName = thumbFile.getOriginalFilename();
						thumbFileName = FileUploadUtil.getFileNameWithoutSpecialCharaters(thumbFileName);
						String ext1 =  thumbFileName.substring(thumbFileName.lastIndexOf("."));
						String path1 = URLUtil.FANCLUB_MEDIA+"VIDEO_THUMB_"+video.getId()+ext1;
						
						File newFile1 = new File(path1);
						newFile1.createNewFile();
						BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(newFile1));
						FileCopyUtils.copy(thumbFile.getInputStream(), stream1);
						stream1.close();
						//serverFileName = FileUploadUtil.generateFileName(ext1);
						
						AwsS3Response awsRsponse1 = AWSFileService.upLoadFileToS3(AWSFileService.CUSTOMER_MEDIA_BACKET,"VIDEO_THUMB_"+video.getId()+ext1, AWSFileService.FANCLUB_POSTER_FOLDER, newFile1);
						if(null != awsRsponse1 && awsRsponse1.getStatus() != 1){
							responseDTO.setStatus(0);
							error.setDescription("Video Details save successfully, Error occured while uploading video thumbnail image to S3.");
							responseDTO.setError(error);
							return responseDTO;
						}
						video.setThumbnailUrl(AWSFileService.AWS_FANCLUB_MEDIA_URL+AWSFileService.FANCLUB_POSTER_FOLDER+"/VIDEO_THUMB_"+video.getId()+ext1);
					}else{
						String thumbnailImagePath = FileUploadUtil.generateThumbnailImage(newFile,"VIDEO_THUMB_"+video.getId());
						//String thumbnailFilename = FileUploadUtil.generateFileName("jpg");
						File thubnailFile = new File(thumbnailImagePath);

						if(thumbnailImagePath != null && !thumbnailImagePath.isEmpty()){
							AwsS3Response awsRsp = AWSFileService.upLoadFileToS3(AWSFileService.CUSTOMER_MEDIA_BACKET,"VIDEO_THUMB_"+video.getId()+".jpg", AWSFileService.FANCLUB_POSTER_FOLDER, thubnailFile);
							if(null != awsRsp && awsRsp.getStatus() != 1){
								responseDTO.setStatus(0);
								error.setDescription("Video Details save successfully, Error occured while uploading video thumbnail image to S3.");
								responseDTO.setError(error);
								return responseDTO;
							}
							video.setThumbnailUrl(AWSFileService.AWS_FANCLUB_MEDIA_URL+AWSFileService.FANCLUB_POSTER_FOLDER+"/VIDEO_THUMB_"+video.getId()+".jpg");
						}
					}
					QuizDAORegistry.getFanClubVideoDAO().update(video);
				}
				if(thumbFile!=null && thumbFile.getSize() > 0){
					String thumbFileName = thumbFile.getOriginalFilename();
					thumbFileName = FileUploadUtil.getFileNameWithoutSpecialCharaters(thumbFileName);
					String ext1 =  thumbFileName.substring(thumbFileName.lastIndexOf("."));
					String path1 = URLUtil.FANCLUB_MEDIA+"VIDEO_THUMB_"+video.getId()+ext1;
					
					File newFile1 = new File(path1);
					newFile1.createNewFile();
					BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(newFile1));
					FileCopyUtils.copy(thumbFile.getInputStream(), stream1);
					stream1.close();
					//String serverFileName = FileUploadUtil.generateFileName(ext1);
					
					AwsS3Response awsRsponse1 = AWSFileService.upLoadFileToS3(AWSFileService.CUSTOMER_MEDIA_BACKET,"VIDEO_THUMB_"+video.getId()+ext1, AWSFileService.FANCLUB_POSTER_FOLDER, newFile1);
					if(null != awsRsponse1 && awsRsponse1.getStatus() != 1){
						responseDTO.setStatus(0);
						error.setDescription("Video Details save successfully, Error occured while uploading video thumbnail image to S3.");
						responseDTO.setError(error);
						return responseDTO;
					}
					video.setThumbnailUrl(AWSFileService.AWS_FANCLUB_MEDIA_URL+AWSFileService.FANCLUB_POSTER_FOLDER+"/VIDEO_THUMB_"+video.getId()+ext1);
					QuizDAORegistry.getFanClubVideoDAO().update(video);
				}
				
				PollingVideoInventory inventory = DAORegistry.getPollingVideoInventoryDAO().getVideoByUrl(video.getVideoUrl());
				if(inventory==null){
					inventory = new PollingVideoInventory();
				}
				inventory.setCategoryId(video.getCategoryId());
				inventory.setCreatedBy(userName);
				inventory.setCreatedDate(new Date());
				inventory.setIsDefault(false);
				inventory.setIsDeleted(0);
				inventory.setPosterUrl(video.getThumbnailUrl());
				inventory.setStatus(true);
				inventory.setTitle(video.getTitle());
				inventory.setVideoUrl(video.getVideoUrl());
				DAORegistry.getPollingVideoInventoryDAO().saveOrUpdate(inventory);
				
				List<FanClubVideo> videos = QuizDAORegistry.getFanClubVideoDAO().getAllFanClubVideos(new GridHeaderFilters(), null,fanClubId, sharedProperty);
				responseDTO.setVideos(videos);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(videos.size()));
				responseDTO.setStatus(1);
				responseDTO.setMessage(msg);
				return responseDTO;
				
			}else if(action.equalsIgnoreCase("DELETE")){
				if(videoIdStr==null || videoIdStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Video id not found.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer videoId = 0;
				try {
					videoId = Integer.parseInt(videoIdStr);
				} catch (Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Video id is invalid.");
					responseDTO.setError(error);
					return responseDTO;
				}
				FanClubVideo video = QuizDAORegistry.getFanClubVideoDAO().get(videoId);
				if(video == null){
					responseDTO.setStatus(0);
					error.setDescription("Video not found with given identifier.");
					responseDTO.setError(error);
					return responseDTO;
				}
				video.setUpdatedBy(userName);
				video.setUpdatedDate(new Date());
				video.setStatus("DELETED");
				QuizDAORegistry.getFanClubVideoDAO().update(video);
				
				PollingVideoInventory inventory = DAORegistry.getPollingVideoInventoryDAO().getVideoByUrl(video.getVideoUrl());
				if(inventory!=null){
					inventory.setStatus(false);
					inventory.setIsDeleted(1);
					DAORegistry.getPollingVideoInventoryDAO().update(inventory);
				}
				
				msg = "Selected video deleted successfully.";
				
				List<FanClubVideo> videos = QuizDAORegistry.getFanClubVideoDAO().getAllFanClubVideos(new GridHeaderFilters(), null,fanClubId, sharedProperty);
				responseDTO.setVideos(videos);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(videos.size()));
				responseDTO.setStatus(1);
				responseDTO.setMessage(msg);
				return responseDTO;
			}
			responseDTO.setStatus(0);
			responseDTO.setMessage("Could not able to identify action.");
			return responseDTO;
			
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured updating video.");
			responseDTO.setError(error);
			return responseDTO;
		}
	}
	
	
	
	
	@RequestMapping(value="/RemoveFanClubMember")
	public FanClubDTO removeFanClubMember(HttpServletRequest request, HttpServletResponse response){
		FanClubDTO responseDTO = new FanClubDTO();
		Error error = new Error();
		try {
			String fanClubIdStr = request.getParameter("fanClubId");
			String memberIdsStr = request.getParameter("memberIds");
			String userName = request.getParameter("userName"); 
			String msg = "";
			
			if(fanClubIdStr==null || fanClubIdStr.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Fan club id not found.");
				responseDTO.setError(error);
				return responseDTO;
			}
			Integer fanClubId = 0;
			try {
				fanClubId = Integer.parseInt(fanClubIdStr);
			} catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Fan club id is invalid.");
				responseDTO.setError(error);
				return responseDTO;
			}
			
			if(memberIdsStr==null || memberIdsStr.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please send member ids to remove from fan club.");
				responseDTO.setError(error);
				return responseDTO;
			}
			Integer memberId = 0;
			if(memberIdsStr.contains(",")){
				String ids [] = memberIdsStr.split(",");
				for(String idStr : ids){
					try {
						memberId = Integer.parseInt(idStr);
						FanClubMember member = QuizDAORegistry.getFanClubMemberDAO().get(memberId);
						if(member == null){
							continue;
						}
						member.setStatus("EXIT");
						member.setExitDate(new Date());
						QuizDAORegistry.getFanClubMemberDAO().update(member);
						msg = "Selected fan club member removed from fan club.";
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}else{
				try {
					memberId = Integer.parseInt(memberIdsStr);
					FanClubMember member = QuizDAORegistry.getFanClubMemberDAO().get(memberId);
					if(member == null){
						responseDTO.setStatus(0);
						error.setDescription("Selected record member details not found in system.");
						responseDTO.setError(error);
						return responseDTO;
					}
					member.setStatus("EXIT");
					member.setExitDate(new Date());
					QuizDAORegistry.getFanClubMemberDAO().update(member);
					msg = "Selected fan club member removed from fan club.";
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			List<FanClubMember> members = QuizDAORegistry.getFanClubMemberDAO().getAllFanClubMembers(new GridHeaderFilters(), null,fanClubId, sharedProperty);
			responseDTO.setMembers(members);
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(members.size()));
			responseDTO.setStatus(1);
			responseDTO.setMessage(msg);
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while removing fan club member.");
			responseDTO.setError(error);
			return responseDTO;
		}
	}
	@RequestMapping(value="/GetFanClubDashBoardInfo")
	public FanClubDTO getFanClubDashBoardInfo(HttpServletRequest request, HttpServletResponse response){
		FanClubDTO responseDTO = new FanClubDTO();
		Error error = new Error();
		try {
			String headerFilter  = request.getParameter("headerFilter");			
			GridHeaderFilters filter = GridHeaderFiltersUtil.getFanClubFIlter(headerFilter);
			List<FanClub> clubs = QuizDAORegistry.getFanClubDAO().getPopularFanClubList(filter,  sharedProperty);
			List<PollingVideoCategory> categories = DAORegistry.getPollingVideoCategoryDAO().getPopularVideoChannels(new GridHeaderFilters());
			responseDTO.setClubs(clubs);
			responseDTO.setCategories(categories);
			responseDTO.setStatus(1);			
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while fetching Dashboard info : Channels &  fan clubs.");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}
	
	
	
	@RequestMapping(value="/GetFanClubAbuseData")
	public FanClubDTO getFanClubAbuseData(HttpServletRequest request, HttpServletResponse response){
		FanClubDTO responseDTO = new FanClubDTO();
		Error error = new Error();
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String type  = request.getParameter("type");
			if(type == null || type.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please send abuse type to load.");
				responseDTO.setError(error);
				return responseDTO;
			}
			GridHeaderFilters filter = GridHeaderFiltersUtil.getFanClubAbuseFIlter(headerFilter);
			List<FanclubAbuseReported> abuseData = null;
			if(type.equalsIgnoreCase("FANCLUB")){
				abuseData = QuizDAORegistry.getFanclubAbuseReportedDAO().getFanClubAbuseReportedData(null, filter, sharedProperty);
			}else if(type.equalsIgnoreCase("POST")){
				abuseData = QuizDAORegistry.getFanclubAbuseReportedDAO().getFanClubPostAbuseReportedData(null, filter, sharedProperty);
			}else if(type.equalsIgnoreCase("EVENT")){
				abuseData = QuizDAORegistry.getFanclubAbuseReportedDAO().getFanClubEventAbuseReportedData(null, filter, sharedProperty);
			}else if(type.equalsIgnoreCase("VIDEO")){
				abuseData = QuizDAORegistry.getFanclubAbuseReportedDAO().getFanClubVideoAbuseReportedData(null, filter, sharedProperty);
			}else if(type.equalsIgnoreCase("COMMENT")){
				abuseData = QuizDAORegistry.getFanclubAbuseReportedDAO().getFanClubCommentsAbuseReportedData(null, filter, sharedProperty);
			}
			if(abuseData == null || abuseData.isEmpty()){
				responseDTO.setAbuseData(abuseData);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(abuseData.size()));
				responseDTO.setStatus(1);
				responseDTO.setMessage("No Data found.");
				return responseDTO;
			}
			responseDTO.setAbuseData(abuseData);
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(abuseData.size()));
			responseDTO.setStatus(1);			
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while fetching fanclub abuse reported data.");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}
	
	
	
	
	@RequestMapping(value="/UpdateFanClubAbuseData")
	public FanClubDTO updateFanClubAbuseData(HttpServletRequest request, HttpServletResponse response){
		FanClubDTO responseDTO = new FanClubDTO();
		Error error = new Error();
		try {
			String type  = request.getParameter("type");
			String action  = request.getParameter("action");
			String userName  = request.getParameter("userName");
			String dataId  = request.getParameter("dataId");
			if(type == null || type.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Invalid abuse type.");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(action == null || action.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Invalid action type.");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(dataId == null || dataId.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please send valid record identifier.");
				responseDTO.setError(error);
				return responseDTO;
			}
			Integer id= 0;
			try {
				id = Integer.parseInt(dataId);
			} catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Invalid record identifier.");
				responseDTO.setError(error);
				return responseDTO;
			}
			List<FanclubAbuseReported> abuseData = null;
			String msg = null;
			FanclubAbuseReported abuse = QuizDAORegistry.getFanclubAbuseReportedDAO().get(id);
			if(abuse==null){
				responseDTO.setStatus(0);
				error.setDescription("Abuse reported record not found in system.");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(type.equalsIgnoreCase("FANCLUB")){
				if(abuse.getFanclubId() == null){
					responseDTO.setStatus(0);
					error.setDescription("Fanclub not found for selected abuse reported record.");
					responseDTO.setError(error);
					return responseDTO;
				}
				FanClub fanclub = QuizDAORegistry.getFanClubDAO().get(abuse.getFanclubId());
				if(fanclub==null){
					responseDTO.setStatus(0);
					error.setDescription("Fanclub not found for selected abuse reported record.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(action.equalsIgnoreCase("REMOVE")){
					fanclub.setStatus("DELETED");
					fanclub.setUpdatedBy(userName);
					fanclub.setUpdatedDate(new Date());
					
					abuse.setUpdatedDate(new Date());
					abuse.setUpdatedUserId(userName);
					QuizDAORegistry.getFanClubDAO().update(fanclub);
					QuizDAORegistry.getFanclubAbuseReportedDAO().update(abuse);
					msg = "Fanclub removed successfully.";
				}else if(action.equalsIgnoreCase("BLOCK")){
					fanclub.setStatus("BLOCKED");
					fanclub.setUpdatedBy(userName);
					fanclub.setUpdatedDate(new Date());
					
					abuse.setUpdatedDate(new Date());
					abuse.setUpdatedUserId(userName);
					QuizDAORegistry.getFanClubDAO().update(fanclub);
					QuizDAORegistry.getFanclubAbuseReportedDAO().update(abuse);
					
					Map<String, String> map = com.rtw.tmat.utils.Util.getParameterMap(request);
					map.put("cuId", String.valueOf(fanclub.getCustomerId()));
					map.put("source", "FANCLUB");
					map.put("sourceId", String.valueOf(fanclub.getId()));
					map.put("userName", userName);
					
					String data = com.rtw.tmat.utils.Util.getObject(map, Constants.BASE_URL + Constants.BLOCK_CUSTOMER_COMMENT);
					Gson gson = GsonCustomConfig.getGsonBuilder();
					JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
					CommonRespInfo respDTO = gson.fromJson(((JsonObject) jsonObject.get("commonRespInfo")),CommonRespInfo.class);
					
					if(respDTO.getSts() == 1){
						msg = "Fanclub removed successfully and fanclub user blocked for future uploads.";
					}else{
						msg = "Fanclub removed successfully, While blocking user Error occured : "+respDTO.getErr().getDesc();
					}
					
				}
				abuseData = QuizDAORegistry.getFanclubAbuseReportedDAO().getFanClubAbuseReportedData(null, new GridHeaderFilters(), sharedProperty);
				responseDTO.setAbuseData(abuseData);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(abuseData.size()));
				responseDTO.setMessage(msg);
				responseDTO.setStatus(1);			
				return responseDTO;
			}else if(type.equalsIgnoreCase("POST")){
				if(abuse.getPostId() == null){
					responseDTO.setStatus(0);
					error.setDescription("Fanclub post not found for selected abuse reported record.");
					responseDTO.setError(error);
					return responseDTO;
				}
				FanClubPost post = QuizDAORegistry.getFanClubPostDAO().get(abuse.getPostId());
				if(post==null){
					responseDTO.setStatus(0);
					error.setDescription("Fanclub post not found for selected abuse reported record.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(action.equalsIgnoreCase("REMOVE")){
					post.setStatus("DELETED");
					post.setUpdatedBy(userName);
					post.setUpdatedDate(new Date());
					
					abuse.setUpdatedDate(new Date());
					abuse.setUpdatedUserId(userName);
					QuizDAORegistry.getFanClubPostDAO().update(post);
					QuizDAORegistry.getFanclubAbuseReportedDAO().update(abuse);
					msg = "Fanclub post removed successfully.";
				}else if(action.equalsIgnoreCase("BLOCK")){
					post.setStatus("BLOCKED");
					post.setUpdatedBy(userName);
					post.setUpdatedDate(new Date());
					
					abuse.setUpdatedDate(new Date());
					abuse.setUpdatedUserId(userName);
					QuizDAORegistry.getFanClubPostDAO().update(post);
					QuizDAORegistry.getFanclubAbuseReportedDAO().update(abuse);
					
					Map<String, String> map = com.rtw.tmat.utils.Util.getParameterMap(request);
					map.put("cuId", String.valueOf(post.getCustomerId()));
					map.put("source", "FANCLUBPOST");
					map.put("sourceId", String.valueOf(post.getId()));
					map.put("userName", userName);
					
					String data = com.rtw.tmat.utils.Util.getObject(map, Constants.BASE_URL + Constants.BLOCK_CUSTOMER_COMMENT);
					Gson gson = GsonCustomConfig.getGsonBuilder();
					JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
					CommonRespInfo respDTO = gson.fromJson(((JsonObject) jsonObject.get("commonRespInfo")),CommonRespInfo.class);
					
					if(respDTO.getSts() == 1){
						msg = "Fanclub post removed successfully and fanclub post user blocked for future uploads.";
					}else{
						msg = "Fanclub post removed successfully, While blocking user Error occured : "+respDTO.getErr().getDesc();
					}
				}
				abuseData = QuizDAORegistry.getFanclubAbuseReportedDAO().getFanClubPostAbuseReportedData(null, new GridHeaderFilters(), sharedProperty);
				responseDTO.setAbuseData(abuseData);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(abuseData.size()));
				responseDTO.setMessage(msg);
				responseDTO.setStatus(1);			
				return responseDTO;
			}else if(type.equalsIgnoreCase("EVENT")){
				if(abuse.getEventId() == null){
					responseDTO.setStatus(0);
					error.setDescription("Fanclub event not found for selected abuse reported record.");
					responseDTO.setError(error);
					return responseDTO;
				}
				FanClubEvent event = QuizDAORegistry.getFanClubEventDAO().get(abuse.getEventId());
				if(event==null){
					responseDTO.setStatus(0);
					error.setDescription("Fanclub event not found for selected abuse reported record.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(action.equalsIgnoreCase("REMOVE")){
					event.setStatus("DELETED");
					event.setUpdatedBy(userName);
					event.setUpdatedDate(new Date());
					
					abuse.setUpdatedDate(new Date());
					abuse.setUpdatedUserId(userName);
					QuizDAORegistry.getFanClubEventDAO().update(event);
					QuizDAORegistry.getFanclubAbuseReportedDAO().update(abuse);
					msg = "Fanclub event removed successfully.";
				}else if(action.equalsIgnoreCase("BLOCK")){
					event.setStatus("BLOCKED");
					event.setUpdatedBy(userName);
					event.setUpdatedDate(new Date());
					
					abuse.setUpdatedDate(new Date());
					abuse.setUpdatedUserId(userName);
					QuizDAORegistry.getFanClubEventDAO().update(event);
					QuizDAORegistry.getFanclubAbuseReportedDAO().update(abuse);
					
					Map<String, String> map = com.rtw.tmat.utils.Util.getParameterMap(request);
					map.put("cuId", String.valueOf(event.getCustomerId()));
					map.put("source", "FANCLUBEVENT");
					map.put("sourceId", String.valueOf(event.getId()));
					map.put("userName", userName);
					
					String data = com.rtw.tmat.utils.Util.getObject(map, Constants.BASE_URL + Constants.BLOCK_CUSTOMER_COMMENT);
					Gson gson = GsonCustomConfig.getGsonBuilder();
					JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
					CommonRespInfo respDTO = gson.fromJson(((JsonObject) jsonObject.get("commonRespInfo")),CommonRespInfo.class);
					
					if(respDTO.getSts() == 1){
						msg = "Fanclub event removed successfully and fanclub event user blocked for future uploads.";
					}else{
						msg = "Fanclub event removed successfully, While blocking user Error occured : "+respDTO.getErr().getDesc();
					}
				}
				abuseData = QuizDAORegistry.getFanclubAbuseReportedDAO().getFanClubEventAbuseReportedData(null, new GridHeaderFilters(), sharedProperty);
				responseDTO.setAbuseData(abuseData);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(abuseData.size()));
				responseDTO.setMessage(msg);
				responseDTO.setStatus(1);			
				return responseDTO;
			}else if(type.equalsIgnoreCase("VIDEO")){
				if(abuse.getVideoId() == null){
					responseDTO.setStatus(0);
					error.setDescription("Fanclub video not found for selected abuse reported record.");
					responseDTO.setError(error);
					return responseDTO;
				}
				FanClubVideo video = QuizDAORegistry.getFanClubVideoDAO().get(abuse.getVideoId());
				if(video==null){
					responseDTO.setStatus(0);
					error.setDescription("Fanclub video not found for selected abuse reported record.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(action.equalsIgnoreCase("REMOVE")){
					video.setStatus("DELETED");
					video.setUpdatedBy(userName);
					video.setUpdatedDate(new Date());
					
					abuse.setUpdatedDate(new Date());
					abuse.setUpdatedUserId(userName);
					QuizDAORegistry.getFanClubVideoDAO().update(video);
					QuizDAORegistry.getFanclubAbuseReportedDAO().update(abuse);
					msg = "Fanclub video removed successfully.";
				}else if(action.equalsIgnoreCase("BLOCK")){
					video.setStatus("BLOCKED");
					video.setUpdatedBy(userName);
					video.setUpdatedDate(new Date());
					
					abuse.setUpdatedDate(new Date());
					abuse.setUpdatedUserId(userName);
					QuizDAORegistry.getFanClubVideoDAO().update(video);
					QuizDAORegistry.getFanclubAbuseReportedDAO().update(abuse);
					
					Map<String, String> map = com.rtw.tmat.utils.Util.getParameterMap(request);
					map.put("cuId", String.valueOf(video.getCustomerId()));
					map.put("source", "FANCLUBVIDEO");
					map.put("sourceId", String.valueOf(video.getId()));
					map.put("userName", userName);
					
					String data = com.rtw.tmat.utils.Util.getObject(map, Constants.BASE_URL + Constants.BLOCK_CUSTOMER_COMMENT);
					Gson gson = GsonCustomConfig.getGsonBuilder();
					JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
					CommonRespInfo respDTO = gson.fromJson(((JsonObject) jsonObject.get("commonRespInfo")),CommonRespInfo.class);
					
					if(respDTO.getSts() == 1){
						msg = "Fanclub video removed successfully and fanclub video user blocked for future uploads.";
					}else{
						msg = "Fanclub video removed successfully, While blocking user Error occured : "+respDTO.getErr().getDesc();
					}
				}
				abuseData = QuizDAORegistry.getFanclubAbuseReportedDAO().getFanClubVideoAbuseReportedData(null, new GridHeaderFilters(), sharedProperty);
				responseDTO.setAbuseData(abuseData);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(abuseData.size()));
				responseDTO.setMessage(msg);
				responseDTO.setStatus(1);			
				return responseDTO;
			}else if(type.equalsIgnoreCase("COMMENT")){
				if(abuse.getCommentId() == null){
					responseDTO.setStatus(0);
					error.setDescription("Comment not found for selected abuse reported record.");
					responseDTO.setError(error);
					return responseDTO;
				}
				FanClubPostComments comment = QuizDAORegistry.getFanClubPostCommentsDAO().get(abuse.getCommentId());
				if(comment==null){
					responseDTO.setStatus(0);
					error.setDescription("Comment not found for selected abuse reported record.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(action.equalsIgnoreCase("REMOVE")){
					comment.setCommentStatus("DELETED");
					comment.setUpdatedBy(userName);
					comment.setUpdatedDate(new Date());
					
					abuse.setUpdatedDate(new Date());
					abuse.setUpdatedUserId(userName);
					QuizDAORegistry.getFanClubPostCommentsDAO().update(comment);
					QuizDAORegistry.getFanclubAbuseReportedDAO().update(abuse);
					msg = "Comment removed successfully.";
				}else if(action.equalsIgnoreCase("BLOCK")){
					comment.setCommentStatus("BLOCKED");
					comment.setUpdatedBy(userName);
					comment.setUpdatedDate(new Date());
					
					abuse.setUpdatedDate(new Date());
					abuse.setUpdatedUserId(userName);
					QuizDAORegistry.getFanClubPostCommentsDAO().update(comment);
					QuizDAORegistry.getFanclubAbuseReportedDAO().update(abuse);
					
					Map<String, String> map = com.rtw.tmat.utils.Util.getParameterMap(request);
					map.put("cuId", String.valueOf(comment.getCustomerId()));
					map.put("source", "FANCLUBCOMMENT");
					map.put("sourceId", String.valueOf(comment.getId()));
					map.put("userName", userName);
					
					String data = com.rtw.tmat.utils.Util.getObject(map, Constants.BASE_URL + Constants.BLOCK_CUSTOMER_COMMENT);
					Gson gson = GsonCustomConfig.getGsonBuilder();
					JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
					CommonRespInfo respDTO = gson.fromJson(((JsonObject) jsonObject.get("commonRespInfo")),CommonRespInfo.class);
					
					if(respDTO.getSts() == 1){
						msg = "Comment removed successfully and comment user blocked for future uploads.";
					}else{
						msg = "Comment removed successfully, While blocking user Error occured : "+respDTO.getErr().getDesc();
					}
				}
				abuseData = QuizDAORegistry.getFanclubAbuseReportedDAO().getFanClubCommentsAbuseReportedData(null, new GridHeaderFilters(), sharedProperty);
				responseDTO.setAbuseData(abuseData);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(abuseData.size()));
				responseDTO.setMessage(msg);
				responseDTO.setStatus(1);			
				return responseDTO;
			}else{
				responseDTO.setStatus(0);
				error.setDescription("Could not identify action to perfiorm.");
				responseDTO.setError(error);
				return responseDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while fetching fanclub abuse reported data.");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}

}
