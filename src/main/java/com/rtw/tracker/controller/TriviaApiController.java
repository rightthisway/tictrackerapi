package com.rtw.tracker.controller;

import org.springframework.stereotype.Controller;

import com.rtw.tmat.utils.common.SharedProperty;

@Controller
public class TriviaApiController {

	
	SharedProperty sharedProperty;

	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}

	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}
	
	
}
