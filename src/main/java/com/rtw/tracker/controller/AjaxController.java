package com.rtw.tracker.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtw.tmat.utils.Constants;
import com.rtw.tmat.utils.CustomerCardInfo;
import com.rtw.tmat.utils.CustomerDetails;
import com.rtw.tmat.utils.Error;
import com.rtw.tmat.utils.Util;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.FantasyEventTeamTicket;
import com.rtw.tracker.datas.FantasyCategories;
import com.rtw.tracker.datas.FantasySportsProduct;
import com.rtw.tracker.datas.CityAutoSearch;
import com.rtw.tracker.datas.Country;
import com.rtw.tracker.datas.AffiliateCashReward;
import com.rtw.tracker.datas.AffiliateCashRewardHistory;
import com.rtw.tracker.datas.Artist;
import com.rtw.tracker.datas.ArtistList;
import com.rtw.tracker.datas.Cards;
import com.rtw.tracker.datas.ChildCategory;
import com.rtw.tracker.datas.CrownJewelCategoryTeams;
import com.rtw.tracker.datas.CrownJewelLeagues;
import com.rtw.tracker.datas.CrownJewelTeams;
import com.rtw.tracker.datas.CrownJewelTicketList;
import com.rtw.tracker.datas.Customer;
import com.rtw.tracker.datas.CustomerAddress;
import com.rtw.tracker.datas.CustomerLoyalty;
import com.rtw.tracker.datas.CustomerWallet;
import com.rtw.tracker.datas.Event;
import com.rtw.tracker.datas.EventDetails;
import com.rtw.tracker.datas.FantasyChildCategory;
import com.rtw.tracker.datas.FantasyGrandChildCategory;
import com.rtw.tracker.datas.FantasyParentCategory;
import com.rtw.tracker.datas.GrandChildCategory;
import com.rtw.tracker.datas.LoyalFanAutoSearch;
import com.rtw.tracker.datas.LoyalFanStateSearch;
import com.rtw.tracker.datas.PopularArtist;
import com.rtw.tracker.datas.PopularArtistAudit;
import com.rtw.tracker.datas.PopularEventAudit;
import com.rtw.tracker.datas.PopularEvents;
import com.rtw.tracker.datas.PopularVenue;
import com.rtw.tracker.datas.PopularVenueAudit;
import com.rtw.tracker.datas.Role;
import com.rtw.tracker.datas.State;
import com.rtw.tracker.datas.TrackerBrokers;
import com.rtw.tracker.datas.TrackerUser;
import com.rtw.tracker.datas.UserAction;
import com.rtw.tracker.datas.UserPreference;
import com.rtw.tracker.datas.ValidateCustomerLoyalFan;
import com.rtw.tracker.datas.Venue;
import com.rtw.tracker.pojos.AutoCompleteDTO;
import com.rtw.tracker.pojos.AutoCompleteEventsDTO;
import com.rtw.tracker.pojos.CheckUserNameDTO;
import com.rtw.tracker.pojos.CityStateCountryAutoSearchDTO;
import com.rtw.tracker.pojos.FanasyCatTeamDTO;
import com.rtw.tracker.pojos.FanasyCategoryDTO;
import com.rtw.tracker.pojos.FantasyCategoryTeamDTO;
import com.rtw.tracker.pojos.FantasyChildCategoryDTO;
import com.rtw.tracker.pojos.FantasyEventTeamTicketDTO;
import com.rtw.tracker.pojos.FantasyEventsFromGrandChildDTO;
import com.rtw.tracker.pojos.FantasyGrandChildCategoryDTO;
import com.rtw.tracker.pojos.FantasyTicketValidationDTO;
import com.rtw.tracker.pojos.GenericResponseDTO;
import com.rtw.tracker.pojos.GettingUserDTO;
import com.rtw.tracker.pojos.GlobalBrokerDTO;
import com.rtw.tracker.pojos.GettingBrokerDTO;
import com.rtw.tracker.pojos.LoadStateDTO;
import com.rtw.tracker.pojos.PopularArtistDTO;
import com.rtw.tracker.pojos.PopularArtistsCountDTO;
import com.rtw.tracker.pojos.SavedCardsRewardPointsAndLoyalFanDTO;
import com.rtw.tracker.pojos.ShippingAddressDTO;
import com.rtw.tracker.pojos.UserPreferenceDTO;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.GridHeaderFiltersUtil;
import com.rtw.tracker.utils.JsonWrapperUtil;
import com.rtw.tracker.utils.PaginationUtil;

@Controller
public class AjaxController {
	/**
	 * Testing SVN Mitul
	 */
	
	private SharedProperty sharedProperty;
	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}

	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}
	
	//Admincontroller has same request mapping so i have commented
	/*@RequestMapping("/AutoCompleteArtist")
	public void getAutoCompleteGrandChildAndArtistAndVenue(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String param = request.getParameter("q");
//		String brokerId = request.getParameter("brokerId");
		PrintWriter writer =  response.getWriter();
		String strResponse ="";
		Collection<Artist> artists = DAORegistry.getArtistDAO().filterByName(param);
		Collection<Venue> venues = DAORegistry.getVenueDAO().filterByVenue(
				param);
		Collection<GrandChildCategory> grandChildCategories = DAORegistry
				.getGrandChildCategoryDAO().getGrandChildCategoriesByName(
						param);
		Collection<ChildCategory> childCategories = DAORegistry
				.getChildCategoryDAO().getChildTourCategoriesByName(param);

		//if (artists == null && venues == null && grandChildCategories == null) {
		if (artists == null) {
			return;
		}
		if (artists != null) {
			for (Artist artist : artists) {
				strResponse += ("ARTIST" + "|" + artist.getId() + "|" + artist.getName() + "\n") ;
			}
		}
		if (venues != null) {
			for (Venue venue : venues) {
				strResponse += ("VENUE" + "|" + venue.getId() + "|" + venue.getBuilding() + "\n");
			}
		}
		if (grandChildCategories != null) {
			for (GrandChildCategory grandChildCateogry : grandChildCategories) {
				strResponse += ("GRANDCHILD" + "|" + grandChildCateogry.getId() + "|" + grandChildCateogry.getName() + "\n");
			}
		}

		if (childCategories != null) {
			for (ChildCategory childCategory : childCategories) {
				strResponse += ("CHILD" + "|" + childCategory.getId() + "|" + childCategory.getName() + "\n");
			}
		}
		
		writer.write(strResponse);

	}*/
	@RequestMapping("/AutoCompleteEvents")
	public String getAutoCompleteEvents(
			HttpServletRequest request, HttpServletResponse response,Model model)
			throws Exception {
		AutoCompleteEventsDTO autoCompleteEventsDTO = new AutoCompleteEventsDTO();
		model.addAttribute("autoCompleteEventsDTO", autoCompleteEventsDTO);
		try{
			String param = request.getParameter("q");
			String strResponse ="";
			Collection<Event> events = DAORegistry.getQueryManagerDAO().getAllActiveEventWithVenue(param);
			
			if (events == null) {
				return "";
			}
			if (events != null) {
				for (Event event : events) {
					strResponse += ("EVENT" + "|" + event.getEventId() + "|" + event.getNameWithDateandVenue() + "\n") ;
				}
			}
			
			autoCompleteEventsDTO.setStatus(1);
			autoCompleteEventsDTO.setEvents(strResponse);
			
		}catch(Exception e){
			Error error = new Error();
			System.err.println(e.getLocalizedMessage());
			error.setDescription("There is something wrong. Please try again.");
			autoCompleteEventsDTO.setError(error);
			autoCompleteEventsDTO.setStatus(0);
		}
		
		return "";
	}
	
	
	@RequestMapping("/AutoCompleteChildAndGrandChild")
	public void getAutoCompleteChildAndGrandChild(
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String param = request.getParameter("q");
		PrintWriter writer =  response.getWriter();
		String strResponse ="";
		Collection<GrandChildCategory> grandChildCategories = DAORegistry.getGrandChildCategoryDAO().getGrandChildCategoriesByNameAndDisplayOnSearch(param);
		Collection<ChildCategory> childCategories = DAORegistry.getChildCategoryDAO().getChildCategoriesByNameAndDisplayOnSearch(param);

		if (childCategories == null && grandChildCategories == null) {
			return;
		}
		if (grandChildCategories != null) {
			for (GrandChildCategory grandChildCateogry : grandChildCategories) {
				strResponse += ("GRAND" + "|" + grandChildCateogry.getId() + "|" + grandChildCateogry.getName() + "\n");
			}
		}
		
		if (childCategories != null) {
			for (ChildCategory childCategory : childCategories) {
				strResponse += ("CHILD" + "|" + childCategory.getId() + "|" + childCategory.getName() + "\n");
			}
		}
		
		writer.write(strResponse);
	}
	
	
	@RequestMapping("/AutoCompleteFantasyCategoryTeams")
	public AutoCompleteDTO getAutoCompleteFantasyCategoryTeams(HttpServletRequest request, HttpServletResponse response) throws Exception {
		AutoCompleteDTO autoCompleteDTO = new AutoCompleteDTO();
		Error error = new Error();
		
		try{
			String param = request.getParameter("q");
			String grandChildStr = request.getParameter("grandChildId");
			
			List<CrownJewelCategoryTeams> teams = null;			
			String strResponse ="";
			
			if(grandChildStr!=null && !grandChildStr.isEmpty()){
				teams = DAORegistry.getCrownJewelCategoryTeamsDAO().getCategoryTeamsByGrandChildandName(Integer.parseInt(grandChildStr), param);
			}else{
				teams = DAORegistry.getCrownJewelCategoryTeamsDAO().getCategoryTeamsByName(param);
			}
			
			if (teams == null) {
				System.err.println("There is no Fantasy Category Teams from your Search");
				error.setDescription("There is no Fantasy Category Teams from your Search");
				autoCompleteDTO.setError(error);
				autoCompleteDTO.setStatus(0);
				return autoCompleteDTO;
			}
			if (teams != null) {
				for (CrownJewelCategoryTeams team : teams) {
					strResponse += ("TEAM" + "|" + team.getId() + "|" + team.getName() + "\n") ;
				}
			}
			
			autoCompleteDTO.setStatus(1);
			autoCompleteDTO.setAutoCompleteStringResponse(strResponse);
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while Auto Complete Fantasy Category Teams Search");
			autoCompleteDTO.setError(error);
			autoCompleteDTO.setStatus(0);
		}
		return autoCompleteDTO;
	}
	
	@RequestMapping("/GetFantasyChildCategories")
	public void getFantasyChildCategories(HttpServletRequest request, HttpServletResponse response)throws Exception {
		String parentCategoryName = request.getParameter("parentCategoryName");
		List<FantasyChildCategory> childCategories = null;
		JSONArray array = new JSONArray();
		try {
			if(parentCategoryName!=null && !parentCategoryName.isEmpty()){
				FantasyParentCategory parentCategory = DAORegistry.getFantasyParentCategoryDAO().getParentCategoryByName(parentCategoryName);
				if(parentCategory!=null){
					childCategories = DAORegistry.getFantasyChildCategoryDAO().getChildCategoryByParentId(parentCategory.getId());
					JSONObject object = null;
					for(FantasyChildCategory child :childCategories){
						object = new JSONObject();
						object.put("name",child.getName());
						object.put("id", child.getId());
						array.put(object);
					}
				}
			}
			IOUtils.write(array.toString().getBytes(),response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/GetFantasyGrandChildCategories")
	public FantasyGrandChildCategoryDTO getFantasyGrandChildCategories(HttpServletRequest request, HttpServletResponse response)throws Exception {
		FantasyGrandChildCategoryDTO fantasyGrandChildCategoryDTO = new FantasyGrandChildCategoryDTO();
		Error error = new Error();
		
		try {
			String customerIdStr = request.getParameter("customerId");
			List<FantasyCategories> fantasyCategoryList = null;
			
			Map<String, String> map = Util.getParameterMap(request);			
		    map.put("customerId", customerIdStr);
		    
		    String data = Util.getObject(map,sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.GET_FANTASY_PRODUCT_CATEGORY);
		    Gson gson = new Gson();  
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			FantasySportsProduct fantasySportsProductList = gson.fromJson(((JsonObject)jsonObject.get("fantasySportsProduct")), FantasySportsProduct.class);
			
			if(fantasySportsProductList.getStatus() == 1){
				fantasyCategoryList = fantasySportsProductList.getFantasyCategories();
				
				fantasyGrandChildCategoryDTO.setStatus(1);
				
				FanasyCategoryDTO fanasyCategoryDTO = null;
				List<FanasyCategoryDTO> fanasyCategoryDTOs = new ArrayList<FanasyCategoryDTO>();
				for(FantasyCategories grandChild :fantasyCategoryList){
					fanasyCategoryDTO = new FanasyCategoryDTO();
					fanasyCategoryDTO.setName(grandChild.getName());
					fanasyCategoryDTO.setId(grandChild.getId());
					fanasyCategoryDTO.setPackageInfo(grandChild.getPackageInformation());
					fanasyCategoryDTOs.add(fanasyCategoryDTO);
				}
				
				fantasyGrandChildCategoryDTO.setFanasyCategoryDTO(fanasyCategoryDTOs);
			}else{
				error.setDescription(fantasySportsProductList.getError().getDescription());
				fantasyGrandChildCategoryDTO.setError(error);
				fantasyGrandChildCategoryDTO.setStatus(0);
			}
						
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Fantasy Grand Child Category.");
			fantasyGrandChildCategoryDTO.setError(error);
			fantasyGrandChildCategoryDTO.setStatus(0);
		}
		return fantasyGrandChildCategoryDTO;
	}
	
	/*@RequestMapping("/AutoCompleteParentAndChild")
	public void getAutoCompleteParentAndChild(
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String param = request.getParameter("q");
		PrintWriter writer =  response.getWriter();
		String strResponse ="";
		
		Collection<ChildCategory> childCategories = DAORegistry.getChildCategoryDAO().getChildCategoriesByName(param);
		Collection<ParentCategory> parentCategories = DAORegistry.getParentCategoryDAO().getParentCategoriesByName(param);

		if (childCategories == null && parentCategories == null) {
			return;
		}
		if (childCategories != null) {
			for (ChildCategory childCategory : childCategories) {
				strResponse += ("CHILD" + "|" + childCategory.getId() + "|" + childCategory.getName() + "\n");
			}
		}
		if (parentCategories != null) {
			for (ParentCategory parentCategory : parentCategories) {
				strResponse += ("PARENT" + "|" + parentCategory.getId() + "|" + parentCategory.getName() + "\n");
			}
		}
		
		writer.write(strResponse);
	}*/
	
	@RequestMapping(value="/GetArtistsByChildAndGrandChild")
	public void getArtistsByChildAndGrandChild(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		JSONArray jsonArr = new JSONArray();
		try{
			String grandChildId = request.getParameter("grandChildId");
			String childId = request.getParameter("childId");

			List<Artist> artists = null;
			if (childId != null) {
				artists = DAORegistry.getQueryManagerDAO().getAllActiveByGrandchildOrChild(null,Integer.parseInt(childId));
			}
			if (grandChildId != null) {
				artists = DAORegistry.getQueryManagerDAO().getAllActiveByGrandchildOrChild(Integer.parseInt(grandChildId),null);
			}
			JSONObject jObj = null;
			
			if (artists != null) {
				for (Artist artist : artists) {
					jObj = new JSONObject();
					jObj.put("id", artist.getId());
					jObj.put("name", artist.getName());
					jsonArr.put(jObj);
				}
			}
			IOUtils.write(jsonArr.toString().getBytes(),
					response.getOutputStream());
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value="/GetGrandChildByParentAndChild")
	public void getGetGrandChildByParentAndChildA(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		JSONArray jsonArr = new JSONArray();
		try{
			String parentId = request.getParameter("parentId");
			String childId = request.getParameter("childId");

			List<GrandChildCategory> grandChilds = null;
			if (childId != null) {
				grandChilds = DAORegistry.getGrandChildCategoryDAO().getAllGrandChildCategoryByChildCategory(Integer.parseInt(childId));
			}
			if (parentId != null) {
				grandChilds = DAORegistry.getGrandChildCategoryDAO().getAllGrandChildCategoryByParentCategory(Integer.parseInt(parentId));
			}
			JSONObject jObj = null;
			
			if (grandChilds != null) {
				for (GrandChildCategory grandChild : grandChilds) {
					jObj = new JSONObject();
					jObj.put("id", grandChild.getId());
					jObj.put("name", grandChild.getName());
					jsonArr.put(jObj);
				}
			}
			IOUtils.write(jsonArr.toString().getBytes(),
					response.getOutputStream());
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value="/GetChildCategoriesByParentCategory")
	public void getChildCategoriesByParentCategory(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		JSONArray jsonArr = new JSONArray();
		try{
			String parentId = request.getParameter("parentId");

			List<ChildCategory> childs = null;
			if (parentId != null) {
				childs = DAORegistry.getChildCategoryDAO().getAllChildCategoryByParentCategory(Integer.parseInt(parentId));
			}
			JSONObject jObj = null;
			
			if (childs != null) {
				for (ChildCategory child : childs) {
					jObj = new JSONObject();
					jObj.put("id", child.getId());
					jObj.put("name", child.getName());
					jsonArr.put(jObj);
				}
			}
			IOUtils.write(jsonArr.toString().getBytes(),
					response.getOutputStream());
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	@RequestMapping(value="/GetEventsByArtistAndCity")
	public void getEventsByArtistAndCity(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		JSONArray jsonArr = new JSONArray();
		try{
			Integer artistId = Integer.parseInt(request.getParameter("artistId"));
			String city = request.getParameter("city");
			List<EventDetails> events = DAORegistry.getEventDetailsDAO().getEventsByArtistCity(artistId, city);
			JSONObject jObj = null;
			
			for (EventDetails eventDtls : events) {
				jObj = new JSONObject();
				jObj.put("id", eventDtls.getEventId());
				jObj.put("nameWithDateandVenue", eventDtls.getNameWithDateandVenue());
				jsonArr.put(jObj);
			}
			IOUtils.write(jsonArr.toString().getBytes(),
					response.getOutputStream());
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * Get Popular Events Count
	 * @param productId
	 */
	@RequestMapping(value = "/GetPopularEventsCount")
	public @ResponseBody String getPopularEventsCount(HttpServletRequest request, HttpServletResponse response){
		String msg="";
		try {
			String productId = request.getParameter("productId");
				Integer count = DAORegistry.getPopularEventsDAO().getAllActivePopularEventsCountByProductId(Integer.parseInt(productId));
				msg= ""+count;
		} catch (Exception e) {
			e.printStackTrace();
			msg="There is something wrong..Please Try Again.";
		}
		return msg;
	}
	/**
	 * Get Popular Artists Count
	 * @param productId
	 */
	@RequestMapping(value = "/GetPopularArtistsCount")
	public String getPopularArtistsCount(HttpServletRequest request, HttpServletResponse response,Model model){
		PopularArtistsCountDTO popularArtistsCountDTO = new PopularArtistsCountDTO();
		model.addAttribute("popularArtistsCountDTO", popularArtistsCountDTO);
		
		try {
				String productId = request.getParameter("productId");
				Integer count = DAORegistry.getPopularArtistDAO().getAllActivePopularArtistCountByProductId(Integer.parseInt(productId));
				popularArtistsCountDTO.setStatus(1);
				popularArtistsCountDTO.setCount(""+count);
		} catch (Exception e) {
			Error error = new Error();
			System.err.println(e.getLocalizedMessage());
			error.setDescription("There is something wrong..Please Try Again.");
			popularArtistsCountDTO.setError(error);
			popularArtistsCountDTO.setStatus(0);
		}
		return "";
	}
	/**
	 * Get Popular Grand Childs Count
	 * @param productId
	 */
	@RequestMapping(value = "/GetPopularGrandChildsCount")
	public @ResponseBody String getPopularGrandChildsCount(HttpServletRequest request, HttpServletResponse response){
		String msg="";
		try {
			String productId = request.getParameter("productId");
				Integer count = DAORegistry.getPopularGrandChildCategoryDAO().getAllActivePopularGrandChildCategoryCountByProductId(Integer.parseInt(productId));
				msg= ""+count;
		} catch (Exception e) {
			e.printStackTrace();
			msg="There is something wrong..Please Try Again.";
		}
		return msg;
	}
	/**
	 * Get Popular Venues Count
	 * @param productId
	 */
	@RequestMapping(value = "/GetPopularVenuesCount")
	public @ResponseBody String getPopularVenuesCount(HttpServletRequest request, HttpServletResponse response){
		String msg="";
		try {
			String productId = request.getParameter("productId");
				Integer count = DAORegistry.getPopularVenueDAO().getAllActivePopularVenueCountByProductId(Integer.parseInt(productId));
				msg= ""+count;
		} catch (Exception e) {
			e.printStackTrace();
			msg="There is something wrong..Please Try Again.";
		}
		return msg;
	}
	
	
	@RequestMapping(value = "/UpdateTicketGroupNotes")
	public GenericResponseDTO updateTicketGroupNotes(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		
		try {
			String ticketId = request.getParameter("ticketId").trim();
			String editColumn = request.getParameter("editColumn").trim();
			String notes = request.getParameter("value").trim();
			String msg = "";
			
			if(ticketId!=null && !ticketId.isEmpty() && editColumn!=null && !editColumn.isEmpty()){
				notes = notes.replace("\n", " ");
				notes = notes.replace("\t", " ");
				notes = notes.replace("\r", " ");
				DAORegistry.getCategoryTicketGroupDAO().updateCategoryTicketGroupFields(Integer.parseInt(ticketId),editColumn,notes);
				
				genericResponseDTO.setMessage(editColumn+" saved Successfully.");
				genericResponseDTO.setStatus(1);
			}else{				
				error.setDescription("No Data to Save. Please Try Again.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Updating Ticket Group Notes.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
	@RequestMapping(value="/UpdatePopularEvents")
	public void addPopulerEvents(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		try{
			Integer productId = Integer.parseInt(request.getParameter("productId"));
			//Product product = DAORegistry.getProductDAO().getProductByName(Constants.Reward_The_Fan_Product);
			
			String action = request.getParameter("action");
			if(action != null && action.equals("create")){
				
				String eventIdStr = request.getParameter("eventIds");
				String[] eventIdsArr = null;
				if(eventIdStr!=null) {
					eventIdsArr = eventIdStr.split(",");
				}
				Collection<PopularEvents> popEventsDB = DAORegistry.getPopularEventsDAO().getAllActivePopularEventsByProductId(productId);
				Map<Integer,PopularEvents> popEventsMapDB = new HashMap<Integer, PopularEvents>();
				for (PopularEvents popularEvents : popEventsDB) {
					popEventsMapDB.put(popularEvents.getEventDetails().getEventId(), popularEvents);
				}
				if(eventIdsArr != null) {
					String username = SecurityContextHolder.getContext().getAuthentication().getName();
					List<PopularEvents> popularEventsList = new ArrayList<PopularEvents>();
					List<PopularEventAudit> auditList = new ArrayList<PopularEventAudit>();
					for (String eventStr : eventIdsArr) {
						Integer eventId= Integer.parseInt(eventStr);
						if(null != popEventsMapDB.get(eventId)) {
							continue;
						}
						EventDetails eventDetail = DAORegistry.getEventDetailsDAO().get(eventId);
						if(eventDetail == null) {
							continue;
						}
						PopularEvents popEvent = new PopularEvents();
						popEvent.setEventId(eventId);
						popEvent.setStatus("ACTIVE");
						popEvent.setCreatedDate(new Date());
						popEvent.setCreatedBy(username);
						popEvent.setProductId(productId);
						//popEvent.setEventDetails(eventDetail);
						popularEventsList.add(popEvent);
						
						
						PopularEventAudit eventAudit = new PopularEventAudit(eventDetail);
						eventAudit.setAction("CREATED");
						eventAudit.setProductId(productId);
						auditList.add(eventAudit);
					}
					DAORegistry.getPopularEventsDAO().saveAll(popularEventsList);
					DAORegistry.getEventAuditDAO().saveAll(auditList);
				}
				
			} else if(action != null && action.equals("delete")){
				String eventIdStr = request.getParameter("eventIds");
				String[] eventIdsArr = null;
				if(eventIdStr!=null) {
					eventIdsArr = eventIdStr.split(",");
				}
				Collection<PopularEvents> popEventsDB = DAORegistry.getPopularEventsDAO().getAllActivePopularEventsByProductId(productId);
				Map<Integer,PopularEvents> popEventsMapDB = new HashMap<Integer, PopularEvents>();
				for (PopularEvents popularEvents : popEventsDB) {
					popEventsMapDB.put(popularEvents.getEventDetails().getEventId(), popularEvents);
				}
				
				if(eventIdsArr != null) {
					List<PopularEvents> popularEventsList = new ArrayList<PopularEvents>();
					List<PopularEventAudit> auditList = new ArrayList<PopularEventAudit>();
					for (String eventStr : eventIdsArr) {
						Integer eventId= Integer.parseInt(eventStr);
						PopularEvents popularEvent = popEventsMapDB.get(eventId);
						if(null != popularEvent) {
							popularEventsList.add(popularEvent);
						}
						/*EventDetails eventDetail = DAORegistry.getEventDetailsDAO().get(eventId);
						if(eventDetail == null) {
							continue;
						}
						PopularEvents popEvent = new PopularEvents();
						popEvent.setEventId(eventId);
						popEvent.setStatus("ACTIVE");
						popEvent.setCreatedDate(new Date());
						popEvent.setCreatedBy("");
						//popEvent.setEventDetails(eventDetail);
						
						popularEventsList.add(popEvent);*/
						
						PopularEventAudit eventAudit = new PopularEventAudit(popularEvent.getEventDetails());
						eventAudit.setAction("DELETED");
						eventAudit.setProductId(productId);
						auditList.add(eventAudit);
					}
					DAORegistry.getEventAuditDAO().saveAll(auditList);
					DAORegistry.getPopularEventsDAO().deleteAll(popularEventsList);
				}
				
			}
			response.setHeader("Content-type","application/json");
			JSONArray array = getPopularEvents(productId);
			IOUtils.write(array.toString().getBytes(),
					response.getOutputStream());
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * Get Category tickets for event
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	/*@RequestMapping(value = "/GetPopularEventDetails")
	public void getEventDetails(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		JSONArray jsonArr = new JSONArray();
		try{
			
			Integer productId = Integer.parseInt(request.getParameter("productId"));
			JSONArray array = getPopularEvents(productId);
			response.setHeader("Content-type","application/json");
			IOUtils.write(array.toString().getBytes(),
					response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	
	/**
	 * Helper method to get all popular events for given productId
	 * @param productId Integer
	 * @return JSONArray
	 */
	private JSONArray getPopularEvents(Integer productId){
		JSONArray jsonArr = new JSONArray();
		try{
			Collection<PopularEvents> popEvents = DAORegistry.getPopularEventsDAO().getAllActivePopularEventsByProductId(productId);
			JSONObject jObj = null;
			
			for (PopularEvents popEvent : popEvents) {
				EventDetails eventDtls = popEvent.getEventDetails();
				jObj = new JSONObject();
				jObj.put("eventId", eventDtls.getEventId());
				jObj.put("eventName", eventDtls.getEventName());
				jObj.put("eventDate", eventDtls.getEventDateStr());
				jObj.put("eventTime", eventDtls.getEventTimeStr());
				jObj.put("venue", eventDtls.getBuilding());
				jObj.put("city", eventDtls.getCity());
				jObj.put("state", eventDtls.getState());
				jObj.put("country", eventDtls.getCountry());
				jObj.put("artistName", eventDtls.getArtistName());
				jObj.put("grandChildCategory", eventDtls.getGrandChildCategoryName());
				jObj.put("childCategory", eventDtls.getChildCategoryName());
				jObj.put("parentCategory", eventDtls.getParentCategoryName());
				jObj.put("createdBy", popEvent.getCreatedBy());
				jObj.put("createdDate", popEvent.getCreatedDateStr());
				jsonArr.put(jObj);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return jsonArr;
	}
	
	
	@RequestMapping(value="/UpdatePopularArtist")
	public String addPopularArtist(HttpServletRequest request, HttpServletResponse response, Model model){
		PopularArtistDTO popularArtistDTO = new PopularArtistDTO();
		model.addAttribute("popularArtistDTO", popularArtistDTO);
		
		try{
			Integer productId = Integer.parseInt(request.getParameter("productId"));
			String action = request.getParameter("action");
			String headerFilter = request.getParameter("headerFilter");
			GridHeaderFilters filter = null;
			String username = request.getParameter("userName");
			if(action != null && action.equals("create")){
				filter = new GridHeaderFilters();
				String artistIdStr = request.getParameter("artistIds");
				String[] artistIdsArr = null;
				if(artistIdStr!=null) {
					artistIdsArr = artistIdStr.split(",");
				}				
				Collection<PopularArtist> popArtistDB = DAORegistry.getQueryManagerDAO().getAllActivePopularArtistByProductId(productId,filter,null);
				Map<Integer,PopularArtist> popArtistMapDB = new HashMap<Integer, PopularArtist>();
				for (PopularArtist popularArtist : popArtistDB) {
					popArtistMapDB.put(popularArtist.getArtistId(), popularArtist);
				}
				if(artistIdsArr != null) {
					List<PopularArtist> popularArtistList = new ArrayList<PopularArtist>();
					List<PopularArtistAudit> auditList = new ArrayList<PopularArtistAudit>();
					List<Integer> ids = new ArrayList<Integer>();
					for (String artistStr : artistIdsArr) {
						Integer artistId= Integer.parseInt(artistStr);
						if(null != popArtistMapDB.get(artistId)) {
							continue;
						}
						if(ids.contains(artistId)){
							continue;
						}
						Artist artist = DAORegistry.getArtistDAO().get(artistId);
						if(artist == null) {
							continue;
						}
						PopularArtist popArtist = new PopularArtist();
						popArtist.setArtistId(artistId);
						popArtist.setStatus("ACTIVE");
						popArtist.setCreatedDate(new Date());
						popArtist.setCreatedBy(username);
						popArtist.setProductId(productId);
						//popArtist.setArtist(artistDetail);
						popularArtistList.add(popArtist);
						
						PopularArtistAudit artistAudit = new PopularArtistAudit(artist.getId(),username);
						artistAudit.setAction("CREATED");
						artistAudit.setProductId(productId);
						auditList.add(artistAudit);
						ids.add(artistId);
					}
					DAORegistry.getPopularArtistDAO().saveAll(popularArtistList);
					DAORegistry.getArtistAuditDAO().saveAll(auditList);
				}
				
				//map.put("successMessage", returnMessage);
				
				//Tracking User Action
				String userActionMsg = "Popular Artist(s) Added.Artist Id's - "+artistIdStr;
				Util.userActionAudit(request, null, userActionMsg);
				
			} if(action != null && action.equals("delete")){
				String artistIdStr = request.getParameter("artistIds");
				String[] artistIdsArr = null;
				if(artistIdStr!=null) {
					artistIdsArr = artistIdStr.split(",");
				}
				filter = new GridHeaderFilters();
				Collection<PopularArtist> popArtistDB = DAORegistry.getQueryManagerDAO().getAllActivePopularArtistByProductId(productId,filter,null);
				Map<Integer,PopularArtist> popArtistMapDB = new HashMap<Integer, PopularArtist>();
				for (PopularArtist popularArtist : popArtistDB) {
					popArtistMapDB.put(popularArtist.getArtistId(), popularArtist);
				}
				
				if(artistIdsArr != null) {
					List<PopularArtist> popularArtistList = new ArrayList<PopularArtist>();
					List<PopularArtistAudit> auditList = new ArrayList<PopularArtistAudit>();
					for (String artistStr : artistIdsArr) {
						Integer artistId= Integer.parseInt(artistStr);
						PopularArtist popularArtist = popArtistMapDB.get(artistId);
						if(null != popularArtist) {
							popularArtistList.add(popularArtist);
						}
						/*EventDetails artistDetail = DAORegistry.getEventDetailsDAO().get(artistId);
						if(artistDetail == null) {
							continue;
						}
						PopularArtist popEvent = new PopularArtist();
						popEvent.setEventId(artistId);
						popEvent.setStatus("ACTIVE");
						popEvent.setCreatedDate(new Date());
						popEvent.setCreatedBy("");
						//popEvent.setEventDetails(artistDetail);
						
						popularArtistList.add(popEvent);*/
						
						PopularArtistAudit artistAudit = new PopularArtistAudit(popularArtist.getArtistId(),username);
						artistAudit.setAction("DELETED");
						artistAudit.setProductId(productId);
						auditList.add(artistAudit);
					}
					DAORegistry.getArtistAuditDAO().saveAll(auditList);
					DAORegistry.getPopularArtistDAO().deleteAll(popularArtistList);
				}
				
				//Tracking User Action
				String userActionMsg = "Popular Artist(s) Deleted.Popular Artist Id's - "+artistIdStr;
				Util.userActionAudit(request, null, userActionMsg);
			}
			if(action != null && action.equalsIgnoreCase("search")){
				filter = GridHeaderFiltersUtil.getPopularArtistSearchHeaderFilters(headerFilter);
			}
			
			popularArtistDTO.setStatus(1);
			popularArtistDTO.setPopularArtistCustomDTO(com.rtw.tracker.utils.Util.getAllArtistArray(com.rtw.tracker.utils.Util.getPopularArtists(productId, filter)));
			
		}catch (Exception e) {
			Error error = new Error();
			System.err.println(e.getLocalizedMessage());
			error.setDescription("There is something wrong. Please try again.");
			popularArtistDTO.setError(error);
			popularArtistDTO.setStatus(0);
		}
		return "";
	}

	/**
	 * Get Category tickets for artist
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	/*@RequestMapping(value = "/GetPopularArtist")
	public void getPopularArtist(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		
		try{
			Integer productId = Integer.parseInt(request.getParameter("productId"));
			JSONArray array = getPopularArtists(productId);
			response.setHeader("Content-type","application/json");
			IOUtils.write(array.toString().getBytes(),
					response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/**
	 * Helper method to get all popular artists for given productId
	 * @param productId Integer
	 * @return JSONArray
	 */
	private JSONArray getPopularArtists(Integer productId, GridHeaderFilters filter){	
		JSONArray jsonArr = new JSONArray();
		//JSONObject jObj = null;
		Collection<PopularArtist> popularArtists = new ArrayList<PopularArtist>();
		try {
			Collection<PopularArtist> popArtists = DAORegistry.getQueryManagerDAO().getAllActivePopularArtistByProductId(productId,filter,null);
			Set<Integer> artistIds = new HashSet<Integer>();
			
			for(PopularArtist popArtist: popArtists){				
				if(artistIds.add(popArtist.getArtistId())){
					popularArtists.add(popArtist);
				}
			}	
			
			jsonArr = JsonWrapperUtil.getPopArtistArray(popularArtists);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonArr;
	}
	
	
	/*@RequestMapping(value="/UpdatePopularGrandChildCategory")
	public void updatePopularGrandChildCategory(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		
		try{
			Integer productId = Integer.parseInt(request.getParameter("productId"));
			
			String action = request.getParameter("action");
			if(action != null && action.equals("create")){
				
				String grandChildIdStr = request.getParameter("grandChildIds");
				String[] grandChildIdsArr = null;
				if(grandChildIdStr!=null) {
					grandChildIdsArr = grandChildIdStr.split(",");
				}
				Collection<PopularGrandChildCategory> popGrandChildDB = DAORegistry.getPopularGrandChildCategoryDAO().getAllActivePopularGrandChildCategoryByProductId(productId);
				Map<Integer,PopularGrandChildCategory> popGrandChildMapDB = new HashMap<Integer, PopularGrandChildCategory>();
				for (PopularGrandChildCategory popularGrandChild : popGrandChildDB) {
					popGrandChildMapDB.put(popularGrandChild.getGrandChildCategoryId(), popularGrandChild);
				}
				if(grandChildIdsArr != null) {
					String username = SecurityContextHolder.getContext().getAuthentication().getName();
					List<PopularGrandChildCategory> popularGrandChildList = new ArrayList<PopularGrandChildCategory>();
					List<PopularGrandChildCategoryAudit> auditList = new ArrayList<PopularGrandChildCategoryAudit>();
					for (String grandChildStr : grandChildIdsArr) {
						Integer grandChildId= Integer.parseInt(grandChildStr);
						if(null != popGrandChildMapDB.get(grandChildId)) {
							continue;
						}
						GrandChildCategory grandChild = DAORegistry.getGrandChildCategoryDAO().get(grandChildId);
						if(grandChild == null) {
							continue;
						}
						PopularGrandChildCategory popgrandChild = new PopularGrandChildCategory();
						popgrandChild.setGrandChildCategoryId(grandChildId);
						popgrandChild.setStatus("ACTIVE");
						popgrandChild.setCreatedDate(new Date());
						popgrandChild.setCreatedBy(username);
						popgrandChild.setProductId(productId);
						//popArtist.setArtist(grandChildDetail);
						popularGrandChildList.add(popgrandChild);
						
						PopularGrandChildCategoryAudit audit = new PopularGrandChildCategoryAudit(grandChild);
						audit.setAction("CREATED");
						audit.setProductId(productId);
						auditList.add(audit);
					}
					DAORegistry.getPopularGrandChildCategoryDAO().saveAll(popularGrandChildList);
					DAORegistry.getGrandChildCategoryAuditDAO().saveAll(auditList);
				}
				
				//map.put("successMessage", returnMessage);
			} if(action != null && action.equals("delete")){
				String grandChildIdStr = request.getParameter("grandChildIds");
				String[] grandChildIdsArr = null;
				if(grandChildIdStr!=null) {
					grandChildIdsArr = grandChildIdStr.split(",");
				}
				Collection<PopularGrandChildCategory> popGrandChildDB = DAORegistry.getPopularGrandChildCategoryDAO().getAllActivePopularGrandChildCategoryByProductId(productId);
				Map<Integer,PopularGrandChildCategory> popGrandChildMapDB = new HashMap<Integer, PopularGrandChildCategory>();
				for (PopularGrandChildCategory popularGrandChild : popGrandChildDB) {
					popGrandChildMapDB.put(popularGrandChild.getGrandChildCategoryId(), popularGrandChild);
				}
				
				if(grandChildIdsArr != null) {
					List<PopularGrandChildCategory> popularGrandChildList = new ArrayList<PopularGrandChildCategory>();
					List<PopularGrandChildCategoryAudit> auditList = new ArrayList<PopularGrandChildCategoryAudit>();
					for (String grandChildStr : grandChildIdsArr) {
						Integer grandChildId= Integer.parseInt(grandChildStr);
						PopularGrandChildCategory popularChildCategory = popGrandChildMapDB.get(grandChildId);
						if(null != popularChildCategory) {
							popularGrandChildList.add(popularChildCategory);
						}
						EventDetails grandChildDetail = DAORegistry.getEventDetailsDAO().get(grandChildId);
						if(grandChildDetail == null) {
							continue;
						}
						PopularGrandChildCategory popEvent = new PopularGrandChildCategory();
						popEvent.setEventId(grandChildId);
						popEvent.setStatus("ACTIVE");
						popEvent.setCreatedDate(new Date());
						popEvent.setCreatedBy("");
						//popEvent.setEventDetails(grandChildDetail);
						
						popularArtistList.add(popEvent);
						
						PopularGrandChildCategoryAudit audit = new PopularGrandChildCategoryAudit(popularChildCategory.getGrandChildCategory());
						audit.setAction("DELETED");
						audit.setProductId(productId);
						auditList.add(audit);
					}
					DAORegistry.getGrandChildCategoryAuditDAO().saveAll(auditList);
					DAORegistry.getPopularGrandChildCategoryDAO().deleteAll(popularGrandChildList);
				}
				
			}
			JSONArray array = getPopolarGrandChildCategorys(productId);
			response.setHeader("Content-type","application/json");
			IOUtils.write(array.toString().getBytes(),
					response.getOutputStream());
		}catch (Exception e) {
			e.printStackTrace();
			//map.put("errorMessage", "There is something wrong..Please Try Again.");
			//returnMessage = "There is something wrong..Please Try Again.";
		}
	}*/

	/**
	 * Get Category tickets for artist
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	/*@RequestMapping(value = "/GetPopularGrandChildCategory")
	public void getPopularGrandChildCategory(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		try{
			Integer productId = Integer.parseInt(request.getParameter("productId"));
			JSONArray array = getPopolarGrandChildCategorys(productId);
			response.setHeader("Content-type","application/json");
			IOUtils.write(array.toString().getBytes(),
					response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/**
	 * Helper method to get all popular grand child category for given productId
	 * @param productId Integer
	 * @return JSONArray
	 */
	/*private JSONArray getPopolarGrandChildCategorys(Integer productId){
		JSONArray jsonArr = new JSONArray();
		try {
			Collection<PopularGrandChildCategory> popGrandChildList = DAORegistry.getPopularGrandChildCategoryDAO().getAllActivePopularGrandChildCategoryByProductId(productId);
			JSONObject jObj = null;
			
			for (PopularGrandChildCategory popGrandChild : popGrandChildList) {
				GrandChildCategory grandChild = popGrandChild.getGrandChildCategory();
				jObj = new JSONObject();
				jObj.put("grandChildCategoryId", grandChild.getId());
				jObj.put("grandChildCategory", grandChild.getName());
				jObj.put("childCategory", grandChild.getChildCategory().getName());
				jObj.put("parentCategory", grandChild.getChildCategory().getParentCategory().getName());
				jObj.put("createdBy", popGrandChild.getCreatedBy());
				jObj.put("createdDate", popGrandChild.getCreatedDateStr());
				jsonArr.put(jObj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonArr;
	}*/
	
	
	@RequestMapping(value="/UpdatePopularVenue")
	public void updatePopularVenue(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		
		try{
			Integer productId = Integer.parseInt(request.getParameter("productId"));
			
			String action = request.getParameter("action");
			if(action != null && action.equals("create")){
				
				String venueIdStr = request.getParameter("venueIds");
				String[] venueIdsArr = null;
				if(venueIdStr!=null) {
					venueIdsArr = venueIdStr.split(",");
				}
				Collection<PopularVenue> popVenueDB = DAORegistry.getPopularVenueDAO().getAllActivePopularVenueByProductId(productId);
				Map<Integer,PopularVenue> popVenueMapDB = new HashMap<Integer, PopularVenue>();
				for (PopularVenue popularVenue : popVenueDB) {
					popVenueMapDB.put(popularVenue.getVenueId(), popularVenue);
				}
				if(venueIdsArr != null) {
					String username = SecurityContextHolder.getContext().getAuthentication().getName();
					List<PopularVenue> popularVenueList = new ArrayList<PopularVenue>();
					List<PopularVenueAudit> auditList = new ArrayList<PopularVenueAudit>();
					for (String venueStr : venueIdsArr) {
						Integer venueId= Integer.parseInt(venueStr);
						if(null != popVenueMapDB.get(venueId)) {
							continue;
						}
						Venue venue = DAORegistry.getVenueDAO().get(venueId);
						if(venue == null) {
							continue;
						}
						PopularVenue popVenue = new PopularVenue();
						popVenue.setVenueId(venueId);
						popVenue.setStatus("ACTIVE");
						popVenue.setCreatedDate(new Date());
						popVenue.setCreatedBy(username);
						popVenue.setProductId(productId);
						//popVenue.setVenueDetails(venueDetail);
						popularVenueList.add(popVenue);
						
						PopularVenueAudit audit = new PopularVenueAudit(venue);
						audit.setAction("CREATED");
						audit.setProductId(productId);
						auditList.add(audit);
					}
					DAORegistry.getPopularVenueDAO().saveAll(popularVenueList);
					DAORegistry.getVenueAuditDAO().saveAll(auditList);
				}
				
				//map.put("successMessage", returnMessage);
			} if(action != null && action.equals("delete")){
				String venueIdStr = request.getParameter("venueIds");
				String[] venueIdsArr = null;
				if(venueIdStr!=null) {
					venueIdsArr = venueIdStr.split(",");
				}
				Collection<PopularVenue> popVenueDB = DAORegistry.getPopularVenueDAO().getAllActivePopularVenueByProductId(productId);
				Map<Integer,PopularVenue> popVenueMapDB = new HashMap<Integer, PopularVenue>();
				for (PopularVenue popularVenue : popVenueDB) {
					popVenueMapDB.put(popularVenue.getVenueId(), popularVenue);
				}
				
				if(venueIdsArr != null) {
					List<PopularVenue> popularVenueList = new ArrayList<PopularVenue>();
					List<PopularVenueAudit> auditList = new ArrayList<PopularVenueAudit>();
					for (String venueStr : venueIdsArr) {
						Integer venueId= Integer.parseInt(venueStr);
						PopularVenue popularVenue = popVenueMapDB.get(venueId);
						if(null != popularVenue) {
							popularVenueList.add(popularVenue);
						}
						/*VenueDetails venueDetail = DAORegistry.getVenueDetailsDAO().get(venueId);
						if(venueDetail == null) {
							continue;
						}
						PopularVenue popVenue = new PopularVenue();
						popVenue.setVenueId(venueId);
						popVenue.setStatus("ACTIVE");
						popVenue.setCreatedDate(new Date());
						popVenue.setCreatedBy("");
						//popVenue.setVenueDetails(venueDetail);
						
						popularVenueList.add(popVenue);*/
						
						PopularVenueAudit audit = new PopularVenueAudit(popularVenue.getVenue());
						audit.setAction("DELETED");
						audit.setProductId(productId);
						auditList.add(audit);
					}
					DAORegistry.getVenueAuditDAO().saveAll(auditList);
					DAORegistry.getPopularVenueDAO().deleteAll(popularVenueList);
				}
				
			}
			JSONArray array = getPopularVenues(productId);
			response.setHeader("Content-type","application/json");
			IOUtils.write(array.toString().getBytes(),
					response.getOutputStream());
		}catch (Exception e) {
			e.printStackTrace();
			//map.put("errorMessage", "There is something wrong..Please Try Again.");
		}
	}
	
	/**
	 * Get Popular venue
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	/*@RequestMapping(value = "/GetPopularVenueDetails")
	public void getPopularVenueDetails(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		
		try{
			Integer productId = Integer.parseInt(request.getParameter("productId"));
			JSONArray array = getPopularVenues(productId);
			response.setHeader("Content-type","application/json");
			IOUtils.write(array.toString().getBytes(),
					response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/**
	 * Helper method to get all popular venues for given productId
	 * @param productId Integer
	 * @return JSONArray
	 */
	private JSONArray getPopularVenues(Integer productId){
		JSONArray jsonArr = new JSONArray();
		try {
			Collection<PopularVenue> popVenueList = DAORegistry.getPopularVenueDAO().getAllActivePopularVenueByProductId(productId);
			JSONObject jObj = null;
			
			for (PopularVenue popVenue : popVenueList) {
				Venue venue = popVenue.getVenue();
				jObj = new JSONObject();
				jObj.put("venueId", venue.getId());
				jObj.put("venue", venue.getBuilding());
				jObj.put("city", venue.getCity());
				jObj.put("state", venue.getState());
				jObj.put("country", venue.getCountry());
				jObj.put("postalCode", venue.getPostalCode());
				jObj.put("createdBy", popVenue.getCreatedBy());
				jObj.put("createdDate", popVenue.getCreatedDateStr());
				jsonArr.put(jObj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonArr;
	}
	
	
	/**
	 * Check customer already exists
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/CheckCustomer")
	public GenericResponseDTO checkCustomer(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		
		try{
			String sessionUserName = request.getParameter("sessionUser");
			String userName = request.getParameter("userName");
			
			if(StringUtils.isEmpty(sessionUserName)){
				System.err.println("Please Login");
				error.setDescription("Please Login");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(userName)){
				System.err.println("Please provide UserName.");
				error.setDescription("Please provide UserName.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			Customer isCustomerExists = DAORegistry.getCustomerDAO().checkCustomerExists(userName);
			if(isCustomerExists != null){
				genericResponseDTO.setMessage("There is already an user with this Username or Email.");
			}else{
				genericResponseDTO.setMessage("true");
			}
			genericResponseDTO.setStatus(1);
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while Checking Custoemr");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}

	@RequestMapping(value = "/CheckUser")
	public GenericResponseDTO checkUser(HttpServletRequest request, HttpServletResponse response, Model model){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		model.addAttribute("genericResponseDTO", genericResponseDTO);
		
		try{			
			String userName = request.getParameter("userName");
			String email = request.getParameter("email");
			
			if(StringUtils.isEmpty(userName)){
				System.err.println("Please provide User Name.");
				error.setDescription("Please provide User Name.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(email)){
				System.err.println("Please provide Email.");
				error.setDescription("Please provide Email.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			TrackerUser trackerUser = DAORegistry.getTrackerUserDAO().getTrackerUserByUsernameOrEmail(userName, email);
			
			if(trackerUser != null){				
				genericResponseDTO.setMessage("There is already an user with this Username or Email.");
			}else{
				genericResponseDTO.setMessage("true");
			}
			genericResponseDTO.setStatus(1);			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while Checking User");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
	@RequestMapping(value = "/DeleteCustomer")
	public GenericResponseDTO deleteCustomer(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		
		try{
			String userIdStr = request.getParameter("userId");
			String sessionUser = request.getParameter("trackerUserName");
			String sessionUserId = request.getParameter("trackerUserId");
			
			if(StringUtils.isEmpty(sessionUser)){
				System.err.println("Please Login");
				error.setDescription("Please Login");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(userIdStr)){
				System.err.println("Please select Customer.");
				error.setDescription("Please select Customer.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			Customer deleteCustomer = DAORegistry.getCustomerDAO().get(Integer.parseInt(userIdStr));
			if(deleteCustomer == null){
				System.err.println("No Customer to Delete.");
				error.setDescription("No Customer to Delete.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(sessionUserId.equals(userIdStr)){
				System.err.println("You can't delete your own account.");
				error.setDescription("You can't delete your own account.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			DAORegistry.getCustomerDAO().delete(deleteCustomer);
			
			genericResponseDTO.setStatus(1);
			genericResponseDTO.setMessage("Customer Deleted Successfully.");
			
			//Tracking User Action
			String userActionMsg = "Customer is Deleted.";
			Util.userActionAudit(request, Integer.parseInt(userIdStr), userActionMsg);
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while Delete Customer.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
	@RequestMapping(value = "/DeleteUser")
	public GenericResponseDTO deleteUser(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		
		try{
			String userIdStr = request.getParameter("userId");
			String sessionUser = request.getParameter("trackerUserName");
			String sessionUserId = request.getParameter("trackerUserId");
			
			if(StringUtils.isEmpty(sessionUser)){
				System.err.println("Please Login");
				error.setDescription("Please Login");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(userIdStr)){
				System.err.println("Please select User.");
				error.setDescription("Please select User.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			TrackerUser trackerUser = DAORegistry.getTrackerUserDAO().get(Integer.parseInt(userIdStr));
			if(trackerUser == null){
				System.err.println("No User to Delete.");
				error.setDescription("No User to Delete.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(sessionUserId.equals(userIdStr)){
				System.err.println("You can't delete your own account.");
				error.setDescription("You can't delete your own account.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			DAORegistry.getQueryManagerDAO().deleteUserActions(trackerUser.getId());
			DAORegistry.getTrackerUserDAO().delete(trackerUser);
			//trackerUser.setStatus(false);
			//DAORegistry.getTrackerUserDAO().update(trackerUser);
			
			genericResponseDTO.setStatus(1);
			genericResponseDTO.setMessage("User Deleted Successfully.");
			
			//Tracking User Action
			String userActionMsg = "User is Deleted.";
			Util.userActionAudit(request, Integer.parseInt(userIdStr), userActionMsg);
						
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while Delete User.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
	@RequestMapping(value = "/DeleteShippingAddress")
	public GenericResponseDTO deleteShippingAddress(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		
		try{
			String userName = request.getParameter("userName");
			String shippingAddressIdStr = request.getParameter("shippingId");
			String sessionUser = request.getParameter("trackerUserName");
			
			if(StringUtils.isEmpty(sessionUser)){
				System.err.println("Please Login");
				error.setDescription("Please Login");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(shippingAddressIdStr)){
				System.err.println("Please select Customer Address to Delete.");
				error.setDescription("Please select Customer Address to Delete.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			CustomerAddress shippingAddress = DAORegistry.getCustomerAddressDAO().get(Integer.parseInt(shippingAddressIdStr));
			if(shippingAddress == null){
				System.err.println("There is no Shipping Address to Delete.");
				error.setDescription("There is no Shipping Address to Delete.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			DAORegistry.getCustomerAddressDAO().delete(shippingAddress);
			
			genericResponseDTO.setStatus(1);
			genericResponseDTO.setMessage("Shipping address Deleted Successfully.");
			
			//Tracking User Action
			String userActionMsg = "Shipping address is Deleted.";
			Util.userActionAudit(request, Integer.parseInt(shippingAddressIdStr), userActionMsg);
						
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while Delete Shipping Address.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
	@RequestMapping(value = "/getStates")
	public String getStateForCountry(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try{			
			Integer country = Integer.parseInt(request.getParameter("country"));
			model.addAttribute("states", DAORegistry.getQueryManagerDAO().getStatesForCountry(country));
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}
	
	@RequestMapping(value = "/getCountries")
	public String getCountries(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		try{
			model.addAttribute("countries", DAORegistry.getQueryManagerDAO().getCountries());			
		}catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}
	
	/**
	 * Get shipping address list for customer
	 * @param customerId
	 */
	@RequestMapping(value = "/GetShippingAddressDetails")
	public ShippingAddressDTO getCustomerShippingAddress(HttpServletRequest request, HttpServletResponse response){
		ShippingAddressDTO shippingAddressDTO = new ShippingAddressDTO();
		Error error = new Error();
		
		try {
			String customerIdStr = request.getParameter("customerId");
			String shippingAddressId = request.getParameter("shippingId");
			
			if(StringUtils.isEmpty(customerIdStr)){
				System.err.println("Please select Customer.");
				error.setDescription("Please select Customer.");
				shippingAddressDTO.setError(error);
				shippingAddressDTO.setStatus(0);
				return shippingAddressDTO;
			}
			Integer customerId = 0;
			try{
				customerId = Integer.parseInt(customerIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please select Valid Customer.");
				shippingAddressDTO.setError(error);
				shippingAddressDTO.setStatus(0);
				return shippingAddressDTO;
			}
			
			int shippingAddressCount = 0;
			List<CustomerAddress> shippingAddressList = new ArrayList<CustomerAddress>();
			
			if(shippingAddressId!=null && !shippingAddressId.isEmpty()){
				CustomerAddress shippingAddress  = DAORegistry.getCustomerAddressDAO().get(Integer.parseInt(shippingAddressId));
				shippingAddressList.add(shippingAddress);
			}else{
				shippingAddressList = DAORegistry.getCustomerAddressDAO().updatedShippingAddress(customerId);
			}
			shippingAddressCount = DAORegistry.getCustomerAddressDAO().getShippingAddressCount(customerId);
			
			shippingAddressDTO.setStatus(1);
			if(shippingAddressList != null){
				shippingAddressDTO.setShippingAddressDTOs(com.rtw.tracker.utils.Util.getShippingAddressArray(shippingAddressList));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Shipping Addresses.");
			shippingAddressDTO.setError(error);
			shippingAddressDTO.setStatus(0);
		}
		return shippingAddressDTO;
	}
	
	
	@RequestMapping(value = "/GetSavedCardsRewardPointsAndLoyaFan")
	public SavedCardsRewardPointsAndLoyalFanDTO getCustomerSavedCards(HttpServletRequest request, HttpServletResponse response){
		SavedCardsRewardPointsAndLoyalFanDTO savedCardsRewardPointsAndLoyalFanDTO = new SavedCardsRewardPointsAndLoyalFanDTO();
		Error error = new Error();
		
		try {
			String customerIdStr = request.getParameter("customerId");
			String eventIdStr = request.getParameter("eventId");
			String sessionIdStr = request.getParameter("deviceId");
			
			if(StringUtils.isEmpty(customerIdStr)){
				System.err.println("Please select Customer.");
				error.setDescription("Please select Customer.");
				savedCardsRewardPointsAndLoyalFanDTO.setError(error);
				savedCardsRewardPointsAndLoyalFanDTO.setStatus(0);
				return savedCardsRewardPointsAndLoyalFanDTO;
			}
			Integer customerId = 0;
			try{
				customerId = Integer.parseInt(customerIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please select Valid Customer.");
				savedCardsRewardPointsAndLoyalFanDTO.setError(error);
				savedCardsRewardPointsAndLoyalFanDTO.setStatus(0);
				return savedCardsRewardPointsAndLoyalFanDTO;
			}
			
			/*if(StringUtils.isEmpty(eventIdStr)){
				System.err.println("Please select Event.");
				error.setDescription("Please select Event.");
				savedCardsRewardPointsAndLoyalFanDTO.setError(error);
				savedCardsRewardPointsAndLoyalFanDTO.setStatus(0);
				return savedCardsRewardPointsAndLoyalFanDTO;
			}*/			
			if(StringUtils.isEmpty(sessionIdStr)){
				System.err.println("Session not available. Please refresh and try again.");
				error.setDescription("Please refresh and try again.");
				savedCardsRewardPointsAndLoyalFanDTO.setError(error);
				savedCardsRewardPointsAndLoyalFanDTO.setStatus(0);
				return savedCardsRewardPointsAndLoyalFanDTO;
			}
			
			Map<String, String> paramterMap = Util.getParameterMap(request);
			paramterMap.put("customerId", customerIdStr);
			String data = Util.getObject(paramterMap,sharedProperty.getApiUrl()+Constants.GET_CUSTOMER_CARDS);
		    Gson gson = new Gson();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CustomerDetails cardDetails = gson.fromJson(((JsonObject)jsonObject.get("customerDetails")), CustomerDetails.class);
			
			savedCardsRewardPointsAndLoyalFanDTO.setStatus(1);
			if(null != cardDetails.getCardList()){
				savedCardsRewardPointsAndLoyalFanDTO.setCustomerCardInfoDTO(com.rtw.tracker.utils.Util.getCustomerCardInfoArray(cardDetails.getCardList()));
			}
			
			Double rewardPoints = 0.00;
			CustomerLoyalty loyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customerId);
			if(loyalty!=null){
				rewardPoints = loyalty.getActivePoints();
			}
			savedCardsRewardPointsAndLoyalFanDTO.setRewardPoints(rewardPoints);
			
			
			CustomerWallet wallet = DAORegistry.getCustomerWalletDAO().getCustomerWalletByCustomerId(customerId);
			if(wallet!=null){
				savedCardsRewardPointsAndLoyalFanDTO.setCustomerCredit(wallet.getActiveCredit());
			}else{
				savedCardsRewardPointsAndLoyalFanDTO.setCustomerCredit(0.0);
			}
									
			boolean isLoyalFanPrice = false;
			
			/*if(eventIdStr != null && !eventIdStr.isEmpty()){
			    Map<String, String> map = Util.getParameterMap(request);
			    map.put("customerId", customerIdStr);
			    map.put("eventId", eventIdStr);
			    map.put("sessionId", sessionIdStr);
			    
			    String respdata = Util.getObject(map,sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.VALIDATE_LOYALFAN);
			    Gson respgson = new Gson();  
				JsonObject jsnObject = respgson.fromJson(respdata, JsonObject.class);
				ValidateCustomerLoyalFan validateLoyalFan = respgson.fromJson(((JsonObject)jsnObject.get("validateCustomerLoyalFan")), ValidateCustomerLoyalFan.class);
				
				if(validateLoyalFan.getStatus() == 1){
					if(validateLoyalFan.getIsLoyalFanForTicTracker() != null && validateLoyalFan.getIsLoyalFanForTicTracker()){
						isLoyalFanPrice = true;
					}
				}
			}*/
			savedCardsRewardPointsAndLoyalFanDTO.setIsLoyalFanPrice(isLoyalFanPrice);
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Saved Cars and Reward Points, Loyal Fan Info.");
			savedCardsRewardPointsAndLoyalFanDTO.setError(error);
			savedCardsRewardPointsAndLoyalFanDTO.setStatus(0);
		}
		return savedCardsRewardPointsAndLoyalFanDTO;
	}
	
	@RequestMapping(value="/CheckCardPosition")
	public String checkCardPosition(HttpServletRequest request, HttpServletResponse response, Model model){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		model.addAttribute("genericResponseDTO", genericResponseDTO);
		String returnMessage ="";
		
		try{
			Integer productId = Integer.parseInt(request.getParameter("productId"));
			Integer desktopPosition = Integer.parseInt(request.getParameter("dPosition"));
			Integer mobilePosition = Integer.parseInt(request.getParameter("mPosition"));
			Integer cardId = null;
			if(request.getParameter("cardId") != null && request.getParameter("cardId").trim().length() > 0) {
				cardId = Integer.parseInt(request.getParameter("cardId"));
			}
			Cards card = DAORegistry.getCardsDAO().getExistingCardsByCardPostionsAndCardId(productId,cardId,desktopPosition,mobilePosition);
			if(card == null) {
				returnMessage ="No Card Exist for this Positions.";
			} else {
				if(card.getDesktopPosition().equals(desktopPosition) && card.getMobilePosition().equals(mobilePosition)) {
					returnMessage ="Card already exist for this desktop and mobile positions.";
				} else if(card.getDesktopPosition().equals(desktopPosition)) {
					returnMessage ="Card already exist for this desktop position.";
				} else if(card.getMobilePosition().equals(mobilePosition)) {
					returnMessage ="Card already exist for this mobile position.";
				}
				
			}
			
			genericResponseDTO.setStatus(1);
			genericResponseDTO.setMessage(returnMessage);
		}catch (Exception e) {
			Error error = new Error();
			System.err.println(e.getLocalizedMessage());
			error.setDescription("There is something wrong..Please Try Again.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return returnMessage;
	}
	
	
	@RequestMapping(value = "/saveEventNotes")
	public GenericResponseDTO saveEventNotes(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		
		try {
			String eventId = request.getParameter("eventId");
			String notes = request.getParameter("notes");
			
			if(eventId!=null && !eventId.isEmpty() && notes!=null && !notes.isEmpty()){
				notes = notes.replace("\n", " ");
				notes = notes.replace("\t", " ");
				notes = notes.replace("\r", " ");
				DAORegistry.getQueryManagerDAO().saveEventNotes(Integer.parseInt(eventId), notes);

				genericResponseDTO.setStatus(1);
				genericResponseDTO.setMessage("Notes saved Successfully.");
			}else{
				error.setDescription("No Data to Save. Please Try Again.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Save Event Notes.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
	/**
	 * Updates Event Notes
	 * @param tmatEventId
	 */
	@RequestMapping(value = "/saveVenueNotes")
	public @ResponseBody String saveVenuetNotes(HttpServletRequest request, HttpServletResponse response){
		String msg="";
		try {
			String venueId = request.getParameter("venueId");
			String notes = request.getParameter("notes");
			if(venueId!=null && !venueId.isEmpty() && notes!=null && !notes.isEmpty()){
				notes = notes.replace("\n", " ");
				notes = notes.replace("\t", " ");
				notes = notes.replace("\r", " ");
				DAORegistry.getQueryManagerDAO().saveVenueNotes(Integer.parseInt(venueId), notes);
				msg= "Notes saved successfully.";
			}else{
				msg="No Data found to save, Please try again.";
			}
		} catch (Exception e) {
			e.printStackTrace();
			msg="There is something wrong..Please Try Again.";
		}
		return msg;
	}
	
	
	@RequestMapping(value = "/SaveUserPreference")
	public UserPreferenceDTO saveUserPreference(HttpServletRequest request, HttpServletResponse response){
		UserPreferenceDTO userPreferenceDTO = new UserPreferenceDTO();
		Error error = new Error();
		
		try {
			String columnOrders = request.getParameter("columns");
			String gridName = request.getParameter("gridName");
			String userName = request.getParameter("userName");
			
			UserPreference preference = null;
			String msg="";			
			if(columnOrders!=null && !columnOrders.isEmpty() && gridName!=null && !gridName.isEmpty()){
				preference = DAORegistry.getUserPreferenceDAO().getPreferenceByUserNameandGridName(userName,gridName);
				int index = columnOrders.lastIndexOf(",");
				if(index == (columnOrders.length()-1)){
					columnOrders = 	columnOrders.substring(0, index);
				}
				if(preference==null){
					preference = new UserPreference();
					preference.setColumnOrders(columnOrders);
					preference.setUserName(userName);
					preference.setGridName(gridName);
				}else{
					preference.setColumnOrders(columnOrders);
				}
				DAORegistry.getUserPreferenceDAO().saveOrUpdate(preference);
				
				userPreferenceDTO.setStatus(1);
				userPreferenceDTO.setMessage("OK");
				userPreferenceDTO.setColumnOrders(preference.getColumnOrders());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Save User Preference.");
			userPreferenceDTO.setError(error);
			userPreferenceDTO.setStatus(0);
		}
		return userPreferenceDTO;
	}
	
	@RequestMapping({"/GetStates"})
	public LoadStateDTO clientBrokerManageDetailsPage(HttpServletRequest request, HttpServletResponse response){
		LoadStateDTO loadStateDTO = new LoadStateDTO();
		Error error = new Error();
		
		try{
			String countryIdStr = request.getParameter("countryId");
			
			if(StringUtils.isEmpty(countryIdStr)){
				System.err.println("Please Selelct Country");
				error.setDescription("Please Selelct Country");
				loadStateDTO.setError(error);
				loadStateDTO.setStatus(0);
				return loadStateDTO;
			}
			
			List<State> stateList = new ArrayList<State>();
			if(countryIdStr.contains(",")){
				String arr[] = countryIdStr.split(",");
				for(String str : arr){
					if(str != null && !str.isEmpty()){
						if(str.equalsIgnoreCase("217")){
							stateList.addAll(DAORegistry.getStateDAO().getAllStateForUSA());
						}else{
							stateList.addAll(DAORegistry.getQueryManagerDAO().getStatesForCountry(Integer.parseInt(str)));
						}
					}
				}
			}else{
				stateList = DAORegistry.getQueryManagerDAO().getStatesForCountry(Integer.parseInt(countryIdStr));
			}
			
			loadStateDTO.setStateDTO(com.rtw.tracker.utils.Util.getStateArray(stateList));
			loadStateDTO.setStatus(1);
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching States");
			loadStateDTO.setError(error);
			loadStateDTO.setStatus(0);
		}
		return loadStateDTO;
	}
	
	@RequestMapping({"/GetFantasyChildCategory"})
	public FantasyChildCategoryDTO getChildCategoryByParentID(HttpServletRequest request, HttpServletResponse response){
		FantasyChildCategoryDTO fantasyChildCategoryDTO = new FantasyChildCategoryDTO();
		Error error = new Error();
		
		try{
			String parentId = request.getParameter("parentIdStr");
			String parentType = request.getParameter("parentType");
			
			List<FantasyChildCategory> childCategoryList = null;			
			if(parentId!=null && !parentId.isEmpty()){
				childCategoryList = DAORegistry.getFantasyChildCategoryDAO().getChildCategoryByParentId(Integer.parseInt(parentId));
			}if(parentType != null && !parentType.isEmpty()){
				FantasyParentCategory parentCategory = DAORegistry.getFantasyParentCategoryDAO().getParentCategoryByName(parentType);
				childCategoryList = DAORegistry.getFantasyChildCategoryDAO().getChildCategoryByParentId(parentCategory.getId());
			}
			
			if(childCategoryList != null && childCategoryList.size() > 0){
				FanasyCategoryDTO fanasyCategoryDTO = null;
				List<FanasyCategoryDTO> fanasyCategoryDTOs = new ArrayList<FanasyCategoryDTO>();
				for(FantasyChildCategory childCategory : childCategoryList){
					fanasyCategoryDTO = new FanasyCategoryDTO();
					fanasyCategoryDTO.setId(childCategory.getId());
					fanasyCategoryDTO.setName(childCategory.getName());
					fanasyCategoryDTOs.add(fanasyCategoryDTO);
				}
				fantasyChildCategoryDTO.setFanasyCategoryDTO(fanasyCategoryDTOs);
				fantasyChildCategoryDTO.setStatus(1);
			}else{
				error.setDescription("there is no Fantasy Child Category found.");
				fantasyChildCategoryDTO.setError(error);
				fantasyChildCategoryDTO.setStatus(0);
			}
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Fantasy Child Category.");
			fantasyChildCategoryDTO.setError(error);
			fantasyChildCategoryDTO.setStatus(0);
		}
		return fantasyChildCategoryDTO;
	}
	
	@RequestMapping({"/GetFantasyGrandChildCategory"})
	public FantasyGrandChildCategoryDTO getGrandChildCategoryByChildID(HttpServletRequest request, HttpServletResponse response){
		FantasyGrandChildCategoryDTO fantasyGrandChildCategoryDTO = new FantasyGrandChildCategoryDTO();
		Error error = new Error();
		
		try{
			String childIdStr = request.getParameter("childIdStr");
			
			List<FantasyGrandChildCategory> grandChildCategoryList = new ArrayList<FantasyGrandChildCategory>();
			JSONArray array = new JSONArray();
			if(childIdStr!=null && !childIdStr.isEmpty() && !childIdStr.equalsIgnoreCase("undefined")){
				grandChildCategoryList = DAORegistry.getFantasyGrandChildCategoryDAO().getGrandChildsByChildCategoryId(Integer.parseInt(childIdStr));
			}

			if(grandChildCategoryList != null && grandChildCategoryList.size() > 0){
				FanasyCategoryDTO fanasyCategoryDTO = null;
				List<FanasyCategoryDTO> fanasyCategoryDTOs = new ArrayList<FanasyCategoryDTO>();
				for(FantasyGrandChildCategory grandChildCategory : grandChildCategoryList){
					fanasyCategoryDTO = new FanasyCategoryDTO();
					fanasyCategoryDTO.setId(grandChildCategory.getId());
					fanasyCategoryDTO.setName(grandChildCategory.getName());
					fanasyCategoryDTOs.add(fanasyCategoryDTO);
				}
				fantasyGrandChildCategoryDTO.setFanasyCategoryDTO(fanasyCategoryDTOs);
				fantasyGrandChildCategoryDTO.setStatus(1);
			}else{
				error.setDescription("There is no Fantasy Grand Child Category found.");
				fantasyGrandChildCategoryDTO.setError(error);
				fantasyGrandChildCategoryDTO.setStatus(0);
			}
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Fantasy Grand Child Category by Child.");
			fantasyGrandChildCategoryDTO.setError(error);
			fantasyGrandChildCategoryDTO.setStatus(0);
		}
		return fantasyGrandChildCategoryDTO;
	}
	
	
	@RequestMapping({"/getFantasyCategoryTeam"})
	public FantasyCategoryTeamDTO getFantasyCategoryTeam(HttpServletRequest request, HttpServletResponse response){
		FantasyCategoryTeamDTO fantasyCategoryTeamDTO = new FantasyCategoryTeamDTO();
		Error error = new Error();
		
		try{
			String teamId = request.getParameter("teamId");
			
			FanasyCatTeamDTO fanasyCatTeamDTO = null;
			if(teamId!=null && !teamId.isEmpty()){
				CrownJewelCategoryTeams team = DAORegistry.getCrownJewelCategoryTeamsDAO().get(Integer.parseInt(teamId));
				fanasyCatTeamDTO = new FanasyCatTeamDTO();
				fanasyCatTeamDTO.setId(team.getId());
				fanasyCatTeamDTO.setName(team.getName());
				fanasyCatTeamDTO.setFractionOdds(team.getFractionalOdd());
				fanasyCatTeamDTO.setOdds(team.getOdds());
				fanasyCatTeamDTO.setMarkup(team.getMarkup());
				fanasyCatTeamDTO.setCutoffDate(team.getCutOffDateStr());
				
				fantasyCategoryTeamDTO.setStatus(1);
				fantasyCategoryTeamDTO.setFanasyCatTeamDTO(fanasyCatTeamDTO);
			}else{
				error.setDescription("There is no Fantasy Category Team found.");
				fantasyCategoryTeamDTO.setError(error);
				fantasyCategoryTeamDTO.setStatus(0);
			}
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Fantasy Category Team.");
			fantasyCategoryTeamDTO.setError(error);
			fantasyCategoryTeamDTO.setStatus(0);
		}
		return fantasyCategoryTeamDTO;
	}
	
	@RequestMapping({"/GetFantasyEventsByGrandChild"})
	public FantasyEventsFromGrandChildDTO getFantasyEventsByGrandChild(HttpServletRequest request, HttpServletResponse response){
		FantasyEventsFromGrandChildDTO fantasyEventsFromGrandChildDTO = new FantasyEventsFromGrandChildDTO();
		Error error = new Error();
		
		try{
			String grandChildStr = request.getParameter("grandChildCategoryId");
			String customerIdStr = request.getParameter("customerId");
						
			Map<String, String> map = Util.getParameterMap(request);			
		    map.put("customerId", customerIdStr);
		    map.put("fCategoryId", grandChildStr);
		    
		    String data = Util.getObject(map,sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.GET_FANTASY_EVENTS);
		    Gson gson = new Gson();  
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			FantasySportsProduct fantasySportsProductList = gson.fromJson(((JsonObject)jsonObject.get("fantasySportsProduct")), FantasySportsProduct.class);

			FantasyCategories fantasyCategories = null;
			List<FantasyEventTeamTicket> fantasyEventTeamTicketList = null;
			
			FantasyEventTeamTicketDTO fantasyEventTeamTicketDTO = null;
			List<FantasyEventTeamTicketDTO> fantasyEventTeamTicketDTOs = new ArrayList<FantasyEventTeamTicketDTO>();
			
			if(fantasySportsProductList.getStatus() == 1){
				fantasyCategories = fantasySportsProductList.getFantasyEvent();
				fantasyEventTeamTicketList = fantasyCategories.getFantasyTeamTickets();
								
				for(FantasyEventTeamTicket fantasyEventTeam : fantasyEventTeamTicketList){
					fantasyEventTeamTicketDTO = new FantasyEventTeamTicketDTO();
					fantasyEventTeamTicketDTO.setCategoryId(fantasyEventTeam.getCategoryId());
					fantasyEventTeamTicketDTO.setTeamId(fantasyEventTeam.getTeamId());
					fantasyEventTeamTicketDTO.setTeamName(fantasyEventTeam.getTeamName());
					fantasyEventTeamTicketDTO.setEventId(fantasyEventTeam.getFantasyEventId());
					fantasyEventTeamTicketDTO.setTeamZoneId(fantasyEventTeam.getTeamZoneId());
					fantasyEventTeamTicketDTO.setZone(fantasyEventTeam.getZone());
					fantasyEventTeamTicketDTO.setQuantity(fantasyEventTeam.getQuantity());
					fantasyEventTeamTicketDTO.setRequiredPoints(fantasyEventTeam.getRequiredPoints());
					fantasyEventTeamTicketDTO.setTicketId(fantasyEventTeam.getTicketId());
					fantasyEventTeamTicketDTO.setIsRealTicket(fantasyEventTeam.getIsRealTicket());
					fantasyEventTeamTicketDTO.setTmatEventId(fantasyEventTeam.getTmatEventId());
					fantasyEventTeamTicketDTO.setEventDateStr(fantasyEventTeam.getEventDateStr());
					fantasyEventTeamTicketDTO.setEventTimeStr(fantasyEventTeam.getEventTimeStr());
					fantasyEventTeamTicketDTO.setVenueName(fantasyEventTeam.getVenueName());
					fantasyEventTeamTicketDTO.setCity(fantasyEventTeam.getCity());
					fantasyEventTeamTicketDTO.setState(fantasyEventTeam.getState());
					fantasyEventTeamTicketDTO.setCountry(fantasyEventTeam.getCountry());
					fantasyEventTeamTicketDTOs.add(fantasyEventTeamTicketDTO);
				}
				fantasyEventsFromGrandChildDTO.setStatus(1);
				fantasyEventsFromGrandChildDTO.setFantasyEventTeamTicketDTOs(fantasyEventTeamTicketDTOs);
				fantasyEventsFromGrandChildDTO.setPackageInfo(fantasyCategories.getPackageInformation());
			}
			
			fantasyEventsFromGrandChildDTO.setEventTeamTicketPaginationDTO(PaginationUtil.getDummyPaginationParameters(fantasyEventTeamTicketList.size()));
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Fantasy Events By GrandChild.");
			fantasyEventsFromGrandChildDTO.setError(error);
			fantasyEventsFromGrandChildDTO.setStatus(0);
		}
		return fantasyEventsFromGrandChildDTO;
	}
	
	@RequestMapping({"/GetFantasyTeamsbyEventId"})
	public void getFantasyTeamsbyEventId(HttpServletRequest request, HttpServletResponse response){
		String leagueIdStr = request.getParameter("leagueId");
		JSONObject object= null;
		JSONArray array = new JSONArray();
		try{
			if(leagueIdStr!=null && !leagueIdStr.isEmpty()){
				List<CrownJewelTeams> teams = DAORegistry.getCrownJewelTeamsDAO().getAllActiveTeamsByLeagueId(Integer.parseInt(leagueIdStr));
				for(CrownJewelTeams  team : teams){
					object= new JSONObject();
					object.put("id", team.getId());
					object.put("name", team.getName());
					array.put(object);
				}
			}
			IOUtils.write(array.toString().getBytes(), response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping({"/GetFantasyTicketByTeam"})
	public void getFantasyTicketByTeam(HttpServletRequest request, HttpServletResponse response){
		String leagueIdStr = request.getParameter("leagueId");
		JSONObject object= null;
		JSONArray array = new JSONArray();
		try{
			if(leagueIdStr!=null && !leagueIdStr.isEmpty()){
				List<CrownJewelTeams> teams = DAORegistry.getCrownJewelTeamsDAO().getAllActiveTeamsByLeagueId(Integer.parseInt(leagueIdStr));
				for(CrownJewelTeams  team : teams){
					object= new JSONObject();
					object.put("id", team.getId());
					object.put("name", team.getName());
					array.put(object);
				}
			}
			IOUtils.write(array.toString().getBytes(), response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping({"/GetFantasyTickets"})
	public void getFantasyTickets(HttpServletRequest request, HttpServletResponse response){
		String leagueIdStr = request.getParameter("leagueId");
		String customerIdStr = request.getParameter("customerId");
		String teamIdStr = request.getParameter("teamId");
		JSONObject jsonObject =  new JSONObject();
		try{
			if(customerIdStr==null || customerIdStr.isEmpty()){
				
			}else if (teamIdStr==null || teamIdStr.isEmpty()){
				
			}else if(leagueIdStr==null || leagueIdStr.isEmpty()){
				
			}else{
				Map<String, String> map = Util.getParameterMap(request);
				map.put("leagueId",leagueIdStr);
				map.put("teamId",teamIdStr);
				map.put("customerId",customerIdStr);
				
				String data = Util.getObject(map, Constants.BASE_URL+Constants.GET_SPORTS_CROWN_JEWEL_TICKETS);
				Gson gson = new Gson();		
				JsonObject jsonObject1 = gson.fromJson(data, JsonObject.class);
				CrownJewelTicketList crownJewelTicketList = gson.fromJson(((JsonObject)jsonObject1.get("crownJewelTicketList")), CrownJewelTicketList.class);
				int count = 0;
				if(crownJewelTicketList!=null && crownJewelTicketList.getSectionTicketsMap()!=null
						&& !crownJewelTicketList.getSectionTicketsMap().isEmpty()){
					count = crownJewelTicketList.getSectionTicketsMap().size();
				}
				jsonObject.put("data", data);
				jsonObject.put("ticketPagingInfo", PaginationUtil.getDummyPaginationParameter(count));
			}
			IOUtils.write(jsonObject.toString().getBytes(), response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping({"/SetGlobalProduct"})
	public void setProductTypeForUser(HttpServletRequest request, HttpServletResponse response,HttpSession session){
		try {
			String product = request.getParameter("productType");
			if(product!=null && !product.isEmpty() && !product.equals("-1")){
				session.setAttribute("productType",product);
			}else{
				session.setAttribute("productType",null);
			}
			IOUtils.write("OK".toString().getBytes(), response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping({"/SetGlobalBroker"})
	public GlobalBrokerDTO setBrokerForUser(HttpServletRequest request, HttpServletResponse response){
		GlobalBrokerDTO globalBrokerDTO = new GlobalBrokerDTO();
		Error error = new Error();
	
		try {
			String brokerIdStr = request.getParameter("brokerId");
			
			if(brokerIdStr!=null && !brokerIdStr.isEmpty() && !brokerIdStr.equals("-1")){
				TrackerBrokers trackerBroker = DAORegistry.getTrackerBrokersDAO().get(Integer.parseInt(brokerIdStr));
				if(trackerBroker != null){
					globalBrokerDTO.setCompanyName(trackerBroker.getCompanyName());
				}
				globalBrokerDTO.setBrokerId(brokerIdStr);
				globalBrokerDTO.setStatus(1);
				globalBrokerDTO.setMessage("OK");
			}else{
				globalBrokerDTO.setStatus(0);
				globalBrokerDTO.setBrokerId(null);
				globalBrokerDTO.setCompanyName(null);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Setting Global Broker.");
			globalBrokerDTO.setError(error);
			globalBrokerDTO.setStatus(0);
		}
		return globalBrokerDTO;
	}
	

	@RequestMapping(value = "/SignUp",method=RequestMethod.POST)
	@ResponseBody
	public String addSignUp(HttpServletRequest request, HttpServletResponse response, HttpSession session){//ModelMap map){
		String returnMessage = "";
		try{
			String action = request.getParameter("action");
			String firstName = request.getParameter("firstName");
			String lastName = request.getParameter("lastName");
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			String phoneNo = request.getParameter("phoneNo");
			String userRole = request.getParameter("userRole");
			String companyName = request.getParameter("companyName");
			String serviceFees = request.getParameter("serviceFees");
			TrackerUser trackerUserDb = null;
			
			if(firstName == null || firstName.isEmpty()){
				returnMessage= "Firstname can't be blank";
			}else if(lastName == null || lastName.isEmpty()){
				returnMessage= "Lastname can't be blank";
			}else if(email == null || email.isEmpty()){
				returnMessage= "Email can't be blank";
			}else if(!validateEMail(email)){
				returnMessage = "Invalid Email.";
			}else if(password == null || password.isEmpty()){
				returnMessage= "Password can't be blank";
			}else if(userRole == null || userRole.isEmpty()){
				returnMessage = "Please choose at least one Role.";
			}else if(userRole.toUpperCase().equals("ROLE_BROKER")){
				if(companyName == null || companyName.isEmpty()){
					returnMessage= "Company Name can't be blank";
				}
				if(serviceFees == null || serviceFees.isEmpty()){
					returnMessage= "Service Fess can't be blank";
				}
			}
			if(returnMessage.isEmpty()){
				trackerUserDb = DAORegistry.getTrackerUserDAO().getTrackerUserByUsernameOrEmail(email, email);
				TrackerBrokers trackerBrokersDb = null;
				if(trackerUserDb != null){
					returnMessage = "There is already an user with this Username or Email.";
				}else{
					trackerUserDb = new TrackerUser();
					
					// adding user to database
					trackerUserDb.setFirstName(firstName);
					trackerUserDb.setLastName(lastName);
					trackerUserDb.setUserName(email);
					trackerUserDb.setEmail(email);						
					trackerUserDb.setPassword(encryptPwd(password));
					
					if(phoneNo == null || phoneNo.isEmpty()){
						trackerUserDb.setPhone(null);
					}else{
						trackerUserDb.setPhone(phoneNo);
					}
						
					if(userRole.toUpperCase().equals("ROLE_BROKER")){
						trackerBrokersDb = new TrackerBrokers();
						trackerBrokersDb.setCompanyName(companyName);
						trackerBrokersDb.setServiceFees(Double.parseDouble(serviceFees));
						DAORegistry.getTrackerBrokersDAO().save(trackerBrokersDb);
						
						trackerUserDb.setBroker(trackerBrokersDb);
					}
					
					/*if(userRole.toUpperCase().equals("ROLE_AFFILIATES")){
						trackerUserDb.setPromotionalCode(Util.generateCustomerReferalCode());
					}*/
					
					Set<Role> roleList = new HashSet<Role>();
					Role roleDb = DAORegistry.getRoleDAO().getRoleByName(userRole.toUpperCase());
					roleList.add(roleDb);
							
					trackerUserDb.setRoles(roleList);	
					trackerUserDb.setStatus(true);
					trackerUserDb.setCreateDate(new Date());
					trackerUserDb.setCreatedBy("AUTO");
					
					DAORegistry.getTrackerUserDAO().save(trackerUserDb);
					returnMessage = "You are registered successfully.";
					//map.put("successMessage", returnMessage);					
				}
			}else{
				//map.put("errorMessage", returnMessage);
				return returnMessage;
			}
		}catch(Exception e){
			e.printStackTrace();
			//map.put("errorMessage", "There is something wrong..Please Try Again.");
			returnMessage = "There is something wrong..Please Try Again.";
		}		
		//return "page-login";
		return returnMessage;
	}	
	
	@RequestMapping(value = "/GetAffiliates", method = RequestMethod.POST)
	public void getAffiliatesSearchPage(HttpServletRequest request, HttpServletResponse response,HttpSession session){
		Integer count = 0;
		String pageNo = request.getParameter("pageNo");
		String headerFilter = request.getParameter("headerFilter");
		String status = request.getParameter("status");
		Map<Integer, AffiliateCashReward> cashRewardMapByUserId = new HashMap<Integer, AffiliateCashReward>();
		Collection<TrackerUser> trackerUsers = null;
		JSONObject object = new JSONObject();
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getAffiliatesSearchHeaderFilters(headerFilter);
			trackerUsers = DAORegistry.getTrackerUserDAO().getAllTrackerUsersByRole("ROLE_AFFILIATES", status, filter, pageNo);
			Collection<AffiliateCashReward> rewardList = DAORegistry.getAffiliateCashRewardDAO().getAll();
			
			for (AffiliateCashReward affiliateCashReward : rewardList) {
				cashRewardMapByUserId.put(affiliateCashReward.getUser().getId(), affiliateCashReward);
			}
			if(trackerUsers != null && trackerUsers.size() > 0){
				count = trackerUsers.size();
			}else if(trackerUsers == null || trackerUsers.size() <= 0){
				object.put("msg", "No "+status+" Affiliates found.");
			}
			
			object.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, null));
			object.put("affiliates", JsonWrapperUtil.getManageAffiliatesArray(trackerUsers,cashRewardMapByUserId));
			IOUtils.write(object.toString().getBytes(), response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();			
		}		
	}
	
	/*
	 * Getting Affiliate Report
	 */
	/*@RequestMapping(value = "/AffiliateReport", method = RequestMethod.GET)
	public String getAffiliateReportPage(HttpServletRequest request, HttpServletResponse response,HttpSession session,ModelMap map){
		Integer count = 0;
		AffiliateCashReward affiliateCashReward = null;
		Collection<AffiliateCashRewardHistory> historyList = null;
		try{
			GridHeaderFilters filter = new GridHeaderFilters();
			TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
			
			affiliateCashReward = DAORegistry.getAffiliateCashRewardDAO().getAffiliateByUserId(trackerUserSession.getId());
			historyList = DAORegistry.getAffiliateCashRewardHistoryDAO().getHistoryByUserId(trackerUserSession.getId(), filter);			
			count = DAORegistry.getAffiliateCashRewardHistoryDAO().getHistoryCountByUserId(trackerUserSession.getId(), filter);
		}catch(Exception e){
			e.printStackTrace();
			map.put("error", "There is something wrong..Please Try Again.");
		}
		map.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, null));
		map.put("affiliates", JsonWrapperUtil.getAffiliatesArray(historyList));
		map.put("affiliateCashReward", affiliateCashReward);
		return "page-affiliate-report";		
	}*/
	
		
	@RequestMapping(value = "/GetAffiliateReport", method = RequestMethod.POST)
	public void getAffiliateReportData(HttpServletRequest request, HttpServletResponse response,HttpSession session){
		Integer count =0;		
		String pageNo = request.getParameter("pageNo");
		String headerFilter = request.getParameter("headerFilter");
		
		AffiliateCashReward affiliateCashReward = null;
		Collection<AffiliateCashRewardHistory> historyList = null;
		JSONObject object = new JSONObject();
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getAffiliateReportSearchHeaderFilters(headerFilter);
			TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
			
			affiliateCashReward = DAORegistry.getAffiliateCashRewardDAO().getAffiliateByUserId(trackerUserSession.getId());
			historyList = DAORegistry.getAffiliateCashRewardHistoryDAO().getHistoryByUserId(trackerUserSession.getId(), filter);			
			count = DAORegistry.getAffiliateCashRewardHistoryDAO().getHistoryCountByUserId(trackerUserSession.getId(), filter);
			
			object.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, null));
			object.put("affiliates", JsonWrapperUtil.getAffiliatesArray(historyList));
			object.put("affiliateCashReward", affiliateCashReward);
			IOUtils.write(object.toString().getBytes(), response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/*@RequestMapping(value = "/AffiliateOrderDetails", method = RequestMethod.GET)
	public String getAffiliateReportDetails(HttpServletRequest request, HttpServletResponse response,HttpSession session,ModelMap map){
		Integer count = 0;
		AffiliateCashReward affiliateCashReward = null;
		AffiliateCashRewardHistory rewardHistory = null;
		try{
			GridHeaderFilters filter = new GridHeaderFilters();
			String userId = request.getParameter("userId");
			String orderId = request.getParameter("orderId");
			
			TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
			if(userId != null && !userId.isEmpty()){
				affiliateCashReward = DAORegistry.getAffiliateCashRewardDAO().getAffiliateByUserId(Integer.parseInt(userId));
				rewardHistory = DAORegistry.getAffiliateCashRewardHistoryDAO().getHistoryByUserIdAndOrderId(Integer.parseInt(userId), Integer.parseInt(orderId));
			}else{
				affiliateCashReward = DAORegistry.getAffiliateCashRewardDAO().getAffiliateByUserId(trackerUserSession.getId());
				rewardHistory = DAORegistry.getAffiliateCashRewardHistoryDAO().getHistoryByUserIdAndOrderId(trackerUserSession.getId(), Integer.parseInt(orderId));
			}
		     String eventDateTime = rewardHistory.getOrder().getEventDateStr();
		     
		     if(rewardHistory.getOrder().getEventTimeStr().equals("TBD")){
		      eventDateTime = eventDateTime +" 12:00 AM";
		     }else{
		      eventDateTime = eventDateTime +" "+rewardHistory.getOrder().getEventTimeStr();
		     }
		     
		     SimpleDateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
		     Date activeDate = dateTimeFormat.parse(eventDateTime);
		     Calendar calendar = Calendar.getInstance();
		     calendar.setTime(activeDate);
		     calendar.add(Calendar.DAY_OF_MONTH, 1);
		     
		     activeDate = calendar.getTime();
		     String activeDateStr = dateTimeFormat.format(activeDate);
		     map.put("activeDate", activeDateStr);
		}catch(Exception e){
			e.printStackTrace();
			map.put("error", "There is something wrong..Please Try Again.");
		}
		map.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, null));
		map.put("affiliateHistory", rewardHistory);
		map.put("affiliateCashReward", affiliateCashReward);		
		return "page-affiliate-order-summary";		
	}*/
	
	/*@RequestMapping(value = "/ExportAffiliatesDetails", method = RequestMethod.GET)
	public void exportAffiliatesDetailsToExcel(HttpServletRequest request, HttpServletResponse response,HttpSession session){
		Integer count =0;
		String pageNo = request.getParameter("pageNo");
		String headerFilter = request.getParameter("headerFilter");
		
		Collection<AffiliateCashReward> rewardList = null;
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getAffiliatesSearchHeaderFilters(headerFilter);
			
			rewardList = DAORegistry.getAffiliateCashRewardDAO().getAllAffiliates(filter);			
			count = DAORegistry.getAffiliateCashRewardDAO().getAllAffiliatesCount(filter);
			
			
			if(rewardList!=null && !rewardList.isEmpty()){
				HSSFWorkbook book = new HSSFWorkbook();
				HSSFSheet sheet = book.createSheet("AffiliatesDetails");
				HSSFRow header = sheet.createRow(0);
				header.createCell(0).setCellValue("First Name");
				header.createCell(1).setCellValue("Last Name");
				header.createCell(2).setCellValue("Active Cash");
				header.createCell(3).setCellValue("Pending Cash");
				header.createCell(4).setCellValue("Last Credited Cash");
				header.createCell(5).setCellValue("Last Debited Cash");
				header.createCell(6).setCellValue("Total Credited Cash");
				header.createCell(7).setCellValue("Total Debited Cash");
				header.createCell(8).setCellValue("Last Update");
				int i=1;
				for(AffiliateCashReward reward : rewardList){
					HSSFRow row = sheet.createRow(i);
					row.createCell(0).setCellValue(reward.getUser().getFirstName());
					row.createCell(1).setCellValue(reward.getUser().getLastName());
					row.createCell(2).setCellValue(reward.getActiveCash());
					row.createCell(3).setCellValue(reward.getPendingCash());
					row.createCell(4).setCellValue(reward.getLastCreditedCash());
					row.createCell(5).setCellValue(reward.getLastDebitedCash());
					row.createCell(6).setCellValue(reward.getTotalCreditedCash());
					row.createCell(7).setCellValue(reward.getTotalDebitedCash());
					row.createCell(8).setCellValue(reward.getLastUpdatedDateStr());
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=AffiliatesDetails.xls");
				book.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	public static boolean validateEMail(String email){
		try{
			final String emailPattern = 
					"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
					+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
			Pattern pattern;
			Matcher matcher;
			
			pattern = Pattern.compile(emailPattern);
			matcher = pattern.matcher(email);
			return matcher.matches();
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Method to encrypt the password
	 * @param input
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public static String encryptPwd(String input) throws NoSuchAlgorithmException {
		MessageDigest mDigest = MessageDigest.getInstance("SHA1");
		byte[] result = mDigest.digest(input.getBytes());
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < result.length; i++) {
			sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}
	
	/*@RequestMapping("/GetAffiliatePromoHistory")
	public String getAffiliatePromoHistory(HttpServletRequest request, HttpServletResponse response, ModelMap map)throws Exception {
		String userId = request.getParameter("userId");
		List<AffiliatePromoCodeHistory> affiliatePromoCodeHistories = null;
		JSONArray array = new JSONArray();
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
		try {
			if(userId!=null && !userId.isEmpty()){
				affiliatePromoCodeHistories = DAORegistry.getAffiliatePromoCodeHistoryDAO().getAllHistoryByUserId(Integer.parseInt(userId.trim()));
				JSONObject object = null;
				for(AffiliatePromoCodeHistory obj :affiliatePromoCodeHistories){
					object = new JSONObject();
					object.put("id", obj.getId());
					object.put("promoCode",obj.getPromoCode());
					object.put("createDate",dateFormat.format(obj.getCreateDate()));
					object.put("lastUpdated",dateFormat.format(obj.getUpdateDate()));
					object.put("status",obj.getStatus());
					object.put("fromDate",obj.getEffectiveFromDate()!= null?dateFormat.format(obj.getEffectiveFromDate()):"");
					object.put("toDate",obj.getEffectiveToDate()!=null?dateFormat.format(obj.getEffectiveToDate()):"");
					array.put(object);
				}
			}
			//IOUtils.write(array.toString().getBytes(),response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}
		map.put("promoCodeHistory", array);
		map.put("pagingInfo",PaginationUtil.getDummyPaginationParameter(affiliatePromoCodeHistories!=null?affiliatePromoCodeHistories.size():0));
		return "page-affiliates-promocode-history";
	}*/
	
	@RequestMapping("/AutoCompleteCityStateCountry")
	public AutoCompleteDTO getAutoCompleteCityStateCountry(HttpServletRequest request, HttpServletResponse response) throws Exception {
		AutoCompleteDTO autoCompleteDTO = new AutoCompleteDTO();
		Error error = new Error();
		
		try{
			String param = request.getParameter("q");
			String strResponse = "";
			
			List<LoyalFanStateSearch> stateList = null;
			
			Map<String, String> map = Util.getParameterMap(request);			
		    map.put("searchKey", param);
		    
		    String data = Util.getObject(map,sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.AUTOCOMPLETE_LOYALFAN);
		    Gson gson = new Gson();  
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			LoyalFanAutoSearch autoSearchList = gson.fromJson(((JsonObject)jsonObject.get("loyalFanAutoSearch")), LoyalFanAutoSearch.class);
			
			if(autoSearchList.getStatus() == 1){
				stateList = autoSearchList.getSearchList();
			}
			if (stateList == null) {
				System.err.println("There is no LoyalFan State from your Search");
				error.setDescription("There is no LoyalFan State from your Search");
				autoCompleteDTO.setError(error);
				autoCompleteDTO.setStatus(0);
				return autoCompleteDTO;
			}
			if (stateList != null) {
				for (LoyalFanStateSearch state : stateList) {
					strResponse += ("STATE" + "|" + state.getLoyalFanSelectionValue() + "|" + state.getCity() + "|" + state.getState() + "|" + state.getCountry() + "|" + state.getZipCode() + "|" + state.getSearchGridValue() + "|" + state.getBoxDisplayValue() + "\n") ;
				}
			}
			
			autoCompleteDTO.setStatus(1);
			autoCompleteDTO.setAutoCompleteStringResponse(strResponse);
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while Auto Complete CityStateCountry Search");
			autoCompleteDTO.setError(error);
			autoCompleteDTO.setStatus(0);
		}	
		return autoCompleteDTO;
	}
	
	
	@RequestMapping("/AutoCompleteCustomerArtist")
	public AutoCompleteDTO getAutoCompleteCustomerArtist(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AutoCompleteDTO autoCompleteDTO = new AutoCompleteDTO();
		Error error = new Error();
		
		try{
			String param = request.getParameter("q");
			String strResponse = "";
			Collection<Artist> artists = null;
			
			Map<String, String> map = Util.getParameterMap(request);
		    map.put("searchKey", param);
		    map.put("parentType", "SPORTS");
		    
		    String data = Util.getObject(map,sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.AUTOCOMPLETE_ARTIST);
		    Gson gson = new Gson();  
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ArtistList artistList = gson.fromJson(((JsonObject)jsonObject.get("artistList")), ArtistList.class);
			
			if(artistList.getStatus() == 1){
				artists = artistList.getArtists();
			}
			if (artists == null) {
				System.err.println("There is no Customer Artists from your Search");
				error.setDescription("There is no Customer Artists from your Search");
				autoCompleteDTO.setError(error);
				autoCompleteDTO.setStatus(0);
				return autoCompleteDTO;
			}
			if (artists != null) {
				for (Artist artist : artists) {
					strResponse += ("ARTIST" + "|" + artist.getId() + "|" + artist.getName() + "\n") ;
				}
			}
			
			autoCompleteDTO.setStatus(1);
			autoCompleteDTO.setAutoCompleteStringResponse(strResponse);
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while Auto Complete Customer Artists Search");
			autoCompleteDTO.setError(error);
			autoCompleteDTO.setStatus(0);
		}	
		return autoCompleteDTO;
	}
	
	
	@RequestMapping("/AutoCompleteEventDetails")
	public AutoCompleteEventsDTO getAutoCompleteEventDetails(HttpServletRequest request, HttpServletResponse response) throws Exception {
		AutoCompleteEventsDTO autoCompleteEventsDTO = new AutoCompleteEventsDTO();
		Error error = new Error();
		//model.addAttribute("autoCompleteEventsDTO", autoCompleteEventsDTO);
		
		try{
			String param = request.getParameter("q");
			String strResponse ="";
			Collection<Event> events = DAORegistry.getQueryManagerDAO().getAllActiveEventWithVenue(param);
			
			if (events == null) {
				System.err.println("There is no Event from your Search");
				error.setDescription("There is no Event from your Search");
				autoCompleteEventsDTO.setError(error);
				autoCompleteEventsDTO.setStatus(0);
				return autoCompleteEventsDTO;
			}
			if (events != null) {
				for (Event event : events) {
					strResponse += ("EVENT" + "|" + event.getEventId() + "|" + event.getEventName() + "|" + event.getNameWithDateandVenue() + "\n") ;
				}
			}			
			autoCompleteEventsDTO.setStatus(1);
			autoCompleteEventsDTO.setEvents(strResponse);
			
		}catch(Exception e){
			e.printStackTrace();
			System.err.println(e.getLocalizedMessage());
			error.setDescription("Something went wrong while fetching Auto Complete Event Details");
			autoCompleteEventsDTO.setError(error);
			autoCompleteEventsDTO.setStatus(0);
		}		
		return autoCompleteEventsDTO;
	}

	@RequestMapping({"/GetCityStateCountryAutoSearch"})
	 public CityStateCountryAutoSearchDTO getCityStateCountryAutoSearch(HttpServletRequest request, HttpServletResponse response){
		CityStateCountryAutoSearchDTO cityStateCountryAutoSearchDTO = new CityStateCountryAutoSearchDTO();
		Error error = new Error();
		
		try{
			String zipCodeStr = request.getParameter("zipCode");			
			/*if(StringUtils.isEmpty(zipCodeStr)){
				System.err.println("Please provide ZipCode.");
				error.setDescription("Please provide ZipCode.");
				cityStateCountryAutoSearchDTO.setError(error);
				cityStateCountryAutoSearchDTO.setStatus(0);
				return cityStateCountryAutoSearchDTO;
			}*/
			
			List<CityAutoSearch> cityStateCountryList = null;
			List<Country> countryList = null;
			List<State> stateList = null;			
			CityAutoSearch cityStateCountry = null;
			
			stateList = DAORegistry.getQueryManagerDAO().getStates();
			countryList = DAORegistry.getQueryManagerDAO().getCountries();
			cityStateCountryList = DAORegistry.getQueryManagerDAO().getCityStateCountry(zipCodeStr);
						
			if(cityStateCountryList != null && cityStateCountryList.size() > 0){				
				cityStateCountry = cityStateCountryList.get(0);
				cityStateCountryAutoSearchDTO.setCityStateCountryDTO(com.rtw.tracker.utils.Util.getCityStateCountryValues(cityStateCountry));
			}
			cityStateCountryAutoSearchDTO.setCountryDTO(com.rtw.tracker.utils.Util.getCountryArray(countryList));
			cityStateCountryAutoSearchDTO.setStateDTO(com.rtw.tracker.utils.Util.getStateArray(stateList));
			cityStateCountryAutoSearchDTO.setStatus(1);
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Auto Complete - City State Country from ZipCode");
			cityStateCountryAutoSearchDTO.setError(error);
			cityStateCountryAutoSearchDTO.setStatus(0);
		}
		return cityStateCountryAutoSearchDTO;
	}
	
	@RequestMapping("/AutoCompleteBrokers")
	public AutoCompleteDTO getAutoCompleteBrokers(HttpServletRequest request, HttpServletResponse response) throws Exception {
		AutoCompleteDTO autoCompleteDTO = new AutoCompleteDTO();
		Error error = new Error();
		
		try{
			String param = request.getParameter("q");
			String strResponse ="";
			
			Collection<TrackerBrokers> trackerBrokers = DAORegistry.getTrackerBrokersDAO().getBrokersByName(param);
			
			if (trackerBrokers == null) {
				System.err.println("There is no Broker found from your Search");
				error.setDescription("There is no Broker found from your Search");
				autoCompleteDTO.setError(error);
				autoCompleteDTO.setStatus(0);
				return autoCompleteDTO;
			}
			if (trackerBrokers != null) {
				for (TrackerBrokers trackerBroker : trackerBrokers) {
					strResponse += ("BROKER" + "|" + trackerBroker.getId() + "|" + trackerBroker.getCompanyName() + "\n") ;
				}
			}
			
			autoCompleteDTO.setStatus(1);
			autoCompleteDTO.setAutoCompleteStringResponse(strResponse);
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while Auto Complete Brokers Search.");
			autoCompleteDTO.setError(error);
			autoCompleteDTO.setStatus(0);
		}	
		return autoCompleteDTO;
	}
	
	@RequestMapping({"/GetFantasyTicketValidation"})
	public FantasyTicketValidationDTO getFantasyTicketValidation(HttpServletRequest request, HttpServletResponse response){
		FantasyTicketValidationDTO fantasyTicketValidationDTO = new FantasyTicketValidationDTO();
		Error error = new Error();
		
		try{
			String customerIdStr = request.getParameter("customerId");
			String fantasyCategoryIdStr = request.getParameter("grandChildCategoryId");
			String fantasyEventIdStr = request.getParameter("fantasyEventId");
			String fantasyTeamIdStr = request.getParameter("fantasyTeamId");
			String fantasyTicketIdStr = request.getParameter("fantasyTicketId");
			String fantasyTeamZoneIdStr = request.getParameter("fantasyTeamZoneId");
			String isRealTicketStr = request.getParameter("isRealTicket");
			String zoneStr = request.getParameter("zone");
			String quantityStr = request.getParameter("quantity");
			String requiredPointsStr = request.getParameter("requiredPoints");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("customerId",customerIdStr);
			map.put("fCategoryId",fantasyCategoryIdStr);
			map.put("fEventId",fantasyEventIdStr);
			map.put("fTeamId",fantasyTeamIdStr);
			map.put("fTicketId",fantasyTicketIdStr);
			map.put("fTeamZoneId",fantasyTeamZoneIdStr);
			map.put("isRealTicket",isRealTicketStr);
			map.put("zone",zoneStr);
			map.put("quantity",quantityStr);
			map.put("requiredPoints",requiredPointsStr);
			
			String data = Util.getObject(map, sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.VALIDATE_FST_ORDER);
			Gson gson = new Gson();		
			JsonObject jsonObject1 = gson.fromJson(data, JsonObject.class);
			com.rtw.tracker.datas.FantasySportsProduct fantasySportsProduct = gson.fromJson(((JsonObject)jsonObject1.get("fantasySportsProduct")), FantasySportsProduct.class);
			
			if(fantasySportsProduct.getStatus() == 1){
				fantasyTicketValidationDTO.setStatus(1);
				fantasyTicketValidationDTO.setMessage(fantasySportsProduct.getTicTrackerMessage());
				if(fantasySportsProduct.getProceedPurchase() != null && fantasySportsProduct.getProceedPurchase()){
					fantasyTicketValidationDTO.setProceedPurchase("Yes");
				}else{
					fantasyTicketValidationDTO.setProceedPurchase("No");
				}
			}else{
				error.setDescription(fantasySportsProduct.getError().getDescription());
				fantasyTicketValidationDTO.setError(error);
				fantasyTicketValidationDTO.setStatus(0);
			}			
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Fantasy Ticket Validation from RewardTheFan API.");
			fantasyTicketValidationDTO.setError(error);
			fantasyTicketValidationDTO.setStatus(0);
		}
		return fantasyTicketValidationDTO;
	}
	
	@RequestMapping(value = "/CheckUserName")
	public CheckUserNameDTO checkUserName(HttpServletRequest request, HttpServletResponse response, Model model){
		CheckUserNameDTO checkUserNameDTO = new CheckUserNameDTO();
		Error error = new Error();
		model.addAttribute("checkUserNameDTO", checkUserNameDTO);
		
		try{			
			String userName = request.getParameter("userName");
			
			if(StringUtils.isEmpty(userName)){
				System.err.println("Please provide User Name.");
				error.setDescription("Please provide User Name.");
				checkUserNameDTO.setError(error);
				checkUserNameDTO.setStatus(0);
				return checkUserNameDTO;
			}
			
			TrackerUser trackerUser = DAORegistry.getTrackerUserDAO().getTrackerUserByUsernameOrEmail(userName, userName);
			
			if(trackerUser != null){
				checkUserNameDTO.setStatus(1);
				checkUserNameDTO.setMessage("There is already an user with this Username or Email.");
				checkUserNameDTO.setTrackerUser(trackerUser);
			}else{
				checkUserNameDTO.setStatus(0);
				checkUserNameDTO.setMessage("false");
			}
						
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while Checking User Name.");
			checkUserNameDTO.setError(error);
			checkUserNameDTO.setStatus(0);
		}
		return checkUserNameDTO;
	}
	
	@RequestMapping(value = "/GettingUserPreference")
	public UserPreferenceDTO gettingUserPreference(HttpServletRequest request, HttpServletResponse response){
		UserPreferenceDTO userPreferenceDTO = new UserPreferenceDTO();
		Error error = new Error();
		
		try {
			String userName = request.getParameter("userName");
			
			List<UserPreference> preference = null;			
			preference = DAORegistry.getUserPreferenceDAO().getPreferenceByUserName(userName);
			userPreferenceDTO.setStatus(1);
			userPreferenceDTO.setUserPreferences(preference);
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Getting User Preference.");
			userPreferenceDTO.setError(error);
			userPreferenceDTO.setStatus(0);
		}
		return userPreferenceDTO;
	}
	
	@RequestMapping({"/GetTrackerBroker"})
	public GettingBrokerDTO GetTrackerBroker(HttpServletRequest request, HttpServletResponse response){
		GettingBrokerDTO gettingBrokerDTO = new GettingBrokerDTO();
		Error error = new Error();
	
		try {
			String brokerIdStr = request.getParameter("brokerId");
			
			if(brokerIdStr!=null && !brokerIdStr.isEmpty() && !brokerIdStr.equals("-1")){
				Collection<TrackerBrokers> trackerBrokers = DAORegistry.getTrackerBrokersDAO().getAll();
				TrackerBrokers trackerBroker = DAORegistry.getTrackerBrokersDAO().get(Integer.parseInt(brokerIdStr));
				if(trackerBroker != null){
					gettingBrokerDTO.setCompanyName(trackerBroker.getCompanyName());
				}
				gettingBrokerDTO.setBrokerId(brokerIdStr);
				gettingBrokerDTO.setStatus(1);
				gettingBrokerDTO.setTrackerBrokers(trackerBrokers);
				gettingBrokerDTO.setTrackerBroker(trackerBroker);
			}else{
				gettingBrokerDTO.setStatus(0);
				gettingBrokerDTO.setBrokerId(null);
				gettingBrokerDTO.setCompanyName(null);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Getting Broker.");
			gettingBrokerDTO.setError(error);
			gettingBrokerDTO.setStatus(0);
		}
		return gettingBrokerDTO;
	}
	
	@RequestMapping({"/GetTrackerUser"})
	public GettingUserDTO GetTrackerUser(HttpServletRequest request, HttpServletResponse response){
		GettingUserDTO gettingUserDTO = new GettingUserDTO();
		Error error = new Error();
	
		try {
			String userIdStr = request.getParameter("userId");
			
			if(userIdStr!=null && !userIdStr.isEmpty()){
				TrackerUser trackerUser = DAORegistry.getTrackerUserDAO().get(Integer.parseInt(userIdStr));
				
				gettingUserDTO.setStatus(1);
				gettingUserDTO.setTrackerUser(trackerUser);
			}else{
				gettingUserDTO.setStatus(0);
				gettingUserDTO.setTrackerUser(null);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Getting User.");
			gettingUserDTO.setError(error);
			gettingUserDTO.setStatus(0);
		}
		return gettingUserDTO;
	}
	
	@RequestMapping({"/UpdateUserAction"})
	public GenericResponseDTO updateUserAction(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
	
		try {
			String userName = request.getParameter("trackerUserName");
			String userId = request.getParameter("trackerUserId");
			String clientIPAddress = request.getParameter("clientIPAddress");
			String actionURI = request.getParameter("actionURI");			
			String userActionMsg = request.getParameter("userActionMsg");
			String dataIdStr = request.getParameter("dataId");
			
			Integer dataId = null;
			if(dataIdStr != null && !dataIdStr.isEmpty()){
				dataId = Integer.parseInt(dataIdStr);
			}
			
			UserAction userActionAudit = new UserAction();
			userActionAudit.setUserName(userName);
			userActionAudit.setUserId(Integer.parseInt(userId));
			userActionAudit.setAction(actionURI);
			userActionAudit.setIpAddress(clientIPAddress);
			userActionAudit.setTimeStamp(new Date());
			userActionAudit.setMessage(userActionMsg);
			if(dataId != null){
				userActionAudit.setDataId(dataId);
			}
			
			DAORegistry.getUserActionDAO().save(userActionAudit);
			
			genericResponseDTO.setStatus(1);
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Saving User Action.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
}
