package com.rtw.tracker.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtw.tmat.utils.Constants;
import com.rtw.tmat.utils.Error;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.aws.AWSFileService;
import com.rtw.tracker.aws.AwsS3Response;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.dao.implementation.QuizDAORegistry;
import com.rtw.tracker.datas.Customer;
import com.rtw.tracker.datas.CustomerComment;
import com.rtw.tracker.datas.CustomerReportedMedia;
import com.rtw.tracker.datas.FanClubVideo;
import com.rtw.tracker.datas.PollingCategory;
import com.rtw.tracker.datas.PollingCategoryQuestion;
import com.rtw.tracker.datas.PollingContCategoryMapper;
import com.rtw.tracker.datas.PollingContest;
import com.rtw.tracker.datas.PollingCustomerVideo;
import com.rtw.tracker.datas.PollingRewards;
import com.rtw.tracker.datas.PollingSponsor;
import com.rtw.tracker.datas.PollingVideoCategory;
import com.rtw.tracker.datas.PollingVideoInventory;
import com.rtw.tracker.datas.RtfPointsConversionSetting;
import com.rtw.tracker.datas.RtfRewardConfigInfo;
import com.rtw.tracker.datas.SuperFanLevels;
import com.rtw.tracker.pojos.AWSResponseDTO;
import com.rtw.tracker.pojos.AbuseReportedDTO;
import com.rtw.tracker.pojos.AutoCompleteArtistAndCategoryDTO;
import com.rtw.tracker.pojos.CommonRespInfo;
import com.rtw.tracker.pojos.GenericResponseDTO;
import com.rtw.tracker.pojos.PollingCategoryDTO;
import com.rtw.tracker.pojos.PollingCategoryQuestionDTO;
import com.rtw.tracker.pojos.PollingContestCategoryMapperDTO;
import com.rtw.tracker.pojos.PollingContestDTO;
import com.rtw.tracker.pojos.PollingCustomerVideoDTO;
import com.rtw.tracker.pojos.PollingRewardsDTO;
import com.rtw.tracker.pojos.PollingSponsorDTO;
import com.rtw.tracker.pojos.PollingVideoCategoryDTO;
import com.rtw.tracker.pojos.PollingVideoInventoryDTO;
import com.rtw.tracker.pojos.RTFRewardConfigDTO;
import com.rtw.tracker.pojos.RtfPointConversionSettingDTO;
import com.rtw.tracker.pojos.SponsorAutoComplete;
import com.rtw.tracker.pojos.SuperFanLevelDTO;
import com.rtw.tracker.utils.FileUploadUtil;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.GridHeaderFiltersUtil;
import com.rtw.tracker.utils.GsonCustomConfig;
import com.rtw.tracker.utils.PaginationUtil;
import com.rtw.tracker.utils.URLUtil;
import com.rtw.tracker.utils.Util;
@Controller
public class PollingApiController {
	
	private static Logger contestLog = LoggerFactory.getLogger(ContestController.class);
	SharedProperty sharedProperty;

	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}

	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}
	
	@RequestMapping(value = "/GetPollingSponsors")
	public PollingSponsorDTO getPollingSponsor(HttpServletRequest request, HttpServletResponse response) {
		PollingSponsorDTO pollingSponsorDTO = new PollingSponsorDTO();
		Error error = new Error();
		try {
			String headerFilter = request.getParameter("headerFilter");
			String pcStatus = request.getParameter("pcStatus");
			String status = request.getParameter("status");
			String sortingString = request.getParameter("sortingString");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getPollingSponsorFilter(headerFilter + sortingString);
			if (pcStatus == null || pcStatus.isEmpty()) {
				pollingSponsorDTO.setStatus(0);
				error.setDescription("Please send valid status either ACTIVE or INACTIVE.");
				pollingSponsorDTO.setError(error);
				return pollingSponsorDTO;
			}
			List<PollingSponsor> pollingSponsors = DAORegistry.getPollingSponsorDAO().getAllPollingSponsor(null, filter,
					pcStatus);
			pollingSponsorDTO.setPollingSponsor(pollingSponsors);
			pollingSponsorDTO.setPagination(PaginationUtil.getDummyPaginationParameters(pollingSponsors.size()));
			pollingSponsorDTO.setStatus(1);
			return pollingSponsorDTO;
		} catch (Exception e) {
			e.printStackTrace();
			pollingSponsorDTO.setStatus(0);
			error.setDescription("Error occured while fetching POLLING SPONSORS.");
			pollingSponsorDTO.setError(error);
			return pollingSponsorDTO;
		}

	}

	@RequestMapping(value = "/GetPollingCategory")
	public PollingCategoryDTO getPollingCategory(HttpServletRequest request, HttpServletResponse response) {
		PollingCategoryDTO pollingCategoryDTO = new PollingCategoryDTO();
		Error error = new Error();
		try {
			String headerFilter = request.getParameter("headerFilter");
			String gcStatus = request.getParameter("gcStatus");
			String sortingString = request.getParameter("sortingString");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getGiftCardFilter(headerFilter + sortingString);
			if (gcStatus == null || gcStatus.isEmpty()) {
				pollingCategoryDTO.setStatus(0);
				error.setDescription("Please send valid status either ACTIVE or INACTIVE.");
				pollingCategoryDTO.setError(error);
				return pollingCategoryDTO;
			}
			List<PollingCategory> pollingCategory = DAORegistry.getPollingCategoryDAO().getAllPollingCategory(null,
					filter, gcStatus);
			pollingCategoryDTO.setPollingCategory(pollingCategory);
			int size = 0;
			if (pollingCategory != null) {
				size = pollingCategory.size();
			}
			pollingCategoryDTO.setPagination(PaginationUtil.getDummyPaginationParameters(size));
			pollingCategoryDTO.setStatus(1);
			return pollingCategoryDTO;
		} catch (Exception e) {
			e.printStackTrace();
			pollingCategoryDTO.setStatus(0);
			error.setDescription("Error occured while fetching Polling Categories.");
			pollingCategoryDTO.setError(error);
			return pollingCategoryDTO;
		}

	}
	
	@RequestMapping(value = "/GetPollingCategoryByContId")
	public PollingContestCategoryMapperDTO getPollingCategoryByContId(HttpServletRequest request, HttpServletResponse response) {
		PollingContestCategoryMapperDTO pollingContestCategoryMapperDTO = new PollingContestCategoryMapperDTO();
		Error error = new Error();
		try {
			String headerFilter = request.getParameter("headerFilter");
			String pcStatus = request.getParameter("pcStatus");
			String contestId = request.getParameter("pollId");
			String sortingString = request.getParameter("sortingString");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getGiftCardFilter(headerFilter + sortingString);
			if (pcStatus == null || pcStatus.isEmpty()) {
				pollingContestCategoryMapperDTO.setStatus(0);
				error.setDescription("Please send valid status either ACTIVE or INACTIVE.");
				pollingContestCategoryMapperDTO.setError(error);
				return pollingContestCategoryMapperDTO;
			}
			List<PollingContCategoryMapper> pollingContCategoryMapper = DAORegistry.getPollingContestCategoryMapperDAO().getPollingCategoryByContest(contestId,
					pcStatus);
			if(pollingContCategoryMapper == null || pollingContCategoryMapper.size() <= 0){
				pollingContestCategoryMapperDTO.setMessage("No Category found for selected polling.");
			}
			pollingContestCategoryMapperDTO.setPollingContCategoryMapper(pollingContCategoryMapper);
			int size = 0;
			if (pollingContCategoryMapper != null) {
				size = pollingContCategoryMapper.size();
			}
			pollingContestCategoryMapperDTO.setPagination(PaginationUtil.getDummyPaginationParameters(size));
			pollingContestCategoryMapperDTO.setStatus(1);
			return pollingContestCategoryMapperDTO;
		} catch (Exception e) {
			e.printStackTrace();
			pollingContestCategoryMapperDTO.setStatus(0);
			error.setDescription("Error occured while fetching Polling Categories.");
			pollingContestCategoryMapperDTO.setError(error);
			return pollingContestCategoryMapperDTO;
		}

	}
	
	@RequestMapping(value = "/GetPollingCategorySelection")
	public PollingContestCategoryMapperDTO GetPollingCategorySelection(HttpServletRequest request, HttpServletResponse response) {
		PollingContestCategoryMapperDTO pollingContestCategoryMapperDTO = new PollingContestCategoryMapperDTO();
		Error error = new Error();
		try {
			String headerFilter = request.getParameter("headerFilter");
			String pcStatus = request.getParameter("pcStatus");
			String pollId = request.getParameter("pollId");
			String sortingString = request.getParameter("sortingString");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getGiftCardFilter(headerFilter + sortingString);
			if (pcStatus == null || pcStatus.isEmpty()) {
				pollingContestCategoryMapperDTO.setStatus(0);
				error.setDescription("Please send valid status either ACTIVE or INACTIVE.");
				pollingContestCategoryMapperDTO.setError(error);
				return pollingContestCategoryMapperDTO;
			}
			List<PollingContCategoryMapper> pollingContCategoryMapper = DAORegistry.getPollingContestCategoryMapperDAO().getPollingCategoryBySelection(pollId,
					pcStatus);
			pollingContestCategoryMapperDTO.setPollingContCategoryMapper(pollingContCategoryMapper);
			int size = 0;
			if (pollingContCategoryMapper != null) {
				size = pollingContCategoryMapper.size();
			}
			pollingContestCategoryMapperDTO.setPagination(PaginationUtil.getDummyPaginationParameters(size));
			pollingContestCategoryMapperDTO.setStatus(1);
			return pollingContestCategoryMapperDTO;
		} catch (Exception e) {
			e.printStackTrace();
			pollingContestCategoryMapperDTO.setStatus(0);
			error.setDescription("Error occured while fetching Polling Categories.");
			pollingContestCategoryMapperDTO.setError(error);
			return pollingContestCategoryMapperDTO;
		}

	}

	@RequestMapping(value = "/GetPollingContest")
	public PollingContestDTO getPollingContest(HttpServletRequest request, HttpServletResponse response) {
		PollingContestDTO pollingContestDTO = new PollingContestDTO();
		Error error = new Error();
		try {
			String headerFilter = request.getParameter("headerFilter");
			String pcStatus = request.getParameter("pcStatus");
			String sortingString = request.getParameter("sortingString");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getPollingContestFilter(headerFilter + sortingString);
			if (pcStatus == null || pcStatus.isEmpty()) {
				pollingContestDTO.setStatus(0);
				error.setDescription("Please send valid status either ACTIVE or INACTIVE.");
				pollingContestDTO.setError(error);
				return pollingContestDTO;
			}
			List<PollingContest> pollingContest = DAORegistry.getPollingContestDAO().getAllPollingContest(null, filter,
					pcStatus);
			pollingContestDTO.setPollingContest(pollingContest);
			int size = 0;
			if (pollingContest != null) {
				size = pollingContest.size();
			}
			pollingContestDTO.setPagination(PaginationUtil.getDummyPaginationParameters(size));
			pollingContestDTO.setStatus(1);
			return pollingContestDTO;
		} catch (Exception e) {
			e.printStackTrace();
			pollingContestDTO.setStatus(0);
			error.setDescription("Error occured while fetching Polling Categories.");
			pollingContestDTO.setError(error);
			return pollingContestDTO;
		}

	}

	@RequestMapping(value = "/GetPollingRewards")
	public PollingRewardsDTO getPollingRewards(HttpServletRequest request, HttpServletResponse response) {
		PollingRewardsDTO pollingRewardsDTO = new PollingRewardsDTO();
		Error error = new Error();
		try {
			String gcStatus = request.getParameter("gcStatus");
			String contestId = request.getParameter("contestId");
			if (contestId == null || contestId.isEmpty()) {
				pollingRewardsDTO.setStatus(0);
				error.setDescription("Please send valid Polling Id for Rewards Selection [ " + contestId + "]");
				pollingRewardsDTO.setError(error);
				return pollingRewardsDTO;
			}
			Integer contId = null;
			try {
				contId = Integer.parseInt(contestId);
			} catch (Exception e) {
				pollingRewardsDTO.setStatus(0);
				error.setDescription("Please send Numeral Polling Id for Rewards Selection [ " + contId + "]");
				pollingRewardsDTO.setError(error);
				return pollingRewardsDTO;
			}
			PollingRewards pollingRewards = DAORegistry.getPollingRewardsDAO().getPollingRewardsForContestId(contId);
			pollingRewardsDTO.setPollingRewards(pollingRewards);
			pollingRewardsDTO.setPagination(PaginationUtil.getDummyPaginationParameters(0));
			pollingRewardsDTO.setStatus(1);
			return pollingRewardsDTO;
		} catch (Exception e) {
			e.printStackTrace();
			pollingRewardsDTO.setStatus(0);
			error.setDescription("Error occured while fetching Polling Rewards.");
			pollingRewardsDTO.setError(error);
			return pollingRewardsDTO;
		}
	}
	
	@RequestMapping(value = "/UpdatePollingPrizes")
	public GenericResponseDTO updatePollingPrizes(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		//model.addAttribute("genericResponseDTO", genericResponseDTO);
		
		String action = request.getParameter("action");	
		String status = request.getParameter("pcStatus");
		String pollId = request.getParameter("pollId");
		String pollPrizeId = request.getParameter("pollPrizeId");
		String starsPerQue = request.getParameter("starsPerQue");
		String lifePerQue = request.getParameter("lifePerQue");
		String eraserPerQue = request.getParameter("eraserPerQue");
		String rtfPointsPerQue = request.getParameter("rtfPointsPerQue");
		String returnMessage = "";
		try{			
			if(action.equalsIgnoreCase("create") || action.equalsIgnoreCase("update")){
				PollingRewards pollingRewards = new PollingRewards();
				pollingRewards.setContestId(Integer.parseInt(pollId));
				pollingRewards.setId(Integer.parseInt(pollPrizeId));
				pollingRewards.setEraserPerQue(Integer.parseInt(eraserPerQue));
				pollingRewards.setLifePerQue(Integer.parseInt(lifePerQue));
				pollingRewards.setStarsPerQue(Integer.parseInt(starsPerQue));
				pollingRewards.setRtfPointsPerQue(Integer.parseInt(rtfPointsPerQue));
				pollingRewards.setCreatedBy(request.getParameter("userName"));
				pollingRewards.setStatus("ACTIVE");
				
				DAORegistry.getPollingRewardsDAO().saveOrUpdate(pollingRewards);
				returnMessage = "Polling Rewards Created/Updated successfully";
			}
			
			genericResponseDTO.setStatus(1);
			genericResponseDTO.setMessage(returnMessage);
		} catch (Exception e) {			
			e.printStackTrace();
			error.setDescription("Something went wrong while updating Polling Rewards");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
	@RequestMapping(value = "/UpdatePollingSponsor")
	public PollingSponsorDTO UpdatePollingSponsor(HttpServletRequest request, HttpServletResponse response) {
		PollingSponsorDTO pollingSponsorDTO = new PollingSponsorDTO();
		Error error = new Error();
		try {
			String id = request.getParameter("id");
			String title = request.getParameter("title");
			String description = request.getParameter("description");
			String contactPerson = request.getParameter("contactPerson");
			String email = request.getParameter("email");

			String pcStatus = request.getParameter("pcStatus");
			String action = request.getParameter("action");
			String userName = request.getParameter("userName");
			String phone = request.getParameter("phone");
			String altPhone = request.getParameter("altPhone");
			String address = request.getParameter("address");
			String fileRequired = request.getParameter("fileRequired");
			String imageUrl = request.getParameter("imageUrl");

			if (action == null || action.isEmpty()) {
				pollingSponsorDTO.setStatus(0);
				error.setDescription("Please send valid action to update/save Polling Sponsor.");
				pollingSponsorDTO.setError(error);
				return pollingSponsorDTO;
			}
			GridHeaderFilters filter = GridHeaderFiltersUtil.getPollingSponsorFilter(null);
			if (action.equalsIgnoreCase("EDIT")) {
				if (id == null || id.isEmpty()) {
					pollingSponsorDTO.setStatus(0);
					error.setDescription("Cannot Edit Polling Sponsor, Polling Sponsor id not found.");
					pollingSponsorDTO.setError(error);
					return pollingSponsorDTO;
				}
				Integer pollingSponsorId = 0;
				try {
					pollingSponsorId = Integer.parseInt(id);
				} catch (Exception e) {
					e.printStackTrace();
					pollingSponsorDTO.setStatus(0);
					error.setDescription("Cannot Edit Polling Sponsor, Invalid Polling Sponsor id.");
					pollingSponsorDTO.setError(error);
					return pollingSponsorDTO;
				}
				PollingSponsor pollingSponsor = DAORegistry.getPollingSponsorDAO().get(pollingSponsorId);
				if (pollingSponsor == null ) {
					pollingSponsorDTO.setStatus(0);
					error.setDescription("Cannot Edit Polling Sponsor, Polling Sponsor is not found in system.");
					pollingSponsorDTO.setError(error);
					return pollingSponsorDTO;
				}
				pollingSponsorDTO.setStatus(1);
				pollingSponsorDTO.setPollSponsor(pollingSponsor);
				return pollingSponsorDTO;
			} else if (action.equalsIgnoreCase("SAVE") || action.equalsIgnoreCase("UPDATE")) {

				if (title == null || title.isEmpty()) {
					pollingSponsorDTO.setStatus(0);
					error.setDescription("Polling Sponsor Title is mandatory.");
					pollingSponsorDTO.setError(error);
					return pollingSponsorDTO;
				}
				if (description == null || description.isEmpty()) {
					pollingSponsorDTO.setStatus(0);
					error.setDescription("Polling Sponsor Description is mandatory.");
					pollingSponsorDTO.setError(error);
					return pollingSponsorDTO;
				}
			
				PollingSponsor pollingSponsor = null;
				if (action.equalsIgnoreCase("UPDATE")) {
					if (id == null || id.isEmpty()) {
						pollingSponsorDTO.setStatus(0);
						error.setDescription("Cannot update Polling Sponsor, Polling Sponsor id not found.");
						pollingSponsorDTO.setError(error);
						return pollingSponsorDTO;
					}
					Integer pollingSponsorId = 0;
					try {
						pollingSponsorId = Integer.parseInt(id);
					} catch (Exception e) {
						e.printStackTrace();
						pollingSponsorDTO.setStatus(0);
						error.setDescription("Cannot update Polling Sponsor, Invalid Polling Sponsor id.");
						pollingSponsorDTO.setError(error);
						return pollingSponsorDTO;
					}
					
					MultipartFile file = null;
					if(fileRequired !=null && fileRequired.equalsIgnoreCase("Y")){
						MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
						file = multipartRequest.getFile("imageFile");
						if(file==null){
							pollingSponsorDTO.setStatus(0);
							error.setDescription("Sponsor image is mendatory.");
							pollingSponsorDTO.setError(error);
							return pollingSponsorDTO;
						}
					}
					
					pollingSponsor = DAORegistry.getPollingSponsorDAO().get(pollingSponsorId);		
					pollingSponsor.setName(title);
					pollingSponsor.setAddress(address);
					pollingSponsor.setDescription(description);
					pollingSponsor.setEmail(email);
					pollingSponsor.setPhone(phone);
					pollingSponsor.setAltPhone(altPhone);
					pollingSponsor.setUpdatedDate(new Date());;
					pollingSponsor.setUpdatedBy(userName);
					pollingSponsor.setStatus(pcStatus);
					pollingSponsor.setContactPerson(contactPerson);
					DAORegistry.getPollingSponsorDAO().saveOrUpdate(pollingSponsor);
					
					String ext = FilenameUtils.getExtension(file.getOriginalFilename());
					String imagPath = URLUtil.POLLING_SPONSOR_DIRECTORY+"sp_"+pollingSponsor.getId()+"."+ext;
					File newFile = new File(imagPath);
					
					newFile.createNewFile();
					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(newFile));
			        FileCopyUtils.copy(file.getInputStream(), stream);
					stream.close();
					
					AwsS3Response awsRsponse = AWSFileService.upLoadFileToS3(AWSFileService.BUCKET_NAME_RTF_SERVER_MEDIA, "sp_"+pollingSponsor.getId()+"."+ext, AWSFileService.POLLING_SPONSOR_LOGO_FOLDER, newFile );
					if(null != awsRsponse && awsRsponse.getStatus() != 1){
						pollingSponsorDTO.setStatus(0);
						error.setDescription("Sponsor details updated sucessfully, Error occured while uploading image.");
						pollingSponsorDTO.setError(error);
						return pollingSponsorDTO;
					}
					//String logoUrl = AWSFileService.getAWSBaseURL() + AWSFileService.POLLING_SPONSOR_LOGO_FOLDER+"/"+newFile.getName();
					pollingSponsor.setImageUrl(AWSFileService.AWS_BASE_URL+awsRsponse.getFullFilePath());
					DAORegistry.getPollingSponsorDAO().update(pollingSponsor);
			
					pollingSponsorDTO.setMessage("Polling Sponsor saved successfully.");
				}else {
					MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
					MultipartFile file = multipartRequest.getFile("imageFile");
					if(file==null){
						pollingSponsorDTO.setStatus(0);
						error.setDescription("Polling image is mendatory.");
						pollingSponsorDTO.setError(error);
						return pollingSponsorDTO;
					}
					
					pollingSponsor = new PollingSponsor();
		
					pollingSponsor.setName(title);
					pollingSponsor.setAddress(address);
					pollingSponsor.setEmail(email);
					pollingSponsor.setDescription(description);
					pollingSponsor.setPhone(phone);
					pollingSponsor.setAltPhone(altPhone);
					pollingSponsor.setCreatedDate(new Date());
					pollingSponsor.setCreatedBy(userName);
					pollingSponsor.setStatus(pcStatus);
					pollingSponsor.setContactPerson(contactPerson);
					DAORegistry.getPollingSponsorDAO().saveOrUpdate(pollingSponsor);

					
					String ext = FilenameUtils.getExtension(file.getOriginalFilename());
					String imagPath = URLUtil.POLLING_SPONSOR_DIRECTORY+"sp_"+pollingSponsor.getId()+"."+ext;
					File newFile = new File(imagPath);
					
					newFile.createNewFile();
					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(newFile));
			        FileCopyUtils.copy(file.getInputStream(), stream);
					stream.close();
					
					AwsS3Response awsRsponse = AWSFileService.upLoadFileToS3(AWSFileService.BUCKET_NAME_RTF_SERVER_MEDIA, "sp_"+pollingSponsor.getId()+"."+ext, AWSFileService.POLLING_SPONSOR_LOGO_FOLDER, newFile );
					if(null != awsRsponse && awsRsponse.getStatus() != 1){
						pollingSponsorDTO.setStatus(0);
						error.setDescription("Sponsor details saved sucessfully, Error occured while uploading image.");
						pollingSponsorDTO.setError(error);
						return pollingSponsorDTO;
					}
					//String logoUrl = AWSFileService.getAWSBaseURL() + AWSFileService.POLLING_SPONSOR_LOGO_FOLDER+"/"+newFile.getName();
					pollingSponsor.setImageUrl(AWSFileService.AWS_BASE_URL+awsRsponse.getFullFilePath());
					DAORegistry.getPollingSponsorDAO().update(pollingSponsor);
					pollingSponsorDTO.setMessage("Polling Sponsor saved successfully.");
				}
				List<PollingSponsor> pollingSponsors = DAORegistry.getPollingSponsorDAO().getAllPollingSponsor(null, filter,
						pcStatus);
				pollingSponsorDTO.setPollingSponsor(pollingSponsors);
				pollingSponsorDTO.setPagination(PaginationUtil.getDummyPaginationParameters(pollingSponsors.size()));
				pollingSponsorDTO.setStatus(1);
				return pollingSponsorDTO;
			}else if (action.equalsIgnoreCase("DELETE")) {
				if (id == null || id.isEmpty()) {
					pollingSponsorDTO.setStatus(0);
					error.setDescription("Cannot delete Polling Sponsor, Polling Sponsor id not found.");
					pollingSponsorDTO.setError(error);
					return pollingSponsorDTO;
				}
				Integer pollingSponsorId = 0;
				try {
					pollingSponsorId = Integer.parseInt(id);
				} catch (Exception e) {
					e.printStackTrace();
					pollingSponsorDTO.setStatus(0);
					error.setDescription("Cannot delete Polling Sponsor, Invalid Polling Sponsor id.");
					pollingSponsorDTO.setError(error);
					return pollingSponsorDTO;
				}
				PollingSponsor pollingSponsor = DAORegistry.getPollingSponsorDAO().get(pollingSponsorId);
				if (pollingSponsor == null ) {
					pollingSponsorDTO.setStatus(0);
					error.setDescription("Cannot delete Polling Sponsor, No Polling Sponsor is found with given id.");
					pollingSponsorDTO.setError(error);
					return pollingSponsorDTO;
				}
				int count = 0;
				count = DAORegistry.getPollingSponsorDAO().getCategoryCountForSponsor(pollingSponsorId);
				if (count > 0 ) {
					pollingSponsorDTO.setStatus(0);
					error.setDescription("Cannot delete Polling Sponsor. it has Category linked");
					pollingSponsorDTO.setError(error);
					return pollingSponsorDTO;
				}
				pollingSponsor.setUpdatedBy(userName);
				pollingSponsor.setUpdatedDate(new Date());
				pollingSponsor.setStatus("DELETED");

				DAORegistry.getPollingSponsorDAO().update(pollingSponsor);
				pollingSponsorDTO.setMessage("Polling Sponsor deleted successfully.");
				List<PollingSponsor> pollingSponsors = DAORegistry.getPollingSponsorDAO().getAllPollingSponsor(null, filter,
						pcStatus);
				pollingSponsorDTO.setPollingSponsor(pollingSponsors);
				pollingSponsorDTO.setPagination(PaginationUtil.getDummyPaginationParameters(pollingSponsors.size()));
				pollingSponsorDTO.setStatus(1);
				return pollingSponsorDTO;
			}
			pollingSponsorDTO.setStatus(0);
			error.setDescription("Not able to identify action type EDIT/SAVE/UPDATE/DELETE.");
			pollingSponsorDTO.setError(error);
			return pollingSponsorDTO;

	}catch (Exception e) 
		{
		e.printStackTrace();
		pollingSponsorDTO.setStatus(0);
		error.setDescription("Error occured while fetching Polling Sponsors.");
		pollingSponsorDTO.setError(error);
		return pollingSponsorDTO;
	}
	
	}
	
	@RequestMapping(value = "/UpdatePollingCategoryMapper")
	public PollingContestCategoryMapperDTO updatePollingCategoryMapper(HttpServletRequest request, HttpServletResponse response) {
		PollingContestCategoryMapperDTO pollingContestCategoryMapperDTO = new PollingContestCategoryMapperDTO();
		Error error = new Error();
		try {
			String headerFilter = request.getParameter("headerFilter");
			String pcStatus = request.getParameter("pcStatus");
			String categoryIds = request.getParameter("categoryIds");
			String pollId = request.getParameter("pollId");
			String action = request.getParameter("action");
			String sortingString = request.getParameter("sortingString");
			String userName = request.getParameter("userName");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getGiftCardFilter(headerFilter + sortingString);
//			if (pcStatus == null || pcStatus.isEmpty()) {
//				pollingContestCategoryMapperDTO.setStatus(0);
//				error.setDescription("Please send valid status either ACTIVE or INACTIVE.");
//				pollingContestCategoryMapperDTO.setError(error);
//				return pollingContestCategoryMapperDTO;
//			}
			
			if (action.equalsIgnoreCase("ADD") || action.equalsIgnoreCase("ADDNEW")) {
				List <PollingContCategoryMapper> categoryMapperList = new ArrayList<PollingContCategoryMapper>();
				int i = 0;
				String params[] = categoryIds.split(",");
				for(String param : params){
					PollingContCategoryMapper pollingContCategoryMapper = new PollingContCategoryMapper();
					pollingContCategoryMapper.setCategoryId(Integer.parseInt(params[i]));
					pollingContCategoryMapper.setCreatedBy(userName);
					pollingContCategoryMapper.setContestId(Integer.parseInt(pollId));
					pollingContCategoryMapper.setCreatedDate(new Date());
					pollingContCategoryMapper.setStatus("ACTIVE");
					i++;
					categoryMapperList.add(pollingContCategoryMapper);
				}
				DAORegistry.getPollingContestCategoryMapperDAO().saveOrUpdateAll(categoryMapperList);
			}
			if (action.equalsIgnoreCase("DELETE")) {
				
				DAORegistry.getPollingContestCategoryMapperDAO().deletePollingContCategoryMapper(Integer.parseInt(pollId), Integer.parseInt(categoryIds));
			}
			List<PollingContCategoryMapper> pollingContCategoryMapper = DAORegistry.getPollingContestCategoryMapperDAO().getPollingCategoryByContest(pollId,
					pcStatus);
			pollingContestCategoryMapperDTO.setPollingContCategoryMapper(pollingContCategoryMapper);
			int size = 0;
			if (pollingContCategoryMapper != null) {
				size = pollingContCategoryMapper.size();
			}
			pollingContestCategoryMapperDTO.setPagination(PaginationUtil.getDummyPaginationParameters(size));
			pollingContestCategoryMapperDTO.setStatus(1);
			return pollingContestCategoryMapperDTO;
		} catch (Exception e) {
			e.printStackTrace();
			pollingContestCategoryMapperDTO.setStatus(0);
			error.setDescription("Error occured while fetching Polling Categories.");
			pollingContestCategoryMapperDTO.setError(error);
			return pollingContestCategoryMapperDTO;
		}

	}
	
	@RequestMapping(value = "/UpdatePollingContest")
	public PollingContestDTO UpdatePollingContest(HttpServletRequest request, HttpServletResponse response) {
		PollingContestDTO pollingContestDTO = new PollingContestDTO();
		Error error = new Error();
		try {
			String id = request.getParameter("pollingId");
			String title = request.getParameter("title");
			String description = request.getParameter("description");
			String pollingInterval = request.getParameter("pollingInterval");
			String maxQuePerCust = request.getParameter("maxQuePerCust");

			String pcStatus = request.getParameter("pcStatus");
			String action = request.getParameter("action");
			String userName = request.getParameter("userName");
			String startDate = request.getParameter("startDate");
			String endDate = request.getParameter("endDate");

			if (action == null || action.isEmpty()) {
				pollingContestDTO.setStatus(0);
				error.setDescription("Please send valid action to update/save Polling Sponsor.");
				pollingContestDTO.setError(error);
				return pollingContestDTO;
			}
			GridHeaderFilters filter = GridHeaderFiltersUtil.getPollingContestFilter(null);
			if (action.equalsIgnoreCase("EDIT")) {
				if (id == null || id.isEmpty()) {
					pollingContestDTO.setStatus(0);
					error.setDescription("Cannot Edit Polling, Polling id not found.");
					pollingContestDTO.setError(error);
					return pollingContestDTO;
				}
				Integer pollingContestId = 0;
				try {
					pollingContestId = Integer.parseInt(id);
				} catch (Exception e) {
					e.printStackTrace();
					pollingContestDTO.setStatus(0);
					error.setDescription("Cannot Edit Polling, Invalid Polling id.");
					pollingContestDTO.setError(error);
					return pollingContestDTO;
				}
				PollingContest pollingContest = DAORegistry.getPollingContestDAO().get(pollingContestId);
				if (pollingContest == null ) {
					pollingContestDTO.setStatus(0);
					error.setDescription("Cannot Edit Polling, Polling is not found in system.");
					pollingContestDTO.setError(error);
					return pollingContestDTO;
				}
				
				pollingContestDTO.setStatus(1);
				pollingContestDTO.setPollContest(pollingContest);
				return pollingContestDTO;
			} else if (action.equalsIgnoreCase("SAVE") || action.equalsIgnoreCase("UPDATE")) {

				if (title == null || title.isEmpty()) {
					pollingContestDTO.setStatus(0);
					error.setDescription("Polling Title is mandatory.");
					pollingContestDTO.setError(error);
					return pollingContestDTO;
				}
				if (description == null || description.isEmpty()) {
					pollingContestDTO.setStatus(0);
					error.setDescription("Polling Description is mandatory.");
					pollingContestDTO.setError(error);
					return pollingContestDTO;
				}
				startDate = startDate + " 00:00:00";
				endDate = endDate + " 23:59:59";
				
				//DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
				Date stDate = null;
				try {
					stDate = Util.getDateWithTwentyFourHourFormat1(startDate);
				} catch (Exception e) {
					pollingContestDTO.setStatus(0);
					error.setDescription("Please Select Valid Polling Start Date.");
					pollingContestDTO.setError(error);
					return pollingContestDTO;
				}
				Date eDate = null;
				try {
					eDate = Util.getDateWithTwentyFourHourFormat1(endDate);
				} catch (Exception e) {
					pollingContestDTO.setStatus(0);
					error.setDescription("Please Select Valid Polling End Date.");
					pollingContestDTO.setError(error);
					return pollingContestDTO;
				}
				
				
				if(stDate.after(eDate)){
					pollingContestDTO.setStatus(0);
					error.setDescription("Polling Start Date is greater than Polling End date.");
					pollingContestDTO.setError(error);
					return pollingContestDTO;
				
				}
				DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
				PollingContest pollingContest = null;
				if (action.equalsIgnoreCase("UPDATE")) {
					if (id == null || id.isEmpty()) {
						pollingContestDTO.setStatus(0);
						error.setDescription("Cannot update Polling , Polling id not found.");
						pollingContestDTO.setError(error);
						return pollingContestDTO;
					}
					Integer pollingSponsorId = 0;
					try {
						pollingSponsorId = Integer.parseInt(id);
					} catch (Exception e) {
						e.printStackTrace();
						pollingContestDTO.setStatus(0);
						error.setDescription("Cannot update Polling, Invalid Polling id.");
						pollingContestDTO.setError(error);
						return pollingContestDTO;
					}

					pollingContest = DAORegistry.getPollingContestDAO().get(pollingSponsorId);		
					pollingContest.setTitle(title);
					pollingContest.setPollingInterval(Integer.parseInt(pollingInterval));
					pollingContest.setDescription(description);
					pollingContest.setMaxQuePerCust(Integer.parseInt(maxQuePerCust));
					pollingContest.setStartDate(formatter.parse(startDate));
					pollingContest.setEndDate(formatter.parse(endDate));
					pollingContest.setUpdatedDate(new Date());;
					pollingContest.setUpdatedBy(userName);
					pollingContest.setStatus(pcStatus);
					DAORegistry.getPollingContestDAO().saveOrUpdate(pollingContest);
					
					pollingContestDTO.setMessage("Polling saved successfully.");
				}else {
					pollingContest = new PollingContest();
		
					pollingContest.setTitle(title);
					pollingContest.setPollingInterval(Integer.parseInt(pollingInterval));
					pollingContest.setDescription(description);
					pollingContest.setMaxQuePerCust(Integer.parseInt(maxQuePerCust));
					pollingContest.setStartDate(formatter.parse(startDate));
					pollingContest.setEndDate(formatter.parse(endDate));
					pollingContest.setCreatedDate(new Date());
					pollingContest.setCreatedBy(userName);
					pollingContest.setStatus("PASSIVE");
					Integer contestId = DAORegistry.getPollingContestDAO().save(pollingContest);
					PollingRewards pollingRewards = new PollingRewards();
					pollingRewards.setContestId(contestId);
					pollingRewards.setEraserPerQue(0);
					pollingRewards.setStarsPerQue(0);
					pollingRewards.setLifePerQue(0);
					DAORegistry.getPollingRewardsDAO().saveOrUpdate(pollingRewards);

					pollingContestDTO.setMessage("Polling saved successfully.");
				}
				List<PollingContest> pollingContests = DAORegistry.getPollingContestDAO().getAllPollingContest(null, filter,
						pcStatus);
				pollingContestDTO.setPollingContest(pollingContests);
				pollingContestDTO.setPagination(PaginationUtil.getDummyPaginationParameters(pollingContests.size()));
				pollingContestDTO.setStatus(1);
				return pollingContestDTO;
			}else if (action.equalsIgnoreCase("DELETE")) {
				if (id == null || id.isEmpty()) {
					pollingContestDTO.setStatus(0);
					error.setDescription("Cannot delete Polling, Polling id not found.");
					pollingContestDTO.setError(error);
					return pollingContestDTO;
				}
				Integer pollingContestId = 0;
				try {
					pollingContestId = Integer.parseInt(id);
				} catch (Exception e) {
					e.printStackTrace();
					pollingContestDTO.setStatus(0);
					error.setDescription("Cannot delete Polling, Invalid  Polling id.");
					pollingContestDTO.setError(error);
					return pollingContestDTO;
				}
				PollingContest pollingContest = DAORegistry.getPollingContestDAO().get(pollingContestId);
				if (pollingContest == null ) {
					pollingContestDTO.setStatus(0);
					error.setDescription("Cannot delete Polling, No Polling is found with given id.");
					pollingContestDTO.setError(error);
					return pollingContestDTO;
				}
				pollingContest.setUpdatedBy(userName);
				pollingContest.setUpdatedDate(new Date());
				pollingContest.setStatus("DELETED");

				DAORegistry.getPollingContestDAO().update(pollingContest);
				pollingContestDTO.setMessage("Polling deleted successfully.");
				List<PollingContest> pollingContests = DAORegistry.getPollingContestDAO().getAllPollingContest(null, filter,
						pcStatus);
				pollingContestDTO.setPollingContest(pollingContests);
				pollingContestDTO.setPagination(PaginationUtil.getDummyPaginationParameters(pollingContests.size()));
				pollingContestDTO.setStatus(1);
				return pollingContestDTO;
			}else if (action.equalsIgnoreCase("START")||action.equalsIgnoreCase("STOP")) {
				if (id == null || id.isEmpty()) {
					pollingContestDTO.setStatus(0);
					error.setDescription("Cannot change state of Polling, Polling id not found.");
					pollingContestDTO.setError(error);
					return pollingContestDTO;
				}
				Integer pollingContestId = 0;
				try {
					pollingContestId = Integer.parseInt(id);
				} catch (Exception e) {
					e.printStackTrace();
					pollingContestDTO.setStatus(0);
					error.setDescription("Cannot change state of Polling, Invalid  Polling id.");
					pollingContestDTO.setError(error);
					return pollingContestDTO;
				}
				if(action.equalsIgnoreCase("START")) {
					boolean isActivePollingContest = false;
					isActivePollingContest = DAORegistry.getPollingContestDAO().CheckActivePollingContest();
					if (isActivePollingContest) {
						pollingContestDTO.setStatus(0);
						error.setDescription("You have a Active Polling Running Stop it then try again.");
						pollingContestDTO.setError(error);
						return pollingContestDTO;
					}
					
					PollingRewards pollingRewards = DAORegistry.getPollingRewardsDAO().getPollingRewardsForContestId(Integer.parseInt(id));
					if(pollingRewards.getEraserPerQue()+pollingRewards.getLifePerQue()+pollingRewards.getStarsPerQue() == 0)
					{
						pollingContestDTO.setStatus(0);
						error.setDescription("Please Configure Rewards for Polling.");
						pollingContestDTO.setError(error);
						return pollingContestDTO;
					}
					if(maxQuePerCust.isEmpty() ||Integer.parseInt(maxQuePerCust) < 1)
					{
						pollingContestDTO.setStatus(0);
						error.setDescription("Max Questions not set for Polling");
						pollingContestDTO.setError(error);
						return pollingContestDTO;
					}
					
					int count = 0;
					count = DAORegistry.getPollingContestDAO().getCategoryCountForPolling(pollingContestId);
					if(count < 1)
					{
						pollingContestDTO.setStatus(0);
						error.setDescription("No Category (s) Linked to this Polling");
						pollingContestDTO.setError(error);
						return pollingContestDTO;
					}
					int countQuest = 0;
					countQuest = DAORegistry.getPollingContestDAO().getQuestCountForPolling(pollingContestId);
					if(countQuest < Integer.parseInt(maxQuePerCust))
					{
						pollingContestDTO.setStatus(0);
						error.setDescription("Number of Questions in Categories associated with this Polling is less than Max Questions per Customer.");
						pollingContestDTO.setError(error);
						return pollingContestDTO;
					}
				}
				PollingContest pollingContest = DAORegistry.getPollingContestDAO().get(pollingContestId);
				if (pollingContest == null ) {
					pollingContestDTO.setStatus(0);
					error.setDescription("Cannot change state of Polling, No Polling is found with given id.");
					pollingContestDTO.setError(error);
					return pollingContestDTO;
				}
				pollingContest.setUpdatedBy(userName);
				pollingContest.setUpdatedDate(new Date());
				pollingContest.setStatus(action.equalsIgnoreCase("START") ? "ACTIVE" : "PASSIVE");

				DAORegistry.getPollingContestDAO().update(pollingContest);
				pollingContestDTO.setMessage("Polling State Updated successfully.");
				List<PollingContest> pollingContests = DAORegistry.getPollingContestDAO().getAllPollingContest(null, filter,
						pcStatus);
				pollingContestDTO.setPollingContest(pollingContests);
				pollingContestDTO.setPagination(PaginationUtil.getDummyPaginationParameters(pollingContests.size()));
				pollingContestDTO.setStatus(1);
				return pollingContestDTO;
			}
			pollingContestDTO.setStatus(0);
			error.setDescription("Not able to identify action type EDIT/SAVE/UPDATE/DELETE.");
			pollingContestDTO.setError(error);
			return pollingContestDTO;

	}catch (Exception e) 
		{
		e.printStackTrace();
		pollingContestDTO.setStatus(0);
		error.setDescription("Error occured while fetching Polling.");
		pollingContestDTO.setError(error);
		return pollingContestDTO;
	}
	
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/GetCategoriesForPollingContest")
	public PollingContestDTO GetCategoriesForPollingContest(HttpServletRequest request, HttpServletResponse response) {
		PollingContestDTO pollingContestDTO = new PollingContestDTO();
		Error error = new Error();
		try {
			String gcStatus = request.getParameter("gcStatus");
			String contestId = request.getParameter("contestId");
			if (contestId == null || contestId.isEmpty()) {
				pollingContestDTO.setStatus(0);
				error.setDescription("Please send valid Polling Id for Category Selection [ " + contestId + "]");
				pollingContestDTO.setError(error);
				return pollingContestDTO;
			}
			Integer contId = null;
			try {
				contId = Integer.parseInt(contestId);
			} catch (Exception e) {
				pollingContestDTO.setStatus(0);
				error.setDescription("Please send Numeral Polling Id for Category Selection [ " + contId + "]");
				pollingContestDTO.setError(error);
				return pollingContestDTO;
			}
			List pollingContCategoriesList = DAORegistry.getPollingContestCategoryMapperDAO()
					.getPollingCategoriesMappedForContestId(contId);
			pollingContestDTO.setPollingContCategoriesList(pollingContCategoriesList);
			pollingContestDTO.setPagination(PaginationUtil.getDummyPaginationParameters(0));
			pollingContestDTO.setStatus(1);
			return pollingContestDTO;
		} catch (Exception e) {
			e.printStackTrace();
			pollingContestDTO.setStatus(0);
			error.setDescription("Error occured while fetching Polling Categories .");
			pollingContestDTO.setError(error);
			return pollingContestDTO;
		}

	}
	
	@RequestMapping(value = "/UpdatePollingCategoryQuestion")
	public PollingCategoryQuestionDTO updatePollingCategoryQuestion(HttpServletRequest request, HttpServletResponse response){
		PollingCategoryQuestionDTO questionDTO = new PollingCategoryQuestionDTO();
		Error error = new Error();
		try {			
			String action = request.getParameter("action");
			String categoryIdStr = request.getParameter("categoryId");
			String questionIdStr = request.getParameter("questionId");
			String questionText = request.getParameter("questionText");
			String optionA = request.getParameter("optionA");
			String optionB = request.getParameter("optionB");
			String optionC = request.getParameter("optionC"); 
			String answer = request.getParameter("answer");
			String userName = request.getParameter("userName"); 
			
			if(action != null && action.equalsIgnoreCase("EDIT")){
				Integer categoryId = Integer.parseInt(categoryIdStr.trim());
				List<PollingContest> activePollingContesteList = DAORegistry.getPollingContestDAO().getActivePollingContestByCategoryId(categoryId);
				if(null != activePollingContesteList && !activePollingContesteList.isEmpty()) {
					System.err.println("You cannot edit or delete this  because polling is in active status");
					error.setDescription("You cannot edit or delete this  because polling is in active status.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				if(questionIdStr == null || questionIdStr.isEmpty()){
					System.err.println("Please select any Question to Edit.");
					error.setDescription("Please select any Question to Edit.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				
				Integer questionId = null;
				try {
					questionId = Integer.parseInt(questionIdStr);
				} catch (Exception e) {
					System.err.println("Invalid QuestionId found, Please send valid questioId.");
					error.setDescription("Invalid QuestionId found, Please send valid questioId.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				} 
				
				GridHeaderFilters filter = GridHeaderFiltersUtil.getContestQuestionFilter(null); 
				List<PollingCategoryQuestion> questionList = DAORegistry.getPollingCategoryQuestionDAO().getPollingCategoryQuestion(filter, questionId, null,sharedProperty);
				if(questionList.size() == 0){
					System.err.println("Selected question is not found in system.");
					error.setDescription("Selected question is not found in system.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				
				questionDTO.setQuestionList(questionList);
				questionDTO.setStatus(1);
				return questionDTO;
			}else if(action != null && (action.equalsIgnoreCase("SAVE") || action.equalsIgnoreCase("UPDATE"))){
				
				if(questionText==null || questionText.isEmpty()){
					System.err.println("Please provide Question Text.");
					error.setDescription("Please provide Question Text.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				
				if(optionA==null || optionA.isEmpty()){
					System.err.println("Please provide option A text.");
					error.setDescription("Please provide option A text.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				if(optionB==null || optionB.isEmpty()){
					System.err.println("Please provide option B text.");
					error.setDescription("Please provide option B text.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				if(optionC==null || optionC.isEmpty()){
					System.err.println("Please provide option C text.");
					error.setDescription("Please provide option C text.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				 
				if(answer==null || answer.isEmpty()){
					System.err.println("Please provide answer text.");
					error.setDescription("Please provide answer text.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				if(categoryIdStr==null || categoryIdStr.isEmpty()){
					System.err.println("Please provide categoryId.");
					error.setDescription("Please provide categoryId.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				Integer categoryId = null;
				try {
					categoryId = Integer.parseInt(categoryIdStr);
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("Please provide valid categoryId.");
					error.setDescription("Please provide valid categoryId.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				
				PollingCategory pollingCategory =  DAORegistry.getPollingCategoryDAO().get(categoryId);
				
				Date today = new Date();
				PollingCategoryQuestion question = null;
				
				if(action.equalsIgnoreCase("UPDATE")){
					if(questionIdStr == null || questionIdStr.isEmpty()){
						System.err.println("Please select any Question to Update.");
						error.setDescription("Please select any Question to Update.");
						questionDTO.setError(error);
						questionDTO.setStatus(0);
						return questionDTO;
					}
					question = DAORegistry.getPollingCategoryQuestionDAO().get(Integer.parseInt(questionIdStr));
					if(question == null){
						System.err.println("Question is not found.");
						error.setDescription("Question is not found.");
						questionDTO.setError(error);
						questionDTO.setStatus(0);
						return questionDTO;
					}
				}
				if(question == null){
					question = new PollingCategoryQuestion();
				}
				if(action.equalsIgnoreCase("SAVE")){
					question.setCreatedBy(userName);
					question.setCreatedDate(today);
				}
				if(action.equalsIgnoreCase("UPDATE")){
					question.setUpdatedBy(userName);
					question.setUpdatedDate(today);
				}
				question.setStatus("ACTIVE");
				if(pollingCategory.getPollingType().equals("FEEDBACK")) {
					question.setAnswer("");
				}else {
					question.setAnswer(answer);
				}
				
				question.setCategoryId(categoryId);
				question.setOptionA(optionA);
				question.setOptionB(optionB);
				question.setOptionC(optionC); 
				question.setQuestion(questionText); 
				
				DAORegistry.getPollingCategoryQuestionDAO().saveOrUpdate(question);
				
				List<PollingCategoryQuestion> questionList = null;
				
				GridHeaderFilters filter = GridHeaderFiltersUtil.getContestsFilter(null);
				
				questionList = DAORegistry.getPollingCategoryQuestionDAO().getPollingCategoryQuestion(filter, null, categoryId,sharedProperty);
				
				if(action.equalsIgnoreCase("SAVE")){
					questionDTO.setMessage("Question Created successfully.");
				}
				if(action.equalsIgnoreCase("UPDATE")){
					questionDTO.setMessage("Question Updated successfully.");
				}
				questionDTO.setStatus(1);			
				questionDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(questionList!=null?questionList.size():0));				
				questionDTO.setQuestionList(questionList);
				return questionDTO;
			}else if(action != null && action.equalsIgnoreCase("DELETE")){
				Integer categoryId = Integer.parseInt(categoryIdStr.trim());
				List<PollingContest> activePollingContesteList = DAORegistry.getPollingContestDAO().getActivePollingContestByCategoryId(categoryId);
				if(null != activePollingContesteList && !activePollingContesteList.isEmpty()) {
					System.err.println("You cannot edit or delete this because polling is in active status.");
					error.setDescription("You cannot edit or delete this because polling is in active status.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				PollingCategoryQuestion question =  null;
				if(questionIdStr == null || questionIdStr.isEmpty()){
					System.err.println("Please select any Question to Delete.");
					error.setDescription("Please select any Question to Delete.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				if(categoryIdStr == null || categoryIdStr.isEmpty()){
					System.err.println("CategoryId is not found.");
					error.setDescription("CategoryId is not found.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				question = DAORegistry.getPollingCategoryQuestionDAO().get(Integer.parseInt(questionIdStr));
				if(question == null){
					System.err.println("Question is not found.");
					error.setDescription("Question is not found.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				DAORegistry.getPollingCategoryQuestionDAO().delete(question);
				
				GridHeaderFilters filter = GridHeaderFiltersUtil.getContestsFilter(null); 
				List<PollingCategoryQuestion> list = DAORegistry.getPollingCategoryQuestionDAO().getPollingCategoryQuestion(filter, null, Integer.parseInt(categoryIdStr),sharedProperty);
				questionDTO.setMessage("Question Deleted successfully.");
				questionDTO.setStatus(1);			
				questionDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(list!=null?list.size():0));				
				questionDTO.setQuestionList(list);
				return questionDTO;	
			}else if(action != null && action.equalsIgnoreCase("ADDNEW")){
				
				/*String questionBankIdsStr = request.getParameter("questionBankIds");
				
				Integer contestId = null;
				try {
					contestId = Integer.parseInt(contestIdStr);
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("Please provide valid contestId.");
					error.setDescription("Please provide valid contestId.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				
				if(questionBankIdsStr == null && questionBankIdsStr.isEmpty()){
					System.err.println("Please select valid Question(s).");
					error.setDescription("Please select valid Question(s).");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				
				int usedQuestionCount = QuizDAORegistry.getQuestionBankDAO().getUsedQuestionCount(questionBankIdsStr);
				
				if(usedQuestionCount  > 0 ){
					System.err.println("opps it's looks like somebody already used some of the selected questions , Please refresh grid and try again ");
					error.setDescription("opps it's looks like somebody already used some of the selected questions , Please refresh grid and try again ");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}	
				Contests contest = QuizDAORegistry.getContestsDAO().get(contestId);
				Integer contestQuestionCount = QuizDAORegistry.getContestQuestionsDAO().getQuestionCount(contestId);
				
				Date today = new Date();
				List<ContestQuestions> contestQuestionList = new ArrayList<ContestQuestions>();
				ContestQuestions contestQuestion = null;
				List<QuestionBank> questionBankList = new ArrayList<QuestionBank>();
				QuestionBank questionBank = null;
				
				if(questionBankIdsStr != null && !questionBankIdsStr.isEmpty()){
					String arr[] = questionBankIdsStr.split(",");
					Integer noOfQuestionsCount = arr.length + contestQuestionCount;
					
					if(contest.getQuestionSize() < noOfQuestionsCount){
						System.err.println("Contest has declared question size "+contest.getQuestionSize()+" you can add only "+contest.getQuestionSize()+" questions.");
						error.setDescription("Contest has declared question size "+contest.getQuestionSize()+" you can add only "+contest.getQuestionSize()+" questions.");
						questionDTO.setError(error);
						questionDTO.setStatus(0);
						return questionDTO;
					}
					if(arr.length > 0){
						for(String questionBankId : arr){
							questionBank = QuizDAORegistry.getQuestionBankDAO().get(Integer.parseInt(questionBankId));
							
							contestQuestion = QuizDAORegistry.getContestQuestionsDAO().getContestQuestionByQuestion(contestId, questionBank.getQuestion());
							if(contestQuestion == null){
								contestQuestion = new ContestQuestions();
								contestQuestion.setQuestion(questionBank.getQuestion());
								contestQuestion.setOptionA(questionBank.getOptionA());
								contestQuestion.setOptionB(questionBank.getOptionB());
								contestQuestion.setOptionC(questionBank.getOptionC());
								contestQuestion.setDifficultyLevel(questionBank.getDificultyLevel());
								contestQuestion.setAnswer(questionBank.getAnswer());
								contestQuestion.setQuestionReward(questionBank.getQuestionReward());
								contestQuestion.setContestId(contestId);
								contestQuestion.setCreatedBy(userName);
								contestQuestion.setCreatedDate(today);
								
								contestQuestionList.add(contestQuestion);
								
								//Question Bank Status - Update
								questionBank.setStatus("USED");
								questionBank.setUpdatedBy(userName);
								questionBank.setUpdatedDate(today);
								questionBankList.add(questionBank);
							}
						}
						
						QuizDAORegistry.getContestQuestionsDAO().saveAll(contestQuestionList);
						QuizDAORegistry.getQuestionBankDAO().updateAll(questionBankList);
						
						questionDTO.setMessage("Question(s) Created Successfully."); 
					}
				}
				
				GridHeaderFilters filter = GridHeaderFiltersUtil.getContestsFilter(null); 
				List<ContestQuestions> newContestQuestionList = QuizDAORegistry.getContestQuestionsDAO().getContestQuestions(filter, null, contestId,sharedProperty);
				if(!newContestQuestionList.isEmpty()){
					int i=1;
					for(ContestQuestions q : newContestQuestionList){
						q.setSerialNo(i);
						i++;
					}
					QuizDAORegistry.getContestQuestionsDAO().updateAll(newContestQuestionList);
				}
				questionDTO.setStatus(1);			
				questionDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(newContestQuestionList!=null?newContestQuestionList.size():0));				
				questionDTO.setQuestionList(newContestQuestionList);
				return questionDTO;*/
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			questionDTO.setStatus(0);			
			System.err.println("Error occured while updating contest question.");
			error.setDescription("Error occured while updating contest question.");
			return questionDTO;
		}
		return questionDTO;
	}
	
	
	@RequestMapping(value = "/PollingCategoryQuestion")
	public PollingCategoryQuestionDTO getPollingCategoryQuestions(HttpServletRequest request, HttpServletResponse response){
		PollingCategoryQuestionDTO questionDTO = new PollingCategoryQuestionDTO();
		Error error = new Error();
		try{			
			String headerFilter = request.getParameter("headerFilter");
			String categoryIdStr = request.getParameter("categoryId");
				
			if(categoryIdStr == null || categoryIdStr.trim().isEmpty()){
				questionDTO.setStatus(0);
				questionDTO.setMessage("Not able to identfy polling category, pelase send valid category id to load questions.");
				return questionDTO;
			}
			
			Integer categoryId = null;
			try {
				categoryId = Integer.parseInt(categoryIdStr);
			} catch (Exception e) {
				questionDTO.setStatus(0);
				questionDTO.setMessage("Invalid categoryId, Please send valid CategoryId.");
				return questionDTO;
			}
			
			Integer count = 0;
			System.out.println("CT- GETPOLLINGCATEGORYQUESTIONS FILTERS: Begins -> "+new Date());
			GridHeaderFilters filter = GridHeaderFiltersUtil.getPollingCategoryQuestionFilter(headerFilter);
			System.out.println("CT- GETPOLLINGCATEGORYQUESTIONS FILTERS: Ends -> "+new Date());
			
			System.out.println("CT- GETPOLLINGCATEGORYQUESTIONS SQL: Begins -> "+new Date());
			List<PollingCategoryQuestion> questionList =  DAORegistry.getPollingCategoryQuestionDAO().getPollingCategoryQuestion(filter, null,categoryId,sharedProperty);
			System.out.println("CT- GETPOLLINGCATEGORYQUESTIONS SQL: Ends -> "+new Date());
			
			if(questionList != null && questionList.size() > 0){
				count = questionList.size();
			}
			if(questionList == null || questionList.size() <= 0){
				questionDTO.setMessage("No Questions found for selected polling category.");
			}
			
			questionDTO.setStatus(1);			
			questionDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(count));
			questionDTO.setQuestionList(questionList);
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while loading polling category questions.");
			questionDTO.setError(error);
			questionDTO.setStatus(0);
		}		
		return questionDTO;
	}
	
	
	@RequestMapping("/AutoCompletePollingSponsor")
	public AutoCompleteArtistAndCategoryDTO getAutoCompletePollingSponsor(HttpServletRequest request, HttpServletResponse response) throws Exception {
		AutoCompleteArtistAndCategoryDTO autoCompleteArtistAndCategoryDTO = new AutoCompleteArtistAndCategoryDTO();
		Error error = new Error();
		
		try{
			String param = request.getParameter("q");
			String strResponse = "";
			
			Collection<SponsorAutoComplete> list = DAORegistry.getPollingSponsorDAO().filterByName(param);	
			
			if (list != null) {
				for (SponsorAutoComplete obj : list) {
					strResponse += ("SPONSOR" + "|" + obj.getSponsorId() + "|" + obj.getName() + "\n") ;
				}
			}
			autoCompleteArtistAndCategoryDTO.setStatus(1);
			autoCompleteArtistAndCategoryDTO.setArtistAndCategory(strResponse);
		}catch(Exception e){
			e.printStackTrace();
			System.err.println(e.getLocalizedMessage());
			error.setDescription("Something went wrong while fetching Auto Complete Sponsors.");
			autoCompleteArtistAndCategoryDTO.setError(error);
			autoCompleteArtistAndCategoryDTO.setStatus(0);
		}
		return autoCompleteArtistAndCategoryDTO;
	}
	
	@RequestMapping(value="/UpdatePollingCategory")
	public PollingCategoryDTO updatePollingCategory(HttpServletRequest request, HttpServletResponse response){
		PollingCategoryDTO dto = new PollingCategoryDTO();
		Error error = new Error();
		try {
			 
			String status = request.getParameter("status");
			String categoryIdStr = request.getParameter("categoryId");
			String title = request.getParameter("title");
			String pollingType = request.getParameter("pollingType");
			String sponsorStr = request.getParameter("sponsor"); 
			String action = request.getParameter("action");
			String userName = request.getParameter("userName");
			
			if(action == null || action.isEmpty()){
				dto.setStatus(0);
				error.setDescription("Please send valid action to update/save polling category.");
				dto.setError(error);
				return dto;
			}
			GridHeaderFilters filter = GridHeaderFiltersUtil.getPollingCategoryFilter(null);
			if(action.equalsIgnoreCase("EDIT")){
				if(categoryIdStr==null || categoryIdStr.isEmpty()){
					dto.setStatus(0);
					error.setDescription("Cannot Edit Polling Category, Polling Category id not found.");
					dto.setError(error);
					return dto;
				}
				Integer categoryId = 0;
				try {
					categoryId = Integer.parseInt(categoryIdStr);
				} catch (Exception e) {
					e.printStackTrace();
					dto.setStatus(0);
					error.setDescription("Cannot Edit Polling Category, Invalid Polling Category Id.");
					dto.setError(error);
					return dto;
				}
				List<PollingContest> activePollingContesteList = DAORegistry.getPollingContestDAO().getActivePollingContestByCategoryId(categoryId);
				if(null != activePollingContesteList && !activePollingContesteList.isEmpty()) {
					System.err.println("You cannot edit or delete this because polling is in active status.");
					error.setDescription("You cannot edit or delete this because polling is in active status.");
					dto.setError(error);
					dto.setStatus(0);
					return dto;
				}
				PollingCategory pollingCategory = DAORegistry.getPollingCategoryDAO().get(categoryId);
				//List<PollingCategoryQuestion> questionList = DAORegistry.getPollingCategoryQuestionDAO().getPollingCategoryQuestion(filter, null, categoryId, sharedProperty);
				if(pollingCategory == null /*|| questionList.isEmpty()*/){
					dto.setStatus(0);
					error.setDescription("Cannot Edit Polling Category, Polling Category is not found in system.");
					dto.setError(error);
					return dto;
				}
				
				if(null != pollingCategory.getSponsorId()) {
					PollingSponsor pollingSponsor = DAORegistry.getPollingSponsorDAO().get(pollingCategory.getSponsorId());
					pollingCategory.setSponsorName(pollingSponsor.getName());
				}
				dto.setPollingCategoryObj(pollingCategory);
				dto.setStatus(1);
				return dto;
			}else if(action.equalsIgnoreCase("SAVE") || action.equalsIgnoreCase("UPDATE")){
				 
				if(title==null || title.isEmpty()){
					dto.setStatus(0);
					error.setDescription("Polling Category Title is mandatory.");
					dto.setError(error);
					return dto;
				}
				if(pollingType==null || pollingType.isEmpty()){
					dto.setStatus(0);
					error.setDescription("Polling Type is mandatory.");
					dto.setError(error);
					return dto;
				}
				if(status==null || status.isEmpty()){
					dto.setStatus(0);
					error.setDescription("Polling Category Status is mandatory.");
					dto.setError(error);
					return dto;
				}
				Integer sponsorId = null;
				if(pollingType.equals("SPONSOR")) {
					if(sponsorStr==null || sponsorStr.isEmpty()){
						dto.setStatus(0);
						error.setDescription("Polling Sponsor is mandatory.");
						dto.setError(error);
						return dto;
					}
					try {
						sponsorId = Integer.parseInt(sponsorStr);
					}catch(Exception e) {
						e.printStackTrace();
						dto.setStatus(0);
						error.setDescription("Polling Sponsor is mandatory.");
						dto.setError(error);
						return dto;
					}
				}
				
				PollingCategory pollingCategory = null;
				if(action.equalsIgnoreCase("UPDATE")){
					if(categoryIdStr ==null || categoryIdStr.isEmpty()){
						dto.setStatus(0);
						error.setDescription("Cannot update Polling Category, Polling Category id not found.");
						dto.setError(error);
						return dto;
					}
					Integer categoryId = 0;
					try {
						categoryId = Integer.parseInt(categoryIdStr);
					} catch (Exception e) {
						e.printStackTrace();
						dto.setStatus(0);
						error.setDescription("Cannot update Polling Category, Invalid Polling Category id.");
						dto.setError(error);
						return dto;
					}
					
					pollingCategory = DAORegistry.getPollingCategoryDAO().get(categoryId);
					pollingCategory.setPollingType(pollingType);
					pollingCategory.setSponsorId(sponsorId);
					pollingCategory.setStatus(status);
					pollingCategory.setTitle(title);
					pollingCategory.setUpdatedDate(new Date());
					pollingCategory.setUpdatedBy(userName);
					
					DAORegistry.getPollingCategoryDAO().saveOrUpdate(pollingCategory);
					dto.setMessage("Polling Category updated successfully.");
				}else{
					pollingCategory = new PollingCategory();
					pollingCategory.setPollingType(pollingType);
					pollingCategory.setSponsorId(sponsorId);
					pollingCategory.setStatus(status);
					pollingCategory.setTitle(title);
					pollingCategory.setCreatedDate(new Date());
					pollingCategory.setCreatedBy(userName);
					
					DAORegistry.getPollingCategoryDAO().saveOrUpdate(pollingCategory);
					 
					dto.setMessage("Polling Category saved successfully.");
				}
				List<PollingCategory> pollingCategoryList = DAORegistry.getPollingCategoryDAO().getAllPollingCategory(null,  filter, status);
				dto.setPollingCategory(pollingCategoryList);
				dto.setPagination(PaginationUtil.getDummyPaginationParameters(pollingCategoryList.size()));
				dto.setStatus(1);
				return dto;
			}else if(action.equalsIgnoreCase("DELETE")){
				
				if(categoryIdStr ==null || categoryIdStr.isEmpty()){
					dto.setStatus(0);
					error.setDescription("Cannot delete Polling Category, Polling Category id not found.");
					dto.setError(error);
					return dto;
				}
				Integer categoryId = 0;
				try {
					categoryId = Integer.parseInt(categoryIdStr);
				} catch (Exception e) {
					e.printStackTrace();
					dto.setStatus(0);
					error.setDescription("Cannot delete Polling Category, Invalid Polling Category id.");
					dto.setError(error);
					return dto;
				}
				List<PollingContest> activePollingContesteList = DAORegistry.getPollingContestDAO().getActivePollingContestByCategoryId(categoryId);
				if(null != activePollingContesteList && !activePollingContesteList.isEmpty()) {
					System.err.println("You cannot edit or delete this because polling is in active status.");
					error.setDescription("You cannot edit or delete this because polling is in active status.");
					dto.setError(error);
					dto.setStatus(0);
					return dto;
				}
				PollingCategory pollingCategory = DAORegistry.getPollingCategoryDAO().get(categoryId);
				  
				if(pollingCategory ==null){
					dto.setStatus(0);
					error.setDescription("Cannot delete Polling Category, No Polling Category is found with given id.");
					dto.setError(error);
					return dto;
				}
				
				pollingCategory.setUpdatedDate(new Date());
				pollingCategory.setUpdatedBy(userName);
				pollingCategory.setStatus("DELETED");
				DAORegistry.getPollingCategoryDAO().update(pollingCategory);
				dto.setMessage("Polling Category deleted successfully.");
				List<PollingCategory> pollingCategoryList = DAORegistry.getPollingCategoryDAO().getAllPollingCategory(null,  filter, "ACTIVE");
				dto.setPollingCategory(pollingCategoryList);
				dto.setPagination(PaginationUtil.getDummyPaginationParameters(pollingCategoryList.size()));
				dto.setStatus(1);
				return dto;
			}
			dto.setStatus(0);
			error.setDescription("Not able to identify action type EDIT/SAVE/UPDATE/DELETE.");
			dto.setError(error);
			return dto;
		} catch (Exception e) {
			e.printStackTrace();
			dto.setStatus(0);
			error.setDescription("Error occured while fetching polling category.");
			dto.setError(error);
			return dto;
		}
	
	}
	
	@RequestMapping(value = "/GetAllPollingVideos")
	public PollingVideoInventoryDTO getAllPollingVideos(HttpServletRequest request, HttpServletResponse response){
		PollingVideoInventoryDTO responseDTO = new PollingVideoInventoryDTO();
		Error error = new Error();
		try{
			//String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String sortingString = request.getParameter("sortingString");
			String status  = request.getParameter("videoStatus");
			GridHeaderFilters filter  = GridHeaderFiltersUtil.getPollingVideoFilter(headerFilter+ sortingString);
			List<PollingVideoInventory> list = DAORegistry.getPollingVideoInventoryDAO().getAllPollingVideoINventories(filter, status);
			 
			List<PollingVideoCategory> categoryList = DAORegistry.getPollingVideoCategoryDAO().getAllPollingVideoCategory(filter, null);
			List<PollingSponsor> sponsors = DAORegistry.getPollingSponsorDAO().getAllPollingSponsor(null, new GridHeaderFilters() , "ACTIVE");
			responseDTO.setCategoryList(categoryList);
			responseDTO.setSponsors(sponsors);
			responseDTO.setStatus(1);
			responseDTO.setVideoList(list);
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(list!=null?list.size():0));
			return responseDTO;	
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while gertting polling video inventory.");
			responseDTO.setError(error);
			responseDTO.setStatus(0);
		}
		return responseDTO;
	}
	
	
	
	
	@RequestMapping(value = "/UpdatePollingVideo")
	public PollingVideoInventoryDTO UpdatePollingVideo(HttpServletRequest request, HttpServletResponse response){
		PollingVideoInventoryDTO responseDTO = new PollingVideoInventoryDTO();
		Error error = new Error();
		try{
			String action = request.getParameter("action");
			String videoIdStr = request.getParameter("videoId");
			String awsUrl = AWSFileService.getAWSBaseURL();
			String videoFile = request.getParameter("videoFile"); 
			String status =  request.getParameter("videoStatus");
			String sponsor = request.getParameter("sponsor");
			String imageFile = request.getParameter("imageFile");
			String bucketName = request.getParameter("bucketName");
			
			if(action==null || action.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please send valid action.");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(action.equalsIgnoreCase("EDIT")){
				if(videoIdStr==null || videoIdStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Polling video inventory id not found.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer videoId = null;
				try {
					videoId = Integer.parseInt(videoIdStr);
				} catch (Exception e) {
					e.printStackTrace();
					responseDTO.setStatus(0);
					error.setDescription("Invalid polling video inventory id.");
					responseDTO.setError(error);
					return responseDTO;
				}
				PollingVideoInventory videoInventory = DAORegistry.getPollingVideoInventoryDAO().get(videoId);
				if(videoInventory ==  null){
					responseDTO.setStatus(0);
					error.setDescription("Selected polling video inventory not found in system.");
					responseDTO.setError(error);
					return responseDTO;
				}
				
				responseDTO.setStatus(1);
				responseDTO.setVideo(videoInventory);
				return responseDTO;
				
				
			}else if(action.equalsIgnoreCase("UPDATE") || action.equalsIgnoreCase("SAVE")){
				
				String title = request.getParameter("title");
				String description = request.getParameter("description");
				String category = request.getParameter("category");
				String userName = request.getParameter("userName");
				
				if(action.equalsIgnoreCase("SAVE") && (videoFile==null || videoFile.isEmpty())){
					responseDTO.setStatus(0);
					error.setDescription("Video file is mandatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(title==null || title.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Video title is mandatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(description==null || description.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Video description is mandatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(category==null || category.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Video category is mandatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(sponsor == null || sponsor.isEmpty()){
					sponsor = "0";
				}
				Integer sponsorId = null;
				try {
					sponsorId = Integer.parseInt(sponsor);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				PollingVideoInventory videoInventory = null;
				String message = "";
				if(action.equalsIgnoreCase("UPDATE")){
					if(videoIdStr==null || videoIdStr.isEmpty()){
						responseDTO.setStatus(0);
						error.setDescription("Polling video inventory id not found.");
						responseDTO.setError(error);
						return responseDTO;
					}
					Integer videoId = null;
					try {
						videoId = Integer.parseInt(videoIdStr);
					} catch (Exception e) {
						e.printStackTrace();
						responseDTO.setStatus(0);
						error.setDescription("Invalid polling video inventory id.");
						responseDTO.setError(error);
						return responseDTO;
					}
					videoInventory = DAORegistry.getPollingVideoInventoryDAO().get(videoId);
					if(videoInventory ==  null){
						responseDTO.setStatus(0);
						error.setDescription("Selected polling video inventory not found in system.");
						responseDTO.setError(error);
						return responseDTO;
					}
					message = "Selected Polling video Inventory updateesuccessfully.";
				}else if(action.equalsIgnoreCase("SAVE")){
					videoInventory = new PollingVideoInventory();
					videoInventory.setStatus(true);
					videoInventory.setCreatedDate(new Date());
					videoInventory.setCreatedBy(userName);
					videoInventory.setVideoUrl(awsUrl);
					videoInventory.setIsDeleted(0);
					videoInventory.setIsDefault(false);
					message = "Polling video Inventory saved successfully.";
				}
				MultipartFile file = null;
				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
				file = multipartRequest.getFile("videoFile");

				String fileName = file.getOriginalFilename();
				fileName = fileName.replaceAll("[\\\\/:*?\"<>|]", "");
				fileName = fileName.replaceAll(" ", "");
				
				String videoPath = URLUtil.RTFMEDIA_DIRECTORY+fileName;
				File newFile = new File(videoPath);
				
				newFile.createNewFile();
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(newFile));
		        FileCopyUtils.copy(file.getInputStream(), stream);
				stream.close();
				AwsS3Response awsRsponse = AWSFileService.upLoadFileToS3(AWSFileService.getRTFTVMediaBucketName(), fileName, category, newFile );
				if(null != awsRsponse && awsRsponse.getStatus() != 1){
					responseDTO.setStatus(0);
					error.setDescription("Selected video could not be uploaded .");
					responseDTO.setError(error);
					return responseDTO;
				}
				
			    videoInventory.setVideoUrl(awsUrl + category +"/"+fileName);
				videoInventory.setCategoryId(Integer.parseInt(category));
				videoInventory.setTitle(title);
				videoInventory.setDescription(description);
				videoInventory.setSponsorId(sponsorId);
				DAORegistry.getPollingVideoInventoryDAO().saveOrUpdate(videoInventory);
				
				String thumbnailImagePath = null;
				String thumbnailFilename = null;
				File imgFile = null;
				if(imageFile!=null && !imageFile.isEmpty()){
					file = multipartRequest.getFile("imageFile");
					thumbnailFilename = file.getOriginalFilename();
					thumbnailFilename = thumbnailFilename.replaceAll("[\\\\/:*?\"<>|]", "");
					thumbnailFilename = thumbnailFilename.replaceAll(" ", "");
					String ext = FilenameUtils.getExtension(file.getOriginalFilename());
					
					thumbnailFilename = videoInventory.getId()+"."+ext;
					thumbnailImagePath = URLUtil.RTFMEDIA_THUMBNAIL+thumbnailFilename;
					
					imgFile = new File(thumbnailImagePath);
					imgFile.createNewFile();
					BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(imgFile));
			        FileCopyUtils.copy(file.getInputStream(), stream1);
					stream1.close();
					
					AwsS3Response awsRsp = AWSFileService.upLoadFileToS3(AWSFileService.getRTFTVMediaBucketName(), thumbnailFilename, AWSFileService.POLLING_VIDEO_THUMBNAIL_FOLDER, imgFile );
					if(null != awsRsp && awsRsp.getStatus() != 1){
						responseDTO.setStatus(0);
						error.setDescription("Selected video could not be uploaded, while uploading thumbnail image error occured.");
						responseDTO.setError(error);
						return responseDTO;
					}
					videoInventory.setPosterUrl(awsUrl+AWSFileService.POLLING_VIDEO_THUMBNAIL_FOLDER+"/"+thumbnailFilename);
					DAORegistry.getPollingVideoInventoryDAO().update(videoInventory);
				}else{
					thumbnailImagePath = FileUploadUtil.generateThumbnailImage(newFile,String.valueOf(videoInventory.getId()));
					thumbnailFilename = videoInventory.getId()+".jpg";
					imgFile = new File(thumbnailImagePath);
				
					if(thumbnailImagePath != null && !thumbnailImagePath.isEmpty()){
						AwsS3Response awsRsp = AWSFileService.upLoadFileToS3(AWSFileService.getRTFTVMediaBucketName(), thumbnailFilename, AWSFileService.POLLING_VIDEO_THUMBNAIL_FOLDER, imgFile );
						if(null != awsRsp && awsRsp.getStatus() != 1){
							responseDTO.setStatus(0);
							error.setDescription("Selected video could not be uploaded, while uploading thumbnail image error occured.");
							responseDTO.setError(error);
							return responseDTO;
						}
					}
					
					videoInventory.setPosterUrl(awsUrl+AWSFileService.POLLING_VIDEO_THUMBNAIL_FOLDER+"/"+thumbnailFilename);
					DAORegistry.getPollingVideoInventoryDAO().update(videoInventory);
				}
				GridHeaderFilters filter  = GridHeaderFiltersUtil.getPollingVideoFilter(null);
				List<PollingVideoInventory> list = DAORegistry.getPollingVideoInventoryDAO().getAllPollingVideoINventories(filter, status);
				
				responseDTO.setStatus(1);
				responseDTO.setMessage(message);
				responseDTO.setVideoList(list);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(list!=null?list.size():0));
				return responseDTO;
			}else if(action.equalsIgnoreCase("PASSIVE")|| action.equalsIgnoreCase("ACTIVE")){
				String message = "";
				if(videoIdStr==null || videoIdStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Polling video inventory id not found.");
					responseDTO.setError(error);
					return responseDTO;
				}
				String[] videoIdArr = null;
				if(videoIdStr!=null){
					videoIdArr = videoIdStr.split(",");
				}
								
				for (String idStr : videoIdArr) {
					Integer id= Integer.parseInt(idStr);
					PollingVideoInventory object = DAORegistry.getPollingVideoInventoryDAO().get(id);
					
					if(action.equalsIgnoreCase("PASSIVE")){
						object.setStatus(false);
					}else
					{
						object.setStatus(true);
					}
					DAORegistry.getPollingVideoInventoryDAO().update(object);
				}
				GridHeaderFilters filter  = GridHeaderFiltersUtil.getPollingVideoFilter(null);
				List<PollingVideoInventory> list = DAORegistry.getPollingVideoInventoryDAO().getAllPollingVideoINventories(filter, status);
				
				message = "Polling video Inventory updated successfully.";
				responseDTO.setStatus(1);
				responseDTO.setMessage(message);
				responseDTO.setVideoList(list);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(list!=null?list.size():0));
				return responseDTO;
				
			}else if(action.equalsIgnoreCase("DEFAULT")||action.equalsIgnoreCase("NORMAL")){
				String message = "";
				if(videoIdStr==null || videoIdStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Polling video inventory id not found.");
					responseDTO.setError(error);
					return responseDTO;
				}
				String[] videoIdArr = null;
				if(videoIdStr!=null){
					videoIdArr = videoIdStr.split(",");
				}
								
				for (String idStr : videoIdArr) {
					Integer id= Integer.parseInt(idStr);
					PollingVideoInventory object = DAORegistry.getPollingVideoInventoryDAO().get(id);
					if(object.getStatus() && action.equalsIgnoreCase("DEFAULT")){
						object.setIsDefault(true);
						message = "Polling video Inventory updated successfully.";
					}else if(!object.getStatus() && action.equalsIgnoreCase("DEFAULT")){
						message = "Some of the records not updated.";
					}else if(action.equalsIgnoreCase("NORMAL")){
						object.setIsDefault(false);
						message = "Polling video Inventory updated successfully.";
					}
					DAORegistry.getPollingVideoInventoryDAO().update(object);
				}
				GridHeaderFilters filter  = GridHeaderFiltersUtil.getPollingVideoFilter(null);
				List<PollingVideoInventory> list = DAORegistry.getPollingVideoInventoryDAO().getAllPollingVideoINventories(filter, status);
				
				
				responseDTO.setStatus(1);
				responseDTO.setMessage(message);
				responseDTO.setVideoList(list);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(list!=null?list.size():0));
				return responseDTO;
				
			}else if(action.equalsIgnoreCase("DELETE")){
				
				if(videoIdStr==null || videoIdStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Polling video inventory id not found.");
					responseDTO.setError(error);
					return responseDTO;
				}
				
				String[] videoIdArr = null;
				if(videoIdStr!=null){
					videoIdArr = videoIdStr.split(",");
				}
								
				/*for (String idStr : videoIdArr) {
					Integer id= Integer.parseInt(idStr);
					PollingVideoInventory object = DAORegistry.getPollingVideoInventoryDAO().get(id);
					String file = com.rtw.tmat.utils.Util.getFileNameFromPath(object.getVideoUrl());
					AWSFileService.deleteFilesFromS3(AWSFileService.getRTFTVMediaBucketName(),file,object.getCategory());
					if(object.getPosterUrl() != null && !object.getPosterUrl().isEmpty()){
						String imgFile = com.rtw.tmat.utils.Util.getFileNameFromPath(object.getPosterUrl());
						AWSFileService.deleteFilesFromS3(AWSFileService.getRTFTVMediaBucketName(),imgFile,AWSFileService.POLLING_VIDEO_THUMBNAIL_FOLDER);
					}
					
//					DAORegistry.getPollingVideoInventoryDAO().deleteById(object.getId());
				}*/
				DAORegistry.getPollingVideoInventoryDAO().deleteRtfVideos(videoIdStr);
				
				GridHeaderFilters filter  = GridHeaderFiltersUtil.getPollingVideoFilter(null);
				List<PollingVideoInventory> list = DAORegistry.getPollingVideoInventoryDAO().getAllPollingVideoINventories(filter, status);
				
				responseDTO.setStatus(1);
				responseDTO.setMessage("Selected Polling video inventory deleted successfully.");
				responseDTO.setVideoList(list);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(list!=null?list.size():0));
				return responseDTO;
			}
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while Adding/Updating Polling video inventory.");
			responseDTO.setError(error);
			responseDTO.setStatus(0);
			return responseDTO;
		}
		return responseDTO;
		
	}
	
	@RequestMapping(value="/GetPollingCustomerVideos")
	public PollingCustomerVideoDTO getPollingCustomerVideos(HttpServletRequest request, HttpServletResponse response){
		PollingCustomerVideoDTO dto = new PollingCustomerVideoDTO();
		Error error = new Error();
		try {
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String name = request.getParameter("name");
			String videoType = request.getParameter("videoType");
			String playType = request.getParameter("playType"); 
			
			String headerFilter  = request.getParameter("headerFilter"); 
			String sortingString = request.getParameter("sortingString");
			String pageNo = request.getParameter("pageNo");
			Integer count = 0;
			
			String fromDateFinal = "";
			String toDateFinal = "";
			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";
			}else{
				fromDateFinal = null;
				toDateFinal = null;
			}
			GridHeaderFilters filter  = GridHeaderFiltersUtil.getPollingCustomerVideoFilter(headerFilter+ sortingString);
			List<PollingCustomerVideo> videoList = DAORegistry.getPollingCustomerVideoDAO().getActiveVideos(fromDateFinal,toDateFinal,name,videoType,playType,filter, sharedProperty); 
			List<PollingVideoCategory> categories = DAORegistry.getPollingVideoCategoryDAO().getAllPollingVideoCategory(new GridHeaderFilters(), "ACTIVE");
			if(videoList == null || videoList.size() <= 0){
				dto.setMessage("No Videos found.");
			}
			dto.setCustomerVideos(videoList);
			int size = 0 ;
			if ( videoList != null ) { size =  videoList.size(); }
			dto.setPagination(PaginationUtil.getDummyPaginationParameters(size));
			dto.setCategories(categories);
			dto.setStatus(1);			
			return dto;
		} catch (Exception e) {
			e.printStackTrace();
			dto.setStatus(0);
			error.setDescription("Error occured while fetching Polling videos.");
			dto.setError(error);
			return dto;
		}
	
	}
	
	@RequestMapping(value="/ManagePollingCustomerVideo")
	public GenericResponseDTO manageCustomerMedia(HttpServletRequest request, HttpServletResponse response, Model model){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		try {
			genericResponseDTO.setStatus(0);
			String action = request.getParameter("action");
			if(action != null && action.equalsIgnoreCase("update")){
				String videoIdStr = request.getParameter("videoIds");
				String[] videoIdArr = null;
				if(videoIdStr!=null) {
					videoIdArr = videoIdStr.split(",");
				}
				for (String artistStr : videoIdArr) {
					Integer artistId= Integer.parseInt(artistStr);
					PollingCustomerVideo object = DAORegistry.getPollingCustomerVideoDAO().get(artistId);
					object.setStatus("PLAYED");
					DAORegistry.getPollingCustomerVideoDAO().update(object);
				}
				genericResponseDTO.setStatus(1);
				com.rtw.tmat.utils.Util.userActionAudit(request, null, "Marked as played successfully.!");
			}
			if(action != null && action.equalsIgnoreCase("delete")){
				String videoIdStr = request.getParameter("videoIds");
				Collection <PollingCustomerVideo> custVideos = new ArrayList<PollingCustomerVideo>();
				String[] videoIdArr = null;
				if(videoIdStr!=null) {
					videoIdArr = videoIdStr.split(",");
				}
				for (String idStr : videoIdArr) {
					Integer id= Integer.parseInt(idStr);					
					/*PollingCustomerVideo object = DAORegistry.getPollingCustomerVideoDAO().get(id);
					String folderName ="";
					folderName = AWSFileService.getCustomerMediaFolderName(object.getIsWinnerOrGeneral());
					String file = com.rtw.tmat.utils.Util.getFileNameFromPath(object.getVideoUrl());
					AWSFileService.deleteFilesFromS3(AWSFileService.getPublicUploadMediaBucketName(),file,folderName);*/
					
					PollingCustomerVideo pollingCustomerVideo = new PollingCustomerVideo();
					pollingCustomerVideo.setId(id);
					custVideos.add(pollingCustomerVideo);
				}
				DAORegistry.getPollingCustomerVideoDAO().deleteAll(custVideos);
				genericResponseDTO.setStatus(1);
				com.rtw.tmat.utils.Util.userActionAudit(request, null, "Delete of Videos successfull.!");
			}
		} catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while marking video as played.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}	
	
	
	@RequestMapping(value="/PublishCustomerMediaToRTFMedia")
	public GenericResponseDTO publishCustomerMediaToRTFMedia(HttpServletRequest request, HttpServletResponse response, Model model){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		try {
			String userName = request.getParameter("userName");
			String videoIdStr = request.getParameter("videoIds");
			String categoryId = request.getParameter("categoryId"); 
			String title = request.getParameter("title"); 
			
			if(title == null || title.isEmpty()){
				genericResponseDTO.setStatus(0);
				error.setDescription("Please add title.");
				genericResponseDTO.setError(error);
				return genericResponseDTO;
			}
			if(categoryId == null || categoryId.isEmpty()){
				genericResponseDTO.setStatus(0);
				error.setDescription("Please select category.");
				genericResponseDTO.setError(error);
				return genericResponseDTO;
			}
			Integer catId = null;
			try {
				catId = Integer.parseInt(categoryId);
			} catch (Exception e) {
				genericResponseDTO.setStatus(0);
				error.setDescription("Please select valid category.");
				genericResponseDTO.setError(error);
				return genericResponseDTO;
			}
			if(videoIdStr == null || videoIdStr.isEmpty()){
				genericResponseDTO.setStatus(0);
				error.setDescription("Please select video to publish.");
				genericResponseDTO.setError(error);
				return genericResponseDTO;
			}
			Map<String, String> map = com.rtw.tmat.utils.Util.getParameterMap(request);
			if(videoIdStr.contains(",")){
				String[] videoIdArr = null;
				videoIdArr = videoIdStr.split(",");
				String filePath= "";
				Boolean flag = false;
				for (String artistStr : videoIdArr) {
					try {
						Integer artistId= Integer.parseInt(artistStr);
						PollingCustomerVideo object = DAORegistry.getPollingCustomerVideoDAO().get(artistId);
						if(object.getIsWinnerOrGeneral()){
							filePath = "winneruploads/"+object.getVideoUrl();
						}else{
							filePath = "generaluploads/"+object.getVideoUrl();
						}
						String fileName = categoryId+"/"+object.getVideoUrl();
						AwsS3Response awsResponse = AWSFileService.transferCustomerMediaFileToRTFMedia(AWSFileService.getPublicUploadMediaBucketName(), filePath, AWSFileService.getRTFTVMediaBucketName(), fileName);
						if(awsResponse == null || awsResponse.getStatus() == 0){
							/*genericResponseDTO.setStatus(0);
							error.setDescription("Error occured while uploading video to S3.");
							genericResponseDTO.setError(error);
							return genericResponseDTO;*/
							flag = true;
							continue;
						}
						object.setIsPublished(true);
						object.setUpdatedBy(userName);
						object.setUpdatedDate(new Date());
						DAORegistry.getPollingCustomerVideoDAO().update(object);
						
						PollingVideoInventory inventory = new PollingVideoInventory();
						inventory.setTitle(title);
						inventory.setCategoryId(catId);
						inventory.setCreatedBy(userName);
						inventory.setCreatedDate(new Date());
						inventory.setCustVideoId(object.getId());
						inventory.setIsDefault(false);
						inventory.setIsDeleted(0);
						inventory.setSponsorId(0);
						inventory.setStatus(true);
						inventory.setVideoUrl(AWSFileService.getAWSBaseURL()+fileName);
						DAORegistry.getPollingVideoInventoryDAO().saveOrUpdate(inventory);
						
						/*map.put("cuId",String.valueOf(object.getCustomerId()));
						map.put("mId", String.valueOf(object.getId()));
						String data = com.rtw.tmat.utils.Util.getObject(map,Constants.BASE_URL+Constants.CREDIT_CUSTOMER_REWARDS);
						Gson gson = GsonCustomConfig.getGsonBuilder();		
						JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
						VideoLikeInfo resp = gson.fromJson(((JsonObject)jsonObject.get("VideoLikeInfo")), VideoLikeInfo.class);
						if(resp.getSts()==0){
							System.out.println("RTF API ERROR : "+resp.getErr().getDesc());
							error.setDescription(resp.getErr().getDesc());
							genericResponseDTO.setError(error);
							genericResponseDTO.setStatus(0);
							return genericResponseDTO;
						}*/
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				String msg = null;
				if(flag){
					msg = "Some media are not upoaded to S3, Please check published column of grid. ";
				}else{
					msg="Selected Media are published successfully.";
				}
				genericResponseDTO.setStatus(1);
				genericResponseDTO.setMessage(msg);
				return genericResponseDTO;
			}else{
				Integer artistId= Integer.parseInt(videoIdStr);
				PollingCustomerVideo object = DAORegistry.getPollingCustomerVideoDAO().get(artistId);
				String filePath = "";
				if(object.getIsWinnerOrGeneral()){
					filePath = "winneruploads/"+object.getVideoUrl();
				}else{
					filePath = "generaluploads/"+object.getVideoUrl();
				}
				String fileName = categoryId+"/"+object.getVideoUrl();
				AwsS3Response awsResponse = AWSFileService.transferCustomerMediaFileToRTFMedia(AWSFileService.getPublicUploadMediaBucketName(), filePath, AWSFileService.getRTFTVMediaBucketName(), fileName);
				if(awsResponse == null || awsResponse.getStatus() == 0){
					genericResponseDTO.setStatus(0);
					error.setDescription("Error occured while uploading video to S3.");
					genericResponseDTO.setError(error);
					return genericResponseDTO;
				}
				
				object.setIsPublished(true);
				object.setUpdatedBy(userName);
				object.setUpdatedDate(new Date());
				DAORegistry.getPollingCustomerVideoDAO().update(object);
				
				PollingVideoInventory inventory = new PollingVideoInventory();
				inventory.setTitle(title);
				inventory.setCategoryId(catId);
				inventory.setCreatedBy(userName);
				inventory.setCreatedDate(new Date());
				inventory.setCustVideoId(object.getId());
				inventory.setIsDefault(false);
				inventory.setIsDeleted(0);
				inventory.setSponsorId(0);
				inventory.setStatus(true);
				inventory.setVideoUrl(AWSFileService.getAWSBaseURL()+fileName);
				 DAORegistry.getPollingVideoInventoryDAO().saveOrUpdate(inventory);
				 
				/*map.put("cuId",String.valueOf(object.getCustomerId()));
				map.put("mId", String.valueOf(object.getId()));
				 String data = com.rtw.tmat.utils.Util.getObject(map,Constants.BASE_URL+Constants.CREDIT_CUSTOMER_REWARDS);
					Gson gson = GsonCustomConfig.getGsonBuilder();		
					JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
					VideoLikeInfo resp = gson.fromJson(((JsonObject)jsonObject.get("VideoLikeInfo")), VideoLikeInfo.class);
					if(resp.getSts()==0){
						System.out.println("RTF API ERROR : "+resp.getErr().getDesc());
						error.setDescription(resp.getErr().getDesc());
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}*/
				 
				 genericResponseDTO.setStatus(1);
				genericResponseDTO.setMessage("Selected Media is published successfully.");
				return genericResponseDTO;
			}
				
		} catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while marking video as played.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}	
	
	
	/*@RequestMapping(value="/DownloadPollingCustomerVideo")
	public AWSResponseDTO downloadPollingCustomerVideo(HttpServletRequest request, HttpServletResponse response, Model model){
		AWSResponseDTO genericResponseDTO = new AWSResponseDTO();
		Error error = new Error();
		try {
			genericResponseDTO.setStatus(0);
			String action = request.getParameter("action");
			if(action != null && action.equalsIgnoreCase("update")){
				String videoIdStr = request.getParameter("videoIds");
				String[] videoIdArr = null;
				if(videoIdStr!=null) {
					videoIdArr = videoIdStr.split(",");
				}
				for (String artistStr : videoIdArr) {
					Integer artistId= Integer.parseInt(artistStr);
					PollingCustomerVideo object = DAORegistry.getPollingCustomerVideoDAO().get(artistId);					
					Customer cust =  DAORegistry.getCustomerDAO().get(object.getCustomerId());
					String userName = cust.getUserId();
					try {
						AwsS3Response awsResponse = AWSFileService.downloadFilesToSharedDrive(userName,object.getVideoUrl(), object.getIsWinnerOrGeneral());
						genericResponseDTO.setFilePath(awsResponse.getFullFilePath());
					}catch(Exception e) {
						e.printStackTrace();
					}
					
				}
				genericResponseDTO.setStatus(1);
				com.rtw.tmat.utils.Util.userActionAudit(request, null, "Videos are downloaded to shared drive.!");
			}
		} catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while downloading videos.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}*/
	
	@RequestMapping(value="/GetAllPollingVideoCategory")
	public PollingVideoCategoryDTO GetAllPollingVideoCategory(HttpServletRequest request, HttpServletResponse response){
		PollingVideoCategoryDTO dto = new PollingVideoCategoryDTO();
		Error error = new Error();
		try {
			String name = request.getParameter("name");
			String videoType = request.getParameter("videoType");
			String playType = request.getParameter("playType"); 
			
			String headerFilter  = request.getParameter("headerFilter"); 
			String sortingString = request.getParameter("sortingString");
			String pageNo = request.getParameter("pageNo");
			
			GridHeaderFilters filter  = GridHeaderFiltersUtil.getPollingVideoCategoryFilter(headerFilter+ sortingString);
			List<PollingVideoCategory> videoCategoryList = DAORegistry.getPollingVideoCategoryDAO().getAllPollingVideoCategory(filter, ""); 
			if(videoCategoryList == null || videoCategoryList.size() <= 0){
				dto.setMessage("No Videos found.");
			}
			dto.setPollingVideoCategories(videoCategoryList);
			int size = 0 ;
			if ( videoCategoryList != null ) { size =  videoCategoryList.size(); }
			dto.setPagination(PaginationUtil.getDummyPaginationParameters(size));
			dto.setStatus(1);			
			return dto;
		} catch (Exception e) {
			e.printStackTrace();
			dto.setStatus(0);
			error.setDescription("Error occured while fetching Polling videos.");
			dto.setError(error);
			return dto;
		}
	
	}
	
	@RequestMapping(value = "/UpdatePollingVideoCategory")
	public PollingVideoCategoryDTO UpdatePollingVideoCategory(HttpServletRequest request, HttpServletResponse response){
		PollingVideoCategoryDTO responseDTO = new PollingVideoCategoryDTO();
		Error error = new Error();
		try{
			String action = request.getParameter("action");
			String idStr = request.getParameter("id");
			String awsUrl = AWSFileService.AWS_MEDIA_URL;
			String imageFile = request.getParameter("imageFile");
			String bucketName = "GENERAL";
			
			if(action==null || action.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please send valid action.");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(action.equalsIgnoreCase("EDIT")){
				if(idStr==null || idStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Polling video category id not found.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer videoCategoryId = null;
				try {
					videoCategoryId = Integer.parseInt(idStr);
				} catch (Exception e) {
					e.printStackTrace();
					responseDTO.setStatus(0);
					error.setDescription("Invalid polling video category id.");
					responseDTO.setError(error);
					return responseDTO;
				}
				PollingVideoCategory pollingvideoCategory = DAORegistry.getPollingVideoCategoryDAO().get(videoCategoryId);
				if(pollingvideoCategory ==  null){
					responseDTO.setStatus(0);
					error.setDescription("Selected polling video category not found in system.");
					responseDTO.setError(error);
					return responseDTO;
				}
				
				responseDTO.setStatus(1);
				responseDTO.setPollingVideoCategory(pollingvideoCategory);
				return responseDTO;
				
				
			}else if(action.equalsIgnoreCase("UPDATE") || action.equalsIgnoreCase("SAVE")){
				
				String categoryName = request.getParameter("categoryName");
				String categoryDescription = request.getParameter("categoryDescription");
				String category = request.getParameter("category");
				String userName = request.getParameter("userName"); 
				if(action.equalsIgnoreCase("SAVE") && (imageFile==null || imageFile.isEmpty())){
					responseDTO.setStatus(0);
					error.setDescription("Video file is mandatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(categoryName==null || categoryName.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Video category name is mandatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(categoryDescription==null || categoryDescription.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Video category description is mandatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				
				PollingVideoCategory videoCategory = null;
				String message = "";
				if(action.equalsIgnoreCase("UPDATE")){
					if(idStr==null || idStr.isEmpty()){
						responseDTO.setStatus(0);
						error.setDescription("Polling video category id not found.");
						responseDTO.setError(error);
						return responseDTO;
					}
					Integer videoId = null;
					try {
						videoId = Integer.parseInt(idStr);
					} catch (Exception e) {
						e.printStackTrace();
						responseDTO.setStatus(0);
						error.setDescription("Invalid polling video category id.");
						responseDTO.setError(error);
						return responseDTO;
					}
					videoCategory = DAORegistry.getPollingVideoCategoryDAO().get(videoId);
					if(videoCategory ==  null){
						responseDTO.setStatus(0);
						error.setDescription("Selected polling video Category not found in system.");
						responseDTO.setError(error);
						return responseDTO;
					}
					message = "Selected Polling video category updateesuccessfully.";
				}else if(action.equalsIgnoreCase("SAVE")){
					Integer maxSeq  = DAORegistry.getPollingVideoCategoryDAO().getMaxSeqPollingCategory();
					videoCategory = new PollingVideoCategory();
					videoCategory.setCreatedDate(new Date());
					videoCategory.setCreatedBy(userName);
					videoCategory.setImageUrl(awsUrl);
					videoCategory.setSequenceNum(maxSeq+1);
					message = "Polling video category saved successfully.";
				}
				MultipartFile file = null;
				MultipartFile iconFile = null;
				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
				file = multipartRequest.getFile("imageFile");
				iconFile = multipartRequest.getFile("iconFile");
				
				if((iconFile==null || iconFile.getSize()<=0) && action.equalsIgnoreCase("SAVE")){
					responseDTO.setStatus(0);
					error.setDescription("Icon file mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if((file==null || file.getSize()<=0) && action.equalsIgnoreCase("SAVE")){
					responseDTO.setStatus(0);
					error.setDescription("Image file mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(file!=null && file.getSize() > 0){
					String path = URLUtil.RTFMEDIA_DIRECTORY+file.getOriginalFilename();
					File newFile = new File(path);
					
					newFile.createNewFile();
					BufferedOutputStream stream = new BufferedOutputStream(
							new FileOutputStream(newFile));
			        FileCopyUtils.copy(file.getInputStream(), stream);
					stream.close();
					AwsS3Response resp = AWSFileService.upLoadFileToS3(AWSFileService.getRTFTVMediaBucketName(), file.getOriginalFilename(), AWSFileService.getUploadMediaIconFolder(), newFile );
					if(resp==null || resp.getStatus() != 1){
						responseDTO.setStatus(0);
						error.setDescription("Error occured while uploading image file to S3.");
						responseDTO.setError(error);
						return responseDTO;
					}
					videoCategory.setImageUrl(awsUrl + AWSFileService.getUploadMediaIconFolder() +"/"+file.getOriginalFilename());
				}
				if(iconFile!=null && iconFile.getSize() > 0){
					String path = URLUtil.RTFMEDIA_DIRECTORY+"icon_"+iconFile.getOriginalFilename();
					File icFile = new File(path);
					
					icFile.createNewFile();
					BufferedOutputStream stream1 = new BufferedOutputStream(
							new FileOutputStream(icFile));
			        FileCopyUtils.copy(iconFile.getInputStream(), stream1);
					stream1.close();
					AwsS3Response resp = AWSFileService.upLoadFileToS3(AWSFileService.getRTFTVMediaBucketName(), "icon_"+iconFile.getOriginalFilename(), AWSFileService.getUploadMediaIconFolder(), icFile );
					if(resp==null || resp.getStatus() != 1){
						responseDTO.setStatus(0);
						error.setDescription("Error occured while uploading icon file to S3.");
						responseDTO.setError(error);
						return responseDTO;
					}
					videoCategory.setIconUrl(awsUrl + AWSFileService.getUploadMediaIconFolder() +"/icon_"+iconFile.getOriginalFilename());
				}
				
				videoCategory.setDescription(categoryDescription);
				videoCategory.setCategoryName(categoryName);
				DAORegistry.getPollingVideoCategoryDAO().saveOrUpdate(videoCategory);
				
				GridHeaderFilters filter  = GridHeaderFiltersUtil.getPollingVideoFilter(null);
				List<PollingVideoCategory> list = DAORegistry.getPollingVideoCategoryDAO().getAllPollingVideoCategory(filter, null);
				
				responseDTO.setStatus(1);
				responseDTO.setMessage(message);
				responseDTO.setPollingVideoCategories(list);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(list!=null?list.size():0));
				return responseDTO;
			}else if(action.equalsIgnoreCase("DELETE")){
				
				if(idStr==null || idStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Polling video inventory id not found.");
					responseDTO.setError(error);
					return responseDTO;
				}
				
				/*Integer videoId = null;
				try {
					videoId = Integer.parseInt(videoIdStr);
				} catch (Exception e) {
					e.printStackTrace();
					responseDTO.setStatus(0);
					error.setDescription("Invalid polling video inventory id.");
					responseDTO.setError(error);
					return responseDTO;
				}
				PollingVideoInventory videoInventory = DAORegistry.getPollingVideoInventoryDAO().get(videoId);
				if(videoInventory ==  null){
					responseDTO.setStatus(0);
					error.setDescription("Selected polling video inventory not found in system.");
					responseDTO.setError(error);
					return responseDTO;
				}*/
				int count = 0;
				count = DAORegistry.getPollingVideoCategoryDAO().getVideoInventoryLinkedToCategory(idStr);
;				if(count > 0){
					responseDTO.setStatus(0);
					error.setDescription("Please delete video associated to this Category in system.");
					responseDTO.setError(error);
					return responseDTO;
				}
				
				DAORegistry.getPollingVideoCategoryDAO().deleteById(Integer.parseInt(idStr));
				
				GridHeaderFilters filter  = GridHeaderFiltersUtil.getPollingVideoCategoryFilter(null);
				List<PollingVideoCategory> list = DAORegistry.getPollingVideoCategoryDAO().getAllPollingVideoCategory(filter, null);
				
				responseDTO.setStatus(1);
				responseDTO.setMessage("Selected Polling video category deleted successfully.");
				responseDTO.setPollingVideoCategories(list);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(list!=null?list.size():0));
				
				return responseDTO;
			}
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while Adding/Updating Polling video category.");
			responseDTO.setError(error);
			responseDTO.setStatus(0);
			return responseDTO;
		}
		return responseDTO;
		
	}
	
	
	@RequestMapping(value = "/UpdateSequencePosition")
	public PollingVideoCategoryDTO updateSequencePosition(HttpServletRequest request, HttpServletResponse response){
		PollingVideoCategoryDTO dto = new PollingVideoCategoryDTO();
		Error error = new Error();
		try {
			String sequenceString = request.getParameter("sequenceString");
			String userName = request.getParameter("userName");
			
			
			if(sequenceString==null || sequenceString.isEmpty()){
				dto.setStatus(0);
				error.setDescription("Please send valid action UP or DOWN to update sequence position.");
				dto.setError(error);
				return dto;
			}
			
			GridHeaderFilters filter = GridHeaderFiltersUtil.getPollingVideoCategoryFilter(null);
			
			if(sequenceString.contains(",")){
				Map<Integer, Integer> map = new HashMap<Integer, Integer>();
				List<Integer> positionList = new ArrayList<Integer>();
				String quePosition[] = sequenceString.split(",");
				for(String str : quePosition){
					if(str.trim().isEmpty()){
						continue;
					}
					if(str.contains(":")){
						String positions[] = str.split(":");
						try {
							map.put(Integer.parseInt(positions[0]),Integer.parseInt(positions[1]));
							positionList.add(Integer.parseInt(positions[1]));
						} catch (Exception e) {
							dto.setStatus(0);
							error.setDescription("Invalid sequence position found.");
							dto.setError(error);
							return dto;
						}
						
					}else{
						dto.setStatus(0);
						error.setDescription("Invalid or bad format sequence position found.");
						dto.setError(error);
						return dto;
					}
				}
				
				Collections.sort(positionList);
				Integer i=1;
				for(Integer p : positionList){
					if(p!=i){
						dto.setStatus(0);
						error.setDescription("Entered position is not sequencial, please enter sequencial position without gap.");
						dto.setError(error);
						return dto;
					}
					i++;
				}
				List<PollingVideoCategory> videoCategoryList = DAORegistry.getPollingVideoCategoryDAO().getAllPollingVideoCategory(filter, ""); 
				for(PollingVideoCategory q : videoCategoryList){
					q.setSequenceNum(map.get(q.getId()));
				}
				DAORegistry.getPollingVideoCategoryDAO().updateAll(videoCategoryList);
				dto.setMessage("Sequence positions updated.");
				
			}else{
				dto.setStatus(0);
				error.setDescription("Invalid or bad format sequence positoin found.");
				dto.setError(error);
				return dto;
			}
			
			
			
			List<PollingVideoCategory> videoCategoryList = DAORegistry.getPollingVideoCategoryDAO().getAllPollingVideoCategory(filter, ""); 
			dto.setStatus(1);
			int size = 0 ;
			if ( videoCategoryList != null ) { size =  videoCategoryList.size(); }
			dto.setPagination(PaginationUtil.getDummyPaginationParameters(size));
			dto.setPollingVideoCategories(videoCategoryList);
			return dto;
		} catch (Exception e) {
			e.printStackTrace();
			dto.setStatus(0);
			error.setDescription("Error occured while updating polling video sequence position.");
			dto.setError(error);
			return dto;	
		}
		
	}
	
	
	/*@RequestMapping(value="/GetCustomerMediaUrl")
	public GenericResponseDTO getCustomerMediaUrl(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO dto = new GenericResponseDTO();
		Error error = new Error();
		try {
			String idStr = request.getParameter("id");
			String userName = request.getParameter("userName");
			
			if(idStr == null || idStr.isEmpty()){
				error.setDescription("Please select view to preview it.");
				dto.setStatus(0);
				dto.setError(error);
				return dto;
			}
			if(userName == null || userName.isEmpty()){
				error.setDescription("Not able to identify logged in user detail, please referesh page and try again.");
				dto.setStatus(0);
				dto.setError(error);
				return dto;
			}
			 Integer id = null;
			try {
				id = Integer.parseInt(idStr);
			} catch (Exception e) {
				error.setDescription("Error coccured while getting customer media Url.");
				dto.setStatus(0);
				dto.setError(error);
				return dto;
			}
			PollingCustomerVideo media = DAORegistry.getPollingCustomerVideoDAO().get(id);
			if(media == null){
				error.setDescription("No Media found in system with given identifier.");
				dto.setStatus(0);
				dto.setError(error);
				return dto;
			}
			
			String filePath = "";
			if(media.getIsWinnerOrGeneral()){
				filePath = "winneruploads/"+media.getVideoUrl();
			}else{
				filePath = "generaluploads/"+media.getVideoUrl();
			}
			
			int lastIndexOf = media.getVideoUrl().lastIndexOf(".");
			String ext =  media.getVideoUrl().substring(lastIndexOf);
			String fileName = AWSFileService.CUSTOMER_VIDEO_PUBLIC_FOLDER+"/"+userName+ext;
			
			AwsS3Response awsS3Response = AWSFileService.transferCustomerMediaFileToRTFMedia(AWSFileService.getPublicUploadMediaBucketName(), filePath, AWSFileService.getRTFTVMediaBucketName(), fileName);
			if(awsS3Response.getStatus() == null || awsS3Response.getStatus().equals(0)){
				error.setDescription("Error occured while generating customer media public URL.");
				dto.setStatus(0);
				dto.setError(error);
				return dto;
			}
			String publicURL = AWSFileService.AWS_MEDIA_URL+fileName;
			dto.setMessage(publicURL);
			dto.setStatus(1);
			return dto;
			
		} catch (Exception e) {
			e.printStackTrace();
			dto.setStatus(0);
			error.setDescription("Error occured while fetching Polling videos.");
			dto.setError(error);
			return dto;
		}
	}*/
	
	
	
	@RequestMapping(value="/GetRewardConfigDetails")
	public RTFRewardConfigDTO getRewardConfigDetails(HttpServletRequest request, HttpServletResponse response){
		RTFRewardConfigDTO responseDTO = new RTFRewardConfigDTO();
		Error error = new Error();
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String status = request.getParameter("status");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getRewardConfigFilter(headerFilter);
			if(status==null || status.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please send valid status.");
				responseDTO.setError(error);
				return responseDTO;
			}
			List<RtfRewardConfigInfo> configs = QuizDAORegistry.getRtfRewardConfigInfoDAO().getAllRtfRewardConfigByStatus(filter, status);
			responseDTO.setConfigs(configs);
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(configs.size()));
			responseDTO.setStatus(1);
			if(configs.isEmpty()){
				responseDTO.setStatus(1);
				responseDTO.setMessage("No reward config info  found in system.");
				return responseDTO;
			}
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while fetching reward config info.");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}
	
	
	
	@RequestMapping(value="/UpdateRewardConfigInfo")
	public RTFRewardConfigDTO updateRewardConfigInfo(HttpServletRequest request, HttpServletResponse response){
		RTFRewardConfigDTO responseDTO = new RTFRewardConfigDTO();
		Error error = new Error();
		try {
			String title = request.getParameter("title");
			String configIdStr = request.getParameter("configId");
			String actionType = request.getParameter("actionType");
			String livesStr = request.getParameter("lives");
			String magicWandStr = request.getParameter("magicWand");
			String starsStr = request.getParameter("stars");
			String pointsStr = request.getParameter("points");
			String dollarsStr = request.getParameter("dollars");
			String userName = request.getParameter("userName");
			String action = request.getParameter("action");
			String maxRewardStr = request.getParameter("maxReward");
			String noOfLikesStr = request.getParameter("noOfLikes");
			String minVideoTimeStr = request.getParameter("minVideoTime");
			String rewardIntervaStrl = request.getParameter("rewardInterval");
			
			if(action == null || action.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please send valid action to update/save reward config info.");
				responseDTO.setError(error);
				return responseDTO;
			}
			GridHeaderFilters filter = GridHeaderFiltersUtil.getGiftCardFilter(null);
			if(action.equalsIgnoreCase("EDIT")){
				if(configIdStr==null || configIdStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Cannot Edit reward config info id not found.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer configId = 0;
				try {
					configId = Integer.parseInt(configIdStr);
				} catch (Exception e) {
					e.printStackTrace();
					responseDTO.setStatus(0);
					error.setDescription("Cannot Edit invalid reward config info id.");
					responseDTO.setError(error);
					return responseDTO;
				}
				RtfRewardConfigInfo configInfo  = QuizDAORegistry.getRtfRewardConfigInfoDAO().get(configId);
				if(configInfo == null){
					responseDTO.setStatus(0);
					error.setDescription("Cannot Edit reward config details its not found with given identifier.");
					responseDTO.setError(error);
					return responseDTO;
				}
				responseDTO.setConfig(configInfo);
				responseDTO.setStatus(1);
				return responseDTO;
			}else if(action.equalsIgnoreCase("SAVE") || action.equalsIgnoreCase("UPDATE")){
				if(actionType==null || actionType.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("action Type is Mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(title==null || title.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Media description is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer lives = 0;
				Integer magicWand = 0;
				Integer stars = 0;
				Integer points =0;
				Double rewardDollars = 0.00;
				if(livesStr!=null && !livesStr.isEmpty()){
					try {
						lives = Integer .parseInt(livesStr);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if(magicWandStr!=null && !magicWandStr.isEmpty()){
					try {
						magicWand = Integer.parseInt(magicWandStr);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if(starsStr!=null && !starsStr.isEmpty()){
					try {
						stars = Integer.parseInt(starsStr);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if(pointsStr!=null && !pointsStr.isEmpty()){
					try {
						points = Integer.parseInt(pointsStr);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if(dollarsStr!=null && !dollarsStr.isEmpty()){
					try {
						rewardDollars = Double.parseDouble(dollarsStr);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				Integer maxRewards = 0;
				Integer batchSize = 0;
				Integer minVideoPlayTime = 0;
				Integer videoInterval = 0;
				if(maxRewardStr == null || maxRewardStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Please Add Max Rewards per day.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(maxRewardStr != null && !maxRewardStr.isEmpty()){
					try {
						maxRewards = Integer.parseInt(maxRewardStr);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if(noOfLikesStr != null && !noOfLikesStr.isEmpty()){
					try {
						batchSize = Integer.parseInt(noOfLikesStr);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if(minVideoTimeStr != null && !minVideoTimeStr.isEmpty()){
					try {
						minVideoPlayTime = Integer.parseInt(minVideoTimeStr);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if(rewardIntervaStrl != null && !rewardIntervaStrl.isEmpty()){
					try {
						videoInterval = Integer.parseInt(rewardIntervaStrl);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				RtfRewardConfigInfo config = null;
				String msg = null;
				if(action.equalsIgnoreCase("UPDATE")){
					if(configIdStr==null || configIdStr.isEmpty()){
						responseDTO.setStatus(0);
						error.setDescription("Cannot update reward config info id not found.");
						responseDTO.setError(error);
						return responseDTO;
					}
					Integer configId = 0;
					try {
						configId = Integer.parseInt(configIdStr);
					} catch (Exception e) {
						e.printStackTrace();
						responseDTO.setStatus(0);
						error.setDescription("Cannot update invalid reward config info id.");
						responseDTO.setError(error);
						return responseDTO;
					}
					config = QuizDAORegistry.getRtfRewardConfigInfoDAO().get(configId);
					if(config == null){
						responseDTO.setStatus(0);
						error.setDescription("Cannot update reward config info its not found with given identifier.");
						responseDTO.setError(error);
						return responseDTO;
					}
					config.setUpdatedDate(new Date());
					config.setUpdatedBy(userName);
					config.setStatus(false);
					QuizDAORegistry.getRtfRewardConfigInfoDAO().update(config);
					msg = "Reward config info updated successfully.";
				}else{
					List<RtfRewardConfigInfo> oldConfigs = QuizDAORegistry.getRtfRewardConfigInfoDAO().getRtfRewardConfigByRewardType(actionType);
					if(oldConfigs!=null && !oldConfigs.isEmpty()){
						responseDTO.setStatus(0);
						error.setDescription("There is already reward config record for "+actionType+", Multiple configuration with same reward type is not allowed.");
						responseDTO.setError(error);
						return responseDTO;
					}
					config = new RtfRewardConfigInfo();
					msg = "Reward config info saved successfully.";
				}
				
				config.setCreatedDate(new Date());
				config.setCreatedBy(userName);
				config.setActionType(actionType);
				config.setActiveDate(new Date());
				config.setLives(lives);
				config.setMagicWands(magicWand);
				config.setMediaDescription(title);
				config.setRtfPoints(points);
				config.setRwdDollars(rewardDollars);
				config.setSfStars(stars);
				config.setStatus(true);
				config.setMaxRewardsPerDay(maxRewards);
				config.setMinVideoPlayTime(minVideoPlayTime);
				config.setVideoRewardInterval(videoInterval);
				config.setBatchSize(batchSize);
				config.setIsDisplay(true);
				QuizDAORegistry.getRtfRewardConfigInfoDAO().saveOrUpdate(config);
				
				List<RtfRewardConfigInfo> configs = QuizDAORegistry.getRtfRewardConfigInfoDAO().getAllRtfRewardConfigByStatus(filter, "ACTIVE");
				responseDTO.setConfigs(configs);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(configs.size()));
				responseDTO.setStatus(1);
				responseDTO.setMessage(msg);
				return responseDTO;
			}else if(action.equalsIgnoreCase("DELETE")){
				if(configIdStr==null || configIdStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Cannot update reward config info id not found.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer configId = 0;
				try {
					configId = Integer.parseInt(configIdStr);
				} catch (Exception e) {
					e.printStackTrace();
					responseDTO.setStatus(0);
					error.setDescription("Cannot delete invalid reward config info id.");
					responseDTO.setError(error);
					return responseDTO;
				}
				RtfRewardConfigInfo config = QuizDAORegistry.getRtfRewardConfigInfoDAO().get(configId);
				if(config == null){
					responseDTO.setStatus(0);
					error.setDescription("Cannot delete reward config info its not found with given identifier.");
					responseDTO.setError(error);
					return responseDTO;
				}
				config.setStatus(false);
				config.setUpdatedDate(new Date());
				config.setUpdatedBy(userName);
				
				QuizDAORegistry.getRtfRewardConfigInfoDAO().update(config);
				List<RtfRewardConfigInfo> configs = QuizDAORegistry.getRtfRewardConfigInfoDAO().getAllRtfRewardConfigByStatus(filter, "ACTIVE");
				responseDTO.setConfigs(configs);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(configs.size()));
				responseDTO.setStatus(1);
				responseDTO.setMessage("Selected reward config settings deleted successfully.");
				return responseDTO;
			}
			responseDTO.setStatus(0);
			error.setDescription("Not able to identify action type EDIT/SAVE/UPDATE/DELETE.");
			responseDTO.setError(error);
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while fetching reward config settings.");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}
	
	
	
	@RequestMapping(value="/GetRtfPointsConvSetting")
	public RtfPointConversionSettingDTO getRtfPointsConvSetting(HttpServletRequest request, HttpServletResponse response){
		RtfPointConversionSettingDTO responseDTO = new RtfPointConversionSettingDTO();
		Error error = new Error();
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String status = request.getParameter("status");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getRtfPointConversionSeettingsFilter(headerFilter);
			if(status==null || status.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please send valid status.");
				responseDTO.setError(error);
				return responseDTO;
			}
			List<RtfPointsConversionSetting> settings = QuizDAORegistry.getRtfPointsConversionSettingDAO().getAllRtfPOintsSettingsByStatus(filter, status);
			responseDTO.setSettings(settings);
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(settings.size()));
			responseDTO.setStatus(1);
			if(settings.isEmpty()){
				responseDTO.setStatus(1);
				responseDTO.setMessage("No Rtf Points conversion settings found in system.");
				return responseDTO;
			}
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while Rtf Points conversion settings.");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}
	
	
	
	@RequestMapping(value="/UpdateRtfPointsConvSetting")
	public RtfPointConversionSettingDTO updateRtfPointsConvSetting(HttpServletRequest request, HttpServletResponse response){
		RtfPointConversionSettingDTO responseDTO = new RtfPointConversionSettingDTO();
		Error error = new Error();
		try {
			String powerUpType = request.getParameter("powerUpType");
			String settingIdStr = request.getParameter("settingId");
			String qtyStr = request.getParameter("qty");
			String pointsStr = request.getParameter("points");
			String userName = request.getParameter("userName");
			String action = request.getParameter("action");
			
			if(action == null || action.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please send valid action to update/save points conversion setting.");
				responseDTO.setError(error);
				return responseDTO;
			}
			GridHeaderFilters filter = GridHeaderFiltersUtil.getGiftCardFilter(null);
			if(action.equalsIgnoreCase("EDIT")){
				if(settingIdStr==null || settingIdStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Cannot Edit points conversion setting id not found.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer settingId = 0;
				try {
					settingId = Integer.parseInt(settingIdStr);
				} catch (Exception e) {
					e.printStackTrace();
					responseDTO.setStatus(0);
					error.setDescription("Cannot Edit invalid points conversion setting id.");
					responseDTO.setError(error);
					return responseDTO;
				}
				RtfPointsConversionSetting setting = QuizDAORegistry.getRtfPointsConversionSettingDAO().get(settingId);
				if(setting == null){
					responseDTO.setStatus(0);
					error.setDescription("Cannot Edit points conversion setting its not found with given identifier.");
					responseDTO.setError(error);
					return responseDTO;
				}
				responseDTO.setSetting(setting);
				responseDTO.setStatus(1);
				return responseDTO;
			}else if(action.equalsIgnoreCase("SAVE") || action.equalsIgnoreCase("UPDATE")){
				if(powerUpType==null || powerUpType.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Poit conversion type is Mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(qtyStr==null || qtyStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Quantity is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(pointsStr==null || pointsStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Points is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer qty = 0;
				Integer points =0;
				try {
					qty = Integer .parseInt(qtyStr);
				} catch (Exception e) {
					e.printStackTrace();
					responseDTO.setStatus(0);
					error.setDescription("please add valid quantity.");
					responseDTO.setError(error);
					return responseDTO;
				}
				try {
					points = Integer .parseInt(pointsStr);
				} catch (Exception e) {
					e.printStackTrace();
					responseDTO.setStatus(0);
					error.setDescription("please add valid points.");
					responseDTO.setError(error);
					return responseDTO;
				}
				RtfPointsConversionSetting setting = null;
				String msg = null;
				if(action.equalsIgnoreCase("UPDATE")){
					if(settingIdStr==null || settingIdStr.isEmpty()){
						responseDTO.setStatus(0);
						error.setDescription("Cannot update point conversion setting id not found.");
						responseDTO.setError(error);
						return responseDTO;
					}
					Integer settingId = 0;
					try {
						settingId = Integer.parseInt(settingIdStr);
					} catch (Exception e) {
						e.printStackTrace();
						responseDTO.setStatus(0);
						error.setDescription("Cannot update invalid point conversion setting id.");
						responseDTO.setError(error);
						return responseDTO;
					}
					setting = QuizDAORegistry.getRtfPointsConversionSettingDAO().get(settingId);
					if(setting == null){
						responseDTO.setStatus(0);
						error.setDescription("Cannot Edit points conversion setting its not found with given identifier.");
						responseDTO.setError(error);
						return responseDTO;
					}
					setting.setUpdatedDate(new Date());
					setting.setUpdatedBy(userName);
					setting.setStatus(false);
					QuizDAORegistry.getRtfPointsConversionSettingDAO().update(setting);
					msg = "Points conversion setting updated successfully.";
				}else{
					List<RtfPointsConversionSetting> oldSettings = QuizDAORegistry.getRtfPointsConversionSettingDAO().getRtfPointsSettingByTypeAndQty(powerUpType, qty);
					if(oldSettings!=null && !oldSettings.isEmpty()){
						responseDTO.setStatus(0);
						error.setDescription("There is already point conversion setting type '"+powerUpType+"' with given quantity, Multiple setting with same conversion type and quantity is not allowed.");
						responseDTO.setError(error);
						return responseDTO;
					}
					msg = "Points conversion setting saved successfully.";
				}
				setting = new RtfPointsConversionSetting();
				setting.setCreatedDate(new Date());
				setting.setCreatedBy(userName);
				setting.setQty(qty);
				setting.setPowerUpType(powerUpType);
				setting.setRtfPoints(points);
				setting.setStatus(true);
				QuizDAORegistry.getRtfPointsConversionSettingDAO().save(setting);
				
				List<RtfPointsConversionSetting> settings = QuizDAORegistry.getRtfPointsConversionSettingDAO().getAllRtfPOintsSettingsByStatus(filter, "ACTIVE");
				responseDTO.setSettings(settings);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(settings.size()));
				responseDTO.setStatus(1);
				responseDTO.setMessage(msg);
				return responseDTO;
			}else if(action.equalsIgnoreCase("DELETE")){
				if(settingIdStr==null || settingIdStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Cannot delete point conversion setting id not found.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer settingId = 0;
				try {
					settingId = Integer.parseInt(settingIdStr);
				} catch (Exception e) {
					e.printStackTrace();
					responseDTO.setStatus(0);
					error.setDescription("Cannot delete invalid point conversion setting id.");
					responseDTO.setError(error);
					return responseDTO;
				}
				RtfPointsConversionSetting setting = QuizDAORegistry.getRtfPointsConversionSettingDAO().get(settingId);
				if(setting == null){
					responseDTO.setStatus(0);
					error.setDescription("Cannot delete points conversion setting its not found with given identifier.");
					responseDTO.setError(error);
					return responseDTO;
				}
				setting.setStatus(false);
				setting.setUpdatedDate(new Date());
				setting.setUpdatedBy(userName);
				
				QuizDAORegistry.getRtfPointsConversionSettingDAO().update(setting);
				List<RtfPointsConversionSetting> settings = QuizDAORegistry.getRtfPointsConversionSettingDAO().getAllRtfPOintsSettingsByStatus(filter, "ACTIVE");
				responseDTO.setSettings(settings);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(settings.size()));
				responseDTO.setStatus(1);
				responseDTO.setMessage("Selected points conversion setting deleted successfully.");
				return responseDTO;
			}
			responseDTO.setStatus(0);
			error.setDescription("Not able to identify action type EDIT/SAVE/UPDATE/DELETE.");
			responseDTO.setError(error);
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while fetching points conversion setting.");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}
	
	
	@RequestMapping(value="/GetAbusedComments")
	public AbuseReportedDTO getAbusedComment(HttpServletRequest request, HttpServletResponse response){
		AbuseReportedDTO responseDTO = new AbuseReportedDTO();
		Error error = new Error();
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String status = request.getParameter("status");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getReportedCommentFilter(headerFilter);
			if(status==null || status.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please send valid status.");
				responseDTO.setError(error);
				return responseDTO;
			}
			List<CustomerReportedMedia> reportedDatas = QuizDAORegistry.getCustomerReportedMediaDAO().getAllReportedComment(filter,status, sharedProperty);
			responseDTO.setReportedDatas(reportedDatas);
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(reportedDatas.size()));
			responseDTO.setStatus(1);
			if(reportedDatas.isEmpty()){
				responseDTO.setStatus(1);
				responseDTO.setMessage("No Abused reported comments found.");
				return responseDTO;
			}
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while fetching Abused reported comments.");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}
	
	
	@RequestMapping(value="/UpdateAbuseComment")
	public AbuseReportedDTO updateAbuseComment(HttpServletRequest request, HttpServletResponse response){
		AbuseReportedDTO responseDTO = new AbuseReportedDTO();
		Error error = new Error();
		try {
			String abuseIdStr  = request.getParameter("abuseId");
			String action = request.getParameter("action");
			String reason = request.getParameter("reason");
			String userName = request.getParameter("userName");
			String msg = "";
			if(action==null || action.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Could not able to identify action to be perform.");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(abuseIdStr==null || abuseIdStr.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Could not able to identify abuse comment.");
				responseDTO.setError(error);
				return responseDTO;
			}
			Integer abuseId = 0;
			try {
				abuseId = Integer.parseInt(abuseIdStr);
			} catch (Exception e) {
				e.printStackTrace();
				responseDTO.setStatus(0);
				error.setDescription("Invalid abuse comment id.");
				responseDTO.setError(error);
				return responseDTO;
			}
			
			CustomerReportedMedia abuseComment = QuizDAORegistry.getCustomerReportedMediaDAO().get(abuseId);
			if(abuseComment==null){
				responseDTO.setStatus(0);
				error.setDescription("Abuse reported comment not found in system with given identifier.");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(abuseComment.getCommentId() == null || abuseComment.getCommentId() < 0){
				responseDTO.setStatus(0);
				error.setDescription("Abuse reported comment not found in system..");
				responseDTO.setError(error);
				return responseDTO;
			}
			CustomerComment comment = QuizDAORegistry.getCustomerCommentDAO().get(abuseComment.getCommentId());
			if(comment == null){
				responseDTO.setStatus(0);
				error.setDescription("Customer Comment not found in system.");
				responseDTO.setError(error);
				return responseDTO;
			}
			
			if(action.equalsIgnoreCase("REMOVE")){
				comment.setHideBy(userName);
				comment.setHideComment(true);
				comment.setHideDate(new Date());
				comment.setHideReason(reason);
				QuizDAORegistry.getCustomerCommentDAO().update(comment);
				msg = "Selected comment reomved successfully.";
			}else if(action.equalsIgnoreCase("BLOCK")){
				comment.setHideBy(userName);
				comment.setHideComment(true);
				comment.setHideDate(new Date());
				comment.setHideReason(reason);
				comment.setBlockBy(userName);
				comment.setBlockDate(new Date());
				comment.setBlockReason(reason);
				comment.setBolckStatus(true);
				QuizDAORegistry.getCustomerCommentDAO().update(comment);
				
				/*Customer customer = DAORegistry.getCustomerDAO().get(comment.getCustomerId());
				DAORegistry.getCustomerDAO().update(customer);*/
				Map<String, String> map = com.rtw.tmat.utils.Util.getParameterMap(request);
				map.put("cuId", String.valueOf(comment.getCustomerId()));
				map.put("source", "MEDIACOMMENT");
				map.put("sourceId", String.valueOf(comment.getId()));
				map.put("userName", userName);
				

				String data = com.rtw.tmat.utils.Util.getObject(map, Constants.BASE_URL + Constants.BLOCK_CUSTOMER_COMMENT);
				Gson gson = GsonCustomConfig.getGsonBuilder();
				JsonObject jsonObject = gson.fromJson(data, JsonObject.class);

				CommonRespInfo respDTO = gson.fromJson(((JsonObject) jsonObject.get("commonRespInfo")),CommonRespInfo.class);
				
				if(respDTO.getSts() == 1){
					msg = "Selected Media comment is Removed and comment user blocked.";
				}else{
					msg = "Selected media comment removed, While blocking user Error occured : "+respDTO.getErr().getDesc();
				}
			}
			List<CustomerReportedMedia> reportedDatas = QuizDAORegistry.getCustomerReportedMediaDAO().getAllReportedComment(new GridHeaderFilters(),"ACTIVE", sharedProperty);
			responseDTO.setReportedDatas(reportedDatas);
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(reportedDatas.size()));
			responseDTO.setStatus(1);
			responseDTO.setMessage(msg);
			if(reportedDatas.isEmpty()){
				responseDTO.setStatus(1);
				responseDTO.setMessage("No Abused reported comments found.");
				return responseDTO;
			}
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while fetching Abused reported comments.");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}
	
	
	
	@RequestMapping(value="/GetAbusedMedias")
	public AbuseReportedDTO getAbusedMedias(HttpServletRequest request, HttpServletResponse response){
		AbuseReportedDTO responseDTO = new AbuseReportedDTO();
		Error error = new Error();
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String status = request.getParameter("status");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getReportedMediaFIlter(headerFilter);
			if(status==null || status.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please send valid status.");
				responseDTO.setError(error);
				return responseDTO;
			}
			List<CustomerReportedMedia> reportedDatas = QuizDAORegistry.getCustomerReportedMediaDAO().getAllReportedMedia(filter,status, sharedProperty);
			responseDTO.setReportedDatas(reportedDatas);
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(reportedDatas.size()));
			responseDTO.setStatus(1);
			if(reportedDatas.isEmpty()){
				responseDTO.setStatus(1);
				responseDTO.setMessage("No Abused reported media found.");
				return responseDTO;
			}
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while fetching Abused reported media.");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}
	
	
	@RequestMapping(value="/UpdateAbuseMedia")
	public AbuseReportedDTO updateAbuseMedia(HttpServletRequest request, HttpServletResponse response){
		AbuseReportedDTO responseDTO = new AbuseReportedDTO();
		Error error = new Error();
		try {
			String abuseIdStr  = request.getParameter("abuseId");
			String action = request.getParameter("action");
			String reason = request.getParameter("reason");
			String userName = request.getParameter("userName");
			String msg = "";
			if(action==null || action.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Could not able to identify action to be perform.");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(abuseIdStr==null || abuseIdStr.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Could not able to identify abuse media.");
				responseDTO.setError(error);
				return responseDTO;
			}
			Integer abuseId = 0;
			try {
				abuseId = Integer.parseInt(abuseIdStr);
			} catch (Exception e) {
				e.printStackTrace();
				responseDTO.setStatus(0);
				error.setDescription("Invalid abuse media id.");
				responseDTO.setError(error);
				return responseDTO;
			}
			
			CustomerReportedMedia abuseMedia = QuizDAORegistry.getCustomerReportedMediaDAO().get(abuseId);
			if(abuseMedia==null){
				responseDTO.setStatus(0);
				error.setDescription("Abuse reported Media not found in system with given identifier.");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(abuseMedia.getMediaId() == null || abuseMedia.getMediaId() < 0){
				responseDTO.setStatus(0);
				error.setDescription("Abuse reported Media not found in system..");
				responseDTO.setError(error);
				return responseDTO;
			}
			PollingVideoInventory media = DAORegistry.getPollingVideoInventoryDAO().get(abuseMedia.getMediaId());
			if(media == null){
				responseDTO.setStatus(0);
				error.setDescription("Customer media not found in system.");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(media.getCustVideoId() != null){
				responseDTO.setStatus(0);
				error.setDescription("Customer media not found in system.");
				responseDTO.setError(error);
				return responseDTO;
			}
			Integer customerId = null;
			if(media.getCustVideoId()!=null){
				PollingCustomerVideo video  =  DAORegistry.getPollingCustomerVideoDAO().get(media.getCustVideoId());
				video.setBlockBy(userName);
				customerId = video.getCustomerId();
				video.setBlockDate(new Date());
				video.setStatus(action.equalsIgnoreCase("REMOVE")?"DELETED":"BLOCKED");
				DAORegistry.getPollingCustomerVideoDAO().update(video);
			}else if(media.getFanclubVideoId()!=null){
				FanClubVideo fVideo  = QuizDAORegistry.getFanClubVideoDAO().get(media.getFanclubVideoId());
				fVideo.setBlockBy(userName);
				customerId = fVideo.getCustomerId();
				fVideo.setBlockDate(new Date());
				fVideo.setBlockReason(reason);
				fVideo.setStatus(action.equalsIgnoreCase("REMOVE")?"DELETED":"BLOCKED");
				QuizDAORegistry.getFanClubVideoDAO().update(fVideo);
			}
			
			
			if(action.equalsIgnoreCase("REMOVE")){
				media.setHideBy(userName);
				media.setHideMedia(true);
				media.setHideDate(new Date());
				media.setHideReason(reason);
				DAORegistry.getPollingVideoInventoryDAO().update(media);
				msg = "Selected video removed successfully.";
			}else if(action.equalsIgnoreCase("BLOCK")){
				media.setHideBy(userName);
				media.setHideMedia(true);
				media.setHideDate(new Date());
				media.setHideReason(reason);
				media.setBlockBy(userName);
				media.setBlockDate(new Date());
				media.setBlockReason(reason);
				media.setBolckStatus(true);
				DAORegistry.getPollingVideoInventoryDAO().update(media);
				
				/*Customer customer =  DAORegistry.getCustomerDAO().get(video.getCustomerId());
				customer.setIsBlocked(true);
				DAORegistry.getCustomerDAO().update(customer);*/
				Map<String, String> map = com.rtw.tmat.utils.Util.getParameterMap(request);
				map.put("cuId", String.valueOf(customerId));
				map.put("source", "CUSTOMERMEDIA");
				map.put("sourceId", String.valueOf(media.getId()));
				map.put("userName", userName);
				

				String data = com.rtw.tmat.utils.Util.getObject(map, Constants.BASE_URL + Constants.BLOCK_CUSTOMER_COMMENT);
				Gson gson = GsonCustomConfig.getGsonBuilder();
				JsonObject jsonObject = gson.fromJson(data, JsonObject.class);

				CommonRespInfo respDTO = gson.fromJson(((JsonObject) jsonObject.get("commonRespInfo")),CommonRespInfo.class);
				
				if(respDTO.getSts() == 1){
					msg = "Selected Media is Removed and media user blocked for future uploads.";
				}else{
					msg = "Selected media removed, While blocking user Error occured : "+respDTO.getErr().getDesc();
				}
			}
			
			List<CustomerReportedMedia> reportedDatas = QuizDAORegistry.getCustomerReportedMediaDAO().getAllReportedMedia(new GridHeaderFilters(),"ACTIVE", sharedProperty);
			responseDTO.setReportedDatas(reportedDatas);
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(reportedDatas.size()));
			responseDTO.setStatus(1);
			responseDTO.setMessage(msg);
			if(reportedDatas.isEmpty()){
				responseDTO.setStatus(1);
				responseDTO.setMessage("No Abused reported comments found.");
				return responseDTO;
			}
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while fetching Abused reported comments.");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}
	
	
	
	@RequestMapping(value="/GetSuperFanLevelConf")
	public SuperFanLevelDTO getSuperFanLevelConf(HttpServletRequest request, HttpServletResponse response){
		SuperFanLevelDTO responseDTO = new SuperFanLevelDTO();
		Error error = new Error();
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String status = request.getParameter("status");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getSuperfanStarsFilter(headerFilter);
			if(status==null || status.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please send valid status.");
				responseDTO.setError(error);
				return responseDTO;
			}
			List<SuperFanLevels> levels = QuizDAORegistry.getSuperFanLevelsDAO().getAllActiveSuperFanLevels(filter, status);
			responseDTO.setLevels(levels);
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(levels.size()));
			responseDTO.setStatus(1);
			if(levels.isEmpty()){
				responseDTO.setStatus(1);
				responseDTO.setMessage("No super fan levels configurations found.");
				return responseDTO;
			}
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while fetching super fan levels.");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}
	
	
	@RequestMapping(value="/UpdateSuperFanLevelConf")
	public SuperFanLevelDTO updateSuperFanLevelConf(HttpServletRequest request, HttpServletResponse response){
		SuperFanLevelDTO responseDTO = new SuperFanLevelDTO();
		Error error = new Error();
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String action = request.getParameter("action");
			String levelIdStr = request.getParameter("levelId");
			String userName = request.getParameter("userName"); 
			String msg = "";
			GridHeaderFilters filter = GridHeaderFiltersUtil.getSuperfanStarsFilter(headerFilter);
			if(action==null || action.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Could not able to identify action to be perform.");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(action.equalsIgnoreCase("EDIT")){
				if(levelIdStr==null || levelIdStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Super fan Level id not found.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer levelId = 0;
				try {
					levelId = Integer.parseInt(levelIdStr);
				} catch (Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Super fan Level id is invalid.");
					responseDTO.setError(error);
					return responseDTO;
				}
				SuperFanLevels level = QuizDAORegistry.getSuperFanLevelsDAO().get(levelId);
				if(level == null){
					responseDTO.setStatus(0);
					error.setDescription("Super fan level configuration not found with given identifier.");
					responseDTO.setError(error);
					return responseDTO;
				}
				responseDTO.setStatus(1);
				responseDTO.setLevel(level);
				return responseDTO;
			}else if(action.equalsIgnoreCase("SAVE") || action.equalsIgnoreCase("UPDATE")){
				String starsFromStr = request.getParameter("starsFrom");
				String starsToStr = request.getParameter("starsTo");
				String levelNoStr = request.getParameter("levelNo");
				String title = request.getParameter("title");
				
				if(title==null || title.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Level title is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(starsFromStr==null || starsFromStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Stars start from is mendatory for level.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(starsToStr==null || starsToStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Stars start to is mendatory for level.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer starsTo = 0;
				Integer starsFrom = 0;
				try {
					starsFrom = Integer.parseInt(starsFromStr);
				} catch (Exception e) {
					e.printStackTrace();
					responseDTO.setStatus(0);
					error.setDescription("Stars start from is invalid for level.");
					responseDTO.setError(error);
					return responseDTO;
				}
				try {
					starsTo = Integer.parseInt(starsToStr);
				} catch (Exception e) {
					e.printStackTrace();
					responseDTO.setStatus(0);
					error.setDescription("Stars start to is invalid for level.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(starsFrom >= starsTo){
					responseDTO.setStatus(0);
					error.setDescription("Invalid range, stars from should always less than stars to.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(levelNoStr==null || levelNoStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Level No is mendatory for level.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer levelNo = 0;
				try {
					levelNo = Integer.parseInt(levelNoStr);
				} catch (Exception e) {
					e.printStackTrace();
					responseDTO.setStatus(0);
					error.setDescription("Level No is invalid for level.");
					responseDTO.setError(error);
					return responseDTO;
				}
				SuperFanLevels level = null;
				if(action.equalsIgnoreCase("UPDATE")){
					if(levelIdStr==null || levelIdStr.isEmpty()){
						responseDTO.setStatus(0);
						error.setDescription("Super fan Level id not found.");
						responseDTO.setError(error);
						return responseDTO;
					}
					Integer levelId = 0;
					try {
						levelId = Integer.parseInt(levelIdStr);
					} catch (Exception e) {
						responseDTO.setStatus(0);
						error.setDescription("Super fan Level id is invalid.");
						responseDTO.setError(error);
						return responseDTO;
					}
					level = QuizDAORegistry.getSuperFanLevelsDAO().get(levelId);
					if(level == null){
						responseDTO.setStatus(0);
						error.setDescription("Super fan level configuration not found with given identifier.");
						responseDTO.setError(error);
						return responseDTO;
					}
					level.setUpdatedBy(userName);
					level.setUpdatedDate(new Date());
					msg = "Selected Super fan Level configuration updated successfully.";
				}else if(action.equalsIgnoreCase("SAVE")){
					SuperFanLevels oldLevel = QuizDAORegistry.getSuperFanLevelsDAO().getSuperFanLevelByLeveNo(levelNo);
					if(oldLevel !=null){
						responseDTO.setStatus(0);
						error.setDescription("Provided super fan level no is laready configured, Cannot have multiple records with same level no.");
						responseDTO.setError(error);
						return responseDTO;
					}
					level = new SuperFanLevels();
					level.setCreatedBy(userName);
					level.setCreatedDate(new Date());
					level.setStatus("ACTIVE");
					msg = "Selected Super fan Level configuration saved successfully.";
				}
				
				level.setStarsFrom(starsFrom);
				level.setStarsTo(starsTo);
				level.setLevelNo(levelNo);
				level.setTitle(title);
				QuizDAORegistry.getSuperFanLevelsDAO().saveOrUpdate(level);
				
				List<SuperFanLevels> levels = QuizDAORegistry.getSuperFanLevelsDAO().getAllActiveSuperFanLevels(filter, "ACTIVE");
				responseDTO.setLevels(levels);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(levels.size()));
				responseDTO.setStatus(1);
				responseDTO.setMessage(msg);
				return responseDTO;
				
			}else if(action.equalsIgnoreCase("DELETE")){
				if(levelIdStr==null || levelIdStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Super fan Level id not found.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer levelId = 0;
				try {
					levelId = Integer.parseInt(levelIdStr);
				} catch (Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Super fan Level id is invalid.");
					responseDTO.setError(error);
					return responseDTO;
				}
				SuperFanLevels level = QuizDAORegistry.getSuperFanLevelsDAO().get(levelId);
				if(level == null){
					responseDTO.setStatus(0);
					error.setDescription("Super fan level configuration not found with given identifier.");
					responseDTO.setError(error);
					return responseDTO;
				}
				level.setUpdatedBy(userName);
				level.setUpdatedDate(new Date());
				level.setStatus("DELETED");
				QuizDAORegistry.getSuperFanLevelsDAO().saveOrUpdate(level);
				
				msg = "Selected Super fan Level configuration deleted successfully.";
				
				List<SuperFanLevels> levels = QuizDAORegistry.getSuperFanLevelsDAO().getAllActiveSuperFanLevels(filter, "ACTIVE");
				responseDTO.setLevels(levels);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(levels.size()));
				responseDTO.setStatus(1);
				responseDTO.setMessage(msg);
				return responseDTO;
			}
			responseDTO.setStatus(0);
			responseDTO.setMessage("Could not able to identify action.");
			return responseDTO;
			
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while fetching super fan levels.");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}
	

}
