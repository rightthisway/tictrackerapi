package com.rtw.tracker.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.rtw.tmat.utils.Error;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.Artist;
import com.rtw.tracker.datas.ArtistImage;
import com.rtw.tracker.datas.ArtistImageAudit;
import com.rtw.tracker.datas.ChildCategory;
import com.rtw.tracker.datas.ChildCategoryImage;
import com.rtw.tracker.datas.ChildCategoryImageAudit;
import com.rtw.tracker.datas.FantasyGrandChildCategory;
import com.rtw.tracker.datas.FantasyGrandChildCategoryImage;
import com.rtw.tracker.datas.GrandChildCategory;
import com.rtw.tracker.datas.GrandChildCategoryImage;
import com.rtw.tracker.datas.GrandChildCategoryImageAudit;
import com.rtw.tracker.datas.LoyalFanParentCategoryImage;
import com.rtw.tracker.datas.ParentCategory;
import com.rtw.tracker.datas.ParentCategoryImage;
import com.rtw.tracker.datas.ParentCategoryImageAudit;
import com.rtw.tracker.datas.PopularArtist;
import com.rtw.tracker.datas.Product;
import com.rtw.tracker.pojos.ArtistImageDTO;
import com.rtw.tracker.pojos.ChildCategoryImagesDTO;
import com.rtw.tracker.pojos.FantasyGrandChildCategoryImagesDTO;
import com.rtw.tracker.pojos.GenericResponseDTO;
import com.rtw.tracker.pojos.GrandChildCategoryImagesDTO;
import com.rtw.tracker.pojos.LoyalFanParentCategoryImagesDTO;
import com.rtw.tracker.pojos.ParentCategoryImagesDTO;
import com.rtw.tracker.pojos.PopularArtistDTO;
import com.rtw.tracker.utils.Constants;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.GridHeaderFiltersUtil;
import com.rtw.tracker.utils.PaginationUtil;
import com.rtw.tracker.utils.URLUtil;
import com.rtw.tracker.utils.Util;

@Controller
public class EventCategoryImageController {
	

	private SharedProperty sharedProperty;
	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}

	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}
	
	@RequestMapping({"/ArtistImages"})
	public PopularArtistDTO artistImages(HttpServletRequest request, HttpServletResponse response){
		PopularArtistDTO popularArtistDTO = new PopularArtistDTO();
		Error error = new Error();
		//model.addAttribute("popularArtistDTO", popularArtistDTO);
		
		try{
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String sortingString = request.getParameter("sortingString");
			
			Integer count = 0;
			GridHeaderFilters filter = null;
			try {
				filter = GridHeaderFiltersUtil.getManageArtistSearchHeaderFilters(headerFilter+sortingString);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				popularArtistDTO.setError(error);
				popularArtistDTO.setStatus(0);
				return popularArtistDTO;
			}
			
			List<PopularArtist> artistList = DAORegistry.getQueryManagerDAO().getAllArtist(filter, pageNo);
			count = DAORegistry.getQueryManagerDAO().getAllArtistCount(filter);
			
			if(artistList == null || artistList.size() < 0){
				popularArtistDTO.setMessage("There is no Artist Found.");
			}
			
			popularArtistDTO.setPopularArtistCustomDTO(Util.getAllArtistArray(artistList));
			popularArtistDTO.setPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));					
			popularArtistDTO.setStatus(1);
		
			/*
			String msg = request.getParameter("msg");			
			String grandChildStr = request.getParameter("grandChild");
			String[] artistStr = request.getParameterValues("artists");
			String childStr = request.getParameter("child");
			
			String selectedArtist = "";
			String artistsCheckAll = request.getParameter("artistsCheckAll");
			boolean  isAllArtist = ((artistsCheckAll==null || !artistsCheckAll.equalsIgnoreCase("on") )?false:true);
			String selectedValue = "";
			String selectedOption = "";
			List<Artist> artists = null;
			
			if(grandChildStr!=null && !grandChildStr.isEmpty()){
				GrandChildCategory grandChildCategory = DAORegistry.getGrandChildCategoryDAO().get(Integer.parseInt(grandChildStr));
				artists = DAORegistry.getQueryManagerDAO().getAllActiveByGrandchildOrChildAndDisplayOnSearch(Integer.parseInt(grandChildStr),null);
				selectedOption="GrandChild";
				selectedValue= grandChildCategory.getName();
			}else if(childStr!=null && !childStr.isEmpty()){
				ChildCategory childCategory = DAORegistry.getChildCategoryDAO().get(Integer.parseInt(childStr));
				artists = DAORegistry.getQueryManagerDAO().getAllActiveByGrandchildOrChildAndDisplayOnSearch(null,Integer.parseInt(childStr));
				selectedOption="Child";
				selectedValue= childCategory.getName();
			}
			
			
			String action = request.getParameter("action");
			if(action != null) {
				String username = SecurityContextHolder.getContext().getAuthentication().getName();
				Date now = new Date();
				if(action.equals("update")) {
					MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
					List<ArtistImage> artistImageSaveList = new ArrayList<ArtistImage>();
					List<ArtistImage> artistImageUpdateList = new ArrayList<ArtistImage>();
					List<ArtistImageAudit> artistImageAudits = new ArrayList<ArtistImageAudit>();
					
					Map<String, String[]> requestParams = request.getParameterMap();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						if(key.contains("checkbox_")){
							Integer artistId = Integer.parseInt(key.replace("checkbox_", ""));
							//String idStr = request.getParameter("id_"+artistId);
							Artist artist = DAORegistry.getArtistDAO().get(artistId);
							ArtistImage artistImage = DAORegistry.getArtistImageDAO().getArtistImageByArtistId(artistId);
							if(artistImage == null) {
								artistImage = new ArtistImage();
							}
							artistImage.setArtist(artist);
							artistImage.setLastUpdated(now);
							artistImage.setLastUpdatedBy(username);
							artistImage.setStatus("ACTIVE");
							
							String fileRequired = request.getParameter("fileRequired_"+artistId);
							if(fileRequired != null && fileRequired.equals("Y")) {
								MultipartFile file = multipartRequest.getFile("file_"+artistId);
								if (file == null || file.getSize() == 0) {
									continue;
								}
								File newFile = null;
								if(artistImage.getImageFileUrl() != null) {
									String fullFileName = URLUtil.ARTIST_DIRECTORY+artistImage.getImageFileUrl();
									newFile = new File(fullFileName);
									if (newFile.exists()) {
										newFile.delete();
									}
								}
								String ext = FilenameUtils.getExtension(file.getOriginalFilename());
								String fileName = "ArtistImage_" +artistId+"."+ext;
								String fullFileName = URLUtil.ARTIST_DIRECTORY+fileName;

								newFile = new File(fullFileName);
								if (!newFile.exists()) {
									newFile.createNewFile();
								}
								BufferedOutputStream stream = new BufferedOutputStream(
										new FileOutputStream(newFile));
						        FileCopyUtils.copy(file.getInputStream(), stream);
								stream.close();
								artistImage.setImageFileUrl(fileName);
							}
							String auditAction="UPDATE";
							if(artistImage.getId() == null) {
								artistImageSaveList.add(artistImage);
								auditAction="CREATE";
							} else {
								artistImageUpdateList.add(artistImage);
							}
							ArtistImageAudit audit = new ArtistImageAudit(artistImage);
							audit.setAction(auditAction);
							artistImageAudits.add(audit);
						}
					}
					DAORegistry.getArtistImageDAO().saveAll(artistImageSaveList);
					DAORegistry.getArtistImageDAO().updateAll(artistImageUpdateList);
					DAORegistry.getArtistImageAuditDAO().saveAll(artistImageAudits);
					
					map.put("info", "Artist/Team Image Saved Successfully.");
				} else if(action.equals("delete")) {
					List<ArtistImage> artistImageList = new ArrayList<ArtistImage>();
					List<ArtistImageAudit> artistImageAudits = new ArrayList<ArtistImageAudit>();
					
					Map<String, String[]> requestParams = request.getParameterMap();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						if(key.contains("checkbox_")){
							Integer artistId = Integer.parseInt(key.replace("checkbox_", ""));
							String idStr = request.getParameter("id_"+artistId);
							if(idStr == null || idStr.isEmpty()) {
								continue;
							}
							Integer id=Integer.parseInt(idStr);
							ArtistImage artistImage = DAORegistry.getArtistImageDAO().get(id);
							artistImage.setLastUpdated(now);
							artistImage.setLastUpdatedBy(username);
							artistImage.setStatus("DELETED");
							
							String fullFileName = URLUtil.ARTIST_DIRECTORY+artistImage.getImageFileUrl();
							File newFile = new File(fullFileName);
							if (newFile.exists()) {
								newFile.delete();
							}
							artistImageList.add(artistImage);
							
							ArtistImageAudit audit = new ArtistImageAudit(artistImage);
							audit.setAction("DELETE");
							artistImageAudits.add(audit);
						}
					}
					DAORegistry.getArtistImageDAO().deleteAll(artistImageList);
					DAORegistry.getArtistImageAuditDAO().saveAll(artistImageAudits);
					map.put("info", "Artist/Team Image Deleted Successfully.");
				}
					
			}
			
			Map<Integer,Artist> artistMap = new HashMap<Integer, Artist>();
			if(artists != null) {
				for (Artist artist : artists) {
					artistMap.put(artist.getId(),artist);
				}
			}
			List<ArtistImage> artistImageList = new ArrayList<ArtistImage>();
			if(artistStr!=null){
				for (String artistId : artistStr) {
					try{
						selectedArtist += artistId + ",";
						Artist artist = artistMap.get(Integer.parseInt(artistId));
						ArtistImage artistImage = DAORegistry.getArtistImageDAO().getArtistImageByArtistId(artist.getId());
						 if(artistImage == null){
							 artistImage = new ArtistImage();
							 artistImage.setArtist(artist);
						 }
						 artistImageList.add(artistImage);
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}else{
				if(msg!=null && !msg.isEmpty()){
					popularArtistDTO.setMessage(msg);
					popularArtistDTO.setStatus(1);
				}else{
					String pageNo = request.getParameter("pageNo");
					String headerFilter = request.getParameter("headerFilter");
					GridHeaderFilters filter = new GridHeaderFilters();
					if(headerFilter != null && !headerFilter.isEmpty()){
						filter = GridHeaderFiltersUtil.getManageArtistSearchHeaderFilters(headerFilter);
					}else{
						filter = new GridHeaderFilters();
					}*/
					
				/*}
			}
			
			map.put("artists", artists);
			map.addAttribute("artistImageList", artistImageList);
			map.addAttribute("eventsCheckAll", isAllArtist);
			map.addAttribute("artistStr", selectedArtist);
			map.addAttribute("child", childStr);
			map.addAttribute("grandChild", grandChildStr);
			map.addAttribute("selectedValue", selectedValue);
			map.addAttribute("selectedOption", selectedOption);
			*/
		}catch(Exception e){
			e.printStackTrace();
			System.err.println(e.getLocalizedMessage());			
			error.setDescription("There is something wrong..Please Try Again.");
			popularArtistDTO.setError(error);
			popularArtistDTO.setStatus(0);
		}
		return popularArtistDTO;
	}
	
	@RequestMapping({"/GetArtistImages"})
	public PopularArtistDTO getArtistImages(HttpServletRequest request, HttpServletResponse response,HttpSession session, Model model){
		PopularArtistDTO popularArtistDTO = new PopularArtistDTO();
		Error error = new Error();
		model.addAttribute("popularArtistDTO", popularArtistDTO);
		
		try{
			//JSONObject returnObject = new JSONObject();			
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			Integer count = 0;
			
			if(StringUtils.isEmpty(pageNo)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				popularArtistDTO.setError(error);
				popularArtistDTO.setStatus(0);
				return popularArtistDTO;
			}
			
			GridHeaderFilters filter = null;
			try {
				filter = GridHeaderFiltersUtil.getManageArtistSearchHeaderFilters(headerFilter);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				popularArtistDTO.setError(error);
				popularArtistDTO.setStatus(0);
				return popularArtistDTO;
			}
			
			List<PopularArtist> artistList = DAORegistry.getQueryManagerDAO().getAllArtist(filter, pageNo);
			count = DAORegistry.getQueryManagerDAO().getAllArtistCount(filter);
			
			popularArtistDTO.setPopularArtistCustomDTO(Util.getAllArtistArray(artistList));
			popularArtistDTO.setPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));			
			popularArtistDTO.setStatus(1);
			
			/*returnObject.put("artistList", JsonWrapperUtil.getAllArtistArray(artistList));
			returnObject.put("artistPagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));
			IOUtils.write(returnObject.toString().getBytes(),response.getOutputStream());;*/
			
		}catch(Exception e){
			e.printStackTrace();
			System.err.println(e.getLocalizedMessage());			
			error.setDescription("There is something wrong..Please Try Again.");
			popularArtistDTO.setError(error);
			popularArtistDTO.setStatus(0);
		}
		return popularArtistDTO;
	}
	
	@RequestMapping({"/ArtistImagesExportToExcel"})
	public void artistImagesToExport(HttpServletRequest request, HttpServletResponse response){
		
		String headerFilter = request.getParameter("headerFilter");
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getManageArtistSearchHeaderFilters(headerFilter);
			List<PopularArtist> artistList = DAORegistry.getQueryManagerDAO().getAllArtistToExport(filter);
			
			if(artistList!=null && !artistList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Artist_Images");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Artist Id");
				header.createCell(1).setCellValue("Artist Name");
				header.createCell(2).setCellValue("Grand Child Category");
				header.createCell(3).setCellValue("Child Category");
				header.createCell(4).setCellValue("Parent Category");
				header.createCell(5).setCellValue("Image File URL");
				header.createCell(6).setCellValue("Image Uploaded");
				Integer i=1;
				for(PopularArtist artist : artistList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(artist.getArtistId());
					row.createCell(1).setCellValue(artist.getArtistName());
					row.createCell(2).setCellValue(artist.getGrandChildCategoryName()!=null?artist.getGrandChildCategoryName():"");
					row.createCell(3).setCellValue(artist.getChildCategoryName()!=null?artist.getChildCategoryName():"");
					row.createCell(4).setCellValue(artist.getParentType()!=null?artist.getParentType():"");
					row.createCell(5).setCellValue(artist.getImageFileUrl()!=null?artist.getImageFileUrl():"");
					if(artist.getImageFileUrl() != null && artist.getImageFileUrl() != ""){
						row.createCell(6).setCellValue("Yes");
					}else{
						row.createCell(6).setCellValue("No");
					}
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Artist_Images.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping({"/ArtistExportToExcel"})
	public void artistToExport(HttpServletRequest request, HttpServletResponse response){
		
		String headerFilter = request.getParameter("headerFilter");
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getManageArtistSearchHeaderFilters(headerFilter);
			List<PopularArtist> artistList = DAORegistry.getQueryManagerDAO().getAllArtistToExport(filter);
			
			if(artistList!=null && !artistList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Artist");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Artist Id");
				header.createCell(1).setCellValue("Artist Name");
				header.createCell(2).setCellValue("Grand Child Category");
				header.createCell(3).setCellValue("Child Category");
				header.createCell(4).setCellValue("Parent Category");
				Integer i=1;
				for(PopularArtist artist : artistList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(artist.getArtistId());
					row.createCell(1).setCellValue(artist.getArtistName());
					row.createCell(2).setCellValue(artist.getGrandChildCategoryName()!=null?artist.getGrandChildCategoryName():"");
					row.createCell(3).setCellValue(artist.getChildCategoryName()!=null?artist.getChildCategoryName():"");
					row.createCell(4).setCellValue(artist.getParentType()!=null?artist.getParentType():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Artist.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping({"/PopularArtistExportToExcel"})
	public void popularArtistToExport(HttpServletRequest request, HttpServletResponse response){
		
		String headerFilter = request.getParameter("headerFilter");
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getPopularArtistSearchHeaderFilters(headerFilter);
			Product product = DAORegistry.getProductDAO().getProductByName(Constants.Reward_The_Fan_Product);
			Collection<PopularArtist> popArtists = DAORegistry.getQueryManagerDAO().getAllActivePopularArtistByProductIdToExport(product.getId(),filter);
			Collection<PopularArtist> popularArtists = new ArrayList<PopularArtist>();
			Set<Integer> artistIds = new HashSet<Integer>();
			
			for(PopularArtist popArtist: popArtists){				
				if(artistIds.add(popArtist.getArtistId())){
					popularArtists.add(popArtist);
				}
			}
			
			if(popularArtists!=null && !popularArtists.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Popular_Artist");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Artist Id");
				header.createCell(1).setCellValue("Artist Name");
				header.createCell(2).setCellValue("Grand Child Category");
				header.createCell(3).setCellValue("Child Category");
				header.createCell(4).setCellValue("Parent Category");
				header.createCell(5).setCellValue("Created By");
				header.createCell(6).setCellValue("Created Date");
				Integer i=1;
				for(PopularArtist artist : popularArtists){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(artist.getArtistId());
					row.createCell(1).setCellValue(artist.getArtistName());
					row.createCell(2).setCellValue(artist.getGrandChildCategoryName()!=null?artist.getGrandChildCategoryName():"");
					row.createCell(3).setCellValue(artist.getChildCategoryName()!=null?artist.getChildCategoryName():"");
					row.createCell(4).setCellValue(artist.getParentType()!=null?artist.getParentType():"");
					row.createCell(5).setCellValue(artist.getCreatedBy()!=null?artist.getCreatedBy():"");
					row.createCell(6).setCellValue(artist.getCreatedDateStr()!=null?artist.getCreatedDateStr():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Popular_Artist.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/*@RequestMapping({"/GetArtistsForCustomer"})
	public void getArtistsForCustomer(HttpServletRequest request, HttpServletResponse response,HttpSession session){
		try{
			JSONObject returnObject = new JSONObject();
			Integer count = 0;
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String customerIdStr = request.getParameter("customerId");
			System.out.println("headerFilters   :   "+headerFilter);
			GridHeaderFilters filter = GridHeaderFiltersUtil.getManageArtistSearchHeaderFilters(headerFilter);
			List<PopularArtist> artistList = DAORegistry.getQueryManagerDAO().getAllArtist(filter, pageNo);
			count = DAORegistry.getQueryManagerDAO().getAllArtistCount(filter);
						  
			if(customerIdStr != null && !customerIdStr.isEmpty()){
				Integer customerId = Integer.parseInt(customerIdStr);

				Map<String, String> map = Util.getParameterMap(request);
			    map.put("customerId", customerIdStr);
			    map.put("productType", com.rtw.tmat.utils.Constants.PRODUCTTYPE);
			    map.put("platForm", com.rtw.tmat.utils.Constants.PLATFORM);
			    String data = Util.getObject(map,sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.GET_LOYALFAN_STATUS);
			    Gson gson = new Gson();  
				JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
				LoyalFanStatus loyalFans = gson.fromJson(((JsonObject)jsonObject.get("loyalFanStatus")), LoyalFanStatus.class);
				
				CustomerLoyalFan loyalFans = DAORegistry.getCustomerLoyalFanDAO().getActiveLoyalFanByCustomerId(customerId);
				JSONObject loyalFanObj = new JSONObject();				
				if(loyalFans != null){
					if(loyalFans.getLoyalFanType() != null && loyalFans.getLoyalFanType().equalsIgnoreCase("SPORTS")){
						loyalFanObj.put("artistName", loyalFans.getLoyalName());
					}else{
						loyalFanObj.put("cityName", loyalFans.getLoyalName()!=null?loyalFans.getLoyalName()+", "+loyalFans.getCountry():"");
					}
					loyalFanObj.put("categoryName", loyalFans.getLoyalFanType()!=null?loyalFans.getLoyalFanType():"");
					if(loyalFans.getTicketPurchased() != null && loyalFans.getTicketPurchased()){
						loyalFanObj.put("ticketsPurchased", "Yes");
					}else{
						loyalFanObj.put("ticketsPurchased", "No");
					}
					loyalFanObj.put("startDate", loyalFans.getStartDateStr()!=null?loyalFans.getStartDateStr():"");
					loyalFanObj.put("endDate", loyalFans.getEndDateStr()!=null?loyalFans.getEndDateStr():"");
				}
				returnObject.put("loyalFanInfo", loyalFanObj);
			}
			
			returnObject.put("artistList", JsonWrapperUtil.getAllArtistArray(artistList));
			returnObject.put("artistPagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));
			response.setHeader("Content-type","application/json");
			IOUtils.write(returnObject.toString().getBytes(),response.getOutputStream());;
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	@RequestMapping({"/updateArtistImages"})
	public GenericResponseDTO updateArtistImages(Model model,@ModelAttribute("artistImage")ArtistImage artistImage, BindingResult result, HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		model.addAttribute("genericResponseDTO", genericResponseDTO);
		model.addAttribute("artistImage", "");
		
		String returnMessage = null;
		try{
			String action = request.getParameter("action");
			if(action != null) {
				String username = request.getParameter("userName");
				String artistIdStr = request.getParameter("artistIdStr");
				Date now = new Date();
				if(action.equals("update")) {
					String auditAction="";
					MultipartFile file = artistImage.getFile();
					String fileRequired = request.getParameter("fileRequired");
					List<ArtistImage> artistImageSaveList = new ArrayList<ArtistImage>();
					List<ArtistImage> artistImageUpdateList = new ArrayList<ArtistImage>();
					List<ArtistImageAudit> artistImageAudits = new ArrayList<ArtistImageAudit>();					
					
							Integer artistId = Integer.parseInt(artistIdStr);
							Artist artist = DAORegistry.getArtistDAO().get(artistId);
							
							artistImage.setArtist(artist);
							artistImage.setLastUpdated(now);
							artistImage.setLastUpdatedBy(username);
							artistImage.setStatus("ACTIVE");							
							
							if(fileRequired != null && fileRequired.equals("Y")) {
								
								File newFile = null;
								if(artistImage.getImageFileUrl() != null) {
									String fullFileName = URLUtil.ARTIST_DIRECTORY+artistImage.getImageFileUrl();
									newFile = new File(fullFileName);
									if (newFile.exists()) {
										newFile.delete();
									}
								}
								DAORegistry.getArtistImageDAO().saveOrUpdate(artistImage);
								
								String ext = FilenameUtils.getExtension(file.getOriginalFilename());
								String fileName = "ArtistImage_" +artistId+"."+ext;
								String fullFileName = URLUtil.ARTIST_DIRECTORY+fileName;

								newFile = new File(fullFileName);
								if (!newFile.exists()) {
									newFile.createNewFile();
								}
								System.out.println("file path...."+newFile.getPath());
								BufferedOutputStream stream = new BufferedOutputStream(
										new FileOutputStream(newFile));
						        FileCopyUtils.copy(file.getInputStream(), stream);
								stream.close();
								artistImage.setImageFileUrl(fileName);
								DAORegistry.getArtistImageDAO().updateArtistImageUrl(artistImage);
							}
							auditAction="UPDATE";
							if(artistImage.getId() == null) {
								artistImageSaveList.add(artistImage);
								auditAction="CREATE";
							} else {
								artistImageUpdateList.add(artistImage);
							}
							ArtistImageAudit audit = new ArtistImageAudit(artistImage);
							audit.setAction(auditAction);
							artistImageAudits.add(audit);
						//}
					//}
					//DAORegistry.getArtistImageDAO().saveAll(artistImageSaveList);
					//DAORegistry.getArtistImageDAO().updateAll(artistImageUpdateList);
					DAORegistry.getArtistImageAuditDAO().saveAll(artistImageAudits);

					returnMessage = "Artist/Team Image Saved Successfully.";
					
					//Tracking User Action
					String userActionMsg = "Artist/Team Image "+auditAction+" Successfully.";
					com.rtw.tmat.utils.Util.userActionAudit(request, artistId, userActionMsg);
				}
				else if(action.equals("delete")) {
					List<ArtistImage> artistImageList = new ArrayList<ArtistImage>();
					List<ArtistImageAudit> artistImageAudits = new ArrayList<ArtistImageAudit>();
					
					Integer artistId = Integer.parseInt(artistIdStr);
					ArtistImage artistImg = DAORegistry.getArtistImageDAO().getArtistImageByArtistId(artistId);
					
					artistImg.setLastUpdated(now);
					artistImg.setLastUpdatedBy(username);
					artistImg.setStatus("DELETED");
					
					String fullFileName = URLUtil.ARTIST_DIRECTORY+artistImg.getImageFileUrl();
					File newFile = new File(fullFileName);
					if (newFile.exists()) {
						newFile.delete();
					}
					artistImageList.add(artistImg);
					
					ArtistImageAudit audit = new ArtistImageAudit(artistImg);
					audit.setAction("DELETE");
					artistImageAudits.add(audit);
					
					DAORegistry.getArtistImageDAO().deleteAll(artistImageList);
					DAORegistry.getArtistImageAuditDAO().saveAll(artistImageAudits);
					returnMessage = "Artist/Team Image Deleted Successfully.";
					
					//Tracking User Action
					String userActionMsg = "Artist/Team Image DELETE Successfully.";					
					com.rtw.tmat.utils.Util.userActionAudit(request, artistId, userActionMsg);
				}
				genericResponseDTO.setMessage(returnMessage);
				genericResponseDTO.setStatus(1);
			}
		}catch(Exception e){
			e.printStackTrace();
			System.err.println(e.getMessage());
			error.setDescription("There is something wrong..Please Try Again.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
	@RequestMapping({"/GetArtistImagesForEdit"})
	public ArtistImageDTO getArtistImagesForEdit(HttpServletRequest request, HttpServletResponse response, Model model){
		ArtistImageDTO artistImageDTO = new ArtistImageDTO();
		Error error = new Error();
		model.addAttribute("artistImageDTO", artistImageDTO);
		
		try{
			String artistId = request.getParameter("artistId");
			if(StringUtils.isEmpty(artistId)){
				System.err.println("Please select Artist.");
				error.setDescription("Please select Artist.");
				artistImageDTO.setError(error);
				artistImageDTO.setStatus(0);
				return artistImageDTO;
			}
			
			ArtistImage artistImage = DAORegistry.getArtistImageDAO().getArtistImageByArtistId(Integer.parseInt(artistId));
			if(artistImage == null){				
				artistImageDTO.setMessage("There is No Image found in Selected Artist.");
			}
			artistImageDTO.setStatus(1);
			artistImageDTO.setArtistImage(artistImage);
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Please select Artist.");
			artistImageDTO.setError(error);
			artistImageDTO.setStatus(0);
			return artistImageDTO;
		}
		return artistImageDTO;
	}
	
	@RequestMapping({"/GrandChildCategoryImages"})
	public GrandChildCategoryImagesDTO getGrandChildCategoryImages(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		GrandChildCategoryImagesDTO grandChildCategoryImagesDTO = new GrandChildCategoryImagesDTO();
		Error error = new Error();
		model.addAttribute("grandChildCategoryImagesDTO",grandChildCategoryImagesDTO);
		
		try{
			/*
			String parentStr = request.getParameter("parent");
			String[] grandChildStr = request.getParameterValues("grandChilds");
			String childStr = request.getParameter("child");
			*/
			String msg = "";
			String param = request.getParameter("grandChildSearch");
			List<GrandChildCategory> grandChilds = new ArrayList<GrandChildCategory>();
			String selectedGrandChild = "";
			String artistsCheckAll = request.getParameter("artistsCheckAll");
			boolean isAllGrandChild = ((artistsCheckAll==null || !artistsCheckAll.equalsIgnoreCase("on") )?false:true);
			if(param != null && !param.isEmpty()){
				GrandChildCategory grandChild = DAORegistry.getGrandChildCategoryDAO().get(Integer.parseInt(param));
				grandChilds.add(grandChild);
			}else{
				grandChilds = DAORegistry.getGrandChildCategoryDAO().getGrandChildsByDisplayOnSearch();
			}
			 
			
			/*
			if(parentStr!=null && !parentStr.isEmpty()){
				ParentCategory parentCategory = DAORegistry.getParentCategoryDAO().get(Integer.parseInt(parentStr));
				grandChilds = DAORegistry.getGrandChildCategoryDAO().getAllGrandChildCategoryByParentCategory(Integer.parseInt(parentStr));
				selectedOption="Parent";
				selectedValue= parentCategory.getName();
			}else if(childStr!=null && !childStr.isEmpty()){
				ChildCategory childCategory = DAORegistry.getChildCategoryDAO().get(Integer.parseInt(childStr));
				grandChilds = DAORegistry.getGrandChildCategoryDAO().getAllGrandChildCategoryByChildCategory(Integer.parseInt(childStr));
				selectedOption="Child";
				selectedValue= childCategory.getName();
			}*/
			
			String action = request.getParameter("action");
			if(action != null) {
				String username = request.getParameter("userName");
				Date now = new Date();
				if(action.equals("update")) {
					MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
					List<GrandChildCategoryImage> grandChildImageSaveList = new ArrayList<GrandChildCategoryImage>();
					List<GrandChildCategoryImage> grandChildImageUpdateList = new ArrayList<GrandChildCategoryImage>();
					List<GrandChildCategoryImageAudit> grandChildImageAudits = new ArrayList<GrandChildCategoryImageAudit>();
					String auditAction = "";
					String grandChildIdStr = "";
					
					Map<String, String[]> requestParams = request.getParameterMap();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						if(key.contains("checkbox_")){
							Integer grandChildId = Integer.parseInt(key.replace("checkbox_", ""));
							grandChildIdStr += grandChildId+",";
							
							GrandChildCategory grandChildCategory = DAORegistry.getGrandChildCategoryDAO().get(grandChildId);
							List<GrandChildCategory> grandChildLists = DAORegistry.getGrandChildCategoryDAO().getAllGrandChildCategoriesByNameAndDisplayOnSearch(grandChildCategory.getName());
							for(GrandChildCategory grandChild : grandChildLists){
								GrandChildCategoryImage grandChildImage = DAORegistry.getGrandChildCategoryImageDAO().getGrandChildCategoryImageByGrandChildId(grandChild.getId());
								if(grandChildImage == null) {
									grandChildImage = new GrandChildCategoryImage();
								}
								grandChildImage.setGrandChildCategory(grandChild);
								grandChildImage.setLastUpdated(now);
								grandChildImage.setLastUpdatedBy(username);
								grandChildImage.setStatus("ACTIVE");
								
								String fileDeleted = request.getParameter("fileDeleted_"+grandChildId);
								if(fileDeleted != null && fileDeleted.equals("Y")) {
									File newFile = null;
									if(grandChildImage.getImageFileUrl() != null) {
										try{
											String fullFileNAme=URLUtil.GRANDCHILD_DIRECTORY+grandChildImage.getImageFileUrl();
											newFile = new File(fullFileNAme);
											if (newFile.exists()) {
												newFile.delete();
											}
										}catch(Exception e){
											e.printStackTrace();									
											error.setDescription("Error occured while retrieving Grand Child Category Image.");
											grandChildCategoryImagesDTO.setError(error);
											grandChildCategoryImagesDTO.setStatus(0);
											return grandChildCategoryImagesDTO;
										}
									}
									grandChildImage.setImageFileUrl(null);
								} else {
									MultipartFile file = multipartRequest.getFile("file_"+grandChildId);
									if (file != null && file.getSize() != 0) {
										File newFile = null;
										if(grandChildImage.getImageFileUrl() != null) {
											try{
												String fullFileNAme=URLUtil.GRANDCHILD_DIRECTORY+grandChildImage.getImageFileUrl();
												newFile = new File(fullFileNAme);
												if (newFile.exists()) {
													newFile.delete();
												}
											}catch(Exception e){
												e.printStackTrace();									
												error.setDescription("Error occured while retrieving Grand Child Category Image.");
												grandChildCategoryImagesDTO.setError(error);
												grandChildCategoryImagesDTO.setStatus(0);
												return grandChildCategoryImagesDTO;
											}
										}
										String ext = FilenameUtils.getExtension(file.getOriginalFilename());
										String fileName = "GrandChildCategoryImage_" +grandChild.getId()+"."+ext;
										String fullFileName = URLUtil.GRANDCHILD_DIRECTORY+fileName;
			
										newFile = new File(fullFileName);
										if (!newFile.exists()) {
											newFile.createNewFile();
										}
										BufferedOutputStream stream = new BufferedOutputStream(
												new FileOutputStream(newFile));
								        FileCopyUtils.copy(file.getInputStream(), stream);
										stream.close();
										grandChildImage.setImageFileUrl(fileName);
									}
								}
								
								/*String circleFileDeleted = request.getParameter("circleFileDeleted_"+grandChildId);
								if(circleFileDeleted != null && circleFileDeleted.equals("Y")) {
									File newFile = null;
									if(grandChildImage.getCircleImageFileUrl() != null) {
										String fullFileNAme=URLUtil.GRANDCHILD_DIRECTORY+grandChildImage.getCircleImageFileUrl();
										newFile = new File(fullFileNAme);
										if (newFile.exists()) {
											newFile.delete();
										}
									}
									grandChildImage.setCircleImageFileUrl(null);
								} else {
									MultipartFile circleFile = multipartRequest.getFile("circleFile_"+grandChildId);
									if (circleFile != null && circleFile.getSize() != 0) {
										File newCircleFile = null;
										if(grandChildImage.getCircleImageFileUrl() != null) {
											String fullFileNAme=URLUtil.GRANDCHILD_DIRECTORY+grandChildImage.getCircleImageFileUrl();
											newCircleFile = new File(fullFileNAme);
											if (newCircleFile.exists()) {
												newCircleFile.delete();
											}
										}
										String ext = FilenameUtils.getExtension(circleFile.getOriginalFilename());
										String fileName = "GrandChildCircleImage_" +grandChildId+"."+ext;
										String fullFileName = URLUtil.GRANDCHILD_DIRECTORY+fileName;
		
										newCircleFile = new File(fullFileName);
										if (!newCircleFile.exists()) {
											newCircleFile.createNewFile();
										}
										BufferedOutputStream stream = new BufferedOutputStream(
												new FileOutputStream(newCircleFile));
								        FileCopyUtils.copy(circleFile.getInputStream(), stream);
										stream.close();
										grandChildImage.setCircleImageFileUrl(fileName);
									}
								}*/
									
								auditAction="UPDATE";
								if(grandChildImage.getId() == null) {
									grandChildImageSaveList.add(grandChildImage);
									auditAction="CREATE";
								} else {
									grandChildImageUpdateList.add(grandChildImage);
								}
								GrandChildCategoryImageAudit audit = new GrandChildCategoryImageAudit(grandChildImage);
								audit.setAction(auditAction);
								grandChildImageAudits.add(audit);
							}
							
						}
					}
					DAORegistry.getGrandChildCategoryImageDAO().saveAll(grandChildImageSaveList);
					DAORegistry.getGrandChildCategoryImageDAO().updateAll(grandChildImageUpdateList);
					DAORegistry.getGrandChildCategoryImageAuditDAO().saveAll(grandChildImageAudits);
					
					msg = "GrandChild Category Image Saved Successfully.";
					action = "search";
					
					//Tracking User Action
					grandChildIdStr = grandChildIdStr.substring(0, grandChildIdStr.length()-1);
					String userActionMsg = "GrandChildCategory Image "+auditAction+" Successfully.GrandChildCategory Id's - "+grandChildIdStr;
					com.rtw.tmat.utils.Util.userActionAudit(request, null, userActionMsg);
					
				} else if(action.equals("delete")) {
					List<GrandChildCategoryImage> grandChildImageList = new ArrayList<GrandChildCategoryImage>();
					List<GrandChildCategoryImageAudit> grandChildImageAudits = new ArrayList<GrandChildCategoryImageAudit>();
					String grandChildIdStr = "";
					
					Map<String, String[]> requestParams = request.getParameterMap();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						if(key.contains("checkbox_")){
							Integer grandChildId = Integer.parseInt(key.replace("checkbox_", ""));
							grandChildIdStr += grandChildId+",";
							
							String idStr = request.getParameter("id_"+grandChildId);
							if(idStr == null || idStr.isEmpty()) {
								continue;
							}
							Integer id=Integer.parseInt(idStr);
							GrandChildCategoryImage grandChildImage1 = DAORegistry.getGrandChildCategoryImageDAO().get(id);
							GrandChildCategory category =  grandChildImage1.getGrandChildCategory();
							List<GrandChildCategory> grandChildLists = DAORegistry.getGrandChildCategoryDAO().getAllGrandChildCategoriesByNameAndDisplayOnSearch(category.getName());
							for(GrandChildCategory grandChild : grandChildLists){
								GrandChildCategoryImage grandChildImage = DAORegistry.getGrandChildCategoryImageDAO().getGrandChildCategoryImageByGrandChildId(grandChild.getId());
								if(grandChildImage==null){
									continue;
								}
								grandChildImage.setLastUpdated(now);
								grandChildImage.setLastUpdatedBy(username);
								grandChildImage.setStatus("DELETED");
								
								try{
									String fullFileNAme=URLUtil.GRANDCHILD_DIRECTORY+grandChildImage.getImageFileUrl();
									File newFile = new File(fullFileNAme);
									if (newFile.exists()) {
										newFile.delete();
									}
									String fullCircleFileNAme=URLUtil.GRANDCHILD_DIRECTORY+grandChildImage.getCircleImageFileUrl();
									File newCircleFile = new File(fullCircleFileNAme);
									if (newCircleFile.exists()) {
										newCircleFile.delete();
									}
								}catch(Exception e){
									e.printStackTrace();									
									error.setDescription("Error occured while deleting Grand Child Category Image.");
									grandChildCategoryImagesDTO.setError(error);
									grandChildCategoryImagesDTO.setStatus(0);
									return grandChildCategoryImagesDTO;
								}
								grandChildImageList.add(grandChildImage);
								
								GrandChildCategoryImageAudit audit = new GrandChildCategoryImageAudit(grandChildImage);
								audit.setAction("DELETE");
								grandChildImageAudits.add(audit);
							}
						}
					}
					DAORegistry.getGrandChildCategoryImageDAO().deleteAll(grandChildImageList);
					DAORegistry.getGrandChildCategoryImageAuditDAO().saveAll(grandChildImageAudits);
					msg = "GrandChild Category Image Deleted Successfully.";
					action = "search";
					
					//Tracking User Action
					grandChildIdStr = grandChildIdStr.substring(0, grandChildIdStr.length()-1);
					String userActionMsg = "GrandChildCategory Image DELETE Successfully.GrandChildCategory Id's - "+grandChildIdStr;
					com.rtw.tmat.utils.Util.userActionAudit(request, null, userActionMsg);
				}
			}
			Map<Integer,GrandChildCategory> grandChildMap = new HashMap<Integer, GrandChildCategory>();
			if(grandChilds != null) {
				for (GrandChildCategory grandChild : grandChilds) {
					grandChildMap.put(grandChild.getId(),grandChild);
				}
			}
			List<GrandChildCategoryImage> grandChildImageList = new ArrayList<GrandChildCategoryImage>();
			if(grandChilds!=null){
				for (GrandChildCategory grandChildId : grandChilds) {
					try{
						selectedGrandChild += grandChildId + ",";
						GrandChildCategory grandChild = grandChildMap.get(grandChildId.getId());
						GrandChildCategoryImage grandChildImage = DAORegistry.getGrandChildCategoryImageDAO().getGrandChildCategoryImageByGrandChildId(grandChild.getId());
						 if(grandChildImage == null){
							 grandChildImage = new GrandChildCategoryImage();
							 grandChildImage.setGrandChildCategory(grandChild);
						 }
						 grandChildImageList.add(grandChildImage);
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			
			List<GrandChildCategory> list = DAORegistry.getGrandChildCategoryDAO().getGrandChildsByDisplayOnSearch();
			List<GrandChildCategory> grandChildList = new ArrayList<GrandChildCategory>();
			List<String> cateNames = new ArrayList<String>();
			for(GrandChildCategory cat : list){
				if(!cateNames.contains(cat.getName())){
					grandChildList.add(cat);
				}
				cateNames.add(cat.getName());
			}

			grandChildCategoryImagesDTO.setGrandChilds(grandChilds);
			grandChildCategoryImagesDTO.setGrandChildsList(grandChildList);
			grandChildCategoryImagesDTO.setGrandChildImageList(grandChildImageList);
			grandChildCategoryImagesDTO.setGrandChildStr(selectedGrandChild);
			grandChildCategoryImagesDTO.setGrandChildsCheckAll(isAllGrandChild);
			grandChildCategoryImagesDTO.setProductName("Reward The Fan");
			grandChildCategoryImagesDTO.setStatus(1);
			grandChildCategoryImagesDTO.setMessage(msg);
			
		}catch(Exception e){
			System.err.println(e.getLocalizedMessage());
			e.printStackTrace();
			error.setDescription("Error occured while fetching/updating Grand Child Category Image.");
			grandChildCategoryImagesDTO.setError(error);
			grandChildCategoryImagesDTO.setStatus(0);
		}
		return grandChildCategoryImagesDTO;
	}
	
	
	@RequestMapping({"/FantasyGrandChildCategoryImages"})
	public FantasyGrandChildCategoryImagesDTO getFantasyGrandChildCategoryImages(HttpServletRequest request, HttpServletResponse response, Model model){
		FantasyGrandChildCategoryImagesDTO fantasyGrandChildCategoryImagesDTO = new FantasyGrandChildCategoryImagesDTO();
		Error error = new Error();
		model.addAttribute("fantasyGrandChildCategoryImagesDTO", fantasyGrandChildCategoryImagesDTO);
		
		try{
			String msg = "";
			String param = request.getParameter("grandChildSearch");
			List<FantasyGrandChildCategory> grandChilds = new ArrayList<FantasyGrandChildCategory>();
			String selectedGrandChild = "";
			String artistsCheckAll = request.getParameter("artistsCheckAll");
			boolean  isAllGrandChild = ((artistsCheckAll==null || !artistsCheckAll.equalsIgnoreCase("on") )?false:true);
			String selectedValue = "";
			String selectedOption = "";
			if(param != null && !param.isEmpty()){
				FantasyGrandChildCategory grandChild = DAORegistry.getFantasyGrandChildCategoryDAO().get(Integer.parseInt(param));
				grandChilds.add(grandChild);
			}else{
				grandChilds = DAORegistry.getFantasyGrandChildCategoryDAO().getAllExceptNameAsCollege();
			}
			
			String action = request.getParameter("action");
			if(action != null) {
				String username = request.getParameter("userName");
				Date now = new Date();
				if(action.equals("update")) {
					MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
					List<FantasyGrandChildCategoryImage> grandChildImageSaveList = new ArrayList<FantasyGrandChildCategoryImage>();
					List<FantasyGrandChildCategoryImage> grandChildImageUpdateList = new ArrayList<FantasyGrandChildCategoryImage>();
					String auditAction = "";
					String fantasyGrandChildCategoryIdStr = "";
					
					Map<String, String[]> requestParams = request.getParameterMap();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						if(key.contains("checkbox_")){
							Integer grandChildId = Integer.parseInt(key.replace("checkbox_", ""));
							fantasyGrandChildCategoryIdStr += grandChildId+",";
							
							String imageName = request.getParameter("grandChildImageName_"+grandChildId);
							String visibleVenue = request.getParameter("showVenueInfo_"+grandChildId);
							String venueInfo = request.getParameter("venueInfo_"+grandChildId);
							FantasyGrandChildCategory grandChildCategory = DAORegistry.getFantasyGrandChildCategoryDAO().get(grandChildId);
							//List<GrandChildCategory> grandChildLists = DAORegistry.getGrandChildCategoryDAO().getAllGrandChildCategoriesByNameAndDisplayOnSearch(grandChildCategory.getName());
							//for(GrandChildCategory grandChild : grandChildLists){
								FantasyGrandChildCategoryImage grandChildImage = DAORegistry.getFantasyGrandChildCategoryImageDAO().getFantasyGrandChildCategoryImageByGrandChildId(grandChildCategory.getId());
								if(grandChildImage == null) {
									grandChildImage = new FantasyGrandChildCategoryImage();
								}
								grandChildImage.setGrandChildCategory(grandChildCategory);
								grandChildImage.setLastUpdated(now);
								grandChildImage.setLastUpdatedBy(username);
								grandChildImage.setStatus("ACTIVE");
								grandChildImage.setImageName(imageName);
								if(visibleVenue.equalsIgnoreCase("Yes")){
									grandChildImage.setShowVenueInfo(true);
								}else{
									grandChildImage.setShowVenueInfo(false);
								}								
								grandChildImage.setVenueInfo(venueInfo);
								
								String fileDeleted = request.getParameter("fileDeleted_"+grandChildId);
								String mobileFileDeleted = request.getParameter("mobileFileDeleted_"+grandChildId);
								if(fileDeleted != null && fileDeleted.equals("Y")) {
									File newFile = null;
									if(grandChildImage.getImageFileUrl() != null) {
										try{
											String fullFileNAme=URLUtil.FANTASY_DIRECTORY+grandChildImage.getImageFileUrl();
											newFile = new File(fullFileNAme);
											if (newFile.exists()) {
												newFile.delete();
											}
										}catch(Exception e){
											e.printStackTrace();									
											error.setDescription("Error occured while retrieving Fantasy Grand Child Category Image.");
											fantasyGrandChildCategoryImagesDTO.setError(error);
											fantasyGrandChildCategoryImagesDTO.setStatus(0);
											return fantasyGrandChildCategoryImagesDTO;
										}
									}
									grandChildImage.setImageFileUrl(null);
								}
								else if(mobileFileDeleted != null && mobileFileDeleted.equals("Y")){
									File newFile = null;								
									if(grandChildImage.getMobileImageFileUrl() != null) {
										try{
											String fullFileNAme=URLUtil.FANTASY_DIRECTORY+grandChildImage.getMobileImageFileUrl();
											newFile = new File(fullFileNAme);
											if (newFile.exists()) {
												newFile.delete();
											}
										}catch(Exception e){
											e.printStackTrace();									
											error.setDescription("Error occured while retrieving Fantasy Grand Child Category Mobile Image.");
											fantasyGrandChildCategoryImagesDTO.setError(error);
											fantasyGrandChildCategoryImagesDTO.setStatus(0);
											return fantasyGrandChildCategoryImagesDTO;
										}
									}
									grandChildImage.setMobileImageFileUrl(null);
								} else {
									MultipartFile file = multipartRequest.getFile("file_"+grandChildId);
									if (file != null && file.getSize() != 0) {
										File newFile = null;
										if(grandChildImage.getImageFileUrl() != null) {
											try{
												String fullFileNAme=URLUtil.FANTASY_DIRECTORY+grandChildImage.getImageFileUrl();
												newFile = new File(fullFileNAme);
												if (newFile.exists()) {
													newFile.delete();
												}
											}catch(Exception e){
												e.printStackTrace();									
												error.setDescription("Error occured while retrieving Fantasy Grand Child Category Image.");
												fantasyGrandChildCategoryImagesDTO.setError(error);
												fantasyGrandChildCategoryImagesDTO.setStatus(0);
												return fantasyGrandChildCategoryImagesDTO;
											}
										}
										String ext = FilenameUtils.getExtension(file.getOriginalFilename());
										String fileName = "GrandChildCategoryImage_" +grandChildCategory.getId()+"."+ext;
										String fullFileName = URLUtil.FANTASY_DIRECTORY+fileName;
			
										newFile = new File(fullFileName);
										if (!newFile.exists()) {
											newFile.createNewFile();
										}
										BufferedOutputStream stream = new BufferedOutputStream(
												new FileOutputStream(newFile));
								        FileCopyUtils.copy(file.getInputStream(), stream);
										stream.close();
										grandChildImage.setImageFileUrl(fileName);
									}
									
									MultipartFile mobileFile = multipartRequest.getFile("mobileFile_"+grandChildId);
									if (mobileFile != null && mobileFile.getSize() != 0) {
										File newFile = null;
										if(grandChildImage.getMobileImageFileUrl() != null) {
											try{
												String fullFileNAme=URLUtil.FANTASY_DIRECTORY+grandChildImage.getMobileImageFileUrl();
												newFile = new File(fullFileNAme);
												if (newFile.exists()) {
													newFile.delete();
												}
											}catch(Exception e){
												e.printStackTrace();									
												error.setDescription("Error occured while retrieving Fantasy Grand Child Category Mobile Image.");
												fantasyGrandChildCategoryImagesDTO.setError(error);
												fantasyGrandChildCategoryImagesDTO.setStatus(0);
												return fantasyGrandChildCategoryImagesDTO;
											}
										}
										String ext = FilenameUtils.getExtension(mobileFile.getOriginalFilename());
										String fileName = "GrandChildCategoryMobileImage_" +grandChildCategory.getId()+"."+ext;
										String fullFileName = URLUtil.FANTASY_DIRECTORY+fileName;
			
										newFile = new File(fullFileName);
										if (!newFile.exists()) {
											newFile.createNewFile();
										}
										BufferedOutputStream stream = new BufferedOutputStream(
												new FileOutputStream(newFile));
								        FileCopyUtils.copy(mobileFile.getInputStream(), stream);
										stream.close();
										grandChildImage.setMobileImageFileUrl(fileName);
									}
								}
									
								auditAction="UPDATE";
								if(grandChildImage.getId() == null) {
									grandChildImageSaveList.add(grandChildImage);
									auditAction="CREATE";
								} else {
									grandChildImageUpdateList.add(grandChildImage);
								}
								/*GrandChildCategoryImageAudit audit = new GrandChildCategoryImageAudit(grandChildImage);
								audit.setAction(auditAction);
								grandChildImageAudits.add(audit);*/
							//}
							
						}
					}
					DAORegistry.getFantasyGrandChildCategoryImageDAO().saveAll(grandChildImageSaveList);
					DAORegistry.getFantasyGrandChildCategoryImageDAO().updateAll(grandChildImageUpdateList);
					//DAORegistry.getGrandChildCategoryImageAuditDAO().saveAll(grandChildImageAudits);
					
					msg = "GrandChild Category Image Saved Successfully.";
					action = "search";
					
					//Tracking User Action
					fantasyGrandChildCategoryIdStr = fantasyGrandChildCategoryIdStr.substring(0, fantasyGrandChildCategoryIdStr.length()-1);
					String userActionMsg = "FantasyGrandChildCategory Image "+auditAction+" Successfully.FantasyGrandChildCategory Id's - "+fantasyGrandChildCategoryIdStr;
					com.rtw.tmat.utils.Util.userActionAudit(request, null, userActionMsg);
					
				} else if(action.equals("delete")) {
					List<FantasyGrandChildCategoryImage> grandChildImageList = new ArrayList<FantasyGrandChildCategoryImage>();
					//List<GrandChildCategoryImageAudit> grandChildImageAudits = new ArrayList<GrandChildCategoryImageAudit>();
					String fantasyGrandChildCategoryIdStr = "";
					
					Map<String, String[]> requestParams = request.getParameterMap();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						if(key.contains("checkbox_")){
							Integer grandChildId = Integer.parseInt(key.replace("checkbox_", ""));
							fantasyGrandChildCategoryIdStr += grandChildId+",";
							
							String idStr = request.getParameter("id_"+grandChildId);
							if(idStr == null || idStr.isEmpty()) {
								continue;
							}
							Integer id=Integer.parseInt(idStr);
							FantasyGrandChildCategoryImage grandChildImage1 = DAORegistry.getFantasyGrandChildCategoryImageDAO().get(id);
							FantasyGrandChildCategory category =  grandChildImage1.getGrandChildCategory();
							//List<GrandChildCategory> grandChildLists = DAORegistry.getGrandChildCategoryDAO().getAllGrandChildCategoriesByNameAndDisplayOnSearch(category.getName());
							//for(GrandChildCategory grandChild : grandChildLists){
							FantasyGrandChildCategoryImage grandChildImage = DAORegistry.getFantasyGrandChildCategoryImageDAO().getFantasyGrandChildCategoryImageByGrandChildId(category.getId());
							if(grandChildImage==null){
								continue;
							}
							grandChildImage.setLastUpdated(now);
							grandChildImage.setLastUpdatedBy(username);
							grandChildImage.setStatus("DELETED");
							
							try{
								String fullFileNAme=URLUtil.FANTASY_DIRECTORY+grandChildImage.getImageFileUrl();
								File newFile = new File(fullFileNAme);
								if (newFile.exists()) {
									newFile.delete();
								}
								String fullFileName1=URLUtil.FANTASY_DIRECTORY+grandChildImage.getMobileImageFileUrl();
								File newFile1 = new File(fullFileName1);
								if (newFile1.exists()) {
									newFile1.delete();
								}
							}catch(Exception e){
								e.printStackTrace();									
								error.setDescription("Error occured while deleting Fantasy Grand Child Category Image.");
								fantasyGrandChildCategoryImagesDTO.setError(error);
								fantasyGrandChildCategoryImagesDTO.setStatus(0);
								return fantasyGrandChildCategoryImagesDTO;
							}
							grandChildImageList.add(grandChildImage);
							
							/*GrandChildCategoryImageAudit audit = new GrandChildCategoryImageAudit(grandChildImage);
							audit.setAction("DELETE");
							grandChildImageAudits.add(audit);*/
							////}
						}
					}
					DAORegistry.getFantasyGrandChildCategoryImageDAO().deleteAll(grandChildImageList);
					//DAORegistry.getFantasyGrandChildCategoryImageAuditDAO().saveAll(grandChildImageAudits);
					msg = "GrandChild Category Image Deleted Successfully.";
					action = "search";
					
					//Tracking User Action
					fantasyGrandChildCategoryIdStr = fantasyGrandChildCategoryIdStr.substring(0, fantasyGrandChildCategoryIdStr.length()-1);
					String userActionMsg = "FantasyGrandChildCategory Image DELETE Successfully.FantasyGrandChildCategory Id's - "+fantasyGrandChildCategoryIdStr;
					com.rtw.tmat.utils.Util.userActionAudit(request, null, userActionMsg);
				}
			}
			Map<Integer,FantasyGrandChildCategory> grandChildMap = new HashMap<Integer, FantasyGrandChildCategory>();
			if(grandChilds != null) {
				for (FantasyGrandChildCategory grandChild : grandChilds) {
					grandChildMap.put(grandChild.getId(),grandChild);
				}
			}
			List<FantasyGrandChildCategoryImage> grandChildImageList = new ArrayList<FantasyGrandChildCategoryImage>();
			if(grandChilds!=null){
				for (FantasyGrandChildCategory grandChildId : grandChilds) {
					try{
						selectedGrandChild += grandChildId + ",";
						FantasyGrandChildCategory grandChild = grandChildMap.get(grandChildId.getId());
						FantasyGrandChildCategoryImage grandChildImage = DAORegistry.getFantasyGrandChildCategoryImageDAO().getFantasyGrandChildCategoryImageByGrandChildId(grandChild.getId());
						if(grandChildImage == null){
							 grandChildImage = new FantasyGrandChildCategoryImage();
							 grandChildImage.setGrandChildCategory(grandChild);
						}
						grandChildImageList.add(grandChildImage);
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			
			List<FantasyGrandChildCategory> list = DAORegistry.getFantasyGrandChildCategoryDAO().getAllExceptNameAsCollege();
			List<String> cateNames = new ArrayList<String>();
			
			fantasyGrandChildCategoryImagesDTO.setGrandChilds(grandChilds);
			fantasyGrandChildCategoryImagesDTO.setGrandChildsList(list);
			fantasyGrandChildCategoryImagesDTO.setGrandChildsCheckAll(isAllGrandChild);
			fantasyGrandChildCategoryImagesDTO.setGrandChildStr(selectedGrandChild);
			fantasyGrandChildCategoryImagesDTO.setGrandChildImageList(grandChildImageList);
			fantasyGrandChildCategoryImagesDTO.setStatus(1);
			fantasyGrandChildCategoryImagesDTO.setMessage(msg);
			fantasyGrandChildCategoryImagesDTO.setProductName("Reward The Fan");
			
		}catch(Exception e){			
			System.err.println(e.getLocalizedMessage());
			e.printStackTrace();
			error.setDescription("Error occured while fetching/updating Fantasy Grand Child Category Image.");
			fantasyGrandChildCategoryImagesDTO.setError(error);
			fantasyGrandChildCategoryImagesDTO.setStatus(0);
		}
		return fantasyGrandChildCategoryImagesDTO;
	}
		
	
	@RequestMapping({"/LoyalFanParentCategoryImages"})
	public LoyalFanParentCategoryImagesDTO getLoyalFanParentCategoryImages(HttpServletRequest request, HttpServletResponse response, Model model){
		LoyalFanParentCategoryImagesDTO loyalFanParentCategoryImagesDTO = new LoyalFanParentCategoryImagesDTO();
		Error error = new Error();
		model.addAttribute("loyalFanParentCategoryImagesDTO", loyalFanParentCategoryImagesDTO);
		
		try{
			String msg = "";
			String param = request.getParameter("parentSearch");
			List<ParentCategory> parents = new ArrayList<ParentCategory>();
			String selectedParent = "";
			String artistsCheckAll = request.getParameter("parentCheckAll");
			boolean  isAllParent= ((artistsCheckAll==null || !artistsCheckAll.equalsIgnoreCase("on") )?false:true);
			String selectedValue = "";
			String selectedOption = "";
			if(param != null && !param.isEmpty()){
				ParentCategory parentCategory = DAORegistry.getParentCategoryDAO().get(Integer.parseInt(param));
				parents.add(parentCategory);
			}else{
				parents= DAORegistry.getParentCategoryDAO().getSportConcertTheater();
			}
			
			String action = request.getParameter("action");
			if(action != null) {
				String username = request.getParameter("userName");
				Date now = new Date();
				if(action.equals("update")) {
					MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
					List<LoyalFanParentCategoryImage> imageSaveList = new ArrayList<LoyalFanParentCategoryImage>();
					List<LoyalFanParentCategoryImage> imageUpdateList = new ArrayList<LoyalFanParentCategoryImage>();
					String auditAction = "";
					String parentCategoryIdStr = "";
					
					Map<String, String[]> requestParams = request.getParameterMap();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						if(key.contains("checkbox_")){
							Integer parentId = Integer.parseInt(key.replace("checkbox_", ""));
							parentCategoryIdStr += parentId+",";
							
							ParentCategory parentCat = DAORegistry.getParentCategoryDAO().get(parentId);
								LoyalFanParentCategoryImage loyalFanImage = DAORegistry.getLoyalFanParentCategoryImageDAO().getLoyalFanImageByParentCategory(parentId);
								if(loyalFanImage == null) {
									loyalFanImage = new LoyalFanParentCategoryImage();
								}
								loyalFanImage.setCategory(parentCat);
								loyalFanImage.setLastUpdated(now);
								loyalFanImage.setLastUpdatedBy(username);
								loyalFanImage.setStatus("ACTIVE");
								
								String fileDeleted = request.getParameter("fileDeleted_"+parentId);
								if(fileDeleted != null && fileDeleted.equals("Y")) {
									File newFile = null;
									if(loyalFanImage.getImageFileUrl() != null) {
										try{
											String fullFileNAme=URLUtil.LOYALFAN_DIRECTORY+loyalFanImage.getImageFileUrl();
											newFile = new File(fullFileNAme);
											if (newFile.exists()) {
												newFile.delete();
											}
										}catch(Exception e){
											e.printStackTrace();									
											error.setDescription("Error occured while retrieving LoyalFan Parent Category Image.");
											loyalFanParentCategoryImagesDTO.setError(error);
											loyalFanParentCategoryImagesDTO.setStatus(0);
											return loyalFanParentCategoryImagesDTO;
										}
									}
									loyalFanImage.setImageFileUrl(null);
								} else {
									MultipartFile file = multipartRequest.getFile("file_"+parentId);
									if (file != null && file.getSize() != 0) {
										File newFile = null;
										if(loyalFanImage.getImageFileUrl() != null) {
											try{
												String fullFileNAme=URLUtil.LOYALFAN_DIRECTORY+loyalFanImage.getImageFileUrl();
												newFile = new File(fullFileNAme);
												if (newFile.exists()) {
													newFile.delete();
												}
											}catch(Exception e){
												e.printStackTrace();									
												error.setDescription("Error occured while retrieving LoyalFan Parent Category Image.");
												loyalFanParentCategoryImagesDTO.setError(error);
												loyalFanParentCategoryImagesDTO.setStatus(0);
												return loyalFanParentCategoryImagesDTO;
											}
										}
										String ext = FilenameUtils.getExtension(file.getOriginalFilename());
										String fileName = "LoyalFanImage_" +parentCat.getId()+"."+ext;
										String fullFileName = URLUtil.LOYALFAN_DIRECTORY+fileName;
			
										newFile = new File(fullFileName);
										if (!newFile.exists()) {
											newFile.createNewFile();
										}
										BufferedOutputStream stream = new BufferedOutputStream(
												new FileOutputStream(newFile));
								        FileCopyUtils.copy(file.getInputStream(), stream);
										stream.close();
										loyalFanImage.setImageFileUrl(fileName);
									}
								}
									
								auditAction="UPDATE";
								if(loyalFanImage.getId() == null) {
									imageSaveList.add(loyalFanImage);
									auditAction="CREATE";
								} else {
									imageUpdateList.add(loyalFanImage);
								}
								/*GrandChildCategoryImageAudit audit = new GrandChildCategoryImageAudit(grandChildImage);
								audit.setAction(auditAction);
								grandChildImageAudits.add(audit);*/
							//}
							
						}
					}
					DAORegistry.getLoyalFanParentCategoryImageDAO().saveAll(imageSaveList);
					DAORegistry.getLoyalFanParentCategoryImageDAO().updateAll(imageUpdateList);
					//DAORegistry.getGrandChildCategoryImageAuditDAO().saveAll(grandChildImageAudits);
					
					msg = "Loyal Fan Image Saved Successfully.";
					action = "search";
					
					//Tracking User Action
					parentCategoryIdStr = parentCategoryIdStr.substring(0, parentCategoryIdStr.length()-1);
					String userActionMsg = "LoyalFanParentCategory Image "+auditAction+" Successfully.ParentCategory Id's - "+parentCategoryIdStr;
					com.rtw.tmat.utils.Util.userActionAudit(request, null, userActionMsg);
					
				} else if(action.equals("delete")) {
					List<LoyalFanParentCategoryImage> loyalFanImages = new ArrayList<LoyalFanParentCategoryImage>();
					String parentCategoryIdStr = "";
					
					Map<String, String[]> requestParams = request.getParameterMap();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						if(key.contains("checkbox_")){
							Integer parentId = Integer.parseInt(key.replace("checkbox_", ""));
							parentCategoryIdStr += parentId+",";
							
							String idStr = request.getParameter("id_"+parentId);
							if(idStr == null || idStr.isEmpty()) {
								continue;
							}
							Integer id=Integer.parseInt(idStr);
							LoyalFanParentCategoryImage loyalFanImage = DAORegistry.getLoyalFanParentCategoryImageDAO().get(id);
							if(loyalFanImage==null){
								continue;
							}
							loyalFanImage.setLastUpdated(now);
							loyalFanImage.setLastUpdatedBy(username);
							loyalFanImage.setStatus("DELETED");
							
							try{
								String fullFileNAme=URLUtil.LOYALFAN_DIRECTORY+loyalFanImage.getImageFileUrl();
								File newFile = new File(fullFileNAme);
								if (newFile.exists()) {
									newFile.delete();
								}
							}catch(Exception e){
								e.printStackTrace();									
								error.setDescription("Error occured while deleting LoyalFan Parent Category Image.");
								loyalFanParentCategoryImagesDTO.setError(error);
								loyalFanParentCategoryImagesDTO.setStatus(0);
								return loyalFanParentCategoryImagesDTO;
							}
							loyalFanImages.add(loyalFanImage);
						}
					}
					DAORegistry.getLoyalFanParentCategoryImageDAO().deleteAll(loyalFanImages);
					msg = "Loyal Fan Image Deleted Successfully.";
					action = "search";
					
					//Tracking User Action
					parentCategoryIdStr = parentCategoryIdStr.substring(0, parentCategoryIdStr.length()-1);
					String userActionMsg = "LoyalFanParentCategory Image DELETE Successfully.ParentCategory Id's - "+parentCategoryIdStr;
					com.rtw.tmat.utils.Util.userActionAudit(request, null, userActionMsg);
				}
			}
			Map<Integer,ParentCategory> parentMap = new HashMap<Integer, ParentCategory>();
			if(parents != null) {
				for (ParentCategory parent : parents) {
					parentMap.put(parent.getId(),parent);
				}
			}
			List<LoyalFanParentCategoryImage> parentImageList = new ArrayList<LoyalFanParentCategoryImage>();
			if(parents!=null){
				for (ParentCategory parent : parents) {
					try{
						selectedParent += parent.getId() + ",";
						ParentCategory parentCategory = parentMap.get(parent.getId());
						LoyalFanParentCategoryImage loyalFanImage = DAORegistry.getLoyalFanParentCategoryImageDAO().getLoyalFanImageByParentCategory(parent.getId());
						if(loyalFanImage == null){
							loyalFanImage = new LoyalFanParentCategoryImage();
							loyalFanImage.setCategory(parentCategory);
						}
						parentImageList.add(loyalFanImage);
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			
			List<ParentCategory> list = DAORegistry.getParentCategoryDAO().getSportConcertTheater();
			
			loyalFanParentCategoryImagesDTO.setProductName("Reward The Fan");
			loyalFanParentCategoryImagesDTO.setStatus(1);
			loyalFanParentCategoryImagesDTO.setMessage(msg);
			loyalFanParentCategoryImagesDTO.setParents(parents);
			loyalFanParentCategoryImagesDTO.setParentStr(selectedParent);
			loyalFanParentCategoryImagesDTO.setParentCheckAll(isAllParent);
			loyalFanParentCategoryImagesDTO.setParentList(list);
			loyalFanParentCategoryImagesDTO.setParentImageList(parentImageList);
			
		}catch(Exception e){			
			System.err.println(e.getLocalizedMessage());
			e.printStackTrace();
			error.setDescription("There is something wrong..Please Try Again.");
			loyalFanParentCategoryImagesDTO.setError(error);
			loyalFanParentCategoryImagesDTO.setStatus(0);
		}
		return loyalFanParentCategoryImagesDTO;
	}
	
	
	@RequestMapping({"/ChildCategoryImages"})
	public ChildCategoryImagesDTO getChildCategoryImages(HttpServletRequest request, HttpServletResponse response, Model model){
		ChildCategoryImagesDTO childCategoryImagesDTO = new ChildCategoryImagesDTO();
		Error error = new Error();
		model.addAttribute("childCategoryImagesDTO", childCategoryImagesDTO);
		
		try{
			/*
			String parentStr = request.getParameter("parentCategory");
			String[] childStr = request.getParameterValues("childs");
			*/
			String msg = "";
			String param = request.getParameter("childSearch");
			String selectedChild = "";
			List<ChildCategory> childs = new ArrayList<ChildCategory>();
			String childsCheckAll = request.getParameter("childsCheckAll");
			boolean  isAllChild = ((childsCheckAll==null || !childsCheckAll.equalsIgnoreCase("on") )?false:true);
			if(param != null  && !param.isEmpty()){
				ChildCategory child =  DAORegistry.getChildCategoryDAO().get(Integer.parseInt(param));
				childs.add(child);
			}else{
				childs = DAORegistry.getChildCategoryDAO().getAllChildCategoryByDisplayOnSearch();
			}
			
			
			
			/*
			if(parentStr!=null && !parentStr.isEmpty()){
				childs = DAORegistry.getChildCategoryDAO().getAllChildCategoryByParentCategory(Integer.parseInt(parentStr));
			}
			*/
			
			
			String action = request.getParameter("action");
			if(action != null) {
				String username = request.getParameter("userName");
				Date now = new Date();
				if(action.equals("update")) {
					MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
					List<ChildCategoryImage> childImageSaveList = new ArrayList<ChildCategoryImage>();
					List<ChildCategoryImage> childImageUpdateList = new ArrayList<ChildCategoryImage>();
					List<ChildCategoryImageAudit> childImageAudits = new ArrayList<ChildCategoryImageAudit>();
					String auditAction="";
					String childIdStr = "";
					
					Map<String, String[]> requestParams = request.getParameterMap();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						if(key.contains("checkbox_")){
							Integer childId = Integer.parseInt(key.replace("checkbox_", ""));
							childIdStr += childId+",";
							
							ChildCategory child1 = DAORegistry.getChildCategoryDAO().get(childId);
							List<ChildCategory> list = DAORegistry.getChildCategoryDAO().getAllChildCategoriesByNameAndDisplayOnSearch(child1.getName());
							for(ChildCategory child : list){
								ChildCategoryImage childImage = DAORegistry.getChildCategoryImageDAO().getChildCategoryImageByChildCategoryId(child.getId());
								if(childImage == null) {
									childImage = new ChildCategoryImage();
								}
								childImage.setChildCategory(child);
								childImage.setLastUpdated(now);
								childImage.setLastUpdatedBy(username);
								childImage.setStatus("ACTIVE");
								
								String fileDeleted = request.getParameter("fileDeleted_"+childId);
								if(fileDeleted != null && fileDeleted.equals("Y")) {
									File newFile = null;
									if(childImage.getImageFileUrl() != null) {
										try{
											String fullFileNAme=URLUtil.CHILD_DIRECTORY+childImage.getImageFileUrl();
											newFile = new File(fullFileNAme);
											if (newFile.exists()) {
												newFile.delete();
											}
										}catch(Exception e){
											e.printStackTrace();									
											error.setDescription("Error occured while retrieving Child Category Image.");
											childCategoryImagesDTO.setError(error);
											childCategoryImagesDTO.setStatus(0);
											return childCategoryImagesDTO;
										}
									}
									childImage.setImageFileUrl(null);
								} else {
									MultipartFile file = multipartRequest.getFile("file_"+childId);
									if (file != null && file.getSize() != 0) {
										File newFile = null;
										if(childImage.getImageFileUrl() != null) {
											try{
												String fullFileNAme=URLUtil.CHILD_DIRECTORY+childImage.getImageFileUrl();
												newFile = new File(fullFileNAme);
												if (newFile.exists()) {
													newFile.delete();
												}
											}catch(Exception e){
												e.printStackTrace();									
												error.setDescription("Error occured while retrieving Child Category Image.");
												childCategoryImagesDTO.setError(error);
												childCategoryImagesDTO.setStatus(0);
												return childCategoryImagesDTO;
											}
										}
										String ext = FilenameUtils.getExtension(file.getOriginalFilename());
										String fileName = "ChildCategoryImage_" +child.getId()+"."+ext;
										String fullFileName = URLUtil.CHILD_DIRECTORY+fileName;
			
										newFile = new File(fullFileName);
										if (!newFile.exists()) {
											newFile.createNewFile();
										}
										BufferedOutputStream stream = new BufferedOutputStream(
												new FileOutputStream(newFile));
								        FileCopyUtils.copy(file.getInputStream(), stream);
										stream.close();
										childImage.setImageFileUrl(fileName);
									}
								}
								
								/*String circleFileDeleted = request.getParameter("circleFileDeleted_"+childId);
								if(circleFileDeleted != null && circleFileDeleted.equals("Y")) {
									File newFile = null;
									if(childImage.getCircleImageFileUrl() != null) {
										String fullFileNAme=URLUtil.CHILD_DIRECTORY+childImage.getCircleImageFileUrl();
										newFile = new File(fullFileNAme);
										if (newFile.exists()) {
											newFile.delete();
										}
									}
									childImage.setCircleImageFileUrl(null);
								} else {
									MultipartFile circleFile = multipartRequest.getFile("circleFile_"+childId);
									if (circleFile != null && circleFile.getSize() != 0) {
										File newCircleFile = null;
										if(childImage.getCircleImageFileUrl() != null) {
											String fullFileNAme=URLUtil.CHILD_DIRECTORY+childImage.getCircleImageFileUrl();
											newCircleFile = new File(fullFileNAme);
											if (newCircleFile.exists()) {
												newCircleFile.delete();
											}
										}
										String ext = FilenameUtils.getExtension(circleFile.getOriginalFilename());
										String fileName = "ChildCircleImage_" +childId+"."+ext;
										String fullFileName = URLUtil.CHILD_DIRECTORY+fileName;
		
										newCircleFile = new File(fullFileName);
										if (!newCircleFile.exists()) {
											newCircleFile.createNewFile();
										}
										BufferedOutputStream stream = new BufferedOutputStream(
												new FileOutputStream(newCircleFile));
								        FileCopyUtils.copy(circleFile.getInputStream(), stream);
										stream.close();
										childImage.setCircleImageFileUrl(fileName);
									}
								}*/
								auditAction="UPDATE";
								if(childImage.getId() == null) {
									childImageSaveList.add(childImage);
									auditAction="CREATE";
								} else {
									childImageUpdateList.add(childImage);
								}
								ChildCategoryImageAudit audit = new ChildCategoryImageAudit(childImage);
								audit.setAction(auditAction);
								childImageAudits.add(audit);
							}
							
						}
					}
					DAORegistry.getChildCategoryImageDAO().saveAll(childImageSaveList);
					DAORegistry.getChildCategoryImageDAO().updateAll(childImageUpdateList);
					DAORegistry.getChildCategoryImageAuditDAO().saveAll(childImageAudits);
					
					msg = "ChildCategory Image Saved Successfully.";
					action = "search";
					
					//Tracking User Action
					childIdStr = childIdStr.substring(0, childIdStr.length()-1);
					String userActionMsg = "ChildCategory Image "+auditAction+" Successfully.ChildCategory Id's - "+childIdStr;
					com.rtw.tmat.utils.Util.userActionAudit(request, null, userActionMsg);
					
				} else if(action.equals("delete")) {
					List<ChildCategoryImage> childImageList = new ArrayList<ChildCategoryImage>();
					List<ChildCategoryImageAudit> childImageAudits = new ArrayList<ChildCategoryImageAudit>();
					String childIdStr = "";
					
					Map<String, String[]> requestParams = request.getParameterMap();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						if(key.contains("checkbox_")){
							Integer childId = Integer.parseInt(key.replace("checkbox_", ""));
							childIdStr += childId+",";
							
							String idStr = request.getParameter("id_"+childId);
							if(idStr == null || idStr.isEmpty()) {
								continue;
							}
							Integer id=Integer.parseInt(idStr);
							ChildCategoryImage childImage1 = DAORegistry.getChildCategoryImageDAO().get(id);
							ChildCategory childCategory = childImage1.getChildCategory();
							List<ChildCategory> list = DAORegistry.getChildCategoryDAO().getAllChildCategoriesByNameAndDisplayOnSearch(childCategory.getName());
							for(ChildCategory child : list){
								ChildCategoryImage childImage = DAORegistry.getChildCategoryImageDAO().getChildCategoryImageByChildCategoryId(child.getId());
								if(childImage == null) {
									continue;
								}
								childImage.setLastUpdated(now);
								childImage.setLastUpdatedBy(username);
								childImage.setStatus("DELETED");
								
								try{
									String fullFileName = URLUtil.CHILD_DIRECTORY+childImage.getImageFileUrl();
									File newFile = new File(fullFileName);
									if (newFile.exists()) {
										newFile.delete();
									}
									String fullCircleFileNAme=URLUtil.CHILD_DIRECTORY+childImage.getCircleImageFileUrl();
									File newCircleFile = new File(fullCircleFileNAme);
									if (newCircleFile.exists()) {
										newCircleFile.delete();
									}
								}catch(Exception e){
									e.printStackTrace();									
									error.setDescription("Error occured while deleting Child Category Image.");
									childCategoryImagesDTO.setError(error);
									childCategoryImagesDTO.setStatus(0);
									return childCategoryImagesDTO;
								}
								childImageList.add(childImage);
								
								ChildCategoryImageAudit audit = new ChildCategoryImageAudit(childImage);
								audit.setAction("DELETE");
								childImageAudits.add(audit);
							}
							
						}
					}
					DAORegistry.getChildCategoryImageDAO().deleteAll(childImageList);
					DAORegistry.getChildCategoryImageAuditDAO().saveAll(childImageAudits);
					msg = "ChildCategory Image Deleted Successfully.";
					action = "search";
					
					//Tracking User Action
					childIdStr = childIdStr.substring(0, childIdStr.length()-1);
					String userActionMsg = "ChildCategory Image DELETE Successfully.ChildCategory Id's - "+childIdStr;
					com.rtw.tmat.utils.Util.userActionAudit(request, null, userActionMsg);
				}
					
			}
			Map<Integer,ChildCategory> childMap = new HashMap<Integer, ChildCategory>();
			if(childs != null) {
				for (ChildCategory child : childs) {
					childMap.put(child.getId(),child);
				}
			}
			List<ChildCategoryImage> childImageList = new ArrayList<ChildCategoryImage>();
			if(childs!=null){
				for (ChildCategory childId : childs) {
					try{
						//selectedChild += childId + ",";
						ChildCategory child = childMap.get(childId.getId());
						ChildCategoryImage childImage = DAORegistry.getChildCategoryImageDAO().getChildCategoryImageByChildCategoryId(childId.getId());
						 if(childImage == null){
							 childImage = new ChildCategoryImage();
							 childImage.setChildCategory(child);
						 }
						 childImageList.add(childImage);
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}

			List<ChildCategory> list = DAORegistry.getChildCategoryDAO().getAllChildCategoryByDisplayOnSearch();
			List<ChildCategory> childList = new ArrayList<ChildCategory>();
			List<String> cateNames = new ArrayList<String>();
			for(ChildCategory cat : list){
				if(!cateNames.contains(cat.getName())){
					childList.add(cat);
				}
				cateNames.add(cat.getName());
			}
			
			childCategoryImagesDTO.setChildImageList(childImageList);
			childCategoryImagesDTO.setChilds(childs);
			childCategoryImagesDTO.setChildsList(childList);
			childCategoryImagesDTO.setChildsCheckAll(isAllChild);
			childCategoryImagesDTO.setChildStr(selectedChild);
			childCategoryImagesDTO.setStatus(1);
			childCategoryImagesDTO.setMessage(msg);
			childCategoryImagesDTO.setProductName("Reward The Fan");
			
		}catch(Exception e){
			System.err.println(e.getLocalizedMessage());
			e.printStackTrace();			
			error.setDescription("Error occured while fetching/updating Child Category Image.");
			childCategoryImagesDTO.setError(error);
			childCategoryImagesDTO.setStatus(0);
		}		
		return childCategoryImagesDTO;
	}
	
	@RequestMapping({"/ParentCategoryImages"})
	public ParentCategoryImagesDTO getParentCategoryImages(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		ParentCategoryImagesDTO parentCategoryImagesDTO = new ParentCategoryImagesDTO();
		Error error = new Error();
		model.addAttribute("parentCategoryImagesDTO", parentCategoryImagesDTO);
		
		try{
			String msg = "";
			String action = request.getParameter("action");
			if(action != null) {
				String username = request.getParameter("userName");
				Date now = new Date();
				if(action.equals("update")) {
					MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
					List<ParentCategoryImage> parentImageSaveList = new ArrayList<ParentCategoryImage>();
					List<ParentCategoryImage> parentImageUpdateList = new ArrayList<ParentCategoryImage>();
					List<ParentCategoryImageAudit> parentImageAudits = new ArrayList<ParentCategoryImageAudit>();
					String auditAction = "";
					String parentIdStr = "";
					
					Map<String, String[]> requestParams = request.getParameterMap();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						if(key.contains("checkbox_")){
							Integer parentId = Integer.parseInt(key.replace("checkbox_", ""));
							parentIdStr += parentId+",";
							
							//String idStr = request.getParameter("id_"+parentId);
							ParentCategory parent = DAORegistry.getParentCategoryDAO().get(parentId);
							ParentCategoryImage parentImage = DAORegistry.getParentCategoryImageDAO().getParentCategoryImageByParentCategoryId(parentId);
							if(parentImage == null) {
								parentImage = new ParentCategoryImage();
							}
							parentImage.setParentCategory(parent);
							parentImage.setLastUpdated(now);
							parentImage.setLastUpdatedBy(username);
							parentImage.setStatus("ACTIVE");
							
							String fileRequired = request.getParameter("fileRequired_"+parentId);
							if(fileRequired != null && fileRequired.equals("Y")) {
								MultipartFile file = multipartRequest.getFile("file_"+parentId);
								if (file == null || file.getSize() == 0) {
									continue;
								}
								File newFile = null;
								if(parentImage.getImageFileUrl() != null) {
									try{
										String fullFileName = URLUtil.PARENT_DIRECTORY+parentImage.getImageFileUrl();
										newFile = new File(fullFileName);
										if (newFile.exists()) {
											newFile.delete();
										}
									}catch(Exception e){
										e.printStackTrace();									
										error.setDescription("Error occured while retrieving Parent Category Image.");
										parentCategoryImagesDTO.setError(error);
										parentCategoryImagesDTO.setStatus(0);
										return parentCategoryImagesDTO;
									}
								}
								String ext = FilenameUtils.getExtension(file.getOriginalFilename());
								String fileName = "ParentCategoryImage_" +parentId+"."+ext;
								String fullFileName = URLUtil.PARENT_DIRECTORY+fileName;

								newFile = new File(fullFileName);
								if (!newFile.exists()) {
									newFile.createNewFile();
								}
								BufferedOutputStream stream = new BufferedOutputStream(
										new FileOutputStream(newFile));
						        FileCopyUtils.copy(file.getInputStream(), stream);
								stream.close();
								parentImage.setImageFileUrl(fileName);
							}
							auditAction="UPDATE";
							if(parentImage.getId() == null) {
								parentImageSaveList.add(parentImage);
								auditAction="CREATE";
							} else {
								parentImageUpdateList.add(parentImage);
							}
							ParentCategoryImageAudit audit = new ParentCategoryImageAudit(parentImage);
							audit.setAction(auditAction);
							parentImageAudits.add(audit);
						}
					}
					DAORegistry.getParentCategoryImageDAO().saveAll(parentImageSaveList);
					DAORegistry.getParentCategoryImageDAO().updateAll(parentImageUpdateList);
					DAORegistry.getParentCategoryImageAuditDAO().saveAll(parentImageAudits);
					
					msg = "ParentCategory Image Saved Successfully.";
					
					//Tracking User Action
					parentIdStr = parentIdStr.substring(0, parentIdStr.length()-1);
					String userActionMsg = "ParentCategory Image "+auditAction+" Successfully.ParentCategory Id's - "+parentIdStr;
					com.rtw.tmat.utils.Util.userActionAudit(request, null, userActionMsg);
					
				} else if(action.equals("delete")) {
					List<ParentCategoryImage> parentImageList = new ArrayList<ParentCategoryImage>();
					List<ParentCategoryImageAudit> parentImageAudits = new ArrayList<ParentCategoryImageAudit>();
					String parentIdStr = "";
					
					Map<String, String[]> requestParams = request.getParameterMap();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						if(key.contains("checkbox_")){
							Integer parentId = Integer.parseInt(key.replace("checkbox_", ""));
							parentIdStr += parentId+",";
							
							String idStr = request.getParameter("id_"+parentId);
							if(idStr == null || idStr.isEmpty()) {
								continue;
							}
							Integer id=Integer.parseInt(idStr);
							ParentCategoryImage parentImage = DAORegistry.getParentCategoryImageDAO().get(id);
							parentImage.setLastUpdated(now);
							parentImage.setLastUpdatedBy(username);
							parentImage.setStatus("DELETED");
							
							try{
								File newFile = new File(parentImage.getImageFileUrl());
								if (newFile.exists()) {
									newFile.delete();
								}
							}catch(Exception e){
								e.printStackTrace();									
								error.setDescription("Error occured while deleting Parent Category Image.");
								parentCategoryImagesDTO.setError(error);
								parentCategoryImagesDTO.setStatus(0);
								return parentCategoryImagesDTO;
							}
							parentImageList.add(parentImage);
							
							ParentCategoryImageAudit audit = new ParentCategoryImageAudit(parentImage);
							audit.setAction("DELETE");
							parentImageAudits.add(audit);
						}
					}
					DAORegistry.getParentCategoryImageDAO().deleteAll(parentImageList);
					DAORegistry.getParentCategoryImageAuditDAO().saveAll(parentImageAudits);
					msg = "ParentCategory Image Deleted Successfully.";
					
					//Tracking User Action
					parentIdStr = parentIdStr.substring(0, parentIdStr.length()-1);
					String userActionMsg = "ParentCategory Image DELETE Successfully.ParentCategory Id's - "+parentIdStr;
					com.rtw.tmat.utils.Util.userActionAudit(request, null, userActionMsg);
				}
					
			}
			List<ParentCategory> parentList = DAORegistry.getParentCategoryDAO().getAll();
			List<ParentCategoryImage> parentImageList = new ArrayList<ParentCategoryImage>();
			for (ParentCategory parentCategory : parentList) {
				try{
					ParentCategoryImage parentImage = DAORegistry.getParentCategoryImageDAO().getParentCategoryImageByParentCategoryId(parentCategory.getId());
					 if(parentImage == null){
						 parentImage = new ParentCategoryImage();
						 parentImage.setParentCategory(parentCategory);
					 }
					 parentImageList.add(parentImage);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			parentCategoryImagesDTO.setParentImageList(parentImageList);
			parentCategoryImagesDTO.setProductName("Reward The Fan");
			parentCategoryImagesDTO.setStatus(1);
			parentCategoryImagesDTO.setMessage(msg);
			
		}catch(Exception e){			
			System.err.println(e.getLocalizedMessage());
			e.printStackTrace();
			error.setDescription("Error occured while fetching/updating Parent Category Image.");
			parentCategoryImagesDTO.setError(error);
			parentCategoryImagesDTO.setStatus(0);
		}
		return parentCategoryImagesDTO;
	}

}
