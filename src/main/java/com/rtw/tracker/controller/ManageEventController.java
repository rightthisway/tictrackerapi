package com.rtw.tracker.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.rtw.tmat.dao.implementation.TMATDAORegistry;
import com.rtw.tmat.data.Event;
import com.rtw.tmat.data.RewardthefanCatsExchangeEvent;
import com.rtw.tmat.data.RewardthefanCatsExchangeEventAudit;
import com.rtw.tmat.utils.Constants;
import com.rtw.tmat.utils.Error;
import com.rtw.tmat.utils.Util;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.dao.implementation.QuizDAORegistry;
import com.rtw.tracker.datas.ApplicationPlatform;
import com.rtw.tracker.datas.Artist;
import com.rtw.tracker.datas.AutoCatsLockedTickets;
import com.rtw.tracker.datas.CategoryTicket;
import com.rtw.tracker.datas.CategoryTicketGroup;
import com.rtw.tracker.datas.ContestEvents;
import com.rtw.tracker.datas.Contests;
import com.rtw.tracker.datas.EventDetails;
import com.rtw.tracker.datas.InventoryHold;
import com.rtw.tracker.datas.ProductType;
import com.rtw.tracker.datas.ShippingMethod;
import com.rtw.tracker.datas.Ticket;
import com.rtw.tracker.datas.TicketGroup;
import com.rtw.tracker.datas.TicketStatus;
import com.rtw.tracker.datas.Venue;
import com.rtw.tracker.pojos.ArtistDTO;
import com.rtw.tracker.pojos.AutoCompleteArtistVenueDTO;
import com.rtw.tracker.pojos.CategoryAndLongTickets;
import com.rtw.tracker.pojos.CategoryTicketGroupDTO;
import com.rtw.tracker.pojos.CategoryTicketListDTO;
import com.rtw.tracker.pojos.EventListDTO;
import com.rtw.tracker.pojos.EventTicketDetailsDTO;
import com.rtw.tracker.pojos.GenericResponseDTO;
import com.rtw.tracker.pojos.HoldInventoryDTO;
import com.rtw.tracker.pojos.RtfCatsEventsDTO;
import com.rtw.tracker.pojos.ShippingMethodDTO;
import com.rtw.tracker.pojos.TicketGroupQty;
import com.rtw.tracker.pojos.TicketList;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.GridHeaderFiltersUtil;
import com.rtw.tracker.utils.PaginationUtil;
import com.rtw.tracker.utils.URLUtil;

@Controller
@RequestMapping({"/"})
public class ManageEventController {
	

	/*@RequestMapping({"/Events"})
	public String loadDashboardPage(HttpServletRequest request, HttpServletResponse response, ModelMap map,HttpSession session){
		try{
			Integer eventCount = 0;
			Collection<EventDetails> events = null;
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			Calendar cal = Calendar.getInstance();
			Date fromDate = new Date();
			cal.add(Calendar.MONTH, 3);
			Date toDate = cal.getTime();
			String fromDateStr = dateFormat.format(fromDate);
			String toDateStr = dateFormat.format(toDate);
			GridHeaderFilters filter = new GridHeaderFilters();
			Integer brokerId= Util.getBrokerId(session);
			if(brokerId > 0){
				events = DAORegistry.getQueryManagerDAO().getAllActiveEventDetailsByBroker(null,null,fromDateStr,toDateStr,null,brokerId,filter,null,true);
				eventCount = DAORegistry.getQueryManagerDAO().getAllActiveEventDetailsCountByBroker(null,null,fromDateStr,toDateStr,null,brokerId,filter);
			}else{
				events = DAORegistry.getQueryManagerDAO().getAllActiveEventDetailsByFilter(null,null,fromDateStr,toDateStr,null,filter,null);
				eventCount = DAORegistry.getQueryManagerDAO().getEventDetailsByFilterCount(null,null,fromDateStr,toDateStr,null,filter);
			}
			Collection<ShippingMethod> shippingMethods = DAORegistry.getShippingMethodDAO().getAll();
			map.put("status", "Available");
			map.put("events", JsonWrapperUtil.getEventArray(events));
			map.put("pagingInfo", PaginationUtil.calculatePaginationParameters(eventCount, null));
			map.put("ticketPagingInfo", PaginationUtil.getDummyPaginationParameter(0));
			map.put("fromDate", fromDateStr);
			map.put("toDate", toDateStr);
			map.put("shippingMethods", shippingMethods);
			map.put("currentYear", Calendar.getInstance().get(Calendar.YEAR));
			
		}catch(Exception e){
			e.printStackTrace();
			map.put("error", "There is something wrong..Please Try Again.");
		}
		return "page-manage-events";
	}*/
	
	
	@RequestMapping({"/GetEvents"})
	public EventListDTO getEvents(HttpServletRequest request, HttpServletResponse response){
		EventListDTO eventListDTO = new EventListDTO();
		Error error = new Error();
		try{
			String pageNo = request.getParameter("pageNo");
			String brokerIdStr = request.getParameter("brokerId");
			String searchType = request.getParameter("searchType");			
			String searchValue = request.getParameter("searchValue");			
			String fromDateStr = request.getParameter("fromDate");			
			String toDateStr = request.getParameter("toDate");
			String headerFilter = request.getParameter("headerFilter");
			String sortingString = request.getParameter("sortingString");
			String productTypeStr = request.getParameter("productType");
			
			if(StringUtils.isEmpty(pageNo)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				eventListDTO.setError(error);
				eventListDTO.setStatus(0);
				return eventListDTO;
			}
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide BrokerId");
				error.setDescription("Please provide BrokerId");
				eventListDTO.setError(error);
				eventListDTO.setStatus(0);
				return eventListDTO;
			}
			
			Integer brokerId = 0;	
			try{
				brokerId = Integer.parseInt(brokerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid BrokerId");
				error.setDescription("Please provide valid BrokerId");
				eventListDTO.setError(error);
				eventListDTO.setStatus(0);
				return eventListDTO;
			}
			try {
				Integer.parseInt(pageNo);
			} catch (Exception e) {
				System.err.println("Please provide valid PageNo.");
				error.setDescription("Please provide valid PageNo.");
				eventListDTO.setError(error);
				eventListDTO.setStatus(0);
				return eventListDTO;
			}
			
			GridHeaderFilters filter = null;
			try {
				filter = GridHeaderFiltersUtil.getEventSearchHeaderFilters(headerFilter+sortingString);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				eventListDTO.setError(error);
				eventListDTO.setStatus(0);
				return eventListDTO;
			}

			Collection<EventDetails> events = null;
			Integer eventCount = 0;
			String fromDateFinal = null;
			String toDateFinal = null;
			if((fromDateStr != null && !fromDateStr.isEmpty()) && (toDateStr != null && !toDateStr.isEmpty())){
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:23";
			}
			if(brokerId > 0){
				events = DAORegistry.getQueryManagerDAO().getAllActiveEventDetailsByBroker(searchType,searchValue,fromDateFinal,toDateFinal,null,brokerId,filter,pageNo,true);
				eventCount = DAORegistry.getQueryManagerDAO().getAllActiveEventDetailsCountByBroker(searchType,searchValue,fromDateFinal,toDateFinal,null,brokerId,filter);
			}else{
				events = DAORegistry.getQueryManagerDAO().getAllActiveEventDetailsByFilter(searchType,searchValue,fromDateFinal,toDateFinal,null,filter,pageNo);
				eventCount = DAORegistry.getQueryManagerDAO().getEventDetailsByFilterCount(searchType,searchValue,fromDateFinal,toDateFinal,null,filter);
			}
			
			Collection<ShippingMethod> shippingMethods = DAORegistry.getShippingMethodDAO().getAll();
			if(eventCount==0 || events==null || events.isEmpty()){
				eventListDTO.setMessage("No Events found with selected search filters.");
			}
			eventListDTO.setPaginationDTO(PaginationUtil.calculatePaginationParameter(eventCount, pageNo));
			eventListDTO.setStatus(1);
			eventListDTO.setEvents(events);
			eventListDTO.setTicketPagingInfo(PaginationUtil.getDummyPaginationParameters(0));
			eventListDTO.setFromDate(fromDateStr);
			eventListDTO.setToDate(toDateStr);
			eventListDTO.setShippingMethods(shippingMethods);
			eventListDTO.setCurrentYear(Calendar.getInstance().get(Calendar.YEAR));
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching events.");
			eventListDTO.setError(error);
			eventListDTO.setStatus(0);
		}		
		return eventListDTO;
	}
	
	/*@RequestMapping({"/GetZoneMapTickets"})
	public String getZoneMapWithTickets(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		TicketList ticketList = new TicketList();
		
		try{
			String eventId = request.getParameter("eventId");
			if(StringUtils.isEmpty(eventId)){
				Error error = new Error();
				System.err.println("Please select valid Event.");
				error.setDescription("Please select valid Event.");
				ticketList.setError(error);
				ticketList.setStatus(0);
			}
			
			if(eventId!=null && !eventId.isEmpty()){
				Map<String, String> requestMap = Util.getParameterMap(request);
				requestMap.put("eventId", eventId);
				String data = Util.getObject(requestMap,Constants.BASE_URL+Constants.GET_EVENT_DETAILS);
				Gson gson = new GsonBuilder().setDateFormat("MM/dd/yyyy HH:mm:ss").create();		
				JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
				ticketList = gson.fromJson(((JsonObject)jsonObject.get("ticketList")), TicketList.class);
				int count=0;
				if(ticketList!=null){
					for(TicketGroupQty qty : ticketList.getTicketGroupQtyList()){
						count += qty.getCategoryTicketGroups().size();
					}
				}
				ticketList.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(count));
			}
			map.addAttribute("ticketList", ticketList);
		}catch (Exception e) {
			System.err.println(e.getLocalizedMessage());
		}
		return "page-event-zone-map-tickets";
	}*/	
	
	//for mobile applications
	@RequestMapping({"/GetZoneMap"})
	public String GetZoneMap(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		try{
			String eventId = request.getParameter("eventId");
			if(eventId!=null && !eventId.isEmpty()){
				Map<String, String> requestMap = Util.getParameterMap(request);
				requestMap.put("eventId", eventId);
				String data = Util.getObject(requestMap,Constants.BASE_URL+Constants.GET_EVENT_DETAILS);
				Gson gson = new GsonBuilder().setDateFormat("MM/dd/yyyy HH:mm:ss").create();		
				JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
				TicketList ticketList = gson.fromJson(((JsonObject)jsonObject.get("ticketList")), TicketList.class);
				Map<String,String> colorsMap = new HashMap<String, String>();
				int count=0;
				if(ticketList!=null){
					for(TicketGroupQty qty : ticketList.getTicketGroupQtyList()){
						count += qty.getCategoryTicketGroups().size();
						for(CategoryTicketGroup categoryTicketGroup : qty.getCategoryTicketGroups()){
							colorsMap.put(categoryTicketGroup.getSvgKey(), categoryTicketGroup.getColorCode());
						}
					}
					map.put("colorsMap", colorsMap);
					map.put("ticketListJson", jsonObject);
					map.put("ticketList", ticketList);
					map.put("pagingInfo", PaginationUtil.getDummyPaginationParameter(count));
				}else{
					map.put("error", "Please provide valid event Identifier.");
				}
				
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return "page-event-zone-map-tickets";
	}
	
	//for mobile applications
	@RequestMapping({"/GetZoneMapTicketsData"})
	public String getZoneMapWithTicketsData(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		
		TicketList ticketList = null;
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		map.addAttribute("genericResponseDTO", genericResponseDTO);
		
		try{
			String msg = new String();
			
			String eventId = request.getParameter("eventId");
			if(StringUtils.isEmpty(eventId)){
				msg = "Please provide valid Event Id.";
			}
			
			if(!msg.isEmpty()){	
				if(eventId!=null && !eventId.isEmpty()){
					Map<String, String> requestMap = Util.getParameterMap(request);
					requestMap.put("eventId", eventId);
					String data = Util.getObject(requestMap,"http://api.rewardthefan.com/"+Constants.GET_EVENT_DETAILS);
					Gson gson = new GsonBuilder().setDateFormat("MM/dd/yyyy HH:mm:ss").create();		
					JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
					ticketList = gson.fromJson(((JsonObject)jsonObject.get("ticketList")), TicketList.class);
					map.put("ticketList", ticketList);
				}
			}else{
				Error error = new Error();
				System.err.println(msg);
				error.setDescription(msg);
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
			}
		}catch (Exception e) {
			Error error = new Error();
			System.err.println("Error occured while getting Zone map Tickets Data.");
			error.setDescription("Error occured while getting Zone map Tickets Data.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return "";
	}
	
	/*@RequestMapping({"/GetEventsForCustomer"})
	public void getEventsForCustomer(HttpServletRequest request, HttpServletResponse response){
		try{
			JSONObject returnObject = new JSONObject();
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			Integer eventCount = 0;
			Collection<EventDetails> events = null;
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			Calendar cal = Calendar.getInstance();
			Date fromDate = new Date();
			cal.add(Calendar.MONTH, 3);
			Date toDate = cal.getTime();
			String fromDateStr = dateFormat.format(fromDate);
			String toDateStr = dateFormat.format(toDate);
			GridHeaderFilters filter = GridHeaderFiltersUtil.getEventSearchHeaderFilters(headerFilter);
			if((fromDateStr != null && !fromDateStr.isEmpty()) && (toDateStr != null && !toDateStr.isEmpty())){
				String fromDateFinal = fromDateStr + " 00:00:00";
				String toDateFinal = toDateStr + " 23:59:23";
				events = DAORegistry.getQueryManagerDAO().getAllActiveEventDetailsByFilter(null,null,fromDateFinal,toDateFinal,null,filter,pageNo);
				eventCount = DAORegistry.getQueryManagerDAO().getEventDetailsByFilterCount(null,null,fromDateFinal,toDateFinal,null,filter);
			}else{
				events = DAORegistry.getQueryManagerDAO().getAllActiveEventDetailsByFilter(null,null,null,null,null,filter,pageNo);
				eventCount = DAORegistry.getQueryManagerDAO().getEventDetailsByFilterCount(null,null,null,null,null,filter);
			}
			returnObject.put("events", JsonWrapperUtil.getEventArray(events));
			returnObject.put("pagingInfo", PaginationUtil.calculatePaginationParameters(eventCount, pageNo));
			IOUtils.write(returnObject.toString(), response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	@RequestMapping({"/EventsExportToExcel"})
	public void getEventsToExport(HttpServletRequest request, HttpServletResponse response){
		try{
			String searchType = request.getParameter("searchType");
			String searchValue = request.getParameter("searchValue");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String headerFilter = request.getParameter("headerFilter");
			Collection<EventDetails> events = null;
			GridHeaderFilters filter = GridHeaderFiltersUtil.getEventSearchHeaderFilters(headerFilter);
			if((fromDateStr != null && !fromDateStr.isEmpty()) && (toDateStr != null && !toDateStr.isEmpty())){
				String fromDateFinal = fromDateStr + " 00:00:00";
				String toDateFinal = toDateStr + " 23:59:23";
				events = DAORegistry.getQueryManagerDAO().getAllActiveEventDetailsByFilterToExport(searchType,searchValue,fromDateFinal,toDateFinal,null,filter);
			}else{
				events = DAORegistry.getQueryManagerDAO().getAllActiveEventDetailsByFilterToExport(searchType,searchValue,null,null,null,filter);
			}
			
			if(events!=null && !events.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("events");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Event Id");
				header.createCell(1).setCellValue("Event Name");
				header.createCell(2).setCellValue("Event Date");
				header.createCell(3).setCellValue("Event Time");
				header.createCell(4).setCellValue("Day of Week");
				header.createCell(5).setCellValue("Building");
				header.createCell(6).setCellValue("Venue Id");
				header.createCell(7).setCellValue("No. of Ticket Count");
				header.createCell(8).setCellValue("No. of Ticket Sold Count");
				header.createCell(9).setCellValue("City");
				header.createCell(10).setCellValue("State");
				header.createCell(11).setCellValue("Country");
				header.createCell(12).setCellValue("Grand Child Category Name");
				header.createCell(13).setCellValue("Child Category Name");
				header.createCell(14).setCellValue("Parent Category Name");
				header.createCell(15).setCellValue("Event Creation");
				header.createCell(16).setCellValue("Event Updated");
				header.createCell(17).setCellValue("Notes");
				Integer i=1;
				for(EventDetails event : events){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(event.getEventId());
					row.createCell(1).setCellValue(event.getEventName());
					row.createCell(2).setCellValue(event.getEventDateStr()!=null?event.getEventDateStr():"");
					row.createCell(3).setCellValue(event.getEventTimeStr()!=null?event.getEventTimeStr():"");
					row.createCell(4).setCellValue(event.getDayOfWeek());
					row.createCell(5).setCellValue(event.getBuilding()!=null?event.getBuilding():"");
					row.createCell(6).setCellValue(event.getVenueId()!=null?event.getVenueId():0);
					row.createCell(7).setCellValue(event.getNoOfTixCount()!=null?event.getNoOfTixCount():0);
					row.createCell(8).setCellValue(event.getNoOfTixSoldCount()!=null?event.getNoOfTixSoldCount():0);
					row.createCell(9).setCellValue(event.getCity()!=null?event.getCity():"");
					row.createCell(10).setCellValue(event.getState()!=null?event.getState():"");
					row.createCell(11).setCellValue(event.getCountry()!=null?event.getCountry():"");
					row.createCell(12).setCellValue(event.getGrandChildCategoryName()!=null?event.getGrandChildCategoryName():"");
					row.createCell(13).setCellValue(event.getChildCategoryName()!=null?event.getChildCategoryName():"");
					row.createCell(14).setCellValue(event.getParentCategoryName()!=null?event.getParentCategoryName():"");
					row.createCell(15).setCellValue(event.getEventCreationStr()!=null?event.getEventCreationStr():"");
					row.createCell(16).setCellValue(event.getEventUpdatedStr()!=null?event.getEventUpdatedStr():"");
					row.createCell(17).setCellValue(event.getNotes()!=null?event.getNotes():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=events.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping({"/GetHoldTicketsForEvent"})
	public HoldInventoryDTO getHoldTicketsForEvent(HttpServletRequest request, HttpServletResponse response){
		HoldInventoryDTO holdInventoryDTO = new HoldInventoryDTO();
		Error error = new Error();
		//model.addAttribute("holdInventoryDTO", holdInventoryDTO);
		
		try{
			String pageNo = request.getParameter("pageNo");
			String brokerIdStr = request.getParameter("brokerId");
			String eventId = request.getParameter("eventId");
			String headerFilter = request.getParameter("headerFilter");
			String productType = request.getParameter("productType");
			
			if(StringUtils.isEmpty(pageNo)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				holdInventoryDTO.setError(error);
				holdInventoryDTO.setStatus(0);
				return holdInventoryDTO;
			}
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide BrokerId");
				error.setDescription("Please provide BrokerId");
				holdInventoryDTO.setError(error);
				holdInventoryDTO.setStatus(0);
				return holdInventoryDTO;
			}
			if(StringUtils.isEmpty(eventId)){				
				System.err.println("Please provide valid Event Id.");
				error.setDescription("Please provide valid Event Id.");
				holdInventoryDTO.setError(error);
				holdInventoryDTO.setStatus(0);
				return holdInventoryDTO;
			}
			
			Integer brokerId = 0;	
			try{
				brokerId = Integer.parseInt(brokerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid BrokerId");
				error.setDescription("Please provide valid BrokerId");
				holdInventoryDTO.setError(error);
				holdInventoryDTO.setStatus(0);
				return holdInventoryDTO;
			}
			try {
				Integer.parseInt(pageNo);
			} catch (Exception e) {
				System.err.println("Please provide valid PageNo.");
				error.setDescription("Please provide valid PageNo.");
				holdInventoryDTO.setError(error);
				holdInventoryDTO.setStatus(0);
				return holdInventoryDTO;
			}
			
			GridHeaderFilters filter = null;
			try {
				filter = GridHeaderFiltersUtil.getHoldTicketsSearchHeaderFilters(headerFilter);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				holdInventoryDTO.setError(error);
				holdInventoryDTO.setStatus(0);
				return holdInventoryDTO;
			}
			
			List<InventoryHold> inventoryHoldList = null;
			Integer count = 0;
						
			inventoryHoldList = DAORegistry.getInventoryHoldDAO().getAllHoldTicketsWithPagination(brokerId, Integer.parseInt(eventId), filter, pageNo);
			count = DAORegistry.getInventoryHoldDAO().getAllHoldTicketsCount(brokerId, Integer.parseInt(eventId), filter);
			
			if(count == 0 || inventoryHoldList == null || inventoryHoldList.isEmpty()){
				holdInventoryDTO.setMessage("No Hold Tickets found for selected event.");
			}
			
			Set<Integer> qty = new HashSet<Integer>();
			for(InventoryHold inventoryHold : inventoryHoldList){
				qty.add(inventoryHold.getQuantity());
			}
			
			holdInventoryDTO.setInventoryHoldQty(qty);
			holdInventoryDTO.setInventoryHoldList(inventoryHoldList);
			holdInventoryDTO.setStatus(1);
			holdInventoryDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(count));			
		}catch(Exception e){
			e.printStackTrace();			
			error.setDescription("Something went wrong while fetching Hold Tickets.");
			holdInventoryDTO.setError(error);
			holdInventoryDTO.setStatus(0);
		}		
		return holdInventoryDTO;
	}
	
	@RequestMapping({"/HoldTicketsExportToExcel"})
	public void getHoldTicketsToExport(HttpServletRequest request, HttpServletResponse response){
		
		try{
			String headerFilter = request.getParameter("headerFilter");
			String eventId = request.getParameter("eventId");
			String brokerIdStr = request.getParameter("brokerId");
			Integer brokerId = Integer.parseInt(brokerIdStr);
			
			List<InventoryHold> inventoryHoldList = null;
			GridHeaderFilters filter = GridHeaderFiltersUtil.getHoldTicketsSearchHeaderFilters(headerFilter);
			inventoryHoldList = DAORegistry.getInventoryHoldDAO().getAllHoldTickets(brokerId, Integer.parseInt(eventId), filter);
			
			if(inventoryHoldList!=null && !inventoryHoldList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("holdTickets");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Customer Name");
				header.createCell(1).setCellValue("Sale Price");
				header.createCell(2).setCellValue("Created By");
				header.createCell(3).setCellValue("Expiration Date");
				header.createCell(4).setCellValue("Expiration Minutes");
				header.createCell(5).setCellValue("Shipping Method");
				header.createCell(6).setCellValue("Hold Date");
				header.createCell(7).setCellValue("Internal Notes");
				header.createCell(8).setCellValue("External Notes");
				header.createCell(9).setCellValue("Ticket Id's");
				header.createCell(10).setCellValue("Ticket Group Id");
				header.createCell(11).setCellValue("Section");
				header.createCell(12).setCellValue("Row");
				header.createCell(13).setCellValue("Seat Low");
				header.createCell(14).setCellValue("Seat High");
				header.createCell(15).setCellValue("External PO");
				header.createCell(16).setCellValue("Status");
				header.createCell(17).setCellValue("Broker Id");
				Integer i=1;
				for(InventoryHold inventoryHold : inventoryHoldList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(inventoryHold.getCustomerName());
					row.createCell(1).setCellValue(inventoryHold.getSalePrice());
					row.createCell(2).setCellValue(inventoryHold.getcSR());
					row.createCell(3).setCellValue(inventoryHold.getExpirationDateStr()!=null?inventoryHold.getExpirationDateStr():"");
					row.createCell(4).setCellValue(inventoryHold.getExpirationMinutes()!=null?inventoryHold.getExpirationMinutes().toString():"");
					row.createCell(5).setCellValue(inventoryHold.getShippingMethod()!=null?inventoryHold.getShippingMethod():"");
					row.createCell(6).setCellValue(inventoryHold.getHoldDateTimeStr()!=null?inventoryHold.getHoldDateTimeStr():"");
					row.createCell(7).setCellValue(inventoryHold.getInternalNote()!=null?inventoryHold.getInternalNote():"");
					row.createCell(8).setCellValue(inventoryHold.getExternalNote()!=null?inventoryHold.getExternalNote():"");
					row.createCell(9).setCellValue(inventoryHold.getTicketIds()!=null?inventoryHold.getTicketIds():"");
					row.createCell(10).setCellValue(inventoryHold.getTicketGroupId()!=null?inventoryHold.getTicketGroupId():0);
					row.createCell(11).setCellValue(inventoryHold.getSection()!=null?inventoryHold.getSection():"");
					row.createCell(12).setCellValue(inventoryHold.getRow()!=null?inventoryHold.getRow():"");
					row.createCell(13).setCellValue(inventoryHold.getSeatLow()!=null?inventoryHold.getSeatLow():"");
					row.createCell(14).setCellValue(inventoryHold.getSeatHigh()!=null?inventoryHold.getSeatHigh():"");
					row.createCell(15).setCellValue(inventoryHold.getExternalPo()!=null?inventoryHold.getExternalPo():"");
					row.createCell(16).setCellValue(inventoryHold.getStatus()!=null?inventoryHold.getStatus():"");
					row.createCell(17).setCellValue(inventoryHold.getBrokerId()!=null?inventoryHold.getBrokerId():0);
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=holdTickets.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping({"/GetLockedTicketsForEvent"})
	public CategoryTicketListDTO getLockedTicketsForEvent(HttpServletRequest request, HttpServletResponse response){
		CategoryTicketListDTO categoryTicketListDTO = new CategoryTicketListDTO();
		Error error = new Error();
		//model.addAttribute("categoryTicketListDTO", categoryTicketListDTO);
		
		try{			
			String pageNo = request.getParameter("pageNo");
			String brokerIdStr = request.getParameter("brokerId");
			String eventId = request.getParameter("eventId");
			String headerFilter = request.getParameter("headerFilter");			
			String productType = request.getParameter("productType");
			
			if(StringUtils.isEmpty(pageNo)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				categoryTicketListDTO.setError(error);
				categoryTicketListDTO.setStatus(0);
				return categoryTicketListDTO;
			}
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide BrokerId");
				error.setDescription("Please provide BrokerId");
				categoryTicketListDTO.setError(error);
				categoryTicketListDTO.setStatus(0);
				return categoryTicketListDTO;
			}
			if(StringUtils.isEmpty(eventId)){				
				System.err.println("Please provide valid Event Id.");
				error.setDescription("Please provide valid Event Id.");
				categoryTicketListDTO.setError(error);
				categoryTicketListDTO.setStatus(0);
				return categoryTicketListDTO;
			}
			
			Integer brokerId = 0;	
			try{
				brokerId = Integer.parseInt(brokerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid BrokerId");
				error.setDescription("Please provide valid BrokerId");
				categoryTicketListDTO.setError(error);
				categoryTicketListDTO.setStatus(0);
				return categoryTicketListDTO;
			}
			try {
				Integer.parseInt(pageNo);
			} catch (Exception e) {
				System.err.println("Please provide valid PageNo.");
				error.setDescription("Please provide valid PageNo.");
				categoryTicketListDTO.setError(error);
				categoryTicketListDTO.setStatus(0);
				return categoryTicketListDTO;
			}
			
			GridHeaderFilters filter = null;
			try {
				filter = GridHeaderFiltersUtil.getLockedTicketsSearchHeaderFilters(headerFilter);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				categoryTicketListDTO.setError(error);
				categoryTicketListDTO.setStatus(0);
				return categoryTicketListDTO;
			}
			
			List<CategoryTicketGroup> lockedTicketsList = null;
			EventDetails eventDtls = null;
			Integer count = 0;
			
			lockedTicketsList = DAORegistry.getQueryManagerDAO().getLockedTicketDetails(Integer.parseInt(eventId), filter);
			count = DAORegistry.getQueryManagerDAO().getLockedTicketDetailsCount(Integer.parseInt(eventId), filter);
			eventDtls = DAORegistry.getEventDetailsDAO().getEventById(Integer.parseInt(eventId));
			
			if(count == 0 || lockedTicketsList == null || lockedTicketsList.isEmpty()){
				categoryTicketListDTO.setMessage("No Locked Tickets found for selected event.");
			}
			
			Set<Integer> qty = new HashSet<Integer>();
			for(CategoryTicketGroup categoryTicketGroup : lockedTicketsList){
				qty.add(categoryTicketGroup.getQuantity());
			}
				
			categoryTicketListDTO.setCategoryTicketQty(qty);
			categoryTicketListDTO.setCategoryTickets(lockedTicketsList);			
			categoryTicketListDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(count));
			categoryTicketListDTO.setEventDetails(eventDtls);			
			categoryTicketListDTO.setStatus(1);			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Locked Tickets.");
			categoryTicketListDTO.setError(error);
			categoryTicketListDTO.setStatus(0);
		}			
		return categoryTicketListDTO;
	}
		

	@RequestMapping({"/LockedTicketsExportToExcel"})
	public void getLockedTicketsToExport(HttpServletRequest request, HttpServletResponse response){
		
		try{
			String headerFilter = request.getParameter("headerFilter");
			String eventId = request.getParameter("eventId");
			List<CategoryTicketGroup> lockedTicketsList = null;
			
			GridHeaderFilters filter = GridHeaderFiltersUtil.getLockedTicketsSearchHeaderFilters(headerFilter);
			lockedTicketsList = DAORegistry.getQueryManagerDAO().getLockedTicketDetails(Integer.parseInt(eventId), filter);
			EventDetails eventDtls = DAORegistry.getEventDetailsDAO().getEventById(Integer.parseInt(eventId));
			
			if(lockedTicketsList!=null && !lockedTicketsList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("lockedTickets");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Section");
				header.createCell(1).setCellValue("Row");
				header.createCell(2).setCellValue("Quantity");
				header.createCell(3).setCellValue("Price");
				header.createCell(4).setCellValue("Tax Amount");
				header.createCell(5).setCellValue("Section Range");
				header.createCell(6).setCellValue("Row Range");
				header.createCell(7).setCellValue("Shipping Method");
				header.createCell(8).setCellValue("Product Type");
				header.createCell(9).setCellValue("Retail Price");
				header.createCell(10).setCellValue("Wholesale Price");
				header.createCell(11).setCellValue("Face Price");
				header.createCell(12).setCellValue("Cost");
				header.createCell(13).setCellValue("Broadcast");
				header.createCell(14).setCellValue("Internal Notes");
				header.createCell(15).setCellValue("External Notes");
				header.createCell(16).setCellValue("Marketplace Notes");
				header.createCell(17).setCellValue("Max Showing");
				header.createCell(18).setCellValue("Seat Low");
				header.createCell(19).setCellValue("Seat High");
				header.createCell(20).setCellValue("Near Term Display Option");
				header.createCell(21).setCellValue("Event Name");
				header.createCell(22).setCellValue("Event Date");
				header.createCell(23).setCellValue("Event Time");
				header.createCell(24).setCellValue("Venue");
				header.createCell(25).setCellValue("Ticket Type");
				header.createCell(26).setCellValue("Category Ticket Group Id");
				header.createCell(27).setCellValue("IP Address");
				header.createCell(28).setCellValue("Platform");
				header.createCell(29).setCellValue("Creation Date");
				header.createCell(30).setCellValue("Lock Status");				
				
				Integer i=1;
				for(CategoryTicketGroup lockedTicket : lockedTicketsList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(lockedTicket.getSection()!=null?lockedTicket.getSection():"");
					row.createCell(1).setCellValue(lockedTicket.getRow()!=null?lockedTicket.getRow():"");
					row.createCell(2).setCellValue(lockedTicket.getQuantity()!=null?lockedTicket.getQuantity():0);
					row.createCell(3).setCellValue(lockedTicket.getPrice()!=null?lockedTicket.getPrice():0);
					row.createCell(4).setCellValue(lockedTicket.getTaxAmount()!=null?lockedTicket.getTaxAmount():0);
					row.createCell(5).setCellValue(lockedTicket.getSectionRange()!=null?lockedTicket.getSectionRange():"");
					row.createCell(6).setCellValue(lockedTicket.getRowRange()!=null?lockedTicket.getRowRange():"");
					row.createCell(7).setCellValue(lockedTicket.getShippingMethod()!=null?lockedTicket.getShippingMethod():"");
					row.createCell(8).setCellValue(lockedTicket.getProducttype()!=null?lockedTicket.getProducttype().toString():"");
					row.createCell(9).setCellValue(lockedTicket.getPrice()!=null?lockedTicket.getPrice():0);
					row.createCell(10).setCellValue(lockedTicket.getPrice()!=null?lockedTicket.getPrice():0);
					row.createCell(11).setCellValue(0.00);
					row.createCell(12).setCellValue(0.00);
					row.createCell(13).setCellValue(1);
					row.createCell(14).setCellValue("ZTP");
					row.createCell(15).setCellValue(lockedTicket.getExternalNotes()!=null?lockedTicket.getExternalNotes():"");
					row.createCell(16).setCellValue(lockedTicket.getMarketPlaceNotes()!=null?lockedTicket.getMarketPlaceNotes():"");
					row.createCell(17).setCellValue(lockedTicket.getMaxShowing()!=null?lockedTicket.getMaxShowing():0);
					row.createCell(18).setCellValue(lockedTicket.getSeatLow()!=null?lockedTicket.getSeatLow():"");
					row.createCell(19).setCellValue(lockedTicket.getSeatHigh()!=null?lockedTicket.getSeatHigh():"");
					row.createCell(20).setCellValue(lockedTicket.getNearTermDisplayOption()!=null?lockedTicket.getNearTermDisplayOption():"");
					row.createCell(21).setCellValue((eventDtls!=null && eventDtls.getEventName()!=null)?eventDtls.getEventName():"");
					row.createCell(22).setCellValue((eventDtls!=null && eventDtls.getEventDateStr()!=null)?eventDtls.getEventDateStr():"");
					row.createCell(23).setCellValue((eventDtls!=null && eventDtls.getEventTimeStr()!=null)?eventDtls.getEventTimeStr():"");
					row.createCell(24).setCellValue((eventDtls!=null && eventDtls.getBuilding()!=null)?eventDtls.getBuilding():"");
					row.createCell(25).setCellValue("Category");
					row.createCell(26).setCellValue(lockedTicket.getCatgeoryTicketGroupId()!=null?lockedTicket.getCatgeoryTicketGroupId():0);
					row.createCell(27).setCellValue(lockedTicket.getIpAddress()!=null?lockedTicket.getIpAddress():"");
					row.createCell(28).setCellValue(lockedTicket.getPlatform()!=null?lockedTicket.getPlatform():"");
					row.createCell(29).setCellValue(lockedTicket.getCreationDate()!=null?lockedTicket.getCreationDate().toString():"");
					row.createCell(30).setCellValue(lockedTicket.getLockStatus()!=null?lockedTicket.getLockStatus().toString():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=lockedTickets.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Get Sold Tickets for given event
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/GetSoldTicketsForEvent")
	public CategoryTicketListDTO getSoldTicketsForEvent(HttpServletRequest request, HttpServletResponse response){
		CategoryTicketListDTO categoryTicketListDTO = new CategoryTicketListDTO();
		Error error = new Error();
		//model.addAttribute("categoryTicketListDTO", categoryTicketListDTO);
		
		try {
			String brokerIdStr = request.getParameter("brokerId");
			String eventId = request.getParameter("eventId");			
			String headerFilter = request.getParameter("headerFilter");			
			String productType = request.getParameter("productType");
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide BrokerId");
				error.setDescription("Please provide BrokerId");
				categoryTicketListDTO.setError(error);
				categoryTicketListDTO.setStatus(0);
				return categoryTicketListDTO;
			}
			if(StringUtils.isEmpty(eventId)){				
				System.err.println("Please provide valid Event Id.");
				error.setDescription("Please provide valid Event Id.");
				categoryTicketListDTO.setError(error);
				categoryTicketListDTO.setStatus(0);
				return categoryTicketListDTO;
			}
			
			Integer brokerId = 0;	
			try{
				brokerId = Integer.parseInt(brokerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid BrokerId");
				error.setDescription("Please provide valid BrokerId");
				categoryTicketListDTO.setError(error);
				categoryTicketListDTO.setStatus(0);
				return categoryTicketListDTO;
			}
						
			GridHeaderFilters filter = null;
			try {
				filter = GridHeaderFiltersUtil.getTicketSearchHeaderFilters(headerFilter);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				categoryTicketListDTO.setError(error);
				categoryTicketListDTO.setStatus(0);
				return categoryTicketListDTO;
			}
			
			List<CategoryTicketGroup> categoryTicketGroupList = new ArrayList<CategoryTicketGroup>();
			
			Collection<CategoryTicketGroup> soldTickets = null;
			List<CategoryTicket> categoryTickets = null;
			Ticket ticket = null;
			TicketGroup ticketGroup = null;
			List<TicketGroup> ticketGroupList = new ArrayList<TicketGroup>();
			
			EventDetails eventDetails =null;
			Set<Integer> ticketGroupIDSet = new HashSet<Integer>();
			Set<Integer> catTicketGroupIDSet = new HashSet<Integer>();
			 
			eventDetails = DAORegistry.getEventDetailsDAO().get(Integer.parseInt(eventId));
			soldTickets = DAORegistry.getCategoryTicketGroupDAO().getSoldTicketsSearchFilterByEventId(Integer.parseInt(eventId), brokerId, filter);
			
			for(CategoryTicketGroup catTicketGroup: soldTickets){
				categoryTickets = DAORegistry.getCategoryTicketDAO().getCategoryTicketByCategoryTicketGroupId(catTicketGroup.getId());
				for(CategoryTicket catTicket: categoryTickets){
					if(catTicket.getTicketId() != null && catTicket.getTicketId() > 0){
						ticket = DAORegistry.getTicketDAO().get(catTicket.getTicketId());
						ticketGroup = DAORegistry.getTicketGroupDAO().get(ticket.getTicketGroupId());
						if(ticketGroup != null){
							Integer ticGroupId = ticketGroup.getId();
							if(ticGroupId != null){
								if(ticketGroupIDSet.add(ticGroupId)){								
									/*ticketGroupList.add(ticketGroup);
									object = JsonWrapperUtil.getSoldTicketDetailsArray(catTicketGroup, ticketGroup);
									ObjectArray.put(object);*/
									if(ticketGroup != null){
										catTicketGroup.setSection(ticketGroup.getSection());
										catTicketGroup.setRow(ticketGroup.getRow());
										catTicketGroup.setSeatLow(ticketGroup.getSeatLow());
										catTicketGroup.setSeatHigh(ticketGroup.getSeatHigh());
										catTicketGroup.setQuantity(ticketGroup.getQuantity());
									}
									categoryTicketGroupList.add(catTicketGroup);
								}
							}
						}
					}else{
						Integer catTicGroupId = catTicketGroup.getId();
						if(catTicGroupId != null){
							if(catTicketGroupIDSet.add(catTicGroupId)){
								categoryTicketGroupList.add(catTicketGroup);
								/*object = JsonWrapperUtil.getSoldTicketDetailsArray(catTicketGroup, null);
								ObjectArray.put(object);*/
							}
						}
					}
				}
			}
			
			if(categoryTicketGroupList == null || categoryTicketGroupList.isEmpty()){
				categoryTicketListDTO.setMessage("No Sold Tickets Found for selected event.");
			}			
			
			Set<Integer> qty = new HashSet<Integer>();
			for(CategoryTicketGroup categoryTicketGroup : categoryTicketGroupList){
				qty.add(categoryTicketGroup.getQuantity());
			}
			
			categoryTicketListDTO.setCategoryTicketQty(qty);						
			categoryTicketListDTO.setCategoryTickets(categoryTicketGroupList);
			categoryTicketListDTO.setStatus(1);
			categoryTicketListDTO.setEventDetails(eventDetails);
			categoryTicketListDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(soldTickets!=null?soldTickets.size():0));				

		} catch (Exception e) {			
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Sold Tickets.");
			categoryTicketListDTO.setError(error);
			categoryTicketListDTO.setStatus(0);
		}		
		return categoryTicketListDTO;
	}
	
	@RequestMapping({"/SoldTicketsExportToExcel"})
	public void getSoldTicketsToExport(HttpServletRequest request, HttpServletResponse response){
		
		try{
			String headerFilter = request.getParameter("headerFilter");
			String eventId = request.getParameter("eventId");
			String brokerIdStr = request.getParameter("brokerId");
			Integer brokerId = Integer.parseInt(brokerIdStr);
			
			Collection<CategoryTicketGroup> soldTickets = null;
			List<CategoryTicket> categoryTickets = null;
			JSONObject object = null;
			JSONArray ObjectArray = new JSONArray();
			Ticket ticket = null;
			TicketGroup ticketGroup = null;
			List<TicketGroup> ticketGroupList = new ArrayList<TicketGroup>();
			Set<Integer> ticketGroupIDSet = new HashSet<Integer>();
			Set<Integer> catTicketGroupIDSet = new HashSet<Integer>();
			
			GridHeaderFilters filter = GridHeaderFiltersUtil.getTicketSearchHeaderFilters(headerFilter);
			soldTickets = DAORegistry.getCategoryTicketGroupDAO().getSoldTicketsSearchFilterByEventId(Integer.parseInt(eventId), brokerId, filter);

			if(soldTickets!=null && !soldTickets.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("soldTickets");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Invoice Id");
				header.createCell(1).setCellValue("Section");
				header.createCell(2).setCellValue("Row");
				header.createCell(3).setCellValue("Seat Low");
				header.createCell(4).setCellValue("Seat High");
				header.createCell(5).setCellValue("Quantity");
				header.createCell(6).setCellValue("Retail Price");
				header.createCell(7).setCellValue("Wholesale Price");
				header.createCell(8).setCellValue("Face Price");
				header.createCell(9).setCellValue("Price");
				header.createCell(10).setCellValue("Cost");
				header.createCell(11).setCellValue("Shipping Method");
				header.createCell(12).setCellValue("Near Term Display Option");
				header.createCell(13).setCellValue("Broadcast");
				header.createCell(14).setCellValue("Section Range");
				header.createCell(15).setCellValue("Row Range");
				header.createCell(16).setCellValue("Internal Notes");
				header.createCell(17).setCellValue("External Notes");
				header.createCell(18).setCellValue("Marketplace Notes");
				Integer i=1;
				for(CategoryTicketGroup catTicketGroup: soldTickets){

					Row row = sheet.createRow(i);
					
					categoryTickets = DAORegistry.getCategoryTicketDAO().getCategoryTicketByCategoryTicketGroupId(catTicketGroup.getId());
					for(CategoryTicket catTicket: categoryTickets){
						
						if(catTicket.getTicketId() != null && catTicket.getTicketId() > 0){
							ticket = DAORegistry.getTicketDAO().get(catTicket.getTicketId());
							ticketGroup = DAORegistry.getTicketGroupDAO().get(ticket.getTicketGroupId());
							if(ticketGroup != null){
								Integer ticGroupId = ticketGroup.getId();
								if(ticGroupId != null){
									if(ticketGroupIDSet.add(ticGroupId)){								
										ticketGroupList.add(ticketGroup);
										
										row.createCell(0).setCellValue(catTicketGroup.getInvoiceId()!=null?catTicketGroup.getInvoiceId():0);
										if(ticketGroup!=null){
											row.createCell(1).setCellValue(ticketGroup.getSection()!=null?ticketGroup.getSection():"");
											row.createCell(2).setCellValue(ticketGroup.getRow()!=null?ticketGroup.getRow():"");
											row.createCell(3).setCellValue(ticketGroup.getSeatLow()!=null?ticketGroup.getSeatLow():"");
											row.createCell(4).setCellValue(ticketGroup.getSeatHigh()!=null?ticketGroup.getSeatHigh():"");							
										}
										row.createCell(5).setCellValue(catTicketGroup.getQuantity()!=null?catTicketGroup.getQuantity():0);
										row.createCell(6).setCellValue(catTicketGroup.getPrice()!=null?catTicketGroup.getPrice():0);
										row.createCell(7).setCellValue(catTicketGroup.getPrice()!=null?catTicketGroup.getPrice():0);
										row.createCell(8).setCellValue(0.00);
										row.createCell(9).setCellValue(catTicketGroup.getPrice()!=null?catTicketGroup.getPrice():0);
										row.createCell(10).setCellValue(0.00);
										row.createCell(11).setCellValue(catTicketGroup.getShippingMethod()!=null?catTicketGroup.getShippingMethod():"");
										row.createCell(12).setCellValue(catTicketGroup.getNearTermDisplayOption()!=null?catTicketGroup.getNearTermDisplayOption():"");
										row.createCell(13).setCellValue(catTicketGroup.getNearTermDisplayOption()!=null?catTicketGroup.getNearTermDisplayOption():"");
										row.createCell(14).setCellValue(catTicketGroup.getSectionRange()!=null?catTicketGroup.getSectionRange():"");
										row.createCell(15).setCellValue(catTicketGroup.getRowRange()!=null?catTicketGroup.getRowRange():"");
										row.createCell(16).setCellValue(catTicketGroup.getInternalNotes()!=null?catTicketGroup.getInternalNotes():"");
										row.createCell(17).setCellValue(catTicketGroup.getExternalNotes()!=null?catTicketGroup.getExternalNotes():"");
										row.createCell(18).setCellValue(catTicketGroup.getMarketPlaceNotes()!=null?catTicketGroup.getMarketPlaceNotes():"");
										//object = JsonWrapperUtil.getSoldTicketDetailsArray(catTicketGroup, ticketGroup);
										//ObjectArray.put(object);
									}
								}
							}
						}else{
							Integer catTicGroupId = catTicketGroup.getId();
							if(catTicGroupId != null){
								if(catTicketGroupIDSet.add(catTicGroupId)){
									
									row.createCell(0).setCellValue(catTicketGroup.getInvoiceId()!=null?catTicketGroup.getInvoiceId():0);
									row.createCell(1).setCellValue(catTicketGroup.getSection()!=null?catTicketGroup.getSection():"");
									row.createCell(2).setCellValue(catTicketGroup.getRow()!=null?catTicketGroup.getRow():"");
									row.createCell(3).setCellValue(catTicketGroup.getSeatLow()!=null?catTicketGroup.getSeatLow():"");
									row.createCell(4).setCellValue(catTicketGroup.getSeatHigh()!=null?catTicketGroup.getSeatHigh():"");
									row.createCell(5).setCellValue(catTicketGroup.getQuantity()!=null?catTicketGroup.getQuantity():0);
									row.createCell(6).setCellValue(catTicketGroup.getPrice()!=null?catTicketGroup.getPrice():0);
									row.createCell(7).setCellValue(catTicketGroup.getPrice()!=null?catTicketGroup.getPrice():0);
									row.createCell(8).setCellValue(0.00);
									row.createCell(9).setCellValue(catTicketGroup.getPrice()!=null?catTicketGroup.getPrice():0);
									row.createCell(10).setCellValue(0.00);
									row.createCell(11).setCellValue(catTicketGroup.getShippingMethod()!=null?catTicketGroup.getShippingMethod():"");
									row.createCell(12).setCellValue(catTicketGroup.getNearTermDisplayOption()!=null?catTicketGroup.getNearTermDisplayOption():"");
									row.createCell(13).setCellValue(catTicketGroup.getNearTermDisplayOption()!=null?catTicketGroup.getNearTermDisplayOption():"");
									row.createCell(14).setCellValue(catTicketGroup.getSectionRange()!=null?catTicketGroup.getSectionRange():"");
									row.createCell(15).setCellValue(catTicketGroup.getRowRange()!=null?catTicketGroup.getRowRange():"");
									row.createCell(16).setCellValue(catTicketGroup.getInternalNotes()!=null?catTicketGroup.getInternalNotes():"");
									row.createCell(17).setCellValue(catTicketGroup.getExternalNotes()!=null?catTicketGroup.getExternalNotes():"");
									row.createCell(18).setCellValue(catTicketGroup.getMarketPlaceNotes()!=null?catTicketGroup.getMarketPlaceNotes():"");
									//object = JsonWrapperUtil.getSoldTicketDetailsArray(catTicketGroup, null);
									//ObjectArray.put(object);
								}
							}
						}
					}
					
					i++;			
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=soldTickets.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Get Category tickets for event
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/GetEventDetails")
	public EventListDTO getEventDetails(HttpServletRequest request, HttpServletResponse response){
		EventListDTO eventListDTO = new EventListDTO();
		Error error = new Error();
		//model.addAttribute("eventListDTO", eventListDTO);
		
		try{
			String pageNo = request.getParameter("pageNo");
			String brokerIdStr = request.getParameter("brokerId");
			String headerFilter = request.getParameter("headerFilter");
			String productType = request.getParameter("productType");
			String searchType = request.getParameter("searchType");
			String searchValue = request.getParameter("eventSearchValue");
									
			if(StringUtils.isEmpty(pageNo)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				eventListDTO.setError(error);
				eventListDTO.setStatus(0);
				return eventListDTO;
			}
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide BrokerId");
				error.setDescription("Please provide BrokerId");
				eventListDTO.setError(error);
				eventListDTO.setStatus(0);
				return eventListDTO;
			}
			
			Integer brokerId = 0;	
			try{
				brokerId = Integer.parseInt(brokerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid BrokerId");
				error.setDescription("Please provide valid BrokerId");
				eventListDTO.setError(error);
				eventListDTO.setStatus(0);
				return eventListDTO;
			}
			try {
				Integer.parseInt(pageNo);
			} catch (Exception e) {
				System.err.println("Please provide valid PageNo.");
				error.setDescription("Please provide valid PageNo.");
				eventListDTO.setError(error);
				eventListDTO.setStatus(0);
				return eventListDTO;
			}
			
			GridHeaderFilters filter = null;
			try {
				filter = GridHeaderFiltersUtil.getEventSearchHeaderFilters(headerFilter);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				eventListDTO.setError(error);
				eventListDTO.setStatus(0);
				return eventListDTO;
			}
			
			Collection<EventDetails> events = null;
			Integer count =0;
			
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MINUTE, 30);
			String fromDateStr = Util.formatDateToMonthDateYear(cal.getTime());
			fromDateStr = fromDateStr + " 00:00:00";
			
			events = DAORegistry.getQueryManagerDAO().getAllActiveEventForPO(searchType, searchValue,fromDateStr,null,filter,pageNo);
			count = DAORegistry.getQueryManagerDAO().getEventDetailsCountForPO(searchType, searchValue,fromDateStr,null,filter);

			if(count == 0 || events == null || events.isEmpty()){
				eventListDTO.setMessage("No events found for selected search filters.");
			}
			
			eventListDTO.setEvents(events);
			eventListDTO.setStatus(1);
			eventListDTO.setPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));			
		} catch(Exception e){
			e.printStackTrace();			
			error.setDescription("Something went wrong while fetching events.");
			eventListDTO.setError(error);
			eventListDTO.setStatus(0);
		}		
		return eventListDTO;
	}
	
	
	/**
	 * Get Sold Tickets for given event
	 * @param request
	 * @param response
	 * @return
	 */
	/*@RequestMapping(value = "/GetSoldTickets")
	public String getSoldTickets(HttpServletRequest request, HttpServletResponse response,ModelMap map){
		Collection<CategoryTicketGroup> soldTickets = null;
		List<CategoryTicket> categoryTickets = null;
		JSONObject object = null;
		JSONArray ObjectArray = new JSONArray();
		Ticket ticket = null;
		TicketGroup ticketGroup = null;
		List<TicketGroup> ticketGroupList = new ArrayList<TicketGroup>();
		EventDetails eventDetails =null;
		Set<Integer> ticketGroupIDSet = new HashSet<Integer>();
		Set<Integer> catTicketGroupIDSet = new HashSet<Integer>();
		try {
			String eventId = request.getParameter("eventId");
			if(eventId != null && !eventId.isEmpty()){
				eventDetails = DAORegistry.getEventDetailsDAO().get(Integer.parseInt(eventId));
				soldTickets = DAORegistry.getCategoryTicketGroupDAO().getSoldTicketsByEventId(Integer.parseInt(eventId));
				
				for(CategoryTicketGroup catTicketGroup: soldTickets){
					categoryTickets = DAORegistry.getCategoryTicketDAO().getCategoryTicketByCategoryTicketGroupId(catTicketGroup.getId());
					for(CategoryTicket catTicket: categoryTickets){
						if(catTicket.getTicketId() != null && catTicket.getTicketId() > 0){
							ticket = DAORegistry.getTicketDAO().get(catTicket.getTicketId());
							ticketGroup = DAORegistry.getTicketGroupDAO().get(ticket.getTicketGroupId());
							if(ticketGroup != null){
								Integer ticGroupId = ticketGroup.getId();
								if(ticGroupId != null){
									if(ticketGroupIDSet.add(ticGroupId)){								
										ticketGroupList.add(ticketGroup);
										object = JsonWrapperUtil.getSoldTicketDetailsArray(catTicketGroup, ticketGroup);
										ObjectArray.put(object);
									}
								}
							}
						}else{
							Integer catTicGroupId = catTicketGroup.getId();
							if(catTicGroupId != null){
								if(catTicketGroupIDSet.add(catTicGroupId)){
									object = JsonWrapperUtil.getSoldTicketDetailsArray(catTicketGroup, null);
									ObjectArray.put(object);
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		map.put("soldTickets", ObjectArray);
		map.put("eventDetails", eventDetails);
		map.put("pagingInfo", PaginationUtil.getDummyPaginationParameter(soldTickets!=null?soldTickets.size():0));
		return "page-event-sold-tickets";
	}*/
	
	
	 @RequestMapping({"/EditHoldTickets"})
	 public HoldInventoryDTO editHoldTickets(HttpServletRequest request, HttpServletResponse response){
		 HoldInventoryDTO holdInventoryDTO = new HoldInventoryDTO();
		 Error error = new Error();
		 //model.addAttribute("holdInventoryDTO", holdInventoryDTO);
		
		try{			
			String pageNo = request.getParameter("pageNo");
			String brokerIdStr = request.getParameter("brokerId");
			String productType = request.getParameter("productType");
			
			if(StringUtils.isEmpty(pageNo)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				holdInventoryDTO.setError(error);
				holdInventoryDTO.setStatus(0);
				return holdInventoryDTO;
			}
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide BrokerId");
				error.setDescription("Please provide BrokerId");
				holdInventoryDTO.setError(error);
				holdInventoryDTO.setStatus(0);
				return holdInventoryDTO;
			}
			
			Integer brokerId = 0;	
			try{
				brokerId = Integer.parseInt(brokerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid BrokerId");
				error.setDescription("Please provide valid BrokerId");
				holdInventoryDTO.setError(error);
				holdInventoryDTO.setStatus(0);
				return holdInventoryDTO;
			}
			try {
				Integer.parseInt(pageNo);
			} catch (Exception e) {
				System.err.println("Please provide valid PageNo.");
				error.setDescription("Please provide valid PageNo.");
				holdInventoryDTO.setError(error);
				holdInventoryDTO.setStatus(0);
				return holdInventoryDTO;
			}
			
			List<InventoryHold> inventoryHoldList = null;
			Integer count = 0;
			GridHeaderFilters filter = new GridHeaderFilters();
			
			inventoryHoldList = DAORegistry.getInventoryHoldDAO().getAllHoldTicketsWithPagination(brokerId, null, filter,pageNo);
			count = DAORegistry.getInventoryHoldDAO().getAllHoldTicketsCount(brokerId, null, filter);
			
			if(count == 0 || inventoryHoldList == null || inventoryHoldList.isEmpty()){
				holdInventoryDTO.setMessage("No Hold Tickets Found for selected ticket.");
			}
			
			holdInventoryDTO.setStatus(1);
			holdInventoryDTO.setInventoryHoldList(inventoryHoldList);
			holdInventoryDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(count));
		}catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Hold Tickets.");
			holdInventoryDTO.setError(error);
			holdInventoryDTO.setStatus(0);
		}		
		return holdInventoryDTO;
	}
	
	
	@RequestMapping({"/GetHoldTickets"})
	public HoldInventoryDTO getHoldTickets(HttpServletRequest request, HttpServletResponse response){		
		HoldInventoryDTO holdInventoryDTO = new HoldInventoryDTO();
		Error error = new Error();
		//model.addAttribute("holdInventoryDTO", holdInventoryDTO);
		
		try{			
			String pageNo = request.getParameter("pageNo");
			String brokerIdStr = request.getParameter("brokerId");
			String headerFilter = request.getParameter("headerFilter");
			String productType = request.getParameter("productType");			
			
			if(StringUtils.isEmpty(pageNo)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				holdInventoryDTO.setError(error);
				holdInventoryDTO.setStatus(0);
				return holdInventoryDTO;
			}
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide BrokerId");
				error.setDescription("Please provide BrokerId");
				holdInventoryDTO.setError(error);
				holdInventoryDTO.setStatus(0);
				return holdInventoryDTO;
			}
			
			Integer brokerId = 0;	
			try{
				brokerId = Integer.parseInt(brokerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid BrokerId");
				error.setDescription("Please provide valid BrokerId");
				holdInventoryDTO.setError(error);
				holdInventoryDTO.setStatus(0);
				return holdInventoryDTO;
			}
			try {
				Integer.parseInt(pageNo);
			} catch (Exception e) {
				System.err.println("Please provide valid PageNo.");
				error.setDescription("Please provide valid PageNo.");
				holdInventoryDTO.setError(error);
				holdInventoryDTO.setStatus(0);
				return holdInventoryDTO;
			}
			
			GridHeaderFilters filter = null;
			try {
				filter = GridHeaderFiltersUtil.getHoldTicketsSearchHeaderFilters(headerFilter);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				holdInventoryDTO.setError(error);
				holdInventoryDTO.setStatus(0);
				return holdInventoryDTO;
			}
			
			List<InventoryHold> inventoryHoldList = null;
			Integer count =0;
			
			inventoryHoldList = DAORegistry.getInventoryHoldDAO().getAllHoldTicketsWithPagination(null, null, filter, pageNo);
			count = DAORegistry.getInventoryHoldDAO().getAllHoldTicketsCount(null, null, filter);
			
			if(count == 0 || inventoryHoldList == null || inventoryHoldList.isEmpty()){
				holdInventoryDTO.setMessage("No Hold Tickets found for selected search filter.");
			}
						
			holdInventoryDTO.setInventoryHoldList(inventoryHoldList);
			holdInventoryDTO.setStatus(1);
			holdInventoryDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(count));		
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Hold Tickets.");
			holdInventoryDTO.setError(error);
			holdInventoryDTO.setStatus(0);
		}		
		return holdInventoryDTO; 
	}
 
	/**
	 * Get Artist Details
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/GetArtist")
	public ArtistDTO getArtist(HttpServletRequest request, HttpServletResponse response, HttpSession session){		
		ArtistDTO artistDTO = new ArtistDTO();
		Error error = new Error();
		//JSONObject returnObject = new JSONObject();
		
		try{
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String searchType = request.getParameter("searchType");
			String searchValue = request.getParameter("searchValue");
			
			if(StringUtils.isEmpty(pageNo)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				artistDTO.setError(error);
				artistDTO.setStatus(0);
				return artistDTO;
			}
			
			try {
				Integer.parseInt(pageNo);
			} catch (Exception e) {
				System.err.println("Please provide valid PageNo.");
				error.setDescription("Please provide valid PageNo.");
				artistDTO.setError(error);
				artistDTO.setStatus(0);
				return artistDTO;
			}
			
			GridHeaderFilters filter = null;
			try {
				filter = GridHeaderFiltersUtil.getManageArtistSearchHeaderFilters(headerFilter);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				artistDTO.setError(error);
				artistDTO.setStatus(0);
				return artistDTO;
			}
			
			Collection<EventDetails> artists = null;
			Integer count = 0;
			artists = DAORegistry.getEventDetailsDAO().getAllActiveArtistsByFilter(filter,pageNo);
			count = DAORegistry.getEventDetailsDAO().getAllActiveArtistsTotalCount(filter);
			
			if(count == 0 || artists == null || artists.isEmpty()){
				artistDTO.setMessage("No Artists found for selected search filter.");
			}
			
			artistDTO.setStatus(1);
			artistDTO.setArtists(artists);
			artistDTO.setPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
			//returnObject.put("artistList", JsonWrapperUtil.getArtistArray(artists));
			//returnObject.put("artistPagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));
			//IOUtils.write(returnObject.toString().getBytes(),response.getOutputStream());;
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Artists.");
			artistDTO.setError(error);
			artistDTO.setStatus(0);
		}
		return artistDTO;
	}
	
	@RequestMapping({"/ManageArtistExportToExcel"})
	public void manageArtistToExport(HttpServletRequest request, HttpServletResponse response){
		
		String headerFilter = request.getParameter("headerFilter");
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getManageArtistSearchHeaderFilters(headerFilter);
			Collection<EventDetails> artistList = DAORegistry.getEventDetailsDAO().getAllActiveArtistsByFilterToExport(filter);
			
			if(artistList!=null && !artistList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Manage_Artist");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Artist Id");
				header.createCell(1).setCellValue("Artist Name");
				header.createCell(2).setCellValue("Grand Child Category");
				header.createCell(3).setCellValue("Child Category");
				header.createCell(4).setCellValue("Parent Category");
				header.createCell(5).setCellValue("Visible/In Visible");
				Integer i=1;
				for(EventDetails artist : artistList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(artist.getArtistId());
					row.createCell(1).setCellValue(artist.getArtistName());
					row.createCell(2).setCellValue(artist.getGrandChildCategoryName()!=null?artist.getGrandChildCategoryName():"");
					row.createCell(3).setCellValue(artist.getChildCategoryName()!=null?artist.getChildCategoryName():"");
					row.createCell(4).setCellValue(artist.getParentCategoryName()!=null?artist.getParentCategoryName():"");
					row.createCell(5).setCellValue((artist.getDisplayOnSearch()!=null&&artist.getDisplayOnSearch()==true)?"Yes":"No");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Manage_Artist.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Get Artist Details
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/GetGrandChildCategory")
	public void getGrandChildCategory(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		JSONArray jsonArr = new JSONArray();
		try{
			
			String searchType = request.getParameter("searchType");
			String searchValue = request.getParameter("searchValue");
			
			if(searchType != null && searchType.trim().length() > 0 &&
					searchValue != null && searchValue.trim().length() > 0) {
				Collection<EventDetails> events = DAORegistry.getEventDetailsDAO().getAllActiveGrandChildCategoryByFilter(searchType,searchValue);
				System.out.println("=====" +events.size());
				JSONObject jObj = null;
				
				for (EventDetails eventDtls : events) {
					jObj = new JSONObject();
					jObj.put("grandChildCategoryId", eventDtls.getGrandChildCategoryId());
					jObj.put("grandChildCategory", eventDtls.getGrandChildCategoryName());
					jObj.put("childCategory", eventDtls.getChildCategoryName());
					jObj.put("parentCategory", eventDtls.getParentCategoryName());
					jsonArr.put(jObj);
				}
			}
			//return jsonArr;
			IOUtils.write(jsonArr.toString().getBytes(),
					response.getOutputStream());;
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Get Artist Details
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/GetVenueDetails")
	public void getVenueDetails(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		JSONArray jsonArr = new JSONArray();
		try{
			
			String searchType = request.getParameter("searchType");
			String searchValue = request.getParameter("searchValue");
			
			if(searchType != null && searchType.trim().length() > 0 &&
					searchValue != null && searchValue.trim().length() > 0) {
				//Collection<EventDetails> events = DAORegistry.getEventDetailsDAO().getAllActiveVenueByFilter(searchType,searchValue);
				Collection<Venue> venueList = DAORegistry.getQueryManagerDAO().getAllVenueDetails(searchType,searchValue);
				JSONObject jObj = null;
				
				for (Venue venue : venueList) {
					jObj = new JSONObject();
					jObj.put("id", venue.getId());
					jObj.put("building", venue.getBuilding());
					jObj.put("city", venue.getCity());
					jObj.put("state", venue.getState());
					jObj.put("country", venue.getCountry());
					jObj.put("postalCode", venue.getPostalCode());
					jObj.put("notes", venue.getNotes());
					jObj.put("catTicketCount", venue.getCatTicketCount());
					jsonArr.put(jObj);
				}
			}
			response.setHeader("Content-type","application/json");
			IOUtils.write(jsonArr.toString().getBytes(),
					response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Get Category tickets for event
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/getCategoryTicketGroups")
	public EventTicketDetailsDTO getCategoryTicketGroups(HttpServletRequest request, HttpServletResponse response){
		EventTicketDetailsDTO eventTicketDetailsDTO = new EventTicketDetailsDTO();
		Error error = new Error();
		//model.addAttribute("eventTicketDetailsDTO", eventTicketDetailsDTO);
		
		try{
			String eventId = request.getParameter("eventId");
			String status = request.getParameter("status");
			String section = request.getParameter("sectionSearch");
			String row = request.getParameter("rowSearch");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String seat = request.getParameter("seatSearch");
			String brokerIdStr = request.getParameter("brokerId");
			String productType = request.getParameter("productType");
			
			if(StringUtils.isEmpty(pageNo)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				eventTicketDetailsDTO.setError(error);
				eventTicketDetailsDTO.setStatus(0);
				return eventTicketDetailsDTO;
			}
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide BrokerId");
				error.setDescription("Please provide BrokerId");
				eventTicketDetailsDTO.setError(error);
				eventTicketDetailsDTO.setStatus(0);
				return eventTicketDetailsDTO;
			}
			if(StringUtils.isEmpty(eventId)){				
				System.err.println("Please provide valid Event Id.");
				error.setDescription("Please provide valid Event Id.");
				eventTicketDetailsDTO.setError(error);
				eventTicketDetailsDTO.setStatus(0);
				return eventTicketDetailsDTO;
			}
			
			Integer brokerId = 0;	
			try{
				brokerId = Integer.parseInt(brokerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid BrokerId");
				error.setDescription("Please provide valid BrokerId");
				eventTicketDetailsDTO.setError(error);
				eventTicketDetailsDTO.setStatus(0);
				return eventTicketDetailsDTO;
			}
			try {
				Integer.parseInt(pageNo);
			} catch (Exception e) {
				System.err.println("Please provide valid PageNo.");
				error.setDescription("Please provide valid PageNo.");
				eventTicketDetailsDTO.setError(error);
				eventTicketDetailsDTO.setStatus(0);
				return eventTicketDetailsDTO;
			}
			
			GridHeaderFilters filter = null;
			try {
				filter = GridHeaderFiltersUtil.getTicketSearchHeaderFilters(headerFilter);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				eventTicketDetailsDTO.setError(error);
				eventTicketDetailsDTO.setStatus(0);
				return eventTicketDetailsDTO;
			}
			
			Collection<CategoryTicketGroup> categoryTicketGroups = null;
			Collection<TicketGroup> ticketGroups = null;
			Map<Integer, String> shippingMap = new HashMap<Integer, String>();
						
			EventDetails eventDetails = DAORegistry.getEventDetailsDAO().getEventById(Integer.parseInt(eventId));
			Collection<ShippingMethod> methodList = DAORegistry.getShippingMethodDAO().getAll();
			for(ShippingMethod method : methodList){
				shippingMap.put(method.getId(),method.getName());
			}
			ticketGroups = DAORegistry.getQueryManagerDAO().getUnmappedTicketGroupsWithPagination(brokerId,Integer.parseInt(eventId),filter,pageNo);
			categoryTicketGroups = DAORegistry.getCategoryTicketGroupDAO().getAllActiveCategoryTicketsbyEventId(brokerId,Integer.parseInt(eventId),filter,pageNo);
			
			if((ticketGroups == null || ticketGroups.isEmpty()) && (categoryTicketGroups == null || categoryTicketGroups.isEmpty())){
				eventTicketDetailsDTO.setMessage("No Tickets found for selected search filter.");
			}
			if(!CollectionUtils.isEmpty(ticketGroups)){
				Set<Integer> qty = new HashSet<Integer>();
				for(TicketGroup ticketGroup : ticketGroups){
					qty.add(ticketGroup.getQuantity());
				}				
				eventTicketDetailsDTO.setTicketQty(qty);
			}
			
			if(!CollectionUtils.isEmpty(categoryTicketGroups)){
				Set<Integer> qty = new HashSet<Integer>();
				for(CategoryTicketGroup categoryTicketGroup : categoryTicketGroups){
					qty.add(categoryTicketGroup.getQuantity());
				}				
				eventTicketDetailsDTO.setTicketQty(qty);
			}
			
			Collection<CategoryAndLongTickets> categoryAndLongTickets = Util.mergeCategoryTicketsAndTickets(categoryTicketGroups, ticketGroups, shippingMap, eventDetails);
			
			eventTicketDetailsDTO.setPaginationDTO(PaginationUtil.calculatePaginationParameter(categoryAndLongTickets.size(),null));
			eventTicketDetailsDTO.setCategoryAndLongTickets(categoryAndLongTickets);
			eventTicketDetailsDTO.setStatus(1);			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Tickets.");
			eventTicketDetailsDTO.setError(error);
			eventTicketDetailsDTO.setStatus(0);
		}
		return eventTicketDetailsDTO;
	}
	
	
	@RequestMapping(value = "/TicketsExportToExcel")
	public void getCategoryTicketGroupsToExport(HttpServletRequest request, HttpServletResponse response){
				
		try{
			String section = request.getParameter("sectionSearch");
			String rows = request.getParameter("rowSearch");
			String seat = request.getParameter("seatSearch");
			String pageNo = request.getParameter("pageNo");			
			Integer brokerId = StringUtils.isNotEmpty(request.getParameter("brokerId")) ? Integer.parseInt(request.getParameter("brokerId")) : 0;			
			Integer eventId = Integer.parseInt(request.getParameter("eventId"));
			String headerFilter = request.getParameter("headerFilter");
			
			GridHeaderFilters filter = GridHeaderFiltersUtil.getTicketSearchHeaderFilters(headerFilter);
			
			Collection<CategoryTicketGroup> categoryTicketGroups = null;
			Collection<TicketGroup> ticketGroups = null;
			Map<Integer, String> shippingMap = new HashMap<Integer, String>();
			
			EventDetails eventDtls = DAORegistry.getEventDetailsDAO().getEventById(eventId);
			categoryTicketGroups = DAORegistry.getCategoryTicketGroupDAO().getAllActiveCategoryTicketsbyEventId(brokerId,eventId,filter,pageNo);
			ticketGroups = DAORegistry.getQueryManagerDAO().getUnmappedTicketGroupsWithPagination(brokerId,eventId,filter,pageNo);
			Collection<ShippingMethod> methodList = DAORegistry.getShippingMethodDAO().getAll();
			for(ShippingMethod method : methodList){
				shippingMap.put(method.getId(),method.getName());
			}
			
			if((categoryTicketGroups!=null && !categoryTicketGroups.isEmpty()) || (ticketGroups != null && !ticketGroups.isEmpty())){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("tickets");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Id");
				header.createCell(1).setCellValue("Event Id");
				header.createCell(2).setCellValue("Section");
				header.createCell(3).setCellValue("Row");
				header.createCell(4).setCellValue("Quantity");
				header.createCell(5).setCellValue("Price");
				header.createCell(6).setCellValue("Tax Amount");
				header.createCell(7).setCellValue("Section Range");
				header.createCell(8).setCellValue("Row Range");
				header.createCell(9).setCellValue("Shipping Method");
				header.createCell(10).setCellValue("Product Type");
				header.createCell(11).setCellValue("Retail Price");
				header.createCell(12).setCellValue("Wholesale Price");
				header.createCell(13).setCellValue("Face Price");
				header.createCell(14).setCellValue("Cost");
				header.createCell(15).setCellValue("Broadcast");
				header.createCell(16).setCellValue("Internal Notes");
				header.createCell(17).setCellValue("External Notes");
				header.createCell(18).setCellValue("Market Place Notes");
				header.createCell(19).setCellValue("Max Showing");
				header.createCell(20).setCellValue("Seat Low");
				header.createCell(21).setCellValue("Seat High");
				header.createCell(22).setCellValue("Near Term Display Option");
				header.createCell(23).setCellValue("Event Name");
				header.createCell(24).setCellValue("Event Date");
				header.createCell(25).setCellValue("Event Time");
				header.createCell(26).setCellValue("Venue");
				header.createCell(27).setCellValue("Ticket Type");
				header.createCell(28).setCellValue("Broker Id");
				Integer i=1;
				if(ticketGroups != null && !ticketGroups.isEmpty()){
					for(TicketGroup tx : ticketGroups){
						Row row = sheet.createRow(i);
						row.createCell(0).setCellValue(tx.getId());
						row.createCell(1).setCellValue(tx.getEventId());
						row.createCell(2).setCellValue(tx.getSection()!=null?tx.getSection():"");
						row.createCell(3).setCellValue(tx.getRow()!=null?tx.getRow():"");
						row.createCell(4).setCellValue(tx.getQuantity());
						row.createCell(5).setCellValue(tx.getPrice());
						row.createCell(6).setCellValue("0.0");
						row.createCell(7).setCellValue("");
						row.createCell(8).setCellValue("");
						row.createCell(9).setCellValue(shippingMap.get(tx.getShippingMethodId())!=null?shippingMap.get(tx.getShippingMethodId()):"");
						row.createCell(10).setCellValue("REWARDTHEFAN");
						row.createCell(11).setCellValue(tx.getPrice()!=null?tx.getPrice():0);
						row.createCell(12).setCellValue(tx.getPrice()!=null?tx.getPrice():0);
						row.createCell(13).setCellValue(0.0);
						row.createCell(14).setCellValue(0.0);
						if(tx.getBroadcast()!=null && tx.getBroadcast()){
							row.createCell(15).setCellValue(1);
						}else{
							row.createCell(15).setCellValue(0);
						}
						row.createCell(16).setCellValue("ZTP");
						row.createCell(17).setCellValue("");
						row.createCell(18).setCellValue("");
						row.createCell(19).setCellValue("");
						row.createCell(20).setCellValue(tx.getSeatLow()!=null?tx.getSeatLow():"");
						row.createCell(21).setCellValue(tx.getSeatHigh()!=null?tx.getSeatHigh():"");
						row.createCell(22).setCellValue("");
						row.createCell(23).setCellValue(eventDtls.getEventName()!=null?eventDtls.getEventName():"");
						row.createCell(24).setCellValue(eventDtls.getEventDateStr()!=null?eventDtls.getEventDateStr():"");
						row.createCell(25).setCellValue(eventDtls.getEventTimeStr()!=null?eventDtls.getEventTimeStr():"");
						row.createCell(26).setCellValue(eventDtls.getBuilding()!=null?eventDtls.getBuilding():"");
						row.createCell(27).setCellValue("Real");
						row.createCell(28).setCellValue(tx.getBrokerId());
						i++;
					}
				}
				if(categoryTicketGroups != null && !categoryTicketGroups.isEmpty()){
					for(CategoryTicketGroup tx : categoryTicketGroups){
						Row row = sheet.createRow(i);
						row.createCell(0).setCellValue(tx.getId());
						row.createCell(1).setCellValue(tx.getEventId());
						row.createCell(2).setCellValue(tx.getSection()!=null?tx.getSection():"");
						row.createCell(3).setCellValue(tx.getRow()!=null?tx.getRow():"");
						row.createCell(4).setCellValue(tx.getQuantity());
						row.createCell(5).setCellValue(tx.getPrice());
						row.createCell(6).setCellValue(tx.getTaxAmount()!=null?tx.getTaxAmount():0);
						row.createCell(7).setCellValue(tx.getSectionRange()!=null?tx.getSectionRange():"");
						row.createCell(8).setCellValue(tx.getRowRange()!=null?tx.getRowRange():"");
						row.createCell(9).setCellValue(tx.getShippingMethod()!=null?tx.getShippingMethod():"");
						row.createCell(10).setCellValue(tx.getProducttype().toString());
						row.createCell(11).setCellValue(tx.getPrice()!=null?tx.getPrice():0);
						row.createCell(12).setCellValue(tx.getPrice()!=null?tx.getPrice():0);
						row.createCell(13).setCellValue(0.0);
						row.createCell(14).setCellValue(0.0);
						if(tx.getBroadcast()!=null && tx.getBroadcast()){
							row.createCell(15).setCellValue(1);
						}else{
							row.createCell(15).setCellValue(0);
						}
						row.createCell(16).setCellValue("ZTP");
						row.createCell(17).setCellValue(tx.getExternalNotes()!=null?tx.getExternalNotes():"");
						row.createCell(18).setCellValue(tx.getMarketPlaceNotes()!=null?tx.getMarketPlaceNotes():"");
						row.createCell(19).setCellValue(tx.getMaxShowing()!=null?tx.getMaxShowing():0);
						row.createCell(20).setCellValue(tx.getSeatLow()!=null?tx.getSeatLow():"");
						row.createCell(21).setCellValue(tx.getSeatHigh()!=null?tx.getSeatHigh():"");
						row.createCell(22).setCellValue(tx.getNearTermDisplayOption()!=null?tx.getNearTermDisplayOption():"");
						row.createCell(23).setCellValue(eventDtls.getEventName()!=null?eventDtls.getEventName():"");
						row.createCell(24).setCellValue(eventDtls.getEventDateStr()!=null?eventDtls.getEventDateStr():"");
						row.createCell(25).setCellValue(eventDtls.getEventTimeStr()!=null?eventDtls.getEventTimeStr():"");
						row.createCell(26).setCellValue(eventDtls.getBuilding()!=null?eventDtls.getBuilding():"");
						row.createCell(27).setCellValue("Category");
						row.createCell(28).setCellValue(tx.getBrokerId());
						i++;
					}
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=tickets.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Get Category tickets for event
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/getCategoryTicketGroupById")
	public CategoryTicketGroupDTO getCategoryTicketGroup(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		CategoryTicketGroupDTO categoryTicketGroupDTO = new CategoryTicketGroupDTO();
		Error error = new Error();
		
		try{
			Integer ticketId = 0;
			String ticketIdStr = request.getParameter("ticketId");
			
			if(StringUtils.isEmpty(ticketIdStr)){
				System.err.println("Please provide Ticket Id.");
				error.setDescription("Please provide Ticket Id.");
				categoryTicketGroupDTO.setError(error);
				categoryTicketGroupDTO.setStatus(0);
				return categoryTicketGroupDTO;
			}			
			try {
				ticketId = Integer.parseInt(ticketIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Ticket Id.");
				error.setDescription("Please provide valid Ticket Id.");
				categoryTicketGroupDTO.setError(error);
				categoryTicketGroupDTO.setStatus(0);
				return categoryTicketGroupDTO;
			}
			
			CategoryTicketGroup tx = DAORegistry.getCategoryTicketGroupDAO().getActiveCategoryTicketById(ticketId);
			if(tx == null){
				categoryTicketGroupDTO.setMessage("No Category Ticket found for selected Ticket.");
			}
			categoryTicketGroupDTO.setStatus(1);
			categoryTicketGroupDTO.setCategoryTicketGroup(tx);
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Tickets.");
			categoryTicketGroupDTO.setError(error);
			categoryTicketGroupDTO.setStatus(0);
		}
		return categoryTicketGroupDTO;
	}
	
	/**
	 * Save shipping address
	 * @param addShippingAddress
	 * @param request
	 * @param response
	 * @param map
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/AddCategoryTickets",method=RequestMethod.POST)
	public GenericResponseDTO saveShippingAddress(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		//model.addAttribute("genericResponseDTO", genericResponseDTO);
		
		try{			
			String brokerIdStr = request.getParameter("brokerId");
			String productType = request.getParameter("productType");			
			String action = request.getParameter("action");
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide BrokerId");
				error.setDescription("Please provide BrokerId");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			Integer brokerId = 0;	
			try{
				brokerId = Integer.parseInt(brokerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid BrokerId");
				error.setDescription("Please provide valid BrokerId");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			if(action != null && action.equals("create")){
				String ticketId = request.getParameter("id");				
				String username = request.getParameter("userName");
												
				CategoryTicketGroup catTicket = null;
				if(ticketId != null && ticketId.trim().length() > 0) {
					catTicket = DAORegistry.getCategoryTicketGroupDAO().getActiveCategoryTicketById(Integer.parseInt(ticketId));
					if(catTicket == null) {
						System.err.println("Please provide valid TicketId");
						error.setDescription("Please provide valid TicketId");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
				} else {					
					catTicket = new CategoryTicketGroup();
					catTicket.setProducttype(ProductType.MANUAL);
					catTicket.setStatus(TicketStatus.ACTIVE);
					catTicket.setCreatedDate(new Date());
					catTicket.setCreatedBy(username);
				}
				
				String eventIdStr = request.getParameter("eventId");
				String section = request.getParameter("section");
				String row = request.getParameter("row");
				String sectionRange = request.getParameter("sectionRange");
				String rowRange = request.getParameter("rowRange");
				String seatLow = request.getParameter("seatLow");
				String seatHigh = request.getParameter("seatHigh");
				Integer quantity = 0;
				String quantityStr = request.getParameter("quantity");
				Double price = 0.0;
				String priceStr = request.getParameter("price");
				String shippingMethod = request.getParameter("shippingMethod");
				String nearTermDisplayOption = request.getParameter("nearTermDisplayOption");
				String internalNotes = request.getParameter("internalNotes");
				String externalNotes = request.getParameter("externalNotes");
				String marketPlaceNotes = request.getParameter("marketPlaceNotes");
				String broadcastStr = request.getParameter("broadcast");
				Double taxAmount=0.0;
				String taxAmountStr = request.getParameter("taxAmount");
				Boolean broadcast = true;
				
				if(StringUtils.isEmpty(eventIdStr)){				
					System.err.println("Please provide valid Event Id.");
					error.setDescription("Please provide valid Event Id.");
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					return genericResponseDTO;
				}
				
				if(section==null || section.isEmpty()){
					//return "Section cannot be empty or Null, Please add valid section.";
					System.err.println("Section cannot be empty or Null, Please add valid section.");
					error.setDescription("Section cannot be empty or Null, Please add valid section.");
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					return genericResponseDTO;
				}
				
				if(quantityStr==null || quantityStr.isEmpty()){
					System.err.println("Quantity cannot be empty or Null, Please add valid Quantity.");
					error.setDescription("Quantity cannot be empty or Null, Please add valid Quantity.");
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					return genericResponseDTO;
				}
				try{
					quantity = Integer.parseInt(quantityStr);
				}catch (Exception e) {
					System.err.println("Please provide valid Quantity");
					error.setDescription("Please provide valid Quantity");
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					return genericResponseDTO;
				}
				
				if(priceStr==null || priceStr.isEmpty()){
					System.err.println("Price cannot be empty or Null, Please add valid Price.");
					error.setDescription("Price cannot be empty or Null, Please add valid Price.");
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					return genericResponseDTO;
				}
				try{
					price = Double.parseDouble(priceStr);
				}catch (Exception e) {
					System.err.println("Please provide valid Price");
					error.setDescription("Please provide valid Price");
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					return genericResponseDTO;
				}
				
				if(taxAmountStr==null || taxAmountStr.isEmpty()){
					taxAmountStr ="0";
					/*System.err.println("Tax Amount cannot be empty or Null, Please add valid Tax Amount.");
					error.setDescription("Tax Amount cannot be empty or Null, Please add valid Tax Amount.");
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					return genericResponseDTO;*/
				}
				try{
					taxAmount = Double.parseDouble(taxAmountStr);
				}catch (Exception e) {
					System.err.println("Please provide valid Tax Amount");
					error.setDescription("Please provide valid Tax Amount");
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					return genericResponseDTO;
				}
				/*if(taxAmountStr!=null && !taxAmountStr.isEmpty()){
					taxAmount = Double.parseDouble(taxAmountStr);
				}*/
				
				if(broadcastStr.equals("0")) {
					broadcast = false;
				}
				
				
				if(shippingMethod == null || shippingMethod.isEmpty()){
					//return "Shipping Method cannot be empty or Null, Please select valid shupping method.";
					System.err.println("Shipping Method cannot be empty or Null, Please select valid shipping method.");
					error.setDescription("Shipping Method cannot be empty or Null, Please select valid shipping method.");
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					return genericResponseDTO;
				}
				
				//ShippingMethod shipMethod = DAORegistry.getShippingMethodDAO().get(Integer.parseInt(shippingMethod));
				
				catTicket.setBrokerId(brokerId);
				catTicket.setTaxAmount(taxAmount);
				catTicket.setEventId(Integer.parseInt(eventIdStr));
				catTicket.setSection(section);
				catTicket.setRow(row);
				catTicket.setSectionRange(sectionRange);
				catTicket.setRowRange(rowRange);
				catTicket.setSeatLow(seatLow);
				catTicket.setSeatHigh(seatHigh);
				catTicket.setQuantity(quantity);
				catTicket.setPrice(price);
				if(Integer.parseInt(shippingMethod) >= 0){
					ShippingMethod shipMethod = DAORegistry.getShippingMethodDAO().get(Integer.parseInt(shippingMethod));				
					if(shipMethod!=null){
						catTicket.setShippingMethodId(shipMethod.getId());
						catTicket.setShippingMethod(shipMethod.getName());
					}
				}else{
					catTicket.setShippingMethodId(null);
					catTicket.setShippingMethod(null);
				}
				catTicket.setNearTermDisplayOption(nearTermDisplayOption);
				catTicket.setLastUpdatedDate(new Date());
				catTicket.setInternalNotes(internalNotes);
				catTicket.setExternalNotes(externalNotes);
				catTicket.setMarketPlaceNotes(marketPlaceNotes);
				catTicket.setBroadcast(broadcast);
				
				DAORegistry.getCategoryTicketGroupDAO().saveOrUpdate(catTicket);
				List<CategoryTicket> catTickets = DAORegistry.getCategoryTicketDAO().getCategoryTicketByCategoryTicketGroupId(catTicket.getId());
				if(catTickets!=null && !catTickets.isEmpty()){
					DAORegistry.getCategoryTicketDAO().deleteAll(catTickets);
				}
				catTickets = new ArrayList<CategoryTicket>();
				CategoryTicket ticket = null;
				for(int i=0;i<catTicket.getQuantity();i++){
					ticket = new CategoryTicket();
					ticket.setActualPrice(catTicket.getPrice());
					ticket.setCategoryTicketGroupId(catTicket.getId());
					ticket.setExchangeRequestId(null);
					ticket.setFillDate(null);
					ticket.setInvoiceId(catTicket.getInvoiceId());
					ticket.setIsProcessed(false);
					ticket.setTicketId(null);
					ticket.setUserName(username);
					ticket.setSoldPrice(catTicket.getSoldPrice());
					catTickets.add(ticket);
				}
				DAORegistry.getCategoryTicketDAO().saveAll(catTickets); 
				
				genericResponseDTO.setStatus(1);
				genericResponseDTO.setMessage("Category Tickets added successfully");
				
				//Tracking User Action
				String userActionMsg = "";
				if(ticketId != null && ticketId.trim().length() > 0) {
					userActionMsg = "Category Ticket(s) Updated Successfully.";
				}else{
					userActionMsg = "Category Ticket(s) Added Successfully.";
				}
				Util.userActionAudit(request, catTicket.getId(), userActionMsg);
			}
		}catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while saving category ticket.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
	
	@RequestMapping(value = "/UploadTickets")
	public String uploadTickets(HttpServletRequest request, HttpServletResponse response, Model model){		
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		model.addAttribute("genericResponseDTO", genericResponseDTO);
		String message = "";
		String msg = "";
		
		try {
			String brokerIdStr = request.getParameter("brokerId");
			String productType = request.getParameter("productType");
			String eventIdStr = request.getParameter("eventId");
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide BrokerId");
				error.setDescription("Please provide BrokerId");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				//return genericResponseDTO;
			}
			if(StringUtils.isEmpty(eventIdStr)){				
				System.err.println("Please provide valid Event Id.");
				error.setDescription("Please provide valid Event Id.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				//return genericResponseDTO;
			}
			
			Integer brokerId = 0;	
			try{
				brokerId = Integer.parseInt(brokerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid BrokerId");
				error.setDescription("Please provide valid BrokerId");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				//return genericResponseDTO;
			}
						
			//String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			String userName = request.getParameter("trackerSessionUser");
			
			File f = null;
			DiskFileItemFactory  fileItemFactory = new DiskFileItemFactory ();
			File ticketFile = new File(URLUtil.TICKET_DIRECTORY);
	        fileItemFactory.setRepository(ticketFile);
	        ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
	        List items = uploadHandler.parseRequest(request);
	        Iterator iterator = items.iterator();
	        List<CategoryTicketGroup> ticketGroups = new ArrayList<CategoryTicketGroup>();
	        String ext ="";
	        boolean isQtyMissing = false;
	        boolean isSectionMissing = false;
	        boolean isCostMissing = false;
	        boolean isIDMissing = false;
	        ShippingMethod sm = DAORegistry.getShippingMethodDAO().getShippingMethodByName("E-Ticket");
	        	        
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			
			if(msg.isEmpty()){
				MultipartFile file = multipartRequest.getFile("ticketFile");
				if(file!=null){
					ext = FilenameUtils.getExtension(file.getOriginalFilename());
					f = new File(URLUtil.TICKET_DIRECTORY+eventIdStr+"_"+brokerId+"."+ext);
					f.createNewFile();
					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(f));
			        FileCopyUtils.copy(file.getInputStream(), stream);
					stream.close();
				}else{
					msg="File not found";
				}
				/*Iterator iterator1 = items.iterator();
				while(iterator1.hasNext()){
		        	FileItem item = (FileItem)iterator1.next();
		        	if(!item.isFormField()){
		        		ext = FilenameUtils.getExtension(file.getOriginalFilename());
		        		f = new File(com.rtw.tracker.utils.Constants.Real_ticket_Path+eventIdStr+"_"+brokerId+"."+ext);
		        		item.write(f);
		        	}
		        }*/
				
			}
	       
			CategoryTicketGroup tg = null;
			if(msg.isEmpty() && f!=null && f.length() > 0){
				if(ext.equalsIgnoreCase("csv")){
					Scanner scanner = new Scanner(f);
			        while (scanner.hasNext()) {
			            String line = scanner.nextLine();
			            String arr[] = null;
			            System.out.println("====================================");
			            if(line!=null && !line.isEmpty()){
			            	arr = line.split(",");
			            	
			            }
			            if(arr[0]==null || !arr[0].equalsIgnoreCase("Y")){
			            	continue;
			            }
			            if(arr.length> 0){
			            	 String eventName = null;
		            		 String eventDate = null;
		            		 String eventTime = null;
		            		 String venueName = null;
			            	 tg = new CategoryTicketGroup();
			            	 for(int i=0;i<arr.length;i++){
		            			switch(i){
			            		case 0:
			                		System.out.println(arr[i]);
			                		break;
			                	case 1:
			                		eventName = arr[i];
			                		System.out.println(arr[i]);
			                		break;
			                	case 2:
			                		venueName = arr[i];
			                		System.out.println(arr[i]);
			                		break;
			                	case 3:
			                		eventDate = arr[i];
			                		System.out.println(arr[i]);
			                		break;
			                	case 4:
			                		eventTime = arr[i];
			                		System.out.println(arr[i]);
			                		break;
			                	case 5:
			                		if(arr[i]!=null && !arr[i].isEmpty()){
			                			try {
			                				tg.setQuantity(Integer.parseInt(arr[i]));
										} catch (Exception e) {
											e.printStackTrace();
											isQtyMissing =true;
										}
			                		}else{
			                			isQtyMissing =true;
			                		}
			                		System.out.println(arr[i]);
			                		break;
			                	case 6:
			                		if(arr[i]!=null && !arr[i].isEmpty()){
			                			tg.setSection(arr[i]);
			                		}else{
			                			isSectionMissing = true;
			                		}
			                		System.out.println(arr[i]);
			                		break;
			                	case 7:
			                		tg.setRow(arr[i]);
			                		System.out.println(arr[i]);
			                		break;
			                	case 8:
			                		tg.setInternalNotes(arr[i]);
			                		System.out.println(arr[i]);
			                		break;
			                	case 9:
			                		if(arr[i]!=null && !arr[i].isEmpty()){
			                			try {
			                				tg.setPrice(Double.parseDouble(arr[i]));
			                				tg.setCost(Double.parseDouble(arr[i]));
			                				tg.setRetailPrice(Double.parseDouble(arr[i]));
			                				
										} catch (Exception e) {
											e.printStackTrace();
											isCostMissing =true;
										}
			                		}else{
			                			isCostMissing =true;
			                		}
			                		System.out.println(arr[i]);
			                		break;
			                	case 10:
			                		/*cell.setCellType(Cell.CELL_TYPE_STRING);
			                		if(cell.getStringCellValue()!=null && !cell.getStringCellValue().isEmpty()){
			                			try {
			                				
										} catch (Exception e) {
											e.printStackTrace();
											isIDMissing =true;
										}
			                		}else{
			                			isIDMissing =true;
			                		}*/
			                		System.out.println(arr[i]);
			                		break;
			                	case 11:
			                		if(arr[i]!=null && !arr[i].isEmpty()){
			                			tg.setTicketOnhandStatus("Yes");
			                		}
			                		System.out.println(arr[i]);
			                		break;
			                	case 12:
			                		if(arr[i]!=null && arr[i].equalsIgnoreCase("Y")){
			                			tg.setShippingMethodId(sm.getId());
			                		}
			                		System.out.println(arr[i]);
			                		break;
			                	case 13:
			                		System.out.println(arr[i]);
			                		break;
			                	
		            			}
			            	}
			            	 if(eventName == null || eventName.isEmpty()){
		            				continue;
		            			}
		            			if(venueName == null || venueName.isEmpty()){
		            				continue;
		            			}
		            			if(eventDate == null || eventDate.isEmpty()){
		            				continue;
		            			}
		            			if(eventTime==null || eventTime.isEmpty()){
		            				continue;
		            			}
		            			eventTime = "01/01/1970 "+eventTime;
		            			Integer eventId = DAORegistry.getEventDetailsDAO().getActiveEventIdByEventDateTimeVenue(eventName,venueName,eventDate,eventTime);
		            			if(eventId==null){
		            				continue;
		            			}
		            			List<String> list=TMATDAORegistry.getCategoryMappingDAO().getCategoryByEventIdAndZone(eventId,tg.getSection());
		            			if(list.isEmpty() && list.size() < 4){
		            				continue;
		            			}
		            			tg.setSectionRange(list.get(2));
		            			tg.setRowRange(list.get(3));
		            			tg.setBroadcast(true);
				                tg.setBrokerId(brokerId);
				                tg.setPlatform(ApplicationPlatform.TICK_TRACKER.toString());
		        				tg.setCreatedBy(userName);
		        				tg.setCreatedDate(new Date());
		        				tg.setEventId(eventId);
		        				tg.setLastUpdatedDate(new Date());
		        				tg.setProducttype(ProductType.REWARDTHEFAN);
		        				tg.setStatus(TicketStatus.ACTIVE);
		        				tg.setTaxAmount(0.000);
		        				ticketGroups.add(tg);
			            }
			            
			        }
			        scanner.close();
				}else if(ext.equalsIgnoreCase("xls") || ext.equalsIgnoreCase("xlsx")){
					FileInputStream stream = new FileInputStream(f);
					Workbook wb = new XSSFWorkbook(stream);
					Sheet firstSheet = wb.getSheetAt(0);
			        Iterator<Row> it = firstSheet.iterator();
			        
			        while (it.hasNext()) {
			            Row nextRow = it.next();
			            if(nextRow.getRowNum()==0){
			            	continue;
			            }
			            System.out.println("=============================================");
			            tg = new CategoryTicketGroup();
			            Iterator<Cell> cellIterator = nextRow.cellIterator();
			            while (cellIterator.hasNext()) {
			                Cell cell = cellIterator.next();
			                switch (cell.getColumnIndex()){
			                	case 0:
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 1:
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 2:
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 3:
			                		if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC){
			                			System.out.println(cell.getDateCellValue());
			                		}
			                		break;
			                	case 4:
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 5:
			                		cell.setCellType(Cell.CELL_TYPE_STRING);
			                		if(cell.getStringCellValue()!=null && !cell.getStringCellValue().isEmpty()){
			                			try {
			                				tg.setQuantity(Integer.parseInt(cell.getStringCellValue()));
										} catch (Exception e) {
											e.printStackTrace();
											isQtyMissing =true;
										}
			                		}else{
			                			isQtyMissing =true;
			                		}
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 6:
			                		if(cell.getStringCellValue()!=null && !cell.getStringCellValue().isEmpty()){
			                			tg.setSection(cell.getStringCellValue());
			                		}else{
			                			isSectionMissing = true;
			                		}
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 7:
			                		tg.setRow(cell.getStringCellValue());
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	/*case 8:
			                		tg.setSeatLow(cell.getStringCellValue());
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 9:
			                		tg.setSeatHigh(cell.getStringCellValue());
			                		System.out.println(cell.getStringCellValue());
			                		break;*/
			                	case 8:
			                		tg.setInternalNotes(cell.getStringCellValue());
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 9:
			                		cell.setCellType(Cell.CELL_TYPE_STRING);
			                		if(cell.getStringCellValue()!=null && !cell.getStringCellValue().isEmpty()){
			                			try {
			                				tg.setPrice(Double.parseDouble(cell.getStringCellValue()));
			                				tg.setCost(Double.parseDouble(cell.getStringCellValue()));
			                				tg.setRetailPrice(Double.parseDouble(cell.getStringCellValue()));
			                				
										} catch (Exception e) {
											e.printStackTrace();
											isCostMissing =true;
										}
			                		}else{
			                			isCostMissing =true;
			                		}
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 10:
			                		/*cell.setCellType(Cell.CELL_TYPE_STRING);
			                		if(cell.getStringCellValue()!=null && !cell.getStringCellValue().isEmpty()){
			                			try {
			                				
										} catch (Exception e) {
											e.printStackTrace();
											isIDMissing =true;
										}
			                		}else{
			                			isIDMissing =true;
			                		}*/
			                		System.out.println(cell.getNumericCellValue());
			                		break;
			                	case 11:
			                		if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC && cell.getDateCellValue()!=null){
			                			tg.setTicketOnhandStatus("Yes");
			                		}
			                		System.out.println(cell.getDateCellValue());
			                		break;
			                	case 12:
			                		if(cell.getStringCellValue()!=null && !cell.getStringCellValue().equalsIgnoreCase("Y")){
			                			tg.setShippingMethodId(sm.getId());
			                		}
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 13:
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	
			                }
			                tg.setBroadcast(true);
			                tg.setBrokerId(brokerId);
			                tg.setPlatform(ApplicationPlatform.TICK_TRACKER.toString());
            				tg.setCreatedBy(userName);
            				tg.setCreatedDate(new Date());
            				tg.setEventId(Integer.parseInt(eventIdStr));
            				tg.setLastUpdatedDate(new Date());
            				tg.setProducttype(ProductType.REWARDTHEFAN);
            				tg.setTaxAmount(0.000);
            				tg.setStatus(TicketStatus.ACTIVE);
            				ticketGroups.add(tg);
			            }
			        }
				}
				
				if(!ticketGroups.isEmpty()){
		        	if(isCostMissing){
		        		 msg = "Cost is missing in some of the tickets.";
		        	}else if(isQtyMissing){
		        		 msg = "Quantity is missing in some of the tickets.";
		        	}else if(isSectionMissing){
		        		 msg = "Section is missing in some of the tickets.";
		        	}else{
		        		List<CategoryTicketGroup> finalInsertList = new ArrayList<CategoryTicketGroup>();
		        		List<CategoryTicketGroup> finalUpdateList = new ArrayList<CategoryTicketGroup>();
		        		try {
		        			for(CategoryTicketGroup ticGrp : ticketGroups){
		        				CategoryTicketGroup dbTg = DAORegistry.getCategoryTicketGroupDAO().getDuplicateCategoryTicketGroupForBroker(ticGrp.getSection(),ticGrp.getQuantity(),ticGrp.getEventId(),brokerId);
								if(dbTg == null){
									finalInsertList.add(ticGrp);
								}else{
									dbTg.setPrice(ticGrp.getPrice());
									dbTg.setCost(ticGrp.getCost());
									dbTg.setRetailPrice(ticGrp.getRetailPrice());
									dbTg.setLastUpdatedDate(new Date());
									finalUpdateList.add(dbTg);
								}
		        			}
							DAORegistry.getCategoryTicketGroupDAO().saveAll(finalInsertList);
							DAORegistry.getCategoryTicketGroupDAO().updateAll(finalUpdateList);
						} catch (Exception e) {
							msg = "Something went wrong, Please try again.";
							e.printStackTrace();
						}
						if(msg.isEmpty()){
							List<CategoryTicket> insertList = new ArrayList<CategoryTicket>();
							List<CategoryTicket> updateList = new ArrayList<CategoryTicket>();
							CategoryTicket tic = null;
							for(CategoryTicketGroup ticGrp : finalInsertList){
								for(int i=1;i<=ticGrp.getQuantity();i++){
									tic = new CategoryTicket();
									tic.setCategoryTicketGroupId(ticGrp.getId());
									tic.setActualPrice(tg.getPrice());
									tic.setIsProcessed(true);
									tic.setPosition(i);
									tic.setUserName(tg.getCreatedBy());
									insertList.add(tic);
								}
							}
							for(CategoryTicketGroup tGroup : finalUpdateList){
								List<CategoryTicket> tics =DAORegistry.getCategoryTicketDAO().getCategoryTicketByCategoryTicketGroupId(tGroup.getId());
								for(CategoryTicket t : tics){
									t.setActualPrice(tGroup.getPrice());
								}
								updateList.addAll(tics);
							}
							DAORegistry.getCategoryTicketDAO().saveAll(insertList);
							DAORegistry.getCategoryTicketDAO().updateAll(updateList);
							msg = finalInsertList.size()+" Tickets are Created and "+finalUpdateList.size()+" are Updated successfully.";
							
							//Tracking User Action
							Util.userActionAudit(request, Integer.parseInt(eventIdStr), "Ticket(s) are Uploaded Successfully.");
						}
						
		        	}
		        }
			}else{
				msg = "No Data Found in File.";
			}
			
			//IOUtils.write(msg.getBytes(), response.getOutputStream());
		} catch (Exception e) {			
			e.printStackTrace();
			error.setDescription("Something went wrong while Uploading Tickets.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		genericResponseDTO.setStatus(1);
		genericResponseDTO.setMessage(message);
		return "";
	}
	
	@RequestMapping(value = "/UpdateTicketDetails")
	public GenericResponseDTO updateTicketDetails(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		//model.addAttribute("genericResponseDTO", genericResponseDTO);
		String message = "";		
		
		try{
			String brokerIdStr = request.getParameter("brokerId");
			String productType = request.getParameter("productType");			
			String ticketIdStr = request.getParameter("ticketIds");
			String broadCastStr = request.getParameter("broadcast");
			String eventIdStr = request.getParameter("eventId");
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide BrokerId");
				error.setDescription("Please provide BrokerId");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
						
			Integer brokerId = 0;	
			try{
				brokerId = Integer.parseInt(brokerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid BrokerId");
				error.setDescription("Please provide valid BrokerId");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			if(eventIdStr != null && !eventIdStr.isEmpty()){
				Integer eventId = Integer.parseInt(eventIdStr);
				Boolean broadCast = Boolean.parseBoolean(broadCastStr);
				
				AutoCatsLockedTickets lockedTickets = DAORegistry.getAutoCatsLockedTicketsDAO().getLockedTicketsByEventId(eventId);
				if(lockedTickets == null){
					//Update Ticket Group
					DAORegistry.getTicketGroupDAO().updateBroadCastByEventId(eventId, broadCast);
					//Update Category Ticket Group
					DAORegistry.getCategoryTicketGroupDAO().updateBroadcastByEventId(eventId, broadCast);
					genericResponseDTO.setMessage("Event - Ticket Details Updated.");
					genericResponseDTO.setStatus(1);
					
					//Tracking User Action
					String userActionMsg = "";
					if(broadCast){
						userActionMsg = "Event - All Ticket's are BroadCasted.";
					}else{
						userActionMsg = "Event - All Ticket's are UnBroadCasted.";
					}
					Util.userActionAudit(request, eventId, userActionMsg);
				}else{
					//jsonObject.put("eventMessage", "Event - 1 or More Ticket(s) has been locked from User.");
				}
			}
			if(ticketIdStr != null && !ticketIdStr.isEmpty()){
				String[] ticketIdsArr = null;
				if(ticketIdStr != null) {
					ticketIdsArr = ticketIdStr.split(",");
				}
				Set<Integer> ticketIdSet = new HashSet<Integer>();
				TicketGroup ticketGroup = null;
				CategoryTicketGroup catTicketGroup = null;
				Collection<TicketGroup> ticketGroupList = new ArrayList<TicketGroup>();
				Collection<CategoryTicketGroup> catTicketGroupList = new ArrayList<CategoryTicketGroup>();
				AutoCatsLockedTickets lockedTickets = null;
				Boolean lockedTicketsFlag = false;
				
				for (String ticketIds : ticketIdsArr) {
					if(ticketIds != null && !ticketIds.isEmpty()){
						lockedTickets = DAORegistry.getAutoCatsLockedTicketsDAO().getLockedTicketsByCategoryTicketGroupId(Integer.parseInt(ticketIds));
						if(lockedTickets != null){
							lockedTicketsFlag = true;
							break;
						}
					}
				}
				
				if(!lockedTicketsFlag){
					for (String ticketIds : ticketIdsArr) {
						if(ticketIds != null && !ticketIds.isEmpty()){
							Integer ticketId = Integer.parseInt(ticketIds);
							if(ticketIdSet.add(ticketId)){
								Boolean broadCast = Boolean.parseBoolean(broadCastStr);
								ticketGroup = DAORegistry.getTicketGroupDAO().get(ticketId);
								if(ticketGroup != null){
									ticketGroup.setBroadcast(broadCast);
									ticketGroupList.add(ticketGroup);
								}else{
									catTicketGroup = DAORegistry.getCategoryTicketGroupDAO().get(ticketId);
									if(catTicketGroup != null){
										catTicketGroup.setBroadcast(broadCast);
										catTicketGroupList.add(catTicketGroup);
									}
								}
							}
						}
					}
					if(ticketGroupList != null && ticketGroupList.size() >0){
						DAORegistry.getTicketGroupDAO().updateAll(ticketGroupList);
					}
					if(catTicketGroupList != null && catTicketGroupList.size() > 0){
						DAORegistry.getCategoryTicketGroupDAO().updateAll(catTicketGroupList);
					}
					//jsonObject.put("ticketMessage", "Ticket Details Updated.");
					genericResponseDTO.setMessage("Ticket Details Updated.");
					genericResponseDTO.setStatus(1);
					
					//Tracking User Action
					String userActionMsg = "";
					Integer eventId = null;
					if(Boolean.parseBoolean(broadCastStr)){
						userActionMsg = "Ticket(s) are BroadCasted.Ticket Id's - "+ticketIdStr;
					}else{
						userActionMsg = "Ticket(s) are UnBroadCasted.Ticket Id's - "+ticketIdStr;
					}
					if(ticketGroup != null){
						eventId = ticketGroup.getEventId();
					}else if(catTicketGroup != null){
						eventId = catTicketGroup.getEventId();
					}
					Util.userActionAudit(request, eventId, userActionMsg);
					
				}else{					
					error.setDescription("1 or More ticket(s) has been locked from User.");
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					//jsonObject.put("ticketMessage", "1 or More ticket(s) has been locked from User.");
				}
			}
			/*response.setHeader("Content-type","application/json");
			IOUtils.write(jsonObject.toString().getBytes(),response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while updating BroadCast/UnBroadCast Tickets.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
	@RequestMapping(value = "/CheckLockedTickets")
	public GenericResponseDTO checkLockedTickets(HttpServletRequest request, HttpServletResponse response, HttpSession session,Model model){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		//model.addAttribute("genericResponseDTO", genericResponseDTO);

		try{
			String ticketIdStr = request.getParameter("ticketId");
			if(StringUtils.isEmpty(ticketIdStr)){
				System.err.println("Please provide TicketId");
				error.setDescription("Please provide TicketId");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
						
			Integer ticketId = 0;	
			try{
				ticketId = Integer.parseInt(ticketIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid TicketId");
				error.setDescription("Please provide valid TicketId");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
									
			AutoCatsLockedTickets lockedTickets = DAORegistry.getAutoCatsLockedTicketsDAO().getLockedTicketsByCategoryTicketGroupId(ticketId);
			if(lockedTickets != null){
				genericResponseDTO.setMessage("Ticket cannot be edit, user locked the ticket.");
			}else{
				genericResponseDTO.setMessage("true");
			}
			
			genericResponseDTO.setStatus(1);
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while checking Locking Tickets.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
	@RequestMapping(value = "/GetShippingMethod")
	public String checkLockedTickets(HttpServletRequest request, HttpServletResponse response, Model model){		
		ShippingMethodDTO shippingMethodDTO = new ShippingMethodDTO();
		Error error = new Error();
		model.addAttribute("shippingMethodDTO", shippingMethodDTO);
		try{
			String productType = request.getParameter("productType");
			shippingMethodDTO.setShippingMethodList(DAORegistry.getShippingMethodDAO().getAll());
			shippingMethodDTO.setStatus(1);
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while getting shipping methods.");
			shippingMethodDTO.setError(error);
			shippingMethodDTO.setStatus(0);
		}
		return "";
	}
	
	@RequestMapping(value = "/RtfCatsEventsList")
	public RtfCatsEventsDTO getRtfCatsEventsList(HttpServletRequest request, HttpServletResponse response){
		RtfCatsEventsDTO eventDTO = new RtfCatsEventsDTO();
		Error error = new Error();
		try{	
			
			String selectedIdStr = request.getParameter("selectedId");
			String selectedTypeStr = request.getParameter("selectedType");
			String dataType = request.getParameter("dataType");
//			String childStr = request.getParameter("child");
			String selectedEvent = "";
			String selectedValue = "";
			String selectedOption = "";
			Integer selectedId = null;
			
			Integer artistId=null,venueId=null,grandChildId=null;
			if(selectedTypeStr != null) {
				if(selectedTypeStr.equals("ARTIST")){
					com.rtw.tmat.data.Artist artist = TMATDAORegistry.getArtistDAO().get(Integer.parseInt(selectedIdStr));
					//events = TMATDAORegistry.getEventDAO().getAllActiveEventsByArtistId(Integer.parseInt(selectedIdStr));
					//dbMiniExchangeEventList  = TMATDAORegistry.getAutoCats96ExchangeEventDAO().getAllAutoCats96ExchangeEventsByArtistId(Integer.valueOf(artistStr));
					selectedValue= artist.getName();
					artistId = artist.getId();
				}else if(selectedTypeStr.equals("VENUE")){
					com.rtw.tmat.data.Venue venue = TMATDAORegistry.getVenueDAO().get(Integer.parseInt(selectedIdStr));
					//events = TMATDAORegistry.getEventDAO().getAllEventsByVenue(Integer.parseInt(selectedIdStr));
					selectedValue= venue.getBuilding();
					venueId = venue.getId();
				}/*else if(grandChildStr!=null && !grandChildStr.isEmpty()){
					GrandChildCategory grandChildCategory = TMATDAORegistry.getGrandChildCategoryDAO().get(Integer.parseInt(grandChildStr));
					dbMiniExchangeEventList  = TMATDAORegistry.getAutoCats96ExchangeEventDAO().getAllAutoCats96ExchangeEventsByGrandChildCategoryId(Integer.valueOf(grandChildStr));
					selectedOption="GrandChildCategory";
					selectedValue= grandChildCategory.getName();
					events = TMATDAORegistry.getEventDAO().getAllEventsByGrandChildCategoryOrderdByEventDate(Integer.parseInt(grandChildStr));
					selectedId = grandChildCategory.getId();
				}*/
			} 
			if(selectedTypeStr == null || selectedValue == null || selectedValue.isEmpty()){
				eventDTO.setStatus(0);
				eventDTO.setMessage("Not able to identfy Filter, pelase send valid Filter to load events.");
				return eventDTO;
			}
			
			String headerFilter = request.getParameter("headerFilter");
			String pageNo = request.getParameter("pageNo");
			Integer count = 0;
			Integer excludeContestEventCount = 0;
			
			if(dataType == null || dataType.equals("ALL") || dataType.equals("ACTIVE")) {
				GridHeaderFilters filter = GridHeaderFiltersUtil.getRtfCatsEventFilter(headerFilter);
				List<RewardthefanCatsExchangeEvent> allEventList = TMATDAORegistry.getRewardthefanCatsExchangeEventDAO().getAllRtfCatsEvents(filter, artistId, venueId, grandChildId, pageNo, false, true);
				count = TMATDAORegistry.getRewardthefanCatsExchangeEventDAO().getAllRtfCatsEventsCount(filter, artistId, venueId, grandChildId, pageNo);
				
				if(allEventList == null || allEventList.size() <= 0){
					eventDTO.setMessage("No Events found for selected Option.");
				}
				
				eventDTO.setAllEventList(allEventList);
				eventDTO.setPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
			}
			
			if(dataType == null || dataType.equals("ALL") || dataType.equals("EXCLUDE")) {
				
				GridHeaderFilters filter = new GridHeaderFilters();
				if(dataType != null && dataType.equals("EXCLUDE")){
					filter = GridHeaderFiltersUtil.getRtfCatsEventFilter(headerFilter);
				}
				
				List<RewardthefanCatsExchangeEvent> excludeContestEventList = TMATDAORegistry.getRewardthefanCatsExchangeEventDAO().getAllRtfCatsEvents(filter, artistId, venueId, grandChildId, pageNo, true, false);
				
				if(excludeContestEventList != null && excludeContestEventList.size() > 0){
					excludeContestEventCount = excludeContestEventList.size();
				} else if(dataType != null && dataType.equals("EXCLUDE")){
					eventDTO.setMessage("No Exclude Events found for selected Contest.");
				}
				eventDTO.setExcludeEventList(excludeContestEventList);
				eventDTO.setExcludeEventPaginationDTO(PaginationUtil.getDummyPaginationParameters(excludeContestEventCount));

			}
			eventDTO.setStatus(1);
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while loading Rtf Cats events.");
			eventDTO.setError(error);
			eventDTO.setStatus(0);
		}		
		return eventDTO;
	}
	
	@RequestMapping(value = "/UpdateRtfCatsEvents")
	public GenericResponseDTO updateRtfCatsEvents(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		try{
			String action = request.getParameter("action");
			//String contestIdStr = request.getParameter("contestId");
			String eventIdStr = request.getParameter("eventIdStr");
			String userName = request.getParameter("userName");
			
			if(eventIdStr == null || eventIdStr.trim().isEmpty()){
				genericResponseDTO.setStatus(0);
				genericResponseDTO.setMessage("Not able to identfy event, pelase send valid event id to load events.");
				return genericResponseDTO;
			}
			
			if(action != null && action.equalsIgnoreCase("EXCLUDE")){
				
				//Insert Contest Events
				List<RewardthefanCatsExchangeEvent> rtfCatsEventsList = new ArrayList<RewardthefanCatsExchangeEvent>();
				List<RewardthefanCatsExchangeEventAudit> auditsList = new ArrayList<RewardthefanCatsExchangeEventAudit>();
				String arr[] = eventIdStr.split(",");
				for(String str : arr){
					if(str != null && !str.isEmpty()){
						Integer eventId = Integer.parseInt(str);
						
						RewardthefanCatsExchangeEvent rtfCatsEvent = TMATDAORegistry.getRewardthefanCatsExchangeEventDAO().getRtfCatsExchangeEventByEventId(eventId);
						if(rtfCatsEvent != null) {
							RewardthefanCatsExchangeEventAudit audit = new RewardthefanCatsExchangeEventAudit(rtfCatsEvent);
							auditsList.add(audit);
							
							rtfCatsEvent.setStatus("DELETED");
							rtfCatsEvent.setLastUpdatedBy(userName);
							rtfCatsEvent.setLastUpdatedDate(new Date());
							
							rtfCatsEventsList.add(rtfCatsEvent);
						}
					}
				}
				TMATDAORegistry.getRewardthefanCatsExchangeEventDAO().updateAll(rtfCatsEventsList);
				TMATDAORegistry.getRewardthefanCatsExchangeEventAuditDAO().saveAll(auditsList);
							
				genericResponseDTO.setStatus(1);			
				genericResponseDTO.setMessage("RTF Cats Events Excluded.");
				
			}else if(action != null && action.equalsIgnoreCase("REMOVEEXCLUDE")){
				
				List<RewardthefanCatsExchangeEvent> rtfCatsEventsList = new ArrayList<RewardthefanCatsExchangeEvent>();
				List<RewardthefanCatsExchangeEventAudit> auditsList = new ArrayList<RewardthefanCatsExchangeEventAudit>();
				String arr[] = eventIdStr.split(",");
				for(String str : arr){
					if(str != null && !str.isEmpty()){
						Integer eventId = Integer.parseInt(str);
						
						RewardthefanCatsExchangeEvent rtfCatsEvent = TMATDAORegistry.getRewardthefanCatsExchangeEventDAO().getRtfCatsExchangeEventByEventId(eventId);
						if(rtfCatsEvent != null) {
							RewardthefanCatsExchangeEventAudit audit = new RewardthefanCatsExchangeEventAudit(rtfCatsEvent);
							auditsList.add(audit);
							
							rtfCatsEvent.setStatus("ACTIVE");
							rtfCatsEvent.setLastUpdatedBy(userName);
							rtfCatsEvent.setLastUpdatedDate(new Date());
							
							rtfCatsEventsList.add(rtfCatsEvent);
						}
					}
				}
				
				TMATDAORegistry.getRewardthefanCatsExchangeEventDAO().updateAll(rtfCatsEventsList);
				TMATDAORegistry.getRewardthefanCatsExchangeEventAuditDAO().saveAll(auditsList);
				
				genericResponseDTO.setStatus(1);			
				genericResponseDTO.setMessage("Removed - Excluded RTF Cats Events.");
			}
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while updating exclude/remove RTF Cats events.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}		
		return genericResponseDTO;
	}


	@RequestMapping("/AutoCompleteArtistAndVenueGChildTMAT")
	public AutoCompleteArtistVenueDTO getAutoCompleteArtistAndVenueGChildTMAT(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		AutoCompleteArtistVenueDTO autoCompleteArtistVenueDTO = new AutoCompleteArtistVenueDTO();
		Error error = new Error();
		
		try {
			String param = request.getParameter("q");
			String strResponse = "";
			
			Collection<com.rtw.tmat.data.Artist> artists = TMATDAORegistry.getArtistDAO().filterByName(param);
			if (artists != null) {
				for (com.rtw.tmat.data.Artist artist : artists) {
					strResponse += ("ARTIST" + "|" + artist.getId() + "|" + artist.getName() + "\n") ;
				}
			}
			Collection<com.rtw.tmat.data.Venue> venues = TMATDAORegistry.getVenueDAO().filterByVenue(param);
			if (venues != null) {
				for (com.rtw.tmat.data.Venue venue : venues) {
					strResponse += ("VENUE" + "|" + venue.getId() + "|" + venue.getLocation() + "\n") ;
				}
			}
			autoCompleteArtistVenueDTO.setArtistVenue(strResponse);
			autoCompleteArtistVenueDTO.setStatus(1);
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Soomething went wrong while fetching Artist and Venue.");
			autoCompleteArtistVenueDTO.setError(error);
			autoCompleteArtistVenueDTO.setStatus(0);
		}
		return autoCompleteArtistVenueDTO;			
	}
	
	public static void main(String[] args) {
		String eventIdStr = "123,";
		if(eventIdStr.contains(",")){
			String arr[] = eventIdStr.split(",");
			for(String str : arr){
				System.out.println("str : "+str);
			}
				
		}
		String arr[] = eventIdStr.split(",");
		for(String str : arr){
			System.out.println("str str : "+str+" : "+arr.length);
		}
	}
}
