package com.rtw.tracker.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtw.tmat.dao.implementation.TMATDAORegistry;
import com.rtw.tmat.data.CrownJewelEvents;
import com.rtw.tmat.data.CrownJewelEventsAudit;
import com.rtw.tmat.utils.Constants;
import com.rtw.tmat.utils.Error;
import com.rtw.tmat.utils.Util;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.ApplicationPlatform;
import com.rtw.tracker.datas.CategoryTicket;
import com.rtw.tracker.datas.CategoryTicketGroup;
import com.rtw.tracker.datas.CrownJewelCategoryTeams;
import com.rtw.tracker.datas.CrownJewelCategoryTeamsAudit;
import com.rtw.tracker.datas.CrownJewelCategoryTicket;
import com.rtw.tracker.datas.CrownJewelCustomerOrder;
import com.rtw.tracker.datas.CrownJewelLeagues;
import com.rtw.tracker.datas.CrownJewelLeaguesAudit;
import com.rtw.tracker.datas.CrownJewelTeamZones;
import com.rtw.tracker.datas.CrownJewelTeamZonesAudit;
import com.rtw.tracker.datas.CrownJewelTeams;
import com.rtw.tracker.datas.CrownJewelTeamsAudit;
import com.rtw.tracker.datas.CrownJewelZonesSoldDetails;
import com.rtw.tracker.datas.Customer;
import com.rtw.tracker.datas.CustomerAddress;
import com.rtw.tracker.datas.CustomerLoyalty;
import com.rtw.tracker.datas.CustomerLoyaltyHistory;
import com.rtw.tracker.datas.CustomerOrder;
import com.rtw.tracker.datas.CustomerOrderDetails;
import com.rtw.tracker.datas.Event;
import com.rtw.tracker.datas.EventDetails;
import com.rtw.tracker.datas.EventStatus;
import com.rtw.tracker.datas.FantasyChildCategory;
import com.rtw.tracker.datas.FantasyGrandChildCategory;
import com.rtw.tracker.datas.FantasyParentCategory;
import com.rtw.tracker.datas.FantasySportsProduct;
import com.rtw.tracker.datas.Invoice;
import com.rtw.tracker.datas.ProductType;
import com.rtw.tracker.datas.ShippingMethod;
import com.rtw.tracker.datas.SuperFanOrderSummary;
import com.rtw.tracker.datas.TicketStatus;
import com.rtw.tracker.datas.Venue;
import com.rtw.tracker.enums.CrownJewelOrderStatus;
import com.rtw.tracker.enums.InvoiceStatus;
import com.rtw.tracker.enums.OrderStatus;
import com.rtw.tracker.enums.OrderType;
import com.rtw.tracker.enums.PaymentMethod;
import com.rtw.tracker.enums.RewardStatus;
import com.rtw.tracker.enums.SecondaryOrderType;
import com.rtw.tracker.enums.ZonesQuantityTypes;
import com.rtw.tracker.fedex.Property;
import com.rtw.tracker.mail.MailManager;
import com.rtw.tracker.pojos.CategoryTicketGroupListDTO;
import com.rtw.tracker.pojos.CreateFantasyOrderDTO;
import com.rtw.tracker.pojos.CrownJewelCustomerOrderDTO;
import com.rtw.tracker.pojos.CrownJewelLeaguesDTO;
import com.rtw.tracker.pojos.CrownJewelOrderSummaryDTO;
import com.rtw.tracker.pojos.CrownJewelOrderUpdateReasonDTO;
import com.rtw.tracker.pojos.CrownJewelOrdersDTO;
import com.rtw.tracker.pojos.CrownJewelTeamZonesDTO;
import com.rtw.tracker.pojos.CrownJewelTeamsDTO;
import com.rtw.tracker.pojos.CustomerLoyaltyDTO;
import com.rtw.tracker.pojos.CustomersCustomDTO;
import com.rtw.tracker.pojos.EventDetailsDTO;
import com.rtw.tracker.pojos.FantasyOrderCreationDTO;
import com.rtw.tracker.pojos.FantasyParentCategoryDTO;
import com.rtw.tracker.pojos.GenericResponseDTO;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.GridHeaderFiltersUtil;
import com.rtw.tracker.utils.JsonWrapperUtil;
import com.rtw.tracker.utils.PaginationUtil;

@Controller
public class CrownJewelEventsController {
	
	MailManager mailManager;
	SharedProperty sharedProperty;
	public MailManager getMailManager() {
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}
	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}
	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}

	@RequestMapping(value="/CrownJewelEvents")
	public CrownJewelLeaguesDTO loadCrownJewelLeagues(HttpServletRequest request, HttpServletResponse response){
		CrownJewelLeaguesDTO crownJewelLeaguesDTO = new CrownJewelLeaguesDTO();
		Error error = new Error();
		//model.addAttribute("crownJewelLeaguesDTO", crownJewelLeaguesDTO);
		
		try {
			String action = request.getParameter("action");
			String childId = request.getParameter("childType");
			String grandChildId = request.getParameter("grandChildType");
			
			if(action != null) {
				String username = request.getParameter("userName");
				if(StringUtils.isEmpty(username)){
					System.err.println("Please Login.");
					error.setDescription("Please Login.");
					crownJewelLeaguesDTO.setError(error);
					crownJewelLeaguesDTO.setStatus(0);
					return crownJewelLeaguesDTO;
				}
				
				Date now = new Date();
				if(action.equals("update")) {
					List<CrownJewelLeagues> leagueSaveList = new ArrayList<CrownJewelLeagues>();
					List<CrownJewelLeagues> leagueUpdateList = new ArrayList<CrownJewelLeagues>();
					List<CrownJewelLeaguesAudit> auditList = new ArrayList<CrownJewelLeaguesAudit>();
					
					List<CrownJewelEvents> cjEventsSaveList = new ArrayList<CrownJewelEvents>();
					List<CrownJewelEvents> cjEventsUpdateList = new ArrayList<CrownJewelEvents>();
					List<CrownJewelEventsAudit> cjEventAudits = new ArrayList<CrownJewelEventsAudit>();
					Boolean isRealEvent = false;
					Map<String, String[]> requestParams = request.getParameterMap();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						isRealEvent= false;
						if(key.contains("checkbox_")){
							Integer rowNumber = Integer.parseInt(key.replace("checkbox_", ""));
							String idStr = request.getParameter("id_"+rowNumber);
							String name = request.getParameter("name_"+rowNumber);
							String categoryTypes = request.getParameter("categoryType_"+rowNumber);
							String grandChildCategoryTypes = request.getParameter("grandChildCategoryType_"+rowNumber);
							String packageApplicable = request.getParameter("packageApplicable_"+rowNumber);
							Boolean onlyPackageCost = false;
							if(packageApplicable != null && !packageApplicable.isEmpty()
									&& packageApplicable.equalsIgnoreCase("on")){
								onlyPackageCost = true;
							}
							
							CrownJewelLeagues crownJewelLeagues = null;
							if(idStr != null && idStr.trim().length() > 0) {
								crownJewelLeagues = DAORegistry.getCrownJewelLeaguesDAO().get(Integer.parseInt(idStr));	
							} else {
								crownJewelLeagues = new CrownJewelLeagues();
							}
							crownJewelLeagues.setName(name);
							crownJewelLeagues.setIsPackageCost(onlyPackageCost);
							crownJewelLeagues.setLastUpdated(now);
							crownJewelLeagues.setLastUpdateddBy(username);
							crownJewelLeagues.setStatus("ACTIVE");
							
							if(categoryTypes != null && !categoryTypes.isEmpty()){
								String categoryType[] = categoryTypes.split("_");
								if(categoryType[0].equalsIgnoreCase("gc")){
									crownJewelLeagues.setGrandChildId(Integer.parseInt(categoryType[1]));
								}
							}
							if(grandChildCategoryTypes != null && !grandChildCategoryTypes.isEmpty()){								
								if(Integer.parseInt(grandChildCategoryTypes)>0){
									crownJewelLeagues.setGrandChildId(Integer.parseInt(grandChildCategoryTypes));
								}else{
									crownJewelLeagues.setGrandChildId(null);
								}
							}
							String eventStr = request.getParameter("eventId_"+rowNumber);
							Integer evnetId = null;
							if(eventStr!=null && !eventStr.isEmpty()){
								evnetId = Integer.parseInt(eventStr);
								EventDetails eventDetails = DAORegistry.getEventDetailsDAO().get(evnetId);
								if(eventDetails!=null && eventDetails.getEventStatus().equals(EventStatus.ACTIVE)){
									isRealEvent = true;
								}
							}
								
							String oldEventIdStr = request.getParameter("oldEventId_"+rowNumber);
							CrownJewelEvents crownJewelEvent = null;
							if(oldEventIdStr!=null && !oldEventIdStr.isEmpty() && evnetId!=null){
								System.out.println("Old event id: "+oldEventIdStr);
								crownJewelEvent = TMATDAORegistry.getCrownJewelEventsDAO().getCrownJewelEventByEventId(Integer.parseInt(oldEventIdStr));
								List<CrownJewelTeamZones> zones = DAORegistry.getCrownJewelTeamZonesDAO().getAllTeamZonesByLeagueId(crownJewelLeagues.getId());
								List<CrownJewelZonesSoldDetails> zoneSoldTickets = DAORegistry.getCrownJewelZonesSoldDetailsDAO().getAllTeamZonesByLeagueId(crownJewelLeagues.getId());
								List<CrownJewelCategoryTicket> CJETickets = DAORegistry.getCrownJewelCategoryTicketDAO().getAllCrownjewelTicketsByEventId(Integer.parseInt(oldEventIdStr));
								List<CrownJewelCustomerOrder> CJEorders = null;
								try{
									CJEorders = DAORegistry.getCrownJewelCustomerOrderDAO().getCrownJewelOrdersByLeagueId(crownJewelLeagues.getId());
								}catch(Exception e){
									e.printStackTrace();
								}
								
								
								for(CrownJewelTeamZones zone : zones){
									zone.setEventId(evnetId);
								}
								for(CrownJewelZonesSoldDetails zoneSold : zoneSoldTickets){
									zoneSold.setEventId(evnetId);
								}
								for(CrownJewelCategoryTicket ticket : CJETickets){
									ticket.setEventId(evnetId);
								}
								for(CrownJewelCustomerOrder order : CJEorders){
									order.setEventId(evnetId);
								}
								DAORegistry.getCrownJewelTeamZonesDAO().updateAll(zones);
								DAORegistry.getCrownJewelZonesSoldDetailsDAO().updateAll(zoneSoldTickets);
								DAORegistry.getCrownJewelCategoryTicketDAO().updateAll(CJETickets);
								DAORegistry.getCrownJewelCustomerOrderDAO().updateAll(CJEorders);
							}else if(evnetId!=null){
								crownJewelEvent = TMATDAORegistry.getCrownJewelEventsDAO().getCrownJewelEventByEventId(evnetId);
							}
							
							crownJewelLeagues.setEventId(evnetId);
							crownJewelLeagues.setIsRealEvent(isRealEvent);
							
							if(evnetId!=null){
								if(crownJewelEvent == null) {
									crownJewelEvent = new CrownJewelEvents();
								}
								crownJewelEvent.setEventId(evnetId);
								crownJewelEvent.setParentType("SPORTS");
								crownJewelEvent.setExposure("2-OXP");
								crownJewelEvent.setRptFactor(20.0);
								crownJewelEvent.setPriceBreakup(0.0);
								crownJewelEvent.setLowerMarkup(20.0);
								crownJewelEvent.setUpperMarkup(20.0);
								crownJewelEvent.setLowerShippingFees(0.0);
								crownJewelEvent.setUpperShippingFees(0.0);
								crownJewelEvent.setAllowSectionRange(false);
								crownJewelEvent.setShippingDays(0);
								crownJewelEvent.setShippingMethod(null);
								crownJewelEvent.setNearTermDisplayOption(null);
								crownJewelEvent.setZoneTicketBrokerId(1);//MZTix
								crownJewelEvent.setLastUpdatedBy(username);
								crownJewelEvent.setLastUpdatedDate(now);
								crownJewelEvent.setStatus("ACTIVE");
								
								String eventAction="CREATE";
								if(crownJewelEvent.getId() == null) {
									cjEventsSaveList.add(crownJewelEvent);
								} else {
									eventAction = "UPDATE";
									cjEventsUpdateList.add(crownJewelEvent);
								}
								CrownJewelEventsAudit eventAudit = new CrownJewelEventsAudit(crownJewelEvent);
								eventAudit.setStatus("ACTIVE");
								eventAudit.setAction(eventAction);
								cjEventAudits.add(eventAudit);
							}
							
							
							CrownJewelLeaguesAudit audit = new CrownJewelLeaguesAudit(crownJewelLeagues);
							if(crownJewelLeagues.getId() != null) {
								audit.setAction("UPDATE");
								
								leagueUpdateList.add(crownJewelLeagues);
							} else {
								audit.setAction("CREATE");
								
								leagueSaveList.add(crownJewelLeagues);
							}
							auditList.add(audit);
						}
					}
					DAORegistry.getCrownJewelLeaguesDAO().saveAll(leagueSaveList);
					DAORegistry.getCrownJewelLeaguesDAO().updateAll(leagueUpdateList);
					DAORegistry.getCrownJewelLeaguesAuditDAO().saveAll(auditList);
					
					TMATDAORegistry.getCrownJewelEventsDAO().saveAll(cjEventsSaveList);
					TMATDAORegistry.getCrownJewelEventsDAO().updateAll(cjEventsUpdateList);
					TMATDAORegistry.getCrownJewelEventsAuditDAO().saveAll(cjEventAudits);
					
					crownJewelLeaguesDTO.setMessage("Events Saved Successfully.");
					
				} else if(action.equals("delete")) {
					List<CrownJewelLeagues> leagueList = new ArrayList<CrownJewelLeagues>();
					List<CrownJewelTeams> teamList = new ArrayList<CrownJewelTeams>();
					
					List<CrownJewelLeaguesAudit> leagueAuditList = new ArrayList<CrownJewelLeaguesAudit>();
					List<CrownJewelTeamsAudit> teamAuditList = new ArrayList<CrownJewelTeamsAudit>();
					
					List<CrownJewelEvents> eventsList = new ArrayList<CrownJewelEvents>();
					List<CrownJewelEventsAudit> eventAuditList = new ArrayList<CrownJewelEventsAudit>();
					
					List<CrownJewelTeamZones> cjTeamZones = new ArrayList<CrownJewelTeamZones>();
					List<CrownJewelTeamZonesAudit> zonesAuditList = new ArrayList<CrownJewelTeamZonesAudit>();
					
					Map<String, String[]> requestParams = request.getParameterMap();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						if(key.contains("checkbox_")){
							Integer rowNumber = Integer.parseInt(key.replace("checkbox_", ""));
							String idStr = request.getParameter("id_"+rowNumber);
							if(idStr == null || idStr.isEmpty()) {
								continue;
							}
							Integer id=Integer.parseInt(idStr);
					
							CrownJewelLeagues crownJewelLeagueDB = DAORegistry.getCrownJewelLeaguesDAO().get(id);
							crownJewelLeagueDB.setLastUpdated(now);
							crownJewelLeagueDB.setLastUpdateddBy(username);
							crownJewelLeagueDB.setStatus("DELETED");
							
							//Setting status to crownjewel_zones_sold_count when delete the league
							DAORegistry.getCrownJewelZonesSoldDetailsDAO().updateDeleteStatusForLeague(id);
														
							CrownJewelLeaguesAudit leagueAudit = new CrownJewelLeaguesAudit(crownJewelLeagueDB);
							leagueAudit.setAction("DELETE");
							
							leagueAuditList.add(leagueAudit);
							leagueList.add(crownJewelLeagueDB);
					
							CrownJewelEvents crownJewelEvent = null;
							if(crownJewelLeagueDB.getEventId() != null) {
								crownJewelEvent = TMATDAORegistry.getCrownJewelEventsDAO().getCrownJewelEventByEventId(crownJewelLeagueDB.getEventId());
							}
							if(crownJewelEvent != null) {
								crownJewelEvent.setLastUpdatedBy(username);
								crownJewelEvent.setLastUpdatedDate(now);
								crownJewelEvent.setStatus("DELETED");
								
								CrownJewelEventsAudit eventAudit = new CrownJewelEventsAudit(crownJewelEvent);
								eventAudit.setStatus("DELETED");
								eventAudit.setAction("DELETE");
							
								eventAuditList.add(eventAudit);
								eventsList.add(crownJewelEvent);
							}
							
							List<CrownJewelTeams> teamsListDB = DAORegistry.getCrownJewelTeamsDAO().getAllTeamsByLeagueId(crownJewelLeagueDB.getId());
					
							for (CrownJewelTeams crownJewelTeams : teamsListDB) {
								crownJewelTeams.setLastUpdated(now);
								crownJewelTeams.setLastUpdateddBy(username);
								crownJewelTeams.setStatus("DELETED");
						
								CrownJewelTeamsAudit teamAudit = new CrownJewelTeamsAudit(crownJewelTeams);
								teamAudit.setAction("DELETE");

								teamAuditList.add(teamAudit);
								teamList.add(crownJewelTeams);
							}
							List<CrownJewelTeamZones> zonesListDB = DAORegistry.getCrownJewelTeamZonesDAO().getAllTeamZonesByLeagueId(crownJewelLeagueDB.getId());
							for (CrownJewelTeamZones cjtZone : zonesListDB) {
								cjtZone.setLastUpdated(now);
								cjtZone.setLastUpdateddBy(username);
								cjtZone.setStatus("DELETED");
						
								CrownJewelTeamZonesAudit teamAudit = new CrownJewelTeamZonesAudit(cjtZone);
								teamAudit.setAction("DELETE");

								zonesAuditList.add(teamAudit);
								cjTeamZones.add(cjtZone);
							}
						}
					}
					DAORegistry.getCrownJewelTeamZonesDAO().updateAll(cjTeamZones);
					DAORegistry.getCrownJewelTeamsDAO().updateAll(teamList);
					DAORegistry.getCrownJewelLeaguesDAO().updateAll(leagueList);
			
					DAORegistry.getCrownJewelTeamZonesAuditDAO().saveAll(zonesAuditList);
					DAORegistry.getCrownJewelLeaguesAuditDAO().saveAll(leagueAuditList);
					DAORegistry.getCrownJewelTeamsAuditDAO().saveAll(teamAuditList);
					
					TMATDAORegistry.getCrownJewelEventsDAO().deleteAll(eventsList);
					TMATDAORegistry.getCrownJewelEventsAuditDAO().deleteAll(eventAuditList);
					
					crownJewelLeaguesDTO.setMessage("Events Deleted Successfully.");
				}
			}
			
			List<CrownJewelLeagues> cjLeaguesList = null;
			List<FantasyChildCategory> childCategories = null;
			List<FantasyGrandChildCategory> grandChildCategories = null;
			if(childId !=null && !childId.isEmpty()){
				grandChildCategories = DAORegistry.getFantasyGrandChildCategoryDAO().getGrandChildsByChildCategoryId(Integer.parseInt(childId));
			}if(grandChildId!=null && !grandChildId.isEmpty()){
				cjLeaguesList = DAORegistry.getCrownJewelLeaguesDAO().getActiveLeaguesByGrandChildId(Integer.parseInt(grandChildId));
			}
			
			childCategories = DAORegistry.getFantasyChildCategoryDAO().getAll();
			
			if(cjLeaguesList!=null && !cjLeaguesList.isEmpty()){
				Map<Integer,List<FantasyGrandChildCategory>> childGrandChildMap = new HashMap<Integer, List<FantasyGrandChildCategory>>();
				Map<Integer,Integer> grandChidlChildMap = new HashMap<Integer, Integer>();
				List<FantasyGrandChildCategory> grandChildCategoryList = DAORegistry.getFantasyGrandChildCategoryDAO().getAll();
				for(FantasyGrandChildCategory grandChild : grandChildCategoryList){
					grandChidlChildMap.put(grandChild.getId(),grandChild.getChildId());
				}
				for(FantasyChildCategory childCat : childCategories){
					childGrandChildMap.put(childCat.getId(),DAORegistry.getFantasyGrandChildCategoryDAO().getGrandChildsByChildCategoryId(childCat.getId()));
				}
				
				for(CrownJewelLeagues league : cjLeaguesList){
					if(league.getEventId()!=null){
						Event event = DAORegistry.getEventDAO().get(league.getEventId());
						Venue venue = DAORegistry.getVenueDAO().get(event.getVenueId());
						league.setVenueString(event.getEventName()+" "+event.getEventDateStr()+" "+event.getEventTimeStr()+" "+venue.getLocation());
					}
					league.setChildId(grandChidlChildMap.get(league.getGrandChildId()));
					league.setGrandChildCategory(childGrandChildMap.get(league.getChildId()));
					
				}
			}
			//List<FantasyParentCategory> parentCategoryList = DAORegistry.getFantasyParentCategoryDAO().getAll();
			//List<FantasyGrandChildCategory> grandChildCategoryList = DAORegistry.getFantasyGrandChildCategoryDAO().getAll();
			
			crownJewelLeaguesDTO.setStatus(1);
			crownJewelLeaguesDTO.setLeaguesList(cjLeaguesList);
			crownJewelLeaguesDTO.setChildId(childId);
			crownJewelLeaguesDTO.setGrandChildId(grandChildId);
			crownJewelLeaguesDTO.setChildCategories(childCategories);
			crownJewelLeaguesDTO.setGrandChildCategories(grandChildCategories);
			//map.put("parentCategoryList", parentCategoryList);
			//map.put("grandChildCategoryList", grandChildCategoryList);
		} catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching/updating Fantasy Sports Tickets");
			crownJewelLeaguesDTO.setError(error);
			crownJewelLeaguesDTO.setStatus(0);
		}
		//map.put("productName", "Crown Jewel Events");
		return crownJewelLeaguesDTO;
	}
	
	@RequestMapping(value="/CrownJewelTeams")
	public CrownJewelTeamsDTO crownJewelTeams(HttpServletRequest request, HttpServletResponse response){
		CrownJewelTeamsDTO crownJewelTeamsDTO = new CrownJewelTeamsDTO();
		Error error = new Error();
		//model.addAttribute("crownJewelTeamsDTO", crownJewelTeamsDTO);
		
		try {
			String childId = request.getParameter("childType");
			String grandChildId = request.getParameter("grandChildType");
			String action = request.getParameter("action");
			String leagueIdStr = request.getParameter("leagueId");
			String copyFromLeagueIdStr = request.getParameter("copyFromLeagueId");
			
			Integer leagueId = null;
			if(leagueIdStr != null && leagueIdStr.trim().length() > 0) {
				leagueId = Integer.parseInt(leagueIdStr);
			}
			
			if(action != null) {
				String username = request.getParameter("userName");
				if(StringUtils.isEmpty(username)){
					System.err.println("Please Login.");
					error.setDescription("Please Login.");
					crownJewelTeamsDTO.setError(error);
					crownJewelTeamsDTO.setStatus(0);
					return crownJewelTeamsDTO;
				}
				
				Date now = new Date();
				if(action.equals("copy")){
					String copyToLeageIds = request.getParameter("copyToLeagueIds");
					if(copyToLeageIds!=null && !copyToLeageIds.isEmpty() && copyFromLeagueIdStr!=null && !copyFromLeagueIdStr.isEmpty()){
						String copyToIdArr[] = copyToLeageIds.split(",");
						int teamSize = 0;
						int zoneSize = 0;
						Integer copyFromleague = Integer.parseInt(copyFromLeagueIdStr);
						List<CrownJewelTeams> teams = DAORegistry.getCrownJewelTeamsDAO().getAllActiveTeamsByLeagueId(copyFromleague);
						for(String idStr : copyToIdArr){
							if(idStr==null || idStr.isEmpty()){
								continue;
							}
							CrownJewelLeagues league = DAORegistry.getCrownJewelLeaguesDAO().get(Integer.parseInt(idStr));
							List<CrownJewelTeams> copiedTeams = new ArrayList<CrownJewelTeams>();
							List<CrownJewelTeamZones> copiedZones = new ArrayList<CrownJewelTeamZones>();
							for(CrownJewelTeams team  :teams){
								CrownJewelTeams t = new CrownJewelTeams();
								t.setCategoryTeamId(team.getCategoryTeamId());
								t.setCutOffDate(team.getCutOffDate());
								t.setFractionalOdd(team.getFractionalOdd());
								t.setLastUpdated(new Date());
								t.setLastUpdateddBy(username);
								t.setLeagueId(league.getId());
								t.setMarkup(team.getMarkup());
								t.setName(team.getName());
								t.setStatus(team.getStatus());
								t.setOdds(team.getOdds());
								copiedTeams.add(t);
							}
							
							List<CrownJewelTeams> DBteams = DAORegistry.getCrownJewelTeamsDAO().getAllActiveTeamsByLeagueId(league.getId());
							DAORegistry.getCrownJewelTeamsDAO().deleteAll(DBteams);
							DAORegistry.getCrownJewelTeamsDAO().saveAll(copiedTeams);
							teamSize = teamSize + copiedTeams.size();
							if(league.getEventId()==null){
								continue;
							}
							Event event = DAORegistry.getEventDAO().get(league.getEventId());
							List<String> zoneList = TMATDAORegistry.getCategoryDAO().getCategorySymbolsByVenueCategoryId(event.getVenueCategoryId());
							for(CrownJewelTeams team  :teams){
								List<CrownJewelTeamZones> zones = DAORegistry.getCrownJewelTeamZonesDAO().getAllActiveTeamZonesByTeamId(team.getId());
								CrownJewelTeamZones cjeZone = null;
								Boolean copyZones=false;
								for(CrownJewelTeamZones zone : zones){
									copyZones=false;
									for(String str : zoneList){
										if(str.equalsIgnoreCase(zone.getZone())){
											copyZones = true;
											break;
										}
									}
									if(copyZones){
										cjeZone = new CrownJewelTeamZones();
										for(CrownJewelTeams t : copiedTeams){
											if(t.getCategoryTeamId()==team.getCategoryTeamId()){
												cjeZone.setTeamId(t.getId());
												break;
											}
										}
										cjeZone.setQuantityType(zone.getQuantityType());
										cjeZone.setEventId(league.getEventId());
										cjeZone.setLeagueId(league.getId());
										cjeZone.setZone(zone.getZone());
										cjeZone.setLastUpdateddBy(username);
										cjeZone.setLastUpdated(new Date());
										cjeZone.setTicketsCount(zone.getTicketsCount());
										cjeZone.setStatus(zone.getStatus());
										cjeZone.setPrice(zone.getPrice());
										copiedZones.add(cjeZone);
									}
									
								}
							}
							
							List<CrownJewelTeamZones> deletedZones = DAORegistry.getCrownJewelTeamZonesDAO().getAllActiveTeamZonesByLeagueId(league.getId());
							DAORegistry.getCrownJewelTeamZonesDAO().deleteAll(deletedZones);
							if(!copiedZones.isEmpty()){
								DAORegistry.getCrownJewelTeamZonesDAO().saveAll(copiedZones);
							}
							zoneSize = zoneSize + copiedZones.size();
						}
						crownJewelTeamsDTO.setMessage("Total "+teamSize+" Teams and "+zoneSize+" matching zones Are Copied for selected Event");
					}else{
						crownJewelTeamsDTO.setMessage("Please select source and destination event to copy teams.");
					}
				}
				else if(action.equals("update")) {

					List<CrownJewelTeams> teamSaveList = new ArrayList<CrownJewelTeams>();
					List<CrownJewelTeams> teamUpdateList = new ArrayList<CrownJewelTeams>();
					List<CrownJewelTeamsAudit> auditList = new ArrayList<CrownJewelTeamsAudit>();
					
					List<CrownJewelEvents> cjEventsSaveList = new ArrayList<CrownJewelEvents>();
					List<CrownJewelEvents> cjEventsUpdateList = new ArrayList<CrownJewelEvents>();
					List<CrownJewelEventsAudit> cjEventAudits = new ArrayList<CrownJewelEventsAudit>();
					
					Map<String, String[]> requestParams = request.getParameterMap();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						if(key.contains("checkbox_")){
							Integer rowNumber = Integer.parseInt(key.replace("checkbox_", ""));
							String idStr = request.getParameter("id_"+rowNumber);
							CrownJewelTeams crownJewelTeam = null;
							if(idStr != null && idStr.trim().length() > 0) {
								crownJewelTeam = DAORegistry.getCrownJewelTeamsDAO().get(Integer.parseInt(idStr));	
							} else {
								crownJewelTeam = new CrownJewelTeams();
							}
							
							crownJewelTeam.setLeagueId(leagueId);
							crownJewelTeam.setLastUpdated(now);
							crownJewelTeam.setLastUpdateddBy(username);
							crownJewelTeam.setStatus("ACTIVE");
							
							/*Integer ticketsCount = Integer.parseInt(request.getParameter("ticketsCount_"+rowNumber));
							crownJewelTeam.setTicketsCount(ticketsCount);*/
							String name = request.getParameter("name_"+rowNumber);
							String markupStr = request.getParameter("markup_"+rowNumber);
							if(markupStr!=null && !markupStr.isEmpty()){
								crownJewelTeam.setMarkup(Integer.parseInt(markupStr));
							}
							String categoryTeamIdStr = request.getParameter("categoryTeamId_"+rowNumber);
							if(categoryTeamIdStr!=null && !categoryTeamIdStr.isEmpty()){
								crownJewelTeam.setCategoryTeamId(Integer.parseInt(categoryTeamIdStr));
							}
							Double odds = Double.parseDouble(request.getParameter("odds_"+rowNumber));
							String fractionalOdd = request.getParameter("fractionalOdd_"+rowNumber);
							String cutoffDateStr = request.getParameter("cutoffDate_"+rowNumber);
							if(cutoffDateStr!=null && !cutoffDateStr.isEmpty()){
								DateFormat formatDateTime = new SimpleDateFormat("MM/dd/yyyy HH:mm");
								cutoffDateStr +=" 00:00:00";
								crownJewelTeam.setCutOffDate(formatDateTime.parse(cutoffDateStr));
							}
							crownJewelTeam.setName(name);
							crownJewelTeam.setFractionalOdd(fractionalOdd);
							crownJewelTeam.setOdds(odds);
							CrownJewelTeamsAudit audit = new CrownJewelTeamsAudit(crownJewelTeam);
							if(crownJewelTeam.getId() != null) {
								audit.setAction("UPDATE");
								
								teamUpdateList.add(crownJewelTeam);
							} else {
								audit.setAction("CREATE");
								
								teamSaveList.add(crownJewelTeam);
							}
							auditList.add(audit);
						}
					}
					DAORegistry.getCrownJewelTeamsDAO().saveAll(teamSaveList);
					DAORegistry.getCrownJewelTeamsDAO().updateAll(teamUpdateList);
					DAORegistry.getCrownJewelTeamsAuditDAO().saveAll(auditList);
					
					TMATDAORegistry.getCrownJewelEventsDAO().saveAll(cjEventsSaveList);
					TMATDAORegistry.getCrownJewelEventsDAO().updateAll(cjEventsUpdateList);
					TMATDAORegistry.getCrownJewelEventsAuditDAO().saveAll(cjEventAudits);
					crownJewelTeamsDTO.setMessage("Selected Teams Saved/Updated Successfully.");
				} else if(action.equals("delete")) {
					boolean isOrderUpdated = true;
					List<CrownJewelTeams> teamList = new ArrayList<CrownJewelTeams>();
					List<CrownJewelTeamsAudit> teamAuditList = new ArrayList<CrownJewelTeamsAudit>();
					
					List<CrownJewelTeamZones> cjtZoneList = new ArrayList<CrownJewelTeamZones>();
					List<CrownJewelTeamZonesAudit> zouneaudits = new ArrayList<CrownJewelTeamZonesAudit>();
					List<CrownJewelEvents> eventsList = new ArrayList<CrownJewelEvents>();
					
					List<CrownJewelEventsAudit> eventAuditList = new ArrayList<CrownJewelEventsAudit>();
					
					Map<String, String[]> requestParams = request.getParameterMap();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						if(key.contains("checkbox_")){
							Integer rowNumber = Integer.parseInt(key.replace("checkbox_", ""));
							String idStr = request.getParameter("id_"+rowNumber);
							if(idStr == null || idStr.isEmpty()) {
								continue;
							}
							Integer id=Integer.parseInt(idStr);
					
							CrownJewelTeams crownJewelTeamDB = DAORegistry.getCrownJewelTeamsDAO().get(id);
							crownJewelTeamDB.setLastUpdated(now);
							crownJewelTeamDB.setLastUpdateddBy(username);
							crownJewelTeamDB.setStatus("DELETED");

							//Setting status to crownjewel_zones_sold_count when delete the team
							DAORegistry.getCrownJewelZonesSoldDetailsDAO().updateDeleteStatusForTeam(id);
							
							CrownJewelTeamsAudit teamAudit = new CrownJewelTeamsAudit(crownJewelTeamDB);
							teamAudit.setAction("DELETE");
							
							teamAuditList.add(teamAudit);
							teamList.add(crownJewelTeamDB);
							
							CrownJewelEvents crownJewelEvent = null;
							if(crownJewelTeamDB.getEventId() != null) {
								crownJewelEvent = TMATDAORegistry.getCrownJewelEventsDAO().getCrownJewelEventByEventId(crownJewelTeamDB.getEventId());
							}
							if(crownJewelEvent != null) {
								crownJewelEvent.setLastUpdatedBy(username);
								crownJewelEvent.setLastUpdatedDate(now);
								crownJewelEvent.setStatus("DELETED");
								
								CrownJewelEventsAudit eventAudit = new CrownJewelEventsAudit(crownJewelEvent);
								eventAudit.setStatus("DELETED");
								eventAudit.setAction("DELETE");

								eventAuditList.add(eventAudit);
								eventsList.add(crownJewelEvent);
							}
							List<CrownJewelTeamZones> zonesListDB = DAORegistry.getCrownJewelTeamZonesDAO().getAllTeamZonesByTeamId(crownJewelTeamDB.getId());
							for (CrownJewelTeamZones cjtZone : zonesListDB) {
								cjtZone.setLastUpdated(now);
								cjtZone.setLastUpdateddBy(username);
								cjtZone.setStatus("DELETED");
						
								CrownJewelTeamZonesAudit zoneAudit = new CrownJewelTeamZonesAudit(cjtZone);
								zoneAudit.setAction("DELETE");

								zouneaudits.add(zoneAudit);
								cjtZoneList.add(cjtZone);
							}
						
							List<CrownJewelCustomerOrder> orders = DAORegistry.getCrownJewelCustomerOrderDAO().getCrownJewelOrdersByTeamId(crownJewelTeamDB.getId());
							if(!orders.isEmpty()){
								isOrderUpdated = updateOrdersAndRevertRewardPoints(orders,username);
							}
							
							/*List<CrownJewelCustomerOrder> orderList = new ArrayList<CrownJewelCustomerOrder>();
							List<CustomerOrder> regularOrderList = new ArrayList<CustomerOrder>();
							List<Invoice> invoiceList = new ArrayList<Invoice>();
							List<CrownJewelCustomerOrder> orders = DAORegistry.getCrownJewelCustomerOrderDAO().getCrownJewelOrdersByTeamId(crownJewelTeamDB.getId());
							List<Integer> regularOrderIds = new ArrayList<Integer>();
							List<CustomerLoyalty> loyaltyList = new ArrayList<CustomerLoyalty>();
							for (CrownJewelCustomerOrder order : orders) {
								order.setStatus(CrownJewelOrderStatus.REJECTED.toString());
								order.setLastUpdatedDate(now);
								order.setAcceptRejectReson("Team was Excluded.");
								if(order.getRegularOrderId()!=null){
									regularOrderIds.add(order.getRegularOrderId());
								}
								orderList.add(order);
							}
							if(!regularOrderIds.isEmpty()){
								List<CustomerOrder> regularOrders = DAORegistry.getCustomerOrderDAO().getCustomerOrderByIds(regularOrderIds);
								for(CustomerOrder order:regularOrders){
									order.setStatus(OrderStatus.VOIDED.toString());
									order.setLastUpdated(now);
									regularOrderList.add(order);
								}
								
								List<Invoice> invoices = DAORegistry.getInvoiceDAO().getInvoiceByOrderIds(regularOrderIds);
								for(Invoice invoice:invoices){
									invoice.setStatus(InvoiceStatus.Voided);
									invoice.setLastUpdated(now);
									invoice.setLastUpdatedBy(username);
									invoiceList.add(invoice);
								}
							}
							DAORegistry.getCrownJewelCustomerOrderDAO().updateAll(orderList);
							DAORegistry.getCustomerOrderDAO().updateAll(regularOrderList);
							DAORegistry.getInvoiceDAO().updateAll(invoiceList);
							for(CrownJewelCustomerOrder order : orderList){
								Map<String, String> requestMap = Util.getParameterMap(null);
								requestMap.put("customerId",String.valueOf(order.getCustomerId()));
								requestMap.put("orderType",OrderType.CROWNJEWEL.toString());
								requestMap.put("orderNo",String.valueOf(order.getId()));
								String data = Util.getObject(requestMap,Constants.BASE_URL + Constants.CANCEL_ORDER);
								Gson gson = new Gson();  
								JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
								System.out.println("Data");
								CancelOrderResponse cancelOrderResponse = gson.fromJson(((JsonObject)jsonObject.get("cancelOrderResponse")), CancelOrderResponse.class);
							}*/
							
							/*if(!dataList.isEmpty()){
								for(Map<String, Object> dataMap : dataList){
									 com.rtw.tracker.mail.MailAttachment[] mailAttachment = new com.rtw.tracker.mail.MailAttachment[1];
									 mailAttachment[0] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","logo.png",com.rtw.tmat.utils.Util.getFilePath(request, "logo.png", "/images/"));
									 mailManager.sendMailNow("text/html","sales@rewardthefan.com",(String)dataMap.get("email"), 
											null,null, "Fantasy Sports Tickets Team Excluded",
											"mail-fantasy-ticket-team-exclude.html", dataMap, "text/html", null,mailAttachment,null);
								}
								 
							}*/
						}
					}
					if(isOrderUpdated){
						DAORegistry.getCrownJewelTeamZonesDAO().updateAll(cjtZoneList);
						DAORegistry.getCrownJewelTeamsDAO().updateAll(teamList);
						DAORegistry.getCrownJewelTeamsAuditDAO().saveAll(teamAuditList);
						DAORegistry.getCrownJewelTeamZonesAuditDAO().saveAll(zouneaudits);
						TMATDAORegistry.getCrownJewelEventsDAO().updateAll(eventsList);
						TMATDAORegistry.getCrownJewelEventsAuditDAO().deleteAll(eventAuditList);
						crownJewelTeamsDTO.setMessage("Selected Teams are Deleted and all associated Orders are voided.");
					}else{
						crownJewelTeamsDTO.setMessage("Something went wrong, not able to delete team.");
					}
					
				}
			}
			List<FantasyChildCategory> childCategories = null;
			List<FantasyGrandChildCategory> grandChildCategories = null;
			List<CrownJewelLeagues> cjLeaguesList = null;
			
			if(childId != null && !childId.isEmpty()){
				grandChildCategories = DAORegistry.getFantasyGrandChildCategoryDAO().getGrandChildsByChildCategoryId(Integer.parseInt(childId));
			}if(grandChildId!=null && !grandChildId.isEmpty()){
				cjLeaguesList = DAORegistry.getCrownJewelLeaguesDAO().getActiveLeaguesByGrandChildId(Integer.parseInt(grandChildId));
			}
			List<FantasyParentCategory> parentCategoryList = DAORegistry.getFantasyParentCategoryDAO().getAll();
			List<CrownJewelTeams> cjTeamsList = null;
			if(leagueId != null){
				cjTeamsList = DAORegistry.getCrownJewelTeamsDAO().getAllActiveTeamsByLeagueId(leagueId);
			}
			childCategories = DAORegistry.getFantasyChildCategoryDAO().getAll();
			
			crownJewelTeamsDTO.setStatus(1);
			crownJewelTeamsDTO.setChildId(childId);
			crownJewelTeamsDTO.setGrandChildId(grandChildId);
			crownJewelTeamsDTO.setChildCategories(childCategories);
			crownJewelTeamsDTO.setGrandChildCategories(grandChildCategories);
			crownJewelTeamsDTO.setParentCategoryList(parentCategoryList);
			crownJewelTeamsDTO.setLeaguesList(cjLeaguesList);
			crownJewelTeamsDTO.setTeamsList(cjTeamsList);
			crownJewelTeamsDTO.setLeagueId(leagueIdStr);
			crownJewelTeamsDTO.setCopyFromLeagueId(copyFromLeagueIdStr);
		} catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching/updating Fantasy Sports Tickets Team");
			crownJewelTeamsDTO.setError(error);
			crownJewelTeamsDTO.setStatus(0);
		}
		return crownJewelTeamsDTO;
	}
	
	
	
	
	@RequestMapping(value="/CrownJewelCategoryTeams")
	public CrownJewelTeamsDTO crownJewelCategoryTeams(HttpServletRequest request, HttpServletResponse response){
		CrownJewelTeamsDTO crownJewelTeamsDTO = new CrownJewelTeamsDTO();
		//model.addAttribute("crownJewelTeamsDTO", crownJewelTeamsDTO);
		
		String msg="";
		String eventNames = "";
		Integer cnt =0;
		try {
			String childId = request.getParameter("childType");
			String grandChildId = request.getParameter("grandChildType");
			String action = request.getParameter("action");
			
			if(action != null) {
				String username = request.getParameter("userName");
				Date now = new Date();
				if(action.equals("update")) {
					List<CrownJewelCategoryTeams> teamSaveList = new ArrayList<CrownJewelCategoryTeams>();
					List<CrownJewelCategoryTeams> teamUpdateList = new ArrayList<CrownJewelCategoryTeams>();
					List<CrownJewelCategoryTeamsAudit> auditList = new ArrayList<CrownJewelCategoryTeamsAudit>();
					
					Map<String, String[]> requestParams = request.getParameterMap();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						if(key.contains("checkbox_")){
							Integer rowNumber = Integer.parseInt(key.replace("checkbox_", ""));
							String idStr = request.getParameter("id_"+rowNumber);
							CrownJewelCategoryTeams crownJewelCategoryTeam = null;
							List<CrownJewelTeams> teams = null;
							if(idStr != null && idStr.trim().length() > 0) {
								crownJewelCategoryTeam = DAORegistry.getCrownJewelCategoryTeamsDAO().get(Integer.parseInt(idStr));	
								teams = DAORegistry.getCrownJewelTeamsDAO().getAllActiveTeamsByCategoryTeamId(crownJewelCategoryTeam.getId());
							} else {
								crownJewelCategoryTeam = new CrownJewelCategoryTeams();
							}
							
							crownJewelCategoryTeam.setLastUpdated(now);
							crownJewelCategoryTeam.setLastUpdatedBy(username);
							crownJewelCategoryTeam.setStatus("ACTIVE");
							
							String name = request.getParameter("name_"+rowNumber);
							String markupStr = request.getParameter("markup_"+rowNumber);
							if(markupStr!=null && !markupStr.isEmpty()){
								crownJewelCategoryTeam.setMarkup(Integer.parseInt(markupStr));
							}
							Double odds = Double.parseDouble(request.getParameter("odds_"+rowNumber));
							String fractionalOdd = request.getParameter("fractionalOdd_"+rowNumber);
							String cutoffDateStr = request.getParameter("cutoffDate_"+rowNumber);
							if(cutoffDateStr!=null && !cutoffDateStr.isEmpty()){
								DateFormat formatDateTime = new SimpleDateFormat("MM/dd/yyyy HH:mm");
								cutoffDateStr +=" 00:00:00";
								crownJewelCategoryTeam.setCutOffDate(formatDateTime.parse(cutoffDateStr));
							}
							crownJewelCategoryTeam.setName(name);
							crownJewelCategoryTeam.setFractionalOdd(fractionalOdd);
							crownJewelCategoryTeam.setOdds(odds);
							crownJewelCategoryTeam.setGrandChildId(Integer.parseInt(grandChildId));
							
							CrownJewelCategoryTeamsAudit audit = new CrownJewelCategoryTeamsAudit(crownJewelCategoryTeam);
							if(crownJewelCategoryTeam.getId() != null) {
								audit.setAction("UPDATE");
								teamUpdateList.add(crownJewelCategoryTeam);
							} else {
								audit.setAction("CREATE");
								
								teamSaveList.add(crownJewelCategoryTeam);
							}
							auditList.add(audit);
							
							if(teams!=null && !teams.isEmpty()){
								for(CrownJewelTeams team : teams){
									team.setMarkup(crownJewelCategoryTeam.getMarkup());
									team.setFractionalOdd(crownJewelCategoryTeam.getFractionalOdd());
									team.setOdds(crownJewelCategoryTeam.getOdds());
									team.setCutOffDate(crownJewelCategoryTeam.getCutOffDate());
									team.setName(crownJewelCategoryTeam.getName());
									team.setLastUpdated(new Date());
									team.setLastUpdateddBy(username);
									if(odds <= 0){
										team.setStatus("DELETED");
									}
								}
								DAORegistry.getCrownJewelTeamsDAO().updateAll(teams);
							}
						}
					}
					DAORegistry.getCrownJewelCategoryTeamsDAO().saveAll(teamSaveList);
					DAORegistry.getCrownJewelCategoryTeamsDAO().updateAll(teamUpdateList);
					DAORegistry.getCrownJewelCategoryTeamsAuditDAO().saveAll(auditList);
					crownJewelTeamsDTO.setMessage("Selected Teams Saved/Updated Successfully.");
				
				} else if(action.equals("delete")) {
					List<CrownJewelCategoryTeams> teamList = new ArrayList<CrownJewelCategoryTeams>();
					List<CrownJewelCategoryTeamsAudit> teamAuditList = new ArrayList<CrownJewelCategoryTeamsAudit>();
					
					Map<String, String[]> requestParams = request.getParameterMap();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						if(key.contains("checkbox_")){
							Integer rowNumber = Integer.parseInt(key.replace("checkbox_", ""));
							String idStr = request.getParameter("id_"+rowNumber);
							if(idStr == null || idStr.isEmpty()) {
								continue;
							}
							Integer id=Integer.parseInt(idStr);
							CrownJewelCategoryTeams crownJewelCategoryTeamDB = DAORegistry.getCrownJewelCategoryTeamsDAO().get(id);
							List<CrownJewelTeams> teams = DAORegistry.getCrownJewelTeamsDAO().getAllActiveTeamsByCategoryTeamId(id);
							if(!teams.isEmpty()){
								for(CrownJewelTeams team : teams){
									if(cnt==5){
										break;
									}
									eventNames += team.getCrownJewelLeagues().getName()+",";
									cnt++;
								}
								msg = msg + crownJewelCategoryTeamDB.getName();
							}else{
								crownJewelCategoryTeamDB.setLastUpdated(now);
								crownJewelCategoryTeamDB.setLastUpdatedBy(username);
								crownJewelCategoryTeamDB.setStatus("DELETED");

								CrownJewelCategoryTeamsAudit teamAudit = new CrownJewelCategoryTeamsAudit(crownJewelCategoryTeamDB);
								teamAudit.setAction("DELETE");
								
								teamAuditList.add(teamAudit);
								teamList.add(crownJewelCategoryTeamDB);
							}
							
						}
					}
					
					DAORegistry.getCrownJewelCategoryTeamsDAO().updateAll(teamList);
					DAORegistry.getCrownJewelCategoryTeamsAuditDAO().saveAll(teamAuditList);
					if(!msg.isEmpty()){
						crownJewelTeamsDTO.setMessage("Not able to delete teams("+msg.substring(0,msg.length()-1)+") its attached with Events("+eventNames.substring(0,eventNames.length()-1)+"..).");
					}else{
						crownJewelTeamsDTO.setMessage("Selected Teams are Deleted sucessfully.");
					}
					
				}
			}
			List<FantasyChildCategory> childCategories = null;
			List<FantasyGrandChildCategory> grandChildCategories = null;
			List<CrownJewelLeagues> cjLeaguesList = null;
			List<CrownJewelCategoryTeams> cjCategoryTeams = null;
			if(childId != null && !childId.isEmpty()){
				grandChildCategories = DAORegistry.getFantasyGrandChildCategoryDAO().getGrandChildsByChildCategoryId(Integer.parseInt(childId));
			}
			if(grandChildId != null && !grandChildId.isEmpty()){
				cjCategoryTeams = DAORegistry.getCrownJewelCategoryTeamsDAO().getCategoryTeamsByGrandChild(Integer.parseInt(grandChildId));
			}
			childCategories = DAORegistry.getFantasyChildCategoryDAO().getAll();
			
			crownJewelTeamsDTO.setStatus(1);
			crownJewelTeamsDTO.setChildId(childId);
			crownJewelTeamsDTO.setGrandChildId(grandChildId);
			crownJewelTeamsDTO.setChildCategories(childCategories);
			crownJewelTeamsDTO.setGrandChildCategories(grandChildCategories);
			crownJewelTeamsDTO.setLeaguesList(cjLeaguesList);
			crownJewelTeamsDTO.setCrownJewelCategoryTeams(cjCategoryTeams);
		} catch(Exception e){
			e.printStackTrace();
		}
		return crownJewelTeamsDTO;
		//return "page-crown-jewel-category-teams";
	}
	
	
	@RequestMapping(value="/TeamsExportToExcel")
	public void getCrownJewelTeamsToExport(HttpServletRequest request, HttpServletResponse response){
		try{
			String leagueIdStr = request.getParameter("leagueId");
			Integer leagueId = null;
			if(leagueIdStr != null && leagueIdStr.trim().length() > 0) {
				leagueId = Integer.parseInt(leagueIdStr);
			}
			List<CrownJewelLeagues> cjLeaguesList = null;
			
			List<CrownJewelTeams> cjTeamsList = null;
			if(leagueId != null){
				cjTeamsList = DAORegistry.getCrownJewelTeamsDAO().getAllActiveTeamsByLeagueId(leagueId);
			}
		
			if(cjTeamsList!=null && !cjTeamsList.isEmpty()){
				HSSFWorkbook book = new HSSFWorkbook();
				HSSFSheet sheet = book.createSheet("fantasy_ticket_teams");
				HSSFRow header = sheet.createRow(0);
				header.createCell(0).setCellValue("Fantasy Ticket Team Id");
				header.createCell(1).setCellValue("League Id");
				header.createCell(2).setCellValue("Team Name");
				header.createCell(3).setCellValue("Odds");
				header.createCell(4).setCellValue("Cut off Date");
				int i=1; String packageApplicable = "";
				for(CrownJewelTeams team : cjTeamsList){
					HSSFRow row = sheet.createRow(i);					
					row.createCell(0).setCellValue(team.getId()!=null?team.getId():0);
					row.createCell(1).setCellValue(team.getLeagueId()!=null?team.getLeagueId():0);
					row.createCell(2).setCellValue(team.getName()!=null?team.getName():"");
					row.createCell(3).setCellValue(team.getOdds()!=null?team.getOdds():0);
					row.createCell(4).setCellValue(team.getCutOffDateStr()!=null?team.getCutOffDateStr():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=fantasy_ticket_teams.xls");
				book.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping(value="/CrownJewelTeamZones")
	public CrownJewelTeamZonesDTO getCrownJewelTeamZones(HttpServletRequest request, HttpServletResponse response){
		CrownJewelTeamZonesDTO crownJewelTeamZonesDTO = new CrownJewelTeamZonesDTO();
		Error error = new Error();
		//model.addAttribute("crownJewelTeamZonesDTO", crownJewelTeamZonesDTO);
		
		try {
			String childId = request.getParameter("childType");
			String grandChildId = request.getParameter("grandChildType");
			String action = request.getParameter("action");
			String leagueIdStr = request.getParameter("leagueId");
			String teamIdStr = request.getParameter("teamId");
			String copyFromLeagueIdStr = request.getParameter("copyFromLeagueId");
			Integer leagueId = null;
			if(leagueIdStr != null && leagueIdStr.trim().length() > 0) {
				leagueId = Integer.parseInt(leagueIdStr);
			}
			Integer teamId = null;
			if(teamIdStr != null && teamIdStr.trim().length() > 0) {
				teamId = Integer.parseInt(teamIdStr);
			}
			
			if(action != null) {
				String username = request.getParameter("userName");
				if(StringUtils.isEmpty(username)){
					System.err.println("Please Login.");
					error.setDescription("Please Login.");
					crownJewelTeamZonesDTO.setError(error);
					crownJewelTeamZonesDTO.setStatus(0);
					return crownJewelTeamZonesDTO;
				}
				
				Date now = new Date();
				
				List<CrownJewelTeamZones> cjzoneListDB= new ArrayList<CrownJewelTeamZones>();
				if(teamId != null) {
					cjzoneListDB = DAORegistry.getCrownJewelTeamZonesDAO().getAllActiveTeamZonesByTeamId(teamId);
				} else {
					cjzoneListDB = DAORegistry.getCrownJewelTeamZonesDAO().getAllActiveTeamZonesByLeagueId(leagueId);
				}
				Map<String,CrownJewelTeamZones> cjTeamZoneMap = new HashMap<String, CrownJewelTeamZones>();
				for (CrownJewelTeamZones cjTeamZones : cjzoneListDB) {
					String key = ""+cjTeamZones.getTeamId()+'_'+cjTeamZones.getZone().toUpperCase();
					cjTeamZoneMap.put(key, cjTeamZones);
				}
				if(action.equals("copy")) {
					String copyToLeagueIdStr = request.getParameter("copyToLeagueIds");
					if(copyToLeagueIdStr !=null && !copyToLeagueIdStr.isEmpty() && copyFromLeagueIdStr!=null && !copyFromLeagueIdStr.isEmpty()){
						Integer copyFromleague = Integer.parseInt(copyFromLeagueIdStr);
						List<CrownJewelTeams> souceTeams = DAORegistry.getCrownJewelTeamsDAO().getAllActiveTeamsByLeagueId(copyFromleague);
						String copyToIdArr[] = copyToLeagueIdStr.split(",");
						Integer teams = 0;
						Integer zoneSize = 0;
						for(String idStr:copyToIdArr){
							if(idStr==null || idStr.isEmpty()){
								continue;
							}
							CrownJewelLeagues league = DAORegistry.getCrownJewelLeaguesDAO().get(Integer.parseInt(idStr));
							List<CrownJewelTeams> destTeams = DAORegistry.getCrownJewelTeamsDAO().getAllActiveTeamsByLeagueId(Integer.parseInt(idStr));
							if(league.getEventId()==null){
								continue;
							}
							Event event = DAORegistry.getEventDAO().get(league.getEventId());
							List<String> zoneList = TMATDAORegistry.getCategoryDAO().getCategorySymbolsByVenueCategoryId(event.getVenueCategoryId());
							List<CrownJewelTeamZones> zonList = null;
							for(CrownJewelTeams sTeam  :souceTeams){
								for(CrownJewelTeams dTeam  :destTeams){
									if(sTeam.getCategoryTeamId()!=null && dTeam.getCategoryTeamId()!=null
											&& sTeam.getCategoryTeamId().equals(dTeam.getCategoryTeamId())){
										List<CrownJewelTeamZones> zones = DAORegistry.getCrownJewelTeamZonesDAO().getAllActiveTeamZonesByTeamId(sTeam.getId());
										CrownJewelTeamZones cjeZone = null;
										teams++;
										zonList = new ArrayList<CrownJewelTeamZones>();
										Boolean copyZones=false;
										for(CrownJewelTeamZones zone : zones){
											copyZones=false;
											for(String str : zoneList){
												if(str.equalsIgnoreCase(zone.getZone())){
													copyZones = true;
													break;
												}
											}
											if(copyZones){
												cjeZone = new CrownJewelTeamZones();
												cjeZone.setTeamId(dTeam.getId());
												cjeZone.setEventId(league.getEventId());
												cjeZone.setLeagueId(league.getId());
												cjeZone.setZone(zone.getZone());
												cjeZone.setLastUpdateddBy(username);
												cjeZone.setQuantityType(zone.getQuantityType());
												cjeZone.setLastUpdated(new Date());
												cjeZone.setTicketsCount(zone.getTicketsCount());
												cjeZone.setStatus(zone.getStatus());
												cjeZone.setPrice(zone.getPrice());
												zonList.add(cjeZone);
											}
											
										}
										zoneSize = zoneSize + zonList.size();
										if(!zoneList.isEmpty()){
											List<CrownJewelTeamZones> savedZones = DAORegistry.getCrownJewelTeamZonesDAO().getAllActiveTeamZonesByTeamId(dTeam.getId());
											if(!savedZones.isEmpty()){
												DAORegistry.getCrownJewelTeamZonesDAO().deleteAll(savedZones);
											}
											DAORegistry.getCrownJewelTeamZonesDAO().saveAll(zonList);
										}
										
									}
								}
							}
						}
						if(teams==0){
							crownJewelTeamZonesDTO.setMessage("No Single matching teams and Zones are found for selected souce and destination event.");
						}else if(zoneSize==0){
							crownJewelTeamZonesDTO.setMessage(" Matching Teams found but no matching zones are found.");
						}else{
							crownJewelTeamZonesDTO.setMessage(" Matching Teams found and "+zoneSize+" Matching Zones are copied to Selected Events");
						}
					}else{
						crownJewelTeamZonesDTO.setMessage("Please select souce and destination event to copy Zones.");
					}
				}else if(action.equals("update")) {
					List<CrownJewelTeamZones> zoneSaveList = new ArrayList<CrownJewelTeamZones>();
					List<CrownJewelTeamZones> zoneUpdateList = new ArrayList<CrownJewelTeamZones>();
					List<CrownJewelTeamZonesAudit> auditList = new ArrayList<CrownJewelTeamZonesAudit>();
					
					Map<String, String[]> requestParams = request.getParameterMap();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						if(key.contains("checkbox_")){
							String rowIdStr = key.replace("checkbox_", "");
							Integer rowTeamId = Integer.parseInt(request.getParameter("teamId_"+rowIdStr));
							String zone = request.getParameter("zone_"+rowIdStr);
							String priceStr = request.getParameter("price_"+rowIdStr);
							String quantityTypeStr = request.getParameter("quantityType_"+rowIdStr);
							
							String eventStr = request.getParameter("eventId_"+rowIdStr);
							Integer eventId = null;
							if(eventStr!=null && !eventStr.isEmpty()){
								eventId = Integer.parseInt(eventStr);
							}
							
							Integer ticketsCount = Integer.parseInt(request.getParameter("ticketsCount_"+rowIdStr));
							Integer soldTicketsCount = Integer.parseInt(request.getParameter("soldTicketsCount_"+rowIdStr));
							
							CrownJewelTeamZones cjTeamZone = cjTeamZoneMap.get(rowIdStr);
							if(cjTeamZone == null){
								//Existing Team Zones - Status set Deleted
								List<CrownJewelTeamZones> delTeamZonesList = new ArrayList<CrownJewelTeamZones>();
								List<CrownJewelTeamZonesAudit> delAudits = new ArrayList<CrownJewelTeamZonesAudit>();
								List<CrownJewelTeamZones> cjTemZoneList = DAORegistry.getCrownJewelTeamZonesDAO().getAllActiveTeamZonesByTeamId(rowTeamId);
								
								if(cjTemZoneList != null && cjTemZoneList.size() > 0){
									for(CrownJewelTeamZones cjTemZone : cjTemZoneList){
										cjTemZone.setLastUpdated(now);
										cjTemZone.setLastUpdateddBy(username);
										cjTemZone.setStatus("DELETED");
										
										CrownJewelTeamZonesAudit audit = new CrownJewelTeamZonesAudit(cjTemZone);
										audit.setAction("DELETE");
										
										delAudits.add(audit);
										delTeamZonesList.add(cjTemZone);
									}
									DAORegistry.getCrownJewelTeamZonesDAO().deleteAll(delTeamZonesList);
									DAORegistry.getCrownJewelTeamZonesAuditDAO().saveAll(delAudits);
								}
							}
							
							if(cjTeamZone == null) {
								cjTeamZone = new CrownJewelTeamZones();
							}
							if(priceStr!=null && !priceStr.isEmpty()){
								cjTeamZone.setPrice(Double.parseDouble(priceStr));
							}
							ZonesQuantityTypes quantityType =null;
							if(quantityTypeStr!=null && !quantityTypeStr.isEmpty()){
								quantityType = ZonesQuantityTypes.valueOf(quantityTypeStr);
							}
							cjTeamZone.setQuantityType(quantityType);
							cjTeamZone.setZone(zone);
							cjTeamZone.setLeagueId(leagueId);
							cjTeamZone.setTeamId(rowTeamId);
							cjTeamZone.setEventId(eventId);
							cjTeamZone.setLastUpdated(now);
							cjTeamZone.setLastUpdateddBy(username);
							cjTeamZone.setStatus("ACTIVE");
							cjTeamZone.setTicketsCount(ticketsCount);
							cjTeamZone.setSoldTicketsCount(soldTicketsCount);
							
							CrownJewelTeamZonesAudit audit = new CrownJewelTeamZonesAudit(cjTeamZone);
							if(cjTeamZone.getId() != null) {
								audit.setAction("UPDATE");
								
								zoneUpdateList.add(cjTeamZone);
							} else {
								audit.setAction("CREATE");
								
								zoneSaveList.add(cjTeamZone);
							}
							auditList.add(audit);
						}
					}
					DAORegistry.getCrownJewelTeamZonesDAO().saveAll(zoneSaveList);
					DAORegistry.getCrownJewelTeamZonesDAO().updateAll(zoneUpdateList);
					DAORegistry.getCrownJewelTeamZonesAuditDAO().saveAll(auditList);
					
					crownJewelTeamZonesDTO.setMessage("Fantasy Sports Tickets Team Zones Saved Successfully.");
					
				
				} else if(action.equals("delete")) {

					List<CrownJewelTeamZones> teamZonesList = new ArrayList<CrownJewelTeamZones>();
					List<CrownJewelTeamZonesAudit> audits = new ArrayList<CrownJewelTeamZonesAudit>();
					
					
					Map<String, String[]> requestParams = request.getParameterMap();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						if(key.contains("checkbox_")){
							String rowIdStr = key.replace("checkbox_", "");
							Integer rowTeamId = Integer.parseInt(request.getParameter("teamId_"+rowIdStr));
							String zone = request.getParameter("zone_"+rowIdStr);
							//String idStr = request.getParameter("id_"+rowIdStr);
					
							CrownJewelTeamZones cjTeamZone = cjTeamZoneMap.get(rowIdStr);
							if(cjTeamZone == null) {
								continue;
							}
							cjTeamZone.setLastUpdated(now);
							cjTeamZone.setLastUpdateddBy(username);
							cjTeamZone.setStatus("DELETED");
							
							CrownJewelTeamZonesAudit audit = new CrownJewelTeamZonesAudit(cjTeamZone);
							audit.setAction("DELETE");
							
							audits.add(audit);
							teamZonesList.add(cjTeamZone);
						}
					}
					DAORegistry.getCrownJewelTeamZonesDAO().deleteAll(teamZonesList);
					DAORegistry.getCrownJewelTeamZonesAuditDAO().saveAll(audits);
					
					crownJewelTeamZonesDTO.setMessage("Fantasy Sports Tickets Team Zones Deleted Successfully.");
				
				}
			}
			
			List<FantasyChildCategory> childCategories = null;
			List<FantasyGrandChildCategory> grandChildCategories = null;			
			List<CrownJewelLeagues> cjLeaguesList = null;
			List<CrownJewelLeagues> finalLeagues = new ArrayList<CrownJewelLeagues>();
			
			if(childId!=null && !childId.isEmpty()){
				grandChildCategories = DAORegistry.getFantasyGrandChildCategoryDAO().getGrandChildsByChildCategoryId(Integer.parseInt(childId));
			}if(grandChildId!=null && !grandChildId.isEmpty()){
				cjLeaguesList = DAORegistry.getCrownJewelLeaguesDAO().getActiveLeaguesByGrandChildId(Integer.parseInt(grandChildId));
			}
			if(cjLeaguesList!=null){
				for(CrownJewelLeagues league : cjLeaguesList){
					if(league.getEventId()!=null){
						finalLeagues.add(league);
					}
					
				}
			}
			
			if(leagueId != null){
				CrownJewelLeagues cjLeague = DAORegistry.getCrownJewelLeaguesDAO().get(leagueId);	
				List<CrownJewelTeamZones> cjzoneListDB= new ArrayList<CrownJewelTeamZones>();
				List<CrownJewelTeamZones> zoneSoldTixListDB = new ArrayList<CrownJewelTeamZones>();
				List<CrownJewelTeams> cjTeamsList = null;
				String savedZoneStr = "";
				if(teamId != null) {
					CrownJewelTeams crownjewelTeam = DAORegistry.getCrownJewelTeamsDAO().get(teamId);
					cjTeamsList = new ArrayList<CrownJewelTeams>();
					cjTeamsList.add(crownjewelTeam);
					cjzoneListDB = DAORegistry.getCrownJewelTeamZonesDAO().getAllActiveTeamZonesByTeamId(teamId);
					
					zoneSoldTixListDB = DAORegistry.getQueryManagerDAO().getCrownJewelTeamZonesByTeamId(teamId);
				} else {
					cjTeamsList = DAORegistry.getCrownJewelTeamsDAO().getAllActiveTeamsByLeagueId(leagueId);
					cjzoneListDB = DAORegistry.getCrownJewelTeamZonesDAO().getAllActiveTeamZonesByLeagueId(leagueId);
					
					zoneSoldTixListDB = DAORegistry.getQueryManagerDAO().getCrownJewelTeamZonesByLeagueId(leagueId);
				}
				
				Map<String,CrownJewelTeamZones> cjTeamZoneMap = new HashMap<String, CrownJewelTeamZones>();
				for (CrownJewelTeamZones cjTeamZones : cjzoneListDB) {
					savedZoneStr = cjTeamZones.getZone().toUpperCase();
					String key = ""+cjTeamZones.getTeamId()+'_'+cjTeamZones.getZone().toUpperCase();
					cjTeamZoneMap.put(key, cjTeamZones);
				}
				
				Map<String,CrownJewelTeamZones> zoneSoldTixMap = new HashMap<String, CrownJewelTeamZones>();
				for (CrownJewelTeamZones zoneZoldTix : zoneSoldTixListDB) {
					String key = ""+zoneZoldTix.getTeamId()+'_'+zoneZoldTix.getZone().toUpperCase();
					zoneSoldTixMap.put(key, zoneZoldTix);
				}
				List<CrownJewelTeamZones> cjTeamZone = new ArrayList<CrownJewelTeamZones>();
				if(cjLeague.getEventId()!=null){
					Event event = DAORegistry.getEventDAO().get(cjLeague.getEventId());
					List<String> zoneList = TMATDAORegistry.getCategoryDAO().getCategorySymbolsByVenueCategoryId(event.getVenueCategoryId());
					for (CrownJewelTeams crownJewelTeams : cjTeamsList) {
						for (String zone : zoneList) {
							String key = crownJewelTeams.getId()+"_"+zone.toUpperCase();
							CrownJewelTeamZones cjTeamZones = cjTeamZoneMap.remove(key);
							if(cjTeamZones == null) {
								cjTeamZones = new CrownJewelTeamZones();
								cjTeamZones.setTeamId(crownJewelTeams.getId());
								cjTeamZones.setZone(zone.toUpperCase());
							}
							cjTeamZones.setEventId(crownJewelTeams.getCrownJewelLeagues().getEventId());
							cjTeamZones.setEventId(crownJewelTeams.getCrownJewelLeagues().getEventId());
							cjTeamZones.setTeam(crownJewelTeams);
							
							CrownJewelTeamZones zoneSoldTix = zoneSoldTixMap.get(key);
							if(zoneSoldTix != null) {
								cjTeamZones.setSoldTicketsCount(zoneSoldTix.getSoldTicketsCount());
								cjTeamZones.setPointsRedeemed(zoneSoldTix.getPointsRedeemed());
							} else {
								cjTeamZones.setSoldTicketsCount(0);
								cjTeamZones.setPointsRedeemed(0);
							}
							if(cjTeamZones.getTicketsCount()==0 && cjTeamZones.getSoldTicketsCount()==0){
								cjTeamZones.setTicketsCount(10);//initialise default to 10.
							}else{
								int availableTixCount = cjTeamZones.getTicketsCount()-cjTeamZones.getSoldTicketsCount();
								if(availableTixCount>0) {
									cjTeamZones.setAvailableTixCount(availableTixCount);
								} else {
									cjTeamZones.setAvailableTixCount(0);
								}
							}
							//if zone price is null consider it from CJE ticket cheapest ticket for given zone
							cjTeamZones.setIsAutoPrice(true);
							Double price = DAORegistry.getQueryManagerDAO().getCheapestZonesPrice(cjTeamZones.getEventId(), zone);
							if(price!=null && price > 0){
								cjTeamZones.setPrice(price);
							}else{
								cjTeamZones.setIsAutoPrice(false);
							}
							
							
							cjTeamZone.add(cjTeamZones);
						}
					}
				}
				crownJewelTeamZonesDTO.setCjTeamZone(cjTeamZone);
				crownJewelTeamZonesDTO.setSavedZoneStr(savedZoneStr);
				List<CrownJewelTeams> allteamList = DAORegistry.getCrownJewelTeamsDAO().getAllActiveTeamsByLeagueId(leagueId);
				if(allteamList == null || allteamList.isEmpty()){
					crownJewelTeamZonesDTO.setMessage("No Teams are added in selected league, Please add teams first.");
				}
				crownJewelTeamZonesDTO.setTeamsList(allteamList);
			}
			//List<FantasyParentCategory> parentCategoryList = DAORegistry.getFantasyParentCategoryDAO().getAll();
			childCategories = DAORegistry.getFantasyChildCategoryDAO().getAll();
			crownJewelTeamZonesDTO.setStatus(1);
			crownJewelTeamZonesDTO.setChildId(childId);
			crownJewelTeamZonesDTO.setQuantityTypes(ZonesQuantityTypes.values());
			crownJewelTeamZonesDTO.setGrandChildId(grandChildId);
			crownJewelTeamZonesDTO.setChildCategories(childCategories);
			crownJewelTeamZonesDTO.setGrandChildCategories(grandChildCategories);
			//map.put("parentCategoryList", parentCategoryList);
			crownJewelTeamZonesDTO.setLeaguesList(finalLeagues);
			crownJewelTeamZonesDTO.setLeagueId(leagueIdStr);
			crownJewelTeamZonesDTO.setTeamId(teamIdStr);
			crownJewelTeamZonesDTO.setCopyFromLeagueIdStr(copyFromLeagueIdStr);
		} catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching/updating Fantasy Sports Tickets Zones");
			crownJewelTeamZonesDTO.setError(error);
			crownJewelTeamZonesDTO.setStatus(0);
		}
		return crownJewelTeamZonesDTO;
	}
	
	@RequestMapping("/CrownJewelOrders")
	public CrownJewelOrdersDTO getCrownJewelOrdersPage(HttpServletRequest request, HttpServletResponse response){
		CrownJewelOrdersDTO crownJewelOrdersDTO = new CrownJewelOrdersDTO();
		Error error = new Error();
		//model.addAttribute("crownJewelOrdersDTO", crownJewelOrdersDTO);		
		
		try {
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String teamName = request.getParameter("teamName");
			String leagueName = request.getParameter("leagueName");		
			String status = request.getParameter("status");
			String orderId = request.getParameter("orderId");
			String parentCategory = request.getParameter("parentCategory");
			String grandChildCategory = request.getParameter("grandChildCategory");

			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			String fromDateFinal=null;
			String toDateFinal = null;
			List<CrownJewelCustomerOrder> CJEOrders = null;
			Integer count = 0;
			
			GridHeaderFilters filter = null;
			try{
				filter = GridHeaderFiltersUtil.getCrownJewelOrderSearchHeaderFilters(headerFilter);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				crownJewelOrdersDTO.setError(error);
				crownJewelOrdersDTO.setStatus(0);
				return crownJewelOrdersDTO;
			}
			
			if(status.equalsIgnoreCase("All")){
				status = "";
			}			
			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " 00:00:00"; 
				toDateFinal = toDateStr + " 23:59:59";
			}
			
			CJEOrders = DAORegistry.getCrownJewelCustomerOrderDAO().getCronJewelOrdersByFilter(orderId,fromDateFinal,toDateFinal,parentCategory,grandChildCategory,teamName,leagueName,status,pageNo,filter);
			count = DAORegistry.getQueryManagerDAO().getCronJewelOrdersByFilterCount(orderId,fromDateFinal,toDateFinal,parentCategory,grandChildCategory,teamName,leagueName,status,filter);
			
			crownJewelOrdersDTO.setStatus(1);
			crownJewelOrdersDTO.setFromDate(fromDateStr);	
			crownJewelOrdersDTO.setToDate(toDateStr);
			crownJewelOrdersDTO.setTeamName(teamName);
			crownJewelOrdersDTO.setLeagueName(leagueName);
			crownJewelOrdersDTO.setCjStatus(status);
			crownJewelOrdersDTO.setParentCategory(parentCategory);
			crownJewelOrdersDTO.setGrandChildCategory(grandChildCategory);
			crownJewelOrdersDTO.setOrders(com.rtw.tracker.utils.Util.getCrownJewelOrderArray(CJEOrders));
			crownJewelOrdersDTO.setPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
			crownJewelOrdersDTO.setParentCategories(DAORegistry.getFantasyParentCategoryDAO().getAll());
			crownJewelOrdersDTO.setGrandChildCategories(DAORegistry.getFantasyGrandChildCategoryDAO().getAll());
						
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Fantasy Orders.");
			crownJewelOrdersDTO.setError(error);
			crownJewelOrdersDTO.setStatus(0);
		}
		return crownJewelOrdersDTO;
	}	
	
	@RequestMapping("/FantasyOrderCreation")
	public FantasyOrderCreationDTO fantasyOrderCreation(HttpServletRequest request, HttpServletResponse response){
		FantasyOrderCreationDTO fantasyOrderCreationDTO = new FantasyOrderCreationDTO();
		Error error = new Error();
		
		try{
			String customerIdStr = request.getParameter("customerId");
			String fantasyCategoryIdStr = request.getParameter("grandChildCategoryId");
			String fantasyEventIdStr = request.getParameter("fantasyEventId");
			String fantasyTeamIdStr = request.getParameter("fantasyTeamId");
			String fantasyTicketIdStr = request.getParameter("fantasyTicketId");
			String fantasyTeamZoneIdStr = request.getParameter("fantasyTeamZoneId");
			String isRealTicketStr = request.getParameter("isRealTicket");
			String zoneStr = request.getParameter("zone");
			String quantityStr = request.getParameter("quantity");
			String requiredPointsStr = request.getParameter("requiredPoints");
						
			Map<String, String> map = Util.getParameterMap(request);
			map.put("customerId",customerIdStr);
			map.put("fCategoryId",fantasyCategoryIdStr);
			map.put("fEventId",fantasyEventIdStr);
			map.put("fTeamId",fantasyTeamIdStr);
			map.put("fTicketId",fantasyTicketIdStr);
			map.put("fTeamZoneId",fantasyTeamZoneIdStr);
			map.put("isRealTicket",isRealTicketStr);
			map.put("zone",zoneStr);
			map.put("quantity",quantityStr);
			map.put("requiredPoints",requiredPointsStr);
			
			String data = Util.getObject(map, sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.CREATE_FST_ORDER);
			Gson gson = new Gson();		
			JsonObject jsonObject1 = gson.fromJson(data, JsonObject.class);
			FantasySportsProduct fantasySportsProduct = gson.fromJson(((JsonObject)jsonObject1.get("fantasySportsProduct")), FantasySportsProduct.class);
			
			if(fantasySportsProduct.getStatus() == 1){
				fantasyOrderCreationDTO.setStatus(1);
				fantasyOrderCreationDTO.setMessage(fantasySportsProduct.getMessage());
				fantasyOrderCreationDTO.setOrderNo(fantasySportsProduct.getFantasyOrderNo());
			}else{
				error.setDescription(fantasySportsProduct.getError().getDescription());
				fantasyOrderCreationDTO.setError(error);
				fantasyOrderCreationDTO.setStatus(0);
			}
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while Create Fantasy Order.");
			fantasyOrderCreationDTO.setError(error);
			fantasyOrderCreationDTO.setStatus(0);
		}
		return fantasyOrderCreationDTO;
	}
	
	@RequestMapping("/CreateFantasyOrder")
	public CreateFantasyOrderDTO createFantasyOrder(HttpServletRequest request, HttpServletResponse response){
		CreateFantasyOrderDTO createFantasyOrderDTO = new CreateFantasyOrderDTO();
		Error error = new Error();
		
		try {			
			List<FantasyParentCategory> fantasyParentCategoryList = DAORegistry.getFantasyParentCategoryDAO().getAll();
			
			List<FantasyParentCategoryDTO> fantasyParentCategoryDTOs = new ArrayList<FantasyParentCategoryDTO>();
			FantasyParentCategoryDTO fantasyParentCategoryDTO = null;
			if(fantasyParentCategoryList != null && fantasyParentCategoryList.size() > 0){
				for(FantasyParentCategory parentCategory : fantasyParentCategoryList){
					fantasyParentCategoryDTO = new FantasyParentCategoryDTO();
					fantasyParentCategoryDTO.setId(parentCategory.getId());
					fantasyParentCategoryDTO.setName(parentCategory.getName());
					fantasyParentCategoryDTOs.add(fantasyParentCategoryDTO);
				}
			}
			
			createFantasyOrderDTO.setStatus(1);
			createFantasyOrderDTO.setFantasyParentCategoryDTOs(fantasyParentCategoryDTOs);
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Create Fantasy Order");
			createFantasyOrderDTO.setError(error);
			createFantasyOrderDTO.setStatus(0);
		}		
		return createFantasyOrderDTO;
	}
	
	
	
	/*@RequestMapping("/GetCrownJewelOrders")
	public void getCrownJewelOrders(HttpServletRequest request, HttpServletResponse response){//, ModelMap map){
		JSONObject returnObject = new JSONObject();
		String fromDateStr = request.getParameter("fromDate");
		String teamName = request.getParameter("teamName");
		String leagueName = request.getParameter("leagueName");
		String toDateStr = request.getParameter("toDate");		
		String status = request.getParameter("status");
		String pageNo = request.getParameter("pageNo");
		String headerFilter = request.getParameter("headerFilter");
		String parentCategory = request.getParameter("parentCategory");
		String grandChildCategory = request.getParameter("grandChildCategory");
		String fromDateFinal=null;
		String toDateFinal = null;
		List<CrownJewelCustomerOrder> CJEOrders = null;
		Integer count = 0;
		try {
			if(status.equalsIgnoreCase("All")){
				status = "";
			}
			GridHeaderFilters filter = GridHeaderFiltersUtil.getCrownJewelOrderSearchHeaderFilters(headerFilter);
			
			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " 00:00:00"; 
				toDateFinal = toDateStr + " 23:59:59";
				CJEOrders = DAORegistry.getCrownJewelCustomerOrderDAO().getCronJewelOrdersByFilter(null,fromDateFinal,toDateFinal,parentCategory,grandChildCategory,teamName,leagueName,status,pageNo,filter);
				count = DAORegistry.getQueryManagerDAO().getCronJewelOrdersByFilterCount(null,fromDateFinal,toDateFinal,parentCategory,grandChildCategory,teamName,leagueName,status,filter);
			}
			else{
			CJEOrders = DAORegistry.getCrownJewelCustomerOrderDAO().getCronJewelOrdersByFilter(null,fromDateFinal,toDateFinal,parentCategory,grandChildCategory,teamName,leagueName,status,pageNo,filter);
			count = DAORegistry.getQueryManagerDAO().getCronJewelOrdersByFilterCount(null,fromDateFinal,toDateFinal,parentCategory,grandChildCategory,teamName,leagueName,status,filter);
			}
		
			returnObject.put("fromDate", fromDateStr);
			returnObject.put("status", status);		
			returnObject.put("toDate", toDateStr);
			returnObject.put("teamName", teamName);
			returnObject.put("leagueName", leagueName);
			returnObject.put("grandChildCategory", grandChildCategory);
			returnObject.put("orders", JsonWrapperUtil.getCrownJewelOrderArray(CJEOrders));
			returnObject.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));
			IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	

	@RequestMapping({"/CrownJewelOrdersExportToExcel"})
	public void getCrownJewelOrdersToExport(HttpServletRequest request, HttpServletResponse response){
		
		try{
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String teamName = request.getParameter("teamName");
			String leagueName = request.getParameter("leagueName");
			String parentCategory = request.getParameter("parentCategory");	
			String grandChildCategory = request.getParameter("grandChildCategory");
			String status = request.getParameter("status");
			String headerFilter = request.getParameter("headerFilter");
			
			String fromDateFinal = null;
			String toDateFinal = null;
			
			GridHeaderFilters filter = GridHeaderFiltersUtil.getCrownJewelOrderSearchHeaderFilters(headerFilter);
			List<CrownJewelCustomerOrder> CJEOrdersList = null;
			if(status.equalsIgnoreCase("All")){
				status = "";
			}
			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " 00:00:00"; 
				toDateFinal = toDateStr + " 23:59:59";				
			}
			
			CJEOrdersList = DAORegistry.getCrownJewelCustomerOrderDAO().getCrownJewelOrdersByFilterToExport(null,fromDateFinal,toDateFinal,parentCategory,grandChildCategory,teamName,leagueName,status,filter);
						
			if(CJEOrdersList!=null && !CJEOrdersList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Fantasy_Order");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Fantasy Sports Tickets Order Id");
				header.createCell(1).setCellValue("Order Id");
				header.createCell(2).setCellValue("Event Name");
				header.createCell(3).setCellValue("Team Name");
				header.createCell(4).setCellValue("Venue");
				header.createCell(5).setCellValue("City");
				header.createCell(6).setCellValue("State");
				header.createCell(7).setCellValue("Country");
				header.createCell(8).setCellValue("Package Selected");
				header.createCell(9).setCellValue("Package Cost");
				//header.createCell(10).setCellValue("Package Notes");
				header.createCell(10).setCellValue("Event Id");
				header.createCell(11).setCellValue("Ticket Id");
				header.createCell(12).setCellValue("Zone");
				header.createCell(13).setCellValue("Ticket Qty");
				header.createCell(14).setCellValue("Ticket Price");
				header.createCell(15).setCellValue("Require Points");
				header.createCell(16).setCellValue("Customer Name");
				header.createCell(17).setCellValue("Grand Child Name");
				header.createCell(18).setCellValue("Platform");
				header.createCell(19).setCellValue("Status");
				header.createCell(20).setCellValue("Created Date");
				header.createCell(21).setCellValue("Last Updated Date");
				Integer i=1;
				for(CrownJewelCustomerOrder CJEOrder : CJEOrdersList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(CJEOrder.getId());
					row.createCell(1).setCellValue(CJEOrder.getRegularOrderId()!=null?CJEOrder.getRegularOrderId().toString():"");
					row.createCell(2).setCellValue(CJEOrder.getFantasyEventName()!=null?CJEOrder.getFantasyEventName():"");
					row.createCell(3).setCellValue(CJEOrder.getTeamName()!=null?CJEOrder.getTeamName():"");
					row.createCell(4).setCellValue(CJEOrder.getVenueName()!=null?CJEOrder.getVenueName():"");
					row.createCell(5).setCellValue(CJEOrder.getVenueCity()!=null?CJEOrder.getVenueCity():"");
					row.createCell(6).setCellValue(CJEOrder.getVenueState()!=null?CJEOrder.getVenueState():"");
					row.createCell(7).setCellValue(CJEOrder.getVenueCountry()!=null?CJEOrder.getVenueCountry():"");
					if(CJEOrder.getIsPackageSelected() != null && CJEOrder.getIsPackageSelected()){
						row.createCell(8).setCellValue("Yes");
					}else{
						row.createCell(8).setCellValue("No");
					}
					row.createCell(9).setCellValue(CJEOrder.getPackageCost()!=null?CJEOrder.getPackageCost():0.00);
					//row.createCell(10).setCellValue(CJEOrder.getPackageNote()!=null?CJEOrder.getPackageNote():"");
					row.createCell(10).setCellValue(CJEOrder.getEventId()!=null?CJEOrder.getEventId().toString():"");
					row.createCell(11).setCellValue(CJEOrder.getTicketId()!=null?CJEOrder.getTicketId().toString():"");
					row.createCell(12).setCellValue(CJEOrder.getTicketSelection()!=null?CJEOrder.getTicketSelection():"");
					row.createCell(13).setCellValue(CJEOrder.getTicketQty()!=null?CJEOrder.getTicketQty():0);
					row.createCell(14).setCellValue(CJEOrder.getTicketPrice()!=null?CJEOrder.getTicketPrice():0.00);
					//row.createCell(13).setCellValue(CJEOrder.getTicketPoints()!=null?CJEOrder.getTicketPoints().toString():"");
					row.createCell(15).setCellValue(CJEOrder.getRequiredPoints()!=null?CJEOrder.getRequiredPoints():0.00);
					//row.createCell(15).setCellValue(CJEOrder.getPackagePoints()!=null?CJEOrder.getPackagePoints().toString():"");
					row.createCell(16).setCellValue(CJEOrder.getCustomerName()!=null?CJEOrder.getCustomerName():"");
					//row.createCell(17).setCellValue(CJEOrder.getRemainingPoints()!=null?CJEOrder.getRemainingPoints().toString():"");
					row.createCell(17).setCellValue(CJEOrder.getGrandChildName()!=null?CJEOrder.getGrandChildName():"");
					row.createCell(18).setCellValue(CJEOrder.getPlatform()!=null?CJEOrder.getPlatform():"");
					row.createCell(19).setCellValue(CJEOrder.getStatus()!=null?CJEOrder.getStatus():"");
					row.createCell(20).setCellValue(CJEOrder.getCreatedDateStr()!=null?CJEOrder.getCreatedDateStr():"");
					row.createCell(21).setCellValue(CJEOrder.getLastUpdatedStr()!=null?CJEOrder.getLastUpdatedStr():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Fantasy_Order.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping("/ViewOrder")
	public CrownJewelOrderSummaryDTO getCrownJewelOrderById(HttpServletRequest request, HttpServletResponse response){
		CrownJewelOrderSummaryDTO crownJewelOrderSummaryDTO = new CrownJewelOrderSummaryDTO();
		Error error = new Error();
		
		try {
			String orderIdStr = request.getParameter("orderId");
			String msg = request.getParameter("msg");
			
			if(StringUtils.isEmpty(orderIdStr)){
				System.err.println("Please select Customer Order.");
				error.setDescription("Please select Customer Order.");
				crownJewelOrderSummaryDTO.setError(error);
				crownJewelOrderSummaryDTO.setStatus(0);
				return crownJewelOrderSummaryDTO;
			}
			Integer orderId = 0;
			try{
				orderId = Integer.parseInt(orderIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please select Valid Customer Order.");
				crownJewelOrderSummaryDTO.setError(error);
				crownJewelOrderSummaryDTO.setStatus(0);
				return crownJewelOrderSummaryDTO;
			}
			
			CrownJewelCustomerOrder CJEOrder = null;
			CJEOrder = DAORegistry.getCrownJewelCustomerOrderDAO().get(orderId);
			Integer regularOrderId = CJEOrder.getRegularOrderId();
			
			if(regularOrderId != null && regularOrderId > 0){
				CustomerOrder customerOrder = DAORegistry.getCustomerOrderDAO().get(CJEOrder.getRegularOrderId());
				CategoryTicketGroup catTicketGroup = DAORegistry.getCategoryTicketGroupDAO().get(customerOrder.getCategoryTicketGroupId());
				//Invoice invoice = DAORegistry.getInvoiceDAO().get(CJEOrder.getRegularOrderId());
				///map.put("invoice", invoice);
				//map.put("order", customerOrder);
				if(catTicketGroup != null){
					CategoryTicketGroupListDTO categoryTicketGroupListDTO = new CategoryTicketGroupListDTO();					
					categoryTicketGroupListDTO.setSection(catTicketGroup.getSection());
					categoryTicketGroupListDTO.setRow(catTicketGroup.getRow());
					categoryTicketGroupListDTO.setQuantity(catTicketGroup.getQuantity());
					categoryTicketGroupListDTO.setPrice(String.format("%.2f", catTicketGroup.getPrice()));
					
					crownJewelOrderSummaryDTO.setCategoryTicketGroupDTO(categoryTicketGroupListDTO);
				}				
			}else{
				if(CJEOrder.getIsRealTicket() != null && CJEOrder.getIsRealTicket()){
					CrownJewelCategoryTicket crownJewelCategoryTicket = DAORegistry.getCrownJewelCategoryTicketDAO().get(CJEOrder.getTicketId());
					if(crownJewelCategoryTicket != null){
						CategoryTicketGroupListDTO categoryTicketGroupListDTO = new CategoryTicketGroupListDTO();						
						categoryTicketGroupListDTO.setSection(crownJewelCategoryTicket.getSection());
						categoryTicketGroupListDTO.setRow("");
						categoryTicketGroupListDTO.setQuantity(crownJewelCategoryTicket.getQuantity());
						categoryTicketGroupListDTO.setPrice(String.format("%.2f", crownJewelCategoryTicket.getZonePrice()));
						
						crownJewelOrderSummaryDTO.setCategoryTicketGroupDTO(categoryTicketGroupListDTO);
					}
				}else{
					CrownJewelTeamZones crownJewelTeamZones = DAORegistry.getCrownJewelTeamZonesDAO().get(CJEOrder.getTicketId());
					if(crownJewelTeamZones != null){
						CategoryTicketGroupListDTO categoryTicketGroupListDTO = new CategoryTicketGroupListDTO();
						categoryTicketGroupListDTO.setSection(crownJewelTeamZones.getZone());
						categoryTicketGroupListDTO.setRow("");
						categoryTicketGroupListDTO.setQuantity(crownJewelTeamZones.getTicketsCount());
						categoryTicketGroupListDTO.setPrice(String.format("%.2f", crownJewelTeamZones.getPrice()));

						crownJewelOrderSummaryDTO.setCategoryTicketGroupDTO(categoryTicketGroupListDTO);
					}
				}
			}
			Customer customer = DAORegistry.getCustomerDAO().get(CJEOrder.getCustomerId());
			Event event = DAORegistry.getEventDAO().get(CJEOrder.getEventId());
			CustomerLoyaltyHistory customerLoyaltyHistory = null;
			if(CJEOrder.getRegularOrderId() != null){
				customerLoyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyByOrderId(CJEOrder.getRegularOrderId());
			}
			if(CJEOrder != null){
				CrownJewelCustomerOrderDTO crownJewelCustomerOrderDTO = new CrownJewelCustomerOrderDTO();
				crownJewelCustomerOrderDTO.setId(CJEOrder.getId());
				crownJewelCustomerOrderDTO.setRegularOrderId(CJEOrder.getRegularOrderId());
				crownJewelCustomerOrderDTO.setCreatedDateStr(CJEOrder.getCreatedDateStr());
				crownJewelCustomerOrderDTO.setStatus(CJEOrder.getStatus());
				crownJewelCustomerOrderDTO.setLeagueName(CJEOrder.getFantasyEventName());
				crownJewelCustomerOrderDTO.setTeamName(CJEOrder.getTeamName());
				if(CJEOrder.getIsPackageSelected()){
					crownJewelCustomerOrderDTO.setIsPackageSelected("Yes");
				}else{
					crownJewelCustomerOrderDTO.setIsPackageSelected("No");
				}
				
				crownJewelOrderSummaryDTO.setCrownJewelCustomerOrderDTO(crownJewelCustomerOrderDTO);
			}
			if(customer != null){
				CustomersCustomDTO customersCustomDTO = new CustomersCustomDTO(); 
				customersCustomDTO.setCustomerName(customer.getCustomerName());
				customersCustomDTO.setLastName(customer.getLastName());
				customersCustomDTO.setEmail(customer.getEmail());
				customersCustomDTO.setPhone(customer.getPhone());
				
				crownJewelOrderSummaryDTO.setCustomersCustomDTO(customersCustomDTO);
			}
			if(event != null){
				EventDetailsDTO eventDetailsDTO = new EventDetailsDTO();
				eventDetailsDTO.setEventName(event.getEventName());
				eventDetailsDTO.setEventDateStr(event.getEventDateStr());
				eventDetailsDTO.setEventTimeStr(event.getEventTimeStr());
				
				crownJewelOrderSummaryDTO.setEventDetailsDTO(eventDetailsDTO);
			}
			if(customerLoyaltyHistory != null){
				CustomerLoyaltyDTO customerLoyaltyDTO = new CustomerLoyaltyDTO();
				customerLoyaltyDTO.setPointsSpent(customerLoyaltyHistory.getPointsSpent());
				
				crownJewelOrderSummaryDTO.setCustomerLoyaltyDTO(customerLoyaltyDTO);
			}
			
			if(regularOrderId != null && regularOrderId > 0){
				crownJewelOrderSummaryDTO.setInvoiceId(regularOrderId);
			}
			crownJewelOrderSummaryDTO.setStatus(1);
			
			if(msg!=null && !msg.isEmpty()){
				crownJewelOrderSummaryDTO.setMessage(msg);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching CrownJewel Customer Order Summary.");
			crownJewelOrderSummaryDTO.setError(error);
			crownJewelOrderSummaryDTO.setStatus(0);
		}
		return crownJewelOrderSummaryDTO;
	}
	
	@RequestMapping("/UpdateCrownJewelOrder")
	public CrownJewelOrderUpdateReasonDTO updateCrownJewelOrder(HttpServletRequest request, HttpServletResponse response){
		CrownJewelOrderUpdateReasonDTO crownJewelOrderUpdateReasonDTO = new CrownJewelOrderUpdateReasonDTO();
		Error error = new Error();
		
		try {
			String orderIds = request.getParameter("orderIds");
			String reason = request.getParameter("reason");
			String status = request.getParameter("status");
			String username = request.getParameter("userName");
			
			List<CrownJewelCustomerOrderDTO> crownJewelCustomerOrderDTOs = new ArrayList<CrownJewelCustomerOrderDTO>();
			CrownJewelCustomerOrderDTO crownJewelCustomerOrderDTO = null;
			
			if(StringUtils.isEmpty(orderIds)){
				System.err.println("Please Select Customer Order to Update.");
				error.setDescription("Please Select Customer Order to Update.");
				crownJewelOrderUpdateReasonDTO.setError(error);
				crownJewelOrderUpdateReasonDTO.setStatus(0);
				return crownJewelOrderUpdateReasonDTO;
			}
			if(StringUtils.isEmpty(reason)){
				System.err.println("Please Specify Reason to Update.");
				error.setDescription("Please Specify Reason to Update.");
				crownJewelOrderUpdateReasonDTO.setError(error);
				crownJewelOrderUpdateReasonDTO.setStatus(0);
				return crownJewelOrderUpdateReasonDTO;
			}
			if(StringUtils.isEmpty(status)){
				System.err.println("Please Specify Status to Update.");
				error.setDescription("Please Specify Status to Update.");
				crownJewelOrderUpdateReasonDTO.setError(error);
				crownJewelOrderUpdateReasonDTO.setStatus(0);
				return crownJewelOrderUpdateReasonDTO;
			}
			
			if(orderIds != null && !orderIds.isEmpty()){
				String arr[] = orderIds.split(",");
				
				for(String orderId : arr){
					CrownJewelCustomerOrder order = DAORegistry.getCrownJewelCustomerOrderDAO().get(Integer.parseInt(orderId));
					CrownJewelOrderStatus orderStatus = CrownJewelOrderStatus.valueOf(status);
					order.setStatus(orderStatus.toString());
					order.setAcceptRejectReson(reason);
					
					crownJewelCustomerOrderDTO = new CrownJewelCustomerOrderDTO();
					crownJewelCustomerOrderDTO.setId(order.getId());
					crownJewelCustomerOrderDTOs.add(crownJewelCustomerOrderDTO);
					
					CustomerOrder customerOrder = null;
					if(orderStatus.equals(CrownJewelOrderStatus.APPROVED)){
						if(order.getRegularOrderId()==null){
							CrownJewelCategoryTicket catTicketGroup = null;
							CrownJewelTeamZones crownJewelTeamZones = null;
							EventDetails event = DAORegistry.getEventDetailsDAO().get(order.getEventId());
							if(order.getIsRealTicket() != null && order.getIsRealTicket()){
								catTicketGroup = DAORegistry.getCrownJewelCategoryTicketDAO().get(order.getTicketId());
								if(catTicketGroup==null){
									System.err.println("Ticket group is not found for one of selected order.");
									error.setDescription("Ticket group is not found for one of selected order.");
									crownJewelOrderUpdateReasonDTO.setError(error);
									crownJewelOrderUpdateReasonDTO.setStatus(0);
									return crownJewelOrderUpdateReasonDTO;
								}
							}else{
								crownJewelTeamZones = DAORegistry.getCrownJewelTeamZonesDAO().get(order.getTicketId());
								if(crownJewelTeamZones==null){
									System.err.println("Ticket Information is not found for selected team's order.");
									error.setDescription("Ticket Information is not found for selected team's order.");
									crownJewelOrderUpdateReasonDTO.setError(error);
									crownJewelOrderUpdateReasonDTO.setStatus(0);
									return crownJewelOrderUpdateReasonDTO;
								}
							}
							CustomerAddress blAddress = DAORegistry.getCustomerAddressDAO().getCustomerAddressInfo(order.getCustomerId());
							List<CustomerAddress> shippingAddresses = DAORegistry.getCustomerAddressDAO().getShippingAddressInfo(order.getCustomerId());
							
							if(event==null){
								System.err.println("Event not found in system for one of selected order.");
								error.setDescription("Event not found in system for one of selected order.");
								crownJewelOrderUpdateReasonDTO.setError(error);
								crownJewelOrderUpdateReasonDTO.setStatus(0);
								return crownJewelOrderUpdateReasonDTO;
							}
							if(blAddress==null){
								System.err.println("Customer billing address is not found for one of selected order.");
								error.setDescription("Customer billing address is not found for one of selected order.");
								crownJewelOrderUpdateReasonDTO.setError(error);
								crownJewelOrderUpdateReasonDTO.setStatus(0);
								return crownJewelOrderUpdateReasonDTO;
							}
							if(shippingAddresses==null || shippingAddresses.isEmpty()){
								System.err.println("Customer shipping address is not found for one of selected order.");
								error.setDescription("Customer shipping address is not found for one of selected order.");
								crownJewelOrderUpdateReasonDTO.setError(error);
								crownJewelOrderUpdateReasonDTO.setStatus(0);
								return crownJewelOrderUpdateReasonDTO;
							}
							CustomerAddress shippingAddress = shippingAddresses.get(0);
							customerOrder = createNewCustomerOrderAndInvoice(order,event,catTicketGroup,crownJewelTeamZones,blAddress,shippingAddress,username);
							order.setRegularOrderId(customerOrder.getId());							
						}
					}else if(orderStatus.equals(CrownJewelOrderStatus.REJECTED)){
						if(order.getRegularOrderId()!=null){
							customerOrder = DAORegistry.getCustomerOrderDAO().get(order.getRegularOrderId());
							Invoice invoice = DAORegistry.getInvoiceDAO().getInvoiceByOrderId(order.getRegularOrderId());
							customerOrder.setStatus(OrderStatus.VOIDED.toString());
							customerOrder.setLastUpdated(new Date());
							invoice.setStatus(InvoiceStatus.Voided);
							invoice.setLastUpdated(new Date());
							invoice.setLastUpdatedBy(username);
							DAORegistry.getInvoiceDAO().update(invoice);
							DAORegistry.getCustomerOrderDAO().update(customerOrder);
						}
					}
					DAORegistry.getCrownJewelCustomerOrderDAO().update(order);
					
					crownJewelOrderUpdateReasonDTO.setStatus(1);
					crownJewelOrderUpdateReasonDTO.setMessage("selected orders are updated successfully.");
				}				
			}
						
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Updating Crown Jeweel Customer Order.");
			crownJewelOrderUpdateReasonDTO.setError(error);
			crownJewelOrderUpdateReasonDTO.setStatus(0);
		}
		return crownJewelOrderUpdateReasonDTO;
	}
	
	
	@RequestMapping("/CrownJewelFinalTeam")
	public CrownJewelTeamsDTO GetCrownJewelFinalTeam(HttpServletRequest request, HttpServletResponse response){
		CrownJewelTeamsDTO crownJewelTeamsDTO = new CrownJewelTeamsDTO();
		Error error = new Error();
		//model.addAttribute("crownJewelTeamsDTO", crownJewelTeamsDTO);
		
		String childId = request.getParameter("childType");
		String grandChildId = request.getParameter("grandChildType");
		String action = request.getParameter("action");
		String leagueIdStr = request.getParameter("leagueId");
		String statusId = request.getParameter("statusId");
		List<Map<String, Object>> dataList = new ArrayList<Map<String,Object>>();
		try {
			Integer leagueId = null;
			if(leagueIdStr != null && leagueIdStr.trim().length() > 0) {
				leagueId = Integer.parseInt(leagueIdStr);
			}
			if(action != null) {
				String username = request.getParameter("userName");
				if(StringUtils.isEmpty(username)){
					System.err.println("Please Login.");
					error.setDescription("Please Login.");
					crownJewelTeamsDTO.setError(error);
					crownJewelTeamsDTO.setStatus(0);
					return crownJewelTeamsDTO;
				}
				
				Date now = new Date();
				 if(action.equals("Exclude")) {
					List<CrownJewelTeams> teamList = new ArrayList<CrownJewelTeams>();
					List<CrownJewelTeamsAudit> teamAuditList = new ArrayList<CrownJewelTeamsAudit>();
					boolean isOrderUpdated = true;
					List<CrownJewelTeamZones> cjtZoneList = new ArrayList<CrownJewelTeamZones>();
					List<CrownJewelTeamZonesAudit> zouneaudits = new ArrayList<CrownJewelTeamZonesAudit>();
					List<CrownJewelEvents> eventsList = new ArrayList<CrownJewelEvents>();
					List<CrownJewelEventsAudit> eventAuditList = new ArrayList<CrownJewelEventsAudit>();
					/*List<CrownJewelCustomerOrder> orderList = new ArrayList<CrownJewelCustomerOrder>();
					List<CustomerOrder> regularOrderList = new ArrayList<CustomerOrder>();
					List<Invoice> invoiceList = new ArrayList<Invoice>();*/
					
					Map<String, String[]> requestParams = request.getParameterMap();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						if(key.contains("checkbox_")){
							Integer rowNumber = Integer.parseInt(key.replace("checkbox_", ""));
							String idStr = request.getParameter("id_"+rowNumber);
							if(idStr == null || idStr.isEmpty()) {
								continue;
							}
							Integer id=Integer.parseInt(idStr);
							
							CrownJewelTeams crownJewelTeamDB = DAORegistry.getCrownJewelTeamsDAO().get(id);
							crownJewelTeamDB.setLastUpdated(now);
							crownJewelTeamDB.setLastUpdateddBy(username);
							crownJewelTeamDB.setStatus("DELETED");
							
							//Setting status to crownjewel_zones_sold_count when delete the team
							DAORegistry.getCrownJewelZonesSoldDetailsDAO().updateDeleteStatusForTeam(id);
							
							CrownJewelTeamsAudit teamAudit = new CrownJewelTeamsAudit(crownJewelTeamDB);
							teamAudit.setAction("DELETED");
							
							teamAuditList.add(teamAudit);
							teamList.add(crownJewelTeamDB);
							
							CrownJewelEvents crownJewelEvent = null;
							if(crownJewelTeamDB.getEventId() != null) {
								crownJewelEvent = TMATDAORegistry.getCrownJewelEventsDAO().getCrownJewelEventByEventId(crownJewelTeamDB.getEventId());
							}
							if(crownJewelEvent != null) {
								crownJewelEvent.setLastUpdatedBy(username);
								crownJewelEvent.setLastUpdatedDate(now);
								crownJewelEvent.setStatus("DELETED");
								
								CrownJewelEventsAudit eventAudit = new CrownJewelEventsAudit(crownJewelEvent);
								eventAudit.setStatus("DELETED");
								eventAudit.setAction("DELETE");

								eventAuditList.add(eventAudit);
								eventsList.add(crownJewelEvent);
							}
							List<CrownJewelTeamZones> zonesListDB = DAORegistry.getCrownJewelTeamZonesDAO().getAllTeamZonesByTeamId(crownJewelTeamDB.getId());
							for (CrownJewelTeamZones cjtZone : zonesListDB) {
								cjtZone.setLastUpdated(now);
								cjtZone.setLastUpdateddBy(username);
								cjtZone.setStatus("DELETED");
						
								CrownJewelTeamZonesAudit zoneAudit = new CrownJewelTeamZonesAudit(cjtZone);
								zoneAudit.setAction("DELETE");

								zouneaudits.add(zoneAudit);
								cjtZoneList.add(cjtZone);
							}
							List<CrownJewelCustomerOrder> orders = DAORegistry.getCrownJewelCustomerOrderDAO().getCrownJewelOrdersByTeamId(crownJewelTeamDB.getId());
							if(!orders.isEmpty()){
								isOrderUpdated = updateOrdersAndRevertRewardPoints(orders,username);
							}
							/*List<CrownJewelCustomerOrder> orders = DAORegistry.getCrownJewelCustomerOrderDAO().getCrownJewelOrdersByTeamId(crownJewelTeamDB.getId());
							List<Integer> regularOrderIds = new ArrayList<Integer>();
							for (CrownJewelCustomerOrder order : orders) {
								order.setStatus(CrownJewelOrderStatus.REJECTED.toString());
								order.setLastUpdatedDate(now);
								order.setAcceptRejectReson("Order team/artist was Excluded.");
								if(order.getRegularOrderId()!=null){
									regularOrderIds.add(order.getRegularOrderId());
								}
								orderList.add(order);
							}
							if(!regularOrderIds.isEmpty()){
								List<CustomerOrder> regularOrders = DAORegistry.getCustomerOrderDAO().getCustomerOrderByIds(regularOrderIds);
								for(CustomerOrder order:regularOrders){
									order.setStatus(OrderStatus.VOIDED.toString());
									order.setLastUpdated(now);
									regularOrderList.add(order);
								}
								
								List<Invoice> invoices = DAORegistry.getInvoiceDAO().getInvoiceByOrderIds(regularOrderIds);
								for(Invoice invoice:invoices){
									invoice.setStatus(InvoiceStatus.Voided);
									invoice.setLastUpdated(now);
									invoice.setLastUpdatedBy(username);
									invoiceList.add(invoice);
								}
							}*/
						}
					}
					/*DAORegistry.getCrownJewelCustomerOrderDAO().updateAll(orderList);
					DAORegistry.getCustomerOrderDAO().updateAll(regularOrderList);
					DAORegistry.getInvoiceDAO().updateAll(invoiceList);
					
					for(CrownJewelCustomerOrder order : orderList){
						Map<String, String> requestMap = Util.getParameterMap(null);
						requestMap.put("customerId",String.valueOf(order.getCustomerId()));
						requestMap.put("orderType",OrderType.CROWNJEWEL.toString());
						requestMap.put("orderNo",String.valueOf(order.getId()));
						String data = Util.getObject(requestMap,Constants.BASE_URL + Constants.CANCEL_ORDER);
					}*/
					if(isOrderUpdated){
						DAORegistry.getCrownJewelTeamZonesDAO().updateAll(cjtZoneList);
						DAORegistry.getCrownJewelTeamsDAO().updateAll(teamList);
						DAORegistry.getCrownJewelTeamsAuditDAO().saveAll(teamAuditList);
						DAORegistry.getCrownJewelTeamZonesAuditDAO().saveAll(zouneaudits);
						TMATDAORegistry.getCrownJewelEventsDAO().updateAll(eventsList);
						TMATDAORegistry.getCrownJewelEventsAuditDAO().updateAll(eventAuditList);
						crownJewelTeamsDTO.setMessage("Selected Teams and all related Orders Excluded Successfully.");
					}else{
						crownJewelTeamsDTO.setMessage("Somehting went wrong, not able to exlude team.");
					}
				}else if(action.equals("Revert")){
					List<CrownJewelTeams> revertedTeamList = new ArrayList<CrownJewelTeams>();
					List<CrownJewelTeamsAudit> auditList = new ArrayList<CrownJewelTeamsAudit>();
					
					Map<String, String[]> requestParams = request.getParameterMap();
					for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
						String key = entry.getKey();
						if(key.contains("checkbox_")){
							Integer rowNumber = Integer.parseInt(key.replace("checkbox_", ""));
							String idStr = request.getParameter("id_"+rowNumber);
							CrownJewelTeams crownJewelTeam = null;
							if(idStr != null && idStr.trim().length() > 0) {
								crownJewelTeam = DAORegistry.getCrownJewelTeamsDAO().get(Integer.parseInt(idStr));	
								crownJewelTeam.setLastUpdated(now);
								crownJewelTeam.setLastUpdateddBy(username);
								crownJewelTeam.setStatus("ACTIVE");
							} 
							
							//Setting status to crownjewel_zones_sold_count when active the team
							if(idStr != null && idStr.trim().length() > 0) {
								DAORegistry.getCrownJewelZonesSoldDetailsDAO().updateActiveStatusForTeam(Integer.parseInt(idStr));
							}
							
							List<CrownJewelCustomerOrder> orderList = new ArrayList<CrownJewelCustomerOrder>();
							List<CustomerOrder> regularOrderList = new ArrayList<CustomerOrder>();
							List<Invoice> invoiceList = new ArrayList<Invoice>();
							List<CustomerLoyalty> loyaltyList = new ArrayList<CustomerLoyalty>();
							List<CustomerLoyaltyHistory> historyUpdateList = new ArrayList<CustomerLoyaltyHistory>();
							List<CrownJewelCustomerOrder> orders = DAORegistry.getCrownJewelCustomerOrderDAO().getCrownJewelOrdersByTeamId(crownJewelTeam.getId());
							List<Integer> regularOrderIds = new ArrayList<Integer>();
							CustomerLoyalty loyalty = null;
							for (CrownJewelCustomerOrder order : orders) {
								order.setStatus(CrownJewelOrderStatus.OUTSTANDING.toString());
								order.setLastUpdatedDate(now);
								order.setAcceptRejectReson("Team is Reverted back again");
								
								loyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(order.getCustomerId());
								CustomerLoyaltyHistory loyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getVoidCustomerLoyaltyHistoryOfCrownJewelOrder(order.getId(),order.getCustomerId());
								if(loyaltyHistory!=null){
									loyaltyHistory.setRewardStatus(RewardStatus.ACTIVE);
									loyaltyHistory.setUpdatedDate(now);
									historyUpdateList.add(loyaltyHistory);
								}
								CustomerLoyaltyHistory activeLoyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getActiveRefundCustomerLoyaltyHistoryOfCrownJewelOrder(order.getId(),order.getCustomerId());
								if(activeLoyaltyHistory!=null){
									activeLoyaltyHistory.setRewardStatus(RewardStatus.VOIDED);
									activeLoyaltyHistory.setUpdatedDate(now);
									historyUpdateList.add(activeLoyaltyHistory);
								}
								
								loyalty.setActivePoints(loyalty.getActivePoints() - order.getRequiredPoints());
								loyalty.setRevertedSpentPoints(loyalty.getRevertedSpentPoints() - order.getRequiredPoints());
								loyaltyList.add(loyalty);
								DAORegistry.getCustomerLoyaltyDAO().update(loyalty);
								
								if(order.getRegularOrderId()!=null){
									regularOrderIds.add(order.getRegularOrderId());
								}
								orderList.add(order);
							}
							if(!regularOrderIds.isEmpty()){
								List<CustomerOrder> regularOrders = DAORegistry.getCustomerOrderDAO().getCustomerOrderByIds(regularOrderIds);
								for(CustomerOrder order:regularOrders){
									order.setStatus(OrderStatus.ACTIVE.toString());
									order.setLastUpdated(now);
									regularOrderList.add(order);
								}
								
								List<Invoice> invoices = DAORegistry.getInvoiceDAO().getInvoiceByOrderIds(regularOrderIds);
								for(Invoice invoice:invoices){
									invoice.setStatus(InvoiceStatus.Outstanding);
									invoice.setLastUpdated(now);
									invoice.setLastUpdatedBy(username);
									invoiceList.add(invoice);
								}
							}
							DAORegistry.getCrownJewelCustomerOrderDAO().updateAll(orderList);
							DAORegistry.getCustomerOrderDAO().updateAll(regularOrderList);
							DAORegistry.getInvoiceDAO().updateAll(invoiceList);
							//DAORegistry.getCustomerLoyaltyDAO().updateAll(loyaltyList);
							DAORegistry.getCustomerLoyaltyHistoryDAO().updateAll(historyUpdateList);
							
							CrownJewelTeamsAudit audit = new CrownJewelTeamsAudit(crownJewelTeam);
							audit.setAction("REVERTED");
							revertedTeamList.add(crownJewelTeam);
							auditList.add(audit);
							
							 com.rtw.tracker.mail.MailAttachment[] mailAttachment = new com.rtw.tracker.mail.MailAttachment[1];
							 mailAttachment[0] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","logo.png",com.rtw.tmat.utils.Util.getFilePath(request, "logo.png", "/images/"));
							Map<String, Object> dataMap = new HashMap<String, Object>();
							dataMap.put("teamName", crownJewelTeam.getName());
							dataMap.put("userName", username);
							 mailManager.sendMailNow("text/html","sales@rewardthefan.com","msanghani@rightthisway.com,amit.raut@rightthisway.com", 
										null,null, "Fantasy Sports Tickets Team Reverted",
										"mail-fantasy-ticket-team-revert.html", dataMap, "text/html", null,mailAttachment,null);
						}
					}
					DAORegistry.getCrownJewelTeamsDAO().updateAll(revertedTeamList);
					DAORegistry.getCrownJewelTeamsAuditDAO().saveAll(auditList);
					statusId="ACTIVE";
					crownJewelTeamsDTO.setMessage("Selected Teams are reverted back to Active and all associated order updated to Active");
				} else if(action.equals("Save Final")){
					if(leagueId != null){
						boolean isOrderUpdated = true;
						Map<String, String[]> requestParams = request.getParameterMap();
						List<Integer> ids = new ArrayList<Integer>();
						for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
							String key = entry.getKey();
							if(key.contains("checkbox_")){
								Integer rowNumber = Integer.parseInt(key.replace("checkbox_", ""));
								String idStr = request.getParameter("id_"+rowNumber);
								if(idStr == null || idStr.isEmpty()) {
									continue;
								}
								Integer id=Integer.parseInt(idStr);
								ids.add(id);
							}
						}
						if(ids.size()==2){
							List<CrownJewelTeams> teams = DAORegistry.getCrownJewelTeamsDAO().getAllActiveTeamsByLeagueId(leagueId);
							List<CrownJewelTeams> finalTeams = new ArrayList<CrownJewelTeams>();
							List<CrownJewelTeams> otherTeams =  new ArrayList<CrownJewelTeams>();
							if(teams!=null && !teams.isEmpty()){
								for(CrownJewelTeams team :teams){
									if(ids.contains(team.getId())){
										finalTeams.add(team);
									}else{
										team.setStatus("DELETED");
										team.setLastUpdated(now);
										team.setLastUpdateddBy(username);
										otherTeams.add(team);
									}
								}
								
							}
							
							
							List<CrownJewelTeamZones> teamZones = DAORegistry.getCrownJewelTeamZonesDAO().getAllTeamZonesByLeagueId(leagueId);
							List<CrownJewelTeamZones> finalZones = new ArrayList<CrownJewelTeamZones>();
							List<CrownJewelTeamZones> otherZones =  new ArrayList<CrownJewelTeamZones>();
							if(teamZones!=null && teamZones.isEmpty()){
								for(CrownJewelTeamZones zone :teamZones){
									if(ids.contains(zone.getTeamId())){
										finalZones.add(zone);
									}else{
										zone.setStatus("DELETED");
										zone.setLastUpdated(now);
										zone.setLastUpdateddBy(username);
										otherZones.add(zone);
									}
								}
							}
							/*List<CrownJewelEvents> events = TMATDAORegistry.getCrownJewelEventsDAO().getCrownJewelEventByEventIds(eventIds);
							if(events!=null && events.isEmpty()){
								for(CrownJewelEvents event :events){
									event.setStatus("DELETED");
									event.setLastUpdatedDate(now);
									event.setLastUpdatedBy(username);
								}
							}*/
							
							List<CrownJewelCustomerOrder> orders = DAORegistry.getCrownJewelCustomerOrderDAO().getCrownJewelOrdersByLeagueId(leagueId);
							List<CrownJewelCustomerOrder> finalOrders = new ArrayList<CrownJewelCustomerOrder>();
							List<CrownJewelCustomerOrder> otherOrders =  new ArrayList<CrownJewelCustomerOrder>();
							//List<Integer> regularOrderIds = new ArrayList<Integer>();
							if(orders!=null && !orders.isEmpty()){
								for(CrownJewelCustomerOrder order :orders){
									if(ids.contains(order.getTeamId())){
										finalOrders.add(order);
										order.setStatus(CrownJewelOrderStatus.APPROVED.toString());
										order.setLastUpdatedDate(now);
										order.setAcceptRejectReson("Order team/artist is selected As final team");
									}else{
										order.setStatus(CrownJewelOrderStatus.REJECTED.toString());
										order.setLastUpdatedDate(now);
										order.setAcceptRejectReson("Order team/artist is not selected in final.");
										otherOrders.add(order);
										/*if(order.getRegularOrderId()!=null){
											regularOrderIds.add(order.getRegularOrderId());
										}*/
									}
								}
							}
							/*List<CustomerOrder> regularOrders = new ArrayList<CustomerOrder>();
							List<Invoice> invoices = new ArrayList<Invoice>();
							if(!regularOrderIds.isEmpty()){
								regularOrders = DAORegistry.getCustomerOrderDAO().getCustomerOrderByIds(regularOrderIds);
								for(CustomerOrder order:regularOrders){
									order.setStatus(OrderStatus.VOIDED.toString());
									order.setLastUpdated(now);
								}
								
								invoices = DAORegistry.getInvoiceDAO().getInvoiceByOrderIds(regularOrderIds);
								for(Invoice invoice:invoices){
									invoice.setStatus(InvoiceStatus.Voided);
									invoice.setLastUpdated(now);
									invoice.setLastUpdatedBy(username);
								}
							}*/
							String msg="";
							if(!finalOrders.isEmpty()){
								CustomerOrder customerOrder = null;
								 com.rtw.tracker.mail.MailAttachment[] mailAttachment = new com.rtw.tracker.mail.MailAttachment[1];
								 mailAttachment[0] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","logo.png",com.rtw.tmat.utils.Util.getFilePath(request, "logo.png", "/images/"));
								for(CrownJewelCustomerOrder order:finalOrders){
									CrownJewelCategoryTicket catTicketGroup = null;
									CrownJewelTeamZones crownJewelTeamZones = null;
									if(order.getRegularOrderId()==null){
										EventDetails event = DAORegistry.getEventDetailsDAO().get(order.getEventId());
										if(order.getIsRealTicket() != null && order.getIsRealTicket()){
											catTicketGroup = DAORegistry.getCrownJewelCategoryTicketDAO().get(order.getTicketId());
											if(catTicketGroup==null){
												msg="Ticket group is not found for selected team's order.";
												break;
											}
										}else{
											crownJewelTeamZones = DAORegistry.getCrownJewelTeamZonesDAO().get(order.getTicketId());
											if(crownJewelTeamZones==null){
												msg="Ticket Information is not found for selected team's order.";
												break;
											}
										}
										CustomerAddress blAddress = DAORegistry.getCustomerAddressDAO().getCustomerAddressInfo(order.getCustomerId());
										List<CustomerAddress> shippingAddresses = DAORegistry.getCustomerAddressDAO().getShippingAddressInfo(order.getCustomerId());
										CustomerAddress shippingAddress = shippingAddresses.get(0);
										
										if(event==null){
											msg="Event not found in system for one of the selected Team.";
											break;
										}
										if(blAddress==null){
											msg="Customer billing address is not found for selected team's order.";
											break;
										}
										if(shippingAddresses==null || shippingAddresses.isEmpty()){
											msg="Customer shipping address is not found for selected team's order.";
											break;
										}
										customerOrder = createNewCustomerOrderAndInvoice(order,event,catTicketGroup,crownJewelTeamZones,blAddress,shippingAddress,username);
										order.setRegularOrderId(customerOrder.getId());
										DAORegistry.getCrownJewelCustomerOrderDAO().saveOrUpdate(order);
										Map<String,Object> dataMap  = new HashMap<String, Object>();
										dataMap.put("teamName", order.getTeamName());
										dataMap.put("leagueName", order.getFantasyEventName());
										 CustomerOrderDetails orderDetails = DAORegistry.getCustomerOrderDetailsDAO().getCustomerOrderDetailsByOrderId(customerOrder.getId());
										 dataMap.put("firstName",orderDetails.getShFirstName());
										 dataMap.put("lastName",orderDetails.getShLastName());
										 String email = (orderDetails.getShEmail()!=null&&!orderDetails.getShEmail().isEmpty())?orderDetails.getShEmail():orderDetails.getBlEmail();
										 mailManager.sendMailNow("text/html","sales@rewardthefan.com", email, 
												null,null, "Fantasy Sports Tickets",
												"mail-fantasy-ticket-makes-final.html", dataMap, "text/html", null,mailAttachment,null);
									}
								}
							}
							
							if(finalTeams.size()==2){
								if(!orders.isEmpty()){
									isOrderUpdated = updateOrdersAndRevertRewardPoints(otherOrders,username);
								}
								if(isOrderUpdated){
									DAORegistry.getCrownJewelTeamsDAO().updateAll(otherTeams);
									DAORegistry.getCrownJewelTeamZonesDAO().updateAll(otherZones);
									crownJewelTeamsDTO.setMessage("Selected Team set as final teams, all other teams marked as deleted and associted order are marked as void.");
								}else{
									crownJewelTeamsDTO.setMessage("Something went wrong,Please try again.");
								}
								
								/*DAORegistry.getCrownJewelCustomerOrderDAO().updateAll(otherOrders);
								DAORegistry.getCustomerOrderDAO().updateAll(regularOrders);
								DAORegistry.getInvoiceDAO().updateAll(invoices);*/
								
								/*for(CrownJewelCustomerOrder order : otherOrders){
									Map<String, String> requestMap = Util.getParameterMap(null);
									requestMap.put("customerId",String.valueOf(order.getCustomerId()));
									requestMap.put("orderType",OrderType.CROWNJEWEL.toString());
									requestMap.put("orderNo",String.valueOf(order.getId()));
									String data = Util.getObject(requestMap,Constants.BASE_URL + Constants.CANCEL_ORDER);
								}*/
							}else{
								crownJewelTeamsDTO.setMessage("Something went wrong selected teams not found in system, Please try again.");
							}
						}else{
							crownJewelTeamsDTO.setMessage("two final teams are not found, Please select Two teams as final.");
						}
						
					}
				}
			}
			
			List<FantasyChildCategory> childCategories = null;
			List<FantasyGrandChildCategory> grandChildCategories = null;
			List<CrownJewelLeagues> cjLeaguesList = null;
			List<CrownJewelLeagues> finalLeagues = new ArrayList<CrownJewelLeagues>();
			if(childId != null && !childId.isEmpty()){
				grandChildCategories = DAORegistry.getFantasyGrandChildCategoryDAO().getGrandChildsByChildCategoryId(Integer.parseInt(childId));
			}if(grandChildId != null && !grandChildId.isEmpty()){
				cjLeaguesList = DAORegistry.getCrownJewelLeaguesDAO().getActiveLeaguesByGrandChildId(Integer.parseInt(grandChildId));
			}
			if(cjLeaguesList!=null){
				for(CrownJewelLeagues league : cjLeaguesList){
					if(league.getEventId()!=null){
						finalLeagues.add(league);
					}
					
				}
			}
			
			List<CrownJewelTeams> cjTeamsList = null;
			if(leagueId != null){
				//cjTeamsList = DAORegistry.getCrownJewelTeamsDAO().getAllActiveTeamsByLeagueId(leagueId);
				if(action.equalsIgnoreCase("searchDeleted")){
					cjTeamsList = DAORegistry.getCrownJewelTeamsDAO().getAllDeletedTeamsByLeagueId(leagueId);
				}else{
					cjTeamsList = DAORegistry.getCrownJewelTeamsDAO().getAllActiveTeamsByLeagueId(leagueId);
				}
				if(cjTeamsList!=null && !cjTeamsList.isEmpty()){
					for(CrownJewelTeams team : cjTeamsList){
						List<CrownJewelCustomerOrder> list = DAORegistry.getCrownJewelCustomerOrderDAO().getCrownJewelOrdersByTeamId(team.getId());
						team.setOrdersCount(list.size());
					}
				}
			}
			
			childCategories = DAORegistry.getFantasyChildCategoryDAO().getAll();
			
			crownJewelTeamsDTO.setStatus(1);
			crownJewelTeamsDTO.setStatusId(statusId);
			crownJewelTeamsDTO.setChildId(childId);
			crownJewelTeamsDTO.setGrandChildId(grandChildId);
			crownJewelTeamsDTO.setChildCategories(childCategories);
			crownJewelTeamsDTO.setGrandChildCategories(grandChildCategories);
			crownJewelTeamsDTO.setLeaguesList(finalLeagues);
			crownJewelTeamsDTO.setTeamsList(cjTeamsList);
			crownJewelTeamsDTO.setLeagueId(leagueIdStr);
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching/updating Fantasy Sports Tickets Finals");
			crownJewelTeamsDTO.setError(error);
			crownJewelTeamsDTO.setStatus(0);
		}
		return crownJewelTeamsDTO;
	}
	
	
	
	private CustomerOrder createNewCustomerOrderAndInvoice(CrownJewelCustomerOrder order,EventDetails event,
			CrownJewelCategoryTicket catTicketGroup,CrownJewelTeamZones crownJewelTeamZones,CustomerAddress blAddress,CustomerAddress shippingAddress,String username){
		try {
			Date now = new Date();
			
			CategoryTicketGroup categoryTicketGroup = new CategoryTicketGroup();
			categoryTicketGroup.setBroadcast(true);
			categoryTicketGroup.setCost(0.0);
			categoryTicketGroup.setTaxAmount(0.0);
			categoryTicketGroup.setCreatedBy(username);
			categoryTicketGroup.setCreatedDate(now);
			categoryTicketGroup.setEventId(event.getEventId());
			categoryTicketGroup.setExternalNotes(null);
			categoryTicketGroup.setFacePrice(0.0);
			categoryTicketGroup.setInternalNotes("Manual");
			categoryTicketGroup.setLastUpdatedDate(now);
			categoryTicketGroup.setMarketPlaceNotes(null);
			categoryTicketGroup.setMaxShowing(null);
			categoryTicketGroup.setNearTermDisplayOption(null);
			//categoryTicketGroup.setPrice(catTicketGroup.getActualPrice());
			categoryTicketGroup.setProducttype(ProductType.REWARDTHEFAN); //confirm
			if(catTicketGroup != null){
				categoryTicketGroup.setQuantity(catTicketGroup.getQuantity());
				//categoryTicketGroup.setRetailPrice(catTicketGroup.getActualPrice());
				categoryTicketGroup.setRow(null);
				categoryTicketGroup.setRowRange(catTicketGroup.getRowRange());
				categoryTicketGroup.setSeatHigh(null);
				categoryTicketGroup.setSeatLow(null);
				categoryTicketGroup.setSection(catTicketGroup.getSection());
				categoryTicketGroup.setSectionRange(catTicketGroup.getSectionRange());
				categoryTicketGroup.setShippingMethod(catTicketGroup.getShippingMethod());
				if(catTicketGroup.getShippingMethod()!=null){
					ShippingMethod sMethod = DAORegistry.getShippingMethodDAO().getShippingMethodByName(catTicketGroup.getShippingMethod());
					if(sMethod!=null){
						categoryTicketGroup.setShippingMethodId(sMethod.getId());
					}
				}
				
				//categoryTicketGroup.setSoldPrice(catTicketGroup.getActualPrice()); //check with order price and ticket price
				categoryTicketGroup.setSoldQuantity(catTicketGroup.getQuantity());
			}else{
				if(crownJewelTeamZones != null){
					//CrownJewelCustomerOrder - Quantity
					categoryTicketGroup.setQuantity(order.getTicketQty());//crownJewelTeamZones.getTicketsCount()
					categoryTicketGroup.setRow(null);
					categoryTicketGroup.setRowRange(null);
					categoryTicketGroup.setSeatHigh(null);
					categoryTicketGroup.setSeatLow(null);
					categoryTicketGroup.setSection(crownJewelTeamZones.getZone());
					categoryTicketGroup.setSectionRange(null);
					categoryTicketGroup.setShippingMethod(null);
					/*if(crownJewelTeamZones.getShippingMethod()!=null){
						ShippingMethod sMethod = DAORegistry.getShippingMethodDAO().getShippingMethodByName(crownJewelTeamZones.getShippingMethod());
						if(sMethod!=null){
							categoryTicketGroup.setShippingMethodId(sMethod.getId());
						}
					}*/
					categoryTicketGroup.setSoldQuantity(order.getTicketQty());
				}
			}
			categoryTicketGroup.setStatus(TicketStatus.SOLD);
			categoryTicketGroup.setTicketOnhandStatus(null);
			//categoryTicketGroup.setWholeSalePrice(catTicketGroup.getActualPrice());
			
			CustomerOrder customerOrder = new CustomerOrder();
			customerOrder.setAcceptedBy(username);
			customerOrder.setAcceptedDate(now);
			customerOrder.setCreatedDate(now);
			customerOrder.setLastUpdated(now);
			customerOrder.setStatus(OrderStatus.ACTIVE.toString());
			customerOrder.setCustomerId(order.getCustomerId());
			customerOrder.setQty(order.getTicketQty());
			
			customerOrder.setRow(categoryTicketGroup.getRow());
			customerOrder.setSeatHigh(categoryTicketGroup.getSeatHigh());
			customerOrder.setSeatLow(categoryTicketGroup.getSeatLow());
			customerOrder.setSection(categoryTicketGroup.getSection());
			
			customerOrder.setEventDate(event.getEventDate());
			customerOrder.setEventId(event.getEventId());
			customerOrder.setEventName(event.getEventName());
			customerOrder.setEventTime(event.getEventTime());
			customerOrder.setVenueCategory(event.getVenueCategoryName());
			customerOrder.setVenueCity(event.getCity());
			customerOrder.setVenueCountry(event.getCountry());
			customerOrder.setVenueId(event.getVenueId());
			customerOrder.setVenueName(event.getBuilding());
			customerOrder.setVenueState(event.getState());
			
			customerOrder.setTicketPrice(order.getTicketPrice());
			customerOrder.setOriginalOrderTotal(categoryTicketGroup.getPrice() * categoryTicketGroup.getQuantity()); //need to confirm with ulaga
			customerOrder.setTixGroupNotes(null);
			customerOrder.setShippingDate(event.getEventDate());
			customerOrder.setShippingMethodId(categoryTicketGroup.getShippingMethodId());
			customerOrder.setOrderTotal(categoryTicketGroup.getPrice() * categoryTicketGroup.getQuantity()); 
			customerOrder.setProductType(ProductType.REWARDTHEFAN.toString());
			customerOrder.setPlatform(ApplicationPlatform.TICK_TRACKER); //confirm with ulaga
			customerOrder.setPrimaryPaymentMethod(PaymentMethod.FULL_REWARDS);
			customerOrder.setSecondaryPayAmt(0.0);
			customerOrder.setDiscountAmt(0.0);
			customerOrder.setOrderType(OrderType.REGULAR);
			customerOrder.setSecondaryOrdertype(SecondaryOrderType.CROWNJEWEL);
			customerOrder.setSecondaryOrderId(order.getId().toString());
			Double orderTotal = categoryTicketGroup.getPrice() * categoryTicketGroup.getQuantity(); //Confirm With Amit
			if(order.getIsPackageSelected()){
				customerOrder.setCjePackageEnabled(true);
				customerOrder.setCjePackageNote(order.getPackageNote());
				customerOrder.setCjePackagePrice(order.getPackageCost());
				//customerOrder.setCjePackagePoints(order.getPackagePoints());
				orderTotal = (categoryTicketGroup.getPrice() * categoryTicketGroup.getQuantity()) + order.getPackageCost();
				//orderTotal = (categoryTicketGroup.getPrice()+ order.getPackageCost())* categoryTicketGroup.getQuantity();
				//Confirm With Amit
			}else{
				customerOrder.setCjePackageEnabled(false);
				customerOrder.setCjePackagePrice(0.00);
				customerOrder.setCjePackagePoints(0.00);
			}
			
			customerOrder.setTicketPrice(order.getTicketPrice());
			customerOrder.setSoldPrice(order.getTicketPrice());
			customerOrder.setTaxes(0.0);
			customerOrder.setOriginalOrderTotal(orderTotal);
			customerOrder.setOrderTotal(orderTotal); 
			customerOrder.setPrimaryPayAmt(order.getRequiredPoints().doubleValue());
			customerOrder.setCjeTicketPrice(order.getTicketPrice());
			//customerOrder.setCjeTicketPoints(order.getTicketPoints());
			customerOrder.setCjeRequirePoints(order.getRequiredPoints());
			
			Property property = DAORegistry.getPropertyDAO().get("rtf.fullrewards.auto.primarytrxid");
			Long primaryTrxId =  Long.valueOf(property.getValue());
			primaryTrxId++;
			String primaryTransactionId = "FULLREWARDS_TRX_0000"+primaryTrxId;
			property.setValue(String.valueOf(primaryTrxId));
			DAORegistry.getPropertyDAO().update(property);
			
			customerOrder.setPrimaryTransactionId(primaryTransactionId);
			
			CustomerOrderDetails orderDetails = new CustomerOrderDetails();
			orderDetails.setBlAddress1(blAddress.getAddressLine1());
			orderDetails.setBlAddress2(blAddress.getAddressLine2());
			orderDetails.setBlCity(blAddress.getCity());
			orderDetails.setBlCountry(blAddress.getCountry().getId());
			orderDetails.setBlCountryName(blAddress.getCountryName());
			orderDetails.setBlEmail(blAddress.getEmail());
			orderDetails.setBlFirstName(blAddress.getFirstName());
			orderDetails.setBlLastName(blAddress.getLastName());
			orderDetails.setBlPhone1(blAddress.getPhone1());
			orderDetails.setBlPhone2(blAddress.getPhone2());
			orderDetails.setBlState(blAddress.getState().getId());
			orderDetails.setBlStateName(blAddress.getStateName());
			orderDetails.setBlZipCode(blAddress.getZipCode());
			orderDetails.setShAddress1(shippingAddress.getAddressLine1());
			orderDetails.setShAddress2(shippingAddress.getAddressLine2());
			orderDetails.setShCity(shippingAddress.getCity());
			orderDetails.setShCountry(shippingAddress.getCountry().getId());
			orderDetails.setShCountryName(shippingAddress.getCountryName());
			orderDetails.setShEmail(shippingAddress.getEmail());
			orderDetails.setShFirstName(shippingAddress.getFirstName());
			orderDetails.setShLastName(shippingAddress.getLastName());
			orderDetails.setShPhone1(shippingAddress.getPhone1());
			orderDetails.setShPhone2(shippingAddress.getPhone2());
			orderDetails.setShState(shippingAddress.getState().getId());
			orderDetails.setShStateName(shippingAddress.getStateName());
			orderDetails.setShZipCode(shippingAddress.getZipCode());
			
			
			Invoice invoice = new Invoice();
			invoice.setCustomerId(order.getCustomerId());
			invoice.setCreatedBy(username);
			invoice.setCreatedDate(now);
			invoice.setInvoiceType("INVOICE");
			invoice.setLastUpdated(now);
			invoice.setLastUpdatedBy(username);
			invoice.setStatus(InvoiceStatus.Outstanding);
			invoice.setTicketCount(order.getTicketQty()); //check while creating invoice in tic tracker
			invoice.setInvoiceTotal(categoryTicketGroup.getPrice() * categoryTicketGroup.getQuantity());
			invoice.setShippingMethodId(categoryTicketGroup.getShippingMethodId());
			
			DAORegistry.getCategoryTicketGroupDAO().save(categoryTicketGroup);
			customerOrder.setCategoryTicketGroupId(categoryTicketGroup.getId());
			DAORegistry.getCustomerOrderDAO().save(customerOrder);
			invoice.setCustomerOrderId(customerOrder.getId());
			orderDetails.setOrderId(customerOrder.getId());
			DAORegistry.getCustomerOrderDetailsDAO().save(orderDetails);
			DAORegistry.getInvoiceDAO().save(invoice);
			categoryTicketGroup.setInvoiceId(invoice.getId());
			DAORegistry.getCategoryTicketGroupDAO().update(categoryTicketGroup);
			
			List<CategoryTicket> catTicketList = new ArrayList<CategoryTicket>();
			if(categoryTicketGroup.getQuantity()>0){
				CategoryTicket catTicket = null;
				for(int i=0;i<categoryTicketGroup.getQuantity();i++){
					catTicket = new CategoryTicket();
					catTicket.setActualPrice(categoryTicketGroup.getPrice());
					catTicket.setCategoryTicketGroupId(categoryTicketGroup.getId());
					catTicket.setExchangeRequestId(null);
					catTicket.setFillDate(now);
					catTicket.setInvoiceId(invoice.getId());
					catTicket.setIsProcessed(true);
					catTicket.setTicketId(null);
					catTicket.setUserName(username);
					catTicket.setSoldPrice(categoryTicketGroup.getSoldPrice());
					catTicketList.add(catTicket);
				}
				DAORegistry.getCategoryTicketDAO().saveAll(catTicketList);
			}
			return customerOrder;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@RequestMapping(value = "/CrownJewelChild")
	public CrownJewelTeamsDTO getChildCategoryDetails(HttpServletRequest request, HttpServletResponse response){		
		CrownJewelTeamsDTO crownJewelTeamsDTO = new CrownJewelTeamsDTO();
		Error error = new Error();
		//model.addAttribute("crownJewelTeamsDTO", crownJewelTeamsDTO);
		
		try{
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
						
			GridHeaderFilters filter = GridHeaderFiltersUtil.getFantasyChildCategorySearchHeaderFilters(headerFilter);
			List<FantasyParentCategory> parentCategoryList = DAORegistry.getFantasyParentCategoryDAO().getParentCategoriesByName("SPORTS");
			List<FantasyChildCategory> childCategoryList = DAORegistry.getFantasyChildCategoryDAO().getAll();
			List<FantasyChildCategory> childCategory = null;
			int count = 0;
			
			if(pageNo == null || pageNo.isEmpty()){
				childCategory = DAORegistry.getQueryManagerDAO().getChildCategoryDetails(filter, null);
			}else{
				childCategory = DAORegistry.getQueryManagerDAO().getChildCategoryDetails(filter, pageNo);
			}
			if(childCategory != null){
				count = childCategory.size();
			}else{
				crownJewelTeamsDTO.setMessage("No Child Category found for selected search filter");
			}
			
			crownJewelTeamsDTO.setStatus(1);
			crownJewelTeamsDTO.setParentCategoryList(parentCategoryList);
			crownJewelTeamsDTO.setChildCategories(childCategoryList);
			crownJewelTeamsDTO.setPaginationDTO(PaginationUtil.calculatePaginationParameter(count, null));
			crownJewelTeamsDTO.setChildCategory(childCategory);			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Fantasy Child Category");
			crownJewelTeamsDTO.setError(error);
			crownJewelTeamsDTO.setStatus(0);
		}
		return crownJewelTeamsDTO;
	}
	
	@RequestMapping(value = "/getChildCategory")
	public void getChildCategory(HttpServletRequest request, HttpServletResponse response){		
		JSONObject returnObject = new JSONObject();
		String pageNo = request.getParameter("pageNo");
		String headerFilter = request.getParameter("headerFilter");
		Integer count =0;
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getFantasyChildCategorySearchHeaderFilters(headerFilter);
			List<FantasyParentCategory> parentCategoryList = DAORegistry.getFantasyParentCategoryDAO().getParentCategoriesByName("SPORTS");
			List<FantasyChildCategory> childCategoryList = DAORegistry.getFantasyChildCategoryDAO().getAll();
			List<FantasyChildCategory> childCategory = DAORegistry.getQueryManagerDAO().getChildCategoryDetails(filter, pageNo); 		
			if(childCategory != null){
				count = childCategory.size();
			}
			
			returnObject.put("parentCategoryList", parentCategoryList);
			returnObject.put("childCategoryList", childCategoryList);
			returnObject.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));
			returnObject.put("childCategory", JsonWrapperUtil.getFantasyChildCategoryArray(childCategory));
			IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping(value = "/getChildCategoryValue")
	public CrownJewelTeamsDTO getChildCategoryValue(HttpServletRequest request, HttpServletResponse response){
		CrownJewelTeamsDTO crownJewelTeamsDTO = new CrownJewelTeamsDTO();
		Error error = new Error();
		
		try{
			String childId = request.getParameter("childId");
			if(StringUtils.isEmpty(childId)){
				System.err.println("Please select Child Category.");
				error.setDescription("Please select Child Category.");
				crownJewelTeamsDTO.setError(error);
				crownJewelTeamsDTO.setStatus(0);
				return crownJewelTeamsDTO;
			}
			
			FantasyChildCategory childCat = DAORegistry.getFantasyChildCategoryDAO().getChildCategoryById(Integer.parseInt(childId));
			if(childCat == null){
				crownJewelTeamsDTO.setMessage("No Child Category Found");
			}
			FantasyParentCategory parentCat = DAORegistry.getFantasyParentCategoryDAO().getParentCategoryById(childCat.getParentId());
			
			crownJewelTeamsDTO.setStatus(1);
			crownJewelTeamsDTO.setFantasyParentCategory(parentCat);
			crownJewelTeamsDTO.setFantasyChildCategory(childCat);
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Edit Fantasy Child Category");
			crownJewelTeamsDTO.setError(error);
			crownJewelTeamsDTO.setStatus(0);
		}
		return crownJewelTeamsDTO;
	}
	
	
	@RequestMapping(value = "/updateChildCategory", method=RequestMethod.POST)
	public GenericResponseDTO updateChildCategory(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		//model.addAttribute("genericResponseDTO", genericResponseDTO);
		
		String action = request.getParameter("action");		
		String returnMessage = "";
		try{			
			if(action.equalsIgnoreCase("create") || action.equalsIgnoreCase("update")){
				FantasyChildCategory childCat = null;
				String childId = request.getParameter("id");	
				String childCategoryName = request.getParameter("name");
				String parentCategoryId = request.getParameter("parentId");	
				String username = request.getParameter("userName");
				if(StringUtils.isEmpty(childCategoryName)){
					System.err.println("Please provide Child Category Name.");
					error.setDescription("Please provide Child Category Name.");
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					return genericResponseDTO;
				}
				if(StringUtils.isEmpty(parentCategoryId)){
					System.err.println("Please provide Parent Category.");
					error.setDescription("Please provide Parent Category.");
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					return genericResponseDTO;
				}
				if(StringUtils.isEmpty(username)){
					System.err.println("Please Login.");
					error.setDescription("Please Login.");
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					return genericResponseDTO;
				}
				
				Date now = new Date();
				if(childId != null && !childId.isEmpty()){
					childCat = DAORegistry.getFantasyChildCategoryDAO().get(Integer.parseInt(childId));
					childCat.setName(childCategoryName);
					childCat.setParentId(Integer.parseInt(parentCategoryId));
					childCat.setLastUpdatedBy(username);
					childCat.setLastUpdated(now);
				}else{
					childCat = new FantasyChildCategory();
					childCat.setName(childCategoryName);
					childCat.setParentId(Integer.parseInt(parentCategoryId));
					childCat.setLastUpdatedBy(username);
					childCat.setLastUpdated(now);
				}
				DAORegistry.getFantasyChildCategoryDAO().saveOrUpdate(childCat);
				returnMessage = "Child Category Created/Updated successfully";
			}
			else if(action != null && action.equalsIgnoreCase("delete")){
				String childIdStr = request.getParameter("childIds");
				if(StringUtils.isEmpty(childIdStr)){
					System.err.println("Please select Child Category.");
					error.setDescription("Please select Child Category.");
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					return genericResponseDTO;
				}
				
				String[] childIdsArr = null;
				if(childIdStr!=null) {
					childIdsArr = childIdStr.split(",");
				}
				List<FantasyChildCategory> childCatList = new ArrayList<FantasyChildCategory>();
				FantasyChildCategory childCategory = null;
				boolean isLeague =false;
				for(String childCatId : childIdsArr){
					if(childCatId != null && !childCatId.isEmpty()){
						Integer childCat = Integer.parseInt(childCatId);
						List<FantasyGrandChildCategory> grandChilds = DAORegistry.getFantasyGrandChildCategoryDAO().getGrandChildsByChildCategoryId(childCat);
						for(FantasyGrandChildCategory grand : grandChilds){
							List<CrownJewelLeagues> list = DAORegistry.getCrownJewelLeaguesDAO().getActiveLeaguesByGrandChildId(grand.getId());
							if(list!=null && !list.isEmpty()){
								isLeague = true;
								break;
							}
						}
						/*List<CrownJewelLeagues> list = DAORegistry.getCrownJewelLeaguesDAO().getActiveLeaguesByChildId(childCat);
						if(isLeague || (list!=null && !list.isEmpty())){
							isLeague = true;
							
						}*/
						if(!isLeague){
							childCategory = DAORegistry.getFantasyChildCategoryDAO().get(childCat);
							if(null != childCategory){
								childCatList.add(childCategory);
							}
						}else{
							returnMessage = "One or more Childs or its grandchild are associated with League are not deleted, others are deleted.";
						}
						
					}					
				}
				DAORegistry.getFantasyChildCategoryDAO().deleteAll(childCatList);
				if(returnMessage.isEmpty())
				returnMessage = "Child Category and all its grandchild are removed successfully";
			}
			
			genericResponseDTO.setStatus(1);
			genericResponseDTO.setMessage(returnMessage);
		} catch (Exception e) {			
			e.printStackTrace();
			error.setDescription("Something went wrong while updating Fantasy Child Category");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
		

	@RequestMapping({"/ChildCategoryExportToExcel"})
	public void childCategoryToExport(HttpServletRequest request, HttpServletResponse response){
		String headerFilter = request.getParameter("headerFilter");
		
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getFantasyChildCategorySearchHeaderFilters(headerFilter);
			List<FantasyChildCategory> childCategoryList = DAORegistry.getQueryManagerDAO().getChildCategoryDetailsToExport(filter);
			
			if(childCategoryList!=null && !childCategoryList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Fantasy_Child");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Child Category Id");
				header.createCell(1).setCellValue("Child Category Name");
				header.createCell(2).setCellValue("Parent Category Name");
				header.createCell(3).setCellValue("Last Updated By");
				header.createCell(4).setCellValue("Last Updated Date");
				Integer i=1;
				for(FantasyChildCategory fantasyChild : childCategoryList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(fantasyChild.getId());
					row.createCell(1).setCellValue(fantasyChild.getName());
					row.createCell(2).setCellValue(fantasyChild.getParentCategoryName()!=null?fantasyChild.getParentCategoryName():"");
					row.createCell(3).setCellValue(fantasyChild.getLastUpdatedBy()!=null?fantasyChild.getLastUpdatedBy():"");
					row.createCell(4).setCellValue(fantasyChild.getLastUpdatedStr()!=null?fantasyChild.getLastUpdatedStr():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Fantasy_Child.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping(value = "/CrownJewelGrandChild")
	public CrownJewelTeamsDTO getGrandChildCategoryDetails(HttpServletRequest request, HttpServletResponse response){		
		CrownJewelTeamsDTO crownJewelTeamsDTO = new CrownJewelTeamsDTO();
		Error error = new Error();
		//model.addAttribute("crownJewelTeamsDTO", crownJewelTeamsDTO);
				
		try{
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			GridHeaderFilters filter = GridHeaderFiltersUtil.getFantasyGrandChildCategorySearchHeaderFilters(headerFilter);
			List<FantasyParentCategory> parentCategoryList = DAORegistry.getFantasyParentCategoryDAO().getParentCategoriesByName("SPORTS");
			List<FantasyChildCategory> childCategoryList = DAORegistry.getFantasyChildCategoryDAO().getAll();
			List<FantasyGrandChildCategory> grandChildCategoryList = DAORegistry.getFantasyGrandChildCategoryDAO().getAll();
			List<FantasyGrandChildCategory> grandChildCategory = null;
			int count = 0;
			if(pageNo == null || pageNo.isEmpty()){
				grandChildCategory = DAORegistry.getQueryManagerDAO().getGrandChildCategoryDetails(filter, null);
			}else{
				grandChildCategory = DAORegistry.getQueryManagerDAO().getGrandChildCategoryDetails(filter, pageNo);
			}
			if(grandChildCategory != null){
				count = grandChildCategory.size();
			}else{
				crownJewelTeamsDTO.setMessage("No Grand Child Category found for selected search filter");
			}
			
			crownJewelTeamsDTO.setStatus(1);
			crownJewelTeamsDTO.setParentCategoryList(parentCategoryList);
			crownJewelTeamsDTO.setChildCategories(childCategoryList);
			crownJewelTeamsDTO.setGrandChildCategories(grandChildCategoryList);
			crownJewelTeamsDTO.setPaginationDTO(PaginationUtil.calculatePaginationParameter(count, null));
			crownJewelTeamsDTO.setGrandChildCategory(grandChildCategory);			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Somethingwent wrong while fetching Fantasy Grand Child Category");
			crownJewelTeamsDTO.setError(error);
			crownJewelTeamsDTO.setStatus(0);
		}
		return crownJewelTeamsDTO;
	}
	
	@RequestMapping(value = "/getGrandChildCategory")
	public void getGrandChildCategory(HttpServletRequest request, HttpServletResponse response){		
		JSONObject returnObject = new JSONObject();
		String pageNo = request.getParameter("pageNo");
		String headerFilter = request.getParameter("headerFilter");
		Integer count =0;
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getFantasyGrandChildCategorySearchHeaderFilters(headerFilter);
			List<FantasyParentCategory> parentCategoryList = DAORegistry.getFantasyParentCategoryDAO().getParentCategoriesByName("SPORTS");
			List<FantasyChildCategory> childCategoryList = DAORegistry.getFantasyChildCategoryDAO().getAll();
			List<FantasyGrandChildCategory> grandChildCategoryList = DAORegistry.getFantasyGrandChildCategoryDAO().getAll();
			List<FantasyGrandChildCategory> grandChildCategory = DAORegistry.getQueryManagerDAO().getGrandChildCategoryDetails(filter, pageNo); 		
			if(grandChildCategory != null){
				count = grandChildCategory.size();
			}
			
			returnObject.put("parentCategoryList", parentCategoryList);
			returnObject.put("childCategoryList", childCategoryList);
			returnObject.put("grandChildCategoryList", grandChildCategoryList);
			returnObject.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));
			returnObject.put("grandChildCategory", JsonWrapperUtil.getFantasyGrandChildCategoryArray(grandChildCategory));
			IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@RequestMapping(value = "/getGrandChildCategoryValue")
	public CrownJewelTeamsDTO getGrandChildCategoryValue(HttpServletRequest request, HttpServletResponse response){		
		CrownJewelTeamsDTO crownJewelTeamsDTO = new CrownJewelTeamsDTO();
		Error error = new Error();
			
		try{
			String grandChildId = request.getParameter("grandChildId");
			if(StringUtils.isEmpty(grandChildId)){
				System.err.println("Please select Grand Child Category.");
				error.setDescription("Please select Grand Child Category.");
				crownJewelTeamsDTO.setError(error);
				crownJewelTeamsDTO.setStatus(0);
				return crownJewelTeamsDTO;
			}
			
			FantasyGrandChildCategory grandChildCat = DAORegistry.getFantasyGrandChildCategoryDAO().getGrandChildCategoryById(Integer.parseInt(grandChildId));
			if(grandChildCat == null){
				crownJewelTeamsDTO.setMessage("No Grand Child Category Found");
			}
			FantasyChildCategory childCat = DAORegistry.getFantasyChildCategoryDAO().getChildCategoryById(grandChildCat.getChildId());
			FantasyParentCategory parentCat = DAORegistry.getFantasyParentCategoryDAO().getParentCategoryById(childCat.getParentId());
			
			crownJewelTeamsDTO.setStatus(1);
			crownJewelTeamsDTO.setFantasyParentCategory(parentCat);
			crownJewelTeamsDTO.setFantasyChildCategory(childCat);
			crownJewelTeamsDTO.setFantasyGrandChildCategory(grandChildCat);
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Edit Fantasy Grand Child Category");
			crownJewelTeamsDTO.setError(error);
			crownJewelTeamsDTO.setStatus(0);
		}
		return crownJewelTeamsDTO;
	}
	
	
	@RequestMapping(value = "/updateGrandChildCategory",method=RequestMethod.POST)	
	public GenericResponseDTO updateGrandChildCategory(HttpServletRequest request, HttpServletResponse response){		
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		//model.addAttribute("genericResponseDTO", genericResponseDTO);
		
		String action = request.getParameter("action");		
		String returnMessage = "";
		try{			
			if(action.equalsIgnoreCase("create") || action.equalsIgnoreCase("update")){
				FantasyGrandChildCategory grandChildCat = null;
				String grandChildId = request.getParameter("id");	
				String grandchildCategoryName = request.getParameter("name");
				String childCategoryId = request.getParameter("childId");
				String costStr = request.getParameter("cost");
				String description1 = request.getParameter("description1");
				String description2 = request.getParameter("description2");
				String description3 = request.getParameter("description3");
				String description4 = request.getParameter("description4");
				String description5 = request.getParameter("description5");
				String description6 = request.getParameter("description6");
				String username = request.getParameter("userName");
				if(StringUtils.isEmpty(grandchildCategoryName)){
					System.err.println("Please provide Grand Child Category Name.");
					error.setDescription("Please provide Grand Child Category Name.");
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					return genericResponseDTO;
				}
				if(StringUtils.isEmpty(childCategoryId)){
					System.err.println("Please select Child Category.");
					error.setDescription("Please select Child Category.");
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					return genericResponseDTO;
				}
				if(StringUtils.isEmpty(username)){
					System.err.println("Please Login.");
					error.setDescription("Please Login.");
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					return genericResponseDTO;
				}
				
				Date now = new Date();
				Double cost = null;
				if(costStr != null && !costStr.isEmpty()){
					try {
						cost = Double.parseDouble(costStr);
					} catch (Exception e) {
						e.printStackTrace();
						error.setDescription("Invalid value for package cost, please enter valid double value or keep blank.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
				}
				
				if(grandChildId != null && !grandChildId.isEmpty()){
					grandChildCat = DAORegistry.getFantasyGrandChildCategoryDAO().get(Integer.parseInt(grandChildId));
					grandChildCat.setName(grandchildCategoryName);
					grandChildCat.setChildId(Integer.parseInt(childCategoryId));
					grandChildCat.setLastUpdatedBy(username);
					grandChildCat.setLastUpdated(now);
					//grandChildCat.setPackageDescription(description);
					grandChildCat.setPackageLine1(description1);
					grandChildCat.setPackageLine2(description2);
					grandChildCat.setPackageLine3(description3);
					grandChildCat.setPackageLine4(description4);
					grandChildCat.setPackageLine5(description5);
					grandChildCat.setPackageLine6(description6);
					grandChildCat.setPackageCost(cost);
				}else{
					grandChildCat = new FantasyGrandChildCategory();
					grandChildCat.setName(grandchildCategoryName);
					grandChildCat.setChildId(Integer.parseInt(childCategoryId));
					grandChildCat.setLastUpdatedBy(username);
					grandChildCat.setLastUpdated(now);
					//grandChildCat.setPackageDescription(description);
					grandChildCat.setPackageLine1(description1);
					grandChildCat.setPackageLine2(description2);
					grandChildCat.setPackageLine3(description3);
					grandChildCat.setPackageLine4(description4);
					grandChildCat.setPackageLine5(description5);
					grandChildCat.setPackageLine6(description6);
					grandChildCat.setPackageCost(cost);
				}
				DAORegistry.getFantasyGrandChildCategoryDAO().saveOrUpdate(grandChildCat);
				returnMessage = "Grand Child Category Created/Updated successfully";
			}
			else if(action != null && action.equalsIgnoreCase("delete")){
				String grandChildIdStr = request.getParameter("grandChildIds");
				if(StringUtils.isEmpty(grandChildIdStr)){
					System.err.println("Please select Grand Child Category.");
					error.setDescription("Please select Grand Child Category.");
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					return genericResponseDTO;
				}
				
				String[] grandChildIdsArr = null;
				if(grandChildIdStr!=null) {
					grandChildIdsArr = grandChildIdStr.split(",");
				}
				List<FantasyGrandChildCategory> grandChildCatList = new ArrayList<FantasyGrandChildCategory>();
				FantasyGrandChildCategory grandChildCategory = null;
				for(String grandChildCatId : grandChildIdsArr){
					if(grandChildCatId != null && !grandChildCatId.isEmpty()){
						Integer grandChildCat = Integer.parseInt(grandChildCatId);
						List<CrownJewelLeagues> list = DAORegistry.getCrownJewelLeaguesDAO().getActiveLeaguesByGrandChildId(grandChildCat);
						if(list==null || list.isEmpty()){
							grandChildCategory = DAORegistry.getFantasyGrandChildCategoryDAO().get(grandChildCat);
							if(null != grandChildCategory){
								grandChildCatList.add(grandChildCategory);
							}
						}else{
							returnMessage = "One or more grandchild category from selected are associated with leagues, other are deleted.";
						}
					}					
				}
				DAORegistry.getFantasyGrandChildCategoryDAO().deleteAll(grandChildCatList);
				if(returnMessage.isEmpty())
					returnMessage = "Grand Child Category Removed successfully";
			}
			
			genericResponseDTO.setStatus(1);
			genericResponseDTO.setMessage(returnMessage);
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while updating Fantasy Grand Child Category");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
			return genericResponseDTO;
		}
		return genericResponseDTO;
	}
	

	@RequestMapping({"/GrandChildCategoryExportToExcel"})
	public void grandChildCategoryToExport(HttpServletRequest request, HttpServletResponse response){
		String headerFilter = request.getParameter("headerFilter");
		
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getFantasyGrandChildCategorySearchHeaderFilters(headerFilter);
			List<FantasyGrandChildCategory> grandChildCategoryList = DAORegistry.getQueryManagerDAO().getGrandChildCategoryDetailsToExport(filter);
			
			if(grandChildCategoryList!=null && !grandChildCategoryList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Fantasy_GrandChild");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Grand Child Category Id");
				header.createCell(1).setCellValue("Grand Child Category Name");
				header.createCell(2).setCellValue("Parent Category Name");
				header.createCell(3).setCellValue("Child Category Name");
				header.createCell(4).setCellValue("Package Cost");
				header.createCell(5).setCellValue("Last Updated By");
				header.createCell(6).setCellValue("Last Updated Date");
				Integer i=1;
				for(FantasyGrandChildCategory fantasyGrandChild : grandChildCategoryList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(fantasyGrandChild.getId());
					row.createCell(1).setCellValue(fantasyGrandChild.getName());
					row.createCell(2).setCellValue(fantasyGrandChild.getParentCategoryName()!=null?fantasyGrandChild.getParentCategoryName():"");
					row.createCell(3).setCellValue(fantasyGrandChild.getChildCategoryName()!=null?fantasyGrandChild.getChildCategoryName():"");
					row.createCell(4).setCellValue(fantasyGrandChild.getPackageCost()!=null?fantasyGrandChild.getPackageCost():0.00);
					row.createCell(5).setCellValue(fantasyGrandChild.getLastUpdatedBy()!=null?fantasyGrandChild.getLastUpdatedBy():"");
					row.createCell(6).setCellValue(fantasyGrandChild.getLastUpdatedStr()!=null?fantasyGrandChild.getLastUpdatedStr():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Fantasy_GrandChild.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	private boolean updateOrdersAndRevertRewardPoints(List<CrownJewelCustomerOrder> orders,String username){
		try {
			Date now = new Date(); 
			List<CrownJewelCustomerOrder> orderList = new ArrayList<CrownJewelCustomerOrder>();
			List<CustomerOrder> regularOrderList = new ArrayList<CustomerOrder>();
			List<Invoice> invoiceList = new ArrayList<Invoice>();
			List<Integer> regularOrderIds = new ArrayList<Integer>();
			List<CustomerLoyalty> loyaltyList = new ArrayList<CustomerLoyalty>();
			List<CustomerLoyaltyHistory> historyUpdateList = new ArrayList<CustomerLoyaltyHistory>();
			List<CustomerLoyaltyHistory> newHistoryList = new ArrayList<CustomerLoyaltyHistory>();
			CustomerLoyalty loyalty = null;
			for (CrownJewelCustomerOrder order : orders) {
				order.setStatus(CrownJewelOrderStatus.REJECTED.toString());
				order.setLastUpdatedDate(now);
				order.setAcceptRejectReson("Team was Excluded.");
				
				loyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(order.getCustomerId());
				CustomerLoyaltyHistory loyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getActiveCustomerLoyaltyHistoryOfCrownJewelOrder(order.getId(),order.getCustomerId());
				if(loyaltyHistory!=null){
					loyaltyHistory.setRewardStatus(RewardStatus.VOIDED);
					loyaltyHistory.setUpdatedDate(now);
					historyUpdateList.add(loyaltyHistory);
				}
				
				CustomerLoyaltyHistory newHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getVoidRefundCustomerLoyaltyHistoryOfCrownJewelOrder(order.getId(),order.getCustomerId());
				if(newHistory==null){
					newHistory = new CustomerLoyaltyHistory();
					newHistory.setBalanceRewardAmount(loyalty.getActivePoints() + order.getRequiredPoints());
					newHistory.setBalanceRewardPoints(loyalty.getActivePoints() + order.getRequiredPoints());
					newHistory.setCreateDate(now);
					newHistory.setCustomerId(order.getCustomerId());
					newHistory.setDollarConversionRate(1.00);
					newHistory.setInitialRewardAmount(loyalty.getActivePoints());
					newHistory.setInitialRewardPoints(loyalty.getActivePoints());
					newHistory.setOrderId(order.getId());
					newHistory.setOrderTotal(order.getTicketPrice() * order.getTicketQty());
					newHistory.setOrderType(OrderType.CROWNJEWEL_REFUND);
					newHistory.setPointsEarned(order.getRequiredPoints());
					newHistory.setPointsSpent(0.00);
					newHistory.setRewardConversionRate(0.100);
					newHistory.setRewardEarnAmount(order.getRequiredPoints());
					newHistory.setRewardSpentAmount(0.00);
					newHistory.setRewardStatus(RewardStatus.ACTIVE);
					newHistory.setUpdatedDate(now);
					newHistoryList.add(newHistory);
				}else{
					newHistory.setRewardStatus(RewardStatus.ACTIVE);
					newHistory.setUpdatedDate(now);
					historyUpdateList.add(newHistory);
				}
				
				
				loyalty.setActivePoints(loyalty.getActivePoints() + order.getRequiredPoints());
				loyalty.setRevertedSpentPoints(loyalty.getRevertedSpentPoints() + order.getRequiredPoints());
				loyaltyList.add(loyalty);
				DAORegistry.getCustomerLoyaltyDAO().update(loyalty);
				
				if(order.getRegularOrderId()!=null){
					regularOrderIds.add(order.getRegularOrderId());
				}
				orderList.add(order);
			}
			if(!regularOrderIds.isEmpty()){
				List<CustomerOrder> regularOrders = DAORegistry.getCustomerOrderDAO().getCustomerOrderByIds(regularOrderIds);
				for(CustomerOrder order:regularOrders){
					order.setStatus(OrderStatus.VOIDED.toString());
					order.setLastUpdated(now);
					regularOrderList.add(order);
				}
				
				List<Invoice> invoices = DAORegistry.getInvoiceDAO().getInvoiceByOrderIds(regularOrderIds);
				for(Invoice invoice:invoices){
					invoice.setStatus(InvoiceStatus.Voided);
					invoice.setLastUpdated(now);
					invoice.setLastUpdatedBy(username);
					invoiceList.add(invoice);
				}
			}
			DAORegistry.getCrownJewelCustomerOrderDAO().updateAll(orderList);
			DAORegistry.getCustomerOrderDAO().updateAll(regularOrderList);
			DAORegistry.getInvoiceDAO().updateAll(invoiceList);
			//DAORegistry.getCustomerLoyaltyDAO().updateAll(loyaltyList);
			DAORegistry.getCustomerLoyaltyHistoryDAO().updateAll(historyUpdateList);
			DAORegistry.getCustomerLoyaltyHistoryDAO().saveAll(newHistoryList);
			for(CrownJewelCustomerOrder order : orderList){
				Map<String, String> requestMap = Util.getParameterMap(null);
				requestMap.put("customerId",String.valueOf(order.getCustomerId()));
				requestMap.put("orderType",OrderType.FANTASYTICKETS.toString());
				requestMap.put("orderNo",String.valueOf(order.getId()));
				String data = Util.getObject(requestMap,Constants.BASE_URL + Constants.CANCEL_ORDER);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	

	@RequestMapping(value="/CrownJewelTicketsExportToExcel")
	public void getFantasySportsTicketsToExport(HttpServletRequest request, HttpServletResponse response){
		try{
			String grandChildId = request.getParameter("grandChildType");
			FantasyGrandChildCategory grandChildCat = null;
				
			List<CrownJewelLeagues> cjLeaguesList = null;
			if(grandChildId != null && !grandChildId.isEmpty()){
				cjLeaguesList = DAORegistry.getCrownJewelLeaguesDAO().getActiveLeaguesByGrandChildId(Integer.parseInt(grandChildId));
			}
			if(cjLeaguesList!=null && !cjLeaguesList.isEmpty()){
				for(CrownJewelLeagues league : cjLeaguesList){
					if(league.getEventId()!=null){
						Event event = DAORegistry.getEventDAO().get(league.getEventId());
						Venue venue = DAORegistry.getVenueDAO().get(event.getVenueId());
						league.setVenueString(event.getEventName()+" "+event.getEventDateStr()+" "+event.getEventTimeStr()+" "+venue.getLocation());
					}
					
				}
			}
			
			if(cjLeaguesList!=null && !cjLeaguesList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Fantasy_Sports_Tickets");
				Row header = sheet.createRow((int)0);				
				header.createCell(0).setCellValue("Fantasy Ticket Id");
				header.createCell(1).setCellValue("Event Name");
				header.createCell(2).setCellValue("Final TMAT Event");
				//header.createCell(3).setCellValue("Child Category");
				header.createCell(3).setCellValue("GrandChild Category");
				header.createCell(4).setCellValue("Last Updated By");
				header.createCell(5).setCellValue("Last Updated Date");
				int i=1;
				for(CrownJewelLeagues league : cjLeaguesList){
					Row row = sheet.createRow(i);					
					row.createCell(0).setCellValue(league.getId()!=null?league.getId():0);
					row.createCell(1).setCellValue(league.getName()!=null?league.getName():"");
					row.createCell(2).setCellValue(league.getVenueString()!=null?league.getVenueString():"");
					
					//childCat = DAORegistry.getFantasyChildCategoryDAO().getChildCategoryById(league.getChildId());
					//row.createCell(3).setCellValue(childCat!=null?childCat.getName():"");
					grandChildCat = DAORegistry.getFantasyGrandChildCategoryDAO().getGrandChildCategoryById(league.getGrandChildId());					
					row.createCell(3).setCellValue(grandChildCat!=null?grandChildCat.getName():"");
										
					row.createCell(4).setCellValue(league.getLastUpdateddBy()!=null?league.getLastUpdateddBy():"");
					row.createCell(5).setCellValue(league.getLastUpdatedStr()!=null?league.getLastUpdatedStr():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Fantasy_Sports_Tickets.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	@RequestMapping(value="/CategoryTeamsExportToExcel")
	public void getFantasyTeamsToExport(HttpServletRequest request, HttpServletResponse response){
		
		try{
			String parentType = request.getParameter("parentType");
			String childId = request.getParameter("childType");
			String grandChildId = request.getParameter("grandChildType");
				
			List<CrownJewelCategoryTeams> cjCategoryTeams = null;
			if(grandChildId != null && !grandChildId.isEmpty()){
				cjCategoryTeams = DAORegistry.getCrownJewelCategoryTeamsDAO().getCategoryTeamsByGrandChild(Integer.parseInt(grandChildId));
			}
			
			if(cjCategoryTeams!=null && !cjCategoryTeams.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Fantasy_Teams");
				Row header = sheet.createRow((int)0);				
				header.createCell(0).setCellValue("Fantasy Team Id");
				header.createCell(1).setCellValue("Team Name");
				header.createCell(2).setCellValue("Markup");
				header.createCell(3).setCellValue("Fractional Odds");
				header.createCell(4).setCellValue("Odds");
				header.createCell(5).setCellValue("Cutoff Date");
				header.createCell(6).setCellValue("Last Updated By");
				header.createCell(7).setCellValue("Last Updated Date");
				int i=1;
				for(CrownJewelCategoryTeams team : cjCategoryTeams){
					Row row = sheet.createRow(i);					
					row.createCell(0).setCellValue(team.getId()!=null?team.getId():0);
					row.createCell(1).setCellValue(team.getName()!=null?team.getName():"");
					row.createCell(2).setCellValue(team.getMarkup()!=null?team.getMarkup():0);
					row.createCell(3).setCellValue(team.getFractionalOdd()!=null?team.getFractionalOdd():"");
					row.createCell(4).setCellValue(team.getOdds()!=null?team.getOdds():0);
					row.createCell(5).setCellValue(team.getCutOffDateStr()!=null?team.getCutOffDateStr():"");
					row.createCell(6).setCellValue(team.getLastUpdatedBy()!=null?team.getLastUpdatedBy():"");
					row.createCell(7).setCellValue(team.getLastUpdatedStr()!=null?team.getLastUpdatedStr():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Fantasy_Teams.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}