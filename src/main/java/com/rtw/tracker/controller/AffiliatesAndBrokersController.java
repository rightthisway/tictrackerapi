package com.rtw.tracker.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.AffiliateCashReward;
import com.rtw.tracker.datas.AffiliateCashRewardHistory;
import com.rtw.tracker.datas.AffiliatePromoCodeHistory;
import com.rtw.tracker.datas.ContestAffiliates;
import com.rtw.tracker.datas.Customer;
import com.rtw.tracker.datas.Role;
import com.rtw.tracker.datas.TrackerUser;
import com.rtw.tracker.enums.RewardStatus;
import com.rtw.tracker.pojos.AffiliateCashRewardHistoryDTO;
import com.rtw.tracker.pojos.AffiliateOrderNoteDTO;
import com.rtw.tracker.pojos.AffiliateOrderSummaryDTO;
import com.rtw.tracker.pojos.AffiliatePromoCodeDTO;
import com.rtw.tracker.pojos.AffiliatePromoCodeHistoryDTO;
import com.rtw.tracker.pojos.AffiliateReportDTO;
import com.rtw.tracker.pojos.AffiliatesDTO;
import com.rtw.tracker.pojos.ContestAffiliateEarning;
import com.rtw.tracker.pojos.ContestAffiliateEarningDTO;
import com.rtw.tracker.pojos.ContestAffiliatesDTO;
import com.rtw.tracker.pojos.GenericResponseDTO;
import com.rtw.tracker.pojos.ManageUsersDTO;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.GridHeaderFiltersUtil;
import com.rtw.tracker.utils.PaginationUtil;
import com.rtw.tracker.utils.Util;

@Controller
public class AffiliatesAndBrokersController {

	/*
	 * Getting Affiliate Details
	 */
	@RequestMapping(value = "/Affiliates")
	public String getAffiliatesPage(HttpServletRequest request, HttpServletResponse response,ModelMap map){
		AffiliatesDTO affiliatesDTO = new AffiliatesDTO();
		Error error = new Error();
		map.put("affiliatesDTO", affiliatesDTO);
		
		try{
			String status = request.getParameter("status");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
						
			Integer count = 0;
			Collection<TrackerUser> trackerUsers = null;
			Map<Integer, AffiliateCashReward> cashRewardMapByUserId = new HashMap<Integer, AffiliateCashReward>();
						
			GridHeaderFilters filter = GridHeaderFiltersUtil.getAffiliatesSearchHeaderFilters(headerFilter);
			
			if(pageNo == null || pageNo.isEmpty()){
				trackerUsers = DAORegistry.getTrackerUserDAO().getAllTrackerUsersByRole("ROLE_AFFILIATES", status, filter, null);
			}else{
				trackerUsers = DAORegistry.getTrackerUserDAO().getAllTrackerUsersByRole("ROLE_AFFILIATES", status, filter, pageNo);
			}
			
			Collection<AffiliateCashReward> rewardList = DAORegistry.getAffiliateCashRewardDAO().getAll();
			
			for (AffiliateCashReward affiliateCashReward : rewardList) {
				cashRewardMapByUserId.put(affiliateCashReward.getUser().getId(), affiliateCashReward);
			}
			if(trackerUsers != null && trackerUsers.size() > 0){
				count = trackerUsers.size();
			}
			if(trackerUsers == null || trackerUsers.size() <= 0){
				affiliatesDTO.setMessage("No "+status+" Affiliates found.");
			}
						
			affiliatesDTO.setStatus(1);			
			affiliatesDTO.setPaginationDTO(PaginationUtil.calculatePaginationParameter(count, null));
			affiliatesDTO.setAffiliatesPaginationDTO(PaginationUtil.getDummyPaginationParameters(0));
			affiliatesDTO.setManageAffiliatesDTOs(Util.getManageAffiliatesArray(trackerUsers,cashRewardMapByUserId));
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Affiliates.");
			affiliatesDTO.setError(error);
			affiliatesDTO.setStatus(0);
		}		
		return "page-affiliate-details";		
	}
	
	@RequestMapping({"/ManageBrokers"})
	public String loadManageBrokersPage(HttpServletRequest request, HttpServletResponse response, ModelMap map, Model model){
		ManageUsersDTO manageUsersDTO = new ManageUsersDTO();
		Error error = new Error();
		model.addAttribute("manageUsersDTO", manageUsersDTO);
		
		try{
			String status = request.getParameter("status");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			Integer count = 0;
			Collection<TrackerUser> trackerBrokerList = null;
			GridHeaderFilters filter = GridHeaderFiltersUtil.getUserSearchHeaderFilters(headerFilter);
			
			if(pageNo == null || pageNo.isEmpty()){
				trackerBrokerList = DAORegistry.getTrackerUserDAO().getAllTrackerUsersByRole("ROLE_BROKER",status,filter,null);
			}else{
				trackerBrokerList = DAORegistry.getTrackerUserDAO().getAllTrackerUsersByRole("ROLE_BROKER",status,filter,pageNo);
			}
			
			if(trackerBrokerList != null && trackerBrokerList.size() > 0){
				count = trackerBrokerList.size();
			}
			if(trackerBrokerList == null || trackerBrokerList.size() <= 0){
				manageUsersDTO.setMessage("No "+status+" Brokers found.");
			}
			
			manageUsersDTO.setStatus(1);
			manageUsersDTO.setTrackerUserCustomDTO(com.rtw.tracker.utils.Util.getMangeUsersObject(trackerBrokerList));
			manageUsersDTO.setPaginationDTO(PaginationUtil.calculatePaginationParameter(count, null));
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Brokers.");
			manageUsersDTO.setError(error);
			manageUsersDTO.setStatus(0);
		}
		return "page-admin-manage-brokers";
	}
	
	
	@RequestMapping({"/UsersExportToExcel"})
	public void usersToExport(HttpServletRequest request, HttpServletResponse response){
		String user = request.getParameter("user");
		String status = request.getParameter("status");
		String headerFilter = request.getParameter("headerFilter");
		String roles = "";
		Map<Integer, AffiliateCashReward> cashRewardMapByUserId = new HashMap<Integer, AffiliateCashReward>();
		Collection<TrackerUser> trackerUserList = null;
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getUserSearchHeaderFilters(headerFilter); 
			if(user != null && user.equalsIgnoreCase("USER")){
				trackerUserList = DAORegistry.getTrackerUserDAO().getAllTrackerUsersByRoles("ROLE_SUPER_ADMIN", "ROLE_USER", status, filter, null);
			}else if(user != null && user.equalsIgnoreCase("BROKER")){
				trackerUserList = DAORegistry.getTrackerUserDAO().getAllTrackerUsersByRole("ROLE_BROKER",status,filter,null);
			}else if(user != null && user.equalsIgnoreCase("AFFILIATE")){
				trackerUserList = DAORegistry.getTrackerUserDAO().getAllTrackerUsersByRole("ROLE_AFFILIATES", status, filter, null);
				Collection<AffiliateCashReward> rewardList = DAORegistry.getAffiliateCashRewardDAO().getAll();
				
				for (AffiliateCashReward affiliateCashReward : rewardList) {
					cashRewardMapByUserId.put(affiliateCashReward.getUser().getId(), affiliateCashReward);
				}
			}
			
			if(trackerUserList!=null && !trackerUserList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(user);
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("User Id");
				header.createCell(1).setCellValue("User Name");
				header.createCell(2).setCellValue("First Name");
				header.createCell(3).setCellValue("Last Name");
				header.createCell(4).setCellValue("Email");
				header.createCell(5).setCellValue("Phone");
				header.createCell(6).setCellValue("Status");
				if(user != null && user.equalsIgnoreCase("USER")){
					header.createCell(7).setCellValue("Role");
				}else if(user != null && user.equalsIgnoreCase("BROKER")){
					header.createCell(7).setCellValue("Broker Id");
					header.createCell(8).setCellValue("Company Name");
				}else if(user != null && user.equalsIgnoreCase("AFFILIATE")){
					header.createCell(7).setCellValue("Promotional Code");
					header.createCell(8).setCellValue("Active Cash");
					header.createCell(9).setCellValue("Pending Cash");
				}
				Integer i=1;
				for(TrackerUser trackerUser : trackerUserList){
					roles = "";
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(trackerUser.getId());
					row.createCell(1).setCellValue(trackerUser.getUserName()!=null?trackerUser.getUserName():"");
					row.createCell(2).setCellValue(trackerUser.getFirstName()!=null?trackerUser.getFirstName():"");
					row.createCell(3).setCellValue(trackerUser.getLastName()!=null?trackerUser.getLastName():"");
					row.createCell(4).setCellValue(trackerUser.getEmail()!=null?trackerUser.getEmail():"");
					row.createCell(5).setCellValue(trackerUser.getPhone()!=null?trackerUser.getPhone():"");
					if(trackerUser.getStatus() != null && trackerUser.getStatus() == true){
						row.createCell(6).setCellValue("ACTIVE");
					}else{
						row.createCell(6).setCellValue("DEACTIVE");
					}
					
					for(Role role: trackerUser.getRoles()){
						roles += role.getName();
					}
					
					if(user != null && user.equalsIgnoreCase("USER")){
						row.createCell(7).setCellValue(roles);
					}else if(user != null && user.equalsIgnoreCase("BROKER")){
						row.createCell(7).setCellValue(trackerUser.getBroker()!= null?trackerUser.getBroker().getId().toString():"");
						row.createCell(8).setCellValue(trackerUser.getBroker()!= null?trackerUser.getBroker().getCompanyName():"");
					}else if(user != null && user.equalsIgnoreCase("AFFILIATE")){
						row.createCell(7).setCellValue(trackerUser.getPromotionalCode()!=null?trackerUser.getPromotionalCode():"");
						row.createCell(8).setCellValue(cashRewardMapByUserId.get(trackerUser.getId())!=null? cashRewardMapByUserId.get(trackerUser.getId()).getActiveCash(): 0.00);
						row.createCell(9).setCellValue(cashRewardMapByUserId.get(trackerUser.getId())!=null? cashRewardMapByUserId.get(trackerUser.getId()).getPendingCash(): 0.00);
					}
					
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename="+user+".xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/ExportAffiliateReport")
	public void exportAffiliateReportToExcel(HttpServletRequest request, HttpServletResponse response){
		String userId = request.getParameter("userId");
		String headerFilter = request.getParameter("headerFilter");
		
		Collection<AffiliateCashRewardHistory> historyList = null;
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getAffiliateReportSearchHeaderFilters(headerFilter);
			//TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
			
			if(userId != null && !userId.isEmpty()){
				historyList = DAORegistry.getAffiliateCashRewardHistoryDAO().getHistoryByUserId(Integer.parseInt(userId), filter);
			}/*else{
				historyList = DAORegistry.getAffiliateCashRewardHistoryDAO().getHistoryByUserId(trackerUserSession.getId(), filter);
			}*/
			
			if(historyList!=null && !historyList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Affiliate_Customer_Orders");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Order ID");
				header.createCell(1).setCellValue("First Name");
				header.createCell(2).setCellValue("Last Name");
				header.createCell(3).setCellValue("Event Name");
				header.createCell(4).setCellValue("Event Date");
				header.createCell(5).setCellValue("Event Time");
				header.createCell(6).setCellValue("Quantity");
				header.createCell(7).setCellValue("Order Total");
				header.createCell(8).setCellValue("Cash Credited");
				header.createCell(9).setCellValue("Status");
				header.createCell(10).setCellValue("Promotional Code");
				int i=1;
				for(AffiliateCashRewardHistory history : historyList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(history.getOrder().getId());
					row.createCell(1).setCellValue(history.getCustomer()!=null?history.getCustomer().getCustomerName():"");
					row.createCell(2).setCellValue((history.getCustomer()!=null&&history.getCustomer().getLastName()!=null)?history.getCustomer().getLastName():"");
					row.createCell(3).setCellValue(history.getOrder()!=null?history.getOrder().getEventName():"");
					row.createCell(4).setCellValue(history.getOrder()!=null?history.getOrder().getEventDateStr():"");
					row.createCell(5).setCellValue(history.getOrder()!=null?history.getOrder().getEventTimeStr():"");
					row.createCell(6).setCellValue(history.getOrder()!=null?history.getOrder().getQty():0);
					row.createCell(7).setCellValue(history.getOrderTotal()!=null?history.getOrderTotal():0);
					row.createCell(8).setCellValue(history.getCreditedCash()!=null?history.getCreditedCash():0);
					row.createCell(9).setCellValue(history.getRewardStatus()!=null?history.getRewardStatus().toString():"");
					row.createCell(10).setCellValue(history.getPromoCode()!=null?history.getPromoCode():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Affiliate_Customer_Orders.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/GetAffiliatePromoHistory")
	public AffiliatePromoCodeHistoryDTO getAffiliatePromoHistory(HttpServletRequest request, HttpServletResponse response,Model model)throws Exception {
		AffiliatePromoCodeHistoryDTO affiliatePromoCodeHistoryDTO = new AffiliatePromoCodeHistoryDTO();
		Error error = new Error();
		model.addAttribute("affiliatePromoCodeHistoryDTO", affiliatePromoCodeHistoryDTO);
		
		try {
			String userId = request.getParameter("userId");
			if(StringUtils.isEmpty(userId)){
				System.err.println("Please select Affiliate.");
				error.setDescription("Please select Affiliate.");
				affiliatePromoCodeHistoryDTO.setError(error);
				affiliatePromoCodeHistoryDTO.setStatus(0);
				return affiliatePromoCodeHistoryDTO;
			}
			
			List<AffiliatePromoCodeHistory> affiliatePromoCodeHistories = null;
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
						
			affiliatePromoCodeHistories = DAORegistry.getAffiliatePromoCodeHistoryDAO().getAllHistoryByUserId(Integer.parseInt(userId.trim()));
				
			if(affiliatePromoCodeHistories == null || affiliatePromoCodeHistories.isEmpty()){
				affiliatePromoCodeHistoryDTO.setMessage("No PromoCode History found for selected Affiliate");
			}
			affiliatePromoCodeHistoryDTO.setStatus(1);
			affiliatePromoCodeHistoryDTO.setAffiliatePromoCodeHistories(affiliatePromoCodeHistories);
			affiliatePromoCodeHistoryDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(affiliatePromoCodeHistories!=null?affiliatePromoCodeHistories.size():0));
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching PromoCode History.");
			affiliatePromoCodeHistoryDTO.setError(error);
			affiliatePromoCodeHistoryDTO.setStatus(0);
		}
		return affiliatePromoCodeHistoryDTO;
	}
	
	@RequestMapping(value = "/GenerateAffiliatePromoCode")
	public AffiliatePromoCodeDTO generateAffiliatePromoCode(HttpServletRequest request, HttpServletResponse response,Model model){
		AffiliatePromoCodeDTO affiliatePromoCodeDTO = new AffiliatePromoCodeDTO();
		Error error = new Error();
		model.addAttribute("affiliatePromoCodeDTO", affiliatePromoCodeDTO);

		try{
			String promoCode = null;
			String userId = request.getParameter("userId");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");			
			promoCode = request.getParameter("promoCode");
			
			if(StringUtils.isEmpty(userId)){
				System.err.println("Please select Affiliate.");
				error.setDescription("Please select Affiliate.");
				affiliatePromoCodeDTO.setError(error);
				affiliatePromoCodeDTO.setStatus(0);
				return affiliatePromoCodeDTO;
			}
			if(StringUtils.isEmpty(fromDateStr)){
				System.err.println("Please select From Date.");
				error.setDescription("Please select From Date.");
				affiliatePromoCodeDTO.setError(error);
				affiliatePromoCodeDTO.setStatus(0);
				return affiliatePromoCodeDTO;
			}
			if(StringUtils.isEmpty(toDateStr)){
				System.err.println("Please select To Date.");
				error.setDescription("Please select To Date.");
				affiliatePromoCodeDTO.setError(error);
				affiliatePromoCodeDTO.setStatus(0);
				return affiliatePromoCodeDTO;
			}
			
			TrackerUser trackerUser = DAORegistry.getTrackerUserDAO().get(Integer.parseInt(userId.trim()));
			fromDateStr = fromDateStr+" 00:00:00";
			toDateStr = toDateStr+" 23:59:59";
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
			Date fromDate = dateFormat.parse(fromDateStr);
			Date toDate = dateFormat.parse(toDateStr);
			
			AffiliatePromoCodeHistory affiliatePromoHistory = null;
			
			if(promoCode == null || promoCode.isEmpty()){
				promoCode = com.rtw.tmat.utils.Util.generateCustomerReferalCode();
			}
			
			//Affiliate Promotional Code History
			affiliatePromoHistory = new AffiliatePromoCodeHistory();
			affiliatePromoHistory.setUserId(trackerUser.getId());
			affiliatePromoHistory.setPromoCode(promoCode);
			affiliatePromoHistory.setCreateDate(new Date());
			affiliatePromoHistory.setUpdateDate(new Date());
			affiliatePromoHistory.setStatus("ACTIVE");
			affiliatePromoHistory.setEffectiveFromDate(fromDate);
			affiliatePromoHistory.setEffectiveToDate(toDate);
			
			//Affiliate - Set Promotional Code
			trackerUser.setPromotionalCode(promoCode);
			
			DAORegistry.getTrackerUserDAO().saveOrUpdate(trackerUser);
			DAORegistry.getAffiliatePromoCodeHistoryDAO().saveOrUpdate(affiliatePromoHistory);
			
			affiliatePromoCodeDTO.setStatus(1);
			affiliatePromoCodeDTO.setPromoCode(promoCode);
			
			//Tracking User Action
			String userActionMsg = "Affiliate PromoCode Generated.";
			com.rtw.tmat.utils.Util.userActionAudit(request, Integer.parseInt(userId.trim()), userActionMsg);
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while creating Promotional Code for Affiliate");
			affiliatePromoCodeDTO.setError(error);
			affiliatePromoCodeDTO.setStatus(0);
		}
		return affiliatePromoCodeDTO;
	}
	
	@RequestMapping(value = "/CheckUserForEdit")
	public GenericResponseDTO checkUserForEdit(HttpServletRequest request, HttpServletResponse response,Model model){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		model.addAttribute("genericResponseDTO", genericResponseDTO);
		
		try{
			String userName = request.getParameter("userName");
			String email = request.getParameter("email");
			String userIdStr = request.getParameter("userId");
			
			if(StringUtils.isEmpty(userName)){
				System.err.println("Please provide User Name.");
				error.setDescription("Please provide User Name.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(email)){
				System.err.println("Please provide Email.");
				error.setDescription("Please provide Email.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(userIdStr)){
				System.err.println("Please select User.");
				error.setDescription("Please select User.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			TrackerUser trackerUser = DAORegistry.getTrackerUserDAO().getTrackerUserByUsernameOrEmailExceptUserId(userName, email, Integer.parseInt(userIdStr));
			
			if(trackerUser != null){				
				genericResponseDTO.setMessage("There is already an user with this Username or Email.");
			}else{				
				genericResponseDTO.setMessage("true");
			}
			genericResponseDTO.setStatus(1);			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while Checking User");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
	@RequestMapping({"/GetAffiliateOrderSummary"})
	public AffiliateOrderSummaryDTO getAffiliateOrderSummary(HttpServletRequest request, HttpServletResponse response,Model model){
		AffiliateOrderSummaryDTO affiliateOrderSummaryDTO = new AffiliateOrderSummaryDTO();
		Error error = new Error();
		model.addAttribute("affiliateOrderSummaryDTO", affiliateOrderSummaryDTO);
		
		try{
			String userId = request.getParameter("userId");
			String orderId = request.getParameter("orderId");
			
			if(StringUtils.isEmpty(userId)){
				System.err.println("Please select User.");
				error.setDescription("Please select User.");
				affiliateOrderSummaryDTO.setError(error);
				affiliateOrderSummaryDTO.setStatus(0);
				return affiliateOrderSummaryDTO;
			}
			if(StringUtils.isEmpty(orderId)){
				System.err.println("Please select Customer Order.");
				error.setDescription("Please select Customer Order.");
				affiliateOrderSummaryDTO.setError(error);
				affiliateOrderSummaryDTO.setStatus(0);
				return affiliateOrderSummaryDTO;
			}
			
			//Integer count = 0;
			//GridHeaderFilters filter = new GridHeaderFilters();
			AffiliateCashReward affiliateCashReward = null;
			AffiliateCashRewardHistory rewardHistory = null;
			SimpleDateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
						
			affiliateCashReward = DAORegistry.getAffiliateCashRewardDAO().getAffiliateByUserId(Integer.parseInt(userId));
			rewardHistory = DAORegistry.getAffiliateCashRewardHistoryDAO().getHistoryByUserIdAndOrderId(Integer.parseInt(userId), Integer.parseInt(orderId));
			
			if(rewardHistory == null){
				affiliateOrderSummaryDTO.setMessage("No Order Detail(s) found for selected  Affiliate Customer");
			}
			if(rewardHistory != null){
				String eventDateTime = rewardHistory.getOrder().getEventDateStr();
				String activeDateStr = null;
				
				if(rewardHistory.getOrder().getEventTimeStr() == null || rewardHistory.getOrder().getEventTimeStr().isEmpty()){
			    	eventDateTime = eventDateTime;
			    	Date activeDate = dateFormat.parse(eventDateTime);
				    Calendar calendar = Calendar.getInstance();
				    calendar.setTime(activeDate);
				    calendar.add(Calendar.DAY_OF_MONTH, 1);		     
				    activeDate = calendar.getTime();
				    activeDateStr = dateFormat.format(activeDate);
			    }else if(rewardHistory.getOrder().getEventTimeStr().equals("TBD")){
			    	eventDateTime = eventDateTime +" 12:00 AM";
			    }else{
			    	eventDateTime = eventDateTime +" "+rewardHistory.getOrder().getEventTimeStr();
			    }
			    
				if(rewardHistory.getOrder().getEventTimeStr() != null && !rewardHistory.getOrder().getEventTimeStr().isEmpty()){
				    Date activeDate = dateTimeFormat.parse(eventDateTime);
				    Calendar calendar = Calendar.getInstance();
				    calendar.setTime(activeDate);
				    calendar.add(Calendar.DAY_OF_MONTH, 1);		     
				    activeDate = calendar.getTime();
				    activeDateStr = dateTimeFormat.format(activeDate);
				}
			    affiliateOrderSummaryDTO.setActiveDate(activeDateStr);
			}
		    		    		    
		    affiliateOrderSummaryDTO.setStatus(1);
		    affiliateOrderSummaryDTO.setRewardHistory(rewardHistory);		    
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Affiliate Customer Order Details");
			affiliateOrderSummaryDTO.setError(error);
			affiliateOrderSummaryDTO.setStatus(0);
		}	
		return affiliateOrderSummaryDTO;
	}
	
	@RequestMapping(value = "/GetAffiliatesOrder")
	public AffiliateCashRewardHistoryDTO getAffiliatesOrderData(HttpServletRequest request, HttpServletResponse response,Model model){
		AffiliateCashRewardHistoryDTO affiliateCashRewardHistoryDTO = new AffiliateCashRewardHistoryDTO();
		Error error = new Error();
		model.addAttribute("affiliateCashRewardHistoryDTO", affiliateCashRewardHistoryDTO);
		
		try{			
			String userId = request.getParameter("userId");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			if(StringUtils.isEmpty(userId)){
				System.err.println("Please select Affiliate.");
				error.setDescription("Please select Affiliate.");
				affiliateCashRewardHistoryDTO.setError(error);
				affiliateCashRewardHistoryDTO.setStatus(0);
				return affiliateCashRewardHistoryDTO;
			}
			if(StringUtils.isEmpty(pageNo)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				affiliateCashRewardHistoryDTO.setError(error);
				affiliateCashRewardHistoryDTO.setStatus(0);
				return affiliateCashRewardHistoryDTO;
			}
			try {
				Integer.parseInt(pageNo);
			} catch (Exception e) {
				System.err.println("Please provide valid PageNo.");
				error.setDescription("Please provide valid PageNo.");
				affiliateCashRewardHistoryDTO.setError(error);
				affiliateCashRewardHistoryDTO.setStatus(0);
				return affiliateCashRewardHistoryDTO;
			}
			
			GridHeaderFilters filter = null;
			try {
				filter = GridHeaderFiltersUtil.getAffiliateReportSearchHeaderFilters(headerFilter);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				affiliateCashRewardHistoryDTO.setError(error);
				affiliateCashRewardHistoryDTO.setStatus(0);
				return affiliateCashRewardHistoryDTO;
			}
			
			Integer count = 0;
			Collection<AffiliateCashRewardHistory> historyList = null;
			
			historyList = DAORegistry.getAffiliateCashRewardHistoryDAO().getHistoryByUserId(Integer.parseInt(userId), filter);			
			count = DAORegistry.getAffiliateCashRewardHistoryDAO().getHistoryCountByUserId(Integer.parseInt(userId), filter);
			
			if(historyList == null || historyList.isEmpty()){
				affiliateCashRewardHistoryDTO.setMessage("No Affiliate Order found for selected Affiliates");
			}
			affiliateCashRewardHistoryDTO.setStatus(1);
			affiliateCashRewardHistoryDTO.setAffiliatesOrderDTO(Util.getAffiliatesArray(historyList));
			affiliateCashRewardHistoryDTO.setPaginationDTO(PaginationUtil.calculatePaginationParameter(count, null));
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Affiliates Order");
			affiliateCashRewardHistoryDTO.setError(error);
			affiliateCashRewardHistoryDTO.setStatus(0);
		}
		return affiliateCashRewardHistoryDTO;
	}
	
	
	@RequestMapping(value = "/CheckAffiliatePromoCode")
	public String checkAffiliatePromoCode(HttpServletRequest request, HttpServletResponse response, Model model){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		model.addAttribute("genericResponseDTO", genericResponseDTO);
		try{
			String msg = "";
			String userId = request.getParameter("userId");
			String promoCode = request.getParameter("promoCode");
			
			AffiliatePromoCodeHistory affiliatePromoHistory = DAORegistry.getAffiliatePromoCodeHistoryDAO().checkActiveAffiliatePromoCode(promoCode);			
			if(affiliatePromoHistory != null){
				msg = "There is already an Affiliate with this Promotional Code.";
			}else{
				msg = "true";
			}
			genericResponseDTO.setStatus(1);
			genericResponseDTO.setMessage(msg);
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("There is something wrong. Please try again.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return "";
	}
	
	@RequestMapping(value = "/getAffiliatePaymentNote")
	public AffiliateOrderNoteDTO getAffiliatePaymentNote(HttpServletRequest request, HttpServletResponse response, Model model){
		AffiliateOrderNoteDTO affiliateOrderNoteDTO = new AffiliateOrderNoteDTO();
		Error error = new Error();
		model.addAttribute("affiliateOrderNoteDTO", affiliateOrderNoteDTO);
		
		try{
			String userIdStr = request.getParameter("userId");
			String orderIdStr = request.getParameter("orderId");
			
			if(StringUtils.isEmpty(userIdStr)){
				System.err.println("Please select Affiliate.");
				error.setDescription("Please select Affiliate.");
				affiliateOrderNoteDTO.setError(error);
				affiliateOrderNoteDTO.setStatus(0);
				return affiliateOrderNoteDTO;
			}			
			if(StringUtils.isEmpty(orderIdStr)){
				System.err.println("Please select Customer Order.");
				error.setDescription("Please select Customer Order.");
				affiliateOrderNoteDTO.setError(error);
				affiliateOrderNoteDTO.setStatus(0);
				return affiliateOrderNoteDTO;
			}
			
			AffiliateCashRewardHistory affiliateCashRewardHistory = DAORegistry.getAffiliateCashRewardHistoryDAO().getHistoryByUserIdAndOrderId(Integer.parseInt(userIdStr), Integer.parseInt(orderIdStr));			
			if(affiliateCashRewardHistory == null){
				System.err.println("Affiliate Order not found.");
				error.setDescription("Affiliate Order not found.");
				affiliateOrderNoteDTO.setError(error);
				affiliateOrderNoteDTO.setStatus(0);
				return affiliateOrderNoteDTO;
			}
						
			affiliateOrderNoteDTO.setStatus(1);
			affiliateOrderNoteDTO.setOrderId(affiliateCashRewardHistory.getOrder().getId());
			affiliateOrderNoteDTO.setPaymentNote(affiliateCashRewardHistory.getPaymentNote());
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Payment Note in Affiliate Cash Reward.");
			affiliateOrderNoteDTO.setError(error);
			affiliateOrderNoteDTO.setStatus(0);
		}
		return affiliateOrderNoteDTO;
	}
	
	@RequestMapping(value = "/saveAffiliatePaymentNote")
	public GenericResponseDTO saveAffiliatePaymentNote(HttpServletRequest request, HttpServletResponse response, Model model){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		model.addAttribute("genericResponseDTO", genericResponseDTO);
		
		try{			
			String userIdStr = request.getParameter("userId");
			String orderIdStr = request.getParameter("orderId");
			String paymentNote = request.getParameter("paymentNote");
			String userActionMsg = "";
			
			if(StringUtils.isEmpty(userIdStr)){
				System.err.println("Please select Affiliate.");
				error.setDescription("Please select Affiliate.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}			
			if(StringUtils.isEmpty(orderIdStr)){
				System.err.println("Please select Customer Order.");
				error.setDescription("Please select Customer Order.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			AffiliateCashRewardHistory affiliateCashRewardHistory = DAORegistry.getAffiliateCashRewardHistoryDAO().getHistoryByUserIdAndOrderId(Integer.parseInt(userIdStr), Integer.parseInt(orderIdStr));			
			if(affiliateCashRewardHistory == null){
				System.err.println("Affiliate Order not found.");
				error.setDescription("Affiliate Order not found.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			
			if(affiliateCashRewardHistory.getRewardStatus().equals(RewardStatus.ACTIVE)){
				Double creditedCash = affiliateCashRewardHistory.getCreditedCash();	//From History table
				
				AffiliateCashReward affiliateCashReward = DAORegistry.getAffiliateCashRewardDAO().getAffiliateByUserId(Integer.parseInt(userIdStr));
				if(affiliateCashReward == null){
					System.err.println("Affiliate Cash Reward not found.");
					error.setDescription("Affiliate Cash Reward not found.");
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					return genericResponseDTO;
				}
				Double activeCash = affiliateCashReward.getActiveCash()!=null?affiliateCashReward.getActiveCash():0.00;//From Reward Table
				Double totalPaidCash = affiliateCashReward.getTotalPaidCash()!=null?affiliateCashReward.getTotalPaidCash():0.00;
				
				affiliateCashReward.setActiveCash(activeCash - creditedCash);
				affiliateCashReward.setLastPaidCash(creditedCash);
				affiliateCashReward.setTotalPaidCash(totalPaidCash + creditedCash);
				affiliateCashReward.setLastUpdate(new Date());
				DAORegistry.getAffiliateCashRewardDAO().update(affiliateCashReward);
				
				//Update Affiliate Cash Reward History
				affiliateCashRewardHistory.setPaymentNote(paymentNote);
				affiliateCashRewardHistory.setRewardStatus(RewardStatus.PAID);
				affiliateCashRewardHistory.setUpdatedDate(new Date());
				DAORegistry.getAffiliateCashRewardHistoryDAO().update(affiliateCashRewardHistory);
				
				genericResponseDTO.setMessage("Affiliate Order is Paid.");
				
				userActionMsg = "Affiliate - Customer Order Fee is Paid"; //Tracking User Action Msg
			}else{
				//Update Affiliate Cash Reward History
				affiliateCashRewardHistory.setPaymentNote(paymentNote);
				affiliateCashRewardHistory.setUpdatedDate(new Date());
				DAORegistry.getAffiliateCashRewardHistoryDAO().update(affiliateCashRewardHistory);
				
				genericResponseDTO.setMessage("Notes Updated Successfully.");
				
				userActionMsg = "Affiliate - Customer Order Notes Updated"; //Tracking User Action Msg
			}
						
			genericResponseDTO.setStatus(1);
			
			//Tracking User Action			
			com.rtw.tmat.utils.Util.userActionAudit(request, Integer.parseInt(userIdStr), userActionMsg);
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while updating Payment in Affiliate Cash Reward.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
	@RequestMapping(value = "/ContestAffiliates")
	public ContestAffiliatesDTO getContestAffiliates(HttpServletRequest request, HttpServletResponse response){
		ContestAffiliatesDTO affiliateDTO = new ContestAffiliatesDTO();
		Error error = new Error();
		try {
			String status = request.getParameter("status");
			String headerFilter = request.getParameter("headerFilter");
			
			if(status==null || status.isEmpty()){
				error.setDescription("Please send Affiliate status.");
				affiliateDTO.setError(error);
				affiliateDTO.setStatus(0);
				return affiliateDTO;
			}
			GridHeaderFilters filter = GridHeaderFiltersUtil.getContestAffiliatesFilters(headerFilter);
			List<ContestAffiliates> affiliates = DAORegistry.getContestAffiliatesDAO().getAllContestAffiliates(status, filter);
			affiliateDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(affiliates.size()));
			affiliateDTO.setContestAffiliatesList(affiliates);
			affiliateDTO.setStatus(1);
			return affiliateDTO;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while getting contest affiliates.");
			affiliateDTO.setError(error);
			affiliateDTO.setStatus(0);
			return affiliateDTO;
		}
	}
	
	
	
	@RequestMapping(value = "/ContestAffiliatesEarning")
	public ContestAffiliateEarningDTO getContestAffiliatesEarning(HttpServletRequest request, HttpServletResponse response){
		ContestAffiliateEarningDTO affiliateDTO = new ContestAffiliateEarningDTO();
		Error error = new Error();
		try {
			String customerId = request.getParameter("customerId");
			String headerFilter = request.getParameter("headerFilter");
			String referralStatus = request.getParameter("referralStatus");
			
			if(customerId==null || customerId.isEmpty()){
				error.setDescription("Please send affilate id.");
				affiliateDTO.setError(error);
				affiliateDTO.setStatus(0);
				return affiliateDTO;
			}
			if(referralStatus==null || referralStatus.isEmpty()){
				error.setDescription("Please send earning status pending/completed.");
				affiliateDTO.setError(error);
				affiliateDTO.setStatus(0);
				return affiliateDTO;
			}
			
			Integer custId = 0;
			try {
				custId = Integer.parseInt(customerId);
			} catch (Exception e) {
				error.setDescription("Please send valid affilate id.");
				affiliateDTO.setError(error);
				affiliateDTO.setStatus(0);
				return affiliateDTO;
			}
			GridHeaderFilters filter = GridHeaderFiltersUtil.getContestAffiliateEarningFilters(headerFilter);
			if(referralStatus.equalsIgnoreCase("COMPLETED")){
				List<ContestAffiliateEarning> earnings = DAORegistry.getContestAffiliatesDAO().getContestAffiliateEarnings(custId,filter);
				if(earnings.size()==0){
					if(headerFilter!=null && !headerFilter.isEmpty()){
						error.setDescription("No referral earning records found for selected Affiliate with give search parameter.");
					}else{
						error.setDescription("No referral earning records found for selected Affiliate.");
					}
					
					affiliateDTO.setError(error);
					affiliateDTO.setStatus(0);
					return affiliateDTO;
				}
				affiliateDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(earnings.size()));
				affiliateDTO.setAffiliateEarnings(earnings);
				affiliateDTO.setStatus(1);
				return affiliateDTO;
			}else if(referralStatus.equalsIgnoreCase("PENDING")){
				List<ContestAffiliateEarning> earnings = DAORegistry.getContestAffiliatesDAO().getContestAffiliatePendingEarning(custId,filter);
				if(earnings.size()==0){
					if(headerFilter!=null && !headerFilter.isEmpty()){
						error.setDescription("No pending referral earning records found for selected Affiliate with give search parameter.");
					}else{
						error.setDescription("No pending referral earning records found for selected Affiliate.");
					}
					
					affiliateDTO.setError(error);
					affiliateDTO.setStatus(0);
					return affiliateDTO;
				}
				affiliateDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(earnings.size()));
				affiliateDTO.setAffiliateEarnings(earnings);
				affiliateDTO.setStatus(1);
				return affiliateDTO;
			}
			error.setDescription("Please send earning status either pending or completed.");
			affiliateDTO.setError(error);
			affiliateDTO.setStatus(0);
			return affiliateDTO;
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while getting contest affiliates.");
			affiliateDTO.setError(error);
			affiliateDTO.setStatus(0);
			return affiliateDTO;
		}
	}
	
	@RequestMapping(value = "/UpdateContestAffiliate")
	public ContestAffiliatesDTO updateContestAffiliates(HttpServletRequest request, HttpServletResponse response){
		ContestAffiliatesDTO affiliateDTO = new ContestAffiliatesDTO();
		Error error = new Error();
		try {
			String customerId = request.getParameter("customerId");
			String rewardType = request.getParameter("rewardType");
			String action = request.getParameter("action");
			String rewardValue = request.getParameter("rewardValue");
			String liveCount = request.getParameter("liveCount");
			String affilaiteLivesCount = request.getParameter("affilaiteLivesCount");
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			String userName = request.getParameter("userName");
			String custRewardDollar = request.getParameter("custRewardDollar");
			String custSuperFanStars = request.getParameter("custSuperFanStars");
			
			ContestAffiliates affiliate = null;
			if(action==null || action.isEmpty()){
				error.setDescription("Please provide valid action.");
				affiliateDTO.setError(error);
				affiliateDTO.setStatus(0);
				return affiliateDTO;
			}
			
			if(customerId==null || customerId.isEmpty()){
				error.setDescription("Please provide Customer id.");
				affiliateDTO.setError(error);
				affiliateDTO.setStatus(0);
				return affiliateDTO;
			}
			Integer custId = null;
			try {
				custId = Integer.parseInt(customerId);
			} catch (Exception e) {
				error.setDescription("Please provide Valid Customer id.");
				affiliateDTO.setError(error);
				affiliateDTO.setStatus(0);
				return affiliateDTO;
			}
			
			if(action.equalsIgnoreCase("EDIT")){
				Customer customer = DAORegistry.getCustomerDAO().get(custId);
				affiliate = DAORegistry.getContestAffiliatesDAO().getContestAffiliateByCustomerId(custId);
				affiliate.setFirstName(customer.getUserId()+" - "+customer.getEmail());
				affiliateDTO.setContestAffiliates(affiliate);
				affiliateDTO.setStatus(1);
				return affiliateDTO;
			}
			
			if(rewardType==null || rewardType.isEmpty()){
				error.setDescription("Please provide reward type.");
				affiliateDTO.setError(error);
				affiliateDTO.setStatus(0);
				return affiliateDTO;
			}
			if(rewardValue==null || rewardValue.isEmpty()){
				error.setDescription("Please provide reward value.");
				affiliateDTO.setError(error);
				affiliateDTO.setStatus(0);
				return affiliateDTO;
			}
			Double rewardVal = null;
			try {
				rewardVal = Double.parseDouble(rewardValue);
			} catch (Exception e) {
				error.setDescription("Please provide valid reward value.");
				affiliateDTO.setError(error);
				affiliateDTO.setStatus(0);
				return affiliateDTO;
			}
			if(liveCount==null || liveCount.isEmpty()){
				error.setDescription("Please provide Customer Lives.");
				affiliateDTO.setError(error);
				affiliateDTO.setStatus(0);
				return affiliateDTO;
			}
			if(affilaiteLivesCount==null || affilaiteLivesCount.isEmpty()){
				error.setDescription("Please provide Affiliate Lives.");
				affiliateDTO.setError(error);
				affiliateDTO.setStatus(0);
				return affiliateDTO;
			}
			Integer customerLive = 0;
			Integer affiliateLive = 0;
			try {
				customerLive = Integer.parseInt(liveCount);
				affiliateLive = Integer.parseInt(affilaiteLivesCount);
			} catch (Exception e) {
				error.setDescription("Please provide valid Customer/Affiliate Lives.");
				affiliateDTO.setError(error);
				affiliateDTO.setStatus(0);
				return affiliateDTO;
			}
			
			if(fromDate==null || fromDate.isEmpty()){
				error.setDescription("Please provide from date.");
				affiliateDTO.setError(error);
				affiliateDTO.setStatus(0);
				return affiliateDTO;
			}
			
			Date startDate = null;
			Date endDate = null;
			fromDate = fromDate+" 00:00:00";
			if(toDate!=null && !toDate.isEmpty()){
				toDate = toDate+" 23:59:59";
				try {
					endDate = Util.getDateWithTwentyFourHourFormat1(toDate);
				} catch (Exception e) {
					System.err.println("Please provide Valid To Date");
					error.setDescription("Please provide Valid To Date");
					affiliateDTO.setError(error);
					affiliateDTO.setStatus(0);
					return affiliateDTO;
				}
			}
			
			
			try {
				startDate = Util.getDateWithTwentyFourHourFormat1(fromDate);
			} catch (Exception e) {
				System.err.println("Please provide Valid From and To Date");
				error.setDescription("Please provide Valid From and To Date");
				affiliateDTO.setError(error);
				affiliateDTO.setStatus(0);
				return affiliateDTO;
			}
			if(custRewardDollar==null || custRewardDollar.isEmpty()){
				System.err.println("Please provide customer reward dollars.");
				error.setDescription("Please provide customer reward dollars.");
				affiliateDTO.setError(error);
				affiliateDTO.setStatus(0);
				return affiliateDTO;
			}
			if(custSuperFanStars==null || custSuperFanStars.isEmpty()){
				System.err.println("Please provide customer superfan starts.");
				error.setDescription("Please provide customer superfan starts.");
				affiliateDTO.setError(error);
				affiliateDTO.setStatus(0);
				return affiliateDTO;
			}
			Integer superfanStar = 0;
			Double rewardDollar = 0.00;
			try {
				superfanStar = Integer.parseInt(custSuperFanStars);
				rewardDollar = Double.parseDouble(custRewardDollar);
			} catch (Exception e) {
				System.err.println("Please provide valid customer superfan starts and customer reward dollars.");
				error.setDescription("Please provide valid customer superfan starts and customer reward dollars.");
				affiliateDTO.setError(error);
				affiliateDTO.setStatus(0);
				return affiliateDTO;
			}
			
			Customer customer = DAORegistry.getCustomerDAO().get(custId);
			if(customer==null){
				error.setDescription("Customer not found in system.");
				affiliateDTO.setError(error);
				affiliateDTO.setStatus(0);
				return affiliateDTO;
			}
			String msg = "";
			if(action.equalsIgnoreCase("SAVE")){
				affiliate = DAORegistry.getContestAffiliatesDAO().getContestAffiliateByCustomerId(custId);
				if(affiliate!=null){
					error.setDescription("Selected Customer is already affiliate, you can update it from list of affiliates.");
					affiliateDTO.setError(error);
					affiliateDTO.setStatus(0);
					return affiliateDTO;
				}
				if(custRewardDollar==null || custRewardDollar.isEmpty()){
					
				}
				affiliate = new ContestAffiliates();
				affiliate.setCreatedBy(userName);
				affiliate.setCreatedDate(new Date());
				affiliate.setStatus("ACTIVE");
				affiliate.setActiveCashReward(0.00);
				affiliate.setDebitedCashReward(0.00);
				affiliate.setTotalCashReward(0.00);
				affiliate.setTotalCreditedRewardDollar(0.00);
				affiliate.setVoidedCashReward(0.00);
				affiliate.setCustomerRewardDollars(rewardDollar);
				affiliate.setCustomerSuperFanStars(superfanStar);
				msg = "Affiliate Created Successfully.";
			}else if(action.equalsIgnoreCase("UPDATE")){
				affiliate = DAORegistry.getContestAffiliatesDAO().getContestAffiliateByCustomerId(custId);
				affiliate.setUpdatedBy(userName);
				affiliate.setUpdatedDate(new Date());
				msg = "Affiliate Updated Successfully.";
			}
			
			affiliate.setAffiliateLives(affiliateLive);
			affiliate.setCustomerId(custId);
			affiliate.setCustomerLives(customerLive);
			affiliate.setReferralCode(customer.getUserId());
			affiliate.setFromDate(startDate);
			affiliate.setToDate(endDate);
			affiliate.setRewardType(rewardType);
			affiliate.setRewardValue(rewardVal);
			affiliate.setCustomerRewardDollars(rewardDollar);
			affiliate.setCustomerSuperFanStars(superfanStar);
			DAORegistry.getContestAffiliatesDAO().saveOrUpdate(affiliate);
			affiliateDTO.setMessage(msg);
			affiliateDTO.setStatus(1);
			return affiliateDTO;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while updating/saving contest affiliates.");
			affiliateDTO.setError(error);
			affiliateDTO.setStatus(0);
			return affiliateDTO;
		}
	}
	
	
	@RequestMapping(value = "/ChangeStatusContestAffiliate")
	public GenericResponseDTO changeStatusContestAffiliate(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO affiliateDTO = new GenericResponseDTO();
		Error error = new Error();
		try {
			String status = request.getParameter("status");
			String customerId = request.getParameter("customerId");
			String userName = request.getParameter("userName");
			
			if(status==null || status.isEmpty()){
				error.setDescription("Please send Affiliate status.");
				affiliateDTO.setError(error);
				affiliateDTO.setStatus(0);
				return affiliateDTO;
			}
			if(customerId==null || customerId.isEmpty()){
				error.setDescription("Please send Affiiate id.");
				affiliateDTO.setError(error);
				affiliateDTO.setStatus(0);
				return affiliateDTO;
			}
			Integer custId = 0;
			try {
				custId = Integer.parseInt(customerId);
			} catch (Exception e) {
				error.setDescription("Please send valid Affiiate id.");
				affiliateDTO.setError(error);
				affiliateDTO.setStatus(0);
				return affiliateDTO;
			}
			
			ContestAffiliates affiliate = DAORegistry.getContestAffiliatesDAO().getContestAffiliateByCustomerId(custId);
			if(affiliate == null){
				error.setDescription("Affiliate not found in system.");
				affiliateDTO.setError(error);
				affiliateDTO.setStatus(0);
				return affiliateDTO;
			}
			
			affiliate.setStatus(status);
			affiliate.setUpdatedBy(userName);
			affiliate.setUpdatedDate(new Date());
			DAORegistry.getContestAffiliatesDAO().update(affiliate);
			
			String msg = null;
			if(status.equalsIgnoreCase("ACTIVE")){
				msg = "Affililate Moved to Active.";
			}else if(status.equalsIgnoreCase("INACTIVE")){
				msg = "Affililate Moved to In Active.";
			}
			affiliateDTO.setStatus(1);
			affiliateDTO.setMessage(msg);
			return affiliateDTO;
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while moving affiliates to active/inactive.");
			affiliateDTO.setError(error);
			affiliateDTO.setStatus(0);
			return affiliateDTO;
		}
	}
	
	@RequestMapping(value = "/AffiliateReport")
	public String getAffiliateReportPage(HttpServletRequest request, HttpServletResponse response,Model model){
		AffiliateReportDTO affiliateReportDTO = new AffiliateReportDTO();
		Error error = new Error();
		model.addAttribute("affiliateReportDTO", affiliateReportDTO);
		
		try{			
			String userId = request.getParameter("userId");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			if(StringUtils.isEmpty(userId)){
				System.err.println("Please Login.");
				error.setDescription("Please Login.");
				affiliateReportDTO.setError(error);
				affiliateReportDTO.setStatus(0);
			}			
			
			GridHeaderFilters filter = GridHeaderFiltersUtil.getAffiliateReportSearchHeaderFilters(headerFilter);
			
			Integer count = 0;
			AffiliateCashReward affiliateCashReward = null;
			Collection<AffiliateCashRewardHistory> historyList = null;
			AffiliatePromoCodeHistory history = null;
			
			affiliateCashReward = DAORegistry.getAffiliateCashRewardDAO().getAffiliateByUserId(Integer.parseInt(userId));
			historyList = DAORegistry.getAffiliateCashRewardHistoryDAO().getHistoryByUserId(Integer.parseInt(userId), filter);			
			count = DAORegistry.getAffiliateCashRewardHistoryDAO().getHistoryCountByUserId(Integer.parseInt(userId), filter);
			history = DAORegistry.getAffiliatePromoCodeHistoryDAO().getActiveAffiliatePromoCode(Integer.parseInt(userId));
			if(affiliateCashReward == null){
				affiliateCashReward = new AffiliateCashReward();
				affiliateCashReward.setActiveCash(0.00);
				affiliateCashReward.setLastCreditedCash(0.00);
				affiliateCashReward.setLastDebitedCash(0.00);
				affiliateCashReward.setLastVoidCash(0.00);
				affiliateCashReward.setPendingCash(0.00);
				affiliateCashReward.setTotalCreditedCash(0.00);
				affiliateCashReward.setTotalDebitedCash(0.00);
				affiliateCashReward.setTotalVoidedCash(0.00);
				affiliateCashReward.setTotalPaidCash(0.00);
			}
			
			if(historyList == null || historyList.isEmpty()){
				affiliateReportDTO.setMessage("No Customer Order found for selected Affiliates");
			}
			affiliateReportDTO.setStatus(1);
			affiliateReportDTO.setAffiliateCashRewardDTO(affiliateCashReward);
			affiliateReportDTO.setAffiliatePromoCodeHistoryDTO(history);
			affiliateReportDTO.setAffiliatesOrderDTO(Util.getAffiliatesArray(historyList));
			affiliateReportDTO.setPaginationDTO(PaginationUtil.calculatePaginationParameter(count, null));
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Affiliates Report");
			affiliateReportDTO.setError(error);
			affiliateReportDTO.setStatus(0);
		}
		return "page-affiliate-report";
	}
	
}
