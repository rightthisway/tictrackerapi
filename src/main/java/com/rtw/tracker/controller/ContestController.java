package com.rtw.tracker.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.microsoft.sqlserver.jdbc.SQLServerDriver;
import com.rtw.tmat.utils.Constants;
import com.rtw.tmat.utils.Error;
import com.rtw.tmat.utils.TextUtil;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.aws.AWSFileService;
import com.rtw.tracker.aws.AwsS3Response;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.dao.implementation.QuizDAORegistry;
import com.rtw.tracker.datas.Artist;
import com.rtw.tracker.datas.ChildCategory;
import com.rtw.tracker.datas.ContestConfigSettings;
import com.rtw.tracker.datas.ContestEventRequest;
import com.rtw.tracker.datas.ContestEvents;
import com.rtw.tracker.datas.ContestGrandWinners;
import com.rtw.tracker.datas.ContestPromocode;
import com.rtw.tracker.datas.ContestQuestions;
import com.rtw.tracker.datas.Contests;
import com.rtw.tracker.datas.Customer;
import com.rtw.tracker.datas.CustomerAddress;
import com.rtw.tracker.datas.CustomerContestQuestion;
import com.rtw.tracker.datas.CustomerLoyalty;
import com.rtw.tracker.datas.CustomerLoyaltyHistory;
import com.rtw.tracker.datas.CustomerProfileQuestion;
import com.rtw.tracker.datas.Event;
import com.rtw.tracker.datas.GiftCardBrand;
import com.rtw.tracker.datas.GiftCardOrderAttachment;
import com.rtw.tracker.datas.GiftCardOrders;
import com.rtw.tracker.datas.GrandChildCategory;
import com.rtw.tracker.datas.LoyaltySettings;
import com.rtw.tracker.datas.ParentCategory;
import com.rtw.tracker.datas.PreContestChecklist;
import com.rtw.tracker.datas.PreContestChecklistTracking;
import com.rtw.tracker.datas.PromotionalEarnLifeOffer;
import com.rtw.tracker.datas.QuestionBank;
import com.rtw.tracker.datas.RTFPromoOffers;
import com.rtw.tracker.datas.RTFPromoType;
import com.rtw.tracker.datas.RtfGiftCard;
import com.rtw.tracker.datas.RtfGiftCardQuantity;
import com.rtw.tracker.datas.RtfRewardConfigInfo;
import com.rtw.tracker.datas.TrackerUser;
import com.rtw.tracker.enums.ContestJackpotType;
import com.rtw.tracker.enums.FileType;
import com.rtw.tracker.enums.MiniJackpotType;
import com.rtw.tracker.enums.PromotionalType;
import com.rtw.tracker.enums.RewardStatus;
import com.rtw.tracker.pojos.AutoCompleteArtistAndCategoryDTO;
import com.rtw.tracker.pojos.AutoCompleteDTO;
import com.rtw.tracker.pojos.CassError;
import com.rtw.tracker.pojos.CommonRespInfo;
import com.rtw.tracker.pojos.ContWinnerRewardsInfo;
import com.rtw.tracker.pojos.ContestChecklistDTO;
import com.rtw.tracker.pojos.ContestChecklistTrackingDTO;
import com.rtw.tracker.pojos.ContestConfigSettingsDTO;
import com.rtw.tracker.pojos.ContestEventsDTO;
import com.rtw.tracker.pojos.ContestPromocodeDTO;
import com.rtw.tracker.pojos.ContestQuestionDTO;
import com.rtw.tracker.pojos.ContestServerNodesDTO;
import com.rtw.tracker.pojos.ContestWinner;
import com.rtw.tracker.pojos.ContestWinnerDTO;
import com.rtw.tracker.pojos.ContestsDTO;
import com.rtw.tracker.pojos.CustomerProfileQuestionDTO;
import com.rtw.tracker.pojos.CustomerQuestionDTO;
import com.rtw.tracker.pojos.GenericResponseDTO;
import com.rtw.tracker.pojos.GiftCardAutoComplete;
import com.rtw.tracker.pojos.GiftCardOrdersDTO;
import com.rtw.tracker.pojos.GiftCardOrdersEditDTO;
import com.rtw.tracker.pojos.GiftCardPojo;
import com.rtw.tracker.pojos.QuestionBankDTO;
import com.rtw.tracker.pojos.QuizContestInfo;
import com.rtw.tracker.pojos.QuizQuestionInfo;
import com.rtw.tracker.pojos.RtfConfigContestClusterNodes;
import com.rtw.tracker.pojos.RtfGiftCardsDTO;
import com.rtw.tracker.utils.EndContestThread;
import com.rtw.tracker.utils.FileUploadUtil;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.GridHeaderFiltersUtil;
import com.rtw.tracker.utils.GsonCustomConfig;
import com.rtw.tracker.utils.PaginationUtil;
import com.rtw.tracker.utils.URLUtil;
import com.rtw.tracker.utils.Util;

@Controller
public class ContestController {

	
	private static Logger contestLog = LoggerFactory.getLogger(ContestController.class);
	SharedProperty sharedProperty;
	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}
	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}
	
	@RequestMapping(value = "/Contests")
	public ContestsDTO getContestsPage(HttpServletRequest request, HttpServletResponse response){
		ContestsDTO contestsDTO = new ContestsDTO();
		Error error = new Error();
		try{			
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String status = request.getParameter("status");
			String type = request.getParameter("type");
			GridHeaderFilters filter = new GridHeaderFilters();
			if(type== null || type.isEmpty()){
				System.err.println("Please send valid Contest type.");
				error.setDescription("Please send valid Contest type.");
				contestsDTO.setError(error);
				contestsDTO.setStatus(0);
				return contestsDTO;
			}
			if(status== null || status.isEmpty()){
				System.err.println("Please send valid Tab Name.");
				error.setDescription("Please send valid Tab Name.");
				contestsDTO.setError(error);
				contestsDTO.setStatus(0);
				return contestsDTO;
			}
			
			if(status.equalsIgnoreCase("ALL")){
				Integer count = 0;
				filter = GridHeaderFiltersUtil.getContestsFilter(headerFilter);
				List<Contests> contestsList = QuizDAORegistry.getContestsDAO().getAllContests(null, filter,type,"'ACTIVE','STARTED'",sharedProperty);
				
				if(contestsList != null && contestsList.size() > 0){
					count = contestsList.size();
				}
				if(contestsList == null || contestsList.size() <= 0){
					contestsDTO.setMessage("No Contests found.");
				}
				
				contestsDTO.setStatus(1);			
				contestsDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(count));
				contestsDTO.setContestsList(contestsList);
			}else if(status.equalsIgnoreCase("EXPIRED")){
				Integer count = 0;
				filter = GridHeaderFiltersUtil.getContestsFilter(headerFilter);
				List<Contests> contestsList = QuizDAORegistry.getContestsDAO().getAllContests(null, filter,type,"'EXPIRED'",sharedProperty);
				
				if(contestsList != null && contestsList.size() > 0){
					count = contestsList.size();
				}
				if(contestsList == null || contestsList.size() <= 0){
					contestsDTO.setMessage("No Contests found.");
				}
				
				contestsDTO.setStatus(1);			
				contestsDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(count));
				contestsDTO.setContestsList(contestsList);
			}else if(status.equalsIgnoreCase("TODAY")){
				Calendar today = Calendar.getInstance();
				today.set(Calendar.HOUR, 00);
				today.set(Calendar.MINUTE, 00);
				today.set(Calendar.SECOND, 00);
				List<Contests> list = QuizDAORegistry.getContestsDAO().getAllContests(null, filter,type,"'ACTIVE','STARTED'",sharedProperty);
				Contests contest = QuizDAORegistry.getContestsDAO().getTodaysContest(type,"'ACTIVE','STARTED'");
				if(contest== null){
					System.err.println("There is no scheduled contest for today.");
					error.setDescription("There is no scheduled contest for today.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				Integer questionCount = QuizDAORegistry.getContestQuestionsDAO().getQuestionCount(contest.getId());
				List<Contests> contestsList = new ArrayList<Contests>();
				contestsList.add(contest);
				contestsDTO.setStatus(1);
				contestsDTO.setContestsList(contestsList);
				Contests c  = list.get(0);
				if(today.getTime().compareTo(c.getStartDateTime()) > 0){
					System.err.println("Not allowed to start contest, There is a past dated Contest in ACTIVE status please end it or update it to future date from All contest ACTIVE tab.");
					contestsDTO.setMessage("Not allowed to start contest, Contest with past date is in ACTIVE status please end it/update it to future date from All contest ACTIVE tab.");
					contestsDTO.setStatus(1);
					return contestsDTO;
				}
				if(!contest.getQuestionSize().equals(questionCount)){
					System.err.println("Not allowed to start contest, There is mismatch in number of question configured in contest level is "+contest.getQuestionSize()+" and actual number of question in contest is."+questionCount);
					contestsDTO.setMessage("Not allowed to start contest, There is mismatch in number of question configured in contest level is "+contest.getQuestionSize()+" and actual number of question in contest is."+questionCount);
					contestsDTO.setStatus(1);
					return contestsDTO;
				}
				
			}else if(status.equalsIgnoreCase("ACTIVECONTEST") || status.equalsIgnoreCase("EXPIREDCONTEST")){
				List<Contests> contestsList = null;
				if(status.equalsIgnoreCase("ACTIVECONTEST")){
					contestsList = QuizDAORegistry.getContestsDAO().getAllContests(null, filter,type,"'ACTIVE','STARTED'",sharedProperty);
				}else if(status.equalsIgnoreCase("EXPIREDCONTEST")){
					contestsList = QuizDAORegistry.getContestsDAO().getAllContests(null, filter,type,"'EXPIRED'",sharedProperty);
				}
				if(contestsList == null || contestsList.size() <= 0){
					contestsDTO.setMessage("No Contests found.");
				}
				
				contestsDTO.setStatus(1);
				contestsDTO.setContestsList(contestsList);
			}else if(status.equalsIgnoreCase("ACTIVEQ") || status.equalsIgnoreCase("USEDQ")){
				Integer count = 0;
				filter = GridHeaderFiltersUtil.getQuestionBankFilter(headerFilter); 
				List<QuestionBank> questionBankList = null;
				if(status.equalsIgnoreCase("ACTIVEQ")){
					questionBankList = QuizDAORegistry.getQuestionBankDAO().getQuestionBankList(filter, null, pageNo,"'ACTIVE'",true);
					count = QuizDAORegistry.getQuestionBankDAO().getQuestionBankCount(filter, null,"'ACTIVE'");
				}else if(status.equalsIgnoreCase("USEDQ")){
					questionBankList = QuizDAORegistry.getQuestionBankDAO().getQuestionBankList(filter, null, pageNo,"'USED'",true);
					count = QuizDAORegistry.getQuestionBankDAO().getQuestionBankCount(filter, null,"'USED'");
				}
				
				if(questionBankList == null || questionBankList.size() <= 0){
					contestsDTO.setMessage("No Question's found from Question Bank.");
				}
				
				contestsDTO.setStatus(1);			
				contestsDTO.setQuestionBankPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
				contestsDTO.setQuestionBankList(questionBankList);
			}else if(status.equalsIgnoreCase("REQUEST")){
				Integer count = 0;
				filter = GridHeaderFiltersUtil.getContestEventRequestFilter(headerFilter);
				List<ContestEventRequest> contestEventRequestList = QuizDAORegistry.getContestEventRequestDAO().getContestEventRequests(filter, null);
				
				if(contestEventRequestList != null && contestEventRequestList.size() > 0){
					count = contestEventRequestList.size();
				}
				if(contestEventRequestList == null || contestEventRequestList.size() <= 0){
					contestsDTO.setMessage("No Contest Event Request found.");
				}
				
				contestsDTO.setStatus(1);			
				contestsDTO.setContestEventRequestPaginationDTO(PaginationUtil.getDummyPaginationParameters(count));
				contestsDTO.setContestEventRequestList(contestEventRequestList);
			}
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Contests.");
			contestsDTO.setError(error);
			contestsDTO.setStatus(0);
		}		
		return contestsDTO;
	}
	
	
	@RequestMapping(value = "/ContestExportToExcel")
	public void contestExportToExcel(HttpServletRequest request, HttpServletResponse response){
		String type = request.getParameter("type");
		String status = request.getParameter("status");
		String headerFilter = request.getParameter("headerFilter");
		try {
			if(type!=null && !type.isEmpty()){
				if(status.equalsIgnoreCase("ACTIVE")){
					status = "'ACTIVE','STARTED'";
				}else if(status.equalsIgnoreCase("EXPIRED")){
					status = "'EXPIRED'";
				}
				GridHeaderFilters filter = GridHeaderFiltersUtil.getContestsFilter(headerFilter);
				List<Contests> contests = QuizDAORegistry.getContestsDAO().getAllContests(null, filter, type, status, sharedProperty);
				if(!contests.isEmpty()){
					SXSSFWorkbook workbook = new SXSSFWorkbook();
					SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Contests");
					Row header = sheet.createRow((int)0);
					header.createCell(0).setCellValue("Id");
					header.createCell(1).setCellValue("Contest Name");
					header.createCell(2).setCellValue("Start Date Time");
					header.createCell(3).setCellValue("Ticket Prize");
					header.createCell(4).setCellValue("Ticket Winner Threshold");
					header.createCell(5).setCellValue("Reward Point Prize");
					header.createCell(6).setCellValue("Ticket Max Price Threshold");
					header.createCell(7).setCellValue("Question Size");
					header.createCell(8).setCellValue("Artist/Event/Category");
					header.createCell(9).setCellValue("Promotional Code");
					header.createCell(10).setCellValue("Discount Percentage");
					header.createCell(11).setCellValue("Participant Stars");
					header.createCell(12).setCellValue("Referral Stars");
					header.createCell(13).setCellValue("Participant Rewards");
					header.createCell(14).setCellValue("Referral Rewards");
					header.createCell(15).setCellValue("Participant Lives");
					header.createCell(16).setCellValue("Status");
					header.createCell(17).setCellValue("Created Date");
					header.createCell(18).setCellValue("Created By");
					header.createCell(19).setCellValue("Updated Date");
					header.createCell(10).setCellValue("Updated By");
					
					int i=1;
					for(Contests c : contests){
						Row row = sheet.createRow(i);
						row.createCell(0).setCellValue(c.getId());
						row.createCell(1).setCellValue(c.getContestName());
						row.createCell(2).setCellValue(c.getStartDateTimeStr());
						row.createCell(3).setCellValue(c.getFreeTicketPerWinner());
						row.createCell(4).setCellValue(c.getTicketWinnerThreshhold());
						row.createCell(5).setCellValue(c.getRewardPoints());
						row.createCell(6).setCellValue(c.getSingleTicketPrice()!=null?c.getSingleTicketPrice():0);
						row.createCell(7).setCellValue(c.getQuestionSize());
						row.createCell(8).setCellValue(c.getPromoRefName());
						row.createCell(9).setCellValue(c.getPromotionalCode());
						row.createCell(10).setCellValue(c.getDiscountPercentage()!=null?c.getDiscountPercentage():0);
						row.createCell(11).setCellValue(c.getParticipantStars());
						row.createCell(12).setCellValue(c.getReferralStars());
						row.createCell(13).setCellValue(c.getParticipantRewards());
						row.createCell(14).setCellValue(c.getReferralRewards());
						row.createCell(15).setCellValue(c.getParticipantLives());
						row.createCell(16).setCellValue(c.getStatus());
						row.createCell(17).setCellValue(c.getCreatedDateTimeStr());
						row.createCell(18).setCellValue(c.getCreatedBy());
						row.createCell(19).setCellValue(c.getUpdatedDateTimeStr());
						row.createCell(20).setCellValue(c.getUpdatedBy());
						i++;
					}
					
					response.setContentType("application/vnd.ms-excel");
					response.setHeader("Content-Disposition", "attachment; filename=Contests.xls");
					workbook.write(response.getOutputStream());
					response.getOutputStream().flush();
				}
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/QuestionExportToExcel")
	public void questionExportToExcel(HttpServletRequest request, HttpServletResponse response){
		String contestId = request.getParameter("contestId");
		String headerFilter = request.getParameter("headerFilter");
		try {
			if(contestId!=null && !contestId.isEmpty()){
				GridHeaderFilters filter = GridHeaderFiltersUtil.getContestQuestionFilter(headerFilter);
				List<ContestQuestions> questions = QuizDAORegistry.getContestQuestionsDAO().getContestQuestions(filter, null, Integer.parseInt(contestId),sharedProperty);
				if(!questions.isEmpty()){
					SXSSFWorkbook workbook = new SXSSFWorkbook();
					SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Questions");
					Row header = sheet.createRow((int)0);
					header.createCell(0).setCellValue("Id");
					header.createCell(1).setCellValue("Contest Id");
					header.createCell(2).setCellValue("Serial No");
					header.createCell(3).setCellValue("Question");
					header.createCell(4).setCellValue("Oprion A");
					header.createCell(5).setCellValue("Oprion B");
					header.createCell(6).setCellValue("Oprion C");
					header.createCell(7).setCellValue("Answer");
					header.createCell(8).setCellValue("Question Rewards");
					header.createCell(9).setCellValue("Created Date");
					header.createCell(10).setCellValue("Created By");
					header.createCell(11).setCellValue("Updated Date");
					header.createCell(12).setCellValue("Updated By");
					header.createCell(13).setCellValue("Difficulty Level");
					
					int i=1;
					for(ContestQuestions q : questions){
						Row row = sheet.createRow(i);
						row.createCell(0).setCellValue(q.getId());
						row.createCell(1).setCellValue(q.getContestId());
						row.createCell(2).setCellValue(q.getSerialNo());
						row.createCell(3).setCellValue(q.getQuestion());
						row.createCell(4).setCellValue(q.getOptionA());
						row.createCell(5).setCellValue(q.getOptionB());
						row.createCell(6).setCellValue(q.getOptionC());
						row.createCell(7).setCellValue(q.getAnswer());
						row.createCell(8).setCellValue(q.getRewardPerQuestionStr());
						row.createCell(9).setCellValue(q.getCreatedDateTimeStr());
						row.createCell(10).setCellValue(q.getCreatedBy());
						row.createCell(11).setCellValue(q.getUpdatedDateTimeStr());
						row.createCell(12).setCellValue(q.getUpdatedBy());
						row.createCell(13).setCellValue(q.getDifficultyLevel());
						i++;
						
					}
					
					response.setContentType("application/vnd.ms-excel");
					response.setHeader("Content-Disposition", "attachment; filename=Questions.xls");
					workbook.write(response.getOutputStream());
					response.getOutputStream().flush();
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/ContestQuesBankExportToExcel")
	public void questionBankExportToExcel(HttpServletRequest request, HttpServletResponse response){
		String status = request.getParameter("status");
		String headerFilter = request.getParameter("headerFilter");
		try {
			if(status!=null && !status.isEmpty()){
				if(status.equalsIgnoreCase("ACTIVE")){
					status = "'ACTIVE'";
				}else if(status.equalsIgnoreCase("USED")){
					status = "'USED'";
				}
				GridHeaderFilters filter = GridHeaderFiltersUtil.getQuestionBankFilter(headerFilter);
				List<QuestionBank> questions = QuizDAORegistry.getQuestionBankDAO().getQuestionBankList(filter, null, null, status,false);
				if(!questions.isEmpty()){
					SXSSFWorkbook workbook = new SXSSFWorkbook();
					SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Questions");
					Row header = sheet.createRow((int)0);
					header.createCell(0).setCellValue("Id");
					header.createCell(1).setCellValue("Category");
					header.createCell(2).setCellValue("Difficulty Level");
					header.createCell(3).setCellValue("Question");
					header.createCell(4).setCellValue("Oprion A");
					header.createCell(5).setCellValue("Oprion B");
					header.createCell(6).setCellValue("Oprion C");
					header.createCell(7).setCellValue("Answer");
					header.createCell(8).setCellValue("Question Rewards");
					header.createCell(9).setCellValue("Created Date");
					header.createCell(10).setCellValue("Created By");
					header.createCell(11).setCellValue("Updated Date");
					header.createCell(12).setCellValue("Updated By");
					header.createCell(13).setCellValue("Status");
					
					int i=1;
					for(QuestionBank q : questions){
						Row row = sheet.createRow(i);
						row.createCell(0).setCellValue(q.getId());
						row.createCell(1).setCellValue(q.getCategory());
						row.createCell(2).setCellValue(q.getDificultyLevel());
						row.createCell(3).setCellValue(q.getQuestion());
						row.createCell(4).setCellValue(q.getOptionA());
						row.createCell(5).setCellValue(q.getOptionB());
						row.createCell(6).setCellValue(q.getOptionC());
						row.createCell(7).setCellValue(q.getAnswer());
						row.createCell(8).setCellValue(q.getRewardPerQuestionStr());
						row.createCell(9).setCellValue(q.getCreatedDateTimeStr());
						row.createCell(10).setCellValue(q.getCreatedBy());
						row.createCell(11).setCellValue(q.getUpdatedDateTimeStr());
						row.createCell(12).setCellValue(q.getUpdatedBy());
						row.createCell(13).setCellValue(q.getStatus());
						i++;
						
					}
					
					response.setContentType("application/vnd.ms-excel");
					response.setHeader("Content-Disposition", "attachment; filename=QuestionBank.xls");
					workbook.write(response.getOutputStream());
					response.getOutputStream().flush();
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping(value = "/AllContestEventExportToExcel")
	public ContestEventsDTO getContestEventExportToExcel(HttpServletRequest request, HttpServletResponse response){
		ContestEventsDTO eventDTO = new ContestEventsDTO();
		Error error = new Error();
		try{			
			String headerFilter = request.getParameter("headerFilter");
			String contestIdStr = request.getParameter("contestId");
			
			if(contestIdStr != null && !contestIdStr.trim().isEmpty()){
				Integer contestId = Integer.parseInt(contestIdStr);
				Contests contest = QuizDAORegistry.getContestsDAO().get(contestId);
				String zones = "";
				if(null != contest.getZone() && !contest.getZone().isEmpty()){
					String[] temp = contest.getZone().split(",");
					int i=0;
					for (String zone : temp) {
						if(i==0){
							zones = "'"+zone+"'";
							i++;
							continue;
						}
						zones = zones+",'"+zone+"'";
						i++;
					}
					
				}
				Double tixPrice = 0.00;
				if(null != contest.getSingleTicketPrice() && contest.getSingleTicketPrice() > 0){
					tixPrice = contest.getSingleTicketPrice();
				}
				GridHeaderFilters filter = GridHeaderFiltersUtil.getContestAllEventFilter(headerFilter);
				List<ContestEvents> allEventList = QuizDAORegistry.getContestEventsDAO().getAllEvents(filter, contestId, null,zones,tixPrice,contest.getFreeTicketPerWinner(),sharedProperty,false);
				if(!allEventList.isEmpty()){
					SXSSFWorkbook workbook = new SXSSFWorkbook();
					SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Contest_Events");
					Row header = sheet.createRow((int)0);
					header.createCell(0).setCellValue("Event Id");
					header.createCell(1).setCellValue("Contest Id");
					header.createCell(2).setCellValue("Event Name");
					header.createCell(3).setCellValue("Event Date");
					header.createCell(4).setCellValue("Event Time");
					header.createCell(5).setCellValue("Venue Id");
					header.createCell(6).setCellValue("Building");
					
					int i=1;
					for(ContestEvents e : allEventList){
						Row row = sheet.createRow(i);
						row.createCell(0).setCellValue(e.getEventId());
						row.createCell(1).setCellValue(e.getContestId());
						row.createCell(2).setCellValue(e.getEventName());
						row.createCell(3).setCellValue(e.getEventDateStr());
						row.createCell(4).setCellValue(e.getEventTimeStr());
						row.createCell(5).setCellValue(e.getVenueId());
						row.createCell(6).setCellValue(e.getBuilding());
						i++;
					}
					
					response.setContentType("application/vnd.ms-excel");
					response.setHeader("Content-Disposition", "attachment; filename=ContestEvents.xls");
					workbook.write(response.getOutputStream());
					response.getOutputStream().flush();
					
				}
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while loading contest events.");
			eventDTO.setError(error);
			eventDTO.setStatus(0);
		}		
		return eventDTO;
	}
	
	
	@RequestMapping(value = "/ExcludeContestEventExportToExcel")
	public ContestEventsDTO getContestExcludedEventExportToExcel(HttpServletRequest request, HttpServletResponse response){
		ContestEventsDTO eventDTO = new ContestEventsDTO();
		Error error = new Error();
		try{			
			String headerFilter = request.getParameter("headerFilter");
			String contestIdStr = request.getParameter("contestId");
			
			if(contestIdStr != null && !contestIdStr.trim().isEmpty()){
				Integer contestId = Integer.parseInt(contestIdStr);
				GridHeaderFilters filter1 = new GridHeaderFilters();
				List<ContestEvents> excludeContestEventList = QuizDAORegistry.getContestEventsDAO().getContestEvents(filter1, contestId,sharedProperty);
				if(!excludeContestEventList.isEmpty()){
					SXSSFWorkbook workbook = new SXSSFWorkbook();
					SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Contest_Events");
					Row header = sheet.createRow((int)0);
					header.createCell(0).setCellValue("Event Id");
					header.createCell(1).setCellValue("Contest Id");
					header.createCell(2).setCellValue("Event Name");
					header.createCell(3).setCellValue("Event Date");
					header.createCell(4).setCellValue("Event Time");
					header.createCell(5).setCellValue("Venue Id");
					header.createCell(6).setCellValue("Building");
					
					int i=1;
					for(ContestEvents e : excludeContestEventList){
						Row row = sheet.createRow(i);
						row.createCell(0).setCellValue(e.getEventId());
						row.createCell(1).setCellValue(e.getContestId());
						row.createCell(2).setCellValue(e.getEventName());
						row.createCell(3).setCellValue(e.getEventDateStr());
						row.createCell(4).setCellValue(e.getEventTimeStr());
						row.createCell(5).setCellValue(e.getVenueId());
						row.createCell(6).setCellValue(e.getBuilding());
						i++;
					}
					
					response.setContentType("application/vnd.ms-excel");
					response.setHeader("Content-Disposition", "attachment; filename=ContestExcludedEvents.xls");
					workbook.write(response.getOutputStream());
					response.getOutputStream().flush();
				}
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while loading contest events.");
			eventDTO.setError(error);
			eventDTO.setStatus(0);
		}		
		return eventDTO;
	}
		
	
	
	@RequestMapping(value = "/ResetContest")
	public ContestsDTO resetContest(HttpServletRequest request, HttpServletResponse response){
		ContestsDTO contestsDTO = new ContestsDTO();
		Error error = new Error();
		try {
			String contestId = request.getParameter("contestId");
			String userName = request.getParameter("userName");
			String action = request.getParameter("action");
			if(contestId== null || contestId.isEmpty()){
				System.err.println("Please send Contest Id to reset.");
				error.setDescription("Please send Contest Id to reset.");
				contestsDTO.setError(error);
				contestsDTO.setStatus(0);
				return contestsDTO;
			}
			if(action== null || action.isEmpty()){
				System.err.println("Please send valid Action RESET OR END.");
				error.setDescription("Please send valid Action RESET OR END.");
				contestsDTO.setError(error);
				contestsDTO.setStatus(0);
				return contestsDTO;
			}
			Contests contest = QuizDAORegistry.getContestsDAO().get(Integer.parseInt(contestId));
			if(contest == null){
				System.err.println("Contest not found in system with given id.");
				error.setDescription("Contest not found in system with given id.");
				contestsDTO.setError(error);
				contestsDTO.setStatus(0);
				return contestsDTO;
			}
			if(action.equalsIgnoreCase("RESET")){
				//contest.setWinnerCount(0);
				//contest.setPartipantCount(0);
				//contest.setPointWinnerCount(0);
				//contest.setTicketWinnerCount(0);
				contest.setUpdatedBy(userName);
				contest.setUpdatedDate(new Date());
				contest.setStatus("ACTIVE");
				contest.setLastAction(null);
				contest.setLastQuestionNo(null);
				contest.setIsCustomerStatUpdated(false);
				
				QuizDAORegistry.getContestsDAO().update(contest);
				Util.isContestRunning = false;
				Map<String, String> map = com.rtw.tmat.utils.Util.getParameterMap(request);
				map.put("coId", contestId.toString());
				map.put("customerId","0");
				
				for(RtfConfigContestClusterNodes node : Util.getContestServerNodeUrls()){
					if(node.getUrl().equalsIgnoreCase(Constants.BASE_URL)){
						map.put("clearDbData","Y");
					}else{
						map.put("clearDbData","N");
					}
					String data = com.rtw.tmat.utils.Util.getObject(map,node.getUrl()+Constants.RESET_CONTEST_CASS);
					if(data == null || data.isEmpty()){
						if(node.getUrl().equalsIgnoreCase(Constants.BASE_URL)){
							contestLog.error("RTFAPI ERROR (RemoveCassContCacheData): NULL RESPONSE : --URL: "+node.getUrl());
							error.setDescription("No Response from API server, Please check API server.");
							contestsDTO.setError(error);
							contestsDTO.setStatus(0);
							return contestsDTO;
						}else{
							contestLog.error("RTFAPI ERROR (RemoveCassContCacheData): NULL RESPONSE : --URL: "+node.getUrl());
							continue;
						}
					}
					
					Gson gson = GsonCustomConfig.getGsonBuilder();		
					JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
					CommonRespInfo contestInfo = gson.fromJson(((JsonObject)jsonObject.get("commonRespInfo")), CommonRespInfo.class);
					
					if(contestInfo.getSts() == 0){
						if(node.getUrl().equalsIgnoreCase(Constants.BASE_URL)){
							contestLog.error("RTFAPI ERROR (RemoveCassContCacheData): "+contestInfo.getErr().getDesc()+" : --URL: "+node.getUrl());
							error.setDescription(contestInfo.getErr().getDesc());
							contestsDTO.setError(error);
							contestsDTO.setStatus(0);
							return contestsDTO;
						}else{
							contestLog.error("RTFAPI ERROR (RemoveCassContCacheData): "+contestInfo.getErr().getDesc()+" : --URL: "+node.getUrl());
							continue;
						}
					}
				}
				
				Map<String, String> map1 = com.rtw.tmat.utils.Util.getParameterMap(request);
				map1.put("cType","MOBILE");
				com.rtw.tmat.utils.Util.getObject(map1,Constants.BASE_URL+Constants.REFRESH_CONTEST);
				contestsDTO.setMessage("Contest data reseted sucessfully.");
				contestLog.info("Contest Data Reseted : -- coId: "+contestId+" -- user: "+userName);
				contestsDTO.setStatus(1);
				return contestsDTO;
			}else if(action.equalsIgnoreCase("END")){
				Boolean isEndedSuccessfully = false;
				if(contest.getLastAction()!=null && contest.getLastAction().equalsIgnoreCase("WINNER")){
					isEndedSuccessfully = true;
				}
				
				contest.setUpdatedBy(userName);
				contest.setUpdatedDate(new Date());
				contest.setStatus("EXPIRED");
				contest.setLastQuestionNo(contest.getQuestionSize());
				contest.setLastAction("END");
				contest.setIsCustomerStatUpdated(false);
				QuizDAORegistry.getContestsDAO().update(contest);
				Util.isContestRunning = false;
				
				PreContestChecklist preContest = QuizDAORegistry.getContestChecklistDAO().getPreContestChecklistByContestId(contest.getId());
				if(preContest!=null){
					preContest.setStatus("EXPIRED");
					PreContestChecklistTracking tracking = new PreContestChecklistTracking(preContest);
					QuizDAORegistry.getContestChecklistDAO().update(preContest);
					QuizDAORegistry.getPreContestChecklistTrackingDAO().save(tracking);
				}
				
				
				Map<String, String> map = com.rtw.tmat.utils.Util.getParameterMap(request);
				map.put("contestId", contestId.toString());
				map.put("eType", "FORCESTOP");
				
				for(RtfConfigContestClusterNodes node : Util.getContestServerNodeUrls()){
					if(node.getUrl().equalsIgnoreCase(Constants.BASE_URL)){
						map.put("clearDbData","Y");
					}else{
						map.put("clearDbData","N");
					}
					String data = com.rtw.tmat.utils.Util.getObject(map,node.getUrl()+Constants.END_QUIZ_CONTEST);
					if(data == null || data.isEmpty()){
						if(node.getUrl().equalsIgnoreCase(Constants.BASE_URL)){
							contestLog.error("RTFAPI ERROR (UpdateEndQuizContest): NULL RESPONSE : --URL: "+node.getUrl());
							error.setDescription("No Response from API server, Please check API server.");
							contestsDTO.setError(error);
							contestsDTO.setStatus(0);
							return contestsDTO;
						}else{
							contestLog.error("RTFAPI ERROR (UpdateEndQuizContest): NULL RESPONSE : --URL: "+node.getUrl());
							continue;
						}
					}
					
					Gson gson = GsonCustomConfig.getGsonBuilder();		
					JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
					QuizContestInfo quizContestInfo = gson.fromJson(((JsonObject)jsonObject.get("quizContestInfo")), QuizContestInfo.class);
					
					if(quizContestInfo.getStatus() == 0){
						if(node.getUrl().equalsIgnoreCase(Constants.BASE_URL)){
							contestLog.error("RTFAPI ERROR (UpdateEndQuizContest): "+quizContestInfo.getError().getDescription()+" -- URL: "+node.getUrl());
							error.setDescription(quizContestInfo.getError().getDescription());
							contestsDTO.setError(error);
							contestsDTO.setStatus(0);
							return contestsDTO;
						}else{
							contestLog.error("RTFAPI ERROR (UpdateEndQuizContest): "+quizContestInfo.getError().getDescription()+" -- URL: "+node.getUrl());
							continue;
						}
					}
				}
				
				EndContestThread endQuiz = new EndContestThread(contest.getId(), request, isEndedSuccessfully);
				Thread thread = new Thread(endQuiz);
				thread.start();
				
				contestsDTO.setMessage("Contest Ended successfully.");
				contestsDTO.setStatus(1);
				return contestsDTO;
			}else if(action.equalsIgnoreCase("ACTIVE")){
				contest.setUpdatedBy(userName);
				contest.setUpdatedDate(new Date());
				contest.setStatus("ACTIVE");
				contest.setIsCustomerStatUpdated(false);
				QuizDAORegistry.getContestsDAO().update(contest);
				
				Map<String, String> map1 = com.rtw.tmat.utils.Util.getParameterMap(request);
				map1.put("cType","MOBILE");
				String data1 = com.rtw.tmat.utils.Util.getObject(map1,Constants.BASE_URL+Constants.REFRESH_CONTEST);
				
				PreContestChecklist preContest = QuizDAORegistry.getContestChecklistDAO().getPreContestChecklistByContestId(contest.getId());
				if(preContest!=null){
					preContest.setStatus("ACTIVE");
					PreContestChecklistTracking tracking = new PreContestChecklistTracking(preContest);
					QuizDAORegistry.getContestChecklistDAO().update(preContest);
					QuizDAORegistry.getPreContestChecklistTrackingDAO().save(tracking);
				}
				
				contestsDTO.setMessage("Contest move to Active status successfully.");
				contestsDTO.setStatus(1);
				return contestsDTO;
			}else if(action.equalsIgnoreCase("REFRESH")){
				Map<String, String> map = com.rtw.tmat.utils.Util.getParameterMap(request);
				map.put("contestId", contestId.toString());
				String data = com.rtw.tmat.utils.Util.getObject(map,Constants.BASE_URL+Constants.REFRESH_CONTEST_CACHE);
				Gson gson = GsonCustomConfig.getGsonBuilder();		
				JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
				QuizContestInfo quizContestInfo = gson.fromJson(((JsonObject)jsonObject.get("quizContestInfo")), QuizContestInfo.class);
				if(quizContestInfo == null || quizContestInfo.getStatus()==0){
					System.err.println(quizContestInfo.getError().getDescription());
					error.setDescription(quizContestInfo.getError().getDescription());
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				contestsDTO.setMessage("Contest API Cache data refreshed successfully.");
				contestsDTO.setStatus(1);
				return contestsDTO;
			}
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while Resetting Contest Data.");
			contestsDTO.setError(error);
			contestsDTO.setStatus(0);
		}
		return contestsDTO;
	}
		
			
	
	
	
	@RequestMapping(value = "/UpdateContest")
	public ContestsDTO updateContest(HttpServletRequest request, HttpServletResponse response, Model model){
		ContestsDTO contestsDTO = new ContestsDTO();
		Error error = new Error();
		model.addAttribute("contestsDTO", contestsDTO);
		
		try {			
			String action = request.getParameter("action");
			String contestIdStr = request.getParameter("contestId");
			String preContestIdStr = request.getParameter("preContestId");
			String contestName = request.getParameter("contestName");
			String startDateHour = request.getParameter("startDateHour");
			String startDateMinute = request.getParameter("startDateMinute");
			String contestStartDateStr = request.getParameter("contestFromDate");
			String maxTickets = request.getParameter("maxTickets");
			String contestCategory = request.getParameter("contestCategory");
			String rewardPoints = request.getParameter("rewardPoints");
			String ticketsPerWinner = request.getParameter("ticketsPerWinner");
			String contestMode = request.getParameter("contestMode");
			String gamePassword = request.getParameter("gamePassword");
			String questionSize = request.getParameter("questionSize");
			String promoOfferIdStr = request.getParameter("promoOfferId");
			String artistIdStr = request.getParameter("artistId");
			String eventIdStr = request.getParameter("eventId");
			String parentIdStr = request.getParameter("parentId");
			String childIdStr = request.getParameter("childId");
			String grandChildIdStr = request.getParameter("grandChildId");
			String selectedString = request.getParameter("artistEventCategoryName");
			String selectedPromoRefType = request.getParameter("artistEventCategoryType");
			String zone = request.getParameter("zone");
			String tixPrice = request.getParameter("tixPrice");
			String promotionalCode = request.getParameter("promotionalCode");
			String discountPercentage = request.getParameter("discountPercentage");
			String userName = request.getParameter("userName");
			String type = request.getParameter("type");
			String status = request.getParameter("status");
			String extContestName = request.getParameter("extContestName");
			String participantStar = request.getParameter("participantStar");
			String referralStar = request.getParameter("referralStar");
			String referralRewards = request.getParameter("referralRewards");
			String participantPoints = request.getParameter("participantPoints");
			String referralPoints = request.getParameter("referralPoints");
			String participantLives = request.getParameter("participantLives");
			String participantRewards = request.getParameter("participantRewards");
			
			String contestJackpotTypeStr = request.getParameter("contestJackpotType");
			//String mjpWinnerPerQuestionStr = request.getParameter("mjpWinnerPerQuestion");
			
			String giftCardIdStr = request.getParameter("giftCardId");
			String giftCardsPerWinnerStr = request.getParameter("giftCardPerWinner");
			String questionRewardType = request.getParameter("questionRewardType");
			
			
			String statuses = null;
			if(status!= null && status.equalsIgnoreCase("ALL")){
				statuses = "'ACTIVE','STARTED'";
			}else if(status!= null && status.equalsIgnoreCase("EXPIRED")){
				statuses = "'EXPIRED'";
			}
			
			if(action != null && action.equalsIgnoreCase("EDIT")){
				if(contestIdStr == null || contestIdStr.isEmpty()){
					System.err.println("Please select any Contest to Edit.");
					error.setDescription("Please select any Contest to Edit.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				Integer contestId = Integer.parseInt(contestIdStr);
				GridHeaderFilters filter = GridHeaderFiltersUtil.getContestsFilter(null); 
				List<Contests> contestsList = QuizDAORegistry.getContestsDAO().getAllContests(contestId, filter,type,null,sharedProperty);
				List<PreContestChecklist> contestChecklists = QuizDAORegistry.getContestChecklistDAO().getAllActivePreContestChecklistByType(contestId, type);
				/*if(contestsList.size() > 0 && contestsList.get(0).getStatus().equalsIgnoreCase("EXPIRED")){
					System.err.println("Not allowed to update contest, Selected Contest is EXPIRED.");
					error.setDescription("Not allowed to update contest, Selected Contest is EXPIRED.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}*/
				Integer preContestId = 0;
				for(PreContestChecklist preContest : contestChecklists){
					if(preContest.getContestId()!=null && (preContest.getContestId() == contestId || preContest.getContestId().equals(contestId))){
						preContestId = preContest.getId();
						break;
					}
				}
				contestsList.get(0).setPreContestId(preContestId);
				contestsDTO.setPreContestList(contestChecklists);
				contestsDTO.setContestsList(contestsList);
				contestsDTO.setStatus(1);
				return contestsDTO;
			}else if(action != null && (action.equalsIgnoreCase("SAVE") || action.equalsIgnoreCase("UPDATE"))){
				if(contestName==null || contestName.isEmpty()){
					System.err.println("Please provide Contest Name.");
					error.setDescription("Please provide Contest Name.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				
				if(type==null || type.isEmpty()){
					System.err.println("Please provide Contest Type.");
					error.setDescription("Please provide Contest Type.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				
				if(contestStartDateStr==null || contestStartDateStr.isEmpty()){
					System.err.println("Please provide contest start date.");
					error.setDescription("Please provide contest start date.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				if(startDateHour==null || startDateHour.isEmpty()){
					System.err.println("Please provide contest start time hours.");
					error.setDescription("Please provide contest start time hours.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				if(startDateMinute==null || startDateMinute.isEmpty()){
					System.err.println("Please provide contest start time minutes.");
					error.setDescription("Please provide contest start time minutes.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				
				if(maxTickets==null || maxTickets.isEmpty()){
					System.err.println("Please provide No of user can win tickets.");
					error.setDescription("Please provide No of user can win tickets.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				if(rewardPoints==null || rewardPoints.isEmpty()){
					System.err.println("Please provide Reward Points Prize.");
					error.setDescription("Please provide Reward Points Prize.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				if(ticketsPerWinner==null || ticketsPerWinner.isEmpty()){
					System.err.println("Please provide ticket per winner.");
					error.setDescription("Please provide ticket per winner.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				if(contestMode==null || contestMode.isEmpty()){
					System.err.println("Please provide Contest Type.");
					error.setDescription("Please provide Contest Type.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				if(contestMode.equalsIgnoreCase("PASSWORD")){
					if(gamePassword==null || gamePassword.isEmpty()){
						System.err.println("Please provide Contest password.");
						error.setDescription("Please provide Contest password.");
						contestsDTO.setError(error);
						contestsDTO.setStatus(0);
						return contestsDTO;
					}
				}else{
					gamePassword = null;
				}
				if(contestCategory==null || contestCategory.isEmpty() || contestCategory.equalsIgnoreCase("NONE")){
					System.err.println("Please provide Contest Category.");
					error.setDescription("Please provide Contest Category.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				
				if(questionSize==null || questionSize.isEmpty()){
					System.err.println("Please provide number of question.");
					error.setDescription("Please provide number of question.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				if(participantStar==null || participantStar.isEmpty()){
					System.err.println("Please provide contest participant's super fan star count, It should be Greater than or equals to 0.");
					error.setDescription("Please provide contest participant's super fan star count, It should be Greater than or equals to 0.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				if(referralStar==null || referralStar.isEmpty()){
					System.err.println("Please provide contest referral super fan star count, It should be Greater than or equals to 0.");
					error.setDescription("Please provide contest referral super fan star count, It should be Greater than or equals to 0.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				if(referralRewards==null || referralRewards.isEmpty()){
					System.err.println("Please provide special referral offer reward prize, It should be Greater than or equals to 0.");
					error.setDescription("Please provide special referral offer reward prize, It should be Greater than or equals to 0.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				Integer partPoints = 0;
				Integer referralPoint = 0;
				if(participantPoints!=null && !participantPoints.isEmpty()){
					try {
						partPoints = Integer.parseInt(participantPoints);
					} catch (Exception e) {
						System.err.println("Please provide participant points prize, It should be Greater than or equals to 0.");
						error.setDescription("Please provide participant points prize, It should be Greater than or equals to 0.");
						contestsDTO.setError(error);
						contestsDTO.setStatus(0);
						return contestsDTO;
					}
				}
				if(referralPoints!=null && !referralPoints.isEmpty()){
					try {
						referralPoint = Integer.parseInt(referralPoints);
					} catch (Exception e) {
						System.err.println("Please provide participant points prize, It should be Greater than or equals to 0.");
						error.setDescription("Please provide participant points prize, It should be Greater than or equals to 0.");
						contestsDTO.setError(error);
						contestsDTO.setStatus(0);
						return contestsDTO;
					}
				}
				if(participantLives==null || participantLives.isEmpty()){
					System.err.println("Please provide participant lives, It should be Greater than or equals to 0.");
					error.setDescription("Please provide participant lives, It should be Greater than or equals to 0.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				if(participantRewards==null || participantRewards.isEmpty()){
					System.err.println("Please provide participant rewards, It should be Greater than or equals to 0.");
					error.setDescription("Please provide participant rewards, It should be Greater than or equals to 0.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				if((artistIdStr==null || artistIdStr.isEmpty()) && (eventIdStr==null || eventIdStr.isEmpty()) &&
						(parentIdStr==null || parentIdStr.isEmpty()) && (childIdStr==null || childIdStr.isEmpty()) && 
						(grandChildIdStr==null || grandChildIdStr.isEmpty()) && (selectedPromoRefType==null || selectedPromoRefType.isEmpty())){
					System.err.println("Please select Any one Artist/Event/Parent/Child/Grand Child/All Category."); 
					error.setDescription("Please select Any one Artist/Event/Parent/Child/Grand Child/All Category.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				
				
				Integer maxTicket = null;
				Double rewardPoint = null;
				Integer ticketPerWinner = null;
				Integer questionSizes = null;
				Integer participantStars = 0;
				Integer referralStars = 0;
				Integer participantLive = 0;
				Double participantReward = 0.0;
				Double referralReward = 0.00;
				Double singleTxPrice = 0.00;
				//Boolean isMegaJackpot = Boolean.FALSE;
				//Integer mjpWinnersPerQuestion = null;
				try {
					maxTicket = Integer.parseInt(maxTickets);
					ticketPerWinner =  Integer.parseInt(ticketsPerWinner);
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("Please provide valid integer for Max. ticket winner and Ticket/winner.");
					error.setDescription("Please provide valid integer for Max. ticket winner and Ticket/winner.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				
				try {
					questionSizes = Integer.parseInt(questionSize);
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("Please provide valid integer for Number of questions.");
					error.setDescription("Please provide valid integer for Number of questions.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				
				try {
					participantStars = Integer.parseInt(participantStar);
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("Please provide valid contest participant's super fan game star count, It should be Greater than or equals to 0.");
					error.setDescription("Please provide valid contest participant's super fan fame star count, It should be Greater than or equals to 0.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				
				try {
					referralStars = Integer.parseInt(referralStar);
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("Please provide valid contest referral super fan game star count, It should be Greater than or equals to 0.");
					error.setDescription("Please provide valid contest referral super fan game star count, It should be Greater than or equals to 0.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				
				try {
					participantLive = Integer.parseInt(participantLives);
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("Please provide valid participant lives, It should be Greater than or equals to 0.");
					error.setDescription("Please provide valid participant lives, It should be Greater than or equals to 0.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				
				try {
					referralReward = Double.parseDouble(referralRewards);
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("Please provide valid special referral offer reward prize, It should be Greater than or equals to 0.");
					error.setDescription("Please provide valid special referral offer reward prize, It should be Greater than or equals to 0.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				
				try {
					participantReward = Double.parseDouble(participantRewards);
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("Please provide valid participant rewards, It should be Greater than or equals to 0.");
					error.setDescription("Please provide valid participant rewards, It should be Greater than or equals to 0.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
								
				try {
					rewardPoint =  Double.parseDouble(rewardPoints);
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("Please provide valid Decimal value for Reward Points.");
					error.setDescription("Please provide valid Decimal value for Reward Points.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				
				if(contestJackpotTypeStr==null || contestJackpotTypeStr.isEmpty()){
					System.err.println("Please Select Valid Contest Jackpot Type.");
					error.setDescription("Please Select Valid Contest Jackpot Type.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				ContestJackpotType contestJackpotType = null;
				try {
					contestJackpotType = ContestJackpotType.valueOf(contestJackpotTypeStr);
				}catch (Exception e) {
					e.printStackTrace();
					System.err.println("Please provide Valid Contest Jackpot Type.");
					error.setDescription("Please provide Valid Contest Jackpot Type.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				//if(isMegaJackpotStr==null || isMegaJackpotStr.isEmpty()){
					/*System.err.println("Please Select Valid Mega Jackpot.");
					error.setDescription("Please Select Valid Mega Jackpot.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;*/
				//	isMegaJackpotStr = "NO";
				//}
				
				/*if(isMegaJackpotStr.equals("YES")) {
					isMegaJackpot = Boolean.TRUE;
				}*/
				/*if(isMegaJackpot) {
					if(mjpWinnerPerQuestionStr==null || mjpWinnerPerQuestionStr.isEmpty()){
						System.err.println("Please Provide Mega Jackpot Winners per Question.");
						error.setDescription("Please Provide Mega Jackpot Winners per Question.");
						contestsDTO.setError(error);
						contestsDTO.setStatus(0);
						return contestsDTO;
					}
					try {
						mjpWinnersPerQuestion = Integer.parseInt(mjpWinnerPerQuestionStr);
					}catch (Exception e) {
						e.printStackTrace();
						System.err.println("Please provide valid Mega Jackpot Winners per Question.");
						error.setDescription("Please provide valid Mega Jackpot Winners per Question.");
						contestsDTO.setError(error);
						contestsDTO.setStatus(0);
						return contestsDTO;
					}
				}*/
				Integer giftCardId=null,giftCardPerWinner=null;
				if(giftCardIdStr != null && !giftCardIdStr.isEmpty()) {
					try {
						giftCardId = Integer.parseInt(giftCardIdStr);
					}catch (Exception e) {
						e.printStackTrace();
						System.err.println("Please select valid Gift Card.");
						error.setDescription("Please select valid Gift Card.");
						contestsDTO.setError(error);
						contestsDTO.setStatus(0);
						return contestsDTO;
					}
					RtfGiftCardQuantity rtfGiftCards = DAORegistry.getRtfGiftCardQuantityDAO().get(giftCardId);
					if(rtfGiftCards == null) {
						System.err.println("Please select valid gift Card. selected gift card not exists.");
						error.setDescription("Please select valid gift Card. selected gift card not exists.");
						contestsDTO.setError(error);
						contestsDTO.setStatus(0);
						return contestsDTO;
					}
					
					if(giftCardsPerWinnerStr==null || giftCardsPerWinnerStr.isEmpty()){
						System.err.println("Please Provide Gift Cards per Winner.");
						error.setDescription("Please Provide Gift Cards per Winner.");
						contestsDTO.setError(error);
						contestsDTO.setStatus(0);
						return contestsDTO;
					}
					try {
						giftCardPerWinner = Integer.parseInt(giftCardsPerWinnerStr);
					}catch (Exception e) {
						e.printStackTrace();
						System.err.println("Please provide valid Gift Cards per Winner.");
						error.setDescription("Please provide valid Gift Cards per Winner.");
						contestsDTO.setError(error);
						contestsDTO.setStatus(0);
						return contestsDTO;
					}
					
				}
				
				
				if(null != tixPrice && !tixPrice.isEmpty()){
					try {
						singleTxPrice =  Double.parseDouble(tixPrice);
					} catch (Exception e) {
						e.printStackTrace();
						System.err.println("Please provide valid Decimal value for Single ticket price.");
						error.setDescription("Please provide valid Decimal value for Single ticket price.");
						contestsDTO.setError(error);
						contestsDTO.setStatus(0);
						return contestsDTO;
					}
				}
							
				contestStartDateStr = contestStartDateStr+" "+startDateHour+":"+startDateMinute+":00";
				Date startDate = null;
				try {
					startDate = Util.getDateWithTwentyFourHourFormat1(contestStartDateStr);
				} catch (Exception e) {
					System.err.println("Please provide contest start date.");
					error.setDescription("Please provide contest start date.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				
				Date today = new Date();
				Contests contest = null;
				RTFPromoOffers offer = null;
				List<RTFPromoType> promoTypeOffers = null;

				//SimpleDateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
				Date toDate = new Date();
			    Calendar calendar = Calendar.getInstance();
			    calendar.setTime(startDate);
			    calendar.add(Calendar.HOUR, 24);		     
			    toDate = calendar.getTime();
				
				if(!StringUtils.isEmpty(promotionalCode)){
					RTFPromoOffers promoOffer = DAORegistry.getRtfPromoOffersDAO().getPromoCode(promotionalCode);
					if(action.equalsIgnoreCase("UPDATE")){
						if(promoOffer != null && (!promoOffer.getId().equals(Integer.parseInt(promoOfferIdStr)) || promoOffer.getId() != Integer.parseInt(promoOfferIdStr))){
							System.err.println("Promotional Code is already exist.");
							error.setDescription("Promotional Code is already exist.");
							contestsDTO.setError(error);
							contestsDTO.setStatus(0);
							return contestsDTO;
						}
					}else{
						if(promoOffer != null){
							System.err.println("Promotional Code is already exist.");
							error.setDescription("Promotional Code is already exist.");
							contestsDTO.setError(error);
							contestsDTO.setStatus(0);
							return contestsDTO;
						}
					}
				}
				
				if(action.equalsIgnoreCase("UPDATE")){
					
					if(contestIdStr == null || contestIdStr.isEmpty()){
						System.err.println("Please select any Contest to Update.");
						error.setDescription("Please select any Contest to Update.");
						contestsDTO.setError(error);
						contestsDTO.setStatus(0);
						return contestsDTO;
					}
					contest = QuizDAORegistry.getContestsDAO().get(Integer.parseInt(contestIdStr));
					if(contest == null){
						System.err.println("Contest is not found.");
						error.setDescription("Contest is not found.");
						contestsDTO.setError(error);
						contestsDTO.setStatus(0);
						return contestsDTO;
					}
					/*if(isMegaJackpot) {
						List<ContestQuestions> questions = QuizDAORegistry.getContestQuestionsDAO().getMiniJackpotQuestionsByContestId(contest.getId());
						if(questions != null && !questions.isEmpty()) {
							System.err.println("Mini Jackpot already created for this contest.");
							error.setDescription("Mini Jackpot already created for this contest.");
							contestsDTO.setError(error);
							contestsDTO.setStatus(0);
							return contestsDTO;
						}
					}*/
					
				}
				if(contest == null){
					contest = new Contests();
				}
				contest.setContestName(contestName);
				contest.setStartDateTime(startDate);
				contest.setFreeTicketPerWinner(ticketPerWinner);
				contest.setExtendedName(extContestName);
				contest.setRewardPoints(rewardPoint);
				contest.setContestMode(contestMode);
				contest.setContestPassword(gamePassword);
				contest.setQuestionSize(questionSizes);
				contest.setTicketWinnerThreshhold(maxTicket);
				contest.setContestCategory(contestCategory);
				contest.setQuestionRewardType(questionRewardType);
				contest.setIsTestContest(false);
				contest.setParticipantStars(participantStars);
				contest.setReferralStars(referralStars);
				contest.setParticipantLives(participantLive);
				contest.setParticipantRewards(participantReward);
				contest.setReferralRewards(referralReward);
				contest.setSingleTicketPrice(singleTxPrice);
				contest.setZone(zone);
				contest.setType(type);
				contest.setIsCustomerStatUpdated(false);
				contest.setContestJackpotType(contestJackpotType);
				contest.setParticipantPoints(partPoints);
				contest.setReferralPoints(referralPoint);
				//contest.setIsMegaJackpot(isMegaJackpot);
				//contest.setMjpWinnerPerQuestion(mjpWinnersPerQuestion);
				contest.setGiftCardId(giftCardId);
				contest.setGiftCardPerWinner(giftCardPerWinner);
				
				if((artistIdStr!=null && !artistIdStr.isEmpty()) || (eventIdStr!=null && !eventIdStr.isEmpty()) ||
						(parentIdStr!=null && !parentIdStr.isEmpty()) || (childIdStr!=null && !childIdStr.isEmpty()) || 
						(grandChildIdStr!=null && !grandChildIdStr.isEmpty()) || (selectedPromoRefType!=null && !selectedPromoRefType.isEmpty())){
					if(selectedPromoRefType.equalsIgnoreCase("All")){
						contest.setPromoRefId(0);
					}else if(artistIdStr!=null && !artistIdStr.isEmpty() && selectedPromoRefType.equalsIgnoreCase("ARTIST")){
						contest.setPromoRefId(Integer.parseInt(artistIdStr));
					}else if(eventIdStr!=null && !eventIdStr.isEmpty() && selectedPromoRefType.equalsIgnoreCase("EVENT")){
						contest.setPromoRefId(Integer.parseInt(eventIdStr));
					}else if(grandChildIdStr!=null && !grandChildIdStr.isEmpty() && selectedPromoRefType.equalsIgnoreCase("GRAND")){
						contest.setPromoRefId(Integer.parseInt(grandChildIdStr));
					}else if(childIdStr!=null && !childIdStr.isEmpty() && selectedPromoRefType.equalsIgnoreCase("CHILD")){
						contest.setPromoRefId(Integer.parseInt(childIdStr));
					}else if(parentIdStr!=null && !parentIdStr.isEmpty() && selectedPromoRefType.equalsIgnoreCase("PARENT")){
						contest.setPromoRefId(Integer.parseInt(parentIdStr));
					}
					contest.setPromoRefName(selectedString);
					contest.setPromoRefType(selectedPromoRefType);
					contest.setPromoExpiryDate(toDate);
				}
				 //Promotional Code
				if(!StringUtils.isEmpty(promotionalCode)){
					if(promoOfferIdStr != null && !promoOfferIdStr.isEmpty()){
						Integer promoId = Integer.parseInt(promoOfferIdStr);
						offer = DAORegistry.getRtfPromoOffersDAO().get(promoId);
						promoTypeOffers = DAORegistry.getRtfPromoTypeDAO().getPromoTypeFromOfferId(promoId);
					}else{
						offer = new RTFPromoOffers();
					}
					contest.setPromotionalCode(promotionalCode);
					contest.setDiscountPercentage(Double.parseDouble(discountPercentage));
			    	
					offer.setStartDate(startDate);
					offer.setEndDate(toDate);
					offer.setMaxOrders(1000);
					offer.setCreatedBy(userName);
					offer.setCreatedDate(today);
					offer.setDiscountPer(Double.parseDouble(discountPercentage));
					offer.setMobileDiscountPerc(Double.parseDouble(discountPercentage));
					offer.setModifiedBy(userName);
					offer.setModifiedDate(today);
					offer.setNoOfOrders(0);				
					offer.setPromoCode(promotionalCode);
					
					offer.setFlatOfferOrderThreshold(0.00);	//Order Threshold				
					offer.setStatus("ENABLED");
					offer.setIsFlatDiscount(false);
					DAORegistry.getRtfPromoOffersDAO().saveOrUpdate(offer);
					
					
					if(promoTypeOffers != null && promoTypeOffers.size() > 0){
						DAORegistry.getRtfPromoTypeDAO().deleteAll(promoTypeOffers);
					}
					RTFPromoType promoTypeOffer = new RTFPromoType();
					
					if((artistIdStr!=null && !artistIdStr.isEmpty()) || (eventIdStr!=null && !eventIdStr.isEmpty()) ||
							(parentIdStr!=null && !parentIdStr.isEmpty()) || (childIdStr!=null && !childIdStr.isEmpty()) || 
							(grandChildIdStr!=null && !grandChildIdStr.isEmpty()) || (selectedPromoRefType!=null && !selectedPromoRefType.isEmpty())){
						promoTypeOffer.setPromoOfferId(offer.getId());
						if(selectedPromoRefType.equalsIgnoreCase("All")){
							promoTypeOffer.setPromoType(PromotionalType.ALL.toString());
						}else if(artistIdStr!=null && !artistIdStr.isEmpty() && selectedPromoRefType.equalsIgnoreCase("ARTIST")){
							promoTypeOffer.setArtistId(Integer.parseInt(artistIdStr));
							promoTypeOffer.setPromoType(PromotionalType.ARTIST.toString());
						}else if(eventIdStr!=null && !eventIdStr.isEmpty() && selectedPromoRefType.equalsIgnoreCase("EVENT")){
							promoTypeOffer.setEventId(Integer.parseInt(eventIdStr));
							promoTypeOffer.setPromoType(PromotionalType.EVENT.toString());
						}else if(grandChildIdStr!=null && !grandChildIdStr.isEmpty() && selectedPromoRefType.equalsIgnoreCase("GRAND")){
							promoTypeOffer.setGrandChildId(Integer.parseInt(grandChildIdStr));
							promoTypeOffer.setPromoType(PromotionalType.GRANDCHILD.toString());
						}else if(childIdStr!=null && !childIdStr.isEmpty() && selectedPromoRefType.equalsIgnoreCase("CHILD")){
							promoTypeOffer.setChildId(Integer.parseInt(childIdStr));
							promoTypeOffer.setPromoType(PromotionalType.CHILD.toString());
						}else if(parentIdStr!=null && !parentIdStr.isEmpty() && selectedPromoRefType.equalsIgnoreCase("PARENT")){
							promoTypeOffer.setParentId(Integer.parseInt(parentIdStr));
							promoTypeOffer.setPromoType(PromotionalType.PARENT.toString());
						}
						
						DAORegistry.getRtfPromoTypeDAO().save(promoTypeOffer);
					}
					contest.setRtfPromoOfferId(offer.getId());
				}else if(StringUtils.isEmpty(promotionalCode)){
					if(promoOfferIdStr != null && !promoOfferIdStr.isEmpty()){
						Integer promoId = Integer.parseInt(promoOfferIdStr);
						offer = DAORegistry.getRtfPromoOffersDAO().get(promoId);
						promoTypeOffers = DAORegistry.getRtfPromoTypeDAO().getPromoTypeFromOfferId(promoId);
						
						
					}
					contest.setPromotionalCode(null);
					contest.setDiscountPercentage(null);
					contest.setRtfPromoOfferId(null);
				}
				
				
				
				if(action.equalsIgnoreCase("SAVE")){
					contest.setCreatedBy(userName);
					contest.setCreatedDate(today);
					contest.setStatus("ACTIVE");
				}
				if(action.equalsIgnoreCase("UPDATE")){
					contest.setUpdatedBy(userName);
					contest.setUpdatedDate(today);
					contest.setStatus("ACTIVE");
				}
				QuizDAORegistry.getContestsDAO().saveOrUpdate(contest);
				
				//Promotional Code
				if(preContestIdStr!=null && !preContestIdStr.isEmpty()){
					Integer preContestId = Integer.parseInt(preContestIdStr);
					PreContestChecklist preContest = QuizDAORegistry.getContestChecklistDAO().getPreContestChecklistByContestId(contest.getId());
					if(preContest!=null && preContest.getId() != preContestId){
						preContest.setContestId(null);
						QuizDAORegistry.getContestChecklistDAO().update(preContest);
					}
					PreContestChecklist preContest1 = QuizDAORegistry.getContestChecklistDAO().get(preContestId);
					if(preContest1!=null){
						preContest1.setContestId(contest.getId());
						QuizDAORegistry.getContestChecklistDAO().update(preContest1);
					}
				}
				    
				
				if(action.equalsIgnoreCase("SAVE")){
					contestsDTO.setMessage("Contest Created successfully.");
				}
				if(action.equalsIgnoreCase("UPDATE")){
					contestsDTO.setMessage("Contest Updated successfully.");
				}
				
				Map<String, String> map1 = com.rtw.tmat.utils.Util.getParameterMap(request);
				map1.put("cType",contest.getType());
				String data = com.rtw.tmat.utils.Util.getObject(map1,Constants.BASE_URL+Constants.REFRESH_CONTEST);
				
				GridHeaderFilters filter = GridHeaderFiltersUtil.getContestsFilter(null); 
				List<Contests> contestsList = QuizDAORegistry.getContestsDAO().getAllContests(null, filter,type,statuses,sharedProperty);
				
				//return contestsDTO
				contestsDTO.setStatus(1);			
				contestsDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(contestsList!=null?contestsList.size():0));				
				contestsDTO.setContestsList(contestsList);
				return contestsDTO;
			}else if(action != null && action.equalsIgnoreCase("DELETE")){
				Contests contest =  null;
				if(contestIdStr == null || contestIdStr.isEmpty()){
					System.err.println("Please select any Contest to Delete.");
					error.setDescription("Please select any Contest to Delete.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				contest = QuizDAORegistry.getContestsDAO().get(Integer.parseInt(contestIdStr));
				if(contest == null){
					System.err.println("Contest is not found.");
					error.setDescription("Contest is not found.");
					contestsDTO.setError(error);
					contestsDTO.setStatus(0);
					return contestsDTO;
				}
				if(promoOfferIdStr!=null && !promoOfferIdStr.isEmpty() && !promoOfferIdStr.equalsIgnoreCase("undefined")
					&& !promoOfferIdStr.equalsIgnoreCase("null")){
					Integer promoId = Integer.parseInt(promoOfferIdStr);
					
					RTFPromoOffers offer = DAORegistry.getRtfPromoOffersDAO().get(promoId);
					if(offer == null){
						System.err.println("Please select Contest With PromoCode to Delete.");
						error.setDescription("Please select Contest With PromoCode to Delete.");
						contestsDTO.setError(error);
						contestsDTO.setStatus(0);
						return contestsDTO;
					}
					offer.setStatus("DISABLED");
					offer.setModifiedBy(userName);
					offer.setModifiedDate(new Date());
					DAORegistry.getRtfPromoOffersDAO().update(offer);
				}
				
				
				contest.setUpdatedBy(userName);
				contest.setUpdatedDate(new Date());
				contest.setStatus("DELETED");
				QuizDAORegistry.getContestsDAO().update(contest);
				
				PreContestChecklist preContest = QuizDAORegistry.getContestChecklistDAO().getPreContestChecklistByContestId(contest.getId());
				if(preContest!=null){
					preContest.setContestId(null);
					if(contest.getStartDateTime().before(new Date())){
						preContest.setStatus("EXPIRED");
					}
					QuizDAORegistry.getContestChecklistDAO().update(preContest);
				}
				Map<String, String> map1 = com.rtw.tmat.utils.Util.getParameterMap(request);
				map1.put("cType","MOBILE");
				String data = com.rtw.tmat.utils.Util.getObject(map1,Constants.BASE_URL+Constants.REFRESH_CONTEST);
				
				GridHeaderFilters filter = GridHeaderFiltersUtil.getContestsFilter(null); 
				List<Contests> contestsList = QuizDAORegistry.getContestsDAO().getAllContests(null, filter,type,statuses,sharedProperty);
				contestsDTO.setMessage("Contest Deleted successfully.");
				
				//return contestsDTO
				contestsDTO.setStatus(1);			
				contestsDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(contestsList!=null?contestsList.size():0));				
				contestsDTO.setContestsList(contestsList);
				return contestsDTO;	
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return contestsDTO;
	}
	
	
	@RequestMapping(value = "/ContestQuestions")
	public ContestQuestionDTO getContestsQuestions(HttpServletRequest request, HttpServletResponse response){
		ContestQuestionDTO questionDTO = new ContestQuestionDTO();
		Error error = new Error();
		try{			
			String headerFilter = request.getParameter("headerFilter");
			String contestIdStr = request.getParameter("contestId");
				
			if(contestIdStr == null || contestIdStr.trim().isEmpty()){
				questionDTO.setStatus(0);
				questionDTO.setMessage("Not able to identfy contest, pelase send valid contest id to load questions.");
				return questionDTO;
			}
			
			Integer contestId = null;
			try {
				contestId = Integer.parseInt(contestIdStr);
			} catch (Exception e) {
				questionDTO.setStatus(0);
				questionDTO.setMessage("Invalid contestId, Please send valid ContestId.");
				return questionDTO;
			}
			
			Integer count = 0;
			System.out.println("CT- GETQUESTIONS FILTERS: Begins -> "+new Date());
			GridHeaderFilters filter = GridHeaderFiltersUtil.getContestQuestionFilter(headerFilter);
			System.out.println("CT- GETQUESTIONS FILTERS: Ends -> "+new Date());
			
			System.out.println("CT- GETQUESTIONS SQL: Begins -> "+new Date());
			List<ContestQuestions> questionList = QuizDAORegistry.getContestQuestionsDAO().getContestQuestions(filter, null,contestId,sharedProperty);
			System.out.println("CT- GETQUESTIONS SQL: Ends -> "+new Date());
			
			if(questionList != null && questionList.size() > 0){
				count = questionList.size();
			}
			if(questionList == null || questionList.size() <= 0){
				questionDTO.setMessage("No Questions found for selected Contest.");
			}
			
			questionDTO.setStatus(1);			
			questionDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(count));
			questionDTO.setQuestionList(questionList);
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while loading contest questions.");
			questionDTO.setError(error);
			questionDTO.setStatus(0);
		}		
		return questionDTO;
	}
	
	
	
	@RequestMapping(value = "/UpdateQuestion")
	public ContestQuestionDTO updateQuestion(HttpServletRequest request, HttpServletResponse response){
		ContestQuestionDTO questionDTO = new ContestQuestionDTO();
		Error error = new Error();
		try {			
			String action = request.getParameter("action");
			String contestIdStr = request.getParameter("contestId");
			String questionIdStr = request.getParameter("questionId");
			String questionText = request.getParameter("questionText");
			String optionA = request.getParameter("optionA");
			String optionB = request.getParameter("optionB");
			String optionC = request.getParameter("optionC");
			//String optionD = request.getParameter("optionD");
			String difficultyLevel = request.getParameter("difficultyLevel");
			String questionReward = request.getParameter("questionReward");
			String answer = request.getParameter("answer");
			String userName = request.getParameter("userName");
			
			String miniJackpotTypeStr = request.getParameter("miniJackpotType");
			String mjpNoOfWinnerStr = request.getParameter("mjpNoOfWinner");
			String mjpQtyPerWinnerStr = request.getParameter("mjpQtyPerWinner");
			String mjpGiftCardIdStr = request.getParameter("mjpGiftCardId");
			
			if(action != null && action.equalsIgnoreCase("EDIT")){
				if(questionIdStr == null || questionIdStr.isEmpty()){
					System.err.println("Please select any Question to Edit.");
					error.setDescription("Please select any Question to Edit.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				
				Integer questionId = null;
				try {
					questionId = Integer.parseInt(questionIdStr);
				} catch (Exception e) {
					System.err.println("Invalid QuestionId found, Please send valid questioId.");
					error.setDescription("Invalid QuestionId found, Please send valid questioId.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				} 
				
				GridHeaderFilters filter = GridHeaderFiltersUtil.getContestQuestionFilter(null); 
				List<ContestQuestions> questionList = QuizDAORegistry.getContestQuestionsDAO().getContestQuestions(filter, questionId, null,sharedProperty);
				if(questionList.size() == 0){
					System.err.println("Selected question is not found in system.");
					error.setDescription("Selected question is not found in system.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				
				questionDTO.setQuestionList(questionList);
				questionDTO.setStatus(1);
				return questionDTO;
			}else if(action != null && (action.equalsIgnoreCase("SAVE") || action.equalsIgnoreCase("UPDATE"))){
				
				if(questionText==null || questionText.isEmpty()){
					System.err.println("Please provide Question Text.");
					error.setDescription("Please provide Question Text.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				if(difficultyLevel==null || difficultyLevel.isEmpty()){
					System.err.println("Please provide Question difficulty Level.");
					error.setDescription("Please provide Question difficulty Level.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				
				if(questionReward==null || questionReward.isEmpty()){
					System.err.println("Please provide question reward.");
					error.setDescription("Please provide question reward.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				Double questionRewards = null;
				try {
					questionRewards = Double.parseDouble(questionReward);
					if(questionRewards > 0){
						questionRewards = com.rtw.tmat.utils.Util.roundOffDouble(questionRewards);
					}
				} catch (Exception e) {
					System.err.println("Please provide valid question reward.");
					error.setDescription("Please provide valid question reward.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				
				if(optionA==null || optionA.isEmpty()){
					System.err.println("Please provide option A text.");
					error.setDescription("Please provide option A text.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				if(optionB==null || optionB.isEmpty()){
					System.err.println("Please provide option B text.");
					error.setDescription("Please provide option B text.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				if(optionC==null || optionC.isEmpty()){
					System.err.println("Please provide option C text.");
					error.setDescription("Please provide option C text.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				/*if(optionD==null || optionD.isEmpty()){
					System.err.println("Please provide option D text.");
					error.setDescription("Please provide option D text.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}*/
				if(answer==null || answer.isEmpty()){
					System.err.println("Please provide answer text.");
					error.setDescription("Please provide answer text.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				if(contestIdStr==null || contestIdStr.isEmpty()){
					System.err.println("Please provide contestId.");
					error.setDescription("Please provide contestId.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				Integer contestId = null;
				try {
					contestId = Integer.parseInt(contestIdStr);
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("Please provide valid contestId.");
					error.setDescription("Please provide valid contestId.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}

				Integer mjpGiftCardId = null,mjpNoOfWinner = null;
				Double mjpQtyPerWinner = null;
				MiniJackpotType miniJackpotType = null;
				
				Contests contest = QuizDAORegistry.getContestsDAO().get(contestId);
				if(contest.getContestJackpotType() != null && !contest.getContestJackpotType().equals(ContestJackpotType.NORMAL)) {
					
				
					if(miniJackpotTypeStr!=null && !miniJackpotTypeStr.isEmpty() && !miniJackpotTypeStr.equals("NONE")){
					
						try {
							miniJackpotType = MiniJackpotType.valueOf(miniJackpotTypeStr);
							
						} catch (Exception e) {
							e.printStackTrace();
							System.err.println("Please provide valid Jackpot Type.");
							error.setDescription("Please provide valid Jackpot Type.");
							questionDTO.setError(error);
							questionDTO.setStatus(0);
							return questionDTO;
						}
						/*Contests contest = QuizDAORegistry.getContestsDAO().get(contestId);
						if(contest != null && contest.getIsMegaJackpot()) {
							System.err.println("Mega Jackpot already Created for this Contest.");
							error.setDescription("Mega Jackpot already Created for this Contest.");
							questionDTO.setError(error);
							questionDTO.setStatus(0);
							return questionDTO;
						}*/
						
						if(miniJackpotType.equals(MiniJackpotType.GIFTCARD)) {
							if(mjpGiftCardIdStr==null || mjpGiftCardIdStr.isEmpty()){
								System.err.println("Please provide Jackpot Gift Card.");
								error.setDescription("Please provide Jackpot Gift Card.");
								questionDTO.setError(error);
								questionDTO.setStatus(0);
								return questionDTO;
							}
							try {
								mjpGiftCardId = Integer.parseInt(mjpGiftCardIdStr);
								
							} catch (Exception e) {
								e.printStackTrace();
								System.err.println("Please provide valid Jackpot Gift Card.");
								error.setDescription("Please provide valid Jackpot Gift Card.");
								questionDTO.setError(error);
								questionDTO.setStatus(0);
								return questionDTO;
							}
							//RtfGiftCard rtfGiftCard = DAORegistry.getRtfGiftCardDAO().getActiveRtfGiftCardById(mjpGiftCardId);
							RtfGiftCardQuantity rtfGiftCards = DAORegistry.getRtfGiftCardQuantityDAO().get(mjpGiftCardId);
							if(rtfGiftCards == null) {
								System.err.println("Please Select Valid Jackpot Gift Card.");
								error.setDescription("Please Select Valid Jackpot Gift Card.");
								questionDTO.setError(error);
								questionDTO.setStatus(0);
								return questionDTO;
							}
						}
						if(mjpNoOfWinnerStr==null || mjpNoOfWinnerStr.isEmpty()){
							System.err.println("Please provide Jackpot No of Winner.");
							error.setDescription("Please provide Jackpot No of Winner.");
							questionDTO.setError(error);
							questionDTO.setStatus(0);
							return questionDTO;
						}
						try {
							mjpNoOfWinner = Integer.parseInt(mjpNoOfWinnerStr);
							
						} catch (Exception e) {
							e.printStackTrace();
							System.err.println("Please provide valid Jackpot No of Winner.");
							error.setDescription("Please provide valid Jackpot No of Winner.");
							questionDTO.setError(error);
							questionDTO.setStatus(0);
							return questionDTO;
						}
						if(mjpQtyPerWinnerStr==null || mjpQtyPerWinnerStr.isEmpty()){
							System.err.println("Please provide Jackpot "+miniJackpotTypeStr+" Per Winner.");
							error.setDescription("Please provide Jackpot "+miniJackpotTypeStr+"  Per Winner.");
							questionDTO.setError(error);
							questionDTO.setStatus(0);
							return questionDTO;
						}
						try {
							mjpQtyPerWinner = Double.parseDouble(mjpQtyPerWinnerStr);
							
						} catch (Exception e) {
							e.printStackTrace();
							System.err.println("Please provide valid Jackpot "+miniJackpotTypeStr+" Per Winner.");
							error.setDescription("Please provide valid Jackpot "+miniJackpotTypeStr+" Per Winner.");
							questionDTO.setError(error);
							questionDTO.setStatus(0);
							return questionDTO;
						}
					} /*else {
						GridHeaderFilters filter = GridHeaderFiltersUtil.getContestsFilter(null); 
						List<ContestQuestions> contestQuestionList = QuizDAORegistry.getContestQuestionsDAO().getContestQuestions(filter, null, contestId,sharedProperty);
						int qSize = 0;
						if(contestQuestionList != null) {
							qSize = contestQuestionList.size();
						}
						if(contest.getContestJackpotType().equals(ContestJackpotType.MEGA) && ((contest.getQuestionSize()-qSize) > 1 )) {
							System.err.println("Please Select Jackpot Type for all questions except last question for Mega JAckpot contest.");
							error.setDescription("Please Select Jackpot Type for all questions except last question for Mega JAckpot contest.");
							questionDTO.setError(error);
							questionDTO.setStatus(0);
							return questionDTO;
						}
					}*/
				}
				
				
				Date today = new Date();
				ContestQuestions question = null;
				
				if(action.equalsIgnoreCase("UPDATE")){
					
					if(questionIdStr == null || questionIdStr.isEmpty()){
						System.err.println("Please select any Question to Update.");
						error.setDescription("Please select any Question to Update.");
						questionDTO.setError(error);
						questionDTO.setStatus(0);
						return questionDTO;
					}
					question = QuizDAORegistry.getContestQuestionsDAO().get(Integer.parseInt(questionIdStr));
					if(question == null){
						System.err.println("Question is not found.");
						error.setDescription("Question is not found.");
						questionDTO.setError(error);
						questionDTO.setStatus(0);
						return questionDTO;
					}
				}
				if(question == null){
					/*ContestQuestions cQuestion = QuizDAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId,questionSrNo);
					if(cQuestion!=null){
						System.err.println("Question with same serial number found in system, only unique serial number questions are allowed for one contest.");
						error.setDescription("Question with same serial number found in system, only unique serial number questions are allowed for one contest.");
						questionDTO.setError(error);
						questionDTO.setStatus(0);
						return questionDTO;
					}*/
					question = new ContestQuestions();
				}
				if(action.equalsIgnoreCase("SAVE")){
					question.setCreatedBy(userName);
					question.setCreatedDate(today);
					question.setSerialNo(1000);
				}
				if(action.equalsIgnoreCase("UPDATE")){
					/*ContestQuestions cQuestion = QuizDAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId,questionSrNo);
					if(cQuestion == null || cQuestion.getId() == question.getId() || cQuestion.getId().equals(question.getId())){
						
					}else{
						System.err.println("Question with same serial number found in system, only unique serial number questions are allowed for one contest.");
						error.setDescription("Question with same serial number found in system, only unique serial number questions are allowed for one contest.");
						questionDTO.setError(error);
						questionDTO.setStatus(0);
						return questionDTO;
					}*/
					question.setUpdatedBy(userName);
					question.setUpdatedDate(today);
				}
				
				question.setAnswer(answer);
				question.setContestId(contestId);
				question.setOptionA(optionA);
				question.setOptionB(optionB);
				question.setOptionC(optionC);
				question.setQuestionReward(questionRewards);
				question.setDifficultyLevel(difficultyLevel);
				question.setQuestion(questionText);
				//question.setSerialNo(questionSrNo);
				question.setMiniJackpotType(miniJackpotType);
				question.setMjpNoOfWinner(mjpNoOfWinner);
				question.setMjpQtyPerWinner(mjpQtyPerWinner);
				question.setMjpGiftCardId(mjpGiftCardId);
				
				QuizDAORegistry.getContestQuestionsDAO().saveOrUpdate(question);
				List<ContestQuestions> contestQuestionList = null;
				GridHeaderFilters filter = GridHeaderFiltersUtil.getContestsFilter(null); 
				contestQuestionList = QuizDAORegistry.getContestQuestionsDAO().getContestQuestions(filter, null, contestId,sharedProperty);
				if(action.equalsIgnoreCase("SAVE")){
					questionDTO.setMessage("Question Created successfully.");
					if(!contestQuestionList.isEmpty()){
						int i=1;
						for(ContestQuestions q : contestQuestionList){
							q.setSerialNo(i);
							i++;
						}
						QuizDAORegistry.getContestQuestionsDAO().updateAll(contestQuestionList);
					}
				}
				if(action.equalsIgnoreCase("UPDATE")){
					questionDTO.setMessage("Question Updated successfully.");
				}
				questionDTO.setStatus(1);			
				questionDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(contestQuestionList!=null?contestQuestionList.size():0));				
				questionDTO.setQuestionList(contestQuestionList);
				return questionDTO;
			}else if(action != null && action.equalsIgnoreCase("DELETE")){
				ContestQuestions contestQuestion =  null;
				if(questionIdStr == null || questionIdStr.isEmpty()){
					System.err.println("Please select any Question to Delete.");
					error.setDescription("Please select any Question to Delete.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				if(contestIdStr == null || contestIdStr.isEmpty()){
					System.err.println("ContestId is not found.");
					error.setDescription("ContestId is not found.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				contestQuestion = QuizDAORegistry.getContestQuestionsDAO().get(Integer.parseInt(questionIdStr));
				if(contestQuestion == null){
					System.err.println("Question is not found.");
					error.setDescription("Question is not found.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				QuizDAORegistry.getContestQuestionsDAO().delete(contestQuestion);
				
				GridHeaderFilters filter = GridHeaderFiltersUtil.getContestsFilter(null); 
				List<ContestQuestions> list = QuizDAORegistry.getContestQuestionsDAO().getContestQuestions(filter, null, Integer.parseInt(contestIdStr),sharedProperty);
				if(!list.isEmpty()){
					int i=1;
					for(ContestQuestions q : list){
						q.setSerialNo(i);
						i++;
					}
					QuizDAORegistry.getContestQuestionsDAO().updateAll(list);
				}
				questionDTO.setMessage("Question Deleted successfully.");
				questionDTO.setStatus(1);			
				questionDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(list!=null?list.size():0));				
				questionDTO.setQuestionList(list);
				return questionDTO;	
			}else if(action != null && action.equalsIgnoreCase("ADDNEW")){
				
				String questionBankIdsStr = request.getParameter("questionBankIds");
				
				Integer contestId = null;
				try {
					contestId = Integer.parseInt(contestIdStr);
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("Please provide valid contestId.");
					error.setDescription("Please provide valid contestId.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				
				if(questionBankIdsStr == null && questionBankIdsStr.isEmpty()){
					System.err.println("Please select valid Question(s).");
					error.setDescription("Please select valid Question(s).");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				
				int usedQuestionCount = QuizDAORegistry.getQuestionBankDAO().getUsedQuestionCount(questionBankIdsStr);
				
				if(usedQuestionCount  > 0 ){
					System.err.println("opps it's looks like somebody already used some of the selected questions , Please refresh grid and try again ");
					error.setDescription("opps it's looks like somebody already used some of the selected questions , Please refresh grid and try again ");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}	
				Contests contest = QuizDAORegistry.getContestsDAO().get(contestId);
				Integer contestQuestionCount = QuizDAORegistry.getContestQuestionsDAO().getQuestionCount(contestId);
				
				Date today = new Date();
				List<ContestQuestions> contestQuestionList = new ArrayList<ContestQuestions>();
				ContestQuestions contestQuestion = null;
				List<QuestionBank> questionBankList = new ArrayList<QuestionBank>();
				QuestionBank questionBank = null;
				
				if(questionBankIdsStr != null && !questionBankIdsStr.isEmpty()){
					String arr[] = questionBankIdsStr.split(",");
					Integer noOfQuestionsCount = arr.length + contestQuestionCount;
					
					if(contest.getQuestionSize() < noOfQuestionsCount){
						System.err.println("Contest has declared question size "+contest.getQuestionSize()+" you can add only "+contest.getQuestionSize()+" questions.");
						error.setDescription("Contest has declared question size "+contest.getQuestionSize()+" you can add only "+contest.getQuestionSize()+" questions.");
						questionDTO.setError(error);
						questionDTO.setStatus(0);
						return questionDTO;
					}
					if(arr.length > 0){
						for(String questionBankId : arr){
							questionBank = QuizDAORegistry.getQuestionBankDAO().get(Integer.parseInt(questionBankId));
							
							contestQuestion = QuizDAORegistry.getContestQuestionsDAO().getContestQuestionByQuestion(contestId, questionBank.getQuestion());
							if(contestQuestion == null){
								contestQuestion = new ContestQuestions();
								contestQuestion.setQuestion(questionBank.getQuestion());
								contestQuestion.setOptionA(questionBank.getOptionA());
								contestQuestion.setOptionB(questionBank.getOptionB());
								contestQuestion.setOptionC(questionBank.getOptionC());
								contestQuestion.setDifficultyLevel(questionBank.getDificultyLevel());
								contestQuestion.setAnswer(questionBank.getAnswer());
								contestQuestion.setQuestionReward(questionBank.getQuestionReward());
								contestQuestion.setContestId(contestId);
								contestQuestion.setCreatedBy(userName);
								contestQuestion.setCreatedDate(today);
								
								contestQuestionList.add(contestQuestion);
								
								//Question Bank Status - Update
								questionBank.setStatus("USED");
								questionBank.setUpdatedBy(userName);
								questionBank.setUpdatedDate(today);
								questionBankList.add(questionBank);
							}
						}
						
						QuizDAORegistry.getContestQuestionsDAO().saveAll(contestQuestionList);
						QuizDAORegistry.getQuestionBankDAO().updateAll(questionBankList);
						
						questionDTO.setMessage("Question(s) Created Successfully."); 
					}
				}
				
				GridHeaderFilters filter = GridHeaderFiltersUtil.getContestsFilter(null); 
				List<ContestQuestions> newContestQuestionList = QuizDAORegistry.getContestQuestionsDAO().getContestQuestions(filter, null, contestId,sharedProperty);
				if(!newContestQuestionList.isEmpty()){
					int i=1;
					for(ContestQuestions q : newContestQuestionList){
						q.setSerialNo(i);
						i++;
					}
					QuizDAORegistry.getContestQuestionsDAO().updateAll(newContestQuestionList);
				}
				questionDTO.setStatus(1);			
				questionDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(newContestQuestionList!=null?newContestQuestionList.size():0));				
				questionDTO.setQuestionList(newContestQuestionList);
				return questionDTO;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			questionDTO.setStatus(0);			
			System.err.println("Error occured while updating contest question.");
			error.setDescription("Error occured while updating contest question.");
			return questionDTO;
		}
		return questionDTO;
	}
	
	
	@RequestMapping(value = "/UpdateQuestionRewards")
	public ContestQuestionDTO updateQuestionRewards(HttpServletRequest request, HttpServletResponse response){
		ContestQuestionDTO questionDTO = new ContestQuestionDTO();
		Error error = new Error();
		try {			
			String contestIdStr = request.getParameter("contestId");
			String userName = request.getParameter("userName");
			String sizeStr = request.getParameter("size");
				
			if(contestIdStr==null || contestIdStr.isEmpty()){
				System.err.println("Please provide Contest Id.");
				error.setDescription("Please provide Contest Id.");
				questionDTO.setError(error);
				questionDTO.setStatus(0);
				return questionDTO;
			}
			
			if(sizeStr==null || sizeStr.isEmpty()){
				System.err.println("Please provide size of question.");
				error.setDescription("Please provide size of question.");
				questionDTO.setError(error);
				questionDTO.setStatus(0);
				return questionDTO;
			}
			Double size = null;
			try {
				size = Double.parseDouble(sizeStr);
			} catch (Exception e) {
				System.err.println("Please provide valid size of question.");
				error.setDescription("Please provide valid size of question.");
				questionDTO.setError(error);
				questionDTO.setStatus(0);
				return questionDTO;
			}
			
			Integer contestId = null;
			try {
				contestId = Integer.parseInt(contestIdStr);
			} catch (Exception e) {
				e.printStackTrace();
				System.err.println("Please provide valid contestId.");
				error.setDescription("Please provide valid contestId.");
				questionDTO.setError(error);
				questionDTO.setStatus(0);
				return questionDTO;
			}
			
			Date today = new Date();
			List<ContestQuestions> questions = QuizDAORegistry.getContestQuestionsDAO().getContestQuestions(new GridHeaderFilters(), null, contestId,sharedProperty);
			if(questions.size() != size){
				System.err.println("Actual no. of question not matching with provided question size.");
				error.setDescription("Actual no. of question not matching with provided question size.");
				questionDTO.setError(error);
				questionDTO.setStatus(0);
				return questionDTO;
			}
			for(int i=0;i<size;i++){
				String reward = request.getParameter("questionReward_"+i);
				if(reward==null || reward.isEmpty()){
					System.err.println("Please provide reward for question no :"+i+1);
					error.setDescription("Please provide reward for question no :"+i+1);
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				Double rewardPoints = null;
				try {
					rewardPoints = Double.parseDouble(reward);
					rewardPoints = com.rtw.tmat.utils.Util.roundOffDouble(rewardPoints);
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("Please provide valid reward for question no :"+i+1);
					error.setDescription("Please provide valid reward for question no :"+i+1);
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				questions.get(i).setQuestionReward(rewardPoints);
				questions.get(i).setUpdatedBy(userName);
				questions.get(i).setUpdatedDate(today);
			}
			QuizDAORegistry.getContestQuestionsDAO().updateAll(questions);
			questionDTO.setMessage("Question rewards Updated successfully.");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getContestsFilter(null); 
			List<ContestQuestions> contestQuestionList = QuizDAORegistry.getContestQuestionsDAO().getContestQuestions(filter, null, contestId,sharedProperty);
			if(!contestQuestionList.isEmpty()){
				int i=1;
				for(ContestQuestions q : contestQuestionList){
					q.setSerialNo(i);
					i++;
				}
				QuizDAORegistry.getContestQuestionsDAO().updateAll(contestQuestionList);
			}
			questionDTO.setStatus(1);			
			questionDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(contestQuestionList!=null?contestQuestionList.size():0));				
			questionDTO.setQuestionList(contestQuestionList);
			return questionDTO;
		} catch (Exception e) {
			e.printStackTrace();
			questionDTO.setStatus(0);			
			System.err.println("Error occured while updating contest question.");
			error.setDescription("Error occured while updating contest question.");
			return questionDTO;
		}
	}
	
	@RequestMapping(value = "/ContinueContest")
	public ContestQuestionDTO continueContest(HttpServletRequest request, HttpServletResponse response){
		System.out.println("PERFORMANCE TRACKING 1- :"+new Date());
		ContestQuestionDTO questionDTO = new ContestQuestionDTO();
		Error error = new Error();
		try {			
			String contestIdStr = request.getParameter("contestId");
			String questionNoStr = request.getParameter("questionNo");
			String type = request.getParameter("type");
			
			if(contestIdStr==null || contestIdStr.isEmpty()){
				System.err.println("Contest not found.");
				error.setDescription("Contest not found.");
				questionDTO.setError(error);
				questionDTO.setStatus(0);
				return questionDTO;
			}
			if(questionNoStr==null || questionNoStr.isEmpty()){
				System.err.println("Contest question is not found.");
				error.setDescription("Contest question is not found.");
				questionDTO.setError(error);
				questionDTO.setStatus(0);
				return questionDTO;
			}
			if(type==null || type.isEmpty()){
				System.err.println("Question or Answer type is not found.");
				error.setDescription("Question or Answer type is not found.");
				questionDTO.setError(error);
				questionDTO.setStatus(0);
				return questionDTO;
			}
			Integer contestId = null;
			Integer questionNo = null;
			try {
				contestId = Integer.parseInt(contestIdStr);
			} catch (Exception e) {
				System.err.println("ContestId is invalid.");
				error.setDescription("ContestId is invalid.");
				questionDTO.setError(error);
				questionDTO.setStatus(0);
				return questionDTO;
			}
			try {
				questionNo = Integer.parseInt(questionNoStr);
			} catch (Exception e) {
				System.err.println("Question No is invalid.");
				error.setDescription("Question No is invalid.");
				questionDTO.setError(error);
				questionDTO.setStatus(0);
				return questionDTO;
			}
			ContestQuestions question = null;
			List<ContestQuestions> questionList = new ArrayList<ContestQuestions>();
			ContestQuestions nextQuestion = null;
			Contests contest = QuizDAORegistry.getContestsDAO().get(contestId);
			questionDTO.setContest(contest);
			if(contest.getLastAction().equalsIgnoreCase("ANSWER") || contest.getLastAction().equalsIgnoreCase("COUNT")){
				question= QuizDAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo);
				if(contest.getLastAction().equalsIgnoreCase("ANSWER")){
					nextQuestion = QuizDAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo+1);
				}
				questionList.add(question);
				questionDTO.setQuestionList(questionList);
				questionDTO.setNextQuestion(nextQuestion);
				questionDTO.setStatus(1);
				return questionDTO;
			}else if(contest.getLastAction().equalsIgnoreCase("QUESTION")){
				question= QuizDAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo);
				question.setCorrectAnswer(question.getAnswer());
				question.setAnswer("Z");
				questionList.add(question);
				questionDTO.setQuestionList(questionList);
				questionDTO.setNextQuestion(nextQuestion);
				questionDTO.setStatus(1);
				return questionDTO;
			}else if(contest.getLastAction().equalsIgnoreCase("SUMMARY") || contest.getLastAction().equalsIgnoreCase("LOTTERY")
						|| contest.getLastAction().equalsIgnoreCase("WINNER")){
				question= QuizDAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo);
				question.setAnswer(type);
				questionDTO.setMessage("");
				questionDTO.setStatus(1);
				questionList.add(question);
				questionDTO.setQuestionList(questionList);
				return questionDTO;
			}else if(contest.getLastAction().equalsIgnoreCase("STARTED")){
				question = new ContestQuestions();
				question.setSerialNo(0);
				question.setAnswer("");
				question.setOptionA("");
				question.setOptionB("");
				question.setOptionC("");
				question.setQuestion("");
				question.setContestId(contestId);
				questionList.add(question);
				questionDTO.setStatus(1);
				questionDTO.setQuestionList(questionList);
				return questionDTO;
			}
			questionDTO.setStatus(0);
			error.setDescription("Cannot resume contest, last state of contest is not found.");
			questionDTO.setError(error);
			questionDTO.setQuestionList(questionList);
			return questionDTO;
		}catch (Exception e) {
			e.printStackTrace();
			System.err.println("Error occured whle getting next questions.");
			error.setDescription("Error occured whle getting next questions.");
			questionDTO.setError(error);
			questionDTO.setStatus(0);
			return questionDTO;
		}
	}
	
	
	@RequestMapping(value = "/NextContestQuestion")
	public ContestQuestionDTO getNextContestQuestion(HttpServletRequest request, HttpServletResponse response){
		ContestQuestionDTO questionDTO = new ContestQuestionDTO();
		Error error = new Error();
		try {			
			String contestIdStr = request.getParameter("contestId");
			String questionNoStr = request.getParameter("questionNo");
			String type = request.getParameter("type");
			
			if(contestIdStr==null || contestIdStr.isEmpty()){
				System.err.println("Contest not found.");
				error.setDescription("Contest not found.");
				questionDTO.setError(error);
				questionDTO.setStatus(0);
				return questionDTO;
			}
			if(questionNoStr==null || questionNoStr.isEmpty()){
				System.err.println("Contest question is not found.");
				error.setDescription("Contest question is not found.");
				questionDTO.setError(error);
				questionDTO.setStatus(0);
				return questionDTO;
			}
			if(type==null || type.isEmpty()){
				System.err.println("Question or Answer type is not found.");
				error.setDescription("Question or Answer type is not found.");
				questionDTO.setError(error);
				questionDTO.setStatus(0);
				return questionDTO;
			}
			Integer contestId = null;
			Integer questionNo = null;
			try {
				contestId = Integer.parseInt(contestIdStr);
			} catch (Exception e) {
				System.err.println("ContestId is invalid.");
				error.setDescription("ContestId is invalid.");
				questionDTO.setError(error);
				questionDTO.setStatus(0);
				return questionDTO;
			}
			try {
				questionNo = Integer.parseInt(questionNoStr);
			} catch (Exception e) {
				System.err.println("Question No is invalid.");
				error.setDescription("Question No is invalid.");
				questionDTO.setError(error);
				questionDTO.setStatus(0);
				return questionDTO;
			}
			ContestQuestions question = null;
			List<ContestQuestions> questionList = new ArrayList<ContestQuestions>();
			ContestQuestions nextQuestion = null;
			Contests contest = QuizDAORegistry.getContestsDAO().get(contestId);
			questionDTO.setContest(contest);
			if(type.equals("QUESTION")){
				questionNo++;
				question= QuizDAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo);
				if(question==null){
					question= QuizDAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo-1);
					contest.setStatus("EXPIRED");
					contest.setLastQuestionNo(question.getSerialNo());
					contest.setLastAction("END");
					contest.setActualEndTime(new Date());
					QuizDAORegistry.getContestsDAO().update(contest);
					Util.isContestRunning = false;
					PreContestChecklist preContest = QuizDAORegistry.getContestChecklistDAO().getPreContestChecklistByContestId(contestId);
					if(preContest!=null){
						preContest.setStatus("EXPIRED");
						PreContestChecklistTracking tracking = new PreContestChecklistTracking(preContest);
						QuizDAORegistry.getContestChecklistDAO().update(preContest);
						QuizDAORegistry.getPreContestChecklistTrackingDAO().save(tracking);
					}
					Map<String, String> map = com.rtw.tmat.utils.Util.getParameterMap(request);
					map.put("contestId", contestId.toString());
					map.put("eType", "COMPLETED");
					
					for(RtfConfigContestClusterNodes node : Util.getContestServerNodeUrls()){
						if(node.getUrl().equalsIgnoreCase(Constants.BASE_URL)){
							map.put("clearDbData","Y");
						}else{
							map.put("clearDbData","N");
						}
						String data = com.rtw.tmat.utils.Util.getObject(map,node.getUrl()+Constants.END_QUIZ_CONTEST);
						if(data == null || data.isEmpty()){
							if(node.getUrl().equalsIgnoreCase(Constants.BASE_URL)){
								contestLog.error("RTFAPI ERROR (UpdateEndQuizContest): NULL RESPONSE  -- URL: "+node.getUrl());
								error.setDescription("No Response from API server, Please check API server.");
								questionDTO.setError(error);
								questionDTO.setStatus(0);
								questionList.add(question);
								questionDTO.setQuestionList(questionList);
								return questionDTO;
							}else{
								contestLog.error("RTFAPI ERROR (UpdateEndQuizContest): NULL RESPONSE  -- URL: "+node.getUrl());
								continue;
							}
						}
						
						Gson gson = GsonCustomConfig.getGsonBuilder();		
						JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
						QuizContestInfo quizContestInfo = gson.fromJson(((JsonObject)jsonObject.get("quizContestInfo")), QuizContestInfo.class);
						
						if(quizContestInfo.getStatus() == 0){
							if(node.getUrl().equalsIgnoreCase(Constants.BASE_URL)){
								contestLog.error("RTFAPI ERROR (UpdateEndQuizContest): "+quizContestInfo.getError().getDescription()+" -- URL: "+node.getUrl());
								questionDTO.setError(quizContestInfo.getError());
								questionDTO.setStatus(0);
								questionList.add(question);
								questionDTO.setQuestionList(questionList);
								return questionDTO;
							}else{
								contestLog.error("RTFAPI ERROR (UpdateEndQuizContest): "+quizContestInfo.getError().getDescription()+" -- URL: "+node.getUrl());
								continue;
							}
						}
					}
					
					EndContestThread endQuiz = new EndContestThread(contestId, request, true);
					Thread thread = new Thread(endQuiz);
					thread.start();
					contestLog.info("CONTEST ENDED: -- coid: "+contestId);
					question.setAnswer("END");
					questionDTO.setMessage("Contest is completed.");
					questionDTO.setStatus(1);
					questionList.add(question);
					questionDTO.setQuestionList(questionList);
					return questionDTO;
				}else{
					Map<String, String> map = com.rtw.tmat.utils.Util.getParameterMap(request);
					map.put("contestId", contestId.toString());
					map.put("questionNo", questionNo.toString());
					map.put("customerId", "0");
					
					String data = com.rtw.tmat.utils.Util.getObject(map,Constants.BASE_URL+Constants.GET_QUIZ_QUESTION_INFO);
					Gson gson = GsonCustomConfig.getGsonBuilder();		
					JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
					QuizQuestionInfo quizQuestionInfo = gson.fromJson(((JsonObject)jsonObject.get("quizQuestionInfo")), QuizQuestionInfo.class);
					if(quizQuestionInfo.getStatus()==0){
						contestLog.error("RTFAPI ERROR (GetQuizQuestionInfo): "+quizQuestionInfo.getError().getDescription()+" -- URL: "+Constants.BASE_URL);
						questionDTO.setError(quizQuestionInfo.getError());
						questionDTO.setStatus(0);
						questionList.add(question);
						questionDTO.setLifeCount(0);
						questionDTO.setQuestionList(questionList);
						return questionDTO;
					}
					if(questionDTO.getLifeCount()==null){
						questionDTO.setLifeCount(quizQuestionInfo.getPqLifeCount());
					}
					
					/*for(RtfConfigContestClusterNodes node : Util.getContestServerNodeUrls()){
						String data = com.rtw.tmat.utils.Util.getObject(map,node.getUrl()+Constants.GET_QUIZ_QUESTION_INFO);
						if(data == null || data.isEmpty()){
							if(node.getUrl().equalsIgnoreCase(Constants.BASE_URL)){
								contestLog.error("RTFAPI ERROR (GetQuizQuestionInfo): NULL RESPONSE  -- URL: "+node.getUrl());
								error.setDescription("No Response from API server, Please check API server.");
								questionDTO.setError(error);
								questionDTO.setStatus(0);
								questionList.add(question);
								questionDTO.setQuestionList(questionList);
								return questionDTO;
							}else{
								contestLog.error("RTFAPI ERROR (GetQuizQuestionInfo): NULL RESPONSE  -- URL: "+node.getUrl());
								continue;
							}
						}
						Gson gson = GsonCustomConfig.getGsonBuilder();		
						JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
						QuizQuestionInfo quizQuestionInfo = gson.fromJson(((JsonObject)jsonObject.get("quizQuestionInfo")), QuizQuestionInfo.class);
						
						if(quizQuestionInfo.getStatus() == 0){
							if(node.getUrl().equalsIgnoreCase(Constants.BASE_URL)){
								contestLog.error("RTFAPI ERROR (GetQuizQuestionInfo): "+quizQuestionInfo.getError().getDescription()+" -- URL: "+node.getUrl());
								questionDTO.setError(quizQuestionInfo.getError());
								questionDTO.setStatus(0);
								questionList.add(question);
								questionDTO.setQuestionList(questionList);
								return questionDTO;
							}else{
								contestLog.error("RTFAPI ERROR (GetQuizQuestionInfo): "+quizQuestionInfo.getError().getDescription()+" -- URL: "+node.getUrl());
								continue;
							}
						}
						
						if(questionDTO.getLifeCount()==null){
							questionDTO.setLifeCount(quizQuestionInfo.getPqLifeCount());
						}
						
					}*/
					contest.setLastQuestionNo(question.getSerialNo());
					contest.setLastAction("QUESTION");
					QuizDAORegistry.getContestsDAO().update(contest);
					contestLog.info("CONTEST QUESTION: QNO: "+question.getSerialNo());
				}
				question.setCorrectAnswer(question.getAnswer());
				question.setAnswer("Z");
			}else if(type.equals("SUMMARY") || type.equals("LOTTERY") || type.equals("WINNER") || type.equals("WINNERS")){
				question= QuizDAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo);
				contest.setLastQuestionNo(question.getSerialNo());
				contest.setLastAction(type);
				QuizDAORegistry.getContestsDAO().update(contest);
				question.setAnswer(type);
				questionDTO.setMessage("");
				questionDTO.setStatus(1);
				questionList.add(question);
				contestLog.info("CONTEST "+type+": -- coid: "+contestId);
				questionDTO.setQuestionList(questionList);
				return questionDTO;
			}else if(type.equals("MINIJACKPOT") || type.equals("MINIJACKPOTS")){
				question= QuizDAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo);
				contest.setLastQuestionNo(question.getSerialNo());
				contest.setLastAction(type);
				QuizDAORegistry.getContestsDAO().update(contest);
				question.setAnswer(type);
				questionDTO.setMessage("");
				questionDTO.setStatus(1);
				questionList.add(question);
				contestLog.info("CONTEST "+type+": -- coid: "+contestId+" -- qno: "+question.getSerialNo());
				questionDTO.setQuestionList(questionList);
				return questionDTO;
			}else if(type.equals("MEGAJACKPOT") || type.equals("MEGAJACKPOTS")){
				question= QuizDAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo);
				contest.setLastQuestionNo(question.getSerialNo());
				contest.setLastAction(type);
				QuizDAORegistry.getContestsDAO().update(contest);
				question.setAnswer(type);
				questionDTO.setMessage("");
				questionDTO.setStatus(1);
				questionList.add(question);
				contestLog.info("CONTEST "+type+": -- coid: "+contestId+" -- qno: "+question.getSerialNo());
				questionDTO.setQuestionList(questionList);
				return questionDTO;
			}else if(type.equals("ANSWER") || type.equals("COUNT")){
				if(questionNo == contest.getQuestionSize() || questionNo.equals(contest.getQuestionSize())){
					Map<String, String> map = com.rtw.tmat.utils.Util.getParameterMapCass(request);
					map.put("coId", contestId.toString());
					String data = com.rtw.tmat.utils.Util.getObject(map,Constants.BASE_URL+Constants.UPDATE_CONTEST_WINNER_REWARDS_CASS);
					Gson gson = GsonCustomConfig.getGsonBuilder();		
					JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
					ContWinnerRewardsInfo winnerRewards = gson.fromJson(((JsonObject)jsonObject.get("contWinnerRewardsInfo")), ContWinnerRewardsInfo.class);
					if(winnerRewards == null || winnerRewards.getSts()==0){
						CassError err = winnerRewards.getErr();
						if(err!=null){
							error.setDescription(err.getDesc());
							contestLog.error("RTFAPI ERROR (CompContestWinnerRewards): "+err.getDesc());
						}
						questionDTO.setError(error);
						questionDTO.setStatus(0);
						questionList.add(question);
						questionDTO.setQuestionList(questionList);
						return questionDTO;
					}
				}
				question= QuizDAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo);
				if(type.equals("ANSWER")){
					nextQuestion =  QuizDAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo+1);
				}
				contest.setLastQuestionNo(question.getSerialNo());
				contest.setLastAction(type);
				QuizDAORegistry.getContestsDAO().update(contest);
				contestLog.info("CONTEST "+type+": -- coid: "+contestId+" -- qno: "+question.getSerialNo());
			}else if(type.equals("RESUME")){
				if(contest.getLastAction().equalsIgnoreCase("ANSWER") || contest.getLastAction().equalsIgnoreCase("STARTED")
					 || contest.getLastAction().equalsIgnoreCase("MINIJACKPOT") || contest.getLastAction().equalsIgnoreCase("MEGAJACKPOT")){
					ContestQuestions lastQuestion = QuizDAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo);
					if(questionNo == (contest.getQuestionSize()-1) && contest.getContestJackpotType()!=null 
							&& contest.getContestJackpotType().equals(ContestJackpotType.MEGA)  && contest.getLastAction().equalsIgnoreCase("ANSWER")){
						contest.setLastQuestionNo(lastQuestion.getSerialNo());
						contest.setLastAction("MEGAJACKPOTS");
						QuizDAORegistry.getContestsDAO().update(contest);
						lastQuestion.setAnswer("MEGAJACKPOTS");
						questionDTO.setMessage("");
						questionDTO.setStatus(1);
						questionList.add(lastQuestion);
						contestLog.info("RESUME CONTEST MEGA JACKPOT: -- coid: "+contestId+" -- qno: "+lastQuestion.getSerialNo());
						questionDTO.setQuestionList(questionList);
						return questionDTO;
					}else if(lastQuestion!=null && contest.getContestJackpotType()!=null && lastQuestion.getMiniJackpotType()!=null && contest.getContestJackpotType().equals(ContestJackpotType.MINI)
							&& !lastQuestion.getMiniJackpotType().equals(MiniJackpotType.NONE) && contest.getLastAction().equalsIgnoreCase("ANSWER")){
						contest.setLastQuestionNo(lastQuestion.getSerialNo());
						contest.setLastAction("MINIJACKPOTS");
						QuizDAORegistry.getContestsDAO().update(contest);
						lastQuestion.setAnswer("MINIJACKPOTS");
						questionDTO.setMessage("");
						questionDTO.setStatus(1);
						questionList.add(lastQuestion);
						contestLog.info("RESUME CONTEST MINI JACKPOT: -- coid: "+contestId+" -- qno: "+lastQuestion.getSerialNo());
						questionDTO.setQuestionList(questionList);
						return questionDTO;
					}
					
					
					questionNo++;
					question= QuizDAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo);
					if(question==null){
						question= QuizDAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo-1);
						contest.setLastQuestionNo(question.getSerialNo());
						contest.setLastAction("SUMMARY");
						QuizDAORegistry.getContestsDAO().update(contest);
						question.setAnswer("SUMMARY");
						questionDTO.setMessage("");
						questionDTO.setStatus(1);
						questionList.add(question);
						questionDTO.setQuestionList(questionList);
						contestLog.info("RESUME CONTEST SUMMARY: --coid: "+contestId);
						return questionDTO;
					}else{
						Map<String, String> map = com.rtw.tmat.utils.Util.getParameterMap(request);
						map.put("contestId", contestId.toString());
						map.put("questionNo", questionNo.toString());
						map.put("customerId", "0");
						
						
						String data = com.rtw.tmat.utils.Util.getObject(map,Constants.BASE_URL+Constants.GET_QUIZ_QUESTION_INFO);
						Gson gson = GsonCustomConfig.getGsonBuilder();		
						JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
						QuizQuestionInfo quizQuestionInfo = gson.fromJson(((JsonObject)jsonObject.get("quizQuestionInfo")), QuizQuestionInfo.class);
						if(quizQuestionInfo.getStatus()==0){
							contestLog.error("RESUME RTFAPI ERROR (GetQuizQuestionInfo): "+quizQuestionInfo.getError().getDescription()+" -- URL: "+Constants.BASE_URL);
							questionDTO.setError(quizQuestionInfo.getError());
							questionDTO.setStatus(0);
							questionDTO.setLifeCount(0);
							questionList.add(question);
							questionDTO.setQuestionList(questionList);
							return questionDTO;
						}
						if(questionDTO.getLifeCount()==null){
							questionDTO.setLifeCount(quizQuestionInfo.getPqLifeCount());
						}
						
						/*for(RtfConfigContestClusterNodes node : Util.getContestServerNodeUrls()){
							String data = com.rtw.tmat.utils.Util.getObject(map,node.getUrl()+Constants.GET_QUIZ_QUESTION_INFO);
							if(data == null || data.isEmpty()){
								if(node.getUrl().equalsIgnoreCase(Constants.BASE_URL)){
									contestLog.error("RTFAPI ERROR (GetQuizQuestionInfo): NULL RESPONSE  -- URL: "+node.getUrl());
									error.setDescription("No Response from API server, Please check API server.");
									questionDTO.setError(error);
									questionDTO.setStatus(0);
									questionList.add(question);
									questionDTO.setQuestionList(questionList);
									return questionDTO;
								}else{
									contestLog.error("RTFAPI ERROR (GetQuizQuestionInfo): NULL RESPONSE  -- URL: "+node.getUrl());
									continue;
								}
							}
							Gson gson = GsonCustomConfig.getGsonBuilder();		
							JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
							QuizQuestionInfo quizQuestionInfo = gson.fromJson(((JsonObject)jsonObject.get("quizQuestionInfo")), QuizQuestionInfo.class);
							
							if(quizQuestionInfo.getStatus() == 0){
								if(node.getUrl().equalsIgnoreCase(Constants.BASE_URL)){
									contestLog.error("RTFAPI ERROR (GetQuizQuestionInfo): "+quizQuestionInfo.getError().getDescription()+" -- URL: "+node.getUrl());
									questionDTO.setError(quizQuestionInfo.getError());
									questionDTO.setStatus(0);
									questionList.add(question);
									questionDTO.setQuestionList(questionList);
									return questionDTO;
								}else{
									contestLog.error("RTFAPI ERROR (GetQuizQuestionInfo): "+quizQuestionInfo.getError().getDescription()+" -- URL: "+node.getUrl());
									continue;
								}
							}
							
							if(questionDTO.getLifeCount()==null){
								questionDTO.setLifeCount(quizQuestionInfo.getPqLifeCount());
							}
						}*/
						contest.setLastQuestionNo(question.getSerialNo());
						contest.setLastAction("QUESTION");
						QuizDAORegistry.getContestsDAO().update(contest);
						contestLog.info("RESUME CONTEST QUESTION: -- coid: "+contestId+" -- qno: "+question.getSerialNo());
					}
					question.setCorrectAnswer(question.getAnswer());
					question.setAnswer("Z");
				}else if(contest.getLastAction().equalsIgnoreCase("QUESTION") || contest.getLastAction().equalsIgnoreCase("COUNT")){
					if(questionNo == contest.getQuestionSize() || questionNo.equals(contest.getQuestionSize())){
						Map<String, String> map = com.rtw.tmat.utils.Util.getParameterMapCass(request);
						map.put("coId", contestId.toString());
						String data = com.rtw.tmat.utils.Util.getObject(map,Constants.BASE_URL+Constants.UPDATE_CONTEST_WINNER_REWARDS_CASS);
						Gson gson = GsonCustomConfig.getGsonBuilder();		
						JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
						ContWinnerRewardsInfo winnerRewards = gson.fromJson(((JsonObject)jsonObject.get("contWinnerRewardsInfo")), ContWinnerRewardsInfo.class);
						if(winnerRewards == null || winnerRewards.getSts()==0){
							CassError err = winnerRewards.getErr();
							if(err!=null){
								error.setDescription(err.getDesc());
								contestLog.error("RESUME RTFAPI ERROR (CompContestWinnerRewards): "+err.getDesc());
							}
							questionDTO.setError(error);
							questionDTO.setStatus(0);
							questionList.add(question);
							questionDTO.setQuestionList(questionList);
							return questionDTO;
						}
					}
					question= QuizDAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo);
					if(contest.getLastAction().equalsIgnoreCase("COUNT")){
						nextQuestion =  QuizDAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo+1);
						contest.setLastAction("ANSWER");
					}else{
						contest.setLastAction("COUNT");
					}
					contest.setLastQuestionNo(question.getSerialNo());
					QuizDAORegistry.getContestsDAO().update(contest);
					contestLog.info("RESUME CONTEST "+contest.getLastAction()+": -- coid: "+contestId+" -- qno: "+question.getSerialNo());
				}else if(contest.getLastAction().equalsIgnoreCase("MINIJACKPOTS")){
					question= QuizDAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo);
					question.setAnswer("MINIJACKPOT");
					contest.setLastAction("MINIJACKPOT");
					QuizDAORegistry.getContestsDAO().update(contest);
					questionDTO.setStatus(1);
					questionList.add(question);
					questionDTO.setQuestionList(questionList);
					return questionDTO;
				}else if(contest.getLastAction().equalsIgnoreCase("MEGAJACKPOTS")){
					question= QuizDAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo);
					question.setAnswer("MEGAJACKPOT");
					contest.setLastAction("MEGAJACKPOT");
					QuizDAORegistry.getContestsDAO().update(contest);
					questionDTO.setStatus(1);
					questionList.add(question);
					questionDTO.setQuestionList(questionList);
					return questionDTO;
				}else if(contest.getLastAction().equalsIgnoreCase("SUMMARY") || contest.getLastAction().equalsIgnoreCase("LOTTERY")
						|| contest.getLastAction().equalsIgnoreCase("WINNER") || contest.getLastAction().equalsIgnoreCase("WINNERS")){
					question= QuizDAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo);
					if(contest.getLastAction().equalsIgnoreCase("SUMMARY")){
						question.setAnswer("LOTTERY");
						contest.setLastAction("LOTTERY");
					}else if(contest.getLastAction().equalsIgnoreCase("LOTTERY")){
						question.setAnswer("WINNERS");
						contest.setLastAction("WINNERS");
					}else if(contest.getLastAction().equalsIgnoreCase("WINNERS")){
						contest.setLastQuestionNo(question.getSerialNo());
						contest.setLastAction("WINNER");
						QuizDAORegistry.getContestsDAO().update(contest);
						question.setAnswer("WINNER");
						questionDTO.setStatus(1);
						questionList.add(question);
						questionDTO.setQuestionList(questionList);
						return questionDTO;
					}else if(contest.getLastAction().equalsIgnoreCase("WINNER")){
						contest.setStatus("EXPIRED");
						contest.setLastQuestionNo(question.getSerialNo());
						contest.setLastAction("END");
						contest.setActualEndTime(new Date());
						QuizDAORegistry.getContestsDAO().update(contest);
						Util.isContestRunning = false;
						
						PreContestChecklist preContest = QuizDAORegistry.getContestChecklistDAO().getPreContestChecklistByContestId(contestId);
						if(preContest!=null){
							preContest.setStatus("EXPIRED");
							PreContestChecklistTracking tracking = new PreContestChecklistTracking(preContest);
							QuizDAORegistry.getContestChecklistDAO().update(preContest);
							QuizDAORegistry.getPreContestChecklistTrackingDAO().save(tracking);
						}
						
						Map<String, String> map = com.rtw.tmat.utils.Util.getParameterMap(request);
						map.put("contestId", contestId.toString());
						map.put("eType", "COMPLETED");
						for(RtfConfigContestClusterNodes node : Util.getContestServerNodeUrls()){
							if(node.getUrl().equalsIgnoreCase(Constants.BASE_URL)){
								map.put("clearDbData","Y");
							}else{
								map.put("clearDbData","N");
							}
							String data = com.rtw.tmat.utils.Util.getObject(map,node.getUrl()+Constants.END_QUIZ_CONTEST);
							if(data == null || data.isEmpty()){
								if(node.getUrl().equalsIgnoreCase(Constants.BASE_URL)){
									contestLog.error("RESUME RTFAPI ERROR (UpdateEndQuizContest): NULL RESPONSE : -- URL: "+node.getUrl());
									error.setDescription("No Response from API server, Please check API server.");
									questionDTO.setError(error);
									questionDTO.setStatus(0);
									questionList.add(question);
									questionDTO.setQuestionList(questionList);
									return questionDTO;
								}else{
									contestLog.error("RESUME RTFAPI ERROR (UpdateEndQuizContest): NULL RESPONSE : -- URL: "+node.getUrl());
									continue;
								}
							}
							
							Gson gson = GsonCustomConfig.getGsonBuilder();		
							JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
							QuizContestInfo quizContestInfo = gson.fromJson(((JsonObject)jsonObject.get("quizContestInfo")), QuizContestInfo.class);
							
							if(quizContestInfo.getStatus() == 0){
								if(node.getUrl().equalsIgnoreCase(Constants.BASE_URL)){
									contestLog.error("RESUME RTFAPI ERROR (UpdateEndQuizContest): "+quizContestInfo.getError().getDescription()+" -- URL: "+node.getUrl());
									questionDTO.setError(quizContestInfo.getError());
									questionDTO.setStatus(0);
									questionList.add(question);
									questionDTO.setQuestionList(questionList);
									return questionDTO;
								}else{
									contestLog.error("RESUME RTFAPI ERROR (UpdateEndQuizContest): "+quizContestInfo.getError().getDescription()+" -- URL: "+node.getUrl());
									continue;
								}
							}
						}
						
						EndContestThread endQuiz = new EndContestThread(contestId, request, true);
						Thread thread = new Thread(endQuiz);
						thread.start();
						
						question.setAnswer("END");
						questionDTO.setMessage("Contest is completed.");
						questionDTO.setStatus(1);
						questionList.add(question);
						questionDTO.setQuestionList(questionList);
						return questionDTO;
					}
					contest.setLastQuestionNo(question.getSerialNo());
					QuizDAORegistry.getContestsDAO().update(contest);
					questionDTO.setMessage("");
					questionDTO.setStatus(1);
					questionList.add(question);
					questionDTO.setQuestionList(questionList);
					contestLog.info("RESUME CONTEST "+contest.getLastAction()+": -- coid: "+contestId);
				}
			}else{
				List<Contests> contests = QuizDAORegistry.getContestsDAO().getAllStartedContests();
				if(contests.size() > 0){
					System.err.println("There is already one running contest, Not allowed to run mutiple contest at same time.");
					error.setDescription("There is already one running contest, Not allowed to run mutiple contest at same time.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				if(contest.getStatus().equalsIgnoreCase("ACTIVE")){
					contest.setStatus("STARTED");
					contest.setLastQuestionNo(0);
					contest.setLastAction("STARTED");
					contest.setActualStartTime(new Date());
					QuizDAORegistry.getContestsDAO().update(contest);
					questionDTO.setContest(contest);
					Util.isContestRunning = true;
					
					question= QuizDAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo-1);
					nextQuestion= QuizDAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, 1);
					questionDTO.setNextQuestion(nextQuestion);
					Map<String, String> map = com.rtw.tmat.utils.Util.getParameterMap(request);
					map.put("contestId", contestId.toString());
					
					for(RtfConfigContestClusterNodes node : Util.getContestServerNodeUrls()){
						if(node.getUrl().equalsIgnoreCase(Constants.BASE_URL)){
							map.put("clearDbData","Y");
						}else{
							map.put("clearDbData","N");
						}
						String data = com.rtw.tmat.utils.Util.getObject(map,node.getUrl()+Constants.START_QUIZ_CONTEST);
						if(data == null || data.isEmpty()){
							if(node.getUrl().equalsIgnoreCase(Constants.BASE_URL)){
								contestLog.error("RTFAPI ERROR (UpdateStartQuizContest): NULL RESPONSE -- URL: "+node.getUrl());
								error.setDescription("No Response from API server, Please check API server.");
								questionDTO.setError(error);
								questionDTO.setStatus(0);
								questionList.add(question);
								questionDTO.setQuestionList(questionList);
								return questionDTO;
							}else{
								contestLog.error("RTFAPI ERROR (UpdateStartQuizContest): NULL RESPONSE -- URL: "+node.getUrl());
								continue;
							}
						}

						Gson gson = GsonCustomConfig.getGsonBuilder();		
						JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
						QuizContestInfo quizContestInfo = gson.fromJson(((JsonObject)jsonObject.get("quizContestInfo")), QuizContestInfo.class);
						
						if(quizContestInfo.getStatus() == 0){
							if(node.getUrl().equalsIgnoreCase(Constants.BASE_URL)){
								contestLog.error("RTFAPI ERROR (UpdateStartQuizContest): "+quizContestInfo.getError().getDescription()+" -- URL: "+node.getUrl());
								questionDTO.setError(quizContestInfo.getError());
								questionDTO.setStatus(0);
								questionList.add(question);
								questionDTO.setQuestionList(questionList);
								return questionDTO;
							}else{
								contestLog.error("RTFAPI ERROR (UpdateStartQuizContest): "+quizContestInfo.getError().getDescription()+" -- URL: "+node.getUrl());
								continue;
							}
						}
					}
					contestLog.info("CONTEST STARTED: -- coid: "+contestId);
				}else{
					System.err.println("Contest is Already started by someone, not allowed to start multiple time.");
					error.setDescription("Contest is Already started by someone, not allowed to start multiple time.");
					questionDTO.setError(error);
					questionDTO.setStatus(0);
					return questionDTO;
				}
				//nextQuestion =  QuizDAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo+1);
				question = new ContestQuestions();
				question.setSerialNo(0);
				question.setAnswer("");
				question.setOptionA("");
				question.setOptionB("");
				question.setOptionC("");
				question.setQuestion("");
				question.setContestId(contestId);
			}
			
			questionList.add(question);
			questionDTO.setQuestionList(questionList);
			questionDTO.setStatus(1);
			questionDTO.setNextQuestion(nextQuestion);
			return questionDTO;
			
			
		}catch (Exception e) {
			e.printStackTrace();
			System.err.println("Error occured whle getting next questions.");
			error.setDescription("Error occured whle getting next questions.");
			questionDTO.setError(error);
			questionDTO.setStatus(0);
			return questionDTO;
		}
	}
	
	
	@RequestMapping(value = "/getLastQuestionDetails")
	public ContestQuestionDTO getLastQuestionDetails(HttpServletRequest request, HttpServletResponse response){
		ContestQuestionDTO questionDTO = new ContestQuestionDTO();
		Error error = new Error();
		try {			
			String contestIdStr = request.getParameter("contestId");
			String questionNoStr = request.getParameter("questionNo");
			
			if(contestIdStr==null || contestIdStr.isEmpty()){
				System.err.println("Contest not found.");
				error.setDescription("Contest not found.");
				questionDTO.setError(error);
				questionDTO.setStatus(0);
				return questionDTO;
			}
			if(questionNoStr==null || questionNoStr.isEmpty()){
				System.err.println("Contest question is not found.");
				error.setDescription("Contest question is not found.");
				questionDTO.setError(error);
				questionDTO.setStatus(0);
				return questionDTO;
			}
			Integer contestId = null;
			Integer questionNo = null;
			try {
				contestId = Integer.parseInt(contestIdStr);
			} catch (Exception e) {
				System.err.println("ContestId is invalid.");
				error.setDescription("ContestId is invalid.");
				questionDTO.setError(error);
				questionDTO.setStatus(0);
				return questionDTO;
			}
			try {
				questionNo = Integer.parseInt(questionNoStr);
			} catch (Exception e) {
				System.err.println("Question No is invalid.");
				error.setDescription("Question No is invalid.");
				questionDTO.setError(error);
				questionDTO.setStatus(0);
				return questionDTO;
			}
			ContestQuestions question = QuizDAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo);
			if(question == null){
				System.err.println("Last Question is not found in database.");
				error.setDescription("Last Question is not found in database.");
				questionDTO.setError(error);
				questionDTO.setStatus(0);
				return questionDTO;
			}
			questionDTO.setNextQuestion(question);
			questionDTO.setStatus(1);
			return questionDTO;
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error Occured while getting last question details.");
			questionDTO.setError(error);
			questionDTO.setStatus(0);
			return questionDTO;
		}
	}
	
	
	@RequestMapping(value = "/UpdateQuizConfigSettings")
	public ContestConfigSettingsDTO updateQuizConfigSettings(HttpServletRequest request, HttpServletResponse response){
		ContestConfigSettingsDTO configSettingsDTO = new ContestConfigSettingsDTO();
		Error error = new Error();
		try {			
			String action = request.getParameter("action");
			String liveUrl = request.getParameter("liveUrl");
			String liveThreshold = request.getParameter("liveThreshold");
			String notification7 = request.getParameter("notification7");
			String notification20 = request.getParameter("notification20");
			String nextContest = request.getParameter("nextContest");
			String userName = request.getParameter("userName");
			
			ContestConfigSettings configSettings = QuizDAORegistry.getContestConfigSettingsDAO().getContestConfigSettings();
			if(configSettings==null){
				error.setDescription("Contest Configuration settings record not found in system.");
				configSettingsDTO.setError(error);
				configSettingsDTO.setStatus(0);
				return configSettingsDTO;
			}
			
			if(action.equalsIgnoreCase("LIVEURL")){
				if(liveUrl==null || liveUrl.isEmpty()){
					error.setDescription("Please send valid Live video source URL.");
					configSettingsDTO.setError(error);
					configSettingsDTO.setStatus(0);
					return configSettingsDTO;
				}
				configSettings.setLiveStreamUrl(liveUrl);
				configSettings.setUpdatedBy(userName);
				configSettings.setUpdatedDateTime(new Date());
				QuizDAORegistry.getContestConfigSettingsDAO().update(configSettings);
				configSettingsDTO.setContestConfigSettings(configSettings);
				configSettingsDTO.setStatus(1);
				configSettingsDTO.setMessage("Contest Video Live stream URL Updated successfully.");
				return configSettingsDTO;
			}else if(action.equalsIgnoreCase("LIVESTHRESHOLD")){
				if(liveThreshold==null || liveThreshold.isEmpty()){
					error.setDescription("Please send valid Live video source URL.");
					configSettingsDTO.setError(error);
					configSettingsDTO.setStatus(0);
					return configSettingsDTO;
				}
				
				Integer threshold = 0;
				try {
					threshold = Integer.parseInt(liveThreshold);
				} catch (Exception e) {
					error.setDescription("Please provide valid Integer value for Earn Live Threshold.");
					configSettingsDTO.setError(error);
					configSettingsDTO.setStatus(0);
					return configSettingsDTO;
				}
				configSettings.setLiveThreshold(threshold);
				configSettings.setUpdatedBy(userName);
				configSettings.setUpdatedDateTime(new Date());
				QuizDAORegistry.getContestConfigSettingsDAO().update(configSettings);
				configSettingsDTO.setContestConfigSettings(configSettings);
				configSettingsDTO.setStatus(1);
				configSettingsDTO.setMessage("Contest earn lives threshold Updated successfully.");
				return configSettingsDTO;
			}else if(action.equalsIgnoreCase("NOTIFICATION7")){
				if(notification7==null || notification7.isEmpty()){
					error.setDescription("Please provide 1 Hour Notifiction text.");
					configSettingsDTO.setError(error);
					configSettingsDTO.setStatus(0);
					return configSettingsDTO;
				}
				configSettings.setNotification7(notification7);
				configSettings.setUpdatedBy(userName);
				configSettings.setUpdatedDateTime(new Date());
				QuizDAORegistry.getContestConfigSettingsDAO().update(configSettings);
				configSettingsDTO.setContestConfigSettings(configSettings);
				configSettingsDTO.setStatus(1);
				configSettingsDTO.setMessage("Contest 1 Hours notification text Updated successfully.");
				return configSettingsDTO;
			}else if(action.equalsIgnoreCase("NOTIFICATION20")){
				if(notification20==null || notification20.isEmpty()){
					error.setDescription("Please provide 3 Minutes Notifiction text.");
					configSettingsDTO.setError(error);
					configSettingsDTO.setStatus(0);
					return configSettingsDTO;
				}
				configSettings.setNotification20(notification20);
				configSettings.setUpdatedBy(userName);
				configSettings.setUpdatedDateTime(new Date());
				QuizDAORegistry.getContestConfigSettingsDAO().update(configSettings);
				configSettingsDTO.setContestConfigSettings(configSettings);
				configSettingsDTO.setStatus(1);
				configSettingsDTO.setMessage("Contest 3 minutes notification text Updated successfully.");
				return configSettingsDTO;
			}else if(action.equalsIgnoreCase("SHOWNEXTCONTEST")){
				if(nextContest==null || nextContest.isEmpty()){
					error.setDescription("Please provide display next contest yes or TBD.");
					configSettingsDTO.setError(error);
					configSettingsDTO.setStatus(0);
					return configSettingsDTO;
				}
				configSettings.setShowUpcomingContest(nextContest.equalsIgnoreCase("YES")?true:false);
				configSettings.setUpdatedBy(userName);
				configSettings.setUpdatedDateTime(new Date());
				QuizDAORegistry.getContestConfigSettingsDAO().update(configSettings);
				configSettingsDTO.setContestConfigSettings(configSettings);
				configSettingsDTO.setStatus(1);
				configSettingsDTO.setMessage("Display next contest property updated successfully.");
				return configSettingsDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			configSettingsDTO.setStatus(0);			
			System.err.println("Error occured while updating contest settings.");
			error.setDescription("Error occured while updating contest settings.");
			return configSettingsDTO;
		}
		return configSettingsDTO;
	} 
	
	@RequestMapping(value = "/UpdateReferralRewardSettings")
	public ContestConfigSettingsDTO updateReferralRewardSettings(HttpServletRequest request, HttpServletResponse response){
		ContestConfigSettingsDTO configSettingsDTO = new ContestConfigSettingsDTO();
		Error error = new Error();
		try {			
			String referralRewardCreditType = request.getParameter("referralRewardCreditType");
			String referralRewardStr = request.getParameter("referralReward"); 
			
			Double referralReward = 0.00;
			if(TextUtil.isEmptyOrNull(referralRewardStr)) {
				error.setDescription("Referral reward cannot be empty.");
				configSettingsDTO.setError(error);
				configSettingsDTO.setStatus(0);
				return configSettingsDTO;
			}
			
			try {
				referralReward = Double.valueOf(referralRewardStr.trim());
			}catch(Exception e) {
				error.setDescription("Please enter valid referral rewards.");
				configSettingsDTO.setError(error);
				configSettingsDTO.setStatus(0);
				return configSettingsDTO;
			}
			
			LoyaltySettings loyaltySettings = DAORegistry.getLoyaltySettingsDAO().getActivetLoyaltySettings();
			if(loyaltySettings==null){
				error.setDescription("Loyalty settings record not found in system.");
				configSettingsDTO.setError(error);
				configSettingsDTO.setStatus(0);
				return configSettingsDTO;
			}
			
			loyaltySettings.setContestCustReferralRewardPerc(referralReward);
			loyaltySettings.setReferralRewardCreditType(referralRewardCreditType);
			loyaltySettings.setUpdatedTime(new Date());
			DAORegistry.getLoyaltySettingsDAO().saveOrUpdate(loyaltySettings);
			configSettingsDTO.setLoyaltySettings(loyaltySettings);
			configSettingsDTO.setStatus(1);
			configSettingsDTO.setMessage("Referral reward details Updated successfully.");
			return configSettingsDTO;
			
		} catch (Exception e) {
			e.printStackTrace();
			configSettingsDTO.setStatus(0);			
			System.err.println("Error occured while updating contest referral rewards.");
			error.setDescription("Error occured while updating contest referral rewards.");
			return configSettingsDTO;
		}
	}
	
	@RequestMapping(value = "/GetReferralRewardSettings")
	public ContestConfigSettingsDTO getReferralRewardSettings(HttpServletRequest request, HttpServletResponse response){
		ContestConfigSettingsDTO configSettingsDTO = new ContestConfigSettingsDTO();
		Error error = new Error();
		try {			
			LoyaltySettings loyaltySettings = DAORegistry.getLoyaltySettingsDAO().getActivetLoyaltySettings();
			configSettingsDTO.setLoyaltySettings(loyaltySettings);
			configSettingsDTO.setStatus(1);			
			return configSettingsDTO;
		} catch (Exception e) {
			e.printStackTrace();
			configSettingsDTO.setStatus(0);			
			System.err.println("Error occured while getting contest referral rewards settings.");
			error.setDescription("Error occured while getting contest referral rewards settings.");
			return configSettingsDTO;
		}
	}
	
	
	
	@RequestMapping(value = "/GetContestSettings")
	public ContestConfigSettingsDTO getContestSettings(HttpServletRequest request, HttpServletResponse response){
		ContestConfigSettingsDTO configSettingsDTO = new ContestConfigSettingsDTO();
		Error error = new Error();
		try {			
			ContestConfigSettings configSettings = QuizDAORegistry.getContestConfigSettingsDAO().getContestConfigSettings();
			configSettingsDTO.setContestConfigSettings(configSettings);
			configSettingsDTO.setStatus(1);			
			return configSettingsDTO;
		} catch (Exception e) {
			e.printStackTrace();
			configSettingsDTO.setStatus(0);			
			System.err.println("Error occured while getting contest settings.");
			error.setDescription("Error occured while getting contest settings.");
			return configSettingsDTO;
		}
	}
	
	
	
	@RequestMapping(value = "/ContestEvents")
	public ContestEventsDTO getContestEvents(HttpServletRequest request, HttpServletResponse response){
		ContestEventsDTO eventDTO = new ContestEventsDTO();
		Error error = new Error();
		try{			
			String headerFilter = request.getParameter("headerFilter");
			String contestIdStr = request.getParameter("contestId");
			String pageNo = request.getParameter("pageNo");
			
			if(contestIdStr == null || contestIdStr.trim().isEmpty()){
				eventDTO.setStatus(0);
				eventDTO.setMessage("Not able to identfy contest, pelase send valid contest id to load events.");
				return eventDTO;
			}
			
			Integer contestId = null;
			Contests contest = null;
			try {
				contestId = Integer.parseInt(contestIdStr);
				contest = QuizDAORegistry.getContestsDAO().get(contestId);
				if(null == contest){
					eventDTO.setStatus(0);
					eventDTO.setMessage("Invalid contestId, Please send valid ContestId.");
					return eventDTO;
				}
			} catch (Exception e) {
				eventDTO.setStatus(0);
				eventDTO.setMessage("Invalid contestId, Please send valid ContestId.");
				return eventDTO;
			}
			
			String zones = "";
			if(null != contest.getZone() && !contest.getZone().isEmpty()){
				String[] temp = contest.getZone().split(",");
				int i=0;
				for (String zone : temp) {
					if(i==0){
						zones = "'"+zone+"'";
						i++;
						continue;
					}
					zones = zones+",'"+zone+"'";
					i++;
				}
				
			}
			Double tixPrice = 0.00;
			if(null != contest.getSingleTicketPrice() && contest.getSingleTicketPrice() > 0){
				//freeTicketsPerWinner
				tixPrice = contest.getSingleTicketPrice() /** contest.getFreeTicketPerWinner()*/;
			}
			
			Integer count = 0;
			Integer excludeContestEventCount = 0;
			GridHeaderFilters filter = GridHeaderFiltersUtil.getContestAllEventFilter(headerFilter);
			List<ContestEvents> allEventList = QuizDAORegistry.getContestEventsDAO().getAllEvents(filter, contestId, pageNo,zones,tixPrice,contest.getFreeTicketPerWinner(),sharedProperty,true);
			count = QuizDAORegistry.getContestEventsDAO().getAllEventsCount(filter, contestId,zones,tixPrice,contest.getFreeTicketPerWinner(),sharedProperty);
			
			GridHeaderFilters filter1 = new GridHeaderFilters();
			List<ContestEvents> excludeContestEventList = QuizDAORegistry.getContestEventsDAO().getContestEvents(filter1, contestId,sharedProperty);
			
			if(allEventList == null || allEventList.size() <= 0){
				eventDTO.setMessage("No Events found for selected Contest.");
			}
			
			if(excludeContestEventList != null && excludeContestEventList.size() > 0){
				excludeContestEventCount = excludeContestEventList.size();
			}
			
			eventDTO.setStatus(1);
			eventDTO.setAllEventList(allEventList);
			eventDTO.setPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
			eventDTO.setExcludeEventList(excludeContestEventList);
			eventDTO.setExcludeEventPaginationDTO(PaginationUtil.getDummyPaginationParameters(excludeContestEventCount));
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while loading contest events.");
			eventDTO.setError(error);
			eventDTO.setStatus(0);
		}		
		return eventDTO;
	}
	
	@RequestMapping(value = "/UpdateExcludeContestEvents")
	public GenericResponseDTO updateExcludeContestEvents(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		try{
			String action = request.getParameter("action");
			String contestIdStr = request.getParameter("contestId");
			String eventIdStr = request.getParameter("eventIdStr");
			String userName = request.getParameter("userName");
			
			if(contestIdStr == null || contestIdStr.trim().isEmpty()){
				genericResponseDTO.setStatus(0);
				genericResponseDTO.setMessage("Not able to identfy contest, pelase send valid contest id to load events.");
				return genericResponseDTO;
			}
			if(eventIdStr == null || eventIdStr.trim().isEmpty()){
				genericResponseDTO.setStatus(0);
				genericResponseDTO.setMessage("Not able to identfy event, pelase send valid event id to load events.");
				return genericResponseDTO;
			}
			
			Integer contestId = null;
			try {
				contestId = Integer.parseInt(contestIdStr);
			} catch (Exception e) {
				genericResponseDTO.setStatus(0);
				genericResponseDTO.setMessage("Invalid contestId, Please send valid ContestId.");
				return genericResponseDTO;
			}
			
			/*GridHeaderFilters filter = new GridHeaderFilters();
			List<ContestEvents> contestEventList = QuizDAORegistry.getContestEventsDAO().getContestEvents(filter, contestId);
			List<ContestEvents> contestEventFinalList = new ArrayList<ContestEvents>();
			
			if(eventIdStr.contains(",")){
				String arr[] = eventIdStr.split(",");
				Map<Integer, Boolean> selectedEventMap = new HashMap<Integer, Boolean>();
				for(String str : arr){
					if(str != null && !str.isEmpty()){
						Integer eventId = Integer.parseInt(str);
						selectedEventMap.put(eventId, true);
					}
				}
				for(ContestEvents contestEvent : contestEventList){
					if(null == selectedEventMap.get(contestEvent.getEventId())){
						contestEventFinalList.add(contestEvent);
					}
				}
				
			}else{
				Integer eventId = Integer.parseInt(eventIdStr);
				for(ContestEvents contestEvent : contestEventList){
					if(!contestEvent.getEventId().equals(eventId)){
						contestEventFinalList.add(contestEvent);
					}
				}
			}*/
			
			if(action != null && action.equalsIgnoreCase("EXCLUDE")){
				
				//Insert Contest Events
				ContestEvents contestEventObj = null;
				List<ContestEvents> contestEventSaveList = new ArrayList<ContestEvents>();
				if(eventIdStr.contains(",")){
					String arr[] = eventIdStr.split(",");
					for(String str : arr){
						if(str != null && !str.isEmpty()){
							Integer eventId = Integer.parseInt(str);
							contestEventObj = new ContestEvents();
							contestEventObj.setContestId(contestId);
							contestEventObj.setEventId(eventId);
							contestEventObj.setUpdatedBy(userName);
							contestEventObj.setUpdatedDateTime(new Date());
							
							contestEventSaveList.add(contestEventObj);
						}
					}
				}else{
					Integer eventId = Integer.parseInt(eventIdStr);
					contestEventObj = new ContestEvents();
					contestEventObj.setContestId(contestId);
					contestEventObj.setEventId(eventId);
					contestEventObj.setUpdatedBy(userName);
					contestEventObj.setUpdatedDateTime(new Date());
					
					contestEventSaveList.add(contestEventObj);
				}
				QuizDAORegistry.getContestEventsDAO().saveAll(contestEventSaveList);
							
				genericResponseDTO.setStatus(1);			
				genericResponseDTO.setMessage("Contest Events Excluded.");
				
			}else if(action != null && action.equalsIgnoreCase("REMOVEEXCLUDE")){
				
				//Delete All The Contest Events
				QuizDAORegistry.getContestEventsDAO().deleteByContestIdAndEventId(contestId, eventIdStr);
				
				genericResponseDTO.setStatus(1);			
				genericResponseDTO.setMessage("Removed - Excluded Contest Events.");
			}
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while updating exclude/remove exclude contest events.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}		
		return genericResponseDTO;
	}
	
	@RequestMapping(value = "/ExcludeContestEvents")
	public ContestEventsDTO getExcludeContestEvents(HttpServletRequest request, HttpServletResponse response){
		ContestEventsDTO eventDTO = new ContestEventsDTO();
		Error error = new Error();
		try{			
			String headerFilter = request.getParameter("headerFilter");
			String contestIdStr = request.getParameter("contestId");
			String pageNo = request.getParameter("pageNo");
			
			if(contestIdStr == null || contestIdStr.trim().isEmpty()){
				eventDTO.setStatus(0);
				eventDTO.setMessage("Not able to identfy contest, pelase send valid contest id to load events.");
				return eventDTO;
			}
			
			Integer contestId = null;
			try {
				contestId = Integer.parseInt(contestIdStr);
			} catch (Exception e) {
				eventDTO.setStatus(0);
				eventDTO.setMessage("Invalid contestId, Please send valid ContestId.");
				return eventDTO;
			}
			
			Integer excludeContestEventCount = 0;
			GridHeaderFilters filter = GridHeaderFiltersUtil.getContestEventFilter(headerFilter);
			List<ContestEvents> excludeContestEventList = QuizDAORegistry.getContestEventsDAO().getContestEvents(filter, contestId,sharedProperty);
						
			if(excludeContestEventList != null && excludeContestEventList.size() > 0){
				excludeContestEventCount = excludeContestEventList.size();
			}
			if(excludeContestEventList == null || excludeContestEventList.size() <= 0){
				eventDTO.setMessage("No Exclude Events found for selected Contest.");
			}
			
			eventDTO.setStatus(1);
			eventDTO.setExcludeEventList(excludeContestEventList);
			eventDTO.setExcludeEventPaginationDTO(PaginationUtil.getDummyPaginationParameters(excludeContestEventCount));
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while loading contest events.");
			eventDTO.setError(error);
			eventDTO.setStatus(0);
		}		
		return eventDTO;
	}
	
	@RequestMapping("/AutoCompleteArtistEventAndCategory")
	public AutoCompleteArtistAndCategoryDTO getAutoCompleteArtistEventAndCategory(HttpServletRequest request, HttpServletResponse response) throws Exception {
		AutoCompleteArtistAndCategoryDTO autoCompleteArtistAndCategoryDTO = new AutoCompleteArtistAndCategoryDTO();
		Error error = new Error();
		
		try{
			String param = request.getParameter("q");
			String strResponse = "";
			
			Collection<ParentCategory> parentCat = DAORegistry.getParentCategoryDAO().getParentCategoriesByName(param);
			Collection<GrandChildCategory> grandChildCategories = DAORegistry.getGrandChildCategoryDAO().getGrandChildCategoriesByNameAndDisplayOnSearch(param);
			Collection<ChildCategory> childCategories = DAORegistry.getChildCategoryDAO().getChildCategoriesByNameAndDisplayOnSearch(param);
			Collection<Artist> artists = DAORegistry.getArtistDAO().filterByName(param);	
			Collection<Event> events = DAORegistry.getQueryManagerDAO().getAllActiveEventWithVenue(param);
				
			strResponse += ("ALL"+ "|" + "0" + "|" + "ALL" + "\n");
			if (parentCat != null) {
				for (ParentCategory parent : parentCat) {
					strResponse += ("PARENT" + "|" + parent.getId() + "|" + parent.getName() + "\n");
				}
			}
			
			if (childCategories != null) {
				for (ChildCategory childCategory : childCategories) {
					strResponse += ("CHILD" + "|" + childCategory.getId() + "|" + childCategory.getName() + "\n");
				}
			}
			
			if (grandChildCategories != null) {
				for (GrandChildCategory grandChildCateogry : grandChildCategories) {
					strResponse += ("GRAND" + "|" + grandChildCateogry.getId() + "|" + grandChildCateogry.getName() + "\n");
				}
			}
			
			if (artists != null) {
				for (Artist artist : artists) {
					strResponse += ("ARTIST" + "|" + artist.getId() + "|" + artist.getName() + "\n") ;
				}
			}
			
			if (events != null) {
				for (Event event : events) {
					strResponse += ("EVENT" + "|" + event.getEventId() + "|" + event.getEventName() + "|" + event.getNameWithDateandVenue() + "\n") ;
				}
			}
			autoCompleteArtistAndCategoryDTO.setStatus(1);
			autoCompleteArtistAndCategoryDTO.setArtistAndCategory(strResponse);
			
		}catch(Exception e){
			e.printStackTrace();
			System.err.println(e.getLocalizedMessage());
			error.setDescription("Something went wrong while fetching Auto Complete Artist and Categories.");
			autoCompleteArtistAndCategoryDTO.setError(error);
			autoCompleteArtistAndCategoryDTO.setStatus(0);
		}
		return autoCompleteArtistAndCategoryDTO;
	}
	
	@RequestMapping("/AutoCompleteGiftCard")
	public AutoCompleteArtistAndCategoryDTO getAutoCompleteGiftCard(HttpServletRequest request, HttpServletResponse response) throws Exception {
		AutoCompleteArtistAndCategoryDTO autoCompleteArtistAndCategoryDTO = new AutoCompleteArtistAndCategoryDTO();
		Error error = new Error();
		
		try{
			String param = request.getParameter("q");
			String strResponse = "";
			
			Collection<GiftCardAutoComplete> giftcards = DAORegistry.getRtfGiftCardDAO().filterByName(param);	
				
			//strResponse += ("ALL"+ "|" + "0" + "|" + "ALL" + "\n");
			
			if (giftcards != null) {
				for (GiftCardAutoComplete giftcard : giftcards) {
					strResponse += ("GIFT" + "|" + giftcard.getCardValueId() + "|" + giftcard.getTitle()+ "|" + giftcard.getCardAmountStr()+ "|" + giftcard.getQty() + "\n") ;
				}
			}
			
			autoCompleteArtistAndCategoryDTO.setStatus(1);
			autoCompleteArtistAndCategoryDTO.setArtistAndCategory(strResponse);
			
		}catch(Exception e){
			e.printStackTrace();
			System.err.println(e.getLocalizedMessage());
			error.setDescription("Something went wrong while fetching Auto Complete Gift Cards.");
			autoCompleteArtistAndCategoryDTO.setError(error);
			autoCompleteArtistAndCategoryDTO.setStatus(0);
		}
		return autoCompleteArtistAndCategoryDTO;
	}
	
	@RequestMapping("/GetCustomersAutoComplete")
	public AutoCompleteArtistAndCategoryDTO getAutoCompleteCustomers(HttpServletRequest request, HttpServletResponse response) throws Exception {
		AutoCompleteArtistAndCategoryDTO autoCompleteArtistAndCategoryDTO = new AutoCompleteArtistAndCategoryDTO();
		Error error = new Error();
		
		try{
			String param = request.getParameter("q");
			String strResponse = "";
			
			List<Customer> customers = DAORegistry.getCustomerDAO().getCustomerByEmailUserIdOrFirstNameLastName(param);
				
			if (customers != null) {
				for (Customer cust : customers) {
					strResponse += ("CUST" + "|" + cust.getId() + "|" + cust.getUserId() + "|" + cust.getEmail() + "\n");
				}
			}
			
			autoCompleteArtistAndCategoryDTO.setStatus(1);
			autoCompleteArtistAndCategoryDTO.setArtistAndCategory(strResponse);
			
		}catch(Exception e){
			e.printStackTrace();
			System.err.println(e.getLocalizedMessage());
			error.setDescription("Something went wrong while fetching Auto Complete Customers.");
			autoCompleteArtistAndCategoryDTO.setError(error);
			autoCompleteArtistAndCategoryDTO.setStatus(0);
		}
		return autoCompleteArtistAndCategoryDTO;
	}
	
	
	

	//Question Bank - Update
	@RequestMapping(value = "/UpdateQuestionBank")
	public QuestionBankDTO updateQuestionBank(HttpServletRequest request, HttpServletResponse response){
		QuestionBankDTO questionBankDTO = new QuestionBankDTO();
		Error error = new Error();
		
		try {			
			String action = request.getParameter("action");
			
			String questionIdStr = request.getParameter("qBId");
			String questionText = request.getParameter("qBText");
			String optionA = request.getParameter("qBOptionA");
			String optionB = request.getParameter("qBOptionB");
			String optionC = request.getParameter("qBOptionC");
			//String optionD = request.getParameter("qBOptionD");
			//String questionNo = request.getParameter("qBNo");
			String answer = request.getParameter("qBAnswer");
			String questionReward = request.getParameter("qBReward");
			String difficultyLevel = request.getParameter("difficultyLevel");
			String category = request.getParameter("qBCategory");
			String pageNo = request.getParameter("pageNo");
			String userName = request.getParameter("userName");
			String factChecked = request.getParameter("factChecked");
			String status = null;
			Date today = new Date();
			
			if(action != null && action.equalsIgnoreCase("EDIT")){
				
				if(questionIdStr == null || questionIdStr.isEmpty()){
					System.err.println("Please select any Question's from Bank to Edit.");
					error.setDescription("Please select any Question's from Bank to Edit.");
					questionBankDTO.setError(error);
					questionBankDTO.setStatus(0);
					return questionBankDTO;
				}
				
				Integer questionId = null;
				try {
					questionId = Integer.parseInt(questionIdStr);
				} catch (Exception e) {
					System.err.println("Invalid QuestionBankId found, Please send valid questionBankId.");
					error.setDescription("Invalid QuestionBankId found, Please send valid questionBankId.");
					questionBankDTO.setError(error);
					questionBankDTO.setStatus(0);
					return questionBankDTO;
				} 
				
				GridHeaderFilters filter = GridHeaderFiltersUtil.getQuestionBankFilter(null); 
				List<QuestionBank> questionList = QuizDAORegistry.getQuestionBankDAO().getQuestionBankList(filter, questionId, pageNo,"'ACTIVE','USED'",true);
				if(questionList.size() == 0){
					System.err.println("Selected question is not found in system.");
					error.setDescription("Selected question is not found in system.");
					questionBankDTO.setError(error);
					questionBankDTO.setStatus(0);
					return questionBankDTO;
				}
				
				questionBankDTO.setStatus(1);
				questionBankDTO.setQuestionBankList(questionList);				
				return questionBankDTO;
			}else if(action != null && (action.equalsIgnoreCase("SAVE") || action.equalsIgnoreCase("UPDATE"))){
				
				if(questionText==null || questionText.isEmpty()){
					System.err.println("Please provide Question Text.");
					error.setDescription("Please provide Question Text.");
					questionBankDTO.setError(error);
					questionBankDTO.setStatus(0);
					return questionBankDTO;
				}
				
				if(questionReward==null || questionReward.isEmpty()){
					System.err.println("Please provide question reward.");
					error.setDescription("Please provide question reward.");
					questionBankDTO.setError(error);
					questionBankDTO.setStatus(0);
					return questionBankDTO;
				}
				Double questionRewards = null;
				try {
					questionRewards = Double.parseDouble(questionReward);
					if(questionRewards > 0){
						questionRewards = com.rtw.tmat.utils.Util.roundOffDouble(questionRewards);
					}
				} catch (Exception e) {
					System.err.println("Please provide valid question reward.");
					error.setDescription("Please provide valid question reward.");
					questionBankDTO.setError(error);
					questionBankDTO.setStatus(0);
					return questionBankDTO;
				}
				
				if(optionA==null || optionA.isEmpty()){
					System.err.println("Please provide option A text.");
					error.setDescription("Please provide option A text.");
					questionBankDTO.setError(error);
					questionBankDTO.setStatus(0);
					return questionBankDTO;
				}
				if(optionB==null || optionB.isEmpty()){
					System.err.println("Please provide option B text.");
					error.setDescription("Please provide option B text.");
					questionBankDTO.setError(error);
					questionBankDTO.setStatus(0);
					return questionBankDTO;
				}
				if(optionC==null || optionC.isEmpty()){
					System.err.println("Please provide option C text.");
					error.setDescription("Please provide option C text.");
					questionBankDTO.setError(error);
					questionBankDTO.setStatus(0);
					return questionBankDTO;
				}
				/*if(optionD==null || optionD.isEmpty()){
					System.err.println("Please provide option D text.");
					error.setDescription("Please provide option D text.");
					questionBankDTO.setError(error);
					questionBankDTO.setStatus(0);
					return questionBankDTO;
				}*/
				if(answer==null || answer.isEmpty()){
					System.err.println("Please provide answer text.");
					error.setDescription("Please provide answer text.");
					questionBankDTO.setError(error);
					questionBankDTO.setStatus(0);
					return questionBankDTO;
				}
				if(category==null || category.isEmpty()){
					System.err.println("Please provide category.");
					error.setDescription("Please provide category.");
					questionBankDTO.setError(error);
					questionBankDTO.setStatus(0);
					return questionBankDTO;
				}
				
				QuestionBank question = null;
				
				if(action.equalsIgnoreCase("SAVE")){
					String questionStr = questionText.trim();
					if(questionStr.contains("?")){
						questionStr = questionStr.replace("?", "");
					}
					question = QuizDAORegistry.getQuestionBankDAO().getQuestionBankByQuestion(questionStr.trim());
					if(question != null){
						System.err.println("Question is Already Exist.");
						error.setDescription("Question is Already Exist.");
						questionBankDTO.setError(error);
						questionBankDTO.setStatus(0);
						return questionBankDTO;
					}
				}
				if(action.equalsIgnoreCase("UPDATE")){
					
					if(questionIdStr == null || questionIdStr.isEmpty()){
						System.err.println("Please select any Question from Bank to Update.");
						error.setDescription("Please select any Question from Bank to Update.");
						questionBankDTO.setError(error);
						questionBankDTO.setStatus(0);
						return questionBankDTO;
					}
					question = QuizDAORegistry.getQuestionBankDAO().get(Integer.parseInt(questionIdStr));
					if(question == null){
						System.err.println("Question is not found.");
						error.setDescription("Question is not found.");
						questionBankDTO.setError(error);
						questionBankDTO.setStatus(0);
						return questionBankDTO;
					}
				}
				if(question == null){
					question = new QuestionBank();
				}
				if(action.equalsIgnoreCase("SAVE")){
					question.setCreatedBy(userName);
					question.setCreatedDate(today);
					question.setStatus("ACTIVE");
					status = "'ACTIVE'";
				}
				if(action.equalsIgnoreCase("UPDATE")){
					question.setUpdatedBy(userName);
					question.setUpdatedDate(today);
					status= "'"+question.getStatus() +"'";
				}
				
				if(factChecked != null){
					if(factChecked.equalsIgnoreCase("Yes")){
						question.setFactChecked(true);
					}else{
						question.setFactChecked(false);
					}
				}
								
				question.setQuestion(questionText);
				question.setOptionA(optionA);
				question.setOptionB(optionB);
				question.setOptionC(optionC);
				question.setDificultyLevel(difficultyLevel);
				question.setAnswer(answer);
				question.setQuestionReward(questionRewards);
				question.setCategory(category);
				
				QuizDAORegistry.getQuestionBankDAO().saveOrUpdate(question);
				
				if(action.equalsIgnoreCase("SAVE")){
					questionBankDTO.setMessage("Question Created successfully.");
				}
				if(action.equalsIgnoreCase("UPDATE")){
					questionBankDTO.setMessage("Question Updated successfully.");
				}
				
				GridHeaderFilters filter = GridHeaderFiltersUtil.getQuestionBankFilter(null); 
				List<QuestionBank> questionList = QuizDAORegistry.getQuestionBankDAO().getQuestionBankList(filter, null, pageNo,status,true);
				Integer count = QuizDAORegistry.getQuestionBankDAO().getQuestionBankCount(filter, null,status);
				
				questionBankDTO.setStatus(1);			
				questionBankDTO.setPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));				
				questionBankDTO.setQuestionBankList(questionList);
				return questionBankDTO;
			}else if(action != null && action.equalsIgnoreCase("DELETE")){
				
				QuestionBank question =  null;
				
				if(questionIdStr == null || questionIdStr.isEmpty()){
					System.err.println("Please select any Question to Delete.");
					error.setDescription("Please select any Question to Delete.");
					questionBankDTO.setError(error);
					questionBankDTO.setStatus(0);
					return questionBankDTO;
				}
				List<QuestionBank> qList = new ArrayList<QuestionBank>();
				if(questionIdStr.contains(",")){
					String ids[] = questionIdStr.split(",");
					try {
						for(int i=0;i<ids.length;i++){
							QuestionBank q = QuizDAORegistry.getQuestionBankDAO().get(Integer.parseInt(ids[i]));
							status = "'"+q.getStatus()+"'";
							q.setStatus("DELETED");
							q.setUpdatedBy(userName);
							q.setUpdatedDate(today);
							qList.add(q);
						}
						QuizDAORegistry.getQuestionBankDAO().updateAll(qList);
					} catch (Exception e) {
						System.err.println("Found Bad values in Question Ids to mark Delete.");
						error.setDescription("Found Bad values in Question Ids to mark Delete.");
						questionBankDTO.setError(error);
						questionBankDTO.setStatus(0);
						return questionBankDTO;
					}
					
				}else{
					question = QuizDAORegistry.getQuestionBankDAO().get(Integer.parseInt(questionIdStr));
					if(question == null){
						System.err.println("Question is not found.");
						error.setDescription("Question is not found.");
						questionBankDTO.setError(error);
						questionBankDTO.setStatus(0);
						return questionBankDTO;
					}
					status = "'"+question.getStatus()+"'";
					question.setStatus("DELETED");
					question.setUpdatedBy(userName);
					question.setUpdatedDate(today);
					QuizDAORegistry.getQuestionBankDAO().update(question);
				}
				
				
				GridHeaderFilters filter = GridHeaderFiltersUtil.getQuestionBankFilter(null); 
				List<QuestionBank> questionList = QuizDAORegistry.getQuestionBankDAO().getQuestionBankList(filter, null, pageNo,status,true);
				Integer count = QuizDAORegistry.getQuestionBankDAO().getQuestionBankCount(filter, null,status);
				
				questionBankDTO.setStatus(1);
				questionBankDTO.setMessage("Question Deleted successfully.");
				questionBankDTO.setPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));				
				questionBankDTO.setQuestionBankList(questionList);
				return questionBankDTO;	
			}else if(action.equalsIgnoreCase("FACT")){
				QuestionBank question =  null;
				
				if(questionIdStr == null || questionIdStr.isEmpty()){
					System.err.println("Please select any Question to Delete.");
					error.setDescription("Please select any Question to Delete.");
					questionBankDTO.setError(error);
					questionBankDTO.setStatus(0);
					return questionBankDTO;
				}
				if(factChecked == null || factChecked.isEmpty()){
					System.err.println("Please send valid value for Fact Checked Yes/No.");
					error.setDescription("Please send valid value for Fact Checked Yes/No.");
					questionBankDTO.setError(error);
					questionBankDTO.setStatus(0);
					return questionBankDTO;
				}
				Boolean factCheck = false;
				if(factChecked.equalsIgnoreCase("Yes")){
					factCheck = true;
				}
				
				List<QuestionBank> qList = new ArrayList<QuestionBank>();
				if(questionIdStr.contains(",")){
					String ids[] = questionIdStr.split(",");
					try {
						for(int i=0;i<ids.length;i++){
							QuestionBank q = QuizDAORegistry.getQuestionBankDAO().get(Integer.parseInt(ids[i]));
							status = "'"+q.getStatus()+"'";
							q.setFactChecked(factCheck);
							q.setUpdatedBy(userName);
							q.setUpdatedDate(today);
							qList.add(q);
						}
						QuizDAORegistry.getQuestionBankDAO().updateAll(qList);
					} catch (Exception e) {
						System.err.println("Found Bad values in Question Ids to mark Fact checked.");
						error.setDescription("Found Bad values in Question Ids to mark Fact checked.");
						questionBankDTO.setError(error);
						questionBankDTO.setStatus(0);
						return questionBankDTO;
					}
					
				}else{
					question = QuizDAORegistry.getQuestionBankDAO().get(Integer.parseInt(questionIdStr));
					if(question == null){
						System.err.println("Question is not found.");
						error.setDescription("Question is not found.");
						questionBankDTO.setError(error);
						questionBankDTO.setStatus(0);
						return questionBankDTO;
					}
					status = "'"+question.getStatus()+"'";
					question.setFactChecked(factCheck);
					question.setUpdatedBy(userName);
					question.setUpdatedDate(today);
					QuizDAORegistry.getQuestionBankDAO().update(question);
				}
				
				
				GridHeaderFilters filter = GridHeaderFiltersUtil.getQuestionBankFilter(null); 
				List<QuestionBank> questionList = QuizDAORegistry.getQuestionBankDAO().getQuestionBankList(filter, null, pageNo,status,true);
				Integer count = QuizDAORegistry.getQuestionBankDAO().getQuestionBankCount(filter, null,status);
				
				questionBankDTO.setStatus(1);
				questionBankDTO.setMessage("Question Updated successfully.");
				questionBankDTO.setPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));				
				questionBankDTO.setQuestionBankList(questionList);
				return questionBankDTO;	
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			questionBankDTO.setStatus(0);			
			System.err.println("Error occured while updating Question Bank.");
			error.setDescription("Error occured while updating Question Bank.");
			return questionBankDTO;
		}
		return questionBankDTO;
	}
	
	@RequestMapping("/AutoCompleteQuestionBankCategory")
	public AutoCompleteDTO getAutoCompleteQuestionBankCategory(HttpServletRequest request, HttpServletResponse response) throws Exception {
		AutoCompleteDTO autoCompleteDTO = new AutoCompleteDTO();
		Error error = new Error();
		
		try{
			String param = request.getParameter("q");
			String strResponse = "";
			
			Collection<QuestionBank> questionBankList = QuizDAORegistry.getQuestionBankDAO().getAllQuestionBankCategory(param);
				
			if (questionBankList != null) {
				for (QuestionBank question : questionBankList) {
					strResponse += ("CAT" + "|" + question.getCategory() + "\n");
				}
			}
			autoCompleteDTO.setStatus(1);
			autoCompleteDTO.setAutoCompleteStringResponse(strResponse);
			
		}catch(Exception e){
			e.printStackTrace();
			System.err.println(e.getLocalizedMessage());
			error.setDescription("Something went wrong while fetching Auto Complete Question Bank Categories.");
			autoCompleteDTO.setError(error);
			autoCompleteDTO.setStatus(0);
		}
		return autoCompleteDTO;
	}
	
	@RequestMapping(value = "/ContestQuestionBank")
	public ContestsDTO getContestQuestionBank(HttpServletRequest request, HttpServletResponse response){
		ContestsDTO contestsDTO = new ContestsDTO();
		Error error = new Error();
		try{			
			String headerFilter = request.getParameter("headerFilter");
			String pageNo = request.getParameter("pageNo");
			
			Integer count = 0;
			GridHeaderFilters filter = GridHeaderFiltersUtil.getQuestionBankFilter(headerFilter); 
			List<QuestionBank> questionBankList = QuizDAORegistry.getQuestionBankDAO().getQuestionBankList(filter, null, pageNo,"'ACTIVE'",true);
			count = QuizDAORegistry.getQuestionBankDAO().getQuestionBankCount(filter, null,"'ACTIVE'");
			
			if(questionBankList == null || questionBankList.size() <= 0){
				contestsDTO.setMessage("No Question's found from Question Bank.");
			}
			
			contestsDTO.setStatus(1);			
			contestsDTO.setQuestionBankPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
			contestsDTO.setQuestionBankList(questionBankList);
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while loading contest events.");
			contestsDTO.setError(error);
			contestsDTO.setStatus(0);
		}		
		return contestsDTO;
	}
	
	
	@RequestMapping(value = "/UpdateQuestionPosition")
	public ContestQuestionDTO updateQuestionPosition(HttpServletRequest request, HttpServletResponse response){
		ContestQuestionDTO questionDTO = new ContestQuestionDTO();
		Error error = new Error();
		try {
			String contestIdStr = request.getParameter("contestId");
			String questionString = request.getParameter("questionString");
			//String qId = request.getParameter("qId");
			//String positionStr = request.getParameter("position");
			String userName = request.getParameter("userName");
			
			
			/*if(qId==null || qId.isEmpty()){
				questionDTO.setStatus(0);
				error.setDescription("Please send Question Id to update question position.");
				questionDTO.setError(error);
				return questionDTO;
			}*/
			if(contestIdStr==null || contestIdStr.isEmpty()){
				questionDTO.setStatus(0);
				error.setDescription("Please send Contest Id to update question position.");
				questionDTO.setError(error);
				return questionDTO;
			}
			if(questionString==null || questionString.isEmpty()){
				questionDTO.setStatus(0);
				error.setDescription("Please send valid action UP or DOWN to update question position.");
				questionDTO.setError(error);
				return questionDTO;
			}
			//Integer questionId = null;
			Integer contestId =null;
			/*try {
				questionId = Integer.parseInt(qId);
			} catch (Exception e) {
				questionDTO.setStatus(0);
				error.setDescription("Invalid Question Id, Please send valid question id.");
				questionDTO.setError(error);
				return questionDTO;
			}*/
			try {
				contestId = Integer.parseInt(contestIdStr);
			} catch (Exception e) {
				questionDTO.setStatus(0);
				error.setDescription("Invalid Contest id, Please send valid Contest id.");
				questionDTO.setError(error);
				return questionDTO;
			}
			
			Contests contest = QuizDAORegistry.getContestsDAO().get(contestId);
			GridHeaderFilters filter = GridHeaderFiltersUtil.getContestsFilter(null);
			if(contest == null){
				questionDTO.setStatus(0);
				error.setDescription("Contest not found in system with given contest id.");
				questionDTO.setError(error);
				return questionDTO;
			}
			if(questionString.contains(",")){
				Map<Integer, Integer> map = new HashMap<Integer, Integer>();
				List<Integer> positionList = new ArrayList<Integer>();
				String quePosition[] = questionString.split(",");
				for(String str : quePosition){
					if(str.trim().isEmpty()){
						continue;
					}
					if(str.contains(":")){
						String positions[] = str.split(":");
						try {
							map.put(Integer.parseInt(positions[0]),Integer.parseInt(positions[1]));
							positionList.add(Integer.parseInt(positions[1]));
						} catch (Exception e) {
							questionDTO.setStatus(0);
							error.setDescription("Invalid question position found.");
							questionDTO.setError(error);
							return questionDTO;
						}
						
					}else{
						questionDTO.setStatus(0);
						error.setDescription("Invalid or bad format question position found.");
						questionDTO.setError(error);
						return questionDTO;
					}
				}
				
				if(map.size() == contest.getQuestionSize()){
					Collections.sort(positionList);
					Integer i=1;
					for(Integer p : positionList){
						if(p!=i){
							questionDTO.setStatus(0);
							error.setDescription("Entered position is not sequencial, please enter sequencial position without gap.");
							questionDTO.setError(error);
							return questionDTO;
						}
						i++;
					}
					List<ContestQuestions> questions = QuizDAORegistry.getContestQuestionsDAO().getContestQuestions(filter, null, contestId,sharedProperty);
					for(ContestQuestions q : questions){
						q.setSerialNo(map.get(q.getId()));
					}
					QuizDAORegistry.getContestQuestionsDAO().updateAll(questions);
					questionDTO.setMessage("Question positions updated.");
				}
				
			}else{
				questionDTO.setStatus(0);
				error.setDescription("Invalid or bad format question positoin found.");
				questionDTO.setError(error);
				return questionDTO;
			}
			
			/*ContestQuestions question = QuizDAORegistry.getContestQuestionsDAO().get(questionId);
			if(question == null){
				questionDTO.setStatus(0);
				error.setDescription("Question not found in system with given question no.");
				questionDTO.setError(error);
				return questionDTO;
			}
			Integer actualPosition = question.getSerialNo();
			if(action.equalsIgnoreCase("DOWN")){
				if(contest.getQuestionSize() == actualPosition || contest.getQuestionSize().equals(actualPosition)){
					questionDTO.setStatus(0);
					error.setDescription("Selected question is already last Question, you cant take it down.");
					questionDTO.setError(error);
					return questionDTO;
				}
				ContestQuestions q = QuizDAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId,(question.getSerialNo() + 1));
				if(q == null){
					questionDTO.setStatus(0);
					error.setDescription("Selected question is already last Question, you cant take it down.");
					questionDTO.setError(error);
					return questionDTO;
				}
				q.setSerialNo(actualPosition);
				question.setSerialNo( question.getSerialNo()+1);
				QuizDAORegistry.getContestQuestionsDAO().update(question);
				QuizDAORegistry.getContestQuestionsDAO().update(q);
			}else if(action.equalsIgnoreCase("UP")){
				if(actualPosition == 1 || actualPosition.equals(1)){
					questionDTO.setStatus(0);
					error.setDescription("Selected question is already first Question, you cant take it up.");
					questionDTO.setError(error);
					return questionDTO;
				}
				ContestQuestions q = QuizDAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId,(question.getSerialNo() - 1));
				if(q == null){
					questionDTO.setStatus(0);
					error.setDescription("Selected question is already first Question, you cant take it up.");
					questionDTO.setError(error);
					return questionDTO;
				}
				q.setSerialNo(actualPosition);
				question.setSerialNo( question.getSerialNo()-1);
				QuizDAORegistry.getContestQuestionsDAO().update(question);
				QuizDAORegistry.getContestQuestionsDAO().update(q);
			}else if(action.equalsIgnoreCase("MANUAL")){
				if(positionStr==null || positionStr.isEmpty()){
					questionDTO.setStatus(0);
					error.setDescription("Please sent position to move question.");
					questionDTO.setError(error);
					return questionDTO;
				}
				Integer position = null;
				try {
					position = Integer.parseInt(positionStr);
				} catch (Exception e) {
					questionDTO.setStatus(0);
					error.setDescription("Invalid Position to move question.");
					questionDTO.setError(error);
					return questionDTO;
				}
				ContestQuestions q1 = QuizDAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId,actualPosition);
				if(q1 == null){
					questionDTO.setStatus(0);
					error.setDescription("There is no question on given position, cant replace question with given position.");
					questionDTO.setError(error);
					return questionDTO;
				}
				if(position > actualPosition){
					List<ContestQuestions> questions = QuizDAORegistry.getContestQuestionsDAO().getContestQuestionstoReposition(actualPosition,contestId);
					System.out.println("actualPosition : "+actualPosition+"   position : "+position);
					for(ContestQuestions q : questions){
						if(q.getSerialNo() > actualPosition && q.getSerialNo() <= position ){
							System.out.println(q.getSerialNo()+" to : "+(q.getSerialNo()-1));
							q.setSerialNo(q.getSerialNo()-1);
						}
					}
					QuizDAORegistry.getContestQuestionsDAO().updateAll(questions);
				}else if(position < actualPosition){
					List<ContestQuestions> questions = QuizDAORegistry.getContestQuestionsDAO().getContestQuestionstoReposition(actualPosition,contestId);
					System.out.println("actualPosition : "+actualPosition+"   position : "+position);
					for(ContestQuestions q : questions){
						if(q.getSerialNo() >=  position && q.getSerialNo() < actualPosition){
							System.out.println(q.getSerialNo()+" to : "+(q.getSerialNo()+1));
							q.setSerialNo(q.getSerialNo()+1);
						}
					}
					QuizDAORegistry.getContestQuestionsDAO().updateAll(questions);
				}
				q1.setSerialNo(position);
				QuizDAORegistry.getContestQuestionsDAO().update(q1);
			}*/
			
			List<ContestQuestions> contestQuestionList = QuizDAORegistry.getContestQuestionsDAO().getContestQuestions(filter, null, contestId,sharedProperty);
			questionDTO.setStatus(1);			
			questionDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(contestQuestionList!=null?contestQuestionList.size():0));				
			questionDTO.setQuestionList(contestQuestionList);
			return questionDTO;
		} catch (Exception e) {
			e.printStackTrace();
			questionDTO.setStatus(0);
			error.setDescription("Error occured while updating question position.");
			questionDTO.setError(error);
			return questionDTO;	
		}
		
	}
	
	
	@RequestMapping(value = "/ViewContestWinners")
	public ContestWinnerDTO viewContestWinners(HttpServletRequest request, HttpServletResponse response){
		ContestWinnerDTO winnerDTO = new ContestWinnerDTO();
		Error error = new Error();
		try {
			String contestIdStr = request.getParameter("contestId");
			String headerFilter = request.getParameter("headerFilter");
			String headerFilter1 = request.getParameter("headerFilter1");
			String type = request.getParameter("type");
			
			if(contestIdStr==null || contestIdStr.isEmpty()){
				winnerDTO.setStatus(0);
				error.setDescription("Please send ContestId.");
				winnerDTO.setError(error);
				return winnerDTO;
			}
			Integer contestId = null;
			try {
				contestId = Integer.parseInt(contestIdStr);
			} catch (Exception e) {
				e.printStackTrace();
				winnerDTO.setStatus(0);
				error.setDescription("Please send valid ContestId.");
				winnerDTO.setError(error);
				return winnerDTO;
			}
			if(type==null || type.isEmpty()){
				type="";
			}
			if(type.equalsIgnoreCase("SUMMARY")){
				GridHeaderFilters filter = GridHeaderFiltersUtil.getContestsWinnerFilter(headerFilter);
				List<ContestWinner> summaryWinners = QuizDAORegistry.getContestsDAO().getContestWinners(contestId,filter,sharedProperty);
				if(summaryWinners==null || summaryWinners.isEmpty()){
					winnerDTO.setStatus(0);
					error.setDescription("No summary winners are found for selected contest.");
					winnerDTO.setError(error);
					return winnerDTO;
				}
				winnerDTO.setSummaryPagination(PaginationUtil.getDummyPaginationParameters(summaryWinners!=null?summaryWinners.size():0));
				winnerDTO.setStatus(1);
				winnerDTO.setSummaryWinners(summaryWinners);
				return winnerDTO;
			}else if(type.equalsIgnoreCase("GRAND")){
				GridHeaderFilters filter1 = GridHeaderFiltersUtil.getContestsWinnerFilter(headerFilter1);
				List<ContestWinner> grandWinners = QuizDAORegistry.getContestsDAO().getContestGrandWinners(contestId,filter1,sharedProperty);
				if(grandWinners==null || grandWinners.isEmpty()){
					winnerDTO.setStatus(0);
					error.setDescription("No grand winners are found for selected contest.");
					winnerDTO.setError(error);
					return winnerDTO;
				}
				winnerDTO.setGrandWinnerPagination(PaginationUtil.getDummyPaginationParameters(grandWinners!=null?grandWinners.size():0));
				winnerDTO.setStatus(1);
				winnerDTO.setGrandWinners(grandWinners);
				return winnerDTO;
			}else{
				GridHeaderFilters filter = GridHeaderFiltersUtil.getContestsWinnerFilter(headerFilter);
				List<ContestWinner> summaryWinners = QuizDAORegistry.getContestsDAO().getContestWinners(contestId,filter,sharedProperty);
				GridHeaderFilters filter1 = GridHeaderFiltersUtil.getContestsWinnerFilter(headerFilter1);
				List<ContestWinner> grandWinners = QuizDAORegistry.getContestsDAO().getContestGrandWinners(contestId,filter1,sharedProperty);
				if((grandWinners==null || grandWinners.isEmpty()) && (summaryWinners == null || summaryWinners.isEmpty())){
					winnerDTO.setStatus(0);
					error.setDescription("No summary or grand winners are found for selected contest.");
					winnerDTO.setError(error);
					return winnerDTO;
				}
				winnerDTO.setSummaryPagination(PaginationUtil.getDummyPaginationParameters(summaryWinners!=null?summaryWinners.size():0));
				winnerDTO.setGrandWinnerPagination(PaginationUtil.getDummyPaginationParameters(grandWinners!=null?grandWinners.size():0));
				winnerDTO.setStatus(1);
				winnerDTO.setSummaryWinners(summaryWinners);
				winnerDTO.setGrandWinners(grandWinners);
				return winnerDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			winnerDTO.setStatus(0);
			error.setDescription("Error occured while fetching contest winners details.");
			winnerDTO.setError(error);
			return winnerDTO;	
		}
	}
	
	@RequestMapping(value = "/UpdateExpiryDate")
	public ContestWinnerDTO updateWinnerExpiryDate(HttpServletRequest request, HttpServletResponse response){
		ContestWinnerDTO winnerDTO = new ContestWinnerDTO();
		Error error = new Error();
		try {
			String winnerIdStr = request.getParameter("winnerId");
			String userName = request.getParameter("userName");
			String expiryDateStr = request.getParameter("expiryDate");
			if(winnerIdStr==null || winnerIdStr.isEmpty()){
				winnerDTO.setStatus(0);
				error.setDescription("Please send winnerId.");
				winnerDTO.setError(error);
				return winnerDTO;
			}
			if(expiryDateStr==null || expiryDateStr.isEmpty()){
				winnerDTO.setStatus(0);
				error.setDescription("Please send expiry date.");
				winnerDTO.setError(error);
				return winnerDTO;
			}
			Integer winnerId = 0;
			try {
				winnerId = Integer.parseInt(winnerIdStr);
			} catch (Exception e) {
				winnerDTO.setStatus(0);
				error.setDescription("Please send valid winner id.");
				winnerDTO.setError(error);
				return winnerDTO;
			}
			Date expiryDate =  Util.getDateWithTwentyFourHourFormat1(expiryDateStr + " 23:59:59");
			if(expiryDate ==null){
				winnerDTO.setStatus(0);
				error.setDescription("Please send valid date.");
				winnerDTO.setError(error);
				return winnerDTO;
			}
			
			ContestGrandWinners winner = QuizDAORegistry.getContestGrandWinnersDAO().get(winnerId);
			if(winner==null){
				winnerDTO.setStatus(0);
				error.setDescription("Contest Grand Winner not found with given id.");
				winnerDTO.setError(error);
				return winnerDTO;
			}
			winner.setExpiryDate(expiryDate);
			QuizDAORegistry.getContestGrandWinnersDAO().update(winner);
			winnerDTO.setStatus(1);
			winnerDTO.setMessage("Grand winner expiry date updated successfully.");
			return winnerDTO;
		} catch (Exception e) {
			e.printStackTrace();
			winnerDTO.setStatus(0);
			error.setDescription("Error occured while updating contest winners details.");
			winnerDTO.setError(error);
			return winnerDTO;
		}
	}
	
	
	@RequestMapping(value="/ExportContestUsers")
	public void getInvoicesToExport(HttpServletRequest request, HttpServletResponse response){
		String contestId = request.getParameter("contestId");
		try {
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Contest Customers");
			Row header = sheet.createRow((int)0);
			header.createCell(0).setCellValue("First Name");
			header.createCell(1).setCellValue("Last Name");
			header.createCell(2).setCellValue("User Id");
			header.createCell(3).setCellValue("Email");
			header.createCell(4).setCellValue("Phone");
			header.createCell(5).setCellValue("FirstJoinTime");
			header.createCell(6).setCellValue("Signup Date");
			
			
			if(contestId!=null && !contestId.isEmpty()){
				//List<Customer> customers = DAORegistry.getQueryManagerDAO().getContestCustomers(contestId);
				List<Customer> customers = QuizDAORegistry.getContestsDAO().getContestCustomers(Integer.parseInt(contestId),sharedProperty);
				int i=1;
				for(Customer c : customers){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(c.getCustomerName());
					row.createCell(1).setCellValue(c.getLastName());
					row.createCell(2).setCellValue(c.getUserId());
					row.createCell(3).setCellValue(c.getEmail());
					row.createCell(4).setCellValue(c.getPhone());
					row.createCell(5).setCellValue(c.getJoinDateStr());
					row.createCell(6).setCellValue(c.getSignupDateStr());
					i++;
				}
			}
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=ContestCustomers.xls");
			workbook.write(response.getOutputStream());
			response.getOutputStream().flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@RequestMapping(value="/ContestPromocodes")
	public ContestPromocodeDTO getContestPromocode(HttpServletRequest request, HttpServletResponse response){
		ContestPromocodeDTO codeDTO = new ContestPromocodeDTO();
		Error error = new Error();
		String promocodeId = request.getParameter("promocodeId");
		String headerFilter = request.getParameter("headerFilter");
		String status = request.getParameter("status");
		try {
			if(status==null || status.isEmpty()){
				codeDTO.setStatus(0);
				error.setDescription("Please Send valid Contest promo type name.");
				codeDTO.setError(error);
				return codeDTO;
			}
			if(status.equalsIgnoreCase("FREETICKET")){
				if(promocodeId != null && !promocodeId.isEmpty()){
					Integer codeId = 0;
					try {
						codeId = Integer.parseInt(promocodeId);
					} catch (Exception e) {
						e.printStackTrace();
						codeDTO.setStatus(0);
						error.setDescription("Please Send valid Contest Free Ticket offer Id.");
						codeDTO.setError(error);
						return codeDTO;
					}
					
					ContestPromocode promocode = QuizDAORegistry.getContestPromocodeDAO().get(codeId);
					if(promocode == null){
						codeDTO.setStatus(0);
						error.setDescription("Contest Free Ticket offer not found in system with given Id.");
						codeDTO.setError(error);
						return codeDTO;
					}
					Customer cust = DAORegistry.getCustomerDAO().get(promocode.getCustomerId());
					promocode.setUserId(cust.getUserId());
					promocode.setEmail(cust.getEmail());
					codeDTO.setStatus(1);
					codeDTO.setContestPromocode(promocode);
					return codeDTO;
				}else{
					GridHeaderFilters filter = GridHeaderFiltersUtil.getContestPromocodeFiter(headerFilter);
					List<ContestPromocode> contestPromocodes = QuizDAORegistry.getContestPromocodeDAO().getAllContestPromocodes(null, filter, null, null,sharedProperty);
					codeDTO.setDiscountCodePaginationDTO(PaginationUtil.getDummyPaginationParameters(contestPromocodes!=null?contestPromocodes.size():0));
					codeDTO.setStatus(1);
					codeDTO.setContestPromocodes(contestPromocodes);
					return codeDTO;
				}
			}else if(status.equalsIgnoreCase("FREELIVES")){
				if(promocodeId != null && !promocodeId.isEmpty()){
					Integer codeId = 0;
					try {
						codeId = Integer.parseInt(promocodeId);
					} catch (Exception e) {
						e.printStackTrace();
						codeDTO.setStatus(0);
						error.setDescription("Please Send valid Contest earn live code Id.");
						codeDTO.setError(error);
						return codeDTO;
					}
					
					PromotionalEarnLifeOffer offer = DAORegistry.getPromotionalEarnLifeOfferDAO().get(codeId);
					if(offer == null){
						codeDTO.setStatus(0);
						error.setDescription("Contest earn live promocode not found in system with given Id.");
						codeDTO.setError(error);
						return codeDTO;
					}
					codeDTO.setStatus(1);
					codeDTO.setPromoOffer(offer);
					return codeDTO;
				}else{
					GridHeaderFilters filter = GridHeaderFiltersUtil.getContestPromoOfferFiter(headerFilter);
					List<PromotionalEarnLifeOffer> offers = DAORegistry.getPromotionalEarnLifeOfferDAO().getAllContestPromocodes(null, filter, null, null);
					codeDTO.setDiscountCodePaginationDTO(PaginationUtil.getDummyPaginationParameters(offers!=null?offers.size():0));
					codeDTO.setStatus(1);
					codeDTO.setPromoOffers(offers);
					return codeDTO;
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			codeDTO.setStatus(0);
			error.setDescription("Error occured while fetching contest promotional Offers/codes.");
			codeDTO.setError(error);
			return codeDTO;
		}
		codeDTO.setStatus(0);
		error.setDescription("Error occured while fetching contest promocodes.");
		codeDTO.setError(error);
		return codeDTO;
	}
	
	
	@RequestMapping(value="/UpdateContestPromocodeStatus")
	public ContestPromocodeDTO updateContestPromocodeStatus(HttpServletRequest request, HttpServletResponse response){
		ContestPromocodeDTO codeDTO = new ContestPromocodeDTO();
		Error error = new Error();
		String promocodeId = request.getParameter("promocodeId");
		String status = request.getParameter("status");
		String userName = request.getParameter("userName");
		try {
			if(status == null || status.isEmpty()){
				codeDTO.setStatus(0);
				error.setDescription("Please Send valid status to update contest free ticket offer.");
				codeDTO.setError(error);
				return codeDTO;
			}
			if(promocodeId == null || promocodeId.isEmpty()){
				codeDTO.setStatus(0);
				error.setDescription("Please Send contest free ticket offer Id.");
				codeDTO.setError(error);
				return codeDTO;
			}
			
			Integer codeId = 0;
			try {
				codeId = Integer.parseInt(promocodeId);
			} catch (Exception e) {
				e.printStackTrace();
				codeDTO.setStatus(0);
				error.setDescription("Please Send valid contest free ticket offer Id.");
				codeDTO.setError(error);
				return codeDTO;
			}
			
			ContestPromocode promocode = QuizDAORegistry.getContestPromocodeDAO().get(codeId);
			if(promocode == null){
				codeDTO.setStatus(0);
				error.setDescription("contest free ticket offer not found in system with given Id.");
				codeDTO.setError(error);
				return codeDTO;
			}
			if(status.equalsIgnoreCase("ACTIVE")){
				if(promocode.getStatus().equalsIgnoreCase("ACTIVE")){
					codeDTO.setStatus(0);
					error.setDescription("Selected contest free ticket offer is already in Active status.");
					codeDTO.setError(error);
					return codeDTO;
				}
				promocode.setStatus("ACTIVE");
				promocode.setUpdatedBy(userName);
				promocode.setUpdatedDate(new Date());
				QuizDAORegistry.getContestPromocodeDAO().update(promocode);
				codeDTO.setMessage("Contest promocode is updated successfully");
			}else if(status.equalsIgnoreCase("DISABLED")){
				if(promocode.getStatus().equalsIgnoreCase("DISABLED")){
					codeDTO.setStatus(0);
					error.setDescription("Selected contest free ticket offer is already in DISABLED status.");
					codeDTO.setError(error);
					return codeDTO;
				}
				promocode.setStatus("DISABLED");
				promocode.setUpdatedBy(userName);
				promocode.setUpdatedDate(new Date());
				QuizDAORegistry.getContestPromocodeDAO().update(promocode);
				codeDTO.setMessage("contest free ticket offer is updated successfully");
			}
			
			GridHeaderFilters filter = GridHeaderFiltersUtil.getContestPromocodeFiter(null);
			List<ContestPromocode> contestPromocodes = QuizDAORegistry.getContestPromocodeDAO().getAllContestPromocodes(null, filter, null, null,sharedProperty);
			codeDTO.setDiscountCodePaginationDTO(PaginationUtil.getDummyPaginationParameters(contestPromocodes!=null?contestPromocodes.size():0));
			codeDTO.setStatus(1);
			codeDTO.setContestPromocodes(contestPromocodes);
			return codeDTO;
		} catch (Exception e) {
			e.printStackTrace();
			codeDTO.setStatus(0);
			error.setDescription("Error occured while fetching contest free ticket offer.");
			codeDTO.setError(error);
			return codeDTO;
		}
	}
	
	
	
	@RequestMapping(value="/GenerateContestPromocode")
	public ContestPromocodeDTO updateContestPromocode(HttpServletRequest request, HttpServletResponse response){
		ContestPromocodeDTO codeDTO = new ContestPromocodeDTO();
		Error error = new Error();
		String action = request.getParameter("action");
		String promoDisciption = request.getParameter("promoDisciption");
		String freeTickets = request.getParameter("freeTickets");
		String artistId = request.getParameter("artistId");
		String eventId = request.getParameter("eventId");
		String parentId = request.getParameter("parentId");
		String childId = request.getParameter("childId");
		String grandChildId = request.getParameter("grandChildId");
		String customerId = request.getParameter("customerId");
		String expiryDate = request.getParameter("expiryDate");
		String promoId = request.getParameter("promoId");
		String userName = request.getParameter("userName");
		String artistEventCategoryName = request.getParameter("artistEventCategoryName");
		String contestRefType = "";
		Integer contestRefTypeId = null;
		
		try {
			if(action == null || action.isEmpty()){
				codeDTO.setStatus(0);
				error.setDescription("Please Send valid Action type.");
				codeDTO.setError(error);
				return codeDTO;
			}
			
			if(artistId!=null && !artistId.isEmpty()){
				contestRefType = "ARTIST";
				contestRefTypeId = Integer.parseInt(artistId);
			}else if(eventId!=null && !eventId.isEmpty()){
				contestRefType = "EVENT";
				contestRefTypeId = Integer.parseInt(eventId);
			}else if(parentId!=null && !parentId.isEmpty()){
				contestRefType = "PARENT";
				contestRefTypeId = Integer.parseInt(parentId);
			}else if(childId!=null && !childId.isEmpty()){
				contestRefType = "CHILD";
				contestRefTypeId = Integer.parseInt(childId);
			}else if(grandChildId!=null && !grandChildId.isEmpty()){
				contestRefType = "GRAND";
				contestRefTypeId = Integer.parseInt(grandChildId);
			}
			
			
			if(artistEventCategoryName == null || artistEventCategoryName.isEmpty()){
				codeDTO.setStatus(0);
				error.setDescription("Please Send Promocode Artist/Event/Caegory.");
				codeDTO.setError(error);
				return codeDTO;
			}
			if(promoDisciption == null || promoDisciption.isEmpty()){
				codeDTO.setStatus(0);
				error.setDescription("Please Send valid contest free ticket offer Name.");
				codeDTO.setError(error);
				return codeDTO;
			}
			if(expiryDate == null || expiryDate.isEmpty()){
				codeDTO.setStatus(0);
				error.setDescription("Please Send Expiry Date.");
				codeDTO.setError(error);
				return codeDTO;
			}
			if(freeTickets == null || freeTickets.isEmpty()){
				codeDTO.setStatus(0);
				error.setDescription("Please Send No of Free Tickets.");
				codeDTO.setError(error);
				return codeDTO;
			}
			if(customerId == null || customerId.isEmpty()){
				codeDTO.setStatus(0);
				error.setDescription("Please Send Customer Id.");
				codeDTO.setError(error);
				return codeDTO;
			}
			Integer custId = 0;
			Integer noOfTickets = 0;
			Date promoExpiryDate = null;
			try {
				noOfTickets = Integer.parseInt(freeTickets);
			} catch (Exception e) {
				e.printStackTrace();
				codeDTO.setStatus(0);
				error.setDescription("Please Send valid No of free tickets.");
				codeDTO.setError(error);
				return codeDTO;
			}
			try {
				custId = Integer.parseInt(customerId);
			} catch (Exception e) {
				e.printStackTrace();
				codeDTO.setStatus(0);
				error.setDescription("Please Send valid Customer Id.");
				codeDTO.setError(error);
				return codeDTO;
			}
			try {
				expiryDate += " 23:59:59";
				promoExpiryDate = com.rtw.tmat.utils.Util.getDateWithTwentyFourHourFormat1(expiryDate);
			} catch (Exception e) {
				e.printStackTrace();
				codeDTO.setStatus(0);
				error.setDescription("Please Send valid contest free ticket offer expiry Date.");
				codeDTO.setError(error);
				return codeDTO;
			}
			
			if(action.equalsIgnoreCase("SAVE")){
				ContestPromocode promocode = new ContestPromocode();
				promocode.setCreatedBy(userName);
				promocode.setCreatedDate(new Date());
				promocode.setCustomerId(custId);
				promocode.setDiscountPercentage(0.00);
				promocode.setEarnLifeCount(0);
				promocode.setFreeTixCount(noOfTickets);
				promocode.setPromoexpiryDate(promoExpiryDate);
				promocode.setPromoType("REWARD_FREE_TIX");
				promocode.setStatus("ACTIVE");
				promocode.setPromoName(promoDisciption);
				promocode.setPromoRefType(contestRefType);
				promocode.setPromoRefId(contestRefTypeId);
				promocode.setPromoRefName(artistEventCategoryName);
				QuizDAORegistry.getContestPromocodeDAO().save(promocode);
				codeDTO.setMessage("contest free ticket offer created successfully");
			}else if(action.equalsIgnoreCase("UPDATE")){
				if(promoId == null || promoId.isEmpty()){
					codeDTO.setStatus(0);
					error.setDescription("Please send contest free ticket offer id to be updated.");
					codeDTO.setError(error);
					return codeDTO;
				}
				Integer promotionalId = 0;
				try {
					promotionalId = Integer.parseInt(promoId);
				} catch (Exception e) {
					e.printStackTrace();
					codeDTO.setStatus(0);
					error.setDescription("Please Send valid contest free ticket offer Id.");
					codeDTO.setError(error);
					return codeDTO;
				}
				ContestPromocode promocode = QuizDAORegistry.getContestPromocodeDAO().get(promotionalId);
				if(promocode == null){
					codeDTO.setStatus(0);
					error.setDescription("contest free ticket offer not found in system with given Id.");
					codeDTO.setError(error);
					return codeDTO;
				}
				
				promocode.setCustomerId(custId);
				promocode.setFreeTixCount(noOfTickets);
				promocode.setPromoexpiryDate(promoExpiryDate);
				promocode.setPromoName(promoDisciption);
				promocode.setPromoRefType(contestRefType);
				promocode.setPromoRefId(contestRefTypeId);
				promocode.setPromoRefName(artistEventCategoryName);
				promocode.setUpdatedBy(userName);
				promocode.setUpdatedDate(new Date());
				QuizDAORegistry.getContestPromocodeDAO().update(promocode);
				codeDTO.setMessage("contest free ticket offer updated successfully");
			}
			GridHeaderFilters filter = new GridHeaderFilters();
			List<ContestPromocode> contestPromocodes = QuizDAORegistry.getContestPromocodeDAO().getAllContestPromocodes(null, filter, null, null,sharedProperty);
			codeDTO.setDiscountCodePaginationDTO(PaginationUtil.getDummyPaginationParameters(contestPromocodes!=null?contestPromocodes.size():0));
			codeDTO.setStatus(1);
			codeDTO.setContestPromocodes(contestPromocodes);
			return codeDTO;
		} catch (Exception e) {
			e.printStackTrace();
			codeDTO.setStatus(0);
			error.setDescription("Error occured while updating contest free ticket offer.");
			codeDTO.setError(error);
			return codeDTO;
		}
	}
	
	
	
	@RequestMapping(value="/UpdateContestEarnLivesOfferStatus")
	public ContestPromocodeDTO updateContestEarnLivesOfferStatus(HttpServletRequest request, HttpServletResponse response){
		ContestPromocodeDTO codeDTO = new ContestPromocodeDTO();
		Error error = new Error();
		String promocodeId = request.getParameter("promocodeId");
		String status = request.getParameter("status");
		String userName = request.getParameter("userName");
		try {
			if(status == null || status.isEmpty()){
				codeDTO.setStatus(0);
				error.setDescription("Please Send valid status to update contest earn lives promocode status.");
				codeDTO.setError(error);
				return codeDTO;
			}
			if(promocodeId == null || promocodeId.isEmpty()){
				codeDTO.setStatus(0);
				error.setDescription("Please Send contest earn lives promocode Id.");
				codeDTO.setError(error);
				return codeDTO;
			}
			
			Integer codeId = 0;
			try {
				codeId = Integer.parseInt(promocodeId);
			} catch (Exception e) {
				e.printStackTrace();
				codeDTO.setStatus(0);
				error.setDescription("Please Send valid contest earn lives promocode Id.");
				codeDTO.setError(error);
				return codeDTO;
			}
			
			PromotionalEarnLifeOffer offer = DAORegistry.getPromotionalEarnLifeOfferDAO().get(codeId);
			if(offer == null){
				codeDTO.setStatus(0);
				error.setDescription("contest earn lives promocode not found in system with given Id.");
				codeDTO.setError(error);
				return codeDTO;
			}
			if(status.equalsIgnoreCase("ACTIVE")){
				if(offer.getStatus().equalsIgnoreCase("ACTIVE")){
					codeDTO.setStatus(0);
					error.setDescription("Selected contest earn lives promocode is already in Active status.");
					codeDTO.setError(error);
					return codeDTO;
				}
				offer.setStatus("ACTIVE");
				offer.setUpdatedBy(userName);
				offer.setUpdatedDate(new Date());
				DAORegistry.getPromotionalEarnLifeOfferDAO().update(offer);
				codeDTO.setMessage("contest earn lives promocode is updated successfully");
			}else if(status.equalsIgnoreCase("DISABLED")){
				if(offer.getStatus().equalsIgnoreCase("DISABLED")){
					codeDTO.setStatus(0);
					error.setDescription("Selected contest earn lives promocode is already in DISABLED status.");
					codeDTO.setError(error);
					return codeDTO;
				}
				offer.setStatus("DISABLED");
				offer.setUpdatedBy(userName);
				offer.setUpdatedDate(new Date());
				DAORegistry.getPromotionalEarnLifeOfferDAO().update(offer);
				codeDTO.setMessage("contest earn lives promocode is updated successfully");
			}
			
			GridHeaderFilters filter = GridHeaderFiltersUtil.getContestPromoOfferFiter(null);
			List<PromotionalEarnLifeOffer> offers = DAORegistry.getPromotionalEarnLifeOfferDAO().getAllContestPromocodes(null, filter, null, null);
			codeDTO.setDiscountCodePaginationDTO(PaginationUtil.getDummyPaginationParameters(offers!=null?offers.size():0));
			codeDTO.setStatus(1);
			codeDTO.setPromoOffers(offers);
			return codeDTO;
		} catch (Exception e) {
			e.printStackTrace();
			codeDTO.setStatus(0);
			error.setDescription("Error occured while fetching contest free ticket offer.");
			codeDTO.setError(error);
			return codeDTO;
		}
	}
	
	
	
	
	@RequestMapping(value="/GenerateContestEarnLivePromo")
	public ContestPromocodeDTO updateEarnLivesPromocode(HttpServletRequest request, HttpServletResponse response){
		ContestPromocodeDTO codeDTO = new ContestPromocodeDTO();
		Error error = new Error();
		String action = request.getParameter("action");
		String promoCode = request.getParameter("promoCode");
		String lifePerCustomerStr = request.getParameter("lifePerCustomer");
		String maxThresholdStr = request.getParameter("maxThreshold");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String promoId = request.getParameter("promoId");
		String userName = request.getParameter("userName");
		
		try {
			if(action == null || action.isEmpty()){
				codeDTO.setStatus(0);
				error.setDescription("Please Send valid Action type.");
				codeDTO.setError(error);
				return codeDTO;
			}
			if(lifePerCustomerStr == null || lifePerCustomerStr.isEmpty()){
				codeDTO.setStatus(0);
				error.setDescription("Please Send Max Lives per customer.");
				codeDTO.setError(error);
				return codeDTO;
			}
			if(maxThresholdStr == null || maxThresholdStr.isEmpty()){
				codeDTO.setStatus(0);
				error.setDescription("Please Send Max Total Lives Threshold.");
				codeDTO.setError(error);
				return codeDTO;
			}
			if(startDate == null || startDate.isEmpty()){
				codeDTO.setStatus(0);
				error.setDescription("Please Send earn lives promo Start date.");
				codeDTO.setError(error);
				return codeDTO;
			}
			if(endDate == null || endDate.isEmpty()){
				codeDTO.setStatus(0);
				error.setDescription("Please Send earn lives promo End date.");
				codeDTO.setError(error);
				return codeDTO;
			}
			Integer lifePerCustomer = 0;
			Integer maxThreshold = 0;
			Date promoStartDate = null;
			Date promoEndDate = null;
			try {
				lifePerCustomer = Integer.parseInt(lifePerCustomerStr);
			} catch (Exception e) {
				e.printStackTrace();
				codeDTO.setStatus(0);
				error.setDescription("Please Send valid Max Lives Per Customer.");
				codeDTO.setError(error);
				return codeDTO;
			}
			try {
				maxThreshold = Integer.parseInt(maxThresholdStr);
			} catch (Exception e) {
				e.printStackTrace();
				codeDTO.setStatus(0);
				error.setDescription("Please Send valid Max Total Lives Threshold.");
				codeDTO.setError(error);
				return codeDTO;
			}
			try {
				startDate += " 00:00:00";
				promoStartDate = com.rtw.tmat.utils.Util.getDateWithTwentyFourHourFormat1(startDate);
			} catch (Exception e) {
				e.printStackTrace();
				codeDTO.setStatus(0);
				error.setDescription("Please Send valid earn lives promo Start Date.");
				codeDTO.setError(error);
				return codeDTO;
			}
			try {
				endDate += " 23:59:59";
				promoEndDate = com.rtw.tmat.utils.Util.getDateWithTwentyFourHourFormat1(endDate);
			} catch (Exception e) {
				e.printStackTrace();
				codeDTO.setStatus(0);
				error.setDescription("Please Send valid earn lives promo End Date.");
				codeDTO.setError(error);
				return codeDTO;
			}
			
			if(action.equalsIgnoreCase("SAVE")){
				if(promoCode == null || promoCode.isEmpty()){
					promoCode = com.rtw.tmat.utils.Util.generateCustomerReferalCode();
				}
				List<PromotionalEarnLifeOffer> offs = DAORegistry.getPromotionalEarnLifeOfferDAO().getPromotionalEarLifeByCode(promoCode);
				if(!offs.isEmpty()){
					codeDTO.setStatus(0);
					error.setDescription("Same Earn Lives Promocode Already Exist in System.");
					codeDTO.setError(error);
					return codeDTO;
				}
				PromotionalEarnLifeOffer offer = new PromotionalEarnLifeOffer();
				offer.setCreatedBy(userName);
				offer.setCreatedDate(new Date());
				offer.setEndDate(promoEndDate);
				offer.setStartDate(promoStartDate);
				offer.setMaxAccumulateThreshold(maxThreshold);
				offer.setMaxLifePerCustomer(lifePerCustomer);
				offer.setCurAccumulatedCount(0);
				offer.setStatus("ACTIVE");
				offer.setPromoCode(promoCode);
				DAORegistry.getPromotionalEarnLifeOfferDAO().save(offer);
				codeDTO.setMessage("Earn Live promotional code created successfully");
			}else if(action.equalsIgnoreCase("UPDATE")){
				if(promoId == null || promoId.isEmpty()){
					codeDTO.setStatus(0);
					error.setDescription("Please send earn lives promotional code id to be updated.");
					codeDTO.setError(error);
					return codeDTO;
				}
				Integer promotionalId = 0;
				try {
					promotionalId = Integer.parseInt(promoId);
				} catch (Exception e) {
					e.printStackTrace();
					codeDTO.setStatus(0);
					error.setDescription("Please Send valid earn lives Promotional code Id.");
					codeDTO.setError(error);
					return codeDTO;
				}
				PromotionalEarnLifeOffer offer = DAORegistry.getPromotionalEarnLifeOfferDAO().get(promotionalId);
				if(offer == null){
					codeDTO.setStatus(0);
					error.setDescription("Earn Lives Promotional Offer not found in system with given Id.");
					codeDTO.setError(error);
					return codeDTO;
				}
				
				offer.setEndDate(promoEndDate);
				offer.setMaxAccumulateThreshold(maxThreshold);
				offer.setMaxLifePerCustomer(lifePerCustomer);
				offer.setStartDate(promoStartDate);
				offer.setUpdatedBy(userName);
				offer.setUpdatedDate(new Date());
				DAORegistry.getPromotionalEarnLifeOfferDAO().update(offer);
				codeDTO.setMessage("Earn Lives Promotional Offer updated successfully");
			}
			GridHeaderFilters filter = GridHeaderFiltersUtil.getContestPromoOfferFiter(null);
			List<PromotionalEarnLifeOffer> offers = DAORegistry.getPromotionalEarnLifeOfferDAO().getAllContestPromocodes(null, filter, null, null);
			codeDTO.setDiscountCodePaginationDTO(PaginationUtil.getDummyPaginationParameters(offers!=null?offers.size():0));
			codeDTO.setStatus(1);
			codeDTO.setPromoOffers(offers);
			return codeDTO;
		} catch (Exception e) {
			e.printStackTrace();
			codeDTO.setStatus(0);
			error.setDescription("Error occured while updating Earn Lives Promotional Offer.");
			codeDTO.setError(error);
			return codeDTO;
		}
	}
	
	
	
	@RequestMapping(value="/ContestCustomerQuestions")
	public CustomerQuestionDTO getCustomerQuestions(HttpServletRequest request, HttpServletResponse response){
		CustomerQuestionDTO questionDTO = new CustomerQuestionDTO();
		Error error = new Error();
		String headerFilter = request.getParameter("headerFilter");
		String status = request.getParameter("status");
		try {
			GridHeaderFilters filter = GridHeaderFiltersUtil.getCustomerQuestionFilter(headerFilter);
			List<CustomerContestQuestion> questions = QuizDAORegistry.getCustomerContestQuestionDAO().getAllCustomerQuestion(filter, status);
			questionDTO.setQuestionPagination(PaginationUtil.getDummyPaginationParameters(questions!=null?questions.size():0));
			questionDTO.setCustomerQuestions(questions);
			questionDTO.setStatus(1);
			return questionDTO;
		} catch (Exception e) {
			e.printStackTrace();
			questionDTO.setStatus(0);
			error.setDescription("Error occured while fetching contest promotional Offers/codes.");
			questionDTO.setError(error);
			return questionDTO;
		}
	}
	
	
	
	@RequestMapping(value="/UpdateSubmitTrivia")
	public CustomerQuestionDTO updateSubmitTrivia(HttpServletRequest request, HttpServletResponse response){
		CustomerQuestionDTO questionDTO = new CustomerQuestionDTO();
		Error error = new Error();
		String headerFilter = request.getParameter("headerFilter");
		String status = request.getParameter("status");
		String action = request.getParameter("action");
		String ids = request.getParameter("ids");
		String userName = request.getParameter("userName");
		try {
			if(action == null || action.isEmpty()){
				questionDTO.setStatus(0);
				error.setDescription("Please send valid action to perform.");
				questionDTO.setError(error);
				return questionDTO;
			}
			if(ids == null || ids.isEmpty()){
				questionDTO.setStatus(0);
				error.setDescription("Please select questions to update.");
				questionDTO.setError(error);
				return questionDTO;
			}
			String arr[] = null;
			
			if(ids.contains(",")){
				arr = ids.split(",");
			}
			
			
			if(action.equalsIgnoreCase("MOVE")){
				if(arr!=null && arr.length > 0){
					for(String a : arr){
						if(a.trim().isEmpty()){
							continue;
						}
						Integer id = null;
						try {
							id = Integer.parseInt(a);
							CustomerContestQuestion cq = QuizDAORegistry.getCustomerContestQuestionDAO().get(id);
							if(cq==null){
								continue;
							}
							QuestionBank q = new QuestionBank();
							q.setAnswer(cq.getAnswer());
							q.setCategory(null);
							q.setCreatedBy(cq.getSubmittedBy());
							q.setCreatedDate(cq.getCreatedDate());
							q.setUpdatedBy(userName);
							q.setUpdatedDate(new Date());
							q.setDificultyLevel("Moderate");
							q.setFactChecked(false);
							q.setOptionA(cq.getOptionA());
							q.setOptionB(cq.getOptionB());
							q.setOptionC(cq.getOptionC());
							q.setQuestion(cq.getQuestionText());
							q.setQuestionReward(0.05);
							q.setStatus("ACTIVE");
							
							QuizDAORegistry.getQuestionBankDAO().saveOrUpdate(q);
							cq.setStatus("USED");
							QuizDAORegistry.getCustomerContestQuestionDAO().update(cq);
							questionDTO.setMessage("Selected Question Moved to Question Bank Successfully.");
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						
					}
				}else{
					Integer id = null;
					try {
						id = Integer.parseInt(ids);
						CustomerContestQuestion cq = QuizDAORegistry.getCustomerContestQuestionDAO().get(id);
						if(cq==null){
							questionDTO.setStatus(0);
							error.setDescription("Question is not found in system with given identifier.");
							questionDTO.setError(error);
							return questionDTO;
						}
						QuestionBank q = new QuestionBank();
						q.setAnswer(cq.getAnswer());
						q.setCategory(null);
						q.setCreatedBy(cq.getSubmittedBy());
						q.setCreatedDate(cq.getCreatedDate());
						q.setUpdatedBy(userName);
						q.setUpdatedDate(new Date());
						q.setDificultyLevel("Moderate");
						q.setFactChecked(false);
						q.setOptionA(cq.getOptionA());
						q.setOptionB(cq.getOptionB());
						q.setOptionC(cq.getOptionC());
						q.setQuestion(cq.getQuestionText());
						q.setQuestionReward(0.05);
						q.setStatus("ACTIVE");
						
						QuizDAORegistry.getQuestionBankDAO().saveOrUpdate(q);
						cq.setStatus("USED");
						QuizDAORegistry.getCustomerContestQuestionDAO().update(cq);
						questionDTO.setMessage("Selected Question Moved to Question Bank Successfully.");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}else if(action.equalsIgnoreCase("DELETE")){

				if(arr!=null && arr.length > 0){
					for(String a : arr){
						if(a.trim().isEmpty()){
							continue;
						}
						Integer id = null;
						try {
							id = Integer.parseInt(a);
							CustomerContestQuestion cq = QuizDAORegistry.getCustomerContestQuestionDAO().get(id);
							if(cq==null){
								continue;
							}
							cq.setStatus("DELETED");
							QuizDAORegistry.getCustomerContestQuestionDAO().update(cq);
							questionDTO.setMessage("Selected Question Deleted Successfully.");
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}else{
					Integer id = null;
					try {
						id = Integer.parseInt(ids);
						CustomerContestQuestion cq = QuizDAORegistry.getCustomerContestQuestionDAO().get(id);
						if(cq==null){
							questionDTO.setStatus(0);
							error.setDescription("Question is not found in system with given identifier.");
							questionDTO.setError(error);
							return questionDTO;
						}
						cq.setStatus("DELETED");
						QuizDAORegistry.getCustomerContestQuestionDAO().update(cq);
						questionDTO.setMessage("Selected Question Deleted Successfully.");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			
			}
			GridHeaderFilters filter = GridHeaderFiltersUtil.getCustomerQuestionFilter(headerFilter);
			List<CustomerContestQuestion> questions = QuizDAORegistry.getCustomerContestQuestionDAO().getAllCustomerQuestion(filter, status);
			questionDTO.setQuestionPagination(PaginationUtil.getDummyPaginationParameters(questions!=null?questions.size():0));
			questionDTO.setCustomerQuestions(questions);
			questionDTO.setStatus(1);
			return questionDTO;
		} catch (Exception e) {
			e.printStackTrace();
			questionDTO.setStatus(0);
			error.setDescription("Error occured while fetching contest promotional Offers/codes.");
			questionDTO.setError(error);
			return questionDTO;
		}
	}
	
	
	
	
	
	@RequestMapping(value="/GetPreContestChecklistLogs")
	public ContestChecklistTrackingDTO getContestChecklistLogs(HttpServletRequest request, HttpServletResponse response){
		ContestChecklistTrackingDTO checkListDTO = new ContestChecklistTrackingDTO();
		Error error = new Error();
		String headerFilter = request.getParameter("headerFilter");
		String preContestId = request.getParameter("preContestId");
		try {
			GridHeaderFilters filter = GridHeaderFiltersUtil.getPreHeaderContestChecklistFilter(headerFilter);
			List<PreContestChecklistTracking> contestChecklistLogs = QuizDAORegistry.getPreContestChecklistTrackingDAO().getPreContestChecklistTrackingbyFilters(preContestId, filter, sharedProperty);
			checkListDTO.setTrackings(contestChecklistLogs);
			checkListDTO.setTrackingPagination(PaginationUtil.getDummyPaginationParameters(contestChecklistLogs.size()));
			checkListDTO.setStatus(1);
			return checkListDTO;
		} catch (Exception e) {
			e.printStackTrace();
			checkListDTO.setStatus(0);
			error.setDescription("Error occured while fetching contest checklist Logs.");
			checkListDTO.setError(error);
			return checkListDTO;
		}
	}
	
	
	@RequestMapping(value="/PreContestChecklist")
	public ContestChecklistDTO getContestChecklist(HttpServletRequest request, HttpServletResponse response){
		ContestChecklistDTO checkListDTO = new ContestChecklistDTO();
		Error error = new Error();
		String headerFilter = request.getParameter("headerFilter");
		String action = request.getParameter("action");
		String status = request.getParameter("status");
		String type = request.getParameter("type");
		try {
			if(action!=null &&  action.equals("GET")){
				GridHeaderFilters filter = GridHeaderFiltersUtil.getPreHeaderContestChecklistFilter(headerFilter);
				List<PreContestChecklist> contestChecklists = QuizDAORegistry.getContestChecklistDAO().getPreContestChecklistbyFilters(status,filter,sharedProperty,type);
				checkListDTO.setPreContestChecklists(contestChecklists);
				checkListDTO.setPreContestPaginationDTO(PaginationUtil.getDummyPaginationParameters(contestChecklists.size()));
				checkListDTO.setStatus(1);
				return checkListDTO;
			}else if(action!=null &&  action.equals("ADD")){
				List<PreContestChecklist> contestChecklists = QuizDAORegistry.getContestChecklistDAO().getAllActivePreContestChecklistByType(null,type);
				checkListDTO.setPreContestChecklists(contestChecklists);
				checkListDTO.setStatus(1);
				return checkListDTO;
			}else{
				GridHeaderFilters filter = GridHeaderFiltersUtil.getPreHeaderContestChecklistFilter(headerFilter);
				List<PreContestChecklist> contestChecklists = QuizDAORegistry.getContestChecklistDAO().getPreContestChecklistbyFilters(status,filter,sharedProperty,type);
				List<TrackerUser> users = DAORegistry.getTrackerUserDAO().getAllTrackerUsersByRole("ROLE_CONTEST");
				checkListDTO.setPreContestChecklists(contestChecklists);
				checkListDTO.setPreContestPaginationDTO(PaginationUtil.getDummyPaginationParameters(contestChecklists.size()));
				checkListDTO.setUsers(users);
				checkListDTO.setStatus(1);
				return checkListDTO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			checkListDTO.setStatus(0);
			error.setDescription("Error occured while fetching contest checklist.");
			checkListDTO.setError(error);
			return checkListDTO;
		}
	}
	
	
	@RequestMapping(value="/UpdatePreContestChecklist")
	public ContestChecklistDTO updatePreContestChecklist(HttpServletRequest request, HttpServletResponse response){
		ContestChecklistDTO checkListDTO = new ContestChecklistDTO();
		Error error = new Error();
		
		String preContestIdStr = request.getParameter("preContestId");
		String action = request.getParameter("action");
		String status = request.getParameter("status");
		String type = request.getParameter("type");
		
		String contestName = request.getParameter("contestName");
		String contestCategory = request.getParameter("contestCategory");
		String participantStar = request.getParameter("participantStar");
		String referralStar = request.getParameter("referralStar");
		String extendedName = request.getParameter("extendedName");
		String noOfQuestion = request.getParameter("noOfQuestion");
		String zone = request.getParameter("zone");
		String dateOfGame = request.getParameter("dateOfGame");
		String timeOfGame1 = request.getParameter("timeOfGame1");
		String timeOfGame2 = request.getParameter("timeOfGame2");
		String rewardDollarsPrizeStr = request.getParameter("rewardDollarsPrize");
		String grandPrizeEvent = request.getParameter("grandPrizeEvent");
		String noOfTicketsStr = request.getParameter("noOfTickets");
		String noOfGrandWinnerStr = request.getParameter("noOfGrandWinner");
		String category = request.getParameter("category");
		String categoryIdStr = request.getParameter("categoryId");
		String categoryType = request.getParameter("categoryType");
		String category1 = request.getParameter("category1");
		String categoryIdStr1 = request.getParameter("categoryId1");
		String categoryType1 = request.getParameter("categoryType1");
		String pricePerTicketStr = request.getParameter("pricePerTicket");
		String discountCode = request.getParameter("discountCode");
		String discountPercentageStr = request.getParameter("discountPercentage");
		
		
		String hostName = request.getParameter("hostName");
		String producerName = request.getParameter("producerName");
		String directorName = request.getParameter("directorName");
		String techDirectorName = request.getParameter("techDirectorName");
		String opearatorName = request.getParameter("opearatorName");
		String isQuestionEnteredStr = request.getParameter("isQuestionEntered");
		String questionEnteredByStr = request.getParameter("questionEnteredBy");
		String isQuestionVerifiedStr = request.getParameter("isQuestionVerified");
		String questionVerifiedByStr = request.getParameter("questionVerifiedBy");
		String rewardDollarsPerQueStr = request.getParameter("rewardDollarsPerQue");
		String approvedbyStr = request.getParameter("approvedBy");
		String userName = request.getParameter("userName");
		
		try {
			if(action!=null && action.equalsIgnoreCase("EDIT")){
				if(preContestIdStr == null || preContestIdStr.isEmpty()){
					System.err.println("Please send Pre Contest Checklist Id.");
					error.setDescription("Please send Pre Contest Checklist Id.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				Integer preContestId = null;
				try {
					preContestId = Integer.parseInt(preContestIdStr);
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("Please send valid Contest id.");
					error.setDescription("Please send valid Contest id.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				
				PreContestChecklist preContest = QuizDAORegistry.getContestChecklistDAO().get(preContestId);
				if(preContest == null){
					System.err.println("Pre Contest Checklist not found in system with given id.");
					error.setDescription("Pre Contest Checklist not found in system with given id.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				preContest.getHours();
				preContest.getMinutes();
				checkListDTO.setPreContestChecklist(preContest);
				checkListDTO.setStatus(1);
				return checkListDTO;
			}else if(action!=null && (action.equalsIgnoreCase("SAVE") ||  action.equalsIgnoreCase("UPDATE"))){
				PreContestChecklist preContest = null;
				String msg = "";
				if(action.equalsIgnoreCase("UPDATE")){
					msg = "Contest checklist updated successfully.";
					if(preContestIdStr == null || preContestIdStr.isEmpty()){
						System.err.println("Please send Pre Contest Checklist Id.");
						error.setDescription("Please send Pre Contest Checklist Id.");
						checkListDTO.setError(error);
						checkListDTO.setStatus(0);
						return checkListDTO;
					}
					Integer preContestId = null;
					try {
						preContestId = Integer.parseInt(preContestIdStr);
					} catch (Exception e) {
						e.printStackTrace();
						System.err.println("Please send valid Contest id.");
						error.setDescription("Please send valid Contest id.");
						checkListDTO.setError(error);
						checkListDTO.setStatus(0);
						return checkListDTO;
					}
					
					preContest = QuizDAORegistry.getContestChecklistDAO().get(preContestId);
					if(preContest == null){
						System.err.println("Pre Contest Checklist not found in system with given id.");
						error.setDescription("Pre Contest Checklist not found in system with given id.");
						checkListDTO.setError(error);
						checkListDTO.setStatus(0);
						return checkListDTO;
					}
				}else if(action.equalsIgnoreCase("SAVE")){
					msg = "Contest checklist saved successfully.";
					preContest = new PreContestChecklist();
					preContest.setStatus("ACTIVE");
				}
				if(contestName==null || contestName.isEmpty()){
					System.err.println("Please provide Contest Name.");
					error.setDescription("Please provide Contest Name.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				if(extendedName==null || extendedName.isEmpty()){
					System.err.println("Please provide Contest Extended Name.");
					error.setDescription("Please provide Contest Extended Name.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				if(dateOfGame==null || dateOfGame.isEmpty()){
					System.err.println("Please provide Date Of Game.");
					error.setDescription("Please provide Date Of Game.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				if(timeOfGame1==null || timeOfGame1.isEmpty()){
					System.err.println("Please provide Time(Hour) of Game.");
					error.setDescription("Please provide Time(Hour) of Game.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				if(timeOfGame2==null || timeOfGame2.isEmpty()){
					System.err.println("Please provide Time(Minutes) of Game.");
					error.setDescription("Please provide Time(Minutes) of Game.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				if(contestCategory==null || contestCategory.isEmpty()){
					System.err.println("Please provide Contest Category.");
					error.setDescription("Please provide Contest Category.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				if(participantStar==null || participantStar.isEmpty()){
					System.err.println("Please provide contest participant super fan game star.");
					error.setDescription("Please provide contest participant super fan game star.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				if(referralStar==null || referralStar.isEmpty()){
					System.err.println("Please provide contest referral super fan game star.");
					error.setDescription("Please provide contest referral super fan game star.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				
				dateOfGame = dateOfGame+" "+timeOfGame1+":"+timeOfGame2+":00";
				Date startDate = null;
				try {
					startDate = Util.getDateWithTwentyFourHourFormat1(dateOfGame);
				} catch (Exception e) {
					System.err.println("Please provide valid date of game.");
					error.setDescription("Please provide valid date of game.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				
				if(rewardDollarsPrizeStr==null || rewardDollarsPrizeStr.isEmpty()){
					System.err.println("Please provide Reward Dollars Prize.");
					error.setDescription("Please provide Reward Dollars Prize.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				Double RewardDollarPrize = 0.00;
				try {
					RewardDollarPrize = Double.parseDouble(rewardDollarsPrizeStr);
				} catch (Exception e) {
					System.err.println("Please provide valid Reward Dollars Prize.");
					error.setDescription("Please provide valid Reward Dollars Prize.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				/*if(grandPrizeEvent==null || grandPrizeEvent.isEmpty()){
					System.err.println("Please provide Grand Proze Event.");
					error.setDescription("Please provide Grand Proze Event.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}*/
				if(noOfTicketsStr==null || noOfTicketsStr.isEmpty()){
					System.err.println("Please provide Grand Winner Ticket Prize.");
					error.setDescription("Please provide Grand Winner Ticket Prize.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				Integer noOfTickets = 0;
				try {
					noOfTickets = Integer.parseInt(noOfTicketsStr);
				} catch (Exception e) {
					System.err.println("Please provide valid Grand Winner Ticket Prize.");
					error.setDescription("Please provide valid Grand Winner Ticket Prize.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				if(noOfQuestion==null || noOfQuestion.isEmpty()){
					System.err.println("Please provide No of Questions.");
					error.setDescription("Please provide No of Questions.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				Integer noOfQuestions = 0;
				Integer participantStars = 0;
				Integer referralStars = 0;
				try {
					noOfQuestions = Integer.parseInt(noOfQuestion);
				} catch (Exception e) {
					System.err.println("Please provide No of Questions as valid Number.");
					error.setDescription("Please provide No of Questions as valid Number.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				try {
					participantStars = Integer.parseInt(participantStar);
				} catch (Exception e) {
					System.err.println("Please provide No of super fan game participant star as valid Number.");
					error.setDescription("Please provide No of super fan game participant star as valid Number.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				try {
					referralStars = Integer.parseInt(referralStar);
				} catch (Exception e) {
					System.err.println("Please provide No of super fan game referral star as valid Number.");
					error.setDescription("Please provide No of super fan game referral star as valid Number.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				if(noOfGrandWinnerStr==null || noOfGrandWinnerStr.isEmpty()){
					System.err.println("Please provide # of Grand Winner.");
					error.setDescription("Please provide # of Grand Winner.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				Integer noOfGrandWinner = 0;
				try {
					noOfGrandWinner = Integer.parseInt(noOfGrandWinnerStr);
				} catch (Exception e) {
					System.err.println("Please provide valid # of Grand Winner.");
					error.setDescription("Please provide valid # of Grand Winner.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				if(category==null || category.isEmpty()){
					System.err.println("Please provide Category.");
					error.setDescription("Please provide Category.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				if(category1==null || category1.isEmpty()){
					System.err.println("Please provide Promotional code Category.");
					error.setDescription("Please provide Promotional code Category.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				if(category != null && category.equalsIgnoreCase("All")){
					categoryIdStr = "0";
				}
				if(category1 != null && category1.equalsIgnoreCase("All")){
					categoryIdStr1 = "0";
				}
				if(categoryIdStr==null || categoryIdStr.isEmpty()){
					System.err.println("Please select category.");
					error.setDescription("Please select category.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				if(categoryIdStr1==null || categoryIdStr1.isEmpty()){
					System.err.println("Please select promotional code category.");
					error.setDescription("Please select promotional code category.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				Integer categoryId = 0;
				try {
					categoryId = Integer.parseInt(categoryIdStr);
				} catch (Exception e) {
					System.err.println("Please select valid category.");
					error.setDescription("Please select valid category.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				Integer categoryId1 = 0;
				try {
					categoryId1 = Integer.parseInt(categoryIdStr1);
				} catch (Exception e) {
					System.err.println("Please select valid promotional code category.");
					error.setDescription("Please select valid promotional code category.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				if(categoryType==null || categoryType.isEmpty()){
					System.err.println("Please select category.");
					error.setDescription("Please select category.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				if(categoryType1==null || categoryType1.isEmpty()){
					System.err.println("Please select promotional code category.");
					error.setDescription("Please select promotional code category.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				if(pricePerTicketStr==null || pricePerTicketStr.isEmpty()){
					System.err.println("Please provide Price Per Ticket.");
					error.setDescription("Please provide Price Per Ticket.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				Double pricePerTicket = 0.0;
				try {
					pricePerTicket = Double.parseDouble(pricePerTicketStr);
				} catch (Exception e) {
					System.err.println("Please provide valid Price Per Ticket.");
					error.setDescription("Please provide valid Price Per Ticket.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				if(discountCode==null || discountCode.isEmpty()){
					System.err.println("Please provide Discount code.");
					error.setDescription("Please provide Discount code.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				if(discountPercentageStr==null || discountPercentageStr.isEmpty()){
					System.err.println("Please provide Discount Price for discount code.");
					error.setDescription("Please provide Discount Price for discount code.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				Double discountPercentage = 0.0;
				try {
					discountPercentage = Double.parseDouble(discountPercentageStr);
				} catch (Exception e) {
					System.err.println("Please provide valid Discount Price for discount code.");
					error.setDescription("Please provide valid Discount Price for discount code.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				preContest.setContestName(contestName);
				preContest.setExtendedName(extendedName);
				preContest.setQuestionSize(noOfQuestions);
				preContest.setZone(zone);
				preContest.setGrandPrizeEvent(grandPrizeEvent);
				preContest.setContestDateTime(startDate);
				preContest.setRewardDollarsPrize(RewardDollarPrize);
				preContest.setGrandWinnerTicketPrize(noOfTickets);
				preContest.setGrandPrizeWinner(noOfGrandWinner);
				preContest.setCategoryType(categoryType);
				preContest.setContestCategory(contestCategory);
				preContest.setParticipantStar(participantStars);
				preContest.setReferralStar(referralStars);
				preContest.setCategoryId(categoryId);
				preContest.setCategoryName(category);
				preContest.setPromoCategoryType(categoryType1);
				preContest.setPromoCategoryId(categoryId1);;
				preContest.setPromoCategoryName(category1);
				preContest.setPricePerTicket(pricePerTicket);
				preContest.setDiscountCode(discountCode);
				preContest.setDiscountPercentage(discountPercentage);
				preContest.setCreatedDate(new Date());
				preContest.setType(type);
				preContest.setCreatedBy(userName);
				preContest.setHostName(hostName);
				preContest.setProducerName(producerName);
				preContest.setDirectorName(directorName);
				preContest.setTechnicalDirectorName(techDirectorName);
				preContest.setBackendOperatorName(opearatorName);
				if(isQuestionEnteredStr!=null && (isQuestionEnteredStr.equalsIgnoreCase("true") || isQuestionEnteredStr.equalsIgnoreCase("on"))){
					preContest.setIsQuestionEntered(true);
					try {
						preContest.setQuestionEnteredBy(Integer.parseInt(questionEnteredByStr));
					} catch (Exception e) {
						e.printStackTrace();
						System.err.println("Please select valid user for question entered by.");
						error.setDescription("Please select valid user for question entered by.");
						checkListDTO.setError(error);
						checkListDTO.setStatus(0);
						return checkListDTO;
					}
				}else{
					preContest.setIsQuestionEntered(false);
					preContest.setQuestionEnteredBy(null);
				}
				
				if(isQuestionVerifiedStr!=null && (isQuestionVerifiedStr.equalsIgnoreCase("true") || isQuestionVerifiedStr.equalsIgnoreCase("on"))){
					preContest.setIsQuestionVerified(true);
					try {
						preContest.setQuestionVerifiedBy(Integer.parseInt(questionVerifiedByStr));
					} catch (Exception e) {
						e.printStackTrace();
						System.err.println("Please select valid user for question varified by.");
						error.setDescription("Please select valid user for question varified by.");
						checkListDTO.setError(error);
						checkListDTO.setStatus(0);
						return checkListDTO;
					}
				}else{
					preContest.setIsQuestionVerified(false);
					preContest.setQuestionVerifiedBy(null);
				}
				
				if(rewardDollarsPerQueStr!=null){
					Double rewardPerQue =null;
					try {
						rewardPerQue = Double.parseDouble(rewardDollarsPerQueStr);
					} catch (Exception e) {
						e.printStackTrace();
						System.err.println("Please send valid reward dollars per question.");
						error.setDescription("Please send valid reward dollars per question.");
						checkListDTO.setError(error);
						checkListDTO.setStatus(0);
						return checkListDTO;
					}
					preContest.setRewardDollarsPerQuestion(rewardPerQue);
				}
				
				if(approvedbyStr!=null){
					try {
						preContest.setApprovedBy(Integer.parseInt(approvedbyStr));
					} catch (Exception e) {
						e.printStackTrace();
						System.err.println("Please select valid user for Approved By.");
						error.setDescription("Please select valid user for Approved By.");
						checkListDTO.setError(error);
						checkListDTO.setStatus(0);
						return checkListDTO;
					}
				}
				preContest.setUpdatedBy(userName);
				preContest.setUpdatedDate(new Date());
				
				
				if(action.equalsIgnoreCase("UPDATE") && preContest.getContestId()!=null && preContest.getId()!=null){
					Contests contest = QuizDAORegistry.getContestsDAO().get(preContest.getContestId());
					if(contest.getStatus().equalsIgnoreCase("STARTED")){
						checkListDTO.setStatus(1);
						checkListDTO.setMessage("Contest associated with selected pre contest checklist already started, not allowed to update pre contest checklist or contest now.");
						GridHeaderFilters filter = GridHeaderFiltersUtil.getPreHeaderContestChecklistFilter(null);
						List<PreContestChecklist> contestChecklists = QuizDAORegistry.getContestChecklistDAO().getPreContestChecklistbyFilters(status,filter,sharedProperty,type);
						checkListDTO.setPreContestPaginationDTO(PaginationUtil.getDummyPaginationParameters(contestChecklists.size()));
						checkListDTO.setPreContestChecklists(contestChecklists);
						return checkListDTO;
					}
					
					Date toDate = new Date();
				    Calendar calendar = Calendar.getInstance();
				    calendar.setTime(startDate);
				    calendar.add(Calendar.HOUR, 24);		     
				    toDate = calendar.getTime();
					
				    contest.setContestName(contestName);
				    contest.setExtendedName(extendedName);
				    contest.setQuestionSize(noOfQuestions);
				    contest.setZone(zone);
					contest.setDiscountPercentage(discountPercentage);
					contest.setFreeTicketPerWinner(noOfTickets);
					contest.setIsCustomerStatUpdated(false);
					contest.setPromoRefId(categoryId1);
					contest.setPromoRefName(category1);
					contest.setPromoRefType(categoryType1);
					contest.setPromotionalCode(discountCode);
					contest.setRewardPoints(RewardDollarPrize);
					contest.setSingleTicketPrice(pricePerTicket);
					contest.setStartDateTime(startDate);
					contest.setTicketWinnerThreshhold(noOfGrandWinner);
					contest.setUpdatedBy(userName);
					contest.setUpdatedDate(new Date());
					contest.setPromoExpiryDate(toDate);
					contest.setContestCategory(contestCategory);
					contest.setParticipantStars(participantStars);
					contest.setReferralStars(referralStars);
					
					
					RTFPromoOffers offer = DAORegistry.getRtfPromoOffersDAO().get(contest.getRtfPromoOfferId());
					List<RTFPromoType> promoTypeOffers = DAORegistry.getRtfPromoTypeDAO().getPromoTypeFromOfferId(contest.getRtfPromoOfferId());
					
					if(offer==null){
						offer = new RTFPromoOffers();
					}
					
					offer.setStartDate(startDate);
					offer.setEndDate(toDate);
					offer.setPromoCode(discountCode);
					offer.setDiscountPer(discountPercentage);
					offer.setMobileDiscountPerc(discountPercentage);
					offer.setMaxOrders(1000);
					offer.setNoOfOrders(0);
					offer.setCreatedBy(userName);
					offer.setCreatedDate(new Date());
					offer.setModifiedBy(userName);
					offer.setModifiedDate(new Date());
					offer.setFlatOfferOrderThreshold(0.00);					
					offer.setStatus("ENABLED");
					offer.setIsFlatDiscount(false);
						
					RTFPromoType offerType = new RTFPromoType();
					offerType.setPromoOfferId(contest.getRtfPromoOfferId());
					if(categoryType1.equalsIgnoreCase("All")){
						offerType.setPromoType(PromotionalType.ALL.toString());
					}else if(categoryType1.equalsIgnoreCase("ARTIST")){
						offerType.setArtistId(categoryId1);
						offerType.setPromoType(PromotionalType.ARTIST.toString());
					}else if(categoryType1.equalsIgnoreCase("EVENT")){
						offerType.setEventId(categoryId1);
						offerType.setPromoType(PromotionalType.EVENT.toString());
					}else if(categoryType1.equalsIgnoreCase("GRAND")){
						offerType.setGrandChildId(categoryId1);
						offerType.setPromoType(PromotionalType.GRANDCHILD.toString());
					}else if(categoryType1.equalsIgnoreCase("CHILD")){
						offerType.setChildId(categoryId1);
						offerType.setPromoType(PromotionalType.CHILD.toString());
					}else if(categoryType1.equalsIgnoreCase("PARENT")){
						offerType.setParentId(categoryId1);
						offerType.setPromoType(PromotionalType.PARENT.toString());
					}
					
					DAORegistry.getRtfPromoTypeDAO().deleteAll(promoTypeOffers);
					DAORegistry.getRtfPromoOffersDAO().saveOrUpdate(offer);
					offerType.setPromoOfferId(offer.getId());
					DAORegistry.getRtfPromoTypeDAO().saveOrUpdate(offerType);
					contest.setRtfPromoOfferId(offer.getId());
					QuizDAORegistry.getContestsDAO().update(contest);
					
				}
				
				
				PreContestChecklistTracking preContestTracking = new PreContestChecklistTracking(preContest);
				QuizDAORegistry.getContestChecklistDAO().saveOrUpdate(preContest);
				QuizDAORegistry.getPreContestChecklistTrackingDAO().save(preContestTracking);
				checkListDTO.setStatus(1);
				checkListDTO.setMessage(msg);
			}else if(action!=null && action.equalsIgnoreCase("DELETE")){
				if(preContestIdStr == null || preContestIdStr.isEmpty()){
					System.err.println("Please send Pre Contest Checklist Id.");
					error.setDescription("Please send Pre Contest Checklist Id.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				Integer preContestId = null;
				try {
					preContestId = Integer.parseInt(preContestIdStr);
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("Please send valid Contest id.");
					error.setDescription("Please send valid Contest id.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				
				PreContestChecklist preContest = QuizDAORegistry.getContestChecklistDAO().get(preContestId);
				if(preContest == null){
					System.err.println("Pre Contest Checklist not found in system with given id.");
					error.setDescription("Pre Contest Checklist not found in system with given id.");
					checkListDTO.setError(error);
					checkListDTO.setStatus(0);
					return checkListDTO;
				}
				preContest.setStatus("DELETE");
				preContest.setUpdatedBy(userName);
				preContest.setUpdatedDate(new Date());
				QuizDAORegistry.getContestChecklistDAO().update(preContest);
				checkListDTO.setStatus(1);
				checkListDTO.setMessage("Pre Contest Checklist deleted successfully.");
			}
			GridHeaderFilters filter = GridHeaderFiltersUtil.getPreHeaderContestChecklistFilter(null);
			List<PreContestChecklist> contestChecklists = QuizDAORegistry.getContestChecklistDAO().getPreContestChecklistbyFilters(status,filter,sharedProperty,type);
			checkListDTO.setPreContestPaginationDTO(PaginationUtil.getDummyPaginationParameters(contestChecklists.size()));
			checkListDTO.setPreContestChecklists(contestChecklists);
			return checkListDTO;
		} catch (Exception e) {
			e.printStackTrace();
			checkListDTO.setStatus(0);
			error.setDescription("Error occured while Updating Pre contest checklist.");
			checkListDTO.setError(error);
			return checkListDTO;
		}
	}
	
	
	@RequestMapping(value="/getUpcomingContestDetails")
	public ContestsDTO getNextUpcomingContestData(HttpServletRequest request, HttpServletResponse response){
		ContestsDTO contestsDTO = new ContestsDTO();
		Error error = new Error();
		try {
			String type = request.getParameter("type"); 
			if(type == null || type.isEmpty()){
				System.err.println("Please send contest type.");
				error.setDescription("Please send contest type.");
				contestsDTO.setError(error);
				contestsDTO.setStatus(0);
				return contestsDTO;
			}
			ContestConfigSettings setting = QuizDAORegistry.getContestConfigSettingsDAO().getContestConfigSettings();
			if(setting==null){
				System.err.println("Contest settings are not found in database, please add contest setting properties.");
				error.setDescription("Contest settings are not found in database, please add contest setting properties.");
				contestsDTO.setError(error);
				contestsDTO.setStatus(0);
				return contestsDTO;
			}
			GridHeaderFilters filter = GridHeaderFiltersUtil.getContestsFilter(null); 
			List<Contests> contestsList = QuizDAORegistry.getContestsDAO().getAllContests(null, filter,type,"'ACTIVE','STARTED'",sharedProperty);
			Contests contest = QuizDAORegistry.getContestsDAO().getTodaysContest(type,"'STARTED'");
			if(contest != null){
				contestsDTO.setContestStarted(true);
			}else{
				contestsDTO.setContestStarted(false);
			}
			if(!setting.getShowUpcomingContest()){
				contestsDTO.setStatus(1);
				return contestsDTO;
			}
			contestsDTO.setStatus(1);			
			contestsDTO.setContestsList(contestsList);
			return contestsDTO;
		} catch (Exception e) {
			e.printStackTrace();
			contestsDTO.setStatus(0);
			error.setDescription("Error occured while getting upcoming contest details to update on firebase.");
			contestsDTO.setError(error);
			return contestsDTO;
		}
	
	}
	
	
	
	@RequestMapping(value="/getContestServerNodeUrls")
	public ContestServerNodesDTO getContestServerNodeUrls(HttpServletRequest request, HttpServletResponse response){
		ContestServerNodesDTO nodeDTO = new ContestServerNodesDTO();
		Error error = new Error();
		try {
			List<RtfConfigContestClusterNodes> nodes = DAORegistry.getQueryManagerDAO().getRtfContestNodesUrls();
			if(nodes.isEmpty()){
				nodeDTO.setStatus(0);
				error.setDescription("No Servers Nodes are configured in database.");
				nodeDTO.setError(error);
				return nodeDTO;
			}
			nodeDTO.setStatus(1);
			nodeDTO.setNodes(nodes);
			return nodeDTO;
		} catch (Exception e) {
			e.printStackTrace();
			nodeDTO.setStatus(0);
			error.setDescription("Error occured while getting upcoming contest details to update on firebase.");
			nodeDTO.setError(error);
			return nodeDTO;
		}
	
	}
	
	
	@RequestMapping(value="/GetGiftCards")
	public RtfGiftCardsDTO getGiftCards(HttpServletRequest request, HttpServletResponse response){
		RtfGiftCardsDTO giftCardDTO = new RtfGiftCardsDTO();
		Error error = new Error();
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String gcStatus = request.getParameter("gcStatus");
			String sortingString = request.getParameter("sortingString");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getGiftCardFilter(headerFilter+sortingString);
			if(gcStatus==null || gcStatus.isEmpty()){
				giftCardDTO.setStatus(0);
				error.setDescription("Please send valid status either ACTIVE or EXPIRED.");
				giftCardDTO.setError(error);
				return giftCardDTO;
			}
			List<GiftCardPojo> cards = DAORegistry.getRtfGiftCardDAO().getAllGiftCards(filter,gcStatus);
			List<GiftCardBrand> brands = DAORegistry.getGiftCardBrandDAO().getAllGiftCardBrands(new GridHeaderFilters(),"ACTIVE") ;
			giftCardDTO.setGiftCards(cards);
			giftCardDTO.setBrands(brands);
			giftCardDTO.setPagination(PaginationUtil.getDummyPaginationParameters(cards.size()));
			giftCardDTO.setStatus(1);
			if(cards.isEmpty()){
				giftCardDTO.setStatus(1);
				giftCardDTO.setMessage("No Gift Cards are found with seleted search filters.");
				return giftCardDTO;
			}
			return giftCardDTO;
		} catch (Exception e) {
			e.printStackTrace();
			giftCardDTO.setStatus(0);
			error.setDescription("Error occured while fetching gift cards.");
			giftCardDTO.setError(error);
			return giftCardDTO;
		}
	
	}
	
	
	
	@RequestMapping(value="/UpdateGiftCard")
	public RtfGiftCardsDTO updateGiftCard(HttpServletRequest request, HttpServletResponse response){
		RtfGiftCardsDTO giftCardDTO = new RtfGiftCardsDTO();
		Error error = new Error();
		try {
			String gcStatus = request.getParameter("gcStatus");
			String cardId = request.getParameter("cardId");
			String title = request.getParameter("title");
			String gcBrand = request.getParameter("gcBrand");
			String description = request.getParameter("description");
			String conversionType = request.getParameter("conversionType");
			String conversionRate = request.getParameter("conversionRate");
			String pointConversionRate = request.getParameter("pointConversionRate");
			//String imageUrl = request.getParameter("imageUrl");
			String action = request.getParameter("action");
			String userName = request.getParameter("userName");
			//String fileRequired = request.getParameter("fileRequired");
			String cardType = request.getParameter("cardType");
			String startDate = request.getParameter("startDate");
			String endDate = request.getParameter("endDate");
			
			
			if(action == null || action.isEmpty()){
				giftCardDTO.setStatus(0);
				error.setDescription("Please send valid action to update/save gift card.");
				giftCardDTO.setError(error);
				return giftCardDTO;
			}
			GridHeaderFilters filter = GridHeaderFiltersUtil.getGiftCardFilter(null);
			if(action.equalsIgnoreCase("EDIT")){
				if(cardId==null || cardId.isEmpty()){
					giftCardDTO.setStatus(0);
					error.setDescription("Cannot Edit Gift Card, Gift card id not found.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}
				Integer gcId = 0;
				try {
					gcId = Integer.parseInt(cardId);
				} catch (Exception e) {
					e.printStackTrace();
					giftCardDTO.setStatus(0);
					error.setDescription("Cannot Edit Gift Card, Invalid gift card id.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}
				RtfGiftCard card = DAORegistry.getRtfGiftCardDAO().get(gcId);
				List<RtfGiftCardQuantity> qtyList = DAORegistry.getRtfGiftCardQuantityDAO().getAllGiftCardQuantityByCardId(gcId);
				if(card == null || qtyList.isEmpty()){
					giftCardDTO.setStatus(0);
					error.setDescription("Cannot Edit Gift Card, Gift Card is not found in system.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}
				giftCardDTO.setGiftCard(card);
				giftCardDTO.setQtyList(qtyList);
				giftCardDTO.setStatus(1);
				return giftCardDTO;
			}else if(action.equalsIgnoreCase("SAVE") || action.equalsIgnoreCase("UPDATE")){
				String rowCountStr = request.getParameter("rowCount");
				if(rowCountStr==null || rowCountStr.isEmpty()){
					giftCardDTO.setStatus(0);
					error.setDescription("Gift card row count is not found, please add atleast one row.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}
				Integer rowCount = 0;
				try {
					rowCount = Integer.parseInt(rowCountStr);
				} catch (Exception e) {
					giftCardDTO.setStatus(0);
					error.setDescription("Gift card invalid row count.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}
				if(title==null || title.isEmpty()){
					giftCardDTO.setStatus(0);
					error.setDescription("Gift Card Title is mendatory.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}
				if(gcBrand==null || gcBrand.isEmpty()){
					giftCardDTO.setStatus(0);
					error.setDescription("Gift Card Brand is mendatory.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}
				if(description==null || description.isEmpty()){
					giftCardDTO.setStatus(0);
					error.setDescription("Gift Card Description is mendatory.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}
				if(conversionType==null || conversionType.isEmpty()){
					giftCardDTO.setStatus(0);
					error.setDescription("Gift Card Conversion Type is mendatory.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}
				if(cardType==null || cardType.isEmpty()){
					giftCardDTO.setStatus(0);
					error.setDescription("Gift Card Type is mendatory.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}
				if(conversionRate==null || conversionRate.isEmpty()){
					giftCardDTO.setStatus(0);
					error.setDescription("Gift Card Rewards Conversion Rate is mendatory.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}
				if(pointConversionRate==null || pointConversionRate.isEmpty()){
					giftCardDTO.setStatus(0);
					error.setDescription("Gift Card Points Conversion Rate is mendatory.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}
				
				List<RtfGiftCardQuantity> rtfQtyList = new ArrayList<RtfGiftCardQuantity>();
				for(int i=1;i<=rowCount;i++){
					String amountStr = request.getParameter("amount_"+i);
					String quantityStr = request.getParameter("quantity_"+i);
					String maxThresholdStr = request.getParameter("maxThreshold_"+i);
					String remainingQtyStr = request.getParameter("remainingQty_"+i);
					String usedQtyStr = request.getParameter("usedQty_"+i);
					String idStr = request.getParameter("id_"+i);
					if(amountStr==null || amountStr.isEmpty()){
						giftCardDTO.setStatus(0);
						error.setDescription("Gift Card Amount is mendatory.");
						giftCardDTO.setError(error);
						return giftCardDTO;
					}
					if(quantityStr==null || quantityStr.isEmpty()){
						giftCardDTO.setStatus(0);
						error.setDescription("Gift Card Quantity is mendatory.");
						giftCardDTO.setError(error);
						return giftCardDTO;
					}
					if(maxThresholdStr==null || maxThresholdStr.isEmpty()){
						giftCardDTO.setStatus(0);
						error.setDescription("Gift Card Max. Quantity Threshold is mendatory.");
						giftCardDTO.setError(error);
						return giftCardDTO;
					}
					if(remainingQtyStr==null || remainingQtyStr.isEmpty()){
						remainingQtyStr = "0";
					}
					if(usedQtyStr==null || usedQtyStr.isEmpty()){
						usedQtyStr = "0";
					}
					Double amount=0.00;
					Integer quantity = 0;
					Integer maxThreshold = 0;
					Integer remainigQty = 0;
					Integer usedQty = 0;
					try {
						quantity = Integer.parseInt(quantityStr);
						amount = Double.parseDouble(amountStr);
						maxThreshold = Integer.parseInt(maxThresholdStr);
						remainigQty = Integer.parseInt(remainingQtyStr);
						usedQty = Integer.parseInt(usedQtyStr);
					} catch (Exception e) {
						e.printStackTrace();
						giftCardDTO.setStatus(0);
						error.setDescription("Please send valid amount/quantity/Max. Qty. THreshold for gift card.");
						giftCardDTO.setError(error);
						return giftCardDTO;
					}
					
					RtfGiftCardQuantity giftCardQty = null;
					if(idStr!=null && !idStr.isEmpty()){
						giftCardQty = DAORegistry.getRtfGiftCardQuantityDAO().get(Integer.parseInt(idStr));
					}else{
						giftCardQty = new RtfGiftCardQuantity();
					}
					giftCardQty.setAmount(amount);
					giftCardQty.setStatus(true);
					giftCardQty.setMaxQtyThreshold(maxThreshold);
					giftCardQty.setMaxQty(quantity);
					giftCardQty.setFreeQty(quantity - usedQty);
					giftCardQty.setUsedQty(usedQty);
					/*if(giftCardQty.getMaxQty()!= quantity){
						giftCardQty.setMaxQty(quantity);
						giftCardQty.setFreeQty(quantity);
						giftCardQty.setUsedQty(0);
					}*/
					rtfQtyList.add(giftCardQty);
				}
				
				if(startDate==null || startDate.isEmpty()){
					giftCardDTO.setStatus(0);
					error.setDescription("Gift Card Start Date is mandatory.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				
				}
				if(endDate==null || endDate.isEmpty()){
					giftCardDTO.setStatus(0);
					error.setDescription("Gift Card End Date is mandatory.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				
				}
				
				startDate = startDate + " 00:00:00";
				endDate = endDate + " 23:59:59";
				
				//DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
				Date stDate = null;
				try {
					stDate = Util.getDateWithTwentyFourHourFormat1(startDate);
				} catch (Exception e) {
					giftCardDTO.setStatus(0);
					error.setDescription("Please Select Valid Gift Card Start Date.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}
				Date eDate = null;
				try {
					eDate = Util.getDateWithTwentyFourHourFormat1(endDate);
				} catch (Exception e) {
					giftCardDTO.setStatus(0);
					error.setDescription("Please Select Valid Gift Card End Date.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}
				System.out.println("GiftDate : "+startDate+":"+stDate+":enddate:"+endDate+":"+eDate);
				
				if(stDate.after(eDate)){
					giftCardDTO.setStatus(0);
					error.setDescription("Gift Card Start Date is greater than Gift Card End date.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				
				}
				
				Double convRate = 0.00;
				Double pointConvRate = 0.00;
				try {
					convRate  = Double.parseDouble(conversionRate);
				} catch (Exception e) {
					e.printStackTrace();
					giftCardDTO.setStatus(0);
					error.setDescription("Please add valid gift card rewards conversion rate.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}
				try {
					pointConvRate  = Double.parseDouble(pointConversionRate);
				} catch (Exception e) {
					e.printStackTrace();
					giftCardDTO.setStatus(0);
					error.setDescription("Please add valid gift card pointtconversion rate.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}
				
				
				
				Integer brandId = null;
				try {
					brandId = Integer.parseInt(gcBrand);
				} catch (Exception e) {
					e.printStackTrace();
					giftCardDTO.setStatus(0);
					error.setDescription("Please select valid gift card brand.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}
				
				GiftCardBrand brand = DAORegistry.getGiftCardBrandDAO().get(brandId);
				if(brand == null){
					giftCardDTO.setStatus(0);
					error.setDescription("Selected gift card brand not found.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}
				RtfGiftCard card = null;
				if(action.equalsIgnoreCase("UPDATE")){
					if(cardId==null || cardId.isEmpty()){
						giftCardDTO.setStatus(0);
						error.setDescription("Cannot update Gift Card, Gift card id not found.");
						giftCardDTO.setError(error);
						return giftCardDTO;
					}
					Integer gcId = 0;
					try {
						gcId = Integer.parseInt(cardId);
					} catch (Exception e) {
						e.printStackTrace();
						giftCardDTO.setStatus(0);
						error.setDescription("Cannot update Gift Card, Invalid gift card id.");
						giftCardDTO.setError(error);
						return giftCardDTO;
					}
					
					card = DAORegistry.getRtfGiftCardDAO().get(gcId);
					//MultipartFile file = null;
					/*if(fileRequired !=null && fileRequired.equalsIgnoreCase("Y")){
						MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
						file = multipartRequest.getFile("imageFile");
						if(file==null){
							giftCardDTO.setStatus(0);
							error.setDescription("Gift card image is mendatory.");
							giftCardDTO.setError(error);
							return giftCardDTO;
						}
					}*/
					
					
					card.setConversionRate(convRate);
					card.setPointsConversionRate(pointConvRate);
					card.setConversionType(conversionType);
					card.setDescription(description);
					card.setTitle(title);
					card.setUpdateDate(new Date());
					card.setUpdatedBy(userName);
					card.setCardType(cardType);	
					card.setStartDate(stDate);
					card.setEndDate(eDate);
					card.setBrandId(brandId);
					
					
					
					DAORegistry.getRtfGiftCardDAO().saveOrUpdate(card);
					Map<Integer,RtfGiftCardQuantity> qtyMap = new HashMap<Integer, RtfGiftCardQuantity>();
					List<RtfGiftCardQuantity> newList =  new ArrayList<RtfGiftCardQuantity>();
					List<RtfGiftCardQuantity> updateList =  new ArrayList<RtfGiftCardQuantity>();
					for(RtfGiftCardQuantity qty1 : rtfQtyList){
						qty1.setCardId(card.getId());
						if(qty1.getId()!=null){
							qtyMap.put(qty1.getId(),qty1);
							updateList.add(qty1);
						}else{
							newList.add(qty1);
						}
					}
					DAORegistry.getRtfGiftCardQuantityDAO().updateAll(updateList);
					DAORegistry.getRtfGiftCardQuantityDAO().saveAll(newList);
					List<RtfGiftCardQuantity> allList = new ArrayList<RtfGiftCardQuantity>();
					allList.addAll(updateList);
					allList.addAll(newList);
					qtyMap = new HashMap<Integer, RtfGiftCardQuantity>();
					for(RtfGiftCardQuantity q : allList){
						qtyMap.put(q.getId(),q);
					}
					List<RtfGiftCardQuantity> qties = DAORegistry.getRtfGiftCardQuantityDAO().getAllGiftCardQuantityByCardId(card.getId());
					updateList = new ArrayList<RtfGiftCardQuantity>();
					for(RtfGiftCardQuantity qt : qties){
						if(qtyMap.get(qt.getId()) == null){
							qt.setStatus(false);
							updateList.add(qt);
						}
					}
					DAORegistry.getRtfGiftCardQuantityDAO().updateAll(updateList);
					
					//if(fileRequired !=null && fileRequired.equalsIgnoreCase("Y")){
						/*String ext = FilenameUtils.getExtension(brand.getFileName());
						String imagPath = URLUtil.GIFTCARD_IMAGE_DIRECTORY+"giftcard_"+card.getId()+"."+ext;
						File brandFile = new File(brand.getImageUrl());
						File newFile = new File(imagPath);
						newFile.createNewFile();
						
						FileUtils.copyFile(brandFile, newFile);
						card.setImageUrl(imagPath);
						
						GenericResponseDTO genResponseDTO = FileUploadUtil.uploadGCAssets( "GCMD" ,  newFile ,  Constants.BASE_URL ) ;
						if(genResponseDTO.getStatus() != 1) {
							error.setDescription(genResponseDTO.getMessage());
							giftCardDTO.setStatus(0);
							return giftCardDTO;						
						}*/
						
						DAORegistry.getRtfGiftCardDAO().update(card);
					//}
					giftCardDTO.setMessage("Gift card updated successfully.");
				}else{
					/*MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
					MultipartFile file = multipartRequest.getFile("imageFile");
					if(file==null){
						giftCardDTO.setStatus(0);
						error.setDescription("Gift card image is mendatory.");
						giftCardDTO.setError(error);
						return giftCardDTO;
					}*/
					
					card = new RtfGiftCard();
					
					card.setConversionRate(convRate);
					card.setPointsConversionRate(pointConvRate);
					card.setConversionType(conversionType);
					card.setDescription(description);
					card.setTitle(title);
					card.setCreatedDate(new Date());
					card.setCreatedBy(userName);
					card.setStatus(true);
					card.setCardStatus("ACTIVE");
					card.setCardType(cardType);
					card.setStartDate(stDate);
					card.setBrandId(brandId);
					card.setEndDate(eDate);
					
					DAORegistry.getRtfGiftCardDAO().saveOrUpdate(card);
					for(RtfGiftCardQuantity qty1 : rtfQtyList){
						qty1.setCardId(card.getId());
					}
					DAORegistry.getRtfGiftCardQuantityDAO().saveAll(rtfQtyList);
					
					/*String ext = FilenameUtils.getExtension(brand.getFileName());
					String imagPath = URLUtil.GIFTCARD_IMAGE_DIRECTORY+"giftcard_"+card.getId()+"."+ext;
					File brandFile = new File(brand.getImageUrl());
					File newFile = new File(imagPath);
					newFile.createNewFile();
					
					FileUtils.copyFile(brandFile, newFile);
					card.setImageUrl(imagPath);
					
					GenericResponseDTO genResponseDTO = FileUploadUtil.uploadGCAssets( "GCMD" ,  newFile ,  Constants.BASE_URL ) ;
					if(genResponseDTO.getStatus() != 1) {
						error.setDescription(genResponseDTO.getMessage());
						giftCardDTO.setStatus(0);
						return giftCardDTO;						
					}*/
					
					DAORegistry.getRtfGiftCardDAO().update(card);
					giftCardDTO.setMessage("Gift card saved successfully.");
				}
				
				List<GiftCardPojo> cards = DAORegistry.getRtfGiftCardDAO().getAllGiftCards(filter,gcStatus);
				giftCardDTO.setGiftCards(cards);
				giftCardDTO.setPagination(PaginationUtil.getDummyPaginationParameters(cards.size()));
				giftCardDTO.setStatus(1);
				return giftCardDTO;
			}else if(action.equalsIgnoreCase("DELETE")){
				if(cardId==null || cardId.isEmpty()){
					giftCardDTO.setStatus(0);
					error.setDescription("Cannot delete Gift Card, Gift card id not found.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}
				Integer gcId = 0;
				try {
					gcId = Integer.parseInt(cardId);
				} catch (Exception e) {
					e.printStackTrace();
					giftCardDTO.setStatus(0);
					error.setDescription("Cannot delete Gift Card, Invalid gift card id.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}
				RtfGiftCard card = DAORegistry.getRtfGiftCardDAO().get(gcId);
				List<RtfGiftCardQuantity> qtyList = DAORegistry.getRtfGiftCardQuantityDAO().getAllGiftCardQuantityByCardId(gcId);
				if(card==null || qtyList==null || qtyList.isEmpty()){
					giftCardDTO.setStatus(0);
					error.setDescription("Cannot delete Gift Card, No gift card is found with given id.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}
				
				card.setUpdateDate(new Date());
				card.setUpdatedBy(userName);
				card.setStatus(false);
				for(RtfGiftCardQuantity qty1 : qtyList){
					qty1.setStatus(false);
				}
				
				DAORegistry.getRtfGiftCardDAO().update(card);
				DAORegistry.getRtfGiftCardQuantityDAO().updateAll(qtyList);
				giftCardDTO.setMessage("Gift Card deleted successfully.");
				List<GiftCardPojo> cards = DAORegistry.getRtfGiftCardDAO().getAllGiftCards(filter,gcStatus);
				giftCardDTO.setGiftCards(cards);
				giftCardDTO.setPagination(PaginationUtil.getDummyPaginationParameters(cards.size()));
				giftCardDTO.setStatus(1);
				return giftCardDTO;
			}
			giftCardDTO.setStatus(0);
			error.setDescription("Not able to identify action type EDIT/SAVE/UPDATE/DELETE.");
			giftCardDTO.setError(error);
			return giftCardDTO;
		} catch (Exception e) {
			e.printStackTrace();
			giftCardDTO.setStatus(0);
			error.setDescription("Error occured while fetching gift cards.");
			giftCardDTO.setError(error);
			return giftCardDTO;
		}
	
	}
	
	
	
	@RequestMapping(value = "/GetGiftCardOrders")
	public GiftCardOrdersDTO getGiftCardOrders(HttpServletRequest request, HttpServletResponse response) {
		GiftCardOrdersDTO giftCardOrdersDTO = new GiftCardOrdersDTO();
		Error error = new Error();
		try {
			
			String headerFilter = request.getParameter("headerFilter");
			String status = request.getParameter("status");
			String type = request.getParameter("type");
			GridHeaderFilters filter = new GridHeaderFilters();			
			if (status == null || status.isEmpty()) {
				System.err.println("Please send valid Tab Name.");
				error.setDescription("Please send valid Tab Name.");
				giftCardOrdersDTO.setError(error);
				giftCardOrdersDTO.setStatus(0);
				return giftCardOrdersDTO;
			}
			filter = GridHeaderFiltersUtil.getGiftCardOrderFilter(headerFilter);
			List<GiftCardOrders> giftCardOrdersList = DAORegistry.getGiftCardOrderDAO().getAllGiftCardOrders(null,filter, status);
			int count = 0;

			if (giftCardOrdersList != null && giftCardOrdersList.size() > 0) {
				count = giftCardOrdersList.size();
			}
			if (giftCardOrdersList == null || giftCardOrdersList.size() <= 0) {
				giftCardOrdersDTO.setMessage("No Gift Card Orders Found.");
			}

			giftCardOrdersDTO.setStatus(1);
			giftCardOrdersDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(count));
			giftCardOrdersDTO.setGiftCardOrdersList(giftCardOrdersList);

		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Gift Card Orders.");
			giftCardOrdersDTO.setError(error);
			giftCardOrdersDTO.setStatus(0);
		}
		return giftCardOrdersDTO;
	}

	
	@RequestMapping(value = "/EditGiftCardOrder")
	public GiftCardOrdersEditDTO AddRealGiftCards(HttpServletRequest request, HttpServletResponse response) {
		GiftCardOrdersEditDTO giftCardOrdersEditDTO = new GiftCardOrdersEditDTO();
		Error error = new Error();
		try {

			String orderId = request.getParameter("orderId");

			if (StringUtils.isEmpty(orderId)) {
				System.err.println("Order ID is incorrect. " + orderId);
				error.setDescription("Order ID is incorrect. " + orderId);
				giftCardOrdersEditDTO.setError(error);
				giftCardOrdersEditDTO.setStatus(0);
				return giftCardOrdersEditDTO;
			}

			GiftCardOrders giftCardOrders = DAORegistry.getGiftCardOrderDAO().getGiftCardOrdersById(Integer.valueOf(orderId));
			if (giftCardOrders == null) {
				System.err.println("Order not found for Id " + orderId);
				error.setDescription("Order not found for Id " + orderId);
				giftCardOrdersEditDTO.setError(error);
				giftCardOrdersEditDTO.setStatus(0);
				return giftCardOrdersEditDTO;
			}

			List<GiftCardOrderAttachment> eGiftAttachments = DAORegistry.getGiftCardOrderAttachmentDAO().getOrderEGiftCardAttachments(giftCardOrders.getId());
			List<GiftCardOrderAttachment> barcodeAttachments = DAORegistry.getGiftCardOrderAttachmentDAO().getOrderBarcodeAttachments(giftCardOrders.getId());
			
			Integer shippingAdressId = giftCardOrders.getShipAddrId();
			if (shippingAdressId == null) {
				System.err.println("shippingAdressId not added or format is incorrect  for Order Id  " + orderId+ " Shippid Id recd is " + shippingAdressId);
				error.setDescription("shippingAdressId not added or format is incorrect  for Order Id  " + orderId+ " Shippid Id recd is " + shippingAdressId);
				giftCardOrdersEditDTO.setError(error);
				giftCardOrdersEditDTO.setStatus(0);
				return giftCardOrdersEditDTO;
			}

			Integer custId = giftCardOrders.getCustId();
			if (custId == null) {
				System.err.println("Customer Id  not found for Order Id " + orderId + " Customer Id " + custId);
				error.setDescription("Customer not found  for Order Id " + orderId + " Customer Id " + custId);
				giftCardOrdersEditDTO.setError(error);
				giftCardOrdersEditDTO.setStatus(0);
				return giftCardOrdersEditDTO;
			}

			Customer customer = DAORegistry.getCustomerDAO().get(custId);
			CustomerAddress custAddress = DAORegistry.getCustomerAddressDAO().getCustomerShippingAddresByAddresId(shippingAdressId);
			if (custAddress == null) {
				System.err.println("Shipping Address not Found For Order Id " + orderId + "  &  Shipping Id " + shippingAdressId);
				error.setDescription("Shipping Address not Found For Order Id " + orderId + "  &  Shipping Id " + shippingAdressId);
				giftCardOrdersEditDTO.setError(error);
				giftCardOrdersEditDTO.setStatus(0);
				return giftCardOrdersEditDTO;
			}

			giftCardOrdersEditDTO.setCustomerShippingAddress(custAddress);
			giftCardOrdersEditDTO.setCustomer(customer);
			giftCardOrdersEditDTO.setGiftCardOrders(giftCardOrders);
			giftCardOrdersEditDTO.seteGiftAttachments(eGiftAttachments);
			giftCardOrdersEditDTO.setBarcodeAttachments(barcodeAttachments);
			giftCardOrdersEditDTO.setStatus(1);

		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Invoice Edit.");
			giftCardOrdersEditDTO.setError(error);
			giftCardOrdersEditDTO.setStatus(0);
			return giftCardOrdersEditDTO;
		}
		return giftCardOrdersEditDTO;
	}
	
	
	
	
	@RequestMapping(value = "/SaveGiftCardOrder")
	public GiftCardOrdersDTO saveGiftCardOrder(HttpServletRequest request, HttpServletResponse response) {
		GiftCardOrdersDTO giftCardOrdersDTO = new GiftCardOrdersDTO();
		Error error = new Error();

		try {

			String orderIdStr = request.getParameter("orderId");
			String status = request.getParameter("status");
			String userName = request.getParameter("userName");
			String remark = request.getParameter("remarks");

			if (StringUtils.isEmpty(orderIdStr)) {
				System.err.println("Please Send Order Id.");
				error.setDescription("Please Send Order Id.");
				giftCardOrdersDTO.setError(error);
				giftCardOrdersDTO.setStatus(0);
				return giftCardOrdersDTO;
			}
			
			Integer orderId = 0;
			try {
				orderId = Integer.parseInt(orderIdStr);
			} catch (Exception e) {
				e.printStackTrace();
				System.err.println("Please valid send order Id. "+orderIdStr);
				error.setDescription("Please valid send order Id. "+orderIdStr);
				giftCardOrdersDTO.setError(error);
				giftCardOrdersDTO.setStatus(0);
				return giftCardOrdersDTO;
			}

			GiftCardOrders giftCardOrder = DAORegistry.getGiftCardOrderDAO().getGiftCardOrdersById(orderId);
			if (giftCardOrder == null) {
				System.err.println("Order  not found for Id " + orderId);
				error.setDescription("Order not found for Id " + orderId);
				giftCardOrdersDTO.setError(error);
				giftCardOrdersDTO.setStatus(0);
				return giftCardOrdersDTO;
			}
			
			/*MultipartFile file = null;
			if(fileRequired !=null && fileRequired.equalsIgnoreCase("Y")){
				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
				file = multipartRequest.getFile("giftCardFile");
				if(file==null){
					giftCardOrdersDTO.setStatus(0);
					error.setDescription("Gift card image is mendatory.");
					giftCardOrdersDTO.setError(error);
					return giftCardOrdersDTO;
				}
				
				String ext = FilenameUtils.getExtension(file.getOriginalFilename());
				String imagPath = URLUtil.EGIFTCARD_DIRECTORY+"GiftCards_"+giftCardOrder.getId()+"."+ext;
				File newFile = new File(imagPath);

				newFile.createNewFile();
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(newFile));
				FileCopyUtils.copy(file.getInputStream(), stream);
				stream.close();
				giftCardOrder.setCardFileUrl(imagPath);
				
				GenericResponseDTO genResponseDTO = FileUploadUtil.uploadGCAssets( "GCOD" ,  newFile ,  Constants.BASE_URL ) ;
				if(genResponseDTO.getStatus() != 1) {
					error.setDescription(genResponseDTO.getMessage());
					giftCardOrdersDTO.setStatus(0);
					return giftCardOrdersDTO;						
				}
			}
			
			
			if(giftCardOrder.getCardFileUrl()==null || giftCardOrder.getCardFileUrl().isEmpty()){
				giftCardOrdersDTO.setStatus(0);
				error.setDescription("Please send e-giftcard file.");
				giftCardOrdersDTO.setError(error);
				return giftCardOrdersDTO;
			}*/
			
			if(uploadGiftcardFiles(request, giftCardOrder)){
				List<GiftCardOrderAttachment> eGiftAttachments = DAORegistry.getGiftCardOrderAttachmentDAO().getOrderEGiftCardAttachments(orderId);
				List<GiftCardOrderAttachment> barcodeAttachments = DAORegistry.getGiftCardOrderAttachmentDAO().getOrderBarcodeAttachments(orderId);
				if(eGiftAttachments==null && barcodeAttachments == null){
					giftCardOrdersDTO.setStatus(0);
					error.setDescription("No Giftcard Attachments are found.");
					giftCardOrdersDTO.setError(error);
					return giftCardOrdersDTO;
				}
				if(eGiftAttachments!=null){
					for(GiftCardOrderAttachment attach : eGiftAttachments){
						File newFile = new File(attach.getFilePath());
						GenericResponseDTO genResponseDTO = FileUploadUtil.uploadGCAssets( "GCOD" ,  newFile ,  Constants.BASE_URL ) ;
						if(genResponseDTO.getStatus() != 1) {
							DAORegistry.getGiftCardOrderAttachmentDAO().deleteAll(eGiftAttachments);
							error.setDescription(genResponseDTO.getMessage());
							giftCardOrdersDTO.setStatus(0);
							return giftCardOrdersDTO;						
						}
						AwsS3Response awsRsponse = AWSFileService.upLoadFileToS3(AWSFileService.BUCKET_RTF_ASSETS, attach.getfName(), AWSFileService.GIFTCARD_FOLDER, newFile );
						if(null != awsRsponse && awsRsponse.getStatus() != 1){
							DAORegistry.getGiftCardOrderAttachmentDAO().deleteAll(eGiftAttachments);
							giftCardOrdersDTO.setStatus(0);
							error.setDescription("Error occured while uploading gift card to S3.");
							giftCardOrdersDTO.setError(error);
							return giftCardOrdersDTO;
						}
					}
				}
				if(barcodeAttachments!=null){
					for(GiftCardOrderAttachment attach : barcodeAttachments){
						File newFile = new File(attach.getFilePath());
						GenericResponseDTO genResponseDTO = FileUploadUtil.uploadGCAssets( "GCOD" ,  newFile ,  Constants.BASE_URL ) ;
						if(genResponseDTO.getStatus() != 1) {
							DAORegistry.getGiftCardOrderAttachmentDAO().deleteAll(barcodeAttachments);
							error.setDescription(genResponseDTO.getMessage());
							giftCardOrdersDTO.setStatus(0);
							return giftCardOrdersDTO;						
						}
						AwsS3Response awsRsponse = AWSFileService.upLoadFileToS3(AWSFileService.BUCKET_RTF_ASSETS, attach.getfName(), AWSFileService.GIFTCARD_FOLDER, newFile );
						if(null != awsRsponse && awsRsponse.getStatus() != 1){
							DAORegistry.getGiftCardOrderAttachmentDAO().deleteAll(barcodeAttachments);
							giftCardOrdersDTO.setStatus(0);
							error.setDescription("Error occured while uploading gift card to S3.");
							giftCardOrdersDTO.setError(error);
							return giftCardOrdersDTO;
						}
					}
				}
				
				giftCardOrder.setIsOrderFulfilled(true);
				giftCardOrder.setUpdatedDate(new Date());
				giftCardOrder.setUpdatedBy(userName);
				giftCardOrder.setStatus("COMPLETED");
				giftCardOrder.setRemarks(remark);
				DAORegistry.getGiftCardOrderDAO().update(giftCardOrder);
				giftCardOrdersDTO.setMessage("File attached and order marked as complete successfully.");

				GridHeaderFilters filter = GridHeaderFiltersUtil.getGiftCardOrderFilter(null);
				List<GiftCardOrders> giftCardOrdersList = DAORegistry.getGiftCardOrderDAO().getAllGiftCardOrders(null,filter, status);
				int count = 0;

				if (giftCardOrdersList != null && giftCardOrdersList.size() > 0) {
					count = giftCardOrdersList.size();
				}

				giftCardOrdersDTO.setStatus(1);
				giftCardOrdersDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(count));
				giftCardOrdersDTO.setGiftCardOrdersList(giftCardOrdersList);
				return giftCardOrdersDTO;
			}else{
				giftCardOrdersDTO.setStatus(0);
				error.setDescription("Please send e-giftcard file.");
				giftCardOrdersDTO.setError(error);
				return giftCardOrdersDTO;
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Attaching e-giftcard to order.");
			giftCardOrdersDTO.setError(error);
			giftCardOrdersDTO.setStatus(0);
			return giftCardOrdersDTO;
		}
	}
		
	
	@RequestMapping(value = "/VoidGiftCardOrders")
	public GiftCardOrdersDTO voidGiftCardOrders(HttpServletRequest request, HttpServletResponse response) {
		GiftCardOrdersDTO giftCardOrdersDTO = new GiftCardOrdersDTO();
		Error error = new Error();
		try {
			String orderIdStr = request.getParameter("orderId");
			String userName = request.getParameter("userName");	
			String status = request.getParameter("status");
			String action = request.getParameter("action");
			if (StringUtils.isEmpty(orderIdStr)) {
				System.err.println("Please send order id to update order.");
				error.setDescription("Please send order id to update order.");
				giftCardOrdersDTO.setError(error);
				giftCardOrdersDTO.setStatus(0);
				return giftCardOrdersDTO;
			}
			if (StringUtils.isEmpty(action)) {
				System.err.println("Please send valid action to update order.");
				error.setDescription("Please send valid action to update order.");
				giftCardOrdersDTO.setError(error);
				giftCardOrdersDTO.setStatus(0);
				return giftCardOrdersDTO;
			}
			Integer orderId = 0;
			try {
				orderId = Integer.parseInt(orderIdStr);
			} catch (Exception e) {
				e.printStackTrace();
				System.err.println("Please send valid order id to void order.");
				error.setDescription("Please send valid order id to void order.");
				giftCardOrdersDTO.setError(error);
				giftCardOrdersDTO.setStatus(0);
				return giftCardOrdersDTO;
			}
			
			GiftCardOrders giftCardOrder = DAORegistry.getGiftCardOrderDAO().getGiftCardOrdersById(orderId);
			if (giftCardOrder == null) {
				System.err.println("Order not found for Id " + orderId);
				error.setDescription("Order not found for Id " + orderId);
				giftCardOrdersDTO.setError(error);
				giftCardOrdersDTO.setStatus(0);
				return giftCardOrdersDTO;
			}
			
			List<GiftCardOrderAttachment> attachments = DAORegistry.getGiftCardOrderAttachmentDAO().getOrderAllAttachments(orderId);
			
			if(attachments!=null && !attachments.isEmpty()){
				for(GiftCardOrderAttachment attach : attachments){
					File file  = new File(attach.getFilePath());
					if(file.exists()){
						file.delete();
					}
				}
				
				DAORegistry.getGiftCardOrderAttachmentDAO().deleteAll(attachments);
				
			}
			
			giftCardOrder.setIsOrderFulfilled(false);
			giftCardOrder.setUpdatedBy(userName);
			giftCardOrder.setUpdatedDate(new Date());
			
			if(action.equalsIgnoreCase("VOID")){
				giftCardOrder.setStatus("VOIDED"); 
				if(giftCardOrder.getOrderType().equalsIgnoreCase("REGULAR")){
					CustomerLoyalty loyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(giftCardOrder.getCustId());
					loyalty.setActivePoints(loyalty.getActivePoints() + giftCardOrder.getRedeemedRewards());
					DAORegistry.getCustomerLoyaltyDAO().update(loyalty);
					
					CustomerLoyaltyHistory history = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerGiftCardOrderLoyaltyHistoryByOrderIdAndCustomerId(giftCardOrder.getId(),giftCardOrder.getCustId());
					if(history!=null){
						history.setRewardStatus(RewardStatus.VOIDED);
						DAORegistry.getCustomerLoyaltyHistoryDAO().update(history);
					}
				}
				giftCardOrdersDTO.setMessage("Selected GiftCard Order is cancelled.");
			}else if(action.equalsIgnoreCase("OUTSTANDING")){
				giftCardOrder.setStatus("OUTSTANDING");
				giftCardOrdersDTO.setMessage("Selected GiftCard Order moved to outstanding.");
			}
			
			DAORegistry.getGiftCardOrderDAO().saveOrUpdate(giftCardOrder);
			
			giftCardOrdersDTO.setStatus(1);
			
			GridHeaderFilters filter = GridHeaderFiltersUtil.getGiftCardOrderFilter(null);
			List<GiftCardOrders> giftCardOrdersList = DAORegistry.getGiftCardOrderDAO().getAllGiftCardOrders(null,filter, status);
			int count = 0;

			if (giftCardOrdersList != null && giftCardOrdersList.size() > 0) {
				count = giftCardOrdersList.size();
			}

			giftCardOrdersDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(count));
			giftCardOrdersDTO.setGiftCardOrdersList(giftCardOrdersList);
			return giftCardOrdersDTO;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while updating Gift Card Order " );
			giftCardOrdersDTO.setError(error);
			giftCardOrdersDTO.setStatus(0);
			return giftCardOrdersDTO;
		}
	}
	
	
	@RequestMapping(value = "/DownloadEGiftCard")
	public void downloadEGiftCard(HttpServletRequest request, HttpServletResponse response) {
		String orderIdStr = request.getParameter("orderId");
		String fileType = request.getParameter("fileType");
		String position = request.getParameter("position");
		try {
			if(orderIdStr!=null && !orderIdStr.isEmpty() && fileType!=null && !fileType.isEmpty()
					&& position!=null && !position.isEmpty()){
				GiftCardOrderAttachment attachment = DAORegistry.getGiftCardOrderAttachmentDAO().getOrderAttachmentByTypeAndPosition(Integer.valueOf(orderIdStr), fileType, Integer.valueOf(position));
				if(attachment!=null){
					OutputStream out = response.getOutputStream();
					File file = new File(attachment.getFilePath());
					if(file.exists()){
						FileInputStream in = new FileInputStream(file);
						response.setContentType("application/pdf");
						response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"",file.getName()));
						byte[] buffer = new byte[4096];
						int length;
						while((length = in.read(buffer)) > 0){
						    out.write(buffer, 0, length);
						}
						in.close();
						out.flush();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	@RequestMapping(value = "/DeleteGiftCardOrderAttachment")
	public GenericResponseDTO deleteGiftCardOrderAttachment(HttpServletRequest request, HttpServletResponse response) {
		GenericResponseDTO genericResp = new GenericResponseDTO();
		Error error = new Error();
		String orderIdStr = request.getParameter("orderId");
		String fileType = request.getParameter("fileType");
		String position = request.getParameter("position");
		try {
			if(orderIdStr==null  || orderIdStr.isEmpty()){
				error.setDescription("Please send order id to remove attached file." );
				genericResp.setError(error);
				genericResp.setStatus(0);
				return genericResp;
			}
			if(fileType==null  || fileType.isEmpty()){
				error.setDescription("Please send file type to remove attached file." );
				genericResp.setError(error);
				genericResp.setStatus(0);
				return genericResp;
			}
			if(position==null  || position.isEmpty()){
				error.setDescription("Invalid file position, please refresh page and try again." );
				genericResp.setError(error);
				genericResp.setStatus(0);
				return genericResp;
			}
			
			GiftCardOrderAttachment attachment = DAORegistry.getGiftCardOrderAttachmentDAO().getOrderAttachmentByTypeAndPosition(Integer.valueOf(orderIdStr), fileType, Integer.valueOf(position));
			if(attachment==null){
				error.setDescription("No file attachment found with given order id and file type." );
				genericResp.setError(error);
				genericResp.setStatus(0);
				return genericResp;
			}
			File file = new File(attachment.getFilePath());
			if(file.exists()){
				file.delete();
			}
			DAORegistry.getGiftCardOrderAttachmentDAO().delete(attachment);
			genericResp.setStatus(1);
			genericResp.setMessage("Order file attachment is removed successfully.");
			return genericResp;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while removing order file attachment." );
			genericResp.setError(error);
			genericResp.setStatus(0);
			return genericResp;
		}
	}
	
	
	@RequestMapping(value = "/GiftCardOrdersExportToExcel")
	public void giftCardOrdersExportToExcel(HttpServletRequest request, HttpServletResponse response){
		String type = request.getParameter("type");
		String status = request.getParameter("gcStatus");
		String headerFilter = request.getParameter("headerFilter");
		try {
				GridHeaderFilters filter = GridHeaderFiltersUtil.getGiftCardFilter(headerFilter);
				filter.setText5(type);
				List<GiftCardOrders> orders = DAORegistry.getGiftCardOrderDAO().getAllGiftCardOrders(null, filter, status);
				if(!orders.isEmpty()){
					SXSSFWorkbook workbook = new SXSSFWorkbook();
					SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("GiftCardOrders");
					Row header = sheet.createRow((int)0);
					header.createCell(0).setCellValue("OrderId");
					header.createCell(1).setCellValue("Card Title");
					header.createCell(2).setCellValue("Quantity");
					header.createCell(3).setCellValue("Original Cost");
					header.createCell(4).setCellValue("Created Date");
					header.createCell(5).setCellValue("Updated Date");
					header.createCell(6).setCellValue("First Name");
					header.createCell(7).setCellValue("Last Name");
					header.createCell(8).setCellValue("UserId");
					header.createCell(9).setCellValue("Barcode");
					header.createCell(10).setCellValue("Redeemed ?");
					int i=1;
					for(GiftCardOrders o : orders){
						Row row = sheet.createRow(i);
						row.createCell(0).setCellValue(o.getId());
						row.createCell(1).setCellValue(o.getCardTitle());
						row.createCell(2).setCellValue(o.getQuantity());
						row.createCell(3).setCellValue(o.getOriginalCost());
						row.createCell(4).setCellValue(o.getCreatedDateStr());
						row.createCell(5).setCellValue(o.getUpdatedDateStr());
						row.createCell(6).setCellValue(o.getFirstName());
						row.createCell(7).setCellValue(o.getLastName());
						row.createCell(8).setCellValue(o.getUserId());
						row.createCell(9).setCellValue(o.getBarcode());
						row.createCell(10).setCellValue(o.getIsRedeemed().equals(false)?"N":"Y");
						i++;
					}
					
					response.setContentType("application/vnd.ms-excel");
					response.setHeader("Content-Disposition", "attachment; filename=GiftCard_Orders.xlsx");
					workbook.write(response.getOutputStream());
					response.getOutputStream().flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping(value = "/UploadGiftCardOrderBarcode")
	public GenericResponseDTO uploadGiftCardOrderBarcode(HttpServletRequest request, HttpServletResponse response){		
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		String message = "";
		
		try {
			File f = null;
			DiskFileItemFactory  fileItemFactory = new DiskFileItemFactory ();
			File barcodeFile = new File(URLUtil.EGIFTCARD_DIRECTORY);
	        fileItemFactory.setRepository(barcodeFile);
	        ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
	        List items = uploadHandler.parseRequest(request);
	        Iterator iterator = items.iterator();
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			
			MultipartFile file = multipartRequest.getFile("orderFile");
			if(file == null){
				error.setDescription("Please select valid file to upload.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			String ext = FilenameUtils.getExtension(file.getOriginalFilename());
			f = new File(URLUtil.EGIFTCARD_DIRECTORY+"GCORDER."+ext);
			if(f.exists()){
				f.delete();
			}
			f.createNewFile();
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(f));
	        FileCopyUtils.copy(file.getInputStream(), stream);
			stream.close();
			
			List<GiftCardOrders> orders = DAORegistry.getGiftCardOrderDAO().getGiftCardOrdersByStatus("Completed");
			Map<Integer,GiftCardOrders> orderMap = new HashMap<Integer, GiftCardOrders>();
			for(GiftCardOrders o : orders){
				orderMap.put(o.getId(),o);
			}
			
			FileInputStream inputStream = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(inputStream);
			Sheet firstSheet = wb.getSheetAt(0);
	        Iterator<Row> it = firstSheet.iterator();
	        List<GiftCardOrders> updatedOrderList = new ArrayList<GiftCardOrders>();
	        GiftCardOrders order = null;
	        Integer count = 0;
	        while (it.hasNext()) {
	            Row nextRow = it.next();
	            if(nextRow.getRowNum()==0){
	            	continue;
	            }
	            Integer orderIdStr = null;
                Integer orderId = null;
                String barcodes = null;
                String redeemed = null;
	            Iterator<Cell> cellIterator = nextRow.cellIterator();
	            while (cellIterator.hasNext()) {
	                Cell cell = cellIterator.next();
	                if(cell.getColumnIndex() == 0){
	                	orderId =(int)Math.round(cell.getNumericCellValue());
	                	/*if(orderIdStr == null || orderIdStr.isEmpty()){
		                	continue;
		                }
		                try {
							orderId = Integer.parseInt(orderIdStr);
						} catch (Exception e) {
							e.printStackTrace();
						}*/
	                }else if(cell.getColumnIndex() == 9){
	                	barcodes = cell.getStringCellValue();
	                }/*else if(cell.getColumnIndex() == 10){
	                	redeemed = cell.getStringCellValue();
	                }*/
	            }
	          
	            if(orderId == null){
	            	continue;
	            }
	            order = orderMap.get(orderId);
	            if(order==null){
	            	continue;
	            }
	            /*if(redeemed != null && !redeemed.trim().isEmpty()){
	            	if(redeemed.trim().equalsIgnoreCase("Y")){
	            		order.setIsRedeemed(true);
	            	}else{
	            		order.setIsRedeemed(false);
	            	}
	            }*/
	           order.setBarcode(barcodes);
	            updatedOrderList.add(order);
	            count++;
	        }
	        
	        if(updatedOrderList.isEmpty()){
	        	genericResponseDTO.setStatus(1);
				genericResponseDTO.setMessage("No records are updated, please select valid file where first column have order id.");
				return genericResponseDTO;
	        }
		
	        DAORegistry.getGiftCardOrderDAO().updateAll(updatedOrderList);
	        
			genericResponseDTO.setStatus(1);
			genericResponseDTO.setMessage(count+" Records are updated successfully.");
			return genericResponseDTO;
			
		} catch (Exception e) {			
			e.printStackTrace();
			error.setDescription("Something went wrong while Uploading Gift card order barcodes..");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
			return genericResponseDTO;
		}
	}
	
	
	
	
	@RequestMapping(value="/GetGiftCardBrands")
	public RtfGiftCardsDTO getGiftCardBrands(HttpServletRequest request, HttpServletResponse response){
		RtfGiftCardsDTO giftCardDTO = new RtfGiftCardsDTO();
		Error error = new Error();
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String gbStatus = request.getParameter("gbStatus");
			String sortingString = request.getParameter("sortingString");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getGiftCardBrandFilter(headerFilter+sortingString);
			if(gbStatus==null || gbStatus.isEmpty()){
				giftCardDTO.setStatus(0);
				error.setDescription("Please send valid status..");
				giftCardDTO.setError(error);
				return giftCardDTO;
			}
			List<GiftCardBrand> brands = DAORegistry.getGiftCardBrandDAO().getAllGiftCardBrands(filter,gbStatus) ;
			giftCardDTO.setBrands(brands);
			giftCardDTO.setPagination(PaginationUtil.getDummyPaginationParameters(brands.size()));
			giftCardDTO.setStatus(1);
			if(brands.isEmpty()){
				giftCardDTO.setStatus(1);
				giftCardDTO.setMessage("No Gift Card Brands are found with seleted search filters.");
				return giftCardDTO;
			}
			return giftCardDTO;
		} catch (Exception e) {
			e.printStackTrace();
			giftCardDTO.setStatus(0);
			error.setDescription("Error occured while fetching gift card brands.");
			giftCardDTO.setError(error);
			return giftCardDTO;
		}
	
	}
	
	
	
	@RequestMapping(value="/UpdateGiftcardBrand")
	public RtfGiftCardsDTO updateGiftcardBrand(HttpServletRequest request, HttpServletResponse response){
		RtfGiftCardsDTO giftCardDTO = new RtfGiftCardsDTO();
		Error error = new Error();
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String gbStatus = request.getParameter("gbStatus");
			String action = request.getParameter("action");
			String gbIdStr = request.getParameter("gbId");
			String userName = request.getParameter("userName");
			String fileRequired = request.getParameter("fileRequired");
			String fileRequired1 = request.getParameter("fileRequired1");
			String sortingString = request.getParameter("sortingString");
			String msg = null;
			GridHeaderFilters filter = GridHeaderFiltersUtil.getGiftCardBrandFilter(headerFilter+sortingString);
			if(gbStatus==null || gbStatus.isEmpty()){
				giftCardDTO.setStatus(0);
				error.setDescription("Please send valid status.");
				giftCardDTO.setError(error);
				return giftCardDTO;
			}
			if(action==null || action.isEmpty()){
				giftCardDTO.setStatus(0);
				error.setDescription("Please send valid action to perform..");
				giftCardDTO.setError(error);
				return giftCardDTO;
			}
			if(action.equalsIgnoreCase("EDIT")){
				if(gbIdStr==null || gbIdStr.isEmpty()){
					giftCardDTO.setStatus(0);
					error.setDescription("Please send gift card brand id.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}
				Integer gbId = null;
				try {
					gbId = Integer.parseInt(gbIdStr);
				} catch (Exception e) {
					giftCardDTO.setStatus(0);
					error.setDescription("Please send valid gift card brand id.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}
				GiftCardBrand brand = DAORegistry.getGiftCardBrandDAO().get(gbId);
				if(brand == null){
					giftCardDTO.setStatus(0);
					error.setDescription("Gift card brand not found with given id.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}
				giftCardDTO.setStatus(1);
				giftCardDTO.setBrand(brand);
				return giftCardDTO;
			}else if(action.equalsIgnoreCase("SAVE") || action.equalsIgnoreCase("UPDATE")){
				String name = request.getParameter("name");
				String description = request.getParameter("description");
				//String quantityStr = request.getParameter("quantity");
				String displayOutOfStock = request.getParameter("displayOutOfStock");
				if(name== null || name.isEmpty()){
					giftCardDTO.setStatus(0);
					error.setDescription("Gift card brand name is mendatory.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}
				if(displayOutOfStock== null || displayOutOfStock.isEmpty()){
					giftCardDTO.setStatus(0);
					error.setDescription("Gift card brand display out of stock image is mendatory.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}
				/*if(quantityStr== null || quantityStr.isEmpty()){
					giftCardDTO.setStatus(0);
					error.setDescription("Gift card brand max quantity limit is mendatory.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}
				Integer quantity = null;
				try {
					quantity = Integer.parseInt(quantityStr);
				} catch (Exception e) {
					giftCardDTO.setStatus(0);
					error.setDescription("Gift card brand max quantity limit is not valid.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}*/
				GiftCardBrand brand = null;
				if(action.equalsIgnoreCase("UPDATE")){
					msg = "Gift card brand updated successfully.";
					if(gbIdStr==null || gbIdStr.isEmpty()){
						giftCardDTO.setStatus(0);
						error.setDescription("Please send gift card brand id.");
						giftCardDTO.setError(error);
						return giftCardDTO;
					}
					Integer gbId = null;
					try {
						gbId = Integer.parseInt(gbIdStr);
					} catch (Exception e) {
						giftCardDTO.setStatus(0);
						error.setDescription("Please send valid gift card brand id.");
						giftCardDTO.setError(error);
						return giftCardDTO;
					}
					brand = DAORegistry.getGiftCardBrandDAO().get(gbId);
					if(brand == null){
						giftCardDTO.setStatus(0);
						error.setDescription("Gift card brand not found with given id.");
						giftCardDTO.setError(error);
						return giftCardDTO;
					}
					brand.setUpdatedDate(new Date());
					brand.setUpdatedBy(userName);
				}else if(action.equalsIgnoreCase("SAVE")){
					msg = "Gift card brand added successfully.";
					brand = new GiftCardBrand();
					brand.setStatus("ACTIVE");
					brand.setCreatedBy(userName);
					brand.setCreatedDate(new Date());
				}
				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
				
				brand.setName(name);
				brand.setDescription(description);
				//brand.setQuantity(quantity);
				brand.setDisplayOutOfStock(false);
				if(displayOutOfStock.equalsIgnoreCase("YES")){
					brand.setDisplayOutOfStock(true);
				}
				DAORegistry.getGiftCardBrandDAO().saveOrUpdate(brand);
				
				if(fileRequired !=null && fileRequired.equalsIgnoreCase("Y")){
					try {
						MultipartFile file = multipartRequest.getFile("imageFile");
						if(file==null){
							giftCardDTO.setStatus(0);
							error.setDescription("Gift card image is mendatory.");
							giftCardDTO.setError(error);
							return giftCardDTO;
						}
						String ext = FilenameUtils.getExtension(file.getOriginalFilename());
						String fileName = "giftcardbrand_"+brand.getId()+"."+ext;
						File newFile = new File(URLUtil.GIFTCARD_IMAGE_DIRECTORY+fileName);
						
						newFile.createNewFile();
						BufferedOutputStream stream = new BufferedOutputStream(
								new FileOutputStream(newFile));
				        FileCopyUtils.copy(file.getInputStream(), stream);
						stream.close();
						
						brand.setFileName(fileName);
						AwsS3Response awsRsponse = AWSFileService.upLoadFileToS3(AWSFileService.BUCKET_NAME_RTF_SERVER_MEDIA, fileName, AWSFileService.GIFTCARD_BRAND_FOLDER, newFile);
						if(null != awsRsponse && awsRsponse.getStatus() != 1){
							giftCardDTO.setStatus(0);
							error.setDescription("Error occured while uploading image to s3 bucket.");
							giftCardDTO.setError(error);
							return giftCardDTO;
						}
						DAORegistry.getGiftCardBrandDAO().saveOrUpdate(brand);
					} catch (Exception e) {
						e.printStackTrace();
						giftCardDTO.setStatus(0);
						error.setDescription("Error occured while uploading gift card brand image.");
						giftCardDTO.setError(error);
						return giftCardDTO;
					}
				}
				
				if(fileRequired1 !=null && fileRequired1.equalsIgnoreCase("Y")){
					try {
						MultipartFile file = multipartRequest.getFile("imageFile1");
						if(file==null){
							giftCardDTO.setStatus(0);
							error.setDescription("Gift card image is mendatory.");
							giftCardDTO.setError(error);
							return giftCardDTO;
						}
						String ext = FilenameUtils.getExtension(file.getOriginalFilename());
						String fileName = "giftcardbrandnostock_"+brand.getId()+"."+ext;
						File newFile = new File(URLUtil.GIFTCARD_IMAGE_DIRECTORY+fileName);
						
						newFile.createNewFile();
						BufferedOutputStream stream = new BufferedOutputStream(
								new FileOutputStream(newFile));
				        FileCopyUtils.copy(file.getInputStream(), stream);
						stream.close();
						brand.setOutOfStockFileName(fileName);
						
						AwsS3Response awsRsponse = AWSFileService.upLoadFileToS3(AWSFileService.BUCKET_NAME_RTF_SERVER_MEDIA, fileName, AWSFileService.GIFTCARD_BRAND_FOLDER, newFile);
						if(null != awsRsponse && awsRsponse.getStatus() != 1){
							giftCardDTO.setStatus(0);
							error.setDescription("Error occured while uploading image to s3 bucket.");
							giftCardDTO.setError(error);
							return giftCardDTO;
						}
						
						DAORegistry.getGiftCardBrandDAO().saveOrUpdate(brand);
					} catch (Exception e) {
						e.printStackTrace();
						giftCardDTO.setStatus(0);
						error.setDescription("Error occured while uploading gift card brand out of stock image.");
						giftCardDTO.setError(error);
						return giftCardDTO;
					}
				}
				
				List<GiftCardBrand> brands  = DAORegistry.getGiftCardBrandDAO().getAllGiftCardBrands(filter, gbStatus);
				giftCardDTO.setBrands(brands);
				giftCardDTO.setPagination(PaginationUtil.getDummyPaginationParameters(brands.size()));
				giftCardDTO.setStatus(1);
				giftCardDTO.setMessage(msg);
				return giftCardDTO;
			}else if(action.equalsIgnoreCase("DELETE")){
				if(gbIdStr==null || gbIdStr.isEmpty()){
					giftCardDTO.setStatus(0);
					error.setDescription("Please send gift card brand id.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}
				Integer gbId = null;
				try {
					gbId = Integer.parseInt(gbIdStr);
				} catch (Exception e) {
					giftCardDTO.setStatus(0);
					error.setDescription("Please send valid gift card brand id.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}
				GiftCardBrand brand = DAORegistry.getGiftCardBrandDAO().get(gbId);
				if(brand==null){
					giftCardDTO.setStatus(0);
					error.setDescription("Cannot delete Gift Card Brand, No gift card brand is found with given id.");
					giftCardDTO.setError(error);
					return giftCardDTO;
				}
				
				brand.setUpdatedDate(new Date());
				brand.setUpdatedBy(userName);
				brand.setStatus("DELETED");
				DAORegistry.getGiftCardBrandDAO().update(brand);
				giftCardDTO.setMessage("Gift Card deleted successfully.");
				List<GiftCardBrand> brands  = DAORegistry.getGiftCardBrandDAO().getAllGiftCardBrands(filter, gbStatus);
				giftCardDTO.setBrands(brands);
				giftCardDTO.setPagination(PaginationUtil.getDummyPaginationParameters(brands.size()));
				giftCardDTO.setStatus(1);
				return giftCardDTO;
			}
			
			return giftCardDTO;
		} catch (Exception e) {
			e.printStackTrace();
			giftCardDTO.setStatus(0);
			error.setDescription("Error occured while updating gift card brand.");
			giftCardDTO.setError(error);
			return giftCardDTO;
		}
	}
	@RequestMapping(value="/GetCustomerProfileQuestions")
	public CustomerProfileQuestionDTO getCustomerProfileQuestions(HttpServletRequest request, HttpServletResponse response){
		CustomerProfileQuestionDTO responseDTO = new CustomerProfileQuestionDTO();
		Error error = new Error();
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String status = request.getParameter("status");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getCustomerProfileQuestions(headerFilter);
			if(status==null || status.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please send valid status.");
				responseDTO.setError(error);
				return responseDTO;
			}
			List<CustomerProfileQuestion> questions = QuizDAORegistry.getCustomerProfileQuestionDAO().getAllCustomerQuestionByStatus(filter,status);
			responseDTO.setQuestions(questions);
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(questions.size()));
			responseDTO.setStatus(1);
			if(questions.isEmpty()){
				responseDTO.setStatus(1);
				responseDTO.setMessage("No customer profile question found in system.");
				return responseDTO;
			}
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while fetching customer profile questions..");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}
	
	
	
	
	@RequestMapping(value="/ChangeCustomerProfileQuestionPosition")
	public CustomerProfileQuestionDTO changeCustomerProfileQuestionPosition(HttpServletRequest request, HttpServletResponse response){
		CustomerProfileQuestionDTO responseDTO = new CustomerProfileQuestionDTO();
		Error error = new Error();
		try {
			String action  = request.getParameter("action");
			String questionIdStr = request.getParameter("questionId");
			String userName = request.getParameter("userName");
			if(action==null || action.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please send valid action.");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(questionIdStr==null || questionIdStr.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please send quesiton identifier.");
				responseDTO.setError(error);
				return responseDTO;
			}
			Integer qId = 0;
			try {
				qId = Integer.parseInt(questionIdStr);
			} catch (Exception e) {
				responseDTO.setStatus(0);
				error.setDescription("Please send valid quesiton identifier.");
				responseDTO.setError(error);
				return responseDTO;
			}
			CustomerProfileQuestion question = QuizDAORegistry.getCustomerProfileQuestionDAO().get(qId);
			if(question == null){
				responseDTO.setStatus(0);
				error.setDescription("Question not found in system with given identifier.");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(question.getProfileType().equalsIgnoreCase("BASIC")){
				responseDTO.setStatus(0);
				error.setDescription("Question with profile type as basic is not allowed to update.");
				responseDTO.setError(error);
				return responseDTO;
			}
			if(action.equalsIgnoreCase("UP")){
				Integer currentPosition = question.getSerialNo();
				CustomerProfileQuestion upQuestion = QuizDAORegistry.getCustomerProfileQuestionDAO().getQuestionBySerialNo(currentPosition-1);
				if(upQuestion == null){
					responseDTO.setStatus(0);
					error.setDescription("Selected customer profile question position is already first cannot move it to UP.");
					responseDTO.setError(error);
					return responseDTO;
				}
				question.setSerialNo(currentPosition-1);
				upQuestion.setSerialNo(currentPosition);
				upQuestion.setUpdatedBy(userName);
				question.setUpdatedBy(userName);
				upQuestion.setUpdatedDate(new Date());
				question.setUpdatedDate(new Date());
				
				QuizDAORegistry.getCustomerProfileQuestionDAO().update(question);
				QuizDAORegistry.getCustomerProfileQuestionDAO().update(upQuestion);
			}else if(action.equalsIgnoreCase("DOWN")){
				Integer currentPosition = question.getSerialNo();
				CustomerProfileQuestion downQuestion = QuizDAORegistry.getCustomerProfileQuestionDAO().getQuestionBySerialNo(currentPosition+1);
				if(downQuestion == null){
					responseDTO.setStatus(0);
					error.setDescription("Selected customer profile questiuon position is already last cannot move it to DOWN..");
					responseDTO.setError(error);
					return responseDTO;
				}
				question.setSerialNo(currentPosition+1);
				downQuestion.setSerialNo(currentPosition);
				downQuestion.setUpdatedBy(userName);
				question.setUpdatedBy(userName);
				downQuestion.setUpdatedDate(new Date());
				question.setUpdatedDate(new Date());
				QuizDAORegistry.getCustomerProfileQuestionDAO().update(question);
				QuizDAORegistry.getCustomerProfileQuestionDAO().update(downQuestion);
			}
			List<CustomerProfileQuestion> questions = QuizDAORegistry.getCustomerProfileQuestionDAO().getAllCustomerQuestionByStatus(new GridHeaderFilters(),"ACTIVE");
			responseDTO.setQuestions(questions);
			responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(questions.size()));
			responseDTO.setStatus(1);
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while updating customer profile questions position.");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}
	
	
	
	@RequestMapping(value="/UpdateCustomerProfileQuestion")
	public CustomerProfileQuestionDTO updateCustomerProfileQuestion(HttpServletRequest request, HttpServletResponse response){
		CustomerProfileQuestionDTO responseDTO = new CustomerProfileQuestionDTO();
		Error error = new Error();
		try {
			String questionText = request.getParameter("question");
			String questionType = request.getParameter("questionType");
			String pointsStr = request.getParameter("points");
			String profileType = request.getParameter("profileType");
			//String serialNo = request.getParameter("serialNo");
			String questionIdStr = request.getParameter("questionId");
			String userName = request.getParameter("userName");
			String action = request.getParameter("action");
			
			Integer serialNo=0;
			if(action == null || action.isEmpty()){
				responseDTO.setStatus(0);
				error.setDescription("Please send valid action to update/save customer profile questions..");
				responseDTO.setError(error);
				return responseDTO;
			}
			GridHeaderFilters filter = GridHeaderFiltersUtil.getGiftCardFilter(null);
			if(action.equalsIgnoreCase("EDIT")){
				if(questionIdStr==null || questionIdStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Cannot Edit customer profile question id not found.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer qId = 0;
				try {
					qId = Integer.parseInt(questionIdStr);
				} catch (Exception e) {
					e.printStackTrace();
					responseDTO.setStatus(0);
					error.setDescription("Cannot Edit invalid customer profile question id.");
					responseDTO.setError(error);
					return responseDTO;
				}
				CustomerProfileQuestion question = QuizDAORegistry.getCustomerProfileQuestionDAO().get(qId);
				if(question == null){
					responseDTO.setStatus(0);
					error.setDescription("Cannot Edit customer profile question its not found with given identifier.");
					responseDTO.setError(error);
					return responseDTO;
				}
				/*if(question.getProfileType().equalsIgnoreCase("BASIC")){
					responseDTO.setStatus(0);
					error.setDescription("Basic profile question are not allowed to update.");
					responseDTO.setError(error);
					return responseDTO;
				}*/
				responseDTO.setQuestion(question);
				responseDTO.setStatus(1);
				return responseDTO;
			}else if(action.equalsIgnoreCase("SAVE") || action.equalsIgnoreCase("UPDATE")){
				if(questionType==null || questionType.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Question Type is Mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(profileType==null || profileType.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Profile type is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
			/*	if(serialNo==null || serialNo.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Serial No is mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}*/
				if(pointsStr==null || pointsStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Question points are mendatory.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer points = null;
				try {
					points = Integer.parseInt(pointsStr);
				} catch (Exception e) {
					responseDTO.setStatus(0);
					error.setDescription("Please provide valid question points.");
					responseDTO.setError(error);
					return responseDTO;
				}
				
				CustomerProfileQuestion question = null;
				String msg = null;
				if(action.equalsIgnoreCase("UPDATE")){
					if(questionIdStr==null || questionIdStr.isEmpty()){
						responseDTO.setStatus(0);
						error.setDescription("Cannot update customer profile question id not found.");
						responseDTO.setError(error);
						return responseDTO;
					}
					Integer qId = 0;
					try {
						qId = Integer.parseInt(questionIdStr);
					} catch (Exception e) {
						e.printStackTrace();
						responseDTO.setStatus(0);
						error.setDescription("Cannot update invalid customer profile question id.");
						responseDTO.setError(error);
						return responseDTO;
					}
					question = QuizDAORegistry.getCustomerProfileQuestionDAO().get(qId);
					if(question == null){
						responseDTO.setStatus(0);
						error.setDescription("Cannot update customer profile question its not found with given identifier.");
						responseDTO.setError(error);
						return responseDTO;
					}
					question.setUpdatedDate(new Date());
					question.setUpdatedBy(userName);
					if(question.getProfileType().equalsIgnoreCase("BASIC")){
						question.setPoints(points);
					}else{
						if(questionText==null || questionText.isEmpty()){
							responseDTO.setStatus(0);
							error.setDescription("Question text is mendatory.");
							responseDTO.setError(error);
							return responseDTO;
						}
						question.setQuestion(questionText);
						question.setPoints(points);
					}
					QuizDAORegistry.getCustomerProfileQuestionDAO().update(question);
					reComputeProfilePoints();
					msg = "Customer profile question updated successfully.";
				}else{
					if(questionText==null || questionText.isEmpty()){
						responseDTO.setStatus(0);
						error.setDescription("Question text is mendatory.");
						responseDTO.setError(error);
						return responseDTO;
					}
					question = new CustomerProfileQuestion();
					CustomerProfileQuestion lastQuestion = QuizDAORegistry.getCustomerProfileQuestionDAO().getLastQuestion();
					if(lastQuestion==null){
						serialNo = 1;
					}else{
						serialNo = lastQuestion.getSerialNo() + 1;
					}
					question.setCreatedDate(new Date());
					question.setCreatedBy(userName);
					question.setQuestion(questionText);
					question.setPoints(points);
					question.setProfileType(profileType);
					question.setQuestionType(questionType);
					question.setSerialNo(serialNo);
					question.setStatus("ACTIVE");
					QuizDAORegistry.getCustomerProfileQuestionDAO().save(question);
					reComputeProfilePoints();
					msg = "Customer profile question saved successfully.";
				}
				List<CustomerProfileQuestion> questions = QuizDAORegistry.getCustomerProfileQuestionDAO().getAllCustomerQuestionByStatus(filter, "ACTIVE");
				responseDTO.setQuestions(questions);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(questions.size()));
				responseDTO.setStatus(1);
				responseDTO.setMessage(msg);
				return responseDTO;
			}else if(action.equalsIgnoreCase("DELETE")){
				if(questionIdStr==null || questionIdStr.isEmpty()){
					responseDTO.setStatus(0);
					error.setDescription("Cannot update customer profile question id not found.");
					responseDTO.setError(error);
					return responseDTO;
				}
				Integer qId = 0;
				try {
					qId = Integer.parseInt(questionIdStr);
				} catch (Exception e) {
					e.printStackTrace();
					responseDTO.setStatus(0);
					error.setDescription("Cannot update invalid customer profile question id.");
					responseDTO.setError(error);
					return responseDTO;
				}
				CustomerProfileQuestion question = QuizDAORegistry.getCustomerProfileQuestionDAO().get(qId);
				if(question == null){
					responseDTO.setStatus(0);
					error.setDescription("Cannot update customer profile question its not found with given identifier.");
					responseDTO.setError(error);
					return responseDTO;
				}
				if(question.getProfileType().equalsIgnoreCase("BASIC")){
					responseDTO.setStatus(0);
					error.setDescription("Question with profile type as basic is not allowed to delete.");
					responseDTO.setError(error);
					return responseDTO;
				}
				question.setStatus("DELETED");
				question.setSerialNo(-1);
				question.setUpdatedDate(new Date());
				question.setUpdatedBy(userName);
				
				
				QuizDAORegistry.getCustomerProfileQuestionDAO().update(question);
				filter.setText2("FANDOM");
				List<CustomerProfileQuestion> questions = QuizDAORegistry.getCustomerProfileQuestionDAO().getAllCustomerQuestionByStatus(filter, "ACTIVE");
				Integer i = 1;
				for(CustomerProfileQuestion q : questions){
					q.setSerialNo(i);
					i++;
				}
				QuizDAORegistry.getCustomerProfileQuestionDAO().updateAll(questions);
				reComputeProfilePoints();
				responseDTO.setQuestions(questions);
				responseDTO.setPagination(PaginationUtil.getDummyPaginationParameters(questions.size()));
				responseDTO.setStatus(1);
				responseDTO.setMessage("Selected question deleted successfully.");
				return responseDTO;
			}
			responseDTO.setStatus(0);
			error.setDescription("Not able to identify action type EDIT/SAVE/UPDATE/DELETE.");
			responseDTO.setError(error);
			return responseDTO;
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setStatus(0);
			error.setDescription("Error occured while fetching customer profile question.");
			responseDTO.setError(error);
			return responseDTO;
		}
	
	}
	
	
	
	@RequestMapping(value="/CopyContestSetup")
	public GenericResponseDTO copyContestSetup(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericDTO = new GenericResponseDTO();
		Error error = new Error();
		Connection con = null;
		try {
			String contestIdStr = request.getParameter("contestId");
			String userName = request.getParameter("userName");
			
			if(contestIdStr == null || contestIdStr.isEmpty()){
				System.err.println("Please send contest Id to copy contest to Host App.");
				error.setDescription("Please send contest Id to copy contest to Host App.");
				genericDTO.setError(error);
				genericDTO.setStatus(0);
				return genericDTO;
			}
			
			Integer contestId = 0;
			try {
				contestId = Integer.parseInt(contestIdStr);
			} catch (Exception e) {
				e.printStackTrace();
				System.err.println("Please send valid contest Id to copy contest to Host App.");
				error.setDescription("Please send valid contest Id to copy contest to Host App.");
				genericDTO.setError(error);
				genericDTO.setStatus(0);
				return genericDTO;
			}
			Contests c = QuizDAORegistry.getContestsDAO().get(contestId);
			if(c == null){
				System.err.println("Contest is not found with give contest Id.");
				error.setDescription("Contest is not found with give contest Id.");
				genericDTO.setError(error);
				genericDTO.setStatus(0);
				return genericDTO;
			}
			List<ContestQuestions> questions = QuizDAORegistry.getContestQuestionsDAO().getContestQuestions(new GridHeaderFilters(), null, contestId,sharedProperty);
			//RTFPromoOffers offer = DAORegistry.getRtfPromoOffersDAO().get(c.getRtfPromoOfferId());
			//List<RTFPromoType> offerTypes = DAORegistry.getRtfPromoTypeDAO().getPromoTypeFromOfferId(c.getRtfPromoOfferId());
			
			StringBuffer contestSQL = new StringBuffer();
			contestSQL.append("INSERT INTO contest ");
			contestSQL.append("(contest_name, contest_start_datetime, participants_count,");
			contestSQL.append("winners_count, ticket_winners_count, point_winners_count, status,");
			contestSQL.append("created_datetime, updated_datetime, created_by, updated_by,");
			contestSQL.append("eligible_free_ticket_winners, free_tickets_per_winner, points_per_winner,");
			contestSQL.append("no_of_questions, contest_type, total_rewards, contest_mode,");
			contestSQL.append("question_size, rewards_per_question, is_customer_stats_updated,");
			contestSQL.append("zone, promotional_code, discount_percentage, promo_ref_id,");
			contestSQL.append("promo_ref_name, promo_expiry_date, promo_ref_type, promo_offer_id,");
			contestSQL.append("last_question_no, last_action, single_tix_price, process_status,");
			contestSQL.append("extended_name, contest_category, is_test_contest, referral_rewards,");
			contestSQL.append("participant_star, referral_star, participant_lives,");
			contestSQL.append("participant_rewards, mega_jackpot, no_of_winner_per_question,");
			contestSQL.append("gift_card_value_id, gift_card_per_winner, contest_jackpot_type, ");
			contestSQL.append("actual_start_time, actual_end_time, question_reward_type, ");
			contestSQL.append("contest_password, participants_rtf_points, referral_rtf_points) ");
			contestSQL.append("VALUES ('"+c.getContestName().replaceAll("'","''")+"',");
			contestSQL.append("'"+com.rtw.tmat.utils.Util.getDateWithTwentyFourHourFormat(c.getStartDateTime()).replaceAll("/","-")+"',0,0,0,0,");
			contestSQL.append("'ACTIVE',getDate(),getDate(),'"+userName+"','"+userName+"',");
			contestSQL.append(c.getTicketWinnerThreshhold()+","+c.getFreeTicketPerWinner()+",NULL,"+c.getQuestionSize()+",'"+c.getType()+"',"+c.getRewardPoints()+",");
			contestSQL.append("'"+c.getContestMode()+"',NULL,NULL,0,'"+c.getZone()+"','"+c.getPromotionalCode()+"',"+c.getDiscountPercentage()+","+c.getPromoRefId()+",");
			contestSQL.append("'"+c.getPromoRefName().replaceAll("'","''")+"','"+com.rtw.tmat.utils.Util.getDateWithTwentyFourHourFormat(c.getPromoExpiryDate()).replaceAll("/","-")+"',");
			contestSQL.append("'"+c.getPromoRefType()+"',"+c.getRtfPromoOfferId()+",NULL,NULL,"+c.getSingleTicketPrice()+",NULL,'"+c.getExtendedName().replaceAll("'","''")+"','"+c.getContestCategory()+"',");
			contestSQL.append("0,"+c.getReferralRewards()+","+c.getParticipantStars()+","+c.getReferralStars()+","+c.getParticipantLives()+","+c.getParticipantRewards());
			contestSQL.append(" ,0,"+c.getMjpWinnerPerQuestion()+","+c.getGiftCardId()+","+c.getGiftCardPerWinner()+",'"+c.getContestJackpotType()+"',NULL,NULL,'POINTS','"+c.getContestPassword()+"',0,0);");

			System.out.println("COPY CONTEST : "+contestSQL.toString());
			
			DriverManager.registerDriver(new SQLServerDriver());
			con = DriverManager.getConnection("jdbc:sqlserver://10.0.1.51:1433;DatabaseName=RTFQuizMaster_Sanbox", "tayo", "po01ro02ro30");
			if(con==null){
				System.err.println("Cannot Copy contest, Not able to connect to Sandbox Database.");
				error.setDescription("Cannot Copy contest, Not able to connect to Sandbox Database.");
				genericDTO.setError(error);
				genericDTO.setStatus(0);
				return genericDTO;
			}
			
			int newContestId = 0;
			PreparedStatement stat = con.prepareStatement(contestSQL.toString(),Statement.RETURN_GENERATED_KEYS);
			stat.executeUpdate();
			ResultSet result = stat.getGeneratedKeys();
			if(result.next()){
				newContestId = result.getInt(1);
			}
			
			StringBuffer questionSQL = new StringBuffer();
			for(ContestQuestions q : questions){
				Integer minJAckpotType = null;
				if(q.getMiniJackpotType()!=null){
					minJAckpotType = q.getMiniJackpotType().ordinal();
				}
				questionSQL.append("INSERT INTO contest_questions VALUES ("+newContestId+","+q.getSerialNo()+",'"+q.getQuestion().replaceAll("'","''")+"','"+q.getOptionA().replaceAll("'","''")+"','"+q.getOptionB().replaceAll("'","''")+"',");
				questionSQL.append("'"+q.getOptionC().replaceAll("'","''")+"',NULL,'"+q.getAnswer()+"',getDate(),getDate(),'"+userName+"',NULL,NULL,"+q.getQuestionReward()+",'"+q.getDifficultyLevel()+"',"+minJAckpotType);
				questionSQL.append(","+q.getMjpNoOfWinner()+","+q.getMjpQtyPerWinner()+","+q.getMjpGiftCardId()+");");
			}
			System.out.println("COPY QUESTIONS : "+questionSQL.toString());
			
			Statement statement = con.createStatement();
			statement.executeUpdate(questionSQL.toString());
			
			genericDTO.setStatus(1);
			genericDTO.setMessage("Contest Copied Successfully to Host App.");
			return genericDTO;
		} catch (Exception e) {
			e.printStackTrace();
			genericDTO.setStatus(0);
			error.setDescription("Error occured while Copying Contest to Host App.");
			genericDTO.setError(error);
			return genericDTO;
		}finally{
			if(con!=null){
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	
	
	private void reComputeProfilePoints(){
		try {
			List<CustomerProfileQuestion> questions = QuizDAORegistry.getCustomerProfileQuestionDAO().getAllCustomerQuestionByStatus(new GridHeaderFilters(), "ACTIVE");
			Integer totalPoints = 0;
			for(CustomerProfileQuestion q : questions){
				if(q.getPoints()!=null){
					totalPoints += q.getPoints();
				}
			}
			
			List<RtfRewardConfigInfo> configs = QuizDAORegistry.getRtfRewardConfigInfoDAO().getRtfRewardConfigByRewardType("FILL_CUSTOMER_PROFILE");
			if(configs!=null && !configs.isEmpty()){
				RtfRewardConfigInfo config = configs.get(0);
				config.setRtfPoints(totalPoints);
				QuizDAORegistry.getRtfRewardConfigInfoDAO().update(config);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	private boolean uploadGiftcardFiles(HttpServletRequest request,GiftCardOrders order){
		boolean flag=false;
		try {
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			GiftCardOrderAttachment attachment;
			Map<String, String[]> requestParams = request.getParameterMap();
			List<GiftCardOrderAttachment> orderAttachment = null;
			String userName = request.getParameter("userName");
			Date now = new Date();
			boolean isUpdate = false;
			for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
				String key = entry.getKey();
				String fileOriginalName = null;
				if(key.contains("egiftcardTable_")){
					Integer rowNumber = Integer.parseInt(request.getParameter("egiftcardTable_count"));
					int cnt=0;
					for(int i=1;i<=rowNumber;i++){
						cnt++;
						isUpdate=false;
						MultipartFile file = multipartRequest.getFile("egiftcard_"+i);
						if(file==null){
							continue;
						}
						if(file != null && file.getSize() > 0) {
							File newFile = null;
							fileOriginalName = file.getOriginalFilename();
							String ext = FilenameUtils.getExtension(file.getOriginalFilename());
							String fullFileName =  order.getCardTitle()+"_"+order.getId()+"_egiftcard_"+cnt+"."+ext;
							flag=true;
							orderAttachment = DAORegistry.getGiftCardOrderAttachmentDAO().getOrderEGiftCardAttachments(order.getId());
							for(GiftCardOrderAttachment attach : orderAttachment){
								if(attach.getPosition()!= null && attach.getPosition() == cnt){
									newFile = new File(attach.getFilePath());
									if(newFile.exists()){
										newFile.delete();
									}
									attach.setOrderId(order.getId());
									attach.setFilePath(URLUtil.EGIFTCARD_DIRECTORY+fullFileName);
									attach.setfName(fullFileName);
									attach.setOriginalFileName(fileOriginalName);
									attach.setPosition(cnt);
									attach.setFileType(FileType.EGIFTCARD.toString());
									attach.setCreatedBy(userName);
									attach.setCreatedDate(now);
									attach.setIsEmailSent(false);
									attach.setIsFileSent(false);
									DAORegistry.getGiftCardOrderAttachmentDAO().update(attach);
									isUpdate=true;
									break;
								}
							}
							if(!isUpdate){
								attachment = new GiftCardOrderAttachment();
								attachment.setOrderId(order.getId());
								attachment.setFilePath(URLUtil.EGIFTCARD_DIRECTORY+fullFileName);
								attachment.setfName(fullFileName);
								attachment.setOriginalFileName(fileOriginalName);
								attachment.setPosition(cnt);
								attachment.setFileType(FileType.EGIFTCARD.toString());
								attachment.setCreatedBy(userName);
								attachment.setCreatedDate(now);
								attachment.setIsEmailSent(false);
								attachment.setIsFileSent(false);
								DAORegistry.getGiftCardOrderAttachmentDAO().save(attachment);
								
							}
							isUpdate=false;
							newFile = new File(URLUtil.EGIFTCARD_DIRECTORY+fullFileName);
							newFile.createNewFile();
							BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(newFile));
					        FileCopyUtils.copy(file.getInputStream(), stream);
							stream.close();
						}
					}
				}else if(key.contains("barcodeTable_")){
					Integer rowNumber = Integer.parseInt(request.getParameter("barcodeTable_count"));
					int cnt=0;
					for(int i=1;i<=rowNumber;i++){
						isUpdate=false;
						cnt++;
						MultipartFile file = multipartRequest.getFile("barcode_"+i);
						if(file==null){
							continue;
						}
						if(file != null && file.getSize() > 0) {
							File newFile = null;
							fileOriginalName = file.getOriginalFilename();
							String ext = FilenameUtils.getExtension(file.getOriginalFilename());
							String fullFileName =  order.getCardTitle()+"_"+order.getId()+"_barcode_"+cnt+"."+ext;
							flag=true;
							orderAttachment = DAORegistry.getGiftCardOrderAttachmentDAO().getOrderEGiftCardAttachments(order.getId());
							for(GiftCardOrderAttachment attach : orderAttachment){
								if(attach.getPosition()!= null && attach.getPosition() == cnt){
									newFile = new File(attach.getFilePath());
									if(newFile.exists()){
										newFile.delete();
									}
									attach.setOrderId(order.getId());
									attach.setFilePath(URLUtil.EGIFTCARD_DIRECTORY+fullFileName);
									attach.setfName(fullFileName);
									attach.setOriginalFileName(fileOriginalName);
									attach.setPosition(cnt);
									attach.setFileType(FileType.BARCODE.toString());
									attach.setCreatedBy(userName);
									attach.setCreatedDate(now);
									attach.setIsEmailSent(false);
									attach.setIsFileSent(false);
									DAORegistry.getGiftCardOrderAttachmentDAO().update(attach);
									isUpdate=true;
									break;
								}
							}
							if(!isUpdate){
								attachment = new GiftCardOrderAttachment();
								attachment.setOrderId(order.getId());
								attachment.setFilePath(URLUtil.EGIFTCARD_DIRECTORY+fullFileName);
								attachment.setfName(fullFileName);
								attachment.setOriginalFileName(fileOriginalName);
								attachment.setPosition(cnt);
								attachment.setFileType(FileType.BARCODE.toString());
								attachment.setCreatedBy(userName);
								attachment.setCreatedDate(now);
								attachment.setIsEmailSent(false);
								attachment.setIsFileSent(false);
								DAORegistry.getGiftCardOrderAttachmentDAO().save(attachment);
								
							}
							isUpdate=false;
							newFile = new File(URLUtil.EGIFTCARD_DIRECTORY+fullFileName);
							newFile.createNewFile();
							BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(newFile));
					        FileCopyUtils.copy(file.getInputStream(), stream);
							stream.close();
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			flag = false;
		}
		return flag;
	}
	
	
	
			
}
