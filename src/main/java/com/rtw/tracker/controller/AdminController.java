package com.rtw.tracker.controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.chain.web.servlet.ServletSetLocaleCommand;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rtf.ecomerce.dao.impl.EcommDAORegistry;
import com.rtf.ecomerce.data.RTFSellerDetails;
import com.rtw.tmat.utils.Error;
import com.rtw.tmat.utils.Util;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.Artist;
import com.rtw.tracker.datas.ChildCategory;
import com.rtw.tracker.datas.Country;
import com.rtw.tracker.datas.Customer;
import com.rtw.tracker.datas.Customers;
import com.rtw.tracker.datas.EmailBlastTracking;
import com.rtw.tracker.datas.EmailTemplate;
import com.rtw.tracker.datas.GrandChildCategory;
import com.rtw.tracker.datas.ManualFedexGeneration;
import com.rtw.tracker.datas.PayPalTracking;
import com.rtw.tracker.datas.Role;
import com.rtw.tracker.datas.TrackerUser;
import com.rtw.tracker.datas.UserAction;
import com.rtw.tracker.datas.Venue;
import com.rtw.tracker.pojos.AddUserDTO;
import com.rtw.tracker.pojos.AuditUsersDTO;
import com.rtw.tracker.pojos.AutoCompleteArtistDTO;
import com.rtw.tracker.pojos.AutoCompleteVenueDTO;
import com.rtw.tracker.pojos.EditTrackerUserCustomDTO;
import com.rtw.tracker.pojos.EditUsersDTO;
import com.rtw.tracker.pojos.EmailBlastCustomersDTO;
import com.rtw.tracker.pojos.GenericResponseDTO;
import com.rtw.tracker.pojos.ManageUsersDTO;
import com.rtw.tracker.pojos.ManualFedexDTO;
import com.rtw.tracker.pojos.PaypalTrackingDTO;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.GridHeaderFiltersUtil;
import com.rtw.tracker.utils.GsonCustomConfig;
import com.rtw.tracker.utils.JsonWrapperUtil;
import com.rtw.tracker.utils.PaginationUtil;

@Controller
public class AdminController {
	
	//@Autowired
	//private SessionRegistry sessionRegistry;
	
	@RequestMapping({"/ManageUsers"})
	public String loadManageUsersPage(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		ManageUsersDTO manageUsersDTO = new ManageUsersDTO();
		Error error = new Error();
		model.addAttribute("manageUsersDTO", manageUsersDTO);
		
		try{			
			String status = request.getParameter("status");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			Integer count = 0;
			Collection<TrackerUser> trackerUserList = null;
			GridHeaderFilters filter = GridHeaderFiltersUtil.getUserSearchHeaderFilters(headerFilter);
			if(pageNo == null || pageNo.isEmpty()){
				trackerUserList = DAORegistry.getTrackerUserDAO().getAllTrackerUser(filter,status,pageNo);
				if(trackerUserList != null && trackerUserList.size() > 0){
					count = trackerUserList.size();
				}
			}else{
				trackerUserList = DAORegistry.getTrackerUserDAO().getAllTrackerUser(filter,status,pageNo);
				if(trackerUserList != null && trackerUserList.size() > 0){
					count = trackerUserList.size();
				}
			}
			
			if(trackerUserList == null || trackerUserList.size() <= 0){				
				manageUsersDTO.setMessage("No "+status+" Users found.");
			}			
			manageUsersDTO.setTrackerUserCustomDTO(com.rtw.tracker.utils.Util.getMangeUsersObject(trackerUserList));
			manageUsersDTO.setPaginationDTO(PaginationUtil.calculatePaginationParameter(count, null));
			manageUsersDTO.setStatus(1);
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Users");
			manageUsersDTO.setError(error);
			manageUsersDTO.setStatus(0);
		}
		return "page-admin-manage-users";
	}
	
	@RequestMapping({"/GetManageUsers"})
	public void loadManageUsers(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		JSONObject returnObject = new JSONObject();
		try{
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String status = request.getParameter("status");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getUserSearchHeaderFilters(headerFilter);
			Collection<TrackerUser> trackerUserList = DAORegistry.getTrackerUserDAO().getAllTrackerUser(filter, status,pageNo);
			Integer count = 0;
			
			if(trackerUserList != null && trackerUserList.size() > 0){
				count = trackerUserList.size();
			}else if(trackerUserList == null || trackerUserList.size() <= 0){
				returnObject.put("msg", "No "+status+" Users found.");
			}
			returnObject.put("trackerUserList", JsonWrapperUtil.getMangeUsersArray(trackerUserList));
			returnObject.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));
			IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();			
		}
	}
	
	@RequestMapping({"/AddUser"})
	public AddUserDTO addUserPage(HttpServletRequest request, HttpServletResponse response, Model model){
		AddUserDTO addUserDTO = new AddUserDTO();
		Error error = new Error();
		model.addAttribute("addUserDTO", addUserDTO);
		
		try{
			//String returnMessage = "";			
			String action = request.getParameter("action");
			String roleAffiliateBroker = request.getParameter("roleAffiliateBroker");
			String sessionUser = request.getParameter("sessionUser");
			if(StringUtils.isEmpty(sessionUser)){
				System.err.println("Please Login.");
				error.setDescription("Please Login.");
				addUserDTO.setError(error);
				addUserDTO.setStatus(0);
				return addUserDTO;
			}
			
			TrackerUser trackerUser = new TrackerUser();
			
			if(action != null && action.equals("action")){
				Gson gson = GsonCustomConfig.getGsonBuilder();
								
				String userName = request.getParameter("userName");
				String firstName = request.getParameter("firstName");
				String lastName = request.getParameter("lastName");
				String email = request.getParameter("email");
				String password = request.getParameter("password");
				String rePassword = request.getParameter("rePassword");
				String phone = request.getParameter("phone");
				String roless = request.getParameter("role");
				String sellerIdStr = request.getParameter("sellerId");
								
				if(StringUtils.isEmpty(userName)){
					System.err.println("Please provide User Name.");
					error.setDescription("Please provide User Name.");
					addUserDTO.setError(error);
					addUserDTO.setStatus(0);
					return addUserDTO;
				}
				if(StringUtils.isEmpty(firstName)){
					System.err.println("Please provide First Name.");
					error.setDescription("Please provide First Name.");
					addUserDTO.setError(error);
					addUserDTO.setStatus(0);
					return addUserDTO;
				}
				if(StringUtils.isEmpty(lastName)){
					System.err.println("Please provide Last Name.");
					error.setDescription("Please provide Last Name.");
					addUserDTO.setError(error);
					addUserDTO.setStatus(0);
					return addUserDTO;
				}
				if(StringUtils.isEmpty(email)){
					System.err.println("Please provide Email.");
					error.setDescription("Please provide Email.");
					addUserDTO.setError(error);
					addUserDTO.setStatus(0);
					return addUserDTO;
				}
				if(!validateEMail(email)){
					System.err.println("Please provide Valid Email.");
					error.setDescription("Please provide Valid Email.");
					addUserDTO.setError(error);
					addUserDTO.setStatus(0);
					return addUserDTO;
				}
				if(StringUtils.isEmpty(password)){
					System.err.println("Please provide Password.");
					error.setDescription("Please provide Password.");
					addUserDTO.setError(error);
					addUserDTO.setStatus(0);
					return addUserDTO;
				}				
				if(StringUtils.isEmpty(rePassword)){
					System.err.println("Please provide Re-Password.");
					error.setDescription("Please provide Re-Password.");
					addUserDTO.setError(error);
					addUserDTO.setStatus(0);
					return addUserDTO;
				}
				if(!password.equals(rePassword)){
					System.err.println("Password and Re-Password Must match.");
					error.setDescription("Password and Re-Password Must match.");
					addUserDTO.setError(error);
					addUserDTO.setStatus(0);
					return addUserDTO;
				}
				if(StringUtils.isEmpty(roless)){
					System.err.println("Please select Roles.");
					error.setDescription("Please select Roles.");
					addUserDTO.setError(error);
					addUserDTO.setStatus(0);
					return addUserDTO;
				}
				
				String roles[] = null;
				if(StringUtils.isNotEmpty(roless)){
					roles = gson.fromJson(roless, new TypeToken<String[]>(){}.getType());
				}
				
				TrackerUser trackerUserDb = DAORegistry.getTrackerUserDAO().getTrackerUserByUsernameOrEmail(userName, email);				
				if(trackerUserDb != null){
					System.err.println("There is already an user with this Username or Email.");
					error.setDescription("There is already an user with this Username or Email.");
					addUserDTO.setError(error);
					addUserDTO.setStatus(0);
					return addUserDTO;
				}
				
				// adding user to database
				trackerUser.setUserName(userName);
				trackerUser.setFirstName(firstName);
				trackerUser.setLastName(lastName);
				trackerUser.setEmail(email);
				trackerUser.setPassword(encryptPwd(password));
				if(phone == null || phone.isEmpty()){
					trackerUser.setPhone(null);
				}else{
					trackerUser.setPhone(phone);
				}
				
				Set<Role> roleList = new HashSet<Role>();
				Boolean isSeller = false;
				for(String role : roles){
					Role roleDb = DAORegistry.getRoleDAO().getRoleByName(role.toUpperCase());						
					roleList.add(roleDb);
					if(roleDb.getName().equalsIgnoreCase("ROLE_SELLER")){
						isSeller  = true;
					}
				}
				
				RTFSellerDetails sellerDtl = null;
				if(sellerIdStr!=null && !sellerIdStr.isEmpty()){
					sellerDtl = EcommDAORegistry.getrTFSellerDetailsDAO().get(Integer.parseInt(sellerIdStr));
					if(sellerDtl == null ){
						System.err.println("Seller not found with given identified.");
						error.setDescription("Seller not found with given identified.");
						addUserDTO.setError(error);
						addUserDTO.setStatus(0);
						return addUserDTO;
					}
					if(sellerDtl.getLogId()!=null  && sellerDtl.getLogId() > 0){
						System.err.println("Selected seller is already attached with other tracker user.");
						error.setDescription("Selected seller is already attached with other tracker user.");
						addUserDTO.setError(error);
						addUserDTO.setStatus(0);
						return addUserDTO;
					}
				}
				
				
				trackerUser.setRoles(roleList);						
				trackerUser.setStatus(true);
				trackerUser.setCreateDate(new Date());
				trackerUser.setCreatedBy(sessionUser);
				DAORegistry.getTrackerUserDAO().save(trackerUser);
								
				
				if(isSeller){
					
					if(sellerDtl!=null){
						trackerUser.setSellerId(sellerDtl.getSellerId());
						sellerDtl.setLogId(trackerUser.getId());
						EcommDAORegistry.getrTFSellerDetailsDAO().update(sellerDtl);
					}
				}
				
				
				addUserDTO.setMessage("User added successfully.");
				//returnMessage = "User added successfully.";
				
				//Tracking User Action
				String userActionMsg = "User Created Successfully.";
				Util.userActionAudit(request, trackerUser.getId(), userActionMsg);
				
				//Roles For Display Purpose - Newly Added
				for(String role : roles){
					if(role.toUpperCase().equals("ROLE_SUPER_ADMIN")){
						addUserDTO.setRoleSuperAdmin(role.toUpperCase());
					}
					if(role.toUpperCase().equals("ROLE_USER")){
						addUserDTO.setRoleUser(role.toUpperCase());
					}
					if(role.toUpperCase().equals("ROLE_CONTEST")){
						addUserDTO.setRoleContest(role.toUpperCase());
					}
					if(role.toUpperCase().equals("ROLE_SELLER")){
						addUserDTO.setRoleSeller(role.toUpperCase());
					}
				}
			}
			addUserDTO.setStatus(1);			
			addUserDTO.setTrackerUser(trackerUser);
			addUserDTO.setRoleAffiliateBroker(roleAffiliateBroker!=null?roleAffiliateBroker.toUpperCase():"");			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while Adding User");
			addUserDTO.setError(error);
			addUserDTO.setStatus(0);
		}		
		return addUserDTO;
	}
	
	@RequestMapping({"/EditUser"})
	public EditUsersDTO editUserPage(HttpServletRequest request, HttpServletResponse response, Model model){		
		EditUsersDTO editUsersDTO = new EditUsersDTO();
		Error error = new Error();
		model.addAttribute("editUsersDTO", editUsersDTO);
			
		try{
			Gson gson = GsonCustomConfig.getGsonBuilder();
			String returnMessage = "";
			String userIdStr = request.getParameter("userId");
			String action = request.getParameter("action");
			String sellerId = request.getParameter("sellerId");
			
			if(StringUtils.isEmpty(userIdStr)){
				System.err.println("Please select User.");
				error.setDescription("Please select User.");
				editUsersDTO.setError(error);
				editUsersDTO.setStatus(0);
				return editUsersDTO;
			}
			
			//Collection<Role> roleListDB = DAORegistry.getRoleDAO().getAll();
			Integer userId = Integer.parseInt(userIdStr);
			TrackerUser trackerUserDb = DAORegistry.getTrackerUserDAO().get(userId);
			if(trackerUserDb == null){
				System.err.println("Selected User not found.");
				error.setDescription("Selected User not found.");
				editUsersDTO.setError(error);
				editUsersDTO.setStatus(0);
				return editUsersDTO;				
			}
			
			String userName = trackerUserDb.getUserName();
			TrackerUser trackerUser = new TrackerUser();			
			
			if(action != null && action.equals("editUserInfo")){
				String roless = request.getParameter("role");
				String firstName = request.getParameter("firstName");
				String lastName = request.getParameter("lastName");
				String email = request.getParameter("email");
				String phone = request.getParameter("phone");
				
				/*String companyName = request.getParameter("companyName");
				String serviceFees = request.getParameter("serviceFees");*/								
				
				if(StringUtils.isEmpty(firstName)){
					System.err.println("Please provide First Name.");
					error.setDescription("Please provide First Name.");
					editUsersDTO.setError(error);
					editUsersDTO.setStatus(0);
					return editUsersDTO;
				}
				if(StringUtils.isEmpty(lastName)){
					System.err.println("Please provide Last Name.");
					error.setDescription("Please provide Last Name.");
					editUsersDTO.setError(error);
					editUsersDTO.setStatus(0);
					return editUsersDTO;
				}
				if(StringUtils.isEmpty(email)){
					System.err.println("Please provide Email.");
					error.setDescription("Please provide Email.");
					editUsersDTO.setError(error);
					editUsersDTO.setStatus(0);
					return editUsersDTO;
				}
				if(StringUtils.isEmpty(roless)){
					System.err.println("Please select Roles.");
					error.setDescription("Please select Roles.");
					editUsersDTO.setError(error);
					editUsersDTO.setStatus(0);
					return editUsersDTO;
				}
				
				RTFSellerDetails sellerDtl = null;
				if(sellerId!=null && !sellerId.isEmpty()){
					sellerDtl = EcommDAORegistry.getrTFSellerDetailsDAO().get(Integer.parseInt(sellerId));
					if(sellerDtl == null ){
						System.err.println("Seller not found with given identified.");
						error.setDescription("Seller not found with given identified.");
						editUsersDTO.setError(error);
						editUsersDTO.setStatus(0);
						return editUsersDTO;
					}
					if(sellerDtl.getLogId()!=null  && sellerDtl.getLogId() > 0){
						System.err.println("Selected seller is already attached with other tracker user.");
						error.setDescription("Selected seller is already attached with other tracker user.");
						editUsersDTO.setError(error);
						editUsersDTO.setStatus(0);
						return editUsersDTO;
					}
				}
				
				trackerUser.setId(trackerUserDb.getId());
				trackerUser.setUserName(trackerUserDb.getUserName());					
				trackerUser.setFirstName(firstName);
				trackerUser.setLastName(lastName);
				trackerUser.setEmail(email);
				trackerUser.setPhone(phone);
				trackerUser.setRoles(trackerUserDb.getRoles());
				trackerUser.setPassword(trackerUserDb.getPassword());
				trackerUser.setStatus(trackerUserDb.getStatus());	
				trackerUser.setCreateDate(trackerUserDb.getCreateDate());
				trackerUser.setCreatedBy(trackerUserDb.getCreatedBy());
				
				
				/*TrackerBrokers trackerBrokersDb = null;
				Boolean roleFlag = false;
				Boolean roleAffiliateFlag = false;
				AffiliatePromoCodeHistory affiliatePromoHistory = null;*/
				
				String roles[] = null;
				if(StringUtils.isNotEmpty(roless)){
					roles = gson.fromJson(roless, new TypeToken<String[]>(){}.getType());
				}
				
				Set<Role> roleList = new HashSet<Role>();
				Boolean isSeller = false;
				for(String role : roles){
					Role roleDb = DAORegistry.getRoleDAO().getRoleByName(role.toUpperCase());
					if(roleDb.getName().equalsIgnoreCase("ROLE_SELLER")){
						isSeller = true;
					}
					/*if(role.toUpperCase().equals("ROLE_BROKER")){
						if(trackerUserDb.getBroker() == null){
							trackerBrokersDb = new TrackerBrokers();
							trackerBrokersDb.setCompanyName(companyName);
							trackerBrokersDb.setServiceFees(Double.parseDouble(serviceFees));
							DAORegistry.getTrackerBrokersDAO().save(trackerBrokersDb);
							
							trackerUser.setBroker(trackerBrokersDb);
							//trackerUser.setBrokerId(trackerBrokersDb.getId());
						}
						if(trackerUserDb.getBroker() != null){
							trackerBrokersDb = DAORegistry.getTrackerBrokersDAO().get(trackerUserDb.getBroker().getId());
							if(trackerBrokersDb != null){
								trackerBrokersDb.setServiceFees(Double.parseDouble(serviceFees));
								DAORegistry.getTrackerBrokersDAO().update(trackerBrokersDb);
							}
							
							trackerUser.setBroker(trackerUserDb.getBroker());
						}
						roleFlag = true;
					}
					
					if(role.toUpperCase().equals("ROLE_AFFILIATES")){
						if(trackerUserDb.getPromotionalCode() == null || trackerUserDb.getPromotionalCode().isEmpty()){
							trackerUser.setPromotionalCode(null);
								//String promoCode = Util.generateCustomerReferalCode();
								///trackerUser.setPromotionalCode(promoCode);
								
								//affiliatePromoHistory = new AffiliatePromoCodeHistory();
								//affiliatePromoHistory.setUserId(trackerUser.getId());
								//affiliatePromoHistory.setPromoCode(promoCode);
								//affiliatePromoHistory.setCreateDate(new Date());
								//affiliatePromoHistory.setUpdateDate(new Date());
								//affiliatePromoHistory.setStatus("ACTIVE");
						}
						if(trackerUserDb.getPromotionalCode() != null){				
							trackerUser.setPromotionalCode(trackerUserDb.getPromotionalCode());								
						}
						roleAffiliateFlag = true;
					}*/
					roleList.add(roleDb);
				}
				trackerUser.setRoles(roleList);
				//Delete if Role_Broker is not available when edit.
				/*if(!roleFlag){
					if(trackerUserDb.getBroker() != null){
						DAORegistry.getTrackerBrokersDAO().delete(trackerUserDb.getBroker());							
					}
				}
				if(!roleAffiliateFlag){
					if(trackerUserDb.getPromotionalCode() != null){
						
						affiliatePromoHistory = DAORegistry.getAffiliatePromoCodeHistoryDAO().getActiveAffiliatePromoCode(trackerUser.getId());
						if(null != affiliatePromoHistory ){
							affiliatePromoHistory.setStatus("DELETED");
							affiliatePromoHistory.setUpdateDate(new Date());
						}
						trackerUser.setPromotionalCode(null);
					}
				}
				
				if(null != affiliatePromoHistory){
					DAORegistry.getAffiliatePromoCodeHistoryDAO().saveOrUpdate(affiliatePromoHistory);
				}*/
				
				DAORegistry.getTrackerUserDAO().update(trackerUser);
				
				if(isSeller){
						trackerUser.setSellerId(sellerDtl.getSellerId());
						sellerDtl.setLogId(trackerUser.getId());
						EcommDAORegistry.getrTFSellerDetailsDAO().update(sellerDtl);
				}else{
						sellerDtl = EcommDAORegistry.getrTFSellerDetailsDAO().findSellerByLoginId(trackerUser.getId());
						sellerDtl.setLogId(null);
						EcommDAORegistry.getrTFSellerDetailsDAO().update(sellerDtl);
				}
				
				
				trackerUserDb = DAORegistry.getTrackerUserDAO().get(userId);
				trackerUserDb.setSellerId(trackerUser.getSellerId());
				editUsersDTO.setMessage("User's details updated successfully.");
				
				//returnMessage = "User's details updated successfully.";				
				//return "redirect:EditUser?userId="+userIdStr;
				
				//Tracking User Action
				String userActionMsg = "User Detail(s) Updated.";
				com.rtw.tmat.utils.Util.userActionAudit(request, Integer.parseInt(userIdStr), userActionMsg);
				
			}else if(action != null && action.equals("changePassword")){
				String password = request.getParameter("password");
				String rePassword = request.getParameter("repassword");
				
				if(StringUtils.isEmpty(password)){
					System.err.println("Please provide Password.");
					error.setDescription("Please provide Password.");
					editUsersDTO.setError(error);
					editUsersDTO.setStatus(0);
					return editUsersDTO;
				}
				if(StringUtils.isEmpty(rePassword)){
					System.err.println("Please provide Re-Password.");
					error.setDescription("Please provide Re-Password..");
					editUsersDTO.setError(error);
					editUsersDTO.setStatus(0);
					return editUsersDTO;
				}
				if(!password.equals(rePassword)){
					System.err.println("Password and Re-Password Must match.");
					error.setDescription("Password and Re-Password Must match.");
					editUsersDTO.setError(error);
					editUsersDTO.setStatus(0);
					return editUsersDTO;
				}
				
				trackerUserDb.setAndEncryptPassword(password);
				DAORegistry.getTrackerUserDAO().saveOrUpdate(trackerUserDb);
				
				trackerUserDb = DAORegistry.getTrackerUserDAO().get(userId);
				editUsersDTO.setMessage("User's password changed successfully.");
				//returnMessage = "User's password changed successfully.";				
				//Tracking User Action
				String userActionMsg = "User Password Changed.";
				com.rtw.tmat.utils.Util.userActionAudit(request, Integer.parseInt(userIdStr), userActionMsg);
			}
			
			List<String> rolesList = new ArrayList<String>();
			if(trackerUserDb.getRoles() != null && !trackerUserDb.getRoles().isEmpty()){
				for(Role role : trackerUserDb.getRoles()){
					rolesList.add(role.getName());
				}
			}
			/*TrackerBrokers trackerBrokersDb = null;
			TrackerBrokerCustomDTO trackerBrokerCustomDTO = null;
			if(trackerUserDb.getBroker() != null){
				trackerBrokersDb = DAORegistry.getTrackerBrokersDAO().get(trackerUserDb.getBroker().getId());
				trackerBrokerCustomDTO = com.rtw.tracker.utils.Util.getTrackerBrokerObject(trackerBrokersDb);
			}*/			

			EditTrackerUserCustomDTO editTrackerUserCustomDTO = null;
			if(trackerUserDb != null){
				editTrackerUserCustomDTO = com.rtw.tracker.utils.Util.getEditUsersObject(trackerUserDb);
			}
			
			editUsersDTO.setStatus(1);			
			editUsersDTO.setRolesList(rolesList);
			//editUsersDTO.setTrackerBrokerCustomDTO(trackerBrokerCustomDTO);
			editUsersDTO.setEditTrackerUserCustomDTO(editTrackerUserCustomDTO);
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while edit/updating User Information");
			editUsersDTO.setError(error);
			editUsersDTO.setStatus(0);
		}
		return editUsersDTO;
	}
	
	/*@RequestMapping({"/EditUser"})
	public String editUserPage(@ModelAttribute TrackerUser trackerUser,HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		String userIdStr = request.getParameter("userId");
		String action = request.getParameter("action");
		System.out.println("action:: "+action);
		String returnMessage = "";
		try{
			if(userIdStr == null || userIdStr.isEmpty()){
				return "redirect:ManageUsers";
			}
			Integer userId = Integer.parseInt(userIdStr);
			//Collection<Role> roleListDB = DAORegistry.getRoleDAO().getAll();
			TrackerUser trackerUserDb = DAORegistry.getTrackerUserDAO().get(userId);
			String userName = trackerUserDb.getUserName();
			if(action != null && action.equals("editUserInfo")){
				String[] roles = request.getParameterValues("role");
				String companyName = request.getParameter("companyName");
				String serviceFees = request.getParameter("serviceFees");
				if(trackerUser.getFirstName() == null || trackerUser.getFirstName().isEmpty()){
					returnMessage= "Firstname can't be blank";
				}else if(trackerUser.getLastName() == null || trackerUser.getLastName().isEmpty()){
					returnMessage= "Lastname can't be blank";
				}else if(trackerUser.getEmail() == null || trackerUser.getEmail().isEmpty()){
					returnMessage= "Email can't be blank";
				}else if(!validateEMail(trackerUser.getEmail())){
					returnMessage = "Invalid Email.";
				}else if(roles != null && roles.length == 0){
					returnMessage = "Please choose at least one Role.";
				}
				for(String role : roles){
					if(role.toUpperCase().equals("ROLE_BROKER")){
						if(companyName == null || companyName.isEmpty()){
							returnMessage = "Company can't be blank.";
						}
						if(serviceFees == null || serviceFees.isEmpty()){
							returnMessage = "Service Fees can't be blank.";
						}
					}
				}
				if(returnMessage.isEmpty()){
					trackerUser.setUserName(trackerUserDb.getUserName());
					trackerUser.setId(trackerUserDb.getId());
					trackerUser.setRoles(trackerUserDb.getRoles());
					trackerUser.setPassword(trackerUserDb.getPassword());
					trackerUser.setStatus(trackerUserDb.getStatus());	
					
					TrackerBrokers trackerBrokersDb = null;
					Boolean roleFlag = false;
					Boolean roleAffiliateFlag = false;
					
					
					AffiliatePromoCodeHistory affiliatePromoHistory = null;
					
					Set<Role> roleList = new HashSet<Role>();
					for(String role : roles){
						Role roleDb = DAORegistry.getRoleDAO().getRoleByName(role.toUpperCase());
						
						if(role.toUpperCase().equals("ROLE_BROKER")){
							if(trackerUserDb.getBroker() == null){
								trackerBrokersDb = new TrackerBrokers();
								trackerBrokersDb.setCompanyName(companyName);
								trackerBrokersDb.setServiceFees(Double.parseDouble(serviceFees));
								DAORegistry.getTrackerBrokersDAO().save(trackerBrokersDb);
								
								trackerUser.setBroker(trackerBrokersDb);
								//trackerUser.setBrokerId(trackerBrokersDb.getId());
							}
							if(trackerUserDb.getBroker() != null){
								trackerBrokersDb = DAORegistry.getTrackerBrokersDAO().get(trackerUserDb.getBroker().getId());
								if(trackerBrokersDb != null){
									trackerBrokersDb.setServiceFees(Double.parseDouble(serviceFees));
									DAORegistry.getTrackerBrokersDAO().update(trackerBrokersDb);
								}
								
								trackerUser.setBroker(trackerUserDb.getBroker());
							}
							roleFlag = true;
						}
						
						if(role.toUpperCase().equals("ROLE_AFFILIATES")){
							if(trackerUserDb.getPromotionalCode() == null || trackerUserDb.getPromotionalCode().isEmpty()){
								trackerUser.setPromotionalCode(null);
									String promoCode = Util.generateCustomerReferalCode();
									trackerUser.setPromotionalCode(promoCode);
									
									affiliatePromoHistory = new AffiliatePromoCodeHistory();
									affiliatePromoHistory.setUserId(trackerUser.getId());
									affiliatePromoHistory.setPromoCode(promoCode);
									affiliatePromoHistory.setCreateDate(new Date());
									affiliatePromoHistory.setUpdateDate(new Date());
									affiliatePromoHistory.setStatus("ACTIVE");
							}
							if(trackerUserDb.getPromotionalCode() != null){				
								trackerUser.setPromotionalCode(trackerUserDb.getPromotionalCode());								
							}
							roleAffiliateFlag = true;
						}
						roleList.add(roleDb);
					}
					trackerUser.setRoles(roleList);
					
					//Delete if Role_Broker is not available when edit.
					if(!roleFlag){
						if(trackerUserDb.getBroker() != null){
							DAORegistry.getTrackerBrokersDAO().delete(trackerUserDb.getBroker());							
						}
					}
					if(!roleAffiliateFlag){
						if(trackerUserDb.getPromotionalCode() != null){
							
							affiliatePromoHistory = DAORegistry.getAffiliatePromoCodeHistoryDAO().getActiveAffiliatePromoCode(trackerUser.getId());
							if(null != affiliatePromoHistory ){
								affiliatePromoHistory.setStatus("DELETED");
								affiliatePromoHistory.setUpdateDate(new Date());
							}
							trackerUser.setPromotionalCode(null);
						}
					}
					DAORegistry.getTrackerUserDAO().update(trackerUser);
					
					if(null != affiliatePromoHistory){
						DAORegistry.getAffiliatePromoCodeHistoryDAO().saveOrUpdate(affiliatePromoHistory);
					}
					
					trackerUserDb = DAORegistry.getTrackerUserDAO().get(userId);
					map.put("successMessage", "User's details updated successfully.");
				}else{
					map.put("errorMessage", returnMessage);
				}
				//return "redirect:EditUser?userId="+userIdStr;
			}else if(action != null && action.equals("changePassword")){
				String password = request.getParameter("password");
				String rePassword = request.getParameter("repassword");
				if(password == null || password.isEmpty()){
					returnMessage= "Password can't be blank";
				}else if(rePassword == null || rePassword.isEmpty()){
					returnMessage = "Re-Password can't be blank";
				}else if(!password.equals(rePassword)){
					returnMessage = "Password and Re-Password must match.";
				}
				if(returnMessage.isEmpty()){
					trackerUserDb.setAndEncryptPassword(password);
					DAORegistry.getTrackerUserDAO().saveOrUpdate(trackerUserDb);
					trackerUserDb = DAORegistry.getTrackerUserDAO().get(userId);
					map.put("successMessage", "User's password changed successfully.");
				}else{
					map.put("errorMessage", returnMessage);
				}
				//return "redirect:EditUser?userId="+userIdStr;
			}else if(action != null && action.equals("logoutUser")){
				  for (Object principal : sessionRegistry.getAllPrincipals()) {
				    	if(principal.equals(userName)){
				    		//
				    	}
				  }
				  
				  List<Object> sessionInformations = sessionRegistry.getAllPrincipals();
					if (sessionInformations != null) {
						System.out.println("==="+sessionInformations.size());
						
						//System.out.println("yes");
						return null;
					} else {
						System.out.println("no");
						return null;
					}
					
					
				//Log out the user programatically
				Authentication auth = SecurityContextHolder.getContext().getAuthentication(); // concern you

				if (auth != null) {
					new SecurityContextLogoutHandler().logout(request,
							response, auth);
				}
				
				boolean wasLoggedIn = SessionListener.invalidateSessionsForUser(userName);
				if (wasLoggedIn) {
					map.put("successMessage", "User " + userName + " has been logged out.");
				} else {
					map.put("userActionsMessage", "User " + userName + " was not logged in.");				
				}
			}else{
				map.put("error", "Invalid action " + action);
			}
			List<String> rolesList = new ArrayList<String>();
			if(trackerUserDb.getRoles() != null && !trackerUserDb.getRoles().isEmpty()){
				for(Role role : trackerUserDb.getRoles()){
					rolesList.add(role.getName());
				}
			}
			TrackerBrokers trackerBrokersDb = null;
			if(trackerUserDb.getBroker() != null){
				trackerBrokersDb = DAORegistry.getTrackerBrokersDAO().get(trackerUserDb.getBroker().getId());
			}
			//map.put("roleListDb", roleListDB);
			map.put("trackerBroker", trackerBrokersDb);
			map.put("rolesList", rolesList);
			map.put("trackerUser", trackerUserDb);
			map.put("status", trackerUserDb.getStatus());
		}catch(Exception e){
			e.printStackTrace();
			map.put("error", "There is something wrong..Please Try Again.");
		}
		map.put("userId", userIdStr);
		return "page-admin-edit-user";
	}*/
	
	@RequestMapping({"/AuditUser"})
	public AuditUsersDTO auditUserPage(HttpServletRequest request, HttpServletResponse response){
		AuditUsersDTO auditUsersDTO = new AuditUsersDTO();
		Error error = new Error();
		
		try{
			String userIdStr = request.getParameter("userId");
			String action = request.getParameter("action");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
						
			if(StringUtils.isEmpty(userIdStr)){
				System.err.println("Please select User.");
				error.setDescription("Please select User.");
				auditUsersDTO.setError(error);
				auditUsersDTO.setStatus(0);
				return auditUsersDTO;
			}
			
			GridHeaderFilters filter = null;
			try{
				filter = GridHeaderFiltersUtil.getUserAuditFilter(headerFilter);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				auditUsersDTO.setError(error);
				auditUsersDTO.setStatus(0);
				return auditUsersDTO;
			}
			
			TrackerUser trackerUser = DAORegistry.getTrackerUserDAO().get(Integer.parseInt(userIdStr));
			if(trackerUser == null){
				System.err.println("User not found.");
				error.setDescription("User not found.");
				auditUsersDTO.setError(error);
				auditUsersDTO.setStatus(0);
				return auditUsersDTO;
			}
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH, -1);
			Date fromDate = cal.getTime();
			Date toDate = new Date();
			
			Collection<UserAction> userActionList = null;
			String fromDateStr = "";
			String toDateStr = "";
			Integer count = 0;
			
			if(action != null && action.equals("search")){
				fromDateStr = request.getParameter("fromDate");
				toDateStr = request.getParameter("toDate");
				
				if(StringUtils.isEmpty(fromDateStr)){
					System.err.println("Please provide From Date.");
					error.setDescription("Please provide From Date.");
					auditUsersDTO.setError(error);
					auditUsersDTO.setStatus(0);
					return auditUsersDTO;
				}
				if(StringUtils.isEmpty(toDateStr)){
					System.err.println("Please provide To Date.");
					error.setDescription("Please provide To Date.");
					auditUsersDTO.setError(error);
					auditUsersDTO.setStatus(0);
					return auditUsersDTO;
				}
				
			}else{
				fromDateStr = dateFormat.format(fromDate);
				toDateStr = dateFormat.format(toDate);
			}
			
			userActionList = DAORegistry.getUserActionDAO()
					.getAllActionsByUserIdAndDateRange(Integer.parseInt(userIdStr), fromDateStr+" 00:00:00", toDateStr+" 23:59:59", filter, pageNo);
			count = DAORegistry.getUserActionDAO().getAllActionsByUserIdAndDateRangeCount(Integer.parseInt(userIdStr), fromDateStr+" 00:00:00", toDateStr+" 23:59:59", filter);
			
			if(userActionList == null || userActionList.size() == 0){
				auditUsersDTO.setMessage("No User Action found for Audit");
			}
			
			auditUsersDTO.setStatus(1);
			auditUsersDTO.setUserActionDTOs(com.rtw.tracker.utils.Util.getUserActionsArray(userActionList));
			auditUsersDTO.setAuditPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
			auditUsersDTO.setUserEmail(trackerUser.getEmail());
			auditUsersDTO.setFromDate(fromDateStr);
			auditUsersDTO.setToDate(toDateStr);			
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching User Audit.");
			auditUsersDTO.setError(error);
			auditUsersDTO.setStatus(0);
		}
		return auditUsersDTO;
	}
	
	public static boolean validateEMail(String email){
		try{
			final String emailPattern = 
					"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
					+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
			Pattern pattern;
			Matcher matcher;
			
			pattern = Pattern.compile(emailPattern);
			matcher = pattern.matcher(email);
			return matcher.matches();
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Method to encrypt the password
	 * @param input
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public static String encryptPwd(String input) throws NoSuchAlgorithmException {
		MessageDigest mDigest = MessageDigest.getInstance("SHA1");
		byte[] result = mDigest.digest(input.getBytes());
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < result.length; i++) {
			sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}
	
	
	@RequestMapping("/AutoCompleteVenues")
	public AutoCompleteVenueDTO getAutoCompleteVenues(HttpServletRequest request, HttpServletResponse response)throws Exception {
		AutoCompleteVenueDTO autoCompleteVenueDTO = new AutoCompleteVenueDTO();
		Error error = new Error();
		
		try{
			String param = request.getParameter("q");			
			String strResponse = "";
			
			Collection<Venue> venues = DAORegistry.getVenueDAO().filterByName(param);
			
			if (venues == null) {
				System.err.println("There is no Venue found from your Search.");
				error.setDescription("There is no Venue found from your Search.");
				autoCompleteVenueDTO.setError(error);
				autoCompleteVenueDTO.setStatus(0);
				return autoCompleteVenueDTO;
			}
			if (venues != null) {
				for (Venue venue : venues) {
					strResponse += ("VENUE" + "|" + venue.getId() + "|" + venue.getBuilding()+"|"+venue.getCity()+", "+venue.getState()+", "+venue.getCountry()+ "\n") ;
				}
			}
						
			autoCompleteVenueDTO.setVenue(strResponse);
			autoCompleteVenueDTO.setStatus(1);
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Auto Complete Venue");
			autoCompleteVenueDTO.setError(error);
			autoCompleteVenueDTO.setStatus(0);
		}
		return autoCompleteVenueDTO;
	}
	
	
	@RequestMapping("/AutoCompleteArtist")
	public AutoCompleteArtistDTO getAutoCompleteArtist(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		AutoCompleteArtistDTO autoCompleteArtistDTO = new AutoCompleteArtistDTO();
		Error error = new Error();
		
		try{
			String param = request.getParameter("q");			
			String strResponse = "";
			
			Collection<Artist> artists = DAORegistry.getArtistDAO().filterByName(param);
			if (artists == null) {
				System.err.println("There is no Artist found from your Search.");
				error.setDescription("There is no Artist found from your Search.");
				autoCompleteArtistDTO.setError(error);
				autoCompleteArtistDTO.setStatus(0);
				return autoCompleteArtistDTO;
			}
			if (artists != null) {
				for (Artist artist : artists) {
					strResponse += ("ARTIST" + "|" + artist.getId() + "|" + artist.getName() + "\n") ;
				}
			}
			
			autoCompleteArtistDTO.setArtist(strResponse);
			autoCompleteArtistDTO.setStatus(1);
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Auto Complete Venue");
			autoCompleteArtistDTO.setError(error);
			autoCompleteArtistDTO.setStatus(0);
		}
		return autoCompleteArtistDTO;
	}
	
	
	@RequestMapping(value = "/CleanedClient")
	public EmailBlastCustomersDTO getClientReportPage(HttpServletRequest request, HttpServletResponse response){
		EmailBlastCustomersDTO emailBlastCustomersDTO = new EmailBlastCustomersDTO();
		Error error = new Error();
		
		try{
			String fromDateStr = request.getParameter("emailBlastFromDate");
			String toDateStr = request.getParameter("emailBlastToDate");			
			String cities = request.getParameter("cities");
			String state = request.getParameter("stateIds");
			String country = request.getParameter("countryIds");
			String customerType = request.getParameter("customerType");
			String venues = request.getParameter("venue");
			String productType = request.getParameter("products");
			String grandChildNames = request.getParameter("grandChildIds");
			String childIds = request.getParameter("childIds");
			String artists = request.getParameter("artists");
			String customerCat = request.getParameter("customerCategory");
			String isMailSent = request.getParameter("isMailSent");
			String sortingString = request.getParameter("sortingString");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			List<Country> countryList = null;
			Collection<Customers> rtfCustomerList = new ArrayList<Customers>();
			Collection<Customers> rtwCustomerList = new ArrayList<Customers>();
			List<GrandChildCategory> grandChilds = null;
			List<ChildCategory> childs = null;
			Collection<EmailTemplate> templateList = null;
			String fromDateFinal = null;
			String toDateFinal = null;
			Boolean isRTW = false;
			Boolean isRTW2 = false;
			Boolean isRTF = false;
			Boolean isTIXCITY = false;
			Integer count = 0;
			
			GridHeaderFilters filter = null;
			try{
				filter = GridHeaderFiltersUtil.getCustomerSearchHeaderFilters(headerFilter+sortingString);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				emailBlastCustomersDTO.setError(error);
				emailBlastCustomersDTO.setStatus(0);
				return emailBlastCustomersDTO;
			}
			
			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";
			}else{
				fromDateFinal = null;
				toDateFinal = null;
			}
			if(productType!=null && !productType.isEmpty()){
				if(productType.contains(",")){
					String arr[] = productType.split(",");
					for(String str : arr){
						if(str.equalsIgnoreCase("RTW")){
							isRTW=true;
						}
						if(str.equalsIgnoreCase("RTW2")){
							isRTW2=true;
						}
						if(str.equalsIgnoreCase("REWARDTHEFAN")){
							isRTF=true;
						}
						if(str.equalsIgnoreCase("TIXCITY")){
							isTIXCITY=true;
						}
					}
				}else{
					if(productType.equalsIgnoreCase("RTW")){
						isRTW=true;
					}
					if(productType.equalsIgnoreCase("RTW2")){
						isRTW2=true;
					}
					if(productType.equalsIgnoreCase("REWARDTHEFAN")){
						isRTF=true;
					}
					if(productType.equalsIgnoreCase("TIXCITY")){
						isTIXCITY=true;
					}
				}
			}else{
				isRTW2=true;
				isRTF=true;
				isRTW=true;
				isTIXCITY=true;
			}
			if(isRTF){
				rtfCustomerList = DAORegistry.getQueryManagerDAO().getClientReportData(isMailSent,customerCat,customerType,cities,state,country,venues,productType,grandChildNames,childIds,artists,filter,pageNo,true,fromDateFinal,toDateFinal);
				count += DAORegistry.getQueryManagerDAO().getClientReportDataCount(isMailSent,customerCat,customerType,cities,state,country,venues,productType,grandChildNames,childIds,artists,filter,fromDateFinal,toDateFinal);
			}
			
			String products = "";
			if(isTIXCITY && isRTW && isRTW2){
				products="'TIXCITY','RTW','RTW2'";
			}else if(isTIXCITY && isRTW){
				products="'TIXCITY','RTW'";
			}else if(isTIXCITY && isRTW2){
				products="'TIXCITY','RTW2'";
			}else if(isRTW && isRTW2){
				products="'RTW','RTW2'";
			}else if(isTIXCITY){
				products="'TIXCITY'";
			}else if(isRTW){
				products="'RTW'";
			}else if(isRTW2){
				products="'RTW2'";
			}
			if(products != null && !products.isEmpty()){
				rtwCustomerList = DAORegistry.getQueryManagerDAO().getClientReportDataRTWRTW2(isMailSent,customerCat,customerType,cities,state,country,venues,products,grandChildNames,childIds,artists,filter,pageNo,true,fromDateFinal,toDateFinal);
				count += DAORegistry.getQueryManagerDAO().getClientReportDataCountRTWRTW2(isMailSent,customerCat,customerType,cities,state,country,venues,products,grandChildNames,childIds,artists,filter,fromDateFinal,toDateFinal);
			}
			
			rtfCustomerList.addAll(rtwCustomerList);
						
			countryList = DAORegistry.getCountryDAO().GetUSCANADA();
			childs = DAORegistry.getChildCategoryDAO().getAll();
			grandChilds = DAORegistry.getGrandChildCategoryDAO().getAll();
			templateList = DAORegistry.getEmailTemplateDAO().getAll();
			
			emailBlastCustomersDTO.setStatus(1);
			emailBlastCustomersDTO.setCustomersDTO(com.rtw.tracker.utils.Util.getCustomerArray(rtfCustomerList));
			emailBlastCustomersDTO.setCustomersPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
			emailBlastCustomersDTO.setCountryDTO(com.rtw.tracker.utils.Util.getCountryArray(countryList));
			emailBlastCustomersDTO.setChildDTO(com.rtw.tracker.utils.Util.getChildCategoryArray(childs));
			emailBlastCustomersDTO.setGrandChildDTO(com.rtw.tracker.utils.Util.getGrandChildCategoryArray(grandChilds));
			emailBlastCustomersDTO.setTemplateDTO(com.rtw.tracker.utils.Util.getEmailTemplateArray(templateList));
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Email Blast Customers");
			emailBlastCustomersDTO.setError(error);
			emailBlastCustomersDTO.setStatus(0);
		}
		return emailBlastCustomersDTO;
	}
		
	
	/*@RequestMapping(value = "/GetCleanedClient")
	public EmailBlastCustomersDTO getClientReportData(HttpServletRequest request, HttpServletResponse response){
		EmailBlastCustomersDTO emailBlastCustomersDTO = new EmailBlastCustomersDTO();
		Error error = new Error();
		
		try{			
			String cities = request.getParameter("cities");
			String state = request.getParameter("stateIds");
			String country = request.getParameter("countryIds");
			String customerType = request.getParameter("customerType");
			String venues = request.getParameter("venue");
			String productType = request.getParameter("products");
			String grandChildNames = request.getParameter("grandChildIds");
			String artists = request.getParameter("artists");
			String childIds = request.getParameter("childIds");
			String customerCat = request.getParameter("customerCategory");
			String isMailSent = request.getParameter("isMailSent");
			String fromDateStr = request.getParameter("emailBlastFromDate");
			String toDateStr = request.getParameter("emailBlastToDate");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
					
			Collection<Customers> rtwCustomerList = new ArrayList<Customers>();
			Collection<Customers> rtfCustomerList = new ArrayList<Customers>();
			Boolean isRTW = false;
			Boolean isRTW2 = false;
			Boolean isRTF = false;
			Boolean isTIXCITY = false;
			String fromDateFinal = "";
			String toDateFinal = "";
			Integer count = 0;
			
			if(StringUtils.isEmpty(pageNo)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				emailBlastCustomersDTO.setError(error);
				emailBlastCustomersDTO.setStatus(0);
				return emailBlastCustomersDTO;
			}
			try {
				Integer.parseInt(pageNo);
			} catch (Exception e) {
				System.err.println("Please provide valid PageNo.");
				error.setDescription("Please provide valid PageNo.");
				emailBlastCustomersDTO.setError(error);
				emailBlastCustomersDTO.setStatus(0);
				return emailBlastCustomersDTO;
			}
			
			GridHeaderFilters filter = null;
			try{
				filter = GridHeaderFiltersUtil.getCustomerSearchHeaderFilters(headerFilter);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				emailBlastCustomersDTO.setError(error);
				emailBlastCustomersDTO.setStatus(0);
				return emailBlastCustomersDTO;
			}
			
			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";
			}else{
				fromDateFinal = null;
				toDateFinal = null;
			}
			if(productType!=null && !productType.isEmpty()){
				if(productType.contains(",")){
					String arr[] = productType.split(",");
					for(String str : arr){
						if(str.equalsIgnoreCase("RTW")){
							isRTW=true;
						}
						if(str.equalsIgnoreCase("RTW2")){
							isRTW2=true;
						}
						if(str.equalsIgnoreCase("REWARDTHEFAN")){
							isRTF=true;
						}
						if(str.equalsIgnoreCase("TIXCITY")){
							isTIXCITY=true;
						}
					}
				}else{
					if(productType.equalsIgnoreCase("RTW")){
						isRTW=true;
					}
					if(productType.equalsIgnoreCase("RTW2")){
						isRTW2=true;
					}
					if(productType.equalsIgnoreCase("REWARDTHEFAN")){
						isRTF=true;
					}
					if(productType.equalsIgnoreCase("TIXCITY")){
						isTIXCITY=true;
					}
				}
			}else{
				isRTW2=true;
				isRTF=true;
				isRTW=true;
				isTIXCITY=true;
			}
			if(isRTF){
				rtfCustomerList = DAORegistry.getQueryManagerDAO().getClientReportData(isMailSent,customerCat,customerType,cities,state,country,venues,productType,grandChildNames,childIds,artists,filter,pageNo,true,fromDateFinal,toDateFinal);
				count += DAORegistry.getQueryManagerDAO().getClientReportDataCount(isMailSent,customerCat,customerType,cities,state,country,venues,productType,grandChildNames,childIds,artists,filter,fromDateFinal,toDateFinal);
			}
			String products = "";
			if(isTIXCITY && isRTW && isRTW2){
				products="'TIXCITY','RTW','RTW2'";
			}else if(isTIXCITY && isRTW){
				products="'TIXCITY','RTW'";
			}else if(isTIXCITY && isRTW2){
				products="'TIXCITY','RTW2'";
			}else if(isRTW && isRTW2){
				products="'RTW','RTW2'";
			}else if(isTIXCITY){
				products="'TIXCITY'";
			}else if(isRTW){
				products="'RTW'";
			}else if(isRTW2){
				products="'RTW2'";
			}
			if(products != null && !products.isEmpty()){
				rtwCustomerList = DAORegistry.getQueryManagerDAO().getClientReportDataRTWRTW2(isMailSent,customerCat,customerType,cities,state,country,venues,products,grandChildNames,childIds,artists,filter,pageNo,true,fromDateFinal,toDateFinal);
				count += DAORegistry.getQueryManagerDAO().getClientReportDataCountRTWRTW2(isMailSent,customerCat,customerType,cities,state,country,venues,products,grandChildNames,childIds,artists,filter,fromDateFinal,toDateFinal);
			}
			
			rtfCustomerList.addAll(rtwCustomerList);
			
			emailBlastCustomersDTO.setStatus(1);
			emailBlastCustomersDTO.setCustomersDTO(com.rtw.tracker.utils.Util.getCustomerArray(rtfCustomerList));
			emailBlastCustomersDTO.setCustomersPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while Applying Search - Email Blast Customers");
			emailBlastCustomersDTO.setError(error);
			emailBlastCustomersDTO.setStatus(0);
		}
		return emailBlastCustomersDTO;
	}*/
	
	@RequestMapping(value = "/ExportClientReport")
	public void eportClientReporttoExcel(HttpServletRequest request, HttpServletResponse response){
		
		try{
			Integer count =0;
			String cities = request.getParameter("cities");
			String state = request.getParameter("stateIds");
			String customerType = request.getParameter("customerType");
			String country = request.getParameter("countryIds");
			String venues= request.getParameter("venue");
			String productType = request.getParameter("products");
			String grandChildNames = request.getParameter("grandChildIds");
			String pageNo = request.getParameter("pageNo");
			String artists = request.getParameter("artists");
			String headerFilter = request.getParameter("headerFilter");
			String childIds = request.getParameter("childIds");
			String customerCat = request.getParameter("customerCategory");
			String isMailSent = request.getParameter("isMailSent");
			String fromDateStr = request.getParameter("emailBlastFromDate");
			String toDateStr = request.getParameter("emailBlastToDate");
			
			String fromDateFinal = "";
			String toDateFinal = "";
			Boolean isRTW=false;
			Boolean isRTW2=false;
			Boolean isRTF=false;
			Boolean isTIXCITY=false;
			Collection<Customers> rtwCustomerList = new ArrayList<Customers>();
			Collection<Customers> rtfCustomerList = new ArrayList<Customers>();
			GridHeaderFilters filter = GridHeaderFiltersUtil.getCustomerSearchHeaderFilters(headerFilter);
			
			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";
			}else{
				fromDateFinal = null;
				toDateFinal = null;
			}
			if(productType!=null && !productType.isEmpty()){
				if(productType.contains(",")){
					String arr[] = productType.split(",");
					for(String str : arr){
						if(str.equalsIgnoreCase("RTW")){
							isRTW=true;
						}
						if(str.equalsIgnoreCase("RTW2")){
							isRTW2=true;
						}
						if(str.equalsIgnoreCase("REWARDTHEFAN")){
							isRTF=true;
						}
						if(str.equalsIgnoreCase("TIXCITY")){
							isTIXCITY=true;
						}
					}
				}else{
					if(productType.equalsIgnoreCase("RTW")){
						isRTW=true;
					}
					if(productType.equalsIgnoreCase("RTW2")){
						isRTW2=true;
					}
					if(productType.equalsIgnoreCase("REWARDTHEFAN")){
						isRTF=true;
					}
					if(productType.equalsIgnoreCase("TIXCITY")){
						isTIXCITY=true;
					}
				}
			}else{
				isRTW2=true;
				isRTF=true;
				isRTW=true;
				isTIXCITY=true;
			}
			if(isRTF){
				rtfCustomerList = DAORegistry.getQueryManagerDAO().getClientReportData(isMailSent,customerCat,customerType,cities,state,country,venues,productType,grandChildNames,childIds,artists,filter,pageNo,false,fromDateFinal,toDateFinal);
				//count += DAORegistry.getQueryManagerDAO().getClientReportDataCount(city,state,country,venueId,productType,grandChildIds,filter);
			}
			if(isTIXCITY && isRTW && isRTW2){
				rtwCustomerList = DAORegistry.getQueryManagerDAO().getClientReportDataRTWRTW2(isMailSent,customerCat,customerType,cities,state,country,venues,"'TIXCITY','RTW','RTW2'",grandChildNames,childIds,artists,filter,pageNo,false,fromDateFinal,toDateFinal);
				//count += DAORegistry.getQueryManagerDAO().getClientReportDataCountRTWRTW2(customerType,cities,state,country,venues,"'TIXCITY','RTW','RTW2'",grandChildNames,childIds,artists,filter);
			}else if(isTIXCITY && isRTW){
				rtwCustomerList = DAORegistry.getQueryManagerDAO().getClientReportDataRTWRTW2(isMailSent,customerCat,customerType,cities,state,country,venues,"'TIXCITY','RTW'",grandChildNames,childIds,artists,filter,pageNo,false,fromDateFinal,toDateFinal);
				//count += DAORegistry.getQueryManagerDAO().getClientReportDataCountRTWRTW2(customerType,cities,state,country,venues,"'TIXCITY','RTW'",grandChildNames,childIds,artists,filter);
			}else if(isTIXCITY && isRTW2){
				rtwCustomerList = DAORegistry.getQueryManagerDAO().getClientReportDataRTWRTW2(isMailSent,customerCat,customerType,cities,state,country,venues,"'TIXCITY','RTW2'",grandChildNames,childIds,artists,filter,pageNo,false,fromDateFinal,toDateFinal);
				//count += DAORegistry.getQueryManagerDAO().getClientReportDataCountRTWRTW2(customerType,cities,state,country,venues,"'TIXCITY','RTW2'",grandChildNames,childIds,artists,filter);
			}else if(isRTW && isRTW2){
				rtwCustomerList = DAORegistry.getQueryManagerDAO().getClientReportDataRTWRTW2(isMailSent,customerCat,customerType,cities,state,country,venues,"'RTW','RTW2'",grandChildNames,childIds,artists,filter,pageNo,false,fromDateFinal,toDateFinal);
				//count += DAORegistry.getQueryManagerDAO().getClientReportDataCountRTWRTW2(customerType,cities,state,country,venues,"'RTW','RTW2'",grandChildNames,childIds,artists,filter);
			}else if(isTIXCITY){
				rtwCustomerList = DAORegistry.getQueryManagerDAO().getClientReportDataRTWRTW2(isMailSent,customerCat,customerType,cities,state,country,venues,"'TIXCITY'",grandChildNames,childIds,artists,filter,pageNo,false,fromDateFinal,toDateFinal);
				//count += DAORegistry.getQueryManagerDAO().getClientReportDataCountRTWRTW2(customerType,cities,state,country,venues,"'TIXCITY'",grandChildNames,childIds,artists,filter);
			}else if(isRTW){
				rtwCustomerList = DAORegistry.getQueryManagerDAO().getClientReportDataRTWRTW2(isMailSent,customerCat,customerType,cities,state,country,venues,"'RTW'",grandChildNames,childIds,artists,filter,pageNo,false,fromDateFinal,toDateFinal);
				//count += DAORegistry.getQueryManagerDAO().getClientReportDataCountRTWRTW2(customerType,cities,state,country,venues,"'RTW'",grandChildNames,childIds,artists,filter);
			}else if(isRTW2){
				rtwCustomerList = DAORegistry.getQueryManagerDAO().getClientReportDataRTWRTW2(isMailSent,customerCat,customerType,cities,state,country,venues,"'RTW2'",grandChildNames,childIds,artists,filter,pageNo,false,fromDateFinal,toDateFinal);
				//count += DAORegistry.getQueryManagerDAO().getClientReportDataCountRTWRTW2(customerType,cities,state,country,venues,"'RTW2'",grandChildNames,childIds,artists,filter);
			}
			
			rtfCustomerList.addAll(rtwCustomerList);
			Map<String, Customers> map  = new HashMap<String, Customers>();
			for(Customers cust : rtfCustomerList){
				map.put(cust.getCustomerEmail().toUpperCase().trim(), cust);
			}
			
			if(rtfCustomerList!=null && !rtfCustomerList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet clientSheet = (SXSSFSheet) workbook.createSheet("Clients");
				Row clientHeader = clientSheet.createRow((int)0);
				clientHeader.createCell(0).setCellValue("Client ID");
				clientHeader.createCell(1).setCellValue("Email");
				clientHeader.createCell(2).setCellValue("First Name");
				clientHeader.createCell(3).setCellValue("Last Name");
				clientHeader.createCell(4).setCellValue("companyName");
				clientHeader.createCell(5).setCellValue("Product Type");
				clientHeader.createCell(6).setCellValue("Street1");
				clientHeader.createCell(7).setCellValue("Street2");
				clientHeader.createCell(8).setCellValue("City");
				clientHeader.createCell(9).setCellValue("State");
				clientHeader.createCell(10).setCellValue("Country");
				clientHeader.createCell(11).setCellValue("Zipcode");
				clientHeader.createCell(12).setCellValue("Type");
				
				SXSSFSheet brokerSheet =(SXSSFSheet) workbook.createSheet("Brokers");
				Row brokerHeader = brokerSheet.createRow((int)0);
				brokerHeader.createCell(0).setCellValue("Broker ID");
				brokerHeader.createCell(1).setCellValue("Email");
				brokerHeader.createCell(2).setCellValue("First Name");
				brokerHeader.createCell(3).setCellValue("Last Name");
				brokerHeader.createCell(4).setCellValue("companyName");
				brokerHeader.createCell(5).setCellValue("Product Type");
				brokerHeader.createCell(6).setCellValue("Street1");
				brokerHeader.createCell(7).setCellValue("Street2");
				brokerHeader.createCell(8).setCellValue("City");
				brokerHeader.createCell(9).setCellValue("State");
				brokerHeader.createCell(10).setCellValue("Country");
				brokerHeader.createCell(11).setCellValue("Zipcode");
				brokerHeader.createCell(12).setCellValue("Type");
				
				int i=1;
				int j=1;
				for(Customers cust : map.values()){
					if(cust.getIsClient()==true){
						Row row = clientSheet.createRow(i);
						row.createCell(0).setCellValue(cust.getCustomerId());
						row.createCell(1).setCellValue(cust.getCustomerEmail());
						row.createCell(2).setCellValue(cust.getCustomerName());
						row.createCell(3).setCellValue(cust.getLastName());
						row.createCell(4).setCellValue(cust.getCompanyName());
						row.createCell(5).setCellValue(cust.getProductType());
						row.createCell(6).setCellValue(cust.getAddressLine1());
						row.createCell(7).setCellValue(cust.getAddressLine2());
						row.createCell(8).setCellValue(cust.getCity());
						row.createCell(9).setCellValue(cust.getState());
						row.createCell(10).setCellValue(cust.getCountry());
						row.createCell(11).setCellValue(cust.getZipCode());
						row.createCell(12).setCellValue("Client");
						i++;
					}else if(cust.getIsBroker() == true){
						Row row = brokerSheet.createRow(j);
						row.createCell(0).setCellValue(cust.getCustomerId());
						row.createCell(1).setCellValue(cust.getCustomerEmail());
						row.createCell(2).setCellValue(cust.getCustomerName());
						row.createCell(3).setCellValue(cust.getLastName());
						row.createCell(4).setCellValue(cust.getCompanyName());
						row.createCell(5).setCellValue(cust.getProductType());
						row.createCell(6).setCellValue(cust.getAddressLine1());
						row.createCell(7).setCellValue(cust.getAddressLine2());
						row.createCell(8).setCellValue(cust.getCity());
						row.createCell(9).setCellValue(cust.getState());
						row.createCell(10).setCellValue(cust.getCountry());
						row.createCell(11).setCellValue(cust.getZipCode());
						row.createCell(12).setCellValue("Broker");
						j++;
					}
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Clients.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Manage purchase order
	 * @param request
	 * @param response
	 * @param map
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/PaypalTracking")
	public String getPaypalTrackingPage(HttpServletRequest request, HttpServletResponse response, Model model){
		PaypalTrackingDTO paypalTrackingDTO = new PaypalTrackingDTO();
		Error error = new Error();
		model.addAttribute("paypalTrackingDTO", paypalTrackingDTO);
		
		try {
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter"); 
			String sortingString = request.getParameter("sortingString");
			Integer count = 0;
			Collection<PayPalTracking> trackingList = null;
			
			GridHeaderFilters filter = GridHeaderFiltersUtil.getPaypalSearchHeaderFilters(headerFilter+sortingString);
			if(pageNo == null || pageNo.isEmpty()){
				trackingList = DAORegistry.getQueryManagerDAO().getPayPalTracking(filter, null);
			}else{
				trackingList = DAORegistry.getQueryManagerDAO().getPayPalTracking(filter, pageNo);
			}
			count = DAORegistry.getQueryManagerDAO().getPayPalTrackingCount(filter);		
			
			if(trackingList == null || trackingList.isEmpty()){
				paypalTrackingDTO.setMessage("No Transaction found for selected search filter");
			}
			paypalTrackingDTO.setStatus(1);			
			paypalTrackingDTO.setTrackingList(trackingList);
			paypalTrackingDTO.setPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Paypal Transaction");
			paypalTrackingDTO.setError(error);
			paypalTrackingDTO.setStatus(0);
		}
		return "page-paypal-tracking";
	}
	
	/**
	 * Manage purchase order
	 * @param request
	 * @param response
	 * @param map
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/getPayPalTracking", method = RequestMethod.POST)
	public void getPaypalTracking(HttpServletRequest request, HttpServletResponse response){
		JSONObject returnObject = new JSONObject();
		Integer count = 0;
		String pageNo = request.getParameter("pageNo");
		String orderIdStr = request.getParameter("orderId");
		String customerName = request.getParameter("customerName");
		String transactionStatus = request.getParameter("transactionStatus");
		String headerFilter = request.getParameter("headerFilter"); 
		Collection<PayPalTracking> trackingList=null;
		Integer orderId = 0;
		try {
			if(orderIdStr!=null && !orderIdStr.isEmpty()){
				orderId = Integer.parseInt(orderIdStr);
			}
			GridHeaderFilters filter = GridHeaderFiltersUtil.getPaypalSearchHeaderFilters(headerFilter);
			trackingList = DAORegistry.getQueryManagerDAO().getPayPalTracking(filter, pageNo);
			count = DAORegistry.getQueryManagerDAO().getPayPalTrackingCount(filter);			
			
			returnObject.put("trackingList", JsonWrapperUtil.getPaypalTrackingArray(trackingList));
			returnObject.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));
			IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	
	@RequestMapping(value = "/UserStatus")
	public GenericResponseDTO updateUserStatus(HttpServletRequest request, HttpServletResponse response, HttpSession session,Model model){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		model.addAttribute("genericResponseDTO", genericResponseDTO);
		
		try{			
			String userIdStr = request.getParameter("userId");
			String sessionUserId = request.getParameter("sessionUserId");
			String status = request.getParameter("status");
			
			if(StringUtils.isEmpty(userIdStr)){
				System.err.println("Please select User.");
				error.setDescription("Please select User.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(sessionUserId)){
				System.err.println("Please Login.");
				error.setDescription("Please Login.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(status)){
				System.err.println("Please provide Status.");
				error.setDescription("Please provide Status.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			TrackerUser trackerUser = DAORegistry.getTrackerUserDAO().get(Integer.parseInt(userIdStr));
			//String msg = "";
			if(trackerUser == null){
				System.err.println("Selected User not found.");
				error.setDescription("Selected User not found.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;				
			}
			if(sessionUserId.equals(trackerUser.getId())){
				System.err.println("You can't update your own account.");
				error.setDescription("You can't update your own account.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			if(status != null && status.equalsIgnoreCase("ACTIVE")){
				trackerUser.setStatus(true);
			}else{
				trackerUser.setStatus(false);
			}
			DAORegistry.getTrackerUserDAO().update(trackerUser);
			
			/*if(StringUtils.isEmpty(msg)){
			Set<Role> list = trackerUser.getRoles();
			boolean isAffiliate = false;
			for (Role role : list) {
				if(role.getName().equalsIgnoreCase("ROLE_AFFILIATES")){
					isAffiliate = true;
					break;
				}
			}
			
			AffiliatePromoCodeHistory affiliatePromoHistory = null;
			
			if(status != null && status.equalsIgnoreCase("ACTIVE")){
				if(isAffiliate && (trackerUser.getPromotionalCode() == null || trackerUser.getPromotionalCode().isEmpty())){
					String fromDateStr = request.getParameter("fromDate");
					String toDateStr = request.getParameter("toDate");
					fromDateStr = fromDateStr+" 00:00:00";
					toDateStr = toDateStr+" 23:59:59";
					SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
					Date fromDate = dateFormat.parse(fromDateStr);
					Date toDate = dateFormat.parse(toDateStr);
					
					String promoCode = Util.generateCustomerReferalCode();
					affiliatePromoHistory = new AffiliatePromoCodeHistory();
					affiliatePromoHistory.setUserId(trackerUser.getId());
					affiliatePromoHistory.setPromoCode(promoCode);
					affiliatePromoHistory.setCreateDate(new Date());
					affiliatePromoHistory.setUpdateDate(new Date());
					affiliatePromoHistory.setStatus("ACTIVE");
					affiliatePromoHistory.setEffectiveFromDate(fromDate);
					affiliatePromoHistory.setEffectiveToDate(toDate);
					
					trackerUser.setPromotionalCode(promoCode);
				}
				trackerUser.setStatus(true);
			}else{
				if(isAffiliate){
					//affiliatePromoHistory = DAORegistry.getAffiliatePromoCodeHistoryDAO().getActiveAffiliatePromoCode(trackerUser.getId());
					//if(null != affiliatePromoHistory ){
						//affiliatePromoHistory.setStatus("DELETED");
						//affiliatePromoHistory.setUpdateDate(new Date());
					//}
					DAORegistry.getAffiliatePromoCodeHistoryDAO().updateAllActiveHistoryByUserId(trackerUser.getId());
					trackerUser.setPromotionalCode(null);
				}
				trackerUser.setStatus(false);
			}	
			
			DAORegistry.getTrackerUserDAO().update(trackerUser);
			
			if(null != affiliatePromoHistory){
				DAORegistry.getAffiliatePromoCodeHistoryDAO().saveOrUpdate(affiliatePromoHistory);
			}
				msg ="true";
			}*/
						
			genericResponseDTO.setMessage("true");
			genericResponseDTO.setStatus(1);
			
			//Tracking User Action
			String userActionMsg = "User Status is Updated to "+status;
			Util.userActionAudit(request, Integer.parseInt(userIdStr), userActionMsg);
			
		}catch(Exception e){			
			e.printStackTrace();
			error.setDescription("Something went wrong while Updating User Status.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);			
		}
		return genericResponseDTO;
	}
	

	@RequestMapping({"/PayPalTrackingExportToExcel"})
	public void paypalTrackingToExport(HttpServletRequest request, HttpServletResponse response){
		
		String headerFilter = request.getParameter("headerFilter");
		Collection<PayPalTracking> paypalTrackingList=null;
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getPaypalSearchHeaderFilters(headerFilter);
			paypalTrackingList = DAORegistry.getQueryManagerDAO().getPayPalTrackingToExport(filter);
			
			if(paypalTrackingList!=null && !paypalTrackingList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("PayPal_Tracking");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Paypal Tracking Id");
				header.createCell(1).setCellValue("Customer Id");
				header.createCell(2).setCellValue("First Name");
				header.createCell(3).setCellValue("Last Name");
				header.createCell(4).setCellValue("Email");
				header.createCell(5).setCellValue("Phone");
				header.createCell(6).setCellValue("Order Id");
				header.createCell(7).setCellValue("Order Total");
				header.createCell(8).setCellValue("Order Type");
				header.createCell(9).setCellValue("Event Id");
				header.createCell(10).setCellValue("Quantity");
				header.createCell(11).setCellValue("Zone");
				header.createCell(12).setCellValue("Paypal Transaction Id");
				header.createCell(13).setCellValue("Transaction Id");
				header.createCell(14).setCellValue("Payment Id");
				header.createCell(15).setCellValue("Platform");
				header.createCell(16).setCellValue("Status");
				header.createCell(17).setCellValue("Created Date");
				header.createCell(18).setCellValue("Last Updated");
				header.createCell(19).setCellValue("Transaction Date");
				Integer i=1;
				for(PayPalTracking paypalTracking : paypalTrackingList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(paypalTracking.getId());
					row.createCell(1).setCellValue(paypalTracking.getCustomerId()!=null?paypalTracking.getCustomerId().toString():"");
					row.createCell(2).setCellValue(paypalTracking.getPayerFirstName()!=null?paypalTracking.getPayerFirstName():"");
					row.createCell(3).setCellValue(paypalTracking.getPayerLastName()!=null?paypalTracking.getPayerLastName():"");
					row.createCell(4).setCellValue(paypalTracking.getPayerEmail()!=null?paypalTracking.getPayerEmail():"");
					row.createCell(5).setCellValue(paypalTracking.getPayerPhone()!=null?paypalTracking.getPayerPhone():"");
					row.createCell(6).setCellValue(paypalTracking.getOrderId()!=null?paypalTracking.getOrderId().toString():"");
					row.createCell(7).setCellValue(paypalTracking.getOrderTotal()!=null?paypalTracking.getOrderTotal().toString():"");
					row.createCell(8).setCellValue(paypalTracking.getOrderType()!=null?paypalTracking.getOrderType():"");
					row.createCell(9).setCellValue(paypalTracking.getEventId()!=null?paypalTracking.getEventId().toString():"");
					row.createCell(10).setCellValue(paypalTracking.getQuantity()!=null?paypalTracking.getQuantity():0);
					row.createCell(11).setCellValue(paypalTracking.getZone()!=null?paypalTracking.getZone():"");
					row.createCell(12).setCellValue(paypalTracking.getPaypalTransactionId()!=null?paypalTracking.getPaypalTransactionId():"");
					row.createCell(13).setCellValue(paypalTracking.getTransactionId()!=null?paypalTracking.getTransactionId().toString():"");
					row.createCell(14).setCellValue(paypalTracking.getPaymentId()!=null?paypalTracking.getPaymentId():"");
					row.createCell(15).setCellValue(paypalTracking.getPlatform()!=null?paypalTracking.getPlatform():"");
					row.createCell(16).setCellValue(paypalTracking.getStatus()!=null?paypalTracking.getStatus():"");
					row.createCell(17).setCellValue(paypalTracking.getCreatedDateStr()!=null?paypalTracking.getCreatedDateStr():"");
					row.createCell(18).setCellValue(paypalTracking.getLastUpdatedStr()!=null?paypalTracking.getLastUpdatedStr():"");
					row.createCell(19).setCellValue(paypalTracking.getTransactionDateStr()!=null?paypalTracking.getTransactionDateStr():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=PayPal_Tracking.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}	
	

	@RequestMapping("/SendMailToCustomers")
	public GenericResponseDTO sendMailToCustomers(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		
		try{
			String mailTo = request.getParameter("mailTo");
			String mailCc = request.getParameter("mailCc");
			String mailBcc = request.getParameter("mailBcc");
			String mailSubject = request.getParameter("mailSubject");
			String templateBody = request.getParameter("txtDefaultHtmlArea");
			String templateName = "mail-rewardfan-invite-retail-customer.html";
			String templateIdStr = request.getParameter("templateName");
			String from =  request.getParameter("from");
			String userName = request.getParameter("userName");
			String path = request.getParameter("path");
			Customer customer = null;
			EmailTemplate emailTemplate = null;
			
			if(StringUtils.isEmpty(mailTo)){
				System.err.println("Not able to identify Mail To.");
				error.setDescription("Not able to identify Mail To.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(from)){
				System.err.println("Not able to identify From Address.");
				error.setDescription("Not able to identify From Address.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(mailSubject)){
				System.err.println("Not able to identify Mail Subject.");
				error.setDescription("Not able to identify Mail Subject.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(templateBody)){
				System.err.println("Not able to identify Mail Body.");
				error.setDescription("Not able to identify Mail Body.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(templateName)){
				System.err.println("Not able to identify template, Please select template.");
				error.setDescription("Not able to identify template, Please select template.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(templateIdStr)){
				System.err.println("Not able to identify template, Please select template.");
				error.setDescription("Not able to identify template, Please select template.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			emailTemplate = DAORegistry.getEmailTemplateDAO().get(Integer.parseInt(templateIdStr));
			if(emailTemplate == null){				
				error.setDescription("Not able to identify template in DB, Please select template.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			templateBody = "<html><head></head><body>"+templateBody+"</body></html>";
			templateBody = templateBody.replaceAll("../resources/images/","cid:");
			templateBody = templateBody.replaceAll("https://ticketfulfillment.com/resources/images/","cid:");
			templateBody = templateBody.replace("[","${");
			templateBody = templateBody.replace("]","}");
						
			File templateFile = new File(path+File.separator+templateName);
			if(!templateFile.exists()){
				templateFile.createNewFile();
			}
			
			PrintWriter writter = new PrintWriter(templateFile);
			writter.print("");
			writter.close();
			
			writter = new PrintWriter(templateFile);
			writter.print(templateBody);
			writter.close();
						
			EmailBlastTracking emailBlastTracking = null;
			if(!mailTo.isEmpty()){
				List<String> emails = new ArrayList<String>();
				if(mailTo.indexOf(",") != -1){ 
					String mailToArr[] = mailTo.split(",");
					for(int i=0; i<mailToArr.length; i++){
						emails.add(mailToArr[i].trim());
					}
				}else{
					emails.add(mailTo.trim());
				}
				boolean isMailSent = false;
				for(String email : emails){
					List<Customer> customers = DAORegistry.getCustomerDAO().getCustomerByEmail(email);
					if(customers!= null && !customers.isEmpty()){
						customer = customers.get(0);
					}
					
					emailBlastTracking = new EmailBlastTracking();
					emailBlastTracking.setTemplateId(Integer.parseInt(templateIdStr));
					emailBlastTracking.setSentTime(new Date());
					emailBlastTracking.setSentBy(userName);
					emailBlastTracking.setSentTo(email);
					emailBlastTracking.setStatus(false);
					DAORegistry.getEmailBlastTrackingDAO().save(emailBlastTracking);
					
					//if(customer != null && customer.getEmail()!=null && !customer.getEmail().isEmpty()){
						isMailSent = false;
						Map<String, Object> mailMap = new HashMap<String, Object>();
						//mailMap.put("customerName", customer.getCustomerName()+" "+customer.getLastName());
						com.rtw.tracker.mail.MailAttachment[] mailAttachment = new com.rtw.tracker.mail.MailAttachment[11];
						mailAttachment[0] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","share.png",com.rtw.tmat.utils.Util.getFilePath(request, "share.png", "/resources/images/"));
						mailAttachment[1] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","tyrtf.png",com.rtw.tmat.utils.Util.getFilePath(request, "tyrtf.png", "/resources/images/"));
						mailAttachment[2] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","fb1.png",com.rtw.tmat.utils.Util.getFilePath(request, "fb1.png", "/resources/images/"));
						mailAttachment[3] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","tw1.png",com.rtw.tmat.utils.Util.getFilePath(request, "tw1.png", "/resources/images/"));
						mailAttachment[4] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","ig1.png",com.rtw.tmat.utils.Util.getFilePath(request, "ig1.png", "/resources/images/"));
						mailAttachment[5] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","p1.png",com.rtw.tmat.utils.Util.getFilePath(request, "p1.png", "/resources/images/"));
						mailAttachment[6] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","li1.png",com.rtw.tmat.utils.Util.getFilePath(request, "li1.png", "/resources/images/"));
						mailAttachment[7] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","blue.png",com.rtw.tmat.utils.Util.getFilePath(request, "blue.png", "/resources/images/"));
						mailAttachment[8] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","logo_white_final.png",com.rtw.tmat.utils.Util.getFilePath(request, "logo_white_final.png", "/resources/images/"));
						mailAttachment[9] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","app1.png",com.rtw.tmat.utils.Util.getFilePath(request, "app1.png", "/resources/images/"));
						mailAttachment[10] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","app2.png",com.rtw.tmat.utils.Util.getFilePath(request, "app2.png", "/resources/images/"));
						
						
						String bcc ="msanghani@rightthisway.com,amit.raut@rightthisway.com";
						if(from!=null && (from.equalsIgnoreCase("leor@rewardthefan.com") ||
								from.equalsIgnoreCase("dalia@rewardthefan.com"))){
							bcc += ",leor@rewardthefan.com";
						}
						if(mailBcc != null && !mailBcc.isEmpty()){
							bcc = ","+mailBcc;
						}
						try {
							/*mailManager.sendMailNow("text/html",from,email, 
									mailCc,bcc, mailSubject,
									templateName, mailMap, "text/html", null,mailAttachment,null);*/
							isMailSent = true;
						} catch (Exception e) {
							e.printStackTrace();
							error.setDescription("Error occured while sending mail.");
							genericResponseDTO.setError(error);
							genericResponseDTO.setStatus(0);
							return genericResponseDTO;
						}
						
						if(isMailSent){
							emailBlastTracking.setStatus(true);
							DAORegistry.getEmailBlastTrackingDAO().update(emailBlastTracking);
						}
						if(isMailSent && customer!=null){
							for(Customer cust : customers){
								cust.setIsMailSent(true);
							}
							DAORegistry.getQueryManagerDAO().updateEmailBlastToMailSent(email);
							DAORegistry.getCustomerDAO().updateAll(customers);
						}					
				}
				if(isMailSent){
					genericResponseDTO.setStatus(1);
					genericResponseDTO.setMessage("Email sent to selected customers.");
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while sending mail to customers.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
	@RequestMapping("/UpdateEmailTemplate")
	public GenericResponseDTO updateEmailTemplate(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		
		try{			
			String templateIdStr = request.getParameter("templateId");
			String templateBody = request.getParameter("templateBody");
			String userName = request.getParameter("userName");
			
			if(StringUtils.isEmpty(templateIdStr)){
				System.err.println("Not able to identify Template.");
				error.setDescription("Not able to identify Template.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			Integer templateId = 0;
			try {
				templateId = Integer.parseInt(templateIdStr);
			} catch (Exception e) {
				System.err.println("Could not save, Template body found empty.");
				error.setDescription("Could not save, Template body found emptyo.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			if(StringUtils.isEmpty(templateBody)){
				System.err.println("Could not save, Template body found empty.");
				error.setDescription("Could not save, Template body found empty.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			EmailTemplate template = DAORegistry.getEmailTemplateDAO().get(templateId);
			if(template == null){
				error.setDescription("Template not found in system.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			template.setBody(templateBody);
			template.setLastUpdated(new Date());
			template.setUpdatedBy(userName);			
			DAORegistry.getEmailTemplateDAO().update(template);
			
			genericResponseDTO.setStatus(1);
			genericResponseDTO.setMessage("Template changes saved successfully.");
			
		}catch(Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Updating Email Template.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}

	@RequestMapping({"/ManualFedex"})
	public ManualFedexDTO loadManualFedex(HttpServletRequest request, HttpServletResponse response){
		ManualFedexDTO manualFedexDTO = new ManualFedexDTO();
		Error error = new Error();
		
		try{
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
						
			GridHeaderFilters filter = null;
			try{
				filter = GridHeaderFiltersUtil.getManualFedexGenerationFilter(headerFilter);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				manualFedexDTO.setError(error);
				manualFedexDTO.setStatus(0);
				return manualFedexDTO;
			}
			
			List<ManualFedexGeneration> manualFedexList = null;
			List<Country> countryList = null;
			Integer count = 0;
			
			manualFedexList = DAORegistry.getQueryManagerDAO().getManualFedexGeneration(filter, pageNo);
			count = DAORegistry.getQueryManagerDAO().getManualFedexGenerationCount(filter);
			countryList = DAORegistry.getQueryManagerDAO().getCountries();
			
			if(manualFedexList == null || manualFedexList.size() <= 0){
				manualFedexDTO.setMessage("No Manual Fedex Label Record found.");
			}			
			manualFedexDTO.setStatus(1);
			manualFedexDTO.setFedexGenerationDTO(com.rtw.tracker.utils.Util.getManualFedexGenerationArray(manualFedexList));
			manualFedexDTO.setFedexGenerationPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
			manualFedexDTO.setCountryDTO(com.rtw.tracker.utils.Util.getCountryArray(countryList));
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while Fetching Manual Fedex.");
			manualFedexDTO.setError(error);
			manualFedexDTO.setStatus(0);
		}
		return manualFedexDTO;
	}
	
	/*@RequestMapping({"/GetManualFedex"})
	public ManualFedexDTO getManualFedex(HttpServletRequest request, HttpServletResponse response){
		ManualFedexDTO manualFedexDTO = new ManualFedexDTO();
		Error error = new Error();
		
		try{
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			if(StringUtils.isEmpty(pageNo)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				manualFedexDTO.setError(error);
				manualFedexDTO.setStatus(0);
				return manualFedexDTO;
			}
			try {
				Integer.parseInt(pageNo);
			} catch (Exception e) {
				System.err.println("Please provide valid PageNo.");
				error.setDescription("Please provide valid PageNo.");
				manualFedexDTO.setError(error);
				manualFedexDTO.setStatus(0);
				return manualFedexDTO;
			}
			
			GridHeaderFilters filter = null;
			try{
				filter = GridHeaderFiltersUtil.getManualFedexGenerationFilter(headerFilter);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				manualFedexDTO.setError(error);
				manualFedexDTO.setStatus(0);
				return manualFedexDTO;
			}
			
			List<ManualFedexGeneration> manualFedexList = null;
			Integer count = 0;
			
			manualFedexList = DAORegistry.getQueryManagerDAO().getManualFedexGeneration(filter, pageNo);
			count = DAORegistry.getQueryManagerDAO().getManualFedexGenerationCount(filter);
			
			if(manualFedexList == null || manualFedexList.size() <= 0){
				manualFedexDTO.setMessage("No Manual Fedex Label Record found.");
			}
			manualFedexDTO.setStatus(1);
			manualFedexDTO.setFedexGenerationDTO(com.rtw.tracker.utils.Util.getManualFedexGenerationArray(manualFedexList));
			manualFedexDTO.setFedexGenerationPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while Applying Search - Manual Fedex.");
			manualFedexDTO.setError(error);
			manualFedexDTO.setStatus(0);
		}
		return manualFedexDTO;
	}*/
	

	@RequestMapping({"/ManualFedexExportToExcel"})
	public void manualFedexToExport(HttpServletRequest request, HttpServletResponse response){
				
		try{
			String headerFilter = request.getParameter("headerFilter");
			
			GridHeaderFilters filter = GridHeaderFiltersUtil.getManualFedexGenerationFilter(headerFilter);
			
			List<ManualFedexGeneration> manualFedexList = null;
			manualFedexList = DAORegistry.getQueryManagerDAO().getManualFedexGenerationToExport(filter);
			
			if(manualFedexList!=null && !manualFedexList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Manual_Fedex_Label");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Manual Fedex Id");				
				header.createCell(1).setCellValue("Full Name");
				header.createCell(2).setCellValue("Company Name");
				header.createCell(3).setCellValue("Street1");
				header.createCell(4).setCellValue("Street2");
				header.createCell(5).setCellValue("City");
				header.createCell(6).setCellValue("State");
				header.createCell(7).setCellValue("Country");
				header.createCell(8).setCellValue("ZipCode");
				header.createCell(9).setCellValue("Phone");
				header.createCell(10).setCellValue("From Address First Name");
				header.createCell(11).setCellValue("From Address Last Name");
				header.createCell(12).setCellValue("From Address Company Name");				
				header.createCell(13).setCellValue("From Address Street1");
				header.createCell(14).setCellValue("From Address Street2");
				header.createCell(15).setCellValue("From Address City");
				header.createCell(16).setCellValue("From Address State");
				header.createCell(17).setCellValue("From Address Country");
				header.createCell(18).setCellValue("From Address ZipCode");
				header.createCell(19).setCellValue("From Address Phone");
				header.createCell(20).setCellValue("Service Type");
				header.createCell(21).setCellValue("Tracking Number");
				header.createCell(22).setCellValue("Fedex Label File");
				header.createCell(23).setCellValue("Created Date");
				header.createCell(24).setCellValue("Created By");
				Integer i=1;
				for(ManualFedexGeneration manualFedex : manualFedexList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(manualFedex.getId());
					row.createCell(1).setCellValue(manualFedex.getShCustomerName()!=null?manualFedex.getShCustomerName():"");
					row.createCell(2).setCellValue(manualFedex.getShCompanyName()!=null?manualFedex.getShCompanyName():"");
					row.createCell(3).setCellValue(manualFedex.getShAddress1()!=null?manualFedex.getShAddress1():"");
					row.createCell(4).setCellValue(manualFedex.getShAddress2()!=null?manualFedex.getShAddress2():"");
					row.createCell(5).setCellValue(manualFedex.getShCity()!=null?manualFedex.getShCity():"");
					row.createCell(6).setCellValue(manualFedex.getShStateName()!=null?manualFedex.getShStateName():"");
					row.createCell(7).setCellValue(manualFedex.getShCountryName()!=null?manualFedex.getShCountryName():"");
					row.createCell(8).setCellValue(manualFedex.getShZipCode()!=null?manualFedex.getShZipCode():"");
					row.createCell(9).setCellValue(manualFedex.getShPhone()!=null?manualFedex.getShPhone():"");
					row.createCell(10).setCellValue(manualFedex.getBlFirstName()!=null?manualFedex.getBlFirstName():"");
					row.createCell(11).setCellValue(manualFedex.getBlLastName()!=null?manualFedex.getBlLastName():"");
					row.createCell(12).setCellValue(manualFedex.getBlCompanyName()!=null?manualFedex.getBlCompanyName():"");					
					row.createCell(13).setCellValue(manualFedex.getBlAddress1()!=null?manualFedex.getBlAddress1():"");
					row.createCell(14).setCellValue(manualFedex.getBlAddress2()!=null?manualFedex.getBlAddress2():"");
					row.createCell(15).setCellValue(manualFedex.getBlCity()!=null?manualFedex.getBlCity():"");
					row.createCell(16).setCellValue(manualFedex.getBlStateName()!=null?manualFedex.getBlStateName():"");
					row.createCell(17).setCellValue(manualFedex.getBlCountryName()!=null?manualFedex.getBlCountryName():"");
					row.createCell(18).setCellValue(manualFedex.getBlZipCode()!=null?manualFedex.getBlZipCode():"");
					row.createCell(19).setCellValue(manualFedex.getBlPhone()!=null?manualFedex.getBlPhone():"");
					row.createCell(20).setCellValue(manualFedex.getServiceType()!=null?manualFedex.getServiceType():"");
					row.createCell(21).setCellValue(manualFedex.getTrackingNumber()!=null?manualFedex.getTrackingNumber():"");
					row.createCell(22).setCellValue(manualFedex.getFedexLabelPath()!=null?manualFedex.getFedexLabelPath():"");
					row.createCell(23).setCellValue(manualFedex.getCreatedDateStr()!=null?manualFedex.getCreatedDateStr():"");
					row.createCell(24).setCellValue(manualFedex.getCreatedBy()!=null?manualFedex.getCreatedBy():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Manual_Fedex.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
}
