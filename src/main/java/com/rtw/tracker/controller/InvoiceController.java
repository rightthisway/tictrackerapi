package com.rtw.tracker.controller;

import java.io.File;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtw.tmat.utils.Constants;
import com.rtw.tmat.utils.Error;
import com.rtw.tmat.utils.OrderConfirmation;
import com.rtw.tmat.utils.StripeCredentials;
import com.rtw.tmat.utils.StripeTransaction;
import com.rtw.tmat.utils.TextUtil;
import com.rtw.tmat.utils.Util;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.OrderTicketGroupDetails;
import com.rtw.tracker.datas.CustomerLoyalty;
import com.rtw.tracker.datas.Event;
import com.rtw.tracker.datas.Venue;
import com.rtw.tracker.datas.AddressType;
import com.rtw.tracker.datas.ApplicationPlatform;
import com.rtw.tracker.datas.CategoryTicket;
import com.rtw.tracker.datas.CategoryTicketGroup;
import com.rtw.tracker.datas.Country;
import com.rtw.tracker.datas.Customer;
import com.rtw.tracker.datas.CustomerAddress;
import com.rtw.tracker.datas.CustomerLoyaltyHistory;
import com.rtw.tracker.datas.CustomerOrder;
import com.rtw.tracker.datas.CustomerOrderDetails;
import com.rtw.tracker.datas.EventDetails;
import com.rtw.tracker.datas.InventoryHold;
import com.rtw.tracker.datas.Invoice;
import com.rtw.tracker.datas.ProductType;
import com.rtw.tracker.datas.RealTicketSectionDetails;
import com.rtw.tracker.datas.ReferralCodeValidation;
import com.rtw.tracker.datas.ShippingMethod;
import com.rtw.tracker.datas.SignupType;
import com.rtw.tracker.datas.State;
import com.rtw.tracker.datas.Ticket;
import com.rtw.tracker.datas.TicketGroup;
import com.rtw.tracker.datas.TicketStatus;
import com.rtw.tracker.datas.WalletTransaction;
import com.rtw.tracker.enums.OrderType;
import com.rtw.tracker.enums.PartialPaymentMethod;
import com.rtw.tracker.enums.PaymentMethod;
import com.rtw.tracker.enums.PaymentMethods;
import com.rtw.tracker.mail.MailManager;
import com.rtw.tracker.pojos.GenericResponseDTO;
import com.rtw.tracker.pojos.InvoiceCreationDTO;
import com.rtw.tracker.pojos.InvoiceCreationForHoldTicketDTO;
import com.rtw.tracker.pojos.InvoiceCreationForRealTicketDTO;
import com.rtw.tracker.pojos.InvoiceGetLongTicketDTO;
import com.rtw.tracker.pojos.ShippingAddressDTO;
import com.rtw.tracker.pojos.TicketListDTO;
import com.rtw.tracker.utils.JsonWrapperUtil;
import com.rtw.tracker.utils.PaginationUtil;

@Controller
@RequestMapping(value="/Invoice")
public class InvoiceController {

	private static final Logger LOGGER = LoggerFactory.getLogger(InvoiceController.class);
	private SharedProperty sharedProperty;
	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}

	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}
	
	private MailManager mailManager;
	public MailManager getMailManager() {
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}
	
	@RequestMapping(value = "/CreateInvoice")
	public InvoiceCreationDTO createInvoice(HttpServletRequest request, HttpServletResponse response){
		InvoiceCreationDTO invoiceCreationDTO = new InvoiceCreationDTO();
		Error error = new Error();
		
		try{
			String ticketIdStr = request.getParameter("ticketId");
			String invoiceIdStr = request.getParameter("invoiceId");
			String brokerIdStr = request.getParameter("brokerId");
			
			if(StringUtils.isEmpty(ticketIdStr)){				
				System.err.println("Please provide Ticket Info.");
				error.setDescription("Please provide Ticket Info.");
				invoiceCreationDTO.setError(error);
				invoiceCreationDTO.setStatus(0);
				return invoiceCreationDTO;
			}
			Integer ticketId = 0;
			try{
				ticketId = Integer.parseInt(ticketIdStr);
			}catch(Exception e){
				System.err.println("Please provide valid Ticket Info.");
				error.setDescription("Please provide valid Ticket Info.");
				invoiceCreationDTO.setError(error);
				invoiceCreationDTO.setStatus(0);
				return invoiceCreationDTO;
			}
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide BrokerId");
				error.setDescription("Please provide BrokerId");
				invoiceCreationDTO.setError(error);
				invoiceCreationDTO.setStatus(0);
				return invoiceCreationDTO;
			}			
			Integer brokerId = 0;	
			try{
				brokerId = Integer.parseInt(brokerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid BrokerId");
				error.setDescription("Please provide valid BrokerId");
				invoiceCreationDTO.setError(error);
				invoiceCreationDTO.setStatus(0);
				return invoiceCreationDTO;
			}
			
			CategoryTicketGroup catTix = null;
			catTix = DAORegistry.getCategoryTicketGroupDAO().getCategoryTicketByIdAndBrokerId(ticketId, brokerId);
			if(catTix == null){
				System.err.println("Selected Ticket Information is not found for ticketId : "+ticketId);
				error.setDescription("Selected Ticket Information is not found for ticketId : "+ticketId);
				invoiceCreationDTO.setError(error);
				invoiceCreationDTO.setStatus(0);
				return invoiceCreationDTO;
			}
			
			Event eventDetail = null;
			eventDetail = DAORegistry.getEventDAO().get(catTix.getEventId());
			if(eventDetail == null){
				System.err.println("Event details not found for selected ticket.");
				error.setDescription("Event details not found for selected ticket.");
				invoiceCreationDTO.setError(error);
				invoiceCreationDTO.setStatus(0);
				return invoiceCreationDTO;
			}
			
			if(catTix.getShippingMethodId()!=null && catTix.getShippingMethodId()>=0){
				ShippingMethod shippingMethod = DAORegistry.getShippingMethodDAO().getShippingMethodById(catTix.getShippingMethodId());
				invoiceCreationDTO.setShippingMethod(shippingMethod.getName());
				invoiceCreationDTO.setShippingMethodId(shippingMethod.getId());
			}
			
			Map<String, String> paramMap = Util.getParameterMap(request);
			String data = Util.getObject(paramMap,Constants.BASE_URL+Constants.GET_STRIPE_CREDENTIALS);
			Gson gson = new Gson();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			StripeCredentials stripeCredentials = gson.fromJson(((JsonObject)jsonObject.get("stripeCredentials")), StripeCredentials.class);
			
			if(stripeCredentials== null){
				System.err.println("Not able to retrive stripe configuration credential, It happens mostly when rewardthefan API is down.");
				error.setDescription("Not able to retrive stripe configuration credential, It happens mostly when rewardthefan API is down.");
				invoiceCreationDTO.setError(error);
				invoiceCreationDTO.setStatus(0);
				return invoiceCreationDTO;
			}else if(stripeCredentials.getStatus()==1){
				invoiceCreationDTO.setsPubKey(stripeCredentials.getPublishableKey());
			}else{
				System.err.println(stripeCredentials.getError().getDescription());
				error.setDescription(stripeCredentials.getError().getDescription());
				invoiceCreationDTO.setError(error);
				invoiceCreationDTO.setStatus(0);
				return invoiceCreationDTO;
			}
			
			Collection<ShippingMethod> methodList = DAORegistry.getShippingMethodDAO().getAll();
			Map<Integer, String> shippingMap = new HashMap<Integer, String>();
			for(ShippingMethod method : methodList){
				shippingMap.put(method.getId(),method.getName());
			}
			
			List<CategoryTicketGroup> categoryTickets = new ArrayList<CategoryTicketGroup>();
			categoryTickets.add(catTix);
			
			Venue venue = DAORegistry.getVenueDAO().get(eventDetail.getVenueId());
			
			invoiceCreationDTO.setStatus(1);
			invoiceCreationDTO.setEventDetailsDTO(com.rtw.tracker.utils.Util.getEventDetailsObject(eventDetail, venue));
			invoiceCreationDTO.setCategoryTicketGroupListDTOs(com.rtw.tracker.utils.Util.getCategoryTicketGroupArray(categoryTickets, shippingMap, eventDetail));
			invoiceCreationDTO.setTicketId(ticketId);
			invoiceCreationDTO.setInvoiceId(invoiceIdStr);
			invoiceCreationDTO.setType("CATEGORY");
			
		}catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Opening Invoice Creation Pop-up.");
			invoiceCreationDTO.setError(error);
			invoiceCreationDTO.setStatus(0);
		}
		return invoiceCreationDTO;
	}
	
	@RequestMapping(value = "/CreateInvoiceRealTicket")
	public InvoiceCreationForRealTicketDTO createInvoiceRealticket(HttpServletRequest request, HttpServletResponse response){
		InvoiceCreationForRealTicketDTO invoiceCreationForRealTicketDTO = new InvoiceCreationForRealTicketDTO();
		Error error = new Error();
		
		try{			
			String ticketGroupIdStr = request.getParameter("ticketGroupId");
			String invoiceIdStr = request.getParameter("invoiceId");
			String brokerIdStr = request.getParameter("brokerId");
			
			if(StringUtils.isEmpty(ticketGroupIdStr)){				
				System.err.println("Please provide Ticket Info.");
				error.setDescription("Please provide Ticket Info.");
				invoiceCreationForRealTicketDTO.setError(error);
				invoiceCreationForRealTicketDTO.setStatus(0);
				return invoiceCreationForRealTicketDTO;
			}
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide BrokerId");
				error.setDescription("Please provide BrokerId");
				invoiceCreationForRealTicketDTO.setError(error);
				invoiceCreationForRealTicketDTO.setStatus(0);
				return invoiceCreationForRealTicketDTO;
			}			
			Integer brokerId = 0;	
			try{
				brokerId = Integer.parseInt(brokerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid BrokerId");
				error.setDescription("Please provide valid BrokerId");
				invoiceCreationForRealTicketDTO.setError(error);
				invoiceCreationForRealTicketDTO.setStatus(0);
				return invoiceCreationForRealTicketDTO;
			}
			
			List<Integer> ticketIds = new ArrayList<Integer>();
			if(ticketGroupIdStr.contains(",")){
				String arr[] = ticketGroupIdStr.split(",");
				for(String str : arr){
					try {
						if(str!=null && !str.trim().isEmpty()){
							ticketIds.add(Integer.parseInt(str));
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}else{
				ticketIds.add(Integer.parseInt(ticketGroupIdStr));
			}
			
			if(ticketIds.isEmpty()){
				System.err.println("Please provide Ticket Info.");
				error.setDescription("Please provide Ticket Info.");
				invoiceCreationForRealTicketDTO.setError(error);
				invoiceCreationForRealTicketDTO.setStatus(0);
				return invoiceCreationForRealTicketDTO;
			}
			
			List<TicketGroup> ticketGroups = new ArrayList<TicketGroup>();
			Event eventDetail = null;
			for(Integer ticketGroupId : ticketIds){
				
				List<Ticket> ticketList = DAORegistry.getTicketDAO().getAvailableTicketsByTicketGroupId(ticketGroupId);
				TicketGroup ticGroup = DAORegistry.getTicketGroupDAO().getTicketGroupByIdAndBrokerId(ticketGroupId, brokerId);
				if(ticGroup == null){
					System.err.println("Selected Ticket Information is not found for ticketId : "+ticketGroupIdStr);
					error.setDescription("Selected Ticket Information is not found for ticketId : "+ticketGroupIdStr);
					invoiceCreationForRealTicketDTO.setError(error);
					invoiceCreationForRealTicketDTO.setStatus(0);
					return invoiceCreationForRealTicketDTO;
				}			
				if(!ticketList.isEmpty()){
					ticGroup.setSeatHigh(ticketList.get(ticketList.size()-1).getSeatNo());
					ticGroup.setSeatLow(ticketList.get(0).getSeatNo());
				}
				ticGroup.setQuantity(ticketList.size());
				
				eventDetail = DAORegistry.getEventDAO().get(ticGroup.getEventId());
				if(eventDetail==null){
					System.err.println("Event details not found for selected ticket.");
					error.setDescription("Event details not found for selected ticket.");
					invoiceCreationForRealTicketDTO.setError(error);
					invoiceCreationForRealTicketDTO.setStatus(0);
					return invoiceCreationForRealTicketDTO;
				}
				ticketGroups.add(ticGroup);
			}
			
			Collection<ShippingMethod> methodList = DAORegistry.getShippingMethodDAO().getAll();
			Map<Integer, String> shippingMap = new HashMap<Integer, String>();
			for(ShippingMethod method : methodList){
				shippingMap.put(method.getId(),method.getName());
			}
			
			Map<String, String> paramMap = Util.getParameterMap(request);
			String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+Constants.GET_STRIPE_CREDENTIALS);
			Gson gson = new Gson();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			StripeCredentials stripeCredentials = gson.fromJson(((JsonObject)jsonObject.get("stripeCredentials")), StripeCredentials.class);
			
			if(stripeCredentials== null){
				System.err.println("Not able to retrive stripe configuration credential, It happens mostly when rewardthefan API is down.");
				error.setDescription("Not able to retrive stripe configuration credential, It happens mostly when rewardthefan API is down.");
				invoiceCreationForRealTicketDTO.setError(error);
				invoiceCreationForRealTicketDTO.setStatus(0);
				return invoiceCreationForRealTicketDTO;
			}else if(stripeCredentials.getStatus()==1){
				invoiceCreationForRealTicketDTO.setsPubKey(stripeCredentials.getPublishableKey());
			}else{
				System.err.println(stripeCredentials.getError().getDescription());
				error.setDescription(stripeCredentials.getError().getDescription());
				invoiceCreationForRealTicketDTO.setError(error);
				invoiceCreationForRealTicketDTO.setStatus(0);
				return invoiceCreationForRealTicketDTO;
			}
			
			Venue venue = DAORegistry.getVenueDAO().get(eventDetail.getVenueId());
			
			invoiceCreationForRealTicketDTO.setStatus(1);
			invoiceCreationForRealTicketDTO.setEventDetailsDTO(com.rtw.tracker.utils.Util.getEventDetailsObject(eventDetail, venue));
			invoiceCreationForRealTicketDTO.setTicketGroupDTOs(com.rtw.tracker.utils.Util.getTicketGroupArray(ticketGroups, shippingMap, eventDetail));
			invoiceCreationForRealTicketDTO.setTicketId(ticketGroupIdStr);
			invoiceCreationForRealTicketDTO.setInvoiceId(invoiceIdStr);
			invoiceCreationForRealTicketDTO.setType("REAL");
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return invoiceCreationForRealTicketDTO;
	}
	
	
	@RequestMapping(value = "/CreateInvoiceHoldTicket")
	public InvoiceCreationForHoldTicketDTO createInvoiceHoldTicket(HttpServletRequest request, HttpServletResponse response){
		InvoiceCreationForHoldTicketDTO invoiceCreationForHoldTicketDTO = new InvoiceCreationForHoldTicketDTO();
		Error error = new Error();
		
		try{
			String ticketGroupIdStr = request.getParameter("ticketGroupId");
			String invoiceIdStr = request.getParameter("invoiceId");
			String ids = request.getParameter("ticketIds");
			String salePrice = request.getParameter("salePrice");			
			String brokerIdStr = request.getParameter("brokerId");
			
			if(StringUtils.isEmpty(ticketGroupIdStr)){				
				System.err.println("Please provide Ticket Info.");
				error.setDescription("Please provide Ticket Info.");
				invoiceCreationForHoldTicketDTO.setError(error);
				invoiceCreationForHoldTicketDTO.setStatus(0);
				return invoiceCreationForHoldTicketDTO;
			}
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide BrokerId");
				error.setDescription("Please provide BrokerId");
				invoiceCreationForHoldTicketDTO.setError(error);
				invoiceCreationForHoldTicketDTO.setStatus(0);
				return invoiceCreationForHoldTicketDTO;
			}			
			Integer brokerId = 0;	
			try{
				brokerId = Integer.parseInt(brokerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid BrokerId");
				error.setDescription("Please provide valid BrokerId");
				invoiceCreationForHoldTicketDTO.setError(error);
				invoiceCreationForHoldTicketDTO.setStatus(0);
				return invoiceCreationForHoldTicketDTO;
			}
			
			String arr[] = null;			
			if(ids !=null && !ids.isEmpty()){
				if(ids.contains(",")){
					arr = ids.split(",");
				}else{
					arr = new String[1];
					arr[0] = ids;
				}
			}
			List<Integer> list = new ArrayList<Integer>();
			for(String str : arr){
				if(!str.trim().isEmpty()){
					list.add(Integer.parseInt(str));
				}
			}
						
			Integer ticketGroupId = Integer.parseInt(ticketGroupIdStr);
			List<TicketGroup> ticketGroups = new ArrayList<TicketGroup>();
			
			List<Ticket> ticketList = DAORegistry.getTicketDAO().getTicketsByTicketIdsAndStatus(list);
			TicketGroup ticGroup = DAORegistry.getTicketGroupDAO().getTicketGroupByIdAndBrokerId(ticketGroupId, brokerId);			
			if(ticGroup == null || ticketList == null){
				System.err.println("Selected Ticket Information is not found.");
				error.setDescription("Selected Ticket Information is not found.");
				invoiceCreationForHoldTicketDTO.setError(error);
				invoiceCreationForHoldTicketDTO.setStatus(0);
				return invoiceCreationForHoldTicketDTO;
			}
			if(!ticketList.isEmpty()){
				ticGroup.setSeatHigh(ticketList.get(ticketList.size()-1).getSeatNo());
				ticGroup.setSeatLow(ticketList.get(0).getSeatNo());
				ticGroup.setQuantity(ticketList.size());
			}
			if(ticGroup != null){
				ticketGroups.add(ticGroup);
			}
			
			Event eventDetail = null;
			if(ticGroup != null && ticGroup.getEventId() != null){				
				eventDetail = DAORegistry.getEventDAO().get(ticGroup.getEventId());
				if(eventDetail==null){
					System.err.println("Event details not found for selected ticket.");
					error.setDescription("Event details not found for selected ticket.");
					invoiceCreationForHoldTicketDTO.setError(error);
					invoiceCreationForHoldTicketDTO.setStatus(0);
					return invoiceCreationForHoldTicketDTO;
				}
			}
						
			Collection<ShippingMethod> methodList = DAORegistry.getShippingMethodDAO().getAll();
			Map<Integer, String> shippingMap = new HashMap<Integer, String>();
			for(ShippingMethod method : methodList){
				shippingMap.put(method.getId(),method.getName());
			}			
			
			Map<String, String> paramMap = Util.getParameterMap(request);
			String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+Constants.GET_STRIPE_CREDENTIALS);
			Gson gson = new Gson();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			StripeCredentials stripeCredentials = gson.fromJson(((JsonObject)jsonObject.get("stripeCredentials")), StripeCredentials.class);
			
			if(stripeCredentials== null){
				System.err.println("Not able to retrive stripe configuration credential, It happens mostly when rewardthefan API is down.");
				error.setDescription("Not able to retrive stripe configuration credential, It happens mostly when rewardthefan API is down.");
				invoiceCreationForHoldTicketDTO.setError(error);
				invoiceCreationForHoldTicketDTO.setStatus(0);
				return invoiceCreationForHoldTicketDTO;
			}else if(stripeCredentials.getStatus()==1){
				invoiceCreationForHoldTicketDTO.setsPubKey(stripeCredentials.getPublishableKey());
			}else{
				System.err.println(stripeCredentials.getError().getDescription());
				error.setDescription(stripeCredentials.getError().getDescription());
				invoiceCreationForHoldTicketDTO.setError(error);
				invoiceCreationForHoldTicketDTO.setStatus(0);
				return invoiceCreationForHoldTicketDTO;
			}
			Venue venue = DAORegistry.getVenueDAO().get(eventDetail.getVenueId());
			
			invoiceCreationForHoldTicketDTO.setStatus(1);
			invoiceCreationForHoldTicketDTO.setEventDetailsDTO(com.rtw.tracker.utils.Util.getEventDetailsObject(eventDetail, venue));
			invoiceCreationForHoldTicketDTO.setTicketGroupDTOs(com.rtw.tracker.utils.Util.getTicketGroupArray(ticketGroups, shippingMap, eventDetail));
			invoiceCreationForHoldTicketDTO.setTicketId(ticketGroupIdStr);
			invoiceCreationForHoldTicketDTO.setTicketIds(ids);
			invoiceCreationForHoldTicketDTO.setInvoiceId(invoiceIdStr);
			invoiceCreationForHoldTicketDTO.setSalePrice(salePrice);
			invoiceCreationForHoldTicketDTO.setType("HOLD");
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return invoiceCreationForHoldTicketDTO;
	}
	
	/*
	@RequestMapping(value = "/ShowInvoice")
	public String ShowInvoice(ModelMap map, HttpServletRequest request, HttpServletResponse response, HttpSession session){
		LOGGER.info("Show invoice request processing....");
		try{
			String orderIdStr = request.getParameter("orderId");
			String invoiceIdStr = request.getParameter("invoiceId");
			if(orderIdStr==null || orderIdStr.isEmpty()){
				map.put("errorMessage", "There is something wrong...Please try again");
				return "page-view-invoice-summery";
			}
			
			if(invoiceIdStr==null || invoiceIdStr.isEmpty()){
				map.put("errorMessage", "There is something wrong...Please try again");
				return "page-view-invoice-summery";
			}
			
			
			Integer orderId = Integer.parseInt(orderIdStr);
			Integer invoiceId = Integer.parseInt(invoiceIdStr);
			
			Invoice invoice = DAORegistry.getInvoiceDAO().get(invoiceId);
			CustomerOrderDetails orderDetails = DAORegistry.getCustomerOrderDetailsDAO().getCustomerOrderDetailsByOrderId(orderId);
			CustomerOrder customerOrder = DAORegistry.getCustomerOrderDAO().get(orderId);
			CustomerLoyaltyHistory customerLoyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyByOrderId(orderId);
			
			map.put("invoice", invoice);
			map.put("orderDetails", orderDetails);
			map.put("order", customerOrder);
			map.put("invoiceId", invoiceId);
			map.put("customerLoyalty",customerLoyaltyHistory);
			map.put("successMessage", "Invoice created successfully. Your invoice number is ");
		}catch (Exception e) {
			e.printStackTrace();
			map.put("errorMessage", "There is something wrong...Please try again");
		}
		return "page-view-invoice-summery";
	}
	*/
	
	@RequestMapping(value = "/SaveInvoice")
	public GenericResponseDTO saveInvoice(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		
		try {
			String action = request.getParameter("action");
			String customerId = request.getParameter("customerId");
			String ticketIdStr = request.getParameter("ticketId_0");
			String userName = request.getParameter("userName");
			String brokerIdStr = request.getParameter("brokerId");			
			String clientIpAddress = request.getParameter("clientIPAddress");
			String sessionIdStr = request.getParameter("deviceId");
			String orderType = request.getParameter("orderType");
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide BrokerId");
				error.setDescription("Please provide BrokerId");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}			
			Integer brokerId = 0;	
			try{
				brokerId = Integer.parseInt(brokerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid BrokerId");
				error.setDescription("Please provide valid BrokerId");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			String returnMessage = "";
			
			Integer invoiceId=0,orderId=0;
			Double ticketSoldPrice =null;
			Double taxAmount = 0.0;
			if(returnMessage.isEmpty()){
				
				if(action != null && action.equals("saveInvoice")){
					
					String soldPrice = request.getParameter("soldPrice_0");
					String isLoyalFan = request.getParameter("isLoyalFanPrice");
					String taxAmountstr = request.getParameter("taxAmount_0");
					String ticketCountStr = request.getParameter("ticketCount_0");
					String eventIdStr = request.getParameter("eventId");
					
					String addressLine1 = request.getParameter("addressLine1");
					String addressLine2 = request.getParameter("addressLine2");
					String shAddressLine1 = request.getParameter("shAddressLine1");
					String shAddressLine2 = request.getParameter("shAddressLine2");
					String bAddressId =request.getParameter("bAddressId"); 
					String sAddressId =request.getParameter("sAddressId");
					String referalCode =request.getParameter("referralCode");
					
					String msg = "";
					ticketSoldPrice= Double.parseDouble(soldPrice);
					if(taxAmountstr!=null && !taxAmountstr.isEmpty()){
						taxAmount = Double.parseDouble(taxAmountstr);
					}
					
					if(StringUtils.isEmpty(ticketIdStr)){				
						System.err.println("Not able to identify ticket information.");
						error.setDescription("Not able to identify ticket information.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					if(StringUtils.isEmpty(orderType)){				
						System.err.println("Please select valid order Type.");
						error.setDescription("Please select valid order Type.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					if(StringUtils.isEmpty(eventIdStr)){				
						System.err.println("Not able to identify Event information.");
						error.setDescription("Not able to identify Event information.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					Integer eventId = null;
					try {
						eventId = Integer.parseInt(eventIdStr);
					} catch (Exception e) {
						e.printStackTrace();
						error.setDescription("Not able to identify Event information.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					
					if(StringUtils.isEmpty(ticketCountStr)){				
						System.err.println("Not able to identify ticket quantity.");
						error.setDescription("Not able to identify ticket quantity.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					Integer ticketCount = null;					
					try {
						ticketCount = Integer.parseInt(ticketCountStr);
					} catch (Exception e) {
						e.printStackTrace();
						error.setDescription("Bad value found for Ticket quantity, It should be Valid Integer.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					
					try {
						ticketSoldPrice= Double.parseDouble(soldPrice);
					} catch (Exception e) {
						e.printStackTrace();
						error.setDescription("Bad value found for Sold price, It should be valid Double value.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					
					if(StringUtils.isEmpty(bAddressId)){				
						System.err.println("Billing Address Not found, Please select Billing Address.");
						error.setDescription("Billing Address Not found, Please select Billing Address.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					if(StringUtils.isEmpty(sAddressId)){				
						System.err.println("Shipping Address Not found, Please select Shipping Address.");
						error.setDescription("Shipping Address Not found, Please select Shipping Address.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					if((addressLine1==null || addressLine1.isEmpty()) && (addressLine2==null || addressLine2.isEmpty())){
						System.err.println("Selected billing address does not have either AddressLine1 or AddressLine2.");
						error.setDescription("Selected billing address does not have either AddressLine1 or AddressLine2.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					if((shAddressLine1==null || shAddressLine1.isEmpty()) && (shAddressLine2==null || shAddressLine2.isEmpty())){
						System.err.println("Selected shipping address does not have either AddressLine1 or AddressLine2.");
						error.setDescription("Selected shipping address does not have either AddressLine1 or AddressLine2.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					
					Integer ticketId = Integer.parseInt(ticketIdStr);
					
					CategoryTicketGroup categoryTicketGroup  = DAORegistry.getCategoryTicketGroupDAO().get(ticketId);
					if(categoryTicketGroup==null){
						System.err.println("Selected ticket information is not found, Please refresh page and try again.");
						error.setDescription("Selected ticket information is not found, Please refresh page and try again.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					categoryTicketGroup.setSoldPrice(ticketSoldPrice);
					categoryTicketGroup.setBroadcast(true);
					categoryTicketGroup.setTaxAmount(taxAmount);
					DAORegistry.getCategoryTicketGroupDAO().update(categoryTicketGroup);
					String shippingIsSameAsBilling = request.getParameter("shippingIsSameAsBilling"); 
					
					
					Double orderTotal = (ticketSoldPrice * ticketCount) + Util.getBrokerServiceFees(brokerId, ticketSoldPrice, ticketCount, taxAmount);
					orderTotal = Util.getRoundedValue(orderTotal);
					
					CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(Integer.parseInt(customerId));
					if(customerLoyalty==null){
						customerLoyalty = new CustomerLoyalty();
						customerLoyalty.setActivePoints(0.00);
						customerLoyalty.setActiveRewardDollers(0.00);
						customerLoyalty.setCustomerId(Integer.parseInt(customerId));
						customerLoyalty.setLastUpdate(new Date());
						customerLoyalty.setLatestEarnedPoints(0.00);
						customerLoyalty.setLatestSpentPoints(0.00);
						customerLoyalty.setPendingPoints(0.00);
						customerLoyalty.setRevertedSpentPoints(0.00);
						customerLoyalty.setTotalEarnedPoints(0.00);
						customerLoyalty.setTotalSpentPoints(0.00);
						customerLoyalty.setVoidedRewardPoints(0.00);
						DAORegistry.getCustomerLoyaltyDAO().save(customerLoyalty);
					}
					if(referalCode!=null && !referalCode.isEmpty()){
						Map<String, String> paramMap = Util.getParameterMap(request);
						paramMap.put("referralCode", referalCode);
						paramMap.put("customerId",customerId);
						paramMap.put("eventId",eventIdStr);
						paramMap.put("tgId", ticketIdStr);
						paramMap.put("orderTotal",String.valueOf(orderTotal));
						paramMap.put("clientIPAddress", clientIpAddress);
						paramMap.put("isLongSale", "false");
						if(isLoyalFan!=null && !isLoyalFan.isEmpty()){
							if(isLoyalFan.equalsIgnoreCase("Yes")){
								paramMap.put("isFanPrice","true");
							}else{
								paramMap.put("isFanPrice","false");
							}
						}
						paramMap.put("orderTotal",String.valueOf(orderTotal));
						paramMap.put("sessionId", sessionIdStr);
						paramMap.put("orderQty",ticketCountStr);
						 
					    String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.VALIDATE_PROMOCODE);
					    Gson gson = new Gson();  
						JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
						ReferralCodeValidation referralCodeValidation = gson.fromJson(((JsonObject)jsonObject.get("referralCodeValidation")), ReferralCodeValidation.class);
						
						if(referralCodeValidation == null){
							System.out.println("Failed while validating referal code, Failed to communicate with our API server");
							error.setDescription("Failed while validating referal code, Failed to communicate with our API server");
							genericResponseDTO.setError(error);
							genericResponseDTO.setStatus(0);
							return genericResponseDTO;
						}else if(referralCodeValidation.getStatus()==1){
							msg = "REFERRALCODEVALIDATED";
						}else if(referralCodeValidation.getStatus()==0){							
							msg = referralCodeValidation.getError().getDescription();
							System.out.println(msg);
							error.setDescription(msg);
							genericResponseDTO.setError(error);
							genericResponseDTO.setStatus(0);
							return genericResponseDTO;
						}
					}
					
					String paymentMethod = request.getParameter("paymentMethod");
					if(TextUtil.isEmptyOrNull(paymentMethod)){
						System.out.println("Not able to identify payment method.");
						error.setDescription("Not able to identify payment method.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					
					PaymentMethods method = PaymentMethods.valueOf(paymentMethod);
					
					Map<String, String> orderParamMap = Util.getParameterMap(request);
					orderParamMap.put("serviceFees", String.valueOf(categoryTicketGroup.getTaxAmount()));
					String validationMsg = "";
					String paymentMethodCount = null;
					WalletTransaction walletTranx = null;
					StripeTransaction transaction = null;
					String primaryTrxId = "";
					String secondaryTrxId="";
					String thirdTrxId="";
					String primaryAmount=null;
					String secondaryAmount =null;
					String thirdAmount=null;
					String data = null;
					String rewardPoints = null;
					Gson gson = new Gson();
					JsonObject jsonObject = null;
					switch(method){
					case CREDITCARD :
						primaryAmount = String.valueOf(orderTotal);
						orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.CREDITCARD));
						orderParamMap.put("primaryPaymentAmount",primaryAmount);
						paymentMethodCount="ONE";
						break;
					case WALLET :
						primaryAmount = String.valueOf(orderTotal);
						orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.CUSTOMER_WALLET));
						orderParamMap.put("primaryPaymentAmount",primaryAmount);
						paymentMethodCount="ONE";
						break;
					case REWARDPOINTS :
						primaryAmount = String.valueOf(orderTotal);
						rewardPoints = request.getParameter("rewardPoints");
						orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.FULL_REWARDS));
						orderParamMap.put("primaryPaymentAmount",primaryAmount);
						paymentMethodCount="ONE";
						break;
					case PARTIAL_REWARD_CC :
						rewardPoints =  request.getParameter("rewardPoints");
						primaryAmount = rewardPoints;
						secondaryAmount = request.getParameter("cardAmount");
						if(primaryAmount==null || primaryAmount.isEmpty()){
							validationMsg ="Reward point amount not found.";
						}else if(secondaryAmount==null || secondaryAmount.isEmpty()){
							validationMsg ="Card amount is not found.";
						}else{
							orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.PARTIAL_REWARDS));
							orderParamMap.put("primaryPaymentAmount",primaryAmount);
							orderParamMap.put("secondaryPaymentMethod",String.valueOf(PartialPaymentMethod.CREDITCARD));
							orderParamMap.put("secondaryPaymentAmount",secondaryAmount);
							paymentMethodCount="TWO";
						}
						break;
					case PARTIAL_REWARD_WALLET :
						secondaryAmount = request.getParameter("walletAmount");
						rewardPoints =  request.getParameter("rewardPoints");
						primaryAmount = rewardPoints;
						if(primaryAmount==null || primaryAmount.isEmpty()){
							validationMsg ="Reward point amount is not found.";
						}else if(secondaryAmount==null || secondaryAmount.isEmpty()){
							validationMsg ="Wallet amount is not found.";
						}else{
							orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.PARTIAL_REWARDS));
							orderParamMap.put("primaryPaymentAmount",primaryAmount);
							orderParamMap.put("secondaryPaymentMethod",String.valueOf(PartialPaymentMethod.CUSTOMER_WALLET));
							orderParamMap.put("secondaryPaymentAmount",secondaryAmount);
							paymentMethodCount="TWO";
						}
						break;
					case WALLET_CREDITCARD :
						secondaryAmount = request.getParameter("cardAmount");
						primaryAmount= request.getParameter("walletAmount");
						if(primaryAmount==null || primaryAmount.isEmpty()){
							validationMsg ="Wallet amount is not found.";
						}else if(secondaryAmount==null || secondaryAmount.isEmpty()){
							validationMsg ="Credit card amount is not found.";
						}else{
							orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.CUSTOMER_WALLET));
							orderParamMap.put("primaryPaymentAmount",primaryAmount);
							orderParamMap.put("secondaryPaymentMethod",String.valueOf(PartialPaymentMethod.CREDITCARD));
							orderParamMap.put("secondaryPaymentAmount",secondaryAmount);
							paymentMethodCount="TWO";
						}
						break;
					case PARTIAL_REWARD_WALLET_CC :
						thirdAmount = request.getParameter("cardAmount");
						secondaryAmount = request.getParameter("walletAmount");
						rewardPoints =  request.getParameter("rewardPoints");
						primaryAmount = rewardPoints;
						if(primaryAmount==null || primaryAmount.isEmpty()){
							validationMsg ="Reward points amount is not found.";
						}else if(secondaryAmount==null || secondaryAmount.isEmpty()){
							validationMsg ="Wallet amount is not found.";
						}else if(thirdAmount==null || thirdAmount.isEmpty()){
							validationMsg ="Credit card amount is not found.";
						}else{
							orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.PARTIAL_REWARDS));
							orderParamMap.put("primaryPaymentAmount",primaryAmount);
							orderParamMap.put("secondaryPaymentMethod",String.valueOf(PartialPaymentMethod.CUSTOMER_WALLET));
							orderParamMap.put("secondaryPaymentAmount",secondaryAmount);
							orderParamMap.put("thirdPaymentMethod",String.valueOf(PartialPaymentMethod.CREDITCARD));
							orderParamMap.put("thirdPaymentAmount",thirdAmount);
							paymentMethodCount="THREE";
						}
						break;
					case ACCOUNT_RECIVABLE:
						primaryAmount = String.valueOf(orderTotal);
						orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.ACCOUNT_RECIVABLE));
						orderParamMap.put("primaryPaymentAmount","0.00");
						paymentMethodCount="ONE";
						break;
					}
										
					if(!validationMsg.isEmpty()){
						System.out.println(validationMsg);
						error.setDescription(validationMsg);
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					if(isLoyalFan!=null && !isLoyalFan.isEmpty()){
						if(isLoyalFan.equalsIgnoreCase("Yes")){
							orderParamMap.put("isFanPrice","true");
						}else{
							orderParamMap.put("isFanPrice","false");
						}
					}
					
					orderParamMap.put("eventId", String.valueOf(eventId));
					orderParamMap.put("categoryTicketGroupId", String.valueOf(ticketId));
					orderParamMap.put("orderTotal",String.valueOf(orderTotal));
					orderParamMap.put("paymentMethodCount",paymentMethodCount);
					orderParamMap.put("customerId", String.valueOf(customerId));
					if(rewardPoints!=null && !rewardPoints.isEmpty()){
						Double points = Double.parseDouble(rewardPoints);
						orderParamMap.put("rewardPoints", String.valueOf(points));
					}
					orderParamMap.put("sessionId", sessionIdStr);
					orderParamMap.put("billingAddressId", String.valueOf(bAddressId));
					orderParamMap.put("shippingAddressId", String.valueOf(sAddressId));
					orderParamMap.put("shippingSameAsBilling", shippingIsSameAsBilling);
					orderParamMap.put("referralCode", referalCode);
					orderParamMap.put("isLongSale","false");
					orderParamMap.put("orderType",orderType);
					
					data = Util.getObject(orderParamMap, sharedProperty.getApiUrl()+Constants.CREATE_ORDER_INITIATE);
					gson = new Gson();		
					jsonObject = gson.fromJson(data, JsonObject.class);
					OrderConfirmation orderConfirmation = gson.fromJson(((JsonObject)jsonObject.get("orderConfirmation")), OrderConfirmation.class);	
					
					if(orderConfirmation == null){
						System.out.println("Invoice creation failed not able to communicate with API Server.");
						error.setDescription("Invoice creation failed not able to communicate with API Server.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					if(orderConfirmation.getStatus() == 0){
						System.out.println("Could not create invoice, "+orderConfirmation.getError().getDescription());
						error.setDescription("Could not create invoice, "+orderConfirmation.getError().getDescription());
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					orderId = orderConfirmation.getOrderId();
					boolean isPaymentSuccess = false;
					
					switch(method){
						case CREDITCARD :
							transaction = doCreditCardTransaction(request,String.valueOf(orderTotal),null,false,null);
							if(transaction.getStatus()==0){
								validationMsg = "Credit Card Transaction - "+transaction.getError().getDescription();
								isPaymentSuccess = false;
							}else{
								isPaymentSuccess = true;
								primaryTrxId = transaction.getTransactionId();
								orderParamMap.put("primaryTransactionId",primaryTrxId);
							}
							break;
						case WALLET :
							walletTranx= doWalletTransaction(request, String.valueOf(orderTotal),orderId, userName);
							if(walletTranx.getStatus()==0){
								validationMsg ="Wallet Transaction - "+walletTranx.getError().getDescription();
								isPaymentSuccess = false;
							}else{
								isPaymentSuccess = true;
								primaryTrxId = walletTranx.getWalletHistory().getTransactionId();
								orderParamMap.put("primaryTransactionId",primaryTrxId);
								orderParamMap.put("walletTransactionId",String.valueOf(walletTranx.getWalletHistory().getId()));
							}
							break;
						case REWARDPOINTS :
							isPaymentSuccess = true;
							break;
						case PARTIAL_REWARD_CC :
							transaction = doCreditCardTransaction(request,secondaryAmount,null,false,null);
							if(transaction.getStatus()==0){
								validationMsg ="Credit Card Transaction - "+transaction.getError().getDescription();
								isPaymentSuccess = false;
							}else{
								isPaymentSuccess = true;
								secondaryTrxId = transaction.getTransactionId();
								orderParamMap.put("primaryTransactionId",primaryTrxId);
								orderParamMap.put("secondaryTransactionId",secondaryTrxId);
								paymentMethodCount="TWO";
							}
							break;
						case PARTIAL_REWARD_WALLET :
							walletTranx= doWalletTransaction(request,secondaryAmount,orderId, userName);
							if(walletTranx.getStatus()==0){
								validationMsg ="Wallet Transaction - "+walletTranx.getError().getDescription();
								isPaymentSuccess = false;
							}else{
								isPaymentSuccess = true;
								secondaryTrxId = walletTranx.getWalletHistory().getTransactionId();
								orderParamMap.put("primaryTransactionId",primaryTrxId);
								orderParamMap.put("secondaryTransactionId",secondaryTrxId);
								orderParamMap.put("walletTransactionId",String.valueOf(walletTranx.getWalletHistory().getId()));
							}
							break;
						case WALLET_CREDITCARD :
							walletTranx= doWalletTransaction(request,primaryAmount,orderId, userName);
							transaction = doCreditCardTransaction(request,secondaryAmount,null,false,null);
							if(walletTranx.getStatus()==0){
								validationMsg ="Wallet Transaction - "+walletTranx.getError().getDescription();
								isPaymentSuccess = false;
							}else if(transaction.getStatus()==0){
								isPaymentSuccess = false;
								validationMsg ="Credit Card Transaction - "+transaction.getError().getDescription();
							}else{
								isPaymentSuccess = true;
								secondaryTrxId = transaction.getTransactionId();
								primaryTrxId = walletTranx.getWalletHistory().getTransactionId();
								orderParamMap.put("primaryTransactionId",primaryTrxId);
								orderParamMap.put("secondaryTransactionId",secondaryTrxId);
								orderParamMap.put("walletTransactionId",String.valueOf(walletTranx.getWalletHistory().getId()));
							}
							break;
						case PARTIAL_REWARD_WALLET_CC :
							walletTranx = doWalletTransaction(request,secondaryAmount,orderId, userName);
							transaction = doCreditCardTransaction(request,thirdAmount,null,false,null);
							if(walletTranx.getStatus()==0){
								validationMsg = "Wallet Transaction - "+walletTranx.getError().getDescription();
								isPaymentSuccess = false;
							}else if(transaction.getStatus()==0){
								validationMsg ="Credit Card Transaction - "+transaction.getError().getDescription();
								isPaymentSuccess = false;
							}else{
								isPaymentSuccess = true;
								secondaryTrxId = walletTranx.getWalletHistory().getTransactionId();
								thirdTrxId = transaction.getTransactionId();
								orderParamMap.put("primaryTransactionId",primaryTrxId);
								orderParamMap.put("secondaryTransactionId",secondaryTrxId);
								orderParamMap.put("thirdTransactionId",thirdTrxId);
								orderParamMap.put("walletTransactionId",String.valueOf(walletTranx.getWalletHistory().getId()));
							}
							break;
						case ACCOUNT_RECIVABLE:
							isPaymentSuccess = true;
							orderParamMap.put("primaryTransactionId","dummytransactionid"+orderConfirmation.getOrderId());
							break;
					}
					if(!validationMsg.isEmpty()){
						System.out.println(validationMsg);
						error.setDescription(validationMsg);
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					
					orderParamMap.put("orderId", String.valueOf(orderId));
					orderParamMap.put("isPaymentSuccess",String.valueOf(isPaymentSuccess));
					
					
					data = Util.getObject(orderParamMap, sharedProperty.getApiUrl()+Constants.CREATE_ORDER_CONFIRM);
					gson = new Gson();		
					jsonObject = gson.fromJson(data, JsonObject.class);
					OrderConfirmation orderConfirmation1 = gson.fromJson(((JsonObject)jsonObject.get("orderConfirmation")), OrderConfirmation.class);	
					
					if(orderConfirmation1 == null){
						System.out.println("Order confirmation failed not able to communicate with API Server.");
						error.setDescription("Order confirmation failed not able to communicate with API Server.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					if(orderConfirmation1.getStatus() == 0){
						System.out.println("Order Confirmation Failed, "+orderConfirmation1.getError().getDescription());
						error.setDescription("Order Confirmation Failed, "+orderConfirmation1.getError().getDescription());
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					
					Customer customer = DAORegistry.getCustomerDAO().get(Integer.parseInt(customerId));
					if(!customer.getProductType().equals(ProductType.REWARDTHEFAN) && customer.getEmail()!=null && !customer.getEmail().isEmpty()
							&& (customer.getIsRegistrationMailSent()==null || customer.getIsRegistrationMailSent() == false)){
						customer.setApplicationPlatForm(ApplicationPlatform.DESKTOP_SITE);
						customer.setProductType(ProductType.REWARDTHEFAN);
						customer.setSignupType(SignupType.REWARDTHEFAN);
						customer.setIsRegistrationMailSent(true);
						customer.setSignupDate(new Date());
						customer.setUserId(Util.generatedCustomerUserId(customer.getCustomerName(), customer.getLastName()));
						//customer.setReferrerCode(Util.generateCustomerReferalCode());
						customer.setReferrerCode(customer.getEmail());
						String password = Util.generatePassword(customer.getCustomerName(), "RTF");
						customer.setPassword(Util.generateEncrptedPassword(password));
						
						DAORegistry.getCustomerDAO().update(customer);
						
						Map<String, Object> mailMap = new HashMap<String, Object>();
						mailMap.put("customerName", customer.getCustomerName()+" "+customer.getLastName());
						mailMap.put("password", password);
						mailMap.put("userName", customer.getUserName());
						//mailMap.put("androidUrl","https://play.google.com/store/apps/details?id=com.rewardthefan&hl=en");
						//mailMap.put("iosUrl","https://itunes.apple.com/us/app/reward-the-fan/id1140367203?ls=1&mt=8");
						com.rtw.tracker.mail.MailAttachment[] mailAttachment = new com.rtw.tracker.mail.MailAttachment[8];
						mailAttachment[0] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","tyrtf.png",com.rtw.tmat.utils.Util.getFilePath(request, "tyrtf.png", "/resources/images/"));
						mailAttachment[1] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","blue.png",com.rtw.tmat.utils.Util.getFilePath(request, "blue.png", "/resources/images/"));
						mailAttachment[2] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","fb1.png",com.rtw.tmat.utils.Util.getFilePath(request, "fb1.png", "/resources/images/"));
						mailAttachment[3] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","ig1.png",com.rtw.tmat.utils.Util.getFilePath(request, "ig1.png", "/resources/images/"));
						mailAttachment[4] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","li1.png",com.rtw.tmat.utils.Util.getFilePath(request, "li1.png", "/resources/images/"));
						mailAttachment[5] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","rtflogot.png",com.rtw.tmat.utils.Util.getFilePath(request, "rtflogot.png", "/resources/images/"));
						mailAttachment[6] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","p1.png",com.rtw.tmat.utils.Util.getFilePath(request, "p1.png", "/resources/images/"));
						mailAttachment[7] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","tw1.png",com.rtw.tmat.utils.Util.getFilePath(request, "tw1.png", "/resources/images/"));
						
						
						String email = customer.getEmail();
						//email = "msanghani@rightthisway.com";
						String bcc ="";
						bcc="amit.raut@rightthisway.com,msanghani@righthtisway.com,kulaganathan@rightthisway.com";
						try {
							mailManager.sendMailNow("text/html","sales@rewardthefan.com",email, 
									null,bcc, "You are successfully registered with RewardTheFan.com.",
						    		"mail-rewardfan-customer-registration.html", mailMap, "text/html", null,mailAttachment,null);
						} catch (Exception e) {
							e.printStackTrace();
							error.setDescription("Error occured while sending mail for Order Confirmation to customer.");
							genericResponseDTO.setError(error);
							genericResponseDTO.setStatus(0);
							return genericResponseDTO;
						}
					}
					
					Invoice invoice = DAORegistry.getInvoiceDAO().getInvoiceByOrderId(orderConfirmation1.getOrderId());
					invoiceId = invoice.getId();
					orderId = orderConfirmation.getOrderId();
					CustomerOrder order = DAORegistry.getCustomerOrderDAO().get(orderId);
					if(order.getPrimaryPaymentMethod().equals(PaymentMethod.ACCOUNT_RECIVABLE)){
						invoice.setInvoiceType(PaymentMethod.ACCOUNT_RECIVABLE.toString());
					}
					if(order.getOrderType().equals(OrderType.CONTEST)){
						invoice.setInvoiceType(OrderType.CONTEST.toString());
					}
					DAORegistry.getInvoiceDAO().update(invoice);
					genericResponseDTO.setMessage("Order created successfully, InvoiceId : "+invoiceId);
					genericResponseDTO.setStatus(1);
					
					
					//Tracking User Action
					String userActionMsg = "Invoice Created Successfully.";
					Util.userActionAudit(request, invoiceId, userActionMsg);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Creating Invoice.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
	
	
	public StripeTransaction doCreditCardTransaction(HttpServletRequest request,String transactionAmount,String ticketIdStr,Boolean isLongOrder,Integer quantity){
		StripeTransaction transaction = new StripeTransaction();
		com.rtw.tmat.utils.Error error = new Error();
		String sTempToken = request.getParameter("sTempToken");
		String sBySavedCard = request.getParameter("sBySavedCard");
		String sSavedCardId = request.getParameter("sSavedCardId");
		String customerId = request.getParameter("customerId");
		String eventId = request.getParameter("eventId");
		if(ticketIdStr==null || ticketIdStr.isEmpty()){
			ticketIdStr = request.getParameter("ticketId_0");
		}
		Map<String, String> paramterMap = null;
		Gson gson = new Gson();
		JsonObject jsonObject = null;		 
		try {
			if(TextUtil.isEmptyOrNull(sBySavedCard)){
				error.setDescription("Not able to indentify weather to use Saved card or new one.");
				transaction.setError(error);
				transaction.setStatus(0);
				return transaction;
			}
			paramterMap = Util.getParameterMap(request);
			paramterMap.put("transactionAmount",transactionAmount);
			if(sBySavedCard.equals("YES")){
				if(TextUtil.isEmptyOrNull(sSavedCardId)){
					error.setDescription("Not able to indentify saved credit card number for payment.");
					transaction.setError(error);
					transaction.setStatus(0);
					return transaction;
				}
				paramterMap.put("trxBySavedCard", "true");
				paramterMap.put("custCardId", sSavedCardId);
			}else{
				if(TextUtil.isEmptyOrNull(sTempToken)){
					error.setDescription("Not able to generate Stripe Transaction Token.");
					transaction.setError(error);
					transaction.setStatus(0);
					return transaction;
				}
				paramterMap.put("trxBySavedCard", "false");
				paramterMap.put("token", sTempToken);
				paramterMap.put("saveCustCard", "true");
			}
			paramterMap.put("paymentMethod", "CREDITCARD");
			paramterMap.put("rtfCustId", customerId);
			paramterMap.put("description", "Reward The Fan");
			
			String data = "";
			try {
				if(isLongOrder){
					paramterMap.put("tGroupRefIds",ticketIdStr);
					paramterMap.put("quantity",String.valueOf(quantity));
					paramterMap.put("eventId",eventId);
					data = Util.getObject(paramterMap, sharedProperty.getApiUrl()+Constants.STRIPE_TRANSACTION_REAL_TICKET);
				}else{
					paramterMap.put("tGroupId",ticketIdStr);
					data = Util.getObject(paramterMap, sharedProperty.getApiUrl()+Constants.STRIPE_TRANSACTION);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				transaction = new StripeTransaction();
				error.setDescription("Failed while creating stripe transaction, Failed to communicate with our API server.");
				transaction.setError(error);
				transaction.setStatus(0);
				return transaction;
			}
			
			gson = new Gson();		
			jsonObject = gson.fromJson(data, JsonObject.class);
			transaction = gson.fromJson(((JsonObject)jsonObject.get("stripeTransaction")), StripeTransaction.class);
			if(transaction==null){
				transaction = new StripeTransaction();
				error.setDescription("Failed while creating stripe transaction, Failed to communicate with our API server.");
				transaction.setError(error);
				transaction.setStatus(0);
				return transaction;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return transaction;
	}
	
	public WalletTransaction doWalletTransaction(HttpServletRequest request,String amount,Integer orderId, String userName){
		//String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		String customerId = request.getParameter("customerId");
		WalletTransaction walletTransaction = new WalletTransaction();
		Error error = new Error();
		try {
			Map<String, String> paramMap = Util.getParameterMap(request);
			paramMap.put("transactionAmount",amount);
			paramMap.put("customerId",customerId);
			paramMap.put("orderId",String.valueOf(orderId));
			paramMap.put("transactionType","DEBIT");
			paramMap.put("userName",userName);
			String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.CUSTOMER_WALLET_TRANSACTION);
			Gson gson = new Gson();
			JsonObject cardJsonObject = gson.fromJson(data, JsonObject.class);
			walletTransaction = gson.fromJson(((JsonObject)cardJsonObject.get("walletTransaction")), WalletTransaction.class);
			if(walletTransaction==null){
				walletTransaction = new WalletTransaction();
				error.setDescription("Failed to create wallet transaction, Failed to communicate with API server.");
				walletTransaction.setStatus(0);
				walletTransaction.setError(error);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return walletTransaction;
	}
	
	@RequestMapping(value = "/GetShippingAddress")
	public ShippingAddressDTO getShippingAddress(HttpServletRequest request, HttpServletResponse response){
		ShippingAddressDTO shippingAddressDTO = new ShippingAddressDTO();
		Error error = new Error();
		
		try{
			String customerIdStr = request.getParameter("custId");
			
			if(StringUtils.isEmpty(customerIdStr)){				
				System.err.println("Please select Customer.");
				error.setDescription("Please select Customer.");
				shippingAddressDTO.setError(error);
				shippingAddressDTO.setStatus(0);
				return shippingAddressDTO;
			}
			
			Integer customerId = Integer.parseInt(customerIdStr);
			
			List<CustomerAddress> shippingAddressList = null;
			int shippingAddressCount = 0;			
			shippingAddressList = DAORegistry.getCustomerAddressDAO().getShippingAddressInfo(customerId);
			shippingAddressCount = DAORegistry.getCustomerAddressDAO().getShippingAddressCount(customerId);
			
			if(shippingAddressList.size() ==0){
				System.err.println("No Shipping address found for selected Customer.");
				error.setDescription("No Shipping address found for selected Customer.");
				shippingAddressDTO.setError(error);
				shippingAddressDTO.setStatus(0);
				return shippingAddressDTO;
			}
			
			shippingAddressDTO.setStatus(1);
			shippingAddressDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(shippingAddressCount));
			shippingAddressDTO.setShippingAddressDTOs(com.rtw.tracker.utils.Util.getShippingAddressArray(shippingAddressList));
			shippingAddressDTO.setShippingAddressCount(shippingAddressCount);
			shippingAddressDTO.setCustomerId(customerIdStr);
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Shipping Addresses.");
			shippingAddressDTO.setError(error);
			shippingAddressDTO.setStatus(0);
		}
		return shippingAddressDTO;
	}
	
	/**
	 * Save shipping address
	 * @param addShippingAddress
	 * @param request
	 * @param response
	 * @param map
	 * @param session
	 * @return
	 *//*
	@RequestMapping(value = "/AddShippingAddress", method=RequestMethod.POST)
	public String saveShippingAddress(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		LOGGER.info("Save shipping address..");
		String returnMessage = "";
		String customerId = "";
		String shippingAddressId = "";
		try{
			customerId = request.getParameter("custId");
			shippingAddressId = request.getParameter("shippingAddrId");
			String firstName = request.getParameter("shFirstName");
			String lastName = request.getParameter("shLastName");
			String street1 = request.getParameter("shAddressLine1");
			String street2 = request.getParameter("shAddressLine2");
			String country = request.getParameter("shCountryName");
			String state = request.getParameter("shStateName");
			String city = request.getParameter("shCity");
			String zipCode = request.getParameter("shZipCode");
			String action = request.getParameter("action");
			if(action != null && action.equals("addShippingAddress")){
//				if(addShippingAddress.getFirstName() == null || addShippingAddress.getFirstName().isEmpty()){
//					returnMessage = "Firstname can't be blank";
//				}
				CustomerAddress shippingAddress = new CustomerAddress();
				if(returnMessage.isEmpty()){

					Country countryObj1 = DAORegistry.getCountryDAO().get(Integer.parseInt(country.trim()));
					State stateObj1 =  DAORegistry.getStateDAO().get(Integer.parseInt(state.trim()));
					
					shippingAddress.setFirstName(firstName);
					shippingAddress.setLastName(lastName);
					shippingAddress.setAddressLine1(street1);
					shippingAddress.setAddressLine2(street2);
					shippingAddress.setCountry(countryObj1);
					shippingAddress.setState(stateObj1);
					shippingAddress.setCity(city);
					shippingAddress.setZipCode(zipCode);
					shippingAddress.setCustomerId(Integer.parseInt(customerId));
					shippingAddress.setAddressType(AddressType.SHIPPING_ADDRESS);
					
					CustomerAddress updateAddress = null;
					if(shippingAddressId != null && !shippingAddressId.isEmpty()){
						updateAddress = DAORegistry.getCustomerAddressDAO().get(Integer.parseInt(shippingAddressId));
					}
					
					if(updateAddress != null){
						shippingAddress.setId(updateAddress.getId());
						shippingAddress.setFirstName(firstName);
						shippingAddress.setLastName(lastName);
						shippingAddress.setAddressLine1(street1);
						shippingAddress.setAddressLine2(street2);
						shippingAddress.setCountry(countryObj1);
						shippingAddress.setState(stateObj1);
						shippingAddress.setCity(city);
						shippingAddress.setZipCode(zipCode);
						shippingAddress.setCustomerId(Integer.parseInt(customerId));
						shippingAddress.setAddressType(AddressType.SHIPPING_ADDRESS);
						DAORegistry.getCustomerAddressDAO().update(shippingAddress);
					}else{						
						DAORegistry.getCustomerAddressDAO().save(shippingAddress);
					}
					returnMessage = "Shipping address added successfully";
					map.put("successMessage", returnMessage);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			map.put("errorMessage", "There is something wrong..Please Try Again.");
		}
		return "redirect:getShippingAddress?custId="+customerId;
	}
	*/
	
	@RequestMapping("/HoldTicket")
	public String holdTickets(HttpServletRequest request, HttpServletResponse response,ModelMap map){
		
		Integer brokerId = StringUtils.isNotEmpty(request.getParameter("brokerId")) ? Integer.parseInt(request.getParameter("brokerId")) : 0;
		
		
		String message = new String();
		TicketListDTO ticketListDTO = new TicketListDTO();
		map.put("ticketListDTO", ticketListDTO);
		try {
			String ticketGroupId = request.getParameter("ticketGroupId");
			Integer tgId = 0;
			String action = request.getParameter("action");
			
			if(StringUtils.isNotEmpty(action) && StringUtils.isNotEmpty(ticketGroupId)){
				String arr[] = null;
				//String userName = SecurityContextHolder.getContext().getAuthentication().getName();
				DateFormat formatDateTime = new SimpleDateFormat("MM/dd/yyyy");
				String customerIdStr = request.getParameter("customerId");
				String ids = request.getParameter("ticketIds");
				String salesPrice = request.getParameter("salesPrice");
				String expDate = request.getParameter("expiryDate");
				String expMinutes = request.getParameter("expiryMinutes");
				String userName = request.getParameter("userName");
				
				if(ids !=null && !ids.isEmpty()){
					if(ids.contains(",")){
						arr = ids.split(",");
					}else{
						arr = new String[1];
						arr[0] = ids;
					}
				}
				List<Integer> list = new ArrayList<Integer>();
				if(arr != null){
					for(String str : arr){
						if(!str.trim().isEmpty()){
							list.add(Integer.parseInt(str));
						}
					}
				}
				
				List<Ticket> tickets = DAORegistry.getTicketDAO().getTicketsByTicketIds(list);
				if(action.equalsIgnoreCase("HOLD")){
					if(customerIdStr!=null && !customerIdStr.isEmpty() && salesPrice!=null && !salesPrice.isEmpty()
							&& ((expDate!=null && !expDate.isEmpty()) || (expMinutes!=null && !expMinutes.isEmpty()))){
						List<CustomerAddress> address = DAORegistry.getCustomerAddressDAO().getShippingAddressInfo(Integer.parseInt(customerIdStr));
						Calendar cal = Calendar.getInstance();
						for(Ticket tic : tickets){
							tic.setTicketStatus(TicketStatus.ONHOLD);
							tic.setActualSoldPrice(Double.parseDouble(salesPrice));
							tgId = tic.getTicketGroupId();
						}
						InventoryHold hold = new InventoryHold();
						if(!address.isEmpty()){
							hold.setAddressId(address.get(0).getId());
						}
						hold.setcSR(userName);
						hold.setStatus(TicketStatus.ACTIVE.toString());
						hold.setTicketGroupId(tgId);
						hold.setCustomerId(Integer.parseInt(customerIdStr));
						hold.setHoldDateTime(cal.getTime());
						hold.setSalePrice(Double.parseDouble(salesPrice));
						hold.setBrokerId(brokerId);
						if(expDate!=null && !expDate.isEmpty()){
							Date date = formatDateTime.parse(expDate);
							if(!DateUtils.isSameDay(date, cal.getTime())){
								cal.setTime(date);
							}
						}
						if(expMinutes!=null && !expMinutes.isEmpty()){
							cal.add(Calendar.MINUTE, Integer.parseInt(expMinutes));
							hold.setExpirationMinutes(Integer.parseInt(expMinutes));
						}else{
							hold.setExpirationMinutes(null);
						}
						hold.setExpirationDate(cal.getTime());
						hold.setTicketIds(ids);
						//hold.setShippingMethodId(shippingMethodId)
						
						DAORegistry.getInventoryHoldDAO().save(hold);
						DAORegistry.getTicketDAO().updateAll(tickets);
						message = "Ticket Ids "+ids+" are successfully added on Hold.";
						//map.put("successMessage", "Ticket Ids "+ids+" are successfully added on Hold.");
						
						//Tracking User Action
						Util.userActionAudit(request, null, "Ticket(s) are Added on Hold. Ticket Id's - "+ids);
					}else{
						message = "CustomerId, ExpiryDate, expiryMinutes or salesPrice is not found.";
						//map.put("errorMessage", "CustomerId, ExpiryDate, expiryMinutes or salesPrice is not found.");
					}
				}else if(action.equalsIgnoreCase("UNHOLD")){
					String ticketId = null;
					for(Ticket tic : tickets){
						tic.setTicketStatus(TicketStatus.ACTIVE);
						tic.setActualSoldPrice(0.00);
						tgId = tic.getTicketGroupId();
						ticketId = String.valueOf(tic.getId());
					}
					List<InventoryHold> holds = DAORegistry.getInventoryHoldDAO().getInventoryHoldByTicketGroupId(tgId);
					InventoryHold hold = null;
					if(holds.size()==1){
						hold = holds.get(0);
					}else{
						for(InventoryHold h : holds){
							if(h.getTicketIds().contains(",")){
								String id[] = h.getTicketIds().split(",");
								for(String str : id){
									if(ticketId.equalsIgnoreCase(str)){
										hold = h;
										break;
									}
								}
							}else{
								if(ticketId.equalsIgnoreCase(h.getTicketIds())){
									hold = h;
									break;
								}
							}
							if(hold!=null){
								break;
							}
						}
					}
					hold.setStatus("DELETED");
					DAORegistry.getInventoryHoldDAO().update(hold);
					DAORegistry.getTicketDAO().updateAll(tickets);
					message = "Ticket Ids "+ids+" are successfully removed from Hold.";
					//map.put("successMessage", "Ticket Ids "+ids+" are successfully removed from Hold.");
					
					//Tracking User Action
					Util.userActionAudit(request, null, "Ticket(s) are removed from Hold.Ticket Id's - "+ids);
				}
				
			}
			if(ticketGroupId !=null && !ticketGroupId.isEmpty()){
				TicketGroup tg = DAORegistry.getTicketGroupDAO().get(Integer.parseInt(ticketGroupId));
				List<Ticket> tickets = DAORegistry.getTicketDAO().getAllTicketsByTicketGroupId(tg.getId());
				if(tickets.isEmpty()){
					message = "No ticket found for selected Ticket group.";
					//map.put("successMessage", "No ticket found for selected Ticket group.");
				}
				ticketListDTO.setTickets(com.rtw.tracker.utils.Util.getHoldTickets(tickets, tg));
				ticketListDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(tickets!=null?tickets.size():0));
				/*map.put("ticketGroupId",ticketGroupId);
				map.put("tickets", JsonWrapperUtil.getTicketArray(tickets,tg));
				map.put("pagingInfo",PaginationUtil.getDummyPaginationParameter(tickets!=null?tickets.size():0));*/
			}else{
				message = "Ticket Group Id is not found.";
				//map.put("successMessage", "Ticket Group Id is not found.");
			}
			ticketListDTO.setStatus(1);
			ticketListDTO.setMessage(message);
		} catch (Exception e) {
			com.rtw.tmat.utils.Error error = new com.rtw.tmat.utils.Error();
			e.printStackTrace();
			System.err.println(e.getLocalizedMessage());
			error.setDescription("Error occured while getting events.");
			ticketListDTO.setError(error);
			ticketListDTO.setStatus(0);
		}
		return "page-hold-ticket";
	}
		

	@RequestMapping("/UnHoldTicket")
	public GenericResponseDTO unHoldTickets(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		
		try {			
			String action = request.getParameter("action");			
			String ticketGroupId = request.getParameter("ticketGroupId");
			String brokerIdStr = request.getParameter("brokerId");
			
			if(StringUtils.isEmpty(ticketGroupId)){
				System.err.println("Please choose Ticket to UnHold.");
				error.setDescription("Please choose Ticket to UnHold.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide BrokerId");
				error.setDescription("Please provide BrokerId");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}			
			Integer brokerId = 0;	
			try{
				brokerId = Integer.parseInt(brokerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid BrokerId");
				error.setDescription("Please provide valid BrokerId");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			Integer tgId = 0;
			if(action!=null && ticketGroupId!=null){
				String arr[] = null;				
				String ids = request.getParameter("ticketIds");				
				if(ids !=null && !ids.isEmpty()){
					if(ids.contains(",")){
						arr = ids.split(",");
					}else{
						arr = new String[1];
						arr[0] = ids;
					}
				}
				List<Integer> list = new ArrayList<Integer>();
				for(String str : arr){
					if(!str.trim().isEmpty()){
						list.add(Integer.parseInt(str));
					}
				}
				List<Ticket> tickets = DAORegistry.getTicketDAO().getTicketsByTicketIds(list);
				if(action.equalsIgnoreCase("UNHOLD")){
					String ticketId = null;
					for(Ticket tic : tickets){
						tic.setTicketStatus(TicketStatus.ACTIVE);
						tic.setActualSoldPrice(0.00);
						tgId = tic.getTicketGroupId();
						ticketId = String.valueOf(tic.getId());
					}
					List<InventoryHold> holds = DAORegistry.getInventoryHoldDAO().getInventoryHoldByTicketGroupId(tgId);
					InventoryHold hold = null;
					if(holds.size()==1){
						hold = holds.get(0);
					}else{
						for(InventoryHold h : holds){
							if(h.getTicketIds().contains(",")){
								String id[] = h.getTicketIds().split(",");
								for(String str : id){
									if(ticketId.equalsIgnoreCase(str)){
										hold = h;
										break;
									}
								}
							}else{
								if(ticketId.equalsIgnoreCase(h.getTicketIds())){
									hold = h;
									break;
								}
							}
							if(hold!=null){
								break;
							}
						}
					}
					hold.setStatus("DELETED");
					DAORegistry.getInventoryHoldDAO().update(hold);
					DAORegistry.getTicketDAO().updateAll(tickets);
					
					genericResponseDTO.setStatus(1);
					genericResponseDTO.setMessage("Ticket Ids "+ids+" are successfully removed from Hold.");
					
					//Tracking User Action
					String userActionMsg = "Ticket(s) are Removed from Hold. Ticket Id's - "+ids;
					Util.userActionAudit(request, null, userActionMsg);
				}				
			}			
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while UnHold Ticket(s).");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
	
	@RequestMapping(value = "/SaveInvoiceRealTicket")
	public GenericResponseDTO saveInvoiceRealTicket(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		
		try {
			String action = request.getParameter("action");
			String customerId = request.getParameter("customerId");
			String userName = request.getParameter("userName");
			String brokerIdStr = request.getParameter("brokerId");			
			String clientIpAddress = request.getParameter("clientIPAddress");
			String sessionIdStr = request.getParameter("deviceId");
			String orderType = request.getParameter("orderType");
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide BrokerId");
				error.setDescription("Please provide BrokerId");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}			
			Integer brokerId = 0;	
			try{
				brokerId = Integer.parseInt(brokerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid BrokerId");
				error.setDescription("Please provide valid BrokerId");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			String returnMessage = "";
			
			Integer orderId=null;
			List<TicketGroup> ticketGroups = new ArrayList<TicketGroup>();
			List<TicketGroup> dummyTGList = new ArrayList<TicketGroup>();
			List<OrderTicketGroupDetails> orderTickets = new ArrayList<OrderTicketGroupDetails>();
			
			if(returnMessage.isEmpty()){
				
				if(action != null && action.equals("saveInvoice")){				
					
					String referalCode =request.getParameter("referralCode"); 
					String eventIdStr = request.getParameter("eventId");
					String addressLine1 = request.getParameter("addressLine1");
					String addressLine2 = request.getParameter("addressLine2");
					String shAddressLine1 = request.getParameter("shAddressLine1");
					String shAddressLine2 = request.getParameter("shAddressLine2");
					String isLoyalFan = request.getParameter("isLoyalFanPrice");
					String bAddressId =request.getParameter("bAddressId"); 
					String sAddressId =request.getParameter("sAddressId");
					String invoiceType = request.getParameter("createInvoiceType");
					String ticketSizeStr = request.getParameter("ticketSize");
					String promoTrackingId = request.getParameter("promoTrackingId");
					String affPromoTrackingId = request.getParameter("affPromoTrackingId");
					
					String ticketIds = request.getParameter("ticketIdStr");
					
					if(StringUtils.isEmpty(eventIdStr)){
						System.err.println("Not able to identify Event Information.");
						error.setDescription("Not able to identify Event Information.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}				
					Integer eventId = null;
					try {
						eventId = Integer.parseInt(eventIdStr);
					} catch (Exception e) {
						e.printStackTrace();
						error.setDescription("Invalid Event information found.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					

					if(StringUtils.isEmpty(orderType)){
						System.err.println("Please send order type.");
						error.setDescription("Please send order type.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					
					if(StringUtils.isEmpty(bAddressId)){				
						System.err.println("Billing Address Not found, Please select Billing Address.");
						error.setDescription("Billing Address Not found, Please select Billing Address.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					if(StringUtils.isEmpty(sAddressId)){				
						System.err.println("Shipping Address Not found, Please select Shipping Address.");
						error.setDescription("Shipping Address Not found, Please select Shipping Address.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					if((addressLine1==null || addressLine1.isEmpty()) && (addressLine2==null || addressLine2.isEmpty())){
						System.err.println("Selected billing address does not have either AddressLine1 or AddressLine2.");
						error.setDescription("Selected billing address does not have either AddressLine1 or AddressLine2.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					if((shAddressLine1==null || shAddressLine1.isEmpty()) && (shAddressLine2==null || shAddressLine2.isEmpty())){
						System.err.println("Selected shipping address does not have either AddressLine1 or AddressLine2.");
						error.setDescription("Selected shipping address does not have either AddressLine1 or AddressLine2.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					
					if(StringUtils.isEmpty(ticketSizeStr)){				
						System.err.println("Not able to identify Total No of Tickets.");
						error.setDescription("Not able to identify Total No of Tickets.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					Integer ticketSize = null;
					try {
						ticketSize = Integer.parseInt(ticketSizeStr);
					} catch (Exception e) {
						e.printStackTrace();
						error.setDescription("Bad values found for Total No of Tickets.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					
					Double orderTotal = 0.0;
					Integer orderTotalQty = 0;
					Double totalTaxAmout = 0.00;
					for(int i=0;i<ticketSize;i++){
						String ticketCountStr = request.getParameter("ticketCount_"+i);
						String ticketIdStr = request.getParameter("ticketId_"+i);
						String soldPrice = request.getParameter("soldPrice_"+i);
						String taxAmountStr = request.getParameter("taxAmount_"+i);
	
						Double taxAmount = 0.0;
						if(taxAmountStr != null && !taxAmountStr.isEmpty() ) {
							try {
								taxAmount  = Double.parseDouble(taxAmountStr);
							} catch (Exception e) {
								error.setDescription("Invaid Fees Amount.");
								genericResponseDTO.setError(error);
								genericResponseDTO.setStatus(0);
								return genericResponseDTO;
							}
						}
						totalTaxAmout += taxAmount;
						if(ticketIdStr == null || ticketIdStr.trim().isEmpty()) {
							System.err.println("Not able to identify Ticket Information, Please refresh page and try again.");
							error.setDescription("Not able to identify Ticket Information, Please refresh page and try again.");
							genericResponseDTO.setError(error);
							genericResponseDTO.setStatus(0);
							return genericResponseDTO;
						}
						Integer tgId = null;
						try {
							tgId = Integer.parseInt(ticketIdStr);
						} catch (Exception e) {
							e.printStackTrace();
							error.setDescription("Bad value found for Ticket Id, It should be valid integer value.");
							genericResponseDTO.setError(error);
							genericResponseDTO.setStatus(0);
							return genericResponseDTO;
						}
						
						if(ticketCountStr == null || ticketCountStr.trim().isEmpty()) {
							System.err.println("Not able to identify Ticket Quantity, Please refresh page and try again.");
							error.setDescription("Not able to identify Ticket Quantity, Please refresh page and try again.");
							genericResponseDTO.setError(error);
							genericResponseDTO.setStatus(0);
							return genericResponseDTO;
						}
						Integer ticketCount = null;
						try {
							ticketCount = Integer.parseInt(ticketCountStr);
						} catch (Exception e) {
							e.printStackTrace();
							error.setDescription("Bad value found for Ticket Quantity, It should be valid integer value.");
							genericResponseDTO.setError(error);
							genericResponseDTO.setStatus(0);
							return genericResponseDTO;
						}
						
						if(soldPrice == null || soldPrice.trim().isEmpty()) {
							System.err.println("Sold Price cannot be empty, Please add valid sold price.");
							error.setDescription("Sold Price cannot be empty, Please add valid sold price.");
							genericResponseDTO.setError(error);
							genericResponseDTO.setStatus(0);
							return genericResponseDTO;
						}
						Double ticketSoldPrice = null;
						try {
							ticketSoldPrice= Double.parseDouble(soldPrice);
						} catch (Exception e) {
							e.printStackTrace();
							error.setDescription("Bad value for Sold Price found, It should be valid Double value.");
							genericResponseDTO.setError(error);
							genericResponseDTO.setStatus(0);
							return genericResponseDTO;
						}
						
						TicketGroup tg = new TicketGroup();
						tg.setId(tgId);
						tg.setQuantity(ticketCount);
						tg.setPrice(ticketSoldPrice);
						dummyTGList.add(tg);
						Double total = (tg.getPrice() * tg.getQuantity()) + Util.getBrokerServiceFees(brokerId, tg.getPrice(), tg.getQuantity(), taxAmount);
						orderTotal += Util.getRoundedValue(total);
						orderTotalQty += tg.getQuantity();
						
					}
					CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(Integer.parseInt(customerId));
					if(customerLoyalty==null){
						customerLoyalty = new CustomerLoyalty();
						customerLoyalty.setActivePoints(0.00);
						customerLoyalty.setActiveRewardDollers(0.00);
						customerLoyalty.setCustomerId(Integer.parseInt(customerId));
						customerLoyalty.setLastUpdate(new Date());
						customerLoyalty.setLatestEarnedPoints(0.00);
						customerLoyalty.setLatestSpentPoints(0.00);
						customerLoyalty.setPendingPoints(0.00);
						customerLoyalty.setRevertedSpentPoints(0.00);
						customerLoyalty.setTotalEarnedPoints(0.00);
						customerLoyalty.setTotalSpentPoints(0.00);
						customerLoyalty.setVoidedRewardPoints(0.00);
						DAORegistry.getCustomerLoyaltyDAO().save(customerLoyalty);
					}
					
					String msg = "";
					if(referalCode!=null && !referalCode.isEmpty()){
						System.out.println("Applied promotional/referral code : "+referalCode);
						
						Map<String, String> paramMap = Util.getParameterMap(request);
						paramMap.put("referralCode", referalCode);
						paramMap.put("customerId",customerId);
						paramMap.put("eventId",String.valueOf(eventId));
						paramMap.put("tgId",ticketIds);
						paramMap.put("clientIPAddress", clientIpAddress);
						paramMap.put("isLongSale", "true");
						paramMap.put("orderTotal",String.valueOf(orderTotal));
						if(isLoyalFan.equalsIgnoreCase("Yes")){
							paramMap.put("isFanPrice","true");
						}else{
							paramMap.put("isFanPrice","false");
						}
						paramMap.put("sessionId", sessionIdStr);
						paramMap.put("orderQty",String.valueOf(orderTotalQty));
						
					    String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.VALIDATE_PROMOCODE_REAL_TICKET);
					    Gson gson = new Gson();  
						JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
						ReferralCodeValidation referralCodeValidation = gson.fromJson(((JsonObject)jsonObject.get("referralCodeValidation")), ReferralCodeValidation.class);
						
						if(referralCodeValidation == null){
							System.err.println("Failed while validating referral code, Failed to communicate with our API server");
							error.setDescription("Failed while validating referral code, Failed to communicate with our API server");
							genericResponseDTO.setError(error);
							genericResponseDTO.setStatus(0);
							return genericResponseDTO;
						}
						if(referralCodeValidation.getStatus()==1){
							msg = "REFERRALCODEVALIDATED";
						}else if(referralCodeValidation.getStatus()==0){
							msg = referralCodeValidation.getError().getDescription();
							System.err.println("Referral Code Validation: "+msg);
							error.setDescription(msg);
							genericResponseDTO.setError(error);
							genericResponseDTO.setStatus(0);
							return genericResponseDTO;
						}
					}
					for(TicketGroup tg : dummyTGList){
						TicketGroup ticGroup = null;
						ticGroup = DAORegistry.getTicketGroupDAO().getAllTicketGroupById(tg.getId());
						ticketGroups.add(ticGroup);
						if(ticGroup.getShippingMethodId()== null || ticGroup.getShippingMethodId()<0){
							ticGroup.setShippingMethod("E-Ticket");
							ticGroup.setShippingMethodId(1);
						}
						String zone = Util.computZoneForTickets(eventId, ticGroup.getSection());
						if(zone==null || zone.isEmpty()){
							zone = "N/A";
						}
						OrderTicketGroupDetails orderTicket = new OrderTicketGroupDetails();
						orderTicket.setZone(zone);
						orderTicket.setQuantity(tg.getQuantity());
						orderTicket.setSection(ticGroup.getSection());
						orderTicket.setRow(ticGroup.getRow());
						orderTicket.setTicketGroupId(ticGroup.getId());
						orderTicket.setSoldPrice(tg.getPrice());
						orderTicket.setActualPrice(ticGroup.getPrice());
						orderTicket.setOrderId(-1);
						
						List<Ticket> tickets = DAORegistry.getTicketDAO().getAvailableTicketsByTicketGroupId(tg.getId());
						if(tickets.size() < tg.getQuantity()){
							System.err.println("Given ticket group quantity is greater than available quantity, Please enter valid quantity.");
							error.setDescription("Given ticket group quantity is greater than available quantity, Please enter valid quantity.");
							genericResponseDTO.setError(error);
							genericResponseDTO.setStatus(0);
							return genericResponseDTO;
						}
						if(tg.getQuantity() == 1){
							orderTicket.setSeat(tickets.get(0).getSeatNo());
						}else{
							orderTicket.setSeat(tickets.get(0).getSeatNo()+"-"+tickets.get(tg.getQuantity()-1).getSeatNo());
						}
						orderTickets.add(orderTicket);
						//orderParamMap.put(String.valueOf(orderTicket.getId()),String.valueOf(orderTicket.getQuantity())+"_"+String.valueOf(tg.getPrice()));
					}
					
					String shippingIsSameAsBilling =request.getParameter("shippingIsSameAsBilling");
					String paymentMethod = request.getParameter("paymentMethod");
					
					if(TextUtil.isEmptyOrNull(paymentMethod)){
						System.err.println("Not able to identify payment method, please choose valid payment method.");
						error.setDescription("Not able to identify payment method, please choose valid payment method.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					
					PaymentMethods method = PaymentMethods.valueOf(paymentMethod);
					Map<String, String> orderParamMap = Util.getParameterMap(request);
					orderParamMap.put("serviceFees", String.valueOf(totalTaxAmout));
					orderParamMap.put("promoTrackingId",promoTrackingId);
					orderParamMap.put("affPromoTrackingId",affPromoTrackingId);
					String validationMsg = "";
					String paymentMethodCount = null;
					WalletTransaction walletTranx = null;
					StripeTransaction stripeTranx = null;
					String primaryTrxId = "";
					String secondaryTrxId="";
					String thirdTrxId="";
					String primaryAmount=null;
					String secondaryAmount =null;
					String thirdAmount=null;
					String data = null;
					String rewardPoints = null;
					Gson gson = new Gson();
					JsonObject jsonObject = null;
					
					switch(method){
					case CREDITCARD :
						primaryAmount = String.valueOf(orderTotal);
						orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.CREDITCARD));
						orderParamMap.put("primaryPaymentAmount",primaryAmount);
						paymentMethodCount="ONE";
						break;
					case WALLET :
						primaryAmount = String.valueOf(orderTotal);
						orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.CUSTOMER_WALLET));
						orderParamMap.put("primaryPaymentAmount",primaryAmount);
						paymentMethodCount="ONE";
						break;
					case REWARDPOINTS :
						rewardPoints =  request.getParameter("rewardPoints");
						primaryAmount = String.valueOf(orderTotal);
						orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.FULL_REWARDS));
						orderParamMap.put("primaryPaymentAmount",primaryAmount);
						paymentMethodCount="ONE";
						break;
					case PARTIAL_REWARD_CC :
						rewardPoints =  request.getParameter("rewardPoints");
						primaryAmount = rewardPoints;
						secondaryAmount = request.getParameter("cardAmount");
						if(primaryAmount==null || primaryAmount.isEmpty()){
							validationMsg ="Reward point amount is not found.";
						}else if(secondaryAmount==null || secondaryAmount.isEmpty()){
							validationMsg ="Card amount is not found.";
						}else{
							orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.PARTIAL_REWARDS));
							orderParamMap.put("primaryPaymentAmount",primaryAmount);
							orderParamMap.put("secondaryPaymentMethod",String.valueOf(PartialPaymentMethod.CREDITCARD));
							orderParamMap.put("secondaryPaymentAmount",secondaryAmount);
							paymentMethodCount="TWO";
						}
						break;
					case PARTIAL_REWARD_WALLET :
						secondaryAmount = request.getParameter("walletAmount");
						rewardPoints =  request.getParameter("rewardPoints");
						primaryAmount = rewardPoints;
						if(primaryAmount==null || primaryAmount.isEmpty()){
							validationMsg ="Reward point amount is not found.";
						}else if(secondaryAmount==null || secondaryAmount.isEmpty()){
							validationMsg ="Wallet amount is not found.";
						}else{
							orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.PARTIAL_REWARDS));
							orderParamMap.put("primaryPaymentAmount",primaryAmount);
							orderParamMap.put("secondaryPaymentMethod",String.valueOf(PartialPaymentMethod.CUSTOMER_WALLET));
							orderParamMap.put("secondaryPaymentAmount",secondaryAmount);
							paymentMethodCount="TWO";
						}
						break;
					case WALLET_CREDITCARD :
						secondaryAmount = request.getParameter("cardAmount");
						primaryAmount = request.getParameter("walletAmount");
						if(primaryAmount==null || primaryAmount.isEmpty()){
							validationMsg ="Wallet amount is not found.";
						}else if(secondaryAmount==null || secondaryAmount.isEmpty()){
							validationMsg ="Credit card amount is not found.";
						}else{
							orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.CUSTOMER_WALLET));
							orderParamMap.put("primaryPaymentAmount",primaryAmount);
							orderParamMap.put("secondaryPaymentMethod",String.valueOf(PartialPaymentMethod.CREDITCARD));
							orderParamMap.put("secondaryPaymentAmount",secondaryAmount);
							paymentMethodCount="TWO";
						}
						break;
					case PARTIAL_REWARD_WALLET_CC :
						thirdAmount = request.getParameter("cardAmount");
						secondaryAmount = request.getParameter("walletAmount");
						rewardPoints =  request.getParameter("rewardPoints");
						primaryAmount = rewardPoints;
						if(primaryAmount==null || primaryAmount.isEmpty()){
							validationMsg ="Reward points amount is not found.";
						}else if(secondaryAmount==null || secondaryAmount.isEmpty()){
							validationMsg ="Wallet amount is not found.";
						}else if(thirdAmount==null || thirdAmount.isEmpty()){
							validationMsg ="Credit card amount is not found.";
						}else{
							orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.PARTIAL_REWARDS));
							orderParamMap.put("primaryPaymentAmount",primaryAmount);
							orderParamMap.put("secondaryPaymentMethod",String.valueOf(PartialPaymentMethod.CUSTOMER_WALLET));
							orderParamMap.put("secondaryPaymentAmount",secondaryAmount);
							orderParamMap.put("thirdPaymentMethod",String.valueOf(PartialPaymentMethod.CREDITCARD));
							orderParamMap.put("thirdPaymentAmount",thirdAmount);
							paymentMethodCount="THREE";
						}
						break;
					case ACCOUNT_RECIVABLE:
						primaryAmount = String.valueOf(orderTotal);
						orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.ACCOUNT_RECIVABLE));
						orderParamMap.put("primaryPaymentAmount","0.00");
						paymentMethodCount="ONE";
						break;
					}
					
					if(!validationMsg.isEmpty()){
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					
					if(isLoyalFan.equalsIgnoreCase("Yes")){
						orderParamMap.put("isFanPrice","true");
					}else{
						orderParamMap.put("isFanPrice","false");
					}
					
					DAORegistry.getOrderTicketGroupDetailsDAO().saveAll(orderTickets);
					
					String orderTicketDetailsIds ="";
					for(OrderTicketGroupDetails otd : orderTickets){
						orderTicketDetailsIds += otd.getId()+",";
					}
					orderTicketDetailsIds = orderTicketDetailsIds.substring(0,orderTicketDetailsIds.length()-1);
					orderParamMap.put("eventId", String.valueOf(eventId));
					//orderParamMap.put("realTicketGroupIds", String.valueOf(ticketIds));
					orderParamMap.put("ticketGroupRefIds",orderTicketDetailsIds);
					orderParamMap.put("orderTotal",String.valueOf(orderTotal));
					orderParamMap.put("paymentMethodCount",paymentMethodCount);
					orderParamMap.put("customerId", String.valueOf(customerId));
					if(rewardPoints!=null && !rewardPoints.isEmpty()){
						Double points = Double.parseDouble(rewardPoints);
						orderParamMap.put("rewardPoints", String.valueOf(points));
					}
					orderParamMap.put("sessionId", sessionIdStr);
					orderParamMap.put("billingAddressId", String.valueOf(bAddressId));
					orderParamMap.put("shippingAddressId", String.valueOf(sAddressId));
					orderParamMap.put("shippingSameAsBilling", shippingIsSameAsBilling);
					orderParamMap.put("referralCode", referalCode);
					orderParamMap.put("isLongSale","true");
					orderParamMap.put("orderType",orderType);
					
					try {
						data = Util.getObject(orderParamMap, sharedProperty.getApiUrl()+Constants.INITIATE_ORDER_REAL_TICKET);
					} catch (Exception e) {
						e.printStackTrace();
						System.err.println("Invoice creation failed not able to communicate with API Server.");
						error.setDescription("Invoice creation failed not able to communicate with API Server.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					
					gson = new Gson();		
					jsonObject = gson.fromJson(data, JsonObject.class);
					OrderConfirmation orderConfirmation = gson.fromJson(((JsonObject)jsonObject.get("orderConfirmation")), OrderConfirmation.class);	
					if(orderConfirmation == null){
						System.err.println("Invoice creation failed not able to communicate with API Server.");
						error.setDescription("Invoice creation failed not able to communicate with API Server.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					if(orderConfirmation.getStatus() == 0){
						System.err.println(orderConfirmation.getError().getDescription());
						error.setDescription(orderConfirmation.getError().getDescription());
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					
					orderId = orderConfirmation.getOrderId();
					boolean isPaymentSuccess = false;
					
					switch(method){
					case CREDITCARD :
						stripeTranx = doCreditCardTransaction(request,String.valueOf(orderTotal),String.valueOf(orderTicketDetailsIds),true,orderTotalQty);
						if(stripeTranx.getStatus()==0){
							validationMsg ="Credit card transaction - "+stripeTranx.getError().getDescription();
							isPaymentSuccess = false;
						}else{
							isPaymentSuccess = true;
							primaryTrxId = stripeTranx.getTransactionId();
							orderParamMap.put("primaryTransactionId",primaryTrxId);
						}
						break;
					case WALLET :
						walletTranx = doWalletTransaction(request, String.valueOf(orderTotal), orderId, userName);
						if(walletTranx.getStatus()==0){
							validationMsg ="Wallet Transaction - "+walletTranx.getError().getDescription();
							isPaymentSuccess = false;
						}else{
							isPaymentSuccess = true;
							primaryTrxId = walletTranx.getWalletHistory().getTransactionId();
							orderParamMap.put("primaryTransactionId",primaryTrxId);
							orderParamMap.put("walletTransactionId",String.valueOf(walletTranx.getWalletHistory().getId()));
						}
						break;
					case REWARDPOINTS :
						isPaymentSuccess = true;
						break;
					case PARTIAL_REWARD_CC :
						stripeTranx = doCreditCardTransaction(request,secondaryAmount,String.valueOf(orderTicketDetailsIds),true,orderTotalQty);
						if(stripeTranx.getStatus()==0){
							validationMsg ="Credit card transaction - "+stripeTranx.getError().getDescription();
							isPaymentSuccess = false;
						}else{
							isPaymentSuccess = true;
							secondaryTrxId = stripeTranx.getTransactionId();
							orderParamMap.put("primaryTransactionId",primaryTrxId);
							orderParamMap.put("secondaryTransactionId",secondaryTrxId);
						}
						break;
					case PARTIAL_REWARD_WALLET :
						walletTranx= doWalletTransaction(request,secondaryAmount,orderId,userName);
						if(walletTranx.getStatus()==0){
							validationMsg ="Wallet Transaction - "+walletTranx.getError().getDescription();
							isPaymentSuccess = false;
						}else{
							isPaymentSuccess = true;
							secondaryTrxId = walletTranx.getWalletHistory().getTransactionId();
							orderParamMap.put("primaryTransactionId",primaryTrxId);
							orderParamMap.put("secondaryTransactionId",secondaryTrxId);
							orderParamMap.put("walletTransactionId",String.valueOf(walletTranx.getWalletHistory().getId()));
						}
						break;
					case WALLET_CREDITCARD :
						walletTranx= doWalletTransaction(request,primaryAmount,orderId,userName);
						stripeTranx = doCreditCardTransaction(request,secondaryAmount,String.valueOf(orderTicketDetailsIds),true,orderTotalQty);
						if(walletTranx.getStatus()==0){
							validationMsg ="Wallet Transaction - "+walletTranx.getError().getDescription();
							isPaymentSuccess = false;
						}else if(stripeTranx.getStatus()==0){
							validationMsg ="Credit card transaction - "+stripeTranx.getError().getDescription();
							isPaymentSuccess = false;
						}else{
							isPaymentSuccess = true;
							primaryTrxId = walletTranx.getWalletHistory().getTransactionId();
							secondaryTrxId = stripeTranx.getTransactionId();
							orderParamMap.put("primaryTransactionId",primaryTrxId);
							orderParamMap.put("secondaryTransactionId",secondaryTrxId);
							orderParamMap.put("walletTransactionId",String.valueOf(walletTranx.getWalletHistory().getId()));
						}
						break;
					case PARTIAL_REWARD_WALLET_CC :
						walletTranx = doWalletTransaction(request,secondaryAmount,orderId,userName);
						stripeTranx = doCreditCardTransaction(request,thirdAmount,String.valueOf(orderTicketDetailsIds),true,orderTotalQty);
						if(walletTranx.getStatus()==0){
							validationMsg ="Wallet Transaction - "+walletTranx.getError().getDescription();
							isPaymentSuccess = false;
						}else if(stripeTranx.getStatus()==0){
							validationMsg ="Credit card transaction - "+stripeTranx.getError().getDescription();
							isPaymentSuccess = false;
						}else{
							isPaymentSuccess = true;
							secondaryTrxId = walletTranx.getWalletHistory().getTransactionId();
							thirdTrxId = stripeTranx.getTransactionId();
							orderParamMap.put("primaryTransactionId",primaryTrxId);
							orderParamMap.put("secondaryTransactionId",secondaryTrxId);
							orderParamMap.put("walletTransactionId",String.valueOf(walletTranx.getWalletHistory().getId()));
							orderParamMap.put("thirdTransactionId",thirdTrxId);
						}
						break;
					case ACCOUNT_RECIVABLE:
						isPaymentSuccess = true;
						orderParamMap.put("primaryTransactionId","dummytransactionid"+orderConfirmation.getOrderId());
						break;
					}
					if(!validationMsg.isEmpty()){
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					
					orderParamMap.put("orderId", String.valueOf(orderId));
					orderParamMap.put("isPaymentSuccess",String.valueOf(isPaymentSuccess));
					try {
						data = Util.getObject(orderParamMap, sharedProperty.getApiUrl()+Constants.CREATE_ORDER_REAL_TICKET);
					} catch (Exception e) {
						e.printStackTrace();
						System.err.println("Order confirmation failed not able to communicate with API Server.");
						error.setDescription("Order confirmation failed not able to communicate with API Server.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					
					gson = new Gson();		
					jsonObject = gson.fromJson(data, JsonObject.class);
					OrderConfirmation orderConfirmation1 = gson.fromJson(((JsonObject)jsonObject.get("orderConfirmation")), OrderConfirmation.class);	
					
					if(orderConfirmation1 == null){
						System.err.println("Order confirmation failed not able to communicate with API Server.");
						error.setDescription("Order confirmation failed not able to communicate with API Server.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					if(orderConfirmation1.getStatus() == 0){
						System.err.println("Order Confirmation Failed, "+orderConfirmation1.getError().getDescription());
						error.setDescription("Order Confirmation Failed, "+orderConfirmation1.getError().getDescription());
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					
					Invoice invoice = DAORegistry.getInvoiceDAO().getInvoiceByOrderId(orderConfirmation.getOrderId());
					CustomerOrder order = DAORegistry.getCustomerOrderDAO().get(invoice.getCustomerOrderId());
					invoice.setRealTixMap("Yes");
					if(order.getPrimaryPaymentMethod().equals(PaymentMethod.ACCOUNT_RECIVABLE)){
						invoice.setInvoiceType(PaymentMethod.ACCOUNT_RECIVABLE.toString());
					}
					if(order.getOrderType().equals(OrderType.CONTEST)){
						invoice.setInvoiceType(OrderType.CONTEST.toString());
					}
					DAORegistry.getInvoiceDAO().update(invoice);
					
					for(TicketGroup tic : ticketGroups){
						List<Ticket> tickets = DAORegistry.getTicketDAO().getAvailableTicketsByTicketGroupId(tic.getId());
						Integer qty = 0;
						for(TicketGroup dTic : dummyTGList){
							if(tic.getId() == dTic.getId() || tic.getId().equals(dTic.getId())){
								qty = dTic.getQuantity();
								break;
							}
						}
						for(int i=0;i<qty;i++){
							tickets.get(i).setInvoiceId(invoice.getId());
							tickets.get(i).setTicketStatus(TicketStatus.SOLD);
						}
						DAORegistry.getTicketDAO().updateAll(tickets);
					}
					
					List<RealTicketSectionDetails> realTicketList = DAORegistry.getQueryManagerDAO().getRealTicketSectionDetailsByInvoiceId(invoice.getId());
					Util.setActualTicketDetails(realTicketList, order);
					DAORegistry.getCustomerOrderDAO().update(order);
					
//					genericResponseDTO.setMessage("Order created successfully, InvoiceId : "+invoice.getId());
//					genericResponseDTO.setStatus(1);
					
					Customer customer = DAORegistry.getCustomerDAO().get(Integer.parseInt(customerId));
					if(!customer.getProductType().equals(ProductType.REWARDTHEFAN) && customer.getEmail()!=null && !customer.getEmail().isEmpty()
							&& (customer.getIsRegistrationMailSent()==null || customer.getIsRegistrationMailSent() == false)){
						customer.setApplicationPlatForm(ApplicationPlatform.DESKTOP_SITE);
						customer.setProductType(ProductType.REWARDTHEFAN);
						customer.setSignupType(SignupType.REWARDTHEFAN);
						customer.setIsRegistrationMailSent(true);
						customer.setSignupDate(new Date());
						customer.setUserId(Util.generatedCustomerUserId(customer.getCustomerName(), customer.getLastName()));
						//customer.setReferrerCode(Util.generateCustomerReferalCode());
						customer.setReferrerCode(customer.getEmail());
						String password = Util.generatePassword(customer.getCustomerName(), "RTF");
						customer.setPassword(Util.generateEncrptedPassword(password));
						DAORegistry.getCustomerDAO().update(customer);
						
						Map<String, Object> mailMap = new HashMap<String, Object>();
						mailMap.put("customerName", customer.getCustomerName()+" "+customer.getLastName());
						mailMap.put("password", password);
						mailMap.put("userName", customer.getUserName());
						//mailMap.put("androidUrl","https://play.google.com/store/apps/details?id=com.rewardthefan&hl=en");
						//mailMap.put("iosUrl","https://itunes.apple.com/us/app/reward-the-fan/id1140367203?ls=1&mt=8");
						com.rtw.tracker.mail.MailAttachment[] mailAttachment = new com.rtw.tracker.mail.MailAttachment[8];
						mailAttachment[0] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","tyrtf.png",com.rtw.tmat.utils.Util.getFilePath(request, "tyrtf.png", "/resources/images/"));
						mailAttachment[1] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","blue.png",com.rtw.tmat.utils.Util.getFilePath(request, "blue.png", "/resources/images/"));
						mailAttachment[2] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","fb1.png",com.rtw.tmat.utils.Util.getFilePath(request, "fb1.png", "/resources/images/"));
						mailAttachment[3] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","ig1.png",com.rtw.tmat.utils.Util.getFilePath(request, "ig1.png", "/resources/images/"));
						mailAttachment[4] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","li1.png",com.rtw.tmat.utils.Util.getFilePath(request, "li1.png", "/resources/images/"));
						mailAttachment[5] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","rtflogot.png",com.rtw.tmat.utils.Util.getFilePath(request, "rtflogot.png", "/resources/images/"));
						mailAttachment[6] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","p1.png",com.rtw.tmat.utils.Util.getFilePath(request, "p1.png", "/resources/images/"));
						mailAttachment[7] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","tw1.png",com.rtw.tmat.utils.Util.getFilePath(request, "tw1.png", "/resources/images/"));
						String email = customer.getEmail();
						//email = "msanghani@rightthisway.com";
						String bcc = "";
						bcc = "amit.raut@rightthisway.com,msanghani@righthtisway.com";
						try {
							mailManager.sendMailNow("text/html","sales@rewardthefan.com",email, 
									null,bcc, "You are successfully registered with RewardTheFan.com.",
						    		"mail-rewardfan-customer-registration.html", mailMap, "text/html", null,mailAttachment,null);
						} catch (Exception e) {
							e.printStackTrace();
							error.setDescription("Error occured while sending mail for Real Ticket Order Confirmation to customer.");
							genericResponseDTO.setError(error);
							genericResponseDTO.setStatus(0);
							return genericResponseDTO;
						}
					}
					
					genericResponseDTO.setMessage("Order created successfully, InvoiceId : "+invoice.getId());
					genericResponseDTO.setStatus(1);
					
					//Tracking User Action
					String userActionMsg = "Invoice Created Successfully.";
					Util.userActionAudit(request, invoice.getId(), userActionMsg);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Creating Invoice for Long(Real) Ticket.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
	
	@RequestMapping(value = "/SaveInvoiceHoldTicket")
	public GenericResponseDTO saveInvoiceHoldTicket(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		
		try {
			String action = request.getParameter("action");
			String customerId = request.getParameter("customerId");
			String userName = request.getParameter("userName");
			String brokerIdStr = request.getParameter("brokerId");			
			String clientIpAddress = request.getParameter("clientIPAddress");
			String sessionIdStr = request.getParameter("deviceId");
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide BrokerId");
				error.setDescription("Please provide BrokerId");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}			
			Integer brokerId = 0;	
			try{
				brokerId = Integer.parseInt(brokerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid BrokerId");
				error.setDescription("Please provide valid BrokerId");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			String returnMessage = "";
			
			Integer orderId=null;
			List<TicketGroup> ticketGroups = new ArrayList<TicketGroup>();
			List<TicketGroup> dummyTGList = new ArrayList<TicketGroup>();
			List<OrderTicketGroupDetails> orderTickets = new ArrayList<OrderTicketGroupDetails>();
			
			if(returnMessage.isEmpty()){
				
				if(action != null && action.equals("saveInvoice")){				
					
					String referalCode =request.getParameter("referralCode"); 
					String eventIdStr = request.getParameter("eventId");
					String addressLine1 = request.getParameter("addressLine1");
					String addressLine2 = request.getParameter("addressLine2");
					String shAddressLine1 = request.getParameter("shAddressLine1");
					String shAddressLine2 = request.getParameter("shAddressLine2");
					String isLoyalFan = request.getParameter("isLoyalFanPrice");
					String bAddressId =request.getParameter("bAddressId"); 
					String sAddressId =request.getParameter("sAddressId");
					String invoiceType = request.getParameter("createInvoiceType");
					String ticketSizeStr = request.getParameter("ticketSize");
					String promoTrackingId = request.getParameter("promoTrackingId");
					String affPromoTrackingId = request.getParameter("affPromoTrackingId");
					
					String ticketIds = request.getParameter("ticketIdStr");
					
					//For Hold Ticket
					String ids = request.getParameter("holdTicketIds");
					String arr[] = null;
					if(ids !=null && !ids.isEmpty()){
						if(ids.contains(",")){
							arr = ids.split(",");
						}else{
							arr = new String[1];
							arr[0] = ids;
						}
					}
					List<Integer> listIdArr = new ArrayList<Integer>();
					if(arr != null && arr.length > 0){
						for(String str : arr){
							if(!str.trim().isEmpty()){
								listIdArr.add(Integer.parseInt(str));
							}
						}
					}
					//End Hold Ticket
					
					if(StringUtils.isEmpty(eventIdStr)){
						System.err.println("Not able to identify Event Information.");
						error.setDescription("Not able to identify Event Information.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}				
					Integer eventId = null;
					try {
						eventId = Integer.parseInt(eventIdStr);
					} catch (Exception e) {
						e.printStackTrace();
						error.setDescription("Invalid Event information found.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					
					if(StringUtils.isEmpty(bAddressId)){				
						System.err.println("Billing Address Not found, Please select Billing Address.");
						error.setDescription("Billing Address Not found, Please select Billing Address.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					if(StringUtils.isEmpty(sAddressId)){				
						System.err.println("Shipping Address Not found, Please select Shipping Address.");
						error.setDescription("Shipping Address Not found, Please select Shipping Address.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					if((addressLine1==null || addressLine1.isEmpty()) && (addressLine2==null || addressLine2.isEmpty())){
						System.err.println("Selected billing address does not have either AddressLine1 or AddressLine2.");
						error.setDescription("Selected billing address does not have either AddressLine1 or AddressLine2.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					if((shAddressLine1==null || shAddressLine1.isEmpty()) && (shAddressLine2==null || shAddressLine2.isEmpty())){
						System.err.println("Selected shipping address does not have either AddressLine1 or AddressLine2.");
						error.setDescription("Selected shipping address does not have either AddressLine1 or AddressLine2.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					
					if(StringUtils.isEmpty(ticketSizeStr)){				
						System.err.println("Not able to identify Total No of Tickets.");
						error.setDescription("Not able to identify Total No of Tickets.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					Integer ticketSize = null;
					try {
						ticketSize = Integer.parseInt(ticketSizeStr);
					} catch (Exception e) {
						e.printStackTrace();
						error.setDescription("Bad values found for Total No of Tickets.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					
					Double orderTotal = 0.0;
					Integer orderTotalQty = 0;
					Double totalTaxAmout = 0.00;
					for(int i=0;i<ticketSize;i++){
						String ticketCountStr = request.getParameter("ticketCount_"+i);
						String ticketIdStr = request.getParameter("ticketId_"+i);
						String soldPrice = request.getParameter("soldPrice_"+i);
						String taxAmountStr = request.getParameter("taxAmount_"+i);
	
						Double taxAmount = 0.0;
						if(taxAmountStr != null && !taxAmountStr.isEmpty() ) {
							try {
								taxAmount  = Double.parseDouble(taxAmountStr);
							} catch (Exception e) {
								error.setDescription("Invaid Fees Amount.");
								genericResponseDTO.setError(error);
								genericResponseDTO.setStatus(0);
								return genericResponseDTO;
							}
						}
						totalTaxAmout += taxAmount;
						if(ticketIdStr == null || ticketIdStr.trim().isEmpty()) {
							System.err.println("Not able to identify Ticket Information, Please refresh page and try again.");
							error.setDescription("Not able to identify Ticket Information, Please refresh page and try again.");
							genericResponseDTO.setError(error);
							genericResponseDTO.setStatus(0);
							return genericResponseDTO;
						}
						Integer tgId = null;
						try {
							tgId = Integer.parseInt(ticketIdStr);
						} catch (Exception e) {
							e.printStackTrace();
							error.setDescription("Bad value found for Ticket Id, It should be valid integer value.");
							genericResponseDTO.setError(error);
							genericResponseDTO.setStatus(0);
							return genericResponseDTO;
						}
						
						if(ticketCountStr == null || ticketCountStr.trim().isEmpty()) {
							System.err.println("Not able to identify Ticket Quantity, Please refresh page and try again.");
							error.setDescription("Not able to identify Ticket Quantity, Please refresh page and try again.");
							genericResponseDTO.setError(error);
							genericResponseDTO.setStatus(0);
							return genericResponseDTO;
						}
						Integer ticketCount = null;
						try {
							ticketCount = Integer.parseInt(ticketCountStr);
						} catch (Exception e) {
							e.printStackTrace();
							error.setDescription("Bad value found for Ticket Quantity, It should be valid integer value.");
							genericResponseDTO.setError(error);
							genericResponseDTO.setStatus(0);
							return genericResponseDTO;
						}
						
						if(soldPrice == null || soldPrice.trim().isEmpty()) {
							System.err.println("Sold Price cannot be empty, Please add valid sold price.");
							error.setDescription("Sold Price cannot be empty, Please add valid sold price.");
							genericResponseDTO.setError(error);
							genericResponseDTO.setStatus(0);
							return genericResponseDTO;
						}
						Double ticketSoldPrice = null;
						try {
							ticketSoldPrice= Double.parseDouble(soldPrice);
						} catch (Exception e) {
							e.printStackTrace();
							error.setDescription("Bad value for Sold Price found, It should be valid Double value.");
							genericResponseDTO.setError(error);
							genericResponseDTO.setStatus(0);
							return genericResponseDTO;
						}
						
						TicketGroup tg = new TicketGroup();
						tg.setId(tgId);
						tg.setQuantity(ticketCount);
						tg.setPrice(ticketSoldPrice);
						dummyTGList.add(tg);
						Double total = (tg.getPrice() * tg.getQuantity()) + Util.getBrokerServiceFees(brokerId, tg.getPrice(), tg.getQuantity(), taxAmount);
						orderTotal += Util.getRoundedValue(total);
						orderTotalQty += tg.getQuantity();
						
					}
					CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(Integer.parseInt(customerId));
					if(customerLoyalty==null){
						customerLoyalty = new CustomerLoyalty();
						customerLoyalty.setActivePoints(0.00);
						customerLoyalty.setActiveRewardDollers(0.00);
						customerLoyalty.setCustomerId(Integer.parseInt(customerId));
						customerLoyalty.setLastUpdate(new Date());
						customerLoyalty.setLatestEarnedPoints(0.00);
						customerLoyalty.setLatestSpentPoints(0.00);
						customerLoyalty.setPendingPoints(0.00);
						customerLoyalty.setRevertedSpentPoints(0.00);
						customerLoyalty.setTotalEarnedPoints(0.00);
						customerLoyalty.setTotalSpentPoints(0.00);
						customerLoyalty.setVoidedRewardPoints(0.00);
						DAORegistry.getCustomerLoyaltyDAO().save(customerLoyalty);
					}
					
					String msg = "";
					if(referalCode!=null && !referalCode.isEmpty()){
						System.out.println("Applied promotional/referral code : "+referalCode);
						
						Map<String, String> paramMap = Util.getParameterMap(request);
						paramMap.put("referralCode", referalCode);
						paramMap.put("customerId",customerId);
						paramMap.put("eventId",String.valueOf(eventId));
						paramMap.put("tgId",ticketIds);
						paramMap.put("clientIPAddress", clientIpAddress);
						paramMap.put("isLongSale", "true");
						paramMap.put("orderTotal",String.valueOf(orderTotal));
						if(isLoyalFan.equalsIgnoreCase("Yes")){
							paramMap.put("isFanPrice","true");
						}else{
							paramMap.put("isFanPrice","false");
						}
						paramMap.put("sessionId", sessionIdStr);
						paramMap.put("orderQty",String.valueOf(orderTotalQty));
						
					    String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.VALIDATE_PROMOCODE_REAL_TICKET);
					    Gson gson = new Gson();  
						JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
						ReferralCodeValidation referralCodeValidation = gson.fromJson(((JsonObject)jsonObject.get("referralCodeValidation")), ReferralCodeValidation.class);
						
						if(referralCodeValidation == null){
							System.err.println("Failed while validating referral code, Failed to communicate with our API server");
							error.setDescription("Failed while validating referral code, Failed to communicate with our API server");
							genericResponseDTO.setError(error);
							genericResponseDTO.setStatus(0);
							return genericResponseDTO;
						}
						if(referralCodeValidation.getStatus()==1){
							msg = "REFERRALCODEVALIDATED";
						}else if(referralCodeValidation.getStatus()==0){
							msg = referralCodeValidation.getError().getDescription();
							System.err.println("Referral Code Validation: "+msg);
							error.setDescription(msg);
							genericResponseDTO.setError(error);
							genericResponseDTO.setStatus(0);
							return genericResponseDTO;
						}
					}
					for(TicketGroup tg : dummyTGList){
						TicketGroup ticGroup = null;
						ticGroup = DAORegistry.getTicketGroupDAO().getAllTicketGroupById(tg.getId());
						ticketGroups.add(ticGroup);
						if(ticGroup.getShippingMethodId()== null || ticGroup.getShippingMethodId()<0){
							ticGroup.setShippingMethod("E-Ticket");
							ticGroup.setShippingMethodId(1);
						}
						String zone = Util.computZoneForTickets(eventId, ticGroup.getSection());
						if(zone==null || zone.isEmpty()){
							zone = "N/A";
						}
						OrderTicketGroupDetails orderTicket = new OrderTicketGroupDetails();
						orderTicket.setZone(zone);
						orderTicket.setQuantity(tg.getQuantity());
						orderTicket.setSection(ticGroup.getSection());
						orderTicket.setRow(ticGroup.getRow());
						orderTicket.setTicketGroupId(ticGroup.getId());
						orderTicket.setSoldPrice(tg.getPrice());
						orderTicket.setActualPrice(ticGroup.getPrice());
						orderTicket.setOrderId(-1);
						
						List<Ticket> tickets = null;
						if(invoiceType != null && invoiceType.equalsIgnoreCase("HOLD")){
							tickets = DAORegistry.getTicketDAO().getTicketsByTicketIdsAndStatus(listIdArr);
						}else{
							tickets = DAORegistry.getTicketDAO().getAvailableTicketsByTicketGroupId(tg.getId());
						}
						if(tickets.size() < tg.getQuantity()){
							System.err.println("Given ticket group quantity is greater than available quantity, Please enter valid quantity.");
							error.setDescription("Given ticket group quantity is greater than available quantity, Please enter valid quantity.");
							genericResponseDTO.setError(error);
							genericResponseDTO.setStatus(0);
							return genericResponseDTO;
						}
												
						if(invoiceType != null && invoiceType.equalsIgnoreCase("HOLD")){
							if(!tickets.isEmpty() && tickets.size() > 0){
								if(tickets.size() == 1){
									orderTicket.setSeat(tickets.get(0).getSeatNo());
								}else{
									orderTicket.setSeat(tickets.get(0).getSeatNo()+"-"+tickets.get(tickets.size()-1).getSeatNo());									
								}
							}
						}else{
							if(tg.getQuantity() == 1){
								orderTicket.setSeat(tickets.get(0).getSeatNo());
							}else{
								orderTicket.setSeat(tickets.get(0).getSeatNo()+"-"+tickets.get(tg.getQuantity()-1).getSeatNo());
							}
						}						
						orderTickets.add(orderTicket);
						//orderParamMap.put(String.valueOf(orderTicket.getId()),String.valueOf(orderTicket.getQuantity())+"_"+String.valueOf(tg.getPrice()));
					}
					
					String shippingIsSameAsBilling =request.getParameter("shippingIsSameAsBilling");
					String paymentMethod = request.getParameter("paymentMethod");
					
					if(TextUtil.isEmptyOrNull(paymentMethod)){
						System.err.println("Not able to identify payment method, please choose valid payment method.");
						error.setDescription("Not able to identify payment method, please choose valid payment method.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					
					PaymentMethods method = PaymentMethods.valueOf(paymentMethod);
					Map<String, String> orderParamMap = Util.getParameterMap(request);
					orderParamMap.put("serviceFees", String.valueOf(totalTaxAmout));
					orderParamMap.put("promoTrackingId",promoTrackingId);
					orderParamMap.put("affPromoTrackingId",affPromoTrackingId);
					String validationMsg = "";
					String paymentMethodCount = null;
					WalletTransaction walletTranx = null;
					StripeTransaction stripeTranx = null;
					String primaryTrxId = "";
					String secondaryTrxId="";
					String thirdTrxId="";
					String primaryAmount=null;
					String secondaryAmount =null;
					String thirdAmount=null;
					String data = null;
					String rewardPoints = null;
					Gson gson = new Gson();
					JsonObject jsonObject = null;
					
					switch(method){
					case CREDITCARD :
						primaryAmount = String.valueOf(orderTotal);
						orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.CREDITCARD));
						orderParamMap.put("primaryPaymentAmount",primaryAmount);
						paymentMethodCount="ONE";
						break;
					case WALLET :
						primaryAmount = String.valueOf(orderTotal);
						orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.CUSTOMER_WALLET));
						orderParamMap.put("primaryPaymentAmount",primaryAmount);
						paymentMethodCount="ONE";
						break;
					case REWARDPOINTS :
						rewardPoints =  request.getParameter("rewardPoints");
						primaryAmount = String.valueOf(orderTotal);
						orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.FULL_REWARDS));
						orderParamMap.put("primaryPaymentAmount",primaryAmount);
						paymentMethodCount="ONE";
						break;
					case PARTIAL_REWARD_CC :
						rewardPoints =  request.getParameter("rewardPoints");
						primaryAmount = rewardPoints;
						secondaryAmount = request.getParameter("cardAmount");
						if(primaryAmount==null || primaryAmount.isEmpty()){
							validationMsg ="Reward point amount is not found.";
						}else if(secondaryAmount==null || secondaryAmount.isEmpty()){
							validationMsg ="Card amount is not found.";
						}else{
							orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.PARTIAL_REWARDS));
							orderParamMap.put("primaryPaymentAmount",primaryAmount);
							orderParamMap.put("secondaryPaymentMethod",String.valueOf(PartialPaymentMethod.CREDITCARD));
							orderParamMap.put("secondaryPaymentAmount",secondaryAmount);
							paymentMethodCount="TWO";
						}
						break;
					case PARTIAL_REWARD_WALLET :
						secondaryAmount = request.getParameter("walletAmount");
						rewardPoints =  request.getParameter("rewardPoints");
						primaryAmount = rewardPoints;
						if(primaryAmount==null || primaryAmount.isEmpty()){
							validationMsg ="Reward point amount is not found.";
						}else if(secondaryAmount==null || secondaryAmount.isEmpty()){
							validationMsg ="Wallet amount is not found.";
						}else{
							orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.PARTIAL_REWARDS));
							orderParamMap.put("primaryPaymentAmount",primaryAmount);
							orderParamMap.put("secondaryPaymentMethod",String.valueOf(PartialPaymentMethod.CUSTOMER_WALLET));
							orderParamMap.put("secondaryPaymentAmount",secondaryAmount);
							paymentMethodCount="TWO";
						}
						break;
					case WALLET_CREDITCARD :
						secondaryAmount = request.getParameter("cardAmount");
						primaryAmount = request.getParameter("walletAmount");
						if(primaryAmount==null || primaryAmount.isEmpty()){
							validationMsg ="Wallet amount is not found.";
						}else if(secondaryAmount==null || secondaryAmount.isEmpty()){
							validationMsg ="Credit card amount is not found.";
						}else{
							orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.CUSTOMER_WALLET));
							orderParamMap.put("primaryPaymentAmount",primaryAmount);
							orderParamMap.put("secondaryPaymentMethod",String.valueOf(PartialPaymentMethod.CREDITCARD));
							orderParamMap.put("secondaryPaymentAmount",secondaryAmount);
							paymentMethodCount="TWO";
						}
						break;
					case PARTIAL_REWARD_WALLET_CC :
						thirdAmount = request.getParameter("cardAmount");
						secondaryAmount = request.getParameter("walletAmount");
						rewardPoints =  request.getParameter("rewardPoints");
						primaryAmount = rewardPoints;
						if(primaryAmount==null || primaryAmount.isEmpty()){
							validationMsg ="Reward points amount is not found.";
						}else if(secondaryAmount==null || secondaryAmount.isEmpty()){
							validationMsg ="Wallet amount is not found.";
						}else if(thirdAmount==null || thirdAmount.isEmpty()){
							validationMsg ="Credit card amount is not found.";
						}else{
							orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.PARTIAL_REWARDS));
							orderParamMap.put("primaryPaymentAmount",primaryAmount);
							orderParamMap.put("secondaryPaymentMethod",String.valueOf(PartialPaymentMethod.CUSTOMER_WALLET));
							orderParamMap.put("secondaryPaymentAmount",secondaryAmount);
							orderParamMap.put("thirdPaymentMethod",String.valueOf(PartialPaymentMethod.CREDITCARD));
							orderParamMap.put("thirdPaymentAmount",thirdAmount);
							paymentMethodCount="THREE";
						}
						break;
					case ACCOUNT_RECIVABLE:
						primaryAmount = String.valueOf(orderTotal);
						orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.ACCOUNT_RECIVABLE));
						orderParamMap.put("primaryPaymentAmount",primaryAmount);
						paymentMethodCount="ONE";
						break;
					}
					
					if(!validationMsg.isEmpty()){
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					
					if(isLoyalFan.equalsIgnoreCase("Yes")){
						orderParamMap.put("isFanPrice","true");
					}else{
						orderParamMap.put("isFanPrice","false");
					}
					
					DAORegistry.getOrderTicketGroupDetailsDAO().saveAll(orderTickets);
					
					String orderTicketDetailsIds ="";
					for(OrderTicketGroupDetails otd : orderTickets){
						orderTicketDetailsIds += otd.getId()+",";
					}
					orderTicketDetailsIds = orderTicketDetailsIds.substring(0,orderTicketDetailsIds.length()-1);
					orderParamMap.put("eventId", String.valueOf(eventId));
					//orderParamMap.put("realTicketGroupIds", String.valueOf(ticketIds));
					orderParamMap.put("ticketGroupRefIds",orderTicketDetailsIds);
					orderParamMap.put("orderTotal",String.valueOf(orderTotal));
					orderParamMap.put("paymentMethodCount",paymentMethodCount);
					orderParamMap.put("customerId", String.valueOf(customerId));
					if(rewardPoints!=null && !rewardPoints.isEmpty()){
						Double points = Double.parseDouble(rewardPoints);
						orderParamMap.put("rewardPoints", String.valueOf(points));
					}
					orderParamMap.put("sessionId", sessionIdStr);
					orderParamMap.put("billingAddressId", String.valueOf(bAddressId));
					orderParamMap.put("shippingAddressId", String.valueOf(sAddressId));
					orderParamMap.put("shippingSameAsBilling", shippingIsSameAsBilling);
					orderParamMap.put("referralCode", referalCode);
					orderParamMap.put("isLongSale","true");
					
					try {
						data = Util.getObject(orderParamMap, sharedProperty.getApiUrl()+Constants.INITIATE_ORDER_REAL_TICKET);
					} catch (Exception e) {
						e.printStackTrace();
						System.err.println("Invoice creation failed not able to communicate with API Server.");
						error.setDescription("Invoice creation failed not able to communicate with API Server.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					
					gson = new Gson();		
					jsonObject = gson.fromJson(data, JsonObject.class);
					OrderConfirmation orderConfirmation = gson.fromJson(((JsonObject)jsonObject.get("orderConfirmation")), OrderConfirmation.class);	
					if(orderConfirmation == null){
						System.err.println("Invoice creation failed not able to communicate with API Server.");
						error.setDescription("Invoice creation failed not able to communicate with API Server.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					if(orderConfirmation.getStatus() == 0){
						System.err.println(orderConfirmation.getError().getDescription());
						error.setDescription(orderConfirmation.getError().getDescription());
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					
					orderId = orderConfirmation.getOrderId();
					boolean isPaymentSuccess = false;
					
					switch(method){
					case CREDITCARD :
						stripeTranx = doCreditCardTransaction(request,String.valueOf(orderTotal),String.valueOf(orderTicketDetailsIds),true,orderTotalQty);
						if(stripeTranx.getStatus()==0){
							validationMsg ="Credit card transaction - "+stripeTranx.getError().getDescription();
							isPaymentSuccess = false;
						}else{
							isPaymentSuccess = true;
							primaryTrxId = stripeTranx.getTransactionId();
							orderParamMap.put("primaryTransactionId",primaryTrxId);
						}
						break;
					case WALLET :
						walletTranx = doWalletTransaction(request, String.valueOf(orderTotal), orderId, userName);
						if(walletTranx.getStatus()==0){
							validationMsg ="Wallet Transaction - "+walletTranx.getError().getDescription();
							isPaymentSuccess = false;
						}else{
							isPaymentSuccess = true;
							primaryTrxId = walletTranx.getWalletHistory().getTransactionId();
							orderParamMap.put("primaryTransactionId",primaryTrxId);
							orderParamMap.put("walletTransactionId",String.valueOf(walletTranx.getWalletHistory().getId()));
						}
						break;
					case REWARDPOINTS :
						isPaymentSuccess = true;
						break;
					case PARTIAL_REWARD_CC :
						stripeTranx = doCreditCardTransaction(request,secondaryAmount,String.valueOf(orderTicketDetailsIds),true,orderTotalQty);
						if(stripeTranx.getStatus()==0){
							validationMsg ="Credit card transaction - "+stripeTranx.getError().getDescription();
							isPaymentSuccess = false;
						}else{
							isPaymentSuccess = true;
							secondaryTrxId = stripeTranx.getTransactionId();
							orderParamMap.put("primaryTransactionId",primaryTrxId);
							orderParamMap.put("secondaryTransactionId",secondaryTrxId);
						}
						break;
					case PARTIAL_REWARD_WALLET :
						walletTranx= doWalletTransaction(request,secondaryAmount,orderId,userName);
						if(walletTranx.getStatus()==0){
							validationMsg ="Wallet Transaction - "+walletTranx.getError().getDescription();
							isPaymentSuccess = false;
						}else{
							isPaymentSuccess = true;
							secondaryTrxId = walletTranx.getWalletHistory().getTransactionId();
							orderParamMap.put("primaryTransactionId",primaryTrxId);
							orderParamMap.put("secondaryTransactionId",secondaryTrxId);
							orderParamMap.put("walletTransactionId",String.valueOf(walletTranx.getWalletHistory().getId()));
						}
						break;
					case WALLET_CREDITCARD :
						walletTranx= doWalletTransaction(request,primaryAmount,orderId,userName);
						stripeTranx = doCreditCardTransaction(request,secondaryAmount,String.valueOf(orderTicketDetailsIds),true,orderTotalQty);
						if(walletTranx.getStatus()==0){
							validationMsg ="Wallet Transaction - "+walletTranx.getError().getDescription();
							isPaymentSuccess = false;
						}else if(stripeTranx.getStatus()==0){
							validationMsg ="Credit card transaction - "+stripeTranx.getError().getDescription();
							isPaymentSuccess = false;
						}else{
							isPaymentSuccess = true;
							primaryTrxId = walletTranx.getWalletHistory().getTransactionId();
							secondaryTrxId = stripeTranx.getTransactionId();
							orderParamMap.put("primaryTransactionId",primaryTrxId);
							orderParamMap.put("secondaryTransactionId",secondaryTrxId);
							orderParamMap.put("walletTransactionId",String.valueOf(walletTranx.getWalletHistory().getId()));
						}
						break;
					case PARTIAL_REWARD_WALLET_CC :
						walletTranx = doWalletTransaction(request,secondaryAmount,orderId,userName);
						stripeTranx = doCreditCardTransaction(request,thirdAmount,String.valueOf(orderTicketDetailsIds),true,orderTotalQty);
						if(walletTranx.getStatus()==0){
							validationMsg ="Wallet Transaction - "+walletTranx.getError().getDescription();
							isPaymentSuccess = false;
						}else if(stripeTranx.getStatus()==0){
							validationMsg ="Credit card transaction - "+stripeTranx.getError().getDescription();
							isPaymentSuccess = false;
						}else{
							isPaymentSuccess = true;
							secondaryTrxId = walletTranx.getWalletHistory().getTransactionId();
							thirdTrxId = stripeTranx.getTransactionId();
							orderParamMap.put("primaryTransactionId",primaryTrxId);
							orderParamMap.put("secondaryTransactionId",secondaryTrxId);
							orderParamMap.put("walletTransactionId",String.valueOf(walletTranx.getWalletHistory().getId()));
							orderParamMap.put("thirdTransactionId",thirdTrxId);
						}
						break;
					case ACCOUNT_RECIVABLE:
						isPaymentSuccess = true;
						orderParamMap.put("primaryTransactionId","dummytransactionid"+orderConfirmation.getOrderId());
						break;
					}
					if(!validationMsg.isEmpty()){
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					
					orderParamMap.put("orderId", String.valueOf(orderId));
					orderParamMap.put("isPaymentSuccess",String.valueOf(isPaymentSuccess));
					try {
						data = Util.getObject(orderParamMap, sharedProperty.getApiUrl()+Constants.CREATE_ORDER_REAL_TICKET);
					} catch (Exception e) {
						e.printStackTrace();
						System.err.println("Order confirmation failed not able to communicate with API Server.");
						error.setDescription("Order confirmation failed not able to communicate with API Server.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					
					gson = new Gson();		
					jsonObject = gson.fromJson(data, JsonObject.class);
					OrderConfirmation orderConfirmation1 = gson.fromJson(((JsonObject)jsonObject.get("orderConfirmation")), OrderConfirmation.class);	
					
					if(orderConfirmation1 == null){
						System.err.println("Order confirmation failed not able to communicate with API Server.");
						error.setDescription("Order confirmation failed not able to communicate with API Server.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					if(orderConfirmation1.getStatus() == 0){
						System.err.println("Order Confirmation Failed, "+orderConfirmation1.getError().getDescription());
						error.setDescription("Order Confirmation Failed, "+orderConfirmation1.getError().getDescription());
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					
					Invoice invoice = DAORegistry.getInvoiceDAO().getInvoiceByOrderId(orderConfirmation.getOrderId());
					CustomerOrder order = DAORegistry.getCustomerOrderDAO().get(invoice.getCustomerOrderId());
					invoice.setRealTixMap("Yes");
					if(order.getPrimaryPaymentMethod().equals(PaymentMethod.ACCOUNT_RECIVABLE)){
						invoice.setInvoiceType(PaymentMethod.ACCOUNT_RECIVABLE.toString());
					}
					DAORegistry.getInvoiceDAO().update(invoice);
					
					for(TicketGroup tic : ticketGroups){
						List<Ticket> tickets = null;
						if(invoiceType != null && invoiceType.equalsIgnoreCase("HOLD")){
							tickets = DAORegistry.getTicketDAO().getTicketsByTicketIdsAndStatus(listIdArr);
						}else{
							tickets = DAORegistry.getTicketDAO().getAvailableTicketsByTicketGroupId(tic.getId());
						}
						
						Integer qty = 0;
						for(TicketGroup dTic : dummyTGList){
							if(tic.getId() == dTic.getId() || tic.getId().equals(dTic.getId())){
								qty = dTic.getQuantity();
								break;
							}
						}
						for(int i=0;i<qty;i++){
							if(invoiceType != null && invoiceType.equalsIgnoreCase("HOLD")){
								if(tickets.get(i).getTicketStatus().equals(TicketStatus.ONHOLD)){
									tickets.get(i).setInvoiceId(invoice.getId());
									tickets.get(i).setTicketStatus(TicketStatus.SOLD);
								}
							}else{
								tickets.get(i).setInvoiceId(invoice.getId());
								tickets.get(i).setTicketStatus(TicketStatus.SOLD);
							}
						}
						DAORegistry.getTicketDAO().updateAll(tickets);
					}
					
					List<RealTicketSectionDetails> realTicketList = DAORegistry.getQueryManagerDAO().getRealTicketSectionDetailsByInvoiceId(invoice.getId());
					Util.setActualTicketDetails(realTicketList, order);
					DAORegistry.getCustomerOrderDAO().update(order);
					
					//For Updating Inventory Hold Status - Deleted
					 if(invoiceType != null && invoiceType.equalsIgnoreCase("HOLD")){				 	
//						List<Ticket> ticketsList = DAORegistry.getTicketDAO().getTicketsByTicketIdsAndStatus(listIdArr);
						List<Ticket> ticketsList = DAORegistry.getTicketDAO().getTicketsByTicketIds(listIdArr);
						Integer tgId = 0;
						String ticketId = null;
						for(Ticket tic : ticketsList){
							tgId = tic.getTicketGroupId();
							ticketId = String.valueOf(tic.getId());
						}
						List<InventoryHold> holds = DAORegistry.getInventoryHoldDAO().getInventoryHoldByTicketGroupId(tgId);
						InventoryHold hold = null;
						if(holds.size()==1){
							hold = holds.get(0);
						}else{
							for(InventoryHold h : holds){
								if(h.getTicketIds().contains(",")){
									String id[] = h.getTicketIds().split(",");
									for(String str : id){
										if(ticketId.equalsIgnoreCase(str)){
											hold = h;
											break;
										}
									}
								}else{
									if(ticketId.equalsIgnoreCase(h.getTicketIds())){
										hold = h;
										break;
									}
								}
								if(hold!=null){
									break;
								}
							}
						}
						hold.setStatus("DELETED");
						DAORegistry.getInventoryHoldDAO().update(hold);
					 }
					 //End Inventory Hold Process
					 
//					genericResponseDTO.setMessage("Order created successfully, InvoiceId : "+invoice.getId());
//					genericResponseDTO.setStatus(1);
					
					Customer customer = DAORegistry.getCustomerDAO().get(Integer.parseInt(customerId));
					if(!customer.getProductType().equals(ProductType.REWARDTHEFAN) && customer.getEmail()!=null && !customer.getEmail().isEmpty()
							&& (customer.getIsRegistrationMailSent()==null || customer.getIsRegistrationMailSent() == false)){
						customer.setApplicationPlatForm(ApplicationPlatform.DESKTOP_SITE);
						customer.setProductType(ProductType.REWARDTHEFAN);
						customer.setSignupType(SignupType.REWARDTHEFAN);
						customer.setIsRegistrationMailSent(true);
						customer.setSignupDate(new Date());
						customer.setUserId(Util.generatedCustomerUserId(customer.getCustomerName(), customer.getLastName()));
						//customer.setReferrerCode(Util.generateCustomerReferalCode());
						customer.setReferrerCode(customer.getEmail());
						String password = Util.generatePassword(customer.getCustomerName(), "RTF");
						customer.setPassword(Util.generateEncrptedPassword(password));
						DAORegistry.getCustomerDAO().update(customer);
						
						Map<String, Object> mailMap = new HashMap<String, Object>();
						mailMap.put("customerName", customer.getCustomerName()+" "+customer.getLastName());
						mailMap.put("password", password);
						mailMap.put("userName", customer.getUserName());
						//mailMap.put("androidUrl","https://play.google.com/store/apps/details?id=com.rewardthefan&hl=en");
						//mailMap.put("iosUrl","https://itunes.apple.com/us/app/reward-the-fan/id1140367203?ls=1&mt=8");
						com.rtw.tracker.mail.MailAttachment[] mailAttachment = new com.rtw.tracker.mail.MailAttachment[8];
						mailAttachment[0] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","tyrtf.png",com.rtw.tmat.utils.Util.getFilePath(request, "tyrtf.png", "/resources/images/"));
						mailAttachment[1] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","blue.png",com.rtw.tmat.utils.Util.getFilePath(request, "blue.png", "/resources/images/"));
						mailAttachment[2] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","fb1.png",com.rtw.tmat.utils.Util.getFilePath(request, "fb1.png", "/resources/images/"));
						mailAttachment[3] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","ig1.png",com.rtw.tmat.utils.Util.getFilePath(request, "ig1.png", "/resources/images/"));
						mailAttachment[4] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","li1.png",com.rtw.tmat.utils.Util.getFilePath(request, "li1.png", "/resources/images/"));
						mailAttachment[5] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","rtflogot.png",com.rtw.tmat.utils.Util.getFilePath(request, "rtflogot.png", "/resources/images/"));
						mailAttachment[6] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","p1.png",com.rtw.tmat.utils.Util.getFilePath(request, "p1.png", "/resources/images/"));
						mailAttachment[7] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","tw1.png",com.rtw.tmat.utils.Util.getFilePath(request, "tw1.png", "/resources/images/"));
						String email = customer.getEmail();
						//email = "msanghani@rightthisway.com";
						String bcc = "";
						bcc = "amit.raut@rightthisway.com,msanghani@righthtisway.com";
						try {
							mailManager.sendMailNow("text/html","sales@rewardthefan.com",email, 
									null,bcc, "You are successfully registered with RewardTheFan.com.",
						    		"mail-rewardfan-customer-registration.html", mailMap, "text/html", null,mailAttachment,null);
						} catch (Exception e) {
							e.printStackTrace();
							error.setDescription("Error occured while sending mail for Real Ticket Order Confirmation to customer.");
							genericResponseDTO.setError(error);
							genericResponseDTO.setStatus(0);
							return genericResponseDTO;
						}
					}
					
					genericResponseDTO.setMessage("Order created successfully, InvoiceId : "+invoice.getId());
					genericResponseDTO.setStatus(1);
					
					//Tracking User Action
					String userActionMsg = "Invoice Created Successfully.";
					Util.userActionAudit(request, invoice.getId(), userActionMsg);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Creating Invoice for Long(Real) Ticket.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
}