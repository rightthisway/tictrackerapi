package com.rtw.tracker.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.Artist;
import com.rtw.tracker.datas.Cards;
import com.rtw.tracker.datas.ChildCategory;
import com.rtw.tracker.datas.EventDetails;
import com.rtw.tracker.datas.FileValidator;
import com.rtw.tracker.datas.GrandChildCategory;
import com.rtw.tracker.datas.ParentCategory;
import com.rtw.tracker.datas.PopularArtist;
import com.rtw.tracker.datas.PopularEvents;
import com.rtw.tracker.datas.PopularGrandChildCategory;
import com.rtw.tracker.datas.PopularVenue;
import com.rtw.tracker.datas.Product;
import com.rtw.tracker.datas.ProductType;
import com.rtw.tracker.datas.Venue;
import com.rtw.tracker.enums.CardType;
import com.rtw.tracker.pojos.CardsDTO;
import com.rtw.tracker.pojos.GenericResponseDTO;
import com.rtw.tracker.pojos.PopularArtistScreenDTO;
import com.rtw.tracker.utils.Constants;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.JsonWrapperUtil;
import com.rtw.tracker.utils.PaginationUtil;
import com.rtw.tracker.utils.URLUtil;
import com.rtw.tracker.utils.Util;

@Controller
public class RewardTheFanController {
	
	@Autowired
	FileValidator fileValidator;

	public FileValidator getFileValidator() {
		return fileValidator;
	}
	public void setFileValidator(FileValidator fileValidator) {
		this.fileValidator = fileValidator;
	}
	
	@RequestMapping("/AutoCompleteGrandChildAndArtistAndVenue")
	public void getAutoCompleteGrandChildAndArtistAndVenue(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String param = request.getParameter("q");
//		String brokerId = request.getParameter("brokerId");
		PrintWriter writer =  response.getWriter();
		String strResponse ="";
		Collection<Artist> artists = DAORegistry.getArtistDAO().filterByName(param);
		/*Collection<Venue> venues = DAORegistry.getVenueDAO().filterByVenue(
				param);
		Collection<GrandChildCategory> grandChildCategories = DAORegistry
				.getGrandChildCategoryDAO().getGrandChildCategoriesByName(
						param);
		Collection<ChildCategory> childCategories = DAORegistry
				.getChildCategoryDAO().getChildTourCategoriesByName(param);*/

		//if (artists == null && venues == null && grandChildCategories == null) {
		if (artists == null) {
			return;
		}
		if (artists != null) {
			for (Artist artist : artists) {
				strResponse += ("ARTIST" + "|" + artist.getId() + "|" + artist.getName() + "\n") ;
			}
		}
		/*if (venues != null) {
			for (Venue venue : venues) {
				strResponse += ("VENUE" + "|" + venue.getId() + "|" + venue.getBuilding() + "\n");
			}
		}
		if (grandChildCategories != null) {
			for (GrandChildCategory grandChildCateogry : grandChildCategories) {
				strResponse += ("GRANDCHILD" + "|" + grandChildCateogry.getId() + "|" + grandChildCateogry.getName() + "\n");
			}
		}

		if (childCategories != null) {
			for (ChildCategory childCategory : childCategories) {
				strResponse += ("CHILD" + "|" + childCategory.getId() + "|" + childCategory.getName() + "\n");
			}
		}*/
		
		writer.write(strResponse);
	}
	
	@RequestMapping(value="/PopularEvents")
	public String populerEvents(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		
		try {
			String searchType = request.getParameter("searchType");
			String searchValue = request.getParameter("searchValue");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			Integer totalCount = 0;
			DateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			
			Collection<EventDetails> events = null;
			GridHeaderFilters filter = new GridHeaderFilters();
			if((fromDateStr == null || fromDateStr.isEmpty()) && (toDateStr == null || toDateStr.isEmpty()) && (searchValue == null || searchValue.isEmpty())){
				DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
				Calendar cal = Calendar.getInstance();
				Date fromDate = new Date();
				cal.add(Calendar.MONTH, 3);
				Date toDate = cal.getTime();
				fromDateStr = dateFormat.format(fromDate);
				toDateStr = dateFormat.format(toDate);
				String fromDateFinal = fromDateStr + " 00:00:00";
				String toDateFinal = toDateStr + " 23:59:59";
				//events = DAORegistry.getEventDetailsDAO().getAllActiveEventsByFilter(null, null, fromDate, toDate,ProductType.REWARDTHEFAN);
				events = DAORegistry.getQueryManagerDAO().getAllActiveEventDetailsByFilter(null, null, fromDateFinal, toDateFinal, ProductType.REWARDTHEFAN,filter,null);
				totalCount = DAORegistry.getQueryManagerDAO().getEventDetailsByFilterCount(null, null, fromDateFinal, toDateFinal, ProductType.REWARDTHEFAN,filter);
			}else{
				if((fromDateStr != null && !fromDateStr.isEmpty()) && (toDateStr != null && !toDateStr.isEmpty())){
					String fromDateFinal = fromDateStr + " 00:00:00";
					String toDateFinal = toDateStr + " 23:59:59";
					//events = DAORegistry.getEventDetailsDAO().getAllActiveEventsByFilter(searchType, searchValue, dateTimeFormat.parse(fromDateFinal), dateTimeFormat.parse(toDateFinal),ProductType.REWARDTHEFAN);
					events = DAORegistry.getQueryManagerDAO().getAllActiveEventDetailsByFilter(searchType, searchValue, fromDateFinal, toDateFinal,ProductType.REWARDTHEFAN,filter,null);
					totalCount = DAORegistry.getQueryManagerDAO().getEventDetailsByFilterCount(searchType, searchValue, fromDateFinal, toDateFinal,ProductType.REWARDTHEFAN,filter);
				}else{
					//events = DAORegistry.getEventDetailsDAO().getAllActiveEventsByFilter(searchType, searchValue, null, null,ProductType.REWARDTHEFAN);
					events = DAORegistry.getQueryManagerDAO().getAllActiveEventDetailsByFilter(searchType, searchValue, null, null,ProductType.REWARDTHEFAN,filter,null);
					totalCount = DAORegistry.getQueryManagerDAO().getEventDetailsByFilterCount(searchType, searchValue, null, null,ProductType.REWARDTHEFAN,filter);
				}
			}
			
			Product product = DAORegistry.getProductDAO().getProductByName(Constants.Reward_The_Fan_Product);			
			Collection<PopularEvents> popEvents = DAORegistry.getPopularEventsDAO().getAllActivePopularEventsByProductId(product.getId());
			
			map.put("popEvents", popEvents);
			map.put("eventsList", events);
			map.put("totalCount", totalCount);
			map.put("searchType", searchType);
			map.put("searchValue", searchValue);
			map.put("fromDate", fromDateStr);
			map.put("toDate", toDateStr);
			map.put("product", product);
			
		} catch(Exception e){
			e.printStackTrace();
		}
		//map.put("url", "RewardTheFan");
		map.put("productName", "Reward The Fan");
		return "page-popular-events";
	}
		
	@RequestMapping(value="/PopularArtist")
	public PopularArtistScreenDTO popularArtistPage(HttpServletRequest request, HttpServletResponse response, Model model){
		PopularArtistScreenDTO popularArtistScreenDTO = new PopularArtistScreenDTO();
		Error error = new Error();
		//model.addAttribute("popularArtistScreenDTO", popularArtistScreenDTO);
		
		try {
			Integer artistCount = 0;
			Integer popArtistCount = 0;
			GridHeaderFilters filter = new GridHeaderFilters();
			Product product = DAORegistry.getProductDAO().getProductByName(Constants.Reward_The_Fan_Product);
			Collection<PopularArtist> popArtists = DAORegistry.getQueryManagerDAO().getAllActivePopularArtistByProductId(product.getId(),filter,null);
			popArtistCount = DAORegistry.getQueryManagerDAO().getAllActivePopularArtistByProductIdCount(product.getId());
			
			List<PopularArtist> artistList = DAORegistry.getQueryManagerDAO().getAllArtist(filter, null);
			artistCount = DAORegistry.getQueryManagerDAO().getAllArtistCount(filter);
			
			List<PopularArtist> popularArtists = new ArrayList<PopularArtist>();
			Set<Integer> artistIds = new HashSet<Integer>();
			
			for(PopularArtist popArtist: popArtists){				
				if(artistIds.add(popArtist.getArtistId())){
					popularArtists.add(popArtist);
				}
			}	
			
			if(artistList == null || artistList.isEmpty()){
				popularArtistScreenDTO.setMessage("No Artists found for selected search filter.");
			}			
			popularArtistScreenDTO.setStatus(1);
			popularArtistScreenDTO.setPopularArtistList(Util.getAllArtistArray(popularArtists));
			popularArtistScreenDTO.setArtistList(Util.getAllArtistArray(artistList));
			popularArtistScreenDTO.setPaginationDTO(PaginationUtil.calculatePaginationParameter(artistCount, null));
			popularArtistScreenDTO.setPopularArtistPaginationDTO(PaginationUtil.getDummyPaginationParameters(popArtistCount));
			popularArtistScreenDTO.setProduct(product);
			popularArtistScreenDTO.setProductName("Reward The Fan");			
		} catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Popular Artists");
			popularArtistScreenDTO.setError(error);
			popularArtistScreenDTO.setStatus(0);
		}
		return popularArtistScreenDTO;
	}
	
	/*
	@RequestMapping(value="/GetPopularArtist")
	public String popularArtist(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		
		try {
			Product product = DAORegistry.getProductDAO().getProductByName(Constants.Reward_The_Fan_Product);
			Collection<PopularArtist> popArtists = DAORegistry.getQueryManagerDAO().getAllActivePopularArtistByProductId(product.getId());
			Collection<EventDetails> artistList = DAORegistry.getQueryManagerDAO().getArtistDetails();
			//Collection<Artist> artistList = DAORegistry.getArtistDAO().getAll();
			
			Collection<PopularArtist> popularArtists = new ArrayList<PopularArtist>();
			Set<Integer> artistIds = new HashSet<Integer>();
			
			for(PopularArtist popArtist: popArtists){				
				if(artistIds.add(popArtist.getArtistId())){
					popularArtists.add(popArtist);
				}
			}			
			map.put("popArtists", popularArtists);
			map.put("artistList", artistList);
			map.put("product", product);
			
		} catch(Exception e){
			e.printStackTrace();
		}
		//map.put("url", "RewardTheFan");
		map.put("productName", "Reward The Fan");
		return "page-popular-artist";
	}
	*/
	
	@RequestMapping(value="/ManageArtistForSearch")
	public GenericResponseDTO getArtistForManageSearch(HttpServletRequest request, HttpServletResponse response, Model model){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		//model.addAttribute("genericResponseDTO", genericResponseDTO);
		
		try {
			genericResponseDTO.setStatus(0);
			String action = request.getParameter("action");
			if(action != null && action.equalsIgnoreCase("update")){
				String artistIdStr = request.getParameter("artistIds");
				String isDisplay = request.getParameter("display");
				Boolean display = isDisplay!=null?Boolean.parseBoolean(isDisplay):true;
				//List<Artist> list = new ArrayList<Artist>();
				String[] artistIdsArr = null;
				if(artistIdStr!=null) {
					artistIdsArr = artistIdStr.split(",");
				}
				for (String artistStr : artistIdsArr) {
					Integer artistId= Integer.parseInt(artistStr);
					Artist artist = DAORegistry.getArtistDAO().get(artistId);
					artist.setIsDisplay(display);
					DAORegistry.getArtistDAO().update(artist);
				}
				genericResponseDTO.setStatus(1);
				
				//Tracking User Action
				String userActionMsg = "";
				if(display){
					userActionMsg = "Artist(s) are Make Visible.Artist Id's - "+artistIdStr;
				}else{
					userActionMsg = "Artist(s) are Make InVisible.Artist Id's - "+artistIdStr;
				}
				com.rtw.tmat.utils.Util.userActionAudit(request, null, userActionMsg);
			}
		} catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching/update Artists");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
	@RequestMapping(value="/PopularGrandChildCategory")
	public String popularGrandChildCategory(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		
		try {
			Product product = DAORegistry.getProductDAO().getProductByName(Constants.Reward_The_Fan_Product);
			Collection<PopularGrandChildCategory> popGrandChilds = DAORegistry.getPopularGrandChildCategoryDAO().getAllActivePopularGrandChildCategoryByProductId(product.getId());
			List<EventDetails> grandChilds = DAORegistry.getQueryManagerDAO().getGrandChildDetails();
		
			map.put("popGrandChilds", popGrandChilds);
			map.put("grandChildCategoryList", grandChilds);
			map.put("product", product);
			
		} catch(Exception e){
			e.printStackTrace();
		}
		//map.put("url", "RewardTheFan");
		map.put("productName", "Reward The Fan");
		return "page-popular-grand-child-category";
	}
	
	@RequestMapping(value="/PopularVenue")
	public String popularVenue(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		
		try {
			Product product = DAORegistry.getProductDAO().getProductByName(Constants.Reward_The_Fan_Product);
			Collection<PopularVenue> popVenues = DAORegistry.getPopularVenueDAO().getAllActivePopularVenueByProductId(product.getId());
			//Collection<EventDetails> venueList = DAORegistry.getQueryManagerDAO().getVenueDetails();
			Collection<Venue> venueList = DAORegistry.getQueryManagerDAO().getAllVenueDetails(null,null);
		
			map.put("popVenues", popVenues);
			map.put("venueList", venueList);
			map.put("product", product);
			
		} catch(Exception e){
			e.printStackTrace();
		}
		//map.put("url", "RewardTheFan");
		map.put("productName", "Reward The Fan");
		return "page-popular-venue";
	}
	
	@RequestMapping(value="/Cards")
	public CardsDTO cards(HttpServletRequest request, HttpServletResponse response, Model model){
		CardsDTO cardsDTO = new CardsDTO();
		Error error = new Error();
		//model.addAttribute("cardsDTO", cardsDTO);
		
		try{			 
			Product product = DAORegistry.getProductDAO().getProductByName(Constants.Reward_The_Fan_Product);
			List<Cards> cardsList = DAORegistry.getCardsDAO().getAllCardsByProductId(product.getId());				
			List<ParentCategory> parentCategoryList = DAORegistry.getParentCategoryDAO().getAll();
			List<ChildCategory> childCategoryList = DAORegistry.getChildCategoryDAO().getAll();
			List<GrandChildCategory> grandChildCategoryLsit = DAORegistry.getGrandChildCategoryDAO().getAll();
			
			if(cardsList == null) {
				cardsList = new ArrayList<Cards>();
			}
			
			cardsDTO.setStatus(1);
			cardsDTO.setParentCategoryList(parentCategoryList);
			cardsDTO.setChildCategoryList(childCategoryList);
			cardsDTO.setGrandChildCategoryLsit(grandChildCategoryLsit);
			cardsDTO.setCardsList(cardsList);
			cardsDTO.setProduct(product);
			cardsDTO.setInfo(request.getParameter("info"));
			cardsDTO.setProductName("Reward The Fan");	        
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Cards");
			cardsDTO.setError(error);
			cardsDTO.setStatus(0);
		}
		return cardsDTO;
	}
	
	@RequestMapping(value="/CardsUpdatePopup")
	public CardsDTO cardsUpdatePopup(HttpServletRequest request, HttpServletResponse response, Model model){
		CardsDTO cardsDTO = new CardsDTO();
		Error error = new Error();
		//model.addAttribute("cardsDTO", cardsDTO);
		
		try{
			Integer productId = Integer.parseInt(request.getParameter("productId"));
			String cardIdStr = request.getParameter("cardId");
			Integer cardId = null;
			Cards card = null;
			if(cardIdStr != null && cardIdStr.trim().length()>0) {
				cardId = Integer.parseInt(cardIdStr);
				card = DAORegistry.getCardsDAO().get(cardId);
			}
			
			if(card == null) {
				card = new Cards();
			}
			Product product = DAORegistry.getProductDAO().get(productId);
			
			List<ParentCategory> parentCategoryList = DAORegistry.getParentCategoryDAO().getAll();
			List<ChildCategory> childCategoryList = DAORegistry.getChildCategoryDAO().getAll();
			List<GrandChildCategory> grandChildCategoryLsit = DAORegistry.getGrandChildCategoryDAO().getAll();
			 
			cardsDTO.setStatus(1);
			cardsDTO.setParentCategoryList(parentCategoryList);
			cardsDTO.setChildCategoryList(childCategoryList);
			cardsDTO.setGrandChildCategoryLsit(grandChildCategoryLsit);
			cardsDTO.setCard(card);
			cardsDTO.setProduct(product);	        
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Cards Details");
			cardsDTO.setError(error);
			cardsDTO.setStatus(0);
		}
		return cardsDTO;
	}
	
	@RequestMapping(value = "/updateCards")
	 public GenericResponseDTO updateCards(Model model, @ModelAttribute("cards")Cards cards,
				BindingResult result, HttpServletRequest request, HttpServletResponse response) {
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		model.addAttribute("cards", "");
		
		try{
		String action = request.getParameter("action");
		String username = request.getParameter("userName");
		
		if(action != null) {
			if(action.equals("update")) {
				MultipartFile file = cards.getFile();
				MultipartFile file1 = cards.getMobileFile();
				String fileRequired = request.getParameter("fileRequired");
				String mobileFileRequired = request.getParameter("mobileFileRequired");
				String categoryTypes = request.getParameter("categoryType");
				String showEvents = request.getParameter("eventInfo");
				String noOfEvents = request.getParameter("noOfEvents");
				String userActionMsg = "";
				
				if(fileRequired != null && fileRequired.equals("Y")) {
					fileValidator.validateCards(cards, result);
					if (result.hasErrors()) {
						System.err.println("Please select valid file.");
						error.setDescription("Please select valid file.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}	
				}
				if(mobileFileRequired != null && mobileFileRequired.equals("Y")) {
					fileValidator.validateMobileImageCards(cards, result);
					if (result.hasErrors()) {
						System.err.println("Please select valid Mobile file.");
						error.setDescription("Please select valid Mobile file.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}	
				}
				
				if(cards.getCardType().equals(CardType.PROMOTION)){
					if(showEvents == null || showEvents.isEmpty()){
						System.err.println("Please select Show Event Data.");
						error.setDescription("Please select Show Event Data.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					if(showEvents != null && showEvents.equalsIgnoreCase("Yes")){
						if(noOfEvents == null || noOfEvents.isEmpty()){
							System.err.println("Please Enter No.Of Events.");
							error.setDescription("Please Enter No.Of Events.");
							genericResponseDTO.setError(error);
							genericResponseDTO.setStatus(0);
							return genericResponseDTO;
						}
						try{
							Integer noOfEvent = Integer.parseInt(noOfEvents);
						}catch(Exception e){
							e.printStackTrace();
							error.setDescription("Please Enter No.Of Events in Integer value.");
							genericResponseDTO.setError(error);
							genericResponseDTO.setStatus(0);
							return genericResponseDTO;
						}
					}
				}
				
				Product product = DAORegistry.getProductDAO().getProductByName(Constants.Reward_The_Fan_Product);
				
				if(categoryTypes != null && !categoryTypes.isEmpty()){
					String categoryType[] = categoryTypes.split("_");
					if(categoryType[0].equalsIgnoreCase("pc")){
						cards.setParentCategoryId(Integer.parseInt(categoryType[1]));
					}
					if(categoryType[0].equalsIgnoreCase("cc")){
						cards.setChildCategoryId(Integer.parseInt(categoryType[1]));
					}
					if(categoryType[0].equalsIgnoreCase("gc")){
						cards.setGrandChildCategoryId(Integer.parseInt(categoryType[1]));
					}
				}
				if(cards.getId()!= null) {
					userActionMsg = "Card Updated Successfully.";
					genericResponseDTO.setMessage("Card updated successfully.");
				} else {
					userActionMsg = "Card Added Successfully.";
					genericResponseDTO.setMessage("Card added successfully.");
				}
								
				cards.setLastUpdated(new Date());
				cards.setLastUpdatedBy(username);
				cards.setProductId(product.getId());
				
				if(cards.getCardType().equals(CardType.PROMOTION) || cards.getCardType().equals(CardType.YESNO) || cards.getCardType().equals(CardType.MAINPROMOTION)) {
					
					File newFile1 = null;
					if(mobileFileRequired != null && mobileFileRequired.equals("Y")) {
						if(cards.getCardType().equals(CardType.MAINPROMOTION)){
							if(cards.getMobileImageFileUrl() != null && !(cards.getMobileImageFileUrl()).isEmpty()) {
								String fullFileName = URLUtil.CARDS_DIRECTORY+cards.getMobileImageFileUrl();
								newFile1 = new File(fullFileName);
								if (newFile1.exists()) {
									newFile1.delete();
								}
							}							
						}
						DAORegistry.getCardsDAO().saveOrUpdate(cards);
						
						if(cards.getCardType().equals(CardType.MAINPROMOTION)){
							String ext1 = FilenameUtils.getExtension(file1.getOriginalFilename());
							String fileName1 = "rtwCard_MobileImage_" +cards.getId()+"."+ext1;
							String fullFileName1 = URLUtil.CARDS_DIRECTORY+fileName1;
							newFile1 = new File(fullFileName1);
							if (!newFile1.exists()) {
								newFile1.createNewFile();
							}
							System.out.println("file path...."+newFile1.getPath());
							BufferedOutputStream stream1 = new BufferedOutputStream(
									new FileOutputStream(newFile1));
					        FileCopyUtils.copy(file1.getInputStream(), stream1);
							stream1.close();	
							
							cards.setMobileImageFileUrl(fileName1);							
						}
						DAORegistry.getCardsDAO().updateCardImageUrl(cards);
					}
					if(fileRequired != null && fileRequired.equals("Y")){
						File newFile = null;
						if(cards.getImageFileUrl() != null && !(cards.getImageFileUrl()).isEmpty()) {
							String fullFileName = URLUtil.CARDS_DIRECTORY+cards.getImageFileUrl();
							newFile = new File(fullFileName);
							if (newFile.exists()) {
								newFile.delete();
							}
						}
												
						if(cards.getCardType().equals(CardType.PROMOTION)){
							if(showEvents != null && showEvents.equalsIgnoreCase("Yes")){
								cards.setShowEventData(true);
								cards.setNoOfEvents(Integer.parseInt(noOfEvents));
							}else if(showEvents != null && showEvents.equalsIgnoreCase("No")){
								cards.setShowEventData(false);
							}							
						}
						
						DAORegistry.getCardsDAO().saveOrUpdate(cards);
						
						String ext = FilenameUtils.getExtension(file.getOriginalFilename());
						String fileName = "rtwCard_" +cards.getId()+"."+ext;
						String fullFileName = URLUtil.CARDS_DIRECTORY+fileName;
						newFile = new File(fullFileName);
						if (!newFile.exists()) {
							newFile.createNewFile();
						}
						System.out.println("file path...."+newFile.getPath());
						BufferedOutputStream stream = new BufferedOutputStream(
								new FileOutputStream(newFile));
				        FileCopyUtils.copy(file.getInputStream(), stream);
						stream.close();	
						
						cards.setImageFileUrl(fileName);
												
						DAORegistry.getCardsDAO().updateCardImageUrl(cards);
					} 
					if((fileRequired == null || !fileRequired.equals("Y")) && (mobileFileRequired == null || !mobileFileRequired.equals("Y"))) {
						if(cards.getImageFileUrl() != null) {
							String fullFileName = URLUtil.CARDS_DIRECTORY+cards.getImageFileUrl();
							File newFile = new File(fullFileName);
							if (!newFile.exists()) {
								System.err.println("Please select valid file.");
								error.setDescription("Please select valid file.");
								genericResponseDTO.setError(error);
								genericResponseDTO.setStatus(0);
								return genericResponseDTO;
							}
						}
						
						if(cards.getCardType().equals(CardType.MAINPROMOTION)){
							if(cards.getMobileImageFileUrl() != null) {
								String fullFileName1 = URLUtil.CARDS_DIRECTORY+cards.getMobileImageFileUrl();
								File newFil1 = new File(fullFileName1);
								if (!newFil1.exists()) {
									System.err.println("Please select valid Mobile file.");
									error.setDescription("Please select valid Mobile file.");
									genericResponseDTO.setError(error);
									genericResponseDTO.setStatus(0);
									return genericResponseDTO;
								}
							}
						}
						
						if(cards.getCardType().equals(CardType.PROMOTION)){
							if(showEvents != null && showEvents.equalsIgnoreCase("Yes")){
								cards.setShowEventData(true);
								cards.setNoOfEvents(Integer.parseInt(noOfEvents));
							}else if(showEvents != null && showEvents.equalsIgnoreCase("No")){
								cards.setShowEventData(false);
							}							
						}
						DAORegistry.getCardsDAO().saveOrUpdate(cards);
					}
				} else {
					if(cards.getImageFileUrl() != null) {
						String fullFileName = URLUtil.CARDS_DIRECTORY+cards.getImageFileUrl();
						File newFile = new File(fullFileName);
						if (newFile.exists()) {
							newFile.delete();
						}
						cards.setImageFileUrl(null);
					}
					
					if(cards.getMobileImageFileUrl() != null) {
						String fullFileName1 = URLUtil.CARDS_DIRECTORY+cards.getMobileImageFileUrl();
						File newFile1 = new File(fullFileName1);
						if (newFile1.exists()) {
							newFile1.delete();
						}
						cards.setMobileImageFileUrl(null);
					}
					DAORegistry.getCardsDAO().saveOrUpdate(cards);
				}
				
				//genericResponseDTO.setCard(cards);				
				genericResponseDTO.setStatus(1);
				
				//Tracking User Action				
				com.rtw.tmat.utils.Util.userActionAudit(request, cards.getId(), userActionMsg);
				
			} else if(action.equals("delete")) {
				Cards cardDb = DAORegistry.getCardsDAO().get(cards.getId());
				if(cardDb != null) {
					if(cards.getImageFileUrl() != null) {
						String fullFileName = URLUtil.CARDS_DIRECTORY+cards.getImageFileUrl();
						File newFile = new File(fullFileName);
						if (newFile.exists()) {
							newFile.delete();
						}
						//cards.setImageFileUrl(null);
					}
					DAORegistry.getCardsDAO().delete(cardDb);
				}
				
				genericResponseDTO.setStatus(1);
				genericResponseDTO.setMessage("Card deleted successfully.");
				
				//Tracking User Action
				String userActionMsg = "Card Deleted Successfully.";
				com.rtw.tmat.utils.Util.userActionAudit(request, cards.getId(), userActionMsg);
			}
		}
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while Adding/Updating Cards.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
    @RequestMapping(value = "/GetImageFile")
    public void getFileUrl(HttpServletRequest request,HttpServletResponse response)
	throws IOException, ServletException {

		try {
			String fileName = request.getParameter("filePath");
			String type = request.getParameter("type");
			String fullFileName=null;
			
			if(type.equals("rtwCards")) {
				fullFileName = URLUtil.CARDS_DIRECTORY+fileName;
			} else if(type.equals("artistImage")) {
				fullFileName = URLUtil.ARTIST_DIRECTORY+fileName;
			} else if(type.equals("grandChildCategoryImage")) {
				fullFileName = URLUtil.GRANDCHILD_DIRECTORY+fileName;
			} else if(type.equals("fantasyGrandChildCategoryImage")) {
				fullFileName = URLUtil.FANTASY_DIRECTORY+fileName;
			} else if(type.equals("fantasyGrandChildCategoryMobileImage")) {
				fullFileName = URLUtil.FANTASY_DIRECTORY+fileName;
			} else if(type.equals("childCategoryImage")) {
				fullFileName = URLUtil.CHILD_DIRECTORY+fileName;
			} else if(type.equals("parentCategoryImage")) {
				fullFileName = URLUtil.PARENT_DIRECTORY+fileName;
			} else if(type.equals("loyalFanParentCategoryImage")) {
				fullFileName = URLUtil.LOYALFAN_DIRECTORY+fileName;
			}else if(type.equals("giftCardImage")) {
				fullFileName = URLUtil.GIFTCARD_IMAGE_DIRECTORY+fileName;
			}else {
				fullFileName=fileName;
			}
			
			String ext = FilenameUtils.getExtension(fileName);
			if(ext.equals("jpeg")|| ext.equals("jpg") || ext.equals("jpe")) {
				response.setContentType("image/jpeg");	
			}else if(ext.equals("gif")) {
				response.setContentType("image/gif");
			} else if(ext.equals("png")) {
				response.setContentType("image/png");
			} else {
				response.setContentType("image/"+ext);
			}
			
			response.setHeader("Pragma", "public");
			response.setHeader("Cache-Control", "max-age=0");
		
			File _RealFile = new File(fullFileName);
			
			if(_RealFile.exists()) {
				//_RealFile = new File("C:/cards/REWARDTHEFAN/" + cardPosition+".jpg");
				BufferedInputStream _Stream = new BufferedInputStream(new FileInputStream(_RealFile)) ;
				
				//byte[] imageBytes = IOUtils.toByteArray(_Stream);

				//response.setContentType("image/jpeg");
				//response.setContentLength(imageBytes.length);
				
				int value = 0;
				while( (value = _Stream.read()) != -1) {
					response.getOutputStream().write(value);
				}
				_Stream.close();
			}
			
			response.getOutputStream().flush();
			response.getOutputStream().close();

		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			response.getOutputStream().flush();
			response.getOutputStream().close();
		}

	}
    
}