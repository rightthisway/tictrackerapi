package com.rtw.tracker.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.rtw.tmat.dao.implementation.TMATDAORegistry;
import com.rtw.tmat.data.DefaultPurchasePrice;
import com.rtw.tmat.data.Event;
import com.rtw.tmat.data.ManagePurchasePrice;
import com.rtw.tmat.data.Ticket;
import com.rtw.tmat.enums.ProfitLossSign;
import com.rtw.tmat.utils.OpenOrderUtil;
import com.rtw.tmat.utils.Util;
import com.rtw.tmat.utils.Error;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.Artist;
import com.rtw.tracker.datas.CustomerAddress;
import com.rtw.tracker.datas.Customers;
import com.rtw.tracker.datas.EventDetails;
import com.rtw.tracker.datas.OpenOrderStatus;
import com.rtw.tracker.datas.OpenOrders;
import com.rtw.tracker.datas.SoldTicketDetail;
import com.rtw.tracker.datas.Venue;
import com.rtw.tracker.pojos.AutoCompleteArtistVenueDTO;
import com.rtw.tracker.pojos.ClosedOrderStatusDTO;
import com.rtw.tracker.pojos.GenericResponseDTO;
import com.rtw.tracker.pojos.GetEventsByVenueArtistDTO;
import com.rtw.tracker.pojos.OpenOrderStatusDTO;
import com.rtw.tracker.pojos.OrderNoteDTO;
import com.rtw.tracker.pojos.PastOrderStatusDTO;
import com.rtw.tracker.pojos.PendingReceiptOrderStatusDTO;
import com.rtw.tracker.pojos.PendingShipmentOrderStatusDTO;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.GridHeaderFiltersUtil;
import com.rtw.tracker.utils.JsonWrapperUtil;
import com.rtw.tracker.utils.PaginationUtil;

@Controller
public class DeliveriesController {
	
	
	public static DecimalFormat df = new DecimalFormat("#.##");
	
	@RequestMapping("/AutoCompleteArtistAndVenue")
	public AutoCompleteArtistVenueDTO getAutoCompleteArtistAndVenue(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		AutoCompleteArtistVenueDTO autoCompleteArtistVenueDTO = new AutoCompleteArtistVenueDTO();
		Error error = new Error();
		
		try {
			String param = request.getParameter("q");
			String strResponse = "";
			
			Collection<Artist> artists = DAORegistry.getArtistDAO().filterByName(param);
			if (artists != null) {
				for (Artist artist : artists) {
					strResponse += ("ARTIST" + "|" + artist.getId() + "|" + artist.getName() + "\n") ;
				}
			}
			Collection<Venue> venues = DAORegistry.getVenueDAO().filterByName(param);
			if (venues != null) {
				for (Venue venue : venues) {
					strResponse += ("VENUE" + "|" + venue.getId() + "|" + venue.getLocation() + "\n") ;
				}
			}
			autoCompleteArtistVenueDTO.setArtistVenue(strResponse);
			autoCompleteArtistVenueDTO.setStatus(1);
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Soomething went wrong while fetching Artist and Venue.");
			autoCompleteArtistVenueDTO.setError(error);
			autoCompleteArtistVenueDTO.setStatus(0);
		}
		return autoCompleteArtistVenueDTO;			
	}
	
	@RequestMapping("/GetEventsByVenueOrArtist")
	public GetEventsByVenueArtistDTO getEventsByVenueOrArtist(HttpServletRequest request, HttpServletResponse response){
		GetEventsByVenueArtistDTO getEventsByVenueArtistDTO = new GetEventsByVenueArtistDTO();
		Error error = new Error();
		
		try {
			String type = request.getParameter("type");
			String idStr = request.getParameter("id");
			
			if(StringUtils.isEmpty(type)){
				System.err.println("Please select Artist/Venue");
				error.setDescription("Please select Artist/Venue");
				getEventsByVenueArtistDTO.setError(error);
				getEventsByVenueArtistDTO.setStatus(0);
				return getEventsByVenueArtistDTO;
			}
			if(StringUtils.isEmpty(idStr)){
				System.err.println("Please select Artist/Venue Id");
				error.setDescription("Please select Artist/Venue Id");
				getEventsByVenueArtistDTO.setError(error);
				getEventsByVenueArtistDTO.setStatus(0);
				return getEventsByVenueArtistDTO;
			}
			Integer id = 0;
			try{
				id = Integer.parseInt(idStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please select Valid Artist/Venue Id");
				getEventsByVenueArtistDTO.setError(error);
				getEventsByVenueArtistDTO.setStatus(0);
				return getEventsByVenueArtistDTO;
			}
			
			Collection<EventDetails> events = null;
			if(type.equalsIgnoreCase("ARTIST")){
				events = DAORegistry.getEventDetailsDAO().getEventsByArtistCity(id,null);
			}else{
				events = DAORegistry.getEventDetailsDAO().getEventsByVenueId(id);
			}
			if(events!=null){
				getEventsByVenueArtistDTO.setEventDetails(com.rtw.tracker.utils.Util.getEventDetailsArray(events));
			}
			getEventsByVenueArtistDTO.setStatus(1);
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Artist/Venue from Event");
			getEventsByVenueArtistDTO.setError(error);
			getEventsByVenueArtistDTO.setStatus(0);
			return getEventsByVenueArtistDTO;
		}
		return getEventsByVenueArtistDTO;
	}
	
	@RequestMapping(value="/OpenOrders")
	public OpenOrderStatusDTO getOpenOrdersPage(HttpServletRequest request, HttpServletResponse response){
		OpenOrderStatusDTO openOrderStatusDTO = new OpenOrderStatusDTO();
		Error error = new Error();
		
		try {			
			String eventIdStr = request.getParameter("eventSelect");
			String artistVenueName = request.getParameter("artistVenueName");
			String artistIdStr = request.getParameter("artistId");
			String venueIdStr = request.getParameter("venueId");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String profitLossSign = request.getParameter("profitLoss");
			String productType = request.getParameter("productType");
			String product = request.getParameter("productTypeSession");
			String invoiceNo = request.getParameter("invoiceNo");
			String externalOrderId = request.getParameter("externalOrderId");
			String orderId = request.getParameter("orderId");
			String brokerIdStr = request.getParameter("brokerId");
			String sortingString = request.getParameter("sortingString");
			
			String action = request.getParameter("action");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				openOrderStatusDTO.setError(error);
				openOrderStatusDTO.setStatus(0);
				return openOrderStatusDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				openOrderStatusDTO.setError(error);
				openOrderStatusDTO.setStatus(0);
				return openOrderStatusDTO;
			}
			
			GridHeaderFilters filter = null;
			try{
				filter = GridHeaderFiltersUtil.getOpenOrShipmentPendingOrClosedOrderSearchHeaderFilters(headerFilter+sortingString);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				openOrderStatusDTO.setError(error);
				openOrderStatusDTO.setStatus(0);
				return openOrderStatusDTO;
			}
			
			Collection<OpenOrderStatus> openOrdersList = null;			
			Collection<OpenOrderStatus> list = null;
			Collection<EventDetails> events = null;
			String fromDateFinal = "";
			String toDateFinal="";			
			Integer count=0;
			Integer noOfOrders=0,sectionConsideredOrders=0,sectionNotConsideredOrders=0,totalSectionQty=0,
			zoneConsideredOrders=0,zoneNotConsideredOrders=0,totalZoneQty=0;
			Double sectionTotalNetSoldPrice=0.0,sectionTotalMarketPrice=0.0,sectionTotalActualSoldPrice=0.0,sectionTotalProfitLoss=0.0,
			zoneTotalNetSoldPrice=0.0,zoneTotalPrice=0.0,zoneTotalActualSoldPrice=0.0,zoneTotalProfitLoss=0.0;
			
			if(null == profitLossSign || profitLossSign.isEmpty()){
				profitLossSign = String.valueOf(ProfitLossSign.ALL);
			}			
			
			if(action!=null && action.equalsIgnoreCase("search")){
				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
				}else{
					fromDateFinal = null;
					toDateFinal = null;
				}				
			}else {
				if((fromDateStr != null && !fromDateStr.isEmpty()) && (toDateStr != null && !toDateStr.isEmpty())){
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
				}else{
					fromDateFinal = null;
					toDateFinal = null;
					
					productType = "ALL";
					
					if(product!=null && !product.isEmpty()){
						productType = product;
					}
				}
			}
			
			openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, 
					artistIdStr,venueIdStr,fromDateFinal, toDateFinal,ProfitLossSign.valueOf(profitLossSign.toUpperCase()),"OPEN",productType,filter,false,pageNo,brokerId);				
			list = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, 
					artistIdStr,venueIdStr,fromDateFinal, toDateFinal,ProfitLossSign.valueOf(profitLossSign.toUpperCase()),"OPEN",productType,filter,true,pageNo,brokerId);
			count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, 
					artistIdStr,venueIdStr, fromDateFinal, toDateFinal,ProfitLossSign.valueOf(profitLossSign.toUpperCase()),"OPEN",productType,filter,brokerId);
			
			if(artistIdStr!=null && !artistIdStr.isEmpty()){
				events =  DAORegistry.getEventDetailsDAO().getEventsByArtistCity(Integer.parseInt(artistIdStr),null);				
			}
			else if(venueIdStr!=null && !venueIdStr.isEmpty()){
				events = DAORegistry.getEventDetailsDAO().getEventsByVenueId(Integer.parseInt(venueIdStr));
			}
			
			if(list!=null && list.size() > 0){
				for (OpenOrderStatus openOrderStatus : list) {
					
					noOfOrders++;
					if(openOrderStatus.getSectionTixQty() > 0) {
						totalSectionQty +=openOrderStatus.getSoldQty();
						sectionTotalNetSoldPrice += openOrderStatus.getNetTotalSoldPrice();
						sectionTotalActualSoldPrice += openOrderStatus.getTotalActualSoldPrice();
						sectionTotalMarketPrice += openOrderStatus.getTotalMarketPrice();
						sectionTotalProfitLoss += openOrderStatus.getProfitAndLoss();
						sectionConsideredOrders++;
					} else {
						sectionNotConsideredOrders++;
					}
					
					if(openOrderStatus.getZoneTixQty() > 0) {
						totalZoneQty +=openOrderStatus.getSoldQty();
						zoneTotalNetSoldPrice += openOrderStatus.getNetTotalSoldPrice();
						zoneTotalActualSoldPrice += openOrderStatus.getTotalActualSoldPrice();
						zoneTotalPrice += openOrderStatus.getZoneTotalPrice();
						zoneTotalProfitLoss += openOrderStatus.getZoneProfitAndLoss();
						zoneConsideredOrders++;
					} else {
						zoneNotConsideredOrders++;
					}
				}
			}
			
			openOrderStatusDTO.setStatus(1);
			openOrderStatusDTO.setOpenOrders(com.rtw.tracker.utils.Util.getOpenOrdersArray(openOrdersList));
			openOrderStatusDTO.setOpenOrdersPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
			openOrderStatusDTO.setEventDetails(com.rtw.tracker.utils.Util.getEventDetailsArray(events));
			openOrderStatusDTO.setProfitLossSign(ProfitLossSign.values());
			openOrderStatusDTO.setSelectedProfitLossSign(ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()).toString());
			openOrderStatusDTO.setProductType(productType);
			openOrderStatusDTO.setNoOfOrders(noOfOrders);
			openOrderStatusDTO.setTotalSectionQty(totalSectionQty);	
			openOrderStatusDTO.setSectionTotalNetSoldPrice(df.format(sectionTotalNetSoldPrice));
			openOrderStatusDTO.setSectionTotalActualSoldPrice(df.format(sectionTotalActualSoldPrice));
			openOrderStatusDTO.setSectionTotalMarketPrice(df.format(sectionTotalMarketPrice));
			openOrderStatusDTO.setSectionTotalProfitLoss(df.format(sectionTotalProfitLoss));
			openOrderStatusDTO.setSectionConsideredOrders(sectionConsideredOrders);
			openOrderStatusDTO.setSectionNotConsideredOrders(sectionNotConsideredOrders);
			openOrderStatusDTO.setTotalZoneQty(totalZoneQty);
			openOrderStatusDTO.setZoneTotalNetSoldPrice(df.format(zoneTotalNetSoldPrice));
			openOrderStatusDTO.setZoneTotalActualSoldPrice(df.format(zoneTotalActualSoldPrice));
			openOrderStatusDTO.setZoneTotalPrice(df.format(zoneTotalPrice));
			openOrderStatusDTO.setZoneTotalProfitLoss(df.format(zoneTotalProfitLoss));
			openOrderStatusDTO.setZoneConsideredOrders(zoneConsideredOrders);
			openOrderStatusDTO.setZoneNotConsideredOrders(zoneNotConsideredOrders);
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Open Orders");
			openOrderStatusDTO.setError(error);
			openOrderStatusDTO.setStatus(0);
		}		
		return openOrderStatusDTO;
	}
	
	/*@RequestMapping(value="/GetOpenOrders")
	public OpenOrderStatusDTO getOpenOrders(HttpServletRequest request, HttpServletResponse response){
		OpenOrderStatusDTO openOrderStatusDTO = new OpenOrderStatusDTO();
		Error error = new Error();
		
		try {
			String eventIdStr = request.getParameter("eventSelect");
			String artistIdStr = request.getParameter("artistId");
			String venueIdStr = request.getParameter("venueId");
			String fromDateStr = request.getParameter("fromDate");			
			String toDateStr = request.getParameter("toDate");
			String profitLossSign = request.getParameter("profitLoss");
			String productType = request.getParameter("productType");
			
			String invoiceNo = request.getParameter("invoiceNo");
			String externalOrderId = request.getParameter("externalOrderId");
			String orderId = request.getParameter("orderId");
			String action = request.getParameter("action");

			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
						
			String brokerIdStr = request.getParameter("brokerId");
			
			if(StringUtils.isEmpty(pageNo)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				openOrderStatusDTO.setError(error);
				openOrderStatusDTO.setStatus(0);
				return openOrderStatusDTO;
			}
			try {
				Integer.parseInt(pageNo);
			} catch (Exception e) {
				System.err.println("Please provide valid PageNo.");
				error.setDescription("Please provide valid PageNo.");
				openOrderStatusDTO.setError(error);
				openOrderStatusDTO.setStatus(0);
				return openOrderStatusDTO;
			}
			
			GridHeaderFilters filter = null;
			try{
				filter = GridHeaderFiltersUtil.getOpenOrShipmentPendingOrClosedOrderSearchHeaderFilters(headerFilter);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				openOrderStatusDTO.setError(error);
				openOrderStatusDTO.setStatus(0);
				return openOrderStatusDTO;
			}
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				openOrderStatusDTO.setError(error);
				openOrderStatusDTO.setStatus(0);
				return openOrderStatusDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				openOrderStatusDTO.setError(error);
				openOrderStatusDTO.setStatus(0);
				return openOrderStatusDTO;
			}
			
			Collection<OpenOrderStatus> openOrdersList = null;
			String fromDateFinal = "";
			String toDateFinal = "";
			Integer count=0;
			
			Integer noOfOrders =0,sectionConsideredOrders=0,sectionNotConsideredOrders=0,totalSectionQty=0,
			zoneConsideredOrders=0,zoneNotConsideredOrders=0,totalZoneQty=0;
			Double sectionTotalNetSoldPrice=0.0,sectionTotalMarketPrice=0.0,sectionTotalActualSoldPrice=0.0,sectionTotalProfitLoss=0.0,
			zoneTotalNetSoldPrice=0.0,zoneTotalPrice=0.0,zoneTotalActualSoldPrice=0.0,zoneTotalProfitLoss=0.0;
			
			
			if(action!=null && action.equalsIgnoreCase("search")){
				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
					openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, 
							artistIdStr,venueIdStr,fromDateFinal, toDateFinal,ProfitLossSign.valueOf(profitLossSign.toUpperCase()),"OPEN",productType,filter,false,pageNo,brokerId);
					count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, 
							artistIdStr,venueIdStr, fromDateFinal, toDateFinal,ProfitLossSign.valueOf(profitLossSign.toUpperCase()),"OPEN",productType,filter,brokerId);
				}else{
					openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, 
							artistIdStr,venueIdStr, null, null,ProfitLossSign.valueOf(profitLossSign.toUpperCase()),"OPEN",productType,filter,false,pageNo,brokerId);
					count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, 
							artistIdStr,venueIdStr, null, null,ProfitLossSign.valueOf(profitLossSign.toUpperCase()),"OPEN",productType,filter,brokerId);
				}
			}
			else if((fromDateStr == null || fromDateStr.isEmpty()) && (toDateStr == null || toDateStr.isEmpty())){
				openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, 
						artistIdStr,venueIdStr, null, null,ProfitLossSign.valueOf(profitLossSign.toUpperCase()),"OPEN",productType,filter,false,pageNo,brokerId);
				count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, 
						artistIdStr,venueIdStr, null, null,ProfitLossSign.valueOf(profitLossSign.toUpperCase()),"OPEN",productType,filter,brokerId);
			}else{
				if((fromDateStr != null && !fromDateStr.isEmpty()) && (toDateStr != null && !toDateStr.isEmpty())){
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
					openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, 
							artistIdStr,venueIdStr, fromDateFinal, toDateFinal,ProfitLossSign.valueOf(profitLossSign.toUpperCase()),"OPEN",productType,filter,false,pageNo,brokerId);
					count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, 
							artistIdStr,venueIdStr, fromDateFinal, toDateFinal,ProfitLossSign.valueOf(profitLossSign.toUpperCase()),"OPEN",productType,filter,brokerId);
				}
			}
			
			
			Collection<OpenOrderStatus> list = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, artistIdStr, venueIdStr,
					fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.toUpperCase()), "OPEN", productType, filter,true,null,brokerId);
			if(list!=null && list.size() > 0){
				for (OpenOrderStatus openOrderStatus : list) {
					noOfOrders++;
					if(openOrderStatus.getSectionTixQty() > 0) {
						totalSectionQty +=openOrderStatus.getSoldQty();
						sectionTotalNetSoldPrice += openOrderStatus.getNetTotalSoldPrice();
						sectionTotalActualSoldPrice += openOrderStatus.getTotalActualSoldPrice();
						sectionTotalMarketPrice += openOrderStatus.getTotalMarketPrice();
						sectionTotalProfitLoss += openOrderStatus.getProfitAndLoss();
						sectionConsideredOrders++;
					} else {
						sectionNotConsideredOrders++;
					}
					
					if(openOrderStatus.getZoneTixQty() > 0) {
						totalZoneQty +=openOrderStatus.getSoldQty();
						zoneTotalNetSoldPrice += openOrderStatus.getNetTotalSoldPrice();
						zoneTotalActualSoldPrice += openOrderStatus.getTotalActualSoldPrice();
						zoneTotalPrice += openOrderStatus.getZoneTotalPrice();
						zoneTotalProfitLoss += openOrderStatus.getZoneProfitAndLoss();
						zoneConsideredOrders++;
					} else {
						zoneNotConsideredOrders++;
					}
				}				
			}
			
			openOrderStatusDTO.setStatus(1);
			openOrderStatusDTO.setOpenOrders(com.rtw.tracker.utils.Util.getOpenOrdersArray(openOrdersList));
			openOrderStatusDTO.setOpenOrdersPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
			openOrderStatusDTO.setNoOfOrders(noOfOrders);
			openOrderStatusDTO.setTotalSectionQty(totalSectionQty);	
			openOrderStatusDTO.setSectionTotalNetSoldPrice(df.format(sectionTotalNetSoldPrice));
			openOrderStatusDTO.setSectionTotalActualSoldPrice(df.format(sectionTotalActualSoldPrice));
			openOrderStatusDTO.setSectionTotalMarketPrice(df.format(sectionTotalMarketPrice));
			openOrderStatusDTO.setSectionTotalProfitLoss(df.format(sectionTotalProfitLoss));
			openOrderStatusDTO.setSectionConsideredOrders(sectionConsideredOrders);
			openOrderStatusDTO.setSectionNotConsideredOrders(sectionNotConsideredOrders);
			openOrderStatusDTO.setTotalZoneQty(totalZoneQty);
			openOrderStatusDTO.setZoneTotalNetSoldPrice(df.format(zoneTotalNetSoldPrice));
			openOrderStatusDTO.setZoneTotalActualSoldPrice(df.format(zoneTotalActualSoldPrice));
			openOrderStatusDTO.setZoneTotalPrice(df.format(zoneTotalPrice));
			openOrderStatusDTO.setZoneTotalProfitLoss(df.format(zoneTotalProfitLoss));
			openOrderStatusDTO.setZoneConsideredOrders(zoneConsideredOrders);
			openOrderStatusDTO.setZoneNotConsideredOrders(zoneNotConsideredOrders);
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Applying Search - Open Orders");
			openOrderStatusDTO.setError(error);
			openOrderStatusDTO.setStatus(0);
		}		
		return openOrderStatusDTO;
	}*/

	@RequestMapping(value = "/OpenOrderExportToExcel")
	public void openOrdersToExport(HttpServletRequest request, HttpServletResponse response) throws IOException{
		
		try {
			String eventIdStr = request.getParameter("eventSelect");
			String artistIdStr = request.getParameter("artistId");
			String venueIdStr = request.getParameter("venueId");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String profitLossSign = request.getParameter("profitLoss");
			String productType = request.getParameter("productType");
			
			String invoiceNo = request.getParameter("invoiceNo");
			String externalOrderId = request.getParameter("externalOrderId");
			String orderId = request.getParameter("orderId");
			String headerFilter = request.getParameter("headerFilter");
			String brokerIdStr = request.getParameter("brokerId");
			Integer brokerId = Integer.parseInt(brokerIdStr);
			
			GridHeaderFilters filter = GridHeaderFiltersUtil.getOpenOrShipmentPendingOrClosedOrderSearchHeaderFilters(headerFilter);
			List<OpenOrderStatus> openOrdersList = null;
			String fromDateFinal = "";
			String toDateFinal = "";			
			
			if(null ==  profitLossSign || profitLossSign.isEmpty()){
				profitLossSign = String.valueOf(ProfitLossSign.ALL);
			}
			if(fromDateStr!=null && !fromDateStr.isEmpty() && toDateStr!=null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";
				openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersToExport(invoiceNo,eventIdStr, 
						artistIdStr,venueIdStr, fromDateFinal, toDateFinal,ProfitLossSign.valueOf(profitLossSign.toUpperCase()),"OPEN",productType,externalOrderId,orderId,filter,brokerId);
			}else{
				openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersToExport(invoiceNo,eventIdStr, 
						artistIdStr,venueIdStr, null, null,ProfitLossSign.valueOf(profitLossSign.toUpperCase()),"OPEN",productType,externalOrderId,orderId,filter,brokerId);
			}
		
			if(openOrdersList!=null && !openOrdersList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("open_orders");
				
				int j=0;
				Row header = sheet.createRow((int)0);
				header.createCell(j).setCellValue("Product Type");
				j++;
				header.createCell(j).setCellValue("Invoice Id");
				j++;
				header.createCell(j).setCellValue("Event Name");
				j++;
				header.createCell(j).setCellValue("Event Date");
				j++;
				header.createCell(j).setCellValue("Event Time");
				j++;
				header.createCell(j).setCellValue("Venue");
				j++;
				header.createCell(j).setCellValue("City");
				j++;
				header.createCell(j).setCellValue("State");
				j++;
				header.createCell(j).setCellValue("Country");
				j++;
				header.createCell(j).setCellValue("Zone");
				j++;
				header.createCell(j).setCellValue("Section");
				j++;
				header.createCell(j).setCellValue("Row");
				j++;
				header.createCell(j).setCellValue("Quantity");
				j++;
				header.createCell(j).setCellValue("Section Tix Count");
				j++;
				header.createCell(j).setCellValue("Zone Tix Count");
				j++;
				header.createCell(j).setCellValue("Event Tix Count");
				j++;
				header.createCell(j).setCellValue("Net Sold Price");
				j++;
				header.createCell(j).setCellValue("Market Price");
				j++;
				header.createCell(j).setCellValue("Net Total Sold Price");
				j++;
				header.createCell(j).setCellValue("Profit/Loss");
				j++;
				header.createCell(j).setCellValue("Section Margin (%)");
				j++;
				header.createCell(j).setCellValue("Zone Cheapest Price");
				j++;
				header.createCell(j).setCellValue("Zone Total Price");
				j++;
				header.createCell(j).setCellValue("Zone P/L Value");
				j++;
				header.createCell(j).setCellValue("Zone Margin (%)");
				j++;
				header.createCell(j).setCellValue("Actual Sold Price");
				j++;
				header.createCell(j).setCellValue("Total Market Price");
				j++;
				header.createCell(j).setCellValue("Total Actual Sold Price");
				j++;
				
				header.createCell(j).setCellValue("Zone Tix Group Count");
				j++;
				header.createCell(j).setCellValue("Invoice Date");
				j++;
				header.createCell(j).setCellValue("Internal Notes");
				j++;
				
				header.createCell(j).setCellValue("Last Updated Price");
				j++;
				header.createCell(j).setCellValue("Price Updated Count");
				j++;
				header.createCell(j).setCellValue("Price");
				j++;
				header.createCell(j).setCellValue("Discount Coupen Price");
				j++;
				header.createCell(j).setCellValue("Url");
				j++;
				header.createCell(j).setCellValue("Shipping Method");
				j++;
				header.createCell(j).setCellValue("Tracking No");
				j++;
				header.createCell(j).setCellValue("Secondary Order Id");
				j++;
				header.createCell(j).setCellValue("Secondary Order Type");
				j++;
				header.createCell(j).setCellValue("Order Id");
				j++;
				header.createCell(j).setCellValue("Last Updated");
				j++;
				header.createCell(j).setCellValue("Broker Id");
				j++;
				header.createCell(j).setCellValue("Company Name");
				j++;
				header.createCell(j).setCellValue("Platform");
				j++;
				
				int i=1;
				for(OpenOrderStatus order : openOrdersList){
					Row row = sheet.createRow(i);
					j=0;
					row.createCell(j).setCellValue(order.getProductType());
					j++;
					row.createCell(j).setCellValue(order.getInvoiceNo());
					j++;
					row.createCell(j).setCellValue(order.getEventName());
					j++;
					row.createCell(j).setCellValue(order.getEventDateStr());
					j++;
					row.createCell(j).setCellValue(order.getEventTimeStr());
					j++;
					row.createCell(j).setCellValue(order.getVenueName());
					j++;
					row.createCell(j).setCellValue(order.getVenueCity());
					j++;
					row.createCell(j).setCellValue(order.getVenueState());
					j++;
					row.createCell(j).setCellValue(order.getVenueCountry());
					j++;
					row.createCell(j).setCellValue(order.getZone());
					j++;
					row.createCell(j).setCellValue(order.getSection());
					j++;
					row.createCell(j).setCellValue(order.getRow());
					j++;
					row.createCell(j).setCellValue(order.getSoldQty());
					j++;
					row.createCell(j).setCellValue(order.getSectionTixQty());
					j++;
					row.createCell(j).setCellValue(order.getZoneTixQty());
					j++;
					row.createCell(j).setCellValue(order.getEventTixQty());
					j++;
					row.createCell(j).setCellValue(order.getNetSoldPrice());
					j++;
					row.createCell(j).setCellValue(order.getMarketPrice());
					j++;
					row.createCell(j).setCellValue(order.getNetTotalSoldPrice());
					j++;
					row.createCell(j).setCellValue(order.getProfitAndLoss());
					j++;
					row.createCell(j).setCellValue(order.getSectionMargin());
					j++;
					row.createCell(j).setCellValue(order.getZoneCheapestPrice());
					j++;
					row.createCell(j).setCellValue(order.getZoneTotalPrice());
					j++;
					row.createCell(j).setCellValue(order.getZoneProfitAndLoss());
					j++;
					row.createCell(j).setCellValue(order.getZoneMargin());
					j++;
					row.createCell(j).setCellValue(order.getActualSoldPrice());
					j++;
					row.createCell(j).setCellValue(order.getTotalMarketPrice());
					j++;
					row.createCell(j).setCellValue(order.getTotalActualSoldPrice());
					j++;
					
					row.createCell(j).setCellValue(order.getZoneTixGroupCount());
					j++;
					row.createCell(j).setCellValue(order.getInvoiceDateStr());
					j++;
					row.createCell(j).setCellValue(order.getInternalNotes());
					j++;
					row.createCell(j).setCellValue(order.getLastUpdatedPrice());
					j++;
					row.createCell(j).setCellValue(order.getPriceUpdateCount());
					j++;
					row.createCell(j).setCellValue(order.getPrice()!=null?order.getPrice():0);
					j++;
					row.createCell(j).setCellValue(order.getDiscountCouponPrice()!=null?order.getDiscountCouponPrice():0);
					j++;
					row.createCell(j).setCellValue(order.getUrl()!=null?order.getUrl():"");
					j++;
					row.createCell(j).setCellValue(order.getShippingMethod());
					j++;
					row.createCell(j).setCellValue(order.getTrackingNo());
					j++;
					row.createCell(j).setCellValue(order.getSecondaryOrderId());
					j++;
					row.createCell(j).setCellValue(order.getSecondaryOrderType());
					j++;
					row.createCell(j).setCellValue(order.getOrderId()!=null?order.getOrderId():0);
					j++;
					row.createCell(j).setCellValue(order.getLastUpdateStr());
					j++;
					row.createCell(j).setCellValue(order.getBrokerId()!=null?order.getBrokerId().toString():"");
					j++;
					row.createCell(j).setCellValue(order.getCompanyName()!=null?order.getCompanyName():"");
					j++;
					if(order.getPlatform()!=null && order.getPlatform().equalsIgnoreCase("DESKTOP_SITE")){						
						row.createCell(j).setCellValue("WEB_ORDER");
					}else if(order.getPlatform()!=null && order.getPlatform().equalsIgnoreCase("TICK_TRACKER")){						
						row.createCell(j).setCellValue("RETAIL_ORDER");
					}else{
						row.createCell(j).setCellValue(order.getPlatform()!=null ? order.getPlatform() : "");
					}
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=open_orders.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "/UpdateOpenOrder")
	public GenericResponseDTO updateOpenOrder(HttpServletRequest request, HttpServletResponse response) throws IOException{
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
				
		try {			
			String []requestParams = request.getParameter("updateOrderrecords").split("_");
			Integer id = 0;
			Timestamp date = new Timestamp(new java.util.Date().getTime());
			for (String keyString : requestParams) {
				 String []record = keyString.split(",");
				 if(record.length == 4 && !record[0].isEmpty() && !record[1].isEmpty() && !record[2].isEmpty() && !record[3].isEmpty()){
					 id=Integer.parseInt(record[0]);
					 OpenOrderStatus openOrderStatusId = DAORegistry.getOpenOrderStatusDao().get(id);
					 if (openOrderStatusId != null) {
						openOrderStatusId.setPrice(Double.parseDouble(record[1]));
						openOrderStatusId.setDiscountCouponPrice(Double.parseDouble(record[2]));
						openOrderStatusId.setUrl(record[3]);
						Double price = Double.parseDouble(record[1]);
						if(price != null){
							openOrderStatusId.setLastUpdatedPriceDate(date);
						}else{
							openOrderStatusId.setLastUpdatedPriceDate(null);
						}
							
						DAORegistry.getOpenOrderStatusDao().update(openOrderStatusId);
					} 
				 }else if(record.length == 5 && record[0].trim().isEmpty() && !record[1].isEmpty() && !record[2].isEmpty() && !record[3].isEmpty()){
					 id=Integer.parseInt(record[1]);
					 OpenOrderStatus openOrderStatusId = DAORegistry.getOpenOrderStatusDao().get(id);
					 if (openOrderStatusId != null) {
						openOrderStatusId.setPrice(Double.parseDouble(record[2]));
						openOrderStatusId.setDiscountCouponPrice(Double.parseDouble(record[3]));
						openOrderStatusId.setUrl(record[4]);
						Double price = Double.parseDouble(record[1]);
						if(price != null){
							openOrderStatusId.setLastUpdatedPriceDate(date);
						}else{
							openOrderStatusId.setLastUpdatedPriceDate(null);
						}
							
						DAORegistry.getOpenOrderStatusDao().update(openOrderStatusId);
					} 
				 }
			}
			
			genericResponseDTO.setStatus(1);
			genericResponseDTO.setMessage("Selected record Updated.");
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Updating Open Orders");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);			
		}
		return genericResponseDTO;
	}
	
	@RequestMapping(value="/RefreshOpenOrder")
	public GenericResponseDTO refreshOpenOrder(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		
		try {
			Collection<SoldTicketDetail> ticketDetail =  new ArrayList<SoldTicketDetail>();
			ticketDetail = DAORegistry.getQueryManagerDAO().getAllSoldUnfilledNewTickets();
			OpenOrderUtil.addNewOpenOrders(ticketDetail);

			genericResponseDTO.setStatus(1);
			genericResponseDTO.setMessage("OK");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return genericResponseDTO;
	}
	
	@RequestMapping(value = "/GetOrderNote")
	public OrderNoteDTO getOrderNote(HttpServletRequest request, HttpServletResponse response){
		OrderNoteDTO orderNoteDTO = new OrderNoteDTO();
		Error error = new Error();
		
		try{
			String openOrderIdStr = request.getParameter("openOrderId");
			
			if(StringUtils.isEmpty(openOrderIdStr)){
				System.err.println("Please select Order.");
				error.setDescription("Please select Order.");
				orderNoteDTO.setError(error);
				orderNoteDTO.setStatus(0);
				return orderNoteDTO;
			}
			Integer openOrderId = 0;
			try{
				openOrderId = Integer.parseInt(openOrderIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please select Valid Order.");
				orderNoteDTO.setError(error);
				orderNoteDTO.setStatus(0);
				return orderNoteDTO;
			}
			
			OpenOrderStatus openOrder = DAORegistry.getOpenOrderStatusDao().get(openOrderId);
			if(openOrder == null){
				error.setDescription("Selected Order not found.");
				orderNoteDTO.setError(error);
				orderNoteDTO.setStatus(0);
				return orderNoteDTO;
			}
			
			orderNoteDTO.setStatus(1);
			orderNoteDTO.setOrderId(openOrder.getId());
			orderNoteDTO.setInternalNote(openOrder.getInternalNotes());
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Order Note.");
			orderNoteDTO.setError(error);
			orderNoteDTO.setStatus(0);
			return orderNoteDTO;
		}
		return orderNoteDTO;
	}
	
	@RequestMapping(value = "/UpdateOrderNote")
	public OrderNoteDTO updateOrderNote(HttpServletRequest request, HttpServletResponse response){
		OrderNoteDTO orderNoteDTO = new OrderNoteDTO();
		Error error = new Error();
		
		try{
			String openOrderIdStr = request.getParameter("modifyNotesOpenOrderId");
			String openOrderNote = request.getParameter("modifyNotesOpenOrderNote");
			String action = request.getParameter("action");
						
			if(StringUtils.isEmpty(openOrderIdStr)){
				System.err.println("Please select Order.");
				error.setDescription("Please select Order.");
				orderNoteDTO.setError(error);
				orderNoteDTO.setStatus(0);
				return orderNoteDTO;
			}
			Integer openOrderId = 0;
			try{
				openOrderId = Integer.parseInt(openOrderIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please select Valid Order.");
				orderNoteDTO.setError(error);
				orderNoteDTO.setStatus(0);
				return orderNoteDTO;
			}
			
			OpenOrderStatus openOrder = null;
			openOrder = DAORegistry.getOpenOrderStatusDao().get(openOrderId);
			if(openOrder == null){
				error.setDescription("Selected Order not found.");
				orderNoteDTO.setError(error);
				orderNoteDTO.setStatus(0);
				return orderNoteDTO;
			}
			
			if(action!=null && action.equalsIgnoreCase("update")){
				openOrder.setInternalNotes(openOrderNote);
				try{
					DAORegistry.getOpenOrderStatusDao().update(openOrder);
				}catch(Exception e){
					e.printStackTrace();
					error.setDescription("Error Occured while Updating Internal Note.");
					orderNoteDTO.setError(error);
					orderNoteDTO.setStatus(0);
					return orderNoteDTO;
				}
				orderNoteDTO.setMessage("Notes updated Successfully.");				
			}				
			orderNoteDTO.setStatus(1);
			
			//Tracking User Action
			String userActionMsg = "Open Order Notes Updated Successfully.";
			Util.userActionAudit(request, openOrderId, userActionMsg);
			
		}catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Updating Internal Note.");
			orderNoteDTO.setError(error);
			orderNoteDTO.setStatus(0);
		}
		return orderNoteDTO;
	}
	
		
	@RequestMapping(value="/PendingShipmentOrders")
	public PendingShipmentOrderStatusDTO getShipmentPendingOrdersPage(HttpServletRequest request, HttpServletResponse response){
		PendingShipmentOrderStatusDTO pendingShipmentOrderStatusDTO = new PendingShipmentOrderStatusDTO();
		Error error = new Error();
		
		try {
			String eventIdStr = request.getParameter("eventSelect");
			String artistVenueName = request.getParameter("artistVenueName");
			String artistIdStr = request.getParameter("artistId");
			String venueIdStr = request.getParameter("venueId");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String profitLossSign = request.getParameter("profitLoss");
			String companyProduct = request.getParameter("productType");
			String product = request.getParameter("productTypeSession");
			String invoiceNo = request.getParameter("invoiceNo");
			String externalOrderId = request.getParameter("externalOrderId");
			String orderId = request.getParameter("orderId");
			String sortingString = request.getParameter("sortingString");
			String brokerIdStr = request.getParameter("brokerId");
			
			String action = request.getParameter("action");			
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");			
						
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				pendingShipmentOrderStatusDTO.setError(error);
				pendingShipmentOrderStatusDTO.setStatus(0);
				return pendingShipmentOrderStatusDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				pendingShipmentOrderStatusDTO.setError(error);
				pendingShipmentOrderStatusDTO.setStatus(0);
				return pendingShipmentOrderStatusDTO;
			}
			
			GridHeaderFilters filter = null;
			try{
				filter = GridHeaderFiltersUtil.getOpenOrShipmentPendingOrClosedOrderSearchHeaderFilters(headerFilter+sortingString);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				pendingShipmentOrderStatusDTO.setError(error);
				pendingShipmentOrderStatusDTO.setStatus(0);
				return pendingShipmentOrderStatusDTO;
			}
			
			Collection<OpenOrderStatus> shipmentpendingOrders = null;
			Collection<OpenOrderStatus> shipmentPendingOrderList = null;
			Collection<EventDetails> events = null;
			String fromDateFinal = "";
			String toDateFinal = "";
			Integer count=0;
			
			if(null ==  profitLossSign || profitLossSign.isEmpty()){
				profitLossSign = String.valueOf(ProfitLossSign.ALL);
			}
			
			if(action!=null && action.equalsIgnoreCase("search")){
				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
				}else{
					fromDateFinal = null;
					toDateFinal = null;
				}				
			}else{
				companyProduct = "REWARDTHEFAN";
				
				if(product!=null && !product.isEmpty()){
					companyProduct = product;
				}
			}
			if(companyProduct.equalsIgnoreCase("REWARDTHEFAN") || companyProduct.equalsIgnoreCase("ALL")){ 
				companyProduct = "REWARDTHEFAN";
				shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING SHIPMENT",companyProduct,filter,false,pageNo,brokerId);
				//count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING SHIPMENT",companyProduct,filter,brokerId);
			}else{
				shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getRTWRTW2FilledOrders(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()), "PENDING", companyProduct, filter,pageNo);
				count = DAORegistry.getQueryManagerDAO().getRTWRTW2FilledOrdersCount(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()), "PENDING", companyProduct, filter);
			}
			
			if(artistIdStr!=null && !artistIdStr.isEmpty()){
				events =  DAORegistry.getEventDetailsDAO().getEventsByArtistCity(Integer.parseInt(artistIdStr),null);
				
			}else if(venueIdStr!=null && !venueIdStr.isEmpty()){
				events = DAORegistry.getEventDetailsDAO().getEventsByVenueId(Integer.parseInt(venueIdStr));
			}
			
			//Remove Duplicate Invoices
			Map<Integer, OpenOrderStatus> openOrderMap  = new HashMap<Integer, OpenOrderStatus>();
			for(OpenOrderStatus orderObj : shipmentpendingOrders){

				OpenOrderStatus openOrderStatusOldObj = openOrderMap.get(orderObj.getInvoiceNo());
				
				if(null != openOrderStatusOldObj){
					
					String section = openOrderStatusOldObj.getSection();
					
					if(section != null && !section.isEmpty()){
						String[] sectionArray = null;
						if(section.contains(",")){
							sectionArray = section.split(",");
						}else{
							sectionArray = new String[1];
							sectionArray[0] = section;
						}
						boolean isNewSection = true;
						for (String oldSection : sectionArray) {
							if(oldSection.equals(orderObj.getSection())){
								isNewSection = false;
								break;
							}
						}
						
						if(isNewSection){
							orderObj.setSection(section+","+orderObj.getSection());
						}
					}
					
					String row = openOrderStatusOldObj.getRow();
					
					if(row != null && !row.isEmpty()){
						String[] rowArray = null;
						if(row.contains(",")){
							rowArray = row.split(",");
						}else{
							rowArray = new String[1];
							rowArray[0] = row;
						}
						boolean isNewRow = true;
						for (String oldRow : rowArray) {
							if(oldRow.equals(orderObj.getRow())){
								isNewRow = false;
								break;
							}
						}
						
						if(isNewRow){
							orderObj.setRow(row+","+orderObj.getRow());
						}
					}
				} 
				openOrderMap.put(orderObj.getInvoiceNo(), orderObj);
			}
			shipmentPendingOrderList = openOrderMap.values();
			if(shipmentPendingOrderList != null && shipmentPendingOrderList.size() > 0){
				count = shipmentPendingOrderList.size();
			}
			
			
			pendingShipmentOrderStatusDTO.setStatus(1);
			pendingShipmentOrderStatusDTO.setPendingShipmentOrders(com.rtw.tracker.utils.Util.getOpenOrdersArray(shipmentPendingOrderList));
			pendingShipmentOrderStatusDTO.setPendingShipmentOrdersPaginationDTO(PaginationUtil.getDummyPaginationParameters(count));
			pendingShipmentOrderStatusDTO.setEventDetails(com.rtw.tracker.utils.Util.getEventDetailsArray(events));
			pendingShipmentOrderStatusDTO.setProfitLossSign(ProfitLossSign.values());
			pendingShipmentOrderStatusDTO.setSelectedProfitLossSign(ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()).toString());
			pendingShipmentOrderStatusDTO.setProductType(companyProduct);
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Pending Shipment Orders.");
			pendingShipmentOrderStatusDTO.setError(error);
			pendingShipmentOrderStatusDTO.setStatus(0);			
		}
		return pendingShipmentOrderStatusDTO;
	}
	
	/*@RequestMapping(value="/GetPendingShipmentOrders")
	public PendingShipmentOrderStatusDTO getShipmentPendingOrders(HttpServletRequest request, HttpServletResponse response){
		PendingShipmentOrderStatusDTO pendingShipmentOrderStatusDTO = new PendingShipmentOrderStatusDTO();
		Error error = new Error();
				
		try {
			String eventIdStr = request.getParameter("eventSelect");
			String artistIdStr = request.getParameter("artistId");
			String venueIdStr = request.getParameter("venueId");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");			
			String profitLossSign = request.getParameter("profitLoss");
			String companyProduct = request.getParameter("productType");
			
			String invoiceNo = request.getParameter("invoiceNo");
			String externalOrderId = request.getParameter("externalOrderId");
			String orderId = request.getParameter("orderId");
			String action = request.getParameter("action");
			
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");			
			
			String brokerIdStr = request.getParameter("brokerId");
			
			if(StringUtils.isEmpty(pageNo)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				pendingShipmentOrderStatusDTO.setError(error);
				pendingShipmentOrderStatusDTO.setStatus(0);
				return pendingShipmentOrderStatusDTO;
			}
			try {
				Integer.parseInt(pageNo);
			} catch (Exception e) {
				System.err.println("Please provide valid PageNo.");
				error.setDescription("Please provide valid PageNo.");
				pendingShipmentOrderStatusDTO.setError(error);
				pendingShipmentOrderStatusDTO.setStatus(0);
				return pendingShipmentOrderStatusDTO;
			}
			
			GridHeaderFilters filter = null;
			try{
				filter = GridHeaderFiltersUtil.getOpenOrShipmentPendingOrClosedOrderSearchHeaderFilters(headerFilter);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				pendingShipmentOrderStatusDTO.setError(error);
				pendingShipmentOrderStatusDTO.setStatus(0);
				return pendingShipmentOrderStatusDTO;
			}
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				pendingShipmentOrderStatusDTO.setError(error);
				pendingShipmentOrderStatusDTO.setStatus(0);
				return pendingShipmentOrderStatusDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				pendingShipmentOrderStatusDTO.setError(error);
				pendingShipmentOrderStatusDTO.setStatus(0);
				return pendingShipmentOrderStatusDTO;
			}
			
			Collection<OpenOrderStatus> shipmentpendingOrders = null;
			String fromDateFinal = "";
			String toDateFinal = "";
			Integer count=0;
			
			if(action!=null && action.equalsIgnoreCase("search")){
				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
					if(companyProduct.equalsIgnoreCase("REWARDTHEFAN")){
						shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING SHIPMENT",companyProduct,filter,false,pageNo,brokerId);
						count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING SHIPMENT",companyProduct,filter,brokerId);
					}else{
						shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getRTWRTW2FilledOrders(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()), "PENDING", companyProduct, filter,pageNo);
						count = DAORegistry.getQueryManagerDAO().getRTWRTW2FilledOrdersCount(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()), "PENDING", companyProduct, filter);
					}
				}else{
					if(companyProduct.equalsIgnoreCase("REWARDTHEFAN")){
						shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING SHIPMENT",companyProduct,filter,false,pageNo,brokerId);
						count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING SHIPMENT",companyProduct,filter,brokerId);
					}else{
						shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getRTWRTW2FilledOrders(eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()), "PENDING", companyProduct, filter,pageNo);
						count = DAORegistry.getQueryManagerDAO().getRTWRTW2FilledOrdersCount(eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()), "PENDING", companyProduct, filter);
					}
					
				}
			}
			
			pendingShipmentOrderStatusDTO.setStatus(1);
			pendingShipmentOrderStatusDTO.setPendingShipmentOrders(com.rtw.tracker.utils.Util.getOpenOrdersArray(shipmentpendingOrders));
			pendingShipmentOrderStatusDTO.setPendingShipmentOrdersPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Applying Search - Pending Shipment Orders.");
			pendingShipmentOrderStatusDTO.setError(error);
			pendingShipmentOrderStatusDTO.setStatus(0);			
		}
		return pendingShipmentOrderStatusDTO;
	}*/
		
	@RequestMapping(value="/PendingShipmentExportToExcel")
	public void getShipmentPendingOrdersToExport(HttpServletRequest request, HttpServletResponse response){
		
		try {
			String eventIdStr = request.getParameter("eventSelect");
			String artistIdStr = request.getParameter("artistId");
			String venueIdStr = request.getParameter("venueId");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String profitLossSign = request.getParameter("profitLoss");
			String companyProduct = request.getParameter("productType");

			String invoiceNo = request.getParameter("invoiceNo");
			String externalOrderId = request.getParameter("externalOrderId");
			String orderId = request.getParameter("orderId");
			String headerFilter = request.getParameter("headerFilter");
			String brokerIdStr = request.getParameter("brokerId");
			Integer brokerId = Integer.parseInt(brokerIdStr);
			
			GridHeaderFilters filter = GridHeaderFiltersUtil.getOpenOrShipmentPendingOrClosedOrderSearchHeaderFilters(headerFilter);
			
			Collection<OpenOrderStatus> shipmentpendingOrders = null;
			Collection<OpenOrderStatus> shipmentPendingOrderList = null;
			String fromDateFinal = "";
			String toDateFinal = "";
			
			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";
				if(companyProduct.equalsIgnoreCase("REWARDTHEFAN")){
					shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersToExport(invoiceNo,eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING SHIPMENT",companyProduct,externalOrderId,orderId,filter,brokerId);
				}else{
					shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getRTWRTW2OrdersToExport(invoiceNo,eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING",companyProduct,externalOrderId,orderId,filter);
				}
			}else{
				if(companyProduct.equalsIgnoreCase("REWARDTHEFAN")){
					shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersToExport(invoiceNo,eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING SHIPMENT",companyProduct,externalOrderId,orderId,filter,brokerId);
				}else{
					shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getRTWRTW2OrdersToExport(invoiceNo,eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING",companyProduct,externalOrderId,orderId,filter);
				}
			}
			
			//Remove Duplicate Invoices
			Map<Integer, OpenOrderStatus> openOrderMap  = new HashMap<Integer, OpenOrderStatus>();
			for(OpenOrderStatus orderObj : shipmentpendingOrders){

				OpenOrderStatus openOrderStatusOldObj = openOrderMap.get(orderObj.getInvoiceNo());
				
				if(null != openOrderStatusOldObj){
					
					String section = openOrderStatusOldObj.getSection();
					
					if(section != null && !section.isEmpty()){
						String[] sectionArray = null;
						if(section.contains(",")){
							sectionArray = section.split(",");
						}else{
							sectionArray = new String[1];
							sectionArray[0] = section;
						}
						boolean isNewSection = true;
						for (String oldSection : sectionArray) {
							if(oldSection.equals(orderObj.getSection())){
								isNewSection = false;
								break;
							}
						}
						
						if(isNewSection){
							orderObj.setSection(section+","+orderObj.getSection());
						}
					}
					
					String row = openOrderStatusOldObj.getRow();
					
					if(row != null && !row.isEmpty()){
						String[] rowArray = null;
						if(row.contains(",")){
							rowArray = row.split(",");
						}else{
							rowArray = new String[1];
							rowArray[0] = row;
						}
						boolean isNewRow = true;
						for (String oldRow : rowArray) {
							if(oldRow.equals(orderObj.getRow())){
								isNewRow = false;
								break;
							}
						}
						
						if(isNewRow){
							orderObj.setRow(row+","+orderObj.getRow());
						}
					}
				} 
				openOrderMap.put(orderObj.getInvoiceNo(), orderObj);
			}
			shipmentPendingOrderList = openOrderMap.values();
			
			if(shipmentPendingOrderList!=null && !shipmentPendingOrderList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("pending_shipment_orders");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Invoice Id");
				header.createCell(1).setCellValue("Invoice Date");
				header.createCell(2).setCellValue("Event Name");
				header.createCell(3).setCellValue("Event Date");
				header.createCell(4).setCellValue("Event Time");
				header.createCell(5).setCellValue("Venue");
				header.createCell(6).setCellValue("City");
				header.createCell(7).setCellValue("State");
				header.createCell(8).setCellValue("Country");
				header.createCell(9).setCellValue("Product Type");
				header.createCell(10).setCellValue("Internal Notes");
				header.createCell(11).setCellValue("Quantity");
				header.createCell(12).setCellValue("Section");
				header.createCell(13).setCellValue("Row");
				header.createCell(14).setCellValue("Sold Price");
				header.createCell(15).setCellValue("Market Price");
				header.createCell(16).setCellValue("Last Updated Price");
				header.createCell(17).setCellValue("Section Tix Count");
				header.createCell(18).setCellValue("Event Tix Count");
				header.createCell(19).setCellValue("Total Sold Price");
				header.createCell(20).setCellValue("Total Market Price");
				header.createCell(21).setCellValue("Profit/Loss");
				header.createCell(22).setCellValue("Price Updated Count");
				header.createCell(23).setCellValue("Price");
				header.createCell(24).setCellValue("Discount Coupen Price");
				header.createCell(25).setCellValue("Url");
				header.createCell(26).setCellValue("Shipping Method");
				header.createCell(27).setCellValue("Tracking No");
				header.createCell(28).setCellValue("Secondary Order Id");
				header.createCell(29).setCellValue("Secondary Order Type");
				header.createCell(30).setCellValue("Order Id");
				header.createCell(31).setCellValue("Last Updated");
				header.createCell(32).setCellValue("Broker Id");
				header.createCell(33).setCellValue("Company Name");
				header.createCell(34).setCellValue("Platform");
				
				int i=1;
				for(OpenOrderStatus order : shipmentPendingOrderList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(order.getInvoiceNo());
					row.createCell(1).setCellValue(order.getInvoiceDateStr());
					row.createCell(2).setCellValue(order.getEventName());
					row.createCell(3).setCellValue(order.getEventDateStr());
					row.createCell(4).setCellValue(order.getEventTimeStr());
					row.createCell(5).setCellValue(order.getVenueName());
					row.createCell(6).setCellValue(order.getVenueCity());
					row.createCell(7).setCellValue(order.getVenueState());
					row.createCell(8).setCellValue(order.getVenueCountry());
					row.createCell(9).setCellValue(order.getProductType());
					row.createCell(10).setCellValue(order.getInternalNotes());
					row.createCell(11).setCellValue(order.getSoldQty());
					row.createCell(12).setCellValue(order.getSection());
					row.createCell(13).setCellValue(order.getRow());
					row.createCell(14).setCellValue(order.getActualSoldPrice());
					row.createCell(15).setCellValue(order.getMarketPrice()!=null?String.valueOf(order.getMarketPrice()):"");
					row.createCell(16).setCellValue(order.getLastUpdatedPrice()!=null?String.valueOf(order.getLastUpdatedPrice()):"");
					row.createCell(17).setCellValue(order.getSectionTixQty()!=null?String.valueOf(order.getSectionTixQty()):"");
					row.createCell(18).setCellValue(order.getEventTixQty()!=null?String.valueOf(order.getEventTixQty()):"");
					row.createCell(19).setCellValue(order.getTotalActualSoldPrice());
					row.createCell(20).setCellValue(order.getTotalMarketPrice()!=null?String.valueOf(order.getTotalMarketPrice()):"");
					row.createCell(21).setCellValue(order.getProfitAndLoss()!=null?String.valueOf(order.getProfitAndLoss()):"");
					row.createCell(22).setCellValue(order.getPriceUpdateCount()!=null?String.valueOf(order.getPriceUpdateCount()):"");
					row.createCell(23).setCellValue(order.getPrice()!=null?order.getPrice():0);
					row.createCell(24).setCellValue(order.getDiscountCouponPrice()!=null?order.getDiscountCouponPrice():0);
					row.createCell(25).setCellValue(order.getUrl()!=null?order.getUrl():"");
					row.createCell(26).setCellValue(order.getShippingMethod());
					row.createCell(27).setCellValue(order.getTrackingNo());
					row.createCell(28).setCellValue(order.getSecondaryOrderId()!=null?String.valueOf(order.getSecondaryOrderId()):"");
					row.createCell(29).setCellValue(order.getSecondaryOrderType()!=null?String.valueOf(order.getSecondaryOrderType()):"");
					row.createCell(30).setCellValue(order.getOrderId()!=null?order.getOrderId():0);
					row.createCell(31).setCellValue(order.getLastUpdateStr()!=null?String.valueOf(order.getLastUpdateStr()):"");
					row.createCell(32).setCellValue(order.getBrokerId()!=null?order.getBrokerId().toString():"");
					row.createCell(33).setCellValue(order.getCompanyName()!=null?order.getCompanyName():"");
					if(order.getPlatform()!=null && order.getPlatform().equalsIgnoreCase("DESKTOP_SITE")){						
						row.createCell(34).setCellValue("WEB_ORDER");
					}else if(order.getPlatform()!=null && order.getPlatform().equalsIgnoreCase("TICK_TRACKER")){						
						row.createCell(34).setCellValue("RETAIL_ORDER");
					}else{
						row.createCell(34).setCellValue(order.getPlatform()!=null ? order.getPlatform() : "");
					}
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=pending_shipment_orders.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping(value="/PendingRecieptOrders")
	public PendingReceiptOrderStatusDTO getPendingRecieptOrdersPage(HttpServletRequest request, HttpServletResponse response){
		PendingReceiptOrderStatusDTO pendingReceiptOrderStatusDTO = new PendingReceiptOrderStatusDTO();
		Error error = new Error();
		
		try {			
			String eventIdStr = request.getParameter("eventSelect");
			String artistVenueName = request.getParameter("artistVenueName");
			String artistIdStr = request.getParameter("artistId");
			String venueIdStr = request.getParameter("venueId");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String profitLossSign = request.getParameter("profitLoss");
			String companyProduct = request.getParameter("productType");
			String product = request.getParameter("productTypeSession");
			String invoiceNo = request.getParameter("invoiceNo");
			String externalOrderId = request.getParameter("externalOrderId");
			String orderId = request.getParameter("orderId");
			String sortingString = request.getParameter("sortingString");
			String brokerIdStr = request.getParameter("brokerId");
			
			String action = request.getParameter("action");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				pendingReceiptOrderStatusDTO.setError(error);
				pendingReceiptOrderStatusDTO.setStatus(0);
				return pendingReceiptOrderStatusDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				pendingReceiptOrderStatusDTO.setError(error);
				pendingReceiptOrderStatusDTO.setStatus(0);
				return pendingReceiptOrderStatusDTO;
			}
			
			GridHeaderFilters filter = null;
			try{
				filter = GridHeaderFiltersUtil.getOpenOrShipmentPendingOrClosedOrderSearchHeaderFilters(headerFilter+sortingString);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				pendingReceiptOrderStatusDTO.setError(error);
				pendingReceiptOrderStatusDTO.setStatus(0);
				return pendingReceiptOrderStatusDTO;
			}
			
			Collection<OpenOrderStatus> shipmentpendingOrders = new ArrayList<OpenOrderStatus>();
			Collection<OpenOrderStatus> pendingReceiptOrderList = null;
			Collection<EventDetails> events = null;
			String fromDateFinal = "";
			String toDateFinal="";			
			Integer count=0;
						
			if(null ==  profitLossSign || profitLossSign.isEmpty()){
				profitLossSign = String.valueOf(ProfitLossSign.ALL);
			}
			
			if(action!=null && action.equalsIgnoreCase("search")){
				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
				}else{
					fromDateFinal = null;
					toDateFinal = null;
				}				
			}else{
				companyProduct = "REWARDTHEFAN";
				
				if(product!=null && !product.isEmpty()){
					companyProduct = product;
				}				
			}
			
			if(companyProduct != null && (companyProduct.equalsIgnoreCase("REWARDTHEFAN") || companyProduct.equalsIgnoreCase("ALL"))){ 
				companyProduct = "REWARDTHEFAN";
				shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING RECIEPT",companyProduct,filter,false,pageNo,brokerId);
				//count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING RECIEPT",companyProduct,filter,brokerId);
			}
			
			if(artistIdStr!=null && !artistIdStr.isEmpty()){
				events =  DAORegistry.getEventDetailsDAO().getEventsByArtistCity(Integer.parseInt(artistIdStr),null);
				
			}else if(venueIdStr!=null && !venueIdStr.isEmpty()){
				events = DAORegistry.getEventDetailsDAO().getEventsByVenueId(Integer.parseInt(venueIdStr));
			}
			
			//Remove Duplicate Invoices
			Map<Integer, OpenOrderStatus> openOrderMap  = new HashMap<Integer, OpenOrderStatus>();
			for(OpenOrderStatus orderObj : shipmentpendingOrders){

				OpenOrderStatus openOrderStatusOldObj = openOrderMap.get(orderObj.getInvoiceNo());
				
				if(null != openOrderStatusOldObj){
					
					String section = openOrderStatusOldObj.getSection();
					
					if(section != null && !section.isEmpty()){
						String[] sectionArray = null;
						if(section.contains(",")){
							sectionArray = section.split(",");
						}else{
							sectionArray = new String[1];
							sectionArray[0] = section;
						}
						boolean isNewSection = true;
						for (String oldSection : sectionArray) {
							if(oldSection.equals(orderObj.getSection())){
								isNewSection = false;
								break;
							}
						}
						
						if(isNewSection){
							orderObj.setSection(section+","+orderObj.getSection());
						}
					}
					
					String row = openOrderStatusOldObj.getRow();
					
					if(row != null && !row.isEmpty()){
						String[] rowArray = null;
						if(row.contains(",")){
							rowArray = row.split(",");
						}else{
							rowArray = new String[1];
							rowArray[0] = row;
						}
						boolean isNewRow = true;
						for (String oldRow : rowArray) {
							if(oldRow.equals(orderObj.getRow())){
								isNewRow = false;
								break;
							}
						}
						
						if(isNewRow){
							orderObj.setRow(row+","+orderObj.getRow());
						}
					}
				} 
				openOrderMap.put(orderObj.getInvoiceNo(), orderObj);
			}
			pendingReceiptOrderList = openOrderMap.values();
			if(pendingReceiptOrderList != null && pendingReceiptOrderList.size() > 0){
				count = pendingReceiptOrderList.size();
			}
			
			pendingReceiptOrderStatusDTO.setStatus(1);
			pendingReceiptOrderStatusDTO.setPendingReceiptOrders(com.rtw.tracker.utils.Util.getOpenOrdersArray(pendingReceiptOrderList));
			pendingReceiptOrderStatusDTO.setPendingReceiptOrdersPaginationDTO(PaginationUtil.getDummyPaginationParameters(count));
			pendingReceiptOrderStatusDTO.setEventDetails(com.rtw.tracker.utils.Util.getEventDetailsArray(events));
			pendingReceiptOrderStatusDTO.setProfitLossSign(ProfitLossSign.values());
			pendingReceiptOrderStatusDTO.setSelectedProfitLossSign(ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()).toString());
			pendingReceiptOrderStatusDTO.setProductType(companyProduct);
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Pending Receipt Orders.");
			pendingReceiptOrderStatusDTO.setError(error);
			pendingReceiptOrderStatusDTO.setStatus(0);
		}
		return pendingReceiptOrderStatusDTO;		
	}
	
	
	
	
	
	@RequestMapping(value="/DisputedOrders")
	public PendingReceiptOrderStatusDTO getDisputedOrdersPage(HttpServletRequest request, HttpServletResponse response){
		PendingReceiptOrderStatusDTO disputedDTO = new PendingReceiptOrderStatusDTO();
		Error error = new Error();
		
		try {			
			String eventIdStr = request.getParameter("eventSelect");
			//String artistVenueName = request.getParameter("artistVenueName");
			String artistIdStr = request.getParameter("artistId");
			String venueIdStr = request.getParameter("venueId");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String profitLossSign = request.getParameter("profitLoss");
			String companyProduct = request.getParameter("productType");
			String product = request.getParameter("productTypeSession");
			//String invoiceNo = request.getParameter("invoiceNo");
			//String externalOrderId = request.getParameter("externalOrderId");
			//String orderId = request.getParameter("orderId");
			String sortingString = request.getParameter("sortingString");
			String brokerIdStr = request.getParameter("brokerId");
			
			String action = request.getParameter("action");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				disputedDTO.setError(error);
				disputedDTO.setStatus(0);
				return disputedDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				disputedDTO.setError(error);
				disputedDTO.setStatus(0);
				return disputedDTO;
			}
			
			GridHeaderFilters filter = null;
			try{
				filter = GridHeaderFiltersUtil.getOpenOrShipmentPendingOrClosedOrderSearchHeaderFilters(headerFilter+sortingString);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				disputedDTO.setError(error);
				disputedDTO.setStatus(0);
				return disputedDTO;
			}
			
			Collection<OpenOrderStatus> disputedOrders = new ArrayList<OpenOrderStatus>();
			Collection<OpenOrderStatus> disputedOrderList = null;
			Collection<EventDetails> events = null;
			String fromDateFinal = "";
			String toDateFinal="";			
			Integer count=0;
						
			if(null ==  profitLossSign || profitLossSign.isEmpty()){
				profitLossSign = String.valueOf(ProfitLossSign.ALL);
			}
			
			if(action!=null && action.equalsIgnoreCase("search")){
				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
				}else{
					fromDateFinal = null;
					toDateFinal = null;
				}				
			}else{
				companyProduct = "REWARDTHEFAN";
				
				if(product!=null && !product.isEmpty()){
					companyProduct = product;
				}				
			}
			
			if(companyProduct != null && (companyProduct.equalsIgnoreCase("REWARDTHEFAN") || companyProduct.equalsIgnoreCase("ALL"))){ 
				companyProduct = "REWARDTHEFAN";
				disputedOrders = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"DISPUTED",companyProduct,filter,false,pageNo,brokerId);
				//count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING RECIEPT",companyProduct,filter,brokerId);
			}
			
			if(artistIdStr!=null && !artistIdStr.isEmpty()){
				events =  DAORegistry.getEventDetailsDAO().getEventsByArtistCity(Integer.parseInt(artistIdStr),null);
				
			}else if(venueIdStr!=null && !venueIdStr.isEmpty()){
				events = DAORegistry.getEventDetailsDAO().getEventsByVenueId(Integer.parseInt(venueIdStr));
			}
			
			//Remove Duplicate Invoices
			Map<Integer, OpenOrderStatus> openOrderMap  = new HashMap<Integer, OpenOrderStatus>();
			for(OpenOrderStatus orderObj : disputedOrders){

				OpenOrderStatus openOrderStatusOldObj = openOrderMap.get(orderObj.getInvoiceNo());
				
				if(null != openOrderStatusOldObj){
					
					String section = openOrderStatusOldObj.getSection();
					
					if(section != null && !section.isEmpty()){
						String[] sectionArray = null;
						if(section.contains(",")){
							sectionArray = section.split(",");
						}else{
							sectionArray = new String[1];
							sectionArray[0] = section;
						}
						boolean isNewSection = true;
						for (String oldSection : sectionArray) {
							if(oldSection.equals(orderObj.getSection())){
								isNewSection = false;
								break;
							}
						}
						
						if(isNewSection){
							orderObj.setSection(section+","+orderObj.getSection());
						}
					}
					
					String row = openOrderStatusOldObj.getRow();
					
					if(row != null && !row.isEmpty()){
						String[] rowArray = null;
						if(row.contains(",")){
							rowArray = row.split(",");
						}else{
							rowArray = new String[1];
							rowArray[0] = row;
						}
						boolean isNewRow = true;
						for (String oldRow : rowArray) {
							if(oldRow.equals(orderObj.getRow())){
								isNewRow = false;
								break;
							}
						}
						
						if(isNewRow){
							orderObj.setRow(row+","+orderObj.getRow());
						}
					}
				} 
				openOrderMap.put(orderObj.getInvoiceNo(), orderObj);
			}
			disputedOrderList = openOrderMap.values();
			if(disputedOrderList != null && disputedOrderList.size() > 0){
				count = disputedOrderList.size();
			}
			
			disputedDTO.setStatus(1);
			disputedDTO.setPendingReceiptOrders(com.rtw.tracker.utils.Util.getOpenOrdersArray(disputedOrderList));
			disputedDTO.setPendingReceiptOrdersPaginationDTO(PaginationUtil.getDummyPaginationParameters(count));
			disputedDTO.setEventDetails(com.rtw.tracker.utils.Util.getEventDetailsArray(events));
			disputedDTO.setProfitLossSign(ProfitLossSign.values());
			disputedDTO.setSelectedProfitLossSign(ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()).toString());
			disputedDTO.setProductType(companyProduct);
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Disputed Orders.");
			disputedDTO.setError(error);
			disputedDTO.setStatus(0);
		}
		return disputedDTO;		
	}
	
	/*@RequestMapping(value="/GetPendingRecieptOrders")
	public PendingReceiptOrderStatusDTO getPendingRecieptOrders(HttpServletRequest request, HttpServletResponse response){
		PendingReceiptOrderStatusDTO pendingReceiptOrderStatusDTO = new PendingReceiptOrderStatusDTO();
		Error error = new Error();		
		
		try {
			String eventIdStr = request.getParameter("eventSelect");
			String artistIdStr = request.getParameter("artistId");
			String venueIdStr = request.getParameter("venueId");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");			
			String profitLossSign = request.getParameter("profitLoss");
			String companyProduct = request.getParameter("productType");
			
			String invoiceNo = request.getParameter("invoiceNo");
			String externalOrderId = request.getParameter("externalOrderId");
			String orderId = request.getParameter("orderId");
			String action = request.getParameter("action");

			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			String brokerIdStr = request.getParameter("brokerId");
			
			if(StringUtils.isEmpty(pageNo)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				pendingReceiptOrderStatusDTO.setError(error);
				pendingReceiptOrderStatusDTO.setStatus(0);
				return pendingReceiptOrderStatusDTO;
			}
			try {
				Integer.parseInt(pageNo);
			} catch (Exception e) {
				System.err.println("Please provide valid PageNo.");
				error.setDescription("Please provide valid PageNo.");
				pendingReceiptOrderStatusDTO.setError(error);
				pendingReceiptOrderStatusDTO.setStatus(0);
				return pendingReceiptOrderStatusDTO;
			}
			
			GridHeaderFilters filter = null;
			try{
				filter = GridHeaderFiltersUtil.getOpenOrShipmentPendingOrClosedOrderSearchHeaderFilters(headerFilter);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				pendingReceiptOrderStatusDTO.setError(error);
				pendingReceiptOrderStatusDTO.setStatus(0);
				return pendingReceiptOrderStatusDTO;
			}
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				pendingReceiptOrderStatusDTO.setError(error);
				pendingReceiptOrderStatusDTO.setStatus(0);
				return pendingReceiptOrderStatusDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				pendingReceiptOrderStatusDTO.setError(error);
				pendingReceiptOrderStatusDTO.setStatus(0);
				return pendingReceiptOrderStatusDTO;
			}
			
			Collection<OpenOrderStatus> shipmentpendingOrders = null;			
			String fromDateFinal = "";
			String toDateFinal = "";
			Integer count=0;
			
			if(action!=null && action.equalsIgnoreCase("search")){
				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
					if(companyProduct.equalsIgnoreCase("REWARDTHEFAN")){
						shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING RECIEPT",companyProduct,filter,false,pageNo,brokerId);
						count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING RECIEPT",companyProduct,filter,brokerId);
					}
				}else{
					if(companyProduct.equalsIgnoreCase("REWARDTHEFAN")){
						shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING RECIEPT",companyProduct,filter,false,pageNo,brokerId);
						count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING RECIEPT",companyProduct,filter,brokerId);
					}
					
				}
			}
			
			pendingReceiptOrderStatusDTO.setStatus(1);
			pendingReceiptOrderStatusDTO.setPendingReceiptOrders(com.rtw.tracker.utils.Util.getOpenOrdersArray(shipmentpendingOrders));
			pendingReceiptOrderStatusDTO.setPendingReceiptOrdersPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Applying Search - Pending Receipt Orders.");
			pendingReceiptOrderStatusDTO.setError(error);
			pendingReceiptOrderStatusDTO.setStatus(0);			
		}
		return pendingReceiptOrderStatusDTO;
	}*/
		
	@RequestMapping(value="/PendingRecieptExportToExcel")
	public void getPendingRecieptOrdersToExport(HttpServletRequest request, HttpServletResponse response){
		
		try {
			String eventIdStr = request.getParameter("eventSelect");
			String artistIdStr = request.getParameter("artistId");
			String venueIdStr = request.getParameter("venueId");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String profitLossSign = request.getParameter("profitLoss");
			String companyProduct = request.getParameter("productType");
			
			String invoiceNo = request.getParameter("invoiceNo");
			String externalOrderId = request.getParameter("externalOrderId");
			String orderId = request.getParameter("orderId");
			String headerFilter = request.getParameter("headerFilter");
			String brokerIdStr = request.getParameter("brokerId");
			Integer brokerId = Integer.parseInt(brokerIdStr);
						
			GridHeaderFilters filter = GridHeaderFiltersUtil.getOpenOrShipmentPendingOrClosedOrderSearchHeaderFilters(headerFilter);
			
			Collection<OpenOrderStatus> shipmentpendingOrders = null;
			Collection<OpenOrderStatus> pendingReceiptOrderList = null;
			String fromDateFinal = "";
			String toDateFinal = "";
			
			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";
				if(companyProduct.equalsIgnoreCase("REWARDTHEFAN")){
					shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersToExport(invoiceNo,eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING RECIEPT",companyProduct,externalOrderId,orderId,filter,brokerId);
				}
//				else{
//					shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getRTWRTW2OrdersToExport(invoiceNo,eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING",companyProduct,externalOrderId,orderId,filter);
//				}
			}else{
				if(companyProduct.equalsIgnoreCase("REWARDTHEFAN")){
					shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersToExport(invoiceNo,eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING RECIEPT",companyProduct,externalOrderId,orderId,filter,brokerId);
				}
//				else{
//					shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getRTWRTW2OrdersToExport(invoiceNo,eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING",companyProduct,externalOrderId,orderId,filter);
//				}
			}
			
			//Remove Duplicate Invoices
			Map<Integer, OpenOrderStatus> openOrderMap  = new HashMap<Integer, OpenOrderStatus>();
			for(OpenOrderStatus orderObj : shipmentpendingOrders){

				OpenOrderStatus openOrderStatusOldObj = openOrderMap.get(orderObj.getInvoiceNo());
				
				if(null != openOrderStatusOldObj){
					
					String section = openOrderStatusOldObj.getSection();
					
					if(section != null && !section.isEmpty()){
						String[] sectionArray = null;
						if(section.contains(",")){
							sectionArray = section.split(",");
						}else{
							sectionArray = new String[1];
							sectionArray[0] = section;
						}
						boolean isNewSection = true;
						for (String oldSection : sectionArray) {
							if(oldSection.equals(orderObj.getSection())){
								isNewSection = false;
								break;
							}
						}
						
						if(isNewSection){
							orderObj.setSection(section+","+orderObj.getSection());
						}
					}
					
					String row = openOrderStatusOldObj.getRow();
					
					if(row != null && !row.isEmpty()){
						String[] rowArray = null;
						if(row.contains(",")){
							rowArray = row.split(",");
						}else{
							rowArray = new String[1];
							rowArray[0] = row;
						}
						boolean isNewRow = true;
						for (String oldRow : rowArray) {
							if(oldRow.equals(orderObj.getRow())){
								isNewRow = false;
								break;
							}
						}
						
						if(isNewRow){
							orderObj.setRow(row+","+orderObj.getRow());
						}
					}
				} 
				openOrderMap.put(orderObj.getInvoiceNo(), orderObj);
			}
			pendingReceiptOrderList = openOrderMap.values();
			
			if(pendingReceiptOrderList!=null && !pendingReceiptOrderList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("pending_receipt_orders");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Invoice Id");
				header.createCell(1).setCellValue("Invoice Date");
				header.createCell(2).setCellValue("Event Name");
				header.createCell(3).setCellValue("Event Date");
				header.createCell(4).setCellValue("Event Time");
				header.createCell(5).setCellValue("Venue");
				header.createCell(6).setCellValue("City");
				header.createCell(7).setCellValue("State");
				header.createCell(8).setCellValue("Country");
				header.createCell(9).setCellValue("Product Type");
				header.createCell(10).setCellValue("Internal Notes");
				header.createCell(11).setCellValue("Quantity");
				header.createCell(12).setCellValue("Section");
				header.createCell(13).setCellValue("Row");
				header.createCell(14).setCellValue("Sold Price");
				header.createCell(15).setCellValue("Market Price");
				header.createCell(16).setCellValue("Last Updated Price");
				header.createCell(17).setCellValue("Section Tix Count");
				header.createCell(18).setCellValue("Event Tix Count");
				header.createCell(19).setCellValue("Total Sold Price");
				header.createCell(20).setCellValue("Total Market Price");
				header.createCell(21).setCellValue("Profit/Loss");
				header.createCell(22).setCellValue("Price Updated Count");
				header.createCell(23).setCellValue("Price");
				header.createCell(24).setCellValue("Discount Coupen Price");
				header.createCell(25).setCellValue("Url");
				header.createCell(26).setCellValue("Shipping Method");
				header.createCell(27).setCellValue("Tracking No");
				header.createCell(28).setCellValue("Secondary Order Id");
				header.createCell(29).setCellValue("Secondary Order Type");
				header.createCell(30).setCellValue("Order Id");
				header.createCell(31).setCellValue("Last Updated");
				header.createCell(32).setCellValue("Broker Id");
				header.createCell(33).setCellValue("Company Name");
				header.createCell(34).setCellValue("Platform");
				
				int i=1;
				for(OpenOrderStatus order : pendingReceiptOrderList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(order.getInvoiceNo());
					row.createCell(1).setCellValue(order.getInvoiceDateStr());
					row.createCell(2).setCellValue(order.getEventName());
					row.createCell(3).setCellValue(order.getEventDateStr());
					row.createCell(4).setCellValue(order.getEventTimeStr());
					row.createCell(5).setCellValue(order.getVenueName());
					row.createCell(6).setCellValue(order.getVenueCity());
					row.createCell(7).setCellValue(order.getVenueState());
					row.createCell(8).setCellValue(order.getVenueCountry());
					row.createCell(9).setCellValue(order.getProductType());
					row.createCell(10).setCellValue(order.getInternalNotes());
					row.createCell(11).setCellValue(order.getSoldQty());
					row.createCell(12).setCellValue(order.getSection());
					row.createCell(13).setCellValue(order.getRow());
					row.createCell(14).setCellValue(order.getActualSoldPrice());
					row.createCell(15).setCellValue(order.getMarketPrice()!=null?String.valueOf(order.getMarketPrice()):"");
					row.createCell(16).setCellValue(order.getLastUpdatedPrice()!=null?String.valueOf(order.getLastUpdatedPrice()):"");
					row.createCell(17).setCellValue(order.getSectionTixQty()!=null?String.valueOf(order.getSectionTixQty()):"");
					row.createCell(18).setCellValue(order.getEventTixQty()!=null?String.valueOf(order.getEventTixQty()):"");
					row.createCell(19).setCellValue(order.getTotalActualSoldPrice());
					row.createCell(20).setCellValue(order.getTotalMarketPrice()!=null?String.valueOf(order.getTotalMarketPrice()):"");
					row.createCell(21).setCellValue(order.getProfitAndLoss()!=null?String.valueOf(order.getProfitAndLoss()):"");
					row.createCell(22).setCellValue(order.getPriceUpdateCount()!=null?String.valueOf(order.getPriceUpdateCount()):"");
					row.createCell(23).setCellValue(order.getPrice()!=null?order.getPrice():0);
					row.createCell(24).setCellValue(order.getDiscountCouponPrice()!=null?order.getDiscountCouponPrice():0);
					row.createCell(25).setCellValue(order.getUrl()!=null?order.getUrl():"");
					row.createCell(26).setCellValue(order.getShippingMethod());
					row.createCell(27).setCellValue(order.getTrackingNo());
					row.createCell(28).setCellValue(order.getSecondaryOrderId()!=null?String.valueOf(order.getSecondaryOrderId()):"");
					row.createCell(29).setCellValue(order.getSecondaryOrderType()!=null?String.valueOf(order.getSecondaryOrderType()):"");
					row.createCell(30).setCellValue(order.getOrderId()!=null?order.getOrderId():0);
					row.createCell(31).setCellValue(order.getLastUpdateStr()!=null?String.valueOf(order.getLastUpdateStr()):"");
					row.createCell(32).setCellValue(order.getBrokerId()!=null?order.getBrokerId().toString():"");
					row.createCell(33).setCellValue(order.getCompanyName()!=null?order.getCompanyName():"");
					if(order.getPlatform()!=null && order.getPlatform().equalsIgnoreCase("DESKTOP_SITE")){						
						row.createCell(34).setCellValue("WEB_ORDER");
					}else if(order.getPlatform()!=null && order.getPlatform().equalsIgnoreCase("TICK_TRACKER")){						
						row.createCell(34).setCellValue("RETAIL_ORDER");
					}else{
						row.createCell(34).setCellValue(order.getPlatform()!=null ? order.getPlatform() : "");
					}
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=pending_receipt_orders.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping(value="/DisputedOrdersExportToExcel")
	public void getDisputedOrdersToExport(HttpServletRequest request, HttpServletResponse response){
		
		try {
			String eventIdStr = request.getParameter("eventSelect");
			String artistIdStr = request.getParameter("artistId");
			String venueIdStr = request.getParameter("venueId");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String profitLossSign = request.getParameter("profitLoss");
			String companyProduct = request.getParameter("productType");
			
			String invoiceNo = request.getParameter("invoiceNo");
			String externalOrderId = request.getParameter("externalOrderId");
			String orderId = request.getParameter("orderId");
			String headerFilter = request.getParameter("headerFilter");
			String brokerIdStr = request.getParameter("brokerId");
			Integer brokerId = Integer.parseInt(brokerIdStr);
						
			GridHeaderFilters filter = GridHeaderFiltersUtil.getOpenOrShipmentPendingOrClosedOrderSearchHeaderFilters(headerFilter);
			
			Collection<OpenOrderStatus> disputedOrders = null;
			Collection<OpenOrderStatus> disputedOrderList = null;
			String fromDateFinal = "";
			String toDateFinal = "";
			
			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";
				if(companyProduct.equalsIgnoreCase("REWARDTHEFAN")){
					disputedOrders = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersToExport(invoiceNo,eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"DISPUTED",companyProduct,externalOrderId,orderId,filter,brokerId);
				}
//				else{
//					shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getRTWRTW2OrdersToExport(invoiceNo,eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING",companyProduct,externalOrderId,orderId,filter);
//				}
			}else{
				if(companyProduct.equalsIgnoreCase("REWARDTHEFAN")){
					disputedOrders = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersToExport(invoiceNo,eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"DISPUTED",companyProduct,externalOrderId,orderId,filter,brokerId);
				}
//				else{
//					shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getRTWRTW2OrdersToExport(invoiceNo,eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING",companyProduct,externalOrderId,orderId,filter);
//				}
			}
			
			//Remove Duplicate Invoices
			Map<Integer, OpenOrderStatus> openOrderMap  = new HashMap<Integer, OpenOrderStatus>();
			for(OpenOrderStatus orderObj : disputedOrders){

				OpenOrderStatus openOrderStatusOldObj = openOrderMap.get(orderObj.getInvoiceNo());
				
				if(null != openOrderStatusOldObj){
					
					String section = openOrderStatusOldObj.getSection();
					
					if(section != null && !section.isEmpty()){
						String[] sectionArray = null;
						if(section.contains(",")){
							sectionArray = section.split(",");
						}else{
							sectionArray = new String[1];
							sectionArray[0] = section;
						}
						boolean isNewSection = true;
						for (String oldSection : sectionArray) {
							if(oldSection.equals(orderObj.getSection())){
								isNewSection = false;
								break;
							}
						}
						
						if(isNewSection){
							orderObj.setSection(section+","+orderObj.getSection());
						}
					}
					
					String row = openOrderStatusOldObj.getRow();
					
					if(row != null && !row.isEmpty()){
						String[] rowArray = null;
						if(row.contains(",")){
							rowArray = row.split(",");
						}else{
							rowArray = new String[1];
							rowArray[0] = row;
						}
						boolean isNewRow = true;
						for (String oldRow : rowArray) {
							if(oldRow.equals(orderObj.getRow())){
								isNewRow = false;
								break;
							}
						}
						
						if(isNewRow){
							orderObj.setRow(row+","+orderObj.getRow());
						}
					}
				} 
				openOrderMap.put(orderObj.getInvoiceNo(), orderObj);
			}
			disputedOrderList = openOrderMap.values();
			
			if(disputedOrderList!=null && !disputedOrderList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("disputed_orders");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Invoice Id");
				header.createCell(1).setCellValue("Invoice Date");
				header.createCell(2).setCellValue("Event Name");
				header.createCell(3).setCellValue("Event Date");
				header.createCell(4).setCellValue("Event Time");
				header.createCell(5).setCellValue("Venue");
				header.createCell(6).setCellValue("City");
				header.createCell(7).setCellValue("State");
				header.createCell(8).setCellValue("Country");
				header.createCell(9).setCellValue("Product Type");
				header.createCell(10).setCellValue("Internal Notes");
				header.createCell(11).setCellValue("Quantity");
				header.createCell(12).setCellValue("Section");
				header.createCell(13).setCellValue("Row");
				header.createCell(14).setCellValue("Sold Price");
				header.createCell(15).setCellValue("Market Price");
				header.createCell(16).setCellValue("Last Updated Price");
				header.createCell(17).setCellValue("Section Tix Count");
				header.createCell(18).setCellValue("Event Tix Count");
				header.createCell(19).setCellValue("Total Sold Price");
				header.createCell(20).setCellValue("Total Market Price");
				header.createCell(21).setCellValue("Profit/Loss");
				header.createCell(22).setCellValue("Price Updated Count");
				header.createCell(23).setCellValue("Price");
				header.createCell(24).setCellValue("Discount Coupen Price");
				header.createCell(25).setCellValue("Url");
				header.createCell(26).setCellValue("Shipping Method");
				header.createCell(27).setCellValue("Tracking No");
				header.createCell(28).setCellValue("Secondary Order Id");
				header.createCell(29).setCellValue("Secondary Order Type");
				header.createCell(30).setCellValue("Order Id");
				header.createCell(31).setCellValue("Last Updated");
				header.createCell(32).setCellValue("Broker Id");
				header.createCell(33).setCellValue("Company Name");
				header.createCell(34).setCellValue("Platform");
				
				int i=1;
				for(OpenOrderStatus order : disputedOrderList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(order.getInvoiceNo());
					row.createCell(1).setCellValue(order.getInvoiceDateStr());
					row.createCell(2).setCellValue(order.getEventName());
					row.createCell(3).setCellValue(order.getEventDateStr());
					row.createCell(4).setCellValue(order.getEventTimeStr());
					row.createCell(5).setCellValue(order.getVenueName());
					row.createCell(6).setCellValue(order.getVenueCity());
					row.createCell(7).setCellValue(order.getVenueState());
					row.createCell(8).setCellValue(order.getVenueCountry());
					row.createCell(9).setCellValue(order.getProductType());
					row.createCell(10).setCellValue(order.getInternalNotes());
					row.createCell(11).setCellValue(order.getSoldQty());
					row.createCell(12).setCellValue(order.getSection());
					row.createCell(13).setCellValue(order.getRow());
					row.createCell(14).setCellValue(order.getActualSoldPrice());
					row.createCell(15).setCellValue(order.getMarketPrice()!=null?String.valueOf(order.getMarketPrice()):"");
					row.createCell(16).setCellValue(order.getLastUpdatedPrice()!=null?String.valueOf(order.getLastUpdatedPrice()):"");
					row.createCell(17).setCellValue(order.getSectionTixQty()!=null?String.valueOf(order.getSectionTixQty()):"");
					row.createCell(18).setCellValue(order.getEventTixQty()!=null?String.valueOf(order.getEventTixQty()):"");
					row.createCell(19).setCellValue(order.getTotalActualSoldPrice());
					row.createCell(20).setCellValue(order.getTotalMarketPrice()!=null?String.valueOf(order.getTotalMarketPrice()):"");
					row.createCell(21).setCellValue(order.getProfitAndLoss()!=null?String.valueOf(order.getProfitAndLoss()):"");
					row.createCell(22).setCellValue(order.getPriceUpdateCount()!=null?String.valueOf(order.getPriceUpdateCount()):"");
					row.createCell(23).setCellValue(order.getPrice()!=null?order.getPrice():0);
					row.createCell(24).setCellValue(order.getDiscountCouponPrice()!=null?order.getDiscountCouponPrice():0);
					row.createCell(25).setCellValue(order.getUrl()!=null?order.getUrl():"");
					row.createCell(26).setCellValue(order.getShippingMethod());
					row.createCell(27).setCellValue(order.getTrackingNo());
					row.createCell(28).setCellValue(order.getSecondaryOrderId()!=null?String.valueOf(order.getSecondaryOrderId()):"");
					row.createCell(29).setCellValue(order.getSecondaryOrderType()!=null?String.valueOf(order.getSecondaryOrderType()):"");
					row.createCell(30).setCellValue(order.getOrderId()!=null?order.getOrderId():0);
					row.createCell(31).setCellValue(order.getLastUpdateStr()!=null?String.valueOf(order.getLastUpdateStr()):"");
					row.createCell(32).setCellValue(order.getBrokerId()!=null?order.getBrokerId().toString():"");
					row.createCell(33).setCellValue(order.getCompanyName()!=null?order.getCompanyName():"");
					if(order.getPlatform()!=null && order.getPlatform().equalsIgnoreCase("DESKTOP_SITE")){						
						row.createCell(34).setCellValue("WEB_ORDER");
					}else if(order.getPlatform()!=null && order.getPlatform().equalsIgnoreCase("TICK_TRACKER")){						
						row.createCell(34).setCellValue("RETAIL_ORDER");
					}else{
						row.createCell(34).setCellValue(order.getPlatform()!=null ? order.getPlatform() : "");
					}
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=disputed_orders.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping(value="/ClosedOrders")
	public ClosedOrderStatusDTO getClosedOrderTrackingPage(HttpServletRequest request, HttpServletResponse response){
		ClosedOrderStatusDTO closedOrderStatusDTO = new ClosedOrderStatusDTO();
		Error error = new Error();
		
		try {
			String eventIdStr = request.getParameter("eventSelect");
			String artistVenueName = request.getParameter("artistVenueName");
			String artistIdStr = request.getParameter("artistId");
			String venueIdStr = request.getParameter("venueId");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String profitLossSign = request.getParameter("profitLoss");
			String companyProduct = request.getParameter("productType");
			String invoiceNo = request.getParameter("invoiceNo");
			String externalOrderId = request.getParameter("externalOrderId");
			String orderId = request.getParameter("orderId");
			String product = request.getParameter("productTypeSession");
			String brokerIdStr = request.getParameter("brokerId");
			String sortingString = request.getParameter("sortingString");
			
			String action = request.getParameter("action");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				closedOrderStatusDTO.setError(error);
				closedOrderStatusDTO.setStatus(0);
				return closedOrderStatusDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				closedOrderStatusDTO.setError(error);
				closedOrderStatusDTO.setStatus(0);
				return closedOrderStatusDTO;
			}
			
			GridHeaderFilters filter = null;
			try{
				filter = GridHeaderFiltersUtil.getOpenOrShipmentPendingOrClosedOrderSearchHeaderFilters(headerFilter+sortingString);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				closedOrderStatusDTO.setError(error);
				closedOrderStatusDTO.setStatus(0);
				return closedOrderStatusDTO;
			}
			
			Collection<OpenOrderStatus> closedOrderList = null;
			Collection<OpenOrderStatus> closedOrdersList = null;
			Collection<EventDetails> events = null;
			String fromDateFinal = "";
			String toDateFinal="";
			Integer count = 0;
			
			if(null ==  profitLossSign || profitLossSign.isEmpty()){
				profitLossSign = String.valueOf(ProfitLossSign.ALL);
			}
			
			if(action!=null && action.equalsIgnoreCase("search")){
				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
				}else{
					fromDateFinal = null;
					toDateFinal = null;
				}				
			}else{
				companyProduct = "REWARDTHEFAN";
				
				if(product!=null && !product.isEmpty()){
					companyProduct = product;
				}
			}
			
			if(companyProduct != null && (companyProduct.equalsIgnoreCase("REWARDTHEFAN") || companyProduct.equalsIgnoreCase("ALL"))){
				companyProduct = "REWARDTHEFAN";
				closedOrderList = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"CLOSED",companyProduct,filter,false,pageNo,brokerId);
				count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"CLOSED",companyProduct,filter,brokerId);
			}else{
				closedOrderList = DAORegistry.getQueryManagerDAO().getRTWRTW2FilledOrders(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()), "CLOSED", companyProduct, filter,pageNo);
				count = DAORegistry.getQueryManagerDAO().getRTWRTW2FilledOrdersCount(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()), "CLOSED", companyProduct, filter);
			}
			
			if(artistIdStr!=null && !artistIdStr.isEmpty()){
				events =  DAORegistry.getEventDetailsDAO().getEventsByArtistCity(Integer.parseInt(artistIdStr),null);
				
			}else if(venueIdStr!=null && !venueIdStr.isEmpty()){
				events = DAORegistry.getEventDetailsDAO().getEventsByVenueId(Integer.parseInt(venueIdStr));
			}
			
			//Remove Duplicate Invoices
			Map<Integer, OpenOrderStatus> openOrderMap  = new LinkedHashMap<Integer, OpenOrderStatus>();
			for(OpenOrderStatus orderObj : closedOrderList){

				OpenOrderStatus openOrderStatusOldObj = openOrderMap.get(orderObj.getInvoiceNo());
				
				if(null != openOrderStatusOldObj){
					
					String section = openOrderStatusOldObj.getSection();
					
					if(section != null && !section.isEmpty()){
						String[] sectionArray = null;
						if(section.contains(",")){
							sectionArray = section.split(",");
						}else{
							sectionArray = new String[1];
							sectionArray[0] = section;
						}
						boolean isNewSection = true;
						for (String oldSection : sectionArray) {
							if(oldSection.equals(orderObj.getSection())){
								isNewSection = false;
								break;
							}
						}
						
						if(isNewSection){
							orderObj.setSection(section+","+orderObj.getSection());
						}
					}
					
					String row = openOrderStatusOldObj.getRow();
					
					if(row != null && !row.isEmpty()){
						String[] rowArray = null;
						if(row.contains(",")){
							rowArray = row.split(",");
						}else{
							rowArray = new String[1];
							rowArray[0] = row;
						}
						boolean isNewRow = true;
						for (String oldRow : rowArray) {
							if(oldRow.equals(orderObj.getRow())){
								isNewRow = false;
								break;
							}
						}
						
						if(isNewRow){
							orderObj.setRow(row+","+orderObj.getRow());
						}
					}
				} 
				openOrderMap.put(orderObj.getInvoiceNo(), orderObj);
			}
			closedOrdersList = openOrderMap.values();
//			if(closedOrdersList != null && closedOrdersList.size() > 0){
//				count = closedOrdersList.size();
//			}
						
			closedOrderStatusDTO.setStatus(1);
			closedOrderStatusDTO.setClosedOrders(com.rtw.tracker.utils.Util.getOpenOrdersArray(closedOrdersList));
			closedOrderStatusDTO.setClosedOrdersPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
			closedOrderStatusDTO.setEventDetails(com.rtw.tracker.utils.Util.getEventDetailsArray(events));
			closedOrderStatusDTO.setProfitLossSign(ProfitLossSign.values());
			closedOrderStatusDTO.setSelectedProfitLossSign(ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()).toString());
			closedOrderStatusDTO.setProductType(companyProduct);
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Closed Orders.");
			closedOrderStatusDTO.setError(error);
			closedOrderStatusDTO.setStatus(0);			
		}		
		return closedOrderStatusDTO;
	
	}
	
	/*@RequestMapping(value="/GetClosedOrders")
	public ClosedOrderStatusDTO getClosedOrdersTracking(HttpServletRequest request, HttpServletResponse response){
		ClosedOrderStatusDTO closedOrderStatusDTO = new ClosedOrderStatusDTO();
		Error error = new Error();
		
		try {
			String eventIdStr = request.getParameter("eventSelect");
			String artistIdStr = request.getParameter("artistId");
			String venueIdStr = request.getParameter("venueId");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");			
			String profitLossSign = request.getParameter("profitLoss");
			String companyProduct = request.getParameter("productType");
			
			String invoiceNo = request.getParameter("invoiceNo");
			String externalOrderId = request.getParameter("externalOrderId");
			String orderId = request.getParameter("orderId");
			String action = request.getParameter("action");
			
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			String brokerIdStr = request.getParameter("brokerId");
			
			if(StringUtils.isEmpty(pageNo)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				closedOrderStatusDTO.setError(error);
				closedOrderStatusDTO.setStatus(0);
				return closedOrderStatusDTO;
			}
			try {
				Integer.parseInt(pageNo);
			} catch (Exception e) {
				System.err.println("Please provide valid PageNo.");
				error.setDescription("Please provide valid PageNo.");
				closedOrderStatusDTO.setError(error);
				closedOrderStatusDTO.setStatus(0);
				return closedOrderStatusDTO;
			}
			
			GridHeaderFilters filter = null;
			try{
				filter = GridHeaderFiltersUtil.getOpenOrShipmentPendingOrClosedOrderSearchHeaderFilters(headerFilter);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				closedOrderStatusDTO.setError(error);
				closedOrderStatusDTO.setStatus(0);
				return closedOrderStatusDTO;
			}
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				closedOrderStatusDTO.setError(error);
				closedOrderStatusDTO.setStatus(0);
				return closedOrderStatusDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				closedOrderStatusDTO.setError(error);
				closedOrderStatusDTO.setStatus(0);
				return closedOrderStatusDTO;
			}
			
			Collection<OpenOrderStatus> closedOrderList = null;
			String fromDateFinal = "";
			String toDateFinal="";
			Integer count=0;
			
			if(null ==  profitLossSign || profitLossSign.isEmpty()){
				profitLossSign = String.valueOf(ProfitLossSign.ALL);
			}
						
			if(action!=null && action.equalsIgnoreCase("search")){
				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
					if(companyProduct.equalsIgnoreCase("REWARDTHEFAN")){
						closedOrderList = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"CLOSED",companyProduct,filter,false,pageNo,brokerId);
						count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"CLOSED",companyProduct,filter,brokerId);
					}else{
						closedOrderList = DAORegistry.getQueryManagerDAO().getRTWRTW2FilledOrders(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()), "CLOSED", companyProduct, filter,pageNo);
						count = DAORegistry.getQueryManagerDAO().getRTWRTW2FilledOrdersCount(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()), "CLOSED", companyProduct, filter);
					}
					
				}else{
					if(companyProduct.equalsIgnoreCase("REWARDTHEFAN")){
						closedOrderList = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"CLOSED",companyProduct,filter,false,pageNo,brokerId);
						count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"CLOSED",companyProduct,filter,brokerId);
					}else{
						closedOrderList = DAORegistry.getQueryManagerDAO().getRTWRTW2FilledOrders(eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()), "CLOSED", companyProduct, filter,pageNo);
						count = DAORegistry.getQueryManagerDAO().getRTWRTW2FilledOrdersCount(eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()), "CLOSED", companyProduct, filter);
					}					
				}
			}
			
			closedOrderStatusDTO.setStatus(1);
			closedOrderStatusDTO.setClosedOrders(com.rtw.tracker.utils.Util.getOpenOrdersArray(closedOrderList));
			closedOrderStatusDTO.setClosedOrdersPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Applying Search - Pending Shipment Orders.");
			closedOrderStatusDTO.setError(error);
			closedOrderStatusDTO.setStatus(0);			
		}
		return closedOrderStatusDTO;
	}*/
		
	@RequestMapping(value="/ClosedOrdersExportToExcel")
	public void getClosedOrdersToExport(HttpServletRequest request, HttpServletResponse response){
		
		try {
			String eventIdStr = request.getParameter("eventSelect");
			String artistIdStr = request.getParameter("artistId");
			String venueIdStr = request.getParameter("venueId");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String profitLossSign = request.getParameter("profitLoss");
			String companyProduct = request.getParameter("productType");

			String invoiceNo = request.getParameter("invoiceNo");
			String externalOrderId = request.getParameter("externalOrderId");
			String orderId = request.getParameter("orderId");
			String headerFilter = request.getParameter("headerFilter");
			String brokerIdStr = request.getParameter("brokerId");
			Integer brokerId = Integer.parseInt(brokerIdStr);
			
			GridHeaderFilters filter = GridHeaderFiltersUtil.getOpenOrShipmentPendingOrClosedOrderSearchHeaderFilters(headerFilter);
			
			Collection<OpenOrderStatus> closedOrderList = null;
			Collection<OpenOrderStatus> closedOrdersList = null;
			String fromDateFinal = "";
			String toDateFinal="";
			
			if(null ==  profitLossSign || profitLossSign.isEmpty()){
				profitLossSign = String.valueOf(ProfitLossSign.ALL);
			}
			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";
				if(companyProduct.equalsIgnoreCase("REWARDTHEFAN")){
					closedOrderList = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersToExport(invoiceNo,eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"CLOSED",companyProduct,externalOrderId,orderId,filter,brokerId);
				}else{
					closedOrderList = DAORegistry.getQueryManagerDAO().getRTWRTW2OrdersToExport(invoiceNo,eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"CLOSED",companyProduct,externalOrderId,orderId,filter);
				}
			}else{
				if(companyProduct.equalsIgnoreCase("REWARDTHEFAN")){
					closedOrderList = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersToExport(invoiceNo,eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"CLOSED",companyProduct,externalOrderId,orderId,filter,brokerId);
				}else{
					closedOrderList = DAORegistry.getQueryManagerDAO().getRTWRTW2OrdersToExport(invoiceNo,eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"CLOSED",companyProduct,externalOrderId,orderId,filter);
				}
			}
			
			//Remove Duplicate Invoices
			Map<Integer, OpenOrderStatus> openOrderMap  = new HashMap<Integer, OpenOrderStatus>();
			for(OpenOrderStatus orderObj : closedOrderList){

				OpenOrderStatus openOrderStatusOldObj = openOrderMap.get(orderObj.getInvoiceNo());
				
				if(null != openOrderStatusOldObj){
					
					String section = openOrderStatusOldObj.getSection();
					
					if(section != null && !section.isEmpty()){
						String[] sectionArray = null;
						if(section.contains(",")){
							sectionArray = section.split(",");
						}else{
							sectionArray = new String[1];
							sectionArray[0] = section;
						}
						boolean isNewSection = true;
						for (String oldSection : sectionArray) {
							if(oldSection.equals(orderObj.getSection())){
								isNewSection = false;
								break;
							}
						}
						
						if(isNewSection){
							orderObj.setSection(section+","+orderObj.getSection());
						}
					}
					
					String row = openOrderStatusOldObj.getRow();
					
					if(row != null && !row.isEmpty()){
						String[] rowArray = null;
						if(row.contains(",")){
							rowArray = row.split(",");
						}else{
							rowArray = new String[1];
							rowArray[0] = row;
						}
						boolean isNewRow = true;
						for (String oldRow : rowArray) {
							if(oldRow.equals(orderObj.getRow())){
								isNewRow = false;
								break;
							}
						}
						
						if(isNewRow){
							orderObj.setRow(row+","+orderObj.getRow());
						}
					}
				} 
				openOrderMap.put(orderObj.getInvoiceNo(), orderObj);
			}
			closedOrdersList = openOrderMap.values();			
			
			if(closedOrdersList!=null && !closedOrdersList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("closed_orders");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Invoice Id");
				header.createCell(1).setCellValue("Invoice Date");
				header.createCell(2).setCellValue("Event Name");
				header.createCell(3).setCellValue("Event Date");
				header.createCell(4).setCellValue("Event Time");
				header.createCell(5).setCellValue("Venue");
				header.createCell(6).setCellValue("City");
				header.createCell(7).setCellValue("State");
				header.createCell(8).setCellValue("Country");
				header.createCell(9).setCellValue("Product Type");
				header.createCell(10).setCellValue("Internal Notes");
				header.createCell(11).setCellValue("Quantity");
				header.createCell(12).setCellValue("Section");
				header.createCell(13).setCellValue("Row");
				header.createCell(14).setCellValue("Sold Price");
				header.createCell(15).setCellValue("Market Price");
				header.createCell(16).setCellValue("Last Updated Price");
				header.createCell(17).setCellValue("Section Tix Count");
				header.createCell(18).setCellValue("Event Tix Count");
				header.createCell(19).setCellValue("Total Sold Price");
				header.createCell(20).setCellValue("Total Market Price");
				header.createCell(21).setCellValue("Profit/Loss");
				header.createCell(22).setCellValue("Price Updated Count");
				header.createCell(23).setCellValue("Price");
				header.createCell(24).setCellValue("Discount Coupen Price");
				header.createCell(25).setCellValue("Url");
				header.createCell(26).setCellValue("Shipping Method");
				header.createCell(27).setCellValue("Tracking No");
				header.createCell(28).setCellValue("Secondary Order Id");
				header.createCell(29).setCellValue("Secondary Order Type");
				header.createCell(30).setCellValue("Order Id");
				header.createCell(31).setCellValue("Last Updated");
				header.createCell(32).setCellValue("Broker Id");
				header.createCell(33).setCellValue("Company Name");
				header.createCell(34).setCellValue("Platform");
				int i=1;
				for(OpenOrderStatus order : closedOrdersList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(order.getInvoiceNo());
					row.createCell(1).setCellValue(order.getInvoiceDateStr());
					row.createCell(2).setCellValue(order.getEventName());
					row.createCell(3).setCellValue(order.getEventDateStr());
					row.createCell(4).setCellValue(order.getEventTimeStr());
					row.createCell(5).setCellValue(order.getVenueName());
					row.createCell(6).setCellValue(order.getVenueCity());
					row.createCell(7).setCellValue(order.getVenueState());
					row.createCell(8).setCellValue(order.getVenueCountry());
					row.createCell(9).setCellValue(order.getProductType());
					row.createCell(10).setCellValue(order.getInternalNotes());
					row.createCell(11).setCellValue(order.getSoldQty());
					row.createCell(12).setCellValue(order.getSection());
					row.createCell(13).setCellValue(order.getRow());
					row.createCell(14).setCellValue(order.getActualSoldPrice());
					row.createCell(15).setCellValue(order.getMarketPrice()!=null?String.valueOf(order.getMarketPrice()):"");
					row.createCell(16).setCellValue(order.getLastUpdatedPrice()!=null?String.valueOf(order.getLastUpdatedPrice()):"");
					row.createCell(17).setCellValue(order.getSectionTixQty()!=null?String.valueOf(order.getSectionTixQty()):"");
					row.createCell(18).setCellValue(order.getEventTixQty()!=null?String.valueOf(order.getEventTixQty()):"");
					row.createCell(19).setCellValue(order.getTotalActualSoldPrice());
					row.createCell(20).setCellValue(order.getTotalMarketPrice()!=null?String.valueOf(order.getTotalMarketPrice()):"");
					row.createCell(21).setCellValue(order.getProfitAndLoss()!=null?String.valueOf(order.getProfitAndLoss()):"");
					row.createCell(22).setCellValue(order.getPriceUpdateCount()!=null?String.valueOf(order.getPriceUpdateCount()):"");
					row.createCell(23).setCellValue(order.getPrice()!=null?order.getPrice():0);
					row.createCell(24).setCellValue(order.getDiscountCouponPrice()!=null?order.getDiscountCouponPrice():0);
					row.createCell(25).setCellValue(order.getUrl()!=null?order.getUrl():"");
					row.createCell(26).setCellValue(order.getShippingMethod());
					row.createCell(27).setCellValue(order.getTrackingNo());
					row.createCell(28).setCellValue(order.getSecondaryOrderId()!=null?String.valueOf(order.getSecondaryOrderId()):"");
					row.createCell(29).setCellValue(order.getSecondaryOrderType()!=null?String.valueOf(order.getSecondaryOrderType()):"");
					row.createCell(30).setCellValue(order.getOrderId()!=null?order.getOrderId():0);
					row.createCell(31).setCellValue(order.getLastUpdateStr()!=null?String.valueOf(order.getLastUpdateStr()):"");
					row.createCell(32).setCellValue(order.getBrokerId()!=null?order.getBrokerId().toString():"");
					row.createCell(33).setCellValue(order.getCompanyName()!=null?order.getCompanyName():"");
					if(order.getPlatform()!=null && order.getPlatform().equalsIgnoreCase("DESKTOP_SITE")){						
						row.createCell(34).setCellValue("WEB_ORDER");
					}else if(order.getPlatform()!=null && order.getPlatform().equalsIgnoreCase("TICK_TRACKER")){						
						row.createCell(34).setCellValue("RETAIL_ORDER");
					}else{
						row.createCell(34).setCellValue(order.getPlatform()!=null ? order.getPlatform() : "");
					}
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=closed_orders.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	
	@RequestMapping(value="/PastOrders")
	public PastOrderStatusDTO getPastOrdersTrackingPage(HttpServletRequest request, HttpServletResponse response){
		PastOrderStatusDTO pastOrderStatusDTO = new PastOrderStatusDTO();
		Error error = new Error();
		
		try {
			String eventIdStr = request.getParameter("eventSelect");
			String artistVenueName = request.getParameter("artistVenueName");
			String artistIdStr = request.getParameter("artistId");
			String venueIdStr = request.getParameter("venueId");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String profitLossSign = request.getParameter("profitLoss");
			String companyProduct = request.getParameter("productType");
			String invoiceNo = request.getParameter("invoiceNo");
			String externalOrderId = request.getParameter("externalOrderId");
			String orderId = request.getParameter("orderId");
			String product = request.getParameter("productTypeSession");
			String brokerIdStr = request.getParameter("brokerId");
			
			String action = request.getParameter("action");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				pastOrderStatusDTO.setError(error);
				pastOrderStatusDTO.setStatus(0);
				return pastOrderStatusDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				pastOrderStatusDTO.setError(error);
				pastOrderStatusDTO.setStatus(0);
				return pastOrderStatusDTO;
			}
			
			GridHeaderFilters filter = null;
			try{
				filter = GridHeaderFiltersUtil.getOpenOrShipmentPendingOrClosedOrderSearchHeaderFilters(headerFilter);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				pastOrderStatusDTO.setError(error);
				pastOrderStatusDTO.setStatus(0);
				return pastOrderStatusDTO;
			}
			
			Collection<OpenOrderStatus> passedOrderList = null;
			Collection<OpenOrderStatus> pastOrderList = null;
			Collection<EventDetails> events = null;
			String fromDateFinal = "";
			String toDateFinal="";
			Integer count = 0;
			
			if(null ==  profitLossSign || profitLossSign.isEmpty()){
				profitLossSign = String.valueOf(ProfitLossSign.ALL);
			}
			
			if(action!=null && action.equalsIgnoreCase("search")){
				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
				}else{
					fromDateFinal = null;
					toDateFinal = null;
				}				
			}else{
				companyProduct = "REWARDTHEFAN";
				
				if(product!=null && !product.isEmpty()){
					companyProduct = product;
				}
			}

			if(companyProduct != null && (companyProduct.equalsIgnoreCase("REWARDTHEFAN") || companyProduct.equalsIgnoreCase("ALL"))){
				companyProduct = "REWARDTHEFAN";
			}
			passedOrderList = DAORegistry.getQueryManagerDAO().getPassedOrders(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"DELETED",companyProduct,filter,false,pageNo,brokerId);
			//count = DAORegistry.getQueryManagerDAO().getPassedOrdersCount(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"DELETED",companyProduct,filter,brokerId);
			
			if(artistIdStr!=null && !artistIdStr.isEmpty()){
				events =  DAORegistry.getEventDetailsDAO().getEventsByArtistCity(Integer.parseInt(artistIdStr),null);
				
			}else if(venueIdStr!=null && !venueIdStr.isEmpty()){
				events = DAORegistry.getEventDetailsDAO().getEventsByVenueId(Integer.parseInt(venueIdStr));
			}
			
			//Remove Duplicate Invoices
			Map<Integer, OpenOrderStatus> openOrderMap  = new HashMap<Integer, OpenOrderStatus>();
			for(OpenOrderStatus pastOrderObj : passedOrderList){

				OpenOrderStatus openOrderStatusOldObj = openOrderMap.get(pastOrderObj.getInvoiceNo());
				
				if(null != openOrderStatusOldObj){
					
					String section = openOrderStatusOldObj.getSection();
					
					if(section != null && !section.isEmpty()){
						String[] sectionArray = null;
						if(section.contains(",")){
							sectionArray = section.split(",");
						}else{
							sectionArray = new String[1];
							sectionArray[0] = section;
						}
						boolean isNewSection = true;
						for (String oldSection : sectionArray) {
							if(oldSection.equals(pastOrderObj.getSection())){
								isNewSection = false;
								break;
							}
						}
						
						if(isNewSection){
							pastOrderObj.setSection(section+","+pastOrderObj.getSection());
						}
					}
					
					String row = openOrderStatusOldObj.getRow();
					
					if(row != null && !row.isEmpty()){
						String[] rowArray = null;
						if(row.contains(",")){
							rowArray = row.split(",");
						}else{
							rowArray = new String[1];
							rowArray[0] = row;
						}
						boolean isNewRow = true;
						for (String oldRow : rowArray) {
							if(oldRow.equals(pastOrderObj.getRow())){
								isNewRow = false;
								break;
							}
						}
						
						if(isNewRow){
							pastOrderObj.setRow(row+","+pastOrderObj.getRow());
						}
					}
				} 
				openOrderMap.put(pastOrderObj.getInvoiceNo(), pastOrderObj);
			}
			pastOrderList = openOrderMap.values();
			if(pastOrderList != null && pastOrderList.size() > 0){
				count = pastOrderList.size();
			}
			
			pastOrderStatusDTO.setStatus(1);
			pastOrderStatusDTO.setPastOrders(com.rtw.tracker.utils.Util.getOpenOrdersArray(pastOrderList));
			pastOrderStatusDTO.setPastOrdersPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
			pastOrderStatusDTO.setEventDetails(com.rtw.tracker.utils.Util.getEventDetailsArray(events));
			pastOrderStatusDTO.setProfitLossSign(ProfitLossSign.values());
			pastOrderStatusDTO.setSelectedProfitLossSign(ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()).toString());
			pastOrderStatusDTO.setProductType(companyProduct);
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Past Orders.");
			pastOrderStatusDTO.setError(error);
			pastOrderStatusDTO.setStatus(0);			
		}		
		return pastOrderStatusDTO;	
	}
	
	/*@RequestMapping(value="/GetPastOrders")
	public PastOrderStatusDTO getPastOrdersTracking(HttpServletRequest request, HttpServletResponse response){
		PastOrderStatusDTO pastOrderStatusDTO = new PastOrderStatusDTO();
		Error error = new Error();
		
		try {
			String eventIdStr = request.getParameter("eventSelect");
			String artistIdStr = request.getParameter("artistId");
			String venueIdStr = request.getParameter("venueId");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");			
			String profitLossSign = request.getParameter("profitLoss");
			String companyProduct = request.getParameter("productType");
			
			String invoiceNo = request.getParameter("invoiceNo");
			String externalOrderId = request.getParameter("externalOrderId");
			String orderId = request.getParameter("orderId");
			String action = request.getParameter("action");
			
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			String brokerIdStr = request.getParameter("brokerId");
			
			if(StringUtils.isEmpty(pageNo)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				pastOrderStatusDTO.setError(error);
				pastOrderStatusDTO.setStatus(0);
				return pastOrderStatusDTO;
			}
			try {
				Integer.parseInt(pageNo);
			} catch (Exception e) {
				System.err.println("Please provide valid PageNo.");
				error.setDescription("Please provide valid PageNo.");
				pastOrderStatusDTO.setError(error);
				pastOrderStatusDTO.setStatus(0);
				return pastOrderStatusDTO;
			}
			
			GridHeaderFilters filter = null;
			try{
				filter = GridHeaderFiltersUtil.getOpenOrShipmentPendingOrClosedOrderSearchHeaderFilters(headerFilter);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				pastOrderStatusDTO.setError(error);
				pastOrderStatusDTO.setStatus(0);
				return pastOrderStatusDTO;
			}
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				pastOrderStatusDTO.setError(error);
				pastOrderStatusDTO.setStatus(0);
				return pastOrderStatusDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				pastOrderStatusDTO.setError(error);
				pastOrderStatusDTO.setStatus(0);
				return pastOrderStatusDTO;
			}
			
			Collection<OpenOrderStatus> passedOrderList = null;
			Collection<OpenOrderStatus> pastOrderList = null;
			String fromDateFinal = "";
			String toDateFinal="";
			Integer count=0;
			
			if(null ==  profitLossSign || profitLossSign.isEmpty()){
				profitLossSign = String.valueOf(ProfitLossSign.ALL);
			}
			
			if(action!=null && action.equalsIgnoreCase("search")){
				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
				}else{
					fromDateFinal = null;
					toDateFinal = null;
				}
				if(companyProduct != null && !companyProduct.isEmpty()){
					passedOrderList = DAORegistry.getQueryManagerDAO().getPassedOrders(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"DELETED",companyProduct,filter,false,pageNo,brokerId);
					//count = DAORegistry.getQueryManagerDAO().getPassedOrdersCount(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"DELETED",companyProduct,filter,brokerId);
				}
				//Remove Duplicate Invoices
				Map<Integer, OpenOrderStatus> openOrderMap  = new HashMap<Integer, OpenOrderStatus>();
				for(OpenOrderStatus pastOrderObj : passedOrderList){
					openOrderMap.put(pastOrderObj.getInvoiceNo(), pastOrderObj);
				}
				pastOrderList = openOrderMap.values();
				if(pastOrderList != null && pastOrderList.size() > 0){
					count = pastOrderList.size();
				}
			}
			
			pastOrderStatusDTO.setStatus(1);
			pastOrderStatusDTO.setPastOrders(com.rtw.tracker.utils.Util.getOpenOrdersArray(pastOrderList));
			pastOrderStatusDTO.setPastOrdersPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Applying Search - Past Orders.");
			pastOrderStatusDTO.setError(error);
			pastOrderStatusDTO.setStatus(0);			
		}
		return pastOrderStatusDTO;
	}*/
	
	@RequestMapping(value="/PastOrdersExportToExcel")
	public void getPastOrdersToExport(HttpServletRequest request, HttpServletResponse response){
		
		try {
			String eventIdStr = request.getParameter("eventSelect");
			String artistIdStr = request.getParameter("artistId");
			String venueIdStr = request.getParameter("venueId");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String profitLossSign = request.getParameter("profitLoss");
			String companyProduct = request.getParameter("productType");

			String invoiceNo = request.getParameter("invoiceNo");
			String externalOrderId = request.getParameter("externalOrderId");
			String orderId = request.getParameter("orderId");
			String headerFilter = request.getParameter("headerFilter");
			String brokerIdStr = request.getParameter("brokerId");
			Integer brokerId = Integer.parseInt(brokerIdStr);
			
			GridHeaderFilters filter = GridHeaderFiltersUtil.getOpenOrShipmentPendingOrClosedOrderSearchHeaderFilters(headerFilter);
			
			Collection<OpenOrderStatus> passedOrderList = null;
			Collection<OpenOrderStatus> pastOrderList = null;
			String fromDateFinal = "";
			String toDateFinal="";
			
			if(null ==  profitLossSign || profitLossSign.isEmpty()){
				profitLossSign = String.valueOf(ProfitLossSign.ALL);
			}
			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";
			}else{
				fromDateFinal = null;
				toDateFinal = null;
			}
			if(companyProduct != null && !companyProduct.isEmpty()){
				passedOrderList = DAORegistry.getQueryManagerDAO().getPassedOrdersToExport(invoiceNo,eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"DELETED",companyProduct,externalOrderId,orderId,filter,brokerId);
			}			
						
			//Remove Duplicate Invoices
			Map<Integer, OpenOrderStatus> openOrderMap  = new HashMap<Integer, OpenOrderStatus>();
			for(OpenOrderStatus pastOrderObj : passedOrderList){

				OpenOrderStatus openOrderStatusOldObj = openOrderMap.get(pastOrderObj.getInvoiceNo());
				
				if(null != openOrderStatusOldObj){
					
					String section = openOrderStatusOldObj.getSection();
					
					if(section != null && !section.isEmpty()){
						String[] sectionArray = null;
						if(section.contains(",")){
							sectionArray = section.split(",");
						}else{
							sectionArray = new String[1];
							sectionArray[0] = section;
						}
						boolean isNewSection = true;
						for (String oldSection : sectionArray) {
							if(oldSection.equals(pastOrderObj.getSection())){
								isNewSection = false;
								break;
							}
						}
						
						if(isNewSection){
							pastOrderObj.setSection(section+","+pastOrderObj.getSection());
						}
					}
					
					String row = openOrderStatusOldObj.getRow();
					
					if(row != null && !row.isEmpty()){
						String[] rowArray = null;
						if(row.contains(",")){
							rowArray = row.split(",");
						}else{
							rowArray = new String[1];
							rowArray[0] = row;
						}
						boolean isNewRow = true;
						for (String oldRow : rowArray) {
							if(oldRow.equals(pastOrderObj.getRow())){
								isNewRow = false;
								break;
							}
						}
						
						if(isNewRow){
							pastOrderObj.setRow(row+","+pastOrderObj.getRow());
						}
					}
				} 
				openOrderMap.put(pastOrderObj.getInvoiceNo(), pastOrderObj);
			}
			pastOrderList = openOrderMap.values();
			
			if(pastOrderList!=null && !pastOrderList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("past_orders");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Invoice Id");
				header.createCell(1).setCellValue("Invoice Date");
				header.createCell(2).setCellValue("Event Name");
				header.createCell(3).setCellValue("Event Date");
				header.createCell(4).setCellValue("Event Time");
				header.createCell(5).setCellValue("Venue");
				header.createCell(6).setCellValue("Product Type");
				header.createCell(7).setCellValue("Internal Notes");
				header.createCell(8).setCellValue("Quantity");
				header.createCell(9).setCellValue("Section");
				header.createCell(10).setCellValue("Row");
				header.createCell(11).setCellValue("Sold Price");
				header.createCell(12).setCellValue("Market Price");
				header.createCell(13).setCellValue("Last Updated Price");
				header.createCell(14).setCellValue("Section Tix Count");
				header.createCell(15).setCellValue("Event Tix Count");
				header.createCell(16).setCellValue("Total Sold Price");
				header.createCell(17).setCellValue("Total Market Price");
				header.createCell(18).setCellValue("Profit/Loss");
				header.createCell(19).setCellValue("Price Updated Count");
				header.createCell(20).setCellValue("Price");
				header.createCell(21).setCellValue("Discount Coupen Price");
				header.createCell(22).setCellValue("Url");
				header.createCell(23).setCellValue("Shipping Method");
				header.createCell(24).setCellValue("Tracking No");
				header.createCell(25).setCellValue("Secondary Order Id");
				header.createCell(26).setCellValue("Secondary Order Type");
				header.createCell(27).setCellValue("Order Id");
				header.createCell(28).setCellValue("Last Updated");
				header.createCell(29).setCellValue("Broker Id");
				header.createCell(30).setCellValue("Customer Name");
				header.createCell(31).setCellValue("Company Name");
				header.createCell(32).setCellValue("Platform");
				int i=1;
				for(OpenOrderStatus order : pastOrderList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(order.getInvoiceNo());
					row.createCell(1).setCellValue(order.getInvoiceDateStr());
					row.createCell(2).setCellValue(order.getEventName());
					row.createCell(3).setCellValue(order.getEventDateStr());
					row.createCell(4).setCellValue(order.getEventTimeStr());
					row.createCell(5).setCellValue(order.getVenueName());
					row.createCell(6).setCellValue(order.getProductType());
					row.createCell(7).setCellValue(order.getInternalNotes());
					row.createCell(8).setCellValue(order.getSoldQty());
					row.createCell(9).setCellValue(order.getSection());
					row.createCell(10).setCellValue(order.getRow());
					row.createCell(11).setCellValue(order.getActualSoldPrice());
					row.createCell(12).setCellValue(order.getMarketPrice()!=null?String.valueOf(order.getMarketPrice()):"");
					row.createCell(13).setCellValue(order.getLastUpdatedPrice()!=null?String.valueOf(order.getLastUpdatedPrice()):"");
					row.createCell(14).setCellValue(order.getSectionTixQty()!=null?String.valueOf(order.getSectionTixQty()):"");
					row.createCell(15).setCellValue(order.getEventTixQty()!=null?String.valueOf(order.getEventTixQty()):"");
					row.createCell(16).setCellValue(order.getTotalActualSoldPrice());
					row.createCell(17).setCellValue(order.getTotalMarketPrice()!=null?String.valueOf(order.getTotalMarketPrice()):"");
					row.createCell(18).setCellValue(order.getProfitAndLoss()!=null?String.valueOf(order.getProfitAndLoss()):"");
					row.createCell(19).setCellValue(order.getPriceUpdateCount()!=null?String.valueOf(order.getPriceUpdateCount()):"");
					row.createCell(20).setCellValue(order.getPrice()!=null?order.getPrice():0);
					row.createCell(21).setCellValue(order.getDiscountCouponPrice()!=null?order.getDiscountCouponPrice():0);
					row.createCell(22).setCellValue(order.getUrl()!=null?order.getUrl():"");
					row.createCell(23).setCellValue(order.getShippingMethod());
					row.createCell(24).setCellValue(order.getTrackingNo());
					row.createCell(25).setCellValue(order.getSecondaryOrderId()!=null?String.valueOf(order.getSecondaryOrderId()):"");
					row.createCell(26).setCellValue(order.getSecondaryOrderType()!=null?String.valueOf(order.getSecondaryOrderType()):"");
					row.createCell(27).setCellValue(order.getOrderId()!=null?order.getOrderId():0);
					row.createCell(28).setCellValue(order.getLastUpdateStr()!=null?String.valueOf(order.getLastUpdateStr()):"");
					row.createCell(29).setCellValue(order.getBrokerId()!=null?order.getBrokerId().toString():"");
					row.createCell(30).setCellValue(order.getCustomerName()!=null?order.getCustomerName():"");
					row.createCell(31).setCellValue(order.getCompanyName()!=null?order.getCompanyName():"");					
					if(order.getPlatform()!=null && order.getPlatform().equalsIgnoreCase("DESKTOP_SITE")){						
						row.createCell(32).setCellValue("WEB_ORDER");
					}else if(order.getPlatform()!=null && order.getPlatform().equalsIgnoreCase("TICK_TRACKER")){						
						row.createCell(32).setCellValue("RETAIL_ORDER");
					}else{
						row.createCell(32).setCellValue(order.getPlatform()!=null ? order.getPlatform() : "");
					}
					
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=past_orders.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	/*	
	@RequestMapping(value = "/GetSoldTickets")
	public String loadSoldTicketOfEvent(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		String soldTicketPage = "page-sold-tickets";
		String eventIdStr = request.getParameter("eventId");
		String crawlTimeStr = request.getParameter("crawlTime");
		Date crawlTime;
		Integer eventId = null;
		Event event = null;
		if(eventIdStr!=null && !eventIdStr.isEmpty()){
			eventId = Integer.parseInt(eventIdStr);
			event = TMATDAORegistry.getEventDAO().getEventById(eventId);
		}
		if(event==null){
			return soldTicketPage;
		}
		if(crawlTimeStr == null){
			crawlTime = new Date();
		}else{
			crawlTime = new Date(Long.parseLong(crawlTimeStr));
		}
		Collection<Ticket> tickets = new ArrayList<Ticket>();
		String noCrawl = request.getParameter("nocrawl");
		String id = request.getParameter("id");
		map.put("eventId",event.getId());
		map.put("id",id);
		map.put("crawlTime", crawlTime.getTime());
		if(noCrawl==null){
			noCrawl = "false";
		}
		
		OpenOrderStatus openOrderStatus = DAORegistry.getOpenOrderStatusDao().get(Integer.parseInt(id));		
		if("true".equalsIgnoreCase(noCrawl)){
			
			try{
				tickets = TMATDAORegistry.getTicketDAO().getAllActiveTicketsByNormalizedSectionAndEventId(event.getId(), openOrderStatus.getSection());
				if(tickets != null && tickets.size() > 0){
					List<ManagePurchasePrice> managePurchasePrices = TMATDAORegistry.getManagePurchasePriceDAO().getAllManagePurchasePriceByEventId(event.getId());
					Map<String,ManagePurchasePrice> managePurchasePriceMap = new HashMap<String, ManagePurchasePrice>();
					if(managePurchasePrices!=null && !managePurchasePrices.isEmpty() ){
						for(ManagePurchasePrice managePurchasePrice:managePurchasePrices){
							managePurchasePriceMap.put(managePurchasePrice.getExchange()+"-" + managePurchasePrice.getTicketType(), managePurchasePrice);
						}
					}
					Collection<DefaultPurchasePrice> defaultPurchasePrices = TMATDAORegistry.getDefaultPurchasePriceDAO().getAll();
					Map<String, DefaultPurchasePrice> defaultPurhasePriceMap = new HashMap<String, DefaultPurchasePrice>();
					for(DefaultPurchasePrice defaultPurchasePrice:defaultPurchasePrices){
						defaultPurhasePriceMap.put(defaultPurchasePrice.getExchange()+ "-" + defaultPurchasePrice.getTicketType(), defaultPurchasePrice);
					}
					
					DecimalFormat df2 = new DecimalFormat(".##");
					df2.setRoundingMode(RoundingMode.UP);
					
					for(Ticket ticket:tickets){
						String mapKey = ticket.getSiteId()+ "-" + (ticket.getTicketDeliveryType()==null?"REGULAR":ticket.getTicketDeliveryType());
						ManagePurchasePrice managePurchasePrice=managePurchasePriceMap.get(mapKey);
						double tempPrice = ticket.getCurrentPrice();
						
						if(managePurchasePrice!=null){
							double serviceFee=managePurchasePrice.getServiceFee();
							double  shippingFee=managePurchasePrice.getShipping();
							int currencyType=managePurchasePrice.getCurrencyType();
							tempPrice=Double.parseDouble(df2.format((currencyType==1?((ticket.getCurrentPrice()*(1+serviceFee/100)) + (shippingFee/ticket.getQuantity())):(ticket.getCurrentPrice() + serviceFee + (shippingFee/ticket.getQuantity())))));
						}else{
							DefaultPurchasePrice defaultPurchasePrice =  defaultPurhasePriceMap.get(mapKey);
							if(defaultPurchasePrice!=null){
								double serviceFee=defaultPurchasePrice.getServiceFees();
								double  shippingFee=defaultPurchasePrice.getShipping();
								int currencyType=defaultPurchasePrice.getCurrencyType();
								tempPrice=Double.parseDouble(df2.format((currencyType==1?((ticket.getCurrentPrice()*(1+serviceFee/100)) + (shippingFee/ticket.getQuantity())):(ticket.getCurrentPrice() + serviceFee + (shippingFee/ticket.getQuantity())))));
							}
						}
						ticket.setPurchasePrice(tempPrice);
					}
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			noCrawl = "true";
			map.put("reload", new Date().getTime());
		}else{
			noCrawl = "false";
		}
		map.put("noCrawl", noCrawl);
		map.put("tickets", tickets);
		map.put("openOrderStatus", openOrderStatus);
		return soldTicketPage;
	}
		
	@RequestMapping(value = "/CreateOpenOrder")
	public String createOpenOrders(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		String returnMessage = "";
		String action = "";
		String invoiceId = "";
		try {
			//Getting Broker ID
			HttpSession session = request.getSession(true);
			Integer brokerId = Util.getBrokerId(session);
			
			invoiceId = request.getParameter("invoiceId");
			Integer invId = Integer.parseInt(invoiceId);
			Collection<OpenOrders> openOrderListByInv = DAORegistry.getQueryManagerDAO().getOpenOrderListByInvoice(invId, brokerId);
			Integer customerId = 0;
			Integer eventId = 0;
			for(OpenOrders openOrders: openOrderListByInv){
				customerId = openOrders.getCustomerId();
				eventId = openOrders.getEventId();
			}
			
//			List<CustomerAddress> shippingAddressList = DAORegistry.getCustomerAddressDAO().getShippingAddressInfo(customerId);
			List<CustomerAddress> shippingAddressList = DAORegistry.getCustomerAddressDAO().updatedShippingAddress(customerId);
			EventDetails eventDetail = null;
			if(eventId != null){
				eventDetail = DAORegistry.getEventDetailsDAO().get(eventId);
			}			
			map.put("eventDtl", eventDetail);
			map.put("openOrders", openOrderListByInv);
			map.put("shippingAddress", shippingAddressList);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("errorMessage", "There is something wrong. Please try again..");
		}
		return "page-create-openorder";
	}				
	
	
	@RequestMapping(value="/Fedex")
	public String getFedexDetails(HttpServletRequest request, HttpServletResponse response,ModelMap map){
		String ticketTracking = "page-fedex";
		try {
			
		} catch (Exception e) {
			e.printStackTrace();
		}


		return ticketTracking;
	}
	
	@RequestMapping(value="/TicketTracking")
	public String getTicketTrackingPage(ModelMap map){
		String ticketTracking = "page-ticket-tracking";
		
		map.put("url", "Deliveries");
		return ticketTracking;
	}
	*/
}
