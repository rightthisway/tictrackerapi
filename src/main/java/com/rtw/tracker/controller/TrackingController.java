package com.rtw.tracker.controller;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.rtw.tmat.utils.Error;
import com.rtw.tmat.utils.Util;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.WebServiceTracking;
import com.rtw.tracker.pojos.WebServiceTrackingDTO;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.GridHeaderFiltersUtil;
import com.rtw.tracker.utils.PaginationUtil;

@Controller
public class TrackingController {
	
	@RequestMapping({"/TrackingHome"})
	public WebServiceTrackingDTO loadTrackingHomePage(HttpServletRequest request, HttpServletResponse response){
		WebServiceTrackingDTO webServiceTrackingDTO = new WebServiceTrackingDTO();
		Error error = new Error();
		try{	
			Integer websiteIPCount = 0;
			Collection<WebServiceTracking> webServiceTrackingList = null;
			String fromDateStr = request.getParameter("fromDate");
			String startHour = request.getParameter("startHour");
			String startMinute = request.getParameter("startMinute");
			String toDateStr = request.getParameter("toDate");
			String endHour = request.getParameter("endHour");
			String endMinute = request.getParameter("endMinute");
			String logType = request.getParameter("logType");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String sortingString = request.getParameter("sortingString");
			String fromDateFinal = "";
			String toDateFinal ="";
			GridHeaderFilters filter = GridHeaderFiltersUtil.getWebsiteTrackingSearchHeaderFilters(headerFilter+sortingString);
			if(fromDateStr == null || fromDateStr.isEmpty() || toDateStr == null || toDateStr.isEmpty()){
				Calendar cal = Calendar.getInstance();
				Date toDate = new Date();
				cal.add(Calendar.DAY_OF_MONTH, -3);
				Date fromDate = cal.getTime();
				fromDateStr = Util.formatDateToMonthDateYear(fromDate);
				toDateStr = Util.formatDateToMonthDateYear(toDate);
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";				
			}else if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " "+startHour+":"+startMinute+":00";
				toDateFinal = toDateStr + " "+endHour+":"+endMinute+":00";
			}
			if(logType.equalsIgnoreCase("OTHER")){
				webServiceTrackingList = DAORegistry.getQueryManagerDAO().getAllActionsByDateRangeAndServerIp(fromDateFinal, toDateFinal, filter, pageNo);
				websiteIPCount = DAORegistry.getQueryManagerDAO().getWebsiteIPSearchCount(fromDateFinal, toDateFinal, filter);
			}else if(logType.equalsIgnoreCase("CONTEST")){
				webServiceTrackingList = DAORegistry.getQueryManagerDAO().getContestTracking(fromDateFinal, toDateFinal, filter, pageNo);
				websiteIPCount = DAORegistry.getQueryManagerDAO().getContestTrackingCount(fromDateFinal, toDateFinal, filter);
			}
			
			webServiceTrackingDTO.setFromDate(fromDateStr);
			webServiceTrackingDTO.setToDate(toDateStr);
			webServiceTrackingDTO.setStatus(1);
			webServiceTrackingDTO.setWebServiceTrackingList(webServiceTrackingList);
			webServiceTrackingDTO.setWebsiteIPCount(websiteIPCount);
			webServiceTrackingDTO.setPaginationDTO(PaginationUtil.calculatePaginationParameter(websiteIPCount,pageNo));
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Website Tracking");
			webServiceTrackingDTO.setError(error);
			webServiceTrackingDTO.setStatus(0);
		}
		return webServiceTrackingDTO;
	}

	
	/*@RequestMapping({"/GetHome"})
	public void loadTrackingHom(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		JSONObject returnObject = new JSONObject();
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		Collection<WebServiceTracking> webServiceTrackingList = null;
		String fromDateStr = request.getParameter("fromDate");
		String toDateStr = request.getParameter("toDate");
		String customerIpAddress = request.getParameter("customerIpAddress");
		String pageNo = request.getParameter("pageNo");
		if(StringUtils.isEmpty(pageNo)){
			
		}
		String headerFilter = request.getParameter("headerFilter");
		String productType = request.getParameter("productType");
		String actionType = request.getParameter("actionType");
		Integer websiteIPCount = 0;
		try{			
			GridHeaderFilters filter = GridHeaderFiltersUtil.getWebsiteTrackingSearchHeaderFilters(headerFilter);
			if((fromDateStr == null || fromDateStr.isEmpty()) && (toDateStr == null || toDateStr.isEmpty()) && (customerIpAddress == null || customerIpAddress.isEmpty())){
				Calendar cal = Calendar.getInstance();
				Date toDate = new Date();
				cal.add(Calendar.MONTH, -3);
				Date fromDate = cal.getTime();
				fromDateStr = dateFormat.format(fromDate);
				toDateStr = dateFormat.format(toDate);
				String fromDateFinal = fromDateStr + " 00:00:00";
				String toDateFinal = toDateStr + " 23:59:23";				
				webServiceTrackingList = DAORegistry.getQueryManagerDAO().getAllActionsByDateRangeAndServerIp(productType,actionType,dateTimeFormat.parse(fromDateFinal),dateTimeFormat.parse(toDateFinal),customerIpAddress,filter,pageNo);
				websiteIPCount = DAORegistry.getQueryManagerDAO().getWebsiteIPSearchCount(productType,actionType,dateTimeFormat.parse(fromDateFinal),dateTimeFormat.parse(toDateFinal),customerIpAddress,filter);
			}else{
				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){					
					String fromDateFinal = fromDateStr + " 00:00:00";
					String toDateFinal = toDateStr + " 23:59:23";
					webServiceTrackingList = DAORegistry.getQueryManagerDAO().getAllActionsByDateRangeAndServerIp(productType,actionType,dateTimeFormat.parse(fromDateFinal),dateTimeFormat.parse(toDateFinal),customerIpAddress, filter, pageNo);
					websiteIPCount = DAORegistry.getQueryManagerDAO().getWebsiteIPSearchCount(productType,actionType,dateTimeFormat.parse(fromDateFinal),dateTimeFormat.parse(toDateFinal),customerIpAddress, filter);
				}else{
					webServiceTrackingList = DAORegistry.getQueryManagerDAO().getAllActionsByDateRangeAndServerIp(productType,actionType,null,null,customerIpAddress, filter,pageNo);
					websiteIPCount = DAORegistry.getQueryManagerDAO().getWebsiteIPSearchCount(productType,actionType,null,null,customerIpAddress, filter);
				}				
			}
						
		returnObject.put("webServiceTrackingList", JsonWrapperUtil.getWebServiceTrackingArray(webServiceTrackingList));
		returnObject.put("pagingInfo", PaginationUtil.calculatePaginationParameters(websiteIPCount, pageNo));
		returnObject.put("totalCount", websiteIPCount);
		returnObject.put("fromDate", fromDateStr);
		returnObject.put("toDate", toDateStr);
		returnObject.put("customerIpAddress", customerIpAddress);
		IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
			//returnObject.put("errorMessage", "There is something wrong. Please try again");
		}
	}*/
	
	
	/*@RequestMapping({"/TrackByDate"})
	public String loadTrackingByDatePage(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		
		String returnMessage = "";
		String message = "";
		SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		String action = request.getParameter("action");
			
		try{
			if(action != null && action.equals("action")){
				String fromDate = request.getParameter("fromDate");
				String toDate = request.getParameter("toDate");
				String customerIpAddress = request.getParameter("customerIpAddress");
				String pageNo = request.getParameter("pageNo");												
				Date fromDateCon = (Date)dateFormat.parse(fromDate);
				String fromDateStr = dbDateFormat.format(fromDateCon);
				String finalFromDate=fromDateStr+ " 00:00:00.000000";
				
				Date toDateCon = (Date)dateFormat.parse(toDate);
				String toDateStr = dbDateFormat.format(toDateCon);
				String finalToDate=toDateStr+ " 23:59:00.000000";
				
				Date resultFromDate=null;
				Date resultToDate=null;
			    try {
			       	resultFromDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS").parse(finalFromDate);
			       	resultToDate   = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS").parse(finalToDate);
			       	
			     	
			    } catch (Exception e) {
			        e.printStackTrace();
			    }
			    		    				
				if(finalFromDate == null || finalFromDate.isEmpty()){
					returnMessage = "From date cant' be blank";
				}else if(finalToDate == null || finalToDate.isEmpty()){
					returnMessage = "To date cant' be blank";
				}else if (customerIpAddress==null||customerIpAddress.isEmpty()){
					returnMessage = "Customer IP Address cant' be blank";
				}
				GridHeaderFilters filter = new GridHeaderFilters();
				if(returnMessage.isEmpty()){
					Collection<WebServiceTracking> webServiceTrackingList =DAORegistry.getQueryManagerDAO().getAllActionsByDateRangeAndServerIp(null,null,resultFromDate,resultToDate,customerIpAddress,filter,pageNo);
				
					if(webServiceTrackingList.size()>0){
						map.put("webServiceTrackingList", webServiceTrackingList);
						map.put("fromDate", dateFormat.format(fromDateCon));
						map.put("toDate", dateFormat.format(toDateCon));
					}
					else{
						message = "No Record found for the dates";
						map.put("recordNotFound", message);
					}
					
				}else{
					map.put("errorMessage", returnMessage);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			returnMessage = "There is something wrong. Please try again";
			map.put("errorMessage", returnMessage);
		}				
		return "page-tracking-home";
	}*/
	

	@RequestMapping({"/WebTrackingExportToExcel"})
	public void webTrackingToExport(HttpServletRequest request, HttpServletResponse response){
		
		try{
			String logType = request.getParameter("logType");
			String fromDateStr = request.getParameter("fromDate");
			String startHour = request.getParameter("startHour");
			String startMinute = request.getParameter("startMinute");
			String toDateStr = request.getParameter("toDate");
			String endHour = request.getParameter("endHour");
			String endMinute = request.getParameter("endMinute");
			String headerFilter = request.getParameter("headerFilter");
			Collection<WebServiceTracking> webServiceTrackingList = null;
			
			String fromDateFinal = "";
			String toDateFinal = "";
			GridHeaderFilters filter = GridHeaderFiltersUtil.getWebsiteTrackingSearchHeaderFilters(headerFilter);
			if(fromDateStr == null || fromDateStr.isEmpty() || toDateStr == null || toDateStr.isEmpty()){
				Calendar cal = Calendar.getInstance();
				Date toDate = new Date();
				cal.add(Calendar.DAY_OF_MONTH, -3);
				Date fromDate = cal.getTime();
				fromDateStr = Util.formatDateToMonthDateYear(fromDate);
				toDateStr = Util.formatDateToMonthDateYear(toDate);
				fromDateFinal = fromDateStr + " "+startHour+":"+startMinute+":00";
				toDateFinal = toDateStr + " "+endHour+":"+endMinute+":00";				
			}else if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " "+startHour+":"+startMinute+":00";
				toDateFinal = toDateStr + " "+endHour+":"+endMinute+":00";
			}
			
			if(logType.equalsIgnoreCase("OTHER")){
				webServiceTrackingList = DAORegistry.getQueryManagerDAO().getWebsiteTrackingToExport(fromDateFinal,toDateFinal,filter);
			}else if(logType.equalsIgnoreCase("CONTEST")){
				webServiceTrackingList = DAORegistry.getQueryManagerDAO().getContestLogToExport(fromDateFinal,toDateFinal,filter);
			}
			
			if(webServiceTrackingList!=null && !webServiceTrackingList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Webservice_Tracking");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Tracking Id");
				header.createCell(1).setCellValue("Customer IP Address");
				header.createCell(2).setCellValue("Platform");
				header.createCell(3).setCellValue("Hitting Date");
				header.createCell(4).setCellValue("Api Name");
				header.createCell(5).setCellValue("Customer Id");
				header.createCell(6).setCellValue("Contest Id");
				header.createCell(7).setCellValue("Decription");
				header.createCell(8).setCellValue("App Version");
				
				Integer i=1;
				for(WebServiceTracking tracking : webServiceTrackingList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(tracking.getId());
					row.createCell(1).setCellValue(tracking.getCustomerIpAddress()!=null?tracking.getCustomerIpAddress():"");
					row.createCell(2).setCellValue(tracking.getPlatform()!=null?tracking.getPlatform():"");
					row.createCell(3).setCellValue(tracking.getHitDateStr()!=null?tracking.getHitDateStr():"");
					row.createCell(4).setCellValue(tracking.getApiName()!=null?tracking.getApiName():"");
					row.createCell(5).setCellValue(tracking.getCustId()!=null?tracking.getCustId().toString():"");
					row.createCell(6).setCellValue(tracking.getContestId()!=null?tracking.getContestId().toString():"");
					row.createCell(7).setCellValue(tracking.getDescription()!=null?tracking.getDescription():"");
					row.createCell(8).setCellValue(tracking.getAppVersion()!=null?tracking.getAppVersion():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Webservice_Tracking.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
}
