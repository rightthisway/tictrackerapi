package com.rtw.tracker.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rtw.tmat.utils.Error;
import com.rtw.tmat.utils.Util;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.dao.implementation.QuizDAORegistry;
import com.rtw.tracker.datas.AddressType;
import com.rtw.tracker.datas.AffiliatePromoCodeHistory;
import com.rtw.tracker.datas.AffiliateSetting;
import com.rtw.tracker.datas.ApplicationPlatform;
import com.rtw.tracker.datas.Artist;
import com.rtw.tracker.datas.ChildCategory;
import com.rtw.tracker.datas.Contests;
import com.rtw.tracker.datas.Country;
import com.rtw.tracker.datas.Customer;
import com.rtw.tracker.datas.CustomerAddress;
import com.rtw.tracker.datas.CustomerInvoices;
import com.rtw.tracker.datas.CustomerLevel;
import com.rtw.tracker.datas.CustomerLoyalFan;
import com.rtw.tracker.datas.CustomerLoyalFanAudit;
import com.rtw.tracker.datas.CustomerLoyalty;
import com.rtw.tracker.datas.CustomerLoyaltyTracking;
import com.rtw.tracker.datas.CustomerOrder;
import com.rtw.tracker.datas.CustomerStripeCreditCard;
import com.rtw.tracker.datas.CustomerType;
import com.rtw.tracker.datas.CustomerWallet;
import com.rtw.tracker.datas.CustomerWalletTracking;
import com.rtw.tracker.datas.Customers;
import com.rtw.tracker.datas.DiscountCodeTracking;
import com.rtw.tracker.datas.Event;
import com.rtw.tracker.datas.EventDetails;
import com.rtw.tracker.datas.EventStatus;
import com.rtw.tracker.datas.FavouriteEvents;
import com.rtw.tracker.datas.GrandChildCategory;
import com.rtw.tracker.datas.InvoiceTicketAttachment;
import com.rtw.tracker.datas.ParentCategory;
import com.rtw.tracker.datas.PopularArtist;
import com.rtw.tracker.datas.ProductType;
import com.rtw.tracker.datas.PurchaseOrders;
import com.rtw.tracker.datas.RTFCustomerPromotionalOffer;
import com.rtw.tracker.datas.RTFPromoOffers;
import com.rtw.tracker.datas.RTFPromoType;
import com.rtw.tracker.datas.RTFPromotionalOfferTracking;
import com.rtw.tracker.datas.RewardDetails;
import com.rtw.tracker.datas.Role;
import com.rtw.tracker.datas.SignupType;
import com.rtw.tracker.datas.State;
import com.rtw.tracker.datas.TrackerBrokers;
import com.rtw.tracker.datas.TrackerUser;
import com.rtw.tracker.datas.Venue;
import com.rtw.tracker.enums.PromotionalType;
import com.rtw.tracker.enums.RewardStatus;
import com.rtw.tracker.mail.MailManager;
import com.rtw.tracker.pojos.AddCustomerDTO;
import com.rtw.tracker.pojos.AddUserDTO;
import com.rtw.tracker.pojos.AffiliateSettingsCustomDTO;
import com.rtw.tracker.pojos.AutoCompleteArtistVenueAndCategoryDTO;
import com.rtw.tracker.pojos.CustomerAddRewardPointDTO;
import com.rtw.tracker.pojos.CustomerAddWalletDTO;
import com.rtw.tracker.pojos.CustomerAddressDTO;
import com.rtw.tracker.pojos.CustomerInfoDTO;
import com.rtw.tracker.pojos.CustomerInfoFavouriteEventDTO;
import com.rtw.tracker.pojos.CustomerInfoInvoiceDTO;
import com.rtw.tracker.pojos.CustomerInfoInvoiceTicketDTO;
import com.rtw.tracker.pojos.CustomerInfoLoyalFanDTO;
import com.rtw.tracker.pojos.CustomerInfoPODTO;
import com.rtw.tracker.pojos.CustomerInfoRewardPointsHistoryDTO;
import com.rtw.tracker.pojos.CustomerInvoiceDTO;
import com.rtw.tracker.pojos.CustomerInvoiceTicketAttachmentDTO;
import com.rtw.tracker.pojos.CustomerLoyalFanDTO;
import com.rtw.tracker.pojos.CustomerLoyaltyDTO;
import com.rtw.tracker.pojos.CustomerPromotionalOfferDetailsDTO;
import com.rtw.tracker.pojos.CustomerPromotionalOrderSummaryDTO;
import com.rtw.tracker.pojos.CustomerPurchaseOrderDTO;
import com.rtw.tracker.pojos.CustomerStripeCreditCardDTO;
import com.rtw.tracker.pojos.CustomerTopLevelInfoDTO;
import com.rtw.tracker.pojos.CustomerWalletDTO;
import com.rtw.tracker.pojos.DiscountCodeDetailDTO;
import com.rtw.tracker.pojos.EditTrackerUserCustomDTO;
import com.rtw.tracker.pojos.EditUsersDTO;
import com.rtw.tracker.pojos.GenerateDiscountCodeDTO;
import com.rtw.tracker.pojos.GenericResponseDTO;
import com.rtw.tracker.pojos.ManageCustomersDTO;
import com.rtw.tracker.pojos.SaveDiscountCodeDTO;
import com.rtw.tracker.pojos.SharingDiscountCodeDTO;
import com.rtw.tracker.pojos.TrackerBrokerCustomDTO;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.GridHeaderFiltersUtil;
import com.rtw.tracker.utils.GsonCustomConfig;
import com.rtw.tracker.utils.JsonWrapperUtil;
import com.rtw.tracker.utils.PaginationUtil;

@Controller
public class ClientController {

	private static Logger LOGGER = LoggerFactory.getLogger(ClientController.class);
	
	private SharedProperty sharedProperty;
	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}

	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}
	
	private MailManager mailManager;
	public MailManager getMailManager() {
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}
	
	/**
	 * Manage customer details
	 * @param request
	 * @param response
	 * @param map
	 * @param session
	 * @return
	 */
	@RequestMapping({"/ManageDetails"})
	public ManageCustomersDTO clientBrokerManageDetailsPage(HttpServletRequest request, HttpServletResponse response){
		ManageCustomersDTO manageCustomersDTO = new ManageCustomersDTO();
		Error error = new Error();
		//model.addAttribute("manageCustomersDTO", manageCustomersDTO);		
		
		try{			
			String customerName = request.getParameter("customerName");
			String productType = request.getParameter("productType");
			String product = request.getParameter("productTypeSession");
			String brokerIdStr = request.getParameter("brokerId");
			String sortingString = request.getParameter("sortingString");
			
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");	
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide BrokerId");
				error.setDescription("Please provide BrokerId");
				manageCustomersDTO.setError(error);
				manageCustomersDTO.setStatus(0);
				return manageCustomersDTO;
			}			
			Integer brokerId = 0;	
			try{
				brokerId = Integer.parseInt(brokerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid BrokerId");
				error.setDescription("Please provide valid BrokerId");
				manageCustomersDTO.setError(error);
				manageCustomersDTO.setStatus(0);
				return manageCustomersDTO;
			}
			
			GridHeaderFilters filter = null;
			try {
				filter = GridHeaderFiltersUtil.getCustomerSearchHeaderFilters(headerFilter+sortingString);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				manageCustomersDTO.setError(error);
				manageCustomersDTO.setStatus(0);
				return manageCustomersDTO;
			}
			
			if(product!=null && !product.isEmpty()){
				productType = product;
			}
			
			if(filter.getProductType()==null || filter.getProductType().isEmpty()){
				filter.setProductType(productType);
			}
			
			List<Country> countryList = null;
			Collection<Customers> customerList = null;
			Integer count = 0;
			
			customerList = DAORegistry.getQueryManagerDAO().getCustomers(null,customerName,productType,brokerId,filter,pageNo);			
			count = DAORegistry.getCustomerDAO().getTotalCustomerCount(null,customerName,productType,brokerId,filter);			
			countryList = DAORegistry.getQueryManagerDAO().getCountries();
			
			if(customerList == null || customerList.size() <= 0){
				manageCustomersDTO.setMessage("No Customer found for selected search filter.");
			}
			manageCustomersDTO.setCustomersDTO(com.rtw.tracker.utils.Util.getCustomerArray(customerList));
			manageCustomersDTO.setPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
			manageCustomersDTO.setCountryDTO(com.rtw.tracker.utils.Util.getCountryArray(countryList));
			manageCustomersDTO.setProductType(productType);
			manageCustomersDTO.setStatus(1);
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Customers");
			manageCustomersDTO.setError(error);
			manageCustomersDTO.setStatus(0);
		}
		return manageCustomersDTO;
	}
	
	/*@RequestMapping({"/GetCustomers"})
	public ManageCustomersDTO getCustomers(HttpServletRequest request, HttpServletResponse response,HttpSession session){
		ManageCustomersDTO manageCustomersDTO = new ManageCustomersDTO();
		Error error = new Error();
		//model.addAttribute("manageCustomersDTO", manageCustomersDTO);
		
		try{
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");			
			String brokerIdStr = request.getParameter("brokerId");
			String productType = request.getParameter("productTypeSession");
			Integer count = 0;
			
			if(StringUtils.isEmpty(pageNo)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				manageCustomersDTO.setError(error);
				manageCustomersDTO.setStatus(0);
				return manageCustomersDTO;
			}
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide BrokerId");
				error.setDescription("Please provide BrokerId");
				manageCustomersDTO.setError(error);
				manageCustomersDTO.setStatus(0);
				return manageCustomersDTO;
			}
			
			Integer brokerId = 0;	
			try{
				brokerId = Integer.parseInt(brokerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid BrokerId");
				error.setDescription("Please provide valid BrokerId");
				manageCustomersDTO.setError(error);
				manageCustomersDTO.setStatus(0);
				return manageCustomersDTO;
			}
			try {
				Integer.parseInt(pageNo);
			} catch (Exception e) {
				System.err.println("Please provide valid PageNo.");
				error.setDescription("Please provide valid PageNo.");
				manageCustomersDTO.setError(error);
				manageCustomersDTO.setStatus(0);
				return manageCustomersDTO;
			}
			
			GridHeaderFilters filter = null;
			try {
				filter = GridHeaderFiltersUtil.getCustomerSearchHeaderFilters(headerFilter);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				manageCustomersDTO.setError(error);
				manageCustomersDTO.setStatus(0);
				return manageCustomersDTO;
			}
			if(filter.getProductType()==null || filter.getProductType().isEmpty()){
				filter.setProductType(productType);
			}
			
			Collection<Customers> customerList = DAORegistry.getQueryManagerDAO().getCustomers(null,null,null,brokerId,filter,pageNo);
			count = DAORegistry.getCustomerDAO().getTotalCustomerCount(null,null,null,brokerId,filter);
			
			if(customerList == null || customerList.size() <= 0){
				manageCustomersDTO.setMessage("No Customer found for selected search filter.");
			}
			manageCustomersDTO.setCustomersDTO(com.rtw.tracker.utils.Util.getCustomerArray(customerList));
			manageCustomersDTO.setPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
			manageCustomersDTO.setStatus(1);
		
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Customers");
			manageCustomersDTO.setError(error);
			manageCustomersDTO.setStatus(0);
		}
		return manageCustomersDTO;
	}*/
	
	@RequestMapping({"/ExportToExcel"})
	public void customersToExport(HttpServletRequest request, HttpServletResponse response){
		
		try{
			String searchBy = request.getParameter("searchBy");
			String searchValue = request.getParameter("searchValue");
			String productType = request.getParameter("productType");
			String customerType = request.getParameter("customerTypeSearchValues");
			String brokerIdStr = request.getParameter("brokerId");
			String headerFilter = request.getParameter("headerFilter");
			
			GridHeaderFilters filter = GridHeaderFiltersUtil.getCustomerSearchHeaderFilters(headerFilter);
			
			Integer brokerId = Integer.parseInt(brokerIdStr);
			/*Integer brokerId = Util.getBrokerId(session);*/
			Collection<Customers> customerList = DAORegistry.getQueryManagerDAO().getCustomersToExport(null,searchBy,searchValue,productType,brokerId,filter,customerType);
			
			if(customerList!=null && !customerList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("customers");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Customer Id");
				header.createCell(1).setCellValue("Customer Type");
				header.createCell(2).setCellValue("Product Type");
				header.createCell(3).setCellValue("Signup Type");
				header.createCell(4).setCellValue("Customer Status");
				header.createCell(5).setCellValue("Company Name");
				header.createCell(6).setCellValue("Customer Name");
				header.createCell(7).setCellValue("Last Name");
				header.createCell(8).setCellValue("Customer Email");
				header.createCell(9).setCellValue("Address Line1");
				header.createCell(10).setCellValue("Address Line2");
				header.createCell(11).setCellValue("City");
				header.createCell(12).setCellValue("Billing Address Id");
				header.createCell(13).setCellValue("State");
				header.createCell(14).setCellValue("Country");
				header.createCell(15).setCellValue("Zipcode");
				header.createCell(16).setCellValue("Client");
				header.createCell(17).setCellValue("Broker");
				header.createCell(18).setCellValue("Phone");
				header.createCell(19).setCellValue("Referrer Code");
				header.createCell(20).setCellValue("Broker Id");
				Integer i=1;
				for(Customers customer : customerList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(customer.getCustomerId());
					row.createCell(1).setCellValue(customer.getCustomerType());
					row.createCell(2).setCellValue(customer.getProductType()!=null?customer.getProductType():"");
					row.createCell(3).setCellValue(customer.getSignupType()!=null?customer.getSignupType():"");
					row.createCell(4).setCellValue(customer.getCustomerStatus());
					row.createCell(5).setCellValue(customer.getCompanyName()!=null?customer.getCompanyName():"");
					row.createCell(6).setCellValue(customer.getCustomerName()!=null?customer.getCustomerName():"");
					row.createCell(7).setCellValue(customer.getLastName()!=null?customer.getLastName():"");
					row.createCell(8).setCellValue(customer.getCustomerEmail());
					row.createCell(9).setCellValue(customer.getAddressLine1()!=null?customer.getAddressLine1():"");
					row.createCell(10).setCellValue(customer.getAddressLine2()!=null?customer.getAddressLine2():"");
					row.createCell(11).setCellValue(customer.getCity()!=null?customer.getCity():"");
					row.createCell(12).setCellValue(customer.getBillingAddressId()!=null?customer.getBillingAddressId():0);
					row.createCell(13).setCellValue(customer.getState()!=null?customer.getState():"");
					row.createCell(14).setCellValue(customer.getCountry()!=null?customer.getCountry():"");
					row.createCell(15).setCellValue(customer.getZipCode()!=null?customer.getZipCode():"");
					row.createCell(16).setCellValue(customer.getIsClient() != null ? (customer.getIsClient()==true?"Yes":"No") : "No");
					row.createCell(17).setCellValue(customer.getIsBroker() != null ? (customer.getIsBroker()==true?"Yes":"No") : "No");
					row.createCell(18).setCellValue(customer.getPhone()!=null?customer.getPhone():"");
					row.createCell(19).setCellValue(customer.getReferrerCode()!=null?customer.getReferrerCode():"");
					row.createCell(20).setCellValue(customer.getBrokerId()!=null?customer.getBrokerId().toString():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Customers.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping({"/AddCustomerPopup"})
	public String addCustomerPopup(@ModelAttribute Customer customer, HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		//List<State> stateList = null;
		List<Country> countryList = null;
		try {
			//Integer countryId = 1;
			//stateList = DAORegistry.getQueryManagerDAO().getStatesForCountry(countryId);
			countryList = DAORegistry.getQueryManagerDAO().getCountries();
		} catch (Exception e) {
			e.printStackTrace();
		}
		//map.put("states", stateList);
		map.put("countries", countryList);
		map.put("isPopup", true);
		return "page-add-customer-popup";
	}
	
	/**
	 * Save customer details
	 * @param customer
	 * @param request
	 * @param response
	 * @param map
	 * @param session
	 * @return
	 */
	@RequestMapping({"/AddCustomer"})
	public AddCustomerDTO addCustomer(HttpServletRequest request, HttpServletResponse response){
		AddCustomerDTO addCustomerDTO = new AddCustomerDTO();
		Error error = new Error();
		
		try{
			String action = request.getParameter("action");
			if(action != null && action.equalsIgnoreCase("addCustomer")){

				String brokerIdStr = request.getParameter("brokerId");
				if(StringUtils.isEmpty(brokerIdStr)){
					System.err.println("Please provide BrokerId");
					error.setDescription("Please provide BrokerId");
					addCustomerDTO.setError(error);
					addCustomerDTO.setStatus(0);
					return addCustomerDTO;
				}			
				Integer brokerId = 0;	
				try{
					brokerId = Integer.parseInt(brokerIdStr);
				}catch (Exception e) {
					System.err.println("Please provide valid BrokerId");
					error.setDescription("Please provide valid BrokerId");
					addCustomerDTO.setError(error);
					addCustomerDTO.setStatus(0);
					return addCustomerDTO;
				}
				
				String customerName = request.getParameter("customerName");
				String lastName = request.getParameter("lastName");				
				String email = request.getParameter("email");
				String phone = request.getParameter("phone");
				String extension = request.getParameter("extension");
				String otherPhone = request.getParameter("otherPhone");
				String representativeName = request.getParameter("representativeName");
				String companyName = request.getParameter("companyName");
				String customerType = request.getParameter("customerType");
				String customerLevel = request.getParameter("customerLevel");
				String signupType = request.getParameter("signupType");				
				
				if(StringUtils.isEmpty(customerName)){
					System.err.println("Please provide First Name");
					error.setDescription("Please provide First Name");
					addCustomerDTO.setError(error);
					addCustomerDTO.setStatus(0);
					return addCustomerDTO;
				}else if(StringUtils.isEmpty(lastName)){
					System.err.println("Please provide Last Name");
					error.setDescription("Please provide Last Name");
					addCustomerDTO.setError(error);
					addCustomerDTO.setStatus(0);
					return addCustomerDTO;
				}else if(StringUtils.isEmpty(email)){
					System.err.println("Please provide Email");
					error.setDescription("Please provide Email");
					addCustomerDTO.setError(error);
					addCustomerDTO.setStatus(0);
					return addCustomerDTO;
				}else if(StringUtils.isEmpty(phone)){
					System.err.println("Please provide Phone No");
					error.setDescription("Please provide Phone No");
					addCustomerDTO.setError(error);
					addCustomerDTO.setStatus(0);
					return addCustomerDTO;
				}
				
				//Checking Billing Address
				String blFirstName = request.getParameter("blFirstName");
				String blLastName = request.getParameter("blLastName");
				String blEmail = request.getParameter("blEmail");
				String addressLine1 = request.getParameter("addressLine1");
				String blAddressLine2 = request.getParameter("addressLine2");
				String country = request.getParameter("countryName");
				String state = request.getParameter("stateName");
				String city = request.getParameter("city");
				String zipCode = request.getParameter("zipCode");
				
				if(StringUtils.isEmpty(blFirstName)){
					System.err.println("Please provide Billing Address - First Name");
					error.setDescription("Please provide Billing Address - First Name");
					addCustomerDTO.setError(error);
					addCustomerDTO.setStatus(0);
					return addCustomerDTO;
				}else if(StringUtils.isEmpty(blLastName)){
					System.err.println("Please provide Billing Address - Last Name");
					error.setDescription("Please provide Billing Address - Last Name");
					addCustomerDTO.setError(error);
					addCustomerDTO.setStatus(0);
					return addCustomerDTO;
				}/*else if(StringUtils.isEmpty(blEmail)){
					System.err.println("Please provide Billing Address - Email");
					error.setDescription("Please provide Billing Address - Email");
					addCustomerDTO.setError(error);
					addCustomerDTO.setStatus(0);
					return addCustomerDTO;
				}*/else if(StringUtils.isEmpty(addressLine1)){
					System.err.println("Please provide Billing Address - Street1");
					error.setDescription("Please provide Billing Address - Street1");
					addCustomerDTO.setError(error);
					addCustomerDTO.setStatus(0);
					return addCustomerDTO;
				}else if(StringUtils.isEmpty(zipCode)){
					System.err.println("Please provide Billing Address - ZipCode");
					error.setDescription("Please provide Billing Address - ZipCode");
					addCustomerDTO.setError(error);
					addCustomerDTO.setStatus(0);
					return addCustomerDTO;
				}else if(StringUtils.isEmpty(city)){
					System.err.println("Please provide Billing Address - City");
					error.setDescription("Please provide Billing Address - City");
					addCustomerDTO.setError(error);
					addCustomerDTO.setStatus(0);
					return addCustomerDTO;
				}else if(StringUtils.isEmpty(country)){
					System.err.println("Please provide Billing Address - Country");
					error.setDescription("Please provide Billing Address - Country");
					addCustomerDTO.setError(error);
					addCustomerDTO.setStatus(0);
					return addCustomerDTO;
				}else if(StringUtils.isEmpty(state)){
					System.err.println("Please provide Billing Address - State");
					error.setDescription("Please provide Billing Address - State");
					addCustomerDTO.setError(error);
					addCustomerDTO.setStatus(0);
					return addCustomerDTO;
				}
				
				//Checking Shipping Address
				String shFirstName = request.getParameter("shFirstName");
				String shLastName = request.getParameter("shLastName");
				String shEmail = request.getParameter("shEmail");
				String shstreet1 = request.getParameter("shStreet1");
				String shAddressLine2 = request.getParameter("shStreet2");
				String shCountry = request.getParameter("shCountryName");
				String shState = request.getParameter("shStateName");
				String shCity = request.getParameter("shCity");
				String shZipCode = request.getParameter("shZipCode");
				
				if(StringUtils.isEmpty(shFirstName)){
					System.err.println("Please provide Shipping Address - First Name");
					error.setDescription("Please provide Shipping Address - First Name");
					addCustomerDTO.setError(error);
					addCustomerDTO.setStatus(0);
					return addCustomerDTO;
				}else if(StringUtils.isEmpty(shLastName)){
					System.err.println("Please provide Shipping Address - Last Name");
					error.setDescription("Please provide Shipping Address - Last Name");
					addCustomerDTO.setError(error);
					addCustomerDTO.setStatus(0);
					return addCustomerDTO;
				}/*else if(StringUtils.isEmpty(shEmail)){
					System.err.println("Please provide Shipping Address - Email");
					error.setDescription("Please provide Shipping Address - Email");
					addCustomerDTO.setError(error);
					addCustomerDTO.setStatus(0);
					return addCustomerDTO;
				}*/else if(StringUtils.isEmpty(shstreet1)){
					System.err.println("Please provide Shipping Address - Street1");
					error.setDescription("Please provide Shipping Address - Street1");
					addCustomerDTO.setError(error);
					addCustomerDTO.setStatus(0);
					return addCustomerDTO;
				}else if(StringUtils.isEmpty(shZipCode)){
					System.err.println("Please provide Shipping Address - ZipCode");
					error.setDescription("Please provide Shipping Address - ZipCode");
					addCustomerDTO.setError(error);
					addCustomerDTO.setStatus(0);
					return addCustomerDTO;
				}else if(StringUtils.isEmpty(shCity)){
					System.err.println("Please provide Shipping Address - City");
					error.setDescription("Please provide Shipping Address - City");
					addCustomerDTO.setError(error);
					addCustomerDTO.setStatus(0);
					return addCustomerDTO;
				}else if(StringUtils.isEmpty(shCountry)){
					System.err.println("Please provide Shipping Address - Country");
					error.setDescription("Please provide Shipping Address - Country");
					addCustomerDTO.setError(error);
					addCustomerDTO.setStatus(0);
					return addCustomerDTO;
				}else if(StringUtils.isEmpty(shState)){
					System.err.println("Please provide Shipping Address - State");
					error.setDescription("Please provide Shipping Address - State");
					addCustomerDTO.setError(error);
					addCustomerDTO.setStatus(0);
					return addCustomerDTO;
				}
								
				//Adding customer to database
				Customer customer = new Customer();
				
				Customer isCustomerExists = DAORegistry.getCustomerDAO().checkCustomerExists(email);
				if(isCustomerExists != null){
					System.err.println("There is already an customer with this Username or Email.");
					error.setDescription("There is already an customer with this Username or Email.");
					addCustomerDTO.setError(error);
					addCustomerDTO.setStatus(0);
					return addCustomerDTO;
				}else{
					customer.setCustomerName(customerName);
					customer.setLastName(lastName);
					customer.setEmail(email);
					customer.setPhone(phone);
					customer.setExtension(extension);
					customer.setOtherPhone(otherPhone);
					customer.setRepresentativeName(representativeName);
					customer.setCompanyName(companyName);
					customer.setCustomerType(CustomerType.valueOf(customerType));
					customer.setCustomerLevel(CustomerLevel.valueOf(customerLevel));
					customer.setSignupType(SignupType.valueOf(signupType));
					customer.setIsMailSent(false);
					customer.setReferrerCode(email);
					
					CustomerAddress billingAddress = null;
					if(addressLine1 != null && !addressLine1.isEmpty()){
						billingAddress = new CustomerAddress();
						
						Country countryObj = DAORegistry.getCountryDAO().get(Integer.parseInt(country.trim()));
						State stateObj =  DAORegistry.getStateDAO().get(Integer.parseInt(state.trim()));
						
						billingAddress.setFirstName(blFirstName);
						billingAddress.setLastName(blLastName);
						if(blEmail != null && !blEmail.isEmpty()){
							billingAddress.setEmail(blEmail);
						}
						billingAddress.setAddressLine1(addressLine1);
						billingAddress.setAddressLine2(blAddressLine2);
						billingAddress.setCountry(countryObj);
						billingAddress.setState(stateObj);
						billingAddress.setCity(city);
						billingAddress.setZipCode(zipCode);
						billingAddress.setLastUpdated(new Date());
						billingAddress.setStatus(EventStatus.ACTIVE);
						billingAddress.setAddressType(AddressType.valueOf("BILLING_ADDRESS"));
					}
					
					CustomerAddress shippingAddress = null;						
					String clientBrokerType = request.getParameter("clientBrokerType");
					if(shstreet1 != null && !shstreet1.isEmpty()){
						shippingAddress = new CustomerAddress();
						
						Country countryObj1 = DAORegistry.getCountryDAO().get(Integer.parseInt(shCountry.trim()));
						State stateObj1 =  DAORegistry.getStateDAO().get(Integer.parseInt(shState.trim()));
						
						shippingAddress.setFirstName(shFirstName);
						shippingAddress.setLastName(shLastName);
						if(shEmail != null && !shEmail.isEmpty()){
							shippingAddress.setEmail(shEmail);
						}														
						shippingAddress.setAddressLine1(addressLine1);
						shippingAddress.setAddressLine2(shAddressLine2);
						shippingAddress.setCountry(countryObj1);
						shippingAddress.setState(stateObj1);
						shippingAddress.setCity(shCity);
						shippingAddress.setZipCode(shZipCode);
						shippingAddress.setLastUpdated(new Date());
						shippingAddress.setStatus(EventStatus.ACTIVE);
						shippingAddress.setAddressType(AddressType.valueOf("SHIPPING_ADDRESS"));
					}
					
					if(clientBrokerType!=null){
						if(clientBrokerType.equals("both")){
							customer.setIsBroker(true);
							customer.setIsClient(true);
						}else if(clientBrokerType.equals("client")){
							customer.setIsBroker(false);
							customer.setIsClient(true);
						}else if(clientBrokerType.equals("broker")){
							customer.setIsBroker(true);
							customer.setIsClient(false);
						}
					}
					
					//persist the customer info into database						
					customer.setBrokerId(brokerId);
					customer.setUserName(customer.getEmail());
					customer.setProductType(ProductType.REWARDTHEFAN);
					customer.setApplicationPlatForm(ApplicationPlatform.TICK_TRACKER);
					customer.setSignupDate(new Date());
					
					/*Property property = DAORegistry.getPropertyDAO().get("rtf.customer.referrer.code");
					String preFix = DAORegistry.getPropertyDAO().get("rtf.customer.referral.code.prefix").getValue();
					Long referrerNumber =  Long.valueOf(property.getValue());
					referrerNumber++; */ 
					//customer.setReferrerCode(Util.generateCustomerReferalCode());   
					//property.setValue(String.valueOf(referrerNumber));
					DAORegistry.getCustomerDAO().save(customer);  
					//DAORegistry.getPropertyDAO().saveOrUpdate(property);
					
					//Save Customer Loyalty Information
					CustomerLoyalty customerLoyalty = new CustomerLoyalty();
					customerLoyalty.setCustomerId(customer.getId());
					customerLoyalty.setActivePoints(0.00);
					customerLoyalty.setLatestEarnedPoints(0.00);
					customerLoyalty.setLatestSpentPoints(0.00);
					customerLoyalty.setTotalEarnedPoints(0.00);
					customerLoyalty.setTotalSpentPoints(0.00);
					customerLoyalty.setLastUpdate(new Date());
					customerLoyalty.setPendingPoints(0.00);
					
					DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(customerLoyalty);
					
					if(billingAddress != null){
						billingAddress.setCustomerId(customer.getId());
						DAORegistry.getCustomerAddressDAO().save(billingAddress);
					}
					
					if(shippingAddress != null){
						shippingAddress.setCustomerId(customer.getId());
						DAORegistry.getCustomerAddressDAO().save(shippingAddress);
					}
					addCustomerDTO.setMessage("Customer Saved Successfully");
					addCustomerDTO.setStatus(1);
					
					//Tracking User Action
					String userActionMsg = "Customer Created Successfully.";
					Util.userActionAudit(request, customer.getId(), userActionMsg);
				}			
			}
			
			Integer countryId = 1;
			List<State> stateList = DAORegistry.getQueryManagerDAO().getStatesForCountry(countryId);
			List<Country> countryList = DAORegistry.getQueryManagerDAO().getCountries();
			
			addCustomerDTO.setCountryDTO(com.rtw.tracker.utils.Util.getCountryArray(countryList));
			addCustomerDTO.setStateDTO(com.rtw.tracker.utils.Util.getStateArray(stateList));
			addCustomerDTO.setStatus(1);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return addCustomerDTO;
	}
	
	@RequestMapping(value = "/UpdateCustomerDetails")
	public CustomerInfoDTO updateCustomerDetails(HttpServletRequest request, HttpServletResponse response){
		CustomerInfoDTO customerInfoDTO = new CustomerInfoDTO();
		Error error = new Error();
		//model.addAttribute("customerInfoDTO", customerInfoDTO);
		
		//JSONObject jsonObject = new JSONObject();
		try {
			String customerIdStr = request.getParameter("customerId");
			String action = request.getParameter("action");
			String brokerIdStr = request.getParameter("brokerId");
						
			if(StringUtils.isEmpty(customerIdStr)){
				System.err.println("Please provide CustomerId");
				error.setDescription("Please provide CustomerId");
				customerInfoDTO.setError(error);
				customerInfoDTO.setStatus(0);
				return customerInfoDTO;
			}			
			Integer customerId = 0;	
			try{
				customerId = Integer.parseInt(customerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid CustomerId");
				error.setDescription("Please provide valid CustomerId");
				customerInfoDTO.setError(error);
				customerInfoDTO.setStatus(0);
				return customerInfoDTO;
			}
			
			if(StringUtils.isEmpty(action)){
				System.err.println("Please provide Any Action to Update Customer Info.");
				error.setDescription("Please provide Any Action to Update Customer Info");
				customerInfoDTO.setError(error);
				customerInfoDTO.setStatus(0);
				return customerInfoDTO;
			}

			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide BrokerId");
				error.setDescription("Please provide BrokerId");
				customerInfoDTO.setError(error);
				customerInfoDTO.setStatus(0);
				return customerInfoDTO;
			}			
			Integer brokerId = 0;	
			try{
				brokerId = Integer.parseInt(brokerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid BrokerId");
				error.setDescription("Please provide valid BrokerId");
				customerInfoDTO.setError(error);
				customerInfoDTO.setStatus(0);
				return customerInfoDTO;
			}
				
			Customer customer = DAORegistry.getCustomerDAO().get(customerId);
			if(customer == null){
				System.err.println("Customer data not found");
				error.setDescription("Customer data not found");
				customerInfoDTO.setError(error);
				customerInfoDTO.setStatus(0);
				return customerInfoDTO;
			}
			
			String userActionMsg = "";
			
			if(action.equalsIgnoreCase("customerInfo")){					
				String clientBroker = request.getParameter("clientBrokerType");
				String firstName = request.getParameter("name");
				String lastName = request.getParameter("lastName");
				String email = request.getParameter("email");
				String phone = request.getParameter("phone");
				String companyName = request.getParameter("companyName");
				
				if(StringUtils.isEmpty(firstName)){
					System.err.println("Please provide Customer First Name");
					error.setDescription("Please provide Customer First Name");
					customerInfoDTO.setError(error);
					customerInfoDTO.setStatus(0);
					return customerInfoDTO;
				}else if(StringUtils.isEmpty(lastName)){
					System.err.println("Please provide Customer Last Name");
					error.setDescription("Please provide Customer Last Name");
					customerInfoDTO.setError(error);
					customerInfoDTO.setStatus(0);
					return customerInfoDTO;						
				}else if(StringUtils.isEmpty(email)){
					System.err.println("Please provide Customer Email");
					error.setDescription("Please provide Customer Email");
					customerInfoDTO.setError(error);
					customerInfoDTO.setStatus(0);
					return customerInfoDTO;
				}else if(StringUtils.isEmpty(phone)){
					System.err.println("Please provide Customer Phone No");
					error.setDescription("Please provide Customer Phone No");
					customerInfoDTO.setError(error);
					customerInfoDTO.setStatus(0);
					return customerInfoDTO;
				}else if(!customer.getEmail().equalsIgnoreCase(email)){
					Customer isCustomerExists = DAORegistry.getCustomerDAO().checkCustomerExists(email);
					if(isCustomerExists != null){
						System.err.println("There is already an customer with this Username or Email.");
						error.setDescription("There is already an customer with this Username or Email.");
						customerInfoDTO.setError(error);
						customerInfoDTO.setStatus(0);
						return customerInfoDTO;
					}
				}
				
				if(clientBroker!=null){
					if(clientBroker.equals("both")){
						customer.setIsBroker(true);
						customer.setIsClient(true);
					}else if(clientBroker.equals("client")){
						customer.setIsBroker(false);
						customer.setIsClient(true);
					}else if(clientBroker.equals("broker")){
						customer.setIsBroker(true);
						customer.setIsClient(false);
					}
				}
				customer.setCustomerType(CustomerType.valueOf(request.getParameter("customerType")));
				customer.setCustomerLevel(CustomerLevel.valueOf(request.getParameter("customerLevel")));
				customer.setSignupType(SignupType.valueOf(request.getParameter("signupType")));
				customer.setCustomerName(firstName);
				customer.setCompanyName(companyName);
				customer.setLastName(lastName);
				customer.setEmail(email);
				customer.setReferrerCode(email);
				customer.setUserName(email);
				customer.setPhone(phone);
				customer.setExtension(request.getParameter("extension"));
				customer.setOtherPhone(request.getParameter("otherPhone"));
				customer.setRepresentativeName(request.getParameter("representativeName"));
				
				DAORegistry.getCustomerDAO().saveOrUpdate(customer);
				customerInfoDTO.setMessage("Customer Info Updated Successfully.");
				customerInfoDTO.setStatus(1);
				
				//Tracking User Action Msg
				userActionMsg = "Customer Top-Level Info Updated Successfully.";
				
			}else if(action.equalsIgnoreCase("shippingInfo")){
				String shippingAddressId = request.getParameter("shippingAddrId");					
				String shFirstName = request.getParameter("shFirstName");
				String shLastName = request.getParameter("shLastName");
				String shAddressLine1 = request.getParameter("shAddressLine1");
				String shEmail = request.getParameter("shEmail");
				String shPhone = request.getParameter("shPhone");
				String shZipCode = request.getParameter("shZipCode");
				String shCity = request.getParameter("shCity");					
				String country = request.getParameter("shCountryName");
				String state = request.getParameter("shStateName");
				
				if(StringUtils.isEmpty(shFirstName)){
					System.err.println("Please provide Shipping Address - First Name");
					error.setDescription("Please provide Shipping Address - First Name");
					customerInfoDTO.setError(error);
					customerInfoDTO.setStatus(0);
					return customerInfoDTO;
				}else if(StringUtils.isEmpty(shLastName)){
					System.err.println("Please provide Shipping Address - Last Name");
					error.setDescription("Please provide Shipping Address - Last Name");
					customerInfoDTO.setError(error);
					customerInfoDTO.setStatus(0);
					return customerInfoDTO;
				}else if(StringUtils.isEmpty(shAddressLine1)){
					System.err.println("Please provide Shipping Address - Street1");
					error.setDescription("Please provide Shipping Address - Street1");
					customerInfoDTO.setError(error);
					customerInfoDTO.setStatus(0);
					return customerInfoDTO;
				}else if(StringUtils.isEmpty(shEmail)){
					System.err.println("Please provide Shipping Address - Email");
					error.setDescription("Please provide Shipping Address - Email.");
					customerInfoDTO.setError(error);
					customerInfoDTO.setStatus(0);
					return customerInfoDTO;
				}else if(StringUtils.isEmpty(shPhone)){
					System.err.println("Please provide Shipping Address - Phone");
					error.setDescription("Please provide Shipping Address - Phone");
					customerInfoDTO.setError(error);
					customerInfoDTO.setStatus(0);
					return customerInfoDTO;
				}else if(StringUtils.isEmpty(shZipCode)){
					System.err.println("Please provide Shipping Address - ZipCode");
					error.setDescription("Please provide Shipping Address - ZipCode");
					customerInfoDTO.setError(error);
					customerInfoDTO.setStatus(0);
					return customerInfoDTO;
				}else if(StringUtils.isEmpty(shCity)){
					System.err.println("Please provide Shipping Address - City");
					error.setDescription("Please provide Shipping Address - City");
					customerInfoDTO.setError(error);
					customerInfoDTO.setStatus(0);
					return customerInfoDTO;
				}else if(StringUtils.isEmpty(country)){
					System.err.println("Please provide Shipping Address - Country");
					error.setDescription("Please provide Shipping Address - Country");
					customerInfoDTO.setError(error);
					customerInfoDTO.setStatus(0);
					return customerInfoDTO;
				}else if(StringUtils.isEmpty(state)){
					System.err.println("Please provide Shipping Address - State");
					error.setDescription("Please provide Shipping Address - State");
					customerInfoDTO.setError(error);
					customerInfoDTO.setStatus(0);
					return customerInfoDTO;
				}
				
				Country countryObj = DAORegistry.getCountryDAO().get(Integer.parseInt(country.trim()));
				State stateObj =  DAORegistry.getStateDAO().get(Integer.parseInt(state.trim()));
				CustomerAddress shippingAddress = null;
				if(shippingAddressId!=null && !shippingAddressId.isEmpty()){
					shippingAddress = DAORegistry.getCustomerAddressDAO().get(Integer.parseInt(shippingAddressId));
				}else{
					shippingAddress = new CustomerAddress();
				}
				shippingAddress.setFirstName(shFirstName);
				shippingAddress.setLastName(shLastName);
				shippingAddress.setAddressLine1(shAddressLine1);
				shippingAddress.setAddressLine2(request.getParameter("shAddressLine2"));
				shippingAddress.setCountry(countryObj);
				shippingAddress.setState(stateObj);
				shippingAddress.setEmail(shEmail);
				shippingAddress.setPhone1(shPhone);
				shippingAddress.setCity(shCity);
				shippingAddress.setZipCode(shZipCode);
				shippingAddress.setCustomerId(customerId);
				shippingAddress.setAddressType(AddressType.SHIPPING_ADDRESS);
				shippingAddress.setLastUpdated(new Date());
				shippingAddress.setStatus(EventStatus.ACTIVE);
				
				DAORegistry.getCustomerAddressDAO().saveOrUpdate(shippingAddress);
				customerInfoDTO.setMessage("Shipping Info Updated Successfully.");
				customerInfoDTO.setStatus(1);
				
				//Tracking User Action Msg
				if(shippingAddressId!=null && !shippingAddressId.isEmpty()){
					userActionMsg = "Customer Shipping Info Updated Successfully.";
				}else{
					userActionMsg = "Customer Shipping Info Added Successfully.";
				}
				
				//Getting Shipping Addresses
				int count=0;
				CustomerAddressDTO shippingAddressDTO = null;
				List<CustomerAddressDTO> shippingAddressDTOs = new ArrayList<CustomerAddressDTO>();
				List<CustomerAddress> shippingAddressList = DAORegistry.getCustomerAddressDAO().getShippingAndOtherAddresInfo(customerId);
				if(shippingAddressList!=null && !shippingAddressList.isEmpty()){
					count = DAORegistry.getCustomerAddressDAO().getShippingAddressCount(customerId);
					for(CustomerAddress shippingAddres : shippingAddressList){
						shippingAddressDTO = new CustomerAddressDTO();
						shippingAddressDTO.setId(shippingAddres.getId());
						shippingAddressDTO.setFirstName(shippingAddres.getFirstName());
						shippingAddressDTO.setLastName(shippingAddres.getLastName());
						shippingAddressDTO.setEmail(shippingAddres.getEmail());
						shippingAddressDTO.setPhone(shippingAddres.getPhone1());
						shippingAddressDTO.setStreet1(shippingAddres.getAddressLine1());
						shippingAddressDTO.setStreet2(shippingAddres.getAddressLine2());
						shippingAddressDTO.setState(shippingAddres.getState().getName());
						shippingAddressDTO.setStateId(shippingAddres.getState().getId());
						shippingAddressDTO.setCountry(shippingAddres.getCountry().getName());
						shippingAddressDTO.setCountryId(shippingAddres.getCountry().getId());
						shippingAddressDTO.setCity(shippingAddres.getCity());
						shippingAddressDTO.setZip(shippingAddres.getZipCode());
						shippingAddressDTO.setAddressType(shippingAddres.getAddressType().toString());
						shippingAddressDTOs.add(shippingAddressDTO);
					}
				}
				customerInfoDTO.setShippingAddressDTO(shippingAddressDTOs);
				customerInfoDTO.setShippingPaginationDTO(PaginationUtil.getDummyPaginationParameters(count));
				customerInfoDTO.setShippingCount(count);
				
			}else if(action.equalsIgnoreCase("billingInfo")){					
				String blFirstName = request.getParameter("blFirstName");
				String blLastName = request.getParameter("blLastName");
				String blAddressLine1 = request.getParameter("addressLine1");
				String blEmail = request.getParameter("blEmail");
				String blPhone = request.getParameter("blPhone");
				String blZipCode = request.getParameter("zipCode");
				String blCity = request.getParameter("city");					
				String country = request.getParameter("countryName");
				String state = request.getParameter("stateName");
				
				if(StringUtils.isEmpty(blFirstName)){
					System.err.println("Please provide Billing Address - First Name");
					error.setDescription("Please provide Billing Address - First Name");
					customerInfoDTO.setError(error);
					customerInfoDTO.setStatus(0);
					return customerInfoDTO;
				}else if(StringUtils.isEmpty(blLastName)){
					System.err.println("Please provide Billing Address - Last Name");
					error.setDescription("Please provide Billing Address - Last Name");
					customerInfoDTO.setError(error);
					customerInfoDTO.setStatus(0);
					return customerInfoDTO;
				}else if(StringUtils.isEmpty(blAddressLine1)){
					System.err.println("Please provide Billing Address - Street1");
					error.setDescription("Please provide Billing Address - Street1");
					customerInfoDTO.setError(error);
					customerInfoDTO.setStatus(0);
					return customerInfoDTO;
				}else if(StringUtils.isEmpty(blEmail)){
					System.err.println("Please provide Billing Address - Email");
					error.setDescription("Please provide Billing Address - Email");
					customerInfoDTO.setError(error);
					customerInfoDTO.setStatus(0);
					return customerInfoDTO;
				}else if(StringUtils.isEmpty(blPhone)){
					System.err.println("Please provide Billing Address - Phone No");
					error.setDescription("Please provide Billing Address - Phone No");
					customerInfoDTO.setError(error);
					customerInfoDTO.setStatus(0);
					return customerInfoDTO;
				}else if(StringUtils.isEmpty(blZipCode)){
					System.err.println("Please provide Billing Address - ZipCode");
					error.setDescription("Please provide Billing Address - ZipCode");
					customerInfoDTO.setError(error);
					customerInfoDTO.setStatus(0);
					return customerInfoDTO;
				}else if(StringUtils.isEmpty(blCity)){
					System.err.println("Please provide Billing Address - City");
					error.setDescription("Please provide Billing Address - City");
					customerInfoDTO.setError(error);
					customerInfoDTO.setStatus(0);
					return customerInfoDTO;
				}else if(StringUtils.isEmpty(country)){
					System.err.println("Please provide Billing Address - Country");
					error.setDescription("Please provide Billing Address - Country");
					customerInfoDTO.setError(error);
					customerInfoDTO.setStatus(0);
					return customerInfoDTO;
				}else if(StringUtils.isEmpty(state)){
					System.err.println("Please provide Billing Address - State");
					error.setDescription("Please provide Billing Address - State");
					customerInfoDTO.setError(error);
					customerInfoDTO.setStatus(0);
					return customerInfoDTO;
				}
				
				CustomerAddress billingAddress = DAORegistry.getCustomerAddressDAO().getCustomerAddressInfo(customerId);
				if(billingAddress==null){
					billingAddress = new CustomerAddress();
				}
			
				Country countryObj = DAORegistry.getCountryDAO().get(Integer.parseInt(country.trim()));
				State stateObj =  DAORegistry.getStateDAO().get(Integer.parseInt(state.trim()));
				billingAddress.setFirstName(blFirstName);
				billingAddress.setLastName(blLastName);
				billingAddress.setEmail(blEmail);
				billingAddress.setPhone1(blPhone);
				billingAddress.setAddressLine1(blAddressLine1);
				billingAddress.setAddressLine2(request.getParameter("addressLine2"));
				billingAddress.setCountry(countryObj);
				billingAddress.setState(stateObj);
				billingAddress.setCity(blCity);
				billingAddress.setZipCode(blZipCode);
				billingAddress.setAddressType(AddressType.BILLING_ADDRESS);
				billingAddress.setLastUpdated(new Date());
				billingAddress.setCustomerId(customerId);
				billingAddress.setStatus(EventStatus.ACTIVE);
				
				DAORegistry.getCustomerAddressDAO().saveOrUpdate(billingAddress);
				customerInfoDTO.setMessage("Billing Info Updated Successfully.");
				customerInfoDTO.setStatus(1);
				
				//Tracking User Action Msg
				userActionMsg = "Customer Billing Info Updated Successfully.";				
				
				CustomerAddressDTO billingAddressDTO = new CustomerAddressDTO();				
				if(billingAddress!=null){
					billingAddressDTO.setId(billingAddress.getId());
					billingAddressDTO.setFirstName(billingAddress.getFirstName());
					billingAddressDTO.setLastName(billingAddress.getLastName());
					billingAddressDTO.setEmail(billingAddress.getEmail());
					billingAddressDTO.setPhone(billingAddress.getPhone1());
					billingAddressDTO.setStreet1(billingAddress.getAddressLine1());
					billingAddressDTO.setStreet2(billingAddress.getAddressLine2());
					billingAddressDTO.setState(billingAddress.getState().getName());
					billingAddressDTO.setStateId(billingAddress.getState().getId());
					billingAddressDTO.setCountry(billingAddress.getCountry().getName());
					billingAddressDTO.setCountryId(billingAddress.getCountry().getId());
					billingAddressDTO.setCity(billingAddress.getCity());
					billingAddressDTO.setZip(billingAddress.getZipCode());						
				}				
				customerInfoDTO.setBillingAddressDTO(billingAddressDTO);
				
			}else if(action.equalsIgnoreCase("notes")){					
				String notes = request.getParameter("notes");
				if(StringUtils.isEmpty(notes)){						
					System.err.println("No data to Save in Notes");
					error.setDescription("No data to Save in Notes");
					customerInfoDTO.setError(error);
					customerInfoDTO.setStatus(0);
					return customerInfoDTO;
				}
				
				DAORegistry.getCustomerDAO().updateNotes(notes, customerId);
				customerInfoDTO.setMessage("Notes Updated Successfully.");
				customerInfoDTO.setStatus(1);
				
				//Tracking User Action Msg
				userActionMsg = "Customer Notes Updated Successfully.";
			
			}else if(action.equalsIgnoreCase("loyalFan")){
				String artistStr = request.getParameter("artistSelection");
				String loyalFanSelectionStr = request.getParameter("loyalFanSelection");
				String cityStr = request.getParameter("city");
				String stateStr = request.getParameter("state");
				String countryStr = request.getParameter("country");
				String zipCodeStr = request.getParameter("zipCode");
				String categoryName = request.getParameter("categoryName");
				
			    /*Map<String, String> reqMap = Util.getParameterMap(request);
			    if(categoryName != null && categoryName.equalsIgnoreCase("SPORTS")){
			    	reqMap.put("loyalFanSelection", artistStr);
			    }else{
				    reqMap.put("loyalFanSelection", loyalFanSelectionStr);
				    reqMap.put("state", stateStr);
				    reqMap.put("country", countryStr);
				    reqMap.put("zipCode", zipCodeStr);
			    }
			    reqMap.put("loyalFanType", categoryName);
			    reqMap.put("customerId", customerID);
			    String reqdata = Util.getObject(reqMap,sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.UPDATE_LOYALFAN);
			    Gson reqgson = new Gson();  
				JsonObject jsoObject = reqgson.fromJson(reqdata, JsonObject.class);
				LoyalFanStatus loyalFanStatus = reqgson.fromJson(((JsonObject)jsoObject.get("loyalFanStatus")), LoyalFanStatus.class);
				if(loyalFanStatus.getStatus() == 1){
					jsonObject.put("Message", loyalFanStatus.getMessage());
				}else{
					jsonObject.put("Message", loyalFanStatus.getMessage());
				}*/
				
				/*Map<String, String> map = Util.getParameterMap(request);
			    map.put("customerId", customerID);
			    String data = Util.getObject(map,sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.GET_LOYALFAN_STATUS);
			    Gson gson = new Gson();  
				JsonObject jsnObject = gson.fromJson(data, JsonObject.class);
				LoyalFanStatus loyalFans = gson.fromJson(((JsonObject)jsnObject.get("loyalFanStatus")), LoyalFanStatus.class);*/
				
				Date today = new Date();
				Date afterYear = new Date();
				afterYear.setYear(today.getYear()+1);
				CustomerLoyalFan loyalFan = null;
				CustomerLoyalFanAudit loyalFanAudit = null;
				String userName = request.getParameter("userName");					
				Artist artist = null;
				String msg = "";
				
				if(artistStr != null && !artistStr.isEmpty()){
					artist = DAORegistry.getArtistDAO().get(Integer.parseInt(artistStr));
				}
				if(artist != null || (loyalFanSelectionStr != null && !loyalFanSelectionStr.isEmpty())){							
					loyalFan = DAORegistry.getCustomerLoyalFanDAO().getActiveLoyalFanByCustomerId(customerId);
				}									
				if(loyalFan != null){
					loyalFan.setStatus("DELETED");
					loyalFan.setUpdatedDate(today);
					DAORegistry.getCustomerLoyalFanDAO().update(loyalFan);
					
					//LoyalFanAudit
					loyalFanAudit = new CustomerLoyalFanAudit();
					loyalFanAudit.setCustomerLoyalFanId(loyalFan.getId());
					loyalFanAudit.setCustomerId(customerId);
					loyalFanAudit.setStatus("DELETED");
					loyalFanAudit.setCreatedBy(userName);
					loyalFanAudit.setCreatedDate(today);
					DAORegistry.getCustomerLoyalFanAuditDAO().save(loyalFanAudit);
				}
				
				loyalFan = new CustomerLoyalFan();
				if(categoryName.equalsIgnoreCase("SPORTS")){
					loyalFan.setArtistId(artist.getId());
					loyalFan.setLoyalName(artist.getName());
					msg = artist.getName();
				}else{
					loyalFan.setArtistId(-1);
					loyalFan.setLoyalName(loyalFanSelectionStr);
					loyalFan.setCountry(countryStr);
					loyalFan.setState(stateStr);
					loyalFan.setZipCode(zipCodeStr);
					msg = loyalFanSelectionStr+", "+countryStr;
				}
				loyalFan.setLoyalFanType(categoryName);
				loyalFan.setTicketPurchased(false);
				loyalFan.setStartDate(today);
				loyalFan.setEndDate(afterYear);
				loyalFan.setCreatedDate(today);
				loyalFan.setUpdatedDate(today);
				loyalFan.setCustomerId(customerId);
				loyalFan.setStatus("ACTIVE");
				DAORegistry.getCustomerLoyalFanDAO().save(loyalFan);
				
				//LoyalFanAudit
				loyalFanAudit = new CustomerLoyalFanAudit();
				loyalFanAudit.setCustomerLoyalFanId(loyalFan.getId());
				loyalFanAudit.setCustomerId(customerId);
				loyalFanAudit.setStatus("ACTIVE");
				loyalFanAudit.setCreatedBy(userName);
				loyalFanAudit.setCreatedDate(today);
				DAORegistry.getCustomerLoyalFanAuditDAO().save(loyalFanAudit);
				
				customerInfoDTO.setMessage(msg+" saved as Loyal Fan.");
				customerInfoDTO.setStatus(1);
				
				//Tracking User Action Msg
				userActionMsg = msg+" Saved as Loyal Fan.";
				
				CustomerLoyalFanDTO customerLoyalFanDTO = new CustomerLoyalFanDTO();
				CustomerLoyalFan loyalFans = DAORegistry.getCustomerLoyalFanDAO().getActiveLoyalFanByCustomerId(customerId);
				if(loyalFans != null){
					if(loyalFans.getLoyalFanType() != null && loyalFans.getLoyalFanType().equalsIgnoreCase("SPORTS")){
						customerLoyalFanDTO.setArtistName(loyalFans.getLoyalName());
					}else{
						customerLoyalFanDTO.setCityName(loyalFans.getLoyalName()!=null?loyalFans.getLoyalName()+", "+loyalFans.getCountry():"");
					}
					customerLoyalFanDTO.setCategoryName(loyalFans.getLoyalFanType()!=null?loyalFans.getLoyalFanType():"");
					if(loyalFans.getTicketPurchased() != null && loyalFans.getTicketPurchased()){
						customerLoyalFanDTO.setTicketsPurchased("Yes");
					}else{
						customerLoyalFanDTO.setTicketsPurchased("No");
					}
					customerLoyalFanDTO.setStartDate(loyalFans.getStartDateStr()!=null?loyalFans.getStartDateStr():"");
					customerLoyalFanDTO.setEndDate(loyalFans.getEndDateStr()!=null?loyalFans.getEndDateStr():"");
				}										
				customerInfoDTO.setLoyalFanDTO(customerLoyalFanDTO);
				
			}else if(action.equalsIgnoreCase("favouriteEvent")){
				String eventIdStr = request.getParameter("eventIds");
				
				if(StringUtils.isEmpty(eventIdStr)){						
					System.err.println("Please select Event");
					error.setDescription("Please select Event");
					customerInfoDTO.setError(error);
					customerInfoDTO.setStatus(0);
					return customerInfoDTO;
				}
				Integer eventId = 0;	
				try{
					eventId = Integer.parseInt(eventIdStr);
				}catch (Exception e) {
					System.err.println("Please provide valid Event");
					error.setDescription("Please provide valid Event");
					customerInfoDTO.setError(error);
					customerInfoDTO.setStatus(0);
					return customerInfoDTO;
				}
									
				/*String[] eventIdsArr = null;
				if(eventIdStr != null) {
					eventIdsArr = eventIdStr.split(",");
				}
				Set<Integer> eventIdSet = new HashSet<Integer>();
				
				for (String eventIds : eventIdsArr) {
					if(eventIds != null && !eventIds.isEmpty()){
						Integer eventId = Integer.parseInt(eventIds);
						if(eventIdSet.add(eventId)){
							favEvents.setEventId(eventId);
							favEvents.setCustomerId(customerId);
							favEvents.setInsertDate(today);
							favEvents.setUpdateDate(today);
							favEvents.setStatus("ACTIVE");
							DAORegistry.getFavouriteEventsDAO().save(favEvents);
						}
					}
				}*/
				
				Date today = new Date();
				FavouriteEvents favEvents = new FavouriteEvents();					
				favEvents.setEventId(eventId);
				favEvents.setCustomerId(customerId);
				favEvents.setInsertDate(today);
				favEvents.setUpdateDate(today);
				favEvents.setStatus("ACTIVE");
				DAORegistry.getFavouriteEventsDAO().save(favEvents);
				
				customerInfoDTO.setMessage("Favouite Events Added.");
				customerInfoDTO.setStatus(1);
				
				//Tracking User Action Msg
				userActionMsg = "Favouite Event(s) Added. Event Id - "+eventIdStr;
								
			}
			
			GridHeaderFilters filter = new GridHeaderFilters();
			Collection<Customers> customerList = DAORegistry.getQueryManagerDAO().getCustomers(customerId,null,null,brokerId,filter,null);
			customerInfoDTO.setCustomersDTO(com.rtw.tracker.utils.Util.getCustomerArray(customerList));
			
			//Tracking User Action
			Util.userActionAudit(request, customerId, userActionMsg);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return customerInfoDTO;
	} 
	
	@RequestMapping(value = "/ViewCustomer")
	public String viewInvoiceCustomer(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		String customerID= request.getParameter("custId");
		//List<State> stateList = null;
		List<Country> countryList = null;
		try {
			Integer countryId = 1;
			//stateList = DAORegistry.getQueryManagerDAO().getStatesForCountry(countryId);
			countryList = DAORegistry.getQueryManagerDAO().getCountries();
		} catch (Exception e) {
			e.printStackTrace();
		}
		//map.put("states", stateList);
		map.put("countries", countryList);
		map.put("customerId", customerID);
		return "page-view-customer";
	}
	
	/**
	 * Update customer and billing address info
	 * @param customer
	 * @param request
	 * @param response
	 * @return
	 */
	/*@RequestMapping(value = "/UpdateCustomer")
	public String updateCustomer(@ModelAttribute Customer customer, HttpServletRequest request, HttpServletResponse response, ModelMap map){
		String returnMessage = "";
		String customerID= request.getParameter("custId");
		String billingAddressId = request.getParameter("billingAddressId");
		String action = request.getParameter("action");
		List<State> stateList = null;
		List<Country> countryList = null;
		int shippingAddressCount = 0;
		try{
			Integer countryId = 1;
			stateList = DAORegistry.getQueryManagerDAO().getStatesForCountry(countryId);
			countryList = DAORegistry.getQueryManagerDAO().getCountries();
			Integer customerId = Integer.parseInt(customerID);
			CustomerAddress billingAddress = DAORegistry.getCustomerAddressDAO().getCustomerAddressInfo(customerId);
			if(action != null && action.equals("saveCustomerInfo")){
				if(customer.getCustomerName() == null || customer.getCustomerName().isEmpty()){
					returnMessage = "Customer name can't be blank";
				}else if(customer.getEmail() == null || customer.getEmail().isEmpty()){
					returnMessage = "Email can't be blank";
				}
				else if(customer.getPhone() == null || customer.getPhone().isEmpty()){
					returnMessage = "Phone no can't be blank";
				}
				if(returnMessage.isEmpty()){
				
						Customer customers = DAORegistry.getCustomerDAO().get(Integer.parseInt(customerID));
						customer.setId(customers.getId());
						customer.setUserName(customers.getEmail());
						customer.setProductType(ProductType.REWARDTHEFAN);
						customer.setApplicationPlatForm(ApplicationPlatform.TICK_TRACKER);
						customer.setSignupDate(new Date());
						DAORegistry.getCustomerDAO().update(customer);
				}
			}else if(action != null && action.equals("billingAddressInfo")){
				Integer billingId = Integer.parseInt(billingAddressId.trim());
				CustomerAddress billingAddresss = DAORegistry.getCustomerAddressDAO().get(billingId);
			
				String addressLine1 = request.getParameter("addressLine1");
				//CustomerAddress billingAddressInfo = null;
				if(addressLine1 != null && !addressLine1.isEmpty()){
					LOGGER.info("Saving the billing address ..");
					
					//billingAddressInfo = new CustomerAddress();
					String firstName = request.getParameter("firstName");
					System.out.println("firstname ::"+firstName);
					String lastName = request.getParameter("lastName");
					String addressLine2 = request.getParameter("addressLine2");
					String country = request.getParameter("countryName");
					String state = request.getParameter("stateName");
					String city = request.getParameter("city");
					String zipCode = request.getParameter("zipCode");
					
					Country countryObj = DAORegistry.getCountryDAO().get(Integer.parseInt(country.trim()));
					State stateObj =  DAORegistry.getStateDAO().get(Integer.parseInt(state.trim()));

					billingAddress.setFirstName(firstName);
					billingAddress.setId(billingAddresss.getId());
					billingAddress.setLastName(lastName);
					billingAddress.setAddressLine1(addressLine1);
					billingAddress.setAddressLine2(addressLine2);
					billingAddress.setCountry(countryObj);
					billingAddress.setState(stateObj);
					billingAddress.setCity(city);
					billingAddress.setZipCode(zipCode);
					billingAddress.setAddressType(AddressType.valueOf("BILLING_ADDRESS"));
				}
				if(billingAddress != null){
					billingAddress.setCustomerId(customerId);
					DAORegistry.getCustomerAddressDAO().update(billingAddress);
					LOGGER.info("Saved the billing address for " +customerId);
				}
			}else if(action != null && action.equals("saveNotes")){
				String notes=request.getParameter("notes");
				DAORegistry.getCustomerDAO().updateNotes(notes,customerId);
				
			}
			
			List<CustomerAddress> shippingAddressList = DAORegistry.getCustomerAddressDAO().getShippingAddressInfo(customerId);
			shippingAddressCount = DAORegistry.getCustomerAddressDAO().getShippingAddressCount(customerId);
			Collection<CustomerInvoices> custInvoiceList = DAORegistry.getQueryManagerDAO().getInvoicesForCustomer(customerId);
			Collection<PurchaseOrders> custPOList = DAORegistry.getQueryManagerDAO().getPOForCustomer(customerId);
			Customer customerInfo = DAORegistry.getCustomerDAO().get(customerId);
			map.put("customer", customerInfo);
			map.put("billingAddress", billingAddress);
			map.put("shippingAddressList", shippingAddressList);
			map.put("customerInvoiceList", custInvoiceList);
			map.put("custPOList", custPOList);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		map.put("customerId", customerID);
		map.put("count", shippingAddressCount);
		map.put("states", stateList);
		map.put("countries", countryList);
		return "page-view-customer";
	}*/
	
	/**
	 * Update customer shipping address info
	 * @param customer
	 * @param request
	 * @param response
	 * @param map
	 * @param session
	 * @return
	 */
	/*@RequestMapping(value = "/EditCustomer")
	public String editCustomer(@ModelAttribute Customer customer, HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		String customerID= request.getParameter("custId");
		String action = request.getParameter("action");
		String returnMessage = "";
		List<State> stateList = null;
		List<Country> countryList = null;
		try{
			Integer countryId = 1;
			stateList = DAORegistry.getQueryManagerDAO().getStatesForCountry(countryId);
			countryList = DAORegistry.getQueryManagerDAO().getCountries();
			if(customerID == null || customerID.isEmpty()){
				return "redirect:ManageDetails";
			}
			
			Integer customerId = Integer.parseInt(customerID);
			Customer customerInfo = DAORegistry.getCustomerDAO().get(customerId);
			CustomerAddress billingAddress = DAORegistry.getCustomerAddressDAO().getCustomerAddressInfo(customerId);
			List<CustomerAddress> shippingAddressList = DAORegistry.getCustomerAddressDAO().getShippingAddressInfo(customerId);
			
			if(action != null && action.equals("action")){
				if(customer.getCustomerName() == null || customer.getCustomerName().isEmpty()){
					returnMessage = "Customer name can't be blank";
				}else if(customer.getEmail() == null || customer.getEmail().isEmpty()){
					returnMessage = "Email can't be blank";
				}
				else if(customer.getPhone() == null || customer.getPhone().isEmpty()){
					returnMessage = "Phone no can't be blank";
				}
				if(returnMessage.isEmpty()){
					LOGGER.info("Edit customer info "+ customerID);
					customer.setCustomerType(customerInfo.getCustomerType());
					customer.setCustomerLevel(customerInfo.getCustomerLevel());
					customer.setSignupType(customerInfo.getSignupType());
					customer.setCustomerName(customerInfo.getCustomerName());
					customer.setEmail(customerInfo.getEmail());
					customer.setPhone(customerInfo.getPhone());
					customer.setExtension(customerInfo.getExtension());
					customer.setOtherPhone(customerInfo.getOtherPhone());
					customer.setRepresentativeName(customerInfo.getRepresentativeName());
					
					customer.setProductType(ProductType.REWARDTHEFAN);
					customer.setApplicationPlatForm(ApplicationPlatform.TICK_TRACKER);
					customer.setSignupDate(new Date());
					DAORegistry.getCustomerDAO().update(customer);
					
					map.put("successMessage", "Customer details updated successfully.");
				}else{
					map.put("errorMessage", returnMessage);
				}
				
			}
			map.put("customer", customerInfo);
			map.put("billingAddress", billingAddress);
			map.put("shippingAddressList", shippingAddressList);
		}catch (Exception e) {
			e.printStackTrace();
			map.put("errorMessage", "There is something wrong..Please Try Again.");
		}
		
		map.put("customerId", customerID);
		map.put("states", stateList);
		map.put("countries", countryList);
		return "page-edit-customer";
	}*/
	
	/**
	 * Save shipping address
	 * @param addShippingAddress
	 * @param request
	 * @param response
	 * @param map
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/AddShippingAddress", method=RequestMethod.POST)
	public String saveShippingAddress(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		LOGGER.info("Save shipping address..");
		String returnMessage = "";
		String customerId = "";
		String shippingAddressId = "";
		try{
			customerId = request.getParameter("custId");
			shippingAddressId = request.getParameter("shippingAddrId");
			String firstName = request.getParameter("shFirstName");
			String lastName = request.getParameter("shLastName");
			String street1 = request.getParameter("shAddressLine1");
			String street2 = request.getParameter("shAddressLine2");
			String country = request.getParameter("shCountryName");
			String state = request.getParameter("shStateName");
			String city = request.getParameter("shCity");
			String zipCode = request.getParameter("shZipCode");
			String action = request.getParameter("action");
			if(action != null && action.equals("addShippingAddress")){
				/*if(addShippingAddress.getFirstName() == null || addShippingAddress.getFirstName().isEmpty()){
					returnMessage = "Firstname can't be blank";
				}*/
				CustomerAddress shippingAddress = new CustomerAddress();
				if(returnMessage.isEmpty()){

					Country countryObj1 = DAORegistry.getCountryDAO().get(Integer.parseInt(country.trim()));
					State stateObj1 =  DAORegistry.getStateDAO().get(Integer.parseInt(state.trim()));
					
					shippingAddress.setFirstName(firstName);
					shippingAddress.setLastName(lastName);
					shippingAddress.setAddressLine1(street1);
					shippingAddress.setAddressLine2(street2);
					shippingAddress.setCountry(countryObj1);
					shippingAddress.setState(stateObj1);
					shippingAddress.setCity(city);
					shippingAddress.setZipCode(zipCode);
					shippingAddress.setCustomerId(Integer.parseInt(customerId));
					shippingAddress.setAddressType(AddressType.SHIPPING_ADDRESS);
					
					CustomerAddress updateAddress = null;
					if(shippingAddressId != null && !shippingAddressId.isEmpty()){
						updateAddress = DAORegistry.getCustomerAddressDAO().get(Integer.parseInt(shippingAddressId));
					}
					
					if(updateAddress != null){
						shippingAddress.setId(updateAddress.getId());
						shippingAddress.setFirstName(firstName);
						shippingAddress.setLastName(lastName);
						shippingAddress.setAddressLine1(street1);
						shippingAddress.setAddressLine2(street2);
						shippingAddress.setCountry(countryObj1);
						shippingAddress.setState(stateObj1);
						shippingAddress.setCity(city);
						shippingAddress.setZipCode(zipCode);
						shippingAddress.setCustomerId(Integer.parseInt(customerId));
						shippingAddress.setAddressType(AddressType.SHIPPING_ADDRESS);
						DAORegistry.getCustomerAddressDAO().update(shippingAddress);
					}else{						
						DAORegistry.getCustomerAddressDAO().save(shippingAddress);
					}
					returnMessage = "Shipping address added successfully";
					map.put("successMessage", returnMessage);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			map.put("errorMessage", "There is something wrong..Please Try Again.");
		}
		return "redirect:EditCustomer?custId="+customerId;
	}
	
	
	@RequestMapping({"/GetCustomerInfo"})
	public CustomerInfoDTO getCustomerInfo(HttpServletRequest request, HttpServletResponse response){
		CustomerInfoDTO customerInfoDTO = new CustomerInfoDTO();
		Error error = new Error();
		//model.addAttribute("customerInfoDTO", customerInfoDTO);
		
		//JSONObject returnObject = new JSONObject();
		try{
			String customerIdStr = request.getParameter("customerId");
			
			if(StringUtils.isEmpty(customerIdStr)){
				System.err.println("Please provide CustomerId");
				error.setDescription("Please provide CustomerId");
				customerInfoDTO.setError(error);
				customerInfoDTO.setStatus(0);
				return customerInfoDTO;
			}			
			Integer customerId = 0;	
			try{
				customerId = Integer.parseInt(customerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid CustomerId");
				error.setDescription("Please provide valid CustomerId");
				customerInfoDTO.setError(error);
				customerInfoDTO.setStatus(0);
				return customerInfoDTO;
			}			
			
			Customer customerInfo = DAORegistry.getCustomerDAO().get(customerId);
			CustomerAddress billingAddress = DAORegistry.getCustomerAddressDAO().getCustomerAddressInfo(customerId);
			List<CustomerAddress> shippingAddressList = DAORegistry.getCustomerAddressDAO().getShippingAndOtherAddresInfo(customerId);
			String phoneStr = "";
			String email = "";
			
			phoneStr = customerInfo.getPhone();
			if(phoneStr == null || phoneStr.isEmpty()){
				if(billingAddress != null && billingAddress.getPhone1() != null && !billingAddress.getPhone1().isEmpty()){
					phoneStr = billingAddress.getPhone1();
				}else{
					if(shippingAddressList != null && shippingAddressList.size() > 0){
						for(CustomerAddress shipAddress : shippingAddressList){
							if(shipAddress.getPhone1() != null && !shipAddress.getPhone1().isEmpty()){
								phoneStr = shipAddress.getPhone1();
								break;
							}
						}
					}
				}
			}
			email = customerInfo.getEmail();
			if(email == null || email.isEmpty()){
				if(billingAddress.getEmail() != null && !billingAddress.getEmail().isEmpty()){
					email = billingAddress.getEmail();
				}else{
					if(shippingAddressList != null && shippingAddressList.size() > 0){
						for(CustomerAddress shipAddress : shippingAddressList){
							if(shipAddress.getEmail() != null && !shipAddress.getEmail().isEmpty()){
								email = shipAddress.getEmail();
								break;
							}
						}
					}
				}
			}
			/*JSONObject custInfoObj = new JSONObject();
			custInfoObj.put("id",customerInfo.getId());
			custInfoObj.put("type",customerInfo.getCustomerType());
			custInfoObj.put("level",customerInfo.getCustomerLevel());
			custInfoObj.put("signupType",customerInfo.getSignupType());
			custInfoObj.put("name",customerInfo.getCustomerName());
			custInfoObj.put("lastName",customerInfo.getLastName());
			custInfoObj.put("email",email);
			custInfoObj.put("phone",phoneStr);
			custInfoObj.put("ext",customerInfo.getExtension());
			custInfoObj.put("otherPhone",customerInfo.getOtherPhone());
			custInfoObj.put("productType",customerInfo.getProductType().toString());
			custInfoObj.put("representative",customerInfo.getRepresentativeName());
			custInfoObj.put("notes",customerInfo.getNotes());
			if(customerInfo.getIsClient() && customerInfo.getIsBroker()){
				custInfoObj.put("clientBrokerType","both");
			}else if(customerInfo.getIsClient()){
				custInfoObj.put("clientBrokerType","client");
			}else if(customerInfo.getIsBroker()){
				custInfoObj.put("clientBrokerType","broker");
			}
			returnObject.put("customerInfo", custInfoObj);*/
			
			CustomerTopLevelInfoDTO customerTopLevelInfoDTO = new CustomerTopLevelInfoDTO();
			customerTopLevelInfoDTO.setId(customerInfo.getId());
			customerTopLevelInfoDTO.setType(customerInfo.getCustomerType()!=null?customerInfo.getCustomerType().toString():"");
			customerTopLevelInfoDTO.setLevel(customerInfo.getCustomerLevel()!=null?customerInfo.getCustomerLevel().toString():"");
			customerTopLevelInfoDTO.setProductType(customerInfo.getProductType().toString());
			customerTopLevelInfoDTO.setSignupType(customerInfo.getSignupType()!=null?customerInfo.getSignupType().toString():"");				
			customerTopLevelInfoDTO.setName(customerInfo.getCustomerName());
			customerTopLevelInfoDTO.setLastName(customerInfo.getLastName());
			customerTopLevelInfoDTO.setEmail(email);
			customerTopLevelInfoDTO.setPhone(phoneStr);				
			customerTopLevelInfoDTO.setExt(customerInfo.getExtension());				
			customerTopLevelInfoDTO.setOtherPhone(customerInfo.getOtherPhone());
			customerTopLevelInfoDTO.setRepresentative(customerInfo.getRepresentativeName());
			customerTopLevelInfoDTO.setNotes(customerInfo.getNotes());
			customerTopLevelInfoDTO.setCompanyName(customerInfo.getCompanyName());
			if(customerInfo.getIsClient() != null && customerInfo.getIsBroker() != null && customerInfo.getIsClient() && customerInfo.getIsBroker()){
				customerTopLevelInfoDTO.setClientBrokerType("both");
			}else if(customerInfo.getIsClient() != null && customerInfo.getIsClient()){
				customerTopLevelInfoDTO.setClientBrokerType("client");
			}else if(customerInfo.getIsBroker() != null && customerInfo.getIsBroker()){
				customerTopLevelInfoDTO.setClientBrokerType("broker");
			}				
			customerInfoDTO.setCustomerTopLevelInfoDTO(customerTopLevelInfoDTO);
							
			/*JSONObject countryObj = null;
			JSONArray countryArr = new JSONArray();
			List<Country> countryList = DAORegistry.getCountryDAO().getAllCountry();
			for(Country country : countryList){
				countryObj = new JSONObject();
				countryObj.put("id", country.getId());
				countryObj.put("name", country.getName());
				countryArr.put(countryObj);
			}
			returnObject.put("countries", countryArr);*/
			
			List<Country> countryList = DAORegistry.getCountryDAO().getAllCountry();
			customerInfoDTO.setCountryDTO(com.rtw.tracker.utils.Util.getCountryArray(countryList));
			
			//CustomerAddress billingAddress = DAORegistry.getCustomerAddressDAO().getCustomerAddressInfo(customerId);
			/*JSONObject billingInfoObj = new JSONObject();
			if(billingAddress!=null){
				billingInfoObj.put("id",billingAddress.getId());
				billingInfoObj.put("firstName",billingAddress.getFirstName());
				billingInfoObj.put("lastName",billingAddress.getLastName());
				billingInfoObj.put("email",billingAddress.getEmail());
				billingInfoObj.put("phone",billingAddress.getPhone1());
				billingInfoObj.put("street1",billingAddress.getAddressLine1());
				billingInfoObj.put("street2",billingAddress.getAddressLine2());
				billingInfoObj.put("state", billingAddress.getState().getName());
				billingInfoObj.put("stateId", billingAddress.getState().getId());
				billingInfoObj.put("country",billingAddress.getCountry().getName());
				billingInfoObj.put("countryId", billingAddress.getCountry().getId());
				billingInfoObj.put("city",billingAddress.getCity());
				billingInfoObj.put("zip",billingAddress.getZipCode());
				if(billingAddress.getCountry()!=null){						
					JSONArray stateArr = new JSONArray();
					JSONObject stateObj = null;
					List<State> stateList = DAORegistry.getStateDAO().getAllStateByCountryId(billingAddress.getCountry().getId());
					for(State state : stateList){
						stateObj = new JSONObject();
						stateObj.put("id", state.getId());
						stateObj.put("name", state.getName());
						stateArr.put(stateObj);
					}
					returnObject.put("states", stateArr);
				}
			}
			returnObject.put("billingInfo", billingInfoObj);*/
			
			CustomerAddressDTO billingAddressDTO = new CustomerAddressDTO();				
			if(billingAddress!=null){
				billingAddressDTO.setId(billingAddress.getId());
				billingAddressDTO.setFirstName(billingAddress.getFirstName());
				billingAddressDTO.setLastName(billingAddress.getLastName());
				billingAddressDTO.setEmail(billingAddress.getEmail());
				billingAddressDTO.setPhone(billingAddress.getPhone1());
				billingAddressDTO.setStreet1(billingAddress.getAddressLine1());
				billingAddressDTO.setStreet2(billingAddress.getAddressLine2());
				billingAddressDTO.setState(billingAddress.getState().getName());
				billingAddressDTO.setStateId(billingAddress.getState().getId());
				billingAddressDTO.setCountry(billingAddress.getCountry().getName());
				billingAddressDTO.setCountryId(billingAddress.getCountry().getId());
				billingAddressDTO.setCity(billingAddress.getCity());
				billingAddressDTO.setZip(billingAddress.getZipCode());
				if(billingAddress.getCountry()!=null){
					List<State> stateList = DAORegistry.getStateDAO().getAllStateByCountryId(billingAddress.getCountry().getId());
					customerInfoDTO.setStateDTO(com.rtw.tracker.utils.Util.getStateArray(stateList));
				}
			}				
			customerInfoDTO.setBillingAddressDTO(billingAddressDTO);
			
			//List<CustomerAddress> shippingAddressList = DAORegistry.getCustomerAddressDAO().getShippingAndOtherAddresInfo(customerId);
			/*JSONArray shippingArray = new JSONArray();
			int count=0;
			if(shippingAddressList!=null && !shippingAddressList.isEmpty()){
				count = DAORegistry.getCustomerAddressDAO().getShippingAddressCount(customerId);
				for(CustomerAddress address : shippingAddressList){
					JSONObject object = new JSONObject();
					object.put("id",address.getId());
					object.put("firstName",address.getFirstName());
					object.put("lastName",address.getLastName());
					object.put("email",address.getEmail());
					object.put("phone",address.getPhone1());
					object.put("street1",address.getAddressLine1());
					object.put("street2",address.getAddressLine2());
					object.put("state",address.getState().getName());
					object.put("stateId",address.getState().getId());
					object.put("country",address.getCountry().getName());
					object.put("countryId",address.getCountry().getId());
					object.put("city",address.getCity());
					object.put("zip",address.getZipCode());
					object.put("addressType",address.getAddressType().toString());
					shippingArray.put(object);
				}
			}
			returnObject.put("shippingInfo", shippingArray);
			returnObject.put("shippingPagingInfo", PaginationUtil.getDummyPaginationParameter(count));
			returnObject.put("shippingCount", count);*/
			
			int count=0;
			CustomerAddressDTO shippingAddressDTO = null;
			List<CustomerAddressDTO> shippingAddressDTOs = new ArrayList<CustomerAddressDTO>();
			if(shippingAddressList!=null && !shippingAddressList.isEmpty()){
				count = DAORegistry.getCustomerAddressDAO().getShippingAddressCount(customerId);
				for(CustomerAddress shippingAddress : shippingAddressList){
					shippingAddressDTO = new CustomerAddressDTO();
					shippingAddressDTO.setId(shippingAddress.getId());
					shippingAddressDTO.setFirstName(shippingAddress.getFirstName());
					shippingAddressDTO.setLastName(shippingAddress.getLastName());
					shippingAddressDTO.setEmail(shippingAddress.getEmail());
					shippingAddressDTO.setPhone(shippingAddress.getPhone1());
					shippingAddressDTO.setStreet1(shippingAddress.getAddressLine1());
					shippingAddressDTO.setStreet2(shippingAddress.getAddressLine2());
					shippingAddressDTO.setState(shippingAddress.getState().getName());
					shippingAddressDTO.setStateId(shippingAddress.getState().getId());
					shippingAddressDTO.setCountry(shippingAddress.getCountry().getName());
					shippingAddressDTO.setCountryId(shippingAddress.getCountry().getId());
					shippingAddressDTO.setCity(shippingAddress.getCity());
					shippingAddressDTO.setZip(shippingAddress.getZipCode());
					shippingAddressDTO.setAddressType(shippingAddress.getAddressType().toString());
					shippingAddressDTOs.add(shippingAddressDTO);
				}
			}
			customerInfoDTO.setShippingAddressDTO(shippingAddressDTOs);
			customerInfoDTO.setShippingPaginationDTO(PaginationUtil.getDummyPaginationParameters(count));
			customerInfoDTO.setShippingCount(count);
			
			/*JSONArray cardArray = new JSONArray();
			List<CustomerStripeCreditCard> cards = DAORegistry.getCustomerStripeCreditCardDAO().getCustomerStripCardDetailsByCustomerId(customerId);
			if(cards!=null && !cards.isEmpty()){
				for(CustomerStripeCreditCard card : cards){
					JSONObject object = new JSONObject();
					object.put("id",card.getId());
					object.put("stripeCustomerId",card.getStripeCustomerId());
					object.put("stripeCardId",card.getStripeCardId());
					object.put("cardType",card.getCardType());
					object.put("cardLastFourDigit",card.getCardLastFourDigit());
					object.put("expiryMonth",card.getExpiryMonth());
					object.put("expiryYear",card.getExpiryYear());
					object.put("status",card.getStatus());
					cardArray.put(object);
				}
			}
			returnObject.put("cardInfo", cardArray);
			returnObject.put("cardPagingInfo", PaginationUtil.getDummyPaginationParameter(cardArray.length()));*/
			
			CustomerStripeCreditCardDTO creditCardDTO = null;
			List<CustomerStripeCreditCardDTO> creditCardDTOs = new ArrayList<CustomerStripeCreditCardDTO>();
			List<CustomerStripeCreditCard> cards = DAORegistry.getCustomerStripeCreditCardDAO().getCustomerStripCardDetailsByCustomerId(customerId);
			if(cards!=null && !cards.isEmpty()){
				for(CustomerStripeCreditCard card : cards){
					creditCardDTO = new CustomerStripeCreditCardDTO();
					creditCardDTO.setId(card.getId());
					creditCardDTO.setStripeCustomerId(card.getStripeCustomerId());
					creditCardDTO.setStripeCardId(card.getStripeCardId());
					creditCardDTO.setCardType(card.getCardType());
					creditCardDTO.setCardLastFourDigit(card.getCardLastFourDigit());
					creditCardDTO.setExpiryMonth(card.getExpiryMonth());
					creditCardDTO.setExpiryYear(card.getExpiryYear());
					creditCardDTO.setStatus(card.getStatus());
					creditCardDTOs.add(creditCardDTO);
				}
			}
			customerInfoDTO.setCreditCardDTO(creditCardDTOs);
			customerInfoDTO.setCreditCardPaginationDTO(PaginationUtil.getDummyPaginationParameters(creditCardDTOs.size()));
			
			/*CustomerWallet wallet = DAORegistry.getCustomerWalletDAO().getCustomerWalletByCustomerId(customerId);
			JSONObject creditObj = new JSONObject();
			if(wallet!=null){
				creditObj.put("activeCredit", String.format("%.2f", wallet.getActiveCredit()));
				creditObj.put("totalEarnedCredit", String.format("%.2f", wallet.getTotalCredit()));
				creditObj.put("totalUsedCredit", String.format("%.2f", wallet.getTotalUsedCredit()));
			}else{
				creditObj.put("activeCredit", String.format("%.2f", 0.00));
				creditObj.put("totalEarnedCredit", String.format("%.2f", 0.00));
				creditObj.put("totalUsedCredit", String.format("%.2f", 0.00));				
			}
			returnObject.put("creditInfo", creditObj);*/
			
			CustomerWalletDTO walletDTO = new CustomerWalletDTO();
			CustomerWallet wallet = DAORegistry.getCustomerWalletDAO().getCustomerWalletByCustomerId(customerId);
			if(wallet!=null){
				walletDTO.setActiveCredit(String.format("%.2f", wallet.getActiveCredit()));
				walletDTO.setTotalEarnedCredit(String.format("%.2f", wallet.getTotalCredit()));
				walletDTO.setTotalUsedCredit(String.format("%.2f", wallet.getTotalUsedCredit()));
			}else{
				walletDTO.setActiveCredit(String.format("%.2f", 0.00));
				walletDTO.setTotalEarnedCredit(String.format("%.2f", 0.00));
				walletDTO.setTotalUsedCredit(String.format("%.2f", 0.00));				
			}
			customerInfoDTO.setCustomerWalletDTO(walletDTO);
			
			/*JSONObject rewardPointsObj = new JSONObject();				
			CustomerLoyalty rewardPoints = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customerId);
			if(rewardPoints!=null){
				rewardPointsObj.put("id",rewardPoints.getId());
				rewardPointsObj.put("rewardCustomerId",rewardPoints.getCustomerId());
				rewardPointsObj.put("activePoints", String.format("%.2f", rewardPoints.getActivePoints()));
				rewardPointsObj.put("pendingPoints", String.format("%.2f", rewardPoints.getPendingPoints()));
				rewardPointsObj.put("totalEarnedPoints", String.format("%.2f", rewardPoints.getTotalEarnedPoints()));
				rewardPointsObj.put("totalSpentPoints", String.format("%.2f", rewardPoints.getTotalSpentPoints()));
				rewardPointsObj.put("latestEarnedPoints", String.format("%.2f", rewardPoints.getLatestEarnedPoints()));
				rewardPointsObj.put("latestSpentPoints", String.format("%.2f", rewardPoints.getLatestSpentPoints()));						
				rewardPointsObj.put("voidedPoints", String.format("%.2f", rewardPoints.getVoidedRewardPoints()));
			}
			returnObject.put("rewardPointsInfo", rewardPointsObj);*/
		
			CustomerLoyaltyDTO rewardPointsDTO = new CustomerLoyaltyDTO();
			CustomerLoyalty rewardPoints = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customerId);
			if(rewardPoints!=null){
				rewardPointsDTO.setId(rewardPoints.getId());
				rewardPointsDTO.setRewardCustomerId(rewardPoints.getCustomerId());
				rewardPointsDTO.setActivePoints(String.format("%.2f", rewardPoints.getActivePoints()));
				rewardPointsDTO.setPendingPoints(String.format("%.2f", rewardPoints.getPendingPoints()));
				rewardPointsDTO.setTotalEarnedPoints(String.format("%.2f", rewardPoints.getTotalEarnedPoints()));				
				rewardPointsDTO.setTotalSpentPoints(String.format("%.2f", rewardPoints.getTotalSpentPoints()));
				rewardPointsDTO.setLatestEarnedPoints(String.format("%.2f", rewardPoints.getLatestEarnedPoints()));
				rewardPointsDTO.setLatestSpentPoints(String.format("%.2f", rewardPoints.getLatestSpentPoints()));
				rewardPointsDTO.setVoidedPoints(String.format("%.2f", rewardPoints.getVoidedRewardPoints()));
			}else{
				rewardPointsDTO.setActivePoints(String.format("%.2f", 0.00));
				rewardPointsDTO.setPendingPoints(String.format("%.2f", 0.00));
				rewardPointsDTO.setTotalEarnedPoints(String.format("%.2f", 0.00));
				rewardPointsDTO.setTotalSpentPoints(String.format("%.2f", 0.00));
				rewardPointsDTO.setLatestEarnedPoints(String.format("%.2f", 0.00));
				rewardPointsDTO.setLatestSpentPoints(String.format("%.2f", 0.00));
				rewardPointsDTO.setVoidedPoints(String.format("%.2f", 0.00));
			}
			customerInfoDTO.setRewardPointsDTO(rewardPointsDTO);
				
			customerInfoDTO.setStatus(1);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return customerInfoDTO;	
	}
	
	@RequestMapping({"/GetCustomerInfoForInvoice"})
	public CustomerInfoInvoiceDTO getCustomerInfoForInvoice(HttpServletRequest request, HttpServletResponse response){
		CustomerInfoInvoiceDTO customerInfoInvoiceDTO = new CustomerInfoInvoiceDTO();
		Error error = new Error();
		//model.addAttribute("customerInfoInvoiceDTO", customerInfoInvoiceDTO);
		
		//JSONObject returnObject = new JSONObject();
		try{
			String customerIdStr = request.getParameter("customerId");
			String headerFilter = request.getParameter("headerFilter");
			
			if(StringUtils.isEmpty(customerIdStr)){
				System.err.println("Please provide CustomerId");
				error.setDescription("Please provide CustomerId");
				customerInfoInvoiceDTO.setError(error);
				customerInfoInvoiceDTO.setStatus(0);
				return customerInfoInvoiceDTO;
			}			
			Integer customerId = 0;	
			try{
				customerId = Integer.parseInt(customerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid CustomerId");
				error.setDescription("Please provide valid CustomerId");
				customerInfoInvoiceDTO.setError(error);
				customerInfoInvoiceDTO.setStatus(0);
				return customerInfoInvoiceDTO;
			}
			
			GridHeaderFilters filter = null;
			try {
				filter = GridHeaderFiltersUtil.getInvoiceSearchHeaderFilters(headerFilter);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				customerInfoInvoiceDTO.setError(error);
				customerInfoInvoiceDTO.setStatus(0);
				return customerInfoInvoiceDTO;
			}
			
			Customer customerInfo = DAORegistry.getCustomerDAO().get(customerId);
			
			/*if(customerInfo.getProductType().equals(ProductType.REWARDTHEFAN)){
				String productType = "";
				Collection<CustomerInvoices> custRTFInvoiceList = null;
				if(filter.getOrderType()==null || filter.getOrderType().isEmpty()){
					if(customerInfo.getRtwCustomerId() != null && customerInfo.getRtwCustomerId() > 0){
						productType = "RTW"; 
						custRTFInvoiceList = DAORegistry.getQueryManagerDAO().getRTFInvoicesForCustomer(customerId, productType, filter);
					}
					if(customerInfo.getRtw2CustomerId() != null && customerInfo.getRtw2CustomerId() > 0){
						productType = "RTW2"; 
						custRTFInvoiceList = DAORegistry.getQueryManagerDAO().getRTFInvoicesForCustomer(customerId, productType, filter);
					}
				}
				Collection<CustomerInvoices> custInvoiceList = DAORegistry.getQueryManagerDAO().getInvoicesForCustomer(customerId, filter);
				
				JSONArray invoiceArray = new JSONArray();
				if(custInvoiceList!=null && !custInvoiceList.isEmpty()){
					for(CustomerInvoices invoice : custInvoiceList){
						JSONObject object = new JSONObject();
						object.put("invoiceId",invoice.getInvoiceId());
						object.put("invoiceTotal", String.format("%.2f", invoice.getInvoiceTotal()));
						object.put("createdDate",invoice.getCreatedDateStr());
						object.put("createdBy",invoice.getCreatedBy());
						object.put("ticketCount",invoice.getTicketCount());
						//object.put("ext",invoice.getExtNo());
						object.put("orderType",invoice.getOrderType());
						object.put("productType", invoice.getProductType());
						if(invoice.getStatus().equalsIgnoreCase("completed")){
							object.put("status", "Yes");
						}else{
							object.put("status", "No");
						}
						invoiceArray.put(object);
					}
				}
				if(custRTFInvoiceList!=null && !custRTFInvoiceList.isEmpty()){
					for(CustomerInvoices invoice : custRTFInvoiceList){
						JSONObject object = new JSONObject();
						object.put("invoiceId",invoice.getInvoiceId());
						object.put("invoiceTotal", String.format("%.2f", invoice.getInvoiceTotal()));
						object.put("createdDate",invoice.getCreatedDateStr());
						object.put("createdBy",invoice.getCreatedBy());
						object.put("ticketCount",invoice.getTicketCount());
						//object.put("ext",invoice.getExtNo());
						object.put("orderType",invoice.getOrderType());
						object.put("productType", invoice.getProductType());
						if(invoice.getStatus().equalsIgnoreCase("completed")){
							object.put("status", "Yes");
						}else{
							object.put("status", "No");
						}
						object.put("status", "No");
						invoiceArray.put(object);
					}
				}
				returnObject.put("invoiceInfo", invoiceArray);
				returnObject.put("invoicePagingInfo", PaginationUtil.getDummyPaginationParameter(invoiceArray.length()));
			}else if(customerInfo.getProductType().equals(ProductType.RTW) || customerInfo.getProductType().equals(ProductType.RTW2)){
				//custPOList = DAORegistry.getQueryManagerDAO().getRTWPurchaseOrdersList(null,null,null,null,null,null,customerId);
				
				Collection<CustomerInvoices> custInvoiceList = DAORegistry.getQueryManagerDAO().getRTWInvoicesForCustomer(customerId,filter);
				JSONArray invoiceArray = new JSONArray();
				if(custInvoiceList != null && !custInvoiceList.isEmpty()){
					for(CustomerInvoices invoice : custInvoiceList){
						JSONObject object = new JSONObject();
						object.put("invoiceId",invoice.getInvoiceId());
						object.put("invoiceTotal", String.format("%.2f", invoice.getInvoiceTotal()));
						object.put("createdDate",invoice.getCreatedDateStr());
						object.put("createdBy",invoice.getCreatedBy());
						object.put("ticketCount",invoice.getTicketCount());
						//object.put("ext",invoice.getExtNo());
						object.put("orderType",invoice.getOrderType());
						object.put("productType", invoice.getProductType());
						if(invoice.getStatus().equalsIgnoreCase("completed")){
							object.put("status", "Yes");
						}else{
							object.put("status", "No");
						}
						object.put("status", "No");
						invoiceArray.put(object);
					}
				}
				returnObject.put("invoiceInfo", invoiceArray);
				returnObject.put("invoicePagingInfo", PaginationUtil.getDummyPaginationParameter(invoiceArray.length()));
			}*/
			
			List<CustomerInvoiceDTO> customerInvoiceDTOs = null;
			if(customerInfo.getProductType().equals(ProductType.REWARDTHEFAN)){
				String productType = "";
				Collection<CustomerInvoices> custRTFInvoiceList = null;
				if(filter.getOrderType()==null || filter.getOrderType().isEmpty()){
					if(customerInfo.getRtwCustomerId() != null && customerInfo.getRtwCustomerId() > 0){
						productType = "RTW"; 
						custRTFInvoiceList = DAORegistry.getQueryManagerDAO().getRTFInvoicesForCustomer(customerId, productType, filter);
					}
					if(customerInfo.getRtw2CustomerId() != null && customerInfo.getRtw2CustomerId() > 0){
						productType = "RTW2"; 
						custRTFInvoiceList = DAORegistry.getQueryManagerDAO().getRTFInvoicesForCustomer(customerId, productType, filter);
					}
				}
				Collection<CustomerInvoices> custInvoiceList = DAORegistry.getQueryManagerDAO().getInvoicesForCustomer(customerId, filter);
				
				CustomerInvoiceDTO customerInvoiceDTO = null;
				customerInvoiceDTOs = new ArrayList<CustomerInvoiceDTO>();
				if(custInvoiceList!=null && !custInvoiceList.isEmpty()){
					for(CustomerInvoices invoice : custInvoiceList){
						customerInvoiceDTO = new CustomerInvoiceDTO();
						customerInvoiceDTO.setInvoiceId(invoice.getInvoiceId());
						customerInvoiceDTO.setInvoiceTotal(String.format("%.2f", invoice.getInvoiceTotal()));
						customerInvoiceDTO.setCreatedDate(invoice.getCreatedDateStr());
						customerInvoiceDTO.setCreatedBy(invoice.getCreatedBy());
						customerInvoiceDTO.setTicketCount(invoice.getTicketCount());
						customerInvoiceDTO.setInvoiceType(invoice.getInvoiceType().equalsIgnoreCase("INVOICE")?"REGULAR":invoice.getInvoiceType());
						customerInvoiceDTO.setStatus(invoice.getStatus().toString());
						//customerInvoiceDTO.setExt(invoice.getExtNo());
						customerInvoiceDTO.setOrderType(invoice.getOrderType());
						customerInvoiceDTO.setProductType(invoice.getProductType());
						/*if(invoice.getStatus().equalsIgnoreCase("completed")){
							customerInvoiceDTO.setStatus("Yes");
						}else{
							customerInvoiceDTO.setStatus("No");
						}*/
						customerInvoiceDTOs.add(customerInvoiceDTO);
					}
				}
				if(custRTFInvoiceList!=null && !custRTFInvoiceList.isEmpty()){
					for(CustomerInvoices invoice : custRTFInvoiceList){
						customerInvoiceDTO = new CustomerInvoiceDTO();
						customerInvoiceDTO.setInvoiceId(invoice.getInvoiceId());
						customerInvoiceDTO.setInvoiceTotal(String.format("%.2f", invoice.getInvoiceTotal()));
						customerInvoiceDTO.setCreatedDate(invoice.getCreatedDateStr());
						customerInvoiceDTO.setCreatedBy(invoice.getCreatedBy());
						customerInvoiceDTO.setTicketCount(invoice.getTicketCount());
						customerInvoiceDTO.setInvoiceType("REGULAR");
						customerInvoiceDTO.setStatus(invoice.getStatus().toString());
						//customerInvoiceDTO.setExt(invoice.getExtNo());
						customerInvoiceDTO.setOrderType(invoice.getOrderType());
						customerInvoiceDTO.setProductType(invoice.getProductType());
						/*if(invoice.getStatus().equalsIgnoreCase("completed")){
							customerInvoiceDTO.setStatus("Yes");
						}else{
							customerInvoiceDTO.setStatus("No");
						}*/
						//customerInvoiceDTO.setStatus("No");
						customerInvoiceDTOs.add(customerInvoiceDTO);
					}
				}
				customerInfoInvoiceDTO.setCustomerInvoiceDTO(customerInvoiceDTOs);
				customerInfoInvoiceDTO.setInvoicePaginationDTO(PaginationUtil.getDummyPaginationParameters(customerInvoiceDTOs.size()));
			}else if(customerInfo.getProductType().equals(ProductType.RTW) || customerInfo.getProductType().equals(ProductType.RTW2)){
				//custPOList = DAORegistry.getQueryManagerDAO().getRTWPurchaseOrdersList(null,null,null,null,null,null,customerId);
				Collection<CustomerInvoices> custInvoiceList = DAORegistry.getQueryManagerDAO().getRTWInvoicesForCustomer(customerId,filter);
				
				CustomerInvoiceDTO customerInvoiceDTO = null;
				customerInvoiceDTOs = new ArrayList<CustomerInvoiceDTO>();
				if(custInvoiceList != null && !custInvoiceList.isEmpty()){
					for(CustomerInvoices invoice : custInvoiceList){
						customerInvoiceDTO = new CustomerInvoiceDTO();
						customerInvoiceDTO.setInvoiceId(invoice.getInvoiceId());
						customerInvoiceDTO.setInvoiceTotal(String.format("%.2f", invoice.getInvoiceTotal()));
						customerInvoiceDTO.setCreatedDate(invoice.getCreatedDateStr());
						customerInvoiceDTO.setCreatedBy(invoice.getCreatedBy());
						customerInvoiceDTO.setTicketCount(invoice.getTicketCount());
						//customerInvoiceDTO.setExt(invoice.getExtNo());
						customerInvoiceDTO.setOrderType(invoice.getOrderType());
						customerInvoiceDTO.setProductType(invoice.getProductType());
						/*if(invoice.getStatus().equalsIgnoreCase("completed")){
							customerInvoiceDTO.setStatus("Yes");
						}else{
							customerInvoiceDTO.setStatus("No");
						}*/
						customerInvoiceDTO.setStatus("No");
						customerInvoiceDTOs.add(customerInvoiceDTO);
					}
				}
				customerInfoInvoiceDTO.setCustomerInvoiceDTO(customerInvoiceDTOs);
				customerInfoInvoiceDTO.setInvoicePaginationDTO(PaginationUtil.getDummyPaginationParameters(customerInvoiceDTOs.size()));
			}
			
			if(customerInvoiceDTOs.size() <= 0){
				customerInfoInvoiceDTO.setMessage("No Invoice data found for Selected Customer.");
			}
			customerInfoInvoiceDTO.setStatus(1);
		}catch(Exception e){
			e.printStackTrace();
		}
		return customerInfoInvoiceDTO;
	}
	

	@RequestMapping({"/GetCustomerInfoForInvoiceTickets"})
	public CustomerInfoInvoiceTicketDTO getCustomerInfoForInvoiceTickets(HttpServletRequest request, HttpServletResponse response){
		CustomerInfoInvoiceTicketDTO customerInfoInvoiceTicketDTO = new CustomerInfoInvoiceTicketDTO();
		Error error = new Error();
		//model.addAttribute("customerInfoInvoiceTicketDTO", customerInfoInvoiceTicketDTO);
		
		try{
			String invoiceIdStr = request.getParameter("invoiceId");
			
			if(StringUtils.isEmpty(invoiceIdStr)){
				System.err.println("Please provide InvoiceId");
				error.setDescription("Please provide InvoiceId");
				customerInfoInvoiceTicketDTO.setError(error);
				customerInfoInvoiceTicketDTO.setStatus(0);
				return customerInfoInvoiceTicketDTO;
			}			
			Integer invoiceId = 0;	
			try{
				invoiceId = Integer.parseInt(invoiceIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid InvoiceId");
				error.setDescription("Please provide valid InvoiceId");
				customerInfoInvoiceTicketDTO.setError(error);
				customerInfoInvoiceTicketDTO.setStatus(0);
				return customerInfoInvoiceTicketDTO;
			}
						
			List<InvoiceTicketAttachment> ticketAttachments = DAORegistry.getInvoiceTicketAttachmentDAO().getTicketAttachmentByInvoiceId(invoiceId);
			
			CustomerInvoiceTicketAttachmentDTO invoiceTicketAttachmentDTO = null;
			List<CustomerInvoiceTicketAttachmentDTO> invoiceTicketAttachmentDTOs = new ArrayList<CustomerInvoiceTicketAttachmentDTO>();
			if(ticketAttachments!=null && !ticketAttachments.isEmpty()){
				for(InvoiceTicketAttachment invoice : ticketAttachments){
					invoiceTicketAttachmentDTO = new CustomerInvoiceTicketAttachmentDTO();
					invoiceTicketAttachmentDTO.setId(invoice.getId());
					invoiceTicketAttachmentDTO.setInvoiceId(invoice.getInvoiceId());
					invoiceTicketAttachmentDTO.setFileName(Util.getFileNameFromPath(invoice.getFilePath()));
					invoiceTicketAttachmentDTO.setFileType(invoice.getType().toString());
					invoiceTicketAttachmentDTO.setPosition(invoice.getPosition().toString());
					if(Util.getFileNameFromPath(invoice.getFilePath()) != null){
						invoiceTicketAttachmentDTO.setDownloadTicket("Yes");
					}else{
						invoiceTicketAttachmentDTO.setDownloadTicket("No");
					}
					invoiceTicketAttachmentDTOs.add(invoiceTicketAttachmentDTO);
				}
			}
			
			if(ticketAttachments==null || ticketAttachments.isEmpty()){
				customerInfoInvoiceTicketDTO.setMessage("No Invoice Ticket to Download for Selected Invoice.");
			}
			customerInfoInvoiceTicketDTO.setStatus(1);
			customerInfoInvoiceTicketDTO.setCustomerInvoiceTicketAttachmentDTO(invoiceTicketAttachmentDTOs);
			customerInfoInvoiceTicketDTO.setInvoiceTicketPaginationDTO(PaginationUtil.getDummyPaginationParameters(invoiceTicketAttachmentDTOs.size()));
						
			/*JSONArray invoiceTicketArray = new JSONArray();
			if(ticketAttachments!=null && !ticketAttachments.isEmpty()){
				for(InvoiceTicketAttachment invoice : ticketAttachments){
					JSONObject object = new JSONObject();
					object.put("id", invoice.getId());
					object.put("invoiceId", invoice.getInvoiceId());
					object.put("fileName", Util.getFileNameFromPath(invoice.getFilePath()));
					object.put("fileType", invoice.getType());
					object.put("position", invoice.getPosition());
					if(Util.getFileNameFromPath(invoice.getFilePath()) != null){
						object.put("downloadTicket", "Yes");
					}else{
						object.put("downloadTicket", "No");
					}
					array.put(object);
				}
			}else{
				map.put("successMessage", "There is no ticket to download.");
			}
				
			map.put("invoiceInfo", invoiceTicketArray);
			map.put("pagingInfo", PaginationUtil.getDummyPaginationParameter(invoiceTicketArray.length()));*/
		}catch(Exception e){
			e.printStackTrace();
		}
		return customerInfoInvoiceTicketDTO;
	}
	
	@RequestMapping({"/GetCustomerInfoForPO"})
	public CustomerInfoPODTO getCustomerInfoForPO(HttpServletRequest request, HttpServletResponse response){
		CustomerInfoPODTO customerInfoPODTO = new CustomerInfoPODTO();
		Error error = new Error();
		//model.addAttribute("customerInfoPODTO", customerInfoPODTO);
		
		//JSONObject returnObject = new JSONObject();
		try{
			String customerIdStr = request.getParameter("customerId");
			String headerFilter = request.getParameter("headerFilter");
			
			if(StringUtils.isEmpty(customerIdStr)){
				System.err.println("Please provide CustomerId");
				error.setDescription("Please provide CustomerId");
				customerInfoPODTO.setError(error);
				customerInfoPODTO.setStatus(0);
				return customerInfoPODTO;
			}			
			Integer customerId = 0;	
			try{
				customerId = Integer.parseInt(customerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid CustomerId");
				error.setDescription("Please provide valid CustomerId");
				customerInfoPODTO.setError(error);
				customerInfoPODTO.setStatus(0);
				return customerInfoPODTO;
			}
			
			GridHeaderFilters filter = null;
			try {
				filter = GridHeaderFiltersUtil.getPOSearchHeaderFilters(headerFilter);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				customerInfoPODTO.setError(error);
				customerInfoPODTO.setStatus(0);
				return customerInfoPODTO;
			}
						
			Collection<PurchaseOrders> custPOList = null;
			Collection<PurchaseOrders> custRTFPOList = null;
			
			Customer customerInfo = DAORegistry.getCustomerDAO().get(customerId);
			if(customerInfo.getProductType().equals(ProductType.REWARDTHEFAN)){
				String productType = "";
				
				if(customerInfo.getRtwCustomerId() != null && customerInfo.getRtwCustomerId() > 0){
					productType = "RTW"; 
					custRTFPOList = DAORegistry.getQueryManagerDAO().getRTFPOForCustomer(customerId, productType, filter);
				}
				if(customerInfo.getRtw2CustomerId() != null && customerInfo.getRtw2CustomerId() > 0){
					productType = "RTW2"; 
					custRTFPOList = DAORegistry.getQueryManagerDAO().getRTFPOForCustomer(customerId, productType, filter);
				}
				custPOList = DAORegistry.getQueryManagerDAO().getPOForCustomer(customerId, filter);
				
			}else if(customerInfo.getProductType().equals(ProductType.RTW) || customerInfo.getProductType().equals(ProductType.RTW2)){
				custPOList = DAORegistry.getQueryManagerDAO().getRTWPOForCustomer(customerId,filter);
			}
			
			CustomerPurchaseOrderDTO customerPODTO = null;
			List<CustomerPurchaseOrderDTO> customerPODTOs = new ArrayList<CustomerPurchaseOrderDTO>();
			if(custPOList!=null && !custPOList.isEmpty()){
				for(PurchaseOrders po : custPOList){
					customerPODTO = new CustomerPurchaseOrderDTO();
					customerPODTO.setPoId(po.getId());
					customerPODTO.setCustomerName(po.getCustomerName());
					customerPODTO.setCustomerType(po.getCustomerType());
					customerPODTO.setTotal(String.format("%.2f", po.getPoTotal()));
					customerPODTO.setTicketQty(po.getTicketQty().toString());
					customerPODTO.setCreated(po.getCreateDateStr());
					customerPODTO.setShippingType(po.getShippingType());
					customerPODTO.setStatus(po.getStatus());
					customerPODTO.setProductType(po.getProductType());
					customerPODTOs.add(customerPODTO);
				}
			}
			if(custRTFPOList!=null && !custRTFPOList.isEmpty()){
				for(PurchaseOrders po : custRTFPOList){
					customerPODTO = new CustomerPurchaseOrderDTO();
					customerPODTO.setPoId(po.getId());
					customerPODTO.setCustomerName(po.getCustomerName());
					customerPODTO.setCustomerType(po.getCustomerType());
					customerPODTO.setTotal(String.format("%.2f", po.getPoTotal()));
					customerPODTO.setTicketQty(po.getTicketQty().toString());
					customerPODTO.setCreated(po.getCreateDateStr());
					customerPODTO.setShippingType(po.getShippingType());
					customerPODTO.setStatus(po.getStatus());
					customerPODTO.setProductType(po.getProductType());
					customerPODTOs.add(customerPODTO);
				}
			}
			if(customerPODTOs.size() <= 0){
				customerInfoPODTO.setMessage("No Purchase Order data found for Selected Customer.");
			}
			customerInfoPODTO.setStatus(1);
			customerInfoPODTO.setCustomerPurchaseOrderDTO(customerPODTOs);
			customerInfoPODTO.setPoPaginationDTO(PaginationUtil.getDummyPaginationParameters(customerPODTOs.size()));
			
			
			/*Integer customerId = Integer.parseInt(customerIdStr);			
			
			JSONArray poArray = new JSONArray();
			GridHeaderFilters filter = GridHeaderFiltersUtil.getPOSearchHeaderFilters(headerFilter);
			
			if(custPOList!=null && !custPOList.isEmpty()){
				for(PurchaseOrders po : custPOList){
					JSONObject object = new JSONObject();
					object.put("poId",po.getId());
					object.put("customerName",po.getCustomerName());
					object.put("customerType",po.getCustomerType());
					object.put("total", String.format("%.2f", po.getPoTotal()));
					object.put("ticketQty",po.getTicketQty());
					object.put("created",po.getCreateDateStr());
					object.put("shippingType",po.getShippingType());
					object.put("status",po.getStatus());
					object.put("productType",po.getProductType());
					poArray.put(object);
				}
			}
			if(custRTFPOList!=null && !custRTFPOList.isEmpty()){
				for(PurchaseOrders po : custRTFPOList){
					JSONObject object = new JSONObject();
					object.put("poId",po.getId());
					object.put("customerName",po.getCustomerName());
					object.put("customerType",po.getCustomerType());
					object.put("total", String.format("%.2f", po.getPoTotal()));
					object.put("ticketQty",po.getTicketQty());
					object.put("created",po.getCreateDateStr());
					object.put("shippingType",po.getShippingType());
					object.put("status",po.getStatus());
					object.put("productType",po.getProductType());
					poArray.put(object);
				}
			}
			if(poArray.length() <= 0){
				returnObject.put("msg", "No Purchase Order data found for Selected Customer.");
			}
			returnObject.put("poInfo", poArray)	;
			returnObject.put("poPagingInfo", PaginationUtil.getDummyPaginationParameter(poArray.length()));
			response.setHeader("Content-type","application/json");
			IOUtils.write(returnObject.toString().getBytes(),response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
		}
		return customerInfoPODTO;
	}
	
	@RequestMapping({"/AddClientBroker"})
	public String addClientBroker(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		try{
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//map.put("url", "Client");
		return "page-add-client-broker";
	}
	
	
	@RequestMapping({"/AddRewardPointsForCustomer"})
	public CustomerAddRewardPointDTO addRewardPointsForCustomer(HttpServletRequest request, HttpServletResponse response){
		CustomerAddRewardPointDTO customerAddRewardPointDTO = new CustomerAddRewardPointDTO();
		Error error = new Error();
		//model.addAttribute("customerAddRewardPointDTO", customerAddRewardPointDTO);
		
		try{
			String customerIdStr = request.getParameter("customerId");
			String rewardPoints = request.getParameter("rewardPoints");
			String userName = request.getParameter("userName");			
			
			if(StringUtils.isEmpty(customerIdStr)){
				System.err.println("Please provide CustomerId");
				error.setDescription("Please provide CustomerId");
				customerAddRewardPointDTO.setError(error);
				customerAddRewardPointDTO.setStatus(0);
				return customerAddRewardPointDTO;
			}			
			Integer customerId = 0;	
			try{
				customerId = Integer.parseInt(customerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid CustomerId");
				error.setDescription("Please provide valid CustomerId");
				customerAddRewardPointDTO.setError(error);
				customerAddRewardPointDTO.setStatus(0);
				return customerAddRewardPointDTO;
			}
			
			if(StringUtils.isEmpty(rewardPoints)){
				System.err.println("Please provide Reward Points");
				error.setDescription("Please provide Reward Points");
				customerAddRewardPointDTO.setError(error);
				customerAddRewardPointDTO.setStatus(0);
				return customerAddRewardPointDTO;
			}			
			Double rewardPointDouble = 0.0;	
			try{
				rewardPointDouble = Double.parseDouble(rewardPoints);
			}catch (Exception e) {
				System.err.println("Please provide valid Reward Points");
				error.setDescription("Please provide valid Reward Points");
				customerAddRewardPointDTO.setError(error);
				customerAddRewardPointDTO.setStatus(0);
				return customerAddRewardPointDTO;
			}
			
			Customer customer  = DAORegistry.getCustomerDAO().get(customerId);
			if(customer == null){
				System.err.println("Customer data not Found");
				error.setDescription("Customer data not Found");
				customerAddRewardPointDTO.setError(error);
				customerAddRewardPointDTO.setStatus(0);
				return customerAddRewardPointDTO;
			}
			
			CustomerLoyalty loyalty = null;
			CustomerLoyaltyTracking loyaltyTracking = null;					
			loyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customerId);
			if(loyalty == null){
				loyalty = new CustomerLoyalty();
				loyalty.setActivePoints(0.00);
				loyalty.setActiveRewardDollers(0.00);
				loyalty.setCustomerId(customer.getId());
				loyalty.setLastUpdate(new Date());
				loyalty.setLatestEarnedPoints(0.00);
				loyalty.setLatestSpentPoints(0.00);
				loyalty.setPendingPoints(0.00);
				loyalty.setRevertedSpentPoints(0.00);
				loyalty.setTotalEarnedPoints(0.00);
				loyalty.setTotalSpentPoints(0.00);
				loyalty.setVoidedRewardPoints(0.00);
			}
			loyalty.setActivePoints(loyalty.getActivePoints() + Util.getRoundedValue(rewardPointDouble));
			loyalty.setTotalEarnedPoints(loyalty.getTotalEarnedPoints() + Util.getRoundedValue(rewardPointDouble));
			loyalty.setLatestEarnedPoints(Util.getRoundedValue(rewardPointDouble));
			loyalty.setLastUpdate(new Date());
			DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(loyalty);
			
			//Customer Loyalty Tracking
			loyaltyTracking = new CustomerLoyaltyTracking();
			loyaltyTracking.setCustomerId(customer.getId());
			loyaltyTracking.setRewardPoints(Util.getRoundedValue(rewardPointDouble));
			loyaltyTracking.setCreatedBy(userName);
			loyaltyTracking.setCreatedDate(new Date());
			DAORegistry.getCustomerLoyaltyTrackingDAO().save(loyaltyTracking);
		
			/*Map<String,Object> mailMap = new HashMap<String,Object>();
			mailMap.put("customer", customer); 
			mailMap.put("points", points);
			mailMap.put("rewardPointsLink","https://www.rewardthefan.com/CustomerRewardPoints");
			
			com.rtw.tracker.mail.MailAttachment[] mailAttachment = new com.rtw.tracker.mail.MailAttachment[1];
			mailAttachment[0] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","logo.png",com.rtw.tmat.utils.Util.getFilePath(request, "logo.png", "/images/"));
			try {
				mailManager.sendMailNow("text/html","sales@rewardthefan.com",customer.getEmail(), 
						null, "msanghani@rightthisway.com,AODev@rightthisway.com", 
						"Reward The Fan:Referral Order Reward Points Information",
			    		"mail-reward-points-referral.html", mailMap, "text/html", null,mailAttachment,null);
				} catch (Exception e) {
					e.printStackTrace();
				}*/
			customerAddRewardPointDTO.setMessage("Customer Reward Points Added.");
			customerAddRewardPointDTO.setStatus(1);
			
			//Tracking User Action
			String userActionMsg = "Customer Reward Points Added.";
			Util.userActionAudit(request, customer.getId(), userActionMsg);
			
			CustomerLoyaltyDTO rewardPointsDTO = new CustomerLoyaltyDTO();	
			if(loyalty!=null){
				rewardPointsDTO.setId(loyalty.getId());
				rewardPointsDTO.setRewardCustomerId(loyalty.getCustomerId());
				rewardPointsDTO.setActivePoints(String.format("%.2f", loyalty.getActivePoints()));
				rewardPointsDTO.setPendingPoints(String.format("%.2f", loyalty.getPendingPoints()));
				rewardPointsDTO.setTotalEarnedPoints(String.format("%.2f", loyalty.getTotalEarnedPoints()));				
				rewardPointsDTO.setTotalSpentPoints(String.format("%.2f", loyalty.getTotalSpentPoints()));
				rewardPointsDTO.setLatestEarnedPoints(String.format("%.2f", loyalty.getLatestEarnedPoints()));
				rewardPointsDTO.setLatestSpentPoints(String.format("%.2f", loyalty.getLatestSpentPoints()));
				rewardPointsDTO.setVoidedPoints(String.format("%.2f", loyalty.getVoidedRewardPoints()));
			}
			customerAddRewardPointDTO.setRewardPointsDTO(rewardPointsDTO);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return customerAddRewardPointDTO;
	}
	
	
	@RequestMapping({"/EditCustomerInfoForRewardPoints"})
	 public CustomerInfoRewardPointsHistoryDTO editCustomerInfoForRewardPoints(HttpServletRequest request, HttpServletResponse response){
		CustomerInfoRewardPointsHistoryDTO customerInfoRewardPointsHistoryDTO = new CustomerInfoRewardPointsHistoryDTO();
		Error error = new Error();
		//model.addAttribute("customerInfoRewardPointsHistoryDTO", customerInfoRewardPointsHistoryDTO);
		
		//JSONObject jObj = new JSONObject();
		try{
			String customerIdStr = request.getParameter("customerId");
			String rewardPointBreakUp = request.getParameter("rewardPointBreakUp");
			
			if(StringUtils.isEmpty(customerIdStr)){
				System.err.println("Please provide CustomerId");
				error.setDescription("Please provide CustomerId");
				customerInfoRewardPointsHistoryDTO.setError(error);
				customerInfoRewardPointsHistoryDTO.setStatus(0);
				return customerInfoRewardPointsHistoryDTO;
			}			
			Integer customerId = 0;	
			try{
				customerId = Integer.parseInt(customerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid CustomerId");
				error.setDescription("Please provide valid CustomerId");
				customerInfoRewardPointsHistoryDTO.setError(error);
				customerInfoRewardPointsHistoryDTO.setStatus(0);
				return customerInfoRewardPointsHistoryDTO;
			}
			
			if(StringUtils.isEmpty(rewardPointBreakUp)){
				System.err.println("Please provide Break Up Points");
				error.setDescription("Please provide Break Up Points");
				customerInfoRewardPointsHistoryDTO.setError(error);
				customerInfoRewardPointsHistoryDTO.setStatus(0);
				return customerInfoRewardPointsHistoryDTO;
			}
			
			GridHeaderFilters filter = new GridHeaderFilters(); 
			Integer count = 0;
			List<RewardDetails> rewardList = null;
			
			if(rewardPointBreakUp.equalsIgnoreCase("Earned Points")){
				rewardList = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerRewardsByRewardStatus(RewardStatus.ACTIVE,customerId, filter);
				count = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerRewardsByRewardStatusCount(RewardStatus.ACTIVE, customerId, filter);
			}
			if(rewardPointBreakUp.equalsIgnoreCase("Redeemed Points")){
				rewardList = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerRedeemedRewardsByCustomerId(customerId, filter);
				count = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerRedeemedRewardsByCustomerIdCount(customerId, filter);
			}
			
			if(rewardList == null || rewardList.size() <= 0){
				customerInfoRewardPointsHistoryDTO.setMessage("There is no Reward Points History.");
			}
			customerInfoRewardPointsHistoryDTO.setStatus(1);
			customerInfoRewardPointsHistoryDTO.setCustomerRewardPointsDTO(com.rtw.tracker.utils.Util.getCustomerRewardPointArray(rewardList));
			customerInfoRewardPointsHistoryDTO.setRewardPointsPaginationDTO(PaginationUtil.getDummyPaginationParameters(count));
			
			/*jObj.put("pagingInfo", PaginationUtil.getDummyPaginationParameter(count));
			jObj.put("rewardPointsList", JsonWrapperUtil.getRewardPointsArray(rewardList));
			jObj.put("customerId", customerIdStr);
			jObj.put("rewardPointBreakUp", rewardPointBreakUp);
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();	
		}	
		return customerInfoRewardPointsHistoryDTO;
	} 
		
	 
	@RequestMapping({"/GetCustomerInfoForRewardPoints"})
	public CustomerInfoRewardPointsHistoryDTO getCustomerInfoForRewardPoints(HttpServletRequest request, HttpServletResponse response){
		CustomerInfoRewardPointsHistoryDTO customerInfoRewardPointsHistoryDTO = new CustomerInfoRewardPointsHistoryDTO();
		Error error = new Error();
		//model.addAttribute("customerInfoRewardPointsHistoryDTO", customerInfoRewardPointsHistoryDTO);
		
		//JSONObject returnObject = new JSONObject();		
		try{
			String customerIdStr = request.getParameter("customerId");
			String rewardPointBreakUp = request.getParameter("rewardPointBreakUp");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			if(StringUtils.isEmpty(customerIdStr)){
				System.err.println("Please provide CustomerId");
				error.setDescription("Please provide CustomerId");
				customerInfoRewardPointsHistoryDTO.setError(error);
				customerInfoRewardPointsHistoryDTO.setStatus(0);
				return customerInfoRewardPointsHistoryDTO;
			}			
			Integer customerId = 0;	
			try{
				customerId = Integer.parseInt(customerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid CustomerId");
				error.setDescription("Please provide valid CustomerId");
				customerInfoRewardPointsHistoryDTO.setError(error);
				customerInfoRewardPointsHistoryDTO.setStatus(0);
				return customerInfoRewardPointsHistoryDTO;
			}
			
			if(StringUtils.isEmpty(rewardPointBreakUp)){
				System.err.println("Please provide Break Up Points");
				error.setDescription("Please provide Break Up Points");
				customerInfoRewardPointsHistoryDTO.setError(error);
				customerInfoRewardPointsHistoryDTO.setStatus(0);
				return customerInfoRewardPointsHistoryDTO;
			}
			if(StringUtils.isEmpty(pageNo)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				customerInfoRewardPointsHistoryDTO.setError(error);
				customerInfoRewardPointsHistoryDTO.setStatus(0);
				return customerInfoRewardPointsHistoryDTO;
			}
			try {
				Integer.parseInt(pageNo);
			} catch (Exception e) {
				System.err.println("Please provide valid PageNo.");
				error.setDescription("Please provide valid PageNo.");
				customerInfoRewardPointsHistoryDTO.setError(error);
				customerInfoRewardPointsHistoryDTO.setStatus(0);
				return customerInfoRewardPointsHistoryDTO;
			}
			
			GridHeaderFilters filter = null;
			try {
				filter = GridHeaderFiltersUtil.getRewardPointsSearchHeaderFilters(headerFilter);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				customerInfoRewardPointsHistoryDTO.setError(error);
				customerInfoRewardPointsHistoryDTO.setStatus(0);
				return customerInfoRewardPointsHistoryDTO;
			}
			
			Integer count = 0;
			List<RewardDetails> rewardList = null;
						
			if(rewardPointBreakUp.equalsIgnoreCase("Earned Points")){
				rewardList = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerRewardsByRewardStatus(RewardStatus.ACTIVE, customerId, filter);
				count = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerRewardsByRewardStatusCount(RewardStatus.ACTIVE, customerId, filter);
			}
			if(rewardPointBreakUp.equalsIgnoreCase("Redeemed Points")){
				rewardList = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerRedeemedRewardsByCustomerId(customerId, filter);
				count = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerRedeemedRewardsByCustomerIdCount(customerId, filter);
			}
			
			if(rewardList == null || rewardList.size() <= 0){
				customerInfoRewardPointsHistoryDTO.setMessage("There is no Reward Points History.");
			}
			customerInfoRewardPointsHistoryDTO.setStatus(1);
			customerInfoRewardPointsHistoryDTO.setCustomerRewardPointsDTO(com.rtw.tracker.utils.Util.getCustomerRewardPointArray(rewardList));
			customerInfoRewardPointsHistoryDTO.setRewardPointsPaginationDTO(PaginationUtil.getDummyPaginationParameters(count));
			
			/*returnObject.put("pagingInfo", PaginationUtil.getDummyPaginationParameter(count));
			returnObject.put("rewardPointsList", JsonWrapperUtil.getRewardPointsArray(rewardList));
			IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
		}
		return customerInfoRewardPointsHistoryDTO;
	}
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @param map
	 * @param session
	 * @return
	 */
	@RequestMapping({"/AddClient"})
	public String addClient(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		try{
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//map.put("url", "Client");
		return "page-add-client";
	}
	
	@RequestMapping({"/GetManageBrokers"})
	public void loadManageBrokers(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		JSONObject returnObject = new JSONObject();
		try{
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String status = request.getParameter("status");
			Integer count = 0;
			GridHeaderFilters filter = GridHeaderFiltersUtil.getUserSearchHeaderFilters(headerFilter);
			Collection<TrackerUser> trackerBrokerList= DAORegistry.getTrackerUserDAO().getAllTrackerUsersByRole("ROLE_BROKER",status,filter,pageNo);
			if(trackerBrokerList != null && trackerBrokerList.size() > 0){
				count = trackerBrokerList.size();
			}else if(trackerBrokerList == null || trackerBrokerList.size() <= 0){
				returnObject.put("msg", "No "+status+" Brokers found.");
			}
			
			returnObject.put("trackerBrokerList", JsonWrapperUtil.getMangeUsersArray(trackerBrokerList));
			returnObject.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));
			IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();			
		}
	}
	

	@RequestMapping({"/AddUsers"})
	public AddUserDTO addUserPage(HttpServletRequest request, HttpServletResponse response, Model model){
		AddUserDTO addUserDTO = new AddUserDTO();
		Error error = new Error();
		model.addAttribute("addUserDTO", addUserDTO);
		
		try{
			//String returnMessage = "";			
			String action = request.getParameter("action");
			String roleAffiliateBroker = request.getParameter("roleAffiliateBroker");
			String sessionUser = request.getParameter("sessionUser");
			if(StringUtils.isEmpty(sessionUser)){
				System.err.println("Please Login.");
				error.setDescription("Please Login.");
				addUserDTO.setError(error);
				addUserDTO.setStatus(0);
				return addUserDTO;
			}
			
			TrackerUser trackerUser = new TrackerUser();
			
			if(action != null && action.equals("action")){
				Gson gson = GsonCustomConfig.getGsonBuilder();
				
				String userName = request.getParameter("userName");
				String firstName = request.getParameter("firstName");
				String lastName = request.getParameter("lastName");
				String email = request.getParameter("email");
				String password = request.getParameter("password");
				String rePassword = request.getParameter("rePassword");
				String phone = request.getParameter("phone");
				String roless = request.getParameter("role");
				
				//For Role Broker
				String companyName = request.getParameter("companyName");
				String serviceFees = request.getParameter("serviceFees");
				
				//For Role Affiliates
				String fromDateStr = request.getParameter("fromDateStr");
				String toDateStr = request.getParameter("toDateStr");
				String affiliateDisc = request.getParameter("affiliateDisc");
				String customerOrderDisc = request.getParameter("customerOrderDisc");
				String status = request.getParameter("status");
				String repeatBusiness = request.getParameter("repeatBusiness");
				String phoneAffiliateDisc = request.getParameter("phoneAffiliateDiscount");
				String phoneCustomerOrderDisc = request.getParameter("phoneCustomerOrderDiscount");
				String canEarnRewardPoints = request.getParameter("canEarnRewardPoints");
				String promoCode = request.getParameter("promoCode");
				
				if(StringUtils.isEmpty(userName)){
					System.err.println("Please provide User Name.");
					error.setDescription("Please provide User Name.");
					addUserDTO.setError(error);
					addUserDTO.setStatus(0);
					return addUserDTO;
				}
				if(StringUtils.isEmpty(firstName)){
					System.err.println("Please provide First Name.");
					error.setDescription("Please provide First Name.");
					addUserDTO.setError(error);
					addUserDTO.setStatus(0);
					return addUserDTO;
				}
				if(StringUtils.isEmpty(lastName)){
					System.err.println("Please provide Last Name.");
					error.setDescription("Please provide Last Name.");
					addUserDTO.setError(error);
					addUserDTO.setStatus(0);
					return addUserDTO;
				}
				if(StringUtils.isEmpty(email)){
					System.err.println("Please provide Email.");
					error.setDescription("Please provide Email.");
					addUserDTO.setError(error);
					addUserDTO.setStatus(0);
					return addUserDTO;
				}
				if(!AdminController.validateEMail(email)){
					System.err.println("Please provide Valid Email.");
					error.setDescription("Please provide Valid Email.");
					addUserDTO.setError(error);
					addUserDTO.setStatus(0);
					return addUserDTO;
				}
				if(StringUtils.isEmpty(password)){
					System.err.println("Please provide Password.");
					error.setDescription("Please provide Password.");
					addUserDTO.setError(error);
					addUserDTO.setStatus(0);
					return addUserDTO;
				}				
				if(StringUtils.isEmpty(rePassword)){
					System.err.println("Please provide Re-Password.");
					error.setDescription("Please provide Re-Password.");
					addUserDTO.setError(error);
					addUserDTO.setStatus(0);
					return addUserDTO;
				}
				if(!password.equals(rePassword)){
					System.err.println("Password and Re-Password Must match.");
					error.setDescription("Password and Re-Password Must match.");
					addUserDTO.setError(error);
					addUserDTO.setStatus(0);
					return addUserDTO;
				}
				if(StringUtils.isEmpty(roless)){
					System.err.println("Please select Roles.");
					error.setDescription("Please select Roles.");
					addUserDTO.setError(error);
					addUserDTO.setStatus(0);
					return addUserDTO;
				}
				
				String roles[] = null;
				if(StringUtils.isNotEmpty(roless)){
					roles = gson.fromJson(roless, new TypeToken<String[]>(){}.getType());
				}
				
				Date fromDate = null;
				Date toDate = null;
				for(String role : roles){
					if(role.toUpperCase().equals("ROLE_BROKER")){
						if(StringUtils.isEmpty(companyName)){
							System.err.println("Please provide Company Name.");
							error.setDescription("Please provide Company Name.");
							addUserDTO.setError(error);
							addUserDTO.setStatus(0);
							return addUserDTO;
						}
						if(StringUtils.isEmpty(serviceFees)){
							System.err.println("Please provide Service Fees.");
							error.setDescription("Please provide Service Fees.");
							addUserDTO.setError(error);
							addUserDTO.setStatus(0);
							return addUserDTO;
						}
						if(StringUtils.isNotEmpty(serviceFees)){
							try{
								Double.parseDouble(serviceFees);
							}catch(Exception e){
								System.err.println("Please provide valid Service Fees.");
								error.setDescription("Please provide valid Service Fees.");
								addUserDTO.setError(error);
								addUserDTO.setStatus(0);
								return addUserDTO;
							}
						}						
					}
					
					if(role.toUpperCase().equals("ROLE_AFFILIATES")){
						if(StringUtils.isEmpty(customerOrderDisc)){
							System.err.println("Please provide Customer Order Discount.");
							error.setDescription("Please provide Customer Order Discount.");
							addUserDTO.setError(error);
							addUserDTO.setStatus(0);
							return addUserDTO;
						}
						if(StringUtils.isNotEmpty(customerOrderDisc)){
							try{
								Double.parseDouble(customerOrderDisc);
							}catch(Exception e){
								System.err.println("Please provide valid Customer Order Discount.");
								error.setDescription("Please provide valid Customer Order Discount.");
								addUserDTO.setError(error);
								addUserDTO.setStatus(0);
								return addUserDTO;
							}
						}
						if(StringUtils.isEmpty(affiliateDisc)){
							System.err.println("Please provide Affiliates Discount.");
							error.setDescription("Please provide Affiliates Discount.");
							addUserDTO.setError(error);
							addUserDTO.setStatus(0);
							return addUserDTO;
						}
						if(StringUtils.isNotEmpty(affiliateDisc)){
							try{
								Double.parseDouble(affiliateDisc);
							}catch(Exception e){
								System.err.println("Please provide valid Affiliates Discount.");
								error.setDescription("Please provide valid Affiliates Discount.");
								addUserDTO.setError(error);
								addUserDTO.setStatus(0);
								return addUserDTO;
							}
						}
						if(StringUtils.isEmpty(phoneCustomerOrderDisc)){
							System.err.println("Please provide Customer Order Discount(Phone Orders).");
							error.setDescription("Please provide Customer Order Discount(Phone Orders).");
							addUserDTO.setError(error);
							addUserDTO.setStatus(0);
							return addUserDTO;
						}
						if(StringUtils.isNotEmpty(phoneCustomerOrderDisc)){
							try{
								Double.parseDouble(phoneCustomerOrderDisc);
							}catch(Exception e){
								System.err.println("Please provide valid Customer Order Discount(Phone Orders).");
								error.setDescription("Please provide valid Customer Order Discount(Phone Orders).");
								addUserDTO.setError(error);
								addUserDTO.setStatus(0);
								return addUserDTO;
							}
						}
						if(StringUtils.isEmpty(phoneAffiliateDisc)){
							System.err.println("Please provide Affiliates Discount(Phone Orders).");
							error.setDescription("Please provide Affiliates Discount(Phone Orders).");
							addUserDTO.setError(error);
							addUserDTO.setStatus(0);
							return addUserDTO;
						}
						if(StringUtils.isNotEmpty(phoneAffiliateDisc)){
							try{
								Double.parseDouble(phoneAffiliateDisc);
							}catch(Exception e){
								System.err.println("Please provide valid Affiliates Discount(Phone Orders).");
								error.setDescription("Please provide valid Affiliates Discount(Phone Orders).");
								addUserDTO.setError(error);
								addUserDTO.setStatus(0);
								return addUserDTO;
							}
						}
						if(StringUtils.isEmpty(fromDateStr)){
							System.err.println("Please add valid value in Effective from date.");
							error.setDescription("Please add valid value in Effective from date.");
							addUserDTO.setError(error);
							addUserDTO.setStatus(0);
							return addUserDTO;
						}
						if(StringUtils.isEmpty(fromDateStr)){
							System.err.println("Please add valid value in Effective to date.");
							error.setDescription("Please add valid value in Effective to date.");
							addUserDTO.setError(error);
							addUserDTO.setStatus(0);
							return addUserDTO;
						}						
						try {
							fromDateStr = fromDateStr + " 00:00:00";
							toDateStr = toDateStr + " 23:59:59";
							fromDate = Util.getDateWithTwentyFourHourFormat1(fromDateStr);
							toDate = Util.getDateWithTwentyFourHourFormat1(toDateStr);
						} catch (Exception e) {
							e.printStackTrace();
							error.setDescription("Invalid Effective from or to date format.");
							addUserDTO.setError(error);
							addUserDTO.setStatus(0);
							return addUserDTO;
						}						
					}
				}
				
				TrackerUser trackerUserDb = DAORegistry.getTrackerUserDAO().getTrackerUserByUsernameOrEmail(userName, email);				
				if(trackerUserDb != null){
					System.err.println("There is already an user with this Username or Email.");
					error.setDescription("There is already an user with this Username or Email.");
					addUserDTO.setError(error);
					addUserDTO.setStatus(0);
					return addUserDTO;
				}
				
				// adding user to database
				TrackerBrokers trackerBrokersDb = null;
				AffiliatePromoCodeHistory promoHistory = null;
				AffiliateSetting setting = null;
				
				trackerUser.setUserName(userName);
				trackerUser.setFirstName(firstName);
				trackerUser.setLastName(lastName);
				trackerUser.setEmail(email);
				trackerUser.setPassword(AdminController.encryptPwd(password));
				if(phone == null || phone.isEmpty()){
					trackerUser.setPhone(null);
				}else{
					trackerUser.setPhone(phone);
				}
				
				Set<Role> roleList = new HashSet<Role>();
				for(String role : roles){
					Role roleDb = DAORegistry.getRoleDAO().getRoleByName(role.toUpperCase());	
					
					if(role.toUpperCase().equals("ROLE_BROKER")){
						trackerBrokersDb = new TrackerBrokers();
						trackerBrokersDb.setCompanyName(companyName);
						trackerBrokersDb.setServiceFees(Double.parseDouble(serviceFees));
						DAORegistry.getTrackerBrokersDAO().save(trackerBrokersDb);
						
						trackerUser.setBroker(trackerBrokersDb);
					}
					
					if(role.toUpperCase().equals("ROLE_AFFILIATES")){
						if(promoCode != null && !promoCode.isEmpty()){
							trackerUser.setPromotionalCode(promoCode);
						}else{
							trackerUser.setPromotionalCode(Util.generateCustomerReferalCode());
						}
						promoHistory = new AffiliatePromoCodeHistory();
						promoHistory.setPromoCode(trackerUser.getPromotionalCode());
						promoHistory.setEffectiveFromDate(fromDate);
						promoHistory.setEffectiveToDate(toDate);
						promoHistory.setStatus("ACTIVE");
						promoHistory.setUpdateDate(new Date());
						promoHistory.setCreateDate(new Date());
						
						setting = new AffiliateSetting();
						setting.setCashDiscount((Double.parseDouble(affiliateDisc)/100));
						setting.setPhoneCashDiscount((Double.parseDouble(phoneAffiliateDisc)/100));
						setting.setCustomerDiscount((Double.parseDouble(customerOrderDisc)/100));
						setting.setPhoneCustomerDiscount((Double.parseDouble(phoneCustomerOrderDisc)/100));
						if(repeatBusiness==null || repeatBusiness.equalsIgnoreCase("NO")){
							setting.setIsRepeatBusiness(false);
						}else{
							setting.setIsRepeatBusiness(true);
						}
						setting.setIsEarnRewardPoints(true);
						if(canEarnRewardPoints==null || canEarnRewardPoints.equalsIgnoreCase("NO")){
							setting.setIsEarnRewardPoints(false);
						}
						setting.setStatus(status);
						setting.setLastUpdated(new Date());
						setting.setUpdatedBy(sessionUser);
					}
					roleList.add(roleDb);
				}
				trackerUser.setRoles(roleList);						
				trackerUser.setStatus(true);
				trackerUser.setCreateDate(new Date());
				trackerUser.setCreatedBy(sessionUser);
				DAORegistry.getTrackerUserDAO().save(trackerUser);
				
				if(promoHistory!=null){
					promoHistory.setUserId(trackerUser.getId());
					setting.setUserId(trackerUser.getId());
					DAORegistry.getAffiliatePromoCodeHistoryDAO().save(promoHistory);
					DAORegistry.getAffiliateSettingDAO().save(setting);
				}
				addUserDTO.setMessage("User added successfully.");
				
				//Tracking User Action
				String userActionMsg = "User Created Successfully.";
				Util.userActionAudit(request, trackerUser.getId(), userActionMsg);
				
				addUserDTO.setSetting(JsonWrapperUtil.getAffiliateInfoObject(promoHistory, setting));
				addUserDTO.setTrackerBrokers(trackerBrokersDb);
			}
			addUserDTO.setStatus(1);			
			addUserDTO.setTrackerUser(trackerUser);
			addUserDTO.setRoleAffiliateBroker(roleAffiliateBroker!=null?roleAffiliateBroker.toUpperCase():"");			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while Adding Broker/Affiliate");
			addUserDTO.setError(error);
			addUserDTO.setStatus(0);
		}
		return addUserDTO;
	}
	
	
	@RequestMapping({"/EditUsers"})
	public EditUsersDTO editUserPage(HttpServletRequest request, HttpServletResponse response, Model model){
		EditUsersDTO editUsersDTO = new EditUsersDTO();
		Error error = new Error();
		model.addAttribute("editUsersDTO", editUsersDTO);
		
		try{
			Gson gson = GsonCustomConfig.getGsonBuilder();
			String returnMessage = "";
			String userIdStr = request.getParameter("userId");
			String action = request.getParameter("action");
			
			if(StringUtils.isEmpty(userIdStr)){
				System.err.println("Please select User.");
				error.setDescription("Please select User.");
				editUsersDTO.setError(error);
				editUsersDTO.setStatus(0);
				return editUsersDTO;
			}
			
			Integer userId = Integer.parseInt(userIdStr);
			TrackerUser trackerUserDb = DAORegistry.getTrackerUserDAO().get(userId);
			if(trackerUserDb == null){
				System.err.println("Selected User not found.");
				error.setDescription("Selected User not found.");
				editUsersDTO.setError(error);
				editUsersDTO.setStatus(0);
				return editUsersDTO;				
			}
			
			String userName = trackerUserDb.getUserName();
			TrackerUser trackerUser = new TrackerUser();			
			
			if(action != null && action.equals("editUserInfo")){
				String roless = request.getParameter("role");
				String firstName = request.getParameter("firstName");
				String lastName = request.getParameter("lastName");
				String email = request.getParameter("email");
				String phone = request.getParameter("phone");
				String companyName = request.getParameter("companyName");
				String serviceFees = request.getParameter("serviceFees");
				String affiliateDisc = request.getParameter("affiliateDisc");
				String customerOrderDisc = request.getParameter("customerOrderDisc");
				String status = request.getParameter("affiliateStatus");
				String repeatBusiness = request.getParameter("repeatBusiness");				
				String phoneAffiliateDisc = request.getParameter("phoneAffiliateDiscount");
				String phoneCustomerOrderDisc = request.getParameter("phoneCustomerOrderDiscount");
				String canEarnRewardPoints = request.getParameter("canEarnRewardPoints");
				String sessionUser = request.getParameter("sessionUser");
				
				if(StringUtils.isEmpty(firstName)){
					System.err.println("Please provide First Name.");
					error.setDescription("Please provide First Name.");
					editUsersDTO.setError(error);
					editUsersDTO.setStatus(0);
					return editUsersDTO;
				}
				if(StringUtils.isEmpty(lastName)){
					System.err.println("Please provide Last Name.");
					error.setDescription("Please provide Last Name.");
					editUsersDTO.setError(error);
					editUsersDTO.setStatus(0);
					return editUsersDTO;
				}
				if(StringUtils.isEmpty(email)){
					System.err.println("Please provide Email.");
					error.setDescription("Please provide Email.");
					editUsersDTO.setError(error);
					editUsersDTO.setStatus(0);
					return editUsersDTO;
				}
				if(StringUtils.isEmpty(roless)){
					System.err.println("Please select Roles.");
					error.setDescription("Please select Roles.");
					editUsersDTO.setError(error);
					editUsersDTO.setStatus(0);
					return editUsersDTO;
				}
				
				String roles[] = null;
				if(StringUtils.isNotEmpty(roless)){
					roles = gson.fromJson(roless, new TypeToken<String[]>(){}.getType());
				}
				
				for(String role : roles){
					if(role.toUpperCase().equals("ROLE_BROKER")){
						if(StringUtils.isEmpty(companyName)){
							System.err.println("Please provide Company Name.");
							error.setDescription("Please provide Company Name.");
							editUsersDTO.setError(error);
							editUsersDTO.setStatus(0);
							return editUsersDTO;
						}
						if(StringUtils.isEmpty(serviceFees)){
							System.err.println("Please provide Service Fees.");
							error.setDescription("Please provide Service Fees.");
							editUsersDTO.setError(error);
							editUsersDTO.setStatus(0);
							return editUsersDTO;
						}
						if(StringUtils.isNotEmpty(serviceFees)){
							try{
								Double.parseDouble(serviceFees);
							}catch(Exception e){
								System.err.println("Please provide valid Service Fees.");
								error.setDescription("Please provide valid Service Fees.");
								editUsersDTO.setError(error);
								editUsersDTO.setStatus(0);
								return editUsersDTO;
							}
						}
					}
					if(role.toUpperCase().equals("ROLE_AFFILIATES")){
						if(StringUtils.isEmpty(customerOrderDisc)){
							System.err.println("Please provide Customer Order Discount.");
							error.setDescription("Please provide Customer Order Discount.");
							editUsersDTO.setError(error);
							editUsersDTO.setStatus(0);
							return editUsersDTO;
						}
						if(StringUtils.isNotEmpty(customerOrderDisc)){
							try{
								Double.parseDouble(customerOrderDisc);
							}catch(Exception e){
								System.err.println("Please provide valid Customer Order Discount.");
								error.setDescription("Please provide valid Customer Order Discount.");
								editUsersDTO.setError(error);
								editUsersDTO.setStatus(0);
								return editUsersDTO;
							}
						}
						if(StringUtils.isEmpty(affiliateDisc)){
							System.err.println("Please provide Affiliates Discount.");
							error.setDescription("Please provide Affiliates Discount.");
							editUsersDTO.setError(error);
							editUsersDTO.setStatus(0);
							return editUsersDTO;
						}
						if(StringUtils.isNotEmpty(affiliateDisc)){
							try{
								Double.parseDouble(affiliateDisc);
							}catch(Exception e){
								System.err.println("Please provide valid Affiliates Discount.");
								error.setDescription("Please provide valid Affiliates Discount.");
								editUsersDTO.setError(error);
								editUsersDTO.setStatus(0);
								return editUsersDTO;
							}
						}
						if(StringUtils.isEmpty(phoneCustomerOrderDisc)){
							System.err.println("Please provide Customer Order Discount(Phone Orders).");
							error.setDescription("Please provide Customer Order Discount(Phone Orders).");
							editUsersDTO.setError(error);
							editUsersDTO.setStatus(0);
							return editUsersDTO;
						}
						if(StringUtils.isNotEmpty(phoneCustomerOrderDisc)){
							try{
								Double.parseDouble(phoneCustomerOrderDisc);
							}catch(Exception e){
								System.err.println("Please provide valid Customer Order Discount(Phone Orders).");
								error.setDescription("Please provide valid Customer Order Discount(Phone Orders).");
								editUsersDTO.setError(error);
								editUsersDTO.setStatus(0);
								return editUsersDTO;
							}
						}
						if(StringUtils.isEmpty(phoneAffiliateDisc)){
							System.err.println("Please provide Affiliates Discount(Phone Orders).");
							error.setDescription("Please provide Affiliates Discount(Phone Orders).");
							editUsersDTO.setError(error);
							editUsersDTO.setStatus(0);
							return editUsersDTO;
						}
						if(StringUtils.isNotEmpty(phoneAffiliateDisc)){
							try{
								Double.parseDouble(phoneAffiliateDisc);
							}catch(Exception e){
								System.err.println("Please provide valid Affiliates Discount(Phone Orders).");
								error.setDescription("Please provide valid Affiliates Discount(Phone Orders).");
								editUsersDTO.setError(error);
								editUsersDTO.setStatus(0);
								return editUsersDTO;
							}
						}
					}
				}
				
				trackerUser.setId(trackerUserDb.getId());
				trackerUser.setUserName(trackerUserDb.getUserName());
				trackerUser.setFirstName(firstName);
				trackerUser.setLastName(lastName);
				trackerUser.setEmail(email);
				trackerUser.setPhone(phone);
				trackerUser.setRoles(trackerUserDb.getRoles());
				trackerUser.setPassword(trackerUserDb.getPassword());
				trackerUser.setStatus(trackerUserDb.getStatus());	
				trackerUser.setCreateDate(trackerUserDb.getCreateDate());
				trackerUser.setCreatedBy(trackerUserDb.getCreatedBy());
				
				TrackerBrokers trackerBrokersDb = null;
				Boolean roleFlag = false;
				Boolean roleAffiliateFlag = false;
				AffiliateSetting setting = null;				
				AffiliatePromoCodeHistory affiliatePromoHistory = null;
				
				Set<Role> roleList = new HashSet<Role>();
				for(String role : roles){
					Role roleDb = DAORegistry.getRoleDAO().getRoleByName(role.toUpperCase());
					
					if(role.toUpperCase().equals("ROLE_BROKER")){
						if(trackerUserDb.getBroker() == null){
							trackerBrokersDb = new TrackerBrokers();
							trackerBrokersDb.setCompanyName(companyName);
							trackerBrokersDb.setServiceFees(Double.parseDouble(serviceFees));
							DAORegistry.getTrackerBrokersDAO().save(trackerBrokersDb);
							
							trackerUser.setBroker(trackerBrokersDb);
							//trackerUser.setBrokerId(trackerBrokersDb.getId());
						}
						if(trackerUserDb.getBroker() != null){
							trackerBrokersDb = DAORegistry.getTrackerBrokersDAO().get(trackerUserDb.getBroker().getId());
							if(trackerBrokersDb != null){
								trackerBrokersDb.setServiceFees(Double.parseDouble(serviceFees));
								DAORegistry.getTrackerBrokersDAO().update(trackerBrokersDb);
							}
							
							trackerUser.setBroker(trackerUserDb.getBroker());
						}
						roleFlag = true;
					}
					
					if(role.toUpperCase().equals("ROLE_AFFILIATES")){
						if(trackerUserDb.getPromotionalCode() == null || trackerUserDb.getPromotionalCode().isEmpty()){
							if(trackerUserDb.getStatus() != null && trackerUserDb.getStatus()){
								trackerUser.setPromotionalCode(null);
								String promoCode = Util.generateCustomerReferalCode();
								trackerUser.setPromotionalCode(promoCode);
								
								affiliatePromoHistory = new AffiliatePromoCodeHistory();
								affiliatePromoHistory.setUserId(trackerUser.getId());
								affiliatePromoHistory.setPromoCode(promoCode);
								affiliatePromoHistory.setCreateDate(new Date());
								affiliatePromoHistory.setUpdateDate(new Date());
								affiliatePromoHistory.setStatus("ACTIVE");
								affiliatePromoHistory.setEffectiveFromDate(new Date());
								Date after1Day = new Date();
								after1Day.setDate(new Date().getDate()+1);
								affiliatePromoHistory.setEffectiveToDate(after1Day);
							}								
						}
						if(trackerUserDb.getPromotionalCode() != null){				
							trackerUser.setPromotionalCode(trackerUserDb.getPromotionalCode());								
						}

						setting = DAORegistry.getAffiliateSettingDAO().getSettingByUser(trackerUser.getId());
						if(setting == null){
							setting = new AffiliateSetting();
							setting.setUserId(trackerUser.getId());
						}
						setting.setCashDiscount((Double.parseDouble(affiliateDisc)/100));
						setting.setCustomerDiscount((Double.parseDouble(customerOrderDisc)/100));
						setting.setPhoneCashDiscount((Double.parseDouble(phoneAffiliateDisc)/100));
						setting.setPhoneCustomerDiscount((Double.parseDouble(phoneCustomerOrderDisc)/100));
						if(repeatBusiness==null || repeatBusiness.equalsIgnoreCase("NO")){
							setting.setIsRepeatBusiness(false);
						}else{
							setting.setIsRepeatBusiness(true);
						}
						setting.setIsEarnRewardPoints(true);
						if(canEarnRewardPoints==null || canEarnRewardPoints.equalsIgnoreCase("NO")){
							setting.setIsEarnRewardPoints(false);
						}
						setting.setStatus(status);
						setting.setLastUpdated(new Date());
						setting.setUpdatedBy(sessionUser);
						
						roleAffiliateFlag = true;
					}
					roleList.add(roleDb);
				}
				trackerUser.setRoles(roleList);
				
				//Delete if Role_Broker is not available when edit.
				if(!roleFlag){
					if(trackerUserDb.getBroker() != null){
						DAORegistry.getTrackerBrokersDAO().delete(trackerUserDb.getBroker());							
					}
				}
				if(!roleAffiliateFlag){
					if(trackerUserDb.getPromotionalCode() != null){
						
						affiliatePromoHistory = DAORegistry.getAffiliatePromoCodeHistoryDAO().getActiveAffiliatePromoCode(trackerUser.getId());
						if(null != affiliatePromoHistory ){
							affiliatePromoHistory.setStatus("DELETED");
							affiliatePromoHistory.setUpdateDate(new Date());
						}
						trackerUser.setPromotionalCode(null);
					}
				}
				DAORegistry.getTrackerUserDAO().update(trackerUser);
				
				if(null != affiliatePromoHistory){
					DAORegistry.getAffiliatePromoCodeHistoryDAO().saveOrUpdate(affiliatePromoHistory);					
				}
				if(null != setting){
					DAORegistry.getAffiliateSettingDAO().saveOrUpdate(setting);
				}
				
				trackerUserDb = DAORegistry.getTrackerUserDAO().get(userId);
				editUsersDTO.setMessage("User's details updated successfully.");
				
				//returnMessage = "User's details updated successfully.";				
				//return "redirect:EditUser?userId="+userIdStr;				
				
				//Tracking User Action
				String userActionMsg = "User Detail(s) Updated.";
				com.rtw.tmat.utils.Util.userActionAudit(request, Integer.parseInt(userIdStr), userActionMsg);
				
			}else if(action != null && action.equals("changePassword")){
				String password = request.getParameter("password");
				String rePassword = request.getParameter("repassword");
				
				if(StringUtils.isEmpty(password)){
					System.err.println("Please provide Password.");
					error.setDescription("Please provide Password.");
					editUsersDTO.setError(error);
					editUsersDTO.setStatus(0);
					return editUsersDTO;
				}
				if(StringUtils.isEmpty(rePassword)){
					System.err.println("Please provide Re-Password.");
					error.setDescription("Please provide Re-Password..");
					editUsersDTO.setError(error);
					editUsersDTO.setStatus(0);
					return editUsersDTO;
				}
				if(!password.equals(rePassword)){
					System.err.println("Password and Re-Password Must match.");
					error.setDescription("Password and Re-Password Must match.");
					editUsersDTO.setError(error);
					editUsersDTO.setStatus(0);
					return editUsersDTO;
				}
				
				trackerUserDb.setAndEncryptPassword(password);
				DAORegistry.getTrackerUserDAO().saveOrUpdate(trackerUserDb);
				
				trackerUserDb = DAORegistry.getTrackerUserDAO().get(userId);
				editUsersDTO.setMessage("User's password changed successfully.");				
				//returnMessage = "User's password changed successfully.";				
				//Tracking User Action
				String userActionMsg = "User Password Changed.";
				com.rtw.tmat.utils.Util.userActionAudit(request, Integer.parseInt(userIdStr), userActionMsg);				
			}

			ArrayList<String> rolesList = new ArrayList<String>();
			if(trackerUserDb.getRoles() != null && !trackerUserDb.getRoles().isEmpty()){
				for(Role role : trackerUserDb.getRoles()){
					rolesList.add(role.getName());
				}
			}
			
			TrackerBrokers trackerBrokersDb = null;			
			TrackerBrokerCustomDTO trackerBrokerCustomDTO = null;
			if(trackerUserDb.getBroker() != null){
				trackerBrokersDb = DAORegistry.getTrackerBrokersDAO().get(trackerUserDb.getBroker().getId());
				trackerBrokerCustomDTO = com.rtw.tracker.utils.Util.getTrackerBrokerObject(trackerBrokersDb);				
			}
			
			EditTrackerUserCustomDTO editTrackerUserCustomDTO = null;
			if(trackerUserDb != null){
				editTrackerUserCustomDTO = com.rtw.tracker.utils.Util.getEditUsersObject(trackerUserDb);
			}
			
			AffiliateSettingsCustomDTO affiliateSettingCustomDTO = null;
			AffiliateSetting affiliateSetting = null;
			if(trackerUserDb != null){
				for(String role : rolesList){
					if(role.toUpperCase().equals("ROLE_AFFILIATES")){
						affiliateSetting = DAORegistry.getAffiliateSettingDAO().getSettingByUser(trackerUserDb.getId());
						affiliateSettingCustomDTO = com.rtw.tracker.utils.Util.getAffiliateObject(affiliateSetting);
					}
				}
			}
			
			editUsersDTO.setStatus(1);			
			editUsersDTO.setRolesList(rolesList);
			editUsersDTO.setTrackerBrokerCustomDTO(trackerBrokerCustomDTO);
			editUsersDTO.setEditTrackerUserCustomDTO(editTrackerUserCustomDTO);
			editUsersDTO.setAffiliateSettingCustomDTO(affiliateSettingCustomDTO);			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while edit/updating User Information");
			editUsersDTO.setError(error);
			editUsersDTO.setStatus(0);
		}
		return editUsersDTO;
	}
	
	
	/*@RequestMapping({"/AuditUsers"})
	public AuditUsersDTO auditUserPage(HttpServletRequest request, HttpServletResponse response, Model model){
		AuditUsersDTO auditUsersDTO = new AuditUsersDTO();
		Error error = new Error();
		model.addAttribute("auditUsersDTO", auditUsersDTO);
		
		try{
			String userIdStr = request.getParameter("userId");
			String action = request.getParameter("action");
			
			if(StringUtils.isEmpty(userIdStr)){
				System.err.println("Please select User.");
				error.setDescription("Please select User.");
				auditUsersDTO.setError(error);
				auditUsersDTO.setStatus(0);
				return auditUsersDTO;
			}
			
			//String msg = "";
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			GridHeaderFilters filter = new GridHeaderFilters();
			Collection<UserAction> userActionList = null;			
			Collection<TrackerUser> trackerUserList = null;
			
			if(action != null && action.equals("action")){
				String fromDate = request.getParameter("fromDate");
				String toDate = request.getParameter("toDate");
				
				if(StringUtils.isEmpty(fromDate)){
					System.err.println("Please provide From Date.");
					error.setDescription("Please provide From Date.");
					auditUsersDTO.setError(error);
					auditUsersDTO.setStatus(0);
					return auditUsersDTO;
				}
				if(StringUtils.isEmpty(toDate)){
					System.err.println("Please provide To Date.");
					error.setDescription("Please provide To Date.");
					auditUsersDTO.setError(error);
					auditUsersDTO.setStatus(0);
					return auditUsersDTO;
				}
				
				userActionList = DAORegistry.getUserActionDAO()
						.getAllActionsByUserIdAndDateRange(Integer.parseInt(userIdStr), new Date(fromDate + " 00:00"), new Date(toDate + " 23:59"));
			}else{
				userActionList = DAORegistry.getUserActionDAO()
						.getAllActionsByUserIdAndDateRange(Integer.parseInt(userIdStr), new Date(dateFormat.format(new Date()) + " 00:00"),
								new Date(dateFormat.format(new Date()) + " 23:59"));
			}
						
			trackerUserList = DAORegistry.getTrackerUserDAO().getAllTrackerUser(filter,null,null);
			
			if(userActionList == null || userActionList.isEmpty()){
				auditUsersDTO.setMessage("No User Action found for Audit");
			}			
			auditUsersDTO.setStatus(1);			
			auditUsersDTO.setTrackerUserList(trackerUserList);
			auditUsersDTO.setUserActionList(userActionList);
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching User for Audit");
			auditUsersDTO.setError(error);
			auditUsersDTO.setStatus(0);
		}		
		return auditUsersDTO;
	}*/
	
	@RequestMapping(value = "/UserStatusChange")
	public GenericResponseDTO updateUserStatus(HttpServletRequest request, HttpServletResponse response, Model model){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		model.addAttribute("genericResponseDTO", genericResponseDTO);
		
		try{		
			String userIdStr = request.getParameter("userId");
			String status = request.getParameter("status");
			String sessionUser = request.getParameter("sessionUser");
			
			if(StringUtils.isEmpty(sessionUser)){
				System.err.println("Please Login.");
				error.setDescription("Please Login.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(userIdStr)){
				System.err.println("Please select User.");
				error.setDescription("Please select User.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(status)){
				System.err.println("Please provide Status.");
				error.setDescription("Please provide Status.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			TrackerUser trackerUser = DAORegistry.getTrackerUserDAO().get(Integer.parseInt(userIdStr));			
			if(trackerUser == null){
				System.err.println("Selected User not found.");
				error.setDescription("Selected User not found.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			Set<Role> list = trackerUser.getRoles();
			boolean isAffiliate = false;
			for (Role role : list) {
				if(role.getName().equalsIgnoreCase("ROLE_AFFILIATES")){
					isAffiliate = true;
					break;
				}
			}
			
			AffiliatePromoCodeHistory affiliatePromoHistory = null;
			
			if(status != null && status.equalsIgnoreCase("ACTIVE")){
				if(isAffiliate && (trackerUser.getPromotionalCode() == null || trackerUser.getPromotionalCode().isEmpty())){
					String fromDateStr = request.getParameter("fromDate");
					String toDateStr = request.getParameter("toDate");
					String promoCode = request.getParameter("promoCode");
					fromDateStr = fromDateStr+" 00:00:00";
					toDateStr = toDateStr+" 23:59:59";
					SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
					Date fromDate = dateFormat.parse(fromDateStr);
					Date toDate = dateFormat.parse(toDateStr);
					
					if(promoCode == null || promoCode.isEmpty()){
						promoCode = Util.generateCustomerReferalCode();
					}
					
					//Affiliate Promo Code History
					affiliatePromoHistory = new AffiliatePromoCodeHistory();
					affiliatePromoHistory.setUserId(trackerUser.getId());
					affiliatePromoHistory.setPromoCode(promoCode);
					affiliatePromoHistory.setCreateDate(new Date());
					affiliatePromoHistory.setUpdateDate(new Date());
					affiliatePromoHistory.setStatus("ACTIVE");
					affiliatePromoHistory.setEffectiveFromDate(fromDate);
					affiliatePromoHistory.setEffectiveToDate(toDate);
					
					trackerUser.setPromotionalCode(promoCode);
				}
				trackerUser.setStatus(true);
			}else{
				if(isAffiliate){
					DAORegistry.getAffiliatePromoCodeHistoryDAO().updateAllActiveHistoryByUserId(trackerUser.getId());
					trackerUser.setPromotionalCode(null);
				}
				trackerUser.setStatus(false);
			}	
			
			DAORegistry.getTrackerUserDAO().update(trackerUser);
			
			if(null != affiliatePromoHistory){
				DAORegistry.getAffiliatePromoCodeHistoryDAO().saveOrUpdate(affiliatePromoHistory);
			}
			/*if(StringUtils.isEmpty(msg))
				msg = "true";*/
			
			genericResponseDTO.setMessage("true");
			genericResponseDTO.setStatus(1);
			
			//Tracking User Action
			String userActionMsg = "User Status is Updated to "+status;
			Util.userActionAudit(request, Integer.parseInt(userIdStr), userActionMsg);
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while Updating User Status.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
	
	@RequestMapping(value = "/GenerateDiscountCode")
	public GenerateDiscountCodeDTO generateDiscountCode(HttpServletRequest request, HttpServletResponse response){
		GenerateDiscountCodeDTO generateDiscountCodeDTO = new GenerateDiscountCodeDTO();
		Error error = new Error();
		
		try {
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String sortingString = request.getParameter("sortingString");
			
			GridHeaderFilters filter = null;
			try {
				filter = GridHeaderFiltersUtil.getRTFPromotionalOfferFilter(headerFilter+sortingString);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				generateDiscountCodeDTO.setError(error);
				generateDiscountCodeDTO.setStatus(0);
				return generateDiscountCodeDTO;
			}
			
			List<RTFPromoOffers> list = null;
			Integer count = 0;
			
			list = DAORegistry.getQueryManagerDAO().getDiscountCodes(null,filter,pageNo);
			if(list != null && list.size() > 0){
				count = DAORegistry.getQueryManagerDAO().getDiscountCodesCount(null,filter);
			}else{
				generateDiscountCodeDTO.setMessage("No Promotional Offer found for Selected search filter");
			}
			
			generateDiscountCodeDTO.setPromoOfferDetailPaginationDTO(PaginationUtil.getDummyPaginationParameters(0));
			generateDiscountCodeDTO.setPromoOfferDTO(com.rtw.tracker.utils.Util.getPromotionalCodeArray(list));
			
			generateDiscountCodeDTO.setDiscountCodePaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
			generateDiscountCodeDTO.setStatus(1);
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Generate Discount Code");
			generateDiscountCodeDTO.setError(error);
			generateDiscountCodeDTO.setStatus(0);
		}
		return generateDiscountCodeDTO;
	}
	
	/*@RequestMapping(value = "/GetGenerateDiscountCode")
	public GenerateDiscountCodeDTO getGenerateDiscountCode(HttpServletRequest request, HttpServletResponse response){
		GenerateDiscountCodeDTO generateDiscountCodeDTO = new GenerateDiscountCodeDTO();
		Error error = new Error();
		
		try {
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			if(StringUtils.isEmpty(pageNo)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				generateDiscountCodeDTO.setError(error);
				generateDiscountCodeDTO.setStatus(0);
				return generateDiscountCodeDTO;
			}
			try {
				Integer.parseInt(pageNo);
			} catch (Exception e) {
				System.err.println("Please provide valid PageNo.");
				error.setDescription("Please provide valid PageNo.");
				generateDiscountCodeDTO.setError(error);
				generateDiscountCodeDTO.setStatus(0);
				return generateDiscountCodeDTO;
			}
			
			GridHeaderFilters filter = null;
			try {
				filter = GridHeaderFiltersUtil.getRTFPromotionalOfferFilter(headerFilter);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				generateDiscountCodeDTO.setError(error);
				generateDiscountCodeDTO.setStatus(0);
				return generateDiscountCodeDTO;
			}
			
			List<RTFPromoOffers> list = null;
			Integer count = 0;
			
			list = DAORegistry.getQueryManagerDAO().getDiscountCodes(null,filter);
			if(list != null && list.size() > 0){
				count = list.size();
			}else{
				generateDiscountCodeDTO.setMessage("No Promotional Offer found for Selected search filter");
			}
			
			generateDiscountCodeDTO.setDiscountCodePaginationDTO(PaginationUtil.getDummyPaginationParameters(count));			
			generateDiscountCodeDTO.setPromoOfferDTO(com.rtw.tracker.utils.Util.getPromotionalCodeArray(list));
			generateDiscountCodeDTO.setStatus(1);
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Applying Search - Generate Discount Code");
			generateDiscountCodeDTO.setError(error);
			generateDiscountCodeDTO.setStatus(0);
		}
		return generateDiscountCodeDTO;
	}*/
	
	@RequestMapping(value = "/UpdateDiscountCodeStatus")
	public GenericResponseDTO updateDiscountCodeStatus(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		
		try {
			String idStr = request.getParameter("ids");
			String status = request.getParameter("status");
			String userName = request.getParameter("userName");
			
			if(StringUtils.isEmpty(idStr)){
				System.err.println("Please select Promotional Offer.");
				error.setDescription("Please select Promotional Offer.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(status)){
				System.err.println("Please provide Status.");
				error.setDescription("Please provide Status.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
						
			String userActionMsg = "";			
			if(status != null && status.equalsIgnoreCase("ENABLED")){
				if(idStr.contains(",")){
					String str[] = idStr.split(",");
					for(String s : str){
						if(s.trim().isEmpty()){
							continue;
						}
						RTFPromoOffers offer = DAORegistry.getRtfPromoOffersDAO().get(Integer.parseInt(s));
						
						RTFPromoOffers promoOffer = DAORegistry.getRtfPromoOffersDAO().getPromoCode(offer.getPromoCode());
						if(promoOffer != null){
							System.err.println("Promotional Code is already exist can't enable this.");
							error.setDescription("Promotional Code is already exist can't enable this.");
							genericResponseDTO.setError(error);
							genericResponseDTO.setStatus(0);
							return genericResponseDTO;
						}
						
						offer.setStatus("ENABLED");
						offer.setModifiedBy(userName);
						offer.setModifiedDate(new Date());
						DAORegistry.getRtfPromoOffersDAO().update(offer);
					}
				}else{
					RTFPromoOffers offer = DAORegistry.getRtfPromoOffersDAO().get(Integer.parseInt(idStr));
					
					RTFPromoOffers promoOffer = DAORegistry.getRtfPromoOffersDAO().getPromoCode(offer.getPromoCode());
					if(promoOffer != null){
						System.err.println("Promotional Code is already exist can't enable this.");
						error.setDescription("Promotional Code is already exist can't enable this.");
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}
					
					offer.setStatus("ENABLED");
					offer.setModifiedBy(userName);
					offer.setModifiedDate(new Date());
					DAORegistry.getRtfPromoOffersDAO().update(offer);
				}
				genericResponseDTO.setStatus(1);
				genericResponseDTO.setMessage("Discount Code offer Enabled successfully.");
				
				//Tracking User Action
				if(idStr.contains(",")){
					userActionMsg = "Discount Code Offer Enabled Successfully. Offer Id's - "+idStr;
					Util.userActionAudit(request, null, userActionMsg);
				}else{
					userActionMsg = "Discount Code Offer Enabled Successfully.";
					Util.userActionAudit(request, Integer.parseInt(idStr), userActionMsg);
				}
			}else if(status != null && status.equalsIgnoreCase("DISABLED")){
				if(idStr.contains(",")){
					String str[] = idStr.split(",");
					for(String s : str){
						if(s.trim().isEmpty()){
							continue;
						}
						RTFPromoOffers offer = DAORegistry.getRtfPromoOffersDAO().get(Integer.parseInt(s));
						offer.setStatus("DISABLED");
						offer.setModifiedBy(userName);
						offer.setModifiedDate(new Date());
						DAORegistry.getRtfPromoOffersDAO().update(offer);
						
						//Contest - Promotional Expiry Date Update
						Contests contest = QuizDAORegistry.getContestsDAO().getContestByPromoOfferId(Integer.parseInt(s));
						contest.setPromoExpiryDate(new Date());
						contest.setUpdatedBy(userName);
						contest.setUpdatedDate(new Date());
						QuizDAORegistry.getContestsDAO().update(contest);
					}
				}else{
					RTFPromoOffers offer = DAORegistry.getRtfPromoOffersDAO().get(Integer.parseInt(idStr));
					offer.setStatus("DISABLED");
					offer.setModifiedBy(userName);
					offer.setModifiedDate(new Date());
					DAORegistry.getRtfPromoOffersDAO().update(offer);
					
					//Contest - Promotional Expiry Date Update
					Contests contest = QuizDAORegistry.getContestsDAO().getContestByPromoOfferId(Integer.parseInt(idStr));
					if(contest!=null){
						contest.setPromoExpiryDate(new Date());
						contest.setUpdatedBy(userName);
						contest.setUpdatedDate(new Date());
						QuizDAORegistry.getContestsDAO().update(contest);
					}
				}
				genericResponseDTO.setStatus(1);
				genericResponseDTO.setMessage("Discount Code offer Disabled successfully.");
				
				//Tracking User Action
				if(idStr.contains(",")){
					userActionMsg = "Discount Code Offer Disabled Successfully. Offer Id's - "+idStr;
					Util.userActionAudit(request, null, userActionMsg);
				}else{
					userActionMsg = "Discount Code Offer Disabled Successfully.";
					Util.userActionAudit(request, Integer.parseInt(idStr), userActionMsg);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Updating Status Discount Code");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
		
	@RequestMapping(value = "/SaveDiscountCode")
	public SaveDiscountCodeDTO saveDiscountCode(HttpServletRequest request, HttpServletResponse response){
		SaveDiscountCodeDTO saveDiscountCodeDTO = new SaveDiscountCodeDTO();
		Error error = new Error();
		
		try {			
			String autoGenerate = request.getParameter("promoAutoGenerate");
			String promotionalCode = request.getParameter("promoCode");
			String flatDiscount = request.getParameter("promoFlatDiscount");
			String discountPerc = request.getParameter("promoDiscountPerc");
			String mobileDiscountPerc = request.getParameter("promoMobileDiscountPerc");
			String orderThreshold = request.getParameter("promoOrderThreshold");
			String fromDate = request.getParameter("promoFromDate");
			String toDate = request.getParameter("promoToDate");
			String maxOrders = request.getParameter("promoMaxOrders");
			String countPromoTypes = request.getParameter("count");
			String userName = request.getParameter("userName");
			
			if(StringUtils.isEmpty(autoGenerate)){
				System.err.println("Please select Auto Generate Option.");
				error.setDescription("Please select Auto Generate Option.");
				saveDiscountCodeDTO.setError(error);
				saveDiscountCodeDTO.setStatus(0);
				return saveDiscountCodeDTO;
			}
			if(autoGenerate.equalsIgnoreCase("No")){
				if(StringUtils.isEmpty(promotionalCode)){
					System.err.println("Promotional Code is not found.");
					error.setDescription("Promotional Code is not found.");
					saveDiscountCodeDTO.setError(error);
					saveDiscountCodeDTO.setStatus(0);
					return saveDiscountCodeDTO;
				}else{
					RTFPromoOffers promoOffer = DAORegistry.getRtfPromoOffersDAO().getPromoCode(promotionalCode);
					if(promoOffer != null){
						System.err.println("Promotional Code is already exist.");
						error.setDescription("Promotional Code is already exist.");
						saveDiscountCodeDTO.setError(error);
						saveDiscountCodeDTO.setStatus(0);
						return saveDiscountCodeDTO;
					}
				}
			}
			
			String eventId = "";
			String parentId = "";
			String childId = "";
			String grandChildId = "";
			String artistId = "";
			String venueId = "";		
			String promoTypeId = "";
			String selectedString = "";
			int count = 0;
			for(int i=0; i < Integer.parseInt(countPromoTypes); i++){
				eventId = request.getParameter("eventId_"+i);
				parentId = request.getParameter("parentId_"+i);
				childId = request.getParameter("childId_"+i);
				grandChildId = request.getParameter("grandChildId_"+i);
				artistId = request.getParameter("artistId_"+i);
				venueId = request.getParameter("venueId_"+i);		
				promoTypeId = request.getParameter("promoTypeId_"+i);
				if((promoTypeId == null || promoTypeId.isEmpty()) && (parentId==null || parentId.isEmpty()) && (childId==null || childId.isEmpty())
						&& (grandChildId==null || grandChildId.isEmpty()) && (artistId==null || artistId.isEmpty())
						&& (venueId==null || venueId.isEmpty()) && (eventId==null || eventId.isEmpty())){
					count++;
				}
			}
			if(count == Integer.parseInt(countPromoTypes)){
				System.err.println("Id not found for one of following Artist,Event,venue,parent,child,grand child.");
				error.setDescription("Id not found for one of following Artist,Event,venue,parent,child,grand child.");
				saveDiscountCodeDTO.setError(error);
				saveDiscountCodeDTO.setStatus(0);
				return saveDiscountCodeDTO;
			}
			
			if(StringUtils.isEmpty(flatDiscount)){
				System.err.println("Please select Flat Discount Option.");
				error.setDescription("Please select Flat Discount Option.");
				saveDiscountCodeDTO.setError(error);
				saveDiscountCodeDTO.setStatus(0);
				return saveDiscountCodeDTO;
			}
			if(flatDiscount.equalsIgnoreCase("Yes")){
				if(StringUtils.isEmpty(orderThreshold)){
					System.err.println("Order Threshold is not found.");
					error.setDescription("Order Threshold is not found.");
					saveDiscountCodeDTO.setError(error);
					saveDiscountCodeDTO.setStatus(0);
					return saveDiscountCodeDTO;
				}
			}
			
			if(StringUtils.isEmpty(discountPerc)){
				System.err.println("Discount percentage is not found.");
				error.setDescription("Discount percentage is not found.");
				saveDiscountCodeDTO.setError(error);
				saveDiscountCodeDTO.setStatus(0);
				return saveDiscountCodeDTO;
			}
			
			if(StringUtils.isEmpty(mobileDiscountPerc)){
				System.err.println("Mobile Discount percentage is not found.");
				error.setDescription("Mobile Discount percentage is not found.");
				saveDiscountCodeDTO.setError(error);
				saveDiscountCodeDTO.setStatus(0);
				return saveDiscountCodeDTO;
			}
			
			if(StringUtils.isEmpty(fromDate) || StringUtils.isEmpty(toDate)){
				System.err.println("Missing from Date or To Date.");
				error.setDescription("Missing from Date or To Date.");
				saveDiscountCodeDTO.setError(error);
				saveDiscountCodeDTO.setStatus(0);
				return saveDiscountCodeDTO;
			}
			
			if(StringUtils.isEmpty(maxOrders)){
				System.err.println("Max. Orders is not found.");
				error.setDescription("Max. Orders is not found.");
				saveDiscountCodeDTO.setError(error);
				saveDiscountCodeDTO.setStatus(0);
				return saveDiscountCodeDTO;
			}
			
			fromDate = fromDate + " 00:00:00";
			toDate = toDate + " 23:59:59";
			Date fDate = null;
			Date tDate = null;
			try {
				fDate = Util.getDateWithTwentyFourHourFormat1(fromDate);
				tDate = Util.getDateWithTwentyFourHourFormat1(toDate);
			} catch (Exception e) {
				e.printStackTrace();				
				error.setDescription("Start Date or End Date with bad values.");
				saveDiscountCodeDTO.setError(error);
				saveDiscountCodeDTO.setStatus(0);
				return saveDiscountCodeDTO;
			}
			
			for(int i=0; i < Integer.parseInt(countPromoTypes); i++){
				selectedString = request.getParameter("artistVenueName_"+i);
				if(selectedString != null && !selectedString.isEmpty()){
					break;
				}
			}
			Date today = new Date();
			
			RTFPromoOffers offer  = new RTFPromoOffers();
			offer.setStartDate(fDate);
			offer.setEndDate(tDate);
			offer.setMaxOrders(Integer.parseInt(maxOrders));
			offer.setCreatedBy(userName);
			offer.setCreatedDate(today);
			offer.setDiscountPer(Double.parseDouble(discountPerc));
			offer.setMobileDiscountPerc(Double.parseDouble(mobileDiscountPerc));
			offer.setModifiedBy(userName);
			offer.setModifiedDate(today);
			offer.setNoOfOrders(0);
			if(autoGenerate.equalsIgnoreCase("Yes")){
				offer.setPromoCode(Util.generateEncryptedKey(selectedString));
			}else if(autoGenerate.equalsIgnoreCase("No")){
				offer.setPromoCode(promotionalCode);
			}
			if(flatDiscount.equalsIgnoreCase("Yes")){
				offer.setFlatOfferOrderThreshold(Double.parseDouble(orderThreshold));	//Order Threshold
			}
			offer.setStatus("ENABLED");
			if(flatDiscount.equalsIgnoreCase("Yes")){
				offer.setIsFlatDiscount(true);
			}else if(flatDiscount.equalsIgnoreCase("No")){
				offer.setIsFlatDiscount(false);
			}
			
			DAORegistry.getRtfPromoOffersDAO().save(offer);
			
			RTFPromoType promoTypeOffer = null;
			for(int i=0; i< Integer.parseInt(countPromoTypes); i++){
				promoTypeOffer = new RTFPromoType();
				eventId = request.getParameter("eventId_"+i);
				parentId = request.getParameter("parentId_"+i);
				childId = request.getParameter("childId_"+i);
				grandChildId = request.getParameter("grandChildId_"+i);
				artistId = request.getParameter("artistId_"+i);
				venueId = request.getParameter("venueId_"+i);		
				promoTypeId = request.getParameter("promoTypeId_"+i);
				
				if((promoTypeId != null && !promoTypeId.isEmpty()) || (artistId!=null && !artistId.isEmpty()) || 
					(venueId!=null && !venueId.isEmpty()) || (grandChildId!=null && !grandChildId.isEmpty()) || 
					(childId!=null && !childId.isEmpty()) || (parentId!=null && !parentId.isEmpty()) || 
					(eventId!=null && !eventId.isEmpty())){
					promoTypeOffer.setPromoOfferId(offer.getId());
					if(promoTypeId != null && !promoTypeId.isEmpty()){
						promoTypeOffer.setPromoType(PromotionalType.ALL.toString());
					}else if(artistId!=null && !artistId.isEmpty()){
						promoTypeOffer.setArtistId(Integer.parseInt(artistId));
						promoTypeOffer.setPromoType(PromotionalType.ARTIST.toString());
					}else if(eventId!=null && !eventId.isEmpty()){
						promoTypeOffer.setEventId(Integer.parseInt(eventId));
						promoTypeOffer.setPromoType(PromotionalType.EVENT.toString());
					}else if(venueId!=null && !venueId.isEmpty()){
						promoTypeOffer.setVenueId(Integer.parseInt(venueId));
						promoTypeOffer.setPromoType(PromotionalType.VENUE.toString());
					}else if(grandChildId!=null && !grandChildId.isEmpty()){
						promoTypeOffer.setGrandChildId(Integer.parseInt(grandChildId));
						promoTypeOffer.setPromoType(PromotionalType.GRANDCHILD.toString());
					}else if(childId!=null && !childId.isEmpty()){
						promoTypeOffer.setChildId(Integer.parseInt(childId));
						promoTypeOffer.setPromoType(PromotionalType.CHILD.toString());
					}else if(parentId!=null && !parentId.isEmpty()){
						promoTypeOffer.setParentId(Integer.parseInt(parentId));
						promoTypeOffer.setPromoType(PromotionalType.PARENT.toString());
					}
					
					DAORegistry.getRtfPromoTypeDAO().save(promoTypeOffer);
				}
			}
			saveDiscountCodeDTO.setMessage("Discount Code generated successfully.");
			saveDiscountCodeDTO.setStatus(1);
			
			GridHeaderFilters filter = new GridHeaderFilters();
			List<RTFPromoOffers> list = DAORegistry.getQueryManagerDAO().getDiscountCodes(null,filter);
			
			saveDiscountCodeDTO.setDiscountCodePaginationDTO(PaginationUtil.getDummyPaginationParameters(list!=null?list.size():0));
			saveDiscountCodeDTO.setPromoOfferDTO(com.rtw.tracker.utils.Util.getPromotionalCodeArray(list));
			
			//Tracking User Action
			String userActionMsg = "Discount Code Generated Successfully.";
			Util.userActionAudit(request, offer.getId(), userActionMsg);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return saveDiscountCodeDTO;		
	}
	
	
	@RequestMapping(value = "/EditDiscountCode")
	public SaveDiscountCodeDTO editDiscountCode(HttpServletRequest request, HttpServletResponse response){
		SaveDiscountCodeDTO saveDiscountCodeDTO = new SaveDiscountCodeDTO();
		Error error = new Error();
		
		try {			
			String action = request.getParameter("action");			
			
			if(action != null && action.equalsIgnoreCase("edit")){
				
				String promoIdStr = request.getParameter("promoId");
				
				if(StringUtils.isEmpty(promoIdStr)){
					System.err.println("Please select Any Promotional Code.");
					error.setDescription("Please select Any Promotional Code.");
					saveDiscountCodeDTO.setError(error);
					saveDiscountCodeDTO.setStatus(0);
					return saveDiscountCodeDTO;
				}
				Integer promoId = 0;
				try{
					promoId = Integer.parseInt(promoIdStr);
				}catch(Exception e){
					e.printStackTrace();
					error.setDescription("Please select Valid Promotional Code.");
					saveDiscountCodeDTO.setError(error);
					saveDiscountCodeDTO.setStatus(0);
					return saveDiscountCodeDTO;
				}
				
				GridHeaderFilters filter = new GridHeaderFilters();
				List<RTFPromoOffers> list = DAORegistry.getQueryManagerDAO().getDiscountCodes(promoId,filter);				
				if(list != null && list.size() > 0){
					saveDiscountCodeDTO.setPromoOfferDTO(com.rtw.tracker.utils.Util.getPromotionalCodeArray(list));
				}
				
				saveDiscountCodeDTO.setStatus(1);				
				return saveDiscountCodeDTO;
				
			}else if(action != null && action.equalsIgnoreCase("update")){
				
				String promoIdStr = request.getParameter("promoId");
				String autoGenerate = request.getParameter("promoAutoGenerate");
				String promotionalCode = request.getParameter("promoCode");
				String flatDiscount = request.getParameter("promoFlatDiscount");
				String discountPerc = request.getParameter("promoDiscountPerc");
				String mobileDiscountPerc = request.getParameter("promoMobileDiscountPerc");
				String orderThreshold = request.getParameter("promoOrderThreshold");
				String fromDate = request.getParameter("promoFromDate");
				String toDate = request.getParameter("promoToDate");
				String maxOrders = request.getParameter("promoMaxOrders");
				String countPromoTypes = request.getParameter("count");	
				String userName = request.getParameter("userName");
				
				if(StringUtils.isEmpty(promoIdStr)){
					System.err.println("Please select Any Promotional Code.");
					error.setDescription("Please select Any Promotional Code.");
					saveDiscountCodeDTO.setError(error);
					saveDiscountCodeDTO.setStatus(0);
					return saveDiscountCodeDTO;
				}
				Integer promoId = 0;
				try{
					promoId = Integer.parseInt(promoIdStr);
				}catch(Exception e){
					e.printStackTrace();
					error.setDescription("Please select Valid Promotional Code.");
					saveDiscountCodeDTO.setError(error);
					saveDiscountCodeDTO.setStatus(0);
					return saveDiscountCodeDTO;
				}
				
				if(StringUtils.isEmpty(autoGenerate)){
					System.err.println("Please select Auto Generate Option.");
					error.setDescription("Please select Auto Generate Option.");
					saveDiscountCodeDTO.setError(error);
					saveDiscountCodeDTO.setStatus(0);
					return saveDiscountCodeDTO;
				}
				if(autoGenerate.equalsIgnoreCase("No")){
					/*if(StringUtils.isEmpty(promotionalCode)){
						System.err.println("Promotional Code is not found.");
						error.setDescription("Promotional Code is not found.");
						saveDiscountCodeDTO.setError(error);
						saveDiscountCodeDTO.setStatus(0);
						return saveDiscountCodeDTO;
					}else{
						RTFPromoOffers promoOffer = DAORegistry.getRtfPromoOffersDAO().getPromoCode(promotionalCode);
						if(promoOffer != null){
							System.err.println("Promotional Code is already exist.");
							error.setDescription("Promotional Code is already exist.");
							saveDiscountCodeDTO.setError(error);
							saveDiscountCodeDTO.setStatus(0);
							return saveDiscountCodeDTO;
						}
					}*/
				}
				
				String eventId = "";
				String parentId = "";
				String childId = "";
				String grandChildId = "";
				String artistId = "";
				String venueId = "";		
				String promoTypeId = "";
				String selectedString = "";
				int count = 0;
				int countPromos = 0;
				
				//Fetching Contest
				Contests contest = QuizDAORegistry.getContestsDAO().getContestByPromoOfferId(promoId);
				
				for(int i=0; i < Integer.parseInt(countPromoTypes); i++){
					eventId = request.getParameter("eventId_"+i);
					parentId = request.getParameter("parentId_"+i);
					childId = request.getParameter("childId_"+i);
					grandChildId = request.getParameter("grandChildId_"+i);
					artistId = request.getParameter("artistId_"+i);
					venueId = request.getParameter("venueId_"+i);		
					promoTypeId = request.getParameter("promoTypeId_"+i);
					if((promoTypeId == null || promoTypeId.isEmpty()) && (parentId==null || parentId.isEmpty()) && (childId==null || childId.isEmpty())
							&& (grandChildId==null || grandChildId.isEmpty()) && (artistId==null || artistId.isEmpty())
							&& (venueId==null || venueId.isEmpty()) && (eventId==null || eventId.isEmpty())){
						count++;
					}
					if((promoTypeId != null && !promoTypeId.isEmpty()) || (venueId != null && !venueId.isEmpty())){
						if(contest != null){
							System.err.println("Associated with Contest, Can't Change to ALL/Venue Category.");
							error.setDescription("Associated with Contest, Can't Change to ALL/Venue Category.");
							saveDiscountCodeDTO.setError(error);
							saveDiscountCodeDTO.setStatus(0);
							return saveDiscountCodeDTO;
						}
					}
					if((promoTypeId != null && !promoTypeId.isEmpty()) || (artistId!=null && !artistId.isEmpty()) || 
							(venueId!=null && !venueId.isEmpty()) || (grandChildId!=null && !grandChildId.isEmpty()) || 
							(childId!=null && !childId.isEmpty()) || (parentId!=null && !parentId.isEmpty()) || 
							(eventId!=null && !eventId.isEmpty())){
						countPromos++;
					}
				}
				if(contest != null){
					if(countPromos > 1){
						System.err.println("Associated with Contest, Can't add more than One Artist/Event/Category.");
						error.setDescription("Associated with Contest, Can't add more than One Artist/Event/Category.");
						saveDiscountCodeDTO.setError(error);
						saveDiscountCodeDTO.setStatus(0);
						return saveDiscountCodeDTO;
					}
				}
				if(count == Integer.parseInt(countPromoTypes)){
					System.err.println("Id not found for one of following Artist,Event,venue,parent,child,grand child.");
					error.setDescription("Id not found for one of following Artist,Event,venue,parent,child,grand child.");
					saveDiscountCodeDTO.setError(error);
					saveDiscountCodeDTO.setStatus(0);
					return saveDiscountCodeDTO;
				}
				
				if(StringUtils.isEmpty(flatDiscount)){
					System.err.println("Please select Flat Discount Option.");
					error.setDescription("Please select Flat Discount Option.");
					saveDiscountCodeDTO.setError(error);
					saveDiscountCodeDTO.setStatus(0);
					return saveDiscountCodeDTO;
				}
				if(flatDiscount.equalsIgnoreCase("Yes")){
					if(StringUtils.isEmpty(orderThreshold)){
						System.err.println("Order Threshold is not found.");
						error.setDescription("Order Threshold is not found.");
						saveDiscountCodeDTO.setError(error);
						saveDiscountCodeDTO.setStatus(0);
						return saveDiscountCodeDTO;
					}
				}
				
				if(StringUtils.isEmpty(discountPerc)){
					System.err.println("Discount percentage is not found.");
					error.setDescription("Discount percentage is not found.");
					saveDiscountCodeDTO.setError(error);
					saveDiscountCodeDTO.setStatus(0);
					return saveDiscountCodeDTO;
				}
				
				if(StringUtils.isEmpty(mobileDiscountPerc)){
					System.err.println("Mobile Discount percentage is not found.");
					error.setDescription("Mobile Discount percentage is not found.");
					saveDiscountCodeDTO.setError(error);
					saveDiscountCodeDTO.setStatus(0);
					return saveDiscountCodeDTO;
				}
				
				if(StringUtils.isEmpty(fromDate) || StringUtils.isEmpty(toDate)){
					System.err.println("Missing from Date or To Date.");
					error.setDescription("Missing from Date or To Date.");
					saveDiscountCodeDTO.setError(error);
					saveDiscountCodeDTO.setStatus(0);
					return saveDiscountCodeDTO;
				}
				
				if(StringUtils.isEmpty(maxOrders)){
					System.err.println("Max. Orders is not found.");
					error.setDescription("Max. Orders is not found.");
					saveDiscountCodeDTO.setError(error);
					saveDiscountCodeDTO.setStatus(0);
					return saveDiscountCodeDTO;
				}
								
				fromDate = fromDate + " 00:00:00";
				toDate = toDate + " 23:59:59";
				Date fDate = null;
				Date tDate = null;
				try {
					fDate = Util.getDateWithTwentyFourHourFormat1(fromDate);
					tDate = Util.getDateWithTwentyFourHourFormat1(toDate);
				} catch (Exception e) {
					e.printStackTrace();
					error.setDescription("Start Date or End Date with bad values.");
					saveDiscountCodeDTO.setError(error);
					saveDiscountCodeDTO.setStatus(0);
					return saveDiscountCodeDTO;
				}
				
				for(int i=0; i < Integer.parseInt(countPromoTypes); i++){
					selectedString = request.getParameter("artistVenueName_"+i);
					if(selectedString != null && !selectedString.isEmpty()){
						break;
					}
				}
				Date today = new Date();
				
				RTFPromoOffers offer  = DAORegistry.getRtfPromoOffersDAO().get(promoId);
				offer.setStartDate(fDate);
				offer.setEndDate(tDate);
				offer.setMaxOrders(Integer.parseInt(maxOrders));
				offer.setCreatedBy(userName);
				//offer.setCreatedDate(today);
				offer.setDiscountPer(Double.parseDouble(discountPerc));
				offer.setMobileDiscountPerc(Double.parseDouble(mobileDiscountPerc));
				offer.setModifiedBy(userName);
				offer.setModifiedDate(today);
				offer.setNoOfOrders(0);
				/*if(autoGenerate.equalsIgnoreCase("Yes")){
					offer.setPromoCode(Util.generateEncryptedKey(selectedString));
				}else if(autoGenerate.equalsIgnoreCase("No")){
					offer.setPromoCode(promotionalCode);
				}*/
				
				offer.setStatus("ENABLED");
				if(flatDiscount.equalsIgnoreCase("Yes")){
					offer.setIsFlatDiscount(true);
					offer.setFlatOfferOrderThreshold(Double.parseDouble(orderThreshold));	//Order Threshold
				}else if(flatDiscount.equalsIgnoreCase("No")){
					offer.setIsFlatDiscount(false);
					offer.setFlatOfferOrderThreshold(0.00);	//Order Threshold
				}
				
				DAORegistry.getRtfPromoOffersDAO().update(offer);
				
				List<RTFPromoType> promoTypeOffers =  DAORegistry.getRtfPromoTypeDAO().getPromoTypeFromOfferId(promoId);
				if(promoTypeOffers != null && promoTypeOffers.size() > 0){
					DAORegistry.getRtfPromoTypeDAO().deleteAll(promoTypeOffers);
				}
				RTFPromoType promoTypeOffer = null;				
				for(int i=0; i< Integer.parseInt(countPromoTypes); i++){
					eventId = request.getParameter("eventId_"+i);
					parentId = request.getParameter("parentId_"+i);
					childId = request.getParameter("childId_"+i);
					grandChildId = request.getParameter("grandChildId_"+i);
					artistId = request.getParameter("artistId_"+i);
					venueId = request.getParameter("venueId_"+i);		
					promoTypeId = request.getParameter("promoTypeId_"+i);
					
					if((promoTypeId != null && !promoTypeId.isEmpty()) || (artistId!=null && !artistId.isEmpty()) || 
						(venueId!=null && !venueId.isEmpty()) || (grandChildId!=null && !grandChildId.isEmpty()) || 
						(childId!=null && !childId.isEmpty()) || (parentId!=null && !parentId.isEmpty()) || 
						(eventId!=null && !eventId.isEmpty())){
						
						/*if(j < promoTypeOffers.size() && promoTypeOffers.get(j) != null){
							if(promoTypeOffers.get(j).getPromoOfferId().intValue() == offer.getId().intValue()){
								promoTypeOffer = promoTypeOffers.get(j);
								j++;
							}
						}else{
							promoTypeOffer = new RTFPromoType();
						}*/
						promoTypeOffer = new RTFPromoType();
						
						promoTypeOffer.setPromoOfferId(offer.getId());
						if(promoTypeId != null && !promoTypeId.isEmpty()){
							promoTypeOffer.setPromoType(PromotionalType.ALL.toString());
						}else if(artistId!=null && !artistId.isEmpty()){
							promoTypeOffer.setArtistId(Integer.parseInt(artistId));
							promoTypeOffer.setPromoType(PromotionalType.ARTIST.toString());
						}else if(eventId!=null && !eventId.isEmpty()){
							promoTypeOffer.setEventId(Integer.parseInt(eventId));
							promoTypeOffer.setPromoType(PromotionalType.EVENT.toString());
						}else if(venueId!=null && !venueId.isEmpty()){
							promoTypeOffer.setVenueId(Integer.parseInt(venueId));
							promoTypeOffer.setPromoType(PromotionalType.VENUE.toString());
						}else if(grandChildId!=null && !grandChildId.isEmpty()){
							promoTypeOffer.setGrandChildId(Integer.parseInt(grandChildId));
							promoTypeOffer.setPromoType(PromotionalType.GRANDCHILD.toString());
						}else if(childId!=null && !childId.isEmpty()){
							promoTypeOffer.setChildId(Integer.parseInt(childId));
							promoTypeOffer.setPromoType(PromotionalType.CHILD.toString());
						}else if(parentId!=null && !parentId.isEmpty()){
							promoTypeOffer.setParentId(Integer.parseInt(parentId));
							promoTypeOffer.setPromoType(PromotionalType.PARENT.toString());
						}
						
						DAORegistry.getRtfPromoTypeDAO().save(promoTypeOffer);
						
						//Contest Table
						if(contest != null){
							if(artistId!=null && !artistId.isEmpty()){
								contest.setPromoRefId(Integer.parseInt(artistId));
								contest.setPromoRefType(PromotionalType.ARTIST.toString());
								Artist artist = DAORegistry.getArtistDAO().get(Integer.parseInt(artistId));
								contest.setPromoRefName(artist.getName());
							}else if(eventId!=null && !eventId.isEmpty()){
								contest.setPromoRefId(Integer.parseInt(eventId));
								contest.setPromoRefType(PromotionalType.EVENT.toString());
								Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
								contest.setPromoRefName(event.getEventName());
							}else if(grandChildId!=null && !grandChildId.isEmpty()){
								contest.setPromoRefId(Integer.parseInt(grandChildId));
								contest.setPromoRefType(PromotionalType.GRANDCHILD.toString());
								GrandChildCategory grandChild = DAORegistry.getGrandChildCategoryDAO().get(Integer.parseInt(grandChildId));
								contest.setPromoRefName(grandChild.getName());
							}else if(childId!=null && !childId.isEmpty()){
								contest.setPromoRefId(Integer.parseInt(childId));
								contest.setPromoRefType(PromotionalType.CHILD.toString());
								ChildCategory child = DAORegistry.getChildCategoryDAO().get(Integer.parseInt(childId));
								contest.setPromoRefName(child.getName());
							}else if(parentId!=null && !parentId.isEmpty()){
								contest.setPromoRefId(Integer.parseInt(parentId));
								contest.setPromoRefType(PromotionalType.PARENT.toString());
								ParentCategory parent = DAORegistry.getParentCategoryDAO().get(Integer.parseInt(parentId));
								contest.setPromoRefName(parent.getName());
							}
							contest.setPromoExpiryDate(tDate);
							contest.setDiscountPercentage(Double.parseDouble(mobileDiscountPerc));
							contest.setUpdatedBy(userName);
							contest.setUpdatedDate(new Date());
							
							QuizDAORegistry.getContestsDAO().update(contest);
						}
					}
				}
				
				saveDiscountCodeDTO.setMessage("Discount Code updated successfully.");
				saveDiscountCodeDTO.setStatus(1);
				
				GridHeaderFilters filter = new GridHeaderFilters();
				List<RTFPromoOffers> list = DAORegistry.getQueryManagerDAO().getDiscountCodes(null,filter);
				
				saveDiscountCodeDTO.setDiscountCodePaginationDTO(PaginationUtil.getDummyPaginationParameters(list!=null?list.size():0));
				saveDiscountCodeDTO.setPromoOfferDTO(com.rtw.tracker.utils.Util.getPromotionalCodeArray(list));				
								
				//Tracking User Action
				String userActionMsg = "Discount Code Updated Successfully.";
				Util.userActionAudit(request, offer.getId(), userActionMsg);				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return saveDiscountCodeDTO;
	}
	
	
	@RequestMapping(value = "/GetDiscountCodeDetail")
	public DiscountCodeDetailDTO getDiscountCodeDetail(HttpServletRequest request, HttpServletResponse response){
		DiscountCodeDetailDTO discountCodeDetailDTO = new DiscountCodeDetailDTO();
		Error error = new Error();
		
		try{
			String promoOfferIdStr = request.getParameter("promoOfferId");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			if(StringUtils.isEmpty(promoOfferIdStr)){
				System.err.println("Please select Any Promotional Code.");
				error.setDescription("Please select Any Promotional Code.");
				discountCodeDetailDTO.setError(error);
				discountCodeDetailDTO.setStatus(0);
				return discountCodeDetailDTO;
			}
			Integer promoOfferId = 0;
			try{
				promoOfferId = Integer.parseInt(promoOfferIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please select Valid Promotional Code.");
				discountCodeDetailDTO.setError(error);
				discountCodeDetailDTO.setStatus(0);
				return discountCodeDetailDTO;
			}
			
			if(StringUtils.isEmpty(pageNo)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				discountCodeDetailDTO.setError(error);
				discountCodeDetailDTO.setStatus(0);
				return discountCodeDetailDTO;
			}
			try {
				Integer.parseInt(pageNo);
			} catch (Exception e) {
				System.err.println("Please provide valid PageNo.");
				error.setDescription("Please provide valid PageNo.");
				discountCodeDetailDTO.setError(error);
				discountCodeDetailDTO.setStatus(0);
				return discountCodeDetailDTO;
			}
			
			GridHeaderFilters filter = null;
			try {
				filter = GridHeaderFiltersUtil.getRTFPromotionalOfferFilter(headerFilter);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				discountCodeDetailDTO.setError(error);
				discountCodeDetailDTO.setStatus(0);
				return discountCodeDetailDTO;
			}
			
			List<RTFPromoOffers> promoOfferList = null;
			Integer count = 0;
						
			promoOfferList = DAORegistry.getQueryManagerDAO().getPromotionalOfferDetails(promoOfferId, filter);
			if(promoOfferList != null && promoOfferList.size() > 0){
				count = promoOfferList.size();
			}else{
				discountCodeDetailDTO.setMessage("No Promotional Offer Details found for Selected search filter.");
			}
			
			discountCodeDetailDTO.setPromoOfferDetailPaginationDTO(PaginationUtil.getDummyPaginationParameters(count));
			discountCodeDetailDTO.setPromoOfferDTO(com.rtw.tracker.utils.Util.getPromotionalCodeArray(promoOfferList));
			discountCodeDetailDTO.setStatus(1);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return discountCodeDetailDTO;
	}
	
	@RequestMapping({"/PromoOfferDtlExportToExcel"})
	public void PromoOfferDetailToExport(HttpServletRequest request, HttpServletResponse response){
		
		try{
			String promoOfferId = request.getParameter("promoOfferId");
			String headerFilter = request.getParameter("headerFilter");			

			GridHeaderFilters filter = GridHeaderFiltersUtil.getRTFPromotionalOfferFilter(headerFilter); 
			List<RTFPromoOffers> promoOffersList = DAORegistry.getQueryManagerDAO().getPromotionalOfferDetails(Integer.parseInt(promoOfferId), filter);
			
			if(promoOffersList!=null && !promoOffersList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Promotional_Offer_Detail");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Promotional Offer Detail Id");				
				header.createCell(1).setCellValue("Promo Type");
				header.createCell(2).setCellValue("Artist");
				header.createCell(3).setCellValue("Venue");
				header.createCell(4).setCellValue("Parent Category");
				header.createCell(5).setCellValue("Child Category");
				header.createCell(6).setCellValue("Grand Child Category");
				Integer i=1;
				for(RTFPromoOffers promoOffer : promoOffersList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(promoOffer.getId());
					row.createCell(1).setCellValue(promoOffer.getPromoType()!=null?promoOffer.getPromoType():"");
					row.createCell(2).setCellValue(promoOffer.getArtistName()!=null?promoOffer.getArtistName():"");
					row.createCell(3).setCellValue(promoOffer.getVenueName()!=null?promoOffer.getVenueName():"");
					row.createCell(4).setCellValue(promoOffer.getParentCategory()!=null?promoOffer.getParentCategory():"");
					row.createCell(5).setCellValue(promoOffer.getChildCategory()!=null?promoOffer.getChildCategory():"");
					row.createCell(6).setCellValue(promoOffer.getGrandChildCategory()!=null?promoOffer.getGrandChildCategory():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Promotional_Offer_Detail.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/AutoCompleteArtistAndEventAndVenueAndCategory")
	public AutoCompleteArtistVenueAndCategoryDTO getAutoCompleteArtistAndEventAndVenueAndCategory(HttpServletRequest request, HttpServletResponse response)throws Exception {
		AutoCompleteArtistVenueAndCategoryDTO autoCompleteArtistVenueAndCategoryDTO = new AutoCompleteArtistVenueAndCategoryDTO();
		Error error = new Error();
		
		try{
			String param = request.getParameter("q");
			String strResponse ="";
			
			Collection<ParentCategory> parentCat = DAORegistry.getParentCategoryDAO().getParentCategoriesByName(param);
			Collection<GrandChildCategory> grandChildCategories = DAORegistry.getGrandChildCategoryDAO().getGrandChildCategoriesByNameAndDisplayOnSearch(param);
			Collection<ChildCategory> childCategories = DAORegistry.getChildCategoryDAO().getChildCategoriesByNameAndDisplayOnSearch(param);
			Collection<Artist> artists = DAORegistry.getArtistDAO().filterByName(param);
			Collection<Venue> venues = DAORegistry.getVenueDAO().filterByName(param);
			Collection<Event> events = DAORegistry.getQueryManagerDAO().getAllActiveEventWithVenue(param);
			
			strResponse += ("ALL"+ "|" + "0" + "|" + "ALL" + "\n");
			if (parentCat != null) {
				for (ParentCategory parent : parentCat) {
					strResponse += ("PARENT" + "|" + parent.getId() + "|" + parent.getName() + "\n");
				}
			}
			
			if (childCategories != null) {
				for (ChildCategory childCategory : childCategories) {
					strResponse += ("CHILD" + "|" + childCategory.getId() + "|" + childCategory.getName() + "\n");
				}
			}
			
			if (grandChildCategories != null) {
				for (GrandChildCategory grandChildCateogry : grandChildCategories) {
					strResponse += ("GRAND" + "|" + grandChildCateogry.getId() + "|" + grandChildCateogry.getName() + "\n");
				}
			}
			
			if (artists != null) {
				for (Artist artist : artists) {
					strResponse += ("ARTIST" + "|" + artist.getId() + "|" + artist.getName() + "\n") ;
				}
			}			
			
			if (venues != null) {
				for (Venue venue : venues) {
					strResponse += ("VENUE" + "|" + venue.getId() + "|" + venue.getBuilding() + "\n");
				}
			}
			
			if (events != null) {
				for (Event event : events) {
					strResponse += ("EVENT" + "|" + event.getEventId() + "|" + event.getEventName() + "|" + event.getNameWithDateandVenue() + "\n") ;
				}
			}
			autoCompleteArtistVenueAndCategoryDTO.setArtistVenueAndCategory(strResponse);
			autoCompleteArtistVenueAndCategoryDTO.setStatus(1);
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Auto Complete Artist Venue and Category");
			autoCompleteArtistVenueAndCategoryDTO.setError(error);
			autoCompleteArtistVenueAndCategoryDTO.setStatus(0);
		}
		return autoCompleteArtistVenueAndCategoryDTO;
	}
	
	
	public static boolean isEmptyOrNull(String value){
			
		if(null == value || value.isEmpty()){
			return true;
		}
		return false;
	}
	

	@RequestMapping({"/DiscountCodeExportToExcel"})
	public void discountCodeToExport(HttpServletRequest request, HttpServletResponse response){
				
		try{
			String headerFilter = request.getParameter("headerFilter");
			
			GridHeaderFilters filter = GridHeaderFiltersUtil.getRTFPromotionalOfferFilter(headerFilter); 
			List<RTFPromoOffers> promoOffersList = DAORegistry.getQueryManagerDAO().getDiscountCodesToExport(filter);
			
			if(promoOffersList!=null && !promoOffersList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Discount_Code");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Promotional Offer Id");
				header.createCell(1).setCellValue("Promotional Code");
				header.createCell(2).setCellValue("Desktop Discount");
				header.createCell(3).setCellValue("APP Discount");
				header.createCell(4).setCellValue("Start Date");
				header.createCell(5).setCellValue("End Date");
				header.createCell(6).setCellValue("Max. Order");
				header.createCell(7).setCellValue("Orders");
				header.createCell(8).setCellValue("Status");
				header.createCell(9).setCellValue("Created By");
				header.createCell(10).setCellValue("Created Date");
				header.createCell(11).setCellValue("Modified By");
				Integer i=1;
				for(RTFPromoOffers promoOffer : promoOffersList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(promoOffer.getId());
					row.createCell(1).setCellValue(promoOffer.getPromoCode());
					row.createCell(2).setCellValue(promoOffer.getDiscountPer()!=null?promoOffer.getDiscountPer():0);
					row.createCell(3).setCellValue(promoOffer.getMobileDiscountPerc()!=null?promoOffer.getMobileDiscountPerc():0);
					row.createCell(4).setCellValue(promoOffer.getStartDateStr()!=null?promoOffer.getStartDateStr():"");
					row.createCell(5).setCellValue(promoOffer.getEndDateStr()!=null?promoOffer.getEndDateStr():"");
					row.createCell(6).setCellValue(promoOffer.getMaxOrders()!=null?promoOffer.getMaxOrders():0);
					row.createCell(7).setCellValue(promoOffer.getNoOfOrders()!=null?promoOffer.getNoOfOrders():0);
					row.createCell(8).setCellValue(promoOffer.getStatus()!=null?promoOffer.getStatus():"");
					row.createCell(9).setCellValue(promoOffer.getCreatedBy()!=null?promoOffer.getCreatedBy():"");
					row.createCell(10).setCellValue(promoOffer.getCreatedDateStr()!=null?promoOffer.getCreatedDateStr():"");
					row.createCell(11).setCellValue(promoOffer.getModifiedBy()!=null?promoOffer.getModifiedBy():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Discount_Code.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/CustomerPromotionalOfferDetails")
	public CustomerPromotionalOfferDetailsDTO customerPromotionalOfferDetails(HttpServletRequest request, HttpServletResponse response){
		CustomerPromotionalOfferDetailsDTO customerPromotionalOfferDetailsDTO = new CustomerPromotionalOfferDetailsDTO();
		Error error = new Error();
		
		try {
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String sortingString = request.getParameter("sortingString");
			
			GridHeaderFilters filter = null;
			try {
				filter = GridHeaderFiltersUtil.getRTFCustomerPromotionalOfferFilter(headerFilter+sortingString);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				customerPromotionalOfferDetailsDTO.setError(error);
				customerPromotionalOfferDetailsDTO.setStatus(0);
				return customerPromotionalOfferDetailsDTO;
			}
			
			List<RTFCustomerPromotionalOffer> promoOfferList = null;
			List<RTFPromotionalOfferTracking> promoOfferTrackingList = null;
			int promoOfferCount = 0;
			int promoOfferTrackingCount = 0;
			
			promoOfferList = DAORegistry.getQueryManagerDAO().getCustomerPromotionalOffer(filter,pageNo);
			promoOfferTrackingList = DAORegistry.getQueryManagerDAO().getPromotionalOfferTracking(filter, pageNo);
			
			if(promoOfferList != null && promoOfferList.size() > 0){
				promoOfferCount = DAORegistry.getQueryManagerDAO().getCustomerPromotionalOfferCount(filter);
			}
			if(promoOfferTrackingList != null && promoOfferTrackingList.size() > 0){
				promoOfferTrackingCount =DAORegistry.getQueryManagerDAO().getPromotionalOfferTrackingCount(filter);
			}
			customerPromotionalOfferDetailsDTO.setCusPromoOfferDTO(com.rtw.tracker.utils.Util.getCustomerPromotionalOfferArray(promoOfferList));
			customerPromotionalOfferDetailsDTO.setCusPromoOfferPaginationDTO(PaginationUtil.calculatePaginationParameter(promoOfferCount, pageNo));
			customerPromotionalOfferDetailsDTO.setCusPromoOfferTrackingDTO(com.rtw.tracker.utils.Util.getCustomerPromotionalOfferTrackingArray(promoOfferTrackingList));
			customerPromotionalOfferDetailsDTO.setCusPromoOfferTrackingPaginationDTO(PaginationUtil.calculatePaginationParameter(promoOfferTrackingCount, pageNo));
			customerPromotionalOfferDetailsDTO.setStatus(1);
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Customer Promotional Offer");
			customerPromotionalOfferDetailsDTO.setError(error);
			customerPromotionalOfferDetailsDTO.setStatus(0);
		}
		return customerPromotionalOfferDetailsDTO;		
	}
	
	/*@RequestMapping(value = "/GetCustomerPromotionalOffer")
	public CustomerPromotionalOfferDetailsDTO getCustomerPromotionalOffer(HttpServletRequest request, HttpServletResponse response){
		CustomerPromotionalOfferDetailsDTO customerPromotionalOfferDetailsDTO = new CustomerPromotionalOfferDetailsDTO();
		Error error = new Error();
		
		try {
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			if(StringUtils.isEmpty(pageNo)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				customerPromotionalOfferDetailsDTO.setError(error);
				customerPromotionalOfferDetailsDTO.setStatus(0);
				return customerPromotionalOfferDetailsDTO;
			}
			try {
				Integer.parseInt(pageNo);
			} catch (Exception e) {
				System.err.println("Please provide valid PageNo.");
				error.setDescription("Please provide valid PageNo.");
				customerPromotionalOfferDetailsDTO.setError(error);
				customerPromotionalOfferDetailsDTO.setStatus(0);
				return customerPromotionalOfferDetailsDTO;
			}
			
			GridHeaderFilters filter = null;
			try {
				filter = GridHeaderFiltersUtil.getRTFCustomerPromotionalOfferFilter(headerFilter);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				customerPromotionalOfferDetailsDTO.setError(error);
				customerPromotionalOfferDetailsDTO.setStatus(0);
				return customerPromotionalOfferDetailsDTO;
			}
			
			List<RTFCustomerPromotionalOffer> promoOfferList = null;
			int promoOfferCount = 0;
			
			promoOfferList = DAORegistry.getQueryManagerDAO().getCustomerPromotionalOffer(filter,pageNo);
			if(promoOfferList != null && promoOfferList.size() > 0){
				promoOfferCount = promoOfferList.size();
			}else{
				customerPromotionalOfferDetailsDTO.setMessage("No Promotional Offer found for Selected search filter");
			}
			
			customerPromotionalOfferDetailsDTO.setCusPromoOfferDTO(com.rtw.tracker.utils.Util.getCustomerPromotionalOfferArray(promoOfferList));
			customerPromotionalOfferDetailsDTO.setCusPromoOfferPaginationDTO(PaginationUtil.calculatePaginationParameter(promoOfferCount, null));
			customerPromotionalOfferDetailsDTO.setStatus(1);
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Customer Promotional Offer");
			customerPromotionalOfferDetailsDTO.setError(error);
			customerPromotionalOfferDetailsDTO.setStatus(0);
		}
		return customerPromotionalOfferDetailsDTO;
	}*/
	
	@RequestMapping(value = "/GetCustomerPromotionalOfferTracking")
	public CustomerPromotionalOfferDetailsDTO getCustomerPromotionalOfferTracking(HttpServletRequest request, HttpServletResponse response){
		CustomerPromotionalOfferDetailsDTO customerPromotionalOfferDetailsDTO = new CustomerPromotionalOfferDetailsDTO();
		Error error = new Error();
		
		try {
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String sortingString = request.getParameter("sortingString");
			
			if(StringUtils.isEmpty(pageNo)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				customerPromotionalOfferDetailsDTO.setError(error);
				customerPromotionalOfferDetailsDTO.setStatus(0);
				return customerPromotionalOfferDetailsDTO;
			}
			try {
				Integer.parseInt(pageNo);
			} catch (Exception e) {
				System.err.println("Please provide valid PageNo.");
				error.setDescription("Please provide valid PageNo.");
				customerPromotionalOfferDetailsDTO.setError(error);
				customerPromotionalOfferDetailsDTO.setStatus(0);
				return customerPromotionalOfferDetailsDTO;
			}
			
			GridHeaderFilters filter = null;
			try {
				filter = GridHeaderFiltersUtil.getRTFPromotionalOfferTrackingFilter(headerFilter+sortingString);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				customerPromotionalOfferDetailsDTO.setError(error);
				customerPromotionalOfferDetailsDTO.setStatus(0);
				return customerPromotionalOfferDetailsDTO;
			}
						 
			List<RTFPromotionalOfferTracking> promoOfferTrackingList = null;
			int promoOfferTrackingCount = 0;
			
			promoOfferTrackingList = DAORegistry.getQueryManagerDAO().getPromotionalOfferTracking(filter, pageNo);
			if(promoOfferTrackingList != null && promoOfferTrackingList.size() > 0){
				promoOfferTrackingCount = DAORegistry.getQueryManagerDAO().getPromotionalOfferTrackingCount(filter);
			}else{
				customerPromotionalOfferDetailsDTO.setMessage("No Promotional Offer Tracking found for Selected search filter");
			}
			
			customerPromotionalOfferDetailsDTO.setCusPromoOfferTrackingDTO(com.rtw.tracker.utils.Util.getCustomerPromotionalOfferTrackingArray(promoOfferTrackingList));
			customerPromotionalOfferDetailsDTO.setCusPromoOfferTrackingPaginationDTO(PaginationUtil.calculatePaginationParameter(promoOfferTrackingCount, pageNo));
			customerPromotionalOfferDetailsDTO.setStatus(1);
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Customer Promotional Offer Tracking");
			customerPromotionalOfferDetailsDTO.setError(error);
			customerPromotionalOfferDetailsDTO.setStatus(0);
		}
		return customerPromotionalOfferDetailsDTO;
	}
	
	@RequestMapping({"/GetCustomerPromotionalOrderSummary"})
	public CustomerPromotionalOrderSummaryDTO getCustomerPromotionalOrderSummary(HttpServletRequest request, HttpServletResponse response){
		CustomerPromotionalOrderSummaryDTO customerPromotionalOrderSummaryDTO = new CustomerPromotionalOrderSummaryDTO();
		Error error = new Error();
				
		try{			
			String promoOfferOrderIdStr = request.getParameter("promoOfferOrderId");
			
			if(StringUtils.isEmpty(promoOfferOrderIdStr)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				customerPromotionalOrderSummaryDTO.setError(error);
				customerPromotionalOrderSummaryDTO.setStatus(0);
				return customerPromotionalOrderSummaryDTO;
			}
			Integer promoOfferOrderId = 0;
			try {
				promoOfferOrderId = Integer.parseInt(promoOfferOrderIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid PageNo.");
				error.setDescription("Please provide valid PageNo.");
				customerPromotionalOrderSummaryDTO.setError(error);
				customerPromotionalOrderSummaryDTO.setStatus(0);
				return customerPromotionalOrderSummaryDTO;
			}
			
			RTFPromotionalOfferTracking promoOfferTracking = null;
			Customer customer = null;
			CustomerOrder customerOrder = null;
			promoOfferTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().getCompletedPromoTrackingByOrderId(promoOfferOrderId);
			customer = DAORegistry.getCustomerDAO().get(promoOfferTracking.getCustomerId());
			customerOrder = DAORegistry.getCustomerOrderDAO().get(promoOfferTracking.getOrderId());
			
			customerPromotionalOrderSummaryDTO.setCusPromoOfferTrackingDetailsDTO(com.rtw.tracker.utils.Util.getCustomerPromotionalOfferTrackingSummary(promoOfferTracking, customer, customerOrder));
			customerPromotionalOrderSummaryDTO.setStatus(1);
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Customer Promotional Order Summary");
			customerPromotionalOrderSummaryDTO.setError(error);
			customerPromotionalOrderSummaryDTO.setStatus(0);
		}
		return customerPromotionalOrderSummaryDTO;
	}
	

	@RequestMapping({"/CustomerPromoOfferTrackingExportToExcel"})
	public void customerPromoOfferToExport(HttpServletRequest request, HttpServletResponse response){
				
		try{
			String headerFilter = request.getParameter("headerFilter");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getRTFCustomerPromotionalOfferFilter(headerFilter); 
			
			List<RTFCustomerPromotionalOffer> promoOfferList = DAORegistry.getQueryManagerDAO().getCustomerPromotionalOfferToExport(filter);
			
			if(promoOfferList!=null && !promoOfferList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Customer_Promotional_Offer");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Promotional Offer Id");
				header.createCell(1).setCellValue("Customer First Name");
				header.createCell(2).setCellValue("Last Name");
				header.createCell(3).setCellValue("Email");
				header.createCell(4).setCellValue("Promotional Code");
				header.createCell(5).setCellValue("Discount");
				header.createCell(6).setCellValue("Start Date");
				header.createCell(7).setCellValue("End Date");
				header.createCell(8).setCellValue("Status");
				header.createCell(9).setCellValue("Created Date");
				header.createCell(10).setCellValue("Last Modified Date");
				Integer i=1;
				for(RTFCustomerPromotionalOffer promoOffer : promoOfferList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(promoOffer.getId());
					row.createCell(1).setCellValue(promoOffer.getFirstName());
					row.createCell(2).setCellValue(promoOffer.getLastName()!=null?promoOffer.getLastName():"");
					row.createCell(3).setCellValue(promoOffer.getEmail()!=null?promoOffer.getEmail():"");
					row.createCell(4).setCellValue(promoOffer.getPromoCode()!=null?promoOffer.getPromoCode():"");
					row.createCell(5).setCellValue(promoOffer.getDiscountStr()!=null?promoOffer.getDiscountStr():"");
					row.createCell(6).setCellValue(promoOffer.getStartDateStr()!=null?promoOffer.getStartDateStr():"");
					row.createCell(7).setCellValue(promoOffer.getEndDateStr()!=null?promoOffer.getEndDateStr():"");
					row.createCell(8).setCellValue(promoOffer.getStatus()!=null?promoOffer.getStatus():"");
					row.createCell(9).setCellValue(promoOffer.getCreatedDateStr()!=null?promoOffer.getCreatedDateStr():"");
					row.createCell(10).setCellValue(promoOffer.getModifiedDateStr()!=null?promoOffer.getModifiedDateStr():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Customer_Promotional_Offer.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping({"/PromoOfferTrackingExportToExcel"})
	public void promoOfferTrackingToExport(HttpServletRequest request, HttpServletResponse response){
		
		try{
			String headerFilter = request.getParameter("headerFilter");
			Double sumOrderTotal = 0.00;
			GridHeaderFilters filter = GridHeaderFiltersUtil.getRTFPromotionalOfferTrackingFilter(headerFilter); 
			
			List<RTFPromotionalOfferTracking> promoOfferTrackingList = DAORegistry.getQueryManagerDAO().getPromotionalOfferTrackingToExport(filter);
			
			if(promoOfferTrackingList!=null && !promoOfferTrackingList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Promotional_Offer_Tracking");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Promotional Tracking Id");
				header.createCell(1).setCellValue("Customer First Name");
				header.createCell(2).setCellValue("Last Name");
				header.createCell(3).setCellValue("Email");
				header.createCell(4).setCellValue("Promotional Code");
				header.createCell(5).setCellValue("Discount");
				header.createCell(6).setCellValue("Order Id");
				header.createCell(7).setCellValue("Order Total");
				header.createCell(8).setCellValue("Quantity");
				header.createCell(9).setCellValue("Customer IP Address");
				header.createCell(10).setCellValue("Primary Payment Method");
				header.createCell(11).setCellValue("Secondary Payment Method");
				header.createCell(12).setCellValue("Third Payment Method");
				header.createCell(13).setCellValue("Platform");
				header.createCell(14).setCellValue("Order Creation Date");
				Integer i=1;
				for(RTFPromotionalOfferTracking promoOffer : promoOfferTrackingList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(promoOffer.getId());
					row.createCell(1).setCellValue(promoOffer.getFirstName());
					row.createCell(2).setCellValue(promoOffer.getLastName()!=null?promoOffer.getLastName():"");
					row.createCell(3).setCellValue(promoOffer.getEmail()!=null?promoOffer.getEmail():"");
					row.createCell(4).setCellValue(promoOffer.getPromoCode()!=null?promoOffer.getPromoCode():"");
					row.createCell(5).setCellValue(promoOffer.getDiscount()!=null?promoOffer.getDiscount():"");
					row.createCell(6).setCellValue(promoOffer.getOrderId()!=null?promoOffer.getOrderId():0);
					if(promoOffer.getOrderTotal()!=null){
						sumOrderTotal += promoOffer.getOrderTotal();
						row.createCell(7).setCellValue(promoOffer.getOrderTotal());
					}else{
						sumOrderTotal += 0.00;
						row.createCell(7).setCellValue(0.00);
					}					
					row.createCell(8).setCellValue(promoOffer.getQuantity()!=null?promoOffer.getQuantity():0);
					row.createCell(9).setCellValue(promoOffer.getIpAddress()!=null?promoOffer.getIpAddress():"");
					if(promoOffer.getPrimaryPaymentMethod() == null || promoOffer.getPrimaryPaymentMethod().equalsIgnoreCase("NULL")){
						row.createCell(10).setCellValue("");
					}else{
						row.createCell(10).setCellValue(promoOffer.getPrimaryPaymentMethod());
					}
					if(promoOffer.getSecondaryPaymentMethod() == null || promoOffer.getSecondaryPaymentMethod().equalsIgnoreCase("NULL")){
						row.createCell(11).setCellValue("");
					}else{
						row.createCell(11).setCellValue(promoOffer.getSecondaryPaymentMethod());
					}
					if(promoOffer.getThirdPaymentMethod() == null || promoOffer.getThirdPaymentMethod().equalsIgnoreCase("NULL")){
						row.createCell(12).setCellValue("");
					}else{
						row.createCell(12).setCellValue(promoOffer.getThirdPaymentMethod());
					}
					row.createCell(13).setCellValue(promoOffer.getPlatForm()!=null?promoOffer.getPlatForm().toString():"");
					row.createCell(14).setCellValue(promoOffer.getCreatedDateStr()!=null?promoOffer.getCreatedDateStr():"");
					i++;
				}
				if(promoOfferTrackingList != null && promoOfferTrackingList.size() > 0){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue("");
					row.createCell(1).setCellValue("");
					row.createCell(2).setCellValue("");
					row.createCell(3).setCellValue("");
					row.createCell(4).setCellValue("");
					row.createCell(5).setCellValue("");
					row.createCell(6).setCellValue("");
					row.createCell(7).setCellValue(sumOrderTotal);										
					row.createCell(8).setCellValue("");
					row.createCell(9).setCellValue("");
					row.createCell(10).setCellValue("");
					row.createCell(11).setCellValue("");
					row.createCell(12).setCellValue("");
					row.createCell(13).setCellValue("");
					row.createCell(14).setCellValue("");
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Promotional_Offer_Tracking.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping({"/GetFavouriteEventsForCustomer"})
	public CustomerInfoFavouriteEventDTO getFavouriteEventsForCustomer(HttpServletRequest request, HttpServletResponse response){
		CustomerInfoFavouriteEventDTO customerInfoFavEventDTO = new CustomerInfoFavouriteEventDTO();
		Error error = new Error();
		//model.addAttribute("customerInfoFavEventDTO", customerInfoFavEventDTO);
		
		try{			
			String customerIdStr = request.getParameter("customerId");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			if(StringUtils.isEmpty(customerIdStr)){
				System.err.println("Please provide CustomerId");
				error.setDescription("Please provide CustomerId");
				customerInfoFavEventDTO.setError(error);
				customerInfoFavEventDTO.setStatus(0);
				return customerInfoFavEventDTO;
			}			
			Integer customerId = 0;	
			try{
				customerId = Integer.parseInt(customerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid CustomerId");
				error.setDescription("Please provide valid CustomerId");
				customerInfoFavEventDTO.setError(error);
				customerInfoFavEventDTO.setStatus(0);
				return customerInfoFavEventDTO;
			}
			
			if(StringUtils.isEmpty(pageNo)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				customerInfoFavEventDTO.setError(error);
				customerInfoFavEventDTO.setStatus(0);
				return customerInfoFavEventDTO;
			}
			try {
				Integer.parseInt(pageNo);
			} catch (Exception e) {
				System.err.println("Please provide valid PageNo.");
				error.setDescription("Please provide valid PageNo.");
				customerInfoFavEventDTO.setError(error);
				customerInfoFavEventDTO.setStatus(0);
				return customerInfoFavEventDTO;
			}
			
			GridHeaderFilters filter = null;
			try {
				filter = GridHeaderFiltersUtil.getEventSearchHeaderFilters(headerFilter);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				customerInfoFavEventDTO.setError(error);
				customerInfoFavEventDTO.setStatus(0);
				return customerInfoFavEventDTO;
			}
			
			Integer eventCount = 0;
			Collection<EventDetails> events = null;
			events = DAORegistry.getQueryManagerDAO().getAllActiveFavouriteEventDetailsByFilter(customerId,filter,pageNo);
			if(events != null && events.size() > 0){
				eventCount = events.size();
			}
			
			if(events == null || events.size() <= 0){
				customerInfoFavEventDTO.setMessage("No Favourite Events found for Selected Customer.");
			}
			customerInfoFavEventDTO.setStatus(1);
			customerInfoFavEventDTO.setCustomerFavouriteEventDTO(com.rtw.tracker.utils.Util.getCustomerFavouriteEventArray(events));
			customerInfoFavEventDTO.setFavouriteEventPaginationDTO(PaginationUtil.calculatePaginationParameter(eventCount, pageNo));			
			
			/*JSONObject returnObject = new JSONObject();
			if(events != null && events.size() > 0){
				eventCount = events.size();
				returnObject.put("status", 1);
			}else{
				returnObject.put("msg", "No Favourite Events found for Selected Customer.");
			}
			
			returnObject.put("events", JsonWrapperUtil.getEventArray(events));
			returnObject.put("pagingInfo", PaginationUtil.calculatePaginationParameters(eventCount, pageNo));
			IOUtils.write(returnObject.toString(), response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
		}
		return customerInfoFavEventDTO;
	}
	
	@RequestMapping({"/AddWalletForCustomer"})
	public CustomerAddWalletDTO addWalletForCustomer(HttpServletRequest request, HttpServletResponse response){
		CustomerAddWalletDTO customerAddWalletDTO = new CustomerAddWalletDTO();
		Error error = new Error();
		//model.addAttribute("customerAddWalletDTO", customerAddWalletDTO);
		
		try{
			String customerIdStr = request.getParameter("customerId");
			String customerWalletAmount = request.getParameter("customerWalletAmount");
			String transactionType = request.getParameter("transactionType");
			String userName = request.getParameter("userName");
			
			if(StringUtils.isEmpty(customerIdStr)){
				System.err.println("Please provide CustomerId");
				error.setDescription("Please provide CustomerId");
				customerAddWalletDTO.setError(error);
				customerAddWalletDTO.setStatus(0);
				return customerAddWalletDTO;
			}			
			Integer customerId = 0;	
			try{
				customerId = Integer.parseInt(customerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid CustomerId");
				error.setDescription("Please provide valid CustomerId");
				customerAddWalletDTO.setError(error);
				customerAddWalletDTO.setStatus(0);
				return customerAddWalletDTO;
			}
			
			if(StringUtils.isEmpty(customerWalletAmount)){
				System.err.println("Please provide Wallet Amount");
				error.setDescription("Please provide Wallet Amount");
				customerAddWalletDTO.setError(error);
				customerAddWalletDTO.setStatus(0);
				return customerAddWalletDTO;
			}			
			Double customerWalletAmountDouble = 0.0;	
			try{
				customerWalletAmountDouble = Double.parseDouble(customerWalletAmount);
			}catch (Exception e) {
				System.err.println("Please provide valid Reward Points");
				error.setDescription("Please provide valid Reward Points");
				customerAddWalletDTO.setError(error);
				customerAddWalletDTO.setStatus(0);
				return customerAddWalletDTO;
			}
			
			if(StringUtils.isEmpty(transactionType)){
				System.err.println("Please select Transaction Type");
				error.setDescription("Please select Transaction Type");
				customerAddWalletDTO.setError(error);
				customerAddWalletDTO.setStatus(0);
				return customerAddWalletDTO;
			}
			
			Customer customer  = DAORegistry.getCustomerDAO().get(customerId);
			if(customer == null){
				System.err.println("Customer data not found");
				error.setDescription("Customer data not found");
				customerAddWalletDTO.setError(error);
				customerAddWalletDTO.setStatus(0);
				return customerAddWalletDTO;
			}
			
			Double activeCreditAmount = 0.0;
			Double totalCreditAmount = 0.0;
			Double activeCredit = 0.0;
			Double totalCredit = 0.0;
			CustomerWallet customerWallet = null;		
			customerWallet = DAORegistry.getCustomerWalletDAO().getCustomerWalletByCustomerId(customerId);
			if(customerWallet != null){
				activeCredit = customerWallet.getActiveCredit();
				totalCredit = customerWallet.getTotalCredit();
			}else{
				customerWallet = new CustomerWallet();
				customerWallet.setPendingDebit(0.00);
				customerWallet.setTotalUsedCredit(0.00);
			}
			
			if(transactionType.equalsIgnoreCase("DEBIT")){
				if(activeCredit <= 0){
					System.err.println("Could not update Customer Wallet, Active Credit is 0.");
					error.setDescription("Could not update Customer Wallet, Active Credit is 0.");
					customerAddWalletDTO.setError(error);
					customerAddWalletDTO.setStatus(0);
					return customerAddWalletDTO;
				}
				if(activeCredit > 0 && activeCredit < customerWalletAmountDouble){
					System.err.println("Could not update Customer Wallet, Active Credit less than Specified amount.");
					error.setDescription("Could not update Customer Wallet, Active Credit less than Specified amount.");
					customerAddWalletDTO.setError(error);
					customerAddWalletDTO.setStatus(0);
					return customerAddWalletDTO;
				}
				/*if(totalCredit > 0 && totalCredit < customerWalletAmountDouble){
					System.err.println("Could not update Customer Wallet, Total Credit less than Specified amount.");
					error.setDescription("Could not update Customer Wallet, Total Credit less than Specified amount.");
					customerAddWalletDTO.setError(error);
					customerAddWalletDTO.setStatus(0);
					return customerAddWalletDTO;
				}*/
			}
			
			if(transactionType.equalsIgnoreCase("CREDIT")){
				activeCreditAmount = activeCredit + customerWalletAmountDouble;
				totalCreditAmount = totalCredit + customerWalletAmountDouble;
				customerWallet.setActiveCredit(activeCreditAmount);
				customerWallet.setTotalCredit(totalCreditAmount);
			}
			if(transactionType.equalsIgnoreCase("DEBIT")){
				if(activeCredit > 0){
					activeCreditAmount = activeCredit - customerWalletAmountDouble;
				}
				if(totalCredit > 0){
					totalCreditAmount = totalCredit - customerWalletAmountDouble;
				}
				customerWallet.setActiveCredit(activeCreditAmount);
				customerWallet.setTotalCredit(totalCreditAmount);
			}
								
			customerWallet.setCustomerId(customer.getId());
			customerWallet.setLastUpdated(new Date());
			DAORegistry.getCustomerWalletDAO().saveOrUpdate(customerWallet);
			
			CustomerWalletTracking walletTracking = new CustomerWalletTracking();
			walletTracking.setCustomerId(customer.getId());
			walletTracking.setTransactionType(transactionType);
			walletTracking.setTransactionAmount(customerWalletAmountDouble);
			walletTracking.setCreatedDate(new Date());
			walletTracking.setCreatedBy(userName);
			DAORegistry.getCustomerWalletTrackingDAO().save(walletTracking);
			
			customerAddWalletDTO.setMessage("Customer Wallet Amount updated.");
			customerAddWalletDTO.setStatus(1);
			
			//Tracking User Action
			String userActionMsg = "Customer Wallet Amount "+transactionType+"ED.";
			Util.userActionAudit(request, customer.getId(), userActionMsg);
			
			CustomerWalletDTO walletDTO = new CustomerWalletDTO();
			CustomerWallet wallet = DAORegistry.getCustomerWalletDAO().getCustomerWalletByCustomerId(customerId);
			if(wallet!=null){
				walletDTO.setActiveCredit(String.format("%.2f", wallet.getActiveCredit()));
				walletDTO.setTotalEarnedCredit(String.format("%.2f", wallet.getTotalCredit()));
				walletDTO.setTotalUsedCredit(String.format("%.2f", wallet.getTotalUsedCredit()));
			}else{
				walletDTO.setActiveCredit(String.format("%.2f", 0.00));
				walletDTO.setTotalEarnedCredit(String.format("%.2f", 0.00));
				walletDTO.setTotalUsedCredit(String.format("%.2f", 0.00));				
			}
			customerAddWalletDTO.setCustomerWalletDTO(walletDTO);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return customerAddWalletDTO;
	}
	
	@RequestMapping({"/GetArtistsForCustomer"})
	public CustomerInfoLoyalFanDTO getArtistsForCustomer(HttpServletRequest request, HttpServletResponse response){
		CustomerInfoLoyalFanDTO customerInfoLoyalFanDTO = new CustomerInfoLoyalFanDTO();
		Error error = new Error();
		//model.addAttribute("customerInfoLoyalFanDTO", customerInfoLoyalFanDTO);
		
		try{
			String customerIdStr = request.getParameter("customerId");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			if(StringUtils.isEmpty(customerIdStr)){
				System.err.println("Please provide CustomerId");
				error.setDescription("Please provide CustomerId");
				customerInfoLoyalFanDTO.setError(error);
				customerInfoLoyalFanDTO.setStatus(0);
				return customerInfoLoyalFanDTO;
			}			
			Integer customerId = 0;	
			try{
				customerId = Integer.parseInt(customerIdStr);
			}catch (Exception e) {
				System.err.println("Please provide valid CustomerId");
				error.setDescription("Please provide valid CustomerId");
				customerInfoLoyalFanDTO.setError(error);
				customerInfoLoyalFanDTO.setStatus(0);
				return customerInfoLoyalFanDTO;
			}
			
			if(StringUtils.isEmpty(pageNo)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				customerInfoLoyalFanDTO.setError(error);
				customerInfoLoyalFanDTO.setStatus(0);
				return customerInfoLoyalFanDTO;
			}
			try {
				Integer.parseInt(pageNo);
			} catch (Exception e) {
				System.err.println("Please provide valid PageNo.");
				error.setDescription("Please provide valid PageNo.");
				customerInfoLoyalFanDTO.setError(error);
				customerInfoLoyalFanDTO.setStatus(0);
				return customerInfoLoyalFanDTO;
			}
			
			GridHeaderFilters filter = null;
			try {
				filter = GridHeaderFiltersUtil.getManageArtistSearchHeaderFilters(headerFilter);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				customerInfoLoyalFanDTO.setError(error);
				customerInfoLoyalFanDTO.setStatus(0);
				return customerInfoLoyalFanDTO;
			}
			
			Integer count = 0;
			List<PopularArtist> artistList = DAORegistry.getQueryManagerDAO().getAllArtist(filter, pageNo);
			count = DAORegistry.getQueryManagerDAO().getAllArtistCount(filter);
			
			CustomerLoyalFanDTO customerLoyalFanDTO = new CustomerLoyalFanDTO();
			CustomerLoyalFan loyalFans = DAORegistry.getCustomerLoyalFanDAO().getActiveLoyalFanByCustomerId(customerId);								
			if(loyalFans != null){
				if(loyalFans.getLoyalFanType() != null && loyalFans.getLoyalFanType().equalsIgnoreCase("SPORTS")){
					customerLoyalFanDTO.setArtistName(loyalFans.getLoyalName());
				}else{
					customerLoyalFanDTO.setCityName(loyalFans.getLoyalName()!=null?loyalFans.getLoyalName()+", "+loyalFans.getCountry():"");
				}
				customerLoyalFanDTO.setCategoryName(loyalFans.getLoyalFanType()!=null?loyalFans.getLoyalFanType():"");
				if(loyalFans.getTicketPurchased() != null && loyalFans.getTicketPurchased()){
					customerLoyalFanDTO.setTicketsPurchased("Yes");
				}else{
					customerLoyalFanDTO.setTicketsPurchased("No");
				}
				customerLoyalFanDTO.setStartDate(loyalFans.getStartDateStr()!=null?loyalFans.getStartDateStr():"");
				customerLoyalFanDTO.setEndDate(loyalFans.getEndDateStr()!=null?loyalFans.getEndDateStr():"");
			}
			customerInfoLoyalFanDTO.setLoyalFanDTO(customerLoyalFanDTO);
		
			customerInfoLoyalFanDTO.setStatus(1);
			customerInfoLoyalFanDTO.setPopularArtistCustomDTO(com.rtw.tracker.utils.Util.getAllArtistArray(artistList));
			customerInfoLoyalFanDTO.setPopularArtistPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
		
		}catch(Exception e){
			e.printStackTrace();
		}
		return customerInfoLoyalFanDTO;
	}
	
	@RequestMapping({"/TrackSharingDiscountCode"})
	public SharingDiscountCodeDTO loadTrackSharingDiscountCode(HttpServletRequest request, HttpServletResponse response){
		SharingDiscountCodeDTO sharingDiscountCodeDTO = new SharingDiscountCodeDTO();
		Error error = new Error();
		
		try{
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String fromTimeStr = request.getParameter("fromTime");
			String toTimeStr = request.getParameter("toTime");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			GridHeaderFilters filter = null;
			try {
				filter = GridHeaderFiltersUtil.getDiscountCodeTrackingFilter(headerFilter);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				sharingDiscountCodeDTO.setError(error);
				sharingDiscountCodeDTO.setStatus(0);
				return sharingDiscountCodeDTO;
			}
			
			List<DiscountCodeTracking> discountCodeTrackingList = null;
			Integer count = 0;
			String fromDateFinal = "";
			String toDateFinal = "";
			
			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " "+fromTimeStr+":00:00";
				toDateFinal = toDateStr + " "+toTimeStr+":59:59";
			}else{
				fromDateFinal = null;
				toDateFinal = null;
			}
			discountCodeTrackingList = DAORegistry.getQueryManagerDAO().getDiscountCodeTracking(fromDateFinal, toDateFinal, filter, pageNo);
			count = DAORegistry.getQueryManagerDAO().getDiscountCodeTrackingCount(fromDateFinal, toDateFinal, filter);
			
			if(discountCodeTrackingList == null || discountCodeTrackingList.size() <= 0){
				sharingDiscountCodeDTO.setMessage("No Sharing Discount Code - Tracking Record found.");
			}			
			
			sharingDiscountCodeDTO.setDiscountCodeTrackingDTO(com.rtw.tracker.utils.Util.getDiscountCodeTrackingArray(discountCodeTrackingList));
			sharingDiscountCodeDTO.setShDiscountCodePaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
			sharingDiscountCodeDTO.setStatus(1);
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Sharing Discount Code");
			sharingDiscountCodeDTO.setError(error);
			sharingDiscountCodeDTO.setStatus(0);
		}
		return sharingDiscountCodeDTO;
	}
	
	/*@RequestMapping({"/GetTrackSharingDiscountCode"})
	public SharingDiscountCodeDTO getTrackSharingDiscountCode(HttpServletRequest request, HttpServletResponse response){
		SharingDiscountCodeDTO sharingDiscountCodeDTO = new SharingDiscountCodeDTO();
		Error error = new Error();
		
		try{
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String fromTimeStr = request.getParameter("fromTime");
			String toTimeStr = request.getParameter("toTime");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			if(StringUtils.isEmpty(pageNo)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				sharingDiscountCodeDTO.setError(error);
				sharingDiscountCodeDTO.setStatus(0);
				return sharingDiscountCodeDTO;
			}
			try {
				Integer.parseInt(pageNo);
			} catch (Exception e) {
				System.err.println("Please provide valid PageNo.");
				error.setDescription("Please provide valid PageNo.");
				sharingDiscountCodeDTO.setError(error);
				sharingDiscountCodeDTO.setStatus(0);
				return sharingDiscountCodeDTO;
			}
			
			GridHeaderFilters filter = null;
			try {
				filter = GridHeaderFiltersUtil.getDiscountCodeTrackingFilter(headerFilter);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				sharingDiscountCodeDTO.setError(error);
				sharingDiscountCodeDTO.setStatus(0);
				return sharingDiscountCodeDTO;
			}
			
			List<DiscountCodeTracking> discountCodeTrackingList = null;
			Integer count = 0;
			String fromDateFinal = "";
			String toDateFinal = "";
			
			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " "+fromTimeStr+":00:00";
				toDateFinal = toDateStr + " "+toTimeStr+":59:59";
				discountCodeTrackingList = DAORegistry.getQueryManagerDAO().getDiscountCodeTracking(fromDateFinal, toDateFinal, filter, null);
				count = DAORegistry.getQueryManagerDAO().getDiscountCodeTrackingCount(fromDateFinal, toDateFinal, filter);
			}else{
				discountCodeTrackingList = DAORegistry.getQueryManagerDAO().getDiscountCodeTracking(null, null, filter, null);
				count = DAORegistry.getQueryManagerDAO().getDiscountCodeTrackingCount(null, null, filter);
			}
						
			if(discountCodeTrackingList == null || discountCodeTrackingList.size() <= 0){
				sharingDiscountCodeDTO.setMessage("No Sharing Discount Code - Tracking Record found.");
			}
			sharingDiscountCodeDTO.setDiscountCodeTrackingDTO(com.rtw.tracker.utils.Util.getDiscountCodeTrackingArray(discountCodeTrackingList));
			sharingDiscountCodeDTO.setShDiscountCodePaginationDTO(PaginationUtil.calculatePaginationParameter(count, null));
			sharingDiscountCodeDTO.setStatus(1);
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while Applying Search - Sharing Discount Code");
			sharingDiscountCodeDTO.setError(error);
			sharingDiscountCodeDTO.setStatus(0);
		}
		return sharingDiscountCodeDTO;
	}*/
	

	@RequestMapping({"/TrackSharingDiscountCodeExportToExcel"})
	public void manualFedexToExport(HttpServletRequest request, HttpServletResponse response){
		
		try{
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String fromTimeStr = request.getParameter("fromTime");
			String toTimeStr = request.getParameter("toTime");
			String headerFilter = request.getParameter("headerFilter");
			
			GridHeaderFilters filter = GridHeaderFiltersUtil.getDiscountCodeTrackingFilter(headerFilter);
			List<DiscountCodeTracking> discountCodeTrackingList = null;
			String fromDateFinal = "";
			String toDateFinal = "";
			
			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " "+fromTimeStr+":00:00";
				toDateFinal = toDateStr + " "+toTimeStr+":59:59";
				discountCodeTrackingList = DAORegistry.getQueryManagerDAO().getDiscountCodeTrackingToExport(fromDateFinal, toDateFinal, filter);
			}else{
				discountCodeTrackingList = DAORegistry.getQueryManagerDAO().getDiscountCodeTrackingToExport(null, null, filter);
			}
			
			if(discountCodeTrackingList!=null && !discountCodeTrackingList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Track_Discount_Code_Sharing");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Customer Id");				
				header.createCell(1).setCellValue("First Name");
				header.createCell(2).setCellValue("Last Name");
				header.createCell(3).setCellValue("Email");
				header.createCell(4).setCellValue("Referrer Code");
				header.createCell(5).setCellValue("Facebook");
				header.createCell(6).setCellValue("Twitter");
				header.createCell(7).setCellValue("LinkedIn");
				header.createCell(8).setCellValue("WhatsApp");
				header.createCell(9).setCellValue("Android");
				header.createCell(10).setCellValue("IMessage");
				header.createCell(11).setCellValue("Google");
				header.createCell(12).setCellValue("Outlook");
				Integer i=1;
				for(DiscountCodeTracking discountCodeTrack : discountCodeTrackingList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(discountCodeTrack.getCustomerId());
					row.createCell(1).setCellValue(discountCodeTrack.getFirstName()!=null?discountCodeTrack.getFirstName():"");
					row.createCell(2).setCellValue(discountCodeTrack.getLastName()!=null?discountCodeTrack.getLastName():"");
					row.createCell(3).setCellValue(discountCodeTrack.getEmail()!=null?discountCodeTrack.getEmail():"");
					row.createCell(4).setCellValue(discountCodeTrack.getReferrerCode()!=null?discountCodeTrack.getReferrerCode():"");
					row.createCell(5).setCellValue(discountCodeTrack.getFaceBookCnt()!=null?discountCodeTrack.getFaceBookCnt():0);
					row.createCell(6).setCellValue(discountCodeTrack.getTwitterCnt()!=null?discountCodeTrack.getTwitterCnt():0);
					row.createCell(7).setCellValue(discountCodeTrack.getLinkedinCnt()!=null?discountCodeTrack.getLinkedinCnt():0);
					row.createCell(8).setCellValue(discountCodeTrack.getWhatsappCnt()!=null?discountCodeTrack.getWhatsappCnt():0);
					row.createCell(9).setCellValue(discountCodeTrack.getAndroidCnt()!=null?discountCodeTrack.getAndroidCnt():0);
					row.createCell(10).setCellValue(discountCodeTrack.getImessageCnt()!=null?discountCodeTrack.getImessageCnt():0);
					row.createCell(11).setCellValue(discountCodeTrack.getGoogleCnt()!=null?discountCodeTrack.getGoogleCnt():0);
					row.createCell(12).setCellValue(discountCodeTrack.getOutlookCnt()!=null?discountCodeTrack.getOutlookCnt():0);
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Track_Discount_Code_Sharing.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}