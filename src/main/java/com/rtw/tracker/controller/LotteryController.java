package com.rtw.tracker.controller;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.Artist;
import com.rtw.tracker.datas.ChildCategory;
import com.rtw.tracker.datas.GrandChildCategory;
import com.rtw.tracker.datas.ReferralContest;
import com.rtw.tracker.datas.ReferralContestParticipants;
import com.rtw.tracker.datas.ReferralContestWinner;
import com.rtw.tracker.mail.MailManager;
import com.rtw.tracker.pojos.AutoCompleteArtistAndCategoryDTO;
import com.rtw.tracker.pojos.ReferralContestDTO;
import com.rtw.tracker.pojos.ReferralContestParticipantsDTO;
import com.rtw.tracker.pojos.ReferralContestWinnerDTO;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.GridHeaderFiltersUtil;
import com.rtw.tracker.utils.JsonWrapperUtil;
import com.rtw.tracker.utils.PaginationUtil;
import com.rtw.tmat.utils.Util;

@Controller
public class LotteryController {
	
	MailManager mailManager;	
	public MailManager getMailManager() {
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}

	@RequestMapping(value = "/Lotteries")
	public ReferralContestDTO loadLotteriesPage(HttpServletRequest request, HttpServletResponse response){
		ReferralContestDTO referralContestDTO = new ReferralContestDTO();
		Error error = new Error();
		
		try {
			String headerFilter = request.getParameter("headerFilter");
			
			GridHeaderFilters filter = null;
			try{
				filter = GridHeaderFiltersUtil.getLotteriesFilter(headerFilter);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				referralContestDTO.setError(error);
				referralContestDTO.setStatus(0);
				return referralContestDTO;
			}
			
			List<ReferralContest> contestList = DAORegistry.getReferralContestDAO().getAllContests(null, filter);
			
			referralContestDTO.setStatus(1);
			referralContestDTO.setContestsListDTO(com.rtw.tracker.utils.Util.getLotteriesFilterArray(contestList));
			referralContestDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(contestList!=null?contestList.size():0));
			referralContestDTO.setParticipantsPaginationDTO(PaginationUtil.getDummyPaginationParameters(0));
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Lotteries.");
			referralContestDTO.setError(error);
			referralContestDTO.setStatus(0);
		}
		return referralContestDTO;
	}
	
	/*@RequestMapping(value = "/GetLotteries")
	public ReferralContestDTO getLotteries(HttpServletRequest request, HttpServletResponse response){		
		ReferralContestDTO referralContestDTO = new ReferralContestDTO();
		Error error = new Error();
		
		try {
			String headerFilter = request.getParameter("headerFilter");
				
			GridHeaderFilters filter = null;
			try{
				filter = GridHeaderFiltersUtil.getLotteriesFilter(headerFilter);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				referralContestDTO.setError(error);
				referralContestDTO.setStatus(0);
				return referralContestDTO;
			}
			 
			List<ReferralContest> contestList = DAORegistry.getReferralContestDAO().getAllContests(null, filter);
			
			referralContestDTO.setStatus(1);
			referralContestDTO.setContestsListDTO(com.rtw.tracker.utils.Util.getLotteriesFilterArray(contestList));
			referralContestDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(contestList!=null?contestList.size():0));
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Applying Search - Lotteries.");
			referralContestDTO.setError(error);
			referralContestDTO.setStatus(0);
		}
		return referralContestDTO;
	}*/
	
	@RequestMapping(value = "/UpdateLottery")
	public ReferralContestDTO updateLottery(HttpServletRequest request, HttpServletResponse response){
		ReferralContestDTO referralContestDTO = new ReferralContestDTO();
		Error error = new Error();
		
		try {
			String action = request.getParameter("action");
			String contestIdStr = request.getParameter("contestId");
			String contestName = request.getParameter("contestName");
			String minPurchaseAmountStr = request.getParameter("contestMinPurchaseAmt");				
			String artistIdStr = request.getParameter("artistId");
			String artistName = request.getParameter("artistName");
			String startDateStr = request.getParameter("contestFromDate");
			String endDateStr = request.getParameter("contestToDate");
			String userName = request.getParameter("userName");
			
			if(action != null && action.equalsIgnoreCase("EDIT")){
				
				if(StringUtils.isEmpty(contestIdStr)){
					System.err.println("Please Select Lottery.");
					error.setDescription("Please Select Lottery.");
					referralContestDTO.setError(error);
					referralContestDTO.setStatus(0);
					return referralContestDTO;
				}
				
				GridHeaderFilters filter = new GridHeaderFilters(); 
				List<ReferralContest> contestList = DAORegistry.getReferralContestDAO().getAllContests(Integer.parseInt(contestIdStr), filter);
				
				if(contestList != null && contestList.size() > 0){
					referralContestDTO.setContestsListDTO(com.rtw.tracker.utils.Util.getLotteriesFilterArray(contestList));
				}
				referralContestDTO.setStatus(1);
								
			}else if(action != null && (action.equalsIgnoreCase("SAVE") || action.equalsIgnoreCase("UPDATE"))){
				String userActionMsg = "";
				
				if(StringUtils.isEmpty(contestName)){
					System.err.println("Lottery Name is not found.");
					error.setDescription("Lottery Name is not found.");
					referralContestDTO.setError(error);
					referralContestDTO.setStatus(0);
					return referralContestDTO;
				}
				
				if(StringUtils.isEmpty(minPurchaseAmountStr)){
					System.err.println("Minimum Purchase Amount is not found.");
					error.setDescription("Minimum Purchase Amount is not found.");
					referralContestDTO.setError(error);
					referralContestDTO.setStatus(0);
					return referralContestDTO;
				}
				try{
					Double.parseDouble(minPurchaseAmountStr);
				}catch(Exception e){
					e.printStackTrace();
					error.setDescription("Minimum Purchase Amount is not Valid Double value.");
					referralContestDTO.setError(error);
					referralContestDTO.setStatus(0);
					return referralContestDTO;
				}
				
				if(StringUtils.isEmpty(artistName)){
					System.err.println("Artist Name is not found.");
					error.setDescription("Artist Name is not found.");
					referralContestDTO.setError(error);
					referralContestDTO.setStatus(0);
					return referralContestDTO;
				}
				
				if(StringUtils.isEmpty(artistIdStr)){
					System.err.println("Artist Id is not found.");
					error.setDescription("Artist Id is not found.");
					referralContestDTO.setError(error);
					referralContestDTO.setStatus(0);
					return referralContestDTO;
				}
				Integer artistId = null;
				try{
					artistId = Integer.parseInt(artistIdStr);
				}catch(Exception e){
					e.printStackTrace();
					error.setDescription("Invalid artist id found, it should be valid Integer.");
					referralContestDTO.setError(error);
					referralContestDTO.setStatus(0);
					return referralContestDTO;
				}
								
				if((StringUtils.isEmpty(startDateStr)) || (StringUtils.isEmpty(endDateStr))){
					System.err.println("Missing from Date or To Date.");
					error.setDescription("Missing from Date or To Date.");
					referralContestDTO.setError(error);
					referralContestDTO.setStatus(0);
					return referralContestDTO;
				}
							
				startDateStr = startDateStr + " 00:00:00";
				endDateStr = endDateStr + " 23:59:59";
				Date startDate = null;
				Date endDate = null;
				try {
					startDate = Util.getDateWithTwentyFourHourFormat1(startDateStr);
					endDate = Util.getDateWithTwentyFourHourFormat1(endDateStr);
				} catch (Exception e) {
					e.printStackTrace();
					error.setDescription("Start Date or End Date with bad values.");
					referralContestDTO.setError(error);
					referralContestDTO.setStatus(0);
					return referralContestDTO;
				}
				
				Date today = new Date();
				ReferralContest contest = null;
				
				if(action.equalsIgnoreCase("UPDATE")){
					
					if(StringUtils.isEmpty(contestIdStr)){
						System.err.println("Please select any Lottery to Update.");
						error.setDescription("Please select any Lottery to Update.");
						referralContestDTO.setError(error);
						referralContestDTO.setStatus(0);
						return referralContestDTO;
					}
					Integer contestId = null;
					try{
						contestId = Integer.parseInt(contestIdStr);
					}catch(Exception e){
						e.printStackTrace();
						error.setDescription("Lottery not found, it should be valid Lottery.");
						referralContestDTO.setError(error);
						referralContestDTO.setStatus(0);
						return referralContestDTO;
					}
					
					contest = DAORegistry.getReferralContestDAO().get(contestId);
					if(contest == null){
						System.err.println("Lottery is not found.");
						error.setDescription("Lottery is not found.");
						referralContestDTO.setError(error);
						referralContestDTO.setStatus(0);
						return referralContestDTO;
					}
				}
				
				if(contest == null){
					contest = new ReferralContest();
				}
				contest.setName(contestName);
				contest.setMinimumPurchaseAmount(Double.parseDouble(minPurchaseAmountStr));
				contest.setStartDate(startDate);
				contest.setEndDate(endDate);
				contest.setArtistId(artistId);
				contest.setArtistName(artistName);
				contest.setStatus(true);
				
				if(action.equalsIgnoreCase("SAVE")){
					contest.setCreatedBy(userName);
					contest.setCreatedDate(today);
					contest.setContestStatus("ACTIVE");
				}
				if(action.equalsIgnoreCase("UPDATE")){
					contest.setUpdatedBy(userName);
					contest.setUpdatedDate(today);
				}
				DAORegistry.getReferralContestDAO().saveOrUpdate(contest);
				
				if(action.equalsIgnoreCase("SAVE")){
					userActionMsg = "Lottery Created Successfully.";
					referralContestDTO.setMessage("Lottery Created successfully.");
				}
				if(action.equalsIgnoreCase("UPDATE")){
					userActionMsg = "Lottery Updated Successfully.";
					referralContestDTO.setMessage("Lottery Updated successfully.");
				}
				
				GridHeaderFilters filter = new GridHeaderFilters(); 
				List<ReferralContest> contestList = DAORegistry.getReferralContestDAO().getAllContests(null, filter);
				
				referralContestDTO.setStatus(1);
				referralContestDTO.setContestsListDTO(com.rtw.tracker.utils.Util.getLotteriesFilterArray(contestList));
				referralContestDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(contestList!=null?contestList.size():0));
				
				//Tracking User Action
				Util.userActionAudit(request, contest.getId(), userActionMsg);
							
			}else if(action != null && action.equalsIgnoreCase("DELETE")){
				
				if(StringUtils.isEmpty(contestIdStr)){
					System.err.println("Please select any Lottery to Delete.");
					error.setDescription("Please select any Lottery to Delete.");
					referralContestDTO.setError(error);
					referralContestDTO.setStatus(0);
					return referralContestDTO;
				}
				Integer contestId = null;
				try{
					contestId = Integer.parseInt(contestIdStr);
				}catch(Exception e){
					e.printStackTrace();
					error.setDescription("Lottery not found, it should be valid Lottery.");
					referralContestDTO.setError(error);
					referralContestDTO.setStatus(0);
					return referralContestDTO;
				}
				
				ReferralContest contest =  null;
				contest = DAORegistry.getReferralContestDAO().get(contestId);
				if(contest == null){
					System.err.println("Lottery is not found.");
					error.setDescription("Lottery is not found.");
					referralContestDTO.setError(error);
					referralContestDTO.setStatus(0);
					return referralContestDTO;
				}
				
				contest.setStatus(false);
				contest.setUpdatedBy(userName);
				contest.setUpdatedDate(new Date());
				DAORegistry.getReferralContestDAO().update(contest);
				
				GridHeaderFilters filter = new GridHeaderFilters(); 
				List<ReferralContest> contestList = DAORegistry.getReferralContestDAO().getAllContests(null, filter);
				
				referralContestDTO.setStatus(1);
				referralContestDTO.setMessage("Lottery Deleted successfully.");
				referralContestDTO.setContestsListDTO(com.rtw.tracker.utils.Util.getLotteriesFilterArray(contestList));
				referralContestDTO.setPaginationDTO(PaginationUtil.getDummyPaginationParameters(contestList!=null?contestList.size():0));
				
				//Tracking User Action
				String userActionMsg = "Lottery Deleted Successfully.";
				Util.userActionAudit(request, contest.getId(), userActionMsg);				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching/updating/deleting Lottery.");
			referralContestDTO.setError(error);
			referralContestDTO.setStatus(0);
		}
		return referralContestDTO;
	}
	
	@RequestMapping(value = "/GetParticipants")
	public ReferralContestParticipantsDTO getParticipants(HttpServletRequest request, HttpServletResponse response){
		ReferralContestParticipantsDTO referralContestParticipantsDTO = new ReferralContestParticipantsDTO();
		Error error = new Error();
		
		try{
			String contestIdStr = request.getParameter("contestId");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			if(StringUtils.isEmpty(contestIdStr)){
				System.err.println("Please Select Lottery.");
				error.setDescription("Please Select Lottery.");
				referralContestParticipantsDTO.setError(error);
				referralContestParticipantsDTO.setStatus(0);
				return referralContestParticipantsDTO;
			}
			
			if(StringUtils.isEmpty(pageNo)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				referralContestParticipantsDTO.setError(error);
				referralContestParticipantsDTO.setStatus(0);
				return referralContestParticipantsDTO;
			}
			try {
				Integer.parseInt(pageNo);
			} catch (Exception e) {
				System.err.println("Please provide valid PageNo.");
				error.setDescription("Please provide valid PageNo.");
				referralContestParticipantsDTO.setError(error);
				referralContestParticipantsDTO.setStatus(0);
				return referralContestParticipantsDTO;
			}
			
			GridHeaderFilters filter = null;
			try{
				filter = GridHeaderFiltersUtil.getParticipantsFilter(headerFilter);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				referralContestParticipantsDTO.setError(error);
				referralContestParticipantsDTO.setStatus(0);
				return referralContestParticipantsDTO;
			}
			
			List<ReferralContestParticipants> participantsList = null;			
			participantsList = DAORegistry.getReferralContestParticipantsDAO().getParticipantsListByContestId(Integer.parseInt(contestIdStr), filter);
			
			if(participantsList == null || participantsList.size() <= 0){
				referralContestParticipantsDTO.setMessage("Participant is not found for Selected Lottery/Search Filter");
			}
			referralContestParticipantsDTO.setStatus(1);
			referralContestParticipantsDTO.setContestParticipantsListDTO(com.rtw.tracker.utils.Util.getParticipantFilterArray(participantsList));
			referralContestParticipantsDTO.setParticipantsPaginationDTO(PaginationUtil.getDummyPaginationParameters(participantsList!=null?participantsList.size():0));
						
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while Applying Search - Lottery Participant.");
			referralContestParticipantsDTO.setError(error);
			referralContestParticipantsDTO.setStatus(0);
		}
		return referralContestParticipantsDTO;
	}
	
	@RequestMapping(value = "/GetLotteryWinner")
	public ReferralContestWinnerDTO getLotteryWinner(HttpServletRequest request, HttpServletResponse response){
		ReferralContestWinnerDTO referralContestWinnerDTO = new ReferralContestWinnerDTO();
		Error error = new Error();
		
		try {
			String action = request.getParameter("action");
			String contestIdStr = request.getParameter("contestId");
			String userName = request.getParameter("userName");
			
			if(StringUtils.isEmpty(contestIdStr)){
				System.err.println("Please select any Lottery.");
				error.setDescription("Please select any Lottery.");
				referralContestWinnerDTO.setError(error);
				referralContestWinnerDTO.setStatus(0);
				return referralContestWinnerDTO;
			}
			Integer contestId = 0;
			try{
				contestId = Integer.parseInt(contestIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Lottery not found, it should be valid Lottery.");
				referralContestWinnerDTO.setError(error);
				referralContestWinnerDTO.setStatus(0);
				return referralContestWinnerDTO;
			}
			
			ReferralContestWinner contestWinner = null;
			if(action!=null && action.equalsIgnoreCase("GENERATE")){
				
				contestWinner = DAORegistry.getReferralContestWinnerDAO().getContestWinner(contestId);
				if(contestWinner != null){
					System.err.println("Winner is already selected for selected lottery.");
					error.setDescription("Winner is already selected for selected lottery.");
					referralContestWinnerDTO.setError(error);
					referralContestWinnerDTO.setStatus(0);
					return referralContestWinnerDTO;
				}
				
				GridHeaderFilters filter = new GridHeaderFilters();
				
				List<ReferralContestParticipants> participants = null;
				participants = DAORegistry.getReferralContestParticipantsDAO().getParticipantsListByContestId(contestId,filter);				
				if(participants == null){
					System.err.println("No Participants found for selected lottery.");
					error.setDescription("No Participants found for selected lottery.");
					referralContestWinnerDTO.setError(error);
					referralContestWinnerDTO.setStatus(0);
					return referralContestWinnerDTO;
				}
				
				ReferralContest contest = DAORegistry.getReferralContestDAO().get(contestId);
				List<String> cities = null;
				cities = DAORegistry.getQueryManagerDAO().getArtistCities(contest.getArtistId());
				if(cities == null){
					System.err.println("No artist perfoming city list found for selected lottery artist.");
					error.setDescription("No artist perfoming city list found for selected lottery artist.");
					referralContestWinnerDTO.setError(error);
					referralContestWinnerDTO.setStatus(0);
					return referralContestWinnerDTO;
				}
				
				Random random = new Random();
				Integer index = random.nextInt(participants.size());
				ReferralContestParticipants winner = participants.get(index);
				contestWinner = new ReferralContestWinner();
				contestWinner.setContestId(winner.getContestId());
				contestWinner.setParticipantId(winner.getId());
				contestWinner.setCreatedOn(new Date());
				contestWinner.setCreatedBy(userName);
				contestWinner.setIsEmailed(false);
				DAORegistry.getReferralContestWinnerDAO().save(contestWinner);
				
				Boolean isMailSent = false;
				
				com.rtw.tracker.mail.MailAttachment[] mailAttachment = new com.rtw.tracker.mail.MailAttachment[3];
				mailAttachment[0] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","top.png",com.rtw.tmat.utils.Util.getFilePath(request, "top.png", "/resources/images/"));
				mailAttachment[1] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","bottom.png",com.rtw.tmat.utils.Util.getFilePath(request, "bottom.png", "/resources/images/"));
				mailAttachment[2] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","rtflogo1.png",com.rtw.tmat.utils.Util.getFilePath(request, "rtflogo1.png", "/resources/images/"));
				
				Map<String, Object> mailMap = new HashMap<String, Object>();
				mailMap.put("customerName",winner.getCustomerName());
				mailMap.put("contestName",contest.getName());
				mailMap.put("artistName",contest.getArtistName().toUpperCase());
				mailMap.put("cities",cities);
				mailMap.put("rtfUrl","https://www.rewardthefan.com/ReferralContest?contestId="+contestId+"&city=");
				
				
				try {
					mailManager.sendMailNow("text/html","sales@rewardthefan.com",winner.getCustomerEmail(), 
							null,"msanghani@rightthisway.com", "Congratulations you are a "+contest.getArtistName().toUpperCase()+" Lottery winner!" ,
				    		"email-contest-winner.html", mailMap, "text/html", null,mailAttachment,null);
				
				} catch (Exception e) {
					isMailSent = false;
					e.printStackTrace();
				}
				
				List<ReferralContest> contests = DAORegistry.getReferralContestDAO().getAllActiveContests();
				StringBuffer contestString = new StringBuffer();
				for(ReferralContest con : contests){
					contestString.append("<p><b>"+con.getName()+"</b><br/><br/></p>");
				}
				
				for(ReferralContestParticipants parti : participants){
					if(parti.getCustomerEmail().equalsIgnoreCase(winner.getCustomerEmail())){
						continue;
					}
					Map<String, Object> allMap = new HashMap<String, Object>();
					allMap.put("customerName",parti.getCustomerName());
					allMap.put("artistName",contest.getArtistName().toUpperCase());
					allMap.put("contestName",contest.getName());
					allMap.put("email",winner.getCustomerName());
					allMap.put("contestString",contestString);
					try {
						mailManager.sendMailNow("text/html","sales@rewardthefan.com",parti.getCustomerEmail(), 
								null,"msanghani@rightthisway.com",contest.getArtistName().toUpperCase()+" Lottery winner announced!",
					    		"email-contest-winner1.html", allMap, "text/html", null,mailAttachment,null);
						isMailSent = true;
					} catch (Exception e) {
						isMailSent = false;
						e.printStackTrace();
					}
				}
				
				if(isMailSent){
					contestWinner.setIsEmailed(true);
					DAORegistry.getReferralContestWinnerDAO().update(contestWinner);
				}
				
				referralContestWinnerDTO.setMessage("Lottery Winner Announced.");
				
				//Tracking User Action
				String userActionMsg = "Lottery Winner Announced. Winner Id is - "+winner.getId();
				Util.userActionAudit(request, contest.getId(), userActionMsg);
				
			}
			contestWinner = DAORegistry.getReferralContestWinnerDAO().getContestWinnerDetails(contestId);
			if(contestWinner == null){
				System.err.println("Winner is not declared yet, Please choose winner first.");
				error.setDescription("Winner is not declared yet, Please choose winner first.");
				referralContestWinnerDTO.setError(error);
				referralContestWinnerDTO.setStatus(0);
				return referralContestWinnerDTO;
			}
			
			referralContestWinnerDTO.setStatus(1);
			referralContestWinnerDTO.setContestWinnerDTO(com.rtw.tracker.utils.Util.getReferralContestWinnerObject(contestWinner));
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching/generating Lottery Winner.");
			referralContestWinnerDTO.setError(error);
			referralContestWinnerDTO.setStatus(0);
		}
		return referralContestWinnerDTO;
	}
	
	@RequestMapping("/AutoCompleteArtistAndChildAndGrandChildCategory")
	public AutoCompleteArtistAndCategoryDTO getAutoCompleteArtistAndChildAndGrandChildCategory(HttpServletRequest request, HttpServletResponse response) throws Exception {
		AutoCompleteArtistAndCategoryDTO autoCompleteArtistAndCategoryDTO = new AutoCompleteArtistAndCategoryDTO();
		Error error = new Error();
		
		try{
			String param = request.getParameter("q");
			String strResponse = "";
			
			Collection<GrandChildCategory> grandChildCategories = DAORegistry.getGrandChildCategoryDAO().getGrandChildCategoriesByNameAndDisplayOnSearch(param);
			Collection<ChildCategory> childCategories = DAORegistry.getChildCategoryDAO().getChildCategoriesByNameAndDisplayOnSearch(param);
			Collection<Artist> artists = DAORegistry.getArtistDAO().filterByName(param);		
					
			if (childCategories != null) {
				for (ChildCategory childCategory : childCategories) {
					strResponse += ("CHILD" + "|" + childCategory.getId() + "|" + childCategory.getName() + "\n");
				}
			}
			
			if (grandChildCategories != null) {
				for (GrandChildCategory grandChildCateogry : grandChildCategories) {
					strResponse += ("GRAND" + "|" + grandChildCateogry.getId() + "|" + grandChildCateogry.getName() + "\n");
				}
			}
			
			if (artists != null) {
				for (Artist artist : artists) {
					strResponse += ("ARTIST" + "|" + artist.getId() + "|" + artist.getName() + "\n") ;
				}
			}		
			autoCompleteArtistAndCategoryDTO.setStatus(1);
			autoCompleteArtistAndCategoryDTO.setArtistAndCategory(strResponse);
			
		}catch(Exception e){
			e.printStackTrace();
			System.err.println(e.getLocalizedMessage());
			error.setDescription("Something went wrong while fetching Auto Complete Artist and Categories.");
			autoCompleteArtistAndCategoryDTO.setError(error);
			autoCompleteArtistAndCategoryDTO.setStatus(0);
		}
		return autoCompleteArtistAndCategoryDTO;
	}
	
}
