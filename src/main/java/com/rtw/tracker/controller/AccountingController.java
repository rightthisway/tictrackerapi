package com.rtw.tracker.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.google.common.io.ByteStreams;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtw.tmat.utils.CustomerDetails;
import com.rtw.tmat.utils.Error;
import com.rtw.tmat.utils.StripeCredentials;
import com.rtw.tmat.utils.StripeTransaction;
import com.rtw.tmat.utils.TextUtil;
import com.rtw.tmat.utils.Util;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.aws.AWSFileService;
import com.rtw.tracker.aws.AwsS3Response;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.CategoryTicket;
import com.rtw.tracker.datas.CategoryTicketGroup;
import com.rtw.tracker.datas.Country;
import com.rtw.tracker.datas.Customer;
import com.rtw.tracker.datas.CustomerAddress;
import com.rtw.tracker.datas.CustomerLoyalty;
import com.rtw.tracker.datas.CustomerLoyaltyHistory;
import com.rtw.tracker.datas.CustomerOrder;
import com.rtw.tracker.datas.CustomerOrderDetails;
import com.rtw.tracker.datas.CustomerTicketDownloads;
import com.rtw.tracker.datas.CustomerWallet;
import com.rtw.tracker.datas.EventDetails;
import com.rtw.tracker.datas.Invoice;
import com.rtw.tracker.datas.InvoiceAudit;
import com.rtw.tracker.datas.InvoiceRefund;
import com.rtw.tracker.datas.InvoiceRefundResponse;
import com.rtw.tracker.datas.InvoiceTicketAttachment;
import com.rtw.tracker.datas.ManualFedexGeneration;
import com.rtw.tracker.datas.OpenOrders;
import com.rtw.tracker.datas.OrderTicketGroupDetails;
import com.rtw.tracker.datas.POSCategoryTicket;
import com.rtw.tracker.datas.POSCategoryTicketGroup;
import com.rtw.tracker.datas.POSCustomerOrder;
import com.rtw.tracker.datas.POSPurchaseOrder;
import com.rtw.tracker.datas.POSTicket;
import com.rtw.tracker.datas.POSTicketGroup;
import com.rtw.tracker.datas.POStatus;
import com.rtw.tracker.datas.PaymentHistory;
import com.rtw.tracker.datas.PurchaseOrder;
import com.rtw.tracker.datas.PurchaseOrderPaymentDetails;
import com.rtw.tracker.datas.PurchaseOrders;
import com.rtw.tracker.datas.RealTicketSectionDetails;
import com.rtw.tracker.datas.ReferralCodeValidation;
import com.rtw.tracker.datas.ShippingMethod;
import com.rtw.tracker.datas.State;
import com.rtw.tracker.datas.Ticket;
import com.rtw.tracker.datas.TicketGroup;
import com.rtw.tracker.datas.TicketGroupTicketAttachment;
import com.rtw.tracker.datas.TicketStatus;
import com.rtw.tracker.datas.TrackerUser;
import com.rtw.tracker.datas.WalletTransaction;
/*import com.rtw.tracker.data.CategoryTicket;
import com.rtw.tracker.data.CategoryTicketGroup;

import com.rtw.tracker.data.Customer;
import com.rtw.tracker.data.CustomerAddress;
import com.rtw.tracker.data.CustomerLoyalty;
import com.rtw.tracker.data.CustomerLoyaltyHistory;
import com.rtw.tracker.data.CustomerOrder;
import com.rtw.tracker.data.CustomerOrderDetails;
import com.rtw.tracker.data.CustomerTicketDownloads;
import com.rtw.tracker.data.CustomerWallet;
import com.rtw.tracker.data.EventDetails;
import com.rtw.tracker.data.FileValidator;
import com.rtw.tracker.data.Invoice;
import com.rtw.tracker.data.InvoiceAudit;
import com.rtw.tracker.data.InvoiceRefund;
import com.rtw.tracker.data.InvoiceRefundResponse;
import com.rtw.tracker.data.InvoiceTicketAttachment;
import com.rtw.tracker.data.OpenOrders;
import com.rtw.tracker.data.OrderTicketGroupDetails;
import com.rtw.tracker.data.POSCategoryTicket;
import com.rtw.tracker.data.POSCategoryTicketGroup;
import com.rtw.tracker.data.POSCustomerOrder;
import com.rtw.tracker.data.POSPurchaseOrder;
import com.rtw.tracker.data.POSTicket;
import com.rtw.tracker.data.POSTicketGroup;
import com.rtw.tracker.data.POStatus;
import com.rtw.tracker.data.PaymentHistory;
import com.rtw.tracker.data.ProductType;
import com.rtw.tracker.data.PurchaseOrder;
import com.rtw.tracker.data.PurchaseOrderPaymentDetails;
import com.rtw.tracker.data.PurchaseOrders;
import com.rtw.tracker.data.RealTicketSectionDetails;
import com.rtw.tracker.data.ReferralCodeValidation;
import com.rtw.tracker.data.ShippingMethod;

import com.rtw.tracker.data.Ticket;
import com.rtw.tracker.data.TicketGroup;
import com.rtw.tracker.data.TicketGroupTicketAttachment;
import com.rtw.tracker.data.TicketStatus;
import com.rtw.tracker.data.TrackerUser;
import com.rtw.tracker.data.WalletTransaction;*/
import com.rtw.tracker.enums.CodeType;
import com.rtw.tracker.enums.FileType;
import com.rtw.tracker.enums.InvoiceAuditAction;
import com.rtw.tracker.enums.InvoiceStatus;
import com.rtw.tracker.enums.OrderStatus;
import com.rtw.tracker.enums.PartialPaymentMethod;
import com.rtw.tracker.enums.PaymentMethod;
import com.rtw.tracker.enums.PaymentMethods;
import com.rtw.tracker.enums.RewardStatus;
import com.rtw.tracker.fedex.CreateFedexLabelUtil;
import com.rtw.tracker.fedex.Property;
import com.rtw.tracker.fedex.Recipient;
import com.rtw.tracker.fedex.ServiceType;
import com.rtw.tracker.fedex.SignatureOptionType;
import com.rtw.tracker.mail.MailManager;
import com.rtw.tracker.pojos.AccountReceivableDetailsDTO;
import com.rtw.tracker.pojos.CreatePayInvoiceDTO;
import com.rtw.tracker.pojos.CustomerOrderDetailsDTO;
import com.rtw.tracker.pojos.CustomerOrderDetailsInfoDTO;
import com.rtw.tracker.pojos.CustomersCustomDTO;
import com.rtw.tracker.pojos.GenericResponseDTO;
import com.rtw.tracker.pojos.InvoiceCsrDTO;
import com.rtw.tracker.pojos.InvoiceCustomerOrderAddressDTO;
import com.rtw.tracker.pojos.InvoiceDTO;
import com.rtw.tracker.pojos.InvoiceDetailsDTO;
import com.rtw.tracker.pojos.InvoiceDownloadTicketDTO;
import com.rtw.tracker.pojos.InvoiceEditDTO;
import com.rtw.tracker.pojos.InvoiceGetLongTicketDTO;
import com.rtw.tracker.pojos.InvoiceNoteDTO;
import com.rtw.tracker.pojos.InvoicePaymentDetailsDTO;
import com.rtw.tracker.pojos.InvoiceRefundDTO;
import com.rtw.tracker.pojos.InvoiceRefundDetailsDTO;
import com.rtw.tracker.pojos.InvoiceStatusChangeDTO;
import com.rtw.tracker.pojos.InvoiceTicketCheckFileNameDTO;
import com.rtw.tracker.pojos.POCreateDTO;
import com.rtw.tracker.pojos.POCsrDTO;
import com.rtw.tracker.pojos.POGetShippingAddressForFedexDTO;
import com.rtw.tracker.pojos.PONotesDTO;
import com.rtw.tracker.pojos.POPaymentDetailsDTO;
import com.rtw.tracker.pojos.POTicketGroupOnHandStatusDTO;
import com.rtw.tracker.pojos.POUpdateDTO;
import com.rtw.tracker.pojos.POUploadETicketDTO;
import com.rtw.tracker.pojos.PurchaseOrderDetailsDTO;
import com.rtw.tracker.pojos.RelatedPOInvoicesDTO;
import com.rtw.tracker.pojos.TicketDownloadNotification;
import com.rtw.tracker.pojos.ValidateReferralCodeDTO;
import com.rtw.tracker.utils.Constants;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.GridHeaderFiltersUtil;
import com.rtw.tracker.utils.PDFUtil;
import com.rtw.tracker.utils.PaginationUtil;
import com.rtw.tracker.utils.URLUtil;

@Controller
public class AccountingController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AccountingController.class);
	
	
	MailManager mailManager;	
	public MailManager getMailManager() {
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}
	
	SharedProperty sharedProperty;
	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}

	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}

	
	@RequestMapping(value = "/CreateManualFedexLabel")
	public GenericResponseDTO createManualFedexLabel(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		
		try {
			String blFirstName = request.getParameter("manualFedex_blFirstName");
			String blLastName = request.getParameter("manualFedex_blLastName");		
			String blCompanyName = request.getParameter("manualFedex_blCompanyName");
			String blStreet1 = request.getParameter("manualFedex_blStreet1");
			String blStreet2 = request.getParameter("manualFedex_blStreet2");
			String blCity = request.getParameter("manualFedex_blCity");
			String blState = request.getParameter("manualFedex_blStateName");
			String blCountry = request.getParameter("manualFedex_blCountryName");
			String blZipCode = request.getParameter("manualFedex_blZipCode");
			String blPhone = request.getParameter("manualFedex_blPhone");
			String shCustomerName = request.getParameter("manualFedex_shCustomerName");
			String shCompanyName = request.getParameter("manualFedex_shCompanyName");
			String shStreet1 = request.getParameter("manualFedex_shStreet1");
			String shStreet2 = request.getParameter("manualFedex_shStreet2");
			String shCity = request.getParameter("manualFedex_shCity");
			String shState = request.getParameter("manualFedex_shStateName");
			String shCountry = request.getParameter("manualFedex_shCountryName");
			String shZipCode = request.getParameter("manualFedex_shZipCode");
			String shPhone = request.getParameter("manualFedex_shPhone");
			String serviceType = request.getParameter("manualFedex_serviceType");
			String signatureType = request.getParameter("manualFedex_signatureType");
			String userName = request.getParameter("userName");
						
			if(StringUtils.isEmpty(blFirstName)){
				System.err.println("Billing - First Name is mandatory.");
				error.setDescription("Billing - First Name is mandatory.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(blLastName)){
				System.err.println("Billing - Last Name is mandatory.");
				error.setDescription("Billing - Last Name is mandatory.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(blCompanyName)){
				System.err.println("From Address - Company Name is mandatory.");
				error.setDescription("From Address - Company Name is mandatory.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(blStreet1)){
				System.err.println("From Address - Street1 is mandatory.");
				error.setDescription("From Address - Street1 is mandatory.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(blCity)){
				System.err.println("From Address - City is mandatory.");
				error.setDescription("From Address - City is mandatory.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(blState)){
				System.err.println("From Address - State is mandatory.");
				error.setDescription("From Address - State is mandatory.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(blCountry)){
				System.err.println("From Address - Country is mandatory.");
				error.setDescription("From Address - Country is mandatory.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(blZipCode)){
				System.err.println("From Address - ZipCode is mandatory.");
				error.setDescription("From Address - ZipCode is mandatory.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(blPhone)){
				System.err.println("From Address - Phone is mandatory.");
				error.setDescription("From Address - Phone is mandatory.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(shCustomerName) && StringUtils.isEmpty(shCompanyName)){
				System.err.println("Shipping - Full Name/Company Name is mandatory.");
				error.setDescription("Shipping - Full Name/Company Name is mandatory.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			/*if(StringUtils.isEmpty(shLastName)){
				System.err.println("Shipping - Last Name is mandatory.");
				error.setDescription("Shipping - Last Name is mandatory.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}*/
			if(StringUtils.isEmpty(shStreet1)){
				System.err.println("Shipping - Street1 is mandatory.");
				error.setDescription("Shipping - Street1 is mandatory.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(shCity)){
				System.err.println("Shipping - City is mandatory.");
				error.setDescription("Shipping - City is mandatory.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(shState)){
				System.err.println("Shipping - State is mandatory.");
				error.setDescription("Shipping - State is mandatory.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(shCountry)){
				System.err.println("Shipping - Country is mandatory.");
				error.setDescription("Shipping - Country is mandatory.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(shZipCode)){
				System.err.println("Shipping - ZipCode is mandatory.");
				error.setDescription("Shipping - ZipCode is mandatory.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(shPhone)){
				System.err.println("Shipping - Phone is mandatory.");
				error.setDescription("Shipping - Phone is mandatory.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(serviceType) || serviceType.equalsIgnoreCase("select")){
				System.err.println("Service Type not found.");
				error.setDescription("Service Type not found.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(signatureType)){
				System.err.println("Signature Type not found.");
				error.setDescription("Signature Type not found.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}

			SignatureOptionType signatureOptionType = null;
			Country country = null;
			State state = null;
			String trackingNo="";
			String msg="";
			Integer invoiceId = 0;								
						
			signatureOptionType = SignatureOptionType.fromString(signatureType);
			
			country = DAORegistry.getCountryDAO().get(Integer.parseInt(shCountry));
			state = DAORegistry.getStateDAO().get(Integer.parseInt(shState));
			
			Recipient recipientObj = new Recipient();
			recipientObj.setInvoiceId(1);
			recipientObj.setRecipientName(shCustomerName);
			recipientObj.setCompanyName(shCompanyName);
			recipientObj.setAddressStreet1(shStreet1);
			recipientObj.setAddressStreet2(shStreet2);
			recipientObj.setCity(shCity);
			recipientObj.setCountryCode(country.getDesc());
			recipientObj.setStateProvinceCode(state.getShortDesc());
			recipientObj.setPostalCode(shZipCode);
			recipientObj.setPhoneNumber(shPhone);
			
			ManualFedexGeneration manualFedex = new ManualFedexGeneration();
			manualFedex.setBlFirstName(blFirstName);
			manualFedex.setBlLastName(blLastName);
			manualFedex.setBlCompanyName(blCompanyName);
			manualFedex.setBlAddress1(blStreet1);
			manualFedex.setBlAddress2(blStreet2);
			manualFedex.setBlCity(blCity);
			country = DAORegistry.getCountryDAO().get(Integer.parseInt(blCountry));
			manualFedex.setBlCountry(country.getId());
			manualFedex.setBlCountryName(country.getName());
			state = DAORegistry.getStateDAO().get(Integer.parseInt(blState));
			manualFedex.setBlState(state.getId());
			manualFedex.setBlStateName(state.getName());
			manualFedex.setBlZipCode(blZipCode);
			manualFedex.setBlPhone(blPhone);
			manualFedex.setShCustomerName(shCustomerName);
			manualFedex.setShCompanyName(shCompanyName);
			manualFedex.setShAddress1(shStreet1);
			manualFedex.setShAddress2(shStreet2);
			manualFedex.setShCity(shCity);
			country = DAORegistry.getCountryDAO().get(Integer.parseInt(shCountry));
			manualFedex.setShCountry(country.getId());
			manualFedex.setShCountryName(country.getName());
			state = DAORegistry.getStateDAO().get(Integer.parseInt(shState));
			manualFedex.setShState(state.getId());
			manualFedex.setShStateName(state.getName());
			manualFedex.setShZipCode(shZipCode);
			manualFedex.setShPhone(shPhone);
			manualFedex.setServiceType(serviceType);
			manualFedex.setSignatureType(signatureType);
			
			CreateFedexLabelUtil util = new CreateFedexLabelUtil();
			if(recipientObj.getCountryCode()==null){				
				error.setDescription("Could not create Fedex label, No country found for selected invoice.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}else if(recipientObj.getCountryCode().equalsIgnoreCase("US")){													
				ServiceType service =  ServiceType.fromValue(serviceType);
				msg = util.createManualFedExShipLabel(manualFedex,recipientObj,service,signatureOptionType,userName);// package type = your package					
	    	}else{
	    		ServiceType service =  ServiceType.fromValue(serviceType);
	    		msg = CreateFedexLabelUtil.createManualFedExShipLabel(manualFedex,recipientObj,service,signatureOptionType,userName);
	    	}			 
			 
			if(msg != null && msg != ""){
				error.setDescription(msg);
				genericResponseDTO.setError(error) ;
				genericResponseDTO.setStatus(0);
			}
			if(msg==null){								 
				genericResponseDTO.setStatus(1);
				genericResponseDTO.setMessage("Fedex Label Created with tracking no : "+manualFedex.getTrackingNumber());
				
				//Tracking User Action
				String userActionMsg = "Fedex Label is Created for Manual Fedex. Tracking no - "+manualFedex.getTrackingNumber();
				Util.userActionAudit(request, manualFedex.getId(), userActionMsg);
			}
			
		} catch (Exception e) {
			System.out.println("FedEx: "+e);
			e.printStackTrace();
			error.setDescription("Something went wrong while Creating Manual Fedex Label.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
		
	@RequestMapping(value = "/RemoveFedexLabel")
	public GenericResponseDTO removeFedexLabel(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		
		try {
			String invoiceIdStr = request.getParameter("invoiceId");
			String poIdStr = request.getParameter("poId");
			String manualFedexIdStr = request.getParameter("fedexId");
			String userName = request.getParameter("userName");
			
			String fileWithPath=null;
			String msg="";			
			
			if(invoiceIdStr!=null && !invoiceIdStr.isEmpty()){
				CreateFedexLabelUtil util = new CreateFedexLabelUtil();
				Invoice invoice = DAORegistry.getInvoiceDAO().get(Integer.parseInt(invoiceIdStr));
				if(invoice == null){					
					System.err.println("Invoice Details not found in system.");
					error.setDescription("Invoice Details not found in system.");
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					return genericResponseDTO;
				}
				else if(invoice.getTrackingNo() == null || invoice.getTrackingNo().isEmpty()){
					System.err.println("Invoice - Fedex Tracking No. not found.");
					error.setDescription("Invoice - Fedex Tracking No. not found.");
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					return genericResponseDTO;
				}				
				else{
					msg = util.deleteShipment(invoice, userName);
					genericResponseDTO.setMessage(msg) ;
					genericResponseDTO.setStatus(1);
					
					//Tracking User Action
					String userActionMsg = "Fedex Label is Removed from Invoice.";
					Util.userActionAudit(request, invoice.getId(), userActionMsg);
				}
			}
			if(poIdStr != null && !poIdStr.isEmpty()){
				CreateFedexLabelUtil util = new CreateFedexLabelUtil();
				PurchaseOrder purchaseOrder = DAORegistry.getPurchaseOrderDAO().get(Integer.parseInt(poIdStr));
				if(purchaseOrder == null){
					System.err.println("Purchase Order Details not found in system.");
					error.setDescription("Purchase Order Details not found in system.");
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					return genericResponseDTO;
				}
				else if(purchaseOrder.getTrackingNo() == null || purchaseOrder.getTrackingNo().isEmpty()){
					System.err.println("Purchase Order - Fedex Tracking No. not found.");
					error.setDescription("Purchase Order - Fedex Tracking No. not found.");
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					return genericResponseDTO;
				}
				else{
					msg = util.deletePOShipment(purchaseOrder);
					genericResponseDTO.setMessage(msg) ;
					genericResponseDTO.setStatus(1);
					
					//Tracking User Action
					String userActionMsg = "Fedex Label is Removed from PO.";
					Util.userActionAudit(request, purchaseOrder.getId(), userActionMsg);
				}
			}
			if(manualFedexIdStr != null && !manualFedexIdStr.isEmpty()){
				CreateFedexLabelUtil util = new CreateFedexLabelUtil();
				ManualFedexGeneration manualFedex = DAORegistry.getManualFedexGenerationDAO().get(Integer.parseInt(manualFedexIdStr));
				if(manualFedex == null){
					System.err.println("Manual Fedex Details not found in system.");
					error.setDescription("Manual Fedex Details not found in system.");
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					return genericResponseDTO;
				}				
				else if(manualFedex.getTrackingNumber() == null || manualFedex.getTrackingNumber().isEmpty()){
					System.err.println("Manual - Fedex Tracking No not found.");
					error.setDescription("Manual - Fedex Tracking No not found.");
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					return genericResponseDTO;
				}
				else{
					msg = util.deleteManualFedexShipment(manualFedex, userName);
					genericResponseDTO.setStatus(1);
					genericResponseDTO.setMessage(msg);
										
					//Tracking User Action
					String userActionMsg = "Fedex Label is Removed from Manual Fedex.";
					Util.userActionAudit(request, Integer.parseInt(manualFedexIdStr), userActionMsg);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Remove Fedex Label.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
	@RequestMapping(value = "/CheckFedexLabelGenerated")
	public GenericResponseDTO checkFedexLabelGenerated(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		
		try {
			String invoiceIdStr = request.getParameter("invoiceId");
			String poIdStr = request.getParameter("poId");
			String manualFedexIdStr = request.getParameter("id");
			
			String fileWithPath=null;
			String msg="";
			
			if(invoiceIdStr!=null && !invoiceIdStr.isEmpty()){
				Invoice invoice = DAORegistry.getInvoiceDAO().get(Integer.parseInt(invoiceIdStr));
				Property property = DAORegistry.getPropertyDAO().get(com.rtw.tmat.utils.Constants.FEDEX_LABEL_DIRECTORY);
				if(invoice.getTrackingNo()!=null && !invoice.getTrackingNo().isEmpty()){
					if(property!=null && property.getValue()!=null && !property.getValue().isEmpty()){
						fileWithPath = property.getValue()+File.separator+invoice.getId()+"_"+invoice.getTrackingNo();
						File file = new File(fileWithPath+".pdf");
						if(file.exists()){
							msg="OK";
						}
					}
				}
			}
			if(poIdStr!=null && !poIdStr.isEmpty()){
				PurchaseOrder purchaseOrder = DAORegistry.getPurchaseOrderDAO().get(Integer.parseInt(poIdStr));
				Property property = DAORegistry.getPropertyDAO().get(com.rtw.tmat.utils.Constants.FEDEX_LABEL_DIRECTORY);
				if(purchaseOrder.getTrackingNo()!=null && !purchaseOrder.getTrackingNo().isEmpty()){
					if(property!=null && property.getValue()!=null && !property.getValue().isEmpty()){
						fileWithPath = property.getValue()+File.separator+purchaseOrder.getId()+"_"+purchaseOrder.getTrackingNo();
						File file = new File(fileWithPath+".pdf");
						if(file.exists()){
							msg="OK";
						}
					}
				}
			}
			if(manualFedexIdStr!=null && !manualFedexIdStr.isEmpty()){
				ManualFedexGeneration manualFedex = DAORegistry.getManualFedexGenerationDAO().get(Integer.parseInt(manualFedexIdStr));
				Property property = DAORegistry.getPropertyDAO().get(com.rtw.tmat.utils.Constants.FEDEX_LABEL_MANUAL_DIRECTORY);
				if(manualFedex.getTrackingNumber()!=null && !manualFedex.getTrackingNumber().isEmpty()){
					if(property!=null && property.getValue()!=null && !property.getValue().isEmpty()){
						//fileWithPath = property.getValue()+File.separator+manualFedex.getId()+"_"+manualFedex.getTrackingNumber();
						fileWithPath = property.getValue()+File.separator+"1_"+manualFedex.getTrackingNumber();
						File file = new File(fileWithPath+".pdf");
						if(file.exists()){
							msg="OK";
						}
					}
				}
			}
			genericResponseDTO.setStatus(1);
			genericResponseDTO.setMessage(msg);
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Checking Fedex Label Generated.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}

	@RequestMapping(value = "/GetFedexLabelPdf")
	public void getFedexLabelPdf(HttpServletRequest request, HttpServletResponse response){
		
		try {
			String invoiceIdStr = request.getParameter("invoiceId");
			String poIdStr = request.getParameter("poId");
			String manualFedexIdStr = request.getParameter("id");
			
			String fileWithPath=null;
			if(invoiceIdStr!=null && !invoiceIdStr.isEmpty()){
				Invoice invoice = DAORegistry.getInvoiceDAO().get(Integer.parseInt(invoiceIdStr));
				Property property = DAORegistry.getPropertyDAO().get(com.rtw.tmat.utils.Constants.FEDEX_LABEL_DIRECTORY);
				if(invoice.getTrackingNo()!=null && !invoice.getTrackingNo().isEmpty()){
					if(property!=null && property.getValue()!=null && !property.getValue().isEmpty()){
						fileWithPath = property.getValue()+File.separator+invoice.getId()+"_"+invoice.getTrackingNo();						
						OutputStream out = response.getOutputStream();
						File file = new File(fileWithPath+".pdf");
						if(file.exists()){
							FileInputStream in = new FileInputStream(file);
							response.setContentType("application/octet-stream");
							response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"",file.getName()));
							response.setHeader("Content-Length",String.valueOf(file.length()));
							byte[] buffer = new byte[4096];
							int length;
							while((length = in.read(buffer)) > 0){
							    out.write(buffer, 0, length);
							}
							in.close();
							out.flush();
						}
					}
				}
			}
			if(poIdStr!=null && !poIdStr.isEmpty()){
				PurchaseOrder purchaseOrder = DAORegistry.getPurchaseOrderDAO().get(Integer.parseInt(poIdStr));
				Property property = DAORegistry.getPropertyDAO().get(com.rtw.tmat.utils.Constants.FEDEX_LABEL_DIRECTORY);
				if(purchaseOrder.getTrackingNo()!=null && !purchaseOrder.getTrackingNo().isEmpty()){
					if(property!=null && property.getValue()!=null && !property.getValue().isEmpty()){
						fileWithPath = property.getValue()+File.separator+purchaseOrder.getId()+"_"+purchaseOrder.getTrackingNo();											
						OutputStream out = response.getOutputStream();
						File file = new File(fileWithPath+".pdf");
						if(file.exists()){
							FileInputStream in = new FileInputStream(file);
							response.setContentType("application/octet-stream");
							response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"",file.getName()));
							
							byte[] buffer = new byte[4096];
							int length;
							while((length = in.read(buffer)) > 0){
							    out.write(buffer, 0, length);
							}
							in.close();
							out.flush();
						}
					}
				}
			}
			if(manualFedexIdStr!=null && !manualFedexIdStr.isEmpty()){
				ManualFedexGeneration manualFedex = DAORegistry.getManualFedexGenerationDAO().get(Integer.parseInt(manualFedexIdStr));
				Property property = DAORegistry.getPropertyDAO().get(com.rtw.tmat.utils.Constants.FEDEX_LABEL_MANUAL_DIRECTORY);
				if(manualFedex.getTrackingNumber()!=null && !manualFedex.getTrackingNumber().isEmpty()){
					if(property!=null && property.getValue()!=null && !property.getValue().isEmpty()){
						//fileWithPath = property.getValue()+File.separator+manualFedex.getId()+"_"+manualFedex.getTrackingNumber();
						fileWithPath = property.getValue()+File.separator+"1_"+manualFedex.getTrackingNumber();
						OutputStream out = response.getOutputStream();
						File file = new File(fileWithPath+".pdf");
						if(file.exists()){
							FileInputStream in = new FileInputStream(file);
							response.setContentType("application/octet-stream");
							response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"",file.getName()));
							response.setHeader("Content-Length",String.valueOf(file.length()));
							byte[] buffer = new byte[4096];
							int length;
							while((length = in.read(buffer)) > 0){
							    out.write(buffer, 0, length);
							}
							in.close();
							out.flush();
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	@RequestMapping(value="/GetRelatedPOInvoices")
	public RelatedPOInvoicesDTO getRelatedPOInvoices(HttpServletRequest request, HttpServletResponse response){
		RelatedPOInvoicesDTO relatedPOInvoicesDTO = new RelatedPOInvoicesDTO();
		Error error = new Error();
		
		try{
			String poId = request.getParameter("poId");
			String invoiceId = request.getParameter("invoiceId");
			String productType = request.getParameter("productType");
			String brokerIdStr = request.getParameter("brokerId");
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				relatedPOInvoicesDTO.setError(error);
				relatedPOInvoicesDTO.setStatus(0);
				return relatedPOInvoicesDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				relatedPOInvoicesDTO.setError(error);
				relatedPOInvoicesDTO.setStatus(0);
				return relatedPOInvoicesDTO;
			}
			
			List<PurchaseOrder> poList = new ArrayList<PurchaseOrder>();
			List<POSPurchaseOrder> posList = new ArrayList<POSPurchaseOrder>();
			List<Ticket> ticketList = null;
			List<POSTicket> posTicketList = null;
			Map<Integer, EventDetails> ticketGroupEventMap = new HashMap<Integer, EventDetails>();
			Map<Integer, EventDetails> posTicketGroupEventMap = new HashMap<Integer, EventDetails>();
			TicketGroup ticGroup=null;
			POSTicketGroup posTicGroup = null;
			EventDetails event = null;
						
			if(productType.equalsIgnoreCase("REWARDTHEFAN") || productType.equalsIgnoreCase("SEATGEEK")){
			if(poId!=null && !poId.isEmpty()){
				PurchaseOrder po = DAORegistry.getPurchaseOrderDAO().getPurchaseOrderByIdAndBrokerId(Integer.parseInt(poId), brokerId);
				if(po!=null){
					ticketList = DAORegistry.getTicketDAO().getTIcketsByPOId(po.getId());
					po.setTotalQuantity(ticketList.size());
					ticketList = DAORegistry.getTicketDAO().getMappedTicketByPOId(po.getId());
					po.setUsedQunatity(ticketList.size());
					Customer customer = DAORegistry.getCustomerDAO().get(po.getCustomerId());
					po.setCustomerName(customer.getCustomerName());
					if(ticketList!=null && !ticketList.isEmpty()){
						for(Ticket ticket:ticketList){
							ticGroup = DAORegistry.getTicketGroupDAO().get(ticket.getTicketGroupId());
							ticket.setSection(ticGroup.getSection());
							ticket.setRow(ticGroup.getRow());
							if(ticketGroupEventMap.get(ticket.getTicketGroupId())!=null){
								event = ticketGroupEventMap.get(ticket.getTicketGroupId());
							}else{
								event = DAORegistry.getEventDetailsDAO().get(ticGroup.getEventId());
								ticketGroupEventMap.put(ticket.getTicketGroupId(),event);
							}
							if(event!=null){
								ticket.setEventNameString(event.getEventName()+" "+event.getEventDateTimeStr());
							}
						}
					}
					poList.add(po);
				}else{
					//jObj.put("successMessage", "No Invoices found for selected PO.");
					relatedPOInvoicesDTO.setMessage("No Invoices found for selected PO.");
					relatedPOInvoicesDTO.setStatus(1);
				}
			}else if(invoiceId!=null && !invoiceId.isEmpty()){
				Invoice invoice = DAORegistry.getInvoiceDAO().getInvoiceByIdAndBrokerId(Integer.parseInt(invoiceId), brokerId);
				if(invoice != null){
					ticketList = DAORegistry.getTicketDAO().getMappedTicketsByInvoiceId(Integer.parseInt(invoiceId));
					if(ticketList!=null && !ticketList.isEmpty()){
						List<Integer> poIds = new ArrayList<Integer>();
						for(Ticket ticket:ticketList){
							 ticGroup = DAORegistry.getTicketGroupDAO().get(ticket.getTicketGroupId());
							 ticket.setSection(ticGroup.getSection());
							 ticket.setRow(ticGroup.getRow());
							if(ticketGroupEventMap.get(ticket.getTicketGroupId())!=null){
								event = ticketGroupEventMap.get(ticket.getTicketGroupId());
							}else{
								event = DAORegistry.getEventDetailsDAO().get(ticGroup.getEventId());
								ticketGroupEventMap.put(ticket.getTicketGroupId(),event);
							}
							if(event!=null){
								ticket.setEventNameString(event.getEventName()+" "+event.getEventDateTimeStr());
							}
							if(!poIds.contains(ticket.getPoId())){
								poIds.add(ticket.getPoId());
							}
						}
						List<Ticket> tickets = null;
						for(Integer id : poIds){
							PurchaseOrder po = DAORegistry.getPurchaseOrderDAO().get(id);
							tickets = DAORegistry.getTicketDAO().getTIcketsByPOId(id);
							po.setTotalQuantity(tickets.size());
							tickets = DAORegistry.getTicketDAO().getMappedTicketByPOId(id);
							po.setUsedQunatity(tickets.size());
							Customer customer = DAORegistry.getCustomerDAO().get(po.getCustomerId());
							if(customer != null){
								po.setCustomerName(customer.getCustomerName());
							}
							poList.add(po);
						}
					}else{
						//jObj.put("successMessage", "No PO(s) found for selected invoice.");
						relatedPOInvoicesDTO.setMessage("No PO(s) found for selected invoice.");
						relatedPOInvoicesDTO.setStatus(1);
					}
				}else{
					//jObj.put("errorMessage", "Require Invoice not found.");
					error.setDescription("Require Invoice not found.");
					relatedPOInvoicesDTO.setError(error);
					relatedPOInvoicesDTO.setStatus(0);
				}
			}else{
				//jObj.put("errorMessage", "Require POId/InvoiceId not found.");
				error.setDescription("Require POId/InvoiceId not found.");
				relatedPOInvoicesDTO.setError(error);
				relatedPOInvoicesDTO.setStatus(0);
			}
			}
			if(productType.equalsIgnoreCase("RTW") || productType.equalsIgnoreCase("RTW2")){
				if(poId!=null && !poId.isEmpty()){ 
					POSPurchaseOrder pos = DAORegistry.getPosPurchaseOrderDAO().getPOSPurchaseOrderDAOByPurchaseOrderId(Integer.parseInt(poId), productType);
					if(pos!=null){
						posTicketList = DAORegistry.getPosTicketDAO().getPOSTicketByPurchaseOrderId(pos.getPurchaseOrderId(), productType);
						pos.setTotalQuantity(posTicketList.size());
						posTicketList = DAORegistry.getPosTicketDAO().getMappedPOSTicketByPurchaseOrderId(pos.getPurchaseOrderId(), productType);
						pos.setUsedQunatity(posTicketList.size());
						//Customer customer = DAORegistry.getCustomerDAO().get(po.getCustomerId());
						//po.setCustomerName(customer.getCustomerName());
						if(posTicketList!=null && !posTicketList.isEmpty()){
							for(POSTicket posTicket:posTicketList){
								posTicGroup = DAORegistry.getPosTicketGroupDAO().getPOSTicketGroupByTicketGroupIdWithPdt(posTicket.getTicketGroupId(), productType);
								posTicket.setSection(posTicGroup.getSection());
								posTicket.setRow(posTicGroup.getRow());
								if(posTicketGroupEventMap.get(posTicket.getTicketGroupId())!=null){
									event = posTicketGroupEventMap.get(posTicket.getTicketGroupId());
								}else{
									event = DAORegistry.getEventDetailsDAO().get(posTicGroup.getEventId());
									posTicketGroupEventMap.put(posTicket.getTicketGroupId(),event);
								}
								if(event!=null){
									//posTicket.setEventNameString(event.getEventName()+" "+event.getEventDateTimeStr());
								}
							}
						}
						posList.add(pos);
					}else{
						//jObj.put("successMessage", "No Invoices found for selected PO.");
						relatedPOInvoicesDTO.setMessage("No Invoices found for selected PO.");
						relatedPOInvoicesDTO.setStatus(1);
					}
				}else if(invoiceId!=null && !invoiceId.isEmpty()){ 
					posTicketList = DAORegistry.getPosTicketDAO().getMappedPOSTicketByInvoiceId(Integer.parseInt(invoiceId));
					if(posTicketList!=null && !posTicketList.isEmpty()){
						List<Integer> poIds = new ArrayList<Integer>();						
						for(POSTicket posTicket:posTicketList){
							posTicGroup = DAORegistry.getPosTicketGroupDAO().getPOSTicketGroupByTicketGroupIdWithPdt(posTicket.getTicketGroupId(),productType);
							posTicket.setSection(posTicGroup.getSection());
							posTicket.setRow(posTicGroup.getRow());
							if(posTicketGroupEventMap.get(posTicket.getTicketGroupId())!=null){
								event = posTicketGroupEventMap.get(posTicket.getTicketGroupId());
							}else{
								event = DAORegistry.getEventDetailsDAO().get(posTicGroup.getEventId());
								posTicketGroupEventMap.put(posTicket.getTicketGroupId(),event);
							}
							if(event!=null){
								//posTicket.setEventNameString(event.getEventName()+" "+event.getEventDateTimeStr());
							}
							if(!poIds.contains(posTicket.getPurchaseOrderId())){
								poIds.add(posTicket.getPurchaseOrderId());
							}
						}
						List<POSTicket> posTickets = null;
						for(Integer id : poIds){
							POSPurchaseOrder pos = DAORegistry.getPosPurchaseOrderDAO().getPOSPurchaseOrderDAOByPurchaseOrderId(id, productType);
							if(pos != null){
							posTickets = DAORegistry.getPosTicketDAO().getPOSTicketByPurchaseOrderId(id, productType);							
							pos.setTotalQuantity(posTickets.size());
							posTickets = DAORegistry.getPosTicketDAO().getMappedPOSTicketByPurchaseOrderId(id, productType);
							pos.setUsedQunatity(posTickets.size());
							//Customer customer = DAORegistry.getCustomerDAO().get(po.getCustomerId());
							//po.setCustomerName(customer.getCustomerName());
							posList.add(pos);
							}else{
								//jObj.put("successMessage", "No PO(s) found for selected invoice.");
								relatedPOInvoicesDTO.setMessage("No PO(s) found for selected invoice.");
								relatedPOInvoicesDTO.setStatus(1);
							}
						}
					}else{
						//jObj.put("successMessage", "No PO(s) found for selected invoice.");
						relatedPOInvoicesDTO.setMessage("No PO(s) found for selected invoice.");
						relatedPOInvoicesDTO.setStatus(1);
					}
				}else{
					//jObj.put("errorMessage", "Require POId/InvoiceId not found.");
					error.setDescription("Require POId/InvoiceId not found.");
					relatedPOInvoicesDTO.setError(error);
					relatedPOInvoicesDTO.setStatus(0);
				}
			}
			
			if(ticketList!= null){
				relatedPOInvoicesDTO.setPoPaginationDTO(PaginationUtil.getDummyPaginationParameters(poList!=null?poList.size():0));
				relatedPOInvoicesDTO.setRelatedPODTO(com.rtw.tracker.utils.Util.getRelatedPOArray(poList, null));
				relatedPOInvoicesDTO.setInvoicePaginationDTO(PaginationUtil.getDummyPaginationParameters(ticketList!=null?ticketList.size():0));
				relatedPOInvoicesDTO.setRelatedInvoiceDTO(com.rtw.tracker.utils.Util.getRelatedInvoicesArray(ticketList, null));
				relatedPOInvoicesDTO.setStatus(1);
			}
			if(posTicketList!= null){
				relatedPOInvoicesDTO.setPoPaginationDTO(PaginationUtil.getDummyPaginationParameters(posList!=null?posList.size():0));
				relatedPOInvoicesDTO.setRelatedPODTO(com.rtw.tracker.utils.Util.getRelatedPOArray(null, posList));
				relatedPOInvoicesDTO.setInvoicePaginationDTO(PaginationUtil.getDummyPaginationParameters(posTicketList!=null?posTicketList.size():0));
				relatedPOInvoicesDTO.setRelatedInvoiceDTO(com.rtw.tracker.utils.Util.getRelatedInvoicesArray(null, posTicketList));
				relatedPOInvoicesDTO.setStatus(1);
			}
			relatedPOInvoicesDTO.setSelectedProductType(productType);			
			
		}catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Related PO/Invoices.");
			relatedPOInvoicesDTO.setError(error);
			relatedPOInvoicesDTO.setStatus(0);
		}
		return relatedPOInvoicesDTO;
	}		

	
	@RequestMapping(value = "/ManagePO")
	public PurchaseOrderDetailsDTO managePurchaseOrder(HttpServletRequest request, HttpServletResponse response){
		PurchaseOrderDetailsDTO purchaseOrderDetailsDTO = new PurchaseOrderDetailsDTO();
		Error error = new Error();
		
		try {
			String poNo = request.getParameter("poNo");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String companyProduct = request.getParameter("productType");
			String product = request.getParameter("productTypeSession");
			String csr = request.getParameter("csr");
			String customerStr = request.getParameter("customer");
			String sortingString = request.getParameter("sortingString");
			String brokerIdStr = request.getParameter("brokerId");
			
			String action = request.getParameter("action");			
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				purchaseOrderDetailsDTO.setError(error);
				purchaseOrderDetailsDTO.setStatus(0);
				return purchaseOrderDetailsDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				purchaseOrderDetailsDTO.setError(error);
				purchaseOrderDetailsDTO.setStatus(0);
				return purchaseOrderDetailsDTO;
			}
			
			GridHeaderFilters filter = null;
			try{
				filter = GridHeaderFiltersUtil.getPOSearchHeaderFilters(headerFilter+sortingString);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				purchaseOrderDetailsDTO.setError(error);
				purchaseOrderDetailsDTO.setStatus(0);
				return purchaseOrderDetailsDTO;
			}
			
			Collection<PurchaseOrders> purchaseOrders=null;			
			String fromDateFinal = "";
			String toDateFinal = "";
			double poTotal = 0;
			double roundPOTotal = 0;
			Integer count = 0;
			
			if(action!=null && action.equalsIgnoreCase("search")){				
				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
				}else{
					fromDateFinal = null;
					toDateFinal = null;
				}
			}else{
				if((fromDateStr != null && !fromDateStr.isEmpty()) && (toDateStr != null && !toDateStr.isEmpty())){			
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
				}else{
					if(poNo==null || poNo.isEmpty()){
						DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
						Calendar cal = Calendar.getInstance();
						Date toDate = new Date();
						cal.add(Calendar.MONTH, -3);
						Date fromDate = cal.getTime();
						fromDateStr = dateFormat.format(fromDate);
						toDateStr = dateFormat.format(toDate);
						fromDateFinal = fromDateStr + " 00:00:00";
						toDateFinal = toDateStr + " 23:59:59";
					}
				}
			}
			
			if(product!=null && !product.isEmpty()){
				companyProduct = product;
			}
			
			if(companyProduct!= null && (companyProduct.equalsIgnoreCase("RTW") || companyProduct.equalsIgnoreCase("RTW2"))){
				purchaseOrders = DAORegistry.getQueryManagerDAO().getRTWPurchaseOrdersList(fromDateFinal,toDateFinal,companyProduct,filter,poNo,null,pageNo);
				count = DAORegistry.getQueryManagerDAO().getRTWPurchaseOrdersListCount(fromDateFinal,toDateFinal,companyProduct,filter,poNo,null);
			}else{
				purchaseOrders = DAORegistry.getQueryManagerDAO().purchaseOrdersList(fromDateFinal,toDateFinal,filter,poNo,pageNo,brokerId);
				count = DAORegistry.getQueryManagerDAO().purchaseOrdersListCount(fromDateFinal,toDateFinal,filter,poNo,brokerId);
			}
			
			Map<Integer,Double> poCountMap = new HashMap<Integer, Double>();
			if(purchaseOrders != null && !purchaseOrders.isEmpty()){
				for(PurchaseOrders po:purchaseOrders){
					poCountMap.put(po.getId(), po.getPoTotal());
				}
			}			
			
			if(purchaseOrders != null && !purchaseOrders.isEmpty()){
				for(Double poTot : poCountMap.values()){
					poTotal += poTot;	//po.getPoTotal();
				}
			}
			
			roundPOTotal = Math.round(poTotal*100)/100.00;
			
			purchaseOrderDetailsDTO.setStatus(1);
			purchaseOrderDetailsDTO.setPoPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
			purchaseOrderDetailsDTO.setPurchaseOrderDTO(com.rtw.tracker.utils.Util.getPuchaseOrderArray(purchaseOrders));
			purchaseOrderDetailsDTO.setPoTotal(String.valueOf(roundPOTotal));
			purchaseOrderDetailsDTO.setLayoutProductType(companyProduct);
			purchaseOrderDetailsDTO.setSelectedProductType(companyProduct);
			purchaseOrderDetailsDTO.setFromDate(fromDateStr);
			purchaseOrderDetailsDTO.setToDate(toDateStr);
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Purchase Orders.");
			purchaseOrderDetailsDTO.setError(error);
			purchaseOrderDetailsDTO.setStatus(0);
		}
		return purchaseOrderDetailsDTO;
	}
	
	
	/*@RequestMapping(value = "/GetPurchaseOrders")
	public PurchaseOrderDetailsDTO GetPurchaseOrders(HttpServletRequest request, HttpServletResponse response){
		PurchaseOrderDetailsDTO purchaseOrderDetailsDTO = new PurchaseOrderDetailsDTO();
		Error error = new Error();
		
		try {
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String csr = request.getParameter("csr");
			String customerStr = request.getParameter("customer");
			String companyProduct = request.getParameter("productType");
			String poNoStr = request.getParameter("poNo");
			String action = request.getParameter("action");
			
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String brokerIdStr = request.getParameter("brokerId");
			
			if(StringUtils.isEmpty(pageNo)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				purchaseOrderDetailsDTO.setError(error);
				purchaseOrderDetailsDTO.setStatus(0);
				return purchaseOrderDetailsDTO;
			}
			try {
				Integer.parseInt(pageNo);
			} catch (Exception e) {
				System.err.println("Please provide valid PageNo.");
				error.setDescription("Please provide valid PageNo.");
				purchaseOrderDetailsDTO.setError(error);
				purchaseOrderDetailsDTO.setStatus(0);
				return purchaseOrderDetailsDTO;
			}
			
			GridHeaderFilters filter = null;
			try{
				filter = GridHeaderFiltersUtil.getPOSearchHeaderFilters(headerFilter);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				purchaseOrderDetailsDTO.setError(error);
				purchaseOrderDetailsDTO.setStatus(0);
				return purchaseOrderDetailsDTO;
			}
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				purchaseOrderDetailsDTO.setError(error);
				purchaseOrderDetailsDTO.setStatus(0);
				return purchaseOrderDetailsDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				purchaseOrderDetailsDTO.setError(error);
				purchaseOrderDetailsDTO.setStatus(0);
				return purchaseOrderDetailsDTO;
			}
			
			Collection<PurchaseOrders> purchaseOrders=null;
			String fromDateFinal = "";
			String toDateFinal = "";
			double poTotal = 0;
			double roundPOTotal = 0;
			Integer count = 0;			
			
			if(action!=null && action.equalsIgnoreCase("search")){				
				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
					if(companyProduct!= null && (companyProduct.equalsIgnoreCase("RTW") || companyProduct.equalsIgnoreCase("RTW2"))){
						purchaseOrders = DAORegistry.getQueryManagerDAO().getRTWPurchaseOrdersList(fromDateFinal,toDateFinal,companyProduct,filter,null,null,pageNo);
						count = DAORegistry.getQueryManagerDAO().getRTWPurchaseOrdersListCount(fromDateFinal,toDateFinal,companyProduct,filter,null,null);
					}else{
						purchaseOrders = DAORegistry.getQueryManagerDAO().purchaseOrdersList(fromDateFinal,toDateFinal,filter,null,pageNo,brokerId);
						count = DAORegistry.getQueryManagerDAO().purchaseOrdersListCount(fromDateFinal,toDateFinal,filter,null,brokerId);
					}
				}else{
					if(companyProduct!= null && (companyProduct.equalsIgnoreCase("RTW") || companyProduct.equalsIgnoreCase("RTW2"))){
						purchaseOrders = DAORegistry.getQueryManagerDAO().getRTWPurchaseOrdersList(null,null,companyProduct,filter,null,null,pageNo);
						count = DAORegistry.getQueryManagerDAO().getRTWPurchaseOrdersListCount(null,null,companyProduct,filter,null,null);
					}else{
						purchaseOrders = DAORegistry.getQueryManagerDAO().purchaseOrdersList(null,null,filter,null,pageNo,brokerId);
						count = DAORegistry.getQueryManagerDAO().purchaseOrdersListCount(null,null,filter,null,brokerId);
					}
				}
			}
			else if((fromDateStr != null && !fromDateStr.isEmpty()) && (toDateStr != null && !toDateStr.isEmpty())){
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";
				purchaseOrders = DAORegistry.getQueryManagerDAO().purchaseOrdersList(fromDateFinal,toDateFinal,filter,null,pageNo,brokerId);
				count = DAORegistry.getQueryManagerDAO().purchaseOrdersListCount(fromDateFinal,toDateFinal,filter,null,brokerId);
			}
			Map<Integer,Double> poCountMap = new HashMap<Integer, Double>();
			if(purchaseOrders != null && !purchaseOrders.isEmpty()){
				for(PurchaseOrders po:purchaseOrders){
					poCountMap.put(po.getId(), po.getPoTotal());
				}
			}			
			
			if(purchaseOrders != null && !purchaseOrders.isEmpty()){
				for(Double poTot : poCountMap.values()){
					poTotal += poTot;	//po.getPoTotal();
				}
			}
			
			roundPOTotal = Math.round(poTotal*100)/100.00;
			
			purchaseOrderDetailsDTO.setStatus(1);
			purchaseOrderDetailsDTO.setPoPaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
			purchaseOrderDetailsDTO.setPurchaseOrderDTO(com.rtw.tracker.utils.Util.getPuchaseOrderArray(purchaseOrders));
			purchaseOrderDetailsDTO.setPoTotal(String.valueOf(roundPOTotal));
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Applying Search - Purchase Orders.");
			purchaseOrderDetailsDTO.setError(error);
			purchaseOrderDetailsDTO.setStatus(0);
		}
		return purchaseOrderDetailsDTO;
	}*/
	
	@RequestMapping(value = "/PurchaseOrdersExportToExcel")
	public void purchaseOrdersToExport(HttpServletRequest request, HttpServletResponse response){		
		
		try {
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String csr = request.getParameter("csr");
			String customerStr = request.getParameter("customer");
			String companyProduct = request.getParameter("productType");
			String poNoStr = request.getParameter("poNo");
			
			String headerFilter = request.getParameter("headerFilter");			
			String brokerIdStr = request.getParameter("brokerId");
			Integer brokerId = Integer.parseInt(brokerIdStr);
			
			Collection<PurchaseOrders> purchaseOrders=null;
			String fromDateFinal = "";
			String toDateFinal = "";
			
			GridHeaderFilters filter = GridHeaderFiltersUtil.getPOSearchHeaderFilters(headerFilter);
						
			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";
				if(companyProduct.equalsIgnoreCase("RTW") || companyProduct.equalsIgnoreCase("RTW2")){
					purchaseOrders = DAORegistry.getQueryManagerDAO().getRTWPurchaseOrdersListToExport(poNoStr,csr,customerStr,fromDateFinal,toDateFinal,companyProduct,null,filter);
				}else{
					purchaseOrders = DAORegistry.getQueryManagerDAO().purchaseOrdersListToExport(poNoStr,csr,customerStr,fromDateFinal,toDateFinal,filter,brokerId);
				}
			}else{
				if(companyProduct.equalsIgnoreCase("RTW") || companyProduct.equalsIgnoreCase("RTW2")){
					purchaseOrders = DAORegistry.getQueryManagerDAO().getRTWPurchaseOrdersListToExport(poNoStr,csr,customerStr,null,null,companyProduct,null,filter);
				}else{
					purchaseOrders = DAORegistry.getQueryManagerDAO().purchaseOrdersListToExport(poNoStr,csr,customerStr,null,null,filter,brokerId);
				}
			}
			
			if(purchaseOrders!=null && !purchaseOrders.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("purchase_orders");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Purchase Order Id");
				header.createCell(1).setCellValue("Customer Name");
				header.createCell(2).setCellValue("Customer Id");
				header.createCell(3).setCellValue("Customer Type");
				header.createCell(4).setCellValue("PO Total");
				header.createCell(5).setCellValue("Ticket Qty");
				header.createCell(6).setCellValue("Created Date");
				header.createCell(7).setCellValue("Created By");
				header.createCell(8).setCellValue("Shipping Type");
				header.createCell(9).setCellValue("Status");
				header.createCell(10).setCellValue("Product Type");
				header.createCell(11).setCellValue("Last Updated");
				header.createCell(12).setCellValue("PO Aged");
				header.createCell(13).setCellValue("CSR");
				header.createCell(14).setCellValue("Consignment PO No");
				header.createCell(15).setCellValue("Transaction Office");
				header.createCell(16).setCellValue("Tracking No");
				header.createCell(17).setCellValue("Shipping Notes");
				header.createCell(18).setCellValue("External Notes");
				header.createCell(19).setCellValue("Internal Notes");
				header.createCell(20).setCellValue("Event Id");
				header.createCell(21).setCellValue("Event Name");
				header.createCell(22).setCellValue("Event Date");
				header.createCell(23).setCellValue("Event Time");
				header.createCell(24).setCellValue("Is Emailed");
				header.createCell(25).setCellValue("Event Ticket Qty");
				header.createCell(26).setCellValue("Event Ticket Cost");
				header.createCell(27).setCellValue("Purchase Order Type");
				header.createCell(28).setCellValue("RTW PO");
				header.createCell(29).setCellValue("Broker Id");
				
				int i=1;
				for(PurchaseOrders order : purchaseOrders){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(order.getId());
					row.createCell(1).setCellValue(order.getCustomerName());
					row.createCell(2).setCellValue(order.getCustomerId()!=null?order.getCustomerId():0);
					row.createCell(3).setCellValue(order.getCustomerType()!=null?order.getCustomerType():"");
					row.createCell(4).setCellValue(order.getPoTotal());
					row.createCell(5).setCellValue(order.getTicketQty()!=null?order.getTicketQty():0);
					row.createCell(6).setCellValue(order.getCreateDateStr());
					row.createCell(7).setCellValue(order.getCreatedBy()!=null?order.getCreatedBy():"");
					row.createCell(8).setCellValue(order.getShippingType()!=null?order.getShippingType():"");
					row.createCell(9).setCellValue(order.getStatus()!=null?order.getStatus():"");
					row.createCell(10).setCellValue(order.getProductType()!=null?order.getProductType():"");
					row.createCell(11).setCellValue(order.getLastUpdatedStr()!=null?order.getLastUpdatedStr():"");
					row.createCell(12).setCellValue(order.getPoAged()!=null?order.getPoAged():0);
					row.createCell(13).setCellValue(order.getCsr()!=null?order.getCsr():"");
					row.createCell(14).setCellValue(order.getConsignmentPoNo()!=null?order.getConsignmentPoNo():"");
					row.createCell(15).setCellValue(order.getTransactionOffice());
					row.createCell(16).setCellValue(order.getTrackingNo()!=null?order.getTrackingNo():"");
					row.createCell(17).setCellValue(order.getShippingNotes()!=null?order.getShippingNotes():"");
					row.createCell(18).setCellValue(order.getExternalNotes()!=null?order.getExternalNotes():"");
					row.createCell(19).setCellValue(order.getInternalNotes()!=null?order.getInternalNotes():"");
					row.createCell(20).setCellValue(order.getEventId());
					row.createCell(21).setCellValue(order.getEventName());
					row.createCell(22).setCellValue(order.getEventDateStr()!=null?order.getEventDateStr():"");
					row.createCell(23).setCellValue(order.getEventTimeStr()!=null?order.getEventTimeStr():"");
					row.createCell(24).setCellValue(order.getIsEmailed()!=null?"Yes":"No");
					row.createCell(25).setCellValue(order.getEventTicketQty()!=null?String.valueOf(order.getEventTicketQty()):"-");
					row.createCell(26).setCellValue(order.getEventTicketCost()!=null?String.valueOf(order.getEventTicketCost()):"-");
					row.createCell(27).setCellValue(order.getPurchaseOrderType()!=null?order.getPurchaseOrderType():"");
					if(order.getPosPOId()!=null){
						row.createCell(28).setCellValue("Yes");
					}else{
						row.createCell(28).setCellValue("No");
					}
					row.createCell(29).setCellValue(order.getBrokerId()!=null?order.getBrokerId().toString():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=purchase_orders.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
		
	@RequestMapping(value = "/GetPOPdf")
	public void getPOPdf(HttpServletRequest request, HttpServletResponse response){
		
		try {
			String poId = request.getParameter("poId");
			String download = request.getParameter("inline");
			String brokerIdStr = request.getParameter("brokerId");
			Integer brokerId = Integer.parseInt(brokerIdStr);
			
			if(poId!=null && !poId.isEmpty()){				
				PurchaseOrder PO = DAORegistry.getPurchaseOrderDAO().getPurchaseOrderByIdAndBrokerId(Integer.parseInt(poId), brokerId);
				if(PO != null){
					List<TicketGroup> ticketGroups = DAORegistry.getTicketGroupDAO().getAllTicketGroupsByPOId(PO.getId());
					Customer customer = DAORegistry.getCustomerDAO().get(PO.getCustomerId());
					CustomerAddress customerAddress = DAORegistry.getCustomerAddressDAO().getCustomerAddressInfo(customer.getId());
					
					PDFUtil.generatePOPdf(PO,ticketGroups,customer,customerAddress);
					File file = new File(DAORegistry.getPropertyDAO().get("tictracker.po.pdffile.path").getValue()+"\\PurchaseOrder_"+PO.getId()+".pdf");
					if(!file.exists()){
						file.createNewFile();
					}
					OutputStream out = response.getOutputStream();
					FileInputStream in = new FileInputStream(file);
					response.setContentType("application/pdf");
					response.setHeader("Content-Disposition", String.format(download+"; filename=\"%s\"",file.getName()));
					byte[] buffer = new byte[4096];
					int length;
					while((length = in.read(buffer)) > 0){
					    out.write(buffer, 0, length);
					}
					in.close();
					out.flush();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping(value = "/GetRTWRTW2POPdf")
	public void getRTWRTW2POPdf(HttpServletRequest request, HttpServletResponse response){
		
		try {
			String poId = request.getParameter("poId");
			String productType = request.getParameter("productType");
			String download = request.getParameter("inline");
			POSTicketGroup ticketGroups = null;
			
			if(poId!=null && !poId.isEmpty()){
				Collection<PurchaseOrders> purchaseOrders = DAORegistry.getQueryManagerDAO().getRTWPurchaseOrdersListByPoID(poId);			
				List<POSTicket> ticket = DAORegistry.getPosTicketDAO().getPOSTicketByPurchaseOrderId(Integer.parseInt(poId), productType);
				Map<Integer, List<POSTicketGroup>> ticketMap = new HashMap<Integer, List<POSTicketGroup>>();
				if(ticket!=null && !ticket.isEmpty()){
					List<POSTicketGroup> list = null;
					List<Integer> ticketGroupIds = new ArrayList<Integer>();
					Map<Integer,Integer> ticCountMap = new HashMap<Integer, Integer>();
					Map<Integer,Double> ticPriceMap = new HashMap<Integer, Double>();
					Map<Integer,Integer> ticPoMap = new HashMap<Integer, Integer>();
					Map<Integer,String> ticOnHandMap = new HashMap<Integer, String>();
					Map<Integer,String> ticSeatMap = new HashMap<Integer, String>();
					for(POSTicket tic: ticket){
						if(ticCountMap.get(tic.getTicketGroupId())!=null){
							ticCountMap.put(tic.getTicketGroupId(), ticCountMap.get(tic.getTicketGroupId())+1);
						}else{
							ticCountMap.put(tic.getTicketGroupId(), 1);
						}
						ticPriceMap.put(tic.getTicketGroupId(),tic.getActualSoldPrice());
						ticPoMap.put(tic.getTicketGroupId(),tic.getPurchaseOrderId());
						ticOnHandMap.put(tic.getTicketGroupId(),((tic.getTicketOnHandStatusId()!=null&&tic.getTicketOnHandStatusId()==1)?"Y":"N"));
						if(ticSeatMap.get(tic.getTicketGroupId())!=null){
							String seatNo = ticSeatMap.get(tic.getTicketGroupId());
							if(seatNo.contains("-")){
								seatNo = seatNo.split("-")[0];
							}
							seatNo =seatNo + "-"+tic.getSeatNumber();
							ticSeatMap.put(tic.getTicketGroupId(), ticSeatMap.get(tic.getTicketGroupId())+"-"+tic.getSeatNumber());
						}else{
							ticSeatMap.put(tic.getTicketGroupId(), tic.getSeatNumber());
						}
					}
					for(POSTicket tic: ticket){
						if(ticketGroupIds.contains(tic.getTicketGroupId())){
							continue;
						}
						POSTicketGroup ticketGroup = DAORegistry.getPosTicketGroupDAO().getPOSTicketGroupByTicketGroupId(tic.getTicketGroupId(),productType);
						ticketGroup.setMappedQty(ticCountMap.get(tic.getTicketGroupId()));
						ticketGroup.setActualSoldPrice(ticPriceMap.get(tic.getTicketGroupId()));
						ticketGroup.setPurchaseOrderId(ticPoMap.get(tic.getTicketGroupId()));
						ticketGroup.setOnHand(ticOnHandMap.get(tic.getTicketGroupId()));
						ticketGroup.setSeatNoString(ticSeatMap.get(tic.getTicketGroupId()));
						if(ticketMap.get(ticketGroup.getEventId())!=null){
							ticketMap.get(ticketGroup.getEventId()).add(ticketGroup);
						}else{
							list = new ArrayList<POSTicketGroup>();
							list.add(ticketGroup);
							ticketMap.put(ticketGroup.getEventId(), list);
						}
						ticketGroupIds.add(ticketGroup.getTicketGroupId());
						
						
					}
				}
				List<PurchaseOrders> PO = new ArrayList<PurchaseOrders>();
				PO.addAll(purchaseOrders);			
				CustomerAddress customerAddress = DAORegistry.getCustomerAddressDAO().getCustomerAddressInfo(PO.get(0).getCustomerId());
				PDFUtil.generateRTWPOPdf(purchaseOrders,ticketMap,customerAddress);
				File file = new File(DAORegistry.getPropertyDAO().get("tictracker.po.pdffile.path").getValue()+"\\PurchaseOrder_"+poId+".pdf");
				if(!file.exists()){
					file.createNewFile();
				}
				OutputStream out = response.getOutputStream();
				FileInputStream in = new FileInputStream(file);
				response.setContentType("application/pdf");
				response.setHeader("Content-Disposition", String.format(download+"; filename=\"%s\"",file.getName()));
				byte[] buffer = new byte[4096];
				int length;
				while((length = in.read(buffer)) > 0){
				    out.write(buffer, 0, length);
				}
				in.close();
				out.flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(value="/GetPOPaymentHistory")
	public POPaymentDetailsDTO getPOPaymentHistory(HttpServletRequest request, HttpServletResponse response){
		POPaymentDetailsDTO poPaymentDetailsDTO = new POPaymentDetailsDTO();
		Error error = new Error();
		
		try{
			String poIdStr = request.getParameter("poId");
			String productType = request.getParameter("productType");
			String brokerIdStr = request.getParameter("brokerId");
			
			if(StringUtils.isEmpty(poIdStr)){
				System.err.println("Purchase Order ID not found.");
				error.setDescription("Purchase Order ID not found.");
				poPaymentDetailsDTO.setError(error);
				poPaymentDetailsDTO.setStatus(0);
				return poPaymentDetailsDTO;
			}
			Integer poId = 0;
			try{
				poId = Integer.parseInt(poIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Purchase Order ID should be valid Integer value.");
				poPaymentDetailsDTO.setError(error);
				poPaymentDetailsDTO.setStatus(0);
				return poPaymentDetailsDTO;
			}
			
			if(StringUtils.isEmpty(productType)){
				System.err.println("Product Type not found.");
				error.setDescription("Product Type not found.");
				poPaymentDetailsDTO.setError(error);
				poPaymentDetailsDTO.setStatus(0);
				return poPaymentDetailsDTO;
			}
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				poPaymentDetailsDTO.setError(error);
				poPaymentDetailsDTO.setStatus(0);
				return poPaymentDetailsDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				poPaymentDetailsDTO.setError(error);
				poPaymentDetailsDTO.setStatus(0);
				return poPaymentDetailsDTO;
			}
			
			List<PurchaseOrderPaymentDetails> poList = new ArrayList<PurchaseOrderPaymentDetails>();
			List<PaymentHistory> rtwPOList = null;
			
			if(productType.equalsIgnoreCase("REWARDTHEFAN")){					
				PurchaseOrder purchaseOrder = DAORegistry.getPurchaseOrderDAO().getPurchaseOrderByIdAndBrokerId(poId, brokerId);
				if(purchaseOrder == null){
					System.err.println("Purchase Order Details not found.");
					error.setDescription("Purchase Order Details not found.");
					poPaymentDetailsDTO.setError(error);
					poPaymentDetailsDTO.setStatus(0);
					return poPaymentDetailsDTO;
				}					
				PurchaseOrderPaymentDetails paymentDetails = DAORegistry.getPurchaseOrderPaymentDetailsDAO().getPaymentDetailsByPOId(poId);
				if(paymentDetails == null){
					System.err.println("Purchase Order Payment Details not found.");
					error.setDescription("Purchase Order Payment Details not found.");
					poPaymentDetailsDTO.setError(error);
					poPaymentDetailsDTO.setStatus(0);
					return poPaymentDetailsDTO;
				}					
				poList.add(paymentDetails);
			}
			if(productType.equalsIgnoreCase("RTW") || productType.equalsIgnoreCase("RTW2")){
				rtwPOList = DAORegistry.getQueryManagerDAO().getRTWPaymentDetailsByPOId(poId, productType);					
			}
			
			if(poList!=null && poList.size() > 0){
				poPaymentDetailsDTO.setStatus(1);
				poPaymentDetailsDTO.setPoPaginationDTO(PaginationUtil.getDummyPaginationParameters(poList.size()));
				poPaymentDetailsDTO.setPoList(com.rtw.tracker.utils.Util.getPOPaymentHistoryArray(poList));
			}
			else if(rtwPOList!=null && rtwPOList.size() > 0){
				poPaymentDetailsDTO.setStatus(1);
				poPaymentDetailsDTO.setPoPaginationDTO(PaginationUtil.getDummyPaginationParameters(rtwPOList.size()));
				poPaymentDetailsDTO.setRtwPOList(com.rtw.tracker.utils.Util.getPaymentHistoryArray(null, rtwPOList));
			}
			else{
				poPaymentDetailsDTO.setMessage("No Payment details found for Selected PO.");
				poPaymentDetailsDTO.setPoPaginationDTO(PaginationUtil.getDummyPaginationParameters(0));
			}
			poPaymentDetailsDTO.setSelectedProductType(productType);
			
		}catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching PO Payment History.");
			poPaymentDetailsDTO.setError(error);
			poPaymentDetailsDTO.setStatus(0);
		}
		return poPaymentDetailsDTO;
	}

	@RequestMapping(value="/GetPONote")
	public PONotesDTO getPONote(HttpServletRequest request, HttpServletResponse response){
		PONotesDTO poNotesDTO = new PONotesDTO();
		Error error = new Error();		
		
		try{
			String action = request.getParameter("action");
			String poIdStr = request.getParameter("poId");
			String brokerIdStr = request.getParameter("brokerId");
			
			if(StringUtils.isEmpty(poIdStr)){
				System.err.println("Purchase Order ID not found.");
				error.setDescription("Purchase Order ID not found.");
				poNotesDTO.setError(error);
				poNotesDTO.setStatus(0);
				return poNotesDTO;
			}
			Integer poId = 0;
			try{
				poId = Integer.parseInt(poIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Purchase Order ID should be valid Integer value.");
				poNotesDTO.setError(error);
				poNotesDTO.setStatus(0);
				return poNotesDTO;
			}			
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				poNotesDTO.setError(error);
				poNotesDTO.setStatus(0);
				return poNotesDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				poNotesDTO.setError(error);
				poNotesDTO.setStatus(0);
				return poNotesDTO;
			}
					
			PurchaseOrder po=null;
			po = DAORegistry.getPurchaseOrderDAO().getPurchaseOrderByIdAndBrokerId(poId, brokerId);
			if(po == null){
				System.err.println("Purchase Order not found.");
				error.setDescription("Purchase Order not found.");
				poNotesDTO.setError(error);
				poNotesDTO.setStatus(0);
				return poNotesDTO;
			}
					
			if(action!=null && action.equalsIgnoreCase("update")){
				po.setInternalNotes(request.getParameter("internalNote"));
				po.setExternalNotes(request.getParameter("externalNote"));
				DAORegistry.getPurchaseOrderDAO().update(po);
				poNotesDTO.setMessage("PO Notes updated Successfully.");
				
				//Tracking User Action
				String userActionMsg = "PO Note(s) Updated Successfully.";
				Util.userActionAudit(request, poId, userActionMsg);
			}
			
			poNotesDTO.setStatus(1);
			poNotesDTO.setPoId(poIdStr);
			poNotesDTO.setInternalNote(po.getInternalNotes());
			poNotesDTO.setExternalNote(po.getExternalNotes());
			
		}catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching/updating PO Note.");
			poNotesDTO.setError(error);
			poNotesDTO.setStatus(0);
		}
		return poNotesDTO;
	}

	 @RequestMapping(value="/GetPOCsr")
	 public POCsrDTO getPOCsr(HttpServletRequest request, HttpServletResponse response){
		 POCsrDTO poCsrDTO = new POCsrDTO();
		 Error error = new Error();
		 
		try{
			String action = request.getParameter("action");
			String poIdStr = request.getParameter("poId");			
			String csrUser = request.getParameter("csrUser");
			String brokerIdStr = request.getParameter("brokerId");
			
			if(StringUtils.isEmpty(poIdStr)){
				System.err.println("Purchase Order ID not found.");
				error.setDescription("Purchase Order ID not found.");
				poCsrDTO.setError(error);
				poCsrDTO.setStatus(0);
				return poCsrDTO;
			}
			Integer poId = 0;
			try{
				poId = Integer.parseInt(poIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Purchase Order ID should be valid Integer value.");
				poCsrDTO.setError(error);
				poCsrDTO.setStatus(0);
				return poCsrDTO;
			}			
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				poCsrDTO.setError(error);
				poCsrDTO.setStatus(0);
				return poCsrDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				poCsrDTO.setError(error);
				poCsrDTO.setStatus(0);
				return poCsrDTO;
			}
			
			PurchaseOrder po = null;
			po = DAORegistry.getPurchaseOrderDAO().getPurchaseOrderByIdAndBrokerId(poId, brokerId);
			if(po == null){
				System.err.println("Purchase Order not found.");
				error.setDescription("Purchase Order not found.");
				poCsrDTO.setError(error);
				poCsrDTO.setStatus(0);
				return poCsrDTO;
			}				
				
			if(action!=null && action.equalsIgnoreCase("changeCSR")){
				if(StringUtils.isEmpty(csrUser)){
					System.err.println("CSR not found.");
					error.setDescription("CSR not found.");
					poCsrDTO.setError(error);
					poCsrDTO.setStatus(0);
					return poCsrDTO;
				}
				if(po!=null){
					po.setCsr(csrUser);
					po.setLastUpdated(new Date());
					DAORegistry.getPurchaseOrderDAO().update(po);
					poCsrDTO.setMessage("PO CSR changed Successfully.");
					
					//Tracking User Action
					String userActionMsg = "PO CSR changed Successfully.";
					Util.userActionAudit(request, po.getId(), userActionMsg);
				}
			}else{
				csrUser = po.getCsr();
			}
			
			Collection<TrackerUser> users = null;
			if(brokerId != 1001){
				users =  DAORegistry.getTrackerUserDAO().getAllTrackerUserByBrokerId(brokerId);
			}else{
				users =  DAORegistry.getTrackerUserDAO().getAllTrackerUserExceptBroker();
			}
			
			if(users != null && users.size() > 0){
				poCsrDTO.setTrackerUserDTO(com.rtw.tracker.utils.Util.getTrackerUsersObject(users));
			}
			poCsrDTO.setStatus(1);
			poCsrDTO.setPoId(poIdStr);
			poCsrDTO.setPoCsr(csrUser);
			
		}catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching/updating PO CSR.");
			poCsrDTO.setError(error);
			poCsrDTO.setStatus(0);
		}
		return poCsrDTO;
	}

	@RequestMapping(value="/UpdatePOTicketGroupOnHandStatus")
	public POTicketGroupOnHandStatusDTO updatePOTicketGroupOnHandStatus(HttpServletRequest request, HttpServletResponse response){
		POTicketGroupOnHandStatusDTO poTicketGroupOnHandStatusDTO = new POTicketGroupOnHandStatusDTO();
		Error error = new Error();
		
		try{
			String poIdStr = request.getParameter("poId");
			String ticketGroupIds = request.getParameter("ticketGroupIds");
			String onHand = request.getParameter("onHand");
			String brokerIdStr = request.getParameter("brokerId");
			
			if(StringUtils.isEmpty(poIdStr)){
				System.err.println("Purchase Order ID not found.");
				error.setDescription("Purchase Order ID not found.");
				poTicketGroupOnHandStatusDTO.setError(error);
				poTicketGroupOnHandStatusDTO.setStatus(0);
				return poTicketGroupOnHandStatusDTO;
			}
			Integer poId = 0;
			try{
				poId = Integer.parseInt(poIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Purchase Order ID should be valid Integer value.");
				poTicketGroupOnHandStatusDTO.setError(error);
				poTicketGroupOnHandStatusDTO.setStatus(0);
				return poTicketGroupOnHandStatusDTO;
			}			
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				poTicketGroupOnHandStatusDTO.setError(error);
				poTicketGroupOnHandStatusDTO.setStatus(0);
				return poTicketGroupOnHandStatusDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				poTicketGroupOnHandStatusDTO.setError(error);
				poTicketGroupOnHandStatusDTO.setStatus(0);
				return poTicketGroupOnHandStatusDTO;
			}			
			
			PurchaseOrder purchaseOrder = null;
			purchaseOrder = DAORegistry.getPurchaseOrderDAO().getPurchaseOrderByIdAndBrokerId(poId, brokerId);
			if(purchaseOrder == null){
				System.err.println("Purchase Order not found.");
				error.setDescription("Purchase Order not found.");
				poTicketGroupOnHandStatusDTO.setError(error);
				poTicketGroupOnHandStatusDTO.setStatus(0);
				return poTicketGroupOnHandStatusDTO;
			}			

			List<TicketGroup> ticketGroupList = null;
			if(ticketGroupIds!=null && !ticketGroupIds.isEmpty() && onHand!=null && !onHand.isEmpty()){
				String arr[] = ticketGroupIds.split(",");
				if(arr.length>0){
					TicketGroup tg = null;
					ticketGroupList = new ArrayList<TicketGroup>();
					for(String id : arr){
						tg = DAORegistry.getTicketGroupDAO().get(Integer.parseInt(id));
						tg.setOnHand(onHand);
						ticketGroupList.add(tg);
					}
					DAORegistry.getTicketGroupDAO().updateAll(ticketGroupList);
					poTicketGroupOnHandStatusDTO.setMessage("Selected Ticket Group updated successfully.");
					
					//Tracking User Action
					String userActionMsg = "Ticket Group(s) OnHand Status Updated to "+onHand.toUpperCase()+". Ticket Group Id's - "+ticketGroupIds;					
					Util.userActionAudit(request, purchaseOrder.getId(), userActionMsg);
				}
			}
			
			ticketGroupList = DAORegistry.getTicketGroupDAO().getAllTicketGroupsByPOId(poId);
			for(TicketGroup ticketGroup : ticketGroupList){
				EventDetails event = DAORegistry.getEventDetailsDAO().get(ticketGroup.getEventId());
				ticketGroup.setEventName(event.getEventName());
				ticketGroup.setEventDateStr(event.getEventDateTimeStr());
				ticketGroup.setVenue(event.getBuilding());
			}
				
			if(ticketGroupList != null){
				poTicketGroupOnHandStatusDTO.setTicketGroupDTO(com.rtw.tracker.utils.Util.getPOTicketGroupArray(ticketGroupList));
			}
			poTicketGroupOnHandStatusDTO.setStatus(1);
			poTicketGroupOnHandStatusDTO.setTicketGroupPaginationDTO(PaginationUtil.getDummyPaginationParameters(ticketGroupList!=null?ticketGroupList.size():0));
			poTicketGroupOnHandStatusDTO.setPoId(poIdStr);
			
		}catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching/updating PO Ticket Group On Hand.");
			poTicketGroupOnHandStatusDTO.setError(error);
			poTicketGroupOnHandStatusDTO.setStatus(0);
		}
		return poTicketGroupOnHandStatusDTO;
	}
	
	@RequestMapping(value = "/getShippingAddressForFedex")
	public POGetShippingAddressForFedexDTO getShippingAddressForFedex(HttpServletRequest request, HttpServletResponse response){
		POGetShippingAddressForFedexDTO poGetShippingAddressForFedexDTO = new POGetShippingAddressForFedexDTO();
		Error error = new Error();
		
		try{
			String poIdStr = request.getParameter("poId");
			
			if(StringUtils.isEmpty(poIdStr)){
				System.err.println("Purchase Order ID not found.");
				error.setDescription("Purchase Order ID not found.");
				poGetShippingAddressForFedexDTO.setError(error);
				poGetShippingAddressForFedexDTO.setStatus(0);
				return poGetShippingAddressForFedexDTO;
			}
			Integer poId = 0;
			try{
				poId = Integer.parseInt(poIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Purchase Order ID should be valid Integer value.");
				poGetShippingAddressForFedexDTO.setError(error);
				poGetShippingAddressForFedexDTO.setStatus(0);
				return poGetShippingAddressForFedexDTO;
			}
			
			PurchaseOrder PO = DAORegistry.getPurchaseOrderDAO().get(poId);	
			if(PO == null){
				System.err.println("Purchase Order not found.");
				error.setDescription("Purchase Order not found.");
				poGetShippingAddressForFedexDTO.setError(error);
				poGetShippingAddressForFedexDTO.setStatus(0);
				return poGetShippingAddressForFedexDTO;
			}
			
			Integer customerId = PO.getCustomerId();
			if(customerId == null || customerId < 0){
				System.err.println("Customer not found.");
				error.setDescription("Customer not found.");
				poGetShippingAddressForFedexDTO.setError(error);
				poGetShippingAddressForFedexDTO.setStatus(0);
				return poGetShippingAddressForFedexDTO;
			}
			
				
			List<CustomerAddress> shippingAddressList = null;
			int shippingAddressCount = 0;
			shippingAddressList = DAORegistry.getCustomerAddressDAO().getShippingAddressInfo(customerId);
			shippingAddressCount = DAORegistry.getCustomerAddressDAO().getShippingAddressCount(customerId);
			Customer customer = DAORegistry.getCustomerDAO().get(PO.getCustomerId());
			
			if(shippingAddressList == null){
				poGetShippingAddressForFedexDTO.setMessage("No Shipping address found, Please add shipping Address.");
			}
			poGetShippingAddressForFedexDTO.setStatus(1);
			poGetShippingAddressForFedexDTO.setShippingAddressPaginationDTO(PaginationUtil.getDummyPaginationParameters(shippingAddressCount));
			poGetShippingAddressForFedexDTO.setShippingAddressDTO(com.rtw.tracker.utils.Util.getShippingAddressArray(shippingAddressList));
			poGetShippingAddressForFedexDTO.setShippingAddressCount(shippingAddressCount);
			poGetShippingAddressForFedexDTO.setPoId(poIdStr);
			poGetShippingAddressForFedexDTO.setCustomerId(customerId);
			poGetShippingAddressForFedexDTO.setCompanyName(customer.getCompanyName());
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching/updating PO Ticket Group On Hand.");
			poGetShippingAddressForFedexDTO.setError(error);
			poGetShippingAddressForFedexDTO.setStatus(0);
		}
		return poGetShippingAddressForFedexDTO;
	}
	
	@RequestMapping(value = "/CreateFedexLabelForPO")
	public GenericResponseDTO createFedexLabelForPO(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		
		try {
			String poIdStr = request.getParameter("poId");
			String shippingId = request.getParameter("shippingId");
			String serviceType = request.getParameter("serviceType");
			String signatureType = request.getParameter("signatureType");
			String companyName = request.getParameter("companyName");
			
			if(StringUtils.isEmpty(poIdStr)){
				System.err.println("Purchase Order ID not found.");
				error.setDescription("Purchase Order ID not found.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			Integer poId = 0;
			try{
				poId = Integer.parseInt(poIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Purchase Order ID should be valid Integer value.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			if(StringUtils.isEmpty(serviceType) || serviceType.equalsIgnoreCase("select")){
				System.err.println("Service Type not found.");
				error.setDescription("Service Type not found.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(signatureType)){
				System.err.println("Signature Type not found.");
				error.setDescription("Signature Type not found.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			SignatureOptionType signatureOptionType = null;
			signatureOptionType = SignatureOptionType.fromValue(signatureType);
			
			List<Recipient> recipients=DAORegistry.getQueryManagerDAO().getRecipientListByPO(Integer.parseInt(shippingId), Integer.parseInt(poIdStr));
			if(recipients == null || recipients.isEmpty()){
				System.err.println("Could not create Fedex label, No recipient found for selected PO.");
				error.setDescription("Could not create Fedex label, No recipient found for selected PO.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}			
			
			CreateFedexLabelUtil util = new CreateFedexLabelUtil();
			String trackingNo="";
			String msg="";
			
			 for(Recipient recipient:recipients){
				if(companyName != null && !companyName.isEmpty()){
					recipient.setCompanyName(companyName);
				}
				if(recipient.getCountryCode()==null){
					msg = "Could not create Fedex label, No country found for selected PO.";
					error.setDescription(msg);
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					return genericResponseDTO;
				}else if(recipient.getCountryCode().equalsIgnoreCase("US")){
					ServiceType service =  ServiceType.fromValue(serviceType);
					msg = util.createFedExShipLabel(recipient,service,signatureOptionType);// package type = your package						
		    	}else{
		    		ServiceType service =  ServiceType.fromValue(serviceType);
		    		msg = util.createFedExShipLabel(recipient,service,signatureOptionType);
		    	}
			 }
			 
			 if(msg != null && msg != ""){
				error.setDescription(msg);
				genericResponseDTO.setError(error) ;
				genericResponseDTO.setStatus(0);
			 }
			 
			 trackingNo = DAORegistry.getPurchaseOrderDAO().get(poId).getTrackingNo();
			 if(msg == null){
				 msg = "Fedex Label Created for selected PO with tracking no : "+trackingNo;
				 genericResponseDTO.setStatus(1);
				 genericResponseDTO.setMessage(msg);
				 
				//Tracking User Action
				String userActionMsg = "Fedex Label is Created for PO. Tracking no - "+trackingNo;
				Util.userActionAudit(request, poId, userActionMsg);
			 }
			 
		} catch (Exception e) {
			System.out.println("FedEx: "+e);
			e.printStackTrace();
			error.setDescription("Something went wrong while Creating Fedex Label For PO.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
	@RequestMapping(value = "/UploadEticket")
	public POUploadETicketDTO uploadEticket(HttpServletRequest request, HttpServletResponse response){
		POUploadETicketDTO poUploadETicketDTO = new POUploadETicketDTO();
		Error error = new Error();
		
		try {
			String action = request.getParameter("action");
			String poIdStr = request.getParameter("poId");
			String brokerIdStr = request.getParameter("brokerId");
			
			if(StringUtils.isEmpty(poIdStr)){
				System.err.println("Purchase Order ID not found.");
				error.setDescription("Purchase Order ID not found.");
				poUploadETicketDTO.setError(error);
				poUploadETicketDTO.setStatus(0);
				return poUploadETicketDTO;
			}
			Integer poId = 0;
			try{
				poId = Integer.parseInt(poIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Purchase Order ID should be valid Integer value.");
				poUploadETicketDTO.setError(error);
				poUploadETicketDTO.setStatus(0);
				return poUploadETicketDTO;
			}
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				poUploadETicketDTO.setError(error);
				poUploadETicketDTO.setStatus(0);
				return poUploadETicketDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				poUploadETicketDTO.setError(error);
				poUploadETicketDTO.setStatus(0);
				return poUploadETicketDTO;
			}
			
			String returnMessage = "";
			String shippingMethodName = "";
			
			Collection<TicketGroup> ticketGroups = null;
			Customer customerPOInfo = null;			
			Collection<ShippingMethod> shippingMethods = null;
			TicketGroupTicketAttachment tktGrpTktAttachement = null;
			
			PurchaseOrder poInfo = null;
			poInfo = DAORegistry.getPurchaseOrderDAO().getPurchaseOrderByIdAndBrokerId(poId, brokerId);
			if(poInfo == null){
				System.err.println("Purchase Order Details not found in System.");
				error.setDescription("Purchase Order Details not found in System.");
				poUploadETicketDTO.setError(error);
				poUploadETicketDTO.setStatus(0);
				return poUploadETicketDTO;
			}
			
			ticketGroups = DAORegistry.getTicketGroupDAO().getAllTicketGroupsByPOId(poId);
			if(ticketGroups == null){
				System.err.println("Ticket Group Details not found in System.");
				error.setDescription("Ticket Group Details not found in System.");
				poUploadETicketDTO.setError(error);
				poUploadETicketDTO.setStatus(0);
				return poUploadETicketDTO;
			}
			
			customerPOInfo = DAORegistry.getCustomerDAO().get(poInfo.getCustomerId());
			shippingMethods = DAORegistry.getShippingMethodDAO().getAll();
			List<Integer> ticketGroupsIds = new ArrayList<Integer>();
			for(TicketGroup ticketGroup : ticketGroups){
				ticketGroupsIds.add(ticketGroup.getId());
				tktGrpTktAttachement = DAORegistry.getTicketGroupTicketAttachmentDAO().getTicketByTicketGroupId(ticketGroup.getId());
				ticketGroup.setTicketGroupTicketAttachment(tktGrpTktAttachement);
			}
			boolean isPOMapped = false;
			String invoiceids="";
			if(ticketGroupsIds!=null && !ticketGroupsIds.isEmpty()){
				List<Ticket> tickets = DAORegistry.getTicketDAO().getTicketsByTicketGroupIds(ticketGroupsIds);
				for(Ticket ticket : tickets){
					if(ticket.getInvoiceId()!=null && ticket.getInvoiceId() >0){
						isPOMapped = true;
						invoiceids += ticket.getInvoiceId();
						break;
					}
				}				
			}
			
			if(isPOMapped){
				//jObj.put("msg", "Not allowed to update, Purchase order is mapped with invoice No. ("+invoiceids+")");
				error.setDescription("Not allowed to update, Purchase order is mapped with invoice No. ("+invoiceids+")");
				poUploadETicketDTO.setError(error);
				poUploadETicketDTO.setStatus(0);
				return poUploadETicketDTO;
			}
			
			if(action != null && action.equals("uploadPOInfo")){
				if(returnMessage.isEmpty()){
					
					Map<String, String[]> requestParams = request.getParameterMap();
					if(requestParams != null){						
						Integer rowNo = 0;
						Set<Integer> rowIdSet = new HashSet<Integer>();
						
//						String userName = SecurityContextHolder.getContext().getAuthentication().getName();
						for(Map.Entry<String, String[]> entry : requestParams.entrySet()){
							
							String key = entry.getKey();
							
							if(key.contains("rowId_")){
								rowNo = Integer.parseInt(key.replace("rowId_", ""));
								rowIdSet.add(rowNo);
							}
						}
						for(Integer rowIds : rowIdSet){
							rowNo = rowIds;
						for(Map.Entry<String, String[]> entry : requestParams.entrySet()){
							
							String key = entry.getKey();
														
							if(key.contains("ticketGroup_"+rowNo+"_")){
								Integer id= Integer.parseInt(key.replace("ticketGroup_"+rowNo+"_", ""));
								
								TicketGroup ticGroup = DAORegistry.getTicketGroupDAO().get(id);
								if(ticGroup != null){
									String instantDownloadStr = request.getParameter("instantdownload_"+rowNo+"_"+id);
									Boolean instantDownload = Boolean.parseBoolean(instantDownloadStr);
									
									List<TicketGroupTicketAttachment> ticketGrouptktAttachment = null;
									String appendFileName = "";
									TicketGroupTicketAttachment attachment;
									MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
									MultipartFile file = multipartRequest.getFile("file_"+rowNo+"_"+id);
									
									if(file!=null && file.getSize()>0){
									
									appendFileName = ticGroup.getId()+"_"+ticGroup.getEventName()+"_"+ticGroup.getEventDateStr()+"_"+ticGroup.getEventTimeStr()+"_"; 
									File newFile = null;
									String ext = FilenameUtils.getExtension(file.getOriginalFilename());
									String fullFileName = "";
									String sectionStr =  ticGroup.getSection();
									if(sectionStr != null && !sectionStr.isEmpty()){
										appendFileName += sectionStr+"_";
									}
									String rowStr = ticGroup.getRow();
									if(rowStr != null && !rowStr.isEmpty()){
										appendFileName += rowStr+"_";
									}
									if(ticGroup.getSeatLow() != null && !ticGroup.getSeatLow().isEmpty()){
										appendFileName += ticGroup.getSeatLow()+"_";
									}
									if(ticGroup.getSeatHigh() != null && !ticGroup.getSeatHigh().isEmpty()){
										appendFileName += ticGroup.getSeatHigh()+"_";
									}
									appendFileName = appendFileName.replace(" ", "_");
									appendFileName = appendFileName.replace("/", "_");
									appendFileName = appendFileName.replace(":", "_");
									fullFileName = URLUtil.TICKET_DIRECTORY+appendFileName+"_eticket_1"+"."+ext;
									
									ticketGrouptktAttachment = DAORegistry.getTicketGroupTicketAttachmentDAO().getAttachmentByTicketGroupAndFileType(ticGroup.getId(),FileType.ETICKET);
									if(!ticketGrouptktAttachment.isEmpty()){
										for(TicketGroupTicketAttachment attach : ticketGrouptktAttachment){
											File f = new File(attach.getFilePath());
											if(f.exists()){
												f.delete();
											}
										}
										DAORegistry.getTicketGroupTicketAttachmentDAO().deleteAll(ticketGrouptktAttachment);
									}
									
									attachment = new TicketGroupTicketAttachment();
									attachment.setTicketGroupId(ticGroup.getId());
									attachment.setFilePath(fullFileName);
									attachment.setExtension(ext);
									attachment.setPosition(1);
									attachment.setType(FileType.ETICKET);
									attachment.setUploadedBy("AUTO");
									attachment.setUploadedDateTime(new Date());									
										
									try {
										newFile = new File(fullFileName);
										newFile.createNewFile();
										
										BufferedOutputStream stream = new BufferedOutputStream(
												new FileOutputStream(newFile));
								        FileCopyUtils.copy(file.getInputStream(), stream);
										stream.close();
										DAORegistry.getTicketGroupTicketAttachmentDAO().save(attachment);
									} catch (Exception e) {
										e.printStackTrace();
									}
									
									//Update TicketGroup table for etickets
									ticGroup.setInstantDownload(instantDownload);
									ticGroup.setEticketsAttached(true);
									ticGroup.setEticketsAttachedDate(new Date());
									DAORegistry.getTicketGroupDAO().update(ticGroup);
									}
								}
							}
							}
						}
					}
					poUploadETicketDTO.setMessage("Purchase Order updated successfully");
					poUploadETicketDTO.setStatus(1);
					
					//Tracking User Action
					String userActionMsg = "Purchase Order Ticket Uploaded Successfully.";
					Util.userActionAudit(request, poId, userActionMsg);
				}
				PurchaseOrder poInfoUpdated = DAORegistry.getPurchaseOrderDAO().get(poId);
				//PurchaseOrderPaymentDetails paymentDetailsUpdated = DAORegistry.getPurchaseOrderPaymentDetailsDAO().getPaymentDetailsByPOId(Integer.parseInt(purchaseOrderId));
				Collection<TicketGroup> ticketGroupsUpdated = DAORegistry.getTicketGroupDAO().getAllTicketGroupsByPOId(poId);
				for(TicketGroup ticketGroup : ticketGroupsUpdated){
					tktGrpTktAttachement = DAORegistry.getTicketGroupTicketAttachmentDAO().getTicketByTicketGroupId(ticketGroup.getId());
					ticketGroup.setTicketGroupTicketAttachment(tktGrpTktAttachement);
				}
				
				/*poUploadETicketDTO.setStatus(1);
				poUploadETicketDTO.setTicketgroupDTO(com.rtw.tracker.utils.Util.getPOTicketGroupArray(ticketGroups));
				poUploadETicketDTO.setPurchaseOrderDTO(com.rtw.tracker.utils.Util.getPurchaseOrderObject(poInfo, shippingMethods));
				poUploadETicketDTO.setCustomersDTO(com.rtw.tracker.utils.Util.getPOCustomerObject(customerPOInfo));
				*/
			}
			
			poUploadETicketDTO.setStatus(1);
			poUploadETicketDTO.setTicketgroupDTO(com.rtw.tracker.utils.Util.getPOUploadTicketGroupArray(ticketGroups));
			poUploadETicketDTO.setPurchaseOrderDTO(com.rtw.tracker.utils.Util.getPurchaseOrderObject(poInfo, shippingMethods));
			poUploadETicketDTO.setCustomersDTO(com.rtw.tracker.utils.Util.getPOCustomerObject(customerPOInfo));
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while Fetching/Updating Upload E-Ticket Info for PO.");
			poUploadETicketDTO.setError(error);
			poUploadETicketDTO.setStatus(0);
		}
		return poUploadETicketDTO;
	}

	@RequestMapping({"/CreatePurchaseOrder"})
	public POCreateDTO createPurchaseOrder(HttpServletRequest request, HttpServletResponse response){
		POCreateDTO poCreateDTO = new POCreateDTO();
		Error error = new Error();
		
		try{
			Collection<ShippingMethod> shippingMethods = null;
			shippingMethods = DAORegistry.getShippingMethodDAO().getAll();
			
			poCreateDTO.setStatus(1);
			poCreateDTO.setShippingMethodDTO(com.rtw.tracker.utils.Util.getShippingMethodArray(shippingMethods));
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return poCreateDTO;
	}
	
	@RequestMapping(value= "/SavePO")
	public POCreateDTO savePurchaseOrder(HttpServletRequest request, HttpServletResponse response){
		POCreateDTO poCreateDTO = new POCreateDTO(); 
		Error error = new Error();
		
		try{
			String action = request.getParameter("action");
			String customerId = request.getParameter("customerId");
			String userName = request.getParameter("userName");
			String brokerIdStr = request.getParameter("brokerId");
			
			if(StringUtils.isEmpty(customerId)){
				System.err.println("Customer Id not found.");
				error.setDescription("Customer Id not found.");
				poCreateDTO.setError(error);
				poCreateDTO.setStatus(0);
				return poCreateDTO;
			}
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				poCreateDTO.setError(error);
				poCreateDTO.setStatus(0);
				return poCreateDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				poCreateDTO.setError(error);
				poCreateDTO.setStatus(0);
				return poCreateDTO;
			}
			
			String shippingMethodName = null;
			PurchaseOrder purchaseOrder = null;
			List<TicketGroup> ticketGroups = null;
			PurchaseOrderPaymentDetails	paymentDetails = null;
			CustomerAddress customerAddress = null;
			
			if(action != null && action.equalsIgnoreCase("savePO")){
				Map<String, String[]> requestParams = request.getParameterMap();
				String paymentType = request.getParameter("paymentType");
				
				if(requestParams != null){
					DateFormat formatDateTime = new SimpleDateFormat("MM/dd/yyyy HH:mm");
					purchaseOrder = new PurchaseOrder();
					
					String shippingMethod = request.getParameter("shippingMethod");
					Integer shippingMethodId = -1;
					if(shippingMethod!=null && !shippingMethod.isEmpty()){
						shippingMethodId = Integer.parseInt(shippingMethod);
					}
					customerAddress = DAORegistry.getCustomerAddressDAO().getCustomerAddressInfo(Integer.parseInt(customerId));
					purchaseOrder.setCustomerId(Integer.parseInt(customerId));
					purchaseOrder.setStatus(POStatus.ACTIVE);
					String totalPrice = request.getParameter("totalPrice");
					String poType = request.getParameter("poType");
					purchaseOrder.setPoTotal(Double.parseDouble(totalPrice));
					purchaseOrder.setTicketCount(Integer.parseInt(request.getParameter("totalQty")));
					purchaseOrder.setExternalPONo(request.getParameter("externalPONo"));
					
					purchaseOrder.setTrackingNo(request.getParameter("trackingNo"));
					purchaseOrder.setTransactionOffice(request.getParameter("transactionOffice"));
					if(shippingMethodId>=0){
						purchaseOrder.setShippingMethodId(shippingMethodId);
						ShippingMethod shipMethod = DAORegistry.getShippingMethodDAO().getShippingMethodById(shippingMethodId);
						if(shipMethod!=null){
							shippingMethodName = shipMethod.getName();
						}
					}
					purchaseOrder.setPurchaseOrderType("REGULAR");
					if(poType!=null && !poType.isEmpty()){
						purchaseOrder.setPurchaseOrderType(poType);
						if(poType.equalsIgnoreCase("CONSIGNMENT")){
							purchaseOrder.setConsignmentPoNo(request.getParameter("consignmentPo"));
						}else{
							purchaseOrder.setConsignmentPoNo(null);
						}
					}
					purchaseOrder.setCreatedTime(new Date());
					purchaseOrder.setCreatedBy(userName);
					purchaseOrder.setCsr(userName);
					purchaseOrder.setLastUpdated(new Date());
					purchaseOrder.setBrokerId(brokerId); //Setting Broker ID
					
					//persist purchase order info for customer into db
					//LOGGER.info("Persisting purchase order details for customer " +customerId);
					DAORegistry.getPurchaseOrderDAO().save(purchaseOrder);
					
					paymentDetails = new PurchaseOrderPaymentDetails();
					paymentDetails.setName(request.getParameter("name"));
					String paymentDateStr = request.getParameter("paymentDate");
					if(paymentDateStr!=null && !paymentDateStr.isEmpty()){
						paymentDateStr += " 00:00:00";
						paymentDetails.setPaymentDate(formatDateTime.parse(paymentDateStr));
					}
					paymentDetails.setPaymentStatus(request.getParameter("paymentStatus"));
					paymentDetails.setPaymentNote(request.getParameter("paymentNote"));
					paymentDetails.setAmount(Double.parseDouble(totalPrice));
					paymentDetails.setPurchaseOrderId(purchaseOrder.getId());
					paymentDetails.setPaymentType(paymentType);
					
					System.out.println("Payment Type: "+paymentType);
					
					if(paymentType != null && paymentType.equalsIgnoreCase("card")){
						paymentDetails.setCardLastFourDigit(request.getParameter("cardNo"));
						paymentDetails.setCardType(request.getParameter("cardType"));
						paymentDetails.setAccountLastFourDigit(null);
						paymentDetails.setChequeNo(null);
						paymentDetails.setRoutingLastFourDigit(null);
					}else if(paymentType !=null && paymentType.equalsIgnoreCase("account")){
						paymentDetails.setName(request.getParameter("accountName"));
						paymentDetails.setCardLastFourDigit(null);
						paymentDetails.setCardType(null);
						paymentDetails.setAccountLastFourDigit(request.getParameter("accountNo"));
						paymentDetails.setChequeNo(null);
						paymentDetails.setRoutingLastFourDigit(request.getParameter("routingNo"));
					}else if(paymentType !=null && paymentType.equalsIgnoreCase("cheque")){
						paymentDetails.setName(request.getParameter("chequeName"));
						paymentDetails.setCardLastFourDigit(null);
						paymentDetails.setCardType(null);
						paymentDetails.setAccountLastFourDigit(request.getParameter("accountChequeNo"));
						paymentDetails.setChequeNo(request.getParameter("checkNo"));
						paymentDetails.setRoutingLastFourDigit(request.getParameter("routingChequeNo"));
					}
					if(paymentType != null)
						DAORegistry.getPurchaseOrderPaymentDetailsDAO().saveOrUpdate(paymentDetails);
					
					ticketGroups = new ArrayList<TicketGroup>();
					EventDetails event = null;
					Integer rowNo = 0;
					
					//Map<String, String[]> treeMap = new TreeMap<String, String[]>(requestParams);
					
					for(Map.Entry<String, String[]> entry : requestParams.entrySet()){
						TicketGroup ticketGroup= new TicketGroup();
						String key = entry.getKey();
						
						if(key.contains("rowId_")){
							rowNo = Integer.parseInt(key.replace("rowId_", ""));
						//}
						//if(key.contains("eventId_"+rowNo+"_")){	
							
							Integer id = 0;
							id = Integer.parseInt(request.getParameter("eventId_"+rowNo));
							/*if(key.contains("eventId_"+rowNo+"_")){
								id = Integer.parseInt(key.replace("eventId_"+rowNo+"_", ""));
							}*/
							
							ticketGroup.setStatus(OrderStatus.ACTIVE.toString());
							ticketGroup.setSection(request.getParameter("section_"+rowNo));
							ticketGroup.setRow(request.getParameter("row_"+rowNo));
							ticketGroup.setEventId(id);
							ticketGroup.setQuantity(Integer.parseInt(request.getParameter("qty_"+rowNo)));
							ticketGroup.setSeatLow(request.getParameter("seatLow_"+rowNo));
							ticketGroup.setSeatHigh(request.getParameter("seatHigh_"+rowNo));
							ticketGroup.setPrice(Double.parseDouble(request.getParameter("price_"+rowNo)));
							if(shippingMethodId>=0){
								ticketGroup.setShippingMethodId(shippingMethodId);
								if(shippingMethodName != null && !shippingMethodName.isEmpty()){
									ticketGroup.setShippingMethod(shippingMethodName);
								}
							}
							ticketGroup.setCreatedDate(new Date());
							ticketGroup.setInvoiceId(0);
							ticketGroup.setCreatedBy(userName);
							ticketGroup.setNotes(null);
							ticketGroup.setBroadcast(true);
							
							event = DAORegistry.getEventDetailsDAO().get(id);
							ticketGroup.setEventName(event.getEventName());
							ticketGroup.setEventDate(event.getEventDate());
							ticketGroup.setEventTime(event.getEventTime());
							ticketGroup.setPurchaseOrderId(purchaseOrder.getId());
							ticketGroup.setBrokerId(brokerId); //Setting Broker ID
							if(ticketGroup.getQuantity()>0 && ticketGroup.getSeatLow()!=null && !ticketGroup.getSeatLow().isEmpty()){
								DAORegistry.getTicketGroupDAO().save(ticketGroup);
								ticketGroups.add(ticketGroup);
								Ticket ticket = null;
								int seatNo = Integer.parseInt(ticketGroup.getSeatLow());
								for(int i=0;i<ticketGroup.getQuantity();i++){
									ticket = new Ticket(ticketGroup);
									ticket.setSeatNo(String.valueOf(seatNo+i));
									ticket.setSeatOrder(i+1);
									ticket.setUserName(userName);
									ticket.setTicketStatus(TicketStatus.ACTIVE);
									DAORegistry.getTicketDAO().save(ticket);
								}
							}
								
						}
					}
					//LOGGER.debug("Persisted purchase order details in database for customer " +customerId);
				}
				poCreateDTO.setStatus(1);				
			}
			//jObj.put("purchaseOrder", purchaseOrder);
			//jObj.put("paymentDetails", paymentDetails);
			//jObj.put("ticketGroups", ticketGroups);
			//jObj.put("customerInfo", customerAddress);
			//jObj.put("shippingMethod", shippingMethodName);
			poCreateDTO.setPoId(purchaseOrder.getId());
			poCreateDTO.setMessage("Purchase order created successfully.");
			
			//Tracking User Action
			String userActionMsg = "Purchase Order Created Successfully.";
			Util.userActionAudit(request, purchaseOrder.getId(), userActionMsg);
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while Creating PO.");
			poCreateDTO.setError(error);
			poCreateDTO.setStatus(0);
		}
		return poCreateDTO;
	}
	
	@RequestMapping(value = "/EditPurchaseOrder")
	public POUpdateDTO editPurchaseOrder(HttpServletRequest request, HttpServletResponse response){
		POUpdateDTO poUpdateDTO = new POUpdateDTO();
		Error error = new Error();
		
		try {
			String action =  request.getParameter("action");
			String purchaseOrderId = request.getParameter("poId");
			String customerId = request.getParameter("customerId");			
			String paymentType = request.getParameter("paymentType");
			String userName = request.getParameter("userName");
			String brokerIdStr = request.getParameter("brokerId");
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				poUpdateDTO.setError(error);
				poUpdateDTO.setStatus(0);
				return poUpdateDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				poUpdateDTO.setError(error);
				poUpdateDTO.setStatus(0);
				return poUpdateDTO;
			}
			if(StringUtils.isEmpty(purchaseOrderId)){
				System.err.println("Purchase Order Id not found.");
				error.setDescription("Purchase Order Id not found.");
				poUpdateDTO.setError(error);
				poUpdateDTO.setStatus(0);
				return poUpdateDTO;
			}
			
			String returnMessage = "";
			String shippingMethodName = null;
			
			PurchaseOrderPaymentDetails paymentDetails = null;
			Collection<TicketGroup> ticketGroups = null;
			Customer customerPOInfo = null;
			CustomerAddress customerAddress = null;
			Collection<ShippingMethod> shippingMethods = null;
			DateFormat formatDateTime = new SimpleDateFormat("MM/dd/yyyy HH:mm");
			
			PurchaseOrder poInfo = DAORegistry.getPurchaseOrderDAO().getPurchaseOrderByIdAndBrokerId(Integer.parseInt(purchaseOrderId), brokerId);
			if(poInfo == null){
				System.err.println("Purchase Order Details not found.");
				error.setDescription("Purchase Order Details not found.");
				poUpdateDTO.setError(error);
				poUpdateDTO.setStatus(0);
				return poUpdateDTO;
			}
			
			paymentDetails = DAORegistry.getPurchaseOrderPaymentDetailsDAO().getPaymentDetailsByPOId(Integer.parseInt(purchaseOrderId));
			ticketGroups = DAORegistry.getTicketGroupDAO().getAllTicketGroupsByPOId(Integer.parseInt(purchaseOrderId));
			customerPOInfo = DAORegistry.getCustomerDAO().get(poInfo.getCustomerId());
			customerAddress = DAORegistry.getCustomerAddressDAO().getCustomerAddressInfo(poInfo.getCustomerId());
			shippingMethods = DAORegistry.getShippingMethodDAO().getAll();
			
			List<Integer> ticketGroupsIds = new ArrayList<Integer>();
			for(TicketGroup ticketGroup : ticketGroups){
				ticketGroupsIds.add(ticketGroup.getId());
			}
		
			//To add all Ticket Group Object into Map
			Map<Integer, TicketGroup> ticketGroupMap = new HashMap<Integer, TicketGroup>();
			for(TicketGroup ticketGroup : ticketGroups){
				ticketGroupMap.put(ticketGroup.getId(), ticketGroup);
			}
			
			boolean isPOMapped = false;
			boolean isPOOnHold = false;
			String invoiceids="";
			if(ticketGroupsIds!=null && !ticketGroupsIds.isEmpty()){
				List<Ticket> tickets = DAORegistry.getTicketDAO().getTicketsByTicketGroupIds(ticketGroupsIds);
				for(Ticket ticket : tickets){
					if(ticket.getTicketStatus().equals(TicketStatus.SOLD)){
						if(ticket.getInvoiceId()!=null && ticket.getInvoiceId() >0){
							isPOMapped = true;
							invoiceids += ticket.getInvoiceId();
							break;
						}
					}else if(ticket.getTicketStatus().equals(TicketStatus.ONHOLD)){
						isPOOnHold = true;
						break;
					}
				}
			}
			
			if(isPOMapped){
				System.err.println("Not allowed to update, Selected Purchase Order is mapped with invoice No. ("+invoiceids+")");
				error.setDescription("Not allowed to update, Selected Purchase Order is mapped with invoice No. ("+invoiceids+")");
				poUpdateDTO.setError(error);
				poUpdateDTO.setStatus(0);
				return poUpdateDTO;
			}
			if(isPOOnHold){
				System.err.println("Not Allowed to Update PO : Some tickets quantity of PO is on Hold.");
				error.setDescription("Not Allowed to Update PO : Some tickets quantity of PO is on Hold.");
				poUpdateDTO.setError(error);
				poUpdateDTO.setStatus(0);
				return poUpdateDTO;
			}
		
			if(action != null && action.equals("editPOInfo")){
				if(returnMessage.isEmpty()){
					//LOGGER.debug("Purchase order to update :: " +purchaseOrderId);
					String customeName = request.getParameter("customerName");
					String customeType = request.getParameter("customerType");
					String shippingType = request.getParameter("shippingMethod");
					String poTotal = request.getParameter("totalPrice");
					String tixCount = request.getParameter("totalQty");
					Integer shippingMethodId = -1;
					
					//String status = request.getParameter("status");
					String extPo = request.getParameter("externalPONo");
					String poType = request.getParameter("poType");
					String consignmentPo = request.getParameter("consignmentPo");
					String trackingNo = request.getParameter("trackingNo");
					String transactionOffice = request.getParameter("transactionOffice");
					//String paymentType = request.getParameter("paymentType");
					
					PurchaseOrder purchaseOrder = poInfo;
					//purchaseOrder.setId(Integer.parseInt(purchaseOrderId));
					//purchaseOrder.setCustomerId(Integer.parseInt(customerId));
					//purchaseOrder.setCreatedTime(poCreated);
					
					if(shippingType == null || shippingType.isEmpty()){
						System.err.println("Shipping Method not found.");
						error.setDescription("Shipping Method not found.");
						poUpdateDTO.setError(error);
						poUpdateDTO.setStatus(0);
						return poUpdateDTO;
					}
					try{
						shippingMethodId = Integer.parseInt(shippingType);
					}catch (Exception e) {
						e.printStackTrace();
						error.setDescription("Not able to identify Shipping Method.");
						poUpdateDTO.setError(error);
						poUpdateDTO.setStatus(0);
						return poUpdateDTO;
					}
					
					try {
						Double poTotalDouble = Double.parseDouble(poTotal); 
						purchaseOrder.setPoTotal(Double.parseDouble(poTotal));
					}catch (Exception e) {
						e.printStackTrace();
						error.setDescription("Bad value found in Total Price, It should be valid Double value.");
						poUpdateDTO.setError(error);
						poUpdateDTO.setStatus(0);
						return poUpdateDTO;
					}
					
					try {
						Integer tixCountInt = Integer.parseInt(tixCount); 
						purchaseOrder.setTicketCount(Integer.parseInt(tixCount));
					}catch (Exception e) {
						e.printStackTrace();
						error.setDescription("Bad value found in Total Quantity, It should be valid Integer value.");
						poUpdateDTO.setError(error);
						poUpdateDTO.setStatus(0);
						return poUpdateDTO;
					}
					
					purchaseOrder.setStatus(POStatus.ACTIVE);
					purchaseOrder.setBrokerId(brokerId);
					
					purchaseOrder.setTrackingNo(trackingNo);
					purchaseOrder.setTransactionOffice(transactionOffice);
					purchaseOrder.setExternalPONo(extPo);
					purchaseOrder.setLastUpdated(new Date());
					purchaseOrder.setPurchaseOrderType("REGULAR");
					if(poType!=null && !poType.isEmpty()){
						purchaseOrder.setPurchaseOrderType(poType);
						if(poType.equalsIgnoreCase("CONSIGNMENT")){
							purchaseOrder.setConsignmentPoNo(consignmentPo);
						}else{
							purchaseOrder.setConsignmentPoNo(null);
						}
					}
					
					if(shippingMethodId>=0){
						purchaseOrder.setShippingMethodId(shippingMethodId);
						ShippingMethod shipMethod = DAORegistry.getShippingMethodDAO().getShippingMethodById(shippingMethodId);
						if(shipMethod!=null){
							shippingMethodName = shipMethod.getName();
						}
					}					
					DAORegistry.getPurchaseOrderDAO().update(purchaseOrder);
					
					if(paymentDetails==null){
						paymentDetails = new PurchaseOrderPaymentDetails();
					}
					paymentDetails.setName(request.getParameter("name"));
					String paymentDateStr = request.getParameter("paymentDate");
					if(paymentDateStr!=null && !paymentDateStr.isEmpty()){
						paymentDateStr += " 00:00:00";
						paymentDetails.setPaymentDate(formatDateTime.parse(paymentDateStr));
					}
					paymentDetails.setPaymentStatus(request.getParameter("paymentStatus"));
					paymentDetails.setPaymentNote(request.getParameter("paymentNote"));
					paymentDetails.setAmount(Double.parseDouble(poTotal));
					paymentDetails.setPurchaseOrderId(Integer.parseInt(purchaseOrderId));
					paymentDetails.setPaymentType(paymentType);
					if(paymentType !=null && paymentType.equalsIgnoreCase("card")){
						paymentDetails.setCardLastFourDigit(request.getParameter("cardNo"));
						paymentDetails.setCardType(request.getParameter("cardType"));
						paymentDetails.setAccountLastFourDigit(null);
						paymentDetails.setChequeNo(null);
						paymentDetails.setRoutingLastFourDigit(null);
					}else if(paymentType !=null && paymentType.equalsIgnoreCase("account")){
						paymentDetails.setName(request.getParameter("accountName"));
						paymentDetails.setCardLastFourDigit(null);
						paymentDetails.setCardType(null);
						paymentDetails.setAccountLastFourDigit(request.getParameter("accountNo"));
						paymentDetails.setChequeNo(null);
						paymentDetails.setRoutingLastFourDigit(request.getParameter("routingNo"));
					}else if(paymentType !=null && paymentType.equalsIgnoreCase("cheque")){
						paymentDetails.setName(request.getParameter("chequeName"));
						paymentDetails.setCardLastFourDigit(null);
						paymentDetails.setCardType(null);
						paymentDetails.setAccountLastFourDigit(request.getParameter("accountChequeNo"));
						paymentDetails.setChequeNo(request.getParameter("checkNo"));
						paymentDetails.setRoutingLastFourDigit(request.getParameter("routingChequeNo"));
					}
					DAORegistry.getPurchaseOrderPaymentDetailsDAO().saveOrUpdate(paymentDetails);
					
					TicketGroup tixGroup = null;
					
					Map<String, String[]> requestParams = request.getParameterMap();
					if(requestParams != null){
						EventDetails event = null;
						String section = "";
						String row = "";
						String qty = "";
						String seatLow = "";
						String seatHigh = "";
						String price = "";
						Integer rowNo = 0;
						
						//Map<String, String[]> treeMap = new TreeMap<String, String[]>(requestParams);
						 
						for(Map.Entry<String, String[]> entry : requestParams.entrySet()){
							tixGroup = new TicketGroup();
							String key = entry.getKey();
							
							if(key.contains("rowId_")){
								rowNo = Integer.parseInt(key.replace("rowId_", ""));
							//}
							//if(key.contains("ticketGroup_"+rowNo+"_")){
								Integer id = 0;
								id = Integer.parseInt(request.getParameter("ticketGroup_"+rowNo));
								/*if(key.contains("ticketGroup_"+rowNo+"_")){
									id = Integer.parseInt(key.replace("ticketGroup_"+rowNo+"_", ""));
								}*/
								
								TicketGroup ticGroup = ticketGroupMap.remove(id);
								/*if(id != null && id > 0){
									ticGroup = DAORegistry.getTicketGroupDAO().get(id);
								}*/
								if(ticGroup != null){
									section = request.getParameter("section_"+rowNo);
									row = request.getParameter("row_"+rowNo);
									qty = request.getParameter("qty_"+rowNo);
									seatLow = request.getParameter("seatLow_"+rowNo);
									seatHigh = request.getParameter("seatHigh_"+rowNo);
									price = request.getParameter("price_"+rowNo);									
									if(section == null && row == null && qty == null && seatLow == null && seatHigh == null && price == null){										
										List<Ticket> tempTickets = DAORegistry.getTicketDAO().getAllTicketsByTicketGroupId(ticGroup.getId());
										DAORegistry.getTicketDAO().deleteAll(tempTickets); //Remove all tickets										
										DAORegistry.getTicketGroupDAO().delete(ticGroup); //Remove Ticket Group
									}else{
									tixGroup.setId(ticGroup.getId());
									tixGroup.setSection(section);
									tixGroup.setRow(row);
									tixGroup.setEventId(ticGroup.getEventId());
									tixGroup.setStatus(OrderStatus.ACTIVE.toString());
									tixGroup.setQuantity(Integer.parseInt(qty));
									tixGroup.setSeatLow(seatLow);
									tixGroup.setSeatHigh(seatHigh);
									tixGroup.setPrice(Double.parseDouble(price));	
									if(shippingMethodId>=0){
										tixGroup.setShippingMethodId(shippingMethodId);										
										if(shippingMethodName != null && !shippingMethodName.isEmpty()){
											tixGroup.setShippingMethod(shippingMethodName);
										}
									}
									tixGroup.setCreatedDate(new Date());
									tixGroup.setBrokerId(brokerId);
									tixGroup.setInvoiceId(0);
									tixGroup.setCreatedBy(userName);
									tixGroup.setNotes(null);
									tixGroup.setPurchaseOrderId(Integer.parseInt(purchaseOrderId));
									event = DAORegistry.getEventDetailsDAO().get(ticGroup.getEventId());
									tixGroup.setEventName(event.getEventName());
									tixGroup.setEventDate(event.getEventDate());
									tixGroup.setEventTime(event.getEventTime());
									
									DAORegistry.getTicketGroupDAO().update(tixGroup);
									}
								}else{
									tixGroup.setSection(request.getParameter("section_"+rowNo));
									tixGroup.setRow(request.getParameter("row_"+rowNo));
									tixGroup.setEventId(id);
									tixGroup.setStatus(OrderStatus.ACTIVE.toString());
									tixGroup.setQuantity(Integer.parseInt(request.getParameter("qty_"+rowNo)));
									tixGroup.setSeatLow(request.getParameter("seatLow_"+rowNo));
									tixGroup.setSeatHigh(request.getParameter("seatHigh_"+rowNo));
									tixGroup.setPrice(Double.parseDouble(request.getParameter("price_"+rowNo)));
									if(shippingMethodId>=0){
										tixGroup.setShippingMethodId(shippingMethodId);
										if(shippingMethodName != null && !shippingMethodName.isEmpty()){
											tixGroup.setShippingMethod(shippingMethodName);
										}
									}
									tixGroup.setCreatedDate(new Date());
									tixGroup.setBrokerId(brokerId);
									tixGroup.setInvoiceId(0);
									tixGroup.setCreatedBy(userName);
									tixGroup.setNotes(null);
									tixGroup.setBroadcast(true);
									tixGroup.setPurchaseOrderId(Integer.parseInt(purchaseOrderId));
									event = DAORegistry.getEventDetailsDAO().get(id);
									tixGroup.setEventName(event.getEventName());
									tixGroup.setEventDate(event.getEventDate());
									tixGroup.setEventTime(event.getEventTime());
									
									DAORegistry.getTicketGroupDAO().saveOrUpdate(tixGroup);
								}
								List<Ticket> oldTickets = DAORegistry.getTicketDAO().getAllTicketsByTicketGroupId(tixGroup.getId());
								if(oldTickets != null && !oldTickets.isEmpty()){
									DAORegistry.getTicketDAO().deleteAll(oldTickets);
								}
								if(tixGroup != null){
									if(tixGroup.getQuantity() != null && tixGroup.getQuantity()>0 && tixGroup.getSeatLow()!=null && !tixGroup.getSeatLow().isEmpty()){
										Ticket ticket = null;
										int seatNo = Integer.parseInt(tixGroup.getSeatLow());
										for(int i=0;i<tixGroup.getQuantity();i++){
											ticket = new Ticket(tixGroup);
											ticket.setSeatNo(String.valueOf(seatNo+i));
											ticket.setSeatOrder(i+1);
											ticket.setUserName(userName);
											ticket.setTicketStatus(TicketStatus.ACTIVE);
											DAORegistry.getTicketDAO().save(ticket);
										}
									}
								}
							}
						}
						
						//To remove those values not updated
						for(Integer tixGrpId: ticketGroupMap.keySet()){
							TicketGroup ticGroupObj = ticketGroupMap.get(tixGrpId);
							
							DAORegistry.getTicketDAO().getAllTicketsToDeleteByTicketGroupId(ticGroupObj.getId());//Remove All Tickets from Ticket Group Id																	
							DAORegistry.getTicketGroupDAO().delete(ticGroupObj); //Remove Ticket Group
						}
						
					}
					poUpdateDTO.setMessage("Purchase Order updated successfully");
					poUpdateDTO.setStatus(1);
					
					//Tracking User Action
					String userActionMsg = "Purchase Order Updated Successfully.";
					Util.userActionAudit(request, Integer.parseInt(purchaseOrderId), userActionMsg);
				}
				PurchaseOrder poInfoUpdated = DAORegistry.getPurchaseOrderDAO().get(Integer.parseInt(purchaseOrderId));
				PurchaseOrderPaymentDetails paymentDetailsUpdated = DAORegistry.getPurchaseOrderPaymentDetailsDAO().getPaymentDetailsByPOId(Integer.parseInt(purchaseOrderId));
				Collection<TicketGroup> ticketGroupsUpdated = DAORegistry.getTicketGroupDAO().getAllTicketGroupsByPOId(Integer.parseInt(purchaseOrderId));				
		
				poUpdateDTO.setTicketGroupDTO(com.rtw.tracker.utils.Util.getPOUploadTicketGroupArray(ticketGroupsUpdated));
				poUpdateDTO.setPaymentDetailsDTO(com.rtw.tracker.utils.Util.getPOPaymentHistoryObject(paymentDetailsUpdated));
				poUpdateDTO.setPurchaseOrderDTO(com.rtw.tracker.utils.Util.getPurchaseOrderObject(poInfoUpdated));
				poUpdateDTO.setCustomerAddressDTO(com.rtw.tracker.utils.Util.getCustomerAddressObject(customerAddress));
				poUpdateDTO.setShippingMethodName(shippingMethodName);
				
			}else if(action != null && action.equals("voidPO")){
				//LOGGER.info("Void po request processing....");
				
				//System.out.println(customerId);
				String customeName = request.getParameter("customerName");
				String customeType = request.getParameter("customerType");
				//String createdTime = request.getParameter("createdTime");
				String consignmentPo = request.getParameter("consignmentPo");
				String trackingNo = request.getParameter("trackingNo");
				String transactionOffice = request.getParameter("transactionOffice");
				
				String poTotal = request.getParameter("totalPrice");
				String tixCount = request.getParameter("totalQty");
				String shippingType = request.getParameter("shippingMethod");
				//String paymentType = request.getParameter("paymentType");
				
				Integer shippingMethodId = -1;
				
				//String status = request.getParameter("status");
				String extPo = request.getParameter("externalPONo");
				
				PurchaseOrder purchaseOrder = poInfo;
				//purchaseOrder.setId(Integer.parseInt(purchaseOrderId));
				//purchaseOrder.setCustomerId(Integer.parseInt(customerId));
				//purchaseOrder.setCreatedTime(poCreated);
				
				if(shippingType == null || shippingType.isEmpty()){
					error.setDescription("Not able to indentify Shipping Method.");
					poUpdateDTO.setError(error);
					poUpdateDTO.setStatus(0);
					return poUpdateDTO;
				}
				try{
					shippingMethodId = Integer.parseInt(shippingType);
				}catch (Exception e) {
					e.printStackTrace();
					error.setDescription("Shipping Method should be valid value.");
					poUpdateDTO.setError(error);
					poUpdateDTO.setStatus(0);
					return poUpdateDTO;
				}
				
				try {
					Double poTotalDouble = Double.parseDouble(poTotal);
					purchaseOrder.setPoTotal(Double.parseDouble(poTotal));
				}catch (Exception e) {
					e.printStackTrace();
					error.setDescription("Bad value found in Total Price, It should be valid Double value.");
					poUpdateDTO.setError(error);
					poUpdateDTO.setStatus(0);
					return poUpdateDTO;
				}
				
				try {
					Integer tixCountInt = Integer.parseInt(tixCount);
					purchaseOrder.setTicketCount(Integer.parseInt(tixCount));
				}catch (Exception e) {
					e.printStackTrace();
					error.setDescription("Bad value found in Total Quantity, It should be valid Integer value.");
					poUpdateDTO.setError(error);
					poUpdateDTO.setStatus(0);
					return poUpdateDTO;
				}
				
				if(shippingMethodId>=0){
					purchaseOrder.setShippingMethodId(shippingMethodId);
					ShippingMethod shipMethod = DAORegistry.getShippingMethodDAO().getShippingMethodById(shippingMethodId);
					if(shipMethod!=null){
						shippingMethodName = shipMethod.getName();
					}
				}
				purchaseOrder.setStatus(POStatus.VOID);
				purchaseOrder.setConsignmentPoNo(consignmentPo);
				purchaseOrder.setTrackingNo(trackingNo);
				purchaseOrder.setTransactionOffice(transactionOffice);
				purchaseOrder.setExternalPONo(extPo);
				purchaseOrder.setVoidDate(new Date());
				purchaseOrder.setLastUpdated(new Date());
				DAORegistry.getPurchaseOrderDAO().update(purchaseOrder);
				
				if(paymentDetails==null){
					paymentDetails = new PurchaseOrderPaymentDetails();
				}
				paymentDetails.setName(request.getParameter("name"));
				String paymentDateStr = request.getParameter("paymentDate");
				if(paymentDateStr!=null && !paymentDateStr.isEmpty()){
					paymentDateStr += " 00:00:00";
					paymentDetails.setPaymentDate(formatDateTime.parse(paymentDateStr));
				}
				paymentDetails.setPaymentStatus(request.getParameter("paymentStatus"));
				paymentDetails.setPaymentNote(request.getParameter("paymentNote"));
				paymentDetails.setAmount(Double.parseDouble(poTotal));
				paymentDetails.setPurchaseOrderId(Integer.parseInt(purchaseOrderId));
				paymentDetails.setPaymentType(paymentType);
				if(paymentType !=null && paymentType.equalsIgnoreCase("card")){
					paymentDetails.setCardLastFourDigit(request.getParameter("cardNo"));
					paymentDetails.setCardType(request.getParameter("cardType"));
					paymentDetails.setAccountLastFourDigit(null);
					paymentDetails.setChequeNo(null);
					paymentDetails.setRoutingLastFourDigit(null);
				}else if(paymentType !=null && paymentType.equalsIgnoreCase("account")){
					paymentDetails.setCardLastFourDigit(null);
					paymentDetails.setCardType(null);
					paymentDetails.setAccountLastFourDigit(request.getParameter("accountNo"));
					paymentDetails.setChequeNo(null);
					paymentDetails.setRoutingLastFourDigit(request.getParameter("routingNo"));
				}else if(paymentType !=null && paymentType.equalsIgnoreCase("cheque")){
					paymentDetails.setCardLastFourDigit(null);
					paymentDetails.setCardType(null);
					paymentDetails.setAccountLastFourDigit(request.getParameter("accountNo"));
					paymentDetails.setChequeNo(request.getParameter("checkNo"));
					paymentDetails.setRoutingLastFourDigit(request.getParameter("routingNo"));
				}
				DAORegistry.getPurchaseOrderPaymentDetailsDAO().saveOrUpdate(paymentDetails);
								
				TicketGroup tixGroup = null;
				String section = "";
				String row = "";
				String qty = "";
				String seatLow = "";
				String seatHigh = "";
				String price = "";
				
				@SuppressWarnings("unchecked")
				Map<String, String[]> requestParams = request.getParameterMap();
				if(requestParams != null){
					EventDetails event = null;
					Integer rowNo = 0;
					
					//Map<String, String[]> treeMap = new TreeMap<String, String[]>(requestParams);
					
					for(Map.Entry<String, String[]> entry : requestParams.entrySet()){
						tixGroup = new TicketGroup();
						String key = entry.getKey();
						if(key.contains("rowId_")){
							rowNo = Integer.parseInt(key.replace("rowId_", ""));
						//}
						//if(key.contains("ticketGroup_"+rowNo+"_")){
							
							Integer id = 0;
							id = Integer.parseInt(request.getParameter("ticketGroup_"+rowNo));
							/*if(key.contains("ticketGroup_"+rowNo+"_")){
								id = Integer.parseInt(key.replace("ticketGroup_"+rowNo+"_", ""));
							}*/
														
							TicketGroup ticGroup = null;
							if(id != null && id > 0){
								ticGroup = DAORegistry.getTicketGroupDAO().get(id);
							}
							if(ticGroup != null){
								section = request.getParameter("section_"+rowNo);
								row = request.getParameter("row_"+rowNo);
								qty = request.getParameter("qty_"+rowNo);
								seatLow = request.getParameter("seatLow_"+rowNo);
								seatHigh = request.getParameter("seatHigh_"+rowNo);
								price = request.getParameter("price_"+rowNo);
								if(section == null && row == null && qty == null && seatLow == null && seatHigh == null && price == null){
									DAORegistry.getTicketGroupDAO().delete(ticGroup);
								}else{
								tixGroup.setId(ticGroup.getId());
								tixGroup.setSection(section);
								tixGroup.setRow(row);
								tixGroup.setEventId(ticGroup.getEventId());
								tixGroup.setStatus(OrderStatus.VOIDED.toString());
								tixGroup.setQuantity(Integer.parseInt(qty));
								tixGroup.setSeatLow(seatLow);
								tixGroup.setSeatHigh(seatHigh);
								tixGroup.setPrice(Double.parseDouble(price));
								if(shippingMethodId>=0){
									tixGroup.setShippingMethodId(shippingMethodId);
									if(shippingMethodName != null && !shippingMethodName.isEmpty()){
										tixGroup.setShippingMethod(shippingMethodName);
									}
								}
								tixGroup.setCreatedDate(new Date());
								tixGroup.setInvoiceId(0);
								tixGroup.setCreatedBy(userName);
								tixGroup.setNotes(null);
								tixGroup.setPurchaseOrderId(Integer.parseInt(purchaseOrderId));
								event = DAORegistry.getEventDetailsDAO().get(ticGroup.getEventId());
								tixGroup.setEventName(event.getEventName());
								tixGroup.setEventDate(event.getEventDate());
								tixGroup.setEventTime(event.getEventTime());
								
								DAORegistry.getTicketGroupDAO().update(tixGroup);
								}
							}else{
								tixGroup.setSection(request.getParameter("section_"+rowNo));
								tixGroup.setRow(request.getParameter("row_"+rowNo));
								tixGroup.setEventId(id);
								tixGroup.setStatus(OrderStatus.VOIDED.toString());
								tixGroup.setQuantity(Integer.parseInt(request.getParameter("qty_"+rowNo)));
								tixGroup.setSeatLow(request.getParameter("seatLow_"+rowNo));
								tixGroup.setSeatHigh(request.getParameter("seatHigh_"+rowNo));
								tixGroup.setPrice(Double.parseDouble(request.getParameter("price_"+rowNo)));
								if(shippingMethodId>=0){
									tixGroup.setShippingMethodId(shippingMethodId);
									if(shippingMethodName != null && !shippingMethodName.isEmpty()){
										tixGroup.setShippingMethod(shippingMethodName);
									}
								}
								tixGroup.setCreatedDate(new Date());
								tixGroup.setInvoiceId(0);
								tixGroup.setCreatedBy(userName);
								tixGroup.setNotes(null);
								tixGroup.setPurchaseOrderId(Integer.parseInt(purchaseOrderId));
								event = DAORegistry.getEventDetailsDAO().get(id);
								tixGroup.setEventName(event.getEventName());
								tixGroup.setEventDate(event.getEventDate());
								tixGroup.setEventTime(event.getEventTime());
								
								DAORegistry.getTicketGroupDAO().saveOrUpdate(tixGroup);
							}
						}
					}
				}
				poUpdateDTO.setMessage("Purchase Order voided successfully");
				poUpdateDTO.setStatus(1);
				
				PurchaseOrder poInfoUpdated = DAORegistry.getPurchaseOrderDAO().get(Integer.parseInt(purchaseOrderId));
				PurchaseOrderPaymentDetails paymentDetailsUpdated = DAORegistry.getPurchaseOrderPaymentDetailsDAO().getPaymentDetailsByPOId(Integer.parseInt(purchaseOrderId));
				Collection<TicketGroup> ticketGroupsUpdated = DAORegistry.getTicketGroupDAO().getAllTicketGroupsByPOId(Integer.parseInt(purchaseOrderId));				
				
				poUpdateDTO.setTicketGroupDTO(com.rtw.tracker.utils.Util.getPOUploadTicketGroupArray(ticketGroupsUpdated));
				poUpdateDTO.setPaymentDetailsDTO(com.rtw.tracker.utils.Util.getPOPaymentHistoryObject(paymentDetailsUpdated));
				poUpdateDTO.setPurchaseOrderDTO(com.rtw.tracker.utils.Util.getPurchaseOrderObject(poInfoUpdated));
				poUpdateDTO.setCustomerAddressDTO(com.rtw.tracker.utils.Util.getCustomerAddressObject(customerAddress));
				poUpdateDTO.setShippingMethodName(shippingMethodName);
				
				//Tracking User Action
				String userActionMsg = "Purchase Order Voided Successfully.";
				Util.userActionAudit(request, Integer.parseInt(purchaseOrderId), userActionMsg);
			}
			
			PurchaseOrder poInfoUpdated = DAORegistry.getPurchaseOrderDAO().get(Integer.parseInt(purchaseOrderId));
			PurchaseOrderPaymentDetails paymentDetailsUpdated = DAORegistry.getPurchaseOrderPaymentDetailsDAO().getPaymentDetailsByPOId(Integer.parseInt(purchaseOrderId));
			Collection<TicketGroup> ticketGroupsUpdated = DAORegistry.getTicketGroupDAO().getAllTicketGroupsByPOId(Integer.parseInt(purchaseOrderId));
			
			poUpdateDTO.setStatus(1);
			poUpdateDTO.setTicketGroupDTO(com.rtw.tracker.utils.Util.getPOUploadTicketGroupArray(ticketGroupsUpdated));
			poUpdateDTO.setPaymentDetailsDTO(com.rtw.tracker.utils.Util.getPOPaymentHistoryObject(paymentDetailsUpdated));
			poUpdateDTO.setPurchaseOrderDTO(com.rtw.tracker.utils.Util.getPurchaseOrderObject(poInfoUpdated));
			poUpdateDTO.setCustomerInfoDTO(com.rtw.tracker.utils.Util.getCustomerObject(customerPOInfo));
			poUpdateDTO.setCustomerAddressDTO(com.rtw.tracker.utils.Util.getCustomerAddressObject(customerAddress));
			poUpdateDTO.setShippingMethodDTO(com.rtw.tracker.utils.Util.getShippingMethodArray(shippingMethods));
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Edit/Update PO.");
			poUpdateDTO.setError(error);
			poUpdateDTO.setStatus(0);
		}
		return poUpdateDTO;
	}


	@RequestMapping(value="/Invoices")
	public InvoiceDetailsDTO getInvoicesPage(HttpServletRequest request, HttpServletResponse response){
		InvoiceDetailsDTO invoiceDetailsDTO = new InvoiceDetailsDTO();
		Error error =new Error();
		
		try {
			String status = request.getParameter("status");
			String sortingString = request.getParameter("sortingString");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String invoiceNoStr = request.getParameter("invoiceNo");
			String orderNoStr = request.getParameter("orderNo");			
			String companyProduct = request.getParameter("productType");
			String product = request.getParameter("productTypeSession");
			String csr = request.getParameter("csr");
			String customerStr = request.getParameter("customer");
			String externalOrderId = request.getParameter("externalOrderId");
			
			String action = request.getParameter("action");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String brokerIdStr = request.getParameter("brokerId");
						
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				invoiceDetailsDTO.setError(error);
				invoiceDetailsDTO.setStatus(0);
				return invoiceDetailsDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				invoiceDetailsDTO.setError(error);
				invoiceDetailsDTO.setStatus(0);
				return invoiceDetailsDTO;
			}
			
			GridHeaderFilters filter = null;
			try{
				filter = GridHeaderFiltersUtil.getInvoiceSearchHeaderFilters(headerFilter+sortingString);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				invoiceDetailsDTO.setError(error);
				invoiceDetailsDTO.setStatus(0);
				return invoiceDetailsDTO;
			}
			
			Collection<OpenOrders> openOrdersList=null;
			String fromDateFinal = "";
			String toDateFinal = "";
			double invoiceTotal = 0;
			double roundInvoiceTotal = 0;
			Integer count = 0;
						
			if(action!=null && action.equalsIgnoreCase("search")){
				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";					
				}else{
					fromDateFinal = null;
					toDateFinal = null;
				}
			}else{
				if((fromDateStr != null && !fromDateStr.isEmpty()) && (toDateStr != null && !toDateStr.isEmpty())){
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
				}
				else{
					DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
					Calendar cal = Calendar.getInstance();
					Date toDate = new Date();
					cal.add(Calendar.MONTH, -3);
					Date fromDate = cal.getTime();
					fromDateStr = dateFormat.format(fromDate);
					toDateStr = dateFormat.format(toDate);
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
				}
			}
			
			if(product!=null && !product.isEmpty()){
				companyProduct = product;
			}
			
			if(companyProduct!=null && (companyProduct.equalsIgnoreCase("RTW") || companyProduct.equalsIgnoreCase("RTW2"))){
				openOrdersList = DAORegistry.getQueryManagerDAO().getRTWOpenOrderList(fromDateFinal, toDateFinal, filter, orderNoStr, invoiceNoStr, status, companyProduct,null,pageNo);
				count = DAORegistry.getQueryManagerDAO().getRTWOpenOrderListCount(fromDateFinal, toDateFinal, filter, orderNoStr, invoiceNoStr, status,companyProduct, null);
			}else{
				openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrderList(fromDateFinal, toDateFinal, filter, orderNoStr, invoiceNoStr, status, companyProduct, pageNo, brokerId,null);
				count = DAORegistry.getQueryManagerDAO().getOpenOrderListCount(fromDateFinal, toDateFinal, filter, orderNoStr, invoiceNoStr, status, companyProduct, brokerId,null);
			}
						
			Map<Integer,Double> invoiceCountMap = new HashMap<Integer, Double>();
			if(openOrdersList!=null && !openOrdersList.isEmpty()){
				for(OpenOrders order : openOrdersList){
					invoiceCountMap.put(order.getInvoiceId(), order.getInvoiceTotal());
				}
				for(Double invoiceTot : invoiceCountMap.values()){
					invoiceTotal += invoiceTot;//order.getInvoiceTotal();
				}
				if(openOrdersList.size()==1){
					status = openOrdersList.iterator().next().getStatus();
				}
			}
			roundInvoiceTotal = Math.round(invoiceTotal*100)/100.00;
			
			invoiceDetailsDTO.setStatus(1);
			invoiceDetailsDTO.setInvoicePaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
			invoiceDetailsDTO.setOpenOrdersDTO(com.rtw.tracker.utils.Util.getInvoiceArray(openOrdersList));
			invoiceDetailsDTO.setInvoiceTotal(String.valueOf(roundInvoiceTotal));
			invoiceDetailsDTO.setInvoiceStatus((status==null || status.isEmpty())?"":InvoiceStatus.valueOf(status).toString());
			invoiceDetailsDTO.setSelectedProductType(companyProduct);
			invoiceDetailsDTO.setLayoutProductType(companyProduct);
			invoiceDetailsDTO.setFromDate(fromDateStr);
			invoiceDetailsDTO.setToDate(toDateStr);
			invoiceDetailsDTO.setCurrentYear(String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Invoices.");
			invoiceDetailsDTO.setError(error);
			invoiceDetailsDTO.setStatus(0);
		}
		return invoiceDetailsDTO;
	}
	
	/*@RequestMapping(value="/GetInvoices")
	public InvoiceDetailsDTO getInvoices(HttpServletRequest request, HttpServletResponse response){
		InvoiceDetailsDTO invoiceDetailsDTO = new InvoiceDetailsDTO();
		Error error =new Error();
		
		try {
			String status = request.getParameter("status");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String csr = request.getParameter("csr");
			String customerStr = request.getParameter("customer");
			String companyProduct = request.getParameter("productType");
			String invoiceNoStr = request.getParameter("invoiceNo");
			String orderNo = request.getParameter("orderNo");
			String externalOrderId = request.getParameter("externalOrderId");
			
			String action = request.getParameter("action");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");			
			String brokerIdStr = request.getParameter("brokerId");
			
			if(StringUtils.isEmpty(pageNo)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				invoiceDetailsDTO.setError(error);
				invoiceDetailsDTO.setStatus(0);
				return invoiceDetailsDTO;
			}
			try {
				Integer.parseInt(pageNo);
			} catch (Exception e) {
				System.err.println("Please provide valid PageNo.");
				error.setDescription("Please provide valid PageNo.");
				invoiceDetailsDTO.setError(error);
				invoiceDetailsDTO.setStatus(0);
				return invoiceDetailsDTO;
			}
			
			GridHeaderFilters filter = null;
			try{
				filter = GridHeaderFiltersUtil.getInvoiceSearchHeaderFilters(headerFilter);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				invoiceDetailsDTO.setError(error);
				invoiceDetailsDTO.setStatus(0);
				return invoiceDetailsDTO;
			}
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				invoiceDetailsDTO.setError(error);
				invoiceDetailsDTO.setStatus(0);
				return invoiceDetailsDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				invoiceDetailsDTO.setError(error);
				invoiceDetailsDTO.setStatus(0);
				return invoiceDetailsDTO;
			}
			
			Collection<OpenOrders> openOrdersList=null;
			String fromDateFinal = "";
			String toDateFinal = "";
			double invoiceTotal = 0;
			double roundInvoiceTotal = 0;
			Integer count = 0;
			
			if(action!=null && action.equalsIgnoreCase("search")){
				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
					if(companyProduct!=null && (companyProduct.equalsIgnoreCase("RTW") || companyProduct.equalsIgnoreCase("RTW2"))){
						openOrdersList = DAORegistry.getQueryManagerDAO().getRTWOpenOrderList(fromDateFinal, toDateFinal, filter, null, null, status, companyProduct,null,pageNo);
						count = DAORegistry.getQueryManagerDAO().getRTWOpenOrderListCount(fromDateFinal, toDateFinal, filter, null, null, status, companyProduct, null);
					}else{
						openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrderList(fromDateFinal,toDateFinal,filter,null, null,status,companyProduct,pageNo, brokerId,null);
						count = DAORegistry.getQueryManagerDAO().getOpenOrderListCount(fromDateFinal,toDateFinal,filter,null, null,status,companyProduct, brokerId,null);
					}
				}else{
					if(companyProduct!=null && (companyProduct.equalsIgnoreCase("RTW") || companyProduct.equalsIgnoreCase("RTW2"))){
						openOrdersList = DAORegistry.getQueryManagerDAO().getRTWOpenOrderList(null, null, filter, null, null, status,companyProduct,null,pageNo);
						count = DAORegistry.getQueryManagerDAO().getRTWOpenOrderListCount(null, null, filter, null, null, status, companyProduct, null);
					}else{
						openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrderList(null,null,filter, null, null,status,companyProduct,pageNo,brokerId,null);
						count = DAORegistry.getQueryManagerDAO().getOpenOrderListCount(null,null,filter, null, null,status,companyProduct,brokerId,null);
					}
				}
			}
			else if((fromDateStr != null && !fromDateStr.isEmpty()) && (toDateStr != null && !toDateStr.isEmpty())){
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";
				openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrderList(fromDateFinal,toDateFinal,filter,null,null,status,companyProduct,pageNo,brokerId,null);
				count = DAORegistry.getQueryManagerDAO().getOpenOrderListCount(fromDateFinal,toDateFinal,filter,null,null,status,companyProduct,brokerId,null);
			}
			Map<Integer,Double> invoiceCountMap = new HashMap<Integer, Double>();
			if(openOrdersList!=null && !openOrdersList.isEmpty()){
				for(OpenOrders order : openOrdersList){
					invoiceCountMap.put(order.getInvoiceId(), order.getInvoiceTotal());
				}
			}			
			
			if(openOrdersList!=null && !openOrdersList.isEmpty()){
				for(Double invoiceTot : invoiceCountMap.values()){
					invoiceTotal += invoiceTot;//order.getInvoiceTotal();
				}
			}		
			roundInvoiceTotal = Math.round(invoiceTotal*100)/100.00;
			
			invoiceDetailsDTO.setStatus(1);
			invoiceDetailsDTO.setInvoicePaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
			invoiceDetailsDTO.setOpenOrdersDTO(com.rtw.tracker.utils.Util.getInvoiceArray(openOrdersList));
			invoiceDetailsDTO.setInvoiceTotal(String.valueOf(roundInvoiceTotal));
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Applying Search - Invoices.");
			invoiceDetailsDTO.setError(error);
			invoiceDetailsDTO.setStatus(0);
		}
		return invoiceDetailsDTO;
	}*/
	
	@RequestMapping(value="/InvoiceExportToExcel")
	public void getInvoicesToExport(HttpServletRequest request, HttpServletResponse response){
		
		try {
			String status = request.getParameter("status");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String csr = request.getParameter("csr");
			String customerStr = request.getParameter("customer");
			String companyProduct = request.getParameter("productType");			
			String invoiceNoStr = request.getParameter("invoiceNo");			
			String orderNo = request.getParameter("orderNo");
			String externalOrderId = request.getParameter("externalOrderId");
			String headerFilter = request.getParameter("headerFilter");
			String brokerIdStr = request.getParameter("brokerId");
			Integer brokerId = Integer.parseInt(brokerIdStr);
			
			GridHeaderFilters filter = GridHeaderFiltersUtil.getInvoiceSearchHeaderFilters(headerFilter);
			
			Collection<OpenOrders> openOrdersList=null;
			String fromDateFinal = "";
			String toDateFinal = "";
			
			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";
				if(companyProduct.equalsIgnoreCase("RTW") || companyProduct.equalsIgnoreCase("RTW2")){
					openOrdersList = DAORegistry.getQueryManagerDAO().getRTWOpenOrderListToExport(invoiceNoStr, orderNo, csr, customerStr, fromDateFinal, toDateFinal, status, companyProduct, externalOrderId,null,filter);
				}else{
					openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrderListToExport(invoiceNoStr,orderNo,csr,customerStr,fromDateFinal,toDateFinal,status,companyProduct,externalOrderId,filter,brokerId,null);
				}
			}else{
				if(companyProduct.equalsIgnoreCase("RTW") || companyProduct.equalsIgnoreCase("RTW2")){
					openOrdersList = DAORegistry.getQueryManagerDAO().getRTWOpenOrderListToExport(invoiceNoStr, orderNo, csr, customerStr, null, null, status, companyProduct, externalOrderId,null,filter);
				}else{
					openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrderListToExport(invoiceNoStr,orderNo,csr,customerStr,null,null,status,companyProduct,externalOrderId,filter,brokerId,null);
				}
			}	

			if(openOrdersList!=null && !openOrdersList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("invoice");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Invoice Id");
				header.createCell(1).setCellValue("Product Type");
				header.createCell(2).setCellValue("Customer Id");
				header.createCell(3).setCellValue("Customer Name");
				header.createCell(4).setCellValue("Username");
				header.createCell(5).setCellValue("Customer Type");
				header.createCell(6).setCellValue("Customer Company Name");
				header.createCell(7).setCellValue("Ticket Count");
				header.createCell(8).setCellValue("Invoice Total");
				header.createCell(9).setCellValue("Address");
				header.createCell(10).setCellValue("Country");
				header.createCell(11).setCellValue("State");
				header.createCell(12).setCellValue("City");
				header.createCell(13).setCellValue("Zipcode");
				header.createCell(14).setCellValue("Phone");
				header.createCell(15).setCellValue("Invoice Aged");
				header.createCell(16).setCellValue("Created Date");
				header.createCell(17).setCellValue("Created By");
				header.createCell(18).setCellValue("CSR");
				header.createCell(19).setCellValue("Secondary Order Type");
				header.createCell(20).setCellValue("Secondary Order Id");
				header.createCell(21).setCellValue("Voided Date");
				header.createCell(22).setCellValue("Is Invoice Email Sent");
				header.createCell(23).setCellValue("Purchase Order No");
				header.createCell(24).setCellValue("Shipping Method");
				header.createCell(25).setCellValue("Tracking No");
				header.createCell(26).setCellValue("Customer Order Id");
				header.createCell(27).setCellValue("Last Updated Date");
				header.createCell(28).setCellValue("Last Updated By");
				header.createCell(29).setCellValue("Status");
				header.createCell(30).setCellValue("Event Id");
				header.createCell(31).setCellValue("Event Name");
				header.createCell(32).setCellValue("Event Date");
				header.createCell(33).setCellValue("Event Time");
				header.createCell(34).setCellValue("Broker Id");
				header.createCell(35).setCellValue("Loyal Fan Order");
				header.createCell(36).setCellValue("Primary Payment Method");
				header.createCell(37).setCellValue("Secondary Payment Method");
				header.createCell(38).setCellValue("Third Payment Method");
				header.createCell(39).setCellValue("PO Mapped");
				int i=1;
				for(OpenOrders order : openOrdersList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(order.getInvoiceId());
					row.createCell(1).setCellValue(order.getProductType());
					row.createCell(2).setCellValue(order.getCustomerId()!=null?order.getCustomerId():0);
					row.createCell(3).setCellValue(order.getCustomerName()!=null?order.getCustomerType():"");
					row.createCell(4).setCellValue(order.getUsername());
					row.createCell(5).setCellValue(order.getCustomerType()!=null?order.getCustomerType():"");
					row.createCell(6).setCellValue(order.getCustCompanyName()!=null?order.getCustCompanyName():"");
					row.createCell(7).setCellValue(order.getTicketCount()!=null?order.getTicketCount():0);
					row.createCell(8).setCellValue(order.getInvoiceTotal());
					row.createCell(9).setCellValue(order.getAddressLine1()!=null?order.getAddressLine1():"");
					row.createCell(10).setCellValue(order.getCountry()!=null?order.getCountry():"");
					row.createCell(11).setCellValue(order.getState()!=null?order.getState():"");
					row.createCell(12).setCellValue(order.getCity()!=null?order.getCity():"");
					row.createCell(13).setCellValue(order.getZipCode()!=null?order.getZipCode():"");
					row.createCell(14).setCellValue(order.getPhone()!=null?order.getPhone():"");
					row.createCell(15).setCellValue(order.getInvoiceAged()!=null?order.getInvoiceAged():0);
					row.createCell(16).setCellValue(order.getCreatedDateStr()!=null?order.getCreatedDateStr():"");
					row.createCell(17).setCellValue(order.getCreatedBy()!=null?order.getCreatedBy():"");
					row.createCell(18).setCellValue(order.getCsr()!=null?order.getCsr():"");
					row.createCell(19).setCellValue(order.getSecondaryOrderType()!=null?order.getSecondaryOrderType():"");
					row.createCell(20).setCellValue(order.getSecondaryOrderId()!=null?order.getSecondaryOrderId():"");
					row.createCell(21).setCellValue(order.getVoidedDateStr()!=null?order.getVoidedDateStr():"");
					row.createCell(22).setCellValue(order.getIsInvoiceEmailSent());
					row.createCell(23).setCellValue(order.getPurchaseOrderNo()!=null?order.getPurchaseOrderNo():"");
					row.createCell(24).setCellValue(order.getShippingMethod()!=null?order.getShippingMethod():"");
					row.createCell(25).setCellValue(order.getTrackingNo()!=null?order.getTrackingNo():"");
					row.createCell(26).setCellValue(order.getCustomerOrderId()!=null?order.getCustomerOrderId():0);
					row.createCell(27).setCellValue(order.getLastUpdatedDateStr()!=null?order.getLastUpdatedDateStr():"");
					row.createCell(28).setCellValue(order.getLastUpdatedBy()!=null?order.getLastUpdatedBy():"");
					row.createCell(29).setCellValue(order.getStatus()!=null?order.getStatus():"");
					row.createCell(30).setCellValue(order.getEventId()!=null?order.getEventId():0);
					row.createCell(31).setCellValue(order.getEventName()!=null?order.getEventName():"");
					row.createCell(32).setCellValue(order.getEventDateStr()!=null?order.getEventDateStr():"");
					row.createCell(33).setCellValue(order.getEventTimeStr()!=null?order.getEventTimeStr():"");
					row.createCell(34).setCellValue(order.getBrokerId()!=null?order.getBrokerId().toString():"");
					if(order.getOrderType()!= null && order.getOrderType().equalsIgnoreCase("LOYALFAN")){
						row.createCell(35).setCellValue("Yes");
					}else{
						row.createCell(35).setCellValue("No");
					}
					row.createCell(36).setCellValue(order.getPrimaryPaymentMethod()!=null?order.getPrimaryPaymentMethod():"");
					row.createCell(37).setCellValue(order.getSecondaryPaymentMethod()!=null?order.getSecondaryPaymentMethod():"");
					row.createCell(38).setCellValue(order.getThirdPaymentMethod()!=null?order.getThirdPaymentMethod():"");
					row.createCell(39).setCellValue(order.getIsPoMapped()!=null?order.getIsPoMapped():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=invoice.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	@RequestMapping(value="/AccountReceivables")
	public AccountReceivableDetailsDTO getAccountReceivablePage(HttpServletRequest request, HttpServletResponse response){
		AccountReceivableDetailsDTO accountReceivableDetailsDTO = new AccountReceivableDetailsDTO();
		Error error = new Error();
		
		try {
			String status = request.getParameter("status");
			String sortingString = request.getParameter("sortingString");			
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String invoiceNoStr = request.getParameter("invoiceNo");
			String orderNoStr = request.getParameter("orderNo");			
			String companyProduct = request.getParameter("productType");
			String brokerIdStr = request.getParameter("brokerId");
			
			String action = request.getParameter("action");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				accountReceivableDetailsDTO.setError(error);
				accountReceivableDetailsDTO.setStatus(0);
				return accountReceivableDetailsDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				accountReceivableDetailsDTO.setError(error);
				accountReceivableDetailsDTO.setStatus(0);
				return accountReceivableDetailsDTO;
			}
			
			GridHeaderFilters filter = null;
			try{
				filter = GridHeaderFiltersUtil.getInvoiceSearchHeaderFilters(headerFilter+sortingString);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				accountReceivableDetailsDTO.setError(error);
				accountReceivableDetailsDTO.setStatus(0);
				return accountReceivableDetailsDTO;
			}
			
			Collection<OpenOrders> openOrdersList=null;
			String fromDateFinal = "";
			String toDateFinal = "";
			double invoiceTotal = 0;
			double roundInvoiceTotal = 0;
			Integer count = 0;
			
//			if(invoiceNoStr==null || invoiceNoStr.isEmpty()){
//				if((fromDateStr != null && !fromDateStr.isEmpty()) && (toDateStr != null && !toDateStr.isEmpty())){
//					fromDateFinal = fromDateStr + " 00:00:00";
//					toDateFinal = toDateStr + " 23:59:59";
//				}
//				else{
//					DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
//					Calendar cal = Calendar.getInstance();
//					Date toDate = new Date();
//					cal.add(Calendar.MONTH, -3);
//					Date fromDate = cal.getTime();
//					fromDateStr = dateFormat.format(fromDate);
//					toDateStr = dateFormat.format(toDate);
//					fromDateFinal = fromDateStr + " 00:00:00";
//					toDateFinal = toDateStr + " 23:59:59";
//				}
//			}
			
			if(action!=null && action.equalsIgnoreCase("search")){
				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
				}else{
					fromDateFinal = null;
					toDateFinal = null;
				}
			}
			else if((fromDateStr != null && !fromDateStr.isEmpty()) && (toDateStr != null && !toDateStr.isEmpty())){
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";
			}
			
			openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrderList(fromDateFinal, toDateFinal, filter, orderNoStr, invoiceNoStr, status, companyProduct, pageNo, brokerId,PaymentMethod.ACCOUNT_RECIVABLE.toString());
			count = DAORegistry.getQueryManagerDAO().getOpenOrderListCount(fromDateFinal, toDateFinal, filter, orderNoStr, invoiceNoStr, status, companyProduct, brokerId,PaymentMethod.ACCOUNT_RECIVABLE.toString());
			
			Map<Integer,Double> invoiceCountMap = new HashMap<Integer, Double>();
			if(openOrdersList!=null && !openOrdersList.isEmpty()){
				for(OpenOrders order : openOrdersList){
					invoiceCountMap.put(order.getInvoiceId(), order.getInvoiceTotal());
				}
				for(Double invoiceTot : invoiceCountMap.values()){
					invoiceTotal += invoiceTot;//order.getInvoiceTotal();
				}
			}
			roundInvoiceTotal = Math.round(invoiceTotal*100)/100.00;
			
			accountReceivableDetailsDTO.setStatus(1);
			accountReceivableDetailsDTO.setAccReceivablePaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
			accountReceivableDetailsDTO.setOpenOrdersDTO(com.rtw.tracker.utils.Util.getInvoiceArray(openOrdersList));
			accountReceivableDetailsDTO.setInvoiceTotal(String.valueOf(roundInvoiceTotal));
			accountReceivableDetailsDTO.setInvoiceStatus((status==null || status.isEmpty())?"":InvoiceStatus.valueOf(status).toString());
//			accountReceivableDetailsDTO.setSelectedProductType(companyProduct);
//			accountReceivableDetailsDTO.setLayoutProductType(companyProduct);
//			accountReceivableDetailsDTO.setFromDate(fromDateStr);
//			accountReceivableDetailsDTO.setToDate(toDateStr);
			accountReceivableDetailsDTO.setCurrentYear(String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Account Receivables.");
			accountReceivableDetailsDTO.setError(error);
			accountReceivableDetailsDTO.setStatus(0);
		}
		return accountReceivableDetailsDTO;
	}
	
	
	@RequestMapping(value="/getContestInvoices")
	public AccountReceivableDetailsDTO getContestInvoicePage(HttpServletRequest request, HttpServletResponse response){
		AccountReceivableDetailsDTO accountReceivableDetailsDTO = new AccountReceivableDetailsDTO();
		Error error = new Error();
		
		try {
			String status = request.getParameter("status");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String invoiceNoStr = request.getParameter("invoiceNo");
			String orderNoStr = request.getParameter("orderNo");			
			String companyProduct = request.getParameter("productType");
			String brokerIdStr = request.getParameter("brokerId");
			String sortingString = request.getParameter("sortingString");
			
			String action = request.getParameter("action");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				accountReceivableDetailsDTO.setError(error);
				accountReceivableDetailsDTO.setStatus(0);
				return accountReceivableDetailsDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				accountReceivableDetailsDTO.setError(error);
				accountReceivableDetailsDTO.setStatus(0);
				return accountReceivableDetailsDTO;
			}
			
			GridHeaderFilters filter = null;
			try{
				filter = GridHeaderFiltersUtil.getInvoiceSearchHeaderFilters(headerFilter+sortingString);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				accountReceivableDetailsDTO.setError(error);
				accountReceivableDetailsDTO.setStatus(0);
				return accountReceivableDetailsDTO;
			}
			
			Collection<OpenOrders> openOrdersList=null;
			String fromDateFinal = "";
			String toDateFinal = "";
			double invoiceTotal = 0;
			double roundInvoiceTotal = 0;
			Integer count = 0;
			
			if(action!=null && action.equalsIgnoreCase("search")){
				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
				}else{
					fromDateFinal = null;
					toDateFinal = null;
				}
			}
			else if((fromDateStr != null && !fromDateStr.isEmpty()) && (toDateStr != null && !toDateStr.isEmpty())){
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";
			}
			
			openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrderList(fromDateFinal, toDateFinal, filter, orderNoStr, invoiceNoStr, status, companyProduct, pageNo, brokerId,"CONTEST");
			count = DAORegistry.getQueryManagerDAO().getOpenOrderListCount(fromDateFinal, toDateFinal, filter, orderNoStr, invoiceNoStr, status, companyProduct, brokerId,"CONTEST");
			
			Map<Integer,Double> invoiceCountMap = new HashMap<Integer, Double>();
			if(openOrdersList!=null && !openOrdersList.isEmpty()){
				for(OpenOrders order : openOrdersList){
					invoiceCountMap.put(order.getInvoiceId(), order.getInvoiceTotal());
				}
				for(Double invoiceTot : invoiceCountMap.values()){
					invoiceTotal += invoiceTot;//order.getInvoiceTotal();
				}
			}
			roundInvoiceTotal = Math.round(invoiceTotal*100)/100.00;
			
			accountReceivableDetailsDTO.setStatus(1);
			accountReceivableDetailsDTO.setAccReceivablePaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
			accountReceivableDetailsDTO.setOpenOrdersDTO(com.rtw.tracker.utils.Util.getInvoiceArray(openOrdersList));
			accountReceivableDetailsDTO.setInvoiceTotal(String.valueOf(roundInvoiceTotal));
			accountReceivableDetailsDTO.setInvoiceStatus((status==null || status.isEmpty())?"":InvoiceStatus.valueOf(status).toString());
			accountReceivableDetailsDTO.setCurrentYear(String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Account Receivables.");
			accountReceivableDetailsDTO.setError(error);
			accountReceivableDetailsDTO.setStatus(0);
		}
		return accountReceivableDetailsDTO;
	}
	
	/*@RequestMapping(value="/GetAccountReceivables")
	public AccountReceivableDetailsDTO GetAccountReceivables(HttpServletRequest request, HttpServletResponse response){
		AccountReceivableDetailsDTO accountReceivableDetailsDTO = new AccountReceivableDetailsDTO();
		Error error = new Error();
		
		try {
			String status = request.getParameter("status");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String companyProduct = request.getParameter("productType");
			String action = request.getParameter("action");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String brokerIdStr = request.getParameter("brokerId");
			
			if(StringUtils.isEmpty(pageNo)){
				System.err.println("Please provide PageNo.");
				error.setDescription("Please provide PageNo.");
				accountReceivableDetailsDTO.setError(error);
				accountReceivableDetailsDTO.setStatus(0);
				return accountReceivableDetailsDTO;
			}
			try {
				Integer.parseInt(pageNo);
			} catch (Exception e) {
				System.err.println("Please provide valid PageNo.");
				error.setDescription("Please provide valid PageNo.");
				accountReceivableDetailsDTO.setError(error);
				accountReceivableDetailsDTO.setStatus(0);
				return accountReceivableDetailsDTO;
			}
			
			GridHeaderFilters filter = null;
			try{
				filter = GridHeaderFiltersUtil.getInvoiceSearchHeaderFilters(headerFilter);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Please provide valid grid header filter values.");
				accountReceivableDetailsDTO.setError(error);
				accountReceivableDetailsDTO.setStatus(0);
				return accountReceivableDetailsDTO;
			}
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				accountReceivableDetailsDTO.setError(error);
				accountReceivableDetailsDTO.setStatus(0);
				return accountReceivableDetailsDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				accountReceivableDetailsDTO.setError(error);
				accountReceivableDetailsDTO.setStatus(0);
				return accountReceivableDetailsDTO;
			}
			
			Collection<OpenOrders> openOrdersList=null;
			String fromDateFinal = "";
			String toDateFinal = "";
			double invoiceTotal = 0;
			double roundInvoiceTotal = 0;
			Integer count = 0;
			
			if(action!=null && action.equalsIgnoreCase("search")){
				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
					openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrderList(fromDateFinal,toDateFinal,filter,null, null,status,companyProduct,pageNo, brokerId,PaymentMethod.ACCOUNT_RECIVABLE.toString());
					count = DAORegistry.getQueryManagerDAO().getOpenOrderListCount(fromDateFinal,toDateFinal,filter,null, null,status,companyProduct, brokerId,PaymentMethod.ACCOUNT_RECIVABLE.toString());
				}else{
					openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrderList(null,null,filter, null, null,status,companyProduct,pageNo,brokerId,PaymentMethod.ACCOUNT_RECIVABLE.toString());
					count = DAORegistry.getQueryManagerDAO().getOpenOrderListCount(null,null,filter, null, null,status,companyProduct,brokerId,PaymentMethod.ACCOUNT_RECIVABLE.toString());
				}
			}
			else if((fromDateStr != null && !fromDateStr.isEmpty()) && (toDateStr != null && !toDateStr.isEmpty())){
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";
				openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrderList(fromDateFinal,toDateFinal,filter,null,null,status,companyProduct,pageNo,brokerId,PaymentMethod.ACCOUNT_RECIVABLE.toString());
				count = DAORegistry.getQueryManagerDAO().getOpenOrderListCount(fromDateFinal,toDateFinal,filter,null,null,status,companyProduct,brokerId,PaymentMethod.ACCOUNT_RECIVABLE.toString());
			}
			Map<Integer,Double> invoiceCountMap = new HashMap<Integer, Double>();
			if(openOrdersList!=null && !openOrdersList.isEmpty()){
				for(OpenOrders order : openOrdersList){
					invoiceCountMap.put(order.getInvoiceId(), order.getInvoiceTotal());
				}
			}			
			
			if(openOrdersList!=null && !openOrdersList.isEmpty()){
				for(Double invoiceTot : invoiceCountMap.values()){
					invoiceTotal += invoiceTot;//order.getInvoiceTotal();
				}
			}		
			roundInvoiceTotal = Math.round(invoiceTotal*100)/100.00;		
			
			accountReceivableDetailsDTO.setStatus(1);
			accountReceivableDetailsDTO.setAccReceivablePaginationDTO(PaginationUtil.calculatePaginationParameter(count, pageNo));
			accountReceivableDetailsDTO.setOpenOrdersDTO(com.rtw.tracker.utils.Util.getInvoiceArray(openOrdersList));
			accountReceivableDetailsDTO.setInvoiceTotal(String.valueOf(roundInvoiceTotal));
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Applying Search - Account Receivables.");
			accountReceivableDetailsDTO.setError(error);
			accountReceivableDetailsDTO.setStatus(0);
		}
		return accountReceivableDetailsDTO;
	}*/
			
	@RequestMapping(value="/AccountReceivableExportToExcel")
	public void getAccountReceivableToExport(HttpServletRequest request, HttpServletResponse response){
		
		try {
			String status = request.getParameter("status");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String csr = request.getParameter("csr");
			String customerStr = request.getParameter("customer");
			String companyProduct = request.getParameter("productType");
			String invoiceNoStr = request.getParameter("invoiceNo");
			String orderNo = request.getParameter("orderNo");
			String externalOrderId = request.getParameter("externalOrderId");
			String headerFilter = request.getParameter("headerFilter");
			String brokerIdStr = request.getParameter("brokerId");
			Integer brokerId = Integer.parseInt(brokerIdStr);
			
			GridHeaderFilters filter = GridHeaderFiltersUtil.getInvoiceSearchHeaderFilters(headerFilter);
			
			Collection<OpenOrders> openOrdersList=null;
			String fromDateFinal = "";
			String toDateFinal = "";
						
			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";
				openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrderListToExport(invoiceNoStr,orderNo,csr,customerStr,fromDateFinal,toDateFinal,status,companyProduct,externalOrderId,filter,brokerId,PaymentMethod.ACCOUNT_RECIVABLE.toString());
			}else{
				openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrderListToExport(invoiceNoStr,orderNo,csr,customerStr,null,null,status,companyProduct,externalOrderId,filter,brokerId,PaymentMethod.ACCOUNT_RECIVABLE.toString());
			}	

			if(openOrdersList!=null && !openOrdersList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("invoice");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Invoice Id");
				header.createCell(1).setCellValue("Product Type");
				header.createCell(2).setCellValue("Customer Id");
				header.createCell(3).setCellValue("Customer Name");
				header.createCell(4).setCellValue("Username");
				header.createCell(5).setCellValue("Customer Type");
				header.createCell(6).setCellValue("Customer Company Name");
				header.createCell(7).setCellValue("Ticket Count");
				header.createCell(8).setCellValue("Invoice Total");
				header.createCell(9).setCellValue("Address");
				header.createCell(10).setCellValue("Country");
				header.createCell(11).setCellValue("State");
				header.createCell(12).setCellValue("City");
				header.createCell(13).setCellValue("Zipcode");
				header.createCell(14).setCellValue("Phone");
				header.createCell(15).setCellValue("Invoice Aged");
				header.createCell(16).setCellValue("Created Date");
				header.createCell(17).setCellValue("Created By");
				header.createCell(18).setCellValue("CSR");
				header.createCell(19).setCellValue("Secondary Order Type");
				header.createCell(20).setCellValue("Secondary Order Id");
				header.createCell(21).setCellValue("Voided Date");
				header.createCell(22).setCellValue("Is Invoice Email Sent");
				header.createCell(23).setCellValue("Purchase Order No");
				header.createCell(24).setCellValue("Shipping Method");
				header.createCell(25).setCellValue("Tracking No");
				header.createCell(26).setCellValue("Customer Order Id");
				header.createCell(27).setCellValue("Last Updated Date");
				header.createCell(28).setCellValue("Last Updated By");
				header.createCell(29).setCellValue("Status");
				header.createCell(30).setCellValue("Event Id");
				header.createCell(31).setCellValue("Event Name");
				header.createCell(32).setCellValue("Event Date");
				header.createCell(33).setCellValue("Event Time");
				header.createCell(34).setCellValue("Broker Id");
				header.createCell(35).setCellValue("Loyal Fan Order");
				header.createCell(36).setCellValue("Primary Payment Method");
				header.createCell(37).setCellValue("Secondary Payment Method");
				header.createCell(38).setCellValue("Third Payment Method");
				int i=1;
				for(OpenOrders order : openOrdersList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(order.getInvoiceId());
					row.createCell(1).setCellValue(order.getProductType());
					row.createCell(2).setCellValue(order.getCustomerId()!=null?order.getCustomerId():0);
					row.createCell(3).setCellValue(order.getCustomerName()!=null?order.getCustomerType():"");
					row.createCell(4).setCellValue(order.getUsername());
					row.createCell(5).setCellValue(order.getCustomerType()!=null?order.getCustomerType():"");
					row.createCell(6).setCellValue(order.getCustCompanyName()!=null?order.getCustCompanyName():"");
					row.createCell(7).setCellValue(order.getTicketCount()!=null?order.getTicketCount():0);
					row.createCell(8).setCellValue(order.getInvoiceTotal());
					row.createCell(9).setCellValue(order.getAddressLine1()!=null?order.getAddressLine1():"");
					row.createCell(10).setCellValue(order.getCountry()!=null?order.getCountry():"");
					row.createCell(11).setCellValue(order.getState()!=null?order.getState():"");
					row.createCell(12).setCellValue(order.getCity()!=null?order.getCity():"");
					row.createCell(13).setCellValue(order.getZipCode()!=null?order.getZipCode():"");
					row.createCell(14).setCellValue(order.getPhone()!=null?order.getPhone():"");
					row.createCell(15).setCellValue(order.getInvoiceAged()!=null?order.getInvoiceAged():0);
					row.createCell(16).setCellValue(order.getCreatedDateStr()!=null?order.getCreatedDateStr():"");
					row.createCell(17).setCellValue(order.getCreatedBy()!=null?order.getCreatedBy():"");
					row.createCell(18).setCellValue(order.getCsr()!=null?order.getCsr():"");
					row.createCell(19).setCellValue(order.getSecondaryOrderType()!=null?order.getSecondaryOrderType():"");
					row.createCell(20).setCellValue(order.getSecondaryOrderId()!=null?order.getSecondaryOrderId():"");
					row.createCell(21).setCellValue(order.getVoidedDateStr()!=null?order.getVoidedDateStr():"");
					row.createCell(22).setCellValue(order.getIsInvoiceEmailSent());
					row.createCell(23).setCellValue(order.getPurchaseOrderNo()!=null?order.getPurchaseOrderNo():"");
					row.createCell(24).setCellValue(order.getShippingMethod()!=null?order.getShippingMethod():"");
					row.createCell(25).setCellValue(order.getTrackingNo()!=null?order.getTrackingNo():"");
					row.createCell(26).setCellValue(order.getCustomerOrderId()!=null?order.getCustomerOrderId():0);
					row.createCell(27).setCellValue(order.getLastUpdatedDateStr()!=null?order.getLastUpdatedDateStr():"");
					row.createCell(28).setCellValue(order.getLastUpdatedBy()!=null?order.getLastUpdatedBy():"");
					row.createCell(29).setCellValue(order.getStatus()!=null?order.getStatus():"");
					row.createCell(30).setCellValue(order.getEventId()!=null?order.getEventId():0);
					row.createCell(31).setCellValue(order.getEventName()!=null?order.getEventName():"");
					row.createCell(32).setCellValue(order.getEventDateStr()!=null?order.getEventDateStr():"");
					row.createCell(33).setCellValue(order.getEventTimeStr()!=null?order.getEventTimeStr():"");
					row.createCell(34).setCellValue(order.getBrokerId()!=null?order.getBrokerId().toString():"");
					if(order.getOrderType()!= null && order.getOrderType().equalsIgnoreCase("LOYALFAN")){
						row.createCell(35).setCellValue("Yes");
					}else{
						row.createCell(35).setCellValue("No");
					}
					row.createCell(36).setCellValue(order.getPrimaryPaymentMethod()!=null?order.getPrimaryPaymentMethod():"");
					row.createCell(37).setCellValue(order.getSecondaryPaymentMethod()!=null?order.getSecondaryPaymentMethod():"");
					row.createCell(38).setCellValue(order.getThirdPaymentMethod()!=null?order.getThirdPaymentMethod():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=invoice.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	@RequestMapping(value = "/AddRealTicket")
	public InvoiceEditDTO addRealTicket(HttpServletRequest request, HttpServletResponse response) {
		InvoiceEditDTO invoiceEditDTO = new InvoiceEditDTO();
		Error error = new Error();
		
		try {
			String invoiceIdStr = request.getParameter("invoiceId");
			String orderId = request.getParameter("orderId");
			//String msg = request.getParameter("msg");
			//String statusStr = request.getParameter("status");	
			String brokerIdStr = request.getParameter("brokerId");

			if(StringUtils.isEmpty(invoiceIdStr)){
				System.err.println("Invoice ID not found.");
				error.setDescription("Invoice ID not found.");
				invoiceEditDTO.setError(error);
				invoiceEditDTO.setStatus(0);
				return invoiceEditDTO;
			}
			Integer invoiceId = 0;
			try{
				invoiceId = Integer.parseInt(invoiceIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Invoice ID should be valid Integer value.");
				invoiceEditDTO.setError(error);
				invoiceEditDTO.setStatus(0);
				return invoiceEditDTO;
			}			
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				invoiceEditDTO.setError(error);
				invoiceEditDTO.setStatus(0);
				return invoiceEditDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				invoiceEditDTO.setError(error);
				invoiceEditDTO.setStatus(0);
				return invoiceEditDTO;
			}
						
//			if(msg!=null && !msg.isEmpty()){
//				jObj.put("msg",msg);
//				if(statusStr != null && !statusStr.isEmpty()){
//					jObj.put("status", Boolean.parseBoolean(statusStr));
//				}
//			}
			
			List<OpenOrders> openOrderListByInv = DAORegistry.getQueryManagerDAO().getOpenOrderListByInvoice(invoiceId, brokerId);
			if(openOrderListByInv == null || openOrderListByInv.size() == 0){
				System.err.println("Invoice Details not found.");
				error.setDescription("Invoice Details not found.");
				invoiceEditDTO.setError(error);
				invoiceEditDTO.setStatus(0);
				return invoiceEditDTO;
			}
			
			Integer customerId = 0;
			for (OpenOrders openOrders : openOrderListByInv) {
				customerId = openOrders.getCustomerId();
			}
			
			List<TicketGroup> ticketGroups = new ArrayList<TicketGroup>();
			CustomerOrderDetails orderShippingAddress = DAORegistry.getCustomerOrderDetailsDAO().getCustomerOrderDetailsByOrderId(Integer.parseInt(orderId));
			List<Ticket> tickets = DAORegistry.getTicketDAO().getMappedTicketsByInvoiceId(invoiceId);
			List<Integer> list = new ArrayList<Integer>();
			Map<Integer,List<Ticket>> ticketGroupListMap = new HashMap<Integer, List<Ticket>>(); 
			List<Ticket> ticketList = new ArrayList<Ticket>();
			if(tickets!=null && !tickets.isEmpty()){
				for(Ticket tic : tickets){
					if(!list.contains(tic.getTicketGroupId())){
						list.add(tic.getTicketGroupId());
					}
					if(ticketGroupListMap.get(tic.getTicketGroupId())!=null){
						ticketList = ticketGroupListMap.get(tic.getTicketGroupId());
						ticketList.add(tic);
					}else{
						ticketList = new ArrayList<Ticket>();
						ticketList.add(tic);
					}
					ticketGroupListMap.put(tic.getTicketGroupId(),ticketList);
				}
				for(Integer ticketGroupId : list){
					TicketGroup ticketGroup = DAORegistry.getTicketGroupDAO().get(ticketGroupId);
					ticketList = ticketGroupListMap.get(ticketGroupId);
					if(ticketList!=null && !ticketList.isEmpty()){
						ticketGroup.setMappedSeatLow(ticketList.get(0).getSeatNo());
						ticketGroup.setMappedSeatHigh(ticketList.get(ticketList.size()-1).getSeatNo());
						ticketGroup.setMappedQty(ticketList.size());
					}
					ticketGroups.add(ticketGroup);
				}
			}
			
			OpenOrders operOrder = null;
			if(openOrderListByInv != null && !openOrderListByInv.isEmpty()) {
				operOrder = openOrderListByInv.get(0);
			}
			Invoice invoice = DAORegistry.getInvoiceDAO().get(invoiceId);
			CustomerOrder customerOrder = DAORegistry.getCustomerOrderDAO().get(invoice.getCustomerOrderId());
			List<InvoiceTicketAttachment> eticketAttachments = DAORegistry.getInvoiceTicketAttachmentDAO().getAttachmentByInvoiceAndFileType(invoiceId,FileType.ETICKET);			
			/*for(InvoiceTicketAttachment eTicket: eticketAttachments){
				eTicket.setFileName(Util.getFileNameFromPath(eTicket.getFilePath()));
			}*/
			List<InvoiceTicketAttachment> qrcodeAttachments = DAORegistry.getInvoiceTicketAttachmentDAO().getAttachmentByInvoiceAndFileType(invoiceId,FileType.QRCODE);
			/*for(InvoiceTicketAttachment qrCode: qrcodeAttachments){
				qrCode.setFileName(Util.getFileNameFromPath(qrCode.getFilePath()));
			}*/
			List<InvoiceTicketAttachment> barcodeAttachments = DAORegistry.getInvoiceTicketAttachmentDAO().getAttachmentByInvoiceAndFileType(invoiceId,FileType.BARCODE);
			/*for(InvoiceTicketAttachment barCode: barcodeAttachments){
				barCode.setFileName(Util.getFileNameFromPath(barCode.getFilePath()));
			}*/
			
			if(invoice!=null){
				InvoiceDTO invoiceDTO = new InvoiceDTO();					
				invoiceDTO.setInvoiceId(invoice.getId());
				invoiceDTO.setInvoiceTrackingNo(invoice.getTrackingNo());
				invoiceDTO.setInvoiceStatus(invoice.getStatus());
				invoiceDTO.setInvoiceRealTixMap(invoice.getRealTixMap());
				
				invoiceEditDTO.setInvoiceDTO(invoiceDTO);
				
				if(invoice.getExpDeliveryDate()!=null){
					DateFormat format = new SimpleDateFormat("mm/DD/yyyy");
					InvoiceDTO invoiceExpDelDateDTO = new InvoiceDTO();
					invoiceExpDelDateDTO.setExpDeliveryDate(format.format(invoice.getExpDeliveryDate()));
					
					invoiceEditDTO.setInvoiceExpDeliveryDateDTO(invoiceExpDelDateDTO);
				}				
			}
			if(customerOrder != null){
				invoiceEditDTO.setCustomerOrderDTO(com.rtw.tracker.utils.Util.getCustomerOrderObject(customerOrder));				
			}						
			if(operOrder != null){
				invoiceEditDTO.setOpenOrdersDTO(com.rtw.tracker.utils.Util.getOpenOrderObject(operOrder, customerOrder));
			}			
			if(orderShippingAddress != null){
				invoiceEditDTO.setCustomerShippingAddressDTO(com.rtw.tracker.utils.Util.getCustomerShipppingAddressObject(orderShippingAddress));
			}
			if(ticketGroups != null){
				invoiceEditDTO.setTicketGroupDTO(com.rtw.tracker.utils.Util.getPOTicketGroupArray(ticketGroups));
			}
			if(eticketAttachments != null){
				invoiceEditDTO.setEticketAttachmentDTO(com.rtw.tracker.utils.Util.getETicketAttachmentArray(eticketAttachments));
			}
			invoiceEditDTO.setEticketCount(eticketAttachments.size());
			
			if(qrcodeAttachments != null){
				invoiceEditDTO.setQrcodeAttachmentDTO(com.rtw.tracker.utils.Util.getQRCodeAttachmentArray(qrcodeAttachments));
			}
			invoiceEditDTO.setQrcodeCount(qrcodeAttachments.size());
			
			if(barcodeAttachments != null){
				invoiceEditDTO.setBarcodeAttachmentDTO(com.rtw.tracker.utils.Util.getBarCodeAttachmentArray(barcodeAttachments));
			}
			invoiceEditDTO.setBarcodeCount(barcodeAttachments.size());
			
			Collection<ShippingMethod> shippingMethods = DAORegistry.getShippingMethodDAO().getAll();
			if(shippingMethods != null){
				invoiceEditDTO.setShippingMethodDTO(com.rtw.tracker.utils.Util.getShippingMethodArray(shippingMethods));
			}

			invoiceEditDTO.setStatus(1);
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Invoice Edit.");
			invoiceEditDTO.setError(error);
			invoiceEditDTO.setStatus(0);
			return invoiceEditDTO;
		}
		return invoiceEditDTO;
	}
	
	@RequestMapping(value = "/GetLongInventoryForInvoice")
	public InvoiceGetLongTicketDTO getLongInventoryForInvoice(HttpServletRequest request, HttpServletResponse response){
		InvoiceGetLongTicketDTO invoiceGetLongTicketDTO = new InvoiceGetLongTicketDTO();
		Error error = new Error();
		
		try {
			String eventIdStr = request.getParameter("eventId");
			String rowId = request.getParameter("rowId");
			String ticketId = request.getParameter("ticketId");
			String brokerIdStr = request.getParameter("brokerId");
			
			if(StringUtils.isEmpty(eventIdStr)){
				System.err.println("Not able to Identify event details.");
				error.setDescription("Not able to Identify event details.");
				invoiceGetLongTicketDTO.setError(error);
				invoiceGetLongTicketDTO.setStatus(0);
				return invoiceGetLongTicketDTO;
			}
			Integer eventId = 0;
			try{
				eventId = Integer.parseInt(eventIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Not able to Identify event details.");
				invoiceGetLongTicketDTO.setError(error);
				invoiceGetLongTicketDTO.setStatus(0);
				return invoiceGetLongTicketDTO;
			}			
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				invoiceGetLongTicketDTO.setError(error);
				invoiceGetLongTicketDTO.setStatus(0);
				return invoiceGetLongTicketDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				invoiceGetLongTicketDTO.setError(error);
				invoiceGetLongTicketDTO.setStatus(0);
				return invoiceGetLongTicketDTO;
			}
						
			List<TicketGroup> ticketGroupList = null;
			ticketGroupList = DAORegistry.getTicketGroupDAO().getAllTicketGroupsByEventAndBroker(eventId, brokerId);
			if(ticketGroupList == null || ticketGroupList.isEmpty()){
				System.err.println("No matching Long tickets are found for selected invoice.");
				error.setDescription("No matching Long tickets are found for selected invoice.");
				invoiceGetLongTicketDTO.setError(error);
				invoiceGetLongTicketDTO.setStatus(0);
				return invoiceGetLongTicketDTO;
			}
			
			List<TicketGroup> zeroQuntityList = new ArrayList<TicketGroup>();
			List<Ticket> tickets = null;
			if(ticketGroupList!=null && !ticketGroupList.isEmpty()){
				for(TicketGroup ticGroup : ticketGroupList){
					tickets = DAORegistry.getTicketDAO().getAvailableTicketsByTicketGroupId(ticGroup.getId());
					if(tickets.size() > 0 && tickets.size() <= ticGroup.getQuantity()){
						ticGroup.setAvailableSeatLow(tickets.get(0).getSeatNo());
						ticGroup.setAvailableSeatHigh(tickets.get(tickets.size()-1).getSeatNo());
						ticGroup.setAvailableQty(tickets.size());
					}else{
						zeroQuntityList.add(ticGroup);
					}
				}
				if(!zeroQuntityList.isEmpty()){
					ticketGroupList.removeAll(zeroQuntityList);
				}
			}
			if(ticketGroupList == null || ticketGroupList.isEmpty()){
				System.err.println("No matching Long tickets are found for selected invoice.");
				error.setDescription("No matching Long tickets are found for selected invoice.");
				invoiceGetLongTicketDTO.setError(error);
				invoiceGetLongTicketDTO.setStatus(0);
				return invoiceGetLongTicketDTO;
			}
			invoiceGetLongTicketDTO.setStatus(1);
			invoiceGetLongTicketDTO.setTicketGroupDTO(com.rtw.tracker.utils.Util.getPOTicketGroupArray(ticketGroupList));
			invoiceGetLongTicketDTO.setTicketGroupPaginationDTO(PaginationUtil.getDummyPaginationParameters(ticketGroupList!=null?ticketGroupList.size():0));
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Long Inventory For Invoice.");
			invoiceGetLongTicketDTO.setError(error);
			invoiceGetLongTicketDTO.setStatus(0);
		}
		return invoiceGetLongTicketDTO;
	}

	@RequestMapping(value="/checkFileName")
	public InvoiceTicketCheckFileNameDTO checkFileName(HttpServletRequest request, HttpServletResponse response){
		InvoiceTicketCheckFileNameDTO invoiceTicketCheckFileNameDTO = new InvoiceTicketCheckFileNameDTO();
		Error error = new Error();
		
		try{
			String fileNameStr = request.getParameter("fileName");
			List<InvoiceTicketAttachment> invoiceTktAttachmentList = null;
			String invoiceIdStr = "";
			
			if(fileNameStr!=null && !fileNameStr.isEmpty()){
				invoiceTktAttachmentList = DAORegistry.getInvoiceTicketAttachmentDAO().getAttachmentByFileName(fileNameStr);
				if(invoiceTktAttachmentList != null && invoiceTktAttachmentList.size() > 0){
					for(InvoiceTicketAttachment invoiceTktAttachment : invoiceTktAttachmentList){
						if(invoiceTktAttachmentList.size() <= 1){
							invoiceIdStr += invoiceTktAttachment.getInvoiceId();
						}else{
							invoiceIdStr += invoiceTktAttachment.getInvoiceId()+",";
						}
					}
					invoiceTicketCheckFileNameDTO.setInvoiceId(invoiceIdStr);
					invoiceTicketCheckFileNameDTO.setStatus(1);
				}
				invoiceTicketCheckFileNameDTO.setStatus(0);
			}			
		}catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Checking Real Ticket Attachment File Name.");
			invoiceTicketCheckFileNameDTO.setError(error);
			invoiceTicketCheckFileNameDTO.setStatus(0);
		}
		return invoiceTicketCheckFileNameDTO;
	}
	
	@RequestMapping(value = "/DownloadRealTix")
	public void downloadRealTix(HttpServletRequest request, HttpServletResponse response){
		
		try {
			String invoiceIdStr = request.getParameter("invoiceId");
			String fileTypeStr = request.getParameter("fileType");
			String position = request.getParameter("position");
			
			if(invoiceIdStr!=null && !invoiceIdStr.isEmpty() && fileTypeStr!=null && !fileTypeStr.isEmpty()
					&& position !=null && !position.isEmpty()){
				FileType fileType = FileType.valueOf(fileTypeStr);
				InvoiceTicketAttachment attachment = DAORegistry.getInvoiceTicketAttachmentDAO().getAttachmentByInvoiceFileTypeAndPosition(Integer.parseInt(invoiceIdStr), fileType,Integer.parseInt(position));
				if(attachment!=null){
					OutputStream out = response.getOutputStream();
					File file = new File(attachment.getFilePath());
					if(file.exists()){
						FileInputStream in = new FileInputStream(file);
						//response.setContentType("application/octet-stream");
						response.setContentType("application/pdf");
						response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"",file.getName()));
						byte[] buffer = new byte[4096];
						int length;
						while((length = in.read(buffer)) > 0){
						    out.write(buffer, 0, length);
						}
						in.close();
						out.flush();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/DeleteRealTixAttachment")
	public GenericResponseDTO deleteRealTixAttachment(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		
		try {
			String invoiceIdStr = request.getParameter("invoiceId");
			String fileTypeStr = request.getParameter("fileType");
			String positionStr = request.getParameter("position");
			String userName = request.getParameter("userName");
			
			if(StringUtils.isEmpty(invoiceIdStr)){
				System.err.println("Invoice ID not found.");
				error.setDescription("Invoice ID not found.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			Integer invoiceId = 0;
			try{
				invoiceId = Integer.parseInt(invoiceIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Invoice ID should be valid Integer value.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			if(StringUtils.isEmpty(fileTypeStr)){
				System.err.println("File Type not found.");
				error.setDescription("File Type not found.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			if(StringUtils.isEmpty(positionStr)){
				System.err.println("File Position not found.");
				error.setDescription("File Position not found.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			Integer position = 0;
			try{
				position = Integer.parseInt(positionStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("File Position should be valid Integer value.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
										
			FileType fileType = FileType.valueOf(fileTypeStr);
			InvoiceTicketAttachment attachment = DAORegistry.getInvoiceTicketAttachmentDAO().getAttachmentByInvoiceFileTypeAndPosition(invoiceId, fileType, position);
			if(attachment == null){
				System.err.println("File not found.");
				error.setDescription("File not found.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}			
			OutputStream out = response.getOutputStream();
			File file = new File(attachment.getFilePath());
			if(file.exists()){
				file.delete();
			}
			DAORegistry.getInvoiceTicketAttachmentDAO().delete(attachment);
					
			List<InvoiceTicketAttachment> invoiceTktAttachments = DAORegistry.getInvoiceTicketAttachmentDAO().getTicketAttachmentByInvoiceId(invoiceId);
			if(invoiceTktAttachments == null || invoiceTktAttachments.size() <= 0){
				Invoice invoice = DAORegistry.getInvoiceDAO().get(invoiceId);
				if(invoice != null){
					invoice.setStatus(InvoiceStatus.Outstanding);
					invoice.setLastUpdatedBy(userName);
					invoice.setLastUpdated(new Date());
					invoice.setIsRealTixUploaded("No");
					DAORegistry.getInvoiceDAO().update(invoice);
								
					InvoiceAudit audit = new InvoiceAudit(invoice);
					audit.setAction(InvoiceAuditAction.INVOICE_COMPLETED_TO_OUTSTANDING);
					audit.setCreateBy(userName);
					audit.setCreatedDate(new Date());
					audit.setNote(userName+" has removed uploaded ticket file from invoice, now this invoice does not have any real ticket file uploaded so changing status to outstanding..");
					DAORegistry.getInvoiceAuditDAO().save(audit);
				}
			}
		
			genericResponseDTO.setMessage("File Deleted successfully.");
			genericResponseDTO.setStatus(1);
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Deleting Real Ticket Attachment.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
	@RequestMapping(value = "/SaveOpenOrders")
	public GenericResponseDTO saveOpenOrders(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		
		try {
			String action = request.getParameter("action");
			String invoiceIdStr = request.getParameter("invoiceId");
			String eventIdStr = request.getParameter("eventId");
			String expDeliveryDate = request.getParameter("expDeliveryDate");
			String trackingNo = request.getParameter("trackingNo");
			String userName = request.getParameter("userName");
						
			DateFormat format = new SimpleDateFormat("mm/DD/yyyy");
			String returnMessage = "";
			boolean status = false;
			
			if(StringUtils.isEmpty(invoiceIdStr)){
				System.err.println("Invoice ID not found.");
				error.setDescription("Invoice ID not found.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			Integer invId = 0;
			try{
				invId = Integer.parseInt(invoiceIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Invoice ID should be valid Integer value.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			 
			Invoice invoice = null;
			invoice =  DAORegistry.getInvoiceDAO().get(invId);			 
			if(invoice == null){
				System.err.println("Invoice Details not found.");
				error.setDescription("Invoice Details not found.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			//if(returnMessage != null){
			 if(action != null && action.equals("openOrderSave")){
				 
				Integer rowCount = Integer.parseInt(request.getParameter("rowCount"));
				String isTicketDelivered = request.getParameter("realTixDelivered");
				String sentTicketToCust = request.getParameter("sendTicketToCustomer");
				String shippingMethod = request.getParameter("shippingMethod");
				 
				if(StringUtils.isEmpty(shippingMethod)){
					System.err.println("Not able to idenfity Shipping Method.");
					error.setDescription("Not able to idenfity Shipping Method.");
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					return genericResponseDTO;
				}
				Integer shippingMethodId = -1;
				try{
					shippingMethodId = Integer.parseInt(shippingMethod);
				}catch(Exception e){
					e.printStackTrace();
					error.setDescription("Shipping Method should be valid value.");
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					return genericResponseDTO;
				}
				
				ShippingMethod shipMethod = DAORegistry.getShippingMethodDAO().get(shippingMethodId);
				if(shipMethod == null){
					System.err.println("Shipping method is not found.");
					error.setDescription("Shipping method is not found.");
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					return genericResponseDTO;
				}
				 
				CustomerOrder order = null;
				order = DAORegistry.getCustomerOrderDAO().get(invoice.getCustomerOrderId());
				if(order == null){
					System.err.println("Customer order not found in system.");
					error.setDescription("Customer order not found in system.");
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					return genericResponseDTO;
				}
				
				Boolean isRealTixDelivered = false;
				if(isTicketDelivered != null && isTicketDelivered.equals("1")) {
					isRealTixDelivered = true;
				}
				 
				 invoice.setTrackingNo(trackingNo);
				 invoice.setLastUpdated(new Date());
				 invoice.setLastUpdatedBy(userName);
				 if(shippingMethodId>=0){
					 invoice.setShippingMethodId(shippingMethodId);
					 invoice.setRealTixShippingMethod(shipMethod.getName());
				 }
				 invoice.setRealTixMap("Yes");
				 
				 invoice.setRealTixDelivered(isRealTixDelivered);
				 invoice.setUploadToExchnge(false);
				 if(sentTicketToCust!=null && sentTicketToCust.equalsIgnoreCase("Yes")){
					 invoice.setUploadToExchnge(true);
				 }
				 
				 if(expDeliveryDate!=null && !expDeliveryDate.isEmpty()){
					 invoice.setExpDeliveryDate(format.parse(expDeliveryDate));
				 }
				 order.setShippingMethod(shipMethod.getName());
				 order.setShippingMethodId(shippingMethodId);
				 
				 List<Ticket> ticketList  = new ArrayList<Ticket>();
				 List<CategoryTicket> categoryTicketList = new ArrayList<CategoryTicket>();
				 if(!order.getIsLongSale()){
					 List<CategoryTicket> oldMappedCategoryTicket = DAORegistry.getCategoryTicketDAO().getMappedCategoryTicketByInvoiceId(invId);
					 List<Ticket> oldMappedTicket = DAORegistry.getTicketDAO().getMappedTicketsByInvoiceId(invId);
					 if(oldMappedCategoryTicket!=null){
						 for(CategoryTicket catTicket:oldMappedCategoryTicket) {
							catTicket.setTicketId(null);
						}
						DAORegistry.getCategoryTicketDAO().updateAll(oldMappedCategoryTicket);
					 }
					 if(oldMappedTicket!=null){
						 for(Ticket ticket:oldMappedTicket) {
							 ticket.setInvoiceId(null);
							 ticket.setTicketStatus(TicketStatus.ACTIVE);
						}
						DAORegistry.getTicketDAO().updateAll(oldMappedTicket);
					 }
					 int totalQty = 0;
					 List<Integer> tgIds = new ArrayList<Integer>();
					 for(int i=1; i<=rowCount; i++){
						 int counter = 0;
						String ticketGroupId = request.getParameter("ticketGroup_"+i);
						String seatHighStr = request.getParameter("seatHigh_"+i);
						String seatLowStr = request.getParameter("seatLow_"+i);
						int seatLow = Integer.parseInt(seatLowStr);
						//int seatHigh = Integer.parseInt(seatHighStr);
						Integer qty = Integer.parseInt(request.getParameter("qty_"+i));
						totalQty += qty;
						 if(ticketGroupId!=null && !ticketGroupId.isEmpty() && qty > 0){
							 if(!tgIds.contains(Integer.parseInt(ticketGroupId))){
								 tgIds.add(Integer.parseInt(ticketGroupId));
							 }
							 List<Ticket> tickets = DAORegistry.getTicketDAO().getAvailableTicketsByTicketGroupId(Integer.parseInt(ticketGroupId));
							 if(tickets!=null && !tickets.isEmpty()){
								 List<CategoryTicket> categoryTickets = DAORegistry.getCategoryTicketDAO().getUnmappedCategoryTicketByInvoiceId(invId);
								 for(int j=seatLow;j<(seatLow+100);j++){
									 if(counter == qty){
										 break;
									 }
									 for(Ticket tic:tickets){
										 if(tic.getSeatNo().equalsIgnoreCase(String.valueOf(j)) && tic.getTicketStatus().equals(TicketStatus.ACTIVE)
												 && (tic.getInvoiceId()==null || tic.getInvoiceId()==0)){
											 for(Ticket t : ticketList){
												 if(t.getId() == tic.getId() || t.getId().equals(tic.getId())){
													System.err.println("SeatNo : "+tic.getSeatNo()+" Added two time in mapping, please fill unique seat no range in each ticket group.");
													error.setDescription("SeatNo : "+tic.getSeatNo()+" Added two time in mapping, please fill unique seat no range in each ticket group.");
													genericResponseDTO.setError(error);
													genericResponseDTO.setStatus(0);
													return genericResponseDTO;
												 }
											 }
											 tic.setInvoiceId(invId);
											 tic.setTicketStatus(TicketStatus.SOLD);
											 ticketList.add(tic);
											 if(order.getIsLongSale()){
												 counter++;
											 }
											 if(categoryTickets!=null && !categoryTickets.isEmpty()){
												 for(CategoryTicket catTicket:categoryTickets){
													 if(catTicket.getTicketId()==null || catTicket.getInvoiceId()==invId){
														 catTicket.setTicketId(tic.getId());
														 categoryTicketList.add(catTicket);
														 counter++;
														 break;
													 }
												 }
										 	}
									 	} 
									 }
								 }
								 if(order.getIsLongSale() && ticketList.size() != totalQty){
									  returnMessage = "SeatLow:"+seatLowStr +", SeatHigh:"+seatHighStr+" is already mapped with another invoice, Please select Other avilable seats";									 
									  System.err.println(returnMessage);
									  error.setDescription(returnMessage);
									  genericResponseDTO.setError(error);
									  genericResponseDTO.setStatus(0);
									  return genericResponseDTO;
								 }
								 if(!order.getIsLongSale() && (ticketList.size() != totalQty || categoryTicketList.size()!=totalQty)){
									  returnMessage = "SeatLow:"+seatLowStr +", SeatHigh:"+seatHighStr+" is already mapped with another invoice, Please select Other avilable seats";
									  System.err.println(returnMessage);
									  error.setDescription(returnMessage);
									  genericResponseDTO.setError(error);
									  genericResponseDTO.setStatus(0);
									  return genericResponseDTO;
								 }
								 categoryTickets.clear(); 
							 }
						 }
					 }
					 boolean isMapped  = false;
					 if(order.getIsLongSale() && ticketList.size() == totalQty){
						 DAORegistry.getTicketDAO().updateAll(ticketList);
						 List<RealTicketSectionDetails> realTicketList = DAORegistry.getQueryManagerDAO().getRealTicketSectionDetailsByInvoiceId(invoice.getId());
						 Util.setActualTicketDetails(realTicketList, order);
						 categoryTicketList.clear();
						 ticketList.clear();
						 isMapped=true;
					 }
					 if(!order.getIsLongSale() && ticketList.size() == totalQty && categoryTicketList.size()==totalQty){
						 DAORegistry.getCategoryTicketDAO().updateAll(categoryTicketList);
						 DAORegistry.getTicketDAO().updateAll(ticketList);
						 List<RealTicketSectionDetails> realTicketList = DAORegistry.getQueryManagerDAO().getRealTicketSectionDetailsByInvoiceId(invoice.getId());
						 Util.setActualTicketDetails(realTicketList, order);
						 categoryTicketList.clear();
						 ticketList.clear();
						 isMapped=true;
					 }
					 if(isMapped){
						List<OrderTicketGroupDetails> orderTickets = new ArrayList<OrderTicketGroupDetails>();
						List<OrderTicketGroupDetails> oldOrderTicketGroupDetails = DAORegistry.getOrderTicketGroupDetailsDAO().getOrderTicketGroupDetailsByOrderId(order.getId());
						for(Integer id : tgIds){
							TicketGroup tg = DAORegistry.getTicketGroupDAO().get(id);
							List<Ticket> tickets = DAORegistry.getTicketDAO().getMappedTicketsByInvoiceIdandTicketGroupId(invoice.getId(),tg.getId());
							OrderTicketGroupDetails orderTicket = new OrderTicketGroupDetails();
//							String zone = Util.computZoneForTickets(tg.getEventId(),tg.getSection());
//							if(zone==null || zone.isEmpty()){
//								zone=order.getSection();
//							}
							orderTicket.setZone(order.getSection());
							orderTicket.setQuantity(tickets.size());
							orderTicket.setSection(tg.getSection());
							orderTicket.setRow(tg.getRow());
							orderTicket.setTicketGroupId(tg.getId());
							orderTicket.setSoldPrice(order.getSoldPrice());
							orderTicket.setActualPrice(tg.getPrice());
							orderTicket.setOrderId(order.getId());
							
							
							if(tickets.size() == 1){
								orderTicket.setSeat(tickets.get(0).getSeatNo());
							}else{
								orderTicket.setSeat(tickets.get(0).getSeatNo()+"-"+tickets.get(tickets.size()-1).getSeatNo());
							}
							orderTickets.add(orderTicket);
						}
						
						DAORegistry.getOrderTicketGroupDetailsDAO().saveAll(orderTickets);
						DAORegistry.getOrderTicketGroupDetailsDAO().deleteAll(oldOrderTicketGroupDetails);
						 
						String poNoStr = "";
						 for(Ticket tic:ticketList){
							 if(!poNoStr.contains(String.valueOf(tic.getPoId()))){
								 poNoStr = poNoStr + tic.getPoId()+",";
							 }
						 }
						 returnMessage = "PO Mapped successfully.";
						 						
						 List<InvoiceAudit> list = DAORegistry.getInvoiceAuditDAO().getInvoiceAuditByInvoiceIdAndAction(invoice.getId(), InvoiceAuditAction.REAL_TICKET_MAPPED);
						 for(InvoiceAudit au : list){
							 if(au.getNote().equalsIgnoreCase("PO Mapped, PO. No:"+poNoStr)){
								 isMapped =true;
								 break;
							 }
						 }
						 if(!isMapped){
							 InvoiceAudit audit = new InvoiceAudit(invoice);
							 audit.setAction(InvoiceAuditAction.REAL_TICKET_MAPPED);
							 audit.setNote("PO Mapped, PO. No:"+poNoStr);
							 audit.setCreatedDate(new Date());
							 audit.setCreateBy(userName);
							 DAORegistry.getInvoiceAuditDAO().save(audit);
						 }
					 }else{
						 System.err.println("Not able to map real ticket selected seat is already mapped, Please select some other seat.");
						 error.setDescription("Not able to map real ticket selected seat is already mapped, Please select some other seat.");
						 genericResponseDTO.setError(error);
						 genericResponseDTO.setStatus(0);
						 return genericResponseDTO;
					 }
				 }
				 DAORegistry.getInvoiceDAO().saveOrUpdate(invoice);
				 DAORegistry.getCustomerOrderDAO().update(order);
				 if(returnMessage == null || returnMessage.isEmpty()){
					 returnMessage = "Invoice Updated successfully.";
				 }
				 
				 Map<String,Boolean> fileUploadMap = new HashMap<String, Boolean>();
				 if(uploadRealTicket(request,fileUploadMap)){
					 //invoice.setIsRealTixUploaded("PROCESSING");
					// DAORegistry.getInvoiceDAO().update(invoice);
					 Map<String,String> requestMap = Util.getParameterMap(request);
					 requestMap.put("orderId",String.valueOf(invoice.getCustomerOrderId()));
					 
					 List<InvoiceTicketAttachment> eTicketAttachment = new ArrayList<InvoiceTicketAttachment>();
					 List<InvoiceTicketAttachment> qrCodeAttachment = new ArrayList<InvoiceTicketAttachment>();
					 List<InvoiceTicketAttachment> barcodeAttachment = new ArrayList<InvoiceTicketAttachment>();
					 
					 List<InvoiceTicketAttachment> attachments = DAORegistry.getInvoiceTicketAttachmentDAO().getTicketAttachmentByInvoiceId(invoice.getId());
					 for(InvoiceTicketAttachment attachment :attachments){
						 if(attachment.getIsEmailSent()== null || !attachment.getIsEmailSent()){
							 if(attachment.getType().equals(FileType.ETICKET)){
								 eTicketAttachment.add(attachment);
							 }else if(attachment.getType().equals(FileType.QRCODE)){
								 qrCodeAttachment.add(attachment);
							 }else if(attachment.getType().equals(FileType.BARCODE)){
								 barcodeAttachment.add(attachment);
							 }
						 }
					 }
					 
					 boolean isAllFileUploaded = false;
					 List<String> allFiles = new ArrayList<String>();
					 for(InvoiceTicketAttachment attachment :attachments){
						 if(attachment.getIsEmailSent()== null || !attachment.getIsEmailSent()){
							 allFiles.add(attachment.getFilePath());
						 }
					 }
					
					 List<String> eticketFiles = new ArrayList<String>();
					 List<String> qrCodeFiles = new ArrayList<String>();
					 List<String> barcodeFile = new ArrayList<String>();
					 
					 if(invoice.getUploadToExchnge()){
						 for(InvoiceTicketAttachment a :eTicketAttachment){
							 if(a.getIsSent() == null || !a.getIsSent()){
								 eticketFiles.add(a.getFilePath());
							 }
						 }
						 for(InvoiceTicketAttachment a :qrCodeAttachment){
							 if(a.getIsSent() == null || !a.getIsSent()){
								 qrCodeFiles.add(a.getFilePath());
							 }
						 }
						 for(InvoiceTicketAttachment a :barcodeAttachment){
							 if(a.getIsSent() == null || !a.getIsSent()){
								 barcodeFile.add(a.getFilePath());
							 }
						 }
						 requestMap.put("attachTicket","true");
					 }else{
						 requestMap.put("attachTicket","false");
					 }
					 
					 boolean isEticket = fileUploadMap.get(FileType.ETICKET.toString());
					 boolean isqrCode = fileUploadMap.get(FileType.QRCODE.toString());
					 boolean isBarcode = fileUploadMap.get(FileType.BARCODE.toString());
					 
					 for(String fileName  :allFiles){
						 	File newFile = new File(fileName);
						 	if(!newFile.exists()){
						 		newFile.createNewFile();
						 	}
						 	if(fileName!=null && fileName.contains("/")){
						 		fileName = fileName.substring(fileName.lastIndexOf("/")+1);
							}
							AwsS3Response awsRsponse = AWSFileService.upLoadFileToS3(AWSFileService.BUCKET_RTF_ASSETS, fileName, AWSFileService.TICKET_FOLDER, newFile );
							if(null == awsRsponse || awsRsponse.getStatus() != 1){
								 InvoiceAudit audit = new InvoiceAudit(invoice);
								 audit.setAction(InvoiceAuditAction.REAL_TICKET_UPLOADED);
								 audit.setNote("Error occured while uploading ticket files to S3.");
								 audit.setCreatedDate(new Date());
								 audit.setCreateBy(userName);
								 DAORegistry.getInvoiceAuditDAO().save(audit);
								 DAORegistry.getInvoiceTicketAttachmentDAO().deleteAll(eTicketAttachment);
								 DAORegistry.getInvoiceTicketAttachmentDAO().deleteAll(barcodeAttachment);
								 DAORegistry.getInvoiceTicketAttachmentDAO().deleteAll(qrCodeAttachment);
								genericResponseDTO.setStatus(0);
								error.setDescription("Error occured while uploading ticket file to S3.");
								genericResponseDTO.setError(error);
								return genericResponseDTO;
							}
					 }
					 
					 if(isEticket){
						 requestMap.put("fileType",FileType.ETICKET.toString());						 
						 String data = Util.getObjectWithFile(requestMap, com.rtw.tmat.utils.Constants.BASE_URL + com.rtw.tmat.utils.Constants.TICKET_DOWNLOAD_NOTIFICATION,eticketFiles,allFiles);
						 if(data == null || data.isEmpty() || data.contains("502 Bad Gateway")){
							 returnMessage = "Error Occured while Sending email to customer, Looks like RTF API is down.";
							 InvoiceAudit audit = new InvoiceAudit(invoice);
							 audit.setAction(InvoiceAuditAction.REAL_TICKET_UPLOADED);
							 audit.setNote(returnMessage);
							 audit.setCreatedDate(new Date());
							 audit.setCreateBy(userName);
							 DAORegistry.getInvoiceAuditDAO().save(audit);
							// DAORegistry.getInvoiceTicketAttachmentDAO().deleteAll(eTicketAttachment);
							 error.setDescription(returnMessage);
							 genericResponseDTO.setError(error);
							 genericResponseDTO.setStatus(0);
							 return genericResponseDTO;
						 }
						 
						 Gson gson = new Gson();		
						 JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
						 TicketDownloadNotification ticketDownload = gson.fromJson(((JsonObject)jsonObject.get("ticketDownloadNotification")), TicketDownloadNotification.class);
						 
						 if(ticketDownload!=null && ticketDownload.getStatus()==1){
							 returnMessage = "PO Mapped/Ticket FIle uploaded and email sent to customer.";
							 
							 InvoiceAudit audit = new InvoiceAudit(invoice);
							 audit.setAction(InvoiceAuditAction.REAL_TICKET_UPLOADED);
							 audit.setNote(returnMessage);
							 audit.setCreatedDate(new Date());
							 audit.setCreateBy(userName);
							 DAORegistry.getInvoiceAuditDAO().save(audit);
							 for(InvoiceTicketAttachment a :eTicketAttachment){
								a.setIsEmailSent(true);
								if(invoice.getUploadToExchnge()){
									a.setIsSent(true);
								}
							 }
							 isAllFileUploaded = true;
							 DAORegistry.getInvoiceTicketAttachmentDAO().updateAll(eTicketAttachment);							
						 }else{
							 DAORegistry.getInvoiceTicketAttachmentDAO().deleteAll(eTicketAttachment);
							 returnMessage = "PO Mapped, Error while Uploading  ticket file to API server and sending email to customer.";
							 
							 InvoiceAudit audit = new InvoiceAudit(invoice);
							 audit.setAction(InvoiceAuditAction.REAL_TICKET_UPLOADED);
							 audit.setNote(returnMessage);
							 audit.setCreatedDate(new Date());
							 audit.setCreateBy(userName);
							 DAORegistry.getInvoiceAuditDAO().save(audit);
							 error.setDescription(returnMessage);
							 genericResponseDTO.setError(error);
							 genericResponseDTO.setStatus(0);
							 return genericResponseDTO;
						 }
					 }
					 if(isqrCode){
						 if(isAllFileUploaded == true){
							 allFiles=null;
						 }
						 requestMap.put("fileType",FileType.QRCODE.toString());
						 String data = Util.getObjectWithFile(requestMap, com.rtw.tmat.utils.Constants.BASE_URL + com.rtw.tmat.utils.Constants.TICKET_DOWNLOAD_NOTIFICATION,qrCodeFiles,allFiles);
						 if(data == null || data.isEmpty() || data.contains("502 Bad Gateway")){
							 returnMessage = "Error Occured while sending email to customer, Looks like RTF API is down.";
							 InvoiceAudit audit = new InvoiceAudit(invoice);
							 audit.setAction(InvoiceAuditAction.REAL_TICKET_UPLOADED);
							 audit.setNote(returnMessage);
							 audit.setCreatedDate(new Date());
							 audit.setCreateBy(userName);
							 DAORegistry.getInvoiceAuditDAO().save(audit);
							 //DAORegistry.getInvoiceTicketAttachmentDAO().deleteAll(qrCodeAttachment);
							 error.setDescription(returnMessage);
							 genericResponseDTO.setError(error);
							 genericResponseDTO.setStatus(0);
							 return genericResponseDTO;
						 }
						 
						 Gson gson = new Gson();		
						 JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
						 TicketDownloadNotification ticketDownload = gson.fromJson(((JsonObject)jsonObject.get("ticketDownloadNotification")), TicketDownloadNotification.class);
						 
						 if(ticketDownload!=null && ticketDownload.getStatus()==1){
							 returnMessage = "PO Mapped/Ticket FIle uploaded and email sent to customer.";
							 
							 InvoiceAudit audit = new InvoiceAudit(invoice);
							 audit.setAction(InvoiceAuditAction.REAL_TICKET_UPLOADED);
							 audit.setNote(returnMessage);
							 audit.setCreatedDate(new Date());
							 audit.setCreateBy(userName);
							 DAORegistry.getInvoiceAuditDAO().save(audit);
							 for(InvoiceTicketAttachment a :qrCodeAttachment){
								a.setIsEmailSent(true);
								if(invoice.getUploadToExchnge()){
									a.setIsSent(true);
								}
							 }
							 isAllFileUploaded = true;
							 DAORegistry.getInvoiceTicketAttachmentDAO().updateAll(qrCodeAttachment);
						 }else{
							 DAORegistry.getInvoiceTicketAttachmentDAO().deleteAll(qrCodeAttachment);
							 returnMessage = "PO Mapped, Error while Uploading  ticket file to API server and sending email to customer.";
							 
							 InvoiceAudit audit = new InvoiceAudit(invoice);
							 audit.setAction(InvoiceAuditAction.REAL_TICKET_UPLOADED);
							 audit.setNote(returnMessage);
							 audit.setCreatedDate(new Date());
							 audit.setCreateBy(userName);
							 DAORegistry.getInvoiceAuditDAO().save(audit);
							 error.setDescription(returnMessage);
							 genericResponseDTO.setError(error);
							 genericResponseDTO.setStatus(0);
							 return genericResponseDTO;
						 }
					 }
					 if(isBarcode){
						 if(isAllFileUploaded == true){
							 allFiles=null;
						 }
						 requestMap.put("fileType",FileType.BARCODE.toString());
						 String data = Util.getObjectWithFile(requestMap, com.rtw.tmat.utils.Constants.BASE_URL + com.rtw.tmat.utils.Constants.TICKET_DOWNLOAD_NOTIFICATION,barcodeFile,allFiles);
						 if(data == null || data.isEmpty() || data.contains("502 Bad Gateway")){
							 returnMessage = "Error Occured while Uploading ticket files to RTF API server, Looks like RTF API is down.";
							 InvoiceAudit audit = new InvoiceAudit(invoice);
							 audit.setAction(InvoiceAuditAction.REAL_TICKET_UPLOADED);
							 audit.setNote(returnMessage);
							 audit.setCreatedDate(new Date());
							 audit.setCreateBy(userName);
							 DAORegistry.getInvoiceAuditDAO().save(audit);
							// DAORegistry.getInvoiceTicketAttachmentDAO().deleteAll(barcodeAttachment);
							 error.setDescription(returnMessage);
							 genericResponseDTO.setError(error);
							 genericResponseDTO.setStatus(0);
							 return genericResponseDTO;
						 }
						 
						 Gson gson = new Gson();		
						 JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
						 TicketDownloadNotification ticketDownload = gson.fromJson(((JsonObject)jsonObject.get("ticketDownloadNotification")), TicketDownloadNotification.class);
						 
						 if(ticketDownload!=null && ticketDownload.getStatus()==1){
							 returnMessage = "PO Mapped/Ticket FIle uploaded and email sent to customer.";
							 
							 InvoiceAudit audit = new InvoiceAudit(invoice);
							 audit.setAction(InvoiceAuditAction.REAL_TICKET_UPLOADED);
							 audit.setNote(returnMessage);
							 audit.setCreatedDate(new Date());
							 audit.setCreateBy(userName);
							 DAORegistry.getInvoiceAuditDAO().save(audit);
							 for(InvoiceTicketAttachment a :barcodeAttachment){
								a.setIsEmailSent(true);
								if(invoice.getUploadToExchnge()){
									a.setIsSent(true);
								}
							 }
							 isAllFileUploaded = true;
							 DAORegistry.getInvoiceTicketAttachmentDAO().updateAll(barcodeAttachment);
						 }else{
							 DAORegistry.getInvoiceTicketAttachmentDAO().deleteAll(barcodeAttachment);
							 returnMessage = "PO Mapped, Error while Uploading  ticket file to API server and sending email to customer.";
							 
							 InvoiceAudit audit = new InvoiceAudit(invoice);
							 audit.setAction(InvoiceAuditAction.REAL_TICKET_UPLOADED);
							 audit.setNote(returnMessage);
							 audit.setCreatedDate(new Date());
							 audit.setCreateBy(userName);
							 DAORegistry.getInvoiceAuditDAO().save(audit);
							 error.setDescription(returnMessage);
							 genericResponseDTO.setError(error);
							 genericResponseDTO.setStatus(0);
							 return genericResponseDTO;
						 }
					 }
					 invoice.setIsRealTixUploaded("Yes");
					 invoice.setStatus(InvoiceStatus.Completed);
					 DAORegistry.getInvoiceDAO().saveOrUpdate(invoice);
				 }else if(shipMethod!= null && (shipMethod.getId().equals(13) || shipMethod.getId() == 13)){
					 Map<String,String> requestMap = Util.getParameterMap(request);
					 requestMap.put("orderId",String.valueOf(invoice.getCustomerOrderId()));
					 requestMap.put("fileType",FileType.OTHER.toString());
					 requestMap.put("attachTicket","false");
					 
					 String data = Util.getObject(requestMap, com.rtw.tmat.utils.Constants.BASE_URL + com.rtw.tmat.utils.Constants.TICKET_DOWNLOAD_NOTIFICATION);
					 Gson gson = new Gson();		
					 JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
					 TicketDownloadNotification ticketDownload = gson.fromJson(((JsonObject)jsonObject.get("ticketDownloadNotification")), TicketDownloadNotification.class);
					 
					 if(ticketDownload!=null && ticketDownload.getStatus()==1){
						 returnMessage = "PO Mapped and Flash Seat Details email sent to customer";
						 
						 InvoiceAudit audit = new InvoiceAudit(invoice);
						 audit.setAction(InvoiceAuditAction.REAL_TICKET_UPLOADED);
						 audit.setNote(returnMessage);
						 audit.setCreatedDate(new Date());
						 audit.setCreateBy(userName);
						 DAORegistry.getInvoiceAuditDAO().save(audit);
					 }else{
						 returnMessage = "PO Mapped, Error while sending Flash Seat Details email to customer.";
						 
						 InvoiceAudit audit = new InvoiceAudit(invoice);
						 audit.setAction(InvoiceAuditAction.REAL_TICKET_UPLOADED);
						 audit.setNote(returnMessage);
						 audit.setCreatedDate(new Date());
						 audit.setCreateBy(userName);
						 DAORegistry.getInvoiceAuditDAO().save(audit);
						 genericResponseDTO.setMessage(returnMessage);
						 genericResponseDTO.setStatus(0);
						 return genericResponseDTO;
					 }
					 
					 invoice.setIsRealTixUploaded("Yes");
					 invoice.setStatus(InvoiceStatus.Completed);
					 DAORegistry.getInvoiceDAO().saveOrUpdate(invoice);
				 }
				 status = true;

				 genericResponseDTO.setMessage(returnMessage);
				 genericResponseDTO.setStatus(1);
				 
				 
				//Tracking User Action
				String userActionMsg = returnMessage;
				Util.userActionAudit(request, invoice.getId(), userActionMsg);
				
			  }else if(action != null && action.equals("clearMapping")){
				  
				  String poNoStr = "";
				  String fileNamesStr = "";
				  
				  List<InvoiceTicketAttachment> invoiceAttachments = DAORegistry.getInvoiceTicketAttachmentDAO().getTicketAttachmentByInvoiceId(invoice.getId());
				  if(invoiceAttachments!=null && !invoiceAttachments.isEmpty()){
					 File file = null;
					 for(InvoiceTicketAttachment attachment:invoiceAttachments){
						 file = new File(attachment.getFilePath());
						 if(file.exists()){
							 fileNamesStr = fileNamesStr + attachment.getfName()+",";
							 file.delete();
						 }
					 }
					 DAORegistry.getInvoiceTicketAttachmentDAO().deleteAll(invoiceAttachments);
				  }
				
				 invoice.setLastUpdated(new Date());
				 if(expDeliveryDate!=null && !expDeliveryDate.isEmpty()){
					 invoice.setExpDeliveryDate(format.parse(expDeliveryDate));
				 }
				 invoice.setStatus(InvoiceStatus.Outstanding);
				 invoice.setIsRealTixUploaded("No");
				 invoice.setRealTixMap("No");
				 invoice.setRealTixDelivered(false);
				 
				 CustomerOrder order = DAORegistry.getCustomerOrderDAO().get(invoice.getCustomerOrderId());
				 order.setActualSection(null);
				 order.setActualSeat(null);
				 order.setRow(null);
				 
				 List<CategoryTicket> oldMappedCategoryTicket = DAORegistry.getCategoryTicketDAO().getMappedCategoryTicketByInvoiceId(invId);
				 List<Ticket> oldMappedTicket = DAORegistry.getTicketDAO().getMappedTicketsByInvoiceId(invId);
				 List<OrderTicketGroupDetails> oldOrderTicketGroupDetails = DAORegistry.getOrderTicketGroupDetailsDAO().getOrderTicketGroupDetailsByOrderId(order.getId());
				 
				 if(oldMappedCategoryTicket!=null){
					 for(CategoryTicket catTicket:oldMappedCategoryTicket) {
						catTicket.setTicketId(null);
					}
					DAORegistry.getCategoryTicketDAO().updateAll(oldMappedCategoryTicket);
				 }
				 if(oldMappedTicket!=null){
					 for(Ticket ticket:oldMappedTicket) {
						 ticket.setInvoiceId(null);
						 ticket.setTicketStatus(TicketStatus.ACTIVE);
						 if(!poNoStr.contains(String.valueOf(ticket.getPoId()))){
							 poNoStr = poNoStr + ticket.getPoId()+",";
						 }
					}
					DAORegistry.getTicketDAO().updateAll(oldMappedTicket);
				 }
				 
				 DAORegistry.getCustomerOrderDAO().update(order);
				 DAORegistry.getInvoiceDAO().saveOrUpdate(invoice);
				 DAORegistry.getOrderTicketGroupDetailsDAO().deleteAll(oldOrderTicketGroupDetails);
				 
				 if(poNoStr!=null && !poNoStr.isEmpty()){
					 InvoiceAudit audit = new InvoiceAudit(invoice);
					 audit.setAction(InvoiceAuditAction.REAL_TICKET_MAPPING_REMOVED);
					 audit.setNote("PO Mappings removed, PO No.:"+poNoStr);
					 audit.setCreatedDate(new Date());
					 audit.setCreateBy(userName);
					 DAORegistry.getInvoiceAuditDAO().save(audit);
				 }
				 if(!invoiceAttachments.isEmpty()){
					 InvoiceAudit uploadAudit = new InvoiceAudit(invoice);
					 uploadAudit.setAction(InvoiceAuditAction.REAL_TICKET_REMOVED);
					 uploadAudit.setNote("Tickets files are removed, Files are :"+fileNamesStr);
					 uploadAudit.setCreatedDate(new Date());
					 uploadAudit.setCreateBy(userName);
					 DAORegistry.getInvoiceAuditDAO().save(uploadAudit);
				 }
				 returnMessage = "PO UnMapped and all uploaded ticket files removed successfully.";
				 status = true;
				 
				 genericResponseDTO.setMessage(returnMessage);
				 genericResponseDTO.setStatus(1);
				 
				//Tracking User Action
				String userActionMsg = returnMessage;
				Util.userActionAudit(request, invoice.getId(), userActionMsg);
				
			  }else if(action != null && action.equals("voidOpenOrder")){
					
					if (!invoice.getStatus().equals(InvoiceStatus.Voided)) {
						String refundTypes = request.getParameter("refundType");
						String refundAmountStr = request.getParameter("refundAmount");
						String walletCCAmtStr = request.getParameter("walletCCAmount");
						String walletAmtStr = request.getParameter("walletAmount");
						String rewardPointsStr = request.getParameter("rewardPoint");
						
						Double refndAmount=0.00;
						Double creditWalletAmount=0.00;
						Double creditAmount=0.00;
						Double rewardPoints=0.00;
						Double primaryAmountToDeduct=0.0;
						Double secondaryAmoutToDeduct=0.0;
						Double thirdAmountToDeduct=0.0;
						
						if(refundAmountStr!=null && !refundAmountStr.isEmpty()){
							refndAmount = Double.parseDouble(refundAmountStr);
						}
						if(walletCCAmtStr!=null && !walletCCAmtStr.isEmpty()){
							creditWalletAmount = Double.parseDouble(walletCCAmtStr);
						}
						if(walletAmtStr!=null && !walletAmtStr.isEmpty()){
							creditAmount = Double.parseDouble(walletAmtStr);
						}
						if(rewardPointsStr!=null && !rewardPointsStr.isEmpty()){
							rewardPoints = Double.parseDouble(rewardPointsStr);
						}
						CustomerOrder order = DAORegistry.getCustomerOrderDAO().get(invoice.getCustomerOrderId());
						if(order == null){
							System.err.println("Customer order details not found in system.");
							error.setDescription("Customer order details not found in system.");
							genericResponseDTO.setError(error);
							genericResponseDTO.setStatus(0);
							return genericResponseDTO;
						}
						Customer customer = DAORegistry.getCustomerDAO().get(order.getCustomerId());
						CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(order.getCustomerId());						
						CustomerLoyaltyHistory customerLoyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyByOrderId(order.getId());
						InvoiceRefund invoiceRefund=null;
						if(refundTypes.equalsIgnoreCase("NOREFUND")){
							Util.voidInvoice(invoice, order, null, userName);
							
							returnMessage = "Invoice Voided Successfully.";
							//status = true;
							genericResponseDTO.setMessage(returnMessage);
							genericResponseDTO.setStatus(1);
							
							//Tracking User Action
							String userActionMsg = returnMessage;
							Util.userActionAudit(request, invoice.getId(), userActionMsg);
						}else if(refundTypes.equalsIgnoreCase("PARTIALREFUND")){
							if(order != null && (order.getPrimaryPaymentMethod().equals(PaymentMethod.CREDITCARD) || order.getPrimaryPaymentMethod().equals(PaymentMethod.PAYPAL)
									|| order.getPrimaryPaymentMethod().equals(PaymentMethod.GOOGLEPAY) || order.getPrimaryPaymentMethod().equals(PaymentMethod.IPAY))){
								if((refndAmount+creditWalletAmount) <= invoice.getInvoiceTotal() && 
										(refndAmount+creditWalletAmount) > 0){
									
									if(refndAmount > 0){
										String refundType = "";
										if(order.getPrimaryPaymentMethod().equals(PaymentMethod.CREDITCARD) || order.getPrimaryPaymentMethod().equals(PaymentMethod.GOOGLEPAY) 
												|| order.getPrimaryPaymentMethod().equals(PaymentMethod.IPAY)){
											refundType = PaymentMethod.CREDITCARD.toString();
										}else if(order.getPrimaryPaymentMethod().equals(PaymentMethod.PAYPAL)){
											refundType = PaymentMethod.PAYPAL.toString();
										}

										Map<String, String> paramMap = Util.getParameterMap(request);
										paramMap.put("orderId", String.valueOf(order.getId()));
										paramMap.put("refundType", refundType);
										paramMap.put("refundAmount",String.valueOf(Util.formatDoubleToTwoDecimalPoint(refndAmount)));
										
										String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.INVOICE_REFUND);
										Gson gson = Util.getGsonBuilder().create();	
										JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
										InvoiceRefundResponse invoiceRefundResponse = gson.fromJson(((JsonObject)jsonObject.get("invoiceRefundResponse")), InvoiceRefundResponse.class);
										
										if(invoiceRefundResponse == null){
											System.err.println("Invoice Refund API not available, it happens mostly when rewardthefan API is down.");
											error.setDescription("Invoice Refund API not available, it happens mostly when rewardthefan API is down.");
											genericResponseDTO.setError(error);
											genericResponseDTO.setStatus(0);
											return genericResponseDTO;
										}else if(invoiceRefundResponse.getStatus()==1){
											invoiceRefund = invoiceRefundResponse.getInvoiceRefund();
											if(invoiceRefund != null){
												primaryAmountToDeduct = primaryAmountToDeduct + refndAmount;
											}
										}else{
											returnMessage = invoiceRefundResponse.getError().getDescription();
										}
									}
									if(creditWalletAmount > 0){
										Map<String, String> paramMap = Util.getParameterMap(request);
										paramMap.put("orderId", String.valueOf(order.getId()));
										paramMap.put("transactionAmount",String.valueOf(creditWalletAmount));
										paramMap.put("customerId",String.valueOf(customer.getId()));
										paramMap.put("transactionType","CREDIT");
										paramMap.put("userName",userName);
										
										String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.CUSTOMER_WALLET_TRANSACTION);
										Gson gson = Util.getGsonBuilder().create();	
										JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
										WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
										
										if(walletTransaction == null){
											System.err.println("Wallet Transaction API not available, it happens mostly when rewardthefan API is down.");
											error.setDescription("Wallet Transaction API not available, it happens mostly when rewardthefan API is down.");
											genericResponseDTO.setError(error);
											genericResponseDTO.setStatus(0);
											return genericResponseDTO;
										}else if(walletTransaction.getStatus() == 1){
											primaryAmountToDeduct = primaryAmountToDeduct + creditWalletAmount;
										}else{
											returnMessage = walletTransaction.getError().getDescription();
										}
									}
								}							
							}
							else if(order != null && order.getPrimaryPaymentMethod().equals(PaymentMethod.CUSTOMER_WALLET)){
								if(order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
										|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY)
										|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.IPAY)){
									if((refndAmount+creditWalletAmount+creditAmount) <= invoice.getInvoiceTotal() && 
											(refndAmount+creditWalletAmount+creditAmount) > 0){
										
										if(refndAmount > 0){
											String refundType = "";
											if(order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
													|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY)
													|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.IPAY)){
												refundType = PaymentMethod.CREDITCARD.toString();
											}

											Map<String, String> paramMap = Util.getParameterMap(request);
											paramMap.put("orderId", String.valueOf(order.getId()));
											paramMap.put("refundType", refundType);
											paramMap.put("refundAmount",String.valueOf(Util.formatDoubleToTwoDecimalPoint(refndAmount)));
											
											String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.INVOICE_REFUND);
											Gson gson = Util.getGsonBuilder().create();	
											JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
											InvoiceRefundResponse invoiceRefundResponse = gson.fromJson(((JsonObject)jsonObject.get("invoiceRefundResponse")), InvoiceRefundResponse.class);
											
											if(invoiceRefundResponse == null){
												System.err.println("Invoice Refund API not available, it happens mostly when rewardthefan API is down.");
												error.setDescription("Invoice Refund API not available, it happens mostly when rewardthefan API is down.");
												genericResponseDTO.setError(error);
												genericResponseDTO.setStatus(0);
												return genericResponseDTO;
											}else if(invoiceRefundResponse.getStatus()==1){
												invoiceRefund = invoiceRefundResponse.getInvoiceRefund();
												if(invoiceRefund != null){
													secondaryAmoutToDeduct = secondaryAmoutToDeduct + refndAmount;
												}
											}else{
												returnMessage = invoiceRefundResponse.getError().getDescription();
											}
										}
										if(creditWalletAmount > 0){
											Map<String, String> paramMap = Util.getParameterMap(request);
											paramMap.put("orderId", String.valueOf(order.getId()));
											paramMap.put("transactionAmount",String.valueOf(creditWalletAmount));
											paramMap.put("customerId",String.valueOf(customer.getId()));
											paramMap.put("transactionType","CREDIT");
											paramMap.put("userName",userName);
											
											String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.CUSTOMER_WALLET_TRANSACTION);
											Gson gson = Util.getGsonBuilder().create();	
											JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
											WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
											
											if(walletTransaction == null){
												System.err.println("Wallet Transaction API not available, it happens mostly when rewardthefan API is down.");
												error.setDescription("Wallet Transaction API not available, it happens mostly when rewardthefan API is down.");
												genericResponseDTO.setError(error);
												genericResponseDTO.setStatus(0);
												return genericResponseDTO;
											}else if(walletTransaction.getStatus() == 1){
												secondaryAmoutToDeduct = secondaryAmoutToDeduct + creditWalletAmount;
											}else{
												returnMessage = walletTransaction.getError().getDescription();
											}
										}
										if(creditAmount > 0){
											Map<String, String> paramMap = Util.getParameterMap(request);
											paramMap.put("orderId", String.valueOf(order.getId()));
											paramMap.put("transactionAmount",String.valueOf(creditAmount));
											paramMap.put("customerId",String.valueOf(customer.getId()));
											paramMap.put("transactionType","CREDIT");
											paramMap.put("userName",userName);
											
											String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.CUSTOMER_WALLET_TRANSACTION);
											Gson gson = Util.getGsonBuilder().create();	
											JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
											WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
											
											if(walletTransaction == null){
												System.err.println("Wallet Transaction API not available, it happens mostly when rewardthefan API is down.");
												error.setDescription("Wallet Transaction API not available, it happens mostly when rewardthefan API is down.");
												genericResponseDTO.setError(error);
												genericResponseDTO.setStatus(0);
												return genericResponseDTO;
											}else if(walletTransaction.getStatus() == 1){
												primaryAmountToDeduct = primaryAmountToDeduct + creditAmount;
											}else{
												returnMessage = walletTransaction.getError().getDescription();
											}
										}
									}
								}
								else{
									if(creditAmount > 0){
										Map<String, String> paramMap = Util.getParameterMap(request);
										paramMap.put("orderId", String.valueOf(order.getId()));
										paramMap.put("transactionAmount",String.valueOf(creditAmount));
										paramMap.put("customerId",String.valueOf(customer.getId()));
										paramMap.put("transactionType","CREDIT");
										paramMap.put("userName",userName);
										
										String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.CUSTOMER_WALLET_TRANSACTION);
										Gson gson = Util.getGsonBuilder().create();	
										JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
										WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
										
										if(walletTransaction == null){
											System.err.println("Wallet Transaction API not available, it happens mostly when rewardthefan API is down.");
											error.setDescription("Wallet Transaction API not available, it happens mostly when rewardthefan API is down.");
											genericResponseDTO.setError(error);
											genericResponseDTO.setStatus(0);
											return genericResponseDTO;
										}else if(walletTransaction.getStatus() == 1){
											primaryAmountToDeduct = primaryAmountToDeduct + creditAmount;
										}else{
											returnMessage = walletTransaction.getError().getDescription();
										}
									}
								}
							}
							else if(order != null && order.getPrimaryPaymentMethod().equals(PaymentMethod.FULL_REWARDS)){
								if(rewardPoints <= invoice.getInvoiceTotal() && rewardPoints > 0){
									customerLoyalty.setActivePoints(customerLoyalty.getActivePoints()+rewardPoints);
									customerLoyalty.setRevertedSpentPoints(customerLoyalty.getRevertedSpentPoints() + rewardPoints);
//									if(customerLoyaltyHistory!=null){
//										customerLoyaltyHistory.setRewardStatus(RewardStatus.VOIDED);
//										DAORegistry.getCustomerLoyaltyHistoryDAO().update(customerLoyaltyHistory);
//									}
									DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
									primaryAmountToDeduct = primaryAmountToDeduct + rewardPoints ;
								}
							}
							else if(order != null && order.getPrimaryPaymentMethod().equals(PaymentMethod.PARTIAL_REWARDS)){
								if(order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.PAYPAL)
										|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.IPAY)){
									if((refndAmount+creditWalletAmount+rewardPoints) <= invoice.getInvoiceTotal() && 
											(refndAmount+creditWalletAmount+rewardPoints) > 0){

										if(rewardPoints <= invoice.getInvoiceTotal()){
											customerLoyalty.setActivePoints(customerLoyalty.getActivePoints()+rewardPoints);
											customerLoyalty.setRevertedSpentPoints(customerLoyalty.getRevertedSpentPoints() + rewardPoints);
											if(invoiceRefund!=null){
												invoiceRefund.setRevertedUsedRewardPoints(rewardPoints);
											}
											primaryAmountToDeduct = primaryAmountToDeduct + rewardPoints ;
										}
										if(refndAmount > 0){
											String refundType = "";
											if(order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
													|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY)
													|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.IPAY)){
												refundType = PaymentMethod.CREDITCARD.toString();
											}
											Map<String, String> paramMap = Util.getParameterMap(request);
											paramMap.put("orderId", String.valueOf(order.getId()));
											paramMap.put("refundType", refundType);
											paramMap.put("refundAmount",String.valueOf(Util.formatDoubleToTwoDecimalPoint(refndAmount)));
											
											String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.INVOICE_REFUND);
											Gson gson = Util.getGsonBuilder().create();	
											JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
											InvoiceRefundResponse invoiceRefundResponse = gson.fromJson(((JsonObject)jsonObject.get("invoiceRefundResponse")), InvoiceRefundResponse.class);
											
											if(invoiceRefundResponse == null){
												System.err.println("Invoice Refund API not available, it happens mostly when rewardthefan API is down.");
												error.setDescription("Invoice Refund API not available, it happens mostly when rewardthefan API is down.");
												genericResponseDTO.setError(error);
												genericResponseDTO.setStatus(0);
												return genericResponseDTO;
											}else if(invoiceRefundResponse.getStatus()==1){
												invoiceRefund = invoiceRefundResponse.getInvoiceRefund();
												if(invoiceRefund != null){
													secondaryAmoutToDeduct = secondaryAmoutToDeduct + refndAmount;
												}
											}else{
												returnMessage = invoiceRefundResponse.getError().getDescription();
											}
										}
										if(creditWalletAmount > 0){
											Map<String, String> paramMap = Util.getParameterMap(request);
											paramMap.put("orderId", String.valueOf(order.getId()));
											paramMap.put("transactionAmount",String.valueOf(creditWalletAmount));
											paramMap.put("customerId",String.valueOf(customer.getId()));
											paramMap.put("transactionType","CREDIT");
											paramMap.put("userName",userName);
											
											String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.CUSTOMER_WALLET_TRANSACTION);
											Gson gson = Util.getGsonBuilder().create();	
											JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
											WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
											
											if(walletTransaction == null){
												System.err.println("Wallet Transaction API not available, it happens mostly when rewardthefan API is down.");
												error.setDescription("Wallet Transaction API not available, it happens mostly when rewardthefan API is down.");
												genericResponseDTO.setError(error);
												genericResponseDTO.setStatus(0);
												return genericResponseDTO;
											}else if(walletTransaction.getStatus() == 1){
												secondaryAmoutToDeduct = secondaryAmoutToDeduct + creditWalletAmount;
											}else{
												returnMessage = walletTransaction.getError().getDescription();
											}
										}
									}
								}
								else if(order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CUSTOMER_WALLET) && 
										(order.getThirdPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
												|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY)
												|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.IPAY))){
									if((refndAmount+creditWalletAmount+creditAmount+rewardPoints) <= invoice.getInvoiceTotal() && 
											(refndAmount+creditWalletAmount+creditAmount+rewardPoints) > 0){

										if(rewardPoints <= invoice.getInvoiceTotal()){
											customerLoyalty.setActivePoints(customerLoyalty.getActivePoints()+rewardPoints);
											customerLoyalty.setRevertedSpentPoints(customerLoyalty.getRevertedSpentPoints() + rewardPoints);
											if(invoiceRefund!=null){
												invoiceRefund.setRevertedUsedRewardPoints(rewardPoints);
											}
											primaryAmountToDeduct = primaryAmountToDeduct + rewardPoints ;
										}
										if(creditAmount > 0){
											Map<String, String> paramMap = Util.getParameterMap(request);
											paramMap.put("orderId", String.valueOf(order.getId()));
											paramMap.put("transactionAmount",String.valueOf(creditAmount));
											paramMap.put("customerId",String.valueOf(customer.getId()));
											paramMap.put("transactionType","CREDIT");
											paramMap.put("userName",userName);
											
											String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.CUSTOMER_WALLET_TRANSACTION);
											Gson gson = Util.getGsonBuilder().create();	
											JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
											WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
											
											if(walletTransaction == null){
												System.err.println("Wallet Transaction API not available, it happens mostly when rewardthefan API is down.");
												error.setDescription("Wallet Transaction API not available, it happens mostly when rewardthefan API is down.");
												genericResponseDTO.setError(error);
												genericResponseDTO.setStatus(0);
												return genericResponseDTO;
											}else if(walletTransaction.getStatus() == 1){
												secondaryAmoutToDeduct = secondaryAmoutToDeduct + creditAmount;
											}else{
												returnMessage = walletTransaction.getError().getDescription();
											}
										}
										if(refndAmount > 0){
											String refundType = "";
											if(order.getThirdPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
													|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY)
													|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.IPAY)){
												refundType = PaymentMethod.CREDITCARD.toString();
											}

											Map<String, String> paramMap = Util.getParameterMap(request);
											paramMap.put("orderId", String.valueOf(order.getId()));
											paramMap.put("refundType", refundType);
											paramMap.put("refundAmount",String.valueOf(Util.formatDoubleToTwoDecimalPoint(refndAmount)));
											
											String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.INVOICE_REFUND);
											Gson gson = Util.getGsonBuilder().create();	
											JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
											InvoiceRefundResponse invoiceRefundResponse = gson.fromJson(((JsonObject)jsonObject.get("invoiceRefundResponse")), InvoiceRefundResponse.class);
											
											if(invoiceRefundResponse == null){
												System.err.println("Invoice Refund API not available, it happens mostly when rewardthefan API is down.");
												error.setDescription("Invoice Refund API not available, it happens mostly when rewardthefan API is down.");
												genericResponseDTO.setError(error);
												genericResponseDTO.setStatus(0);
												return genericResponseDTO;
											}else if(invoiceRefundResponse.getStatus()==1){
												invoiceRefund = invoiceRefundResponse.getInvoiceRefund();
												if(invoiceRefund != null){
													thirdAmountToDeduct = thirdAmountToDeduct + refndAmount;
												}
											}else{
												returnMessage = invoiceRefundResponse.getError().getDescription();
											}
										}
										if(creditWalletAmount > 0){
											Map<String, String> paramMap = Util.getParameterMap(request);
											paramMap.put("orderId", String.valueOf(order.getId()));
											paramMap.put("transactionAmount",String.valueOf(creditWalletAmount));
											paramMap.put("customerId",String.valueOf(customer.getId()));
											paramMap.put("transactionType","CREDIT");
											paramMap.put("userName",userName);
											
											String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.CUSTOMER_WALLET_TRANSACTION);
											Gson gson = Util.getGsonBuilder().create();	
											JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
											WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
											
											if(walletTransaction == null){
												System.err.println("Wallet Transaction API not available, it happens mostly when rewardthefan API is down.");
												error.setDescription("Wallet Transaction API not available, it happens mostly when rewardthefan API is down.");
												genericResponseDTO.setError(error);
												genericResponseDTO.setStatus(0);
												return genericResponseDTO;
											}else if(walletTransaction.getStatus() == 1){
												thirdAmountToDeduct = thirdAmountToDeduct + creditWalletAmount;
											}else{
												returnMessage = walletTransaction.getError().getDescription();
											}
										}
									}
								}
								else if(order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CUSTOMER_WALLET)){
									if((creditAmount+rewardPoints) <= invoice.getInvoiceTotal() && 
											(creditAmount+rewardPoints) > 0){
										
										if(rewardPoints <= invoice.getInvoiceTotal()){
											customerLoyalty.setActivePoints(customerLoyalty.getActivePoints()+rewardPoints);
											customerLoyalty.setRevertedSpentPoints(customerLoyalty.getRevertedSpentPoints() + rewardPoints);
											if(invoiceRefund!=null){
												invoiceRefund.setRevertedUsedRewardPoints(rewardPoints);
											}
											primaryAmountToDeduct = primaryAmountToDeduct + rewardPoints ;
										}
										if(creditAmount > 0){
											Map<String, String> paramMap = Util.getParameterMap(request);
											paramMap.put("orderId", String.valueOf(order.getId()));
											paramMap.put("transactionAmount",String.valueOf(creditAmount));
											paramMap.put("customerId",String.valueOf(customer.getId()));
											paramMap.put("transactionType","CREDIT");
											paramMap.put("userName",userName);
											
											String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.CUSTOMER_WALLET_TRANSACTION);
											Gson gson = Util.getGsonBuilder().create();	
											JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
											WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
											
											if(walletTransaction == null){
												System.err.println("Wallet Transaction API not available, it happens mostly when rewardthefan API is down.");
												error.setDescription("Wallet Transaction API not available, it happens mostly when rewardthefan API is down.");
												genericResponseDTO.setError(error);
												genericResponseDTO.setStatus(0);
												return genericResponseDTO;
											}else if(walletTransaction.getStatus() == 1){
												secondaryAmoutToDeduct = secondaryAmoutToDeduct + creditAmount;
											}else{
												returnMessage = walletTransaction.getError().getDescription();
											}
										}
									}
								}
							}
							if(returnMessage==null || returnMessage.isEmpty()){
								Date today = new Date();
								order.setPrimaryRefundAmount(order.getPrimaryRefundAmount()!=null?order.getPrimaryRefundAmount()+primaryAmountToDeduct:primaryAmountToDeduct);
								order.setSecondaryRefundAmount(order.getSecondaryRefundAmount()!=null?order.getSecondaryRefundAmount()+secondaryAmoutToDeduct:secondaryAmoutToDeduct);
								order.setThirdRefundAmount(order.getThirdRefundAmount()!=null?order.getThirdRefundAmount()+thirdAmountToDeduct:thirdAmountToDeduct);
//								order.setOrderTotal(order.getOrderTotal()-(primaryAmountToDeduct + secondaryAmoutToDeduct + thirdAmountToDeduct));							
//								
//								if(order.getEventDate().compareTo(today) >= 0){
//									customerLoyalty.setPendingPoints((customerLoyalty.getPendingPoints()-((primaryAmountToDeduct+secondaryAmoutToDeduct+thirdAmountToDeduct)%10)));
//								}else{
//									customerLoyalty.setActivePoints((customerLoyalty.getActivePoints()-((primaryAmountToDeduct+secondaryAmoutToDeduct+thirdAmountToDeduct)%10)));
//								}
								
								invoice.setInvoiceTotal(invoice.getInvoiceTotal()-(primaryAmountToDeduct + secondaryAmoutToDeduct + thirdAmountToDeduct));
								invoice.setLastUpdated(today);
								invoice.setLastUpdatedBy(userName);
								invoice.setRefundAmount(invoice.getRefundAmount()!=null?invoice.getRefundAmount()+(primaryAmountToDeduct + secondaryAmoutToDeduct + thirdAmountToDeduct)
										:(primaryAmountToDeduct + secondaryAmoutToDeduct + thirdAmountToDeduct));
//								if(invoiceRefund!=null){
//									invoiceRefund.setOrderId(order.getId());
//									DAORegistry.getInvoiceRefundDAO().save(invoiceRefund);
//								}
								DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
								DAORegistry.getCustomerOrderDAO().update(order);
								DAORegistry.getInvoiceDAO().update(invoice);
								
								Util.voidInvoice(invoice, order, invoiceRefund, userName);
								returnMessage = "Invoice Voided Successfully.";
								//status = true;
								genericResponseDTO.setMessage(returnMessage);
								genericResponseDTO.setStatus(1);
								
								//Tracking User Action
								String userActionMsg = returnMessage;
								Util.userActionAudit(request, invoice.getId(), userActionMsg);
							}else{								
								System.err.println(returnMessage);
								error.setDescription(returnMessage);
								genericResponseDTO.setError(error);
								genericResponseDTO.setStatus(0);
								return genericResponseDTO;
							}
//							String logMsg= "";							
//							if(refundAmountStr!=null && !refundAmountStr.isEmpty()){
//								logMsg =logMsg +"Refunded($"+refundAmountStr+"),";
//							}
//							if(walletCCAmtStr!=null && !walletCCAmtStr.isEmpty()){
//								logMsg =logMsg +"Credited($"+walletCCAmtStr+"),";
//							}
//							if(walletAmtStr!=null && !walletAmtStr.isEmpty()){
//								logMsg =logMsg +"Credited($"+walletAmtStr+"),";	
//							}						
//							if(rewardPointsStr!=null && !rewardPointsStr.isEmpty()){
//								logMsg =logMsg +"Reverted Reward Points($"+rewardPointsStr+")";
//							}
//							
//							InvoiceAudit audit = new InvoiceAudit(invoice);
//							audit.setAction(InvoiceAuditAction.INVOICE_REFUNDED);
//							audit.setNote("Invoice is "+logMsg);
//							audit.setCreatedDate(new Date());
//							audit.setCreateBy(userName);
//							DAORegistry.getInvoiceAuditDAO().save(audit);
							
						}
						else if(refundTypes.equalsIgnoreCase("FULLREFUND")){
							if(order!=null && (order.getPrimaryPaymentMethod().equals(PaymentMethod.CREDITCARD) || order.getPrimaryPaymentMethod().equals(PaymentMethod.GOOGLEPAY)
									|| order.getPrimaryPaymentMethod().equals(PaymentMethod.IPAY) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
									|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.IPAY)
									|| (order.getThirdPaymentMethod() != null && (order.getThirdPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
											||order.getThirdPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY) || order.getThirdPaymentMethod().equals(PartialPaymentMethod.IPAY))))){
								Double refundAmount = 0.0;
								if(order.getPrimaryPaymentMethod().equals(PaymentMethod.CREDITCARD) || order.getPrimaryPaymentMethod().equals(PaymentMethod.IPAY) 
										|| order.getPrimaryPaymentMethod().equals(PaymentMethod.GOOGLEPAY)){
									primaryAmountToDeduct = order.getPrimaryAvailableAmt();
									refundAmount = order.getPrimaryAvailableAmt();
								}else if(order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.IPAY) 
										|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY)){
									secondaryAmoutToDeduct = order.getSecondaryAvailableAmt();
									refundAmount = order.getSecondaryAvailableAmt();
								}else if(order.getThirdPaymentMethod() != null && (order.getThirdPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
										|| order.getThirdPaymentMethod().equals(PartialPaymentMethod.IPAY) || order.getThirdPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY))){
									thirdAmountToDeduct = order.getThirdAvailableAmt();
									refundAmount = order.getThirdAvailableAmt();
								}
								if(refundAmount > 0){
									Map<String, String> paramMap = Util.getParameterMap(request);
									paramMap.put("orderId", String.valueOf(order.getId()));
									paramMap.put("refundType", PaymentMethod.CREDITCARD.toString());
									paramMap.put("refundAmount",String.valueOf(Util.formatDoubleToTwoDecimalPoint(refundAmount)));
									
									String data = Util.getObject(paramMap, sharedProperty.getApiUrl() + com.rtw.tmat.utils.Constants.INVOICE_REFUND);
									Gson gson = Util.getGsonBuilder().create();
									JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
									InvoiceRefundResponse invoiceRefundResponse = gson.fromJson(((JsonObject) jsonObject.get("invoiceRefundResponse")), InvoiceRefundResponse.class);
									
									if(invoiceRefundResponse == null){
										System.err.println("Invoice Refund API not available, it happens mostly when rewardthefan API is down.");
										error.setDescription("Invoice Refund API not available, it happens mostly when rewardthefan API is down.");
										genericResponseDTO.setError(error);
										genericResponseDTO.setStatus(0);
										return genericResponseDTO;
									}else if (invoiceRefundResponse.getStatus() == 1) {
										invoiceRefund = invoiceRefundResponse.getInvoiceRefund();
									} else {
										returnMessage = invoiceRefundResponse.getError().getDescription();
									}
								}
								
							}
							if(order!=null && (order.getPrimaryPaymentMethod().equals(PaymentMethod.PAYPAL) 
									|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.PAYPAL)
									|| (order.getThirdPaymentMethod() != null && order.getThirdPaymentMethod().equals(PartialPaymentMethod.PAYPAL)))){
								Double refundAmount = 0.0;
								if(order.getPrimaryPaymentMethod().equals(PaymentMethod.PAYPAL)){
									primaryAmountToDeduct = order.getPrimaryAvailableAmt();
									refundAmount = order.getPrimaryAvailableAmt();
								}else if(order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.PAYPAL)){
									secondaryAmoutToDeduct = order.getSecondaryAvailableAmt();
									refundAmount = order.getSecondaryAvailableAmt();
								}else if(order.getThirdPaymentMethod() != null && order.getThirdPaymentMethod().equals(PartialPaymentMethod.PAYPAL)){
									thirdAmountToDeduct = order.getThirdAvailableAmt();
									refundAmount = order.getThirdAvailableAmt();
								}
								if(refundAmount > 0){
									Map<String, String> paramMap = Util.getParameterMap(request);
									paramMap.put("orderId", String.valueOf(order.getId()));
									paramMap.put("refundType", PaymentMethod.PAYPAL.toString());
									paramMap.put("refundAmount",String.valueOf(Util.formatDoubleToTwoDecimalPoint(refundAmount)));
									
									String data = Util.getObject(paramMap, sharedProperty.getApiUrl() + com.rtw.tmat.utils.Constants.INVOICE_REFUND);
									Gson gson = Util.getGsonBuilder().create();
									JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
									InvoiceRefundResponse invoiceRefundResponse = gson.fromJson(((JsonObject) jsonObject.get("invoiceRefundResponse")), InvoiceRefundResponse.class);
									
									if(invoiceRefundResponse == null){
										System.err.println("Invoice Refund API not available, it happens mostly when rewardthefan API is down.");
										error.setDescription("Invoice Refund API not available, it happens mostly when rewardthefan API is down.");
										genericResponseDTO.setError(error);
										genericResponseDTO.setStatus(0);
										return genericResponseDTO;
									}else if (invoiceRefundResponse.getStatus() == 1) {
										invoiceRefund = invoiceRefundResponse.getInvoiceRefund();
									} else {
										returnMessage = invoiceRefundResponse.getError().getDescription();
									}
								}
								
							}
							if(order!=null && (order.getPrimaryPaymentMethod().equals(PaymentMethod.CUSTOMER_WALLET) 
									|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CUSTOMER_WALLET)
									|| (order.getThirdPaymentMethod() != null && order.getThirdPaymentMethod().equals(PartialPaymentMethod.CUSTOMER_WALLET)))){
								Double refundAmount = 0.00;
								if(order.getPrimaryPaymentMethod().equals(PaymentMethod.CUSTOMER_WALLET)){
									primaryAmountToDeduct = order.getPrimaryAvailableAmt();
									refundAmount = order.getPrimaryAvailableAmt();
								}else if(order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CUSTOMER_WALLET)){
									secondaryAmoutToDeduct = order.getSecondaryAvailableAmt();
									refundAmount = order.getSecondaryAvailableAmt();
								}else if(order.getThirdPaymentMethod() != null && order.getThirdPaymentMethod().equals(PartialPaymentMethod.CUSTOMER_WALLET)){
									thirdAmountToDeduct = order.getThirdAvailableAmt();
									refundAmount = order.getThirdAvailableAmt();
								}
								if(refundAmount > 0){
									Map<String, String> paramMap = Util.getParameterMap(request);
									paramMap.put("orderId", String.valueOf(order.getId()));
									paramMap.put("transactionAmount",String.valueOf(refundAmount));
									paramMap.put("customerId",String.valueOf(customer.getId()));
									paramMap.put("transactionType","CREDIT");
									paramMap.put("userName",userName);
									
									String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.CUSTOMER_WALLET_TRANSACTION);
									Gson gson = Util.getGsonBuilder().create();	
									JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
									WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
									
									if(walletTransaction == null){
										System.err.println("Wallet Transaction API not available, it happens mostly when rewardthefan API is down.");
										error.setDescription("Wallet Transaction API not available, it happens mostly when rewardthefan API is down.");
										genericResponseDTO.setError(error);
										genericResponseDTO.setStatus(0);
										return genericResponseDTO;
									}else if(walletTransaction.getStatus() == 1){
										//secondaryAmoutToDeduct = secondaryAmoutToDeduct + creditAmount;
									}else{
										returnMessage = walletTransaction.getError().getDescription();
									}
								}
								
							}
							if(order!=null && (order.getPrimaryPaymentMethod().equals(PaymentMethod.FULL_REWARDS) ||
									order.getPrimaryPaymentMethod().equals(PaymentMethod.PARTIAL_REWARDS))){
								primaryAmountToDeduct = order.getPrimaryAvailableAmt();
								customerLoyalty.setActivePoints(customerLoyalty.getActivePoints()+primaryAmountToDeduct);
								customerLoyalty.setRevertedSpentPoints(customerLoyalty.getRevertedSpentPoints() + primaryAmountToDeduct);
								if(customerLoyaltyHistory!=null){
									customerLoyaltyHistory.setRewardStatus(RewardStatus.VOIDED);
									DAORegistry.getCustomerLoyaltyHistoryDAO().update(customerLoyaltyHistory);
								}
								DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
								
							}
							if(returnMessage==null || returnMessage.isEmpty()){
								Date today = new Date();
								order.setPrimaryRefundAmount(order.getPrimaryRefundAmount() + primaryAmountToDeduct);
								order.setSecondaryRefundAmount(order.getSecondaryRefundAmount() + secondaryAmoutToDeduct);
								order.setThirdRefundAmount(order.getThirdRefundAmount() + thirdAmountToDeduct);							
								
								invoice.setInvoiceTotal(invoice.getInvoiceTotal()-(primaryAmountToDeduct + secondaryAmoutToDeduct + thirdAmountToDeduct));
								invoice.setRefundAmount(invoice.getRefundAmount()!=null?invoice.getRefundAmount()+(primaryAmountToDeduct + secondaryAmoutToDeduct + thirdAmountToDeduct)
										:(primaryAmountToDeduct + secondaryAmoutToDeduct + thirdAmountToDeduct));
								invoice.setLastUpdated(today);
								invoice.setLastUpdatedBy(userName);
								
								DAORegistry.getCustomerOrderDAO().update(order);
								DAORegistry.getInvoiceDAO().update(invoice);
								Util.voidInvoice(invoice, order, invoiceRefund, userName);
								
								returnMessage = "Invoice Voided Successfully.";
								//status = true;								
								genericResponseDTO.setMessage(returnMessage);
								genericResponseDTO.setStatus(1);
								
								//Tracking User Action
								String userActionMsg = returnMessage;
								Util.userActionAudit(request, invoice.getId(), userActionMsg);
								
							}else{
								System.err.println(returnMessage);
								error.setDescription(returnMessage);
								genericResponseDTO.setError(error);
								genericResponseDTO.setStatus(0);
								return genericResponseDTO;
							}
						}
					} else {
						returnMessage = "Invoice already voided.";
						System.err.println(returnMessage);
						error.setDescription(returnMessage);
						genericResponseDTO.setError(error);
						genericResponseDTO.setStatus(0);
						return genericResponseDTO;
					}	
					//jObj.put("msg", returnMessage);
					genericResponseDTO.setMessage(returnMessage);
			  }
			//}
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Updating Real Ticket in Invoice..");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
		//return "redirect:CreateRealTix?invoiceId="+invoiceId+"&msg="+returnMessage;
		//return "redirect:AddRealTicket?invoiceId="+invoiceId+"&orderId="+invoice.getCustomerOrderId()+"&msg="+returnMessage+"&status="+status;
	}
	
	public boolean uploadRealTicket(HttpServletRequest request,Map<String,Boolean> uploadFileMap){
		final String invoiceIdStr = request.getParameter("invoiceId");
		String eventNameStr = request.getParameter("eventName");
		String eventDateStr = request.getParameter("eventDate");
		String eventTimeStr = request.getParameter("eventTime");
		String sectionStr = request.getParameter("section");
		String rowStr = request.getParameter("row");
		String appendFileName = "";
		String seatStr =null;
		boolean isEticketUploaded = false;
		boolean isQrCodeUploaded = false;
		boolean isBarcodeUploaded = false;
		String fileOriginalName = null;
		
		if(eventNameStr != null && !eventNameStr.isEmpty()){
			eventNameStr = eventNameStr.replaceAll(":", "_");
			eventNameStr = eventNameStr.replaceAll(" ", "_");
			eventNameStr = eventNameStr.replaceAll("-", "_");
			eventNameStr = eventNameStr.replaceAll("/", "_");
			eventNameStr = eventNameStr.replaceAll("'", "");
			eventNameStr = eventNameStr.replaceAll("&", "_");
			eventNameStr = eventNameStr.replaceAll(",", "_");
			//eventNameStr = eventNameStr.replaceAll(".", "_");
			eventNameStr = eventNameStr.replaceAll(";", "_");
			appendFileName += eventNameStr+"_";
		}
		if(invoiceIdStr!=null && !invoiceIdStr.isEmpty()){
			appendFileName += invoiceIdStr+"_";
		}
		if(eventDateStr != null && !eventDateStr.isEmpty()){
			eventDateStr = eventDateStr.replaceAll("/", "_");
			eventDateStr = eventDateStr.replaceAll(" ", "_");
			appendFileName += eventDateStr+"_";
		}
		if(eventTimeStr != null && !eventTimeStr.isEmpty()){
			eventTimeStr = eventTimeStr.replaceAll(":", "_");
			eventTimeStr = eventTimeStr.replaceAll(" ", "_");
			appendFileName += eventTimeStr+"_";
		}
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		boolean flag=false;
		InvoiceTicketAttachment attachment;
		try {
			if(invoiceIdStr != null && !invoiceIdStr.isEmpty()){
				Map<String, String[]> requestParams = request.getParameterMap();
				List<InvoiceTicketAttachment> invoiceAttachment = null;
				List<Ticket> tickets = DAORegistry.getTicketDAO().getMappedTicketsByInvoiceId(Integer.parseInt(invoiceIdStr));
				String userName = request.getParameter("userName");
				Date now = new Date();
				boolean isUpdate = false;
				for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
					String key = entry.getKey();
					fileOriginalName = null;
					if(key.contains("eticketTable_")){
						Integer rowNumber = Integer.parseInt(request.getParameter("eticketTable_count"));
						int cnt=0;
						for(int i=1;i<=rowNumber;i++){
							cnt++;
							isUpdate=false;
							MultipartFile file = multipartRequest.getFile("eticket_"+i);
							if(file==null){
								continue;
							}
							if(file != null && file.getSize() > 0) {
								File newFile = null;
								fileOriginalName = file.getOriginalFilename();
								String ext = FilenameUtils.getExtension(file.getOriginalFilename());
								String fullFileName = "";
								if(tickets.size() > (i-1)){
									seatStr = tickets.get(i-1).getSeatNo().toString();
									TicketGroup ticGroup =  DAORegistry.getTicketGroupDAO().get(tickets.get(i-1).getTicketGroupId());
									rowStr = ticGroup.getRow();
									sectionStr =  ticGroup.getSection();
									fullFileName = appendFileName+sectionStr+"_"+rowStr+"_"+seatStr+"_eticket_"+cnt+"."+ext;
								}else{
									sectionStr = request.getParameter("section");
									fullFileName = appendFileName+sectionStr+"_eticket_"+cnt+"."+ext;
								}
								flag=true;
								invoiceAttachment = DAORegistry.getInvoiceTicketAttachmentDAO().getAttachmentByInvoiceAndFileType(Integer.parseInt(invoiceIdStr),FileType.ETICKET);
								for(InvoiceTicketAttachment attach : invoiceAttachment){
									if(attach.getPosition()!= null && attach.getPosition() == cnt){
										newFile = new File(attach.getFilePath());
										if(newFile.exists()){
											newFile.delete();
										}
										attach.setInvoiceId(Integer.parseInt(invoiceIdStr));
										attach.setFilePath(URLUtil.TICKET_DIRECTORY+fullFileName);
										attach.setfName(fullFileName);
										attach.setFileOriginalName(fileOriginalName);
										attach.setPosition(cnt);
										attach.setExtension(ext);
										attach.setType(FileType.ETICKET);
										attach.setUploadedBy(userName);
										attach.setUploadedDateTime(now);
										DAORegistry.getInvoiceTicketAttachmentDAO().update(attach);
										isUpdate=true;
										break;
									}
								}
								if(!isUpdate){
									attachment = new InvoiceTicketAttachment();
									attachment.setInvoiceId(Integer.parseInt(invoiceIdStr));
									attachment.setFilePath(URLUtil.TICKET_DIRECTORY+fullFileName);
									attachment.setfName(fullFileName);
									attachment.setFileOriginalName(fileOriginalName);
									attachment.setExtension(ext);
									attachment.setPosition(cnt);
									attachment.setType(FileType.ETICKET);
									attachment.setUploadedBy(userName);
									attachment.setUploadedDateTime(now);
									DAORegistry.getInvoiceTicketAttachmentDAO().save(attachment);
									
								}
								isUpdate=false;
								newFile = new File(URLUtil.TICKET_DIRECTORY+fullFileName);
								newFile.createNewFile();
								
								BufferedOutputStream stream = new BufferedOutputStream(
										new FileOutputStream(newFile));
						        FileCopyUtils.copy(file.getInputStream(), stream);
								stream.close();
								isEticketUploaded = true;
							}
							
						}
						
					}else if(key.contains("qrcodeTable_")){
						Integer rowNumber = Integer.parseInt(request.getParameter("qrcodeTable_count"));
						int cnt=0;
						for(int i=1;i<=rowNumber;i++){
							cnt++;
							isUpdate=false;
							MultipartFile file = multipartRequest.getFile("qrcode_"+i);
							if(file==null){
								continue;
							}
							if(file != null && file.getSize() > 0) {
								File newFile = null;
								fileOriginalName = file.getOriginalFilename();
								String ext = FilenameUtils.getExtension(file.getOriginalFilename());
								String fullFileName = "";
								if(tickets.size() > (i-1)){
									seatStr = tickets.get(i-1).getSeatNo().toString();
									TicketGroup ticGroup =  DAORegistry.getTicketGroupDAO().get(tickets.get(i-1).getTicketGroupId());
									rowStr = ticGroup.getRow();
									sectionStr =  ticGroup.getSection();
									fullFileName = appendFileName+sectionStr+"_"+rowStr+"_"+seatStr+"_mobileentry_"+cnt+"."+ext;
								}else{
									sectionStr = request.getParameter("section");
									fullFileName = appendFileName+sectionStr+"_mobileentry_"+cnt+"."+ext;
								}
								flag=true;
								invoiceAttachment = DAORegistry.getInvoiceTicketAttachmentDAO().getAttachmentByInvoiceAndFileType(Integer.parseInt(invoiceIdStr),FileType.QRCODE);
								for(InvoiceTicketAttachment attach : invoiceAttachment){
									if(attach.getPosition()!= null && attach.getPosition() == cnt){
										newFile = new File(attach.getFilePath());
										if(newFile.exists()){
											newFile.delete();
										}
										attach.setInvoiceId(Integer.parseInt(invoiceIdStr));
										attach.setFilePath(URLUtil.TICKET_DIRECTORY+fullFileName);
										attach.setfName(fullFileName);
										attach.setFileOriginalName(fileOriginalName);
										attach.setExtension(ext);
										attach.setPosition(cnt);
										attach.setType(FileType.QRCODE);
										attach.setUploadedBy(userName);
										attach.setUploadedDateTime(now);
										DAORegistry.getInvoiceTicketAttachmentDAO().update(attach);
										isUpdate=true;
										break;
									}
								}
								if(!isUpdate){
									attachment = new InvoiceTicketAttachment();
									attachment.setInvoiceId(Integer.parseInt(invoiceIdStr));
									attachment.setFilePath(URLUtil.TICKET_DIRECTORY+fullFileName);
									attachment.setfName(fullFileName);
									attachment.setFileOriginalName(fileOriginalName);
									attachment.setExtension(ext);
									attachment.setPosition(cnt);
									attachment.setType(FileType.QRCODE);
									attachment.setUploadedBy(userName);
									attachment.setUploadedDateTime(now);
									DAORegistry.getInvoiceTicketAttachmentDAO().save(attachment);
									
								}
								isUpdate=false;
								newFile = new File(URLUtil.TICKET_DIRECTORY+fullFileName);
								newFile.createNewFile();
								
								BufferedOutputStream stream = new BufferedOutputStream(
										new FileOutputStream(newFile));
						        FileCopyUtils.copy(file.getInputStream(), stream);
								stream.close();
								isQrCodeUploaded = true;
							}
							
						}
						
					}else if(key.contains("barcodeTable_")){
						Integer rowNumber = Integer.parseInt(request.getParameter("barcodeTable_count"));
						int cnt=0;
						for(int i=1;i<=rowNumber;i++){
							isUpdate=false;
							cnt++;
							MultipartFile file = multipartRequest.getFile("barcode_"+i);
							if(file==null){
								continue;
							}
							if(file != null && file.getSize() > 0) {
								File newFile = null;
								fileOriginalName = file.getOriginalFilename();
								String ext = FilenameUtils.getExtension(file.getOriginalFilename());
								String fullFileName = "";
								if(tickets.size() > (i-1)){
									seatStr = tickets.get(i-1).getSeatNo().toString();
									TicketGroup ticGroup =  DAORegistry.getTicketGroupDAO().get(tickets.get(i-1).getTicketGroupId());
									rowStr = ticGroup.getRow();
									sectionStr =  ticGroup.getSection();
									fullFileName = appendFileName+sectionStr+"_"+rowStr+"_"+seatStr+"_electronictransfer_"+cnt+"."+ext;
								}else{
									sectionStr = request.getParameter("section");
									fullFileName = appendFileName+sectionStr+"_electronictransfer_"+cnt+"."+ext;
								}
								flag=true;
								invoiceAttachment = DAORegistry.getInvoiceTicketAttachmentDAO().getAttachmentByInvoiceAndFileType(Integer.parseInt(invoiceIdStr),FileType.BARCODE);
								for(InvoiceTicketAttachment attach : invoiceAttachment){
									if(attach.getPosition()!= null &&  attach.getPosition() == cnt){
										newFile = new File(attach.getFilePath());
										if(newFile.exists()){
											newFile.delete();
										}
										attach.setInvoiceId(Integer.parseInt(invoiceIdStr));
										attach.setFilePath(URLUtil.TICKET_DIRECTORY+fullFileName);
										attach.setfName(fullFileName);
										attach.setFileOriginalName(fileOriginalName);
										attach.setExtension(ext);
										attach.setPosition(cnt);
										attach.setType(FileType.BARCODE);
										attach.setUploadedBy(userName);
										attach.setUploadedDateTime(now);
										DAORegistry.getInvoiceTicketAttachmentDAO().update(attach);
										isUpdate=true;
										break;
									}
								}
								if(!isUpdate){
									attachment = new InvoiceTicketAttachment();
									attachment.setInvoiceId(Integer.parseInt(invoiceIdStr));
									attachment.setFilePath(URLUtil.TICKET_DIRECTORY+fullFileName);
									attachment.setfName(fullFileName);
									attachment.setFileOriginalName(fileOriginalName);
									attachment.setExtension(ext);
									attachment.setPosition(cnt);
									attachment.setType(FileType.BARCODE);
									attachment.setUploadedBy(userName);
									attachment.setUploadedDateTime(now);
									DAORegistry.getInvoiceTicketAttachmentDAO().save(attachment);
									
								}
								isUpdate=false;
								newFile = new File(URLUtil.TICKET_DIRECTORY+fullFileName);
								newFile.createNewFile();
								
								BufferedOutputStream stream = new BufferedOutputStream(
										new FileOutputStream(newFile));
						        FileCopyUtils.copy(file.getInputStream(), stream);
								stream.close();
								isBarcodeUploaded = true;
							}
							
						}
						
					}
				}
				
			}
			uploadFileMap.put(FileType.BARCODE.toString(),isBarcodeUploaded);
			uploadFileMap.put(FileType.QRCODE.toString(),isQrCodeUploaded);
			uploadFileMap.put(FileType.ETICKET.toString(),isEticketUploaded);
		} catch (Exception e) {
			e.printStackTrace();
			flag = false;
		}
		return flag;
	}
	
	@RequestMapping(value = "/CreateFedexLabel")
	public GenericResponseDTO createFedexLabel(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();		
		
		try {
			String invoiceIdStr = request.getParameter("invoiceId");
			String serviceType = request.getParameter("serviceType");
			String signatureType = request.getParameter("signatureType");
			String companyName = request.getParameter("companyName");
			String userName = request.getParameter("userName");
			
			if(StringUtils.isEmpty(invoiceIdStr)){
				System.err.println("Invoice ID not found.");
				error.setDescription("Invoice ID not found.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			Integer invoiceId = 0;
			try{
				invoiceId = Integer.parseInt(invoiceIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Invoice ID should be valid Integer value.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			if(StringUtils.isEmpty(serviceType) || serviceType.equalsIgnoreCase("select")){
				System.err.println("Service Type not found.");
				error.setDescription("Service Type not found.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			if(StringUtils.isEmpty(signatureType)){
				System.err.println("Signature Type not found.");
				error.setDescription("Signature Type not found.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			SignatureOptionType signatureOptionType = null;
			signatureOptionType = SignatureOptionType.fromString(signatureType);
						
			List<Recipient> recipients=DAORegistry.getInvoiceDAO().getRecipientListByInvoice(invoiceId,true);
			if(recipients == null || recipients.isEmpty()){
				System.err.println("Could not create Fedex label, No recipient found for selected Invoice.");
				error.setDescription("Could not create Fedex label, No recipient found for selected Invoice.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			Invoice invoice = DAORegistry.getInvoiceDAO().get(invoiceId);
			if(invoice == null){
				System.err.println("Invoice Details not found.");
				error.setDescription("Invoice Details not found.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			CreateFedexLabelUtil util = new CreateFedexLabelUtil();
			String trackingNo="";
			String msg="";
			
			for(Recipient recipient : recipients){
				if(companyName != null && !companyName.isEmpty()){
					recipient.setCompanyName(companyName);
				}
				if(recipient.getCountryCode()==null){
					msg = "Could not create Fedex label, No country found for selected invoice.";
					error.setDescription(msg);
					genericResponseDTO.setError(error);
					genericResponseDTO.setStatus(0);
					return genericResponseDTO;
				}else if(recipient.getCountryCode().equalsIgnoreCase("US")){													
					ServiceType service =  ServiceType.fromValue(serviceType);
					msg = util.createFedExShipLabel(recipient,service,signatureOptionType);// package type = your package					
		    	}else{
		    		ServiceType service =  ServiceType.fromValue(serviceType);
		    		msg = util.createFedExShipLabel(recipient,service,signatureOptionType);
		    	}
			}
			 
			if(msg != null && msg != ""){
				error.setDescription(msg);
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
			 }
			
			invoice = DAORegistry.getInvoiceDAO().get(invoiceId);
			trackingNo = invoice.getTrackingNo();
			if(msg==null){
				Customer cust = DAORegistry.getCustomerDAO().get(invoice.getCustomerId());
				CustomerOrder order = DAORegistry.getCustomerOrderDAO().get(invoice.getCustomerOrderId());
				String eventDetails = order.getEventName().toUpperCase();
				String dateString = order.getEventDateStr()+" "+order.getEventTimeStr();
				String venueString = order.getVenueName()+", "+order.getVenueCity()+", "+order.getVenueState()+", "+order.getVenueCountry();
				com.rtw.tracker.mail.MailAttachment[] mailAttachment = new com.rtw.tracker.mail.MailAttachment[8];
				mailAttachment[0] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","tyrtf.png",com.rtw.tmat.utils.Util.getFilePath(request, "tyrtf.png", "/resources/images/"));
				mailAttachment[1] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","blue.png",com.rtw.tmat.utils.Util.getFilePath(request, "blue.png", "/resources/images/"));
				mailAttachment[2] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","fb1.png",com.rtw.tmat.utils.Util.getFilePath(request, "fb1.png", "/resources/images/"));
				mailAttachment[3] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","ig1.png",com.rtw.tmat.utils.Util.getFilePath(request, "ig1.png", "/resources/images/"));
				mailAttachment[4] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","li1.png",com.rtw.tmat.utils.Util.getFilePath(request, "li1.png", "/resources/images/"));
				mailAttachment[5] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","rtflogot.png",com.rtw.tmat.utils.Util.getFilePath(request, "rtflogot.png", "/resources/images/"));
				mailAttachment[6] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","p1.png",com.rtw.tmat.utils.Util.getFilePath(request, "p1.png", "/resources/images/"));
				mailAttachment[7] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","tw1.png",com.rtw.tmat.utils.Util.getFilePath(request, "tw1.png", "/resources/images/"));
				
				Map<String, Object> mailMap = new HashMap<String, Object>();
				mailMap.put("customerName",cust.getCustomerName()+" "+cust.getLastName());
				mailMap.put("trackingNo",trackingNo);
				mailMap.put("eventDetails", eventDetails);
				mailMap.put("dateString", dateString);
				mailMap.put("venueString", venueString);
				mailMap.put("orderId",order.getId());
				Boolean isMailSent = false;
				try {
					mailManager.sendMailNow("text/html","sales@rewardthefan.com",cust.getEmail(), 
							null,"msanghani@rightthisway.com","Reward The Fan: Ticket delivery information",
				    		"email-fedex-info.html", mailMap, "text/html", null,mailAttachment,null);
					isMailSent = true;
				} catch (Exception e) {
					isMailSent = false;
					e.printStackTrace();
				}
				
				
				 InvoiceAudit audit = new InvoiceAudit(invoice);
				 audit.setAction(InvoiceAuditAction.FEDEX_LABEL);
				 if(isMailSent){
					 msg = "Fedex Label is created with tracking no :"+trackingNo+", Notification mail sent to customer.";
				 }else{
					 msg = "Fedex Label is created with tracking no :"+trackingNo+", Error while sending Notification mail to customer.";
				 }
				 audit.setNote(msg);
				 audit.setCreatedDate(new Date());
				 audit.setCreateBy(userName);
				 DAORegistry.getInvoiceAuditDAO().save(audit);
				 
				 genericResponseDTO.setStatus(1);
				 genericResponseDTO.setMessage(msg);
				 
				//Tracking User Action
				String userActionMsg = "Fedex Label is Created for Invoice. Tracking no - "+trackingNo;
				Util.userActionAudit(request, invoice.getId(), userActionMsg);
			 }
			
		} catch (Exception e) {
			System.out.println("FedEx: "+e);
			e.printStackTrace();
			error.setDescription("Something went wrong while Creating Fedex Label for Invoice.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
	@RequestMapping(value = "/GetInvoicePdf")
	public void getInvoicePdf(HttpServletRequest request, HttpServletResponse response){
		
		try {
			String invoiceIdStr = request.getParameter("invoiceId");
			String downloadView = request.getParameter("inline");
			String brokerIdStr = request.getParameter("brokerId");
			Integer brokerId = Integer.parseInt(brokerIdStr);
			
			if(invoiceIdStr!=null && !invoiceIdStr.isEmpty()){
				
				Invoice invoice = DAORegistry.getInvoiceDAO().getInvoiceByIdAndBrokerId(Integer.parseInt(invoiceIdStr), brokerId);
				if(invoice != null){
					//List<Ticket> ticket = DAORegistry.getTicketDAO().getMappedTicketsByInvoiceId(Integer.parseInt(invoiceIdStr));
					CategoryTicketGroup categoryTicketGroup = DAORegistry.getCategoryTicketGroupDAO().getCategoryTicketByInvoiceId(invoice.getId());
					CustomerOrderDetails orderDetail = DAORegistry.getCustomerOrderDetailsDAO().getCustomerOrderDetailsByOrderId(invoice.getCustomerOrderId());
					//EventDetails event = DAORegistry.getEventDetailsDAO().get(categoryTicketGroup.getEventId());
					CustomerOrder order = DAORegistry.getCustomerOrderDAO().get(invoice.getCustomerOrderId());
					List<OrderTicketGroupDetails> ticketGroupDetails = DAORegistry.getOrderTicketGroupDetailsDAO().getOrderTicketGroupDetailsByOrderId(invoice.getCustomerOrderId());
					PDFUtil.generateInvoicePdf(invoice, categoryTicketGroup, ticketGroupDetails, orderDetail, order);
					File file = new File(DAORegistry.getPropertyDAO().get("tictracker.invoice.pdffile.path").getValue()+"\\invoice_"+invoice.getId()+".pdf");
					if(!file.exists()){
						file.createNewFile();
					}
					OutputStream out = response.getOutputStream();
					FileInputStream in = new FileInputStream(file);
					response.setContentType("application/pdf");
					response.setHeader("Content-Disposition", String.format(downloadView+"; filename=\"%s\"",file.getName()));
					
					byte[] buffer = new byte[4096];
					int length;
					while((length = in.read(buffer)) > 0){
					    out.write(buffer, 0, length);
					}
					in.close();
					out.flush();
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
			
	@RequestMapping(value = "/GetRTWRTW2InvoicePdf")
	public void getRTWRTW2InvoicePdf(HttpServletRequest request, HttpServletResponse response){
		
		try {
			String invoiceIdStr = request.getParameter("invoiceId");
			String downloadView = request.getParameter("inline");
			String productType = request.getParameter("productType");
			Collection<OpenOrders> openOrdersList = null;
			
			if(invoiceIdStr!=null && !invoiceIdStr.isEmpty()){
				openOrdersList = DAORegistry.getQueryManagerDAO().getRTWOpenOrderListByInvoiceID(invoiceIdStr,productType);				
				List<POSTicket> tickets = DAORegistry.getPosTicketDAO().getMappedPOSTicketByInvoiceIdWithPdt(Integer.parseInt(invoiceIdStr), productType);
				List<POSCategoryTicket> categoryTickets = DAORegistry.getPosCategoryTicketDAO().getPOSCategoryTicketByInvoiceId(Integer.parseInt(invoiceIdStr),productType);
				Map<Integer, List<POSTicketGroup>> ticketMap = new HashMap<Integer, List<POSTicketGroup>>();
				
				if(tickets!=null && !tickets.isEmpty()){
					List<POSTicketGroup> list = null;
					List<Integer> ticketGroupIds = new ArrayList<Integer>();
					Map<Integer,Integer> ticCountMap = new HashMap<Integer, Integer>();
					Map<Integer,Double> ticPriceMap = new HashMap<Integer, Double>();
					Map<Integer,Integer> ticPoMap = new HashMap<Integer, Integer>();
					Map<Integer,String> ticOnHandMap = new HashMap<Integer, String>();
					Map<Integer,String> ticSeatMap = new HashMap<Integer, String>();
					for(POSTicket ticket: tickets){
						if(ticCountMap.get(ticket.getTicketGroupId())!=null){
							ticCountMap.put(ticket.getTicketGroupId(), ticCountMap.get(ticket.getTicketGroupId())+1);
						}else{
							ticCountMap.put(ticket.getTicketGroupId(), 1);
						}
						ticPriceMap.put(ticket.getTicketGroupId(),ticket.getActualSoldPrice());
						ticPoMap.put(ticket.getTicketGroupId(),ticket.getPurchaseOrderId());
						ticOnHandMap.put(ticket.getTicketGroupId(),((ticket.getTicketOnHandStatusId()!=null&&ticket.getTicketOnHandStatusId()==1)?"Y":"N"));
						if(ticSeatMap.get(ticket.getTicketGroupId())!=null){
							String seatNo = ticSeatMap.get(ticket.getTicketGroupId());
							if(seatNo.contains("-")){
								seatNo = seatNo.split("-")[0];
							}
							seatNo =seatNo + "-"+ticket.getSeatNumber();
							ticSeatMap.put(ticket.getTicketGroupId(), ticSeatMap.get(ticket.getTicketGroupId())+"-"+ticket.getSeatNumber());
						}else{
							ticSeatMap.put(ticket.getTicketGroupId(), ticket.getSeatNumber());
						}
					}
					for(POSTicket ticket: tickets){
						if(ticketGroupIds.contains(ticket.getTicketGroupId())){
							continue;
						}
						POSTicketGroup ticketGroup = DAORegistry.getPosTicketGroupDAO().getPOSTicketGroupByTicketGroupId(ticket.getTicketGroupId(),productType);
						ticketGroup.setMappedQty(ticCountMap.get(ticket.getTicketGroupId()));
						ticketGroup.setActualSoldPrice(ticPriceMap.get(ticket.getTicketGroupId()));
						ticketGroup.setPurchaseOrderId(ticPoMap.get(ticket.getTicketGroupId()));
						ticketGroup.setOnHand(ticOnHandMap.get(ticket.getTicketGroupId()));
						ticketGroup.setSeatNoString(ticSeatMap.get(ticket.getTicketGroupId()));
						if(ticketMap.get(ticketGroup.getEventId())!=null){
							ticketMap.get(ticketGroup.getEventId()).add(ticketGroup);
						}else{
							list = new ArrayList<POSTicketGroup>();
							list.add(ticketGroup);
							ticketMap.put(ticketGroup.getEventId(), list);
						}
						ticketGroupIds.add(ticketGroup.getTicketGroupId());
					}
				}
				Map<Integer, List<POSCategoryTicketGroup>> categoryTicketMap = new HashMap<Integer, List<POSCategoryTicketGroup>>();
				if(categoryTickets!=null && !categoryTickets.isEmpty()){
					List<POSCategoryTicketGroup> catTicketGroups = null;
					List<Integer> catTicketGroupIds = new ArrayList<Integer>();
					Map<Integer,Double> ticPriceMap = new HashMap<Integer, Double>();
					for(POSCategoryTicket ticket: categoryTickets){
						ticPriceMap.put(ticket.getCategoryTicketGroupId(),ticket.getActualPrice());
					}
					for(POSCategoryTicket ticket: categoryTickets){
						if(catTicketGroupIds.contains(ticket.getCategoryTicketGroupId())){
							continue;
						}
						POSCategoryTicketGroup categoryTicketGroup = DAORegistry.getPosCategoryTicketGroupDAO().getPOSCategoryTicketGroupByCategoryTicketGroupId(ticket.getCategoryTicketGroupId(), productType);
						categoryTicketGroup.setActualPrice(ticPriceMap.get(categoryTicketGroup.getCategoryTicketGroupId()));
						if(categoryTicketMap.get(categoryTicketGroup.getEventId())!=null){
							categoryTicketMap.get(categoryTicketGroup.getEventId()).add(categoryTicketGroup);
						}else{
							catTicketGroups = new ArrayList<POSCategoryTicketGroup>();
							catTicketGroups.add(categoryTicketGroup);
							categoryTicketMap.put(categoryTicketGroup.getEventId(), catTicketGroups);
						}
						catTicketGroupIds.add(categoryTicketGroup.getCategoryTicketGroupId());
					}
				}
				CustomerAddress customerBillingAddress = DAORegistry.getCustomerAddressDAO().getCustomerAddressInfo(openOrdersList.iterator().next().getCustomerId());
				List<CustomerAddress> customerShippingAddress = DAORegistry.getCustomerAddressDAO().getShippingAddressInfo(openOrdersList.iterator().next().getCustomerId());
				POSCustomerOrder order = DAORegistry.getPosCustomerOrderDAO().getPOSCustomerOrderByTicketRequestId(openOrdersList.iterator().next().getCustomerOrderId());
				
				PDFUtil.generateRTWInvoicePdf(openOrdersList, categoryTicketMap, ticketMap, customerBillingAddress, customerShippingAddress, order);
				File file = new File(DAORegistry.getPropertyDAO().get("tictracker.invoice.pdffile.path").getValue()+"\\invoice_"+invoiceIdStr+".pdf");
				if(!file.exists()){
					file.createNewFile();
				}
				OutputStream out = response.getOutputStream();
				FileInputStream in = new FileInputStream(file);
				response.setContentType("application/pdf");
				response.setHeader("Content-Disposition", String.format(downloadView+"; filename=\"%s\"",file.getName()));
				
				byte[] buffer = new byte[4096];
				int length;
				while((length = in.read(buffer)) > 0){
				    out.write(buffer, 0, length);
				}
				in.close();
				out.flush();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value="/ChangeInvoiceStatus")
	public InvoiceStatusChangeDTO changeInvoiceStatus(HttpServletRequest request, HttpServletResponse response){
		InvoiceStatusChangeDTO invoiceStatusChangeDTO = new InvoiceStatusChangeDTO();
		Error error = new Error();
		
		try{
			String status = request.getParameter("status");
			String invoiceIdStr = request.getParameter("invoiceId");
			String userName = request.getParameter("userName");
			String msg="";
			
			if(StringUtils.isEmpty(invoiceIdStr)){
				System.err.println("Invoice ID not found.");
				error.setDescription("Invoice ID not found.");
				invoiceStatusChangeDTO.setError(error);
				invoiceStatusChangeDTO.setStatus(0);
				return invoiceStatusChangeDTO;
			}
			Integer invoiceId = 0;
			try{
				invoiceId = Integer.parseInt(invoiceIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Invoice ID should be valid Integer value.");
				invoiceStatusChangeDTO.setError(error);
				invoiceStatusChangeDTO.setStatus(0);
				return invoiceStatusChangeDTO;
			}
			
			Invoice invoice = DAORegistry.getInvoiceDAO().get(invoiceId);
			if(invoice == null){
				System.err.println("Invoice Details not found.");
				error.setDescription("Invoice Details not found.");
				invoiceStatusChangeDTO.setError(error);
				invoiceStatusChangeDTO.setStatus(0);
				return invoiceStatusChangeDTO;
			}
				
			invoice.setLastUpdatedBy(userName);
			
			if(status!=null && status.equalsIgnoreCase(InvoiceStatus.Voided.toString())){
				if(invoice.getStatus().equals(InvoiceStatus.Voided)){
					System.err.println("Selected Invoice already Voided.");
					error.setDescription("Selected Invoice already Voided.");
					invoiceStatusChangeDTO.setError(error);
					invoiceStatusChangeDTO.setStatus(0);
					return invoiceStatusChangeDTO;
				}
				if(!invoice.getStatus().equals(InvoiceStatus.Voided)){
					CustomerOrder order = DAORegistry.getCustomerOrderDAO().get(invoice.getCustomerOrderId());
					if(order == null){
						System.err.println("Customer Order not found.");
						error.setDescription("Customer Order not found.");
						invoiceStatusChangeDTO.setError(error);
						invoiceStatusChangeDTO.setStatus(0);
						return invoiceStatusChangeDTO;
					}
					else if((order.getPrimaryPaymentMethod().equals(PaymentMethod.CREDITCARD) 
							|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
							|| (order.getThirdPaymentMethod()!= null && order.getThirdPaymentMethod().equals(PartialPaymentMethod.CREDITCARD))
							|| order.getPrimaryPaymentMethod().equals(PaymentMethod.PAYPAL)
							|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.PAYPAL)
							|| (order.getThirdPaymentMethod()!= null && order.getThirdPaymentMethod().equals(PartialPaymentMethod.PAYPAL)))){
						
						String refundType = "";
						if(order.getPrimaryPaymentMethod().equals(PaymentMethod.CREDITCARD) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)){
							refundType = PaymentMethod.CREDITCARD.toString();
						}else if(order.getPrimaryPaymentMethod().equals(PaymentMethod.PAYPAL) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.PAYPAL)){
							refundType = PaymentMethod.PAYPAL.toString();
						}
						
						Map<String, String> paramMap = Util.getParameterMap(request);
						paramMap.put("orderId", String.valueOf(order.getId()));
						paramMap.put("refundType", refundType);
						paramMap.put("refundAmount",String.valueOf(Util.formatDoubleToTwoDecimalPoint(invoice.getInvoiceTotal())));
						
						String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.INVOICE_REFUND);
						Gson gson = Util.getGsonBuilder().create();	
						JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
						InvoiceRefundResponse invoiceRefundResponse = gson.fromJson(((JsonObject)jsonObject.get("invoiceRefundResponse")), InvoiceRefundResponse.class);
						if(invoiceRefundResponse == null){
							System.err.println("Invoice Refund API not found.");
							error.setDescription("Invoice Refund API not found.");
							invoiceStatusChangeDTO.setError(error);
							invoiceStatusChangeDTO.setStatus(0);
							return invoiceStatusChangeDTO;
						}else if(invoiceRefundResponse.getStatus()==1){
							InvoiceRefund invoiceRefund = invoiceRefundResponse.getInvoiceRefund();
//							if(invoiceRefund == null){
//								object.put("msg", "Invoice Refund not found.");
//								IOUtils.write(object.toString().getBytes(), response.getOutputStream());
//								return;
//							}									
							Util.voidInvoice(invoice, order, invoiceRefund, userName);
							invoiceStatusChangeDTO.setMessage("Selected Invoice Voided Successfully.");
							invoiceStatusChangeDTO.setStatus(1);
							
						}else{
							msg = invoiceRefundResponse.getError().getDescription();
							error.setDescription(msg);
							invoiceStatusChangeDTO.setError(error);
							invoiceStatusChangeDTO.setStatus(0);
							return invoiceStatusChangeDTO;
						}
					}else if(order!=null && order.getPrimaryPaymentMethod().equals(PaymentMethod.FULL_REWARDS)){
						Util.voidInvoice(invoice, order, null, userName);
						
						invoiceStatusChangeDTO.setMessage("Selected Invoice Voided Successfully.");
						invoiceStatusChangeDTO.setStatus(1);
					}else{
						msg="Payment method is other than credicard/reward points for selected Order.";
						error.setDescription(msg);
						invoiceStatusChangeDTO.setError(error);
						invoiceStatusChangeDTO.setStatus(0);
						return invoiceStatusChangeDTO;
					}
				}
			}else if(status!=null && status.equalsIgnoreCase(InvoiceStatus.Completed.toString())){
				invoice.setStatus(InvoiceStatus.Completed);
				invoice.setLastUpdatedBy(userName);
				invoice.setLastUpdated(new Date());
				invoice.setIsRealTixUploaded("Yes");
				invoice.setRealTixMap("Yes");
				DAORegistry.getInvoiceDAO().update(invoice);
				
				InvoiceAudit audit = new InvoiceAudit(invoice);
				audit.setAction(InvoiceAuditAction.INVOICE_MANUALLY_COMPLETED);
				audit.setCreateBy(userName);
				audit.setCreatedDate(new Date());
				audit.setNote("Invoice Manully Marked as Completed.");
				DAORegistry.getInvoiceAuditDAO().save(audit);
				
				invoiceStatusChangeDTO.setMessage("Selected Invoice Marked as Completed.");
				invoiceStatusChangeDTO.setStatus(1);
				
				//Tracking User Action
				String userActionMsg = "Invoice Manually Marked as Completed.";
				Util.userActionAudit(request, invoice.getId(), userActionMsg);
				
			}else if(status!=null && status.equalsIgnoreCase(InvoiceStatus.Outstanding.toString())){
				invoice.setStatus(InvoiceStatus.Outstanding);
				invoice.setLastUpdatedBy(userName);
				invoice.setLastUpdated(new Date());
				invoice.setIsRealTixUploaded("No");
				//invoice.setRealTixMap("Yes");
				DAORegistry.getInvoiceDAO().update(invoice);
				
				InvoiceAudit audit = new InvoiceAudit(invoice);
				audit.setAction(InvoiceAuditAction.INVOICE_MANUALLY_OUTSTANDING);
				audit.setCreateBy(userName);
				audit.setCreatedDate(new Date());
				audit.setNote("Invoice Manully Marked as Outstanding.");
				DAORegistry.getInvoiceAuditDAO().save(audit);
				
				invoiceStatusChangeDTO.setMessage("Selected Invoice Marked as Outstanding.");
				invoiceStatusChangeDTO.setStatus(1);
				
				//Tracking User Action
				String userActionMsg = "Invoice Manually Marked as Outstanding.";
				Util.userActionAudit(request, invoice.getId(), userActionMsg);
			}
			invoiceStatusChangeDTO.setInvoiceId(invoiceId);
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while Change Invoice Status.");
			invoiceStatusChangeDTO.setError(error);
			invoiceStatusChangeDTO.setStatus(0);
		}
		return invoiceStatusChangeDTO;
	}
		
	@RequestMapping(value="/GetPaymentHistory")
	public InvoicePaymentDetailsDTO getPaymentHistory(HttpServletRequest request, HttpServletResponse response){
		InvoicePaymentDetailsDTO invoicePaymentDetailsDTO = new InvoicePaymentDetailsDTO();
		Error error = new Error();
		
		try{
			String invoiceIdStr = request.getParameter("invoiceId");
			String productType = request.getParameter("productType");
			String brokerIdStr = request.getParameter("brokerId");
			
			if(StringUtils.isEmpty(invoiceIdStr)){
				System.err.println("Invoice ID not found.");
				error.setDescription("Invoice ID not found.");
				invoicePaymentDetailsDTO.setError(error);
				invoicePaymentDetailsDTO.setStatus(0);
				return invoicePaymentDetailsDTO;
			}
			Integer invoiceId = 0;
			try{
				invoiceId = Integer.parseInt(invoiceIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Invoice ID should be valid Integer value.");
				invoicePaymentDetailsDTO.setError(error);
				invoicePaymentDetailsDTO.setStatus(0);
				return invoicePaymentDetailsDTO;
			}			
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				invoicePaymentDetailsDTO.setError(error);
				invoicePaymentDetailsDTO.setStatus(0);
				return invoicePaymentDetailsDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				invoicePaymentDetailsDTO.setError(error);
				invoicePaymentDetailsDTO.setStatus(0);
				return invoicePaymentDetailsDTO;
			}
			
			if(productType == null || productType.isEmpty()){
				System.err.println("Product Type not found.");
				error.setDescription("Product Type not found.");
				invoicePaymentDetailsDTO.setError(error);
				invoicePaymentDetailsDTO.setStatus(0);
				return invoicePaymentDetailsDTO;
			}
			
			List<PaymentHistory> paymentHistoryList = null;
			List<PaymentHistory> rtwPaymentHistoryList = null;
			if(productType.equalsIgnoreCase("REWARDTHEFAN")){
				paymentHistoryList = DAORegistry.getQueryManagerDAO().getPaymentHistoryByInvoice(invoiceId, brokerId);
			}
			if(productType.equalsIgnoreCase("RTW") || productType.equalsIgnoreCase("RTW2")){
				rtwPaymentHistoryList = DAORegistry.getQueryManagerDAO().getRTWPaymentHistoryByInvoice(invoiceId, productType);
			}
			
			if(paymentHistoryList!=null && paymentHistoryList.size() > 0){
				for(PaymentHistory paymentHistory : paymentHistoryList){
					if(paymentHistory.getPrimaryPaymentMethod().equalsIgnoreCase("ACCOUNT_RECIVABLE")){
						System.err.println("Selected Invoice is Account Receivable, no payment history found.");
						error.setDescription("Selected Invoice is Account Receivable, no payment history found.");
						invoicePaymentDetailsDTO.setError(error);
						invoicePaymentDetailsDTO.setStatus(0);
						return invoicePaymentDetailsDTO;
					}
				}
			}
			if(paymentHistoryList!=null && paymentHistoryList.size() > 0){
				invoicePaymentDetailsDTO.setStatus(1);
				invoicePaymentDetailsDTO.setInvoicePaginationDTO(PaginationUtil.getDummyPaginationParameters(paymentHistoryList.size()));
				invoicePaymentDetailsDTO.setInvoiceList(com.rtw.tracker.utils.Util.getPaymentHistoryArray(paymentHistoryList, null));
			}
			else if(rtwPaymentHistoryList!=null && rtwPaymentHistoryList.size() > 0){
				invoicePaymentDetailsDTO.setStatus(1);
				invoicePaymentDetailsDTO.setInvoicePaginationDTO(PaginationUtil.getDummyPaginationParameters(rtwPaymentHistoryList.size()));
				invoicePaymentDetailsDTO.setRtwInvoiceList(com.rtw.tracker.utils.Util.getPaymentHistoryArray(null, rtwPaymentHistoryList));
			}
			else{
				invoicePaymentDetailsDTO.setMessage("No Payment details found for Selected Invoice.");
				invoicePaymentDetailsDTO.setInvoicePaginationDTO(PaginationUtil.getDummyPaginationParameters(0));
			}
			invoicePaymentDetailsDTO.setSelectedProductType(productType);
			
		}catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Invoice Payment History.");
			invoicePaymentDetailsDTO.setError(error);
			invoicePaymentDetailsDTO.setStatus(0);
		}
		return invoicePaymentDetailsDTO;
	}
	
		
	@RequestMapping(value="/GetInvoiceCsr")
	public InvoiceCsrDTO getInvoiceCsr(HttpServletRequest request, HttpServletResponse response){
		InvoiceCsrDTO invoiceCsrDTO = new InvoiceCsrDTO();
		Error error = new Error();
		
		try{
			String action = request.getParameter("action");
			String invoiceIdStr = request.getParameter("invoiceId");			
			String csrUser = request.getParameter("csrUser");
			String userName = request.getParameter("userName");
			String brokerIdStr = request.getParameter("brokerId");
			
			if(StringUtils.isEmpty(invoiceIdStr)){
				System.err.println("Invoice ID not found.");
				error.setDescription("Invoice ID not found.");
				invoiceCsrDTO.setError(error);
				invoiceCsrDTO.setStatus(0);
				return invoiceCsrDTO;
			}
			Integer invoiceId = 0;
			try{
				invoiceId = Integer.parseInt(invoiceIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Invoice ID should be valid Integer value.");
				invoiceCsrDTO.setError(error);
				invoiceCsrDTO.setStatus(0);
				return invoiceCsrDTO;
			}			
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				invoiceCsrDTO.setError(error);
				invoiceCsrDTO.setStatus(0);
				return invoiceCsrDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				invoiceCsrDTO.setError(error);
				invoiceCsrDTO.setStatus(0);
				return invoiceCsrDTO;
			}
				
			Invoice invoice = null;
			invoice = DAORegistry.getInvoiceDAO().getInvoiceByIdAndBrokerId(invoiceId, brokerId);
			if(invoice == null){
				System.err.println("Invoice Details not found.");
				error.setDescription("Invoice Details not found.");
				invoiceCsrDTO.setError(error);
				invoiceCsrDTO.setStatus(0);
				return invoiceCsrDTO;
			}
				
			if(action!=null && action.equalsIgnoreCase("changeCSR")){
				if(csrUser == null || csrUser.isEmpty()){
					System.err.println("CSR not found.");
					error.setDescription("CSR not found.");
					invoiceCsrDTO.setError(error);
					invoiceCsrDTO.setStatus(0);
					return invoiceCsrDTO;
				}
				
				if(invoice!=null){
					invoice.setCsr(csrUser);
					invoice.setLastUpdated(new Date());
					invoice.setLastUpdatedBy(userName);
					DAORegistry.getInvoiceDAO().update(invoice);
					invoiceCsrDTO.setMessage("Invoice CSR changed Successfully.");
					
					//Tracking User Action
					String userActionMsg = "Invoice CSR changed Successfully.";
					Util.userActionAudit(request, invoice.getId(), userActionMsg);
				}
			}else{
				csrUser = invoice.getCsr();
			}				
			
			Collection<TrackerUser> users = null;
			if(brokerId != 1001){
				users =  DAORegistry.getTrackerUserDAO().getAllTrackerUserByBrokerId(brokerId);
			}else{
				users =  DAORegistry.getTrackerUserDAO().getAllTrackerUserExceptBroker();
			}
						
			if(users != null && users.size() > 0){
				invoiceCsrDTO.setTrackerUserDTO(com.rtw.tracker.utils.Util.getTrackerUsersObject(users));
			}
			invoiceCsrDTO.setStatus(1);
			invoiceCsrDTO.setInvoiceId(invoiceIdStr);
			invoiceCsrDTO.setInvoiceCsr(csrUser);
			
		}catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching/updating Invoice CSR.");
			invoiceCsrDTO.setError(error);
			invoiceCsrDTO.setStatus(0);
		}
		return invoiceCsrDTO;
	}
	 
	@RequestMapping({"/GetCustomerOrderShippingAddress"})
	public InvoiceCustomerOrderAddressDTO getCustomerOrderShippingAddress(HttpServletRequest request, HttpServletResponse response){
		InvoiceCustomerOrderAddressDTO invoiceCustomerOrderAddressDTO = new InvoiceCustomerOrderAddressDTO();
		Error error = new Error();		 
		 
		try{
			String orderIdStr = request.getParameter("orderId");
			String invoiceIdStr = request.getParameter("invoiceId");
			String brokerIdStr = request.getParameter("brokerId");
			
			if(StringUtils.isEmpty(orderIdStr)){
				System.err.println("Customer Order ID not found.");
				error.setDescription("Customer Order ID not found.");
				invoiceCustomerOrderAddressDTO.setError(error);
				invoiceCustomerOrderAddressDTO.setStatus(0);
				return invoiceCustomerOrderAddressDTO;
			}
			Integer orderId = 0;
			try{
				orderId = Integer.parseInt(orderIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Customer Order ID should be valid Integer value.");
				invoiceCustomerOrderAddressDTO.setError(error);
				invoiceCustomerOrderAddressDTO.setStatus(0);
				return invoiceCustomerOrderAddressDTO;
			}			
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				invoiceCustomerOrderAddressDTO.setError(error);
				invoiceCustomerOrderAddressDTO.setStatus(0);
				return invoiceCustomerOrderAddressDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				invoiceCustomerOrderAddressDTO.setError(error);
				invoiceCustomerOrderAddressDTO.setStatus(0);
				return invoiceCustomerOrderAddressDTO;
			}
			
			CustomerOrder customerOrder = null;
			customerOrder = DAORegistry.getCustomerOrderDAO().getCustomerOrderByIdAndBrokerId(orderId, brokerId);
			if(customerOrder == null){
				System.err.println("Customer Order not found.");
				error.setDescription("Customer Order not found.");
				invoiceCustomerOrderAddressDTO.setError(error);
				invoiceCustomerOrderAddressDTO.setStatus(0);
				return invoiceCustomerOrderAddressDTO;
			}

			CustomerOrderDetails orderDetails = null;
			orderDetails = DAORegistry.getCustomerOrderDetailsDAO().getCustomerOrderDetailsByOrderId(orderId);
			if(orderDetails == null){
				System.err.println("Customer Order Details not found.");
				error.setDescription("Customer Order Details not found.");
				invoiceCustomerOrderAddressDTO.setError(error);
				invoiceCustomerOrderAddressDTO.setStatus(0);
				return invoiceCustomerOrderAddressDTO;
			}
						
			List<State> stateList = null;
			List<Country> countryList = null;
			Customer customer = DAORegistry.getCustomerDAO().get(customerOrder.getCustomerId()); 
			stateList = DAORegistry.getQueryManagerDAO().getStatesForCountry(orderDetails.getShCountry());
			countryList = DAORegistry.getQueryManagerDAO().getCountries();
			
			if(customer != null){
				invoiceCustomerOrderAddressDTO.setCustomerDTO(com.rtw.tracker.utils.Util.getCustomerObject(customer));
			}
			if(orderDetails != null){
				invoiceCustomerOrderAddressDTO.setCustomerOrderAddressDTO(com.rtw.tracker.utils.Util.getCustomerShipppingAddressObject(orderDetails));
			}
			if(countryList != null){
				invoiceCustomerOrderAddressDTO.setCountryDTO(com.rtw.tracker.utils.Util.getCountryArray(countryList));
			}
			if(stateList != null){
				invoiceCustomerOrderAddressDTO.setStateDTO(com.rtw.tracker.utils.Util.getStateArray(stateList));
			}
			invoiceCustomerOrderAddressDTO.setStatus(1);
			invoiceCustomerOrderAddressDTO.setInvoiceId(invoiceIdStr);

		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Customer Shipping Address.");
			invoiceCustomerOrderAddressDTO.setError(error);
			invoiceCustomerOrderAddressDTO.setStatus(0);
		}
		return invoiceCustomerOrderAddressDTO;
	}
	
	@RequestMapping({"/UpdateShippingAddress"})
	public GenericResponseDTO updateShippingAddress(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		
		try {
			String orderIdStr = request.getParameter("orderId");			
			String countryIdStr = request.getParameter("country");			
			String stateIdStr = request.getParameter("state");
						
			if(StringUtils.isEmpty(orderIdStr)){
				System.err.println("Customer Order ID not found.");
				error.setDescription("Customer Order ID not found.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			Integer orderId = 0;
			try{
				orderId = Integer.parseInt(orderIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Customer Order ID should be valid Integer value.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			if(StringUtils.isEmpty(countryIdStr)){
				System.err.println("Country not found.");
				error.setDescription("Country not found.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			int countryId = 0;
			try{
				countryId = Integer.parseInt(countryIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Country should be valid value.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			if(StringUtils.isEmpty(stateIdStr)){
				System.err.println("State not found.");
				error.setDescription("State not found.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			int stateId = 0;
			try{
				stateId = Integer.parseInt(stateIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("State should be valid value.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
						
			CustomerOrderDetails orderDetails = null;			
			orderDetails = DAORegistry.getCustomerOrderDetailsDAO().getCustomerOrderDetailsByOrderId(orderId);
			if(orderDetails == null){
				System.err.println("Customer Order Details not found.");
				error.setDescription("Customer Order Details not found.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			//stateList = DAORegistry.getQueryManagerDAO().getStatesForCountry(orderDetails.getShCountry());				
			orderDetails.setShFirstName(request.getParameter("firstName"));
			orderDetails.setShLastName(request.getParameter("lastName"));
			orderDetails.setShAddress1(request.getParameter("street1"));
			orderDetails.setShAddress2(request.getParameter("street2"));
			orderDetails.setShCity(request.getParameter("city"));
			orderDetails.setShZipCode(request.getParameter("zipcode"));
			orderDetails.setShPhone1(request.getParameter("phone"));
			orderDetails.setShEmail(request.getParameter("email"));
			
			List<Country> countryList = null;
			List<State> stateList = null;
			countryList = DAORegistry.getQueryManagerDAO().getCountries();
			stateList = DAORegistry.getQueryManagerDAO().getStatesForCountry(countryId);
						
			for(State state : stateList){
				if(stateId==state.getId()){
					orderDetails.setShState(stateId);
					orderDetails.setShStateName(state.getName());
					break;
				}
			}
			
			for(Country count : countryList){
				if(countryId==count.getId()){
					orderDetails.setShCountry(countryId);
					orderDetails.setShCountryName(count.getName());
					break;
				}
			}
			DAORegistry.getCustomerOrderDetailsDAO().update(orderDetails);
			
			genericResponseDTO.setMessage("Address Updated successfully.");			
			genericResponseDTO.setStatus(1);
			
			//Tracking User Action
			String userActionMsg = "Customer Order - Shipping Address Updated Successfully.";
			Util.userActionAudit(request, orderDetails.getId(), userActionMsg);
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Updating Customer Shipping Address.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	 
	@RequestMapping({"/GetCustomerOrderBillingAddress"})
	public InvoiceCustomerOrderAddressDTO getCustomerOrderBillingAddress(HttpServletRequest request, HttpServletResponse response){
		InvoiceCustomerOrderAddressDTO invoiceCustomerOrderAddressDTO = new InvoiceCustomerOrderAddressDTO();
		Error error = new Error();	
		 
		try{
			String action = request.getParameter("action");
			String orderIdStr = request.getParameter("orderId");			
			String invoiceIdStr = request.getParameter("invoiceId");
			String brokerIdStr = request.getParameter("brokerId");
			
			if(StringUtils.isEmpty(orderIdStr)){
				System.err.println("Customer Order ID not found.");
				error.setDescription("Customer Order ID not found.");
				invoiceCustomerOrderAddressDTO.setError(error);
				invoiceCustomerOrderAddressDTO.setStatus(0);
				return invoiceCustomerOrderAddressDTO;
			}
			Integer orderId = 0;
			try{
				orderId = Integer.parseInt(orderIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Customer Order ID should be valid Integer value.");
				invoiceCustomerOrderAddressDTO.setError(error);
				invoiceCustomerOrderAddressDTO.setStatus(0);
				return invoiceCustomerOrderAddressDTO;
			}			
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				invoiceCustomerOrderAddressDTO.setError(error);
				invoiceCustomerOrderAddressDTO.setStatus(0);
				return invoiceCustomerOrderAddressDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				invoiceCustomerOrderAddressDTO.setError(error);
				invoiceCustomerOrderAddressDTO.setStatus(0);
				return invoiceCustomerOrderAddressDTO;
			}		
						
			CustomerOrder customerOrder = DAORegistry.getCustomerOrderDAO().getCustomerOrderByIdAndBrokerId(orderId, brokerId);
			if(customerOrder == null){
				System.err.println("Customer Order not found.");
				error.setDescription("Customer Order not found.");
				invoiceCustomerOrderAddressDTO.setError(error);
				invoiceCustomerOrderAddressDTO.setStatus(0);
				return invoiceCustomerOrderAddressDTO;
			}
			
			CustomerOrderDetails orderDetails = null;
			orderDetails = DAORegistry.getCustomerOrderDetailsDAO().getCustomerOrderDetailsByOrderId(orderId);
			if(orderDetails == null){
				System.err.println("Customer Order Details not found.");
				error.setDescription("Customer Order Details not found.");
				invoiceCustomerOrderAddressDTO.setError(error);
				invoiceCustomerOrderAddressDTO.setStatus(0);
				return invoiceCustomerOrderAddressDTO;
			}
			
			List<Country> countryList = null;
			List<State> stateList = null;
			countryList = DAORegistry.getQueryManagerDAO().getCountries();
			stateList = DAORegistry.getQueryManagerDAO().getStatesForCountry(orderDetails.getShCountry());
			if(action!=null && action.equalsIgnoreCase("update")){						
				orderDetails.setBlFirstName(request.getParameter("firstName"));
				orderDetails.setBlLastName(request.getParameter("lastName"));
				orderDetails.setBlAddress1(request.getParameter("street1"));
				orderDetails.setBlAddress2(request.getParameter("street2"));
				orderDetails.setBlCity(request.getParameter("city"));
				orderDetails.setBlZipCode(request.getParameter("zipcode"));
				orderDetails.setBlPhone1(request.getParameter("phone"));
				orderDetails.setBlEmail(request.getParameter("email"));
				Integer countryId = Integer.parseInt(request.getParameter("country"));
				
				stateList = DAORegistry.getQueryManagerDAO().getStatesForCountry(countryId);
				String stateId = request.getParameter("state");
				if(stateId!=null && !stateId.isEmpty()){
					int id= Integer.parseInt(stateId);
					for(State state : stateList){
						if(id==state.getId()){
							orderDetails.setBlState(id);
							orderDetails.setBlStateName(state.getName());
							break;
						}
					}
				}
				String country = request.getParameter("country");
				if(country!=null && !country.isEmpty()){
					int id= Integer.parseInt(country);
					for(Country count : countryList){
						if(id==count.getId()){
							orderDetails.setBlCountry(id);
							orderDetails.setBlCountryName(count.getName());
							break;
						}
					}
				}
				DAORegistry.getCustomerOrderDetailsDAO().update(orderDetails);

				invoiceCustomerOrderAddressDTO.setMessage("Billing address updated successfully.");
				
				//Tracking User Action
				String userActionMsg = "Customer Order - Billing Address Updated Successfully.";
				Util.userActionAudit(request, orderDetails.getId(), userActionMsg);
			}	
			
			if(orderDetails != null){
				invoiceCustomerOrderAddressDTO.setCustomerOrderAddressDTO(com.rtw.tracker.utils.Util.getCustomerBillingAddressObject(orderDetails));
			}
			if(countryList != null){
				invoiceCustomerOrderAddressDTO.setCountryDTO(com.rtw.tracker.utils.Util.getCountryArray(countryList));
			}
			if(stateList != null){
				invoiceCustomerOrderAddressDTO.setStateDTO(com.rtw.tracker.utils.Util.getStateArray(stateList));
			}
			invoiceCustomerOrderAddressDTO.setStatus(1);
			invoiceCustomerOrderAddressDTO.setInvoiceId(invoiceIdStr);
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Customer Billing Address.");
			invoiceCustomerOrderAddressDTO.setError(error);
			invoiceCustomerOrderAddressDTO.setStatus(0);
		}
		return invoiceCustomerOrderAddressDTO;
	}
	
	@RequestMapping(value = "/GetInvoiceNote")
	public InvoiceNoteDTO getInvoiceNote(HttpServletRequest request, HttpServletResponse response){
		InvoiceNoteDTO invoiceNoteDTO = new InvoiceNoteDTO();
		Error error = new Error();
		
		try{
			String invoiceIdStr = request.getParameter("invoiceId");
			String brokerIdStr = request.getParameter("brokerId");
						
			if(StringUtils.isEmpty(invoiceIdStr)){
				System.err.println("Invoice ID not found.");
				error.setDescription("Invoice ID not found.");
				invoiceNoteDTO.setError(error);
				invoiceNoteDTO.setStatus(0);
				return invoiceNoteDTO;
			}
			Integer invoiceId = 0;
			try{
				invoiceId = Integer.parseInt(invoiceIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Invoice ID should be valid Integer value.");
				invoiceNoteDTO.setError(error);
				invoiceNoteDTO.setStatus(0);
				return invoiceNoteDTO;
			}			
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				invoiceNoteDTO.setError(error);
				invoiceNoteDTO.setStatus(0);
				return invoiceNoteDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				invoiceNoteDTO.setError(error);
				invoiceNoteDTO.setStatus(0);
				return invoiceNoteDTO;
			}
			
			Invoice invoice = null;
			invoice = DAORegistry.getInvoiceDAO().getInvoiceByIdAndBrokerId(invoiceId, brokerId);
			if(invoice == null){
				System.err.println("Invoice not found.");
				error.setDescription("Invoice not found.");
				invoiceNoteDTO.setError(error);
				invoiceNoteDTO.setStatus(0);
				return invoiceNoteDTO;
			}
				
			invoiceNoteDTO.setStatus(1);
			invoiceNoteDTO.setId(invoice.getId());
			invoiceNoteDTO.setInternalNotes(invoice.getInternalNote());
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Invoice Note.");
			invoiceNoteDTO.setError(error);
			invoiceNoteDTO.setStatus(0);
		}
		return invoiceNoteDTO;
	}
	
	@RequestMapping(value = "/UpdateInvoiceNote")
	public InvoiceNoteDTO updateInvoiceNote(HttpServletRequest request, HttpServletResponse response){
		InvoiceNoteDTO invoiceNoteDTO = new InvoiceNoteDTO();
		Error error = new Error();
		
		try{					
			String action = request.getParameter("action");
			String invoiceIdStr = request.getParameter("modifyNotesInvoiceId");
			String brokerIdStr = request.getParameter("brokerId");			
			
			if(StringUtils.isEmpty(invoiceIdStr)){
				System.err.println("Invoice ID not found.");
				error.setDescription("Invoice ID not found.");
				invoiceNoteDTO.setError(error);
				invoiceNoteDTO.setStatus(0);
				return invoiceNoteDTO;
			}
			Integer invoiceId = 0;
			try{
				invoiceId = Integer.parseInt(invoiceIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Invoice ID should be valid Integer value.");
				invoiceNoteDTO.setError(error);
				invoiceNoteDTO.setStatus(0);
				return invoiceNoteDTO;
			}			
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				invoiceNoteDTO.setError(error);
				invoiceNoteDTO.setStatus(0);
				return invoiceNoteDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				invoiceNoteDTO.setError(error);
				invoiceNoteDTO.setStatus(0);
				return invoiceNoteDTO;
			}
			
			Invoice invoice = null;
			invoice = DAORegistry.getInvoiceDAO().getInvoiceByIdAndBrokerId(invoiceId, brokerId);
			if(invoice == null){
				System.err.println("Invoice not found.");
				error.setDescription("Invoice not found.");
				invoiceNoteDTO.setError(error);
				invoiceNoteDTO.setStatus(0);
				return invoiceNoteDTO;
			}
				
			if(action!=null && action.equalsIgnoreCase("update")){
				invoice.setInternalNote(request.getParameter("modifyNotesInvoiceNote"));
				DAORegistry.getInvoiceDAO().update(invoice);
				
				invoiceNoteDTO.setMessage("Invoice Note updated Successfully.");
			}				
			invoiceNoteDTO.setStatus(1);
			
			//Tracking User Action
			String userActionMsg = "Invoice Note updated Successfully.";
			Util.userActionAudit(request, invoice.getId(), userActionMsg);
			
		}catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while updating Invoice Note.");
			invoiceNoteDTO.setError(error);
			invoiceNoteDTO.setStatus(0);
		}
		return invoiceNoteDTO;
	}
		
	@RequestMapping(value = "/GetInvoiceDownloadTicket")
	public InvoiceDownloadTicketDTO getInvoiceDownloadTicket(HttpServletRequest request, HttpServletResponse response){
		InvoiceDownloadTicketDTO invoiceDownloadTicketDTO = new InvoiceDownloadTicketDTO();
		Error error = new Error();
		
		try{
			String invoiceIdStr = request.getParameter("invoiceId");
			String brokerIdStr = request.getParameter("brokerId");
			
			if(StringUtils.isEmpty(invoiceIdStr)){
				System.err.println("Invoice ID not found.");
				error.setDescription("Invoice ID not found.");
				invoiceDownloadTicketDTO.setError(error);
				invoiceDownloadTicketDTO.setStatus(0);
				return invoiceDownloadTicketDTO;
			}
			Integer invoiceId = 0;
			try{
				invoiceId = Integer.parseInt(invoiceIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Invoice ID should be valid Integer value.");
				invoiceDownloadTicketDTO.setError(error);
				invoiceDownloadTicketDTO.setStatus(0);
				return invoiceDownloadTicketDTO;
			}			
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				invoiceDownloadTicketDTO.setError(error);
				invoiceDownloadTicketDTO.setStatus(0);
				return invoiceDownloadTicketDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				invoiceDownloadTicketDTO.setError(error);
				invoiceDownloadTicketDTO.setStatus(0);
				return invoiceDownloadTicketDTO;
			}
			
			List<CustomerTicketDownloads> customerTicketDownloadList = null;
			customerTicketDownloadList = DAORegistry.getCustomerTicketDownloadsDAO().getCustomerTicketDownloadsByInvoiceId(invoiceId);
			if(customerTicketDownloadList == null || customerTicketDownloadList.isEmpty()){
				System.err.println("There is No Tickets Downloaded for Selected Invoice.");
				error.setDescription("There is No Tickets Downloaded for Selected Invoice.");
				invoiceDownloadTicketDTO.setError(error);
				invoiceDownloadTicketDTO.setStatus(0);
				return invoiceDownloadTicketDTO;
			}
			if(customerTicketDownloadList != null && customerTicketDownloadList.size() > 0){
				for(CustomerTicketDownloads customerTicketDownload : customerTicketDownloadList){
					Invoice invoice = DAORegistry.getInvoiceDAO().getInvoiceByIdAndBrokerId(customerTicketDownload.getInvoiceId(), brokerId);
					if(invoice == null){
						System.err.println("Invoice not found.");
						error.setDescription("Invoice not found.");
						invoiceDownloadTicketDTO.setError(error);
						invoiceDownloadTicketDTO.setStatus(0);
						return invoiceDownloadTicketDTO;
					}
					
					CustomerOrderDetails customerOrderDetails = DAORegistry.getCustomerOrderDetailsDAO().getCustomerOrderDetailsByOrderId(invoice.getCustomerOrderId());
					if(customerOrderDetails != null && customerOrderDetails.getShEmail() != null){
						customerTicketDownload.setEmail(customerOrderDetails.getShEmail());
					}else{
						Customer customer = DAORegistry.getCustomerDAO().get(customerTicketDownload.getCustomerId());
						customerTicketDownload.setEmail(customer.getEmail());								
					}					
				}
			}
			
			if(customerTicketDownloadList != null){
				invoiceDownloadTicketDTO.setTicketsDownloadDTO(com.rtw.tracker.utils.Util.getCustomerTicketsDownloadArray(customerTicketDownloadList));
			}
			invoiceDownloadTicketDTO.setStatus(1);
			invoiceDownloadTicketDTO.setTicketDownloadPaginationDTO(PaginationUtil.getDummyPaginationParameters(customerTicketDownloadList!=null?customerTicketDownloadList.size():0));
			invoiceDownloadTicketDTO.setInvoiceId(invoiceId);
			
		}catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Invoice Download Ticket.");
			invoiceDownloadTicketDTO.setError(error);
			invoiceDownloadTicketDTO.setStatus(0);
		}
		return invoiceDownloadTicketDTO;
	}
	
	@RequestMapping(value = "/GetInvoiceRefund")
	public InvoiceRefundDTO getInvoiceRefund(HttpServletRequest request, HttpServletResponse response){		
		InvoiceRefundDTO invoiceRefundDTO = new InvoiceRefundDTO();
		Error error = new Error();		
		
		try{			
			String orderIdStr = request.getParameter("orderId");
			String brokerIdStr = request.getParameter("brokerId");
			
			if(StringUtils.isEmpty(orderIdStr)){
				System.err.println("Order ID not found.");
				error.setDescription("Order ID not found.");
				invoiceRefundDTO.setError(error);
				invoiceRefundDTO.setStatus(0);
				return invoiceRefundDTO;
			}
			Integer orderId = 0;
			try{
				orderId = Integer.parseInt(orderIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Order ID should be valid Integer value.");
				invoiceRefundDTO.setError(error);
				invoiceRefundDTO.setStatus(0);
				return invoiceRefundDTO;
			}			
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				invoiceRefundDTO.setError(error);
				invoiceRefundDTO.setStatus(0);
				return invoiceRefundDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				invoiceRefundDTO.setError(error);
				invoiceRefundDTO.setStatus(0);
				return invoiceRefundDTO;
			}
				
			CustomerOrder customerOrder = null;
			customerOrder = DAORegistry.getCustomerOrderDAO().getCustomerOrderByIdAndBrokerId(orderId, brokerId);				
			if(customerOrder == null){
				System.err.println("Customer Order not found.");
				error.setDescription("Customer Order not found.");
				invoiceRefundDTO.setError(error);
				invoiceRefundDTO.setStatus(0);
				return invoiceRefundDTO;
			}
			
			List<InvoiceRefund> invoiceRefundList = null;
			invoiceRefundList = DAORegistry.getInvoiceRefundDAO().getInvoiceRefundByOrderId(orderId);
			if(invoiceRefundList == null || invoiceRefundList.isEmpty()){
				System.err.println("There is No Refund are available for selected Invoice.");
				error.setDescription("There is No Refund are available for selected Invoice.");
				invoiceRefundDTO.setError(error);
				invoiceRefundDTO.setStatus(0);
				return invoiceRefundDTO;
			}
			
			if(invoiceRefundList != null){
				invoiceRefundDTO.setInvoiceRefundListDTO(com.rtw.tracker.utils.Util.getInvoiceRefundArray(invoiceRefundList));
			}
			invoiceRefundDTO.setStatus(1);
			invoiceRefundDTO.setInvoiceRefundPaginationDTO(PaginationUtil.getDummyPaginationParameters(invoiceRefundList!=null?invoiceRefundList.size():0));			
			invoiceRefundDTO.setOrderId(orderId);
			
		}catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Invoice Refund List.");
			invoiceRefundDTO.setError(error);
			invoiceRefundDTO.setStatus(0);
		}
		return invoiceRefundDTO;
	}
	
	@RequestMapping(value="/CreateInvoiceRefundOrCredit")
	public InvoiceRefundDetailsDTO createInvoiceRefundOrCredit(HttpServletRequest request, HttpServletResponse response){
		InvoiceRefundDetailsDTO invoiceRefundDetailsDTO = new InvoiceRefundDetailsDTO();
		Error error = new Error();
		
		try {
			String action = request.getParameter("action");
			String orderIdStr = request.getParameter("orderId");
			String userName = request.getParameter("userName");
			String brokerIdStr = request.getParameter("brokerId");
			
			if(StringUtils.isEmpty(orderIdStr)){
				System.err.println("Customer Order ID not found.");
				error.setDescription("Customer Order ID not found.");
				invoiceRefundDetailsDTO.setError(error);
				invoiceRefundDetailsDTO.setStatus(0);
				return invoiceRefundDetailsDTO;
			}
			Integer orderId = 0;
			try{
				orderId = Integer.parseInt(orderIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Customer Order ID should be valid Integer value.");
				invoiceRefundDetailsDTO.setError(error);
				invoiceRefundDetailsDTO.setStatus(0);
				return invoiceRefundDetailsDTO;
			}			
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				invoiceRefundDetailsDTO.setError(error);
				invoiceRefundDetailsDTO.setStatus(0);
				return invoiceRefundDetailsDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				invoiceRefundDetailsDTO.setError(error);
				invoiceRefundDetailsDTO.setStatus(0);
				return invoiceRefundDetailsDTO;
			}
			
			InvoiceRefund invoiceRefund=null;
			Double primaryAmountToDeduct=0.0;
			Double secondaryAmoutToDeduct=0.0;
			Double thirdAmountToDeduct=0.0;
			String msg = "";
			String errorMsg = "";
			
			CustomerOrder order = null;
			order = DAORegistry.getCustomerOrderDAO().getCustomerOrderByIdAndBrokerId(orderId, brokerId);
			if(order == null){
				System.err.println("Customer Order not found.");
				error.setDescription("Customer Order not found.");
				invoiceRefundDetailsDTO.setError(error);
				invoiceRefundDetailsDTO.setStatus(0);
				return invoiceRefundDetailsDTO;
			}
			
			Invoice invoice = null;
			invoice = DAORegistry.getInvoiceDAO().getInvoiceByOrderId(orderId);
			if(invoice == null){
				System.err.println("Invoice not found.");
				error.setDescription("Invoice not found.");
				invoiceRefundDetailsDTO.setError(error);
				invoiceRefundDetailsDTO.setStatus(0);
				return invoiceRefundDetailsDTO;
			}
			
			Customer customer = null;
			customer = DAORegistry.getCustomerDAO().get(order.getCustomerId());
			if(customer == null){
				System.err.println("Customer not found.");
				error.setDescription("Customer not found.");
				invoiceRefundDetailsDTO.setError(error);
				invoiceRefundDetailsDTO.setStatus(0);
				return invoiceRefundDetailsDTO;
			}
			
			if(action!=null && !action.isEmpty()){
				
				String refundAmountStr = request.getParameter("refundAmount");
				String walletCCAmtStr = request.getParameter("creditWalletAmount");
				String creditAmountStr = request.getParameter("creditAmount");
				String rewardPointsStr = request.getParameter("rewardPoints");
				String note = request.getParameter("refundNote");
				
				Double refundAmount=0.0;
				Double creditWalletAmount=0.0;
				Double creditAmount=0.00;
				Double rewardPoints=0.00;
				
				if(refundAmountStr!=null && !refundAmountStr.isEmpty()){
					refundAmount = Double.parseDouble(refundAmountStr);
				}
				if(walletCCAmtStr!=null && !walletCCAmtStr.isEmpty()){
					creditWalletAmount = Double.parseDouble(walletCCAmtStr);
				}
				if(creditAmountStr!=null && !creditAmountStr.isEmpty()){
					creditAmount = Double.parseDouble(creditAmountStr);
				}
				if(rewardPointsStr!=null && !rewardPointsStr.isEmpty()){
					rewardPoints = Double.parseDouble(rewardPointsStr);
				}
				CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(order.getCustomerId());
				Date today = new Date();
				
				
//				if(action.equals(PaymentMethod.CREDITCARD.toString()) || action.equals(PaymentMethod.PAYPAL.toString())){
//					if((refundAmount+creditAmount) <= invoice.getInvoiceTotal() && 
//							(refundAmount+creditAmount) > 0){
//						if(refundAmount>0){
//							String refundType = "";
//							if(order.getPrimaryPaymentMethod().equals(PaymentMethod.CREDITCARD) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)){
//								refundType = PaymentMethod.CREDITCARD.toString();
//							}else if(order.getPrimaryPaymentMethod().equals(PaymentMethod.PAYPAL) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.PAYPAL)){
//								refundType = PaymentMethod.PAYPAL.toString();
//							}
//
//							Map<String, String> paramMap = Util.getParameterMap(request);
//							paramMap.put("orderId", String.valueOf(order.getId()));
//							paramMap.put("refundType", refundType);
//							paramMap.put("refundAmount",String.valueOf(Util.formatDoubleToTwoDecimalPoint(refundAmount)));
//							String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.INVOICE_REFUND);
//							Gson gson = Util.getGsonBuilder().create();	
//							JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
//							InvoiceRefundResponse invoiceRefundResponse = gson.fromJson(((JsonObject)jsonObject.get("invoiceRefundResponse")), InvoiceRefundResponse.class);
//							if(invoiceRefundResponse!=null && invoiceRefundResponse.getStatus()==1){
//								if(invoiceRefundResponse.getInvoiceRefund() != null){
//									primaryAmountToDeduct = primaryAmountToDeduct+refundAmount;
//								}
//							}else{
//								msg = invoiceRefundResponse.getError().getDescription();
//							}
//						}
//						if(creditAmount > 0){
//							Map<String, String> paramMap = Util.getParameterMap(request);
//							paramMap.put("orderId", String.valueOf(order.getId()));
//							paramMap.put("transactionAmount",String.valueOf(creditAmount));
//							paramMap.put("customerId",String.valueOf(customer.getId()));
//							paramMap.put("transactionType","CREDIT");
//							paramMap.put("userName",userName);
//							String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.CUSTOMER_WALLET_TRANSACTION);
//							
//							/*Gson gson = new Gson();
//							JsonObject cardJsonObject = gson.fromJson(data, JsonObject.class);
//							WalletTransaction walletTransaction = gson.fromJson(((JsonObject)cardJsonObject.get("walletTransaction")), WalletTransaction.class);
//							CustomerWallet wallet = walletTransaction.getWallet();
//							CustomerWalletHistory walletHistory = walletTransaction.getWalletHistory();
//							/*CustomerWallet wallet = DAORegistry.getCustomerWalletDAO().getCustomerWalletByCustomerId(customer.getId());
//							CustomerWalletHistory walletHistory = new CustomerWalletHistory();
//							walletHistory.setCreatedOn(today);
//							walletHistory.setTransactionAmount(creditAmount);
//							walletHistory.setCustomerId(customer.getId());
//							walletHistory.setTransactionType("CREDIT");
//							walletHistory.setUserName(userName);
//							walletHistory.setOrderId(order.getId());
//							DAORegistry.getCustomerWalletHistoryDAO().save(walletHistory);
//							if(wallet==null){
//								wallet = new CustomerWallet();
//								wallet.setCustomerId(customer.getId());
//								wallet.setActiveCredit(creditAmount);
//								wallet.setTotalCredit(creditAmount);
//								wallet.setTotalUsedCredit(0.00);
//								wallet.setLastUpdated(today);
//								DAORegistry.getCustomerWalletDAO().save(wallet);
//							}else{
//								wallet.setActiveCredit(wallet.getActiveCredit() + creditAmount);
//								wallet.setLastUpdated(today);
//								wallet.setTotalCredit(wallet.getTotalCredit() + creditAmount);
//								DAORegistry.getCustomerWalletDAO().update(wallet);
//							}
//							primaryAmountToDeduct = primaryAmountToDeduct + creditAmount;
//						}
//					}
//				}else if(action.equals(PaymentMethod.PARTIAL_REWARDS.toString())){
//					
//					
//					if((refundAmount+creditAmount+rewardPoints) <= invoice.getInvoiceTotal() && 
//							(refundAmount+creditAmount+rewardPoints) > 0){
//						if(refundAmount>0){
//							String refundType = "";
//							if(order.getPrimaryPaymentMethod().equals(PaymentMethod.CREDITCARD) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)){
//								refundType = PaymentMethod.CREDITCARD.toString();
//							}else if(order.getPrimaryPaymentMethod().equals(PaymentMethod.PAYPAL) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.PAYPAL)){
//								refundType = PaymentMethod.PAYPAL.toString();
//							}
//							Map<String, String> paramMap = Util.getParameterMap(request);
//							paramMap.put("orderId", String.valueOf(order.getId()));
//							paramMap.put("refundType", refundType);
//							paramMap.put("refundAmount",String.valueOf(refundAmount));
//							String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.INVOICE_REFUND);
//							Gson gson = Util.getGsonBuilder().create();	
//							JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
//							InvoiceRefundResponse invoiceRefundResponse = gson.fromJson(((JsonObject)jsonObject.get("invoiceRefundResponse")), InvoiceRefundResponse.class);
//							if(invoiceRefundResponse!=null && invoiceRefundResponse.getStatus()==1){
//								if(invoiceRefundResponse.getInvoiceRefund() != null){
//									secondaryAmoutToDeduct = secondaryAmoutToDeduct+refundAmount;
//								}
//							}else{
//								msg = invoiceRefundResponse.getError().getDescription();
//							}
//						}
//						if(rewardPoints <= invoice.getInvoiceTotal()){
//							customerLoyalty.setActivePoints(customerLoyalty.getActivePoints()+rewardPoints);
//							customerLoyalty.setRevertedSpentPoints(customerLoyalty.getRevertedSpentPoints() + rewardPoints);
//							//DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
//							if(invoiceRefund!=null){
//								invoiceRefund.setRevertedUsedRewardPoints(rewardPoints);
//							}
//							primaryAmountToDeduct = primaryAmountToDeduct + rewardPoints ;
//						}
//						if(creditAmount > 0){
//							Map<String, String> paramMap = Util.getParameterMap(request);
//							paramMap.put("orderId", String.valueOf(order.getId()));
//							paramMap.put("transactionAmount",String.valueOf(creditAmount));
//							paramMap.put("customerId",String.valueOf(customer.getId()));
//							paramMap.put("transactionType","CREDIT");
//							paramMap.put("userName",userName);
//							String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.CUSTOMER_WALLET_TRANSACTION);
							 
//							Gson gson = new Gson();
//							JsonObject cardJsonObject = gson.fromJson(data, JsonObject.class);
//							WalletTransaction walletTransaction = gson.fromJson(((JsonObject)cardJsonObject.get("walletTransaction")), WalletTransaction.class);
//							CustomerWallet wallet = walletTransaction.getWallet();
//							CustomerWalletHistory walletHistory = walletTransaction.getWalletHistory();
//							walletHistory.setCreatedOn(today);
//							walletHistory.setTransactionAmount(creditAmount);
//							walletHistory.setCustomerId(customer.getId());
//							walletHistory.setTransactionType("CREDITED");
//							walletHistory.setUserName(userName);
//							walletHistory.setOrderId(order.getId());
//							DAORegistry.getCustomerWalletHistoryDAO().save(walletHistory);
//							if(wallet==null){
//								wallet = new CustomerWallet();
//								wallet.setCustomerId(customer.getId());
//								wallet.setActiveCredit(creditAmount);
//								wallet.setTotalCredit(creditAmount);
//								wallet.setTotalUsedCredit(0.00);
//								wallet.setLastUpdated(today);
//								DAORegistry.getCustomerWalletDAO().save(wallet);
//							}else{
//								wallet.setActiveCredit(wallet.getActiveCredit() + creditAmount);
//								wallet.setLastUpdated(today);
//								wallet.setTotalCredit(wallet.getTotalCredit() + creditAmount);
//								DAORegistry.getCustomerWalletDAO().update(wallet);
//							}
							
//							secondaryAmoutToDeduct = secondaryAmoutToDeduct+creditAmount;
//						}
//					}
//				}else if(action.equals(PaymentMethod.FULL_REWARDS.toString())){
//					if(rewardPoints <= invoice.getInvoiceTotal()){
//						customerLoyalty.setActivePoints(customerLoyalty.getActivePoints()+rewardPoints);
//						customerLoyalty.setRevertedSpentPoints(customerLoyalty.getRevertedSpentPoints() + rewardPoints);
//						//DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
//						primaryAmountToDeduct = primaryAmountToDeduct +  rewardPoints;
//					}
//				}
				
				if(action.equals(PaymentMethod.PARTIAL_REWARDS.toString())){
					if(order != null && (order.getPrimaryPaymentMethod().equals(PaymentMethod.CREDITCARD) || order.getPrimaryPaymentMethod().equals(PaymentMethod.PAYPAL)
							|| order.getPrimaryPaymentMethod().equals(PaymentMethod.GOOGLEPAY) || order.getPrimaryPaymentMethod().equals(PaymentMethod.IPAY))){
						if((refundAmount+creditWalletAmount) <= invoice.getInvoiceTotal() && 
								(refundAmount+creditWalletAmount) > 0){
							
							if(refundAmount > 0){
								String refundType = "";
								if(order.getPrimaryPaymentMethod().equals(PaymentMethod.CREDITCARD) || order.getPrimaryPaymentMethod().equals(PaymentMethod.GOOGLEPAY) 
										|| order.getPrimaryPaymentMethod().equals(PaymentMethod.IPAY)){
									refundType = PaymentMethod.CREDITCARD.toString();
								}else if(order.getPrimaryPaymentMethod().equals(PaymentMethod.PAYPAL)){
									refundType = PaymentMethod.PAYPAL.toString();
								}

								Map<String, String> paramMap = Util.getParameterMap(request);
								paramMap.put("orderId", String.valueOf(order.getId()));
								paramMap.put("refundType", refundType);
								paramMap.put("refundAmount",String.valueOf(Util.formatDoubleToTwoDecimalPoint(refundAmount)));
								
								String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.INVOICE_REFUND);								
								Gson gson = Util.getGsonBuilder().create();	
								JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
								InvoiceRefundResponse invoiceRefundResponse = gson.fromJson(((JsonObject)jsonObject.get("invoiceRefundResponse")), InvoiceRefundResponse.class);
								
								if(invoiceRefundResponse == null){
									System.err.println("Invoice Refund API not available.");
									error.setDescription("Invoice Refund API not available.");
									invoiceRefundDetailsDTO.setError(error);
									invoiceRefundDetailsDTO.setStatus(0);
									return invoiceRefundDetailsDTO;
								}else if(invoiceRefundResponse.getStatus()==1){
									invoiceRefund = invoiceRefundResponse.getInvoiceRefund();
									if(invoiceRefund != null){
										invoiceRefund.setRefundedBy(userName);
										primaryAmountToDeduct = primaryAmountToDeduct + refundAmount;
									}
								}else{
									errorMsg = invoiceRefundResponse.getError().getDescription();
								}
							}
							if(creditWalletAmount > 0){
								Map<String, String> paramMap = Util.getParameterMap(request);
								paramMap.put("orderId", String.valueOf(order.getId()));
								paramMap.put("transactionAmount",String.valueOf(creditWalletAmount));
								paramMap.put("customerId",String.valueOf(customer.getId()));
								paramMap.put("transactionType","CREDIT");
								paramMap.put("userName",userName);
								
								String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.CUSTOMER_WALLET_TRANSACTION);
								Gson gson = Util.getGsonBuilder().create();	
								JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
								WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
								
								if(walletTransaction == null){
									System.err.println("Wallet Transaction API not available.");
									error.setDescription("Wallet Transaction API not available.");
									invoiceRefundDetailsDTO.setError(error);
									invoiceRefundDetailsDTO.setStatus(0);
									return invoiceRefundDetailsDTO;
								}else if(walletTransaction.getStatus() == 1){
									primaryAmountToDeduct = primaryAmountToDeduct + creditWalletAmount;
								}else{
									errorMsg = walletTransaction.getError().getDescription();
								}
							}
						}							
					}
					else if(order != null && order.getPrimaryPaymentMethod().equals(PaymentMethod.CUSTOMER_WALLET)){
						if(order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
								|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.IPAY)){
							
							if((refundAmount+creditWalletAmount+creditAmount) <= invoice.getInvoiceTotal() && 
									(refundAmount+creditWalletAmount+creditAmount) > 0){
								
								if(refundAmount > 0){
									String refundType = "";
									if(order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
											|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.IPAY)){
										refundType = PaymentMethod.CREDITCARD.toString();
									}

									Map<String, String> paramMap = Util.getParameterMap(request);
									paramMap.put("orderId", String.valueOf(order.getId()));
									paramMap.put("refundType", refundType);
									paramMap.put("refundAmount",String.valueOf(Util.formatDoubleToTwoDecimalPoint(refundAmount)));
									
									String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.INVOICE_REFUND);
									Gson gson = Util.getGsonBuilder().create();	
									JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
									InvoiceRefundResponse invoiceRefundResponse = gson.fromJson(((JsonObject)jsonObject.get("invoiceRefundResponse")), InvoiceRefundResponse.class);
									
									if(invoiceRefundResponse == null){
										System.err.println("Invoice Refund API not available.");
										error.setDescription("Invoice Refund API not available.");
										invoiceRefundDetailsDTO.setError(error);
										invoiceRefundDetailsDTO.setStatus(0);
										return invoiceRefundDetailsDTO;
									}else if(invoiceRefundResponse.getStatus()==1){
										invoiceRefund = invoiceRefundResponse.getInvoiceRefund();
										if(invoiceRefund != null){
											invoiceRefund.setRefundedBy(userName);
											secondaryAmoutToDeduct = secondaryAmoutToDeduct + refundAmount;
										}
									}else{
										errorMsg = invoiceRefundResponse.getError().getDescription();
									}
								}
								if(creditWalletAmount > 0){
									Map<String, String> paramMap = Util.getParameterMap(request);
									paramMap.put("orderId", String.valueOf(order.getId()));
									paramMap.put("transactionAmount",String.valueOf(creditWalletAmount));
									paramMap.put("customerId",String.valueOf(customer.getId()));
									paramMap.put("transactionType","CREDIT");
									paramMap.put("userName",userName);
									
									String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.CUSTOMER_WALLET_TRANSACTION);
									Gson gson = Util.getGsonBuilder().create();	
									JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
									WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
									
									if(walletTransaction == null){
										System.err.println("Wallet Transaction API not available.");
										error.setDescription("Wallet Transaction API not available.");
										invoiceRefundDetailsDTO.setError(error);
										invoiceRefundDetailsDTO.setStatus(0);
										return invoiceRefundDetailsDTO;
									}else if(walletTransaction.getStatus() == 1){
										secondaryAmoutToDeduct = secondaryAmoutToDeduct + creditWalletAmount;
									}else{
										errorMsg = walletTransaction.getError().getDescription();
									}
								}
								if(creditAmount > 0){
									Map<String, String> paramMap = Util.getParameterMap(request);
									paramMap.put("orderId", String.valueOf(order.getId()));
									paramMap.put("transactionAmount",String.valueOf(creditAmount));
									paramMap.put("customerId",String.valueOf(customer.getId()));
									paramMap.put("transactionType","CREDIT");
									paramMap.put("userName",userName);
									
									String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.CUSTOMER_WALLET_TRANSACTION);
									Gson gson = Util.getGsonBuilder().create();	
									JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
									WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
									
									if(walletTransaction == null){
										System.err.println("Wallet Transaction API not available.");
										error.setDescription("Wallet Transaction API not available.");
										invoiceRefundDetailsDTO.setError(error);
										invoiceRefundDetailsDTO.setStatus(0);
										return invoiceRefundDetailsDTO;
									}else if(walletTransaction.getStatus() == 1){
										primaryAmountToDeduct = primaryAmountToDeduct + creditAmount;
									}else{
										errorMsg = walletTransaction.getError().getDescription();
									}
								}
							}
						}
						else{
							if(creditAmount > 0){
								Map<String, String> paramMap = Util.getParameterMap(request);
								paramMap.put("orderId", String.valueOf(order.getId()));
								paramMap.put("transactionAmount",String.valueOf(creditAmount));
								paramMap.put("customerId",String.valueOf(customer.getId()));
								paramMap.put("transactionType","CREDIT");
								paramMap.put("userName",userName);
								
								String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.CUSTOMER_WALLET_TRANSACTION);
								Gson gson = Util.getGsonBuilder().create();	
								JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
								WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
								
								if(walletTransaction == null){
									System.err.println("Wallet Transaction API not available.");
									error.setDescription("Wallet Transaction API not available.");
									invoiceRefundDetailsDTO.setError(error);
									invoiceRefundDetailsDTO.setStatus(0);
									return invoiceRefundDetailsDTO;
								}else if(walletTransaction.getStatus() == 1){
									primaryAmountToDeduct = primaryAmountToDeduct + creditAmount;
								}else{
									errorMsg = walletTransaction.getError().getDescription();
								}
							}
						}
					}
					else if(order != null && order.getPrimaryPaymentMethod().equals(PaymentMethod.FULL_REWARDS)){
						if(rewardPoints <= invoice.getInvoiceTotal() && rewardPoints > 0){
							customerLoyalty.setActivePoints(customerLoyalty.getActivePoints()+rewardPoints);
							customerLoyalty.setRevertedSpentPoints(customerLoyalty.getRevertedSpentPoints() + rewardPoints);
							if(invoiceRefund!=null){
								invoiceRefund.setRevertedUsedRewardPoints(rewardPoints);
							}
							primaryAmountToDeduct = primaryAmountToDeduct + rewardPoints ;
						}
					}
					else if(order != null && order.getPrimaryPaymentMethod().equals(PaymentMethod.PARTIAL_REWARDS)){
						if(order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.PAYPAL)
								|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.IPAY)){
							if((refundAmount+creditWalletAmount+rewardPoints) <= invoice.getInvoiceTotal() && 
									(refundAmount+creditWalletAmount+rewardPoints) > 0){

								if(rewardPoints <= invoice.getInvoiceTotal()){
									customerLoyalty.setActivePoints(customerLoyalty.getActivePoints()+rewardPoints);
									customerLoyalty.setRevertedSpentPoints(customerLoyalty.getRevertedSpentPoints() + rewardPoints);
									if(invoiceRefund!=null){
										invoiceRefund.setRevertedUsedRewardPoints(rewardPoints);
									}
									primaryAmountToDeduct = primaryAmountToDeduct + rewardPoints ;
								}
								if(refundAmount > 0){
									String refundType = "";
									if(order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
											|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.IPAY)){
										refundType = PaymentMethod.CREDITCARD.toString();
									}

									Map<String, String> paramMap = Util.getParameterMap(request);
									paramMap.put("orderId", String.valueOf(order.getId()));
									paramMap.put("refundType", refundType);
									paramMap.put("refundAmount",String.valueOf(Util.formatDoubleToTwoDecimalPoint(refundAmount)));
									
									String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.INVOICE_REFUND);
									Gson gson = Util.getGsonBuilder().create();	
									JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
									InvoiceRefundResponse invoiceRefundResponse = gson.fromJson(((JsonObject)jsonObject.get("invoiceRefundResponse")), InvoiceRefundResponse.class);
									
									if(invoiceRefundResponse == null){
										System.err.println("Invoice Refund API not available.");
										error.setDescription("Invoice Refund API not available.");
										invoiceRefundDetailsDTO.setError(error);
										invoiceRefundDetailsDTO.setStatus(0);
										return invoiceRefundDetailsDTO;
									}else if(invoiceRefundResponse.getStatus()==1){
										invoiceRefund = invoiceRefundResponse.getInvoiceRefund();
										if(invoiceRefund != null){
											invoiceRefund.setRefundedBy(userName);
											secondaryAmoutToDeduct = secondaryAmoutToDeduct + refundAmount;
										}
									}else{
										errorMsg = invoiceRefundResponse.getError().getDescription();
									}
								}
								if(creditWalletAmount > 0){
									Map<String, String> paramMap = Util.getParameterMap(request);
									paramMap.put("orderId", String.valueOf(order.getId()));
									paramMap.put("transactionAmount",String.valueOf(creditWalletAmount));
									paramMap.put("customerId",String.valueOf(customer.getId()));
									paramMap.put("transactionType","CREDIT");
									paramMap.put("userName",userName);
									
									String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.CUSTOMER_WALLET_TRANSACTION);
									Gson gson = Util.getGsonBuilder().create();	
									JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
									WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
									
									if(walletTransaction == null){
										System.err.println("Wallet Transaction API not available.");
										error.setDescription("Wallet Transaction API not available.");
										invoiceRefundDetailsDTO.setError(error);
										invoiceRefundDetailsDTO.setStatus(0);
										return invoiceRefundDetailsDTO;
									}else if(walletTransaction.getStatus() == 1){
										secondaryAmoutToDeduct = secondaryAmoutToDeduct + creditWalletAmount;
									}else{
										errorMsg = walletTransaction.getError().getDescription();
									}
								}
							}
						}
						else if(order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CUSTOMER_WALLET) && 
								order.getThirdPaymentMethod()!= null && (order.getThirdPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
										|| order.getThirdPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY) || order.getThirdPaymentMethod().equals(PartialPaymentMethod.IPAY))){
							if((refundAmount+creditWalletAmount+creditAmount+rewardPoints) <= invoice.getInvoiceTotal() && 
									(refundAmount+creditWalletAmount+creditAmount+rewardPoints) > 0){

								if(rewardPoints > 0 && rewardPoints <= invoice.getInvoiceTotal()){
									customerLoyalty.setActivePoints(customerLoyalty.getActivePoints()+rewardPoints);
									customerLoyalty.setRevertedSpentPoints(customerLoyalty.getRevertedSpentPoints() + rewardPoints);
									if(invoiceRefund!=null){
										invoiceRefund.setRevertedUsedRewardPoints(rewardPoints);
									}
									primaryAmountToDeduct = primaryAmountToDeduct + rewardPoints ;
								}
								if(creditAmount > 0){
									Map<String, String> paramMap = Util.getParameterMap(request);
									paramMap.put("orderId", String.valueOf(order.getId()));
									paramMap.put("transactionAmount",String.valueOf(creditAmount));
									paramMap.put("customerId",String.valueOf(customer.getId()));
									paramMap.put("transactionType","CREDIT");
									paramMap.put("userName",userName);
									
									String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.CUSTOMER_WALLET_TRANSACTION);
									Gson gson = Util.getGsonBuilder().create();	
									JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
									WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
									
									if(walletTransaction == null){
										System.err.println("Wallet Transaction API not available.");
										error.setDescription("Wallet Transaction API not available.");
										invoiceRefundDetailsDTO.setError(error);
										invoiceRefundDetailsDTO.setStatus(0);
										return invoiceRefundDetailsDTO;
									}else if(walletTransaction.getStatus() == 1){
										secondaryAmoutToDeduct = secondaryAmoutToDeduct + creditAmount;
									}else{
										errorMsg = walletTransaction.getError().getDescription();
									}
								}
								if(refundAmount > 0){
									String refundType = "";
									if(order.getThirdPaymentMethod()!= null && (order.getThirdPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
											|| order.getThirdPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY) || order.getThirdPaymentMethod().equals(PartialPaymentMethod.IPAY))){
										refundType = PaymentMethod.CREDITCARD.toString();
									}

									Map<String, String> paramMap = Util.getParameterMap(request);
									paramMap.put("orderId", String.valueOf(order.getId()));
									paramMap.put("refundType", refundType);
									paramMap.put("refundAmount",String.valueOf(Util.formatDoubleToTwoDecimalPoint(refundAmount)));
									
									String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.INVOICE_REFUND);
									Gson gson = Util.getGsonBuilder().create();	
									JsonObject  jsonObject = gson.fromJson(data, JsonObject.class);
									InvoiceRefundResponse invoiceRefundResponse = gson.fromJson(((JsonObject)jsonObject.get("invoiceRefundResponse")), InvoiceRefundResponse.class);
									
									if(invoiceRefundResponse == null){
										System.err.println("Invoice Refund API not available.");
										error.setDescription("Invoice Refund API not available.");
										invoiceRefundDetailsDTO.setError(error);
										invoiceRefundDetailsDTO.setStatus(0);
										return invoiceRefundDetailsDTO;
									}else if(invoiceRefundResponse.getStatus()==1){
										invoiceRefund = invoiceRefundResponse.getInvoiceRefund();
										if(invoiceRefund != null){
											invoiceRefund.setRefundedBy(userName);
											thirdAmountToDeduct = thirdAmountToDeduct + refundAmount;
										}
									}else{
										errorMsg = invoiceRefundResponse.getError().getDescription();
									}
								}
								if(creditWalletAmount > 0){
									Map<String, String> paramMap = Util.getParameterMap(request);
									paramMap.put("orderId", String.valueOf(order.getId()));
									paramMap.put("transactionAmount",String.valueOf(creditWalletAmount));
									paramMap.put("customerId",String.valueOf(customer.getId()));
									paramMap.put("transactionType","CREDIT");
									paramMap.put("userName",userName);
									
									String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.CUSTOMER_WALLET_TRANSACTION);
									Gson gson = Util.getGsonBuilder().create();	
									JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
									WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
									
									if(walletTransaction == null){
										System.err.println("Wallet Transaction API not available.");
										error.setDescription("Wallet Transaction API not available.");
										invoiceRefundDetailsDTO.setError(error);
										invoiceRefundDetailsDTO.setStatus(0);
										return invoiceRefundDetailsDTO;
									}else if(walletTransaction.getStatus() == 1){
										thirdAmountToDeduct = thirdAmountToDeduct + creditWalletAmount;
									}else{
										errorMsg = walletTransaction.getError().getDescription();
									}
								}
							}
						}
						else if(order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CUSTOMER_WALLET)){
							if((creditAmount+rewardPoints) <= invoice.getInvoiceTotal() && 
									(creditAmount+rewardPoints) > 0){
								
								if(rewardPoints > 0 && rewardPoints <= invoice.getInvoiceTotal()){
									customerLoyalty.setActivePoints(customerLoyalty.getActivePoints()+rewardPoints);
									customerLoyalty.setRevertedSpentPoints(customerLoyalty.getRevertedSpentPoints() + rewardPoints);
									if(invoiceRefund!=null){
										invoiceRefund.setRevertedUsedRewardPoints(rewardPoints);
									}
									primaryAmountToDeduct = primaryAmountToDeduct + rewardPoints ;
								}
								if(creditAmount > 0){
									Map<String, String> paramMap = Util.getParameterMap(request);
									paramMap.put("orderId", String.valueOf(order.getId()));
									paramMap.put("transactionAmount",String.valueOf(creditAmount));
									paramMap.put("customerId",String.valueOf(customer.getId()));
									paramMap.put("transactionType","CREDIT");
									paramMap.put("userName",userName);
									
									String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.CUSTOMER_WALLET_TRANSACTION);
									Gson gson = Util.getGsonBuilder().create();	
									JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
									WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
									
									if(walletTransaction == null){
										System.err.println("Wallet Transaction API not available.");
										error.setDescription("Wallet Transaction API not available.");
										invoiceRefundDetailsDTO.setError(error);
										invoiceRefundDetailsDTO.setStatus(0);
										return invoiceRefundDetailsDTO;
									}else if(walletTransaction.getStatus() == 1){
										secondaryAmoutToDeduct = secondaryAmoutToDeduct + creditAmount;
									}else{
										errorMsg = walletTransaction.getError().getDescription();
									}
								}
							}
						}
					}						
				//}
				
				if(errorMsg.isEmpty()){
					order.setPrimaryRefundAmount(order.getPrimaryRefundAmount()!=null?order.getPrimaryRefundAmount()+primaryAmountToDeduct:primaryAmountToDeduct);
					order.setSecondaryRefundAmount(order.getSecondaryRefundAmount()!=null?order.getSecondaryRefundAmount()+secondaryAmoutToDeduct:secondaryAmoutToDeduct);
					order.setThirdRefundAmount(order.getThirdRefundAmount()!=null?order.getThirdRefundAmount()+thirdAmountToDeduct:thirdAmountToDeduct);
					//order.setOrderTotal(order.getOrderTotal()-(primaryAmountToDeduct + secondaryAmoutToDeduct + thirdAmountToDeduct));
					
					invoice.setInvoiceTotal(invoice.getInvoiceTotal()-(primaryAmountToDeduct + secondaryAmoutToDeduct+ thirdAmountToDeduct));
					invoice.setRefundAmount(invoice.getRefundAmount()!=null?invoice.getRefundAmount()+(primaryAmountToDeduct + secondaryAmoutToDeduct+ thirdAmountToDeduct)
							:(primaryAmountToDeduct + secondaryAmoutToDeduct+ thirdAmountToDeduct));
					invoice.setLastUpdated(today);
					invoice.setLastUpdatedBy(userName);
					if(note!=null && !note.isEmpty()){
						invoice.setInternalNote(note);
					}
					
					if(invoiceRefund!=null){
						invoiceRefund.setOrderId(order.getId());
						DAORegistry.getInvoiceRefundDAO().save(invoiceRefund);
					}
					DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
					DAORegistry.getCustomerOrderDAO().update(order);
					DAORegistry.getInvoiceDAO().update(invoice);
					
					String logMsg= "";
					if(msg.isEmpty()){
						if(refundAmountStr!=null && !refundAmountStr.isEmpty()){
							msg =msg +"Refunded($"+refundAmountStr+"),";
							logMsg =logMsg +"Refunded($"+refundAmountStr+"),";
						}
						if(walletCCAmtStr!=null && !walletCCAmtStr.isEmpty()){
							msg =msg +"Wallet credited from Creditcard/paypal($"+walletCCAmtStr+"),";
							logMsg =logMsg +"Wallet credited from Creditcard/paypal($"+walletCCAmtStr+"),";
						}
						if(creditAmountStr!=null && !creditAmountStr.isEmpty()){
							msg =msg +"Wallet credited from Wallet($"+creditAmountStr+"),";	
							logMsg =logMsg +"Wallet credited from Wallet($"+creditAmountStr+"),";	
						}						
						if(rewardPointsStr!=null && !rewardPointsStr.isEmpty()){
							msg =msg +"Reverted Reward Points($"+rewardPointsStr+")";
							logMsg =logMsg +"Reverted Reward Points($"+rewardPointsStr+")";
						}
					}
					
					InvoiceAudit audit = new InvoiceAudit(invoice);
					audit.setAction(InvoiceAuditAction.INVOICE_REFUNDED);
					audit.setNote("Invoice is "+logMsg);
					audit.setCreatedDate(new Date());
					audit.setCreateBy(userName);
					DAORegistry.getInvoiceAuditDAO().save(audit);
					
					invoiceRefundDetailsDTO.setMessage("Action is successfully completed. Amount "+msg);
					
					//Tracking User Action
					String userActionMsg = "Invoice Refunded. "+logMsg;
					Util.userActionAudit(request, invoice.getId(), userActionMsg);
					
					//jObj.put("order", order);
					//jObj.put("invoice", invoice);
					//jObj.put("customer", customer);
					if(order != null){						
						invoiceRefundDetailsDTO.setCustomerOrderDTO(com.rtw.tracker.utils.Util.getCustomerOrderObject(order));
					}						
					if(invoice != null){
						InvoiceDTO invoiceDTO = new InvoiceDTO();					
						invoiceDTO.setInvoiceId(invoice.getId());
						invoiceDTO.setInvoiceInternalNote(invoice.getInternalNote());
						
						invoiceRefundDetailsDTO.setInvoiceDTO(invoiceDTO);
					}						
					if(customer != null){
						CustomersCustomDTO customerDTO = new CustomersCustomDTO();					
						customerDTO.setCustomerName(customer.getCustomerName());
						customerDTO.setLastName(customer.getLastName());
						customerDTO.setEmail(customer.getEmail());
						customerDTO.setPhone(customer.getPhone());
						
						invoiceRefundDetailsDTO.setCustomerDTO(customerDTO);
					}
					
					invoiceRefundDetailsDTO.setStatus(1);
					invoiceRefundDetailsDTO.setIsValid(true);
					return invoiceRefundDetailsDTO;
					//return "page-invoice-refund";
					}else{
						//jObj.put("msg", errorMsg);						
						error.setDescription(errorMsg);
						invoiceRefundDetailsDTO.setError(error);
						invoiceRefundDetailsDTO.setStatus(0);
						return invoiceRefundDetailsDTO;
					}
				}
			}
			if(invoice != null && invoice.getStatus().equals(InvoiceStatus.Voided)){
				invoiceRefundDetailsDTO.setIsValid(false);
				error.setDescription("Selected Invoice is voided, Cannot make refund on void invoice.");
				invoiceRefundDetailsDTO.setError(error);
				invoiceRefundDetailsDTO.setStatus(0);
				
			}
			else if(order != null && (order.getPrimaryPaymentMethod().equals(PaymentMethod.CREDITCARD)
					|| order.getPrimaryPaymentMethod().equals(PaymentMethod.GOOGLEPAY)
					|| order.getPrimaryPaymentMethod().equals(PaymentMethod.IPAY)
					|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
					|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY)
					|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.IPAY)
					|| (order.getThirdPaymentMethod()!=null && (order.getThirdPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
					|| order.getThirdPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY)
					|| order.getThirdPaymentMethod().equals(PartialPaymentMethod.IPAY)))
					|| order.getPrimaryPaymentMethod().equals(PaymentMethod.PAYPAL) 
					|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.PAYPAL)
					|| order.getPrimaryPaymentMethod().equals(PaymentMethod.FULL_REWARDS)						
					|| order.getPrimaryPaymentMethod().equals(PaymentMethod.CUSTOMER_WALLET)
					|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CUSTOMER_WALLET)
					|| order.getPrimaryPaymentMethod().equals(PaymentMethod.PARTIAL_REWARDS))){
				if(invoice.getInvoiceTotal() > 0){
					if(order != null){						
						invoiceRefundDetailsDTO.setCustomerOrderDTO(com.rtw.tracker.utils.Util.getCustomerOrderObject(order));
					}						
					if(invoice != null){
						InvoiceDTO invoiceDTO = new InvoiceDTO();					
						invoiceDTO.setInvoiceId(invoice.getId());
						invoiceDTO.setInvoiceInternalNote(invoice.getInternalNote());
						
						invoiceRefundDetailsDTO.setInvoiceDTO(invoiceDTO);
					}						
					if(customer != null){
						CustomersCustomDTO customerDTO = new CustomersCustomDTO();					
						customerDTO.setCustomerName(customer.getCustomerName());
						customerDTO.setLastName(customer.getLastName());
						customerDTO.setEmail(customer.getEmail());
						customerDTO.setPhone(customer.getPhone());
						
						invoiceRefundDetailsDTO.setCustomerDTO(customerDTO);
					}
					
					invoiceRefundDetailsDTO.setStatus(1);
					invoiceRefundDetailsDTO.setIsValid(true);
				}else{					
					invoiceRefundDetailsDTO.setIsValid(false);
					error.setDescription("Cannot refund as selected invoice having invoiceTotal 0.");
					invoiceRefundDetailsDTO.setError(error);
					invoiceRefundDetailsDTO.setStatus(0);
				}
			}else{
				invoiceRefundDetailsDTO.setIsValid(false);
				error.setDescription("Cannot refund as selected invoice payment is made using "+order.getPrimaryPaymentMethod().toString());
				invoiceRefundDetailsDTO.setError(error);
				invoiceRefundDetailsDTO.setStatus(0);
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Invoice Refund Details.");
			invoiceRefundDetailsDTO.setError(error);
			invoiceRefundDetailsDTO.setStatus(0);
		}
		return invoiceRefundDetailsDTO;
	}
		
	@RequestMapping(value = "/GetCustomerOrderDetails")
	public CustomerOrderDetailsDTO getCustomerOrderDetails(HttpServletRequest request, HttpServletResponse response){
		CustomerOrderDetailsDTO customerOrderDetailsDTO = new CustomerOrderDetailsDTO();
		Error error = new Error();
		
		try {
			String action = request.getParameter("action");
			String invoiceIdStr = request.getParameter("invoiceId");
			String orderNoStr = request.getParameter("orderNo");
			String userName = request.getParameter("userName");
			String brokerIdStr = request.getParameter("brokerId");
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				customerOrderDetailsDTO.setError(error);
				customerOrderDetailsDTO.setStatus(0);
				return customerOrderDetailsDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				customerOrderDetailsDTO.setError(error);
				customerOrderDetailsDTO.setStatus(0);
				return customerOrderDetailsDTO;
			}
			
			Invoice invoice = null;
			
			if(orderNoStr!=null && !orderNoStr.isEmpty()){
				CustomerOrder customerOrder = DAORegistry.getCustomerOrderDAO().getCustomerOrderByIdAndBrokerId(Integer.parseInt(orderNoStr), brokerId);
				if(customerOrder == null){
					System.err.println("Customer Order not found for selected invoice.");
					error.setDescription("Customer Order not found for selected invoice.");
					customerOrderDetailsDTO.setError(error);
					customerOrderDetailsDTO.setStatus(0);
					return customerOrderDetailsDTO;				
				}
				invoice = DAORegistry.getInvoiceDAO().getInvoiceByOrderId(Integer.parseInt(orderNoStr));
				if(invoice == null){
					System.err.println("Invoice Details not found.");
					error.setDescription("Invoice Details not found.");
					customerOrderDetailsDTO.setError(error);
					customerOrderDetailsDTO.setStatus(0);
					return customerOrderDetailsDTO;
				}
			}else if(invoiceIdStr!=null && !invoiceIdStr.isEmpty()){
				invoice = DAORegistry.getInvoiceDAO().getInvoiceByIdAndBrokerId(Integer.parseInt(invoiceIdStr), brokerId);
				if(invoice == null){
					System.err.println("Invoice Details not found.");
					error.setDescription("Invoice Details not found.");
					customerOrderDetailsDTO.setError(error);
					customerOrderDetailsDTO.setStatus(0);
					return customerOrderDetailsDTO;
				}
			}else{
				System.err.println("Customer Order Details not found for selected invoice.");
				error.setDescription("Customer Order Details not found for selected invoice.");
				customerOrderDetailsDTO.setError(error);
				customerOrderDetailsDTO.setStatus(0);
				return customerOrderDetailsDTO;
			}
			
			if(action!=null && action.equalsIgnoreCase("addNote")){				
				String manualNote = request.getParameter("manualNote");
				if(StringUtils.isEmpty(manualNote)){
					System.err.println("Please add some value in note.");
					error.setDescription("Please add some value in note.");
					customerOrderDetailsDTO.setError(error);
					customerOrderDetailsDTO.setStatus(0);
					return customerOrderDetailsDTO;
				}
				
				InvoiceAudit audit = new InvoiceAudit(invoice);
				audit.setAction(InvoiceAuditAction.MANUAL_NOTE);
				audit.setCreateBy(userName);
				audit.setCreatedDate(new Date());
				audit.setNote(manualNote);
				DAORegistry.getInvoiceAuditDAO().save(audit);
				
				customerOrderDetailsDTO.setMessage("Note added successfully.");
				customerOrderDetailsDTO.setStatus(1);
				
				//Tracking User Action
				String userActionMsg = "Invoice - Order Details, Manual Note Updated Successfully.";
				Util.userActionAudit(request, invoice.getId(), userActionMsg);
			}
			
			if(invoice!=null){
				CustomerOrder order = DAORegistry.getCustomerOrderDAO().get(invoice.getCustomerOrderId());
				if(order == null){
					System.err.println("Customer Order not found for selected invoice.");
					error.setDescription("Customer Order not found for selected invoice.");
					customerOrderDetailsDTO.setError(error);
					customerOrderDetailsDTO.setStatus(0);
					return customerOrderDetailsDTO;					
				}
				if(order.getShippingMethodId()!=null){
					ShippingMethod shipping = DAORegistry.getShippingMethodDAO().getShippingMethodById(order.getShippingMethodId());
					order.setShippingMethod(shipping.getName());
				}
				List<InvoiceAudit> auditList = DAORegistry.getInvoiceAuditDAO().getInvoiceAuditByInvoiceId(invoice.getId());
				List<CustomerTicketDownloads> customerTicketDownloads = DAORegistry.getCustomerTicketDownloadsDAO().getCustomerTicketDownloadsByInvoiceId(invoice.getId());
				CustomerOrderDetails orderDetails = DAORegistry.getCustomerOrderDetailsDAO().getCustomerOrderDetailsByOrderId(order.getId());
				
				if(order != null){
					customerOrderDetailsDTO.setCustomerOrderDTO(com.rtw.tracker.utils.Util.getCustomerOrderObject(order));
				}
				if(orderDetails != null){
					CustomerOrderDetailsInfoDTO orderDetailsDTO = new CustomerOrderDetailsInfoDTO();					
					orderDetailsDTO.setBillingFirstName(orderDetails.getBlFirstName());
					orderDetailsDTO.setBillingLastName(orderDetails.getBlLastName());
					orderDetailsDTO.setBillingEmail(orderDetails.getBlEmail());
					
					customerOrderDetailsDTO.setCustomerOrderDetailsInfoDTO(orderDetailsDTO);
				}
				if(invoice != null){
					InvoiceDTO invoiceDTO = new InvoiceDTO();					
					invoiceDTO.setInvoiceId(invoice.getId());
					
					customerOrderDetailsDTO.setInvoiceDTO(invoiceDTO);
				}
				
				customerOrderDetailsDTO.setAuditPaginationDTO(PaginationUtil.getDummyPaginationParameters(auditList.size()+customerTicketDownloads.size()));
				customerOrderDetailsDTO.setInvoiceAuditDTO(com.rtw.tracker.utils.Util.getInvoiceAuditArray(auditList, customerTicketDownloads));
				
			}
			customerOrderDetailsDTO.setStatus(1);
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while fetching Customer Order Details.");
			customerOrderDetailsDTO.setError(error);
			customerOrderDetailsDTO.setStatus(0);
		}
		return customerOrderDetailsDTO;
	}
		
	@RequestMapping(value = "/GenerateInvoicePdfAndSendMail")
	public GenericResponseDTO generateInvoicePdfAndSendMail(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		
		try {
			String invoiceIdStr = request.getParameter("invoiceId");
			String brokerIdStr = request.getParameter("brokerId");
			
			if(StringUtils.isEmpty(invoiceIdStr)){
				System.err.println("Invoice ID not found.");
				error.setDescription("Invoice ID not found.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			Integer invoiceId = 0;
			try{
				invoiceId = Integer.parseInt(invoiceIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Invoice ID should be valid Integer value.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}			
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			Invoice invoice = null;
			invoice = DAORegistry.getInvoiceDAO().getInvoiceByIdAndBrokerId(Integer.parseInt(invoiceIdStr), brokerId);
			if(invoice == null){
				System.err.println("Invoice not found.");
				error.setDescription("Invoice not found.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			//List<Ticket> ticket = DAORegistry.getTicketDAO().getMappedTicketsByInvoiceId(Integer.parseInt(invoiceIdStr));
			CategoryTicketGroup categoryTicketGroup = DAORegistry.getCategoryTicketGroupDAO().getCategoryTicketByInvoiceId(invoice.getId());
			CustomerOrderDetails orderDetail = DAORegistry.getCustomerOrderDetailsDAO().getCustomerOrderDetailsByOrderId(invoice.getCustomerOrderId());
			//EventDetails event = DAORegistry.getEventDetailsDAO().get(categoryTicketGroup.getEventId());
			CustomerOrder order = DAORegistry.getCustomerOrderDAO().get(invoice.getCustomerOrderId());
			List<OrderTicketGroupDetails> ticketGroupDetails = DAORegistry.getOrderTicketGroupDetailsDAO().getOrderTicketGroupDetailsByOrderId(invoice.getCustomerOrderId());
			
			PDFUtil.generateInvoicePdf(invoice, categoryTicketGroup, ticketGroupDetails, orderDetail, order);
			File file = new File(DAORegistry.getPropertyDAO().get("tictracker.invoice.pdffile.path").getValue()+"\\invoice_"+invoice.getId()+".pdf");
			if(!file.exists()){
				System.err.println("Error occured while generating Invoice PDF file.");
				error.setDescription("Error occured while generating Invoice PDF file.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			BufferedInputStream stream = new BufferedInputStream(new FileInputStream(file));
			byte[] bytes = ByteStreams.toByteArray(stream);
			
			Customer customer = DAORegistry.getCustomerDAO().get(order.getCustomerId());
			if(customer.getEmail()==null || customer.getEmail().isEmpty()){
				System.err.println("Customer Email not found.");
				error.setDescription("Customer Email not found.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
				
			Map<String, Object> mailMap = new HashMap<String, Object>();
			mailMap.put("customer", customer);  
			mailMap.put("customerOrder", order);
			mailMap.put("androidUrl", "https://play.google.com/store/apps/details?id=com.rewardthefan&hl=en");
			mailMap.put("iosUrl", "https://itunes.apple.com/us/app/reward-the-fan/id1140367203?ls=1&mt=8");
			
			com.rtw.tracker.mail.MailAttachment[] ticketAttachment = new com.rtw.tracker.mail.MailAttachment[1];
			ticketAttachment[0] = new com.rtw.tracker.mail.MailAttachment(bytes, "application/pdf", file.getName(), DAORegistry.getPropertyDAO().get("tictracker.invoice.pdffile.path").getValue()+"\\"+file.getName());
			
			com.rtw.tracker.mail.MailAttachment[] mailAttachment = new com.rtw.tracker.mail.MailAttachment[8];
			mailAttachment[0] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","tyrtf.png",com.rtw.tmat.utils.Util.getFilePath(request, "tyrtf.png", "/resources/images/"));
			mailAttachment[1] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","blue.png",com.rtw.tmat.utils.Util.getFilePath(request, "blue.png", "/resources/images/"));
			mailAttachment[2] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","fb1.png",com.rtw.tmat.utils.Util.getFilePath(request, "fb1.png", "/resources/images/"));
			mailAttachment[3] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","ig1.png",com.rtw.tmat.utils.Util.getFilePath(request, "ig1.png", "/resources/images/"));
			mailAttachment[4] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","li1.png",com.rtw.tmat.utils.Util.getFilePath(request, "li1.png", "/resources/images/"));
			mailAttachment[5] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","rtflogot.png",com.rtw.tmat.utils.Util.getFilePath(request, "rtflogot.png", "/resources/images/"));
			mailAttachment[6] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","p1.png",com.rtw.tmat.utils.Util.getFilePath(request, "p1.png", "/resources/images/"));
			mailAttachment[7] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","tw1.png",com.rtw.tmat.utils.Util.getFilePath(request, "tw1.png", "/resources/images/"));
			String email = customer.getEmail();
			String bcc = "";
			bcc = "amit.raut@rightthisway.com,msanghani@rightthisway.com";
			try {
				mailManager.sendMailNow("text/html","sales@rewardthefan.com",email, 
						null,bcc, "Reward The Fan : Your invoice for order No : "+order.getId(),
			    		"mail-rewardfan-invoice-pdf.html", mailMap, "text/html", ticketAttachment, mailAttachment,null);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Error occured while sending invoice PDF to customer.");
				genericResponseDTO.setError(error);
				genericResponseDTO.setStatus(0);
				return genericResponseDTO;
			}
			
			//Tracking User Action
			String userActionMsg = "Invoice Sent Successfully Through Customer Email.";
			Util.userActionAudit(request, invoice.getId(), userActionMsg);
			
			genericResponseDTO.setStatus(1);
			genericResponseDTO.setMessage("Invoice Sent Successfully.");

		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Sending Invoice PDF to customer.");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
	@RequestMapping(value="/CreatePayInvoice")
	public CreatePayInvoiceDTO createPayInvoice(HttpServletRequest request, HttpServletResponse response){
		CreatePayInvoiceDTO createPayInvoiceDTO = new CreatePayInvoiceDTO();
		Error error = new Error();
		
		try {
			String orderIdStr = request.getParameter("orderId");
			String action = request.getParameter("pinv_action");
			String userName = request.getParameter("userName");
			String brokerIdStr = request.getParameter("brokerId");
					
			String msg = "";
			Double rewardPoints = 0.00;
			Double orderTotal = 0.00;
			
			if(StringUtils.isEmpty(orderIdStr)){
				System.err.println("Customer Order ID not found.");
				error.setDescription("Customer Order ID not found.");
				createPayInvoiceDTO.setError(error);
				createPayInvoiceDTO.setStatus(0);
				return createPayInvoiceDTO;
			}
			Integer orderId = 0;
			try{
				orderId = Integer.parseInt(orderIdStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Customer Order ID should be valid Integer value.");
				createPayInvoiceDTO.setError(error);
				createPayInvoiceDTO.setStatus(0);
				return createPayInvoiceDTO;
			}	
			
			if(StringUtils.isEmpty(brokerIdStr)){
				System.err.println("Please provide Broker Id.");
				error.setDescription("Please provide Broker Id.");
				createPayInvoiceDTO.setError(error);
				createPayInvoiceDTO.setStatus(0);
				return createPayInvoiceDTO;
			}
			Integer brokerId = 0;
			try {
				brokerId = Integer.parseInt(brokerIdStr);
			} catch (Exception e) {
				System.err.println("Please provide valid Broker Id.");
				error.setDescription("Please provide valid Broker Id.");
				createPayInvoiceDTO.setError(error);
				createPayInvoiceDTO.setStatus(0);
				return createPayInvoiceDTO;
			}
			
			CustomerOrder order = null;
			order = DAORegistry.getCustomerOrderDAO().getCustomerOrderByIdAndBrokerId(orderId, brokerId);
			if(order == null){
				System.err.println("Customer Order not found.");
				error.setDescription("Customer Order not found.");
				createPayInvoiceDTO.setError(error);
				createPayInvoiceDTO.setStatus(0);
				return createPayInvoiceDTO;
			}
			if(!order.getPrimaryPaymentMethod().equals(PaymentMethod.ACCOUNT_RECIVABLE)){
				System.err.println("Payment has received for Selected Invoice.");
				error.setDescription("Payment has received for Selected Invoice.");
				createPayInvoiceDTO.setError(error);
				createPayInvoiceDTO.setStatus(0);
				return createPayInvoiceDTO;
			}
			
			Invoice invoice = null;
			invoice = DAORegistry.getInvoiceDAO().getInvoiceByOrderId(orderId);
			if(invoice == null){
				System.err.println("Invoice not found.");
				error.setDescription("Invoice not found.");
				createPayInvoiceDTO.setError(error);
				createPayInvoiceDTO.setStatus(0);
				return createPayInvoiceDTO;
			}
			
			Customer customer = null;
			customer = DAORegistry.getCustomerDAO().get(order.getCustomerId());
			if(customer == null){
				System.err.println("Customer not found.");
				error.setDescription("Customer not found.");
				createPayInvoiceDTO.setError(error);
				createPayInvoiceDTO.setStatus(0);
				return createPayInvoiceDTO;
			}
			
			if(action!=null && !action.isEmpty() && action.equalsIgnoreCase("savePayInvoice")){
				String ticketIds = "";
				if(order.getIsLongSale()){
					List<OrderTicketGroupDetails> orderTicketDetails = DAORegistry.getOrderTicketGroupDetailsDAO().getOrderTicketGroupDetailsByOrderId(order.getId());
					for(OrderTicketGroupDetails otd : orderTicketDetails){
						ticketIds += otd.getTicketGroupId()+",";
					}
					ticketIds = ticketIds.substring(0,ticketIds.length()-1);
				}else{
					CategoryTicketGroup ctg = DAORegistry.getCategoryTicketGroupDAO().getCategoryTicketByInvoiceId(invoice.getId());
					ticketIds = ctg.getId().toString();
				}
				
				String paymentMethod = request.getParameter("paymentMethod");
				String paymentType = request.getParameter("paymentType");
				String invoiceNote = request.getParameter("pinv_Note");
				if(TextUtil.isEmptyOrNull(paymentMethod)){
					System.err.println("Not able to identify payment method.");
					error.setDescription("Not able to identify payment method.");
					createPayInvoiceDTO.setError(error);
					createPayInvoiceDTO.setStatus(0);
					return createPayInvoiceDTO;
				}
				if(TextUtil.isEmptyOrNull(paymentType)){
					System.err.println("PaymentType is mendatory.");
					error.setDescription("PaymentType is mendatory.");
					createPayInvoiceDTO.setError(error);
					createPayInvoiceDTO.setStatus(0);
					return createPayInvoiceDTO;
				}
				
				String orderTotalStr = request.getParameter("pinv_orderTotal");
				if(orderTotalStr == null || orderTotalStr.isEmpty()){
					System.err.println("Order Total not found.");
					error.setDescription("Order Total not found.");
					createPayInvoiceDTO.setError(error);
					createPayInvoiceDTO.setStatus(0);
					return createPayInvoiceDTO;
				}
				
				try {
					orderTotal = Double.parseDouble(orderTotalStr);
				} catch (Exception e) {
					System.err.println("Invalid Order Total.");
					error.setDescription("Invalid Order Total.");
					createPayInvoiceDTO.setError(error);
					createPayInvoiceDTO.setStatus(0);
					return createPayInvoiceDTO;
				}
				
				if(orderTotal > order.getOrderTotal() || orderTotal < order.getOrderTotal()){
					System.err.println("Order Total is mismatching.");
					error.setDescription("Order Total is mismatching.");
					createPayInvoiceDTO.setError(error);
					createPayInvoiceDTO.setStatus(0);
					return createPayInvoiceDTO;
				}
				
				
				PaymentMethods method = PaymentMethods.valueOf(paymentMethod);
				Map<String, String> orderParamMap = Util.getParameterMap(request);
				Date today = new Date();
				
				String validationMsg = "";
				String paymentMethodCount = null;	
				Long rewardTxnId = null;
				String primaryTrxId = "";
				String secondaryTrxId = "";
				String thirdTrxId = "";
				String primaryAmount=null;
				String secondaryAmount =null;
				String thirdAmount=null;
				String rewardPointsStr = null;
				Double activePoints = 0.00;
				
				StripeTransaction transaction = null;
				WalletTransaction walletTranx = null;
				CustomerLoyaltyHistory loyaltyHistory = null;
				Property property = null;
								
				InvoiceController invoiceControllerObj = new InvoiceController();
				invoiceControllerObj.setSharedProperty(sharedProperty);				
								   
				CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(order.getCustomerId());
								
				switch(method){
				case CREDITCARD :
					primaryAmount = orderTotalStr;
					if(paymentType.equalsIgnoreCase("PARTIAL")){
						primaryAmount = request.getParameter("cardAmount");
					}
					transaction = invoiceControllerObj.doCreditCardTransaction(request,primaryAmount,ticketIds,order.getIsLongSale(),order.getQty());
					if(transaction.getStatus()==0){
						validationMsg = "Credit Card Transaction - "+transaction.getError().getDescription();
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					loyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyHistoryByOrderIdAndCustomerId(order.getId(),customer.getId());
					if(loyaltyHistory != null){
						Double pointEarned = (Double.parseDouble(primaryAmount) * 0.10);
						loyaltyHistory.setPointsEarned(pointEarned);
						loyaltyHistory.setInitialRewardPoints(customerLoyalty.getActivePoints());
						loyaltyHistory.setInitialRewardAmount(customerLoyalty.getActivePoints());
						loyaltyHistory.setBalanceRewardPoints(customerLoyalty.getActivePoints() + pointEarned);
						loyaltyHistory.setBalanceRewardAmount(customerLoyalty.getActivePoints() + pointEarned);
						loyaltyHistory.setUpdatedDate(today);
						DAORegistry.getCustomerLoyaltyHistoryDAO().update(loyaltyHistory);
						
						customerLoyalty.setPendingPoints(customerLoyalty.getPendingPoints() + loyaltyHistory.getPointsEarned());
						customerLoyalty.setLastUpdate(today);
						DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
					}
					
					primaryTrxId = transaction.getTransactionId();
					order.setPrimaryPaymentMethod(PaymentMethod.CREDITCARD);
					order.setPrimaryPayAmt(Double.parseDouble(primaryAmount));
					order.setPrimaryTransactionId(primaryTrxId);
					DAORegistry.getCustomerOrderDAO().update(order);
					msg = "Payment details updated for Selected Invoice.";
					break;
				case WALLET :
					primaryAmount = orderTotalStr;
					if(paymentType.equalsIgnoreCase("PARTIAL")){
						primaryAmount = request.getParameter("walletAmount");
					}
					walletTranx= invoiceControllerObj.doWalletTransaction(request, primaryAmount, orderId, userName);
					if(walletTranx.getStatus()==0){
						validationMsg ="Wallet Transaction - "+walletTranx.getError().getDescription();
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					
					loyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyHistoryByOrderIdAndCustomerId(order.getId(),customer.getId());
					if(loyaltyHistory != null){
						Double pointEarned = (Double.parseDouble(primaryAmount) * 0.10);
						loyaltyHistory.setPointsEarned(pointEarned);
						loyaltyHistory.setInitialRewardPoints(customerLoyalty.getActivePoints());
						loyaltyHistory.setInitialRewardAmount(customerLoyalty.getActivePoints());
						loyaltyHistory.setBalanceRewardPoints(customerLoyalty.getActivePoints() + pointEarned);
						loyaltyHistory.setBalanceRewardAmount(customerLoyalty.getActivePoints() + pointEarned);
						loyaltyHistory.setUpdatedDate(today);
						DAORegistry.getCustomerLoyaltyHistoryDAO().update(loyaltyHistory);
						
						customerLoyalty.setPendingPoints(customerLoyalty.getPendingPoints() + loyaltyHistory.getPointsEarned());
						customerLoyalty.setLastUpdate(today);
						DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
					}
					
					primaryTrxId = walletTranx.getWalletHistory().getTransactionId();
					order.setPrimaryPaymentMethod(PaymentMethod.CUSTOMER_WALLET);
					order.setPrimaryPayAmt(Double.parseDouble(primaryAmount));
					order.setPrimaryTransactionId(primaryTrxId);
					DAORegistry.getCustomerOrderDAO().update(order);
					msg = "Payment details updated for Selected Invoice.";
					break;
				case REWARDPOINTS :
					primaryAmount = orderTotalStr;
					if(paymentType.equalsIgnoreCase("PARTIAL")){
						primaryAmount = request.getParameter("partialRewardPoints");
					}
					activePoints = customerLoyalty.getActivePoints();
					if(activePoints < Double.parseDouble(primaryAmount)){
						System.err.println("Selected customer have only "+activePoints+" active reward points.");
						error.setDescription("Selected customer have only "+activePoints+" active reward points.");
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					
					property = DAORegistry.getPropertyDAO().get("rtf.fullrewards.auto.primarytrxid");
				    rewardTxnId =  Long.valueOf(property.getValue());
				    rewardTxnId++;
				    primaryTrxId = PaginationUtil.fullRewardPaymentPrefix+rewardTxnId;
				    property.setValue(String.valueOf(rewardTxnId));
				    DAORegistry.getPropertyDAO().update(property);
					
					loyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyHistoryByOrderIdAndCustomerId(order.getId(),customer.getId());
					if(loyaltyHistory != null){
						Double pointSpent = Double.parseDouble(primaryAmount);
						loyaltyHistory.setInitialRewardAmount(customerLoyalty.getActivePoints());
						loyaltyHistory.setInitialRewardPoints(customerLoyalty.getActivePoints());
						loyaltyHistory.setBalanceRewardPoints(customerLoyalty.getActivePoints() - pointSpent);
						loyaltyHistory.setBalanceRewardAmount(customerLoyalty.getActivePoints() - pointSpent);
						loyaltyHistory.setPointsSpent(pointSpent);
						loyaltyHistory.setRewardSpentAmount(pointSpent);
						loyaltyHistory.setUpdatedDate(today);
						DAORegistry.getCustomerLoyaltyHistoryDAO().update(loyaltyHistory);
						
						customerLoyalty.setActivePoints(customerLoyalty.getActivePoints() - pointSpent);
						customerLoyalty.setTotalSpentPoints(customerLoyalty.getTotalSpentPoints() + pointSpent);
						customerLoyalty.setLatestSpentPoints(pointSpent);
						customerLoyalty.setLastUpdate(today);
						DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
					}
											
				    order.setPrimaryPaymentMethod(PaymentMethod.FULL_REWARDS);
					order.setPrimaryPayAmt(Double.parseDouble(primaryAmount));
					order.setPrimaryTransactionId(primaryTrxId);
					DAORegistry.getCustomerOrderDAO().update(order);
					msg = "Payment details updated for Selected Invoice.";
					break;
				case PARTIAL_REWARD_CC :
					rewardPointsStr =  request.getParameter("rewardPoints");
					primaryAmount = rewardPointsStr;
					secondaryAmount = request.getParameter("cardAmount");
					if(primaryAmount==null || primaryAmount.isEmpty()){
						validationMsg ="Reward point amount not found.";
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					if(secondaryAmount==null || secondaryAmount.isEmpty()){
						validationMsg ="Card amount is not found.";
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					activePoints = customerLoyalty.getActivePoints();
					if(activePoints < Double.parseDouble(primaryAmount)){
						System.err.println("Selected customer have only "+activePoints+" active reward points.");
						error.setDescription("Selected customer have only "+activePoints+" active reward points.");
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					
					transaction = invoiceControllerObj.doCreditCardTransaction(request,secondaryAmount,ticketIds,order.getIsLongSale(),order.getQty());
					if(transaction.getStatus()==0){
						validationMsg = "Credit Card Transaction - "+transaction.getError().getDescription();
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					
					property = DAORegistry.getPropertyDAO().get("rtf.partialrewards.auto.primarytrxid");
					rewardTxnId =  Long.valueOf(property.getValue());
					rewardTxnId++;
				    primaryTrxId = PaginationUtil.partialRewardPaymentPrefix+rewardTxnId;
				    property.setValue(String.valueOf(rewardTxnId));
					DAORegistry.getPropertyDAO().update(property);
					
					loyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyHistoryByOrderIdAndCustomerId(order.getId(),customer.getId());
					if(loyaltyHistory != null){
						Double pointSpent = Double.parseDouble(primaryAmount);
						Double pointEarned = (Double.parseDouble(secondaryAmount) * 0.10);
						loyaltyHistory.setPointsEarned(pointEarned);
						loyaltyHistory.setPointsSpent(pointSpent);
						loyaltyHistory.setRewardSpentAmount(pointSpent);
						loyaltyHistory.setInitialRewardAmount(customerLoyalty.getActivePoints());
						loyaltyHistory.setInitialRewardPoints(customerLoyalty.getActivePoints());
						loyaltyHistory.setBalanceRewardPoints(customerLoyalty.getActivePoints() + pointEarned - pointSpent);
						loyaltyHistory.setBalanceRewardAmount(customerLoyalty.getActivePoints() + pointEarned - pointSpent);
						loyaltyHistory.setUpdatedDate(today);
						DAORegistry.getCustomerLoyaltyHistoryDAO().update(loyaltyHistory);
						
						customerLoyalty.setPendingPoints(customerLoyalty.getPendingPoints() + loyaltyHistory.getPointsEarned());
						customerLoyalty.setTotalSpentPoints(customerLoyalty.getTotalSpentPoints() + pointSpent);
						customerLoyalty.setActivePoints(customerLoyalty.getActivePoints() - pointSpent);
						customerLoyalty.setLatestSpentPoints(pointSpent);
						customerLoyalty.setLastUpdate(today);
						DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
					}
					
					secondaryTrxId = transaction.getTransactionId();
					
					order.setPrimaryPaymentMethod(PaymentMethod.PARTIAL_REWARDS);
					order.setPrimaryPayAmt(Double.parseDouble(primaryAmount));
					order.setPrimaryTransactionId(primaryTrxId);
					
					order.setSecondaryPaymentMethod(PartialPaymentMethod.CREDITCARD);
					order.setSecondaryPayAmt(Double.parseDouble(secondaryAmount));
					order.setSecondaryTransactionId(secondaryTrxId);
					DAORegistry.getCustomerOrderDAO().update(order);
					msg = "Payment details updated for Selected Invoice.";
					break;
				case PARTIAL_REWARD_WALLET :
					rewardPointsStr =  request.getParameter("rewardPoints");
					primaryAmount = rewardPointsStr;
					secondaryAmount = request.getParameter("walletAmount");					
					if(primaryAmount==null || primaryAmount.isEmpty()){
						validationMsg ="Reward point amount is not found.";
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					if(secondaryAmount==null || secondaryAmount.isEmpty()){
						validationMsg ="Wallet amount is not found.";
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					
					activePoints = customerLoyalty.getActivePoints();
					if(activePoints < Double.parseDouble(primaryAmount)){
						System.err.println("Selected customer have only "+activePoints+" active reward points.");
						error.setDescription("Selected customer have only "+activePoints+" active reward points.");
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					
					walletTranx= invoiceControllerObj.doWalletTransaction(request, secondaryAmount, orderId, userName);
					if(walletTranx.getStatus()==0){
						validationMsg ="Wallet Transaction - "+walletTranx.getError().getDescription();
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					
					property = DAORegistry.getPropertyDAO().get("rtf.partialrewards.auto.primarytrxid");
					rewardTxnId =  Long.valueOf(property.getValue());
					rewardTxnId++;
					primaryTrxId = PaginationUtil.partialRewardPaymentPrefix+rewardTxnId;
				    property.setValue(String.valueOf(rewardTxnId));
					DAORegistry.getPropertyDAO().update(property);
					
					loyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyHistoryByOrderIdAndCustomerId(order.getId(),customer.getId());
					if(loyaltyHistory != null){
						Double pointSpent = Double.parseDouble(primaryAmount);
						Double pointEarned = (Double.parseDouble(secondaryAmount) * 0.10);
						loyaltyHistory.setPointsEarned(pointEarned);
						loyaltyHistory.setPointsSpent(pointSpent);
						loyaltyHistory.setRewardSpentAmount(pointSpent);
						loyaltyHistory.setInitialRewardAmount(customerLoyalty.getActivePoints());
						loyaltyHistory.setInitialRewardPoints(customerLoyalty.getActivePoints());
						loyaltyHistory.setBalanceRewardPoints(customerLoyalty.getActivePoints() + pointEarned - pointSpent);
						loyaltyHistory.setBalanceRewardAmount(customerLoyalty.getActivePoints() + pointEarned - pointSpent);
						loyaltyHistory.setUpdatedDate(today);
						DAORegistry.getCustomerLoyaltyHistoryDAO().update(loyaltyHistory);
						
						customerLoyalty.setPendingPoints(customerLoyalty.getPendingPoints() + loyaltyHistory.getPointsEarned());
						customerLoyalty.setTotalSpentPoints(customerLoyalty.getTotalSpentPoints() + pointSpent);
						customerLoyalty.setActivePoints(customerLoyalty.getActivePoints() - pointSpent);
						customerLoyalty.setLatestSpentPoints(pointSpent);
						customerLoyalty.setLastUpdate(today);
						DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
					}
					
					secondaryTrxId = walletTranx.getWalletHistory().getTransactionId();
					
					order.setPrimaryPaymentMethod(PaymentMethod.PARTIAL_REWARDS);
					order.setPrimaryPayAmt(Double.parseDouble(primaryAmount));
					order.setPrimaryTransactionId(primaryTrxId);
					
					order.setSecondaryPaymentMethod(PartialPaymentMethod.CUSTOMER_WALLET);
					order.setSecondaryPayAmt(Double.parseDouble(secondaryAmount));
					order.setSecondaryTransactionId(secondaryTrxId);
					DAORegistry.getCustomerOrderDAO().update(order);
					msg = "Payment details updated for Selected Invoice.";
					break;
				case WALLET_CREDITCARD :
					primaryAmount = request.getParameter("walletAmount");
					secondaryAmount = request.getParameter("cardAmount");
					if(primaryAmount==null || primaryAmount.isEmpty()){
						validationMsg ="Wallet amount is not found.";
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					if(secondaryAmount==null || secondaryAmount.isEmpty()){
						validationMsg ="Credit Card amount is not found.";
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					walletTranx= invoiceControllerObj.doWalletTransaction(request, primaryAmount, orderId, userName);
					if(walletTranx.getStatus()==0){
						validationMsg ="Wallet Transaction - "+walletTranx.getError().getDescription();
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					transaction = invoiceControllerObj.doCreditCardTransaction(request,secondaryAmount,ticketIds,order.getIsLongSale(),order.getQty());
					if(transaction.getStatus()==0){
						validationMsg = "Credit Card Transaction - "+transaction.getError().getDescription();
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					
					loyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyHistoryByOrderIdAndCustomerId(order.getId(),customer.getId());
					if(loyaltyHistory != null){
						Double pointEarned = ((Double.parseDouble(primaryAmount) + Double.parseDouble(secondaryAmount)) * 0.10);
						loyaltyHistory.setPointsEarned(pointEarned);
						loyaltyHistory.setInitialRewardAmount(customerLoyalty.getActivePoints());
						loyaltyHistory.setInitialRewardPoints(customerLoyalty.getActivePoints());
						loyaltyHistory.setBalanceRewardPoints(customerLoyalty.getActivePoints() + pointEarned);
						loyaltyHistory.setBalanceRewardAmount(customerLoyalty.getActivePoints() + pointEarned);
						loyaltyHistory.setUpdatedDate(today);
						DAORegistry.getCustomerLoyaltyHistoryDAO().update(loyaltyHistory);
						
						customerLoyalty.setPendingPoints(customerLoyalty.getPendingPoints() + loyaltyHistory.getPointsEarned());
						customerLoyalty.setLastUpdate(today);
						DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
					}
					
					primaryTrxId = walletTranx.getWalletHistory().getTransactionId();
					secondaryTrxId = transaction.getTransactionId();
					
					order.setPrimaryPaymentMethod(PaymentMethod.CUSTOMER_WALLET);
					order.setPrimaryPayAmt(Double.parseDouble(primaryAmount));
					order.setPrimaryTransactionId(primaryTrxId);
					
					order.setSecondaryPaymentMethod(PartialPaymentMethod.CREDITCARD);
					order.setSecondaryPayAmt(Double.parseDouble(secondaryAmount));
					order.setSecondaryTransactionId(secondaryTrxId);
					DAORegistry.getCustomerOrderDAO().update(order);
					msg = "Payment details updated for Selected Invoice.";
					break;
				case PARTIAL_REWARD_WALLET_CC :
					rewardPointsStr =  request.getParameter("rewardPoints");
					primaryAmount = rewardPointsStr;
					secondaryAmount = request.getParameter("walletAmount");
					thirdAmount = request.getParameter("cardAmount");
					if(primaryAmount==null || primaryAmount.isEmpty()){
						validationMsg ="Reward points amount is not found.";
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					if(secondaryAmount==null || secondaryAmount.isEmpty()){
						validationMsg ="Wallet amount is not found.";
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					if(thirdAmount==null || thirdAmount.isEmpty()){
						validationMsg ="Credit card amount is not found.";
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					activePoints = customerLoyalty.getActivePoints();
					if(activePoints < Double.parseDouble(primaryAmount)){
						System.err.println("Selected customer have only "+activePoints+" active reward points.");
						error.setDescription("Selected customer have only "+activePoints+" active reward points.");
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
				    
					walletTranx= invoiceControllerObj.doWalletTransaction(request, secondaryAmount, orderId, userName);
					if(walletTranx.getStatus()==0){
						validationMsg ="Wallet Transaction - "+walletTranx.getError().getDescription();
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					transaction = invoiceControllerObj.doCreditCardTransaction(request,thirdAmount,ticketIds,order.getIsLongSale(),order.getQty());
					if(transaction.getStatus()==0){
						validationMsg = "Credit Card Transaction - "+transaction.getError().getDescription();
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					
					property = DAORegistry.getPropertyDAO().get("rtf.partialrewards.auto.primarytrxid");
				    rewardTxnId =  Long.valueOf(property.getValue());
				    rewardTxnId++;
					primaryTrxId = PaginationUtil.partialRewardPaymentPrefix+rewardTxnId;
				    property.setValue(String.valueOf(rewardTxnId));
					DAORegistry.getPropertyDAO().update(property);
					
					loyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyHistoryByOrderIdAndCustomerId(order.getId(),customer.getId());
					if(loyaltyHistory != null){
						Double pointSpent = Double.parseDouble(primaryAmount);
						Double pointEarned = ((Double.parseDouble(secondaryAmount) + Double.parseDouble(thirdAmount)) * 0.10);
						loyaltyHistory.setPointsEarned(pointEarned);
						loyaltyHistory.setPointsSpent(pointSpent);
						loyaltyHistory.setRewardSpentAmount(pointSpent);
						loyaltyHistory.setInitialRewardAmount(customerLoyalty.getActivePoints());
						loyaltyHistory.setInitialRewardPoints(customerLoyalty.getActivePoints());
						loyaltyHistory.setBalanceRewardPoints(customerLoyalty.getActivePoints() + pointEarned - pointSpent);
						loyaltyHistory.setBalanceRewardAmount(customerLoyalty.getActivePoints() + pointEarned - pointSpent);
						loyaltyHistory.setUpdatedDate(today);
						DAORegistry.getCustomerLoyaltyHistoryDAO().update(loyaltyHistory);
						
						customerLoyalty.setPendingPoints(customerLoyalty.getPendingPoints() + loyaltyHistory.getPointsEarned());
						customerLoyalty.setTotalSpentPoints(customerLoyalty.getTotalSpentPoints() + pointSpent);
						customerLoyalty.setActivePoints(customerLoyalty.getActivePoints() - pointSpent);
						customerLoyalty.setLatestSpentPoints(pointSpent);
						customerLoyalty.setLastUpdate(today);
						DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
					}
					
					secondaryTrxId = walletTranx.getWalletHistory().getTransactionId();
					thirdTrxId = transaction.getTransactionId();
					
					order.setPrimaryPaymentMethod(PaymentMethod.PARTIAL_REWARDS);
					order.setPrimaryPayAmt(Double.parseDouble(primaryAmount));
					order.setPrimaryTransactionId(primaryTrxId);
					
					order.setSecondaryPaymentMethod(PartialPaymentMethod.CUSTOMER_WALLET);
					order.setSecondaryPayAmt(Double.parseDouble(secondaryAmount));
					order.setSecondaryTransactionId(secondaryTrxId);
					
					order.setThirdPaymentMethod(PartialPaymentMethod.CREDITCARD);
					order.setThirdPayAmt(Double.parseDouble(thirdAmount));
					order.setThirdTransactionId(thirdTrxId);
					DAORegistry.getCustomerOrderDAO().update(order);
					msg = "Payment details updated for Selected Invoice.";
					break;
				case BANK_OR_CHEQUE :
					primaryAmount = orderTotalStr;
					primaryTrxId = request.getParameter("bankChequeId");
					if(paymentType.equalsIgnoreCase("PARTIAL")){
						primaryAmount = request.getParameter("bankChequeAmt");
					}
					
					if(primaryTrxId==null || primaryTrxId.isEmpty()){
						validationMsg ="Transaction Id is not found.";
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					loyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyHistoryByOrderIdAndCustomerId(order.getId(),customer.getId());
					if(loyaltyHistory != null){
						Double pointEarned = (Double.parseDouble(primaryAmount) * 0.10);
						loyaltyHistory.setPointsEarned(pointEarned);
						loyaltyHistory.setInitialRewardAmount(customerLoyalty.getActivePoints());
						loyaltyHistory.setInitialRewardPoints(customerLoyalty.getActivePoints());
						loyaltyHistory.setBalanceRewardPoints(customerLoyalty.getActivePoints() + pointEarned);
						loyaltyHistory.setBalanceRewardAmount(customerLoyalty.getActivePoints() + pointEarned);
						loyaltyHistory.setUpdatedDate(today);
						DAORegistry.getCustomerLoyaltyHistoryDAO().update(loyaltyHistory);
						
						customerLoyalty.setPendingPoints(customerLoyalty.getPendingPoints() + loyaltyHistory.getPointsEarned());
						customerLoyalty.setLastUpdate(today);
						DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
					}
					order.setPrimaryPaymentMethod(PaymentMethod.BANK_OR_CHEQUE);
					order.setPrimaryPayAmt(Double.parseDouble(primaryAmount));
					order.setPrimaryTransactionId(primaryTrxId);
					DAORegistry.getCustomerOrderDAO().update(order);
					msg = "Payment details updated for Selected Invoice.";
					break;
				case BANK_OR_CHEQUE_CC :
					primaryTrxId = request.getParameter("bankChequeId");
					primaryAmount = request.getParameter("bankChequeAmt");
					secondaryAmount = request.getParameter("cardAmount");
					if(primaryTrxId==null || primaryTrxId.isEmpty()){
						validationMsg ="Transaction Id is not found.";
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					if(primaryAmount==null || primaryAmount.isEmpty()){
						validationMsg ="Bank/Cheque amount not found.";
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					if(secondaryAmount==null || secondaryAmount.isEmpty()){
						validationMsg ="Credit card amount not found.";
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					transaction = invoiceControllerObj.doCreditCardTransaction(request,secondaryAmount,ticketIds,order.getIsLongSale(),order.getQty());
					if(transaction.getStatus()==0){
						validationMsg = "Credit Card Transaction - "+transaction.getError().getDescription();
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					
					loyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyHistoryByOrderIdAndCustomerId(order.getId(),customer.getId());
					if(loyaltyHistory != null){
						Double pointEarned = ((Double.parseDouble(primaryAmount) + Double.parseDouble(secondaryAmount)) * 0.10);
						loyaltyHistory.setPointsEarned(pointEarned);
						loyaltyHistory.setInitialRewardAmount(customerLoyalty.getActivePoints());
						loyaltyHistory.setInitialRewardPoints(customerLoyalty.getActivePoints());
						loyaltyHistory.setBalanceRewardPoints(customerLoyalty.getActivePoints() + pointEarned);
						loyaltyHistory.setBalanceRewardAmount(customerLoyalty.getActivePoints() + pointEarned);
						loyaltyHistory.setUpdatedDate(today);
						DAORegistry.getCustomerLoyaltyHistoryDAO().update(loyaltyHistory);
						
						customerLoyalty.setPendingPoints(customerLoyalty.getPendingPoints() + loyaltyHistory.getPointsEarned());
						customerLoyalty.setLastUpdate(today);
						DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
					}
					
					secondaryTrxId = transaction.getTransactionId();
					
					order.setPrimaryPaymentMethod(PaymentMethod.BANK_OR_CHEQUE);
					order.setPrimaryPayAmt(Double.parseDouble(primaryAmount));
					order.setPrimaryTransactionId(primaryTrxId);
					
					order.setSecondaryPaymentMethod(PartialPaymentMethod.CREDITCARD);
					order.setSecondaryTransactionId(secondaryTrxId);
					order.setSecondaryPayAmt(Double.parseDouble(secondaryAmount));
					DAORegistry.getCustomerOrderDAO().update(order);
					msg = "Payment details updated for Selected Invoice.";
					break;
				case BANK_OR_CHEQUE_WALLET :
					primaryTrxId = request.getParameter("bankChequeId");
					primaryAmount = request.getParameter("bankChequeAmt");
					secondaryAmount = request.getParameter("walletAmount");
					if(primaryTrxId==null || primaryTrxId.isEmpty()){
						validationMsg ="Transaction Id is not found.";
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					if(primaryAmount==null || primaryAmount.isEmpty()){
						validationMsg ="Bank/Cheque amount not found.";
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					if(secondaryAmount==null || secondaryAmount.isEmpty()){
						validationMsg ="Wallet amount not found.";
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					walletTranx= invoiceControllerObj.doWalletTransaction(request, secondaryAmount, orderId, userName);
					if(walletTranx.getStatus()==0){
						validationMsg ="Wallet Transaction - "+walletTranx.getError().getDescription();
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					
					loyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyHistoryByOrderIdAndCustomerId(order.getId(),customer.getId());
					if(loyaltyHistory != null){
						Double pointEarned = ((Double.parseDouble(primaryAmount) + Double.parseDouble(secondaryAmount)) * 0.10);
						loyaltyHistory.setPointsEarned(pointEarned);
						loyaltyHistory.setInitialRewardAmount(customerLoyalty.getActivePoints());
						loyaltyHistory.setInitialRewardPoints(customerLoyalty.getActivePoints());
						loyaltyHistory.setBalanceRewardPoints(customerLoyalty.getActivePoints() + pointEarned);
						loyaltyHistory.setBalanceRewardAmount(customerLoyalty.getActivePoints() + pointEarned);
						loyaltyHistory.setUpdatedDate(today);
						DAORegistry.getCustomerLoyaltyHistoryDAO().update(loyaltyHistory);
						
						customerLoyalty.setPendingPoints(customerLoyalty.getPendingPoints() + loyaltyHistory.getPointsEarned());
						customerLoyalty.setLastUpdate(today);
						DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
					}
					
					secondaryTrxId = walletTranx.getWalletHistory().getTransactionId();
					
					order.setPrimaryPaymentMethod(PaymentMethod.BANK_OR_CHEQUE);
					order.setPrimaryPayAmt(Double.parseDouble(primaryAmount));
					order.setPrimaryTransactionId(primaryTrxId);
					
					order.setSecondaryPaymentMethod(PartialPaymentMethod.CUSTOMER_WALLET);
					order.setSecondaryTransactionId(secondaryTrxId);
					order.setSecondaryPayAmt(Double.parseDouble(secondaryAmount));
					DAORegistry.getCustomerOrderDAO().update(order);
					msg = "Payment details updated for Selected Invoice.";
					break;
				case BANK_OR_CHEQUE_PARTIAL_REWARD :
					primaryTrxId = request.getParameter("bankChequeId");
					primaryAmount = request.getParameter("bankChequeAmt");
					secondaryAmount =  request.getParameter("rewardPoints");
					if(primaryTrxId==null || primaryTrxId.isEmpty()){
						validationMsg ="Transaction Id is not found.";
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					if(primaryAmount==null || primaryAmount.isEmpty()){
						validationMsg ="Bank/Cheque amount not found.";
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					if(secondaryAmount==null || secondaryAmount.isEmpty()){
						validationMsg ="Reward point amount is not found.";
						System.err.println(validationMsg);
						error.setDescription(validationMsg);
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					activePoints = customerLoyalty.getActivePoints();
					if(activePoints < Double.parseDouble(secondaryAmount)){
						System.err.println("Selected customer have only "+activePoints+" active reward points.");
						error.setDescription("Selected customer have only "+activePoints+" active reward points.");
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					
					property = DAORegistry.getPropertyDAO().get("rtf.partialrewards.auto.primarytrxid");
				    rewardTxnId =  Long.valueOf(property.getValue());
				    rewardTxnId++;
				    secondaryTrxId = PaginationUtil.partialRewardPaymentPrefix+rewardTxnId;
				    property.setValue(String.valueOf(rewardTxnId));
					DAORegistry.getPropertyDAO().update(property);
					
					loyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyHistoryByOrderIdAndCustomerId(order.getId(),customer.getId());
					if(loyaltyHistory != null){
						Double pointSpent = Double.parseDouble(secondaryAmount);
						Double pointEarned = (Double.parseDouble(primaryAmount) * 0.10);
						loyaltyHistory.setPointsEarned(pointEarned);
						loyaltyHistory.setPointsSpent(pointSpent);
						loyaltyHistory.setRewardSpentAmount(pointSpent);
						loyaltyHistory.setInitialRewardAmount(customerLoyalty.getActivePoints());
						loyaltyHistory.setInitialRewardPoints(customerLoyalty.getActivePoints());
						loyaltyHistory.setBalanceRewardPoints(customerLoyalty.getActivePoints() + pointEarned - pointSpent);
						loyaltyHistory.setBalanceRewardAmount(customerLoyalty.getActivePoints() + pointEarned - pointSpent);
						loyaltyHistory.setUpdatedDate(today);
						DAORegistry.getCustomerLoyaltyHistoryDAO().update(loyaltyHistory);
						
						customerLoyalty.setPendingPoints(customerLoyalty.getPendingPoints() + loyaltyHistory.getPointsEarned());
						customerLoyalty.setTotalSpentPoints(customerLoyalty.getTotalSpentPoints() + pointSpent);
						customerLoyalty.setActivePoints(customerLoyalty.getActivePoints() - pointSpent);
						customerLoyalty.setLatestSpentPoints(pointSpent);
						customerLoyalty.setLastUpdate(today);
						DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
					}
					
					order.setPrimaryPaymentMethod(PaymentMethod.BANK_OR_CHEQUE);
					order.setPrimaryPayAmt(Double.parseDouble(primaryAmount));
					order.setPrimaryTransactionId(primaryTrxId);
					
					order.setSecondaryPaymentMethod(PartialPaymentMethod.PARTIAL_REWARDS);
					order.setSecondaryTransactionId(secondaryTrxId);
					order.setSecondaryPayAmt(Double.parseDouble(secondaryAmount));
					DAORegistry.getCustomerOrderDAO().update(order);
					msg = "Payment details updated for Selected Invoice.";
					break;
				}

				createPayInvoiceDTO.setMessage(msg);
				createPayInvoiceDTO.setStatus(1);
				
				//Invoice Audit
				InvoiceAudit audit = new InvoiceAudit(invoice);
				audit.setAction(InvoiceAuditAction.INVOICE_PAYMENT);
				audit.setNote(invoiceNote);
				audit.setCreatedDate(new Date());
				audit.setCreateBy(userName);
				DAORegistry.getInvoiceAuditDAO().save(audit);
				
				//Tracking User Action
				String userActionMsg = "Invoice Payment is Updated.";
				Util.userActionAudit(request, invoice.getId(), userActionMsg);
			}
			
			if(order != null){
				if(invoice.getInvoiceTotal() > 0){
					if(order != null){
						createPayInvoiceDTO.setCustomerOrderDTO(com.rtw.tracker.utils.Util.getCustomerOrderObject(order));
					}						
					if(invoice != null){
						InvoiceDTO invoiceDTO = new InvoiceDTO();					
						invoiceDTO.setInvoiceId(invoice.getId());
						invoiceDTO.setInvoiceInternalNote(invoice.getInternalNote());
						
						createPayInvoiceDTO.setInvoiceDTO(invoiceDTO);
					}						
					if(customer != null){
						CustomersCustomDTO customerDTO = new CustomersCustomDTO();
						customerDTO.setCustomerId(customer.getId());
						customerDTO.setCustomerName(customer.getCustomerName());
						customerDTO.setLastName(customer.getLastName());
						customerDTO.setEmail(customer.getEmail());
						customerDTO.setPhone(customer.getPhone());
						
						createPayInvoiceDTO.setCustomerDTO(customerDTO);						
					}
					
					Map<String, String> paramterMap = Util.getParameterMap(request);
					paramterMap.put("customerId", String.valueOf(customer.getId()));
					String data = Util.getObject(paramterMap,sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.GET_CUSTOMER_CARDS);
				    Gson gson = new Gson();		
					JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
					CustomerDetails cardDetails = gson.fromJson(((JsonObject)jsonObject.get("customerDetails")), CustomerDetails.class);
					
					if(null != cardDetails.getCardList()){
						createPayInvoiceDTO.setCustomerCardInfoDTO(com.rtw.tracker.utils.Util.getCustomerCardInfoArray(cardDetails.getCardList()));
					}
					
					Map<String, String> paramMap = Util.getParameterMap(request);
					String data1 = Util.getObject(paramMap,sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.GET_STRIPE_CREDENTIALS);
					Gson gson1 = new Gson();		
					JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
					StripeCredentials stripeCredentials = gson1.fromJson(((JsonObject)jsonObject1.get("stripeCredentials")), StripeCredentials.class);
					
					if(stripeCredentials == null){
						System.err.println("Not able to retrive stripe configuration credential, it happens mostly when rewardthefan API is down.");
						error.setDescription("Not able to retrive stripe configuration credential, it happens mostly when rewardthefan API is down.");
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}else if(stripeCredentials.getStatus()==1){
						createPayInvoiceDTO.setsPubKey(stripeCredentials.getPublishableKey());
					}else{
						System.err.println(stripeCredentials.getError().getDescription());
						error.setDescription(stripeCredentials.getError().getDescription());
						createPayInvoiceDTO.setError(error);
						createPayInvoiceDTO.setStatus(0);
						return createPayInvoiceDTO;
					}
					
					CustomerLoyalty loyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
					if(loyalty!=null){
						rewardPoints = loyalty.getActivePoints();
					}
					createPayInvoiceDTO.setRewardPoints(rewardPoints);
										
					CustomerWallet wallet = DAORegistry.getCustomerWalletDAO().getCustomerWalletByCustomerId(customer.getId());
					if(wallet!=null){
						createPayInvoiceDTO.setCustomerCredit(wallet.getActiveCredit());
					}else{
						createPayInvoiceDTO.setCustomerCredit(0.0);
					}
					createPayInvoiceDTO.setStatus(1);
				}
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something went wrong while Creating/Updating Pay Invoice.");
			createPayInvoiceDTO.setError(error);
			createPayInvoiceDTO.setStatus(0);
		}
		return createPayInvoiceDTO;
	}

	
	@RequestMapping(value="/ValidateReferralCode")
	public ValidateReferralCodeDTO validateReferalCode(HttpServletRequest request,HttpServletResponse response){
		ValidateReferralCodeDTO validateReferralCodeDTO = new ValidateReferralCodeDTO();
		Error error = new Error();
		
		try {
			String referalCode = request.getParameter("referralCode");
			String customerIdStr = request.getParameter("customerId");
			String eventIdStr = request.getParameter("eventId");
			String ticketGroupIdStr = request.getParameter("ticketGroupId");
			String isLongSale = request.getParameter("isLongSale");
			String orderTotal = request.getParameter("orderTotal");
			String isFanPrice = request.getParameter("isFanPrice");
			String clientIpAddress = request.getParameter("clientIPAddress");
			String sessionIdStr = request.getParameter("deviceId");
			
			String msg = "";
			
			if(StringUtils.isEmpty(referalCode)){
				System.err.println("Please enter referalCode.");
				error.setDescription("Please enter referalCode.");
				validateReferralCodeDTO.setError(error);
				validateReferralCodeDTO.setStatus(0);
				return validateReferralCodeDTO;
			}
			if(StringUtils.isEmpty(customerIdStr)){
				System.err.println("Customer Id is not found.");
				error.setDescription("Customer Id is not found.");
				validateReferralCodeDTO.setError(error);
				validateReferralCodeDTO.setStatus(0);
				return validateReferralCodeDTO;
			}
			if(StringUtils.isEmpty(eventIdStr)){
				System.err.println("Event Id is not found.");
				error.setDescription("Event Id is not found.");
				validateReferralCodeDTO.setError(error);
				validateReferralCodeDTO.setStatus(0);
				return validateReferralCodeDTO;
			}
			if(StringUtils.isEmpty(ticketGroupIdStr)){
				System.err.println("Ticket Group Id is not found.");
				error.setDescription("Ticket Group Id is not found.");
				validateReferralCodeDTO.setError(error);
				validateReferralCodeDTO.setStatus(0);
				return validateReferralCodeDTO;
			}
			if(StringUtils.isEmpty(orderTotal)){
				System.err.println("Order Total is not found.");
				error.setDescription("Order Total is not found.");
				validateReferralCodeDTO.setError(error);
				validateReferralCodeDTO.setStatus(0);
				return validateReferralCodeDTO;
			}
			boolean isValidOrderTotal = true;
			try {
				orderTotal = Util.formatDoubleToTwoDecimalPoint(Double.parseDouble(orderTotal));
			} catch (Exception e) {
				isValidOrderTotal = false;
				e.printStackTrace();
			}
			
			if(!isValidOrderTotal){
				System.err.println("Invalid Order Total.");
				error.setDescription("Invalid Order Total.");
				validateReferralCodeDTO.setError(error);
				validateReferralCodeDTO.setStatus(0);
				return validateReferralCodeDTO;
			}
			
			Map<String, String> map = Util.getParameterMap(request);
		    map.put("referralCode", referalCode);
		    map.put("customerId",customerIdStr);
		    map.put("eventId", eventIdStr);
		    map.put("tgId", ticketGroupIdStr);
		    map.put("clientIPAddress", clientIpAddress);
		    map.put("isLongSale", isLongSale);
		    map.put("isFanPrice", isFanPrice);
		    map.put("orderTotal", Util.formatDoubleToTwoDecimalPoint(Double.parseDouble(orderTotal)));
		    map.put("sessionId", sessionIdStr);
		   
		    String data = null;
		    if(isLongSale.equalsIgnoreCase("true")){
		    	String totalQuantity = request.getParameter("totalQty");
		    	String ticketSize = request.getParameter("ticketSize");
		    	
		    	if(StringUtils.isEmpty(totalQuantity)){
					System.err.println("Order Quantity is not found.");
					error.setDescription("Order Quantity is not found.");
					validateReferralCodeDTO.setError(error);
					validateReferralCodeDTO.setStatus(0);
					return validateReferralCodeDTO;
				}
		    	Integer totalQty = 0;
				try {
					totalQty = Integer.parseInt(totalQuantity);
				} catch (Exception e) {
					e.printStackTrace();
					error.setDescription("Invalid Order Quantity.");
					validateReferralCodeDTO.setError(error);
					validateReferralCodeDTO.setStatus(0);
					return validateReferralCodeDTO;
				}
				
		    	if(StringUtils.isEmpty(ticketSize)){
					System.err.println("Ticket group size is not found.");
					error.setDescription("Ticket group size is not found.");
					validateReferralCodeDTO.setError(error);
					validateReferralCodeDTO.setStatus(0);
					return validateReferralCodeDTO;
				}				
				Integer ticSize = 0;
				try {
					ticSize = Integer.parseInt(ticketSize);
				} catch (Exception e) {
					e.printStackTrace();
					error.setDescription("Invalid Ticket group size.");
					validateReferralCodeDTO.setError(error);
					validateReferralCodeDTO.setStatus(0);
					return validateReferralCodeDTO;
				}
				
				map.put("orderQty",totalQuantity);
		    	data = Util.getObject(map,sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.VALIDATE_PROMOCODE_REAL_TICKET);
		    	Gson gson = new Gson();  
				JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
				ReferralCodeValidation referralCodeValidation = gson.fromJson(((JsonObject)jsonObject.get("referralCodeValidation")), ReferralCodeValidation.class);
				
				if(referralCodeValidation.getStatus()==1){
					msg = referralCodeValidation.getMessage();
					
					validateReferralCodeDTO.setRewardPoints(referralCodeValidation.getRewardPointsAsDouble());
					
					List<String> priceList = new ArrayList<String>();

					for(int i=0;i<ticSize;i++){
						//String quantity = request.getParameter("quantity_"+i);
						String priceStr = request.getParameter("price_"+i);
						
						if(StringUtils.isEmpty(priceStr)){
							System.err.println("Sold Price is not found.");
							error.setDescription("Sold Price is not found.");
							validateReferralCodeDTO.setError(error);
							validateReferralCodeDTO.setStatus(0);
							return validateReferralCodeDTO;
						}
						Double tPrice = 0.00;
						try {
							tPrice = Double.parseDouble(priceStr);
						} catch (Exception e) {
							e.printStackTrace();
							error.setDescription("Sold Price is not valid Numeric.");
							validateReferralCodeDTO.setError(error);
							validateReferralCodeDTO.setStatus(0);
							return validateReferralCodeDTO;
						}
						if(referralCodeValidation.getCodeType().equals(CodeType.PROMOTIONAL)){
							if(referralCodeValidation.getIsFlatDiscount()!= null && referralCodeValidation.getIsFlatDiscount()){
								tPrice  =  (tPrice - referralCodeValidation.getDiscountConv());
								priceList.add(Util.formatDoubleToTwoDecimalPoint(tPrice));
							}else{
								tPrice  = tPrice - (tPrice * referralCodeValidation.getDiscountConv());
								priceList.add(Util.formatDoubleToTwoDecimalPoint(tPrice));
							}
						}else{
							priceList.add(Util.formatDoubleToTwoDecimalPoint(tPrice));
						}
					}
					
					validateReferralCodeDTO.setStatus(1);
					validateReferralCodeDTO.setPromoTrackingId(referralCodeValidation.getPromoTrackingId());
					validateReferralCodeDTO.setAffPromoTrackingId(referralCodeValidation.getAffPromoTrackingId());
					validateReferralCodeDTO.setLongSalePriceList(priceList);
							
				}else{
					msg = referralCodeValidation.getError().getDescription();
					error.setDescription(msg);
					validateReferralCodeDTO.setError(error);
					validateReferralCodeDTO.setStatus(0);
					return validateReferralCodeDTO;
				}
		    }else{
		    	String quantity = request.getParameter("quantity");
				String priceStr = request.getParameter("price");
				
				if(StringUtils.isEmpty(quantity)){
					System.err.println("Quantity is not found.");
					error.setDescription("Quantity is not found.");
					validateReferralCodeDTO.setError(error);
					validateReferralCodeDTO.setStatus(0);
					return validateReferralCodeDTO;
				}
				if(StringUtils.isEmpty(priceStr)){
					System.err.println("Sold Price is not found.");
					error.setDescription("Sold Price is not found.");
					validateReferralCodeDTO.setError(error);
					validateReferralCodeDTO.setStatus(0);
					return validateReferralCodeDTO;
				}
				
				map.put("orderQty",quantity);
		    	data = Util.getObject(map,sharedProperty.getApiUrl()+com.rtw.tmat.utils.Constants.VALIDATE_PROMOCODE);
		    	Gson gson = new Gson();  
				JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
				ReferralCodeValidation referralCodeValidation = gson.fromJson(((JsonObject)jsonObject.get("referralCodeValidation")), ReferralCodeValidation.class);
				
				if(referralCodeValidation.getStatus()==1){					
					msg = referralCodeValidation.getMessage();
					
					validateReferralCodeDTO.setRewardPoints(referralCodeValidation.getRewardPointsAsDouble());
					Double tPrice = Double.parseDouble(priceStr);
					if(referralCodeValidation.getCodeType().equals(CodeType.PROMOTIONAL)){
						if(referralCodeValidation.getIsFlatDiscount()!= null && referralCodeValidation.getIsFlatDiscount()){
							tPrice  =  (tPrice - referralCodeValidation.getDiscountConv());
							validateReferralCodeDTO.setPrice(Util.formatDoubleToTwoDecimalPoint(tPrice));
						}else{
							tPrice  = tPrice - (tPrice * referralCodeValidation.getDiscountConv());
							validateReferralCodeDTO.setPrice(Util.formatDoubleToTwoDecimalPoint(tPrice));
						}
					}else{
						validateReferralCodeDTO.setPrice(Util.formatDoubleToTwoDecimalPoint(tPrice));
					}
				}else{
					msg = referralCodeValidation.getError().getDescription();
					error.setDescription(msg);
					validateReferralCodeDTO.setError(error);
					validateReferralCodeDTO.setStatus(0);
					return validateReferralCodeDTO;
				}
				validateReferralCodeDTO.setStatus(1);
		    }

		    validateReferralCodeDTO.setMessage(msg);
		    
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Something went wrong while Validating Referral Code.");
			validateReferralCodeDTO.setError(error);
			validateReferralCodeDTO.setStatus(0);
		}
		return validateReferralCodeDTO;
	}
	
	@RequestMapping(value = "/DownloadETicket")
	public void downloadETicket(HttpServletRequest request, HttpServletResponse response){
		
		try {
			String ticketGroupIdStr = request.getParameter("ticketGroupId");
			String fileTypeStr = request.getParameter("fileType");
			String position = request.getParameter("position");
			
			if(ticketGroupIdStr!=null && !ticketGroupIdStr.isEmpty() && fileTypeStr!=null && !fileTypeStr.isEmpty()
					&& position !=null && !position.isEmpty()){
				FileType fileType = FileType.valueOf(fileTypeStr);
				TicketGroupTicketAttachment attachment = DAORegistry.getTicketGroupTicketAttachmentDAO().getAttachmentByTicketGroupFileTypeAndPosition(Integer.parseInt(ticketGroupIdStr), fileType,Integer.parseInt(position));
				if(attachment!=null){
					OutputStream out = response.getOutputStream();
					File file = new File(attachment.getFilePath());
					if(file.exists()){
						FileInputStream in = new FileInputStream(file);
						//response.setContentType("application/octet-stream");
						response.setContentType("application/pdf");
						response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"",file.getName()));
						byte[] buffer = new byte[4096];
						int length;
						while((length = in.read(buffer)) > 0){
						    out.write(buffer, 0, length);
						}
						in.close();
						out.flush();
					}
				}				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/*
	@RequestMapping(value = "/PaypalRefundTranList")
	public String paypalRefundTranList(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		try {
			String orderId = request.getParameter("orderId");
			String startDate = request.getParameter("startDate") != null ? request.getParameter("startDate") + "00:00:00" : null;
			String endDate = request.getParameter("endDate") != null ? request.getParameter("endDate") + "23:59:59" : null;
			SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			
			List<CustomerOrderPaypalRefund> customerOrderPaypalRefundList = DAORegistry.getCustomerOrderPaypalRefundDAO().getCustomerOrderPaypalRefundBySearchCriteria(orderId != null ? Integer.parseInt(orderId) : null, startDate != null ? dateFormatter.parse(startDate) : null, endDate != null ? dateFormatter.parse(endDate) : null);
			map.put("customerOrderPaypalRefundList", customerOrderPaypalRefundList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "page-paypal-refund-transaction-list";
	}
	
	
	public boolean sendCredentialMail(){
		Customer customer  = DAORegistry.getCustomerDAO().get(1);
		try {
			//String password = Util.generatePassword(customer.getCustomerName(), "RTF");
			Map<String, Object> mailMap = new HashMap<String, Object>();
			String password = "RTFYzdhYUhlbg";//Util.generatePassword(customer.getCustomerName(), "RTF");
			customer.setPassword(Util.generateEncrptedPassword(password));
			System.out.println(customer.getEmail());
			System.out.println(password);
			mailMap.put("password",password);
			mailMap.put("customerName",customer.getCustomerName()+" "+customer.getLastName());
			mailMap.put("userName", customer.getUserName());
			mailMap.put("androidUrl","https://play.google.com/store/apps/details?id=com.rewardthefan&hl=en");
			mailMap.put("iosUrl","https://itunes.apple.com/us/app/reward-the-fan/id1140367203?ls=1&mt=8");
			String path = "C:\\Program Files\\Apache Software Foundation\\Tomcat 7.0\\webapps\\ROOT\\resources\\images\\";
			com.rtw.tracker.mail.MailAttachment[] mailAttachment =Util.getRegistrationTemplateAttachment(path);
			String email = customer.getEmail();
			email = "msanghani@rightthisway.com";
			String bcc = "";
			//bcc = "amit.raut@rightthisway.com,msanghani@rightthisway.com,kulaganathan@rightthisway.com";
			mailManager.sendMailNow("text/html","sales@rewardthefan.com",email, 
					null,bcc, "You are successfully registered with RewardTheFan.com.",
		    		"mail-rewardfan-customer-registration.html", mailMap, "text/html", null,mailAttachment,null);
			//DAORegistry.getCustomerDAO().update(customer);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}*/
	


	/*
	@RequestMapping(value = "/CreateRealTix")
	public String createOpenOrders(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) {
		String invoiceId = "";
		List<TicketGroup> ticketGroups = new ArrayList<TicketGroup>();
		try {
			invoiceId = request.getParameter("invoiceId");
			String msg = request.getParameter("msg");			
			if(msg!=null && !msg.isEmpty()){
				map.put("successMessage",msg);
			}
			//Getting Broker ID
			HttpSession session = request.getSession(true);
			Integer brokerId = Util.getBrokerId(session);
			Integer invId = Integer.parseInt(invoiceId);
			List<OpenOrders> openOrderListByInv = DAORegistry
					.getQueryManagerDAO().getOpenOrderListByInvoice(invId, brokerId);
			Integer customerId = 0;
			for (OpenOrders openOrders : openOrderListByInv) {
				customerId = openOrders.getCustomerId();
			}
			
//			List<CustomerAddress> shippingAddressList = DAORegistry.getCustomerAddressDAO().getShippingAddressInfo(customerId);
			 
			List<CustomerAddress> shippingAddressList = DAORegistry
					.getCustomerAddressDAO().updatedShippingAddress(customerId);
			List<Ticket> tickets = DAORegistry.getTicketDAO().getMappedTicketsByInvoiceId(Integer.parseInt(invoiceId));
			List<Integer> list = new ArrayList<Integer>();
			Map<Integer,List<Ticket>> ticketGroupListMap = new HashMap<Integer, List<Ticket>>(); 
			List<Ticket> ticketList = new ArrayList<Ticket>();
			if(tickets!=null && !tickets.isEmpty()){
				for(Ticket tic : tickets){
					if(!list.contains(tic.getTicketGroupId())){
						list.add(tic.getTicketGroupId());
					}
					if(ticketGroupListMap.get(tic.getTicketGroupId())!=null){
						ticketList = ticketGroupListMap.get(tic.getTicketGroupId());
						ticketList.add(tic);
					}else{
						ticketList = new ArrayList<Ticket>();
						ticketList.add(tic);
					}
					ticketGroupListMap.put(tic.getTicketGroupId(),ticketList);
				}
				for(Integer ticketGroupId : list){
					TicketGroup ticketGroup = DAORegistry.getTicketGroupDAO().get(ticketGroupId);
					ticketList = ticketGroupListMap.get(ticketGroupId);
					if(ticketList!=null && !ticketList.isEmpty()){
						ticketGroup.setMappedSeatLow(ticketList.get(0).getSeatNo());
						ticketGroup.setMappedSeatHigh(ticketList.get(ticketList.size()-1).getSeatNo());
						ticketGroup.setMappedQty(ticketList.size());
					}
					ticketGroups.add(ticketGroup);
				}
			}
			
			OpenOrders operOrder = null;
			if(openOrderListByInv != null && !openOrderListByInv.isEmpty()) {
				operOrder = openOrderListByInv.get(0);
			}
			Invoice invoice = DAORegistry.getInvoiceDAO().get(Integer.parseInt(invoiceId));
			CustomerOrder customerOrder = DAORegistry.getCustomerOrderDAO().get(invoice.getCustomerOrderId());
			List<InvoiceTicketAttachment> eticketAttachments = DAORegistry.getInvoiceTicketAttachmentDAO().getAttachmentByInvoiceAndFileType(Integer.parseInt(invoiceId),FileType.ETICKET);			
			for(InvoiceTicketAttachment eTicket: eticketAttachments){
				eTicket.setFileName(Util.getFileNameFromPath(eTicket.getFilePath()));
			}
			List<InvoiceTicketAttachment> qrcodeAttachments = DAORegistry.getInvoiceTicketAttachmentDAO().getAttachmentByInvoiceAndFileType(Integer.parseInt(invoiceId),FileType.QRCODE);
			for(InvoiceTicketAttachment qrCode: qrcodeAttachments){
				qrCode.setFileName(Util.getFileNameFromPath(qrCode.getFilePath()));
			}
			List<InvoiceTicketAttachment> barcodeAttachments = DAORegistry.getInvoiceTicketAttachmentDAO().getAttachmentByInvoiceAndFileType(Integer.parseInt(invoiceId),FileType.BARCODE);
			for(InvoiceTicketAttachment barCode: barcodeAttachments){
				barCode.setFileName(Util.getFileNameFromPath(barCode.getFilePath()));
			}
			if(invoice!=null){
				map.put("invoice", invoice);
				if(invoice.getExpDeliveryDate()!=null){
					DateFormat format = new SimpleDateFormat("mm/DD/yyyy");
					map.put("expDeliveryDate", format.format(invoice.getExpDeliveryDate()));
				}
			}
			Collection<ShippingMethod> shippingMethods = DAORegistry.getShippingMethodDAO().getAll();
			map.put("shippingMethods", shippingMethods);
			map.put("eticketAttachments", eticketAttachments);
			map.put("qrcodeAttachments", qrcodeAttachments);
			map.put("barcodeAttachments", barcodeAttachments);
			map.put("eticketCount", eticketAttachments.size());
			map.put("qrcodeCount", qrcodeAttachments.size());
			map.put("barcodeCount", barcodeAttachments.size());
			map.put("customerOrder", customerOrder);
			map.put("openOrders", openOrderListByInv);
			map.put("shippingAddress", shippingAddressList);
			map.put("ticketGroups", ticketGroups);
			map.put("openOrder", operOrder);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("errorMessage",
					"There is something wrong. Please try again..");
		}
		return "page-create-invoice-real-tix";
	}
	
	/** 
	 * Fetch list of open orders for invoice
	 * @param request
	 * @param response
	 * @param map
	 * @return
	 */
	/*@RequestMapping(value = "/CreateOpenOrder")
	public String createOpenOrders(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		String returnMessage = "";
		String action = "";
		String invoiceId = "";
		try {
			invoiceId = request.getParameter("invoiceId");
			Integer invId = Integer.parseInt(invoiceId);
			Collection<OpenOrders> openOrderListByInv = DAORegistry.getQueryManagerDAO().getOpenOrderListByInvoice(invId);
			Integer customerId = 0;
			Integer eventId = 0;
			for(OpenOrders openOrders: openOrderListByInv){
				customerId = openOrders.getCustomerId();
				eventId = openOrders.getEventId();
			}
			
			List<CustomerAddress> shippingAddressList = DAORegistry.getCustomerAddressDAO().getShippingAddressInfo(customerId);
			List<CustomerAddress> shippingAddressList = DAORegistry.getCustomerAddressDAO().updatedShippingAddress(customerId);
			EventDetails eventDetail = null;
			if(eventId != null){
				eventDetail = DAORegistry.getEventDetailsDAO().get(eventId);
			}			
			map.put("eventDtl", eventDetail);
			map.put("openOrders", openOrderListByInv);
			map.put("shippingAddress", shippingAddressList);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("errorMessage", "There is something wrong. Please try again..");
		}
		return "page-create-openorder";
	}*/
	
	
	/**
	 * Upload ticket
	 * @param uploadedFile
	 * @param result
	 * @return
	 * @throws Exception 
	 */
	/*@RequestMapping(value = "/ticketUpload")
	public String ticketUpload(@ModelAttribute("uploadedFile")UploadedFile uploadedFile, 
			BindingResult result, ModelMap map, HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		String action = "";
		String invoiceId = request.getParameter("invoiceId");
		String barCode = "";
		String returnMessage = "";
		
		InputStream inputStream = null;
		OutputStream outputStream = null;
		//File upload starts
		

		try {
			Collection<OpenOrders> openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrderList(null,null,null,null,null,null,null);
			if(openOrdersList.size() > 0){
				map.put("openOrders", openOrdersList);
			}else{
				returnMessage = "No Open Orders found";
				map.put("message",returnMessage);
			}
			action = request.getParameter("action");
			if(action != null && action.equals("uploadTicket")){
				MultipartFile file = uploadedFile.getFile();
				fileValidator.validate(uploadedFile, result);

				String fileName = file.getOriginalFilename();

				if (result.hasErrors()) {
					return "page-open-order";
				}
				
				inputStream = file.getInputStream();
				File newFile = new File("C:/Dhivakar/Tomcat7-DevRTW/tixUploads/" + fileName);
	
				if (!newFile.exists()) {
					newFile.createNewFile();
				}
				outputStream = new FileOutputStream(newFile);
				int read = 0;
				byte[] bytes = new byte[1024];
	
				while ((read = inputStream.read(bytes)) != -1) {
					outputStream.write(bytes, 0, read);
				}
				
				Integer invId = Integer.parseInt(invoiceId);
				DAORegistry.getInvoiceDAO().updateTixUploaded(invId);
				
				map.put("message", fileName);
			}else if(action != null && action.equals("saveBarCode")){
				barCode = request.getParameter("barCode");
				String id = request.getParameter("invoiceId");
				Integer invId = Integer.parseInt(id);
				DAORegistry.getInvoiceDAO().updateBarCode(barCode, invId);
				
				map.put("message", "Barcode ");
			}
			map.put("openOrders", openOrdersList);
		} catch (IOException e) {
			e.printStackTrace();
			map.put("error", "There is something wrong.Please try again..");
		}
		return "page-open-order";
	}*/
	/*
	@RequestMapping(value = "/DeletePO", method=RequestMethod.POST)
	@ResponseBody
	public String deleteCustomer(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		try{
			TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
			if(trackerUserSession == null){
				return "Please Login.";
			}
			
			String poIdStr = request.getParameter("poId");
			PurchaseOrder purchaseOrder = DAORegistry.getPurchaseOrderDAO().get(Integer.parseInt(poIdStr));
			if(purchaseOrder == null){
				return "There is no purchase order to delete.";
			}
			
			DAORegistry.getPurchaseOrderDAO().delete(purchaseOrder);
			return "true";
		}catch(Exception e){
			e.printStackTrace();
			return "There is something wrong. Please try again.";
		}
	}
	*/
		

	
}

