package com.rtw.tracker.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.mapping.Array;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.rtw.tmat.dao.implementation.TMATDAORegistry;
import com.rtw.tmat.data.SVGFileMissingZoneData;
import com.rtw.tmat.utils.Error;
import com.rtw.tmat.utils.TNDEventsSalesReportScheduler;
import com.rtw.tmat.utils.Util;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.dao.implementation.QuizDAORegistry;
import com.rtw.tracker.datas.TicketMasterRefund;
import com.rtw.tracker.datas.TicketMasterRefundCCNotMatched;
import com.rtw.tracker.datas.TicketMasterRefundKnownEmail;
import com.rtw.tracker.datas.Artist;
import com.rtw.tracker.datas.Customers;
import com.rtw.tracker.datas.TicketGroup;
import com.rtw.tracker.datas.TicketMasterRefund;
import com.rtw.tracker.pojos.BotDetail;
import com.rtw.tracker.pojos.EventStatisticsReportDTO;
import com.rtw.tracker.pojos.GenericResponseDTO;
import com.rtw.tracker.pojos.ReportPojo;
import com.rtw.tracker.utils.Constants;
import com.rtw.tracker.utils.ExcelUtil;
import com.rtw.tracker.utils.URLUtil;

@Controller
@RequestMapping({"/Reports"})
public class ReportsController {

	private SharedProperty sharedProperty;
	
	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}

	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}


	@RequestMapping({"/DownloadTNDEventSalesReport"})
	public void downloadTNDEventSalesEventDetailReport(HttpServletRequest request, HttpServletResponse response){
		try{
			SXSSFWorkbook workbook = TNDEventsSalesReportScheduler.getTNDEventSalesReportExcelFile();
			String fileName = "EventLast3DaysSales.xlsx";
//			System.out.println(fileName);
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition", "attachment; filename="+fileName);
			
			workbook.write(response.getOutputStream());
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	@RequestMapping(value = "/UploadPOFile")
	public synchronized String  uploadPOFiles(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {		
		String message = "";
		String msg = "";
		XSSFWorkbook wb = null;
		try {
			File f = null;
			DiskFileItemFactory  fileItemFactory = new DiskFileItemFactory ();
			File ticketFile = new File(URLUtil.TICKET_DIRECTORY);
	        fileItemFactory.setRepository(ticketFile);
	        ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
	        List items = uploadHandler.parseRequest(request);
	        Iterator iterator = items.iterator();
	        List<TicketMasterRefund> refunds = new ArrayList<TicketMasterRefund>();
	        String ext ="";
	        boolean isQtyMissing = false;
	        boolean isCostMissing = false;
	        	        
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			Date today = new Date();
			String fileName = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(today);
			fileName = fileName.replaceAll(":", "_");
			fileName = fileName.replaceAll("/", "_");
			fileName = fileName.replaceAll(" ", "_");
			if(msg.isEmpty()) {
				MultipartFile file = multipartRequest.getFile("poFile");
				if(file!=null){
					ext = FilenameUtils.getExtension(file.getOriginalFilename());
					f = new File(URLUtil.REPORT_TEMPLATE_DIRECTORY+"PO_"+fileName+"."+ext);
					f.createNewFile();
					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(f));
			        FileCopyUtils.copy(file.getInputStream(), stream);
					stream.close();
				}else{
					msg="File not found";
					model.addAttribute("msg", msg);
					return "";
				}
				/*Iterator iterator1 = items.iterator();
				while(iterator1.hasNext()){
		        	FileItem item = (FileItem)iterator1.next();
		        	if(!item.isFormField()){
		        		ext = FilenameUtils.getExtension(file.getOriginalFilename());
		        		f = new File(com.rtw.tracker.utils.Constants.Real_ticket_Path+eventIdStr+"_"+brokerId+"."+ext);
		        		item.write(f);
		        	}
		        }*/
				
			}
			
			TicketMasterRefund ref = null;
			if(msg.isEmpty() && f!=null && f.length() > 0){
				if(ext.equalsIgnoreCase("csv")){
					Scanner scanner = new Scanner(f);
			        while (scanner.hasNext()) {
			            String line = scanner.nextLine();
			            String arr[] = null;
			            System.out.println("====================================");
			            if(line!=null && !line.isEmpty()){
			            	arr = line.split(",");
			            	
			            }
			           /* if(arr[0]==null || arr[0].isEmpty()){
			            	continue;
			            }*/
			            if(arr.length> 0){
			            	 ref = new TicketMasterRefund();
			            	 ref.setCreatedDate(today);
			            	 for(int i=0;i<arr.length;i++){
		            			switch(i){
			            		case 0:
			            			ref.setInvoiceDate(arr[i]);
			                		break;
			                	case 1:
			                		try {
			                			ref.setTicketCost(Double.parseDouble(arr[i].trim()));
									} catch (Exception e) {
										e.printStackTrace();
										isCostMissing = true;
									}
			                		System.out.println(arr[i]);
			                		break;
			                	case 2:
			                		ref.setPoId(arr[i]);
			                		System.out.println(arr[i]);
			                		break;
			                	case 3:
			                		ref.setCcName(arr[i]);
			                		System.out.println(arr[i]);
			                		break;
			                	case 4:
			                		ref.setPoDate(arr[i]);
			                		System.out.println(arr[i]);
			                		break;
			                	case 5:
			                		ref.setVendor(arr[i]);
			                		System.out.println(arr[i]);
			                		break;
			                	case 6:
			                		ref.setEventName(arr[i]);
			                		System.out.println(arr[i]);
			                		break;
			                	case 7:
			                		ref.setEventDate(arr[i]);
			                		System.out.println(arr[i]);
			                		break;
			                	case 8:
			                		ref.setVenue(arr[i]);
			                		System.out.println(arr[i]);
			                		break;
			                	case 9:
			                		try {
										ref.setTicketQty(Integer.parseInt(arr[i]));
									} catch (Exception e) {
										e.printStackTrace();
										isQtyMissing = true;
									}
			                		System.out.println(arr[i]);
			                		break;
			                	case 10:
			                		try {
			                			ref.setRefundQty(Integer.parseInt(arr[i]));
									} catch (Exception e) {
										e.printStackTrace();
									}
			                		System.out.println(arr[i]);
			                		break;
			                	case 11:
			                		try {
										ref.setRefundCost(Double.parseDouble(arr[i]));
									} catch (Exception e) {
										e.printStackTrace();
									}
			                		System.out.println(arr[i]);
			                		break;
			                	case 12:
			                		ref.setSection(arr[i]);
			                		System.out.println(arr[i]);
			                		break;
			                	case 13:
			                		ref.setRow(arr[i]);
			                		System.out.println(arr[i]);
			                		break;
			                	case 14:
			                		ref.setInternalNotes(arr[i]);
			                		System.out.println(arr[i]);
			                		break;
			                	case 15:
			                		ref.setEmail(arr[i]);
			                		System.out.println(arr[i]);
			                		break;
			                	case 16:
			                		ref.setOldEmail(arr[i]);
			                		System.out.println(arr[i]);
			                		break;
			                	case 17:
			                		ref.setError(arr[i]);
			                		System.out.println(arr[i]);
			                		break;
		            			}
			            	}
			            }
			            refunds.add(ref); 
			        }
			        scanner.close();
				}else if(ext.equalsIgnoreCase("xls") || ext.equalsIgnoreCase("xlsx")){
					FileInputStream stream = new FileInputStream(f);
					wb = new XSSFWorkbook(stream);
					Sheet firstSheet = wb.getSheetAt(0);
			        Iterator<Row> it = firstSheet.iterator();
			        
			        while (it.hasNext()) {
			            Row nextRow = it.next();
			            if(nextRow.getRowNum()==0){
			            	continue;
			            }
			            System.out.println("=============================================");
			            ref = new TicketMasterRefund();
			            ref.setCreatedDate(today);
			            Iterator<Cell> cellIterator = nextRow.cellIterator();
			            while (cellIterator.hasNext()) {
			                Cell cell = cellIterator.next();
			                switch (cell.getColumnIndex()){
			                	case 0:
			                		cell.setCellType(CellType.STRING);
			                		try {
										Date date = DateUtil.getJavaDate(Double.parseDouble(cell.getStringCellValue()));
										ref.setInvoiceDate(Util.getTimeStamp(date));
									} catch (Exception e) {
										ref.setInvoiceDate(cell.getStringCellValue());
									}
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 1:
			                		try {
			                			if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC ){
			                				ref.setTicketCost(cell.getNumericCellValue());
			                				System.out.println(cell.getNumericCellValue());
				                		}else{
				                			cell.setCellType(CellType.STRING);
				                			ref.setTicketCost(Double.parseDouble(cell.getStringCellValue()));
				                			System.out.println(cell.getStringCellValue());
				                		}
									} catch (Exception e) {
										e.printStackTrace();
										isCostMissing = true;
									}
			                		break;
			                	case 2:
			                		cell.setCellType(CellType.STRING);
			                		ref.setPoId(cell.getStringCellValue());
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 3:
			                		cell.setCellType(CellType.STRING);
			                		ref.setCcName(cell.getStringCellValue());
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 4:
			                		cell.setCellType(CellType.STRING);
			                		try {
										Date date = DateUtil.getJavaDate(Double.parseDouble(cell.getStringCellValue()));
										ref.setPoDate(Util.getTimeStamp(date));
									} catch (Exception e) {
										ref.setPoDate(cell.getStringCellValue());
									}
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 5:
			                		cell.setCellType(CellType.STRING);
			                		ref.setVendor(cell.getStringCellValue());
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 6:
			                		cell.setCellType(CellType.STRING);
			                		ref.setEventName(cell.getStringCellValue());
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 7:
			                		cell.setCellType(CellType.STRING);
			                		try {
										Date date = DateUtil.getJavaDate(Double.parseDouble(cell.getStringCellValue()));
										ref.setEventDate(Util.getTimeStamp(date));
									} catch (Exception e) {
										ref.setEventDate(cell.getStringCellValue());
									}
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 8:
			                		cell.setCellType(CellType.STRING);
			                		ref.setVenue(cell.getStringCellValue());
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 9:
			                		try {
			                			cell.setCellType(CellType.STRING);
			                			ref.setTicketQty(Integer.parseInt(cell.getStringCellValue()));
									} catch (Exception e) {
										e.printStackTrace();
										isQtyMissing = true;
									}
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 10:
			                		try {
			                			cell.setCellType(CellType.STRING);
			                			ref.setRefundQty(Integer.parseInt(cell.getStringCellValue()));
									} catch (Exception e) {
										e.printStackTrace();
									}
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 11:
			                		try {
			                			if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC ){
			                				ref.setRefundCost(cell.getNumericCellValue());
			                				System.out.println(cell.getNumericCellValue());
				                		}else{
				                			cell.setCellType(CellType.STRING);
				                			ref.setRefundCost(Double.parseDouble(cell.getStringCellValue()));
				                			System.out.println(cell.getStringCellValue());
				                		}
									} catch (Exception e) {
										e.printStackTrace();
									}
			                		break;
			                	case 12:
			                		cell.setCellType(CellType.STRING);
			                		ref.setSection(cell.getStringCellValue());
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 13:
			                		cell.setCellType(CellType.STRING);
			                		ref.setRow(cell.getStringCellValue());
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 14:
			                		cell.setCellType(CellType.STRING);
			                		String str2 = cell.getStringCellValue();
			                		if(str2 == null ||  str2.equalsIgnoreCase("null")){
			                			str2= "";
			                		}
			                		ref.setInternalNotes(str2);
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 15:
			                		cell.setCellType(CellType.STRING);
			                		String str3 = cell.getStringCellValue();
			                		if(str3 == null ||  str3.equalsIgnoreCase("null")){
			                			str3= "";
			                		}
			                		ref.setEmail(str3);
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 16:
			                		cell.setCellType(CellType.STRING);
			                		String str1 = cell.getStringCellValue();
			                		if(str1 == null ||  str1.equalsIgnoreCase("null")){
			                			str1= "";
			                		}
			                		ref.setOldEmail(str1);
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 17:
			                		cell.setCellType(CellType.STRING);
			                		String str = cell.getStringCellValue();
			                		if(str == null ||  str.equalsIgnoreCase("null")){
			                			str= "";
			                		}
			                		ref.setError(str);
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	
			                }
			            }
			            refunds.add(ref);
			        }
				}
				
				if(!refunds.isEmpty()){
		        	if(isCostMissing){
		        		 msg = "Cannot upload data, Found invalid Ticket cost amount in one or more rows.";
		        		 model.addAttribute("msg", msg);
		        		 return "";
		        	}else if(isQtyMissing){
		        		 msg = "Cannot upload data, Found invalid Ticket quantity in one or more rows.";
		        		 model.addAttribute("msg", msg);
		        		 return "";
		        	}else{
		        		DAORegistry.getQueryManagerDAO().removeTicketMasterRefundData();
						DAORegistry.getTicketMasterRefundDAO().saveAll(refunds);
						msg = "Data Uploaded successfully.";
						model.addAttribute("msg", msg);
						return "";
		        	}
		        }
			}else{
				msg = "No Data Found in File.";
				model.addAttribute("msg", msg);
        		return "";
			}
			
			//IOUtils.write(msg.getBytes(), response.getOutputStream());
		} catch (Exception e) {			
			e.printStackTrace();
			model.addAttribute("msg", "Something went wrong while uploading file.");
		}
		/*try {
			List<TicketMasterRefund> dataList = DAORegistry.getQueryManagerDAO().getTmPurchaseRefundsAmounts();
			
			Date processedDate = new Date();
			DAORegistry.getQueryManagerDAO().saveTmCcHoldersRefunds(dataList, processedDate);
			
		} catch(Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "Something went wrong while processing data.");
		}*/
		
		/*try {
			XSSFWorkbook sxssfWorkbook = downloadTmPurchaseRefundsReport(wb);
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename=TM_Purchase_Refunds." +Constants.EXCEL_EXTENSION);
			sxssfWorkbook.write(response.getOutputStream());
			
		} catch(Exception e) {
			e.printStackTrace();
		}*/
		return "";
	}
	
	
	@RequestMapping({"/DownloadPointsSummeryReport"})
	public void downloadPointsSummaryReport(HttpServletRequest request, HttpServletResponse response){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Pending Points");
			headerList.add("Active Points");
			headerList.add("Total Points");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet();
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			//ExcelUtil.generateExcelData(DAORegistry.getQueryManagerDAO(), ssSheet);
			List<Object[]> dataList = DAORegistry.getQueryManagerDAO().getRewardPointSummary();
			Row rowhead =   ssSheet.createRow(1);
			Double totalPoints = 0.00;
			for(Object[] dataObj : dataList){
				int i=0;
				for(Object data : dataObj){
					if(data != null){
						totalPoints += Double.parseDouble(data.toString());
						rowhead.createCell(i).setCellValue(Double.parseDouble(data.toString()));
					}else{
						rowhead.createCell(i).setCellValue("");
					}
					i++;
				}
				rowhead.createCell(2).setCellValue(totalPoints);
			}
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.POINTS_SUMMARY_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping({"/DownloadEventsNotInRTFReport"})
	public void downloadEventsNotInRTFReport(HttpServletRequest request, HttpServletResponse response){		
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Event Id");
			headerList.add("Event Updated DateAndTime");
			headerList.add("Artist Name");
			headerList.add("Event Name");
			headerList.add("Event Date");
			headerList.add("Event Time");			
			headerList.add("Venue Name");
			headerList.add("City");
			headerList.add("State");
			headerList.add("Country");
			headerList.add("Parent Category");
			headerList.add("Child Category");
			headerList.add("GrandChild Category");
			headerList.add("Venue Category Group Name");
			headerList.add("Venue Map");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet();
			CreationHelper createHelper = workbook.getCreationHelper();
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			//ExcelUtil.generateExcelData(DAORegistry.getEventDAO().getEventsNotInRTF(), ssSheet);
			List<Object[]> dataList = DAORegistry.getEventDAO().getEventsNotInRTF();
			
			int j = 0;
			int rowCount = 1;
			
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);
				CellStyle cellStyle = workbook.createCellStyle();				
				CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
				cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
				cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[1].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[4].toString()));
					cell.setCellStyle(cellStyle);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
						
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
						
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[13].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					Boolean venueMap = Boolean.parseBoolean(dataObj[14].toString());
					if(venueMap != null && venueMap){
						rowhead.createCell((int) j).setCellValue("Yes");
					}else{
						rowhead.createCell((int) j).setCellValue("No");
					}					
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.EVENTS_IN_TMAT_BUT_NOT_IN_RTF+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}		
	}
	
	@RequestMapping({"/DownloadRTFUnFilledReport"})
	public void downloadRTFUnFilledReport(HttpServletRequest request, HttpServletResponse response){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Invoice Id");
			headerList.add("Invoice Created Date");		
			headerList.add("Event Category");
			headerList.add("Event Name");
			headerList.add("Event Date");
			headerList.add("Event Time");
			headerList.add("Venue Name");
			headerList.add("Section");
			headerList.add("Row");
			headerList.add("Section Range");
			headerList.add("Row Range");			
			headerList.add("Invoice Ticket Qty");
			headerList.add("Sold Ticket Qty");					
			headerList.add("Invoice Total");			
			headerList.add("Ticket Price");
			headerList.add("Sold Price");			
			headerList.add("Actual Sold Price");			
			headerList.add("Total Sold Price");
			headerList.add("Fees");
			headerList.add("Net Sold Price");	
			headerList.add("Client Name");
			headerList.add("Client Email");
			headerList.add("Client Phone");
			headerList.add("Client BillingAddress");
			headerList.add("Client ShippingAddress");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet();
			CreationHelper createHelper = workbook.getCreationHelper();
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			//ExcelUtil.generateExcelData(DAORegistry.getEventDAO().getRTFUnFilledReport(), ssSheet);
			List<Object[]> dataList = DAORegistry.getEventDAO().getRTFUnFilledReport();

			Double sumInvoiceTktQuantity = 0.0, sumSoldTktQuantity = 0.0, sumInvoiceTotal = 0.0, sumTicketPrice = 0.0;
			Double sumSoldPrice = 0.0, sumActualSoldPrice = 0.0;
			Double sumTotalSoldPrice = 0.0, sumFees = 0.0, sumNetSoldPrice =0.0;
			Double invoiceTktQuantity = 0.0, soldTktQuantity = 0.0, invoiceTotal = 0.0, ticketPrice = 0.0;			
			Double soldPrice = 0.0, actualSoldPrice = 0.0;
			Double totalSoldPrice = 0.0, fees = 0.0, netSoldPrice =0.0;
			int j = 0;
			int rowCount = 1;
			
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);
				CellStyle cellStyle = workbook.createCellStyle();				
				CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
				cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
				cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[1].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[4].toString()));
					cell.setCellStyle(cellStyle);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
						
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
						
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					invoiceTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[11].toString()));
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + invoiceTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTktQuantity));
				}else{
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					soldTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[12].toString()));
					sumSoldTktQuantity = sumSoldTktQuantity + soldTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(soldTktQuantity));
				}else{
					sumSoldTktQuantity = sumSoldTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					invoiceTotal = Util.getRoundedValue(Double.parseDouble(dataObj[13].toString()));
					sumInvoiceTotal = sumInvoiceTotal + invoiceTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTotal));
				}else{
					sumInvoiceTotal = sumInvoiceTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					ticketPrice = Util.getRoundedValue(Double.parseDouble(dataObj[14].toString()));
					sumTicketPrice = sumTicketPrice + ticketPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(ticketPrice));
				}else{
					sumTicketPrice = sumTicketPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					soldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[15].toString()));
					sumSoldPrice = sumSoldPrice + soldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(soldPrice));
				}else{
					sumSoldPrice = sumSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					actualSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[16].toString()));
					sumActualSoldPrice = sumActualSoldPrice + actualSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(actualSoldPrice));
				}else{
					sumActualSoldPrice = sumActualSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[17] != null){
					totalSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[17].toString()));
					sumTotalSoldPrice = sumTotalSoldPrice + totalSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalSoldPrice));
				}else{
					sumTotalSoldPrice = sumTotalSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[18] != null){
					fees = Util.getRoundedValue(Double.parseDouble(dataObj[18].toString()));
					sumFees = sumFees + fees;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(fees));
				}else{
					sumFees = sumFees + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[19] != null){
					netSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[19].toString()));
					sumNetSoldPrice = sumNetSoldPrice + netSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(netSoldPrice));
				}else{
					sumNetSoldPrice = sumNetSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[20] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[20].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[21] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[21].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[22] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[22].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[23] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[23].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[24] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[24].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataList != null && dataList.size() > 0){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSoldTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTicketPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumActualSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumFees));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumNetSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename=" + Constants.RTF_UNFILLED_REPORT + "."+Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping({"/DownloadRTFFilledReport"})
	public void downloadRTFFilledReport(HttpServletRequest request, HttpServletResponse response){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Invoice Id");
			headerList.add("Invoice Created Date");
			headerList.add("Customer Order Id");
			headerList.add("Customer Order Created Date");
			headerList.add("PO Id");
			headerList.add("PO Created Date");			
			headerList.add("Event Category");
			headerList.add("Event Name");
			headerList.add("Event Date");
			headerList.add("Event Time");
			headerList.add("Venue Name");
			headerList.add("Section Range");
			headerList.add("Row Range");			
			headerList.add("Invoice Ticket Qty");
			headerList.add("Sold Ticket Qty");					
			headerList.add("Invoice Total");			
			headerList.add("Ticket Price");
			headerList.add("Sold Price");			
			headerList.add("Actual Sold Price");
			headerList.add("Ticket Cost");
			headerList.add("Total Ticket Cost");			
			headerList.add("Total Sold Price");
			headerList.add("Fees");
			headerList.add("Net Sold Price");
			headerList.add("ActualProfitOrLoss");
			headerList.add("Gross Margin");			
			headerList.add("Client Name");
			headerList.add("Client Email");
			headerList.add("Client Phone");
			headerList.add("Client BillingAddress");
			headerList.add("Client ShippingAddress");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			CreationHelper createHelper = workbook.getCreationHelper();
			Sheet ssSheet =  workbook.createSheet();
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			//ExcelUtil.generateExcelData(DAORegistry.getEventDAO().getRTFFilledReport(), ssSheet);
			List<Object[]> dataList = DAORegistry.getEventDAO().getRTFFilledReport();
			
			Double sumInvoiceTktQuantity = 0.0, sumSoldTktQuantity = 0.0, sumInvoiceTotal = 0.0, sumTicketPrice = 0.0;
			Double sumSoldPrice = 0.0, sumActualSoldPrice = 0.0, sumTicketCost = 0.0, sumTotalTicketCost = 0.0;
			Double sumTotalSoldPrice = 0.0, sumFees = 0.0, sumNetSoldPrice =0.0, sumProfitOrLoss = 0.0, sumGrossMargin = 0.0;
			Double invoiceTktQuantity = 0.0, soldTktQuantity = 0.0, invoiceTotal = 0.0, ticketPrice = 0.0;			
			Double soldPrice = 0.0, actualSoldPrice = 0.0, ticketCost = 0.0, totalTicketCost = 0.0;
			Double totalSoldPrice = 0.0, fees = 0.0, netSoldPrice =0.0, profitOrLoss = 0.0, grossMargin = 0.0;
			int j = 0;
			int rowCount = 1;
			
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);
				CellStyle cellStyle = workbook.createCellStyle();				
				CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
				cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
				cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[1].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[3].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[5].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[8].toString()));
					cell.setCellStyle(cellStyle);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
						
				if(dataObj[12] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					invoiceTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[13].toString()));
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + invoiceTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTktQuantity));
				}else{
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					soldTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[14].toString()));
					sumSoldTktQuantity = sumSoldTktQuantity + soldTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(soldTktQuantity));
				}else{
					sumSoldTktQuantity = sumSoldTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					invoiceTotal = Util.getRoundedValue(Double.parseDouble(dataObj[15].toString()));
					sumInvoiceTotal = sumInvoiceTotal + invoiceTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTotal));
				}else{
					sumInvoiceTotal = sumInvoiceTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					ticketPrice = Util.getRoundedValue(Double.parseDouble(dataObj[16].toString()));
					sumTicketPrice = sumTicketPrice + ticketPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(ticketPrice));
				}else{
					sumTicketPrice = sumTicketPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[17] != null){
					soldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[17].toString()));
					sumSoldPrice = sumSoldPrice + soldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(soldPrice));
				}else{
					sumSoldPrice = sumSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[18] != null){
					actualSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[18].toString()));
					sumActualSoldPrice = sumActualSoldPrice + actualSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(actualSoldPrice));
				}else{
					sumActualSoldPrice = sumActualSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[19] != null){
					ticketCost = Util.getRoundedValue(Double.parseDouble(dataObj[19].toString()));
					sumTicketCost = sumTicketCost + ticketCost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(ticketCost));
				}else{
					sumTicketCost = sumTicketCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[20] != null){
					totalTicketCost = Util.getRoundedValue(Double.parseDouble(dataObj[20].toString()));
					sumTotalTicketCost = sumTotalTicketCost + totalTicketCost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalTicketCost));
				}else{
					sumTotalTicketCost = sumTotalTicketCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[21] != null){
					totalSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[21].toString()));
					sumTotalSoldPrice = sumTotalSoldPrice + totalSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalSoldPrice));
				}else{
					sumTotalSoldPrice = sumTotalSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[22] != null){
					fees = Util.getRoundedValue(Double.parseDouble(dataObj[22].toString()));
					sumFees = sumFees + fees;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(fees));
				}else{
					sumFees = sumFees + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[23] != null){
					netSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[23].toString()));
					sumNetSoldPrice = sumNetSoldPrice + netSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(netSoldPrice));
				}else{
					sumNetSoldPrice = sumNetSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[24] != null){
					profitOrLoss = Util.getRoundedValue(Double.parseDouble(dataObj[24].toString()));
					sumProfitOrLoss = sumProfitOrLoss + profitOrLoss;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(profitOrLoss));
				}else{
					sumProfitOrLoss = sumProfitOrLoss + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[25] != null){
					grossMargin = Util.getRoundedValue(Double.parseDouble(dataObj[25].toString()));
					sumGrossMargin = sumGrossMargin + grossMargin;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(grossMargin));
				}else{
					sumGrossMargin = sumGrossMargin + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[26] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[26].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[27] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[27].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[28] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[28].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[29] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[29].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[30] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[30].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataList != null && dataList.size() > 0){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSoldTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTicketPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumActualSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTicketCost));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalTicketCost));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumFees));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumNetSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumProfitOrLoss));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumGrossMargin));
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_FILLED_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

//	@RequestMapping({"/LoadRTFZonesNotHavingColorsReport"})
//	public String loadRTFZonesNotHavingColorsReport(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session,
//			@RequestParam(name = "startDate",required = false) String startDate,
//			@RequestParam(name = "endDate",required = false) String endDate) throws IOException{
//		
//				SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
//				Calendar cal = Calendar.getInstance(); 
//				//map.addAttribute("startDate", dateFormat.format(cal.getTime()));
//				//map.addAttribute("endDate", dateFormat.format(cal.getTime()));
//				return "page-map-not-having-zones-colors-report";
//	}
	
	@RequestMapping({"/DownloadRTFZonesNotHavingColorsReport"})
	public void downloadRTFZonesNotHavingColorsReport(HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat dbDateFormatter = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		ServletOutputStream out = response.getOutputStream();
				
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_ZONES_NOT_HAVING_COLORS+"." + today
							+ ".xlsx");
		
		String startDateJsp = request.getParameter("startDate");
		String endDateJsp = request.getParameter("endDate");
		String artistIdStr = request.getParameter("artistId");
		
		String startDateStr = null,endDateStr = null;
		String artistName = null;
		Artist artist = null;
		try {
			if(startDateJsp != null && !startDateJsp.equals("")) {
				startDateStr = dbDateFormatter.format(dateFormatter.parse(startDateJsp))+" 00:00:00";
			}
			if(endDateJsp != null && !endDateJsp.equals("")) {
				endDateStr = dbDateFormatter.format(dateFormatter.parse(endDateJsp))+" 23:59:59";
			}
		} catch (Exception e) {
		}
		if(artistIdStr != null && !artistIdStr.equals("")) {
			artist = DAORegistry.getArtistDAO().get(Integer.parseInt(artistIdStr));
		}
		if(artist != null) {
			artistName = artist.getName();
		}
				
		try{
			 
			List<Object[]> list = TMATDAORegistry.getQueryManagerDAO().getAllVenueMapZonesWihoutListingsinRTF(artistName,startDateStr,endDateStr);
			 
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet();
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));				
			//CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			//cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0;
				Row rowhead =   ssSheet.createRow((int)0);
			    rowhead.createCell((int) j).setCellValue("Event Id");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Event Name");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Event Date");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Event Time");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Artist Name");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Venue");
			    j++;
		    	rowhead.createCell((int) j).setCellValue("City");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("State");
		 	    j++;
		 	    rowhead.createCell((int) j).setCellValue("Country");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("Zone");
		    	j++;
			    
			    
			    if(list != null) {
			    	 int rowvariable = 1;
					 for (Object[] objArr : list) {
						 
							 
						 int column = 0;
						Row row = ssSheet.createRow(rowvariable);
						if(objArr[0]!= null) {//Event Id
							row.createCell((int) column).setCellValue(objArr[0].toString());
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
						if(objArr[1]!= null) {//Event Name
							row.createCell((int) column).setCellValue(objArr[1].toString());
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
						if(objArr[2]!= null) {//Event Date
							Cell cell = row.createCell((int) column);
							cell.setCellValue(new Date(objArr[2].toString()));
							cell.setCellStyle(cellStyle);
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
						if(objArr[3]!= null) {//Event Time
							row.createCell((int) column).setCellValue(objArr[3].toString());
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
						if(objArr[4]!= null) {//Artist
							row.createCell((int) column).setCellValue(objArr[4].toString());
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
						if(objArr[5]!= null) {//venue
							row.createCell((int) column).setCellValue(objArr[5].toString());
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
						if(objArr[6]!= null) {//City
							row.createCell((int) column).setCellValue(objArr[6].toString());
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
						if(objArr[7]!= null) {//state
							row.createCell((int) column).setCellValue(objArr[7].toString());
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
						if(objArr[8]!= null) {//Country
							row.createCell((int) column).setCellValue(objArr[8].toString());
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
						if(objArr[9]!= null) {//zone
							row.createCell((int) column).setCellValue(objArr[9].toString());
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
						
						
				        rowvariable++;
					}
			    }
			    
			workbook.write(out);
		 }catch (Exception e){
			e.printStackTrace();
		}		
		//return "page-map-not-having-zones-colors-report";
	}
	
	@RequestMapping({"/DownloadMissingSVGandCSVZones"})
	public void downloadMissingSVGandCSVZones(HttpServletRequest request, HttpServletResponse response) throws IOException{
		
//		if(!StringUtils.isEmpty(startDate) && !StringUtils.isEmpty(endDate)){
			SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy"); 
			DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
			String today = dateFormat.format(new Date());
			ServletOutputStream out = response.getOutputStream();
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-disposition",
				"attachment; filename=SVG_and_CSV_Missing_Zones." + today
						+ ".xlsx");
			
			try{
				List<SVGFileMissingZoneData> svgZonesnotinCsvList = TMATDAORegistry.getQueryManagerDAO().getAllVenueMapZonesNotInCSV();
				List<SVGFileMissingZoneData> csvZonesnotinSvgList = TMATDAORegistry.getQueryManagerDAO().getAllCsvZonesNotInVenueMap();
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				generateMissingSVGandCSVzonesFile(workbook.createSheet("SVG Zones not in CSV"),svgZonesnotinCsvList,true);
				generateMissingSVGandCSVzonesFile(workbook.createSheet("CSV Zones not in SVG"),csvZonesnotinSvgList,true);
				//generateMissingSVGandCSVzonesFile(workbook.createSheet("SVG Files with Invalid Format Data"),invalidFormatSVGFiles,false);
				 
				workbook.write(out);
			 }catch (Exception e){
				 e.printStackTrace();
			 }
//		}else{
//			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
//			Calendar cal = Calendar.getInstance(); 
//			map.addAttribute("startDate", dateFormat.format(cal.getTime()));
//			map.addAttribute("endDate", dateFormat.format(cal.getTime()));
//		}
//		return "page-map-not-having-zones-colors-report";
	}
	
	public void generateMissingSVGandCSVzonesFile(Sheet ssSheet,List<SVGFileMissingZoneData> zoneDataList,boolean showZoneColumn) throws Exception {
		
		try {
			int j = 0;
			Row rowhead =   ssSheet.createRow((int)0);
		    rowhead.createCell((int) j).setCellValue("Venue Id");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Building");
		    j++;
	    	rowhead.createCell((int) j).setCellValue("City");
	    	j++;
	    	rowhead.createCell((int) j).setCellValue("State");
	 	    j++;
	 	    rowhead.createCell((int) j).setCellValue("Country");
	    	j++;
	    	rowhead.createCell((int) j).setCellValue("Venue Category Id");
	    	j++;
		    rowhead.createCell((int) j).setCellValue("Venue Category Group");
		    j++;
		    if(showZoneColumn) {
		    	rowhead.createCell((int) j).setCellValue("Zone");
		    	j++;
		    }
		    
		    if(zoneDataList != null) {
		    	 int rowvariable = 1;
				 for (SVGFileMissingZoneData zoneData : zoneDataList) {
					 	 
					int column = 0;
					Row row = ssSheet.createRow(rowvariable);
					row.createCell((int) column).setCellValue(zoneData.getVenueId().toString());
					column++;
					row.createCell((int) column).setCellValue(zoneData.getBuilding().toString());
					column++;
					
					if(zoneData.getCity() != null) {
						row.createCell((int) column).setCellValue(zoneData.getCity().toString());
				        column++;
					} else {
						row.createCell((int) column).setCellValue("");
				        column++;
					}
					if(zoneData.getState() != null) {
						row.createCell((int) column).setCellValue(zoneData.getState().toString());
				        column++;
					} else {
						row.createCell((int) column).setCellValue("");
				        column++;
					}
					if(zoneData.getCountry() != null) {
						row.createCell((int) column).setCellValue(zoneData.getCountry().toString());
				        column++;
					} else {
						row.createCell((int) column).setCellValue("");
				        column++;
					}
					if(zoneData.getVenueCategoryId() != null) {
						row.createCell((int) column).setCellValue(zoneData.getVenueCategoryId().toString());
				        column++;
					} else {
						row.createCell((int) column).setCellValue("");
				        column++;
					}
					if(zoneData.getCategoryGroupName() != null) {
						row.createCell((int) column).setCellValue(zoneData.getCategoryGroupName().toString());
				        column++;
					} else {
						row.createCell((int) column).setCellValue("");
				        column++;
					}
					if(showZoneColumn) {
						if(zoneData.getZone() != null) {
							row.createCell((int) column).setCellValue(zoneData.getZone().toString());
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
					}
			        rowvariable++;
				}
		    }
		    
		} catch( Exception e) {
			e.printStackTrace();
		}			 
	}
	
	@RequestMapping({"/DownloadRTFNext15DaysUnsentReport"})
	public void downloadRTFNext15DaysUnsentReport(HttpServletRequest request, HttpServletResponse response){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Event Id");
			headerList.add("Event Name");
			headerList.add("Event Date");
			headerList.add("Event Time");
			headerList.add("Venue Name");
			headerList.add("Venue City");
			headerList.add("Venue State");
			headerList.add("Venue Country");
			headerList.add("Section");
			headerList.add("Row");
			headerList.add("Qty");
			headerList.add("Invoice Id");
			headerList.add("Invoice Created By");
			headerList.add("Invoice CreatedDate");
			headerList.add("PO Id");
			headerList.add("PO CreatedDate");
			headerList.add("Shipping Type");
			headerList.add("Vendor Company");
			headerList.add("Vendor Email");
			headerList.add("Customer Name");
			headerList.add("Customer Email");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet();
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			//ExcelUtil.generateExcelData(DAORegistry.getEventDAO().getNext15DaysUnsentOrders(), ssSheet);
			List<Object[]> dataList = DAORegistry.getEventDAO().getNext15DaysUnsentOrders();
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			Double sumQuantity = 0.0, quantity = 0.0;
			int j = 0;
			int rowCount = 1;
			
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[1].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
								
				if(dataObj[2] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[2].toString()));
					cell.setCellStyle(cellStyle);					
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				

				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
								
				if(dataObj[10] != null){
					quantity = Util.getRoundedValue(Double.parseDouble(dataObj[10].toString()));
					sumQuantity = sumQuantity + quantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(quantity));
				}else{
					sumQuantity = sumQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
						
				if(dataObj[13] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[13].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
					
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[14].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[15].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[16].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[17] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[17].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[18] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[18].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[19] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[19].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[20] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[20].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataList != null && dataList.size() > 0){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;

				rowhead.createCell((int) j).setCellValue("");
				j++;
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_NEXT_15_DAYS_UNSENT_ORDERS+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping({"/DownloadRTFUnFilledShortsWithPossibleLongInventoryReport"})
	public void downloadRTFUnFilledShortsWithPossibleLongInventoryReport(HttpServletRequest request, HttpServletResponse response){
		try{
			List<String> headerList = new ArrayList<String>();
			//headerList.add("Event Id");			
			headerList.add("Event Name");
			headerList.add("Event Date");
			headerList.add("Event Time");
			headerList.add("Artist Name");
			headerList.add("Venue Name");
			headerList.add("");
			headerList.add("Short Invoice Id");
			headerList.add("Short Section");
			headerList.add("Short Row");
			headerList.add("Short Qty");
			headerList.add("Short Sold Price");
			headerList.add("");
			headerList.add("Long PO Id");
			headerList.add("Long Section");
			headerList.add("Long Row");
			headerList.add("Long Qty");
			headerList.add("Long Retail Price");
			headerList.add("Long Wholesale Price");
			headerList.add("Long Face Price");
			headerList.add("Long Cost");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet();
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<Object[]> list = DAORegistry.getEventDAO().getUnFilledShortsWithPossibleLongInventory();
			List<Object[]> finalList = new ArrayList<Object[]>();
			
			Integer eventId = new Integer(0);
			Integer categoryTicketGroupId = new Integer(0);
			Integer ticketGroupId = new Integer(0);
			Object[] finalObj = null;
			for(Object[] obj : list){			
				finalObj = new Object[20];
				if(!eventId.equals((Integer)obj[0])){
					finalObj[0] = obj[1];
					finalObj[1] = obj[2];
					finalObj[2] = obj[3];
					finalObj[3] = obj[4];
					finalObj[4] = obj[5];
					finalObj[5] = "";
				}else{
					finalObj[0] = "";
					finalObj[1] = "";
					finalObj[2] = "";
					finalObj[3] = "";
					finalObj[4] = "";
					finalObj[5] = "";
				}
				if(!categoryTicketGroupId.equals((Integer)obj[6])){
					finalObj[6] = obj[7];
					finalObj[7] = obj[8];
					finalObj[8] = obj[9];
					finalObj[9] = obj[10];
					finalObj[10] = obj[11];
					finalObj[11] = "";
				}else{
					finalObj[6] = "";
					finalObj[7] = "";
					finalObj[8] = "";
					finalObj[9] = "";
					finalObj[10] = "";
					finalObj[11] = "";
				}
				if(!ticketGroupId.equals((Integer)obj[12])){
					finalObj[12] = obj[13];
					finalObj[13] = obj[14];
					finalObj[14] = obj[15];
					finalObj[15] = obj[16];
					finalObj[16] = obj[17];
					finalObj[17] = obj[18];
					finalObj[18] = obj[19];
					finalObj[19] = obj[20];
				}else{					
					finalObj[12] = "";
					finalObj[13] = "";
					finalObj[14] = "";
					finalObj[15] = "";
					finalObj[16] = "";
					finalObj[17] = "";
					finalObj[18] = "";
					finalObj[19] = "";
				}
				eventId = (Integer) obj[0];
				categoryTicketGroupId = (Integer) obj[6];
				ticketGroupId = (Integer) obj[12];
				finalList.add(finalObj);
			}			
			
			//ExcelUtil.generateExcelData(finalList, ssSheet);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			//CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			//cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			Double sumShortQty = 0.0, sumShortSoldPrice = 0.0, sumLongQty = 0.0, sumLongRetailPrice = 0.0;
			Double sumLongWholesalePrice = 0.0, sumLongFacePrice = 0.0, sumLongCost = 0.0;
			Double shortQty = 0.0, shortSoldPrice = 0.0, longQty = 0.0, longRetailPrice = 0.0;
			Double longWholesalePrice = 0.0, longFacePrice =0.0, longCost = 0.0;
			int j = 0;
			int rowCount = 1;
			
			for(Object[] dataObj : finalList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null && !dataObj[1].toString().isEmpty()){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[1].toString()));
					cell.setCellStyle(cellStyle);					
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());					
				}else{					
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null && !dataObj[9].toString().isEmpty()){
					shortQty = Util.getRoundedValue(Double.parseDouble(dataObj[9].toString()));
					sumShortQty = sumShortQty + shortQty;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(shortQty));
				}else{
					sumShortQty = sumShortQty + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
								
				if(dataObj[10] != null && !dataObj[10].toString().isEmpty()){
					shortSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[10].toString()));
					sumShortSoldPrice = sumShortSoldPrice + shortSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(shortSoldPrice));
				}else{
					sumShortSoldPrice = sumShortSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[13].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[14].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null && !dataObj[15].toString().isEmpty()){
					longQty = Util.getRoundedValue(Double.parseDouble(dataObj[15].toString()));
					sumLongQty = sumLongQty + longQty;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(longQty));
				}else{
					sumLongQty = sumLongQty + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null && !dataObj[16].toString().isEmpty()){
					longRetailPrice = Util.getRoundedValue(Double.parseDouble(dataObj[16].toString()));
					sumLongRetailPrice = sumLongRetailPrice + longRetailPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(longRetailPrice));
				}else{
					sumLongRetailPrice = sumLongRetailPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[17] != null && !dataObj[17].toString().isEmpty()){
					longWholesalePrice = Util.getRoundedValue(Double.parseDouble(dataObj[17].toString()));
					sumLongWholesalePrice = sumLongWholesalePrice + longWholesalePrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(longWholesalePrice));
				}else{
					sumLongWholesalePrice = sumLongWholesalePrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[18] != null && !dataObj[18].toString().isEmpty()){
					longFacePrice = Util.getRoundedValue(Double.parseDouble(dataObj[18].toString()));
					sumLongFacePrice = sumLongFacePrice + longFacePrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(longFacePrice));
				}else{
					sumLongFacePrice = sumLongFacePrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[19] != null && !dataObj[19].toString().isEmpty()){
					longCost = Util.getRoundedValue(Double.parseDouble(dataObj[19].toString()));
					sumLongCost = sumLongCost + longCost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(longCost));
				}else{
					sumLongCost = sumLongCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(finalList != null && finalList.size() > 0){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumShortQty));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumShortSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumLongQty));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumLongRetailPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumLongWholesalePrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumLongFacePrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumLongCost));
				j++;
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_UNFILLED_SHORTS_WITH_POSSIBLE_LONG_INVENTORY+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@RequestMapping({"/EventsStatisticsReport"})
	public EventStatisticsReportDTO downloadEventsStatisticsReport(HttpServletRequest request, HttpServletResponse response){
		EventStatisticsReportDTO eventStatisticsReportDTO = new EventStatisticsReportDTO();
		Error error = new Error();
		
		try{
			List<String> eventsStatisticsReportHeader = new ArrayList<String>();
			eventsStatisticsReportHeader.add("SPORTS");
			eventsStatisticsReportHeader.add("CONCERTS");
			eventsStatisticsReportHeader.add("THEATER");
			eventsStatisticsReportHeader.add("OTHER");
			eventsStatisticsReportHeader.add("TOTAL");
			
			List<Object[]> eventsStatisticsReportTemp = DAORegistry.getEventDAO().getEventsStatistics();
			List<Integer> eventsStatisticsReport = new ArrayList<Integer>();
			int totalCounts = 0;
			for(Object[] eventCounts : eventsStatisticsReportTemp){
				if(eventCounts[1] != null){
					eventsStatisticsReport.add(Integer.parseInt(String.valueOf(eventCounts[0])));
					totalCounts += Integer.parseInt(String.valueOf(eventCounts[0])); 
				}
			}
			eventsStatisticsReport.add(totalCounts);
			
			eventStatisticsReportDTO.setStatus(1);
			eventStatisticsReportDTO.setEventsStatisticsHeaderDTO(eventsStatisticsReportHeader);
			eventStatisticsReportDTO.setEventsStatisticsDTO(eventsStatisticsReport);
		}catch(Exception e){
			e.printStackTrace();
		}
		return eventStatisticsReportDTO;
	}

//	@RequestMapping({"/UnsoldTickets"})
//	public String unsoldTickets(HttpServletRequest request,Model model){
//		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
//		Calendar cal = Calendar.getInstance(); 
//		model.addAttribute("startDate", dateFormat.format(cal.getTime()));
//		cal.add(Calendar.MONTH, 1);
//		model.addAttribute("endDate", dateFormat.format(cal.getTime()));
//		return "page-unsold-tickets";	
//	}
	
	@RequestMapping({"/DownloadRTFUnsoldInventoryReport"})
	public void downloadRTFUnsoldInventoryReport(HttpServletRequest request, HttpServletResponse response,
												@RequestParam(name = "startDate",required = false) String startDate,
												@RequestParam(name = "endDate",required = false) String endDate){		
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("PO Id");
			headerList.add("PO Created Date");
			headerList.add("Event Id");
			headerList.add("Event Name");
			headerList.add("Event Date");
			headerList.add("Event Time");
			headerList.add("Venue Name");
			headerList.add("Venue City");
			headerList.add("Venue State");
			headerList.add("Venue Country");			
			headerList.add("Section");
			headerList.add("Row");
			headerList.add("Seat");			
			headerList.add("Original Ticket Count");
			headerList.add("Remaining Ticket Count");
			headerList.add("Retail Price");
			headerList.add("Wholesale Price");
			headerList.add("Face Price");
			headerList.add("Cost");
			headerList.add("Total Cost");
			headerList.add("Internal Notes");
			headerList.add("Shipping Method");
			headerList.add("Client Broker Company Name");
			headerList.add("Client Broker Name");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet();
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			//ExcelUtil.generateExcelData(DAORegistry.getEventDAO().getUnsoldInventoryReport(startDate,endDate), ssSheet);
			List<Object[]> dataList = DAORegistry.getEventDAO().getUnsoldInventoryReport(startDate,endDate);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			Double sumOriginalTicketCnt = 0.0, sumRemainingTicketCnt = 0.0, sumRetailPrice = 0.0, sumWholesalePrice = 0.0;
			Double sumFacePrice = 0.0, sumCost = 0.0, sumTotalCost = 0.0;
			Double originalTicketCnt = 0.0, remainingTicketCnt = 0.0, retailPrice = 0.0, wholesalePrice = 0.0;
			Double facePrice = 0.0, cost = 0.0, totalCost = 0.0;
			int j = 0;
			int rowCount = 1;
			
			for(Object[] dataObj : dataList){				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[1].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[4] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[4].toString()));
					cell.setCellStyle(cellStyle);					
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					originalTicketCnt = Util.getRoundedValue(Double.parseDouble(dataObj[13].toString()));
					sumOriginalTicketCnt = sumOriginalTicketCnt + originalTicketCnt;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(originalTicketCnt));
				}else{
					sumOriginalTicketCnt = sumOriginalTicketCnt + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
								
				if(dataObj[14] != null){
					remainingTicketCnt = Util.getRoundedValue(Double.parseDouble(dataObj[14].toString()));
					sumRemainingTicketCnt = sumRemainingTicketCnt + remainingTicketCnt;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(remainingTicketCnt));
				}else{
					sumRemainingTicketCnt = sumRemainingTicketCnt + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					retailPrice = Util.getRoundedValue(Double.parseDouble(dataObj[15].toString()));
					sumRetailPrice = sumRetailPrice + retailPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(retailPrice));
				}else{
					sumRetailPrice = sumRetailPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					wholesalePrice = Util.getRoundedValue(Double.parseDouble(dataObj[16].toString()));
					sumWholesalePrice = sumWholesalePrice + wholesalePrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(wholesalePrice));
				}else{
					sumWholesalePrice = sumWholesalePrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[17] != null){
					facePrice = Util.getRoundedValue(Double.parseDouble(dataObj[17].toString()));
					sumFacePrice = sumFacePrice + facePrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(facePrice));
				}else{
					sumFacePrice = sumFacePrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
						
				if(dataObj[18] != null){
					cost = Util.getRoundedValue(Double.parseDouble(dataObj[18].toString()));
					sumCost = sumCost + cost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(cost));
				}else{
					sumCost = sumCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[19] != null){
					totalCost = Util.getRoundedValue(Double.parseDouble(dataObj[19].toString()));
					sumTotalCost = sumTotalCost + totalCost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalCost));
				}else{
					sumTotalCost = sumTotalCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[20] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[20].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[21] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[21].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[22] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[22].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[23] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[23].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataList != null && dataList.size() > 0){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumOriginalTicketCnt));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumRemainingTicketCnt));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumRetailPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumWholesalePrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumFacePrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumCost));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalCost));
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_UNSOLD_INVENTORY_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}		
	}
	
	@RequestMapping({"/DownloadRTFUnsoldCategoryTicketsReport"})
	public void downloadRTFUnsoldCategoryTicketsReport(HttpServletRequest request, HttpServletResponse response,
														@RequestParam(name = "startDate",required = false) String startDate,
														@RequestParam(name = "endDate",required = false) String endDate){		
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Event Id");
			headerList.add("Event Name");
			headerList.add("Event Date");
			headerList.add("Event Time");
			headerList.add("Venue Name");
			headerList.add("Venue City");
			headerList.add("Venue State");
			headerList.add("Venue Country");
			headerList.add("Section");
			headerList.add("Row");
			headerList.add("Qty");
			headerList.add("Retail Price");
			headerList.add("Wholesale Price");
			headerList.add("Total Retail Price");
			headerList.add("Total Wholesale Price");
			headerList.add("broadcast");

			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet();
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			//ExcelUtil.generateExcelData(DAORegistry.getEventDAO().getUnsoldCategoryTicketsReport(startDate,endDate), ssSheet);
			List<Object[]> dataList = DAORegistry.getEventDAO().getUnsoldCategoryTicketsReport(startDate,endDate);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));				
			//CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			//cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			Double sumQuantity = 0.0, sumRetailPrice = 0.0, sumWholesalePrice = 0.0, sumTotalRetailPrice = 0.0;
			Double sumTotalWholesalePrice = 0.0;
			Double quantity = 0.0, retailPrice = 0.0, wholesalePrice = 0.0, totalRetailPrice = 0.0;
			Double totalWholesalePrice = 0.0;
			int j = 0;
			int rowCount = 1;
			
			for(Object[] dataObj : dataList){				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[1].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[2] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[2].toString()));
					cell.setCellStyle(cellStyle);					
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					quantity = Util.getRoundedValue(Double.parseDouble(dataObj[10].toString()));
					sumQuantity = sumQuantity + quantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(quantity));
				}else{
					sumQuantity = sumQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
								
				if(dataObj[11] != null){
					retailPrice = Util.getRoundedValue(Double.parseDouble(dataObj[11].toString()));
					sumRetailPrice = sumRetailPrice + retailPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(retailPrice));
				}else{
					sumRetailPrice = sumRetailPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					wholesalePrice = Util.getRoundedValue(Double.parseDouble(dataObj[12].toString()));
					sumWholesalePrice = sumWholesalePrice + wholesalePrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(wholesalePrice));
				}else{
					sumWholesalePrice = sumWholesalePrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					totalRetailPrice = Util.getRoundedValue(Double.parseDouble(dataObj[13].toString()));
					sumTotalRetailPrice = sumTotalRetailPrice + totalRetailPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalRetailPrice));
				}else{
					sumTotalRetailPrice = sumTotalRetailPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
						
				if(dataObj[14] != null){
					totalWholesalePrice = Util.getRoundedValue(Double.parseDouble(dataObj[14].toString()));
					sumTotalWholesalePrice = sumTotalWholesalePrice + totalWholesalePrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalWholesalePrice));
				}else{
					sumTotalWholesalePrice = sumTotalWholesalePrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					Boolean broadCast = Boolean.parseBoolean(dataObj[15].toString());
					if(broadCast != null && broadCast){
						rowhead.createCell((int) j).setCellValue("Yes");
					}else{
						rowhead.createCell((int) j).setCellValue("No");
					}
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataList != null && dataList.size() > 0){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumRetailPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumWholesalePrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalRetailPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalWholesalePrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_UNSOLD_CATEGORY_TICKET_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}		
	}
		
//	@RequestMapping({"/PoInvoiceReport"})
//	public String poInvoiceReport(HttpServletRequest request,Model model){	
//		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
//		Calendar cal = Calendar.getInstance(); 
//		model.addAttribute("endDate", dateFormat.format(cal.getTime()));
//		cal.add(Calendar.MONTH, -3);
//		model.addAttribute("startDate", dateFormat.format(cal.getTime()));
//		return "page-po-invoice-report";		
//	}
	
	@RequestMapping({"/DownloadRTFInvoiceReport"})
	public void downloadRTFInvoiceReport(HttpServletRequest request, HttpServletResponse response,
										@RequestParam(name = "startDate",required = false) String startDate,
										@RequestParam(name = "endDate",required = false) String endDate){		
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Invoice Id");
			headerList.add("Created Date");
			headerList.add("Created By");
			headerList.add("Updated Date");
			headerList.add("Updated By");
			headerList.add("Invoice Type");
			headerList.add("Delivery Method");
			headerList.add("Status");
			headerList.add("Notes");
			headerList.add("Customer Name");
			headerList.add("Order Id");
			headerList.add("Invoice Total");
			headerList.add("Qty");
			headerList.add("Real Tix Mapped");
			headerList.add("Tix Uploaded");
			headerList.add("Tix Delivered");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet();
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			//ExcelUtil.generateExcelData(DAORegistry.getEventDAO().getInvoiceReport(startDate,endDate), ssSheet);
			List<Object[]> dataList = DAORegistry.getEventDAO().getInvoiceReport(startDate,endDate);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			//CellStyle cellStyle = workbook.createCellStyle();
			//cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			Double sumInvoiceTotal = 0.0, sumQuantity = 0.0;			
			Double invoiceTotal = 0.0, quantity = 0.0;
			int j = 0;
			int rowCount = 1;
			
			for(Object[] dataObj : dataList){				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[1].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);					
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());					
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[3] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[3].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());					
				}else{					
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
								
				if(dataObj[11] != null){
					invoiceTotal = Util.getRoundedValue(Double.parseDouble(dataObj[11].toString()));
					sumInvoiceTotal = sumInvoiceTotal + invoiceTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTotal));
				}else{
					sumInvoiceTotal = sumInvoiceTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					quantity = Util.getRoundedValue(Double.parseDouble(dataObj[12].toString()));
					sumQuantity = sumQuantity + quantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(quantity));
				}else{
					sumQuantity = sumQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[13].toString());
				}else{					
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
						
				if(dataObj[14] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[14].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[15].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataList != null && dataList.size() > 0){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_INVOICE_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	@RequestMapping({"/DownloadRTFPurchaseOrderReport"})
	public void downloadRTFPurchaseOrderReport(HttpServletRequest request, HttpServletResponse response,
												@RequestParam(name = "startDate",required = false) String startDate,
												@RequestParam(name = "endDate",required = false) String endDate){		
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("PO Id");
			headerList.add("Created Date");
			headerList.add("Created By");
			headerList.add("PO Type");
			headerList.add("Delivery Method");
			headerList.add("Notes");
			headerList.add("Customer Name");
			headerList.add("PO Total");
			headerList.add("Qty");
			headerList.add("External PO No");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet();
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			//ExcelUtil.generateExcelData(DAORegistry.getEventDAO().getPurchaseOrderReport(startDate,endDate), ssSheet);
			List<Object[]> dataList = DAORegistry.getEventDAO().getPurchaseOrderReport(startDate,endDate);
				
			CreationHelper createHelper = workbook.getCreationHelper();
			//CellStyle cellStyle = workbook.createCellStyle();
			//cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			Double sumPOTotal = 0.0, sumQuantity = 0.0;			
			Double poTotal = 0.0, quantity = 0.0;
			int j = 0;
			int rowCount = 1;
			
			for(Object[] dataObj : dataList){				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[1].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);					
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());					
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					poTotal = Util.getRoundedValue(Double.parseDouble(dataObj[7].toString()));
					sumPOTotal = sumPOTotal + poTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(poTotal));
				}else{
					sumPOTotal = sumPOTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					quantity = Util.getRoundedValue(Double.parseDouble(dataObj[8].toString()));
					sumQuantity = sumQuantity + quantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(quantity));
				}else{
					sumQuantity = sumQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataList != null && dataList.size() > 0){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumPOTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_PURCHASE_ORDER_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}		
	}
	
	@RequestMapping({"/DownloadRTFSalesReport"})
	public void downloadRTFSalesReport(HttpServletRequest request, HttpServletResponse response){
		try{
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			//RTF SALES
			List<String> headerList = new ArrayList<String>();
			headerList.add("Invoice No");
			headerList.add("Invoice Created Date");
			headerList.add("Invoice Status");
			headerList.add("Order No");
			headerList.add("Order Created Date");
			headerList.add("PO No.");
			headerList.add("PO Created Date");
			headerList.add("Event Name");
			headerList.add("Event Date");
			headerList.add("Event Time");
			headerList.add("Venue Name");
			headerList.add("Venue City");
			headerList.add("Zone");
			headerList.add("Quantity");
			headerList.add("Section");
			headerList.add("Row");
			headerList.add("Seat");
			headerList.add("Customer Name");
			headerList.add("Customer Email");
			headerList.add("Listing Price");
			headerList.add("Promocode Discount Amt");
			headerList.add("Loyalfan Discount Amt");
			headerList.add("Order Total");
			headerList.add("Cost");
			headerList.add("Net Total");
			headerList.add("Profit And Loss");
			headerList.add("Order Type");
			headerList.add("Platform");
			headerList.add("Payment Methods");
			headerList.add("Promotional Code Used");
			headerList.add("Promotional Code");
			headerList.add("Promotional Discount");
			headerList.add("LoyalFan Discount");
			headerList.add("Referral Code");
			headerList.add("Referrer Customer Email");
			headerList.add("Order Gap");
			headerList.add("Total Commison");
						
			Sheet ssSheet =  workbook.createSheet("RTFSalesOrders");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);			
			List<Object[]> dataList = DAORegistry.getEventDAO().getRTFSalesReport();
			//ExcelUtil.generateExcelData(DAORegistry.getEventDAO().getRTFSalesReportNotInTickTracker(), ssSheet);
			
			List<Integer> invoiceIds = new ArrayList<Integer>();
			Double sumOrderTotal = 0.0,sumNetTotal = 0.0,sumCost = 0.0,sumProfitAndLoss = 0.0,sumTotDiscountAmt=0.0;
			Double orderTotal = 0.0,netTotal = 0.0,cost = 0.0,profitAndLoss = 0.0;
			int j = 0;
			int rowCount = 1;
			boolean flag=false;
			for(Object[] dataObj : dataList){
				if(invoiceIds.contains((Integer)dataObj[0])){
					String poIds = "";
					Double poCost = 0.00;
					poIds = ssSheet.getRow(rowCount-1).getCell(5).getStringCellValue();
					poIds += ","+dataObj[5].toString();
					ssSheet.getRow(rowCount-1).getCell(5).setCellValue(poIds);
					
					poCost = ssSheet.getRow(rowCount-1).getCell(23).getNumericCellValue();
					cost = Util.getRoundedValue(Double.parseDouble(dataObj[23].toString()));
					poCost = poCost + cost;
					ssSheet.getRow(rowCount-1).getCell(23).setCellValue(poCost);
					flag = true;
					continue;
				}
				if(flag){
					Double c =  ssSheet.getRow(rowCount-1).getCell(23).getNumericCellValue();
					Double n =  ssSheet.getRow(rowCount-1).getCell(24).getNumericCellValue();
					ssSheet.getRow(rowCount-1).getCell(25).setCellValue(n-c);
				}
				flag = false;
				Row rowhead = ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				invoiceIds.add((Integer)dataObj[0]);
				j++;
				
				if(dataObj[1] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[1].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[4].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[6].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[8].toString()));
					cell.setCellStyle(cellStyle);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[13].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[14].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[15].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[16].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[17] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[17].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[18] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[18].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				Double doubleVariable = 0.00;
				if(dataObj[19] != null){
					doubleVariable = Util.getRoundedValue(Double.parseDouble(dataObj[19].toString()));
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(doubleVariable));
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				doubleVariable = 0.00;
				if(dataObj[20] != null){
					doubleVariable = Util.getRoundedValue(Double.parseDouble(dataObj[20].toString()));
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(doubleVariable));
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				doubleVariable = 0.00;
				if(dataObj[21] != null){
					doubleVariable = Util.getRoundedValue(Double.parseDouble(dataObj[21].toString()));
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(doubleVariable));
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[22] != null){
					orderTotal = Util.getRoundedValue(Double.parseDouble(dataObj[22].toString()));
					sumOrderTotal = sumOrderTotal + orderTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(orderTotal));
				}else{
					sumOrderTotal = sumOrderTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[23] != null){
					cost = Util.getRoundedValue(Double.parseDouble(dataObj[23].toString()));
					sumCost = sumCost + cost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(cost));
				}else{
					sumCost = sumCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[24] != null){
					netTotal = Util.getRoundedValue(Double.parseDouble(dataObj[24].toString()));
					sumNetTotal = sumNetTotal + netTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(netTotal));
				}else{
					sumNetTotal = sumNetTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[25] != null){
					profitAndLoss = Util.getRoundedValue(Double.parseDouble(dataObj[25].toString()));
					sumProfitAndLoss = sumProfitAndLoss + profitAndLoss;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(profitAndLoss));
				}else{
					sumProfitAndLoss = sumProfitAndLoss + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				
				if(dataObj[26] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[26].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}	
				j++;
				
				if(dataObj[27] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[27].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[28] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[28].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[29] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[29].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[30] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[30].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[31] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[31].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[32] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[32].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[33] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[33].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[34] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[34].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				if(dataObj[35] != null){
					double totDisAmt = Util.getRoundedValue(Double.parseDouble(dataObj[35].toString()));
					sumTotDiscountAmt = sumTotDiscountAmt + totDisAmt;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totDisAmt));
				}else{
					sumTotDiscountAmt = sumTotDiscountAmt + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				if(dataObj[36] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[36].toString());
				}else{
					sumProfitAndLoss = sumProfitAndLoss + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataList != null && dataList.size() > 0){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumOrderTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumCost));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumNetTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumProfitAndLoss));
				j++;
				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				rowhead.createCell((int) j).setCellValue("");
				j++;
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotDiscountAmt));
				j++;
				rowhead.createCell((int) j).setCellValue("");
				j++;
			}
			
			//Filled Report By Invoice
			List<String> headerFilledInvoice = new ArrayList<String>();
			headerFilledInvoice.add("Invoice Id");
			headerFilledInvoice.add("Invoice Created Date");
			headerFilledInvoice.add("Customer Order Id");
			headerFilledInvoice.add("Customer Order Created Date");
			headerFilledInvoice.add("PO Id");
			headerFilledInvoice.add("PO Created Date");			
			headerFilledInvoice.add("Event Category");
			headerFilledInvoice.add("Event Name");
			headerFilledInvoice.add("Event Date");
			headerFilledInvoice.add("Event Time");
			headerFilledInvoice.add("Venue Name");
			headerFilledInvoice.add("Section Range");		
			headerFilledInvoice.add("Invoice Ticket Qty");
			headerFilledInvoice.add("Sold Ticket Qty");					
			headerFilledInvoice.add("Invoice Total");			
			headerFilledInvoice.add("Ticket Price");
			headerFilledInvoice.add("Sold Price");			
			headerFilledInvoice.add("Actual Sold Price");
			headerFilledInvoice.add("Ticket Cost");
			headerFilledInvoice.add("Total Ticket Cost");			
			headerFilledInvoice.add("Total Sold Price");
			headerFilledInvoice.add("Fees");
			headerFilledInvoice.add("Net Sold Price");
			headerFilledInvoice.add("ActualProfitOrLoss");
			headerFilledInvoice.add("Gross Margin");			
			headerFilledInvoice.add("Client Name");
			headerFilledInvoice.add("Client Email");
			headerFilledInvoice.add("Client Phone");
			headerFilledInvoice.add("Client BillingAddress");
			headerFilledInvoice.add("Client ShippingAddress");
			
			Sheet ssSheet1 =  workbook.createSheet("FilledReportByInvoice");
			ExcelUtil.generateExcelHeaderRow(headerFilledInvoice, ssSheet1);			
			List<Object[]> dataFilledByInvoice = DAORegistry.getEventDAO().getRTFFilledReportByInvoice();
			
			Double sumInvoiceTktQuantity = 0.0, sumSoldTktQuantity = 0.0, sumInvoiceTotal = 0.0, sumTicketPrice = 0.0;
			Double sumSoldPrice = 0.0, sumActualSoldPrice = 0.0, sumTicketCost = 0.0, sumTotalTicketCost = 0.0;
			Double sumTotalSoldPrice = 0.0, sumFees = 0.0, sumNetSoldPrice =0.0, sumProfitOrLoss = 0.0, sumGrossMargin = 0.0;
			Double invoiceTktQuantity = 0.0, soldTktQuantity = 0.0, invoiceTotal = 0.0, ticketPrice = 0.0;			
			Double soldPrice = 0.0, actualSoldPrice = 0.0, ticketCost = 0.0, totalTicketCost = 0.0;
			Double totalSoldPrice = 0.0, fees = 0.0, netSoldPrice =0.0, profitOrLoss = 0.0, grossMargin = 0.0;
			j = 0;
			rowCount = 1;
			
			for(Object[] dataObj : dataFilledByInvoice){
				
				Row rowhead =   ssSheet1.createRow((int)rowCount);
				
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[1].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[3].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[5].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[8].toString()));
					cell.setCellStyle(cellStyle);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
								
				if(dataObj[12] != null){
					invoiceTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[12].toString()));
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + invoiceTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTktQuantity));
				}else{
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					soldTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[13].toString()));
					sumSoldTktQuantity = sumSoldTktQuantity + soldTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(soldTktQuantity));
				}else{
					sumSoldTktQuantity = sumSoldTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					invoiceTotal = Util.getRoundedValue(Double.parseDouble(dataObj[14].toString()));
					sumInvoiceTotal = sumInvoiceTotal + invoiceTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTotal));
				}else{
					sumInvoiceTotal = sumInvoiceTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					ticketPrice = Util.getRoundedValue(Double.parseDouble(dataObj[15].toString()));
					sumTicketPrice = sumTicketPrice + ticketPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(ticketPrice));
				}else{
					sumTicketPrice = sumTicketPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					soldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[16].toString()));
					sumSoldPrice = sumSoldPrice + soldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(soldPrice));
				}else{
					sumSoldPrice = sumSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[17] != null){
					actualSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[17].toString()));
					sumActualSoldPrice = sumActualSoldPrice + actualSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(actualSoldPrice));
				}else{
					sumActualSoldPrice = sumActualSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[18] != null){
					ticketCost = Util.getRoundedValue(Double.parseDouble(dataObj[18].toString()));
					sumTicketCost = sumTicketCost + ticketCost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(ticketCost));
				}else{
					sumTicketCost = sumTicketCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[19] != null){
					totalTicketCost = Util.getRoundedValue(Double.parseDouble(dataObj[19].toString()));
					sumTotalTicketCost = sumTotalTicketCost + totalTicketCost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalTicketCost));
				}else{
					sumTotalTicketCost = sumTotalTicketCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[20] != null){
					totalSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[20].toString()));
					sumTotalSoldPrice = sumTotalSoldPrice + totalSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalSoldPrice));
				}else{
					sumTotalSoldPrice = sumTotalSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[21] != null){
					fees = Util.getRoundedValue(Double.parseDouble(dataObj[21].toString()));
					sumFees = sumFees + fees;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(fees));
				}else{
					sumFees = sumFees + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[22] != null){
					netSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[22].toString()));
					sumNetSoldPrice = sumNetSoldPrice + netSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(netSoldPrice));
				}else{
					sumNetSoldPrice = sumNetSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[23] != null){
					profitOrLoss = Util.getRoundedValue(Double.parseDouble(dataObj[23].toString()));
					sumProfitOrLoss = sumProfitOrLoss + profitOrLoss;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(profitOrLoss));
				}else{
					sumProfitOrLoss = sumProfitOrLoss + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[24] != null){
					grossMargin = Util.getRoundedValue(Double.parseDouble(dataObj[24].toString()));
					//sumGrossMargin = sumGrossMargin + grossMargin;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(grossMargin));
				}else{
					//sumGrossMargin = sumGrossMargin + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[25] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[25].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[26] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[26].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[27] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[27].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[28] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[28].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[29] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[29].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataFilledByInvoice != null && dataFilledByInvoice.size() > 0){
				Row rowhead =   ssSheet1.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
								
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSoldTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTicketPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumActualSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTicketCost));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalTicketCost));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumFees));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumNetSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumProfitOrLoss));
				j++;
				
				sumGrossMargin = (sumProfitOrLoss*100)/sumTotalSoldPrice;
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumGrossMargin));
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
			}
			
			//Filled Report By PO
			List<String> headerFilledPO = new ArrayList<String>();
			headerFilledPO.add("PO Id");
			headerFilledPO.add("PO Created Date");
			headerFilledPO.add("Invoice Id");
			headerFilledPO.add("Invoice Created Date");
			headerFilledPO.add("Customer Order Id");
			headerFilledPO.add("Customer Order Created Date");						
			headerFilledPO.add("Event Category");
			headerFilledPO.add("Event Name");
			headerFilledPO.add("Event Date");
			headerFilledPO.add("Event Time");
			headerFilledPO.add("Venue Name");
			headerFilledPO.add("Section Range");
			headerFilledPO.add("Row Range");			
			headerFilledPO.add("Invoice Ticket Qty");
			headerFilledPO.add("Sold Ticket Qty");					
			headerFilledPO.add("Invoice Total");			
			headerFilledPO.add("Ticket Price");
			headerFilledPO.add("Sold Price");			
			headerFilledPO.add("Actual Sold Price");
			headerFilledPO.add("Ticket Cost");
			headerFilledPO.add("Total Ticket Cost");			
			headerFilledPO.add("Total Sold Price");
			headerFilledPO.add("Fees");
			headerFilledPO.add("Net Sold Price");
			headerFilledPO.add("ActualProfitOrLoss");
			headerFilledPO.add("Gross Margin");			
			headerFilledPO.add("Client Name");
			headerFilledPO.add("Client Email");
			headerFilledPO.add("Client Phone");
			headerFilledPO.add("Client BillingAddress");
			headerFilledPO.add("Client ShippingAddress");
			
			Sheet ssSheet3 =  workbook.createSheet("FilledReportByPO");
			ExcelUtil.generateExcelHeaderRow(headerFilledPO, ssSheet3);			
			List<Object[]> dataFilledByPO = DAORegistry.getEventDAO().getRTFFilledReportByPO();
			
			sumInvoiceTktQuantity = 0.0; sumSoldTktQuantity = 0.0; sumInvoiceTotal = 0.0; sumTicketPrice = 0.0;
			sumSoldPrice = 0.0; sumActualSoldPrice = 0.0; sumTicketCost = 0.0; sumTotalTicketCost = 0.0;
			sumTotalSoldPrice = 0.0; sumFees = 0.0; sumNetSoldPrice =0.0; sumProfitOrLoss = 0.0; sumGrossMargin = 0.0;
			invoiceTktQuantity = 0.0; soldTktQuantity = 0.0; invoiceTotal = 0.0; ticketPrice = 0.0;			
			soldPrice = 0.0; actualSoldPrice = 0.0; ticketCost = 0.0; totalTicketCost = 0.0;
			totalSoldPrice = 0.0; fees = 0.0; netSoldPrice =0.0; profitOrLoss = 0.0; grossMargin = 0.0;
			j = 0;
			rowCount = 1;
			
			for(Object[] dataObj : dataFilledByPO){
				
				Row rowhead =   ssSheet3.createRow((int)rowCount);
				
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[1].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[3].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[5].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[8].toString()));
					cell.setCellStyle(cellStyle);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
						
				if(dataObj[12] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					invoiceTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[13].toString()));
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + invoiceTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTktQuantity));
				}else{
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					soldTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[14].toString()));
					sumSoldTktQuantity = sumSoldTktQuantity + soldTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(soldTktQuantity));
				}else{
					sumSoldTktQuantity = sumSoldTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					invoiceTotal = Util.getRoundedValue(Double.parseDouble(dataObj[15].toString()));
					sumInvoiceTotal = sumInvoiceTotal + invoiceTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTotal));
				}else{
					sumInvoiceTotal = sumInvoiceTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					ticketPrice = Util.getRoundedValue(Double.parseDouble(dataObj[16].toString()));
					sumTicketPrice = sumTicketPrice + ticketPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(ticketPrice));
				}else{
					sumTicketPrice = sumTicketPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[17] != null){
					soldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[17].toString()));
					sumSoldPrice = sumSoldPrice + soldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(soldPrice));
				}else{
					sumSoldPrice = sumSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[18] != null){
					actualSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[18].toString()));
					sumActualSoldPrice = sumActualSoldPrice + actualSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(actualSoldPrice));
				}else{
					sumActualSoldPrice = sumActualSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[19] != null){
					ticketCost = Util.getRoundedValue(Double.parseDouble(dataObj[19].toString()));
					sumTicketCost = sumTicketCost + ticketCost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(ticketCost));
				}else{
					sumTicketCost = sumTicketCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[20] != null){
					totalTicketCost = Util.getRoundedValue(Double.parseDouble(dataObj[20].toString()));
					sumTotalTicketCost = sumTotalTicketCost + totalTicketCost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalTicketCost));
				}else{
					sumTotalTicketCost = sumTotalTicketCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[21] != null){
					totalSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[21].toString()));
					sumTotalSoldPrice = sumTotalSoldPrice + totalSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalSoldPrice));
				}else{
					sumTotalSoldPrice = sumTotalSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[22] != null){
					fees = Util.getRoundedValue(Double.parseDouble(dataObj[22].toString()));
					sumFees = sumFees + fees;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(fees));
				}else{
					sumFees = sumFees + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[23] != null){
					netSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[23].toString()));
					sumNetSoldPrice = sumNetSoldPrice + netSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(netSoldPrice));
				}else{
					sumNetSoldPrice = sumNetSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[24] != null){
					profitOrLoss = Util.getRoundedValue(Double.parseDouble(dataObj[24].toString()));
					sumProfitOrLoss = sumProfitOrLoss + profitOrLoss;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(profitOrLoss));
				}else{
					sumProfitOrLoss = sumProfitOrLoss + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[25] != null){
					grossMargin = Util.getRoundedValue(Double.parseDouble(dataObj[25].toString()));
					//sumGrossMargin = sumGrossMargin + grossMargin;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(grossMargin));
				}else{
					//sumGrossMargin = sumGrossMargin + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[26] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[26].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[27] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[27].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[28] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[28].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[29] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[29].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[30] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[30].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataFilledByPO != null && dataFilledByPO.size() > 0){
				Row rowhead =   ssSheet3.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSoldTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTicketPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumActualSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTicketCost));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalTicketCost));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumFees));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumNetSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumProfitOrLoss));
				j++;
				
				sumGrossMargin = (sumProfitOrLoss*100)/sumTotalSoldPrice;
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumGrossMargin));
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
			}
			
			//UnFilled Report By Invoice
			List<String> headerUnFilledInvoice = new ArrayList<String>();
			headerUnFilledInvoice.add("Invoice Id");
			headerUnFilledInvoice.add("Invoice Created Date");		
			headerUnFilledInvoice.add("Event Category");
			headerUnFilledInvoice.add("Event Name");
			headerUnFilledInvoice.add("Event Date");
			headerUnFilledInvoice.add("Event Time");
			headerUnFilledInvoice.add("Venue Name");
			headerUnFilledInvoice.add("Section");
			headerUnFilledInvoice.add("Row");
			headerUnFilledInvoice.add("Section Range");
			headerUnFilledInvoice.add("Row Range");			
			headerUnFilledInvoice.add("Invoice Ticket Qty");
			headerUnFilledInvoice.add("Sold Ticket Qty");					
			headerUnFilledInvoice.add("Invoice Total");			
			headerUnFilledInvoice.add("Ticket Price");
			headerUnFilledInvoice.add("Sold Price");			
			headerUnFilledInvoice.add("Actual Sold Price");			
			headerUnFilledInvoice.add("Total Sold Price");
			headerUnFilledInvoice.add("Fees");
			headerUnFilledInvoice.add("Net Sold Price");	
			headerUnFilledInvoice.add("Client Name");
			headerUnFilledInvoice.add("Client Email");
			headerUnFilledInvoice.add("Client Phone");
			headerUnFilledInvoice.add("Client BillingAddress");
			headerUnFilledInvoice.add("Client ShippingAddress");			
			
			Sheet ssSheet2 =  workbook.createSheet("UnFilledReportByInvoice");
			ExcelUtil.generateExcelHeaderRow(headerUnFilledInvoice, ssSheet2);
			List<Object[]> dataUnFilledInvoice = DAORegistry.getEventDAO().getRTFUnFilledReportByInvoice();
			
			sumInvoiceTktQuantity = 0.0; sumSoldTktQuantity = 0.0; sumInvoiceTotal = 0.0; sumTicketPrice = 0.0;
			sumSoldPrice = 0.0; sumActualSoldPrice = 0.0; sumTicketCost = 0.0; sumTotalTicketCost = 0.0;
			sumTotalSoldPrice = 0.0; sumFees = 0.0; sumNetSoldPrice =0.0; sumProfitOrLoss = 0.0; sumGrossMargin = 0.0;
			invoiceTktQuantity = 0.0; soldTktQuantity = 0.0; invoiceTotal = 0.0; ticketPrice = 0.0;			
			soldPrice = 0.0; actualSoldPrice = 0.0; ticketCost = 0.0; totalTicketCost = 0.0;
			totalSoldPrice = 0.0; fees = 0.0; netSoldPrice =0.0; profitOrLoss = 0.0; grossMargin = 0.0;
			j = 0;
			rowCount = 1;
			
			for(Object[] dataObj : dataUnFilledInvoice){
				
				Row rowhead =   ssSheet2.createRow((int)rowCount);
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[1].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[4].toString()));
					cell.setCellStyle(cellStyle);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
						
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
						
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					invoiceTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[11].toString()));
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + invoiceTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTktQuantity));
				}else{
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					soldTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[12].toString()));
					sumSoldTktQuantity = sumSoldTktQuantity + soldTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(soldTktQuantity));
				}else{
					sumSoldTktQuantity = sumSoldTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					invoiceTotal = Util.getRoundedValue(Double.parseDouble(dataObj[13].toString()));
					sumInvoiceTotal = sumInvoiceTotal + invoiceTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTotal));
				}else{
					sumInvoiceTotal = sumInvoiceTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					ticketPrice = Util.getRoundedValue(Double.parseDouble(dataObj[14].toString()));
					sumTicketPrice = sumTicketPrice + ticketPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(ticketPrice));
				}else{
					sumTicketPrice = sumTicketPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					soldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[15].toString()));
					sumSoldPrice = sumSoldPrice + soldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(soldPrice));
				}else{
					sumSoldPrice = sumSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					actualSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[16].toString()));
					sumActualSoldPrice = sumActualSoldPrice + actualSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(actualSoldPrice));
				}else{
					sumActualSoldPrice = sumActualSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[17] != null){
					totalSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[17].toString()));
					sumTotalSoldPrice = sumTotalSoldPrice + totalSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalSoldPrice));
				}else{
					sumTotalSoldPrice = sumTotalSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[18] != null){
					fees = Util.getRoundedValue(Double.parseDouble(dataObj[18].toString()));
					sumFees = sumFees + fees;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(fees));
				}else{
					sumFees = sumFees + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[19] != null){
					netSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[19].toString()));
					sumNetSoldPrice = sumNetSoldPrice + netSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(netSoldPrice));
				}else{
					sumNetSoldPrice = sumNetSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[20] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[20].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[21] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[21].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[22] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[22].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[23] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[23].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[24] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[24].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataUnFilledInvoice != null && dataUnFilledInvoice.size() > 0){
				Row rowhead =   ssSheet2.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSoldTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTicketPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumActualSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumFees));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumNetSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
			}
			
			//Monthly Filled Report By Invoice
			List<String> hdrMonthlyFilledInvoice = new ArrayList<String>();
			hdrMonthlyFilledInvoice.add("Invoice Year");
			hdrMonthlyFilledInvoice.add("Invoice Month");
			hdrMonthlyFilledInvoice.add("Invoice Id");
			hdrMonthlyFilledInvoice.add("Customer Order Id");						
			hdrMonthlyFilledInvoice.add("PO Id");			
			hdrMonthlyFilledInvoice.add("Invoice Ticket Qty");
			hdrMonthlyFilledInvoice.add("Sold Ticket Qty");					
			hdrMonthlyFilledInvoice.add("Invoice Total");			
			hdrMonthlyFilledInvoice.add("Ticket Price");
			hdrMonthlyFilledInvoice.add("Sold Price");			
			hdrMonthlyFilledInvoice.add("Actual Sold Price");
			hdrMonthlyFilledInvoice.add("Ticket Cost");
			hdrMonthlyFilledInvoice.add("Total Ticket Cost");
			hdrMonthlyFilledInvoice.add("Total Sold Price");
			hdrMonthlyFilledInvoice.add("Net Sold Price");
			hdrMonthlyFilledInvoice.add("ActualProfitOrLoss");
			hdrMonthlyFilledInvoice.add("Gross Margin");
			
			Sheet ssSheet4 =  workbook.createSheet("MonthlyFilledReportByInvoice");
			ExcelUtil.generateExcelHeaderRow(hdrMonthlyFilledInvoice, ssSheet4);			
			List<Object[]> dataMonthlyFilledByInvoice = DAORegistry.getEventDAO().getRTFMonthlyFilledReportByInvoice();
			
			sumInvoiceTktQuantity = 0.0; sumSoldTktQuantity = 0.0; sumInvoiceTotal = 0.0; sumTicketPrice = 0.0;
			sumSoldPrice = 0.0; sumActualSoldPrice = 0.0; sumTicketCost = 0.0; sumTotalTicketCost = 0.0;
			sumTotalSoldPrice = 0.0; sumNetSoldPrice =0.0; sumProfitOrLoss = 0.0; sumGrossMargin = 0.0;
			invoiceTktQuantity = 0.0; soldTktQuantity = 0.0; invoiceTotal = 0.0; ticketPrice = 0.0;			
			soldPrice = 0.0; actualSoldPrice = 0.0; ticketCost = 0.0; totalTicketCost = 0.0;
			totalSoldPrice = 0.0; netSoldPrice =0.0; profitOrLoss = 0.0; grossMargin = 0.0;
			j = 0;
			rowCount = 1;
			
			for(Object[] dataObj : dataMonthlyFilledByInvoice){
				
				Row rowhead =   ssSheet4.createRow((int)rowCount);
				
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[1].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					invoiceTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[5].toString()));
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + invoiceTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTktQuantity));
				}else{
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					soldTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[6].toString()));
					sumSoldTktQuantity = sumSoldTktQuantity + soldTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(soldTktQuantity));
				}else{
					sumSoldTktQuantity = sumSoldTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					invoiceTotal = Util.getRoundedValue(Double.parseDouble(dataObj[7].toString()));
					sumInvoiceTotal = sumInvoiceTotal + invoiceTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTotal));
				}else{
					sumInvoiceTotal = sumInvoiceTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					ticketPrice = Util.getRoundedValue(Double.parseDouble(dataObj[8].toString()));
					sumTicketPrice = sumTicketPrice + ticketPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(ticketPrice));
				}else{
					sumTicketPrice = sumTicketPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					soldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[9].toString()));
					sumSoldPrice = sumSoldPrice + soldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(soldPrice));
				}else{
					sumSoldPrice = sumSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					actualSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[10].toString()));
					sumActualSoldPrice = sumActualSoldPrice + actualSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(actualSoldPrice));
				}else{
					sumActualSoldPrice = sumActualSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					ticketCost = Util.getRoundedValue(Double.parseDouble(dataObj[11].toString()));
					sumTicketCost = sumTicketCost + ticketCost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(ticketCost));
				}else{
					sumTicketCost = sumTicketCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					totalTicketCost = Util.getRoundedValue(Double.parseDouble(dataObj[12].toString()));
					sumTotalTicketCost = sumTotalTicketCost + totalTicketCost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalTicketCost));
				}else{
					sumTotalTicketCost = sumTotalTicketCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					totalSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[13].toString()));
					sumTotalSoldPrice = sumTotalSoldPrice + totalSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalSoldPrice));
				}else{
					sumTotalSoldPrice = sumTotalSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					netSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[14].toString()));
					sumNetSoldPrice = sumNetSoldPrice + netSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(netSoldPrice));
				}else{
					sumNetSoldPrice = sumNetSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					profitOrLoss = Util.getRoundedValue(Double.parseDouble(dataObj[15].toString()));
					sumProfitOrLoss = sumProfitOrLoss + profitOrLoss;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(profitOrLoss));
				}else{
					sumProfitOrLoss = sumProfitOrLoss + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					grossMargin = Util.getRoundedValue(Double.parseDouble(dataObj[16].toString()));
					//sumGrossMargin = sumGrossMargin + grossMargin;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(grossMargin));
				}else{
					//sumGrossMargin = sumGrossMargin + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataMonthlyFilledByInvoice != null && dataMonthlyFilledByInvoice.size() > 0){
				Row rowhead =   ssSheet4.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSoldTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTicketPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumActualSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTicketCost));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalTicketCost));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumNetSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumProfitOrLoss));
				j++;
				
				sumGrossMargin = (sumProfitOrLoss*100)/sumTotalSoldPrice;
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumGrossMargin));
				j++;
			}
			
			//Monthly Filled Report By PO
			List<String> hdrMonthlyFilledPO = new ArrayList<String>();
			hdrMonthlyFilledPO.add("PO Year");
			hdrMonthlyFilledPO.add("PO Month");
			hdrMonthlyFilledPO.add("PO Id");
			hdrMonthlyFilledPO.add("Invoice Id");
			hdrMonthlyFilledPO.add("Customer Order Id");
			hdrMonthlyFilledPO.add("Invoice Ticket Qty");
			hdrMonthlyFilledPO.add("Sold Ticket Qty");					
			hdrMonthlyFilledPO.add("Invoice Total");			
			hdrMonthlyFilledPO.add("Ticket Price");
			hdrMonthlyFilledPO.add("Sold Price");			
			hdrMonthlyFilledPO.add("Actual Sold Price");
			hdrMonthlyFilledPO.add("Ticket Cost");
			hdrMonthlyFilledPO.add("Total Ticket Cost");
			hdrMonthlyFilledPO.add("Total Sold Price");
			hdrMonthlyFilledPO.add("Net Sold Price");
			hdrMonthlyFilledPO.add("ActualProfitOrLoss");
			hdrMonthlyFilledPO.add("Gross Margin");
			
			Sheet ssSheet5 =  workbook.createSheet("MonthlyFilledReportByPO");
			ExcelUtil.generateExcelHeaderRow(hdrMonthlyFilledPO, ssSheet5);			
			List<Object[]> dataMonthlyFilledByPO = DAORegistry.getEventDAO().getRTFMonthlyFilledReportByPO();
			
			sumInvoiceTktQuantity = 0.0; sumSoldTktQuantity = 0.0; sumInvoiceTotal = 0.0; sumTicketPrice = 0.0;
			sumSoldPrice = 0.0; sumActualSoldPrice = 0.0; sumTicketCost = 0.0; sumTotalTicketCost = 0.0;
			sumTotalSoldPrice = 0.0; sumNetSoldPrice =0.0; sumProfitOrLoss = 0.0; sumGrossMargin = 0.0;
			invoiceTktQuantity = 0.0; soldTktQuantity = 0.0; invoiceTotal = 0.0; ticketPrice = 0.0;			
			soldPrice = 0.0; actualSoldPrice = 0.0; ticketCost = 0.0; totalTicketCost = 0.0;
			totalSoldPrice = 0.0; netSoldPrice =0.0; profitOrLoss = 0.0; grossMargin = 0.0;
			j = 0;
			rowCount = 1;
			
			for(Object[] dataObj : dataMonthlyFilledByPO){
				
				Row rowhead =   ssSheet5.createRow((int)rowCount);
				
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[1].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					invoiceTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[5].toString()));
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + invoiceTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTktQuantity));
				}else{
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					soldTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[6].toString()));
					sumSoldTktQuantity = sumSoldTktQuantity + soldTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(soldTktQuantity));
				}else{
					sumSoldTktQuantity = sumSoldTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					invoiceTotal = Util.getRoundedValue(Double.parseDouble(dataObj[7].toString()));
					sumInvoiceTotal = sumInvoiceTotal + invoiceTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTotal));
				}else{
					sumInvoiceTotal = sumInvoiceTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					ticketPrice = Util.getRoundedValue(Double.parseDouble(dataObj[8].toString()));
					sumTicketPrice = sumTicketPrice + ticketPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(ticketPrice));
				}else{
					sumTicketPrice = sumTicketPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					soldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[9].toString()));
					sumSoldPrice = sumSoldPrice + soldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(soldPrice));
				}else{
					sumSoldPrice = sumSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					actualSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[10].toString()));
					sumActualSoldPrice = sumActualSoldPrice + actualSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(actualSoldPrice));
				}else{
					sumActualSoldPrice = sumActualSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					ticketCost = Util.getRoundedValue(Double.parseDouble(dataObj[11].toString()));
					sumTicketCost = sumTicketCost + ticketCost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(ticketCost));
				}else{
					sumTicketCost = sumTicketCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					totalTicketCost = Util.getRoundedValue(Double.parseDouble(dataObj[12].toString()));
					sumTotalTicketCost = sumTotalTicketCost + totalTicketCost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalTicketCost));
				}else{
					sumTotalTicketCost = sumTotalTicketCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					totalSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[13].toString()));
					sumTotalSoldPrice = sumTotalSoldPrice + totalSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalSoldPrice));
				}else{
					sumTotalSoldPrice = sumTotalSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					netSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[14].toString()));
					sumNetSoldPrice = sumNetSoldPrice + netSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(netSoldPrice));
				}else{
					sumNetSoldPrice = sumNetSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					profitOrLoss = Util.getRoundedValue(Double.parseDouble(dataObj[15].toString()));
					sumProfitOrLoss = sumProfitOrLoss + profitOrLoss;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(profitOrLoss));
				}else{
					sumProfitOrLoss = sumProfitOrLoss + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					grossMargin = Util.getRoundedValue(Double.parseDouble(dataObj[16].toString()));
					//sumGrossMargin = sumGrossMargin + grossMargin;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(grossMargin));
				}else{
					//sumGrossMargin = sumGrossMargin + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataMonthlyFilledByPO != null && dataMonthlyFilledByPO.size() > 0){
				Row rowhead =   ssSheet5.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSoldTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTicketPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumActualSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTicketCost));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalTicketCost));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumNetSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumProfitOrLoss));
				j++;
				
				sumGrossMargin = (sumProfitOrLoss*100)/sumTotalSoldPrice;
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumGrossMargin));
				j++;
			}

			//Voided Invoice
			List<String> headerVoidedInvoice = new ArrayList<String>();
			headerVoidedInvoice.add("Invoice Id");
			headerVoidedInvoice.add("Invoice Created Date");
			headerVoidedInvoice.add("Customer Order Id");
			headerVoidedInvoice.add("Customer Order Created Date");
			headerVoidedInvoice.add("Customer Name");
			headerVoidedInvoice.add("Customer Email");
			headerVoidedInvoice.add("Event Name");
			headerVoidedInvoice.add("Event Date");
			headerVoidedInvoice.add("Event Time");
			headerVoidedInvoice.add("Venue Name");
			headerVoidedInvoice.add("Venue City");
			headerVoidedInvoice.add("Zone");
			headerVoidedInvoice.add("Order Tiket Qty");
			headerVoidedInvoice.add("Listing Price");					
			headerVoidedInvoice.add("Order Total");			
			headerVoidedInvoice.add("Ticket Price");
			headerVoidedInvoice.add("Sold Price");			
			headerVoidedInvoice.add("Order Type");
			headerVoidedInvoice.add("Order Platform");
			headerVoidedInvoice.add("Invoice Voided Date");			
			headerVoidedInvoice.add("Invoice Total");
			headerVoidedInvoice.add("Invoice Ticket Qty");
			headerVoidedInvoice.add("Invoice Refund Amount");
			headerVoidedInvoice.add("Order Refund Amount");
			
			Sheet ssSheet6 =  workbook.createSheet("VoidedInvoice");
			ExcelUtil.generateExcelHeaderRow(headerVoidedInvoice, ssSheet6);			
			List<Object[]> dataVoidedInvoice = DAORegistry.getEventDAO().getRTFVoidedInvoiceReport();
			
			Double sumOrderTktQuantity = 0.0, orderTktQuantity = 0.0; 
			sumOrderTotal = 0.0; sumTicketPrice = 0.0; sumSoldPrice = 0.0; sumInvoiceTotal = 0.0; sumInvoiceTktQuantity = 0.0; 			
			orderTotal = 0.0; ticketPrice = 0.0;	soldPrice = 0.0; invoiceTotal = 0.0; invoiceTktQuantity = 0.0; 
			Double sumInvoiceRefundAmt = 0.0, sumOrderRefundAmt = 0.0, invoiceRefundAmt = 0.0, orderRefundAmt = 0.0;
			j = 0;
			rowCount = 1;
			
			for(Object[] dataObj : dataVoidedInvoice){
				
				Row rowhead =   ssSheet6.createRow((int)rowCount);
				
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[1].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[3].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[7].toString()));
					cell.setCellStyle(cellStyle);					
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
						
				if(dataObj[12] != null){
					orderTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[12].toString()));
					sumOrderTktQuantity = sumOrderTktQuantity + orderTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(orderTktQuantity));
				}else{
					sumOrderTktQuantity = sumOrderTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				double listingPrice = 0.0;
				if(dataObj[13] != null){
					listingPrice = Util.getRoundedValue(Double.parseDouble(dataObj[13].toString()));
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(listingPrice));
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					orderTotal = Util.getRoundedValue(Double.parseDouble(dataObj[14].toString()));
					sumOrderTotal = sumOrderTotal + orderTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(orderTotal));
				}else{
					sumOrderTotal = sumOrderTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					ticketPrice = Util.getRoundedValue(Double.parseDouble(dataObj[15].toString()));
					sumTicketPrice = sumTicketPrice + ticketPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(ticketPrice));
				}else{
					sumTicketPrice = sumTicketPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					soldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[16].toString()));
					sumSoldPrice = sumSoldPrice + soldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(soldPrice));
				}else{
					sumSoldPrice = sumSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[17] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[17].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[18] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[18].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[19] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[19].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[20] != null){
					invoiceTotal = Util.getRoundedValue(Double.parseDouble(dataObj[20].toString()));
					sumInvoiceTotal = sumInvoiceTotal + invoiceTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTotal));
				}else{
					sumInvoiceTotal = sumInvoiceTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[21] != null){
					invoiceTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[21].toString()));
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + invoiceTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTktQuantity));
				}else{
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[22] != null){
					invoiceRefundAmt = Util.getRoundedValue(Double.parseDouble(dataObj[22].toString()));
					sumInvoiceRefundAmt = sumInvoiceRefundAmt + invoiceRefundAmt;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceRefundAmt));
				}else{
					sumInvoiceRefundAmt = sumInvoiceRefundAmt + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[23] != null){
					orderRefundAmt = Util.getRoundedValue(Double.parseDouble(dataObj[23].toString()));
					sumOrderRefundAmt = sumOrderRefundAmt + orderRefundAmt;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(orderRefundAmt));
				}else{
					sumOrderRefundAmt = sumOrderRefundAmt + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataVoidedInvoice != null && dataVoidedInvoice.size() > 0){
				Row rowhead =   ssSheet6.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumOrderTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumOrderTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTicketPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceRefundAmt));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumOrderRefundAmt));
				j++;
			}
			
			//response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_SALES_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping({"/DownloadRTFCustomerOrderDetailsReport"})
	public void downloadRTFCustomerOrderDetailsReport(HttpServletRequest request, HttpServletResponse response){
		try{
			List<String> headerList = new ArrayList<String>();
			//headerList.add("Customer ID");
			headerList.add("Customer Name");
			headerList.add("Email");
			headerList.add("Total No. Of Orders");
			headerList.add("No. Of Loyal Fan Orders");
			headerList.add("No. Of Regular Orders");

			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet("WebOrders");
			Sheet ssSheet1 =  workbook.createSheet("PhoneOrders");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet1);
			List<Object[]> webOrderList = DAORegistry.getEventDAO().getRTFCustomerOrderForWebOrdersReport();
			List<Object[]> phoneOrderList = DAORegistry.getEventDAO().getRTFCustomerOrderForPhoneOrdersReport();
			
			
			int j = 0;
			int rowCount = 1;
			for(Object[] dataObj : webOrderList){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
//				if(dataObj[0] != null){
//					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
//				}else{
//					rowhead.createCell((int) j).setCellValue("");
//				}
//				j++;
				
				if(dataObj[1] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[1].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null && !dataObj[3].toString().isEmpty()){
					rowhead.createCell((int) j).setCellValue(Integer.parseInt(dataObj[3].toString()));
				}else{
					rowhead.createCell((int) j).setCellValue(0);
				}
				j++;
				
				if(dataObj[4] != null && !dataObj[4].toString().isEmpty()){
					rowhead.createCell((int) j).setCellValue(Integer.parseInt(dataObj[4].toString()));
				}else{
					rowhead.createCell((int) j).setCellValue(0);
				}
				j++;
				
				if(dataObj[5] != null && !dataObj[5].toString().isEmpty()){
					rowhead.createCell((int) j).setCellValue(Integer.parseInt(dataObj[5].toString()));
				}else{
					rowhead.createCell((int) j).setCellValue(0);
				}
				j++;
			}
			
			rowCount = 1;
			for(Object[] dataObj : phoneOrderList){
				Row rowhead =   ssSheet1.createRow((int)rowCount);
				rowCount++;
				j = 0;
//				if(dataObj[0] != null){
//					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
//				}else{
//					rowhead.createCell((int) j).setCellValue("");
//				}
//				j++;
				
				if(dataObj[1] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[1].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null && !dataObj[3].toString().isEmpty()){
					rowhead.createCell((int) j).setCellValue(Integer.parseInt(dataObj[3].toString()));
				}else{
					rowhead.createCell((int) j).setCellValue(0);
				}
				j++;
				
				if(dataObj[4] != null && !dataObj[4].toString().isEmpty()){
					rowhead.createCell((int) j).setCellValue(Integer.parseInt(dataObj[4].toString()));
				}else{
					rowhead.createCell((int) j).setCellValue(0);
				}
				j++;
				
				if(dataObj[5] != null && !dataObj[5].toString().isEmpty()){
					rowhead.createCell((int) j).setCellValue(Integer.parseInt(dataObj[5].toString()));
				}else{
					rowhead.createCell((int) j).setCellValue(0);
				}
				j++;
			}
			
			//response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_CUSTOMER_ORDER_DETAILS_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping({"/DownloadRTFCustomerOrdersDuplicateReport"})
	public void downloadRTFRepeatedCustomerReport(HttpServletRequest request, HttpServletResponse response){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("No. Of Users");
			headerList.add("No. Of Orders");
			headerList.add("Percentage(%) Of Users");

			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet("RepeatedOrders");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<Object[]> repeatedCustomersList = DAORegistry.getEventDAO().getRTFCustomerOrdersDuplicationsReport();
			
			int j = 0;
			int rowCount = 1;
			for(Object[] dataObj : repeatedCustomersList){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(Integer.parseInt(dataObj[0].toString()));
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[1] != null){
					rowhead.createCell((int) j).setCellValue(Integer.parseInt(dataObj[1].toString()));
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(Double.parseDouble(String.format("%.2f", dataObj[2])));
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;								
			}
			
			//response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_REPEATED_ORDERS_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping({"/DownloadRTFChannelWiseCustomerOrdersReport"})
	public void downloadRTFChannelWiseCustomerOrderReport(HttpServletRequest request, HttpServletResponse response){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("No. Of Users");
			headerList.add("No. Of Orders");
			headerList.add("Platform");
			headerList.add("Percentage(%) Of Users");

			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet("ChannelWiseCustomerOrder");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<Object[]> repeatedCustomersList = DAORegistry.getEventDAO().getRTFChannelWiseCustomerOrdersReport();
			
			int j = 0;
			int rowCount = 1;
			for(Object[] dataObj : repeatedCustomersList){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(Integer.parseInt(dataObj[0].toString()));
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[1] != null){
					rowhead.createCell((int) j).setCellValue(Integer.parseInt(dataObj[1].toString()));
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(Double.parseDouble(String.format("%.2f", dataObj[3])));
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;								
			}
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_CHANNELWISE_ORDERS_REPORT+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping({"/DownloadClientMasterReport"})
	public void downloadClientMasterReport(HttpServletRequest request, HttpServletResponse response){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Customer Id");
			headerList.add("First Name");
			headerList.add("Last Name");
			headerList.add("Email");
			headerList.add("Company Name");
			headerList.add("Product Type");
			headerList.add("Address Line1");
			headerList.add("Address Line2");
			headerList.add("City");
			headerList.add("State");
			headerList.add("Country");
			headerList.add("Zip Code");
			headerList.add("Phone");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet("ClientMaster");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<Customers> clientMastersList = DAORegistry.getEventDAO().getClientMasterReport();
			
			int j = 0;
			int rowCount = 1;
			
			Map<String, Customers> clientMap  = new HashMap<String, Customers>();
			for(Customers cust : clientMastersList){
				clientMap.put(cust.getCustomerEmail().toUpperCase().trim(), cust);
			}
			
			for(Customers dataObj : clientMap.values()){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				if(dataObj.getCustomerId() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getCustomerId().toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getFirstName() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getFirstName());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj.getLastName() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getLastName());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getCustomerEmail() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getCustomerEmail());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getCompanyName() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getCompanyName());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getProductType() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getProductType());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getAddressLine1() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getAddressLine1());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getAddressLine2() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getAddressLine2());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getCity() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getCity());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getState() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getState());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getCountry() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getCountry());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getZipCode() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getZipCode());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getPhone() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getPhone());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			//response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.CLIENT_MASTER_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping({"/DownloadUnsubscribedClientMasterReport"})
	public void downloadUnsubscribedClientMasterReport(HttpServletRequest request, HttpServletResponse response){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Customer Id");
			headerList.add("First Name");
			headerList.add("Last Name");
			headerList.add("Email");
			headerList.add("Company Name");
			headerList.add("Product Type");
			headerList.add("Address Line1");
			headerList.add("Address Line2");
			headerList.add("City");
			headerList.add("State");
			headerList.add("Country");
			headerList.add("Zip Code");
			headerList.add("Phone");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet("UnsubscribedClientMaster");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<Customers> unsubscribedClientMastersList = DAORegistry.getEventDAO().getUnsubscribedClientMasterReport();
			
			int j = 0;
			int rowCount = 1;
			
			Map<String, Customers> clientMap  = new HashMap<String, Customers>();
			for(Customers cust : unsubscribedClientMastersList){
				clientMap.put(cust.getCustomerEmail().toUpperCase().trim(), cust);
			}
			
			for(Customers dataObj : clientMap.values()){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				if(dataObj.getCustomerId() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getCustomerId().toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getFirstName() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getFirstName());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj.getLastName() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getLastName());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getCustomerEmail() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getCustomerEmail());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getCompanyName() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getCompanyName());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getProductType() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getProductType());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getAddressLine1() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getAddressLine1());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getAddressLine2() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getAddressLine2());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getCity() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getCity());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getState() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getState());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getCountry() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getCountry());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getZipCode() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getZipCode());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getPhone() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getPhone());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			//response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.UNSUBSCRIBED_CLIENT_MASTER_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping({"/DownloadPurchasedMasterReport"})
	public void downloadPurchasedMasterReport(HttpServletRequest request, HttpServletResponse response){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Customer Id");
			headerList.add("First Name");
			headerList.add("Last Name");
			headerList.add("Email");
			headerList.add("Company Name");
			headerList.add("Product Type");
			headerList.add("Address Line1");
			headerList.add("Address Line2");
			headerList.add("City");
			headerList.add("State");
			headerList.add("Country");
			headerList.add("Zip Code");
			headerList.add("Phone");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet("PurchasedMaster");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<Object[]> puchasedMastersList = DAORegistry.getEventDAO().getPurchasedMasterReport();
			
			int j = 0;
			int rowCount = 1;
			for(Object[] dataObj : puchasedMastersList){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[1] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[1].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			//response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.PURCHASED_MASTER_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping({"/DownloadRTFRegisteredButnotPurchasedReport"})
	public void downloadRTFRegisteredButnotPurchasedReport(HttpServletRequest request, HttpServletResponse response){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Customer Id");
			headerList.add("First Name");
			headerList.add("Last Name");
			headerList.add("Email");
			headerList.add("Company Name");
			headerList.add("Product Type");
			headerList.add("Address Line1");
			headerList.add("Address Line2");
			headerList.add("City");
			headerList.add("State");
			headerList.add("Country");
			headerList.add("Zip Code");
			headerList.add("Phone");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet("RegisteredButnotPurchased");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<Object[]> registeredButnotPurchasedList = DAORegistry.getEventDAO().getRTFRegisteredButnotPurchasedReport();
			
			int j = 0;
			int rowCount = 1;
			for(Object[] dataObj : registeredButnotPurchasedList){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[1] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[1].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			//response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_REGISTERED_BUT_NOT_PURCHASED_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping({"/DownloadRTFCombinedPurchasedNonPurchasedReport"})
	public void downloadRTFCombinedPurchasedNonPurchasedReport(HttpServletRequest request, HttpServletResponse response){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Customer Id");
			headerList.add("First Name");
			headerList.add("Last Name");
			headerList.add("Email");
			headerList.add("Company Name");
			headerList.add("Product Type");
			headerList.add("Address Line1");
			headerList.add("Address Line2");
			headerList.add("City");
			headerList.add("State");
			headerList.add("Country");
			headerList.add("Zip Code");
			headerList.add("Phone");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet("CombinedPurchasedNonPurchased");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<Object[]> purchasedNonPurchasedList = DAORegistry.getEventDAO().getRTFCombinedPurchasedNonPurchasedReport();
			
			int j = 0;
			int rowCount = 1;
			for(Object[] dataObj : purchasedNonPurchasedList){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[1] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[1].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			//response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_COMBINED_PURCHASED_NONPURCHASED_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	

	@RequestMapping({"/DownloadRTFLoyalFanReport"})
	public void downloadRTFLoyalFanReport(HttpServletRequest request, HttpServletResponse response){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Customer Id");
			headerList.add("First Name");
			headerList.add("Last Name");
			headerList.add("Email");
			headerList.add("Company Name");
			headerList.add("Product Type");
			headerList.add("Address Line1");
			headerList.add("Address Line2");
			headerList.add("City");
			headerList.add("State");
			headerList.add("Country");
			headerList.add("Zip Code");
			headerList.add("Phone");
			headerList.add("Loyal Fan Type");
			headerList.add("Loyal Fan Name");
			headerList.add("Loyal Fan Start Date");
			headerList.add("Loyal Fan End Date");
			headerList.add("Loyal Fan Ticket Purchased");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet("LoyalFan");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<Object[]> loyalFanList = DAORegistry.getEventDAO().getRTFLoyalFanReport();
			
			CreationHelper createHelper = workbook.getCreationHelper();	
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0;
			int rowCount = 1;
			for(Object[] dataObj : loyalFanList){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[1] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[1].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[13].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[14].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[15].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[16].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[17] != null){
					Boolean ticketPurchased = Boolean.parseBoolean(dataObj[17].toString());
					if(ticketPurchased != null && ticketPurchased){
						rowhead.createCell((int) j).setCellValue("Yes");
					}else{
						rowhead.createCell((int) j).setCellValue("No");
					}
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			//response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_LOYAL_FAN_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping({"/DownloadRTFLoyalFanPurchasedReport"})
	public void downloadRTFLoyalFanPurchasedReport(HttpServletRequest request, HttpServletResponse response){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Customer Id");
			headerList.add("First Name");
			headerList.add("Last Name");
			headerList.add("Email");
			headerList.add("Company Name");
			headerList.add("Product Type");
			headerList.add("Address Line1");
			headerList.add("Address Line2");
			headerList.add("City");
			headerList.add("State");
			headerList.add("Country");
			headerList.add("Zip Code");
			headerList.add("Phone");
			headerList.add("Loyal Fan Type");
			headerList.add("Loyal Fan Name");
			headerList.add("Loyal Fan Start Date");
			headerList.add("Loyal Fan End Date");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet("LoyalFanPurchased");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<Object[]> loyalFanPurchasedList = DAORegistry.getEventDAO().getRTFLoyalFanPurchasedReport();
			
			CreationHelper createHelper = workbook.getCreationHelper();	
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0;
			int rowCount = 1;
			for(Object[] dataObj : loyalFanPurchasedList){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[1] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[1].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[13].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[14].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[15].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[16].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			//response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_LOYAL_FAN_PURCHASED_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping({"/DownloadRTFLoyalFanNonPurchasedReport"})
	public void downloadRTFLoyalFanNonPurchasedReport(HttpServletRequest request, HttpServletResponse response){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Customer Id");
			headerList.add("First Name");
			headerList.add("Last Name");
			headerList.add("Email");
			headerList.add("Company Name");
			headerList.add("Product Type");
			headerList.add("Address Line1");
			headerList.add("Address Line2");
			headerList.add("City");
			headerList.add("State");
			headerList.add("Country");
			headerList.add("Zip Code");
			headerList.add("Phone");
			headerList.add("Loyal Fan Type");
			headerList.add("Loyal Fan Name");
			headerList.add("Loyal Fan Start Date");
			headerList.add("Loyal Fan End Date");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet("LoyalFanNonPurchased");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<Object[]> loyalFanNonPurchasedList = DAORegistry.getEventDAO().getRTFLoyalFanNonPurchasedReport();
			
			CreationHelper createHelper = workbook.getCreationHelper();	
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0;
			int rowCount = 1;
			for(Object[] dataObj : loyalFanNonPurchasedList){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[1] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[1].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[13].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[14].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[15].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[16].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			//response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_LOYAL_FAN_NON_PURCHASED_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping({"/DownloadRTFSalesReportByReferral"})
	public void downloadRTFSalesReportByReferral(HttpServletRequest request, HttpServletResponse response){
		try{
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			//RTF Sales Report By Referral
			List<String> headerList = new ArrayList<String>();
			headerList.add("Invoice No");
			headerList.add("Invoice Created Date");
			headerList.add("Invoice Status");
			headerList.add("Order No");
			headerList.add("Order Created Date");
			headerList.add("PO No.");
			headerList.add("PO Created Date");
			headerList.add("Event Name");
			headerList.add("Event Date");
			headerList.add("Event Time");
			headerList.add("Venue Name");
			headerList.add("Venue City");
			headerList.add("Zone");
			headerList.add("Quantity");
			headerList.add("Section");
			headerList.add("Row");
			headerList.add("Seat");
			headerList.add("Customer Name");
			headerList.add("Customer Email");
			headerList.add("Listing Price");
			headerList.add("Promocode Discount Amt");
			headerList.add("Loyalfan Discount Amt");
			headerList.add("Order Total");
			headerList.add("Cost");
			headerList.add("Net Total");
			headerList.add("Profit And Loss");
			headerList.add("Order Type");
			headerList.add("Platform");
			headerList.add("Payment Methods");
			headerList.add("Promotional Code Used");
			headerList.add("Promotional Code");
			headerList.add("Promotional Discount");
			headerList.add("LoyalFan Discount");
			headerList.add("Referral Code");
			headerList.add("Referrer Customer Email");
						
			Sheet ssSheet =  workbook.createSheet("RTFSalesOrdersByReferral");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);			
			List<Object[]> dataList = DAORegistry.getEventDAO().getRTFSalesReportByReferral();
			
			List<Integer> invoiceIds = new ArrayList<Integer>();
			Double sumOrderTotal = 0.0,sumNetTotal = 0.0,sumCost = 0.0,sumProfitAndLoss = 0.0;
			Double orderTotal = 0.0,netTotal = 0.0,cost = 0.0,profitAndLoss = 0.0;
			int j = 0;
			int rowCount = 1;
			boolean flag=false;
			for(Object[] dataObj : dataList){
				if(invoiceIds.contains((Integer)dataObj[0])){
					String poIds = "";
					Double poCost = 0.00;
					poIds = ssSheet.getRow(rowCount-1).getCell(5).getStringCellValue();
					poIds += ","+dataObj[5].toString();
					ssSheet.getRow(rowCount-1).getCell(5).setCellValue(poIds);
					
					poCost = ssSheet.getRow(rowCount-1).getCell(23).getNumericCellValue();
					cost = Util.getRoundedValue(Double.parseDouble(dataObj[23].toString()));
					poCost = poCost + cost;
					ssSheet.getRow(rowCount-1).getCell(23).setCellValue(poCost);
					flag = true;
					continue;
				}
				if(flag){
					Double c =  ssSheet.getRow(rowCount-1).getCell(23).getNumericCellValue();
					Double n =  ssSheet.getRow(rowCount-1).getCell(24).getNumericCellValue();
					ssSheet.getRow(rowCount-1).getCell(25).setCellValue(n-c);
				}
				flag = false;
				Row rowhead = ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				invoiceIds.add((Integer)dataObj[0]);
				j++;
				
				if(dataObj[1] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[1].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[4].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[6].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[8].toString()));
					cell.setCellStyle(cellStyle);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[13].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[14].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[15].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[16].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[17] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[17].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[18] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[18].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				Double doubleVariable = 0.00;
				if(dataObj[19] != null){
					doubleVariable = Util.getRoundedValue(Double.parseDouble(dataObj[19].toString()));
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(doubleVariable));
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				doubleVariable = 0.00;
				if(dataObj[20] != null){
					doubleVariable = Util.getRoundedValue(Double.parseDouble(dataObj[20].toString()));
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(doubleVariable));
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				doubleVariable = 0.00;
				if(dataObj[21] != null){
					doubleVariable = Util.getRoundedValue(Double.parseDouble(dataObj[21].toString()));
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(doubleVariable));
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[22] != null){
					orderTotal = Util.getRoundedValue(Double.parseDouble(dataObj[22].toString()));
					sumOrderTotal = sumOrderTotal + orderTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(orderTotal));
				}else{
					sumOrderTotal = sumOrderTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[23] != null){
					cost = Util.getRoundedValue(Double.parseDouble(dataObj[23].toString()));
					sumCost = sumCost + cost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(cost));
				}else{
					sumCost = sumCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[24] != null){
					netTotal = Util.getRoundedValue(Double.parseDouble(dataObj[24].toString()));
					sumNetTotal = sumNetTotal + netTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(netTotal));
				}else{
					sumNetTotal = sumNetTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[25] != null){
					profitAndLoss = Util.getRoundedValue(Double.parseDouble(dataObj[25].toString()));
					sumProfitAndLoss = sumProfitAndLoss + profitAndLoss;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(profitAndLoss));
				}else{
					sumProfitAndLoss = sumProfitAndLoss + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				
				if(dataObj[26] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[26].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}	
				j++;
				
				if(dataObj[27] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[27].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[28] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[28].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[29] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[29].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[30] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[30].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[31] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[31].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[32] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[32].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[33] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[33].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[34] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[34].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataList != null && dataList.size() > 0){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumOrderTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumCost));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumNetTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumProfitAndLoss));
				j++;
				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
			}
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_SALES_REPORT_BY_REFERRAL+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
//	@RequestMapping({"/LoadRTFCustomerSpentReport"})
//	public String loadRTFCustomerSpentReport(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session) throws IOException{
//				
//		return "page-customer-spent-report";
//	}
	
	@RequestMapping({"/DownloadRTFCustomerSpentReport"})
	public void downloadRTFCustomerSpentReport(HttpServletRequest request, HttpServletResponse response){
		try{
			String selectedYears = request.getParameter("selectedYears");
//			String selectedOrders = request.getParameter("selectedOrders");
			String selectedOrders = request.getParameter("selectedOrders");
			String yearStr = "";
			Boolean isAll = false;
			Boolean isPhone = false;
			Boolean isWeb = false;
			Boolean isApp = false;
			
			if(selectedYears!=null && !selectedYears.isEmpty()){
				if(selectedYears.contains("ALL")){
					yearStr = "ALL";
				}else if(selectedYears.contains(",")){
					String arr[] = selectedYears.split(",");
					for(String str : arr){						
						yearStr = yearStr + "'"+str+"'" +",";						
					}
					yearStr = yearStr.substring(0, yearStr.length()-1);					
				}else{
					if(selectedYears.equalsIgnoreCase("ALL")){
						yearStr = "ALL";
					}else{
						yearStr = yearStr + "'"+selectedYears+"'";
					}
				}
			}
			
			if(selectedOrders!=null && !selectedOrders.isEmpty()){
				if(selectedOrders.contains(",")){
					String arr[] = selectedOrders.split(",");
					for(String str : arr){
						if(str.equalsIgnoreCase("ALL")){
							isAll=true;
						}
						if(str.equalsIgnoreCase("PHONE_ORDERS")){
							isPhone=true;
						}
						if(str.equalsIgnoreCase("WEB/APP_ORDERS")){
							isWeb=true;
							isApp=true;
						}
					}
				}else{
					if(selectedOrders.equalsIgnoreCase("ALL")){
						isAll=true;
					}
					if(selectedOrders.equalsIgnoreCase("PHONE_ORDERS")){
						isPhone=true;
					}
					if(selectedOrders.equalsIgnoreCase("WEB/APP_ORDERS")){
						isWeb=true;
						isApp=true;
					}
				}
			}
			
			String selPlatform = "";
			if(isAll){
				selPlatform="ALL";
			}else if(isPhone){
				selPlatform="'TICK_TRACKER'";
			}else if(isWeb && isApp){
				selPlatform="'DESKTOP_SITE','IOS','ANDROID'";
			}
			
//			if(isAll){
//				selPlatform="ALL";
//			}else if(isPhone && isWeb && isApp){
//				selPlatform="'TICK_TRACKER','DESKTOP_SITE','IOS','ANDROID'";
//			}else if(isPhone && isWeb){
//				selPlatform="'TICK_TRACKER','DESKTOP_SITE'";
//			}else if(isPhone && isApp){
//				selPlatform="'TICK_TRACKER','IOS','ANDROID'";
//			}else if(isWeb && isApp){
//				selPlatform="'DESKTOP_SITE','IOS','ANDROID'";
//			}else if(isPhone){
//				selPlatform="'TICK_TRACKER'";
//			}else if(isWeb){
//				selPlatform="'DESKTOP_SITE'";
//			}else if(isApp){
//				selPlatform="'IOS','ANDROID'";
//			}
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			//RTF Customer Year(Platform Wise) Spent Report
			List<String> headerList = new ArrayList<String>();
//			headerList.add("Order Year");
			headerList.add("CustomerName");
			headerList.add("No Of Orders");
			headerList.add("Total Ticket Quantity");
			headerList.add("Total Order Value");
			headerList.add("Earned Points");
			headerList.add("Redeemed Points");
			headerList.add("LoyalFan Discount");
			headerList.add("App Discount");
			headerList.add("Promotional Discounts");
			headerList.add("SpentOn Customer");
			headerList.add("Average SpentOn Customer");
			
//			if(selPlatform != null && !selPlatform.isEmpty()){
//				headerList.add("Platform");
//			}
			
			Sheet ssSheet = null;
			List<Object[]> dataList = null;
			//if(selPlatform != null && !selPlatform.isEmpty()){
				ssSheet = workbook.createSheet("YearlyCustomerWise");
				ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);			
				dataList = DAORegistry.getEventDAO().getRTFCustomerPlatformWiseReport(yearStr, selPlatform);
//			}else{
//				ssSheet = workbook.createSheet("YearlyCustomerWise");
//				ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);			
//				dataList = DAORegistry.getEventDAO().getRTFCustomerYearlySpentReport(yearStr);
//			}
			
			Double sumOrders = 0.0, sumTotalTicketQuantity = 0.0, sumTotalOrderValue = 0.0;
			Double orders = 0.0, totalTicketQuantity = 0.0, totalOrderValue = 0.0;
			Double sumEarnedPoints = 0.0, sumRedeemedPoints = 0.0;
			Double earnedPoints = 0.0, redeemedPoints = 0.0;
			Double sumLoyalFanDiscount = 0.0, sumAppDiscount = 0.0, sumPromotionalDiscounts = 0.0;
			Double loyalFanDiscount = 0.0, appDiscount = 0.0, promotionalDiscounts = 0.0;
			Double sumSpentOnCustomer = 0.0, sumAverageSpentOnCustomer = 0.0;
			Double spentOnCustomer = 0.0, averageSpentOnCustomer = 0.0;
			
			int j = 0;
			int rowCount = 1;
			
			for(Object[] dataObj : dataList){				
				Row rowhead = ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[1] != null){
					orders = Util.getRoundedValue(Double.parseDouble(dataObj[1].toString()));
					sumOrders = sumOrders + orders;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(orders));
				}else{
					sumOrders = sumOrders + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[2] != null){
					totalTicketQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[2].toString()));
					sumTotalTicketQuantity = sumTotalTicketQuantity + totalTicketQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalTicketQuantity));
				}else{
					sumTotalTicketQuantity = sumTotalTicketQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					totalOrderValue = Util.getRoundedValue(Double.parseDouble(dataObj[3].toString()));
					sumTotalOrderValue = sumTotalOrderValue + totalOrderValue;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalOrderValue));
				}else{
					sumTotalOrderValue = sumTotalOrderValue + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					earnedPoints = Util.getRoundedValue(Double.parseDouble(dataObj[4].toString()));
					sumEarnedPoints = sumEarnedPoints + earnedPoints;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(earnedPoints));
				}else{
					sumEarnedPoints = sumEarnedPoints + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					redeemedPoints = Util.getRoundedValue(Double.parseDouble(dataObj[5].toString()));
					sumRedeemedPoints = sumRedeemedPoints + redeemedPoints;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(redeemedPoints));
				}else{
					sumRedeemedPoints = sumRedeemedPoints + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					loyalFanDiscount = Util.getRoundedValue(Double.parseDouble(dataObj[6].toString()));
					sumLoyalFanDiscount = sumLoyalFanDiscount + loyalFanDiscount;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(loyalFanDiscount));
				}else{
					sumLoyalFanDiscount = sumLoyalFanDiscount + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					appDiscount = Util.getRoundedValue(Double.parseDouble(dataObj[7].toString()));
					sumAppDiscount = sumAppDiscount + appDiscount;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(appDiscount));
				}else{
					sumAppDiscount = sumAppDiscount + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					promotionalDiscounts = Util.getRoundedValue(Double.parseDouble(dataObj[8].toString()));
					sumPromotionalDiscounts = sumPromotionalDiscounts + promotionalDiscounts;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(promotionalDiscounts));
				}else{
					sumPromotionalDiscounts = sumPromotionalDiscounts + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					spentOnCustomer = Util.getRoundedValue(Double.parseDouble(dataObj[9].toString()));
					sumSpentOnCustomer = sumSpentOnCustomer + spentOnCustomer;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(spentOnCustomer));
				}else{
					sumSpentOnCustomer = sumSpentOnCustomer + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					averageSpentOnCustomer = Util.getRoundedValue(Double.parseDouble(dataObj[10].toString()));
					sumAverageSpentOnCustomer = sumAverageSpentOnCustomer + averageSpentOnCustomer;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(averageSpentOnCustomer));
				}else{
					sumAverageSpentOnCustomer = sumAverageSpentOnCustomer + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
//				if(selPlatform != null && !selPlatform.isEmpty()){
//					if(dataObj[12] != null){
//						rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
//					}else{
//						rowhead.createCell((int) j).setCellValue("");
//					}
//					j++;
//				}
			}
			
			if(dataList != null && dataList.size() > 0){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumOrders));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalTicketQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalOrderValue));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumEarnedPoints));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumRedeemedPoints));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumLoyalFanDiscount));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumAppDiscount));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumPromotionalDiscounts));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSpentOnCustomer));
				j++;
				
				sumAverageSpentOnCustomer = sumSpentOnCustomer/sumOrders;
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumAverageSpentOnCustomer));
				j++;
				
//				if(selPlatform != null && !selPlatform.isEmpty()){
//					rowhead.createCell((int) j).setCellValue("");
//					j++;
//				}
			}
			
			//RTF Customer Order Wise Report
			List<String> headerList1 = new ArrayList<String>();
			headerList1.add("Order Year");
			headerList1.add("CustomerName");
			headerList1.add("Order No");
			headerList1.add("Order Date");
			headerList1.add("Order Quantity");
			headerList1.add("Order Total");
			headerList1.add("Earned Points");
			headerList1.add("Redeemed Points");
			headerList1.add("LoyalFan Discount");
			headerList1.add("App Discount");
			headerList1.add("Promotional Discounts");
			headerList1.add("SpentOn Customer");
			headerList1.add("Average SpentOn Customer");
//			headerList1.add("Promotional Type");
//			headerList1.add("Platform");
			
			Sheet ssSheet1 =  workbook.createSheet("CustomerOrderWise");
			ExcelUtil.generateExcelHeaderRow(headerList1, ssSheet1);			
			List<Object[]> dataList1 = DAORegistry.getEventDAO().getRTFCustomerOrderwiseReport(yearStr, selPlatform);
						
			Double sumOrderQuantity = 0.0, sumOrderTotal = 0.0;
			Double orderQuantity = 0.0, orderTotal = 0.0;
			sumEarnedPoints = 0.0; sumRedeemedPoints = 0.0;
			earnedPoints = 0.0; redeemedPoints = 0.0;
			sumLoyalFanDiscount = 0.0; sumAppDiscount = 0.0; sumPromotionalDiscounts = 0.0;
			loyalFanDiscount = 0.0; appDiscount = 0.0; promotionalDiscounts = 0.0;
						
			j = 0;
			rowCount = 1;
			Double redeemedPoint = 0.00, loyalFanDisc = 0.00, appDisc = 0.00, promotionalDisc = 0.00; 
			Double spentOnCustomers = 0.00, totalSpentOnCustomers = 0.00, averageSpentOnCustomers = 0.00;
			Double sumTotalSpentOnCustomers = 0.00, sumAverageSpentOnCustomers = 0.00;
			Integer sumNoOfOrders = 0;
			int noOfOrders = 0;
			
			//Map<String, String> map2 = new HashMap<String, String>();
			Map<Integer, String> map2 = new HashMap<Integer, String>();
			
			for(Object[] dataObj : dataList1){
				
				totalSpentOnCustomers= 0.00;
				noOfOrders = 0;
				//String key = String.valueOf(dataObj[1]).replaceAll("\\s", "").toUpperCase();
				Integer key = Integer.valueOf(dataObj[13].toString());
				
				String value = map2.get(key); 
				//totalOrderSpent + ":"+noOfOrders;
				
				if(value == null || value == ""){
					//spentOnCustomers = Util.getRoundedValue(Double.parseDouble(dataObj[5].toString())); //Order Total
					if(dataObj[7] != null){
						redeemedPoint = Util.getRoundedValue(Double.parseDouble(dataObj[7].toString())); //RedeemedPoints
					}
					if(dataObj[8] != null){
						loyalFanDisc = Util.getRoundedValue(Double.parseDouble(dataObj[8].toString())); //LoyalFanDiscount
					}
					if(dataObj[9] != null){
						appDisc = Util.getRoundedValue(Double.parseDouble(dataObj[9].toString())); //AppDiscount
					}
					if(dataObj[10] != null){
						promotionalDisc = Util.getRoundedValue(Double.parseDouble(dataObj[10].toString())); //PromotionalDiscount
					}
					spentOnCustomers = redeemedPoint + loyalFanDisc + appDisc + promotionalDisc; //Sum of (RedeemedPoints, LoyalFanDiscount, AppDiscount, PromotionalDiscount)
					value=spentOnCustomers+":"+1;
				}else{
					
					noOfOrders = Integer.parseInt(value.split(":")[1]);
					totalSpentOnCustomers = Double.valueOf(value.split(":")[0]);					
					//spentOnCustomers = Util.getRoundedValue(Double.parseDouble(dataObj[5].toString())); //Order Total
					if(dataObj[7] != null){
						redeemedPoint = Util.getRoundedValue(Double.parseDouble(dataObj[7].toString())); //RedeemedPoints
					}
					if(dataObj[8] != null){
						loyalFanDisc = Util.getRoundedValue(Double.parseDouble(dataObj[8].toString())); //LoyalFanDiscount
					}
					if(dataObj[9] != null){
						appDisc = Util.getRoundedValue(Double.parseDouble(dataObj[9].toString())); //AppDiscount
					}
					if(dataObj[10] != null){
						promotionalDisc = Util.getRoundedValue(Double.parseDouble(dataObj[10].toString())); //PromotionalDiscount
					}
					spentOnCustomers = redeemedPoint + loyalFanDisc + appDisc + promotionalDisc; //Sum of (RedeemedPoints, LoyalFanDiscount, AppDiscount, PromotionalDiscount)
					totalSpentOnCustomers = totalSpentOnCustomers + spentOnCustomers;
					noOfOrders++;
					value=totalSpentOnCustomers+":"+noOfOrders;
				}
				
				map2.put(key, value);
				
			}
			
			//Set<String> custNameSet = new HashSet<String>();
			Set<Integer> custNoSet = new HashSet<Integer>();
			
			for(Object[] dataObj : dataList1){
				Row rowhead = ssSheet1.createRow((int)rowCount);
				rowCount++;
				//String key = String.valueOf(dataObj[1]).replaceAll("\\s", "").toUpperCase();
				Integer key = Integer.valueOf(dataObj[13].toString());
				
				//if(custNameSet.add(key)){
				if(custNoSet.add(key)){
					
					String value = map2.get(key);
					
					int noOfOrder = Integer.parseInt(value.split(":")[1]);
					totalSpentOnCustomers = Double.valueOf(value.split(":")[0]);
					averageSpentOnCustomers = totalSpentOnCustomers / noOfOrder;
					
					sumTotalSpentOnCustomers = sumTotalSpentOnCustomers + totalSpentOnCustomers;
					sumNoOfOrders = sumNoOfOrders + noOfOrder;
					
					rowhead.createCell(1).setCellValue(dataObj[1].toString());
					rowhead.createCell(11).setCellValue(Util.getRoundedValue(totalSpentOnCustomers));
					rowhead.createCell(12).setCellValue(Util.getRoundedValue(averageSpentOnCustomers));
				}else{
					rowhead.createCell(1).setCellValue("");
					rowhead.createCell(11).setCellValue("");
					rowhead.createCell(12).setCellValue("");
				}
				
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
//				if(dataObj[1] != null){
//					rowhead.createCell((int) j).setCellValue(dataObj[1].toString());
//				}else{
//					rowhead.createCell((int) j).setCellValue("");
//				}				
				j++;
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{					
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[3].toString()));
					cell.setCellStyle(cellStyle);
				}else{					
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					orderQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[4].toString()));
					sumOrderQuantity = sumOrderQuantity + orderQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(orderQuantity));
				}else{
					sumOrderQuantity = sumOrderQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					orderTotal = Util.getRoundedValue(Double.parseDouble(dataObj[5].toString()));
					sumOrderTotal = sumOrderTotal + orderTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(orderTotal));
				}else{
					sumOrderTotal = sumOrderTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					earnedPoints = Util.getRoundedValue(Double.parseDouble(dataObj[6].toString()));
					sumEarnedPoints = sumEarnedPoints + earnedPoints;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(earnedPoints));
				}else{
					sumEarnedPoints = sumEarnedPoints + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					redeemedPoints = Util.getRoundedValue(Double.parseDouble(dataObj[7].toString()));
					sumRedeemedPoints = sumRedeemedPoints + redeemedPoints;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(redeemedPoints));
				}else{
					sumRedeemedPoints = sumRedeemedPoints + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					loyalFanDiscount = Util.getRoundedValue(Double.parseDouble(dataObj[8].toString()));
					sumLoyalFanDiscount = sumLoyalFanDiscount + loyalFanDiscount;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(loyalFanDiscount));
				}else{
					sumLoyalFanDiscount = sumLoyalFanDiscount + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					appDiscount = Util.getRoundedValue(Double.parseDouble(dataObj[9].toString()));
					sumAppDiscount = sumAppDiscount + appDiscount;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(appDiscount));
				}else{
					sumAppDiscount = sumAppDiscount + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					promotionalDiscounts = Util.getRoundedValue(Double.parseDouble(dataObj[10].toString()));
					sumPromotionalDiscounts = sumPromotionalDiscounts + promotionalDiscounts;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(promotionalDiscounts));
				}else{
					sumPromotionalDiscounts = sumPromotionalDiscounts + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
//				if(dataObj[11] != null){
//					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
//				}else{
//					rowhead.createCell((int) j).setCellValue("");
//				}
//				j++;
//				
//				if(dataObj[12] != null){
//					rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
//				}else{
//					rowhead.createCell((int) j).setCellValue("");
//				}
//				j++;
			}
			
			if(dataList1 != null && dataList1.size() > 0){
				Row rowhead =   ssSheet1.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumOrderQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumOrderTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumEarnedPoints));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumRedeemedPoints));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumLoyalFanDiscount));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumAppDiscount));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumPromotionalDiscounts));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalSpentOnCustomers));
				j++;
				
				sumAverageSpentOnCustomers = sumTotalSpentOnCustomers / sumNoOfOrders;
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumAverageSpentOnCustomers));
				j++;
			}
			
			//RTF Customer Acquisition Cost Report
			List<String> headerList2 = new ArrayList<String>();
			headerList2.add("CustomerName");
			headerList2.add("No Of Orders");
			headerList2.add("Total Order Value");
			headerList2.add("Customer Acquisition Cost");
			headerList2.add("Average Cost Per Order");
		
			Sheet ssSheet2 = workbook.createSheet("AcquisitionCost");
			List<Object[]> dataList2 = DAORegistry.getEventDAO().getRTFCustomerAcquisitionReport(yearStr, selPlatform);			
			ExcelUtil.generateExcelHeaderRow(headerList2, ssSheet2);			
						
			orders = 0.00; sumOrders = 0.00; totalOrderValue = 0.00; sumTotalOrderValue = 0.00;
			Double acquisCost = 0.00, sumAcquisCost = 0.00, avgCostPerOrder = 0.00, sumAvgCostPerOrder = 0.00;
			j = 0;
			rowCount = 1;
			
			for(Object[] dataObj : dataList2){				
				Row rowhead = ssSheet2.createRow((int)rowCount);
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[1] != null){
					orders = Util.getRoundedValue(Double.parseDouble(dataObj[1].toString()));
					sumOrders = sumOrders + orders;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(orders));
				}else{
					sumOrders = sumOrders + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[2] != null){
					totalOrderValue = Util.getRoundedValue(Double.parseDouble(dataObj[2].toString()));
					sumTotalOrderValue = sumTotalOrderValue + totalOrderValue;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalOrderValue));
				}else{
					sumTotalOrderValue = sumTotalOrderValue + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					acquisCost = Util.getRoundedValue(Double.parseDouble(dataObj[3].toString()));
					sumAcquisCost = sumAcquisCost + acquisCost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(acquisCost));
				}else{
					sumAcquisCost = sumAcquisCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					avgCostPerOrder = Util.getRoundedValue(Double.parseDouble(dataObj[4].toString()));
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(avgCostPerOrder));
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataList2 != null && dataList2.size() > 0){
				Row rowhead =   ssSheet2.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumOrders));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalOrderValue));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumAcquisCost));
				j++;
				
				sumAvgCostPerOrder = sumAcquisCost/sumOrders;
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumAvgCostPerOrder));
				j++;				
			}
			
			List<Object[]> dataList3 = DAORegistry.getEventDAO().getRTFCustomerAcquisitionSummaryReport(yearStr, selPlatform);
			Double avgCostOrder = 0.0;
			rowCount++;
			rowCount++;
			
			for(Object[] dataObj : dataList3){				
				Row rowhead = ssSheet2.createRow((int)rowCount);
				rowCount++; 
				j = 0;
				
				if(dataObj[1] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[1].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[2] != null){
					avgCostOrder = Util.getRoundedValue(Double.parseDouble(dataObj[2].toString()));
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(avgCostOrder));
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_CUSTOMER_SPENT_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
//	@RequestMapping({"/LoadRTFTMATSeatAllocationReport"})
//	public String loadRTFTMATSeatAllocationReport(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session) throws IOException{
//				
//		return "page-tmat-seat-allocation-report";
//	}
	
	@RequestMapping({"/DownloadRTFTMATSeatAllocationReport"})
	public void downloadRTFTMATSeatAllocationReport(HttpServletRequest request, HttpServletResponse response){
		try{
			String artistIdStr = request.getParameter("artistId");
			
			String artistName = null;
			Artist artist = null;
			if(artistIdStr != null && !artistIdStr.equals("")) {
				artist = DAORegistry.getArtistDAO().get(Integer.parseInt(artistIdStr));
			}
			if(artist != null){
				artistName = artist.getName();
			}
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("Order Id");
			headerList.add("Order Date");
			headerList.add("Order Type");
			headerList.add("Event Name");
			headerList.add("Event Date");
			headerList.add("Event Time");
			headerList.add("Venue Name");
			headerList.add("Venue City");
			headerList.add("Venue State");
			headerList.add("Venue Country");
			headerList.add("Order Total");
			headerList.add("Zone");
			headerList.add("RTF Quantity");
			headerList.add("TMAT Section");
			headerList.add("TMAT Row");
			headerList.add("TMAT Quantity");
			headerList.add("TMAT Price");
			headerList.add("Site Id");
			headerList.add("TMAT Ticket Created Date");
			headerList.add("TMAT Ticket Last Updated");
			headerList.add("TMAT Ticket Status");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet("RTFTMATSeatAllocation");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<Object[]> dataList = DAORegistry.getEventDAO().getRTFTMATSeatAllocationReport(artistName);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			Double orderTotal = 0.0, sumOrderTotal = 0.0, RTFQuantity = 0.0, sumRTFQuantity = 0.0;
			Double TMATQuantity = 0.0, sumTMATQuantity = 0.0, TMATPrice = 0.0, sumTMATPrice = 0.0;
			int j = 0;
			int rowCount = 1;
			
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[1].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
								
				if(dataObj[2] != null){					
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[4] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[4].toString()));
					cell.setCellStyle(cellStyle);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				

				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
								
				if(dataObj[10] != null){
					orderTotal = Util.getRoundedValue(Double.parseDouble(dataObj[10].toString()));
					sumOrderTotal = sumOrderTotal + orderTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(orderTotal));
				}else{
					sumOrderTotal = sumOrderTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){					
					RTFQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[12].toString()));
					sumRTFQuantity = sumRTFQuantity + RTFQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(RTFQuantity));
				}else{
					sumRTFQuantity = sumRTFQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
						
				if(dataObj[13] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[13].toString());
					
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[14].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					TMATQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[15].toString()));
					sumTMATQuantity = sumTMATQuantity + TMATQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(TMATQuantity));
				}else{
					sumTMATQuantity = sumTMATQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					TMATPrice = Util.getRoundedValue(Double.parseDouble(dataObj[16].toString()));
					sumTMATPrice = sumTMATPrice + TMATPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(TMATPrice));
				}else{
					sumTMATPrice = sumTMATPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[17] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[17].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[18] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[18].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[19] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[19].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[20] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[20].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_TMAT_SEAT_ALLOCATION_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	
	@RequestMapping({"/DownloadContestTicketCostReport"})
	public void downloadContestTicketCostReport(HttpServletRequest request, HttpServletResponse response){
		try{
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("Invoice No");
			headerList.add("Invoice Created Date");
			headerList.add("Invoice Status");
			headerList.add("Order No");
			headerList.add("Order Create Date");
			headerList.add("Po No");
			headerList.add("Po Created Date");
			headerList.add("Event Name");
			headerList.add("Event Date");
			headerList.add("Event Time");
			headerList.add("Venue Name");
			headerList.add("Venue city");
			headerList.add("Zone");
			headerList.add("Section");
			headerList.add("Row");
			headerList.add("Seat");
			headerList.add("Customer Name");
			headerList.add("Email");
			headerList.add("Phone No");
			headerList.add("Quantity");
			headerList.add("Is Bot?");
			headerList.add("Ticket Cost");
			headerList.add("Order Type");
			headerList.add("Platform");
			headerList.add("Payment Method");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet(Constants.RTF_CONTEST_TICKET_COST_REPORT);
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			
			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getContestTicketCostReportData(fromDate,toDate,sharedProperty);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0;
			int rowCount = 1;
			Double totalCost = 0.00;
			
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[1], j, rowhead, cellStyleWithHourMinute);j++;
				Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[3], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[4], j, rowhead, cellStyleWithHourMinute);j++;
				Util.getExcelIntegerCell(dataObj[5], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[6], j, rowhead, cellStyleWithHourMinute);j++;
				Util.getExcelStringCell(dataObj[7], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[8], j, rowhead, cellStyle);j++;
				Util.getExcelStringCell(dataObj[9], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[10], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[11], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[12], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[13], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[14], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[15], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[16], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[17], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[18], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[19], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[20], j, rowhead);j++;
				
				if(dataObj[21]!=null){
					totalCost = totalCost + Util.getRoundedValue(Double.parseDouble(dataObj[21].toString()));
				}
				
				Util.getExcelDecimalCell(dataObj[21], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[22], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[23], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[24], j, rowhead);j++;
				
			}
			
			ssSheet.createRow((int)rowCount); rowCount++;
			Row rowhead =   ssSheet.createRow((int)rowCount);
			Util.getExcelStringCell("Total Cost", 18, rowhead);
			Util.getExcelDecimalCell(totalCost, 19, rowhead);
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_CONTEST_TICKET_COST_REPORT+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	@RequestMapping({"/DownloadContestWinnerInvoiceReport"})
	public void downloadContestWinnerInvoiceReport(HttpServletRequest request, HttpServletResponse response){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Contest Id");
			headerList.add("Contest Name");
			headerList.add("Contest Start DateTime");
			headerList.add("Winner First Name");
			headerList.add("Winner Last Name");
			headerList.add("Winner UserId");
			headerList.add("Winner Email");
			headerList.add("Winner Phone");
			headerList.add("Is Bot?");
			headerList.add("tickets Won");
			headerList.add("Order Made?");
			headerList.add("Order Id");
			headerList.add("Event Name");
			headerList.add("Event Date");
			headerList.add("Event Time");
			headerList.add("Order Date");
			
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet(Constants.RTF_CONTEST_INVOICE_REPORT);
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			
			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getContestWinnerInvoiceReportData(sharedProperty);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0;
			int rowCount = 1;
			
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[2], j, rowhead, cellStyleWithHourMinute);j++;
				Util.getExcelStringCell(dataObj[3], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[4], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[5], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[6], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[7], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[8], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[9], j, rowhead);j++;
				if(dataObj[10]!=null){
					String o = dataObj[10].toString();
					if(o.equalsIgnoreCase("ACTIVE")){
						o = "NO";
						rowhead.createCell((int) j).setCellValue(o);
					}else if(o.equalsIgnoreCase("ORDERED")){
						o = "YES";
						rowhead.createCell((int) j).setCellValue(o);
					}else{
						rowhead.createCell((int) j).setCellValue("");
					}
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				Util.getExcelIntegerCell(dataObj[11], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[12], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[13], j, rowhead, cellStyle);j++;
				Util.getExcelStringCell(dataObj[14], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[15], j, rowhead, cellStyleWithHourMinute);j++;
				
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_CONTEST_INVOICE_REPORT+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	@RequestMapping({"/DownloadCustomerEarnedContestRewardDollarsReport"})
	public void downloadCustomerEarnedContestRewardDollarsReport(HttpServletRequest request, HttpServletResponse response){
		try{
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("Customer Id");
			headerList.add("Contest Participant");
			headerList.add("No. of Game Played");
			headerList.add("Total Contest Rewards Dollars");
			headerList.add("Right question Answered Count");
			headerList.add("Reward Dollar Earned by Questions");
			headerList.add("Reward Dollars Earned By Jackpot");
			headerList.add("Lives Used Count");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet("CustomerContestRewardDollar");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			
			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getContestRewardDollarsReportData(fromDate,toDate,sharedProperty);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0;
			int rowCount = 1;
			Double totalContestRewards = 0.00;
			Double totalQuestionRewards = 0.00;
			Double totalJackpotRewards = 0.00;
			
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[2], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[3], j, rowhead);j++;
				if(dataObj[3]!=null){
					totalContestRewards += Util.getRoundedValue(Double.parseDouble(dataObj[3].toString()));
				}
				Util.getExcelIntegerCell(dataObj[4], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[5], j, rowhead);j++;
				if(dataObj[5]!=null){
					totalQuestionRewards += Util.getRoundedValue(Double.parseDouble(dataObj[5].toString()));
				}
				Util.getExcelDecimalCell(dataObj[6], j, rowhead);j++;
				if(dataObj[6]!=null){
					totalJackpotRewards += Util.getRoundedValue(Double.parseDouble(dataObj[6].toString()));
				}
				Util.getExcelIntegerCell(dataObj[7], j, rowhead);j++;
				
			}
			
			ssSheet.createRow((int)rowCount); rowCount++;
			Row rowhead =   ssSheet.createRow((int)rowCount);
			Util.getExcelStringCell("Total Reward Dollars", 2, rowhead);
			Util.getExcelDecimalCell(totalContestRewards, 3, rowhead);
			Util.getExcelDecimalCell(totalQuestionRewards, 5, rowhead);
			Util.getExcelDecimalCell(totalJackpotRewards, 6, rowhead);
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_CUSTOMER_EARNED_CONTEST_REWARD_DOLLAR_REPORT+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	@RequestMapping({"/DownloadContestReport"})
	public void downloadContestReport(HttpServletRequest request, HttpServletResponse response){
		try{
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("Contest Id");
			headerList.add("Contest Name");
			headerList.add("Contest Date");
			headerList.add("Contest Time");
			headerList.add("Question Size");
			headerList.add("Customer Id");
			headerList.add("Customer Name");
			headerList.add("Customer Email");
			headerList.add("Customer Phone");
			headerList.add("Phone State");
			headerList.add("Customer IP Addr.");
			headerList.add("No. of Question Attempted");
			headerList.add("No Of question right answered");
			headerList.add("Reward Dollars Earned");
			headerList.add("Reward Dollars Earned By Sharing");
			headerList.add("Total Reward Point earned in Contest");
			headerList.add("Tickets Won");
			headerList.add("Lives Used Count");
			headerList.add("Contest Type");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet(Constants.RTF_CONTEST_REPORT);
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			
			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getContestReportData(fromDate,toDate,sharedProperty);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0;
			int rowCount = 1;
			
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				
				Util.getExcelStringCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[2], j, rowhead,cellStyle);j++;
				Util.getExcelStringCell(dataObj[3], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[4], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[5], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[6], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[7], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[8], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[9], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[10], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[11], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[12], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[13], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[14], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[15], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[16], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[17], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[18], j, rowhead);j++;
				
				
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_CONTEST_REPORT+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping({"/DownloadContestDetailedTrackingReport"})
	public void downloadContestDetailedTrackingReport(HttpServletRequest request, HttpServletResponse response){
		try{
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("Contest Name");
			headerList.add("Contest Date");
			headerList.add("Contest Time");
			headerList.add("Contest Day");
			headerList.add("Contest Week Day");
			headerList.add("Contest Month");
			headerList.add("Contest Year");
			headerList.add("Contest Category Type");
			headerList.add("Contest Category");
			headerList.add("Question Size");
			headerList.add("Customer Id");
			headerList.add("Customer Name");
			headerList.add("Customer Email");
			headerList.add("Customer Phone");
			headerList.add("Customer Phone State");
			headerList.add("Customer IP Addr");
			headerList.add("Platform");
			headerList.add("Customer Signup Date");
			headerList.add("No Of Question Attempted");
			headerList.add("No Of Correct Question");
			headerList.add("Reward Dollars Earned In Contest");
			headerList.add("Reward Dollars Earned by Sharing");
			headerList.add("Total Reward Dollars Earned");
			headerList.add("Ticket Won");
			headerList.add("Lives Used");
			headerList.add("Referral Customer Name");
			headerList.add("Referral Customer Signup Date");
			headerList.add("Referral Customer Phone");
			headerList.add("Contest Type");
			headerList.add("Contest Id");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet(Constants.RTF_CONTEST_DETAIL_TACKING_REPORT);
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			
			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getContestDetailedTrackingReportData(fromDate,toDate,sharedProperty);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0;
			int rowCount = 1;
			Double totalContestRewards = 0.00;
			Double totalSharingRewards = 0.00;
			Double totalRewards = 0.00;
			Integer totalTicketWon = 0;
			
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelStringCell(dataObj[0], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[1], j, rowhead,cellStyle);j++;
				Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[3], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[4], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[5], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[6], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[7], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[8], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[9], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[10], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[11], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[12], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[13], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[14], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[15], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[16], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[17], j, rowhead,cellStyleWithHourMinute);j++;
				Util.getExcelIntegerCell(dataObj[18], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[19], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[20], j, rowhead);j++;
				if(dataObj[20]!=null){
					totalContestRewards += Util.getRoundedValue(Double.parseDouble(dataObj[20].toString()));
				}
				Util.getExcelDecimalCell(dataObj[21], j, rowhead);j++;
				if(dataObj[21]!=null){
					totalSharingRewards += Util.getRoundedValue(Double.parseDouble(dataObj[21].toString()));
				}
				Util.getExcelDecimalCell(dataObj[22], j, rowhead);j++;
				if(dataObj[22]!=null){
					totalRewards += Util.getRoundedValue(Double.parseDouble(dataObj[22].toString()));
				}
				Util.getExcelIntegerCell(dataObj[23], j, rowhead);j++;
				if(dataObj[23]!=null){
					totalTicketWon += Integer.parseInt(dataObj[23].toString());
				}
				Util.getExcelIntegerCell(dataObj[24], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[25], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[26], j, rowhead,cellStyleWithHourMinute);j++;
				Util.getExcelStringCell(dataObj[27], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[28], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[29], j, rowhead);j++;
				
			}
			
			ssSheet.createRow((int)rowCount); rowCount++;
			Row rowhead =   ssSheet.createRow((int)rowCount);
			Util.getExcelStringCell("Total", 19, rowhead);
			Util.getExcelDecimalCell(totalContestRewards, 20, rowhead);
			Util.getExcelDecimalCell(totalSharingRewards, 21, rowhead);
			Util.getExcelDecimalCell(totalRewards, 22, rowhead);
			Util.getExcelDecimalCell(totalTicketWon, 23, rowhead);
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_CONTEST_DETAIL_TACKING_REPORT+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping({"/DownloadCustomerPurchaseRewardDollarsReport"})
	public void downloadCustomerPurchaseRewardDollarsReport(HttpServletRequest request, HttpServletResponse response){
		try{
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("Customer Name");
			headerList.add("Customer Email");
			headerList.add("Customer Phone");
			headerList.add("Payment Methods");
			headerList.add("Order Type");
			headerList.add("Invoice Id");
			headerList.add("Invoice Created Date");
			headerList.add("Invoice Status");
			headerList.add("Customer Order Id");
			headerList.add("Customer Order Created Date");
			headerList.add("Purchase Order Id");
			headerList.add("Purchase ORder Created Date");
			headerList.add("Event Name");
			headerList.add("Event Date");
			headerList.add("Event Time");
			headerList.add("Venue Name");
			headerList.add("Venue City");
			headerList.add("Zone");
			headerList.add("Quantity");
			headerList.add("Section");
			headerList.add("Row");
			headerList.add("Seat");
			headerList.add("Listing Price");
			headerList.add("Promotionla code Discount Amount");
			headerList.add("LoyalFan Discount Amount");
			headerList.add("Order Total");
			headerList.add("Cost");
			headerList.add("Ticket Cost");
			headerList.add("Other Cost");
			headerList.add("Reward Dollar Spent");
			headerList.add("Reward Dollar Balance");
			headerList.add("Net Total");
			headerList.add("Profit/Loss");
			headerList.add("Platform");
			headerList.add("Promotional Offer Used");
			headerList.add("Promocode");
			headerList.add("Promotional Discount");
			headerList.add("LoyalFan Discount");
			headerList.add("Referral Code");
			headerList.add("Referral Email");
			headerList.add("Discount Amount");
			headerList.add("Discount Amount Percentage");
			
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet("CustomerPurchaseOnRewardDollar");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			
		    List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getCustomerPurchaseRewardDollarsReportData(fromDate,toDate,sharedProperty);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0;
			int rowCount = 1;
			Double totalListingPrice = 0.00;
			Double totalDiscount = 0.00;
			Double totalLoyalFanDiscout = 0.00;
			Double totalOrderTotal = 0.00;
			Double totalCost = 0.00;
			Double totalTicketCost = 0.00;
			Double totalOtherCost = 0.00;
			Double totalRewardSpent = 0.00;
			Double totalRewardBalance = 0.00;
			Double totalNetTotal = 0.00;
			Double totalProfilLoss = 0.00;
			
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelStringCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[3], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[4], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[5], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[6], j, rowhead,cellStyleWithHourMinute);j++;
				Util.getExcelStringCell(dataObj[7], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[8], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[9], j, rowhead,cellStyleWithHourMinute);j++;
				Util.getExcelIntegerCell(dataObj[10], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[11], j, rowhead,cellStyleWithHourMinute);j++;
				Util.getExcelStringCell(dataObj[12], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[13], j, rowhead,cellStyle);j++;
				Util.getExcelStringCell(dataObj[14], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[15], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[16], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[17], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[18], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[19], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[20], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[21], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[22], j, rowhead);j++;
				if(dataObj[22]!=null){
					totalListingPrice += Util.getRoundedValue(Double.parseDouble(dataObj[22].toString()));
				}
				Util.getExcelDecimalCell(dataObj[23], j, rowhead);j++;
				if(dataObj[23]!=null){
					totalDiscount += Util.getRoundedValue(Double.parseDouble(dataObj[23].toString()));
				}
				Util.getExcelDecimalCell(dataObj[24], j, rowhead);j++;
				if(dataObj[24]!=null){
					totalLoyalFanDiscout += Util.getRoundedValue(Double.parseDouble(dataObj[24].toString()));
				}
				Util.getExcelDecimalCell(dataObj[25], j, rowhead);j++;
				if(dataObj[25]!=null){
					totalOrderTotal += Util.getRoundedValue(Double.parseDouble(dataObj[25].toString()));
				}
				Util.getExcelDecimalCell(dataObj[26], j, rowhead);j++;
				if(dataObj[26]!=null){
					totalCost += Util.getRoundedValue(Double.parseDouble(dataObj[26].toString()));
				}
				Util.getExcelDecimalCell(dataObj[27], j, rowhead);j++;
				if(dataObj[27]!=null){
					totalTicketCost += Util.getRoundedValue(Double.parseDouble(dataObj[27].toString()));
				}
				Util.getExcelDecimalCell(dataObj[28], j, rowhead);j++;
				if(dataObj[28]!=null){
					totalOtherCost += Util.getRoundedValue(Double.parseDouble(dataObj[28].toString()));
				}
				Util.getExcelDecimalCell(dataObj[29], j, rowhead);j++;
				if(dataObj[29]!=null){
					totalRewardSpent += Util.getRoundedValue(Double.parseDouble(dataObj[29].toString()));
				}
				Util.getExcelDecimalCell(dataObj[30], j, rowhead);j++;
				if(dataObj[30]!=null){
					totalRewardBalance += Util.getRoundedValue(Double.parseDouble(dataObj[30].toString()));
				}
				Util.getExcelDecimalCell(dataObj[31], j, rowhead);j++;
				if(dataObj[31]!=null){
					totalNetTotal += Util.getRoundedValue(Double.parseDouble(dataObj[31].toString()));
				}
				Util.getExcelDecimalCell(dataObj[32], j, rowhead);j++;
				if(dataObj[32]!=null){
					totalProfilLoss += Util.getRoundedValue(Double.parseDouble(dataObj[32].toString()));
				}
				Util.getExcelStringCell(dataObj[33], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[34], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[35], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[36], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[37], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[38], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[39], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[40], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[41], j, rowhead);j++;
			}
			
			ssSheet.createRow((int)rowCount); rowCount++;
			Row rowhead =   ssSheet.createRow((int)rowCount);
			Util.getExcelStringCell("Total", 21, rowhead);
			Util.getExcelDecimalCell(totalListingPrice, 22, rowhead);
			Util.getExcelDecimalCell(totalDiscount, 23, rowhead);
			Util.getExcelDecimalCell(totalLoyalFanDiscout, 24, rowhead);
			Util.getExcelDecimalCell(totalOrderTotal, 25, rowhead);
			Util.getExcelDecimalCell(totalCost, 26, rowhead);
			Util.getExcelDecimalCell(totalTicketCost, 27, rowhead);
			Util.getExcelDecimalCell(totalOtherCost, 28, rowhead);
			Util.getExcelDecimalCell(totalRewardSpent, 29, rowhead);
			Util.getExcelDecimalCell(totalRewardBalance, 30, rowhead);
			Util.getExcelDecimalCell(totalNetTotal, 31, rowhead);
			Util.getExcelDecimalCell(totalProfilLoss, 32, rowhead);
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_CUSTOMER_PURCHASE_REWARD_DOLLAR_REPORT+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	@RequestMapping({"/DownloadContestCustomerRefferalPercentageReport"})
	public void downloadContestCustomerRefferalPercentageReport(HttpServletRequest request, HttpServletResponse response){
		try{
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("No Of Contest Participants");
			headerList.add("No Of Customers Refeered");
			headerList.add("Referral Percentages");
			
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet(Constants.RTF_CONTEST_CUSTOMER_REFFERAL_PERCENTAGE_REPORT);
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			
			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getContestCustomerRefferalPercentageReportData(fromDate,toDate,sharedProperty);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0;
			int rowCount = 1;
			
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelStringCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[2], j, rowhead);j++;
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_CONTEST_CUSTOMER_REFFERAL_PERCENTAGE_REPORT+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	@RequestMapping({"/DownloadContestCustomerRefferalAverageReport"})
	public void downloadContestCustomerRefferalAverageReport(HttpServletRequest request, HttpServletResponse response){
		try{
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			
			
			FileInputStream inp  = new FileInputStream(URLUtil.REPORT_TEMPLATE_DIRECTORY+"referralAverage.xlsx");
			XSSFWorkbook workbook  = new XSSFWorkbook(inp);
			Sheet summary = workbook.getSheet("Summary");
			
			List<String> headerList1 = new ArrayList<String>();
			headerList1.add("Customer Id");
			headerList1.add("Customer Name");
			headerList1.add("Customer UserID");
			headerList1.add("SignUp Date");
			headerList1.add("Customer Email");
			headerList1.add("Customer Phone");
			
			//SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet1 = workbook.createSheet("Registered Customers");
			ExcelUtil.generateExcelHeaderRow(headerList1, ssSheet1);
			
			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getRegisteredCustomerReportData(fromDate, toDate, sharedProperty);
			
			//List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getContestCustomerRefferalAverageReportData(fromDate,toDate,sharedProperty);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0;
			int rowCount = 1;
			int regCustomerCount =0;
			
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet1.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[3], j, rowhead,cellStyleWithHourMinute);j++;
				Util.getExcelStringCell(dataObj[4], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[5], j, rowhead);j++;
				regCustomerCount++;
				
			}
			
			ssSheet1.createRow((int)rowCount);rowCount++;
			Row totalRowHead0 =   ssSheet1.createRow((int)rowCount);
			totalRowHead0.createCell(0).setCellValue("Total Customer");
			totalRowHead0.createCell(1).setCellValue(regCustomerCount);
			
			
			
			List<String> headerList2 = new ArrayList<String>();
			headerList2.add("Customer Id");
			headerList2.add("Customer Name");
			headerList2.add("No Of Referrals");
			
			Sheet ssSheet2 = workbook.createSheet("All customer Average Referral");
			ExcelUtil.generateExcelHeaderRow(headerList2, ssSheet2);
			Integer customerCount = 0;
			List<Object[]> dataList2 = QuizDAORegistry.getContestsDAO().getContestParticipantsAverageReportData(fromDate, toDate, sharedProperty);
			customerCount = QuizDAORegistry.getContestsDAO().getRegisteredAndPlayedCustomerCount(fromDate, toDate, sharedProperty);
			j = 0;
			rowCount = 1;
			
			Integer referralCount = 0;
			for(Object[] dataObj : dataList2){
				
				Row rowhead =   ssSheet2.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[2], j, rowhead);j++;
				if(dataObj[2] !=null){
					referralCount  += Integer.parseInt(dataObj[2].toString());
				}
				//customerCount++;
				
			}
			
			ssSheet2.createRow((int)rowCount);rowCount++;
			Row totalRowHead1 =   ssSheet2.createRow((int)rowCount);
			totalRowHead1.createCell(0).setCellValue("Total Customer");
			totalRowHead1.createCell(1).setCellValue(customerCount);
			totalRowHead1.createCell(2).setCellValue(referralCount);
			totalRowHead1.createCell(3).setCellValue("Average Referrals");
			totalRowHead1.createCell(4).setCellValue(Util.getRoundedValue((double)referralCount/customerCount));
				 
			//}
			
			List<String> headerList3 = new ArrayList<String>();
			headerList3.add("Customer Id");
			headerList3.add("Customer Name");
			headerList3.add("No Of Referrals");
			
			Sheet ssSheet3 = workbook.createSheet("Customer Referral Average");
			ExcelUtil.generateExcelHeaderRow(headerList3, ssSheet3);
			
			List<Object[]> dataList3 = QuizDAORegistry.getContestsDAO().getCustomerReferralAverageReportData(fromDate, toDate, sharedProperty);
			
			j = 0;
			rowCount = 1;
			Integer referralCount1 = 0;
			Integer customerCount1=0;
			for(Object[] dataObj : dataList3){
				
				Row rowhead =   ssSheet3.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[2], j, rowhead);j++;
				if(dataObj[2] !=null){
					referralCount1  += Integer.parseInt(dataObj[2].toString());
				}
				customerCount1++;
				
			}
			
			
			ssSheet3.createRow((int)rowCount);rowCount++;
			Row totalRowHead2 =   ssSheet3.createRow((int)rowCount);
			totalRowHead2.createCell(0).setCellValue("Total Customer Who Referred");
			totalRowHead2.createCell(1).setCellValue(customerCount1);
			totalRowHead2.createCell(2).setCellValue("Total Customer Who Were Referred");
			totalRowHead2.createCell(3).setCellValue(referralCount1);
			totalRowHead2.createCell(4).setCellValue("Average Referrals");
			totalRowHead2.createCell(5).setCellValue(Util.getRoundedValue((double)referralCount1/customerCount1));
			
			Row summaryRow = summary.getRow(1);
			if(summaryRow==null){
				summaryRow = summary.createRow(1);
			}
			Cell summaryCell = summaryRow.getCell(1);
			if(summaryCell==null){
				summaryCell = summaryRow.createCell(1);
			}
			summaryCell.setCellValue(customerCount);
			
			summaryCell = summaryRow.getCell(2);
			if(summaryCell==null){
				summaryCell = summaryRow.createCell(2);
			}
			summaryCell.setCellValue(referralCount);
			
			
			summaryRow = summary.getRow(4);
			if(summaryRow==null){
				summaryRow = summary.createRow(4);
			}
			summaryCell = summaryRow.getCell(2);
			if(summaryCell==null){
				summaryCell = summaryRow.createCell(2);
			}
			summaryCell.setCellValue(customerCount1);
			
			
			summaryRow = summary.getRow(16);
			if(summaryRow==null){
				summaryRow = summary.createRow(16);
			}
			summaryCell = summaryRow.getCell(0);
			if(summaryCell==null){
				summaryCell = summaryRow.createCell(0);
			}
			summaryCell.setCellValue(regCustomerCount);
			
			
			summaryRow = summary.getRow(19);
			if(summaryRow==null){
				summaryRow = summary.createRow(19);
			}
			summaryCell = summaryRow.getCell(0);
			if(summaryCell==null){
				summaryCell = summaryRow.createCell(0);
			}
			summaryCell.setCellValue(regCustomerCount);
			
			XSSFFormulaEvaluator.evaluateAllFormulaCells(workbook);
			
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_CONTEST_CUSTOMER_REFFERAL_AVERAGE_REPORT+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	
	@RequestMapping({"/DownloadContestCustomerRefferalAverageExcludingBotsReport"})
	public void downloadContestCustomerRefferalAverageExcludingBotsReport(HttpServletRequest request, HttpServletResponse response){
		try{
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			
			
			FileInputStream inp  = new FileInputStream(URLUtil.REPORT_TEMPLATE_DIRECTORY+"referralAverage.xlsx");
			XSSFWorkbook workbook  = new XSSFWorkbook(inp);
			Sheet summary = workbook.getSheet("Summary");
			
			List<String> headerList1 = new ArrayList<String>();
			headerList1.add("Customer Id");
			headerList1.add("Customer Name");
			headerList1.add("Customer UserID");
			headerList1.add("SignUp Date");
			headerList1.add("Customer Email");
			headerList1.add("Customer Phone");
			
			//SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet1 = workbook.createSheet("Registered Customers");
			ExcelUtil.generateExcelHeaderRow(headerList1, ssSheet1);
			
			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getRegisteredCustomerExcludingBotsReportData(fromDate, toDate, sharedProperty);
			
			//List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getContestCustomerRefferalAverageReportData(fromDate,toDate,sharedProperty);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0;
			int rowCount = 1;
			int regCustomerCount =0;
			
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet1.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[3], j, rowhead,cellStyleWithHourMinute);j++;
				Util.getExcelStringCell(dataObj[4], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[5], j, rowhead);j++;
				regCustomerCount++;
				
			}
			
			ssSheet1.createRow((int)rowCount);rowCount++;
			Row totalRowHead0 =   ssSheet1.createRow((int)rowCount);
			totalRowHead0.createCell(0).setCellValue("Total Customer");
			totalRowHead0.createCell(1).setCellValue(regCustomerCount);
			
			
			
			List<String> headerList2 = new ArrayList<String>();
			headerList2.add("Customer Id");
			headerList2.add("Customer Name");
			headerList2.add("No Of Referrals");
			
			Sheet ssSheet2 = workbook.createSheet("All customer Average Referral");
			ExcelUtil.generateExcelHeaderRow(headerList2, ssSheet2);
			
			List<Object[]> dataList2 = QuizDAORegistry.getContestsDAO().getContestParticipantsAverageexcludingBotsReportData(fromDate, toDate, sharedProperty);
			
			j = 0;
			rowCount = 1;
			Integer customerCount = 0;
			Integer referralCount = 0;
			for(Object[] dataObj : dataList2){
				
				Row rowhead =   ssSheet2.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[2], j, rowhead);j++;
				if(dataObj[2] !=null){
					referralCount  += Integer.parseInt(dataObj[2].toString());
				}
				customerCount++;
				
			}
			
			ssSheet2.createRow((int)rowCount);rowCount++;
			Row totalRowHead1 =   ssSheet2.createRow((int)rowCount);
			totalRowHead1.createCell(0).setCellValue("Total Customer");
			totalRowHead1.createCell(1).setCellValue(customerCount);
			totalRowHead1.createCell(2).setCellValue(referralCount);
			totalRowHead1.createCell(3).setCellValue("Average Referrals");
			totalRowHead1.createCell(4).setCellValue(Util.getRoundedValue((double)referralCount/customerCount));
				 
			//}
			
			List<String> headerList3 = new ArrayList<String>();
			headerList3.add("Customer Id");
			headerList3.add("Customer Name");
			headerList3.add("No Of Referrals");
			
			Sheet ssSheet3 = workbook.createSheet("Customer Referral Average");
			ExcelUtil.generateExcelHeaderRow(headerList3, ssSheet3);
			
			List<Object[]> dataList3 = QuizDAORegistry.getContestsDAO().getCustomerReferralAverageExcludingBotsReportData(fromDate, toDate, sharedProperty);
			
			j = 0;
			rowCount = 1;
			Integer referralCount1 = 0;
			Integer customerCount1=0;
			for(Object[] dataObj : dataList3){
				
				Row rowhead =   ssSheet3.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[2], j, rowhead);j++;
				if(dataObj[2] !=null){
					referralCount1  += Integer.parseInt(dataObj[2].toString());
				}
				customerCount1++;
				
			}
			
			
			ssSheet3.createRow((int)rowCount);rowCount++;
			Row totalRowHead2 =   ssSheet3.createRow((int)rowCount);
			totalRowHead2.createCell(0).setCellValue("Total Customer Who Referred");
			totalRowHead2.createCell(1).setCellValue(customerCount1);
			totalRowHead2.createCell(2).setCellValue("Total Customer Who Were Referred");
			totalRowHead2.createCell(3).setCellValue(referralCount1);
			totalRowHead2.createCell(4).setCellValue("Average Referrals");
			totalRowHead2.createCell(5).setCellValue(Util.getRoundedValue((double)referralCount1/customerCount1));
			
			Row summaryRow = summary.getRow(1);
			if(summaryRow==null){
				summaryRow = summary.createRow(1);
			}
			Cell summaryCell = summaryRow.getCell(1);
			if(summaryCell==null){
				summaryCell = summaryRow.createCell(1);
			}
			summaryCell.setCellValue(customerCount);
			
			summaryCell = summaryRow.getCell(2);
			if(summaryCell==null){
				summaryCell = summaryRow.createCell(2);
			}
			summaryCell.setCellValue(referralCount);
			
			
			summaryRow = summary.getRow(4);
			if(summaryRow==null){
				summaryRow = summary.createRow(4);
			}
			summaryCell = summaryRow.getCell(2);
			if(summaryCell==null){
				summaryCell = summaryRow.createCell(2);
			}
			summaryCell.setCellValue(customerCount1);
			
			
			summaryRow = summary.getRow(16);
			if(summaryRow==null){
				summaryRow = summary.createRow(16);
			}
			summaryCell = summaryRow.getCell(0);
			if(summaryCell==null){
				summaryCell = summaryRow.createCell(0);
			}
			summaryCell.setCellValue(regCustomerCount);
			
			
			summaryRow = summary.getRow(19);
			if(summaryRow==null){
				summaryRow = summary.createRow(19);
			}
			summaryCell = summaryRow.getCell(0);
			if(summaryCell==null){
				summaryCell = summaryRow.createCell(0);
			}
			summaryCell.setCellValue(regCustomerCount);
			
			XSSFFormulaEvaluator.evaluateAllFormulaCells(workbook);
			
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_CONTEST_CUSTOMER_REFFERAL_AVERAGE_REPORT_EXCLUDING_BOTS+"." +Constants.EXCEL_EXTENSION); //Shiva Modified
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	
	
	@RequestMapping({"/DownloadRewardDollarSpentOnContestAndCustomer"})
	public void downloadRewardDollarSpentOnContestAndCustomer(HttpServletRequest request, HttpServletResponse response){
		try{
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			
			
			List<String> headerList1 = new ArrayList<String>();
			headerList1.add("Contest Id");
			headerList1.add("Contest Name");
			headerList1.add("Contest DateTime");
			headerList1.add("Reward Dollars Spent");
			
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet1 = workbook.createSheet("RewardDollarSpentOnContest");
			ExcelUtil.generateExcelHeaderRow(headerList1, ssSheet1);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			List<Object[]> dataList1 = QuizDAORegistry.getContestsDAO().getContesWiseRewardDollarsReportData(fromDate, toDate, sharedProperty);
			int j = 0;
			int rowCount = 1;
			Double totalRewards = 0.00;
			int cnt=0;
			
			for(Object[] dataObj : dataList1){
				Row rowhead =   ssSheet1.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[2], j, rowhead,cellStyleWithHourMinute);j++;
				Util.getExcelDecimalCell(dataObj[3], j, rowhead);j++;
				if(dataObj[3] !=null){
					totalRewards += Double.parseDouble(dataObj[3].toString());
				}
				cnt++;
			}
			
			ssSheet1.createRow((int)rowCount); rowCount++;
			Row rowhead1 =   ssSheet1.createRow((int)rowCount);
			Util.getExcelStringCell("Total Rewards", 2, rowhead1);
			Util.getExcelDecimalCell(totalRewards, 3, rowhead1);
			Util.getExcelStringCell("Average Reward per Game", 4, rowhead1);
			Util.getExcelDecimalCell(totalRewards/cnt, 5, rowhead1);
			
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("Contest Participants");
			headerList.add("No Of Contest Played");
			headerList.add("Total Contest Rewards");
			headerList.add("Average Contest Rewards");
			
			
			Sheet ssSheet = workbook.createSheet("RewardDollarSpentOnCustomer");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			
			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getCustomerWiseRewardDollarsReportData(fromDate,toDate,sharedProperty);
			
			j = 0;
			rowCount = 1;
			Double totalContestRewards = 0.00;
			Double totalAvgRewards = 0.00;
			int cnt1=0;
			
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelStringCell(dataObj[0], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[1], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[2], j, rowhead);j++;
				if(dataObj[2]!=null){
					totalContestRewards += Util.getRoundedValue(Double.parseDouble(dataObj[2].toString()));
				}
				Util.getExcelDecimalCell(dataObj[3], j, rowhead);j++;
				if(dataObj[3]!=null){
					totalAvgRewards += Util.getRoundedValue(Double.parseDouble(dataObj[3].toString()));
				}
				cnt1++;
			}
			
			ssSheet.createRow((int)rowCount); rowCount++;
			Row rowhead =   ssSheet.createRow((int)rowCount);
			Util.getExcelStringCell("Total Reward", 1, rowhead);
			Util.getExcelDecimalCell(totalContestRewards, 2, rowhead);
			Util.getExcelStringCell("Average Reward per customer", 3, rowhead);
			Util.getExcelDecimalCell(totalContestRewards/cnt1, 4, rowhead);
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_REWARD_DOLLAR_SPENT_ON_CONTEST_AND_CUSTOMER+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	@RequestMapping({"/DownloadRefereeReferralsDailyReport"})
	public void downloadReferreReferralsReport(HttpServletRequest request, HttpServletResponse response){
		try{
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("Referree Id");
			headerList.add("Referree UserID");
			headerList.add("Refer To Customer Id");
			headerList.add("Refer To Customer UserID");
			headerList.add("Code Apllied Date");
			headerList.add("Refer To Signup Date");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			Sheet ssSheet1 = workbook.createSheet("Total");
			
			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getReferreRefferalsReportData(fromDate,toDate,sharedProperty);
			SortedMap<String,List<Object[]>> map = new TreeMap<String,List<Object[]>>(Collections.reverseOrder());
			for(Object[] dataObj : dataList){
				if(map.get(dataObj[9].toString()) == null){
					List<Object[]> list = new ArrayList<Object[]>();
					list.add(dataObj);
					map.put(dataObj[9].toString(),list);
				}else{
					map.get(dataObj[9].toString()).add(dataObj);
				}
			}
			boolean isRecordsFound=false;
			for(String key : map.keySet()){
				List<Object[]> list = map.get(key);
				if(list!=null){
					int j = 0;
					int rowCount = 1;
					String totalReferree=null;
					String totalReferrals = null;
					String totalRegisteredCustomer = null;
					Sheet ssSheet = workbook.createSheet(key);
					ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
					for(Object[] dataObj : list){
						Row rowhead =   ssSheet.createRow((int)rowCount);				
						rowCount++;
						j = 0;
						
						Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
						Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
						Util.getExcelIntegerCell(dataObj[2], j, rowhead);j++;
						Util.getExcelStringCell(dataObj[3], j, rowhead);j++;
						Util.getExcelDateCell(dataObj[4], j, rowhead,cellStyleWithHourMinute);j++;
						Util.getExcelDateCell(dataObj[5], j, rowhead,cellStyleWithHourMinute);j++;
						totalRegisteredCustomer =  dataObj[6].toString();
						totalReferree = dataObj[7].toString();
						totalReferrals = dataObj[8].toString();
						isRecordsFound = true;
					}
					ssSheet.createRow((int)rowCount);rowCount++;
					Row rowhead1 =   ssSheet.createRow((int)rowCount);
					Util.getExcelStringCell("Unique Referee ",3, rowhead1);
					Util.getExcelIntegerCell(totalReferree, 4, rowhead1);
					rowCount++;
					
					Row rowhead2 =   ssSheet.createRow((int)rowCount);
					Util.getExcelStringCell("Total Refferrals ",3, rowhead2);
					Util.getExcelIntegerCell(totalReferrals, 4, rowhead2);
					rowCount++;rowCount++;
					
					Row rowhead3 =   ssSheet.createRow((int)rowCount);
					Util.getExcelStringCell("No Of Registered Customers",3, rowhead3);
					Util.getExcelDecimalCell(totalRegisteredCustomer, 4, rowhead3);
					rowCount++;rowCount++;
					
					Double Average = Util.roundOffDouble(Double.parseDouble(totalReferrals)/Double.parseDouble(totalRegisteredCustomer));
					
					Row rowhead4 =   ssSheet.createRow((int)rowCount);
					Util.getExcelStringCell("Average ",3, rowhead4);
					Util.getExcelDecimalCell(Average, 4, rowhead4);
					rowCount++;
					
					Row rowhead5 =   ssSheet.createRow((int)rowCount);
					Util.getExcelStringCell("Average Percentage ",3, rowhead5);
					Util.getExcelDecimalCell(Average*100, 4, rowhead5);
					
				}
			}
			
			
			List<String> headerList1 = new ArrayList<String>();
			headerList1.add("Date");
			headerList1.add("No Of Referrals");
			ExcelUtil.generateExcelHeaderRow(headerList1, ssSheet1);
			if(isRecordsFound){
				int rowCount=1;
				int j = 0;
				int totalCustomer=0;
				int grandTotalReferee = 0;
				int grandTotalReferral = 0;
				for(String key : map.keySet()){
					List<Object[]> list = map.get(key);
					Row rowhead =   ssSheet1.createRow((int)rowCount);				
					rowCount++;
					j = 0;
					String totalReferrals="";
					String totalRegisteredCustomer="";
					String totalReferree="";
					if(list!=null){
						for(Object[] dataObj : list){
							totalRegisteredCustomer =  dataObj[6].toString();
							totalReferree = dataObj[7].toString();
							totalReferrals = dataObj[8].toString();
							
							totalCustomer += Integer.parseInt(totalRegisteredCustomer);
							grandTotalReferee += Integer.parseInt(totalReferree);
							grandTotalReferral += Integer.parseInt(totalReferrals);
							
							break;
						}
					}
					Util.getExcelDateCell(key, j, rowhead, cellStyle);j++;
					Util.getExcelIntegerCell(totalReferrals, j, rowhead);
				}
				
				
				Double average = (Double.parseDouble(String.valueOf(grandTotalReferral))/totalCustomer);
				
				
				rowCount++;rowCount++;
				for(int i=rowCount;i<=rowCount+4;i++){
					Row summaryRow1 = ssSheet1.getRow(i);
					if(summaryRow1==null){
						summaryRow1 = ssSheet1.createRow(i);
					}
					Cell summaryCell1 = summaryRow1.getCell(5);
					if(summaryCell1==null){
						summaryCell1 = summaryRow1.createCell(5);
					}
					Cell summaryCell2 = summaryRow1.getCell(6);
					if(summaryCell2==null){
						summaryCell2 = summaryRow1.createCell(6);
					}
					if(i==rowCount){
						summaryCell1.setCellValue("Total Primary Customers");
						summaryCell2.setCellValue(grandTotalReferee);
					}else if(i==(rowCount+1)){
						summaryCell1.setCellValue("Total Referrals");
						summaryCell2.setCellValue(grandTotalReferral);
					}else if(i==(rowCount+2)){
						summaryCell1.setCellValue("Total Registered Customers");
						summaryCell2.setCellValue(totalCustomer);
					}else if(i==(rowCount+3)){
						summaryCell1.setCellValue("Average");
						summaryCell2.setCellValue(average);
					}else if(i==(rowCount+4)){
						summaryCell1.setCellValue("Average Percentage");
						summaryCell2.setCellValue((average*100));
					}
				}
			}else{
				Row rowhead =   ssSheet1.createRow(5);
				Util.getExcelStringCell("No Records found within given date ranges",5, rowhead);
			}
			
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_CONTEST_REFERRE_REFERRALS_REPORT+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	@RequestMapping({"/DownloadCustomerReferredByReferredToReport"})
	public void downloadCustomerReferredByReferredToReport(HttpServletRequest request, HttpServletResponse response){
		try{
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			
			/*List<String> headerList = new ArrayList<String>();
			headerList.add("Referred By Customer Id");
			headerList.add("Referred By Customer Name");
			headerList.add("Referred By UserId");
			headerList.add("Referred To Customer Id");
			headerList.add("Referred To Customer Name");
			headerList.add("Referred To UserId");
			headerList.add("Referred Count");
			
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet(Constants.RTF_CONTEST_REFERRED_BY_REFERRED_TO_REPORT);
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			
			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getCustomerReferredByReferredToReportData(fromDate,toDate,sharedProperty);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0;
			int rowCount = 1;
			Integer totalreferredCount =0;
			String customerIdStr = "";
			for(Object[] dataObj : dataList){
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				if(dataObj[0] !=null && !dataObj[0].toString().equals(customerIdStr)){
					Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
					Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
					Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
				}else{
					Util.getExcelIntegerCell(null, j, rowhead);j++;
					Util.getExcelStringCell(null, j, rowhead);j++;
					Util.getExcelStringCell(null, j, rowhead);j++;
				}
				
				
				Util.getExcelIntegerCell(dataObj[3], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[4], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[5], j, rowhead);j++;
				if(dataObj[0] !=null && !dataObj[0].toString().equals(customerIdStr)){
					Util.getExcelIntegerCell(dataObj[6], j, rowhead);j++;
					totalreferredCount += Integer.parseInt(dataObj[6].toString());
				}else{
					Util.getExcelIntegerCell(null, j, rowhead);j++;
				}
				
				customerIdStr =  dataObj[0] !=null ? dataObj[0].toString():"";
			}
			
			ssSheet.createRow((int)rowCount); rowCount++;
			Row rowhead =   ssSheet.createRow((int)rowCount);
			Util.getExcelStringCell("Total Reffered ",5, rowhead);
			Util.getExcelIntegerCell(totalreferredCount, 6, rowhead);*/
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("Referree Id");
			headerList.add("Referree UserID");
			headerList.add("Refer To Customer Id");
			headerList.add("Refer To Customer UserID");
			headerList.add("Code Apllied Date");
			headerList.add("Refer To Signup Date");
			
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet("Referred By Customer List");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			
			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getCustomerReferredByReferredToReportData(fromDate,toDate,sharedProperty);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0;
			int rowCount = 1;
			Integer totalreferredCount =0;
			for(Object[] dataObj : dataList){
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[2], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[3], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[4], j, rowhead,cellStyleWithHourMinute);j++;
				Util.getExcelDateCell(dataObj[5], j, rowhead,cellStyleWithHourMinute);j++;
				totalreferredCount++;
			}
			
			ssSheet.createRow((int)rowCount); rowCount++;
			Row rowhead1 =   ssSheet.createRow((int)rowCount);
			Util.getExcelStringCell("Total Refferrals ",3, rowhead1);
			Util.getExcelIntegerCell(totalreferredCount, 4, rowhead1);
			
			
			
			
			
			List<String> headerList1 = new ArrayList<String>();
			headerList1.add("Referred By Customer Id");
			headerList1.add("Referred By Customer UserId");
			headerList1.add("Referred Count");
			
			
			Sheet ssSheet1 = workbook.createSheet("Referred By Customer Count");
			ExcelUtil.generateExcelHeaderRow(headerList1, ssSheet1);
			
			List<Object[]> dataList1 = QuizDAORegistry.getContestsDAO().getCustomerReferredByReferredToReportDataOld(fromDate,toDate,sharedProperty);
			
			j = 0;
			rowCount = 1;
			Integer totalreferredCount1 =0;
			Integer totalCount = 0;
			
			for(Object[] dataObj : dataList1){
				Row rowhead =   ssSheet1.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[2], j, rowhead);j++;
				if(dataObj[2]!=null){
					totalreferredCount1 += Integer.parseInt(dataObj[2].toString());
				}
				totalCount++;
			}
			
			Util.getExcelStringCell("Unique Referee ",0, rowhead1);
			Util.getExcelIntegerCell(totalCount, 1, rowhead1);
			
			ssSheet1.createRow((int)rowCount); rowCount++;
			Row rowhead2 =   ssSheet1.createRow((int)rowCount);
			Util.getExcelStringCell("Unique Referee ",0, rowhead2);
			Util.getExcelIntegerCell(totalCount, 1, rowhead2);
			Util.getExcelStringCell("Total Refferrals",2, rowhead2);
			Util.getExcelIntegerCell(totalreferredCount1, 3, rowhead2);
			
			
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_CONTEST_REFERRED_BY_REFERRED_TO_REPORT+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	@RequestMapping({"/DownloadContestQuestionwiseUserAnswerReport"})
	public void downloadQuestionWiseUserAnswerCountReport(HttpServletRequest request, HttpServletResponse response){
		try{
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("Contest Id");
			headerList.add("Contest Name");
			headerList.add("Contest Start Date Time");
			headerList.add("Total Participants");
			headerList.add("No Of participants Attempted");
			headerList.add("1st Question Attempted count");
			headerList.add("1st Question Aswered count");
			headerList.add("1st Question Attempted Percentage");
			headerList.add("1st Question Aswered Percentage");
			headerList.add("2nd Question Attempted count");
			headerList.add("2nd Question Aswered count");
			headerList.add("2nd Question Attempted Percentage");
			headerList.add("2nd Question Aswered Percentage");
			headerList.add("3rd Question Attempted count");
			headerList.add("3rd Question Aswered count");
			headerList.add("3rd Question Attempted Percentage");
			headerList.add("3rd Question Aswered Percentage");
			headerList.add("4th Question Attempted count");
			headerList.add("4th Question Aswered count");
			headerList.add("4th Question Attempted Percentage");
			headerList.add("4th Question Aswered Percentage");
			headerList.add("5th Question Attempted count");
			headerList.add("5th Question Aswered count");
			headerList.add("5th Question Attempted Percentage");
			headerList.add("5th Question Aswered Percentage");
			headerList.add("6th Question Attempted count");
			headerList.add("6th Question Aswered count");
			headerList.add("6th Question Attempted Percentage");
			headerList.add("6th Question Aswered Percentage");
			headerList.add("7th Question Attempted count");
			headerList.add("7th Question Aswered count");
			headerList.add("7th Question Attempted Percentage");
			headerList.add("7th Question Aswered Percentage");
			headerList.add("8th Question Attempted count");
			headerList.add("8th Question Aswered count");
			headerList.add("8th Question Attempted Percentage");
			headerList.add("8th Question Aswered Percentage");
			headerList.add("9th Question Attempted count");
			headerList.add("9th Question Aswered count");
			headerList.add("9th Question Attempted Percentage");
			headerList.add("9th Question Aswered Percentage");
			headerList.add("10th Question Attempted count");
			headerList.add("10th Question Aswered count");
			headerList.add("10th Question Attempted Percentage");
			headerList.add("10th Question Aswered Percentage");
			headerList.add("Total Question Answered");
			headerList.add("Total Question Attempted");
			headerList.add("Overall Aswer Percentage");
			
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet(Constants.RTF_CONTEST_QUESTION_WISE_USERCOUNT);
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			
			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getQuestionWiseUserAnswerCountReportData(fromDate, toDate, sharedProperty);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0;
			int rowCount = 1;
			
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[2], j, rowhead,cellStyleWithHourMinute);j++;
				Util.getExcelIntegerCell(dataObj[3], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[4], j, rowhead);j++;
				
				Util.getExcelIntegerCell(dataObj[5], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[6], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[7], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[8], j, rowhead);j++;
				
				Util.getExcelIntegerCell(dataObj[9], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[10], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[11], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[12], j, rowhead);j++;
				
				Util.getExcelIntegerCell(dataObj[13], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[14], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[15], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[16], j, rowhead);j++;
				
				Util.getExcelIntegerCell(dataObj[17], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[18], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[19], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[20], j, rowhead);j++;
				
				Util.getExcelIntegerCell(dataObj[21], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[22], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[23], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[24], j, rowhead);j++;
				
				Util.getExcelIntegerCell(dataObj[25], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[26], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[27], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[28], j, rowhead);j++;
				
				Util.getExcelIntegerCell(dataObj[29], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[30], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[31], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[32], j, rowhead);j++;
				
				Util.getExcelIntegerCell(dataObj[33], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[34], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[35], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[36], j, rowhead);j++;
				
				Util.getExcelIntegerCell(dataObj[37], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[38], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[39], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[40], j, rowhead);j++;
				
				Util.getExcelIntegerCell(dataObj[41], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[42], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[43], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[44], j, rowhead);j++;
				
				Util.getExcelIntegerCell(dataObj[45], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[46], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[47], j, rowhead);j++;
				
				
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_CONTEST_QUESTION_WISE_USERCOUNT+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping({"/DownloadCustomerLivesEarnedReport"})
	public void downloadCustomerLiveEarnedReport(HttpServletRequest request, HttpServletResponse response){
		try{
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			
			FileInputStream inp  = new FileInputStream(URLUtil.REPORT_TEMPLATE_DIRECTORY+"earnLiveTemplate.xlsx");
			XSSFWorkbook workbook  = new XSSFWorkbook(inp);
			Sheet summary = workbook.getSheet("Summary");
			
			// Sheet1 Resitered Users
			
			List<String> headerList1 = new ArrayList<String>();
			headerList1.add("Customer Id");
			headerList1.add("Customer Name");
			headerList1.add("Customer UserID");
			headerList1.add("SignUp Date");
			headerList1.add("Customer Email");
			headerList1.add("Customer Phone");
			
			//SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet1 = workbook.createSheet("Registered Customers");
			ExcelUtil.generateExcelHeaderRow(headerList1, ssSheet1);
			
			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getRegisteredCustomerReportData(fromDate, toDate,sharedProperty);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0;
			int rowCount = 1;
			int totalCustomerCount = 0;
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet1.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[3], j, rowhead,cellStyleWithHourMinute);j++;
				Util.getExcelStringCell(dataObj[4], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[5], j, rowhead);j++;
				totalCustomerCount++;
				
			}
			
			ssSheet1.createRow((int)rowCount); rowCount++;
			Row rowhead1 =   ssSheet1.createRow((int)rowCount);
			Util.getExcelStringCell("Total Customers",4, rowhead1);
			Util.getExcelIntegerCell(totalCustomerCount, 5, rowhead1);
			
			// Sheet2 Earned Lives
			
			List<String> headerList2 = new ArrayList<String>();
			headerList2.add("Customer Id");
			headerList2.add("Customer Name");
			headerList2.add("No Of Lives Earned by Referrals");
			
			
			Sheet ssSheet2 = workbook.createSheet("Customer Lives Earned");
			ExcelUtil.generateExcelHeaderRow(headerList2, ssSheet2);
			
			List<Object[]> dataList2 = QuizDAORegistry.getContestsDAO().getCustomerLivesEarnedByReferralReportData(fromDate, toDate, sharedProperty);
			
			j = 0;
			rowCount = 1;
			Integer customerCount = 0;
			Integer livesCount = 0;
			
			for(Object[] dataObj : dataList2){
				
				Row rowhead =   ssSheet2.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[2], j, rowhead);j++;
				if(dataObj[2]!=null){
					livesCount += Integer.parseInt(dataObj[2].toString());
				}
				customerCount++;
			}
			
			ssSheet2.createRow((int)rowCount); rowCount++;
			Row rowhead2 =   ssSheet2.createRow((int)rowCount);
			Util.getExcelStringCell("Total Customer Who Earned Lives",0, rowhead2);
			Util.getExcelIntegerCell(customerCount, 1, rowhead2);
			Util.getExcelStringCell("Total Earned Lives ",2, rowhead2);
			Util.getExcelIntegerCell(livesCount, 3, rowhead2);
			Util.getExcelStringCell("Avg. Earned Lives ",4, rowhead2);
			Util.getExcelDecimalCell(Double.parseDouble(livesCount.toString())/customerCount, 5, rowhead2);
			
			// Sheet3 Referrer Lives Used
			
			List<String> headerList3 = new ArrayList<String>();
			headerList3.add("Contest Id");
			headerList3.add("Contest Name");
			headerList3.add("Contest Date");
			headerList3.add("No Of Lives Used per Game");
			headerList3.add("Available Customer WHo Referred");
			headerList3.add("Average of Lives used Per Game");
			
			Sheet ssSheet3 = workbook.createSheet("Referrer Live Used");
			ExcelUtil.generateExcelHeaderRow(headerList3, ssSheet3);
			
			List<Object[]> dataList3 = QuizDAORegistry.getContestsDAO().getCustomerLivesUsedContestWiseReportData(fromDate, toDate, sharedProperty);
			
			j = 0;
			rowCount = 1;
			int totalLivesUsed = 0;
			Double averageLivesTotal = 0.0;
			int totalGame = 0;
			
			for(Object[] dataObj : dataList3){
				
				Row rowhead =   ssSheet3.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[3], j, rowhead);j++;
				if(dataObj[3]!=null){
					totalLivesUsed += Integer.parseInt(dataObj[3].toString());
				}
				Util.getExcelIntegerCell(dataObj[4], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[5], j, rowhead);j++;
				if(dataObj[5]!=null){
					averageLivesTotal += Double.parseDouble(dataObj[5].toString());
				}
				totalGame++;
			}
			
			
			ssSheet3.createRow((int)rowCount); rowCount++;
			Row rowhead3 =   ssSheet3.createRow((int)rowCount);
			Util.getExcelStringCell("Total No. of Lives Used",2, rowhead3);
			Util.getExcelIntegerCell(totalLivesUsed, 3, rowhead3);
			Util.getExcelStringCell("Average Lives Used Per Game",4, rowhead3);
			Util.getExcelDecimalCell(Util.roundOffDouble(averageLivesTotal/totalGame), 5, rowhead3);
			
			// Sheet4 Lives Used By All
			
			List<String> headerList4 = new ArrayList<String>();
			headerList4.add("Contest Id");
			headerList4.add("Contest Name");
			headerList4.add("Contest Date");
			headerList4.add("No Of Lives Used per Game");
			headerList4.add("Available Customer Who Referred");
			headerList4.add("Average of Lives used Per Game");
			
			Sheet ssSheet4 = workbook.createSheet("Lives Used By All");
			ExcelUtil.generateExcelHeaderRow(headerList4, ssSheet4);
			
			List<Object[]> dataList4 = QuizDAORegistry.getContestsDAO().getCustomerLivesUsedAllReportData(fromDate, toDate, sharedProperty);
			
			j = 0;
			rowCount = 1;
			int totalLivesUsed1 = 0;
			Double averageLivesTotal1 = 0.0;
			int totalGame1 = 0;
			
			for(Object[] dataObj : dataList4){
				
				Row rowhead =   ssSheet4.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[3], j, rowhead);j++;
				if(dataObj[3]!=null){
					totalLivesUsed1 += Integer.parseInt(dataObj[3].toString());
				}
				Util.getExcelIntegerCell(dataObj[4], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[5], j, rowhead);j++;
				if(dataObj[5]!=null){
					averageLivesTotal1 += Double.parseDouble(dataObj[5].toString());
				}
				totalGame1++;
				
			}
			
			ssSheet4.createRow((int)rowCount); rowCount++;
			Row rowhead4 =   ssSheet4.createRow((int)rowCount);
			Util.getExcelStringCell("Total No. of Lives Used",2, rowhead4);
			Util.getExcelIntegerCell(totalLivesUsed1, 3, rowhead4);
			Util.getExcelStringCell("Average Lives Used Per Game",4, rowhead4);
			Util.getExcelDecimalCell(Util.roundOffDouble(averageLivesTotal1/totalGame1), 5, rowhead4);
			
			// Sheet5 Lives Used BreakDown
			
			List<String> headerList5 = new ArrayList<String>();
			headerList5.add("Contest Id");
			headerList5.add("Contest Name");
			headerList5.add("Contest Date");
			headerList5.add("Question No");
			headerList5.add("No Of Lives Used");
			
			Sheet ssSheet5 = workbook.createSheet("Lives Used Breakdown");
			ExcelUtil.generateExcelHeaderRow(headerList5, ssSheet5);
			
			List<Object[]> dataList5 = QuizDAORegistry.getContestsDAO().getCustomerLivesUsedBreackDownReportData(fromDate, toDate, sharedProperty);
			
			j = 0;
			rowCount = 1;
			int totalLivesUsed5 = 0;
			
			for(Object[] dataObj : dataList5){
				
				Row rowhead =   ssSheet5.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[3], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[4], j, rowhead);j++;
				if(dataObj[4]!=null){
					totalLivesUsed5 += Integer.parseInt(dataObj[4].toString());
				}
				
			}
			
			ssSheet5.createRow((int)rowCount); rowCount++;
			Row rowhead5 =   ssSheet5.createRow((int)rowCount);
			Util.getExcelStringCell("Total No. of Lives Used",3, rowhead5);
			Util.getExcelIntegerCell(totalLivesUsed5, 4, rowhead5);
			
			
			// Sheet6 Lives Used Percentage BreakDown
			
			List<String> headerList6 = new ArrayList<String>();
			headerList6.add("Contest Id");
			headerList6.add("Contest Name");
			headerList6.add("Contest Date");
			headerList6.add("Question No");
			headerList6.add("No Of Customer Who Attempted");
			headerList6.add("No Of Lives Used");
			headerList6.add("Percentage of Customer Used Lives");
			
			Sheet ssSheet6 = workbook.createSheet("Lives Used Percentage Breakdown");
			ExcelUtil.generateExcelHeaderRow(headerList6, ssSheet6);
			
			List<Object[]> dataList6 = QuizDAORegistry.getContestsDAO().getCustomerLivesUsedPercentageBreackDownReportData(fromDate, toDate, sharedProperty);
			
			j = 0;
			rowCount = 1;
			
			for(Object[] dataObj : dataList6){
				
				Row rowhead =   ssSheet6.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[3], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[4], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[5], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[6], j, rowhead);j++;
				
			}
			
			// Summary Sheet filling data.
			for(int i=1;i<=7;i++){
				if(i==4){
					continue;
				}
				Row summaryRow1 = summary.getRow(i);
				if(summaryRow1==null){
					summaryRow1 = summary.createRow(i);
				}
				Cell summaryCell1 = summaryRow1.getCell(2);
				if(summaryCell1==null){
					summaryCell1 = summaryRow1.createCell(2);
				}
				
				if(i==1){
					summaryCell1.setCellValue(totalCustomerCount);
				}else if(i==2){
					summaryCell1.setCellValue(customerCount);
				}else if(i==3){
					summaryCell1.setCellValue(livesCount);
				}else if(i==5){
					summaryCell1.setCellValue(totalLivesUsed);
				}else if(i==6){
					summaryCell1.setCellValue(totalLivesUsed1);
				}
			}
			
			XSSFFormulaEvaluator.evaluateAllFormulaCells(workbook);
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_CONTEST_LIVES_REPORT+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	@RequestMapping({"/DownloadAvgQuestionAnsweredByCustomerReport"})
	public void downloadAvgQuestionAnsweredByCustomerReport(HttpServletRequest request, HttpServletResponse response){
		try{
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("Customer Id");
			headerList.add("Customer Name");
			headerList.add("Customer UserID");
			headerList.add("Customer Email");
			headerList.add("No oF Games Played By Answering");
			headerList.add("Total No of Questions from Played Games");
			headerList.add("Total No of Questions Answered");
			headerList.add("Average Percentage Answered by Customer");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet(Constants.RTF_AVG_QUESTION_ASWERED_BY_CUSTOMER);
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			
			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getAvgQuestionAsweredByCustomerReportData(fromDate, toDate, sharedProperty);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0;
			int rowCount = 1;
			int totalCustomerCount = 0;
			int totalGames = 0;
			int totalQuestions  =0;
			int totalAnsweredQuestions=0;
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[3], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[4], j, rowhead);j++;
				if(dataObj[4]!=null){
					totalGames += Integer.parseInt(dataObj[4].toString());
				}
				Util.getExcelIntegerCell(dataObj[5], j, rowhead);j++;
				if(dataObj[5]!=null){
					totalQuestions += Integer.parseInt(dataObj[5].toString());
				}
				Util.getExcelIntegerCell(dataObj[6], j, rowhead);j++;
				if(dataObj[6]!=null){
					totalAnsweredQuestions += Integer.parseInt(dataObj[6].toString());
				}
				Util.getExcelDecimalCell(dataObj[7], j, rowhead);j++;
				totalCustomerCount++;
				
			}
			
			ssSheet.createRow((int)rowCount); rowCount++;
			Row rowhead1 =   ssSheet.createRow((int)rowCount);
			Util.getExcelStringCell("(A1) Total No OF Customer who have played Game atleast by answering one question:",0, rowhead1);
			Util.getExcelIntegerCell(totalCustomerCount, 1, rowhead1);
			
			rowCount++;
			Row rowhead2 =   ssSheet.createRow((int)rowCount);
			Util.getExcelStringCell("(B1) Total No Of Games Played by All Customers: (sum of Column E) ",0, rowhead2);
			Util.getExcelIntegerCell(totalGames, 1, rowhead2);
			
			rowCount++;
			Row rowhead3 =   ssSheet.createRow((int)rowCount);
			Util.getExcelStringCell("(C1) Total No Of Question From all Games Played by All Customers: (sum of column F) ",0, rowhead3);
			Util.getExcelIntegerCell(totalQuestions, 1, rowhead3);
			
			rowCount++;
			Row rowhead4 =   ssSheet.createRow((int)rowCount);
			Util.getExcelStringCell("(D1) Total No Of Question Answered From all Games Played by All Customers: (sum of column G) ",0, rowhead4);
			Util.getExcelIntegerCell(totalAnsweredQuestions, 1, rowhead4);
			
			rowCount++;
			Double totalAnsweredQuestionsAsDouble = Double.parseDouble(String.valueOf(totalAnsweredQuestions));
			ssSheet.createRow((int)rowCount); rowCount++;
			Row rowhead5 =   ssSheet.createRow((int)rowCount);
			Util.getExcelStringCell("(E1) Overall Average Percentage of Questions Answered: ((D1/C1)*100)",0, rowhead5);
			Util.getExcelDecimalCell(((totalAnsweredQuestionsAsDouble/totalQuestions)*100), 1, rowhead5);
		
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_AVG_QUESTION_ASWERED_BY_CUSTOMER+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	@RequestMapping({"/DownloadNoOfGamePlayedByCustomerReport"})
	public void downloadNoOfGamePlayedByCustomerReport(HttpServletRequest request, HttpServletResponse response){
		try{
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			int noOfGames=0;
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("Customer Id");
			headerList.add("Customer Name");
			headerList.add("Customer UserID");
			headerList.add("Customer Email");
			headerList.add("No oF Games Played");
			headerList.add("Average Percentage Game Played by Customer");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet(Constants.RTF_No_OF_GAME_PLAYED_BY_CUSTOMER);
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			
			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getAvgNoOfGamPlayedByCustomerReportData(fromDate, toDate, sharedProperty);
			noOfGames = QuizDAORegistry.getContestsDAO().getTotalContestCount(fromDate, toDate);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0;
			int rowCount = 1;
			int totalCustomerCount = 0;
			int totalGames = 0;
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[3], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[4], j, rowhead);j++;
				if(dataObj[4]!=null){
					totalGames += Integer.parseInt(dataObj[4].toString());
				}
				Util.getExcelDecimalCell(dataObj[5], j, rowhead);j++;
				totalCustomerCount++;
				
			}
			
			ssSheet.createRow((int)rowCount); rowCount++;
			Row rowhead1 =   ssSheet.createRow((int)rowCount);
			Util.getExcelStringCell("Total No.Of Customers Who Have Played Game: (A1)",0, rowhead1);
			Util.getExcelIntegerCell(totalCustomerCount, 1, rowhead1);
			
			rowCount++;
			Row rowhead2 =   ssSheet.createRow((int)rowCount);
			Util.getExcelStringCell("Total No.Of Contest Played Till Date: (B1)",0, rowhead2);
			Util.getExcelIntegerCell(noOfGames, 1, rowhead2);
			
			rowCount++;
			Double totalGamesAsDouble = Double.parseDouble(String.valueOf(totalGames));
			Double totalGamesAsDouble1 = Double.parseDouble(String.valueOf(noOfGames));
			Row rowhead3 =   ssSheet.createRow((int)rowCount);
			Util.getExcelStringCell("Overall Average No Of Customers Who Have Played Game: (A1/B1)",0, rowhead3);
			Util.getExcelDecimalCell((totalCustomerCount/totalGamesAsDouble1), 1, rowhead3);
			
			rowCount++;
			Row rowhead4 =   ssSheet.createRow((int)rowCount);
			Util.getExcelStringCell("Total No. Of Contest Played by All Customers: (C1) (sum of Column E)",0, rowhead4);
			Util.getExcelIntegerCell(totalGames, 1, rowhead4);
			
			rowCount++;
			Row rowhead5 =   ssSheet.createRow((int)rowCount);
			Util.getExcelStringCell("Overall Average No Of Games Participated Per Customer: (C1/A1)",0, rowhead5);
			Util.getExcelDecimalCell((totalGamesAsDouble/totalCustomerCount), 1, rowhead5);
		
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_No_OF_GAME_PLAYED_BY_CUSTOMER+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	@RequestMapping({"/DownloadCustomerRewardDollarsBreakdownDetailedReport"})
	public void downloadCustomerRewardDollarsStatisticsReport(HttpServletRequest request, HttpServletResponse response){
		try{
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("Customer Id");
			headerList.add("Customer Name");
			headerList.add("Customer UserID");
			headerList.add("Customer Phone");
			headerList.add("Customer Email");
			headerList.add("Earned Points");
			headerList.add("Contest Points");
			headerList.add("Redeemed Points");
			headerList.add("Pending Points");
			headerList.add("Active Points");
			headerList.add("Manual Points");
			headerList.add("Total Points");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet(Constants.RTF_CUSTOMER_REWARD_DOLLARS_BREADOWN);
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			
			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getCustomerRewardDollarsReportData(fromDate, toDate, sharedProperty);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0;
			int rowCount = 1;
			Double total5=0.0, total6 = 0.0,total7 =0.0,total8 =0.0,total9 =0.0,total10 =0.0,total11=0.0;
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[3], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[4], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[5], j, rowhead);j++;
				if(dataObj[5]!=null){
					total5 += Double.parseDouble(dataObj[5].toString());
				}
				Util.getExcelDecimalCell(dataObj[6], j, rowhead);j++;
				if(dataObj[6]!=null){
					total6 += Double.parseDouble(dataObj[6].toString());
				}
				Util.getExcelDecimalCell(dataObj[7], j, rowhead);j++;
				if(dataObj[7]!=null){
					total7 += Double.parseDouble(dataObj[7].toString());
				}
				Util.getExcelDecimalCell(dataObj[8], j, rowhead);j++;
				if(dataObj[8]!=null){
					total8 += Double.parseDouble(dataObj[8].toString());
				}
				Util.getExcelDecimalCell(dataObj[9], j, rowhead);j++;
				if(dataObj[9]!=null){
					total9 += Double.parseDouble(dataObj[9].toString());
				}
				Util.getExcelDecimalCell(dataObj[10], j, rowhead);j++;
				if(dataObj[10]!=null){
					total10 += Double.parseDouble(dataObj[10].toString());
				}
				Util.getExcelDecimalCell(dataObj[11], j, rowhead);j++;
				if(dataObj[11]!=null){
					total11 += Double.parseDouble(dataObj[11].toString());
				}
				
			}
			
			ssSheet.createRow((int)rowCount); rowCount++;
			Row rowhead1 =   ssSheet.createRow((int)rowCount);
			Util.getExcelStringCell("Total",0, rowhead1);
			Util.getExcelDecimalCell(total5, 5, rowhead1);
			Util.getExcelDecimalCell(total6, 6, rowhead1);
			Util.getExcelDecimalCell(total7, 7, rowhead1);
			Util.getExcelDecimalCell(total8, 8, rowhead1);
			Util.getExcelDecimalCell(total9, 9, rowhead1);
			Util.getExcelDecimalCell(total10, 10, rowhead1);
			Util.getExcelDecimalCell(total11, 11, rowhead1);
		
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_CUSTOMER_REWARD_DOLLARS_BREADOWN+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	@RequestMapping({"/DownloadContestWiseNewCustomersReport"})
	public void downloadContestWiseNewCustomers(HttpServletRequest request, HttpServletResponse response){
		try{
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("Contest Id");
			headerList.add("Contest Name");
			headerList.add("Contest Date");
			headerList.add("First Time Participants Count");
			headerList.add("Total Participants Count");
			headerList.add("% of First Time Participants");
			headerList.add("Signup Count");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet(Constants.RTF_CONTEST_WISE_FIRST_TIME_CUSTOMER);
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			
			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getContestWiseFirstTimeCustomerReportData();
			int j = 0;
			int rowCount = 1;
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[3], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[4], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[5], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[6], j, rowhead);j++;
				
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_CONTEST_WISE_FIRST_TIME_CUSTOMER+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping({"/DownloadRegisteredCustomersReport"})
	public void downloadRegisteredCustomers(HttpServletRequest request, HttpServletResponse response){
		try{
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("First Name");
			headerList.add("Last Name");
			headerList.add("User Id");
			headerList.add("Email");
			headerList.add("Phone");
			headerList.add("Signup Date");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet(Constants.RTF_REGISTERED_CUSTOMERS);
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getRegisteredUserReportData(fromDate,toDate,sharedProperty);
			int j = 0;
			int rowCount = 1;
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelStringCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[3], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[4], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[5], j, rowhead,cellStyleWithHourMinute);j++;
				
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_REGISTERED_CUSTOMERS+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping({"/DownloadRegisteredCustomersWhoDidNotPlayedContestReport"})
	public void downloadRegisteredButNotPlayedGameCustomers(HttpServletRequest request, HttpServletResponse response){
		try{
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("First Name");
			headerList.add("Last Name");
			headerList.add("User Id");
			headerList.add("Email");
			headerList.add("Phone");
			headerList.add("Signup Date");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet(Constants.RTF_REGISTERED_NOT_PLAYED_CUSTOMERS);
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getRegisteredUserWhoHaveNotPlayedContestReportData(fromDate,toDate,sharedProperty);
			int j = 0;
			int rowCount = 1;
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelStringCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[3], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[4], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[5], j, rowhead,cellStyleWithHourMinute);j++;
				
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_REGISTERED_NOT_PLAYED_CUSTOMERS+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	
	@RequestMapping({"/DownloadCustomerRewardDollarsLiabilityReport"}) // Shiva Modified on 14 Oct 2019
	public void downloadCustomerRewardDollarsLiabilityReport(HttpServletRequest request, HttpServletResponse response){	 // Shiva Modified on 14 Oct 2019
		try{
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("CustomerId");
			headerList.add("Customer Name");
			headerList.add("User Id");
			headerList.add("Phone");
			headerList.add("Email");
			//headerList.add("Contest Reward Dollars");	 // Shiva Modified on 14 Oct 2019
			headerList.add("Redeemed Rewards");	// Shiva Added on 14 Oct 2019
			headerList.add("Balance Rewards");	// Shiva Added on 14 Oct 2019
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet(Constants.RTF_CUSTOMER_REWARD_DOLLARS_LIABILITY);	 // Shiva Modified on 14 Oct 2019
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			
			//List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getContestCustomerRewardDollarsReportData(fromDate,toDate,sharedProperty);	// Shiva Modified on 14 Oct 2019
			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getCustomerRewardDollarsLiabilityReportData(fromDate,toDate,sharedProperty);
			int j = 0;
			int rowCount = 1;
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[3], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[4], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[5], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[6], j, rowhead);j++;	// Shiva Added on 14 Oct 2019
				
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_CUSTOMER_REWARD_DOLLARS_LIABILITY+"." +Constants.EXCEL_EXTENSION);	// Shiva Modified on 14 Oct 2019
			workbook.write(response.getOutputStream());
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	@RequestMapping({"/DownloadCustomerWhoPlayedContestReport"})
	public void downloadCustomerWhoPlayedContestReport(HttpServletRequest request, HttpServletResponse response){
		try{
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("CustomerId");
			headerList.add("Customer Name");
			headerList.add("User Id");
			headerList.add("Phone");
			headerList.add("Email");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet(Constants.RTF_REGISTERED_PLAYED_CUSTOMERS);
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			
			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getRegisteredAndPlayedCustomers(fromDate, toDate, sharedProperty);
			int j = 0;
			int rowCount = 1;
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[3], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[4], j, rowhead);j++;
				
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_REGISTERED_PLAYED_CUSTOMERS+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	/*@RequestMapping({"/DownloadRTFUnsoldInventory"})
	public void downloadRTFUnsoldInventory(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		try{
			StringBuilder eventsCsv = new StringBuilder();
			eventsCsv.append("EventID,EventName,EventDate,EventTime,VenueName,VenueCity,Section,Row,Seat,Original_Ticket_Count,Remaining_Ticket_Count,Retail_Price,Face_Price,Cost,WholeSale_Price,InternalNotes,OnHand,InstandDownload,StockType,PurchaseOrderId,ClientBrokerCompanyName\n");
			eventsCsv.append(Util.getCommaSeperatedStringFromCollection(DAORegistry.getEventDAO().getRTFUnsoldInventory()));
			response.setHeader("Content-Type", "text/csv");
			response.setHeader("Content-disposition", "attachment; filename="+Constants.RTF_UNSOLD_INVENTORY_REPORT+"."+Constants.CSV_EXTENSION);
			response.getOutputStream().write(eventsCsv.toString().getBytes());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/*@RequestMapping({"/DownloadRTFZonesNotHavingColorsReport"})
	public String downloadRTFZonesNotHavingColorsReport(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session,
			@RequestParam(name = "startDate",required = false) String startDate,
			@RequestParam(name = "endDate",required = false) String endDate) throws IOException{
		
			if(!StringUtils.isEmpty(startDate) && !StringUtils.isEmpty(endDate)){
				SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy"); 
				DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
				String today = dateFormat.format(new Date());
				ServletOutputStream out = response.getOutputStream();
				
				response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				response.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_ZONES_NOT_HAVING_COLORS+"." + today
							+ ".xlsx");
				
				try{
					Map<Integer,List<VenueCategory>> venueCategoriesMap = new HashMap<Integer, List<VenueCategory>>();
					
					 Collection<com.rtw.tmat.data.Venue> venues = TMATDAORegistry.getVenueDAO().getAllVenuesForUSandCA(dateFormatter.parse(startDate),dateFormatter.parse(endDate));
					 Set<VenueCategory> venueCategoriesList = TMATDAORegistry.getVenueCategoryDAO().getAllVenueCategoriesForUSandCA(venues);
					 
					 for (VenueCategory venueCategory : venueCategoriesList) {
						 Integer venueId = venueCategory.getVenue().getId();
						 List<VenueCategory> tempVenueCats = venueCategoriesMap.get(venueId);
						 if(tempVenueCats == null) {
							 tempVenueCats = new ArrayList<VenueCategory>();
						 }
						 tempVenueCats.add(venueCategory);
						 venueCategoriesMap.put(venueId, tempVenueCats);
					}
					 
					 FileReader fReader = null;
					 BufferedReader reader = null;
					 String svgText = "",str="";
					 List<SVGFileMissingZoneData> missingSvgZonesList = new ArrayList<SVGFileMissingZoneData>();
					 List<SVGFileMissingZoneData> missingCSVZonesList = new ArrayList<SVGFileMissingZoneData>();
					 List<SVGFileMissingZoneData> invalidFormatSVGFiles = new ArrayList<SVGFileMissingZoneData>();
					 
					 int totVenues=venues.size(),processedVenues=0;
					 for (com.rtw.tmat.data.Venue venue : venues) {
						 Integer venueId = venue.getId();
						 
						 processedVenues++;
						 if(processedVenues%100 == 0) {
							 System.out.println(processedVenues+" / "+totVenues+" : SVG and CSV Missing Venue : "+venueId);
						 }
						 
						 List<VenueCategory> venueCategories = venueCategoriesMap.get(venueId);
						 if(venueCategories == null) {
							 continue;
						 }
						 //List<VenueCategory> venueCategories = DAORegistry.getVenueCategoryDAO().getVenueCategoriesByVenueIdOrderByCategory(venueId);
					
						 for (VenueCategory venueCatObj : venueCategories) {
							 try {
								 String fileName = venueId+"_"+venueCatObj.getCategoryGroup();
								 List<String> missingCsvZones = new ArrayList<String>();
								 List<String> missingSvgZones = new ArrayList<String>();
								
								 
								 Map<String, Boolean> svgZoneMap = new HashMap<String, Boolean>();
								 Map<String, Boolean> csvZoneMap = new HashMap<String, Boolean>();
								 
								 List<String> zoneList = TMATDAORegistry.getQueryManagerDAO().getAllZoneByVenueCategory(venueId);
								 if(zoneList != null) {
									 for (String zone : zoneList) {
										 csvZoneMap.put(zone.toUpperCase(), true);
									}
								 }
								 
								 //str ="";
								 //File svgTxtFile = new File("////C://TMATIMAGESFINAL//SvgText//"+fileName+".txt");
								 
								 Map<String, String> httpParams = new HashMap<String, String>();
								 httpParams.put("configId", "RewardTheFan");
								 httpParams.put("venueId", String.valueOf(venue.getId()));
								 httpParams.put("categoryGroupName", venueCatObj.getCategoryGroup());
								 
								 String httpResponse = HttpUtil.executeHttpGetRequest("api.rewardthefan.com/", "GetSvgTextInformation.json", new HashMap<String, String>(), httpParams);
								 String svgTxtData = null;
								 
								 if(httpResponse != null && !httpResponse.isEmpty()){
									 ObjectMapper mapper = new ObjectMapper();
									 JsonNode actualObj = mapper.readTree(httpResponse);
									 JsonNode jsonNode = actualObj.get("TMATSvgMapDetails");
									 svgTxtData = jsonNode.get("svgText").asText();
									 System.out.println(svgTxtData);
								 }
								 
								 if(svgTxtData != null && !svgTxtData.isEmpty()) {
									fReader = new FileReader(svgTxtFile);
									reader = new BufferedReader(fReader);
									while((str = reader.readLine()) != null) {
										svgText += str;
									}
									try {
										svgZoneMap = getAllZonesFromText(svgTxtData);
									} catch(Exception e) {
										 SVGFileMissingZoneData zoneData = new SVGFileMissingZoneData(venue);
										 zoneData.setVenueCategoryId(venueCatObj.getId());
										 zoneData.setCategoryGroupName(venueCatObj.getCategoryGroup());
										 invalidFormatSVGFiles.add(zoneData);
										 continue;
									}
								 }
								 
								 for (String key : svgZoneMap.keySet()) {
									 Boolean isZonethere = csvZoneMap.remove(key);
									 if(null == isZonethere || !isZonethere){
										 missingCsvZones.add(key);
									 }
								 }
								 for (String key : csvZoneMap.keySet()) {
									 missingSvgZones.add(key);
								 }
								 
								 if(!missingSvgZones.isEmpty()) {
									 for (String zone : missingSvgZones) {
										 SVGFileMissingZoneData zoneData = new SVGFileMissingZoneData(venue);
										 zoneData.setVenueCategoryId(venueCatObj.getId());
										 zoneData.setCategoryGroupName(venueCatObj.getCategoryGroup());
										 zoneData.setZone(zone);
										 missingSvgZonesList.add(zoneData);
										 
									}
								 }
								 if(!missingCsvZones.isEmpty()) {
									 for (String zone : missingCsvZones) {
										 SVGFileMissingZoneData zoneData = new SVGFileMissingZoneData(venue);
										 zoneData.setVenueCategoryId(venueCatObj.getId());
										 zoneData.setCategoryGroupName(venueCatObj.getCategoryGroup());
										 zoneData.setZone(zone);
										 missingCSVZonesList.add(zoneData);
										 
									}
								 }
							 } catch (Exception e) {
								 e.printStackTrace();
							}
						}
					 }
					 SXSSFWorkbook workbook = new SXSSFWorkbook();
					generateMissingSVGandCSVzonesFile(workbook.createSheet("SVG Zones not in CSV"),missingCSVZonesList,true);
					generateMissingSVGandCSVzonesFile(workbook.createSheet("CSV Zones not in SVG"),missingSvgZonesList,true);
					generateMissingSVGandCSVzonesFile(workbook.createSheet("SVG Files with Invalid Format Data"),invalidFormatSVGFiles,false);
					 
					workbook.write(out);
				 }catch (Exception e){
					 e.printStackTrace();
				 }
			}else{
				SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
				Calendar cal = Calendar.getInstance(); 
				map.addAttribute("startDate", dateFormat.format(cal.getTime()));
				map.addAttribute("endDate", dateFormat.format(cal.getTime()));
			}
		return "page-map-not-having-zones-colors-report";
	}*/
	
	/*@RequestMapping({"/DownloadRTFProfitAndLossReport"})
	public String downloadRTFProfitAndLossReport(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(name = "startDate",required = false) String startDate,
			@RequestParam(name = "endDate",required = false) String endDate){
		try{
			if(!StringUtils.isEmpty(startDate) && !StringUtils.isEmpty(endDate)){
				List<String> headerList = new ArrayList<String>();
				headerList.add("Event Name");
				headerList.add("Event Date");
				headerList.add("Event Time");
				headerList.add("Venue Name");
				headerList.add("Venue City");
				headerList.add("Venue State");
				headerList.add("Venue Country");
				headerList.add("Section");
				headerList.add("Row");
				headerList.add("Qty");
				headerList.add("On Hand");
				headerList.add("Stock Type");
				headerList.add("Invoice Id");
				headerList.add("Sales Person");
				headerList.add("Sales Date");
				headerList.add("PurchaseOrder Id");
				headerList.add("PO CreatedDate");
				headerList.add("Vendor Company");
				headerList.add("Vendor Email");
				
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				Sheet longTicketsSheet =  workbook.createSheet("RTF Long");
				ExcelUtil.generateExcelHeaderRow(headerList, longTicketsSheet);
				ExcelUtil.generateExcelData(DAORegistry.getEventDAO().getNext15DaysUnsentOrders(), longTicketsSheet);
				
				Sheet categoryTicketsSheet =  workbook.createSheet("RTF Cats");
				ExcelUtil.generateExcelHeaderRow(headerList, categoryTicketsSheet);
				ExcelUtil.generateExcelData(DAORegistry.getEventDAO().getNext15DaysUnsentOrders(), categoryTicketsSheet);
				
				Sheet summarySheet =  workbook.createSheet("Summary");
				ExcelUtil.generateExcelHeaderRow(headerList, summarySheet);
				ExcelUtil.generateExcelData(DAORegistry.getEventDAO().getNext15DaysUnsentOrders(), summarySheet);
				
				response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				response.setHeader("Content-Type", "application/vnd.ms-excel");
				response.setHeader("Content-disposition",
						"attachment; filename="+Constants.RTF_PROFIT_AND_LOSS_REPORT+"." +Constants.EXCEL_EXTENSION);
				
				workbook.write(response.getOutputStream());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return "page-profit-loss-report";
	}*/
	
	/*public static Map<String, Boolean> getAllZonesFromText(String svgText) throws Exception {
		
		Map<String, Boolean> allZonesMap = new HashMap<String, Boolean>();
		try {			
		 	Pattern pattern = Pattern.compile("<g id=\"zone_letters\" display=\"none\">(.*?)</g>");
			Matcher matcher = pattern.matcher(svgText);
						
			String value = new String();
			
			if(matcher.find()){
				//System.out.println("ZONE :" + dayMatcher.group(1));
				value = matcher.group(1);
			}
			
			String[] strings = value.split("transform=");
			//System.out.println(value);
			for (String text : strings) {
				
				Pattern dayPattern = Pattern.compile("<text id=\"text_(.*?)\" font-weight");
				Matcher dayMatcher = dayPattern.matcher(text);
				
				if(dayMatcher.find()){
					//System.out.println("ZONE :" + dayMatcher.group(1));
					String zone = dayMatcher.group(1);
					allZonesMap.put(zone.toUpperCase(), true);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}		
		return allZonesMap;		 
	 }*/
	
	
	@RequestMapping(value = "/GetBotsReport")
	public void getBotsReport(HttpServletRequest request,HttpServletResponse response,Model model){
		try {
			Map<String, Object> resultMap = QuizDAORegistry.getContestsDAO().getAllBotsInformation(sharedProperty);
			
			Set<String> referralCodeList = new TreeSet<String>((Set<String>) resultMap.get("ReferralCodeList"));
			List<BotDetail> botList = (List<BotDetail>)resultMap.get("BotList");

			List<String> headerList = new ArrayList<String>();
			headerList.add("Customer ID");
			headerList.add("User ID");
			headerList.add("Email");
			headerList.add("SignUp Date");
			headerList.add("SignUp Platform");
			headerList.add("SignUp Device ID");
			headerList.add("Referral Code");
			headerList.add("Is OTP Verified");
			headerList.add("AllDevices");
			headerList.add("IPAddress"); 
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet("Bot Details");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet); 
			
			int j = 0;
			int rowCount = 1;
			
			for(BotDetail obj : botList) {
				
				Row rowhead =   ssSheet.createRow((int)rowCount);
				 
				rowCount++;
				j = 0;
				 
				rowhead.createCell((int) j).setCellValue(obj.getCustomerId().toString());
				j++;
				
				rowhead.createCell((int) j).setCellValue(obj.getUserId());
				j++;
				
				rowhead.createCell((int) j).setCellValue(obj.getEmail());
				j++;
				
				rowhead.createCell((int) j).setCellValue(obj.getSignupDate());
				j++;
				
				rowhead.createCell((int) j).setCellValue(obj.getSignUpPlatform());
				j++;
				
				rowhead.createCell((int) j).setCellValue(obj.getSignUpDeviceId());
				j++;
				
				rowhead.createCell((int) j).setCellValue(obj.getReferrerCode());
				j++;
				
				rowhead.createCell((int) j).setCellValue(obj.getOtpVerified());
				j++;
				
				rowhead.createCell((int) j).setCellValue(obj.getAllLoggedInDevices());
				j++;
				
				rowhead.createCell((int) j).setCellValue(obj.getAllIPAddress());
				j++;
			 
			}
			
			
			headerList = new ArrayList<String>();
			headerList.add("Emails");
			
			ssSheet =  workbook.createSheet("Unique-Emails");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet); 
			
			j = 0;
			rowCount = 1;
			
			for(BotDetail obj : botList) {
				
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				
				rowhead.createCell((int) j).setCellValue(obj.getEmail());
				j++;
			 
			}
			
			headerList = new ArrayList<String>();
			headerList.add("ReferralCode");
			
			ssSheet =  workbook.createSheet("Unique-Referral-Code");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet); 
			
			j = 0;
			rowCount = 1;
			
			for(String obj : referralCodeList) {
				
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				
				rowhead.createCell((int) j).setCellValue(obj);
				j++;
			 
			}
			 
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename=RTFBotsReport." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
			 
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	@RequestMapping(value = "/GetPollingReport")
	public void getPollingReport(HttpServletRequest request,HttpServletResponse response,Model model){
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			Map<String, Object> resultMap = null;
		try {
			resultMap = QuizDAORegistry.getContestsDAO().getPollingInformation(fromDate, toDate, sharedProperty);	
			
			if(resultMap == null) return ; // Show error.txt file in download 
			List<Object[]> pollUserList = (List)resultMap.get("pollingDistinctUserIdList");
			List<Object[]> pollingVideoDistinctUserIdList = (List)resultMap.get("pollingVideoDistinctUserIdList");	
			List<Object[]> dataList = (List)resultMap.get("pollingUserVideoPlayListHistory");
			List<Object[]> dataListrwdHist = (List)resultMap.get("pollingUserAnswerRewardListHistory");
			
			//sheet 0
			Sheet ssSheet = workbook.createSheet("PollingVideoStats");
			Row rowhead1 =   ssSheet.createRow((int)2);
			Util.getExcelStringCell("User Stats  ", 0, rowhead1);
			rowhead1 =   ssSheet.createRow((int)3);
			Util.getExcelStringCell("No. of Distinct Polling Players Count:  ", 0, rowhead1);
			Util.getExcelIntegerCell(pollUserList.size(), 1, rowhead1);
			
			rowhead1 =   ssSheet.createRow((int)4);
			Util.getExcelStringCell("No. of Polling Players Count:  ", 0, rowhead1);
			Util.getExcelIntegerCell(dataListrwdHist.size(), 1, rowhead1);
			
			rowhead1 =   ssSheet.createRow((int)5);
			Util.getExcelStringCell("No. of Distinct Video Viewers Count: ", 0, rowhead1);
			Util.getExcelIntegerCell(pollingVideoDistinctUserIdList.size(), 1, rowhead1);
			rowhead1 =   ssSheet.createRow((int)6);
			Util.getExcelStringCell("No. of Video Viewers Count: ", 0, rowhead1);
			Util.getExcelIntegerCell(dataList.size(), 1, rowhead1);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			//Sheet  1
			
			List<String> headerList = new ArrayList<String>();
			headerList = new ArrayList<String>();
			headerList.add("Customer Name");
			headerList.add("Phone");
			headerList.add("eMail");
			headerList.add("User ID");
			ssSheet = workbook.createSheet("DistinctPollingCustomer");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			int j = 0;
			int rowCount = 1;
			for(Object[] dataObj : pollUserList){					
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;					
				Util.getExcelStringCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[3], j, rowhead);j++;						
					
			}
			//Sheet  2
			headerList = new ArrayList<String>();
			headerList.add("Customer Name");
			headerList.add("Phone");
			headerList.add("eMail");
			headerList.add("User ID");
			headerList.add("Polling Question");
			headerList.add("Option A");
			headerList.add("Option B");
			headerList.add("Option C");
			headerList.add("Customer Answered Option");
			headerList.add("Polling Date");
			headerList.add("Reward Type");
			headerList.add("Reward Quantity");
			ssSheet = workbook.createSheet("PollingAnswerDetails");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);				  	
			j = 0; rowCount = 1;
			for(Object[] dataObj : dataListrwdHist){					
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;					
				Util.getExcelStringCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[3], j, rowhead);j++;	
						
						
				Util.getExcelStringCell(dataObj[4], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[5], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[6], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[7], j, rowhead);j++;
						
				Util.getExcelStringCell(dataObj[8], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[9], j, rowhead,cellStyleWithHourMinute);j++;
				Util.getExcelStringCell(dataObj[10], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[11], j, rowhead);j++;
						
			}				
			
			//Sheet  3
			List<String> headerpollingVideoDistinctUserIdList = new ArrayList<String>();			
			headerpollingVideoDistinctUserIdList = new ArrayList<String>();
			headerpollingVideoDistinctUserIdList.add("Customer Name");
			headerpollingVideoDistinctUserIdList.add("Phone");
			headerpollingVideoDistinctUserIdList.add("eMail");
			headerpollingVideoDistinctUserIdList.add("User ID");
			
			ssSheet =  workbook.createSheet("DistinctCustomerPlayedVideo");
			ExcelUtil.generateExcelHeaderRow(headerpollingVideoDistinctUserIdList, ssSheet);
			j = 0;
			rowCount = 1;
			for(Object[] dataObj : pollingVideoDistinctUserIdList){					
				Row rowhead =   ssSheet.createRow((int)rowCount);				
					rowCount++;
					j = 0;					
				Util.getExcelStringCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[3], j, rowhead);j++;						
					
			}			
			
			//sheet 4
			headerList = new ArrayList<String>();
			headerList.add("Customer Name");
			headerList.add("Phone");
			headerList.add("eMail");
			headerList.add("User ID");
			headerList.add("Video Played Date");
			headerList.add("Video Category");
			headerList.add("Video Title");
			ssSheet = workbook.createSheet("CustomerPlayedVideoDetails");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			
				
				j = 0; rowCount = 1;
				for(Object[] dataObj : dataList){					
					Row rowhead =   ssSheet.createRow((int)rowCount);				
					rowCount++;
					j = 0;					
					Util.getExcelStringCell(dataObj[0], j, rowhead);j++;
					Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
					Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
					Util.getExcelStringCell(dataObj[3], j, rowhead);j++;
					Util.getExcelDateCell(dataObj[4], j, rowhead,cellStyleWithHourMinute);j++;
					Util.getExcelStringCell(dataObj[5], j, rowhead);j++;
					Util.getExcelStringCell(dataObj[6], j, rowhead);j++;
					
				}				
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename=PollingandRTF-TV-VideoPlayedDetailsandStatsReport." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
			 
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/GetNoOfGamesPlayedReport")
	public void getNoOfGamesPlayedReport(HttpServletRequest request,HttpServletResponse response,Model model){
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Map<String, Object> resultMap = null;
		try {
			resultMap = QuizDAORegistry.getContestsDAO().getNoOfGamesPlayedInformation(sharedProperty);	
			
			if(resultMap == null) return ; // Show error.txt file in download 
			List<Object[]> gamesPlayedList = (List)resultMap.get("gamesPlayedList");
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			List<String> headerList = new ArrayList<String>();
			
			headerList = new ArrayList<String>();
			headerList.add("Customer Name");
			headerList.add("User ID");
			headerList.add("Phone");
			headerList.add("eMail");
			headerList.add("No of Games Played");
			headerList.add("Last Game Date");
			Sheet ssSheet = workbook.createSheet("NoOfGamesPlayed");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);				  	
			int j = 0; int rowCount = 1;
			for(Object[] dataObj : gamesPlayedList){					
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;					
				Util.getExcelStringCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[3], j, rowhead);j++;	
						
						
				Util.getExcelStringCell(dataObj[4], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[5], j, rowhead,cellStyleWithHourMinute);j++;
						
			}	
		
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			//response.setHeader("Content-disposition","attachment; filename=RTF_Customer_No.Of_Games_played." +Constants.EXCEL_EXTENSION);
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_CUSTOMER_NO_OF_GAMES_PLAYED+"." +Constants.EXCEL_EXTENSION);  // Shiva modified
			workbook.write(response.getOutputStream());
			 
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/GetCustomerSuperFanReport")
	public void getCustomerSuperFanReport(HttpServletRequest request,HttpServletResponse response,Model model){
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Map<String, Object> resultMap = null;
		try {
			resultMap = QuizDAORegistry.getContestsDAO().getCustomerSuperFanStarsInformation(sharedProperty);	
			
			if(resultMap == null) return ; // Show error.txt file in download 
			List<Object[]> superFanStarsList = (List)resultMap.get("customerSuperFanStars");
			
			
			List<String> headerList = new ArrayList<String>();
			
			headerList = new ArrayList<String>();
			headerList.add("Customer Name");
			headerList.add("User ID");
			headerList.add("Phone");
			headerList.add("eMail");
			headerList.add("Available Stars");
			Sheet ssSheet = workbook.createSheet("SuperFanStarsAvailability");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);				  	
			int j = 0; int rowCount = 1;
			for(Object[] dataObj : superFanStarsList){					
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;					
				Util.getExcelStringCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[3], j, rowhead);j++;
						
				Util.getExcelStringCell(dataObj[4], j, rowhead);j++;
						
			}	
		
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename=RTF_Customer_Available_Superfan_Stars." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
			 
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/GetPollingStatsReport")
	public GenericResponseDTO getPollingStatsReport(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		//model.addAttribute("genericResponseDTO", genericResponseDTO);
		String fromDate = request.getParameter("fromDate");
		String toDate = request.getParameter("toDate");
		String action = request.getParameter("action");				
		String returnMessage = "";
		try{
			Map<String, Object> resultMap = null;
			resultMap = QuizDAORegistry.getContestsDAO().getPollingInformation(fromDate, toDate,sharedProperty);	
			List<Object[]> pollUserList = (List)resultMap.get("pollingDistinctUserIdList");
			List<Object[]> pollingVideoDistinctUserIdList = (List)resultMap.get("pollingVideoDistinctUserIdList");	
			List<Object[]> dataList = (List)resultMap.get("pollingUserVideoPlayListHistory");
			List<Object[]> dataListrwdHist = (List)resultMap.get("pollingUserAnswerRewardListHistory");	
			
			
			List<ReportPojo> result = new ArrayList<ReportPojo>() ;
			ReportPojo report = new ReportPojo();
			report.setName("Distinct Polling Players Count");
			report.setViewCount(pollUserList.size());
			result.add(report);
			report = new ReportPojo();
			report.setName("Polling Players Count");
			report.setViewCount(dataListrwdHist.size());
			result.add(report);
			report = new ReportPojo();
			report.setName("Distinct Video Viewers Count");
			report.setViewCount(pollingVideoDistinctUserIdList.size());
			result.add(report);
			report = new ReportPojo();
			report.setName("Video Viewers Count");
			report.setViewCount(dataList.size());
			result.add(report);
			
			genericResponseDTO.setStatus(1);
			genericResponseDTO.setReportList(result);
			genericResponseDTO.setMessage(returnMessage);
		} catch (Exception e) {			
			e.printStackTrace();
			error.setDescription("Something went wrong while updating Polling Rewards");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}

	@RequestMapping(value = "/GetSuperFanStarsReport")
	public GenericResponseDTO getSuperFanStarsReport(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		//model.addAttribute("genericResponseDTO", genericResponseDTO);
		
		String action = request.getParameter("action");				
		String returnMessage = "";
		try{
			List<Object[]>  result = null;
			result = QuizDAORegistry.getContestsDAO().getSuperFanStartsInformation(sharedProperty);	
			List<ReportPojo> resultPojo = new ArrayList<ReportPojo>() ;
			
			for(Object[] reportPojo : result){					
				ReportPojo repPojo = new ReportPojo();	
				repPojo.setViewName((String) reportPojo[1]);
				repPojo.setViewCount((Integer) reportPojo[2]);
				repPojo.setViewCount1((Integer) reportPojo[3]);
				resultPojo.add(repPojo);
			}
			genericResponseDTO.setStatus(1);
			genericResponseDTO.setReportList(resultPojo);
			genericResponseDTO.setMessage(returnMessage);
		} catch (Exception e) {			
			e.printStackTrace();
			error.setDescription("Something went wrong while updating Polling Rewards");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
	
	
	
	@RequestMapping(value = "/GetTicketCostReport")
	public GenericResponseDTO getTicketCostReport(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();
		//model.addAttribute("genericResponseDTO", genericResponseDTO);
		String fromDate = request.getParameter("fromDate");
		String toDate = request.getParameter("toDate");
		
		String action = request.getParameter("action");				
		String returnMessage = "";
		try{
			List<Object[]> list = null;
			Map<String, Object> resultMap = null;
			Map<String, Object> returnMap = new HashMap<String, Object>();
			resultMap = QuizDAORegistry.getContestsDAO().getTicketCostInformation(fromDate, toDate, sharedProperty);	
			
			list = (List)resultMap.get("MonthWiseInvoiceStatus"); 
            List<ReportPojo> resultPojo = new ArrayList<ReportPojo>() ;
            for(Object[] reportPojo : list){					
				ReportPojo repPojo = new ReportPojo();	
				repPojo.setViewName((String) reportPojo[2]);
				repPojo.setViewCount((Integer)reportPojo[3]);
				if(reportPojo[4]!= null){
					repPojo.setViewCount1((Integer) reportPojo[4]);					
				}
				repPojo.setViewCount2(0);
				resultPojo.add(repPojo);
			}               	
            returnMap.put("MonthWiseInvoiceStatus", resultPojo);
            
            
            list = (List)resultMap.get("MonthWiseTicketCost"); 
            resultPojo = new ArrayList<ReportPojo>() ;
            for(Object[] reportPojo : list){					
				ReportPojo repPojo = new ReportPojo();	
				repPojo.setViewName((String) reportPojo[2]);
				repPojo.setViewCountDouble(((BigDecimal) reportPojo[3]).doubleValue());
				resultPojo.add(repPojo);
			}               	
            returnMap.put("MonthWiseTicketCost", resultPojo);
            
            
            list = (List)resultMap.get("MonthWiseTicketQuantity"); 
            resultPojo = new ArrayList<ReportPojo>() ;
            for(Object[] reportPojo : list){					
				ReportPojo repPojo = new ReportPojo();	
				repPojo.setViewName((String) reportPojo[2]);
				repPojo.setViewCount((Integer)reportPojo[3]);
				resultPojo.add(repPojo);
			}               	
            returnMap.put("MonthWiseTicketQuantity", resultPojo);
            
			genericResponseDTO.setStatus(1);
			genericResponseDTO.setReportMap(returnMap);
			genericResponseDTO.setMessage(returnMessage);
		} catch (Exception e) {			
			e.printStackTrace();
			error.setDescription("Something went wrong while updating Polling Rewards");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
	@RequestMapping(value = "/GetBotsCustomerReport")
	public GenericResponseDTO getBotsCustomerReport(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();		
		
		String action = request.getParameter("action");	

		String fromDate = request.getParameter("fromDate");
		String toDate = request.getParameter("toDate");
		String returnMessage = "";
		try{
			List<Object[]> list = null;
			Map<String, Object> resultMap = null;
			Map<String, Object> returnMap = new HashMap<String, Object>();
			resultMap = QuizDAORegistry.getContestsDAO().getBotsCustomerInformation(fromDate, toDate, sharedProperty);	
			
			list = (List)resultMap.get("MonthWiseBotsCust"); 
            List<ReportPojo> resultPojo = new ArrayList<ReportPojo>() ;
            for(Object[] reportPojo : list){					
				ReportPojo repPojo = new ReportPojo();	
				repPojo.setViewName((String) reportPojo[2]);
				repPojo.setViewCount((Integer)reportPojo[3]);
				if(reportPojo[4]!= null){
					repPojo.setViewCount1((Integer) reportPojo[4]);					
				}
				if(reportPojo[5]!= null){
					repPojo.setViewCount2(0);					
				}
				resultPojo.add(repPojo);
			}               	
            returnMap.put("MonthWiseBotsCust", resultPojo);
            
            
            list = (List)resultMap.get("QuaterlyWiseBotsCust"); 
            resultPojo = new ArrayList<ReportPojo>() ;
            for(Object[] reportPojo : list){					
				ReportPojo repPojo = new ReportPojo();	
				repPojo.setViewName((String) reportPojo[2]);
				repPojo.setViewCount((Integer)reportPojo[3]);
				if(reportPojo[4]!= null){
					repPojo.setViewCount1((Integer) reportPojo[4]);					
				}
				repPojo.setViewCount2(0);
				resultPojo.add(repPojo);
			}               	
            returnMap.put("QuaterlyWiseBotsCust", resultPojo);
            
            /*
            list = (List)resultMap.get("MonthWiseTicketQuantity"); 
            resultPojo = new ArrayList<ReportPojo>() ;
            for(Object[] reportPojo : list){					
				ReportPojo repPojo = new ReportPojo();	
				repPojo.setViewName((String) reportPojo[2]);
				repPojo.setViewCount((Integer)reportPojo[3]);
				resultPojo.add(repPojo);
			}               	
            returnMap.put("MonthWiseTicketQuantity", resultPojo);*/
            
			genericResponseDTO.setStatus(1);
			genericResponseDTO.setReportMap(returnMap);
			genericResponseDTO.setMessage(returnMessage);
		} catch (Exception e) {			
			e.printStackTrace();
			error.setDescription("Something went wrong while updating Polling Rewards");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
	
	@RequestMapping(value = "/GetContestParticipantReport")
	public GenericResponseDTO getContestParticipantReport(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();		
		
		String action = request.getParameter("action");				
		String returnMessage = "";
		try{
			List<Object[]> list = null;
			Map<String, Object> resultMap = null;
			Map<String, Object> returnMap = new HashMap<String, Object>();
			resultMap = QuizDAORegistry.getContestsDAO().getContestParticipantInformation(sharedProperty);	
			
			list = (List)resultMap.get("contestParticipant"); 
            List<ReportPojo> resultPojo = new ArrayList<ReportPojo>() ;
            for(Object[] reportPojo : list){					
				ReportPojo repPojo = new ReportPojo();	
				repPojo.setViewName2((String) reportPojo[0]);
				repPojo.setViewName((String) reportPojo[1]);
				repPojo.setViewCount((Integer)reportPojo[2]);
				
				resultPojo.add(repPojo);
			}               	
            returnMap.put("contestParticipant", resultPojo);
            
            
           /* list = (List)resultMap.get("QuaterlyWiseBotsCust"); 
            resultPojo = new ArrayList<ReportPojo>() ;
            for(Object[] reportPojo : list){					
				ReportPojo repPojo = new ReportPojo();	
				repPojo.setViewName((String) reportPojo[2]);
				repPojo.setViewCount((Integer)reportPojo[3]);
				if(reportPojo[4]!= null){
					repPojo.setViewCount1((Integer) reportPojo[4]);					
				}
				repPojo.setViewCount2(0);
				resultPojo.add(repPojo);
			}               	
            returnMap.put("QuaterlyWiseBotsCust", resultPojo);*/
            
			genericResponseDTO.setStatus(1);
			genericResponseDTO.setReportMap(returnMap);
			genericResponseDTO.setMessage(returnMessage);
		} catch (Exception e) {			
			e.printStackTrace();
			error.setDescription("Something went wrong while updating Polling Rewards");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
	
	
	@RequestMapping(value = "/GetQueWiseUserAnswerCountReport")
	public GenericResponseDTO getQueWiseUserAnswerCountReport(HttpServletRequest request, HttpServletResponse response){
		GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
		Error error = new Error();		
		
		String action = request.getParameter("action");				
		String returnMessage = "";

		String fromDate = request.getParameter("fromDate");
		String toDate = request.getParameter("toDate");
		try{
			
			List<Object[]> list = null;
			Map<String, Object> resultMap = null;
			Map<String, Object> returnMap = new HashMap<String, Object>();
			resultMap = QuizDAORegistry.getContestsDAO().getQueWiseUserAnswerCountInformation(fromDate, toDate,sharedProperty);	
			
			list = (List)resultMap.get("answeredForEachGame"); 
            List<ReportPojo> resultPojo = new ArrayList<ReportPojo>() ;
            for(Object[] reportPojo : list){					
				ReportPojo repPojo = new ReportPojo();	
				repPojo.setViewName((String) reportPojo[1]);
				repPojo.setViewCountDouble(((BigDecimal) reportPojo[2]).doubleValue());
				repPojo.setViewCountDouble1(((BigDecimal) reportPojo[3]).doubleValue());
				resultPojo.add(repPojo);
			}               	
            returnMap.put("answeredForEachGame", resultPojo);
            
            
            list = (List)resultMap.get("participantCount"); 
            resultPojo = new ArrayList<ReportPojo>() ;
            for(Object[] reportPojo : list){					
				ReportPojo repPojo = new ReportPojo();	
				repPojo.setViewName2((String) reportPojo[0]);
				repPojo.setViewName((String) reportPojo[1]);
				repPojo.setViewCount((Integer)reportPojo[2]);
				resultPojo.add(repPojo);
			}               	
            returnMap.put("participantCount", resultPojo);
            
			genericResponseDTO.setStatus(1);
			genericResponseDTO.setReportMap(returnMap);
			genericResponseDTO.setMessage(returnMessage);
		} catch (Exception e) {			
			e.printStackTrace();
			error.setDescription("Something went wrong while updating Polling Rewards");
			genericResponseDTO.setError(error);
			genericResponseDTO.setStatus(0);
		}
		return genericResponseDTO;
	}
	
	
	
	
	@RequestMapping(value = "/GetMonthWiseRegisteredCustomerCount")
	public void getMonthWiseRegisteredCustomerCountReport(HttpServletRequest request, HttpServletResponse response){
		SXSSFWorkbook workbook = new SXSSFWorkbook();
		String fromDate = request.getParameter("fromDate");
		String toDate = request.getParameter("toDate");
		try{
			List<String> headerList = new ArrayList<String>();
			
			headerList = new ArrayList<String>();
			headerList.add("Month");
			headerList.add("Downloaded & Registered");
			Sheet ssSheet = workbook.createSheet("Registered Customers");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<Object[]> list = QuizDAORegistry.getReportDAO().getMothWiseUserRegistrationCount(fromDate, toDate, sharedProperty);
			int j = 0; int rowCount = 1;
			int totalCount = 0;
			for(Object[] dataObj : list){					
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;					
				Util.getExcelStringCell(dataObj[0], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[1], j, rowhead);j++;
				totalCount += Integer.valueOf(dataObj[1].toString());
			}	
			rowCount++;
			ssSheet.createRow((int)rowCount); rowCount++;
			Row rowhead1 =   ssSheet.createRow((int)rowCount);
			Util.getExcelStringCell("Total Donwload & Registered",0, rowhead1);
			Util.getExcelIntegerCell(totalCount, 1, rowhead1);
		
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			//response.setHeader("Content-disposition","attachment; filename=RTF_month_wise_registered_customer_count." +Constants.EXCEL_EXTENSION);
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_MONTH_WISE_REGISTERED_CUSTOMER_COUNT+"." +Constants.EXCEL_EXTENSION);  //Shiva Modified
			workbook.write(response.getOutputStream());              	
		} catch (Exception e) {			
			e.printStackTrace();
			
		}
	}
	
	
	
	@RequestMapping(value = "/GetAverageTimeTakenByContests")
	public void getAverageTimeTakenByContests(HttpServletRequest request, HttpServletResponse response){
		SXSSFWorkbook workbook = new SXSSFWorkbook();
		String fromDate = request.getParameter("fromDate");
		String toDate = request.getParameter("toDate");
		try{
			List<String> headerList = new ArrayList<String>();
			
			headerList = new ArrayList<String>();
			headerList.add("Contest ID");
			headerList.add("Contest Name");
			headerList.add("Contest Date");
			headerList.add("Customer Average Time in Minutes");
			
			Sheet ssSheet = workbook.createSheet("CustomeAverageTime");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<Object[]> list = QuizDAORegistry.getReportDAO().getAvgTimeTakenPerContest(fromDate, toDate);
			int j = 0; int rowCount = 1;
			for(Object[] dataObj : list){					
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;					
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[3], j, rowhead);j++;
			}	
		
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			//response.setHeader("Content-disposition","attachment; filename=RTF_contest_wise_average_time_taken." +Constants.EXCEL_EXTENSION);
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_CONTEST_WISE_AVERAGE_TIME_TAKEN+"." +Constants.EXCEL_EXTENSION); //Shiva Modified
			workbook.write(response.getOutputStream());              	
		} catch (Exception e) {			
			e.printStackTrace();
			
		}
	}
	
	
	
	@RequestMapping(value = "/GetActiveInActiveCustomerCount")
	public void getActiveInActiveCustomerCount(HttpServletRequest request, HttpServletResponse response){
		SXSSFWorkbook workbook = new SXSSFWorkbook();
		try{
			List<String> headerList = new ArrayList<String>();
			
			headerList = new ArrayList<String>();
			headerList.add("Month");
			headerList.add("Active Customers");
			headerList.add("InActive Customers");
			headerList.add("Registered Customer");
			Sheet ssSheet = workbook.createSheet("Customers");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<Object[]> list = QuizDAORegistry.getReportDAO().getActiveInActiveUserCount();
			int j = 0; int rowCount = 1;
			int totalCount = 0;
			for(Object[] dataObj : list){					
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;					
				Util.getExcelStringCell(dataObj[0], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[1], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[2], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[3], j, rowhead);j++;
				totalCount += Integer.valueOf(dataObj[1].toString());
			}	
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			//response.setHeader("Content-disposition","attachment; filename=RTF_active_inactive_customers." +Constants.EXCEL_EXTENSION);
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_ACTIVE_INACTIVE_CUSTOMERS+"." +Constants.EXCEL_EXTENSION); //Shiva Modified
			workbook.write(response.getOutputStream());              	
		} catch (Exception e) {			
			e.printStackTrace();
			
		}
	}
	
	
	@RequestMapping(value = "/GetFantasyGameCustomerCount")
	public void getFantasyGameCustomerCount(HttpServletRequest request, HttpServletResponse response){
		SXSSFWorkbook workbook = new SXSSFWorkbook();
		String fromDate = request.getParameter("fromDate");
		String toDate = request.getParameter("toDate");
		try{
			List<String> headerList = new ArrayList<String>();
			
			headerList = new ArrayList<String>();
			headerList.add("Contest ID");
			headerList.add("Contest Name");
			headerList.add("Contest Date");
			headerList.add("Registered Users on Fantasy Contest Date");
			headerList.add("Total Registered Users from 1st Fantasy Contest Date");
			headerList.add("Fantasy Contest Participated Users");
			headerList.add("Total Participated Users");
			
			Sheet ssSheet = workbook.createSheet("Contest");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<Object[]> list = QuizDAORegistry.getReportDAO().getFantasyContestUserCount(fromDate, toDate, sharedProperty);
			int j = 0; int rowCount = 1;
			for(Object[] dataObj : list){					
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;					
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[3], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[4], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[5], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[6], j, rowhead);j++;
			}	
		
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			//response.setHeader("Content-disposition","attachment; filename=RTF_fantasy_contest_participants." +Constants.EXCEL_EXTENSION);
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_FANTASY_CONTEST_PARTICIPANTS_COUNT+"." +Constants.EXCEL_EXTENSION);  //Shiva Modified
			workbook.write(response.getOutputStream());              	
		} catch (Exception e) {			
			e.printStackTrace();
			
		}
	}
	
	
	/*----// Shiva BEGINS of Adding New Report Controller Methods on 14 Oct 2019 //----*/ 
	
	@RequestMapping(value = "/DownloadFantasyFootballContestWinnersReport")
	public void getFantasyFootballContestWinnersReport(HttpServletRequest request, HttpServletResponse response){
		
		try{
			
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("Contest ID");
			headerList.add("Contest Name");
			headerList.add("Contest Date");
			headerList.add("Customer ID");
			headerList.add("User ID");
			headerList.add("eMail");
			headerList.add("No. Of Tickets Won");

			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet("FantasyFootballWinners");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			
			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getFantasyFootballContestWinnersDetailsReport(fromDate,toDate,sharedProperty);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0; int rowCount = 1;
			
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[2], j, rowhead,cellStyleWithHourMinute);j++;
				Util.getExcelIntegerCell(dataObj[3], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[4], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[5], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[6], j, rowhead);j++;
			}

			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_FANTASY_FOOTBALL_CONTEST_WINNERS+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
		
		} catch (Exception e) {
			e.printStackTrace();

		}
	}
	
	
	
	
	@RequestMapping({"/DownloadMonthlyCustomerParticipantsCountStatistics"})
	public void downloadMonthlyCustomerParticipantsCountStatistics(HttpServletRequest request, HttpServletResponse response){
		try{
			
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");

			// For Sheet 1
			List<String> headerList1 = new ArrayList<String>();
			headerList1.add("Month");
			headerList1.add("No. Of Games Played");
			headerList1.add("Participants Count");
			headerList1.add("Unique Participants Count");
			headerList1.add("Old User Participants Count");
			headerList1.add("Average Viewership");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet1 = workbook.createSheet("MonthlyParticipantsStatistics");
			ExcelUtil.generateExcelHeaderRow(headerList1, ssSheet1);

			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getMonthlyCustomerParticipantsCountStatisticsForSheetOne(fromDate,toDate,sharedProperty);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0; int rowCount = 1;
			Integer totalNoOfGamesPlayed = 0;
			Integer overAllParticipantsCount = 0;
			
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet1.createRow((int)rowCount);
				rowCount++;
				j = 0;
				Util.getExcelStringCell(dataObj[0], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[1], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[2], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[3], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[4], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[5], j, rowhead);j++;
				
				if(dataObj[1]!=null){
					totalNoOfGamesPlayed += Integer.parseInt(dataObj[1].toString());
				}
				if(dataObj[2]!=null){
					overAllParticipantsCount += Integer.parseInt(dataObj[2].toString());
				}
				
			}
			
			if (totalNoOfGamesPlayed > 0){
				rowCount++; 
				ssSheet1.createRow((int)rowCount);
				Row rowheadA =   ssSheet1.createRow((int)rowCount);
				Util.getExcelStringCell("Total No. Of Games Played", 0, rowheadA);
				Util.getExcelIntegerCell(totalNoOfGamesPlayed, 1, rowheadA);
			}
			if (overAllParticipantsCount > 0) {
				rowCount++; 
				ssSheet1.createRow((int)rowCount);
				Row rowheadB =   ssSheet1.createRow((int)rowCount);
				Util.getExcelStringCell("Overall Participants Count", 0, rowheadB);
				Util.getExcelIntegerCell(overAllParticipantsCount, 1, rowheadB);
			}
			
			
			// For Sheet 2
			List<String> headerList2 = new ArrayList<String>();
			headerList2.add("Month");
			headerList2.add("Contest ID");
			headerList2.add("Contest Name");
			headerList2.add("Contest Date");
			headerList2.add("No. Of Participants");
			
			Sheet ssSheet2 = workbook.createSheet("ContestWiseParticipantsCount");
			ExcelUtil.generateExcelHeaderRow(headerList2, ssSheet2);
			
			List<Object[]> dataListb = QuizDAORegistry.getContestsDAO().getMonthlyContestParticipantsCountStatisticsForSheetTwo(fromDate,toDate,sharedProperty);
			
			
			int k = 0; int rowCountk = 1;
			int totNoOfGamesPlayed = 0;
			overAllParticipantsCount = 0;
			
			
			for(Object[] dataObj : dataListb){
				
				Row rowhead =   ssSheet2.createRow((int)rowCountk);
				rowCountk++;
				k = 0;
				Util.getExcelStringCell(dataObj[0], k, rowhead);k++;
				Util.getExcelIntegerCell(dataObj[1], k, rowhead);k++;
				Util.getExcelStringCell(dataObj[2], k, rowhead);k++;
				Util.getExcelDateCell(dataObj[3], k, rowhead,cellStyleWithHourMinute);k++;
				Util.getExcelIntegerCell(dataObj[4], k, rowhead);k++;
				
				if(dataObj[1]!=null){
					totNoOfGamesPlayed++;
				}
				if(dataObj[4]!=null){
					overAllParticipantsCount += Integer.parseInt(dataObj[4].toString());
				}
				
			}
			
			if (totNoOfGamesPlayed > 0){
				rowCountk++; 
				ssSheet2.createRow((int)rowCountk);
				Row rowheadA =   ssSheet2.createRow((int)rowCountk);
				Util.getExcelStringCell("Total No. Of Games Played", 0, rowheadA);
				Util.getExcelIntegerCell(totNoOfGamesPlayed, 1, rowheadA);
			}
			if (overAllParticipantsCount > 0) {
				rowCountk++; 
				ssSheet2.createRow((int)rowCountk);
				Row rowheadB =   ssSheet2.createRow((int)rowCountk);
				Util.getExcelStringCell("Overall Participants Count", 0, rowheadB);
				Util.getExcelIntegerCell(overAllParticipantsCount, 1, rowheadB);
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_MONTHLY_CUSTOMER_PARTICIPANTS_COUNT_STATISTICS+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
			
		}catch (Exception e) {
			e.printStackTrace();

		}
	}
	
	
	@RequestMapping({"/DownloadOutstandingContestOrdersToBeFulfilledReport"})
	public void downloadOutstandingContestOrdersToBeFulfilledReport(HttpServletRequest request, HttpServletResponse response){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Order Year");
			headerList.add("Order Date");
			headerList.add("Order No");
			headerList.add("Order Quantity");
			headerList.add("Order Total");
			headerList.add("Customer Name");
			headerList.add("User ID");
			headerList.add("Email ID");
			headerList.add("Phone");
			headerList.add("Event Name");
			headerList.add("Event Date & Time");


			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet("OutstandingOrders");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);

			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getOutstandingContestOrdersToBeFulfilledReport(sharedProperty);

			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));

			int j = 0;
			int rowCount = 1;

			for(Object[] dataObj : dataList){

				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;

				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[2], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[3], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[4], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[5], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[6], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[7], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[8], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[9], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[10], j, rowhead);j++;

			}

			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_OUTSTANDING_CONTEST_ORDERS_TOBE_FULFILLED+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}


	@RequestMapping({"/DownloadCustomerAverageTimeAccessingRTFTrivia"})
	public void downloadCustomerAverageTimeAccessingRTFTrivia(HttpServletRequest request, HttpServletResponse response){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Customer ID");
			headerList.add("User ID");
			headerList.add("Signup Date");
			headerList.add("No. Of Days Accessed");
			headerList.add("No. Of Minutes Accessed");
			headerList.add("Average Minutes per Day");
			headerList.add("No. Of Contest Participated");
			headerList.add("Average Minutes per Contest");


			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet("AccessingRTFTrivia");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);

			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getCustomerAverageTimeAccessingRTFTriviaReport(sharedProperty);

			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyleWithMDYY = workbook.createCellStyle();
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyleWithMDYY.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));

			int j = 0;
			int rowCount = 1;

			for(Object[] dataObj : dataList){

				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;

				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[2], j, rowhead,cellStyleWithMDYY);j++;
				Util.getExcelIntegerCell(dataObj[3], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[4], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[5], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[6], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[7], j, rowhead);j++;

			}

			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_CUSTOMER_AVERAGE_TIME_ACCESSING_RTF_TRIVIA+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}



	@RequestMapping({"/DownloadMonthlyCustomerParticipantsCountBreakdownStatistics"})
	public void downloadMonthlyCustomerParticipantsCountBreakdownStatistics(HttpServletRequest request, HttpServletResponse response){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Month");
			headerList.add("Registered Users");
			headerList.add("No. Of Shows");
			headerList.add("Never Played / Viewed %");
			headerList.add("Never Played / Viewed");
			headerList.add("One Show Then InActive %");
			headerList.add("One Show Then InActive");
			headerList.add("One Show Per Month %");
			headerList.add("One Show Per Month");
			headerList.add("One Show Per Week %");
			headerList.add("One Show Per Week");
			headerList.add("One Show Per Day %");
			headerList.add("One Show Per Day");


			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet("ParticipantsStatistics");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);

			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getMonthlyCustomerParticipantsCountBreakdownStatistics(sharedProperty);

			int j = 0;
			int rowCount = 1;
			Integer totalRegisteredUsers = 0;
			Integer totalNoOfShows = 0;

			for(Object[] dataObj : dataList){

				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;

				Util.getExcelStringCell(dataObj[0], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[1], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[2], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[3], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[4], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[5], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[6], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[7], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[8], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[9], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[10], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[11], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[12], j, rowhead);j++;
				
				if (dataObj[1] != null){
					totalRegisteredUsers += Integer.parseInt(dataObj[1].toString());
				}
				if (dataObj[2] != null){
					totalNoOfShows += Integer.parseInt(dataObj[2].toString());
				}

			}
			
			if (totalRegisteredUsers > 0) {
				rowCount++; 
				ssSheet.createRow((int)rowCount);
				Row rowhead =   ssSheet.createRow((int)rowCount);
				Util.getExcelStringCell("Total Registered Users", 0, rowhead);
				Util.getExcelIntegerCell(totalRegisteredUsers, 1, rowhead);
			}
			if (totalNoOfShows > 0) {
				rowCount++; 
				ssSheet.createRow((int)rowCount);
				Row rowhead =   ssSheet.createRow((int)rowCount);
				Util.getExcelStringCell("Total No. Of Shows", 0, rowhead);
				Util.getExcelIntegerCell(totalNoOfShows, 1, rowhead);
			}

			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_MONTHLY_CUSTOMER_PARTICIPANTS_COUNT_BREAKDOWN_STATISTICS+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}



	@RequestMapping({"/DownloadRTFCustomerContestStatisticsforBusiness"})
	public void downloadRTFCustomerContestStatisticsforBusiness(HttpServletRequest request, HttpServletResponse response){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Heading");
			headerList.add("Value");

			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet("ForBusinessStatistics");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);

			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getRTFCustomerContestStatisticsforBusiness(sharedProperty);

			int j = 0;
			int rowCount = 1;

			for(Object[] dataObj : dataList){

				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;

				Util.getExcelStringCell(dataObj[0], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[1], j, rowhead);j++;

			}

			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_CUSTOMER_CONTEST_STATISTICS_FOR_BUSINESS+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}



	@RequestMapping(value = "/DownloadGiftCardPurchasedWinnersReport")
	public void downloadGiftCardPurchasedWinnersReport(HttpServletRequest request, HttpServletResponse response){
		
		try{
			
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("Customer ID");
			headerList.add("First Name");
			headerList.add("Last Name");
			headerList.add("User ID");
			headerList.add("eMail");
			headerList.add("Phone");
			headerList.add("SignUp Date");
			headerList.add("Gift Card Tittle");
			headerList.add("Gift Card Order Quantity");
			headerList.add("No. of Contest Played till 9 Questions");
			

			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet("GiftCardPurchased&Winners");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			
			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getGiftCardPurchasedWinnersReport(fromDate,toDate,sharedProperty);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0; int rowCount = 1;
			int totalCustomer = 0;
			Integer totalGCOQuantity = 0;
			
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[3], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[4], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[5], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[6], j, rowhead,cellStyleWithHourMinute);j++;
				Util.getExcelStringCell(dataObj[7], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[8], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[9], j, rowhead);j++;
				
				if (dataObj[0] != null){
					totalCustomer++;
				}
				if (dataObj[8] != null){
					totalGCOQuantity += Integer.parseInt(dataObj[8].toString());
				}
				
			}
			
			if (totalCustomer > 0){
				rowCount++; 
				ssSheet.createRow((int)rowCount);
				Row rowhead =   ssSheet.createRow((int)rowCount);
				Util.getExcelStringCell("Total Customer Count", 0, rowhead);
				Util.getExcelIntegerCell(totalCustomer, 1, rowhead);
			}
			if (totalGCOQuantity > 0){
				rowCount++; 
				ssSheet.createRow((int)rowCount);
				Row rowhead =   ssSheet.createRow((int)rowCount);
				Util.getExcelStringCell("Total Gift Card Order Quantity", 0, rowhead);
				Util.getExcelIntegerCell(totalGCOQuantity, 1, rowhead);
			}

			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_GIFT_CARD_PURCHASED_WINNERS+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
		
		} catch (Exception e) {
			e.printStackTrace();

		}
	}
	
	
	
	@RequestMapping({"/DownloadPollingStatistics"})
	public void downloadPollingStatistics(HttpServletRequest request, HttpServletResponse response){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Total Registered Users");
			headerList.add("Polling Start Date");
			headerList.add("No. of Times Polling Played");
			headerList.add("Unique Players Count");
			headerList.add("% of Users Playing");
			headerList.add("Average Players Per Day");
			headerList.add("Average Players Per Month");
			headerList.add("Average Polling Played per Day");
			headerList.add("Average Polling Played per Month");

			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet("PollingStatistics");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));

			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getPollingStatistics(sharedProperty);

			int j = 0;
			int rowCount = 1;

			for(Object[] dataObj : dataList){

				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;

				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[1], j, rowhead,cellStyle);j++;
				Util.getExcelIntegerCell(dataObj[2], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[3], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[4], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[5], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[6], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[7], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[8], j, rowhead);j++;

			}

			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_POLLING_STATISTICS+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}



	@RequestMapping({"/DownloadRTFTVStatistics"})
	public void downloadRTFTVStatistics(HttpServletRequest request, HttpServletResponse response){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Total Registered Users");
			headerList.add("RTF TV Start Date");
			headerList.add("No. of Times Viewed");
			headerList.add("Unique Viewers Count");
			headerList.add("% of Viewers");
			headerList.add("Average Viewers Per Day");
			headerList.add("Average Viewers Per Month");
			headerList.add("Average Videos Played per Day");
			headerList.add("Average Videos Played per Month");
			headerList.add("Average Videos Uploaded per Day");
			headerList.add("Average Videos Uploaded per Month");


			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet("RTF-TV-Statistics");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));

			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getRTFTVStatistics(sharedProperty);

			int j = 0;
			int rowCount = 1;

			for(Object[] dataObj : dataList){

				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;

				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[1], j, rowhead,cellStyle);j++;
				Util.getExcelIntegerCell(dataObj[2], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[3], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[4], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[5], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[6], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[7], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[8], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[9], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[10], j, rowhead);j++;

			}

			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_TV_STATISTICS+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}



	@RequestMapping(value = "/DownloadCustomerTimeSpentOnEachContest")
	public void downloadCustomerTimeSpentOnEachContest(HttpServletRequest request, HttpServletResponse response){
		
		try{
			
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("Contest ID");
			headerList.add("Contest Name");
			headerList.add("Contest Date");
			headerList.add("Customer ID");
			headerList.add("User ID");
			headerList.add("Minutes Spent On Contest");
			

			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet("TimeSpentOnEachContest");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			
			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getCustomerTimeSpentOnEachContest(fromDate,toDate,sharedProperty);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0; int rowCount = 1;
			
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[2], j, rowhead,cellStyleWithHourMinute);j++;
				Util.getExcelIntegerCell(dataObj[3], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[4], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[5], j, rowhead);j++;
				
			}

			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_CUSTOMER_TIME_SPENT_ON_EACH_CONTEST+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
		
		} catch (Exception e) {
			e.printStackTrace();

		}
	}
	
	
	
	@RequestMapping(value = "/DownloadRTFCustomerAverageTimeTakenperContestStatistics")
	public void downloadRTFCustomerAverageTimeTakenperContestStatistics(HttpServletRequest request, HttpServletResponse response){
		
		try{
			
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("Contest ID");
			headerList.add("Contest Name");
			headerList.add("Contest Date");
			headerList.add("Overall Participants Count");
			headerList.add("Overall Average Minutes Spent On Contest");
			headerList.add("Participants Count who Spent 0 to 5 Mins");
			headerList.add("Average Participants Count who Spent 0 to 5 Mins");
			headerList.add("Participants Count who Spent 5 to 10 Mins");
			headerList.add("Average Participants Count who Spent 5 to 10 Mins");
			headerList.add("Participants Count who Spent 10 to 15 Mins");
			headerList.add("Average Participants Count who Spent 10 to 15 Mins");
			headerList.add("Participants Count who Spent 15 Mins above");
			headerList.add("Average Participants Count who Spent 15 Mins above");
			headerList.add("Participants Count who Spent 0 to 11 Mins");
			headerList.add("Average Participants Count who Spent 0 to 11 Mins");
			headerList.add("Participants Count who Spent 11 Mins above");
			headerList.add("Average Participants Count who Spent 11 Mins above");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet("TimeSpentStatisticsOnEachContest");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			
			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getRTFCustomerAverageTimeTakenperContestStatistics(fromDate,toDate,sharedProperty);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0; int rowCount = 1;
			int totalContest = 0; //0
			Integer totalParticipants = 0; //3
			Double totalAverage = 0.00; //4
			Double totalAvgPrtiZtoFive = 0.00; //6
			Double totalAvgPrtiFivetoTen = 0.00; //8
			Double totalAvgPrtiTentoFifteen = 0.00; //10
			Double totalAvgPrtiFifteenAbove = 0.00; //12
			Double totalAvgPrtiZtoEleven = 0.00;  //14
			Double totalAvgPrtiElevenAbove = 0.00;  //16
			
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[2], j, rowhead,cellStyleWithHourMinute);j++;
				Util.getExcelIntegerCell(dataObj[3], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[4], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[5], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[6], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[7], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[8], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[9], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[10], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[11], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[12], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[13], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[14], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[15], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[16], j, rowhead);j++;
				
				if (dataObj[0] != null){
					totalContest++ ;
				}
				if (dataObj[3] != null){
					totalParticipants += Integer.parseInt(dataObj[3].toString());
				}
				if (dataObj[4] != null){
					totalAverage += Double.parseDouble(dataObj[4].toString());
				}
				if (dataObj[6] != null){
					totalAvgPrtiZtoFive += Double.parseDouble(dataObj[6].toString());
				}
				if (dataObj[8] != null){
					totalAvgPrtiFivetoTen += Double.parseDouble(dataObj[8].toString());
				}
				if (dataObj[10] != null){
					totalAvgPrtiTentoFifteen += Double.parseDouble(dataObj[10].toString());
				}
				if (dataObj[12] != null){
					totalAvgPrtiFifteenAbove += Double.parseDouble(dataObj[12].toString());
				}
				if (dataObj[14] != null){
					totalAvgPrtiZtoEleven += Double.parseDouble(dataObj[14].toString());
				}
				if (dataObj[16] != null){
					totalAvgPrtiElevenAbove += Double.parseDouble(dataObj[16].toString());
				}
			}
			
			if (totalContest > 0){
				rowCount++; 
				ssSheet.createRow((int)rowCount);
				Row rowhead =   ssSheet.createRow((int)rowCount);
				Util.getExcelStringCell("Total No. Of Contest:", 0, rowhead);
				Util.getExcelIntegerCell(totalContest, 1, rowhead);
				
				Util.getExcelStringCell("Total Participants:", 2, rowhead);
				if (totalParticipants > 0){
					Util.getExcelIntegerCell(totalParticipants, 3, rowhead);
				}
				if (totalAverage > 0){
					Double x = totalAverage/totalContest;
					Util.getExcelDecimalCell(x, 4, rowhead);
				}
				if (totalAvgPrtiZtoFive > 0){
					Double x = totalAvgPrtiZtoFive/totalContest;
					Util.getExcelDecimalCell(x, 6, rowhead);
				}
				if (totalAvgPrtiFivetoTen > 0){
					Double x = totalAvgPrtiFivetoTen/totalContest;
					Util.getExcelDecimalCell(x, 8, rowhead);
				}
				if (totalAvgPrtiTentoFifteen > 0){
					Double x = totalAvgPrtiTentoFifteen/totalContest;
					Util.getExcelDecimalCell(x, 10, rowhead);
				}
				if (totalAvgPrtiFifteenAbove > 0){
					Double x = totalAvgPrtiFifteenAbove/totalContest;
					Util.getExcelDecimalCell(x, 12, rowhead);
				}
				if (totalAvgPrtiZtoEleven > 0){
					Double x = totalAvgPrtiZtoEleven/totalContest;
					Util.getExcelDecimalCell(x, 14, rowhead);
				}
				if (totalAvgPrtiElevenAbove > 0){
					Double x = totalAvgPrtiElevenAbove/totalContest;
					Util.getExcelDecimalCell(x, 16, rowhead);
				}
			}

			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_CUSTOMER_AVERAGE_TIME_TAKEN_PER_CONTEST_STATISTICS+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
		
		} catch (Exception e) {
			e.printStackTrace();

		}
	}
	
	
	
	@RequestMapping(value = "/DownloadGrandMegaMINIJackpotTicketWinnerCount")
	public void downloadGrandMegaMINIJackpotTicketWinnerCount(HttpServletRequest request, HttpServletResponse response){
		
		try{
			
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("No. of Contest Played");
			headerList.add("Grand Winners Count");
			headerList.add("Grand Winner Ticket Count");
			headerList.add("Mega Jackpot Winners Count");
			headerList.add("Mega Jackpot Winners Ticket Count");
			headerList.add("Mini Jackpot Winners Count");
			headerList.add("Mini Jackpot Winners Ticket Count");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet("GrandMegaMiniWinCount");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			
			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getGrandMegaMINIJackpotTicketWinnerCount(fromDate,toDate,sharedProperty);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0; int rowCount = 1;
			
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[1], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[2], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[3], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[4], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[5], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[6], j, rowhead);j++;
				
			}

			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_GRAND_MEGA_MINI_JACKPOT_TICKET_WINNER_COUNT+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
		
		} catch (Exception e) {
			e.printStackTrace();

		}
	}
	
	
	
	@RequestMapping(value = "/DownloadCustomersUsingReferralCodesReport")
	public void downloadCustomersUsingReferralCodesReport(HttpServletRequest request, HttpServletResponse response){
		
		try{
			
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("User ID");
			headerList.add("Signup Date");
			headerList.add("Referral Code Used");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet("ReferralCodeUsed");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			
			List<Object[]> dataList = QuizDAORegistry.getContestsDAO().getCustomersUsingReferralCodesReport(fromDate,toDate,sharedProperty);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0; int rowCount = 1;
			int totalDFSKARMACount = 0;
			int totalRTFBWAYCount = 0;
			
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				Util.getExcelStringCell(dataObj[0], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[1], j, rowhead,cellStyleWithHourMinute);j++;
				Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
				
				if(dataObj[2]!=null){
					String cellValue = dataObj[2].toString();
					if(cellValue.equalsIgnoreCase("DFSKARMA")){
						totalDFSKARMACount++;
					}else if(cellValue.equalsIgnoreCase("RTFBWAY")){
						totalRTFBWAYCount++;
					}
				}
			}
			
			if (totalDFSKARMACount > 0){
				rowCount++; 
				ssSheet.createRow((int)rowCount);
				Row rowhead =   ssSheet.createRow((int)rowCount);
				Util.getExcelStringCell("Total DFSKARMA Used", 0, rowhead);
				Util.getExcelIntegerCell(totalDFSKARMACount, 1, rowhead);
			}
			if (totalRTFBWAYCount > 0){
				rowCount++; 
				ssSheet.createRow((int)rowCount);
				Row rowhead =   ssSheet.createRow((int)rowCount);
				Util.getExcelStringCell("Total RTFBWAY Used", 0, rowhead);
				Util.getExcelIntegerCell(totalRTFBWAYCount, 1, rowhead);
			}

			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_CUSTOMERS_USING_REFERRAL_CODE+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
		
		} catch (Exception e) {
			e.printStackTrace();

		}
	}
	

	
	@RequestMapping(value = "/DownloadGiftCardOrdersOutstandingPendingDetailedReport")
	public void downloadGiftCardOrdersOutstandingPendingDetailedReport(HttpServletRequest request, HttpServletResponse response){
		
		try{
			
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("Customer ID");
			headerList.add("Customer Name");
			headerList.add("User ID");
			headerList.add("eMail");
			headerList.add("Phone");
			headerList.add("Gift Card Title");
			headerList.add("Order ID");
			headerList.add("Order Date");
			headerList.add("Quantity");
			headerList.add("Cost");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			//Sheet 1 -- Gift Order Pending from RTF Side
			Sheet ssSheetA = workbook.createSheet("PendingFromRTF");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheetA);
			
			List<Object[]> dataListA = QuizDAORegistry.getContestsDAO().getGiftCardOrdersOutstandingDetails(fromDate,toDate,sharedProperty);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0; int rowCount = 1;
			int totalQuantity = 0;
			int totalCost = 0;
			
			for(Object[] dataObj : dataListA){
				
				Row rowhead =   ssSheetA.createRow((int)rowCount);
				rowCount++;
				j = 0;
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[3], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[4], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[5], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[6], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[7], j, rowhead,cellStyleWithHourMinute);j++;
				Util.getExcelIntegerCell(dataObj[8], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[9], j, rowhead);j++;
				
				if(dataObj[8]!=null){
					totalQuantity += Integer.parseInt(dataObj[8].toString());
				}
				if(dataObj[9]!=null){
					totalCost += Double.parseDouble(dataObj[9].toString());
				}
			}
			
			if (totalQuantity > 0){
				rowCount++; 
				ssSheetA.createRow((int)rowCount);
				Row rowhead =   ssSheetA.createRow((int)rowCount);
				Util.getExcelStringCell("Total: ", 0, rowhead);
				Util.getExcelIntegerCell(totalQuantity, 8, rowhead);
				Util.getExcelDecimalCell(totalCost, 9, rowhead);
			}

			//Sheet 2 -- Gift Order Pending from Customer Side
			Sheet ssSheetB = workbook.createSheet("PendingFromCustomer");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheetB);
			
			List<Object[]> dataListB = QuizDAORegistry.getContestsDAO().getGiftCardOrdersPendingDetails(fromDate,toDate,sharedProperty);
			
			j = 0; rowCount = 1;
			totalQuantity = 0;
			totalCost = 0;
			
			for(Object[] dataObj : dataListB){
				
				Row rowhead =   ssSheetB.createRow((int)rowCount);
				rowCount++;
				j = 0;
				Util.getExcelIntegerCell(dataObj[0], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[1], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[2], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[3], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[4], j, rowhead);j++;
				Util.getExcelStringCell(dataObj[5], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[6], j, rowhead);j++;
				Util.getExcelDateCell(dataObj[7], j, rowhead,cellStyleWithHourMinute);j++;
				Util.getExcelIntegerCell(dataObj[8], j, rowhead);j++;
				Util.getExcelDecimalCell(dataObj[9], j, rowhead);j++;
				
				if(dataObj[8]!=null){
					totalQuantity += Integer.parseInt(dataObj[8].toString());
				}
				if(dataObj[9]!=null){
					totalCost += Double.parseDouble(dataObj[9].toString());
				}
			}
			
			if (totalQuantity > 0){
				rowCount++; 
				ssSheetB.createRow((int)rowCount);
				Row rowhead =   ssSheetB.createRow((int)rowCount);
				Util.getExcelStringCell("Total: ", 0, rowhead);
				Util.getExcelIntegerCell(totalQuantity, 8, rowhead);
				Util.getExcelDecimalCell(totalCost, 9, rowhead);
			}

			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_GIFT_CARD_ORDERS_OUTSTANDING_PENDING_DETAILED+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
		
		} catch (Exception e) {
			e.printStackTrace();

		}
	}
	

	
	@RequestMapping({"/DownloadGiftCardOrdersOutstandingPendingCount"})
	public void downloadGiftCardOrdersOutstandingPendingCount(HttpServletRequest request, HttpServletResponse response){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Gift Card Title");
			headerList.add("$250");
			headerList.add("$200");
			headerList.add("$100");
			headerList.add("$50");
			headerList.add("$25");
			headerList.add("$15");
			headerList.add("$10");
			headerList.add("$5");
			headerList.add("Total");

			SXSSFWorkbook workbook = new SXSSFWorkbook();
			
			//Sheet 1 -- Gift Order Over All Pending Count
			Sheet ssSheetA = workbook.createSheet("OverallPendingCount");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheetA);
			
			List<Object[]> dataListA = QuizDAORegistry.getContestsDAO().getGiftCardOrdersOverallPendingCount(sharedProperty);

			int j = 0;
			int rowCount = 1;
			int tot250 = 0; int tot200 = 0; int tot100 = 0; int tot50 = 0;
			int tot25 = 0; int tot15 = 0; int tot10 = 0; int tot5 = 0;
			int totTotal = 0;

			for(Object[] dataObj : dataListA){

				Row rowhead =   ssSheetA.createRow((int)rowCount);
				rowCount++;
				j = 0;

				Util.getExcelStringCell(dataObj[0], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[1], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[2], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[3], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[4], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[5], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[6], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[7], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[8], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[9], j, rowhead);j++;
				
				if(dataObj[1]!=null){
					tot250 += Integer.parseInt(dataObj[1].toString());
				}
				if(dataObj[2]!=null){
					tot200 += Integer.parseInt(dataObj[2].toString());
				}
				if(dataObj[3]!=null){
					tot100 += Integer.parseInt(dataObj[3].toString());
				}
				if(dataObj[4]!=null){
					tot50 += Integer.parseInt(dataObj[4].toString());
				}
				if(dataObj[5]!=null){
					tot25 += Integer.parseInt(dataObj[5].toString());
				}
				if(dataObj[6]!=null){
					tot15 += Integer.parseInt(dataObj[6].toString());
				}
				if(dataObj[7]!=null){
					tot10 += Integer.parseInt(dataObj[7].toString());
				}
				if(dataObj[8]!=null){
					tot5 += Integer.parseInt(dataObj[8].toString());
				}
				if(dataObj[9]!=null){
					totTotal += Integer.parseInt(dataObj[9].toString());
				}
			}
			
			if (totTotal > 0){
				rowCount++; 
				ssSheetA.createRow((int)rowCount);
				Row rowhead =   ssSheetA.createRow((int)rowCount);
				Util.getExcelStringCell("Total: ", 0, rowhead);
				Util.getExcelIntegerCell(tot250, 1, rowhead);
				Util.getExcelIntegerCell(tot200, 2, rowhead);
				Util.getExcelIntegerCell(tot100, 3, rowhead);
				Util.getExcelIntegerCell(tot50, 4, rowhead);
				Util.getExcelIntegerCell(tot25, 5, rowhead);
				Util.getExcelIntegerCell(tot15, 6, rowhead);
				Util.getExcelIntegerCell(tot10, 7, rowhead);
				Util.getExcelIntegerCell(tot5, 8, rowhead);
				Util.getExcelIntegerCell(totTotal, 9, rowhead);
			}

			//Sheet 2 -- Gift Order Pending Count From RTF Side
			Sheet ssSheetB = workbook.createSheet("PendingCountFromRTF");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheetB);
			
			List<Object[]> dataListB = QuizDAORegistry.getContestsDAO().getGiftCardOrdersPendingFromRTFCount(sharedProperty);
			
			j = 0; rowCount = 1;
			tot250 = 0;  tot200 = 0;  tot100 = 0;  tot50 = 0;
			tot25 = 0;  tot15 = 0;  tot10 = 0;  tot5 = 0;
			totTotal = 0;

			for(Object[] dataObj : dataListB){

				Row rowhead = ssSheetB.createRow((int)rowCount);
				rowCount++;
				j = 0;

				Util.getExcelStringCell(dataObj[0], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[1], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[2], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[3], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[4], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[5], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[6], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[7], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[8], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[9], j, rowhead);j++;
				
				if(dataObj[1]!=null){
					tot250 += Integer.parseInt(dataObj[1].toString());
				}
				if(dataObj[2]!=null){
					tot200 += Integer.parseInt(dataObj[2].toString());
				}
				if(dataObj[3]!=null){
					tot100 += Integer.parseInt(dataObj[3].toString());
				}
				if(dataObj[4]!=null){
					tot50 += Integer.parseInt(dataObj[4].toString());
				}
				if(dataObj[5]!=null){
					tot25 += Integer.parseInt(dataObj[5].toString());
				}
				if(dataObj[6]!=null){
					tot15 += Integer.parseInt(dataObj[6].toString());
				}
				if(dataObj[7]!=null){
					tot10 += Integer.parseInt(dataObj[7].toString());
				}
				if(dataObj[8]!=null){
					tot5 += Integer.parseInt(dataObj[8].toString());
				}
				if(dataObj[9]!=null){
					totTotal += Integer.parseInt(dataObj[9].toString());
				}
			}
			
			if (totTotal > 0){
				rowCount++; 
				ssSheetB.createRow((int)rowCount);
				Row rowhead =   ssSheetA.createRow((int)rowCount);
				Util.getExcelStringCell("Total: ", 0, rowhead);
				Util.getExcelIntegerCell(tot250, 1, rowhead);
				Util.getExcelIntegerCell(tot200, 2, rowhead);
				Util.getExcelIntegerCell(tot100, 3, rowhead);
				Util.getExcelIntegerCell(tot50, 4, rowhead);
				Util.getExcelIntegerCell(tot25, 5, rowhead);
				Util.getExcelIntegerCell(tot15, 6, rowhead);
				Util.getExcelIntegerCell(tot10, 7, rowhead);
				Util.getExcelIntegerCell(tot5, 8, rowhead);
				Util.getExcelIntegerCell(totTotal, 9, rowhead);
			}
			
			
			//Sheet 3 -- Gift Order Pending Count From Customer Side
			Sheet ssSheetC = workbook.createSheet("PendingCountFromCustomer");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheetC);
			
			List<Object[]> dataListC = QuizDAORegistry.getContestsDAO().getGiftCardOrdersPendingFromCustomerCount(sharedProperty);

			j = 0; rowCount = 1;
			tot250 = 0;  tot200 = 0;  tot100 = 0;  tot50 = 0;
			tot25 = 0;  tot15 = 0;  tot10 = 0;  tot5 = 0;
			totTotal = 0;

			for(Object[] dataObj : dataListC){

				Row rowhead =   ssSheetC.createRow((int)rowCount);
				rowCount++;
				j = 0;

				Util.getExcelStringCell(dataObj[0], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[1], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[2], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[3], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[4], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[5], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[6], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[7], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[8], j, rowhead);j++;
				Util.getExcelIntegerCell(dataObj[9], j, rowhead);j++;
				
				if(dataObj[1]!=null){
					tot250 += Integer.parseInt(dataObj[1].toString());
				}
				if(dataObj[2]!=null){
					tot200 += Integer.parseInt(dataObj[2].toString());
				}
				if(dataObj[3]!=null){
					tot100 += Integer.parseInt(dataObj[3].toString());
				}
				if(dataObj[4]!=null){
					tot50 += Integer.parseInt(dataObj[4].toString());
				}
				if(dataObj[5]!=null){
					tot25 += Integer.parseInt(dataObj[5].toString());
				}
				if(dataObj[6]!=null){
					tot15 += Integer.parseInt(dataObj[6].toString());
				}
				if(dataObj[7]!=null){
					tot10 += Integer.parseInt(dataObj[7].toString());
				}
				if(dataObj[8]!=null){
					tot5 += Integer.parseInt(dataObj[8].toString());
				}
				if(dataObj[9]!=null){
					totTotal += Integer.parseInt(dataObj[9].toString());
				}
			}
			
			if (totTotal > 0){
				rowCount++; 
				ssSheetC.createRow((int)rowCount);
				Row rowhead =   ssSheetA.createRow((int)rowCount);
				Util.getExcelStringCell("Total: ", 0, rowhead);
				Util.getExcelIntegerCell(tot250, 1, rowhead);
				Util.getExcelIntegerCell(tot200, 2, rowhead);
				Util.getExcelIntegerCell(tot100, 3, rowhead);
				Util.getExcelIntegerCell(tot50, 4, rowhead);
				Util.getExcelIntegerCell(tot25, 5, rowhead);
				Util.getExcelIntegerCell(tot15, 6, rowhead);
				Util.getExcelIntegerCell(tot10, 7, rowhead);
				Util.getExcelIntegerCell(tot5, 8, rowhead);
				Util.getExcelIntegerCell(totTotal, 9, rowhead);
			}
			
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_GIFT_CARD_ORDERS_OUTSTANDING_PENDING_COUNT+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@RequestMapping({"/ticketMasterRefundsAmt"})
	public void loadTmPurchaseRefundsReport(HttpServletRequest request, HttpServletResponse response) throws Exception {
		XSSFWorkbook workbook = null;
		workbook = downloadTmPurchaseRefundsReport(workbook);
		workbook = createTmRefundsSheetUserwise(workbook);
		
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition","attachment; filename=TM_Purchase_Refunds." +Constants.EXCEL_EXTENSION);
		workbook.write(response.getOutputStream());
	}
	public XSSFWorkbook downloadTmPurchaseRefundsReport(XSSFWorkbook workbook) {
		
		try{
			if(workbook == null) {
				workbook = new XSSFWorkbook();
				createTmRefundsMainSheetData(workbook);
			}
			
				List<TicketMasterRefund> dataList = DAORegistry.getQueryManagerDAO().getTmPurchaseRefundsAmounts();
				
				Date processedDate = new Date();
				DAORegistry.getQueryManagerDAO().saveTmCcHoldersRefunds(dataList, processedDate);
			
			List<TicketMasterRefund> ccRefundsList = DAORegistry.getQueryManagerDAO().getAllTmCcHoldersRefunds();
			Map<String,Map<String,Map<Date, TicketMasterRefund>>> titleMap = new HashMap<String,Map<String,Map<Date, TicketMasterRefund>>>();
			List<Date> orderList = new ArrayList<Date>();
			List<String> nameList = new ArrayList<String>();
			
			List<String> titleList = new ArrayList<String>();
			//Map<String,List<String>> titleMap = new HashMap<String,List<String>>();
			
			for (TicketMasterRefund obj : ccRefundsList) {
				
				Map<String,Map<Date, TicketMasterRefund>> nameMap = titleMap.get(obj.getVenue());
				if(nameMap == null) {
					nameMap = new HashMap<String,Map<Date, TicketMasterRefund>>();
				}
				Date prosDate = obj.getProcessdDate();
				Map<Date, TicketMasterRefund> dateMap = nameMap.get(obj.getCcName());
				if(dateMap == null) {
					dateMap = new HashMap<Date, TicketMasterRefund>();	
				}
				dateMap.put(prosDate, obj);
				nameMap.put(obj.getCcName(), dateMap);
				titleMap.put(obj.getVenue(), nameMap);
				
				if(!orderList.contains(prosDate)) {
					orderList.add(prosDate);
				}
				if(!nameList.contains(obj.getCcName())) {
					nameList.add(obj.getCcName());
				}
				if(!titleList.contains(obj.getVenue())) {
					titleList.add(obj.getVenue());
				}
			}
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("Name");
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			for (Date prosDate :orderList) {
				headerList.add(dateFormat.format(prosDate));
			}
			Sheet ssSheet = workbook.createSheet("Broken down by CC holder");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			int j = 0; int rowCount = 1;
			int k = 0;
			Double totAmt= 0.0;
			
			
			Map<Date,Double> grandTotAmtMap = new HashMap<Date,Double>();
			
			boolean firstFlag= true;
			for (String title : titleList) {
				Map<Date,Double> totAmtMap = new HashMap<Date,Double>();
				Map<String,Map<Date, TicketMasterRefund>> nameMap = titleMap.get(title);
				if(nameMap != null) {
					Row rowhead = ssSheet.createRow((int)rowCount);
					//rowCount++;
					Util.getExcelStringCell(title.replace("ZZZ ", ""), 0, rowhead);
					
					for (String name : nameList) {
						
						
						Map<Date, TicketMasterRefund> dateMap = nameMap.get(name);
						if(dateMap == null || dateMap.isEmpty()) {
							continue;
						}
						j = 0;
						rowCount++;
						rowhead = ssSheet.createRow((int)rowCount);
						Util.getExcelStringCell(name, j, rowhead);j++;
						
						for (Date prosDate : orderList) {
							TicketMasterRefund obj = null;
							if(dateMap != null) {
								obj = dateMap.get(prosDate);
							}
							if(obj != null) {
								Util.getExcelDecimalCell(obj.getRefundCost(), j, rowhead);j++;
								Double amt = totAmtMap.get(prosDate);
								if(amt == null) {
									amt = 0.0;
								}
								amt += obj.getRefundCost();
								totAmtMap.put(prosDate, amt);
								
							} else {
								Util.getExcelDecimalCell(0, j, rowhead);j++;
							}
						}
						
					}
					rowCount++;
					rowCount++;
					ssSheet.createRow((int)rowCount);
					rowhead =   ssSheet.createRow((int)rowCount);
					j=0;
					Util.getExcelStringCell("Total", j, rowhead);j++;
					for (Date prosDate : orderList) {
					
						Double amt = totAmtMap.get(prosDate);
						if(amt == null) {
							amt =0.0;
						}
						Util.getExcelDecimalCell(amt, j, rowhead);j++;
						
						Double grandtotAmt = grandTotAmtMap.get(prosDate);
						if(grandtotAmt == null) {
							grandtotAmt = 0.0;
						}
						grandtotAmt += amt;
						grandTotAmtMap.put(prosDate,grandtotAmt);
					}
					rowCount++;
					rowCount++;
					
				}
			}
			
			if(!grandTotAmtMap.isEmpty()) {
				rowCount++;
				ssSheet.createRow((int)rowCount);
				Row rowhead =   ssSheet.createRow((int)rowCount);
				j=0;
				Util.getExcelStringCell("Grand Total", j, rowhead);j++;
				for (Date prosDate : orderList) {
				
					Double amt = grandTotAmtMap.get(prosDate);
					if(amt == null) {
						amt =0.0;
					}
					Util.getExcelDecimalCell(amt, j, rowhead);j++;
				}
			}
			
			/*String title="";
			
			for (String name : nameList) {
				
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				Util.getExcelStringCell(name, j, rowhead);j++;
				Map<String,TicketMasterRefund> objMap = tempMap.remove(name);
				
				for (Date prosDate : orderList) {
					TicketMasterRefund obj = null;
					if(objMap != null) {
						 obj = objMap.get(prosDate);
					}
					if(obj != null) {
						Util.getExcelDecimalCell(obj.getRefundCost(), j, rowhead);j++;	
					} else {
						Util.getExcelDecimalCell(0, j, rowhead);j++;
					}
					
				}
				
			}
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0; int rowCount = 1;
			Double totAmt= 0.0;
			String title="";
			boolean firstFlag= true;
			for(TicketMasterRefund tg : dataList){
				
				if(!title.equals(tg.getVenue())) {
					if(!firstFlag) {
						if (totAmt > 0){
							rowCount++;
							ssSheet.createRow((int)rowCount);
							Row rowhead =   ssSheet.createRow((int)rowCount);
							Util.getExcelStringCell("Total", 0, rowhead);
							Util.getExcelDecimalCell(totAmt, 1, rowhead);
							
							rowCount++;
							//rowCount++;
						}
						totAmt = 0.0;
					}
					firstFlag = false;
					
					title = tg.getVenue();
					Row rowhead = ssSheet.createRow((int)rowCount);
					rowCount++;
					rowCount++;
					j = 0;
					Util.getExcelStringCell(title.replace("ZZZ ", ""), j, rowhead);j++;
				}
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				Util.getExcelStringCell(tg.getCcName(), j, rowhead);j++;
				Util.getExcelDecimalCell(tg.getRefundCost(), j, rowhead);j++;
				totAmt += tg.getRefundCost();
			}
			
			if (totAmt > 0){
				rowCount++;
				rowCount++;
				ssSheet.createRow((int)rowCount);
				Row rowhead =   ssSheet.createRow((int)rowCount);
				Util.getExcelStringCell("Total", 0, rowhead);
				Util.getExcelDecimalCell(totAmt, 1, rowhead);
			}*/
			
			//CC Refund by user
			headerList = new ArrayList<String>();
			headerList.add("Name Category");
			headerList.add("Name");
			headerList.add("Refunds Due");
			headerList.add("Credits");
			headerList.add("CC Balance");
			headerList.add("Credit Balance");
			headerList.add("Percentage Refund Credited");
			Sheet refSheet = workbook.createSheet("REFUNDS By CC Holder");
			ExcelUtil.generateExcelHeaderRow(headerList, refSheet);
			createHelper = workbook.getCreationHelper();
			
			List<Object[]> refByHolders = DAORegistry.getTicketMasterRefundDAO().getTMRefundByCCUsers();
			j = 0; rowCount = 1;
			Double refTotal=0.0, crTotal = 0.0, balTotal = 0.0, crBalTotal = 0.0;
			for(Object[] obj : refByHolders){
				Row rowhead =   refSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				Util.getExcelStringCell(obj[0], j, rowhead);j++;
				Util.getExcelStringCell(obj[1], j, rowhead);j++;
				Util.getExcelDecimalCell(obj[2], j, rowhead);j++;
				Util.getExcelDecimalCell(obj[3], j, rowhead);j++;
				Util.getExcelDecimalCell(obj[4], j, rowhead);j++;
				Util.getExcelDecimalCell(obj[5], j, rowhead);j++;
				Util.getExcelDecimalCell(obj[6], j, rowhead);j++;
				if(obj[2]!=null){
					refTotal += Double.parseDouble(String.valueOf(obj[2]));
				}
				if(obj[3]!=null){
					crTotal += Double.parseDouble(String.valueOf(obj[3]));
				}
				if(obj[4]!=null){
					balTotal += Double.parseDouble(String.valueOf(obj[4]));
				}
				if(obj[5]!=null){
					crBalTotal += Double.parseDouble(String.valueOf(obj[5]));
				}
				
			}
			rowCount++;j=0;
			Row totalRow =   refSheet.createRow((int)rowCount);
			Util.getExcelStringCell("TOTAL", j, totalRow);j++;
			Util.getExcelStringCell("", j, totalRow);j++;
			Util.getExcelDecimalCell(refTotal, j, totalRow);j++;
			Util.getExcelDecimalCell(crTotal, j, totalRow);j++;
			Util.getExcelDecimalCell(balTotal, j, totalRow);j++;
			Util.getExcelDecimalCell(crBalTotal, j, totalRow);j++;
			Util.getExcelDecimalCell((crTotal*100)/refTotal, j, totalRow);j++;
			
			
			
			headerList = new ArrayList<String>();
			headerList.add("Invoice Date");
			headerList.add("Ticket Cost");
			headerList.add("PO Id");
			headerList.add("NameOnCC");
			headerList.add("PO Date");
			headerList.add("Vendor");
			headerList.add("Event");
			headerList.add("Event Date");
			headerList.add("Venue");
			headerList.add("Ticket Qty");
			headerList.add("Refund Qty");
			headerList.add("Refund Cost");
			headerList.add("Section");
			headerList.add("Row");
			headerList.add("Internal Ticket Notes");
			headerList.add("email");
			headerList.add("OLDemail");
			headerList.add("ErrorsFixedbyHand");
			Sheet ssSheet2 = workbook.createSheet("Unknown Emails");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet2);
			
			List<TicketMasterRefund> unknownList = DAORegistry.getQueryManagerDAO().getTmPurchaseRefundsUnknownData();
			
			 createHelper = workbook.getCreationHelper();
			j = 0; rowCount = 1;
			for(TicketMasterRefund obj : unknownList){
				//Invoice Date	Ticket Cost	PO Id	NameOnCC	PO Date	Vendor	Event	Event Date	Venue	Ticket Qty	Refund Qty	Refund Cost	Section	Row	
				//Internal Ticket Notes	email	OLDemail

				
				Row rowhead =   ssSheet2.createRow((int)rowCount);
				rowCount++;
				j = 0;
				Util.getExcelStringCell(obj.getInvoiceDate(), j, rowhead);j++;
				Util.getExcelDecimalCell(obj.getTicketCost(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getPoId(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getCcName(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getPoDate(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getVendor(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getEventName(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getEventDate(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getVenue(), j, rowhead);j++;
				Util.getExcelIntegerCell(obj.getTicketQty(), j, rowhead);j++;
				Util.getExcelIntegerCell(obj.getRefundQty(), j, rowhead);j++;
				Util.getExcelDecimalCell(obj.getRefundCost(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getSection(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getRow(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getInternalNotes(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getEmail(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getOldEmail(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getError(), j, rowhead);j++;
				
			}
			
			headerList = new ArrayList<String>();
			headerList.add("Name");
			headerList.add("Email");
			Sheet ssSheet1 = workbook.createSheet("Email List By Names");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet1);
			
			 dataList = DAORegistry.getQueryManagerDAO().getTmPurchaseRefundsEmailList();
			
			 createHelper = workbook.getCreationHelper();
			j = 0; rowCount = 1;
			for(TicketMasterRefund tg : dataList){
				
				Row rowhead =   ssSheet1.createRow((int)rowCount);
				rowCount++;
				j = 0;
				Util.getExcelStringCell(tg.getCcName(), j, rowhead);j++;
				Util.getExcelStringCell(tg.getEmail(), j, rowhead);j++;
				//totAmt += tg.getPrice();
			}
			createDuplicateRefundsData(workbook);
		
		} catch (Exception e) {
			e.printStackTrace();

		}
		return workbook;
	}
	public static XSSFWorkbook createTmRefundsMainSheetData(XSSFWorkbook workbook) throws Exception {
		
		try{
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			int j = 0; int rowCount = 1;
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("Invoice Date");
			headerList.add("Ticket Cost");
			headerList.add("PO Id");
			headerList.add("NameOnCC");
			headerList.add("PO Date");
			headerList.add("Vendor");
			headerList.add("Event");
			headerList.add("Event Date");
			headerList.add("Venue");
			headerList.add("Ticket Qty");
			headerList.add("Refund Qty");
			headerList.add("Refund Cost");
			headerList.add("Section");
			headerList.add("Row");
			headerList.add("Internal Ticket Notes");
			headerList.add("email");
			headerList.add("OLDemail");
			headerList.add("ErrorsFixedbyHand");
			Sheet ssSheet2 = workbook.createSheet("REFUNDS");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet2);
			
			Collection<TicketMasterRefund> dataList = DAORegistry.getTicketMasterRefundDAO().getAll();
			
			 createHelper = workbook.getCreationHelper();
			j = 0; rowCount = 1;
			for(TicketMasterRefund obj : dataList){
				//Invoice Date	Ticket Cost	PO Id	NameOnCC	PO Date	Vendor	Event	Event Date	Venue	Ticket Qty	Refund Qty	Refund Cost	Section	Row	
				//Internal Ticket Notes	email	OLDemail

				
				Row rowhead =   ssSheet2.createRow((int)rowCount);
				rowCount++;
				j = 0;
				Util.getExcelStringCell(obj.getInvoiceDate(), j, rowhead);j++;
				Util.getExcelDecimalCell(obj.getTicketCost(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getPoId(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getCcName(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getPoDate(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getVendor(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getEventName(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getEventDate(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getVenue(), j, rowhead);j++;
				Util.getExcelIntegerCell(obj.getTicketQty(), j, rowhead);j++;
				Util.getExcelIntegerCell(obj.getRefundQty(), j, rowhead);j++;
				Util.getExcelDecimalCell(obj.getRefundCost(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getSection(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getRow(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getInternalNotes(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getEmail(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getOldEmail(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getError(), j, rowhead);j++;
				
			}
			
			

		
		} catch (Exception e) {
			e.printStackTrace();

		}
		return workbook;
	
	}
	
	
	@RequestMapping(value = "/UploadPORefundFile")
	public synchronized String  uploadPORefundFile(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {		
		String message = "";
		String msg = "";
		XSSFWorkbook wb = null;
		try {
			File f = null;
			DiskFileItemFactory  fileItemFactory = new DiskFileItemFactory ();
			File ticketFile = new File(URLUtil.TICKET_DIRECTORY);
	        fileItemFactory.setRepository(ticketFile);
	        ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
	        List items = uploadHandler.parseRequest(request);
	        Iterator iterator = items.iterator();
	        List<TicketMasterRefundKnownEmail> refunds = new ArrayList<TicketMasterRefundKnownEmail>();
	        List<TicketMasterRefundCCNotMatched> refunds1 = new ArrayList<TicketMasterRefundCCNotMatched>();
	        String ext ="";
	        boolean isQtyMissing = false;
	        boolean isRefundMiss = false;
	        	        
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			Date today = new Date();
			String fileName = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(today);
			fileName = fileName.replaceAll(":", "_");
			fileName = fileName.replaceAll("/", "_");
			fileName = fileName.replaceAll(" ", "_");
			if(msg.isEmpty()) {
				MultipartFile file = multipartRequest.getFile("poRefundFile");
				if(file!=null){
					ext = FilenameUtils.getExtension(file.getOriginalFilename());
					f = new File(URLUtil.REPORT_TEMPLATE_DIRECTORY+"PO_REFUND_"+fileName+"."+ext);
					f.createNewFile();
					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(f));
			        FileCopyUtils.copy(file.getInputStream(), stream);
					stream.close();
				}else{
					msg="File not found";
					model.addAttribute("msg", msg);
					return "";
				}
			}
			
			TicketMasterRefundKnownEmail ref = null;
			TicketMasterRefundCCNotMatched ref1 = null;
			if(msg.isEmpty() && f!=null && f.length() > 0){
				if(ext.equalsIgnoreCase("xls") || ext.equalsIgnoreCase("xlsx")){
					FileInputStream stream = new FileInputStream(f);
					wb = new XSSFWorkbook(stream);
					Boolean isFirstTime1 = true;
					Boolean isFirstTime2 = true;
					for(Sheet sheet : wb){
						if(sheet == null){
							continue;
						}
						Iterator<Row> it = sheet.iterator();
						int cnt = 0;
						int rowNo = -1;
						Boolean isNewSet = false;
						Boolean lastMerge = false;
						String poId = "";
						 int cellCnt = -1;
						while (it.hasNext()) {
				            Row nextRow = it.next();
				            if(nextRow.getRowNum()==0 || nextRow.getRowNum()  == rowNo || nextRow.getRowNum() == (rowNo+1)){
				            	continue;
				            }
				            Iterator<Cell> cellIterator = nextRow.cellIterator();
				            if(isNewSet){
				            	if(nextRow.getRowNum() == (rowNo+2)){
				            		continue;
				            	}
				            	ref1 = new TicketMasterRefundCCNotMatched();
				            	 while (cellIterator.hasNext()) {
						                Cell cell = cellIterator.next();
						                switch (cell.getColumnIndex()){
					                	case 0:
					                		cell.setCellType(CellType.STRING);
					                		ref1.setNotMatched(cell.getStringCellValue());
					                		//System.out.println(cell.getStringCellValue());
					                		break;
					                	case 1:
					                		cell.setCellType(CellType.STRING);
					                		ref1.setCcRefundDate(cell.getStringCellValue());
					                		//System.out.println(cell.getStringCellValue());
					                		break;
					                	case 2:
					                		cell.setCellType(CellType.STRING);
					                		ref1.setCcName(cell.getStringCellValue());
					                		//System.out.println(cell.getStringCellValue());
					                		break;
					                	case 3:
					                		cell.setCellType(CellType.STRING);
					                		ref1.setCcHolderName(cell.getStringCellValue());
					                		//System.out.println(cell.getStringCellValue());
					                		break;
					                	case 4:
					                		cell.setCellType(CellType.STRING);
					                		ref1.setCcAccount(cell.getStringCellValue());
					                		//System.out.println(cell.getStringCellValue());
					                		break;
					                	case 5:
					                		try {
					                			if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC ){
					                				ref1.setCcRefundAmount(cell.getNumericCellValue());
					                				//System.out.println(cell.getNumericCellValue());
						                		}else{
						                			cell.setCellType(CellType.STRING);
						                			if(cell.getStringCellValue() == null || cell.getStringCellValue().isEmpty()){
						                				ref1.setCcRefundAmount(0.00);
						                			}else{
						                				ref1.setCcRefundAmount(Double.parseDouble(cell.getStringCellValue()));
						                			}
						                			//System.out.println(cell.getStringCellValue());
						                		}
											} catch (Exception e) {
												e.printStackTrace();
											}
					                		break;
					                	case 6:
					                		cell.setCellType(CellType.STRING);
					                		ref1.setCcExtendedDetails(cell.getStringCellValue());
					                		//System.out.println(cell.getStringCellValue());
					                		break;
					                	case 7:
					                		cell.setCellType(CellType.STRING);
					                		ref1.setCcStatementAppearance(cell.getStringCellValue());
					                		//System.out.println(cell.getStringCellValue());
					                		break;
					                	case 8:
					                		cell.setCellType(CellType.STRING);
					                		ref1.setCcAddress(cell.getStringCellValue());
					                		//System.out.println(cell.getStringCellValue());
					                		break;
					                	case 9:
					                		cell.setCellType(CellType.STRING);
					                		ref1.setCcReference(cell.getStringCellValue());
					                		//System.out.println(cell.getStringCellValue());
					                		break;
					                	case 10:
					                		cell.setCellType(CellType.STRING);
					                		ref1.setCcCategory(cell.getStringCellValue());
					                		//System.out.println(cell.getStringCellValue());
					                		break;
						                }
				            	 }
				            	 ref1.setMappedCCHolderName(sheet.getSheetName());
				            	 refunds1.add(ref1);
				            }else{
				            	 ref = new TicketMasterRefundKnownEmail();
				            	 while (cellIterator.hasNext()) {
						                Cell cell = cellIterator.next();
						                if(isNewSet){
						                	continue;
						                }
						                switch (cell.getColumnIndex()){
						                	case 0:
						                		cell.setCellType(CellType.STRING);
						                		ref.setName(cell.getStringCellValue());
						                		if(cell.getStringCellValue() == null || cell.getStringCellValue().trim().isEmpty()){
						                			rowNo = nextRow.getRowNum();
						                			isNewSet = true;
						                			System.out.println("NOT MATCHED ROW NO :"+rowNo);
						                			continue;
						                		}else if(cell.getStringCellValue().equalsIgnoreCase("NotMatched") || cell.getStringCellValue().equalsIgnoreCase("Not Matched")){
						                			rowNo = nextRow.getRowNum()-2;
						                			isNewSet = true;
						                			System.out.println("NOT MATCHED ROW NO :"+rowNo);
						                			continue;
						                		}
						                		///System.out.println(cell.getStringCellValue());
						                		break;
						                	case 1:
						                		cell.setCellType(CellType.STRING);
						                		ref.setCheckedEmail(cell.getStringCellValue());
						                		//System.out.println(cell.getStringCellValue());
						                		break;
						                	case 2:
						                		try {
						                			if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC ){
						                				ref.setRefundDue(cell.getNumericCellValue());
						                				//System.out.println(cell.getNumericCellValue());
							                		}else{
							                			cell.setCellType(CellType.STRING);
							                			if(cell.getStringCellValue() == null || cell.getStringCellValue().isEmpty()){
							                				ref.setRefundDue(0.00);
							                			}else{
							                				ref.setRefundDue(Double.parseDouble(cell.getStringCellValue()));
							                			}
							                			
							                			//System.out.println(cell.getStringCellValue());
							                		}
												} catch (Exception e) {
													ref.setRefundDue(0.00);
													e.printStackTrace();
													isRefundMiss = true;
												}
						                		break;
						                	case 3:
						                		try {
						                			CellRangeAddress range = Util.getMergedRegion(cell);
						                			if(range==null){
						                				lastMerge = false;
						                				if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC ){
							                				ref.setRefundRec(cell.getNumericCellValue());
							                				//System.out.println(cell.getNumericCellValue());
								                		}else{
								                			cell.setCellType(CellType.STRING);
								                			if(cell.getStringCellValue() == null || cell.getStringCellValue().isEmpty()){
								                				ref.setRefundRec(0.00);
								                			}else{
								                				ref.setRefundRec(Double.parseDouble(cell.getStringCellValue()));
								                			}
								                			
								                			//System.out.println(cell.getStringCellValue());
								                		}
						                				cnt++;
						                			}else{
						                				if(!lastMerge){
						                					cellCnt = range.getNumberOfCells();
						                					cellCnt--;
						                					cnt++;
						                					if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC ){
								                				ref.setRefundRec(cell.getNumericCellValue());
								                				//System.out.println(cell.getNumericCellValue());
									                		}else{
									                			cell.setCellType(CellType.STRING);
									                			if(cell.getStringCellValue() == null || cell.getStringCellValue().isEmpty()){
									                				ref.setRefundRec(0.00);
									                			}else{
									                				ref.setRefundRec(Double.parseDouble(cell.getStringCellValue()));
									                			}
									                			///System.out.println(cell.getStringCellValue());
									                			
									                		}
						                					lastMerge = true;
						                					//System.out.println("MERGE : "+cnt+" == "+cellCnt+" == "+lastMerge+" == "+ref.getRefundRec());
						                				}else if(cellCnt > 0){
						                					ref.setRefundRec(0.00);
						                					cellCnt--;
						                					//System.out.println("CNT : "+cnt+" == "+cellCnt+" == "+lastMerge+" == "+ref.getRefundRec());
						                				}else{
						                					cellCnt = range.getNumberOfCells();
						                					cellCnt--;
						                					cnt++;
						                					if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC ){
								                				ref.setRefundRec(cell.getNumericCellValue());
								                				//System.out.println(cell.getNumericCellValue());
									                		}else{
									                			cell.setCellType(CellType.STRING);
									                			if(cell.getStringCellValue() == null || cell.getStringCellValue().isEmpty()){
									                				ref.setRefundRec(0.00);
									                			}else{
									                				ref.setRefundRec(Double.parseDouble(cell.getStringCellValue()));
									                			}
									                			///System.out.println(cell.getStringCellValue());
									                		}
						                					//System.out.println("ELSE : "+cnt+" == "+cellCnt+" == "+lastMerge+" == "+ref.getRefundRec());
						                				}
						                				//System.out.println("LAST : "+cnt+" == "+cellCnt+" == "+lastMerge+" == "+ref.getRefundRec());
						                			}
						                			ref.setMergeRec(cnt);
												} catch (Exception e) {
													ref.setRefundRec(0.00);
													e.printStackTrace();
												}
						                		break;
						                	case 4:
						                		cell.setCellType(CellType.STRING);
						                		ref.setCcName(cell.getStringCellValue());
						                		//System.out.println(cell.getStringCellValue());
						                		break;
						                	case 5:
						                		cell.setCellType(CellType.STRING);
						                		try {
													Date date = DateUtil.getJavaDate(Double.parseDouble(cell.getStringCellValue()));
													ref.setInvoiceDate(Util.getTimeStamp(date));
												} catch (Exception e) {
													ref.setInvoiceDate(cell.getStringCellValue());
												}
						                		//System.out.println(cell.getStringCellValue());
						                		break;
						                	case 6:
						                		cell.setCellType(CellType.STRING);
						                		try {
													Date date = DateUtil.getJavaDate(Double.parseDouble(cell.getStringCellValue()));
													ref.setPoDate(Util.getTimeStamp(date));
												} catch (Exception e) {
													ref.setPoDate(cell.getStringCellValue());
												}
						                		///System.out.println(cell.getStringCellValue());
						                		break;
						                	case 7:
						                		cell.setCellType(CellType.STRING);
						                		ref.setPoId(cell.getStringCellValue());
						                		//System.out.println(cell.getStringCellValue());
						                		break;
						                	case 8:
						                		cell.setCellType(CellType.STRING);
						                		ref.setEventName(cell.getStringCellValue());
						                		//System.out.println(cell.getStringCellValue());
						                		break;
						                	case 9:
						                		cell.setCellType(CellType.STRING);
						                		try {
													Date date = DateUtil.getJavaDate(Double.parseDouble(cell.getStringCellValue()));
													ref.setEventDate(Util.getTimeStamp(date));
												} catch (Exception e) {
													ref.setEventDate(cell.getStringCellValue());
												}
						                		//System.out.println(cell.getStringCellValue());
						                		break;
						                	case 10:
						                		cell.setCellType(CellType.STRING);
						                		ref.setVenue(cell.getStringCellValue());
						                		//System.out.println(cell.getStringCellValue());
						                		break;
						                	case 11:
						                		try {
						                			cell.setCellType(CellType.STRING);
						                			if(cell.getStringCellValue() == null || cell.getStringCellValue().isEmpty()){
						                				ref.setTicketQty(0);
						                			}else{
						                				ref.setTicketQty(Integer.parseInt(cell.getStringCellValue()));
						                			}
						                			
												} catch (Exception e) {
													e.printStackTrace();
													isQtyMissing = true;
												}
						                		break;
						                	case 12:
						                		try {
						                			cell.setCellType(CellType.STRING);
						                			if(cell.getStringCellValue() == null || cell.getStringCellValue().isEmpty()){
						                				ref.setRefundQty(0);
						                			}else{
						                				ref.setRefundQty(Integer.parseInt(cell.getStringCellValue()));
						                			}
						                			
												} catch (Exception e) {
													e.printStackTrace();
													isQtyMissing = true;
												}
						                		break;
						                	case 13:
						                		try {
						                			if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC ){
						                				ref.setRefundCost(cell.getNumericCellValue());
						                				//System.out.println(cell.getNumericCellValue());
							                		}else{
							                			cell.setCellType(CellType.STRING);
							                			if(cell.getStringCellValue() == null || cell.getStringCellValue().isEmpty()){
							                				ref.setRefundCost(0.00);
							                			}else{
							                				ref.setRefundCost(Double.parseDouble(cell.getStringCellValue()));
							                			}
							                			
							                			//System.out.println(cell.getStringCellValue());
							                		}
												} catch (Exception e) {
													e.printStackTrace();
												}
						                		break;
						                	case 14:
						                		cell.setCellType(CellType.STRING);
						                		String str2 = cell.getStringCellValue();
						                		if(str2 == null ||  str2.equalsIgnoreCase("null")){
						                			str2= "";
						                		}
						                		ref.setSection(str2);
						                		//System.out.println(cell.getStringCellValue());
						                		break;
						                	case 15:
						                		cell.setCellType(CellType.STRING);
						                		String str3 = cell.getStringCellValue();
						                		if(str3 == null ||  str3.equalsIgnoreCase("null")){
						                			str3= "";
						                		}
						                		ref.setRow(str3);
						                		//System.out.println(cell.getStringCellValue());
						                		break;
						                	case 16:
						                		cell.setCellType(CellType.STRING);
						                		String vendor = cell.getStringCellValue();
						                		if(vendor == null ||  vendor.equalsIgnoreCase("null")){
						                			vendor= "";
						                		}
						                		ref.setVendor(vendor);;
						                		//System.out.println(cell.getStringCellValue());
						                		break;
						                	case 17:
						                		cell.setCellType(CellType.STRING);
						                		String str1 = cell.getStringCellValue();
						                		if(str1 == null ||  str1.equalsIgnoreCase("null")){
						                			str1= "";
						                		}
						                		ref.setInternalNotes(str1);
						                		//System.out.println(cell.getStringCellValue());
						                		break;
						                	case 18:
						                		cell.setCellType(CellType.STRING);
						                		String str = cell.getStringCellValue();
						                		if(str == null ||  str.equalsIgnoreCase("null")){
						                			str= "";
						                		}
						                		ref.setEmail(str);
						                		//System.out.println(cell.getStringCellValue());
						                		break;
						                	case 19:
						                		cell.setCellType(CellType.STRING);
						                		String st18r = cell.getStringCellValue();
						                		if(st18r == null ||  st18r.equalsIgnoreCase("null")){
						                			st18r= "";
						                		}
						                		ref.setOldEmail(st18r);
						                		//System.out.println(cell.getStringCellValue());
						                		break;
						                	case 20:
						                		cell.setCellType(CellType.STRING);
						                		String str19 = cell.getStringCellValue();
						                		if(str19 == null ||  str19.equalsIgnoreCase("null")){
						                			str19= "";
						                		}
						                		ref.setError(str19);
						                		//System.out.println(cell.getStringCellValue());
						                		break;
						                }
						            }
						            ref.setNameCategory("All Our Cards");
						            if(!isNewSet){
						            	 refunds.add(ref);
						            }
				            	 }
				            }
						
							if(!refunds.isEmpty()){
					        	if(isRefundMiss){
					        		System.out.println(sheet.getSheetName()+"  == INVALID DATA REFUND DUE AMOUNT..");
					        		 msg = "Cannot upload data, Found invalid Refund Due amount in one or more rows.";
					        		 model.addAttribute("msg", msg);
					        		 return "";
					        	}else if(isQtyMissing){
					        		System.out.println(sheet.getSheetName()+"  == INVALID DATA TICKET/REFUND QTY..");
					        		 msg = "Cannot upload data, Found invalid Ticket quantity or Refund Quantity in one or more rows.";
					        		 model.addAttribute("msg", msg);
					        		 return "";
					        	}else{
					        		List<TicketMasterRefundKnownEmail> finalList =new  ArrayList<TicketMasterRefundKnownEmail>();
					        		for(TicketMasterRefundKnownEmail r : refunds){
					        			if(r.getName() == null || r.getName().trim().isEmpty()){
					        				continue;
					        			}
					        			finalList.add(r);
					        		}
					        		if(isFirstTime1){
					        			DAORegistry.getQueryManagerDAO().removeTicketMasterRefundKnownEmailData();
					        			isFirstTime1 = false;
					        		}
									DAORegistry.getTicketMasterRefundKnownEmailDAO().saveAll(finalList);
									System.out.println(sheet.getSheetName()+"  == "+finalList.size()+" RECORDS INSERTED IN CC TRANSACTION SHEET.");
									msg = "Data Uploaded successfully.";
									model.addAttribute("msg", msg);
					        	}
					        }else{
					        	System.out.println(sheet.getSheetName()+" == DATA NOT FOUND IN CC TRANSACTION SHEET.");
								msg = "No Data Found in File.";
								model.addAttribute("msg", msg);
					        }
							if(!refunds1.isEmpty()){
								List<TicketMasterRefundCCNotMatched> finalList =new  ArrayList<TicketMasterRefundCCNotMatched>();
				        		for(TicketMasterRefundCCNotMatched r : refunds1){
				        			if(r.getCcRefundAmount() == null){
				        				continue;
				        			}
				        			finalList.add(r);
				        		}
								if(isFirstTime2){
				        			DAORegistry.getQueryManagerDAO().removeTicketMasterRefundCCNotMatched();
				        			isFirstTime2 = false;
				        		}
								DAORegistry.getTicketMasterRefundCCNotMatchedDAO().saveAll(finalList);
								System.out.println(sheet.getSheetName()+"  == "+finalList.size()+" RECORDS INSERTED IN NOT MATCHED SHEET.");
								msg = "Data Uploaded successfully.";
								model.addAttribute("msg", msg);
							}else{
								System.out.println(sheet.getSheetName()+" == DATA NOT FOUND IN NOT MATCHED SHEET.");
							}
							isQtyMissing = false;
							isQtyMissing = false;
							refunds =  new ArrayList<TicketMasterRefundKnownEmail>();
							refunds1 = new ArrayList<TicketMasterRefundCCNotMatched>();
						}
					}
				}else{
					 msg = "Invalid file format, only xls or xlsx is supported.";
	        		 model.addAttribute("msg", msg);
	        		 return "";
				}
				DAORegistry.getQueryManagerDAO().reconcileRefundData();
				return "";
		} catch (Exception e) {			
			e.printStackTrace();
			model.addAttribute("msg", "Something went wrong while uploading refund file.");
		}
		return "";
	}
	
	
	@RequestMapping({"/downloadUserwiseTMRefund"})
	public void loadUserwiseTMRefunds(HttpServletRequest request, HttpServletResponse response) throws Exception {
		XSSFWorkbook workbook = null;
		workbook = createTmRefundsSheetUserwise(workbook);
		
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition","attachment; filename=TM_Userwise_Purchase_Refunds." +Constants.EXCEL_EXTENSION);
		workbook.write(response.getOutputStream());
	}
	
	
	public static XSSFWorkbook createTmRefundsSheetUserwise(XSSFWorkbook workbook) throws Exception {
		
		try{
			if(workbook == null) {
				workbook = new XSSFWorkbook();
			}
			CreationHelper createHelper = workbook.getCreationHelper();
			int j = 0; int rowCount = 1;
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("Name");
			headerList.add("Verified Email");
			headerList.add("Refund Due");
			headerList.add("Refund Received");
			headerList.add("NameOnCC");
			headerList.add("Invoice Date");
			headerList.add("PO Date");
			headerList.add("PO ID");
			headerList.add("Event");
			headerList.add("Event Date");
			headerList.add("Venue");
			headerList.add("Ticket Qty");
			headerList.add("Refund Qty");
			headerList.add("Refund Cost");
			headerList.add("Section");
			headerList.add("Row");
			headerList.add("Vendor");
			headerList.add("Internal Ticket Notes");
			headerList.add("Email");
			headerList.add("OLD email");
			headerList.add("Errors Fixed by Hand");
			
			List<String> users = DAORegistry.getTicketMasterRefundDAO().getAllUniqueUsers();
			CellStyle bgStyle = workbook.createCellStyle();
			bgStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
			bgStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			
			CellStyle bgHeaderStyle = workbook.createCellStyle();
			bgHeaderStyle.setFillForegroundColor(IndexedColors.AQUA.getIndex());
			bgHeaderStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			XSSFFont font = workbook.createFont();
			font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
			bgHeaderStyle.setFont(font);
			
			bgHeaderStyle.setTopBorderColor((short)1);
			bgHeaderStyle.setBottomBorderColor((short)1);
			int i = 0; 
			for(String user : users){
				Sheet ssSheet = workbook.createSheet(user);
				ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
				
				List<TicketMasterRefund> dataList = DAORegistry.getTicketMasterRefundDAO().getAllTMRecordsByUserName(user);
				
				createHelper = workbook.getCreationHelper();
				j = 0; rowCount = 1;
				Integer prevMergeRec = 0;
				int startRow =-1;
				int endRow = -1;
				Double refRecTotal = 0.00;
				List<Integer> rows = new ArrayList<Integer>();
				List<CellRangeAddress> mergeList = new ArrayList<CellRangeAddress>();
				for(TicketMasterRefund obj : dataList){
					Row rowhead =   ssSheet.createRow((int)rowCount);
					j = 0;
					Util.getExcelStringCell(obj.getName(), j, rowhead);j++;
					Util.getExcelStringCell(obj.getVerifiedEmail(), j, rowhead);j++;
					Util.getExcelDecimalCell(obj.getTicketCost(), j, rowhead);j++;
					Util.getExcelDecimalCell(obj.getRefReceived(), j, rowhead);j++;
					refRecTotal += obj.getRefReceived();
					//System.out.println(user+" : "+(rowCount)+" == "+obj.getMergRec() +" == "+obj.getTicketCost()+" == "+obj.getPoId());
					//mergeList.add(new CellRangeAddress(startRow,endRow,j-1,j-1));
					
					/*Boolean firstTime = true;
					int cnt = 0;
					for(TicketMasterRefund obj1 : dataList){
						if(!obj.equals(obj1) && obj.getMergRec() == obj1.getMergRec()){
							cnt = rowhead.getRowNum();
							if(firstTime){
								rows.add(rowhead.getRowNum());
								firstTime = false;
							}else{
								if(rows.size() > 1){
									rows.add(1, ++cnt);
								}else{
									rows.add(++cnt);
								}
							}
						}
					}*/
					Util.getExcelStringCell(obj.getCcName(), j, rowhead);j++;
					Util.getExcelStringCell(obj.getInvoiceDate(), j, rowhead);j++;
					Util.getExcelStringCell(obj.getPoDate(), j, rowhead);j++;
					Util.getExcelStringCell(obj.getPoId(), j, rowhead);j++;
					Util.getExcelStringCell(obj.getEventName(), j, rowhead);j++;
					Util.getExcelStringCell(obj.getEventDate(), j, rowhead);j++;
					Util.getExcelStringCell(obj.getVenue(), j, rowhead);j++;
					Util.getExcelIntegerCell(obj.getTicketQty(), j, rowhead);j++;
					Util.getExcelIntegerCell(obj.getRefundQty(), j, rowhead);j++;
					Util.getExcelDecimalCell(obj.getRefundCost(), j, rowhead);j++;
					Util.getExcelStringCell(obj.getSection(), j, rowhead);j++;
					Util.getExcelStringCell(obj.getRow(), j, rowhead);j++;
					Util.getExcelStringCell(obj.getVendor(), j, rowhead);j++;
					Util.getExcelStringCell(obj.getInternalNotes(), j, rowhead);j++;
					Util.getExcelStringCell(obj.getEmail(), j, rowhead);j++;
					Util.getExcelStringCell(obj.getOldEmail(), j, rowhead);j++;
					Util.getExcelStringCell(obj.getError(), j, rowhead);j++;
					
					if(prevMergeRec == obj.getMergRec() || prevMergeRec.equals(obj.getMergRec())){
						rows.add(rowhead.getRowNum()-1);
						rows.add(rowhead.getRowNum());
					}else if(rowCount == 2){
						if(obj.getMergRec() == dataList.get(0).getMergRec()){
							rows.add(1);
							rows.add(2);
						}
					}else{
						if(!rows.isEmpty() && rows.size()>1){
							System.out.println(ssSheet.getSheetName()+" : "+rows.get(0)+"  |  "+rows.get(rows.size()-1));
							ssSheet.addMergedRegion(new CellRangeAddress(rows.get(0),rows.get(rows.size()-1),3,3));
						}
						rows  = new ArrayList<Integer>();
					}
					prevMergeRec = obj.getMergRec();
					rowCount++;
					/*for(CellRangeAddress range : mergeList){
						System.out.println(ssSheet.getSheetName()+" : "+range.getFirstRow()+" | "+range.getLastRow()+" | "+range.getFirstColumn()+" | "+range.getLastColumn());
						if(range.getFirstRow() >=0){
							ssSheet.addMergedRegion(range);
						}
					}*/
				}
				
				
				j=0;
				Row totalRow =   ssSheet.createRow((int)rowCount);
				Util.getExcelStringCell("", j, totalRow);j++;
				Util.getExcelStringCell("", j, totalRow);j++;
				Util.getExcelStringCell("", j, totalRow);j++;
				Util.getExcelDecimalCell(refRecTotal, j, totalRow);j++;
				rowCount++;
				
				Collection<TicketMasterRefundCCNotMatched> list = DAORegistry.getTicketMasterRefundCCNotMatchedDAO().getAllUnMatchedRefundsByUser(user);
				if(list!=null && !list.isEmpty()){
					rowCount++;
					int headerCell = 0;
					Row secHeader =   ssSheet.createRow((int)rowCount);
					Util.getExcelHeaderCellWithBG("NotMatched",headerCell, secHeader, bgHeaderStyle);headerCell++;
					Util.getExcelHeaderCellWithBG("CC Refund Date",headerCell, secHeader, bgHeaderStyle);headerCell++;
					Util.getExcelHeaderCellWithBG("CC Name",headerCell, secHeader, bgHeaderStyle);headerCell++;
					Util.getExcelHeaderCellWithBG("CC Holder Name",headerCell, secHeader, bgHeaderStyle);headerCell++;
					Util.getExcelHeaderCellWithBG("CC Account",headerCell, secHeader, bgHeaderStyle);headerCell++;
					Util.getExcelHeaderCellWithBG("CC Refund Amount",headerCell, secHeader, bgHeaderStyle);headerCell++;
					Util.getExcelHeaderCellWithBG("CC Extended Details",headerCell, secHeader, bgHeaderStyle);headerCell++;
					Util.getExcelHeaderCellWithBG("CC Statement Appearence",headerCell, secHeader, bgHeaderStyle);headerCell++;
					Util.getExcelHeaderCellWithBG("CC Holder Address",headerCell, secHeader, bgHeaderStyle);headerCell++;
					Util.getExcelHeaderCellWithBG("CC Rference",headerCell, secHeader, bgHeaderStyle);headerCell++;
					Util.getExcelHeaderCellWithBG("CC Category",headerCell, secHeader, bgHeaderStyle);
					/*secHeader.createCell(headerCell).setCellValue("Not Matched");headerCell++;
					secHeader.createCell(headerCell).setCellValue("");headerCell++;
					secHeader.createCell(headerCell).setCellValue("");headerCell++;
					secHeader.createCell(headerCell).setCellValue("");headerCell++;
					secHeader.createCell(headerCell).setCellValue("");headerCell++;
					secHeader.createCell(headerCell).setCellValue("");headerCell++;
					secHeader.createCell(headerCell).setCellValue("");headerCell++;
					secHeader.createCell(headerCell).setCellValue("");headerCell++;
					secHeader.createCell(headerCell).setCellValue("");headerCell++;
					secHeader.createCell(headerCell).setCellValue("");headerCell++;
					secHeader.createCell(headerCell).setCellValue("");*/
					rowCount++;
					
					for(TicketMasterRefundCCNotMatched obj : list){
						Row rowhead =   ssSheet.createRow((int)rowCount);
						rowCount++;
						j = 0;
						Util.getExcelStringCellRed(obj.getNotMatched(), j, rowhead,bgStyle);j++;
						Util.getExcelStringCellRed(obj.getCcRefundDate(), j, rowhead,bgStyle);j++;
						Util.getExcelStringCellRed(obj.getCcName(), j, rowhead,bgStyle);j++;
						Util.getExcelStringCellRed(obj.getCcHolderName(), j, rowhead,bgStyle);j++;
						Util.getExcelStringCellRed(obj.getCcAccount(), j, rowhead,bgStyle);j++;
						Util.getExcelDecimalCellRed(obj.getCcRefundAmount(), j, rowhead,bgStyle);j++;
						Util.getExcelStringCellRed(obj.getCcExtendedDetails(), j, rowhead,bgStyle);j++;
						Util.getExcelStringCellRed(obj.getCcStatementAppearance(), j, rowhead,bgStyle);j++;
						Util.getExcelStringCellRed(obj.getCcAddress(), j, rowhead,bgStyle);j++;
						Util.getExcelStringCellRed(obj.getCcReference(), j, rowhead,bgStyle);j++;
						Util.getExcelStringCellRed(obj.getCcCategory(), j, rowhead,bgStyle);j++;
					}
				}
				
				
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return workbook;
	
	}
	
	
public static XSSFWorkbook createDuplicateRefundsData(XSSFWorkbook workbook) throws Exception {
		
		try{
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			int j = 0; int rowCount = 1;
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("Invoice Date");
			headerList.add("Ticket Cost");
			headerList.add("PO Id");
			headerList.add("NameOnCC");
			headerList.add("PO Date");
			headerList.add("Vendor");
			headerList.add("Event");
			headerList.add("Event Date");
			headerList.add("Venue");
			headerList.add("Ticket Qty");
			headerList.add("Refund Qty");
			headerList.add("Refund Cost");
			headerList.add("Section");
			headerList.add("Row");
			headerList.add("Internal Ticket Notes");
			headerList.add("email");
			headerList.add("OLDemail");
			headerList.add("ErrorsFixedbyHand");
			headerList.add("No. Of Duplicates");
			Sheet ssSheet2 = workbook.createSheet("Duplicate REFUND Records");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet2);
			
			Collection<TicketMasterRefund> dataList = DAORegistry.getQueryManagerDAO().getTmDuplicateRefundsData();
			
			 createHelper = workbook.getCreationHelper();
			j = 0; rowCount = 1;
			for(TicketMasterRefund obj : dataList){
				//Invoice Date	Ticket Cost	PO Id	NameOnCC	PO Date	Vendor	Event	Event Date	Venue	Ticket Qty	Refund Qty	Refund Cost	Section	Row	
				//Internal Ticket Notes	email	OLDemail

				
				Row rowhead =   ssSheet2.createRow((int)rowCount);
				rowCount++;
				j = 0;
				Util.getExcelStringCell(obj.getInvoiceDate(), j, rowhead);j++;
				Util.getExcelDecimalCell(obj.getTicketCost(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getPoId(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getCcName(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getPoDate(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getVendor(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getEventName(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getEventDate(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getVenue(), j, rowhead);j++;
				Util.getExcelIntegerCell(obj.getTicketQty(), j, rowhead);j++;
				Util.getExcelIntegerCell(obj.getRefundQty(), j, rowhead);j++;
				Util.getExcelDecimalCell(obj.getRefundCost(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getSection(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getRow(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getInternalNotes(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getEmail(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getOldEmail(), j, rowhead);j++;
				Util.getExcelStringCell(obj.getError(), j, rowhead);j++;
				Util.getExcelIntegerCell(obj.getNoOfDuplicates(), j, rowhead);j++;
				
			}
			
			

		
		} catch (Exception e) {
			e.printStackTrace();

		}
		return workbook;
	}
public static void computeTmRefundsAmot() throws Exception {
	
	List<TicketMasterRefund> dataList = DAORegistry.getQueryManagerDAO().getTmPurchaseRefundsAmounts();
	
	Date processedDate = new Date();
	DAORegistry.getQueryManagerDAO().saveTmCcHoldersRefunds(dataList, processedDate);
}


/*----// Shiva ENDS of Adding New Report Controller Methods on 24 Oct 2019 //----*/


}
