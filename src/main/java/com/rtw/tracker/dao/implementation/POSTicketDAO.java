package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.POSTicket;

public class POSTicketDAO extends HibernateDAO<Integer, POSTicket> implements com.rtw.tracker.dao.services.POSTicketDAO{
	
	public POSTicket getPOSTicketByTicketId(Integer ticketId) throws Exception{
		return findSingle("FROM POSTicket WHERE ticketId=? ", new Object[]{ticketId});
	}
	
	public POSTicket getPOSTicketByInvoiceId(Integer invoiceId) throws Exception{
		return findSingle("FROM POSTicket WHERE invoiceId=? ", new Object[]{invoiceId});
	}
	
	@Override
	public List<POSTicket> getPOSTicketByPurchaseOrderId(Integer purchaseOrderId, String productType) throws Exception{
		return find("FROM POSTicket WHERE purchaseOrderId=? AND productType=?", new Object[]{purchaseOrderId, productType});	
	}
	
	@Override
	public List<POSTicket> getMappedPOSTicketByPurchaseOrderId(Integer purchaseOrderId, String productType) throws Exception{
		return find("FROM POSTicket WHERE purchaseOrderId=? AND invoiceId>0 AND productType=?", new Object[]{purchaseOrderId, productType});
	}
	
	public List<POSTicket> getMappedPOSTicketByInvoiceId(Integer invoiceId) throws Exception{
		return find("FROM POSTicket WHERE invoiceId=? order by ticketGroupId,seatOrder", new Object[]{invoiceId});	
	}
	
	public List<POSTicket> getMappedPOSTicketByInvoiceIdWithPdt(Integer invoiceId, String productType) throws Exception{
		return find("FROM POSTicket WHERE invoiceId=? AND productType=? order by ticketGroupId,seatOrder", new Object[]{invoiceId, productType});	
	}
}
