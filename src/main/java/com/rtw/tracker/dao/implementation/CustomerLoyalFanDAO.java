package com.rtw.tracker.dao.implementation; 

import java.util.List;

import com.rtw.tracker.datas.CustomerLoyalFan;


public class CustomerLoyalFanDAO extends HibernateDAO<Integer,CustomerLoyalFan> implements com.rtw.tracker.dao.services.CustomerLoyalFanDAO {

 	
	public List<CustomerLoyalFan> getAll() {
		return find("FROM CustomerLoyalFan");	 
	}
	
	public List<CustomerLoyalFan> getAllLoyalFansByCustomerId(Integer customerId) {
		return find("FROM CustomerLoyalFan WHERE customerId = ? and status='ACTIVE' " ,new Object[]{customerId});
	}
		
	public CustomerLoyalFan getLoyalFansByCustomerIdByArtistId(Integer customerId,Integer artistId){
		return findSingle("FROM CustomerLoyalFan WHERE customerId = ? AND artistId = ? AND status='ACTIVE'" ,new Object[]{customerId,artistId});
	}
	
	public CustomerLoyalFan getActiveLoyalFanByCustomerId(Integer customerId){
		return findSingle("FROM CustomerLoyalFan WHERE customerId = ? AND status='ACTIVE'" ,new Object[]{customerId});
	}
	
}
