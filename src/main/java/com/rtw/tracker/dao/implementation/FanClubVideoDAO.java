package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.datas.FanClub;
import com.rtw.tracker.datas.FanClubVideo;
import com.rtw.tracker.utils.GridHeaderFilters;

public class FanClubVideoDAO extends HibernateDAO<Integer, FanClubVideo> implements com.rtw.tracker.dao.services.FanClubVideoDAO{

	private static Session fanClubSession;
	
	public static Session getFanClubSession() {
		return fanClubSession;
	}
	public static void setFanClubSession(Session fanClubSession) {
		FanClubVideoDAO.fanClubSession = fanClubSession;
	}
	
	@Override
	public List<FanClubVideo> getAllFanClubVideos(GridHeaderFilters filter, String status, Integer fanClubId,SharedProperty prop) {
		List<FanClubVideo> list =null;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT fv.id as id, fv.fanclub_mst_id as fanClubId, fv.customer_id as customerId, fv.video_title as title,");
			sql.append("fv.video_description as description, fv.video_url as videoUrl, fv.video_thumbnail_url as thumbnailUrl,");
			sql.append("fv.video_category_id as categoryId, fv.created_user_id as createdBy, fv.updated_user_id as updatedBy,");
			sql.append("fv.video_status as status, fv.created_date as createdDate,");//, fv.upload_date as uploadedDate
			sql.append("fv.updated_date as updatedDate, fv.blocked_by as blockBy, fv.blocked_date as blockDate,");
			sql.append("fv.blocked_reason as blockReason,vc.category_name as category, c.user_id as userId, c.email as email, c.phone as phone ");
			sql.append("FROM fanclub_videos_mst fv with(nolock) ");
			sql.append("left join "+prop.getDatabasAlias()+"customer c with(nolock) on fv.customer_id = c.id ");
			sql.append("left join "+prop.getDatabasAlias()+"polling_video_categories vc with(nolock) on fv.video_category_id=vc.id ");
			sql.append("WHERE fv.fanclub_mst_id = "+fanClubId);
			
			
			
			if(filter.getArtistName()!=null && !filter.getArtistName().isEmpty()){
				sql.append(" AND fv.video_title like '%"+filter.getArtistName()+"%' ");
			}
			if(filter.getEventDescription()!=null && !filter.getEventDescription().isEmpty()){
				sql.append(" AND fv.video_description like '%"+filter.getEventDescription()+"%' ");
			}
			if(filter.getCreatedBy()!=null && !filter.getCreatedBy().isEmpty()){
				sql.append(" AND fv.created_user_id like '%"+filter.getCreatedBy()+"%' ");
			}
			if(filter.getUpdatedBy()!=null && !filter.getUpdatedBy().isEmpty()){
				sql.append(" AND fv.updated_user_id like '%"+filter.getUpdatedBy()+"%' ");
			}
			if(filter.getStatus()!=null && !filter.getStatus().isEmpty()){
				sql.append(" AND fv.video_status like '%"+filter.getStatus()+"%' ");
			}
			if(filter.getUsername()!=null && !filter.getUsername().isEmpty()){
				sql.append(" AND c.user_id like '%"+filter.getUsername()+"%' ");
			}
			if(filter.getEmail()!=null && !filter.getEmail().isEmpty()){
				sql.append(" AND c.email like '%"+filter.getEmail()+"%' ");
			}
			if(filter.getPhone()!=null && !filter.getPhone().isEmpty()){
				sql.append(" AND c.phone like '%"+filter.getPhone()+"%' ");
			}
			if(filter.getCategory()!=null && !filter.getCategory().isEmpty()){
				sql.append(" AND vc.category_name like '%"+filter.getCategory()+"%' ");
			}
			/*if(filter.getPartipantCount()!=null){
				sql.append(" AND fm.no_of_members ="+filter.getPartipantCount());
			}*/
			
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql.append(" AND DATEPART(day, fv.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql.append(" AND DATEPART(month, fv.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql.append(" AND DATEPART(year, fv.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ");
				}
			}
			
			if(filter.getLastUpdatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					sql.append(" AND DATEPART(day, fv.updated_date) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql.append(" AND DATEPART(month, fv.updated_date) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql.append(" AND DATEPART(year, fv.updated_date) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ");
				}
			}
			
			fanClubSession = FanClubVideoDAO.getFanClubSession();
			if(fanClubSession==null || !fanClubSession.isOpen() || !fanClubSession.isConnected()){
				fanClubSession = getSession();
				FanClubVideoDAO.setFanClubSession(fanClubSession);
			}
			SQLQuery query = fanClubSession.createSQLQuery(sql.toString());
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("fanClubId", Hibernate.INTEGER);
			query.addScalar("customerId", Hibernate.INTEGER);
			query.addScalar("title", Hibernate.STRING);
			query.addScalar("description", Hibernate.STRING);
			query.addScalar("categoryId", Hibernate.INTEGER);
			query.addScalar("videoUrl", Hibernate.STRING);
			query.addScalar("thumbnailUrl", Hibernate.STRING);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("updatedBy", Hibernate.STRING);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("blockBy", Hibernate.STRING);
			query.addScalar("blockDate", Hibernate.TIMESTAMP);
			query.addScalar("blockReason", Hibernate.STRING);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);
			query.addScalar("userId", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("phone", Hibernate.STRING);
			query.addScalar("category", Hibernate.STRING);
			
			list = query.setResultTransformer(Transformers.aliasToBean(FanClubVideo.class)).list();
		} catch (Exception e) {
			if(fanClubSession != null && fanClubSession.isOpen()){
				fanClubSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
}
