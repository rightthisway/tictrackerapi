package com.rtw.tracker.dao.implementation;

import java.util.Collection;
import java.util.List;
import java.util.Properties;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.EnumType;

import com.rtw.tracker.datas.CategoryTicket;
import com.rtw.tracker.datas.CategoryTicketGroup;
import com.rtw.tracker.datas.EventDetails;
import com.rtw.tracker.datas.ProductType;
import com.rtw.tracker.datas.TicketStatus;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.PaginationUtil;

public class CategoryTicketGroupDAO extends HibernateDAO<Integer, CategoryTicketGroup> implements com.rtw.tracker.dao.services.CategoryTicketGroupDAO{


	public Collection<CategoryTicketGroup> getAllActiveCategoryTicketsbyEventId(Integer brokerId, Integer eventId, GridHeaderFilters filter, String pageNo) throws Exception {
		Session session = null;
		Properties paramProductType = new Properties();
		paramProductType.put("enumClass", ProductType.class.getCanonicalName());
		paramProductType.put("type", "12"); /*type 12 instructs to use the String representation of enum value*/
		Integer firstResult = 0;
		try{
		String sql = "select c.id as id, c.event_id as eventId, c.section as section, c.row as row, c.quantity as quantity, "+
				"c.price as price, c.tax_amount as taxAmount, c.section_range as sectionRange, c.row_range as rowRange, "+
				"c.seat_low as seatLow, c.seat_high as seatHigh, c.external_notes as externalNotes, c.internal_notes as internalNotes, "+
				"c.near_term_display_option as nearTermDisplayOption, c.marketplace_notes as marketPlaceNotes, c.broadcast as broadcast, "+
				"c.product_type as producttype, c.shipping_method_id as shippingMethodId, c.max_showing as maxShowing, sm.name as shippingMethod, "+
				"c.broker_id as brokerId FROM category_ticket_group c with(nolock) left join shipping_method sm with(nolock) on c.shipping_method_id = sm.id "+
				" WHERE c.event_id="+eventId+" and c.status='"+TicketStatus.ACTIVE+"'";
		
		if(brokerId != null && brokerId >0){
			sql += " AND c.broker_id = "+brokerId+" ";
		}
		if(filter.getSection() != null){				
			sql += " AND c.section like '%"+filter.getSection()+"%' ";
		}
		if(filter.getRow() != null){
			sql += " AND c.row like '%"+filter.getRow()+"%' ";
		}
		if(filter.getSeatLow() != null){
			sql += " AND c.seat_low like '%"+filter.getSeatLow()+"%' ";
		}
		if(filter.getSeatHigh() != null){
			sql += " AND c.seat_high like '%"+filter.getSeatHigh()+"%' ";
		}
		if(filter.getQuantity() != null){
			sql += " AND c.quantity = "+filter.getQuantity()+" ";
		}
		if(filter.getRetailPrice() != null){
			sql += " AND c.price = "+filter.getRetailPrice()+" ";
		}
		if(filter.getWholeSalePrice() != null){
			sql += " AND c.price = "+filter.getWholeSalePrice()+" ";
		}
		if(filter.getPrice() != null){
			sql += " AND c.price = "+filter.getPrice()+" ";
		}
		if(filter.getTaxAmount() != null){
			sql += " AND c.tax_amount = "+filter.getTaxAmount()+" ";
		}
		if(filter.getShippingMethod() != null){
			sql += " AND sm.name like '%"+filter.getShippingMethod()+"%' ";
		}
		if(filter.getNearTermDisplayOption() != null){
			sql += " AND c.near_term_display_option like '%"+filter.getNearTermDisplayOption()+"%' ";
		}
		if(filter.getSectionRange() != null){
			sql += " AND c.section_range like '%"+filter.getSectionRange()+"%' ";
		}
		if(filter.getRowRange() != null){
			sql += " AND c.row_range like '%"+filter.getRowRange()+"%' ";
		}
		if(filter.getExternalNotes() != null){
			sql += " AND c.external_notes like '%"+filter.getExternalNotes()+"%' ";
		}
		if(filter.getMarketPlaceNotes() != null){
			sql += " AND c.marketplace_notes like '%"+filter.getMarketPlaceNotes()+"%' ";
		}
		if(filter.getProductType() != null){
			sql += " AND c.product_type like '%"+filter.getProductType()+"%' "; 
		}
		if(filter.getBrokerId() != null){
			sql += " AND c.broker_id = "+filter.getBrokerId()+" ";
		}
		if(filter.getBroadcast() != null){
			sql += " AND c.broadcast = "+filter.getBroadcast()+" ";
		}
		
		session = getSession();
		SQLQuery sqlQuery = session.createSQLQuery(sql);
		sqlQuery.addScalar("id", Hibernate.INTEGER);
		sqlQuery.addScalar("eventId", Hibernate.INTEGER);
		sqlQuery.addScalar("section", Hibernate.STRING);
		sqlQuery.addScalar("row", Hibernate.STRING);
		sqlQuery.addScalar("quantity", Hibernate.INTEGER);
		sqlQuery.addScalar("price", Hibernate.DOUBLE);
		sqlQuery.addScalar("taxAmount", Hibernate.DOUBLE);
		sqlQuery.addScalar("sectionRange", Hibernate.STRING);
		sqlQuery.addScalar("rowRange", Hibernate.STRING);
		sqlQuery.addScalar("seatLow", Hibernate.STRING);
		sqlQuery.addScalar("seatHigh", Hibernate.STRING);
		sqlQuery.addScalar("externalNotes", Hibernate.STRING);
		sqlQuery.addScalar("internalNotes", Hibernate.STRING);
		sqlQuery.addScalar("nearTermDisplayOption", Hibernate.STRING);
		sqlQuery.addScalar("marketPlaceNotes", Hibernate.STRING);
		sqlQuery.addScalar("broadcast", Hibernate.BOOLEAN);
		sqlQuery.addScalar("producttype", Hibernate.custom(EnumType.class, paramProductType));
		sqlQuery.addScalar("shippingMethodId", Hibernate.INTEGER);
		sqlQuery.addScalar("maxShowing", Hibernate.INTEGER);
		sqlQuery.addScalar("shippingMethod", Hibernate.STRING);
		sqlQuery.addScalar("brokerId", Hibernate.INTEGER);
		firstResult = PaginationUtil.getNextPageStatFrom(pageNo);
		Collection<CategoryTicketGroup> catTicketGroupList = sqlQuery.setResultTransformer(Transformers.aliasToBean(CategoryTicketGroup.class)).setFirstResult(firstResult).setMaxResults(PaginationUtil.PAGESIZE).list();
				
		return catTicketGroupList;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}		
		//return getSession().createQuery(sql).list();
		//return find("FROM CategoryTicketGroup WHERE eventId=? and status=? ", new Object[]{eventId,TicketStatus.ACTIVE});
	}
	
	@Override
	public CategoryTicketGroup getCategoryTicketByEventId(Integer eventId) throws Exception {
		return findSingle("FROM CategoryTicketGroup WHERE eventId=? and status=? ", new Object[]{eventId,TicketStatus.ACTIVE});
	}
	public CategoryTicketGroup getActiveCategoryTicketById(Integer ticketId) throws Exception {
		return findSingle("FROM CategoryTicketGroup WHERE id=? and status=? ", new Object[]{ticketId,TicketStatus.ACTIVE});
	}

	@Override
	public void updateCategoryTicket(int ticketId, int invoiceId,Double soldPrice) throws Exception {
		try {
			/*String query = "UPDATE category_ticket_group SET status='SOLD',sold_price="+soldPrice+",invoice_id = "+invoiceId+" WHERE status = 'ACTIVE' and id = "+ticketId;
			Transaction transaction = session.beginTransaction();
			transaction.begin();
			session = getSessionFactory().openSession();
			Query sqlQuery1 = session.createSQLQuery(query);
			count = sqlQuery1.executeUpdate();
			//session.close();
			
			query = "UPDATE category_ticket set invoice_id="+invoiceId+" WHERE category_ticket_group_id="+ticketId;
			//session = getSessionFactory().openSession();
			Query sqlQuery2 = session.createSQLQuery(query);
			count = sqlQuery2.executeUpdate();
			transaction.commit();
			session.close();*/
			CategoryTicketGroup ticketGroup  = DAORegistry.getCategoryTicketGroupDAO().get(ticketId);
			ticketGroup.setInvoiceId(invoiceId);
			ticketGroup.setSoldPrice(soldPrice);
			DAORegistry.getCategoryTicketGroupDAO().update(ticketGroup);
			List<CategoryTicket> tickets = DAORegistry.getCategoryTicketDAO().getCategoryTicketByCategoryTicketGroupId(ticketId);
			for(CategoryTicket ticket : tickets){
				ticket.setInvoiceId(invoiceId);
				ticket.setSoldPrice(soldPrice);
			}
			DAORegistry.getCategoryTicketDAO().updateAll(tickets);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void updateCategoryTicketGroupFields(Integer ticketId,String editColumnType,String value) throws Exception {
		
		if(editColumnType.equals("internalNotes")) {
			 bulkUpdate("UPDATE CategoryTicketGroup SET internalNotes=? WHERE id=? ", new Object[] {value,ticketId});	
		} else if(editColumnType.equals("externalNotes")){
			bulkUpdate("UPDATE CategoryTicketGroup SET externalNotes=? WHERE id=? ", new Object[] {value,ticketId});
		} else if(editColumnType.equals("marketPlaceNotes")){
			bulkUpdate("UPDATE CategoryTicketGroup SET marketPlaceNotes=? WHERE id=? ", new Object[] {value,ticketId});
		}
		
	}

	@Override
	public Collection<CategoryTicketGroup> getSoldTicketsByEventId(Integer eventId) {
		return find("FROM CategoryTicketGroup WHERE eventId=? and status=? ", new Object[]{eventId,TicketStatus.SOLD});
	}

	@Override
	public CategoryTicketGroup getCategoryTicketByInvoiceId(Integer invoiceId) {
		return findSingle("FROM CategoryTicketGroup WHERE invoiceId=? and status=? ", new Object[]{invoiceId,TicketStatus.SOLD});
	}
	
	public Collection<CategoryTicketGroup> getSoldTicketsSearchFilterByEventId(Integer eventId, Integer brokerId, GridHeaderFilters filter){
		List<CategoryTicketGroup> categoryTicketGroupList = null;
		Session session = null;
		try{
			String sql = "select c.id as id, c.invoice_id as invoiceId, c.event_id as eventId, c.section as section, c.row as row, c.quantity as quantity, "+
				"c.price as price, c.tax_amount as taxAmount, c.section_range as sectionRange, c.row_range as rowRange, "+
				"c.seat_low as seatLow, c.seat_high as seatHigh, c.external_notes as externalNotes, c.internal_notes as internalNotes, "+
				"c.near_term_display_option as nearTermDisplayOption, c.marketplace_notes as marketPlaceNotes, c.broadcast as broadcast, "+
				"c.shipping_method_id as shippingMethodId, c.max_showing as maxShowing, sm.name as shippingMethod, c.broker_id as brokerId "+
				"FROM category_ticket_group c with(nolock) left join shipping_method sm with(nolock) on c.shipping_method_id = sm.id WHERE c.event_id="+eventId+" and c.status='"+TicketStatus.SOLD+"'";
	
		if(brokerId != null && brokerId > 0){
			sql += " AND c.broker_id = "+brokerId+" ";
		}
		if(filter.getInvoiceId() != null){
			sql += " AND c.invoice_id = "+filter.getInvoiceId()+" ";
		}
		if(filter.getSection() != null){				
			sql += " AND c.section like '%"+filter.getSection()+"%' ";
		}
		if(filter.getRow() != null){
			sql += " AND c.row like '%"+filter.getRow()+"%' ";
		}
		if(filter.getSeatLow() != null){
			sql += " AND c.seat_low like '%"+filter.getSeatLow()+"%' ";
		}
		if(filter.getSeatHigh() != null){
			sql += " AND c.seat_high like '%"+filter.getSeatHigh()+"%' ";
		}
		if(filter.getQuantity() != null){
			sql += " AND c.quantity = "+filter.getQuantity()+" ";
		}
		if(filter.getRetailPrice() != null){
			sql += " AND c.price = "+filter.getRetailPrice()+" ";
		}
		if(filter.getWholeSalePrice() != null){
			sql += " AND c.price = "+filter.getWholeSalePrice()+" ";
		}
		if(filter.getFacePrice() != null){
			sql += " AND c.price = "+filter.getFacePrice()+" ";
		}
		if(filter.getPrice() != null){
			sql += " AND c.price = "+filter.getPrice()+" ";
		}
		if(filter.getTaxAmount() != null){
			sql += " AND c.tax_amount = "+filter.getTaxAmount()+" ";
		}
		if(filter.getShippingMethod() != null){
			sql += " AND sm.name like '%"+filter.getShippingMethod()+"%' ";
		}
		if(filter.getNearTermDisplayOption() != null){
			sql += " AND c.near_term_display_option like '%"+filter.getNearTermDisplayOption()+"%' ";
		}
		if(filter.getSectionRange() != null){
			sql += " AND c.section_range like '%"+filter.getSectionRange()+"%' ";
		}
		if(filter.getRowRange() != null){
			sql += " AND c.row_range like '%"+filter.getRowRange()+"%' ";
		}
		if(filter.getExternalNotes() != null){
			sql += " AND c.external_notes like '%"+filter.getExternalNotes()+"%' ";
		}
		if(filter.getMarketPlaceNotes() != null){
			sql += " AND c.marketplace_notes like '%"+filter.getMarketPlaceNotes()+"%' ";
		}
				
		session = getSession();
		SQLQuery sqlQuery = session.createSQLQuery(sql);
		sqlQuery.addScalar("id", Hibernate.INTEGER);
		sqlQuery.addScalar("invoiceId", Hibernate.INTEGER);
		sqlQuery.addScalar("eventId", Hibernate.INTEGER);
		sqlQuery.addScalar("section", Hibernate.STRING);
		sqlQuery.addScalar("row", Hibernate.STRING);
		sqlQuery.addScalar("quantity", Hibernate.INTEGER);
		sqlQuery.addScalar("price", Hibernate.DOUBLE);
		sqlQuery.addScalar("taxAmount", Hibernate.DOUBLE);
		sqlQuery.addScalar("sectionRange", Hibernate.STRING);
		sqlQuery.addScalar("rowRange", Hibernate.STRING);
		sqlQuery.addScalar("seatLow", Hibernate.STRING);
		sqlQuery.addScalar("seatHigh", Hibernate.STRING);
		sqlQuery.addScalar("externalNotes", Hibernate.STRING);
		sqlQuery.addScalar("internalNotes", Hibernate.STRING);
		sqlQuery.addScalar("nearTermDisplayOption", Hibernate.STRING);
		sqlQuery.addScalar("marketPlaceNotes", Hibernate.STRING);
		sqlQuery.addScalar("broadcast", Hibernate.BOOLEAN);
		sqlQuery.addScalar("shippingMethodId", Hibernate.INTEGER);
		sqlQuery.addScalar("maxShowing", Hibernate.INTEGER);
		sqlQuery.addScalar("shippingMethod", Hibernate.STRING);
		sqlQuery.addScalar("brokerId", Hibernate.INTEGER);
		categoryTicketGroupList = sqlQuery.setResultTransformer(Transformers.aliasToBean(CategoryTicketGroup.class)).list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return categoryTicketGroupList;
	}

	public void updateBroadcastByEventId(Integer eventId, Boolean broadCast){
		bulkUpdate("UPDATE CategoryTicketGroup set broadcast = ? WHERE eventId=? and status=? ", new Object[]{broadCast,eventId,TicketStatus.ACTIVE});
	}
	
	public CategoryTicketGroup getDuplicateCategoryTicketGroupForBroker(String zone,Integer qty,Integer eventId,Integer brokerId){
		CategoryTicketGroup tg = findSingle("FROM CategoryTicketGroup WHERE section=? AND quantity=? AND eventId=? AND brokerId=? AND invoiceId is NULL",new Object[]{zone,qty,eventId,brokerId});
		return tg;
	}
	
	public CategoryTicketGroup getCategoryTicketByIdAndBrokerId(Integer id, Integer brokerId){
		return findSingle("FROM CategoryTicketGroup WHERE id = ? and brokerId = ? and status=? ", new Object[]{id,brokerId,TicketStatus.ACTIVE});
	}
}
