package com.rtw.tracker.dao.implementation;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.rtw.tracker.datas.Invoice;
import com.rtw.tracker.enums.InvoiceStatus;
import com.rtw.tracker.fedex.Recipient;

public class InvoiceDAO extends HibernateDAO<Integer, Invoice> implements com.rtw.tracker.dao.services.InvoiceDAO{

	/*@Override
	public int updateRealTixStatus(int invoiceId,String shippingMethod,Boolean isRealTixDelivered) throws Exception {
		Session session = null;
		int count = 0;		
		try {
			String query = "UPDATE invoice SET realtix_map = 'YES',realtix_shipping_method=?,realtix_delivered=? WHERE id = ?";
			session = getSessionFactory().openSession();
			Query sqlQuery = session.createSQLQuery(query);
			sqlQuery.setString(0, shippingMethod);
			sqlQuery.setBoolean(1, isRealTixDelivered);
			sqlQuery.setInteger(2, invoiceId);
			count = sqlQuery.executeUpdate();
			session.close();
		}catch (Exception e) {
			if(session != null && session.isOpen()){
				session.close();
			}
			e.printStackTrace();
		}
		
		return count;
	}*/
	
	@Override
	public List<Integer> getInvoiceTocreateFedexLabel(){
		Session session = null;
		DateFormat formatDateTime = new SimpleDateFormat("MM/dd/yyyy");
		Date today = new Date();
		String todayStr = formatDateTime.format(today);
		try {
			String query = "SELECT i.id FROM Invoice i join shipping_method s on i.shipping_method_id=s.id " +
					"WHERE i.status='ACTIVE' AND s.name='FedEx' AND i.tracking_no is null AND i.created_Date > '"+todayStr+"'";
			session = getSessionFactory().openSession();
			Query sqlQuery = session.createSQLQuery(query);
			List<Integer>  list = sqlQuery.list();
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return null;
	}
	
	
	@Override
	public int updateTixUploaded(int invoiceId) throws Exception {
		Session session = null;
		int count = 0;		
		try {
			String query = "UPDATE invoice SET is_real_tix_upload = 'YES' WHERE id = "+invoiceId;
			session = getSessionFactory().openSession();
			Query sqlQuery = session.createSQLQuery(query);
			count = sqlQuery.executeUpdate();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		
		return count;
	}
	
	@Override
	public int updateBarCode(String barCode, int invoiceId) throws Exception {
		Session session = null;
		int count = 0;		
		try {
			String query = "UPDATE invoice SET tix_bar_code = "+barCode+" WHERE id = "+invoiceId;
			session = getSessionFactory().openSession();
			Query sqlQuery = session.createSQLQuery(query);
			count = sqlQuery.executeUpdate();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		
		return count;
	}
	
	
	
	public List<Recipient> getRecipientListByInvoice(Integer invoiceId,
			boolean recreateLabel) {
		List<Recipient> recipientList = new ArrayList<Recipient>();
		Session session = null;
		try {

			StringBuffer query = new StringBuffer("");
			List<Object[]> list = new ArrayList<Object[]>();

			session = getSessionFactory().openSession();

			query.append("select distinct i.id AS id,CONCAT(c.sh_first_name,' ',c.sh_last_name) AS name,c.sh_address1 as Add1,c.sh_address2 AS add2,c.sh_city AS city,");
			query.append("s.short_desc AS stateCode,co.short_desc AS countryCode,c.sh_zipcode AS pincode,c.sh_phone1 AS phone  ");
			query.append("from invoice i with(nolock) join customer_order_details c with(nolock) on i.order_id=c.order_id ");
			query.append("left join state s with(nolock) on c.sh_state_name = s.name ");
			query.append("left join country co with(nolock) on c.sh_country_name=co.country_name ");
			query.append("left join shipping_method sm with(nolock) on (sm.id=i.shipping_method_id or sm.name=i.realtix_shipping_method) ");
			query.append("WHERE sm.name='FedEx' ");

			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			Calendar endCalender = Calendar.getInstance();

			// setting endCalender to Jan 1st 2013
			endCalender.set(Calendar.DATE, 1);
			endCalender.set(Calendar.MONTH, 0);

			if (recreateLabel) {
				if (invoiceId != null && invoiceId != 0) {
					query.append(" and i.id =" + invoiceId);
				}
			}
			else if (invoiceId == 0)
				query.append(" and i.create_date >='"
						+ dateFormat.format(endCalender.getTime())
						+ " 00:00:00" + "'");
			else if (invoiceId != null)
				query.append(" and i.id >" + invoiceId);

			query.append(" order by i.id");

			SQLQuery querys = session.createSQLQuery(query.toString());
			list = querys.list();

			if (list != null && list.size() > 0) {
				for (Object[] array : list) {

					Recipient recipient = new Recipient();
					recipient.setInvoiceId((Integer) array[0]);
					recipient.setRecipientName((String) array[1]);
					recipient.setAddressStreet1((String) array[2]);
					recipient.setAddressStreet2((String) array[3]);
					recipient.setCity((String) array[4]);
					recipient.setStateProvinceCode((String) array[5]);
					recipient.setCountryCode((String) array[6]);
					recipient.setPostalCode((String) array[7]);
					recipient.setPhoneNumber((String) array[8]);
					recipientList.add(recipient);

				}
			}
		} catch (Exception e) {
			System.out.println("The Exception is" + e);
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return recipientList;
	}


	@Override
	public List<Invoice> getInvoiceByOrderIds(List<Integer> ids) throws Exception {
		return getHibernateTemplate().findByNamedParam("FROM Invoice WHERE customerOrderId in(:ids)", "ids", ids);
	}


	@Override
	public Invoice getInvoiceByOrderId(Integer orderId) throws Exception {
		return findSingle("FROM Invoice WHERE customerOrderId=?",new Object[]{orderId});
	}


	@Override
	public List<Invoice> getFedexUndeliveredInvoices() throws Exception {
		return find("FROM Invoice WHERE status=? AND trackingNo IS NOT NULL",new Object[]{InvoiceStatus.Outstanding});
	}


	@Override
	public List<Invoice> getInvoiceByTicketUpload() throws Exception {
		return find("FROM Invoice WHERE isRealTixUploaded=?",new Object[]{"PROCESSING"});
	}

	
	public Invoice getInvoiceByIdAndBrokerId(Integer id, Integer brokerId)throws Exception{
		return findSingle("FROM Invoice WHERE id=? AND brokerId=?",new Object[]{id, brokerId});
	}
}
