package com.rtw.tracker.dao.implementation;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.rtw.tmat.enums.ProfitLossSign;
import com.rtw.tracker.datas.ProductType;
import com.rtw.tracker.datas.SoldTicketDetail;


public class SoldTicketDetailDAO extends HibernateDAO<Integer, SoldTicketDetail> implements com.rtw.tracker.dao.services.SoldTicketDetailDAO{

	@Override
	public List<SoldTicketDetail> getSoldTicketDetailsByEventId(Integer eventId) {
		return find("FROM SoldTicketDetail WHERE eventId = ? ", new Object[]{eventId});
	}
	
	@Override
	public List<SoldTicketDetail> getAllSoldUnfilledTicketDetails() {
		return find("FROM SoldTicketDetail WHERE eventDate > ? " +
				"and ( purchaseOrderId is null or purchaseOrderId = 0 ) ", new Object[]{new Date()});
	}
	
	/*@Override
	public List<SoldTicketDetail> getAllSoldUnfilledTicketDetailsByBrokerId(Integer brokerId) {
		return find("FROM SoldTicketDetail WHERE eventDate > ? AND brokerId=? " +
				"and ( purchaseOrderId is null or purchaseOrderId = 0 ) ", new Object[]{new Date(),brokerId});
	}
	
	@Override
	public List<SoldTicketDetail> getRTFAllSoldUnfilledTicketDetails() {
		return find("FROM SoldTicketDetail WHERE eventDate > ? AND brokerId is null " +
				"and ( purchaseOrderId is null or purchaseOrderId = 0 ) ", new Object[]{new Date()});
	}*/
	
	/*@Override
	public List<SoldTicketDetail> getAllSoldFilledTicketDetails(String eventId,
			String artistId,String venueId, Date startDate, Date endDate,ProfitLossSign profitLossSign) {
		try {
			String query ="FROM SoldTicketDetail WHERE ( purchaseOrderId is NULL or purchaseOrderId = 0 ) ";
			List<Object> params = new ArrayList<Object>();
			//params.add(new Date()); eventDate > ? AND
			
			if(eventId!=null && !eventId.isEmpty()){
				query = query+" AND eventId = ? ";
				params.add(Integer.parseInt(eventId));
			}else {
				if(venueId!=null && !venueId.isEmpty()){
					query = query+" AND venueId = ? ";
					params.add(Integer.parseInt(venueId));
				}else if(artistId!=null && !artistId.isEmpty()){
					query = query+" AND eventId in (SELECT id from EventDetails WHERE artistId = ? )";
					params.add(Integer.parseInt(artistId));
				}
			}
				
			
			if(startDate!=null){
				query = query+" AND invoiceDateTime >= ? ";
				params.add(startDate);
			}
			if(endDate!=null){
				query = query+" AND invoiceDateTime <= ? ";
				params.add(endDate);
			}
			
			if(null !=profitLossSign ){
				
				switch (profitLossSign) {
					case POSITIVE:
						query = query+" AND profitAndLoss > 0 ";
						break;
					case NEGATIVE:
						query = query+" AND profitAndLoss < 0 ";
						break;
					case ZERO:
						query = query+" AND profitAndLoss = 0 ";
						break;
					case ALL:
						query = query+"";
						break;
					default:
						break;
				}
			}

			 query = query + "ORDER BY invoiceDateTime desc"; 
			 System.out.println(query);
			return find(query, params.toArray());

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}*/
	
	public List<SoldTicketDetail> getSoldTicketDetailsByBrokerId(Integer brokerId) {
		return find("FROM SoldTicketDetail WHERE brokerId = ? ", new Object[]{brokerId});
	}

	@Override
	public List<SoldTicketDetail> getSoldTicketDetailsByBrokerIdAndEventId(Integer brokerId, Integer eventId) {
		return find("FROM SoldTicketDetail WHERE brokerId = ? AND eventId = ?", new Object[]{brokerId,eventId});
	}

	@Override
	public List<SoldTicketDetail> getSoldTicketDetailsByBrokerIdAndArtistIdAndEventIdAndBySalesDateRange(
			Integer brokerId,Integer artistId, Integer eventId, Date startDate, Date endDate) {
		String url ="FROM SoldTicketDetail WHERE 1=1";
		List<Object> params = new ArrayList<Object>();
		
		if(brokerId!=null){
			url = url+" AND brokerId = ? ";
			params.add(brokerId);
		}
		
		if(eventId!=null){
			url = url+" AND eventId = ? ";
			params.add(eventId);
		}else if(artistId!=null){
			url = url+" AND eventId in (SELECT admitoneId from Event WHERE artistId = ? )";
			params.add(artistId);
		}
		
		if(startDate!=null){
			url = url+" AND invoiceDateTime >= ? ";
			params.add(startDate);
		}
		if(endDate!=null){
			url = url+" AND invoiceDateTime <= ? ";
			params.add(endDate);
		}
		 url = url + "ORDER BY invoiceDateTime desc"; 
		return find(url, params.toArray());
	}

	@Override
	public List<SoldTicketDetail> getAllSoldUnfilledTicketDetailsByProduct(ProductType productType) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR, 00);
		cal.set(Calendar.MINUTE, 00);
		cal.set(Calendar.SECOND, 00);
		cal.set(Calendar.MILLISECOND, 00);
		return find("FROM SoldTicketDetail WHERE actualSoldPrice>=0 and eventDate >= ? AND productType=? " +
				"and ( purchaseOrderId is null or purchaseOrderId = 0 ) ", new Object[]{cal.getTime(),productType.toString()});
	}
}
