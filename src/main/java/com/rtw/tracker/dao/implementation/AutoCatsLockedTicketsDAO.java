package com.rtw.tracker.dao.implementation;

import com.rtw.tracker.datas.AutoCatsLockedTickets;
import com.rtw.tracker.enums.LockedTicketStatus;

public class AutoCatsLockedTicketsDAO extends HibernateDAO<Integer, AutoCatsLockedTickets> implements com.rtw.tracker.dao.services.AutoCatsLockedTicketsDAO{

	public AutoCatsLockedTickets getLockedTicketsByEventId(Integer eventId){
		return findSingle("FROM AutoCatsLockedTickets WHERE eventId = ? AND lockStatus = ?", new Object[]{eventId, LockedTicketStatus.ACTIVE});
	}
	
	public AutoCatsLockedTickets getLockedTicketsByCategoryTicketGroupId(Integer catgeoryTicketGroupId){
		return findSingle("FROM AutoCatsLockedTickets WHERE catgeoryTicketGroupId = ? AND lockStatus = ?", new Object[]{catgeoryTicketGroupId, LockedTicketStatus.ACTIVE});
	}
	
}
