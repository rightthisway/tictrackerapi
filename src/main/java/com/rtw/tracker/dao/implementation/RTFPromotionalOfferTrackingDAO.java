package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.ApplicationPlatform;
import com.rtw.tracker.datas.RTFPromotionalOfferTracking;
import com.rtw.tracker.enums.PromotionalType;


public class RTFPromotionalOfferTrackingDAO extends HibernateDAO<Integer, RTFPromotionalOfferTracking> implements com.rtw.tracker.dao.services.RTFPromotionalOfferTrackingDAO{


	public RTFPromotionalOfferTracking getPromoTracking(ApplicationPlatform platForm,Integer promoOfferId,Integer customerId, String sessionId,
			Integer ticketGroupId, Integer eventId,String promoCode,Boolean isLongTicket,PromotionalType offerType){
		return findSingle("from RTFPromotionalOfferTracking where promotionalOfferId=? and customerId=? " +
				"and  eventId=? and ticketGroupId=? and sessionId=? and platForm=? and promoCode=? " +
				"and status=? and isLongTicket=?", 
				new Object[]{promoOfferId,customerId,eventId,ticketGroupId,sessionId,platForm,promoCode,"PENDING",isLongTicket});
	}
	
	
	public RTFPromotionalOfferTracking getPromoTracking(ApplicationPlatform platForm,Integer customerId, String sessionId,
			Integer ticketGroupId, Integer eventId){
		return findSingle("from RTFPromotionalOfferTracking where customerId=? " +
				"and  eventId=? and ticketGroupId=? and sessionId=? and platForm=? " +
				"and status=?", new Object[]{customerId,eventId,ticketGroupId,sessionId,platForm,"PENDING"});
	}
	
	public RTFPromotionalOfferTracking getPendingPromoTracking(ApplicationPlatform platForm,Integer customerId, String sessionId,
			Integer ticketGroupId, Integer eventId){
		return findSingle("from RTFPromotionalOfferTracking where customerId=? " +
				"and  eventId=? and ticketGroupId=? and sessionId=? and platForm=? " +
				"and status=?", new Object[]{customerId,eventId,ticketGroupId,sessionId,platForm,"PENDING"});
	}
	
	public List<RTFPromotionalOfferTracking> getAllPendingPromoTracking(){
		return find("from RTFPromotionalOfferTracking where status=? order by createdDate asc", new Object[]{"PENDING"});
	}
	
	public RTFPromotionalOfferTracking getCompletedPromoTracking(Integer promoOfferId){
		return findSingle("from RTFPromotionalOfferTracking where promotionalOfferId=? and status=?", new Object[]{promoOfferId, "COMPLETED"});
	}
	
	public RTFPromotionalOfferTracking getCompletedPromoTrackingByOrderId(Integer promoOfferOrderId){
		return findSingle("from RTFPromotionalOfferTracking where orderId=? and status=?", new Object[]{promoOfferOrderId, "COMPLETED"});
	}
	
	public RTFPromotionalOfferTracking getPromoTrackingByOrderId(Integer promoOfferOrderId){
		return findSingle("from RTFPromotionalOfferTracking where orderId=?", new Object[]{promoOfferOrderId});
	}
}
