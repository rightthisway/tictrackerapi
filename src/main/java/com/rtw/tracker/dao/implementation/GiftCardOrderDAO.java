package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.datas.GiftCardOrders;
import com.rtw.tracker.datas.Invoice;
import com.rtw.tracker.utils.GridHeaderFilters;

public class GiftCardOrderDAO extends HibernateDAO<Integer, GiftCardOrders> implements com.rtw.tracker.dao.services.GiftCardOrderDAO {

	
	private static Session giftCardSession = null;

	public static Session getGiftCardOrderSession() {
		
		return GiftCardOrderDAO.giftCardSession;
		
	}

	public static void setGiftCardOrderSession(Session giftCardSession) {
		GiftCardOrderDAO.giftCardSession = giftCardSession;
	}

	public List<GiftCardOrders> getAllGiftCardOrders(Integer id, GridHeaderFilters filter, String status) {
		List<GiftCardOrders> contestsList = null;
		try {
			
			
			// customer first name , last name in one column  and customer user Id  
			
			String sql = "select g.id as id, g.customer_id as custId, g.card_id as cardId,  g.card_title as cardTitle,c.cust_name as firstName,c.user_id as userId,"
					+ " g.card_description as cardDescription, g.quantity as quantity, g.redeemed_rewards as redeemedRewards,c.last_name as lastName,"
					+ " g.original_cost as originalCost,g.order_type as orderType,g.shipping_address_id as shipAddrId,g.is_email_sent as isEmailSent,g.barcode as barcode,"
					+ " g.is_order_fulfilled as isOrderFulfilled, g.card_file_url as cardFileUrl, g.created_date as createdDate, g.created_by as createdBy,c.phone as phone,"
					+ " g.status as status, g.updated_date as updatedDate, g.updated_by as updatedBy, g.remarks as remarks,g.is_redeemed as isRedeemed from GiftCard_Order g"
					+ " join customer c on g.customer_id=c.id where 1=1  ";

			if (id != null && id > 0) {
				sql += " AND g.id=" + id;
			}
			if (status != null && !status.isEmpty() && !status.equalsIgnoreCase("ALL")) {
				sql += " AND g.status like '%"+status+"%'";
			}

			if (filter.getId() != null) {
				sql += " AND g.id=" + filter.getId();
			}
			if (filter.getContestName() != null && !filter.getContestName().isEmpty()) {
				sql += " AND g.card_title like '%"+filter.getContestName()+"%'";
			}
			if (filter.getEventDescription() != null && !filter.getEventDescription() .isEmpty()) {
				sql += " AND g.card_description like '%"+filter.getEventDescription()+"%'";
			}
			if (filter.getText1() != null && !filter.getText1().isEmpty()) {
				if(filter.getText1().equalsIgnoreCase("YES")){
					sql += " AND g.is_email_sent=1";
				}else if(filter.getText1().equalsIgnoreCase("NO")){
					sql += " AND g.is_email_sent=0";
				}
			}
			if (filter.getText2() != null && !filter.getText2() .isEmpty()) {
				sql += " AND g.order_type like '%" + filter.getText2()+"%'";
			}
			if (filter.getText3() != null && !filter.getText3() .isEmpty()) {
				sql += " AND g.remarks like '%" + filter.getText3()+"%'";
			}
			if (filter.getText4()!= null && !filter.getText4().isEmpty()) {
				sql += " AND g.barcode like '%"+filter.getContestName()+"%'";
			}
			if (filter.getText5()!= null && filter.getText5().equalsIgnoreCase("BARCODE")) {
				sql += " AND (g.barcode is null or g.barcode = '') ";
			}
			if (filter.getText6()!= null ){
				if(filter.getText6().equalsIgnoreCase("YES")) {
					sql += " AND g.is_redeemed=1 ";
				}else{
					sql += " AND g.is_redeemed=0 ";
				}
			}
			if (filter.getStatus() != null && !filter.getStatus().isEmpty()) {
				sql += " AND g.status like '%"+filter.getStatus()+"%' ";
			}
			if (filter.getCreatedBy() != null) {
				sql += " AND g.created_by like '%"+filter.getCreatedBy()+"%'";
			}
			if (filter.getUpdatedBy() != null) {
				sql += " AND g.updated_by like '%"+filter.getUpdatedBy()+"%'";
			}
			if (filter.getMinimumPurchaseAmount() != null) {
				sql += " AND g.redeemed_rewards=" + filter.getMinimumPurchaseAmount();
			}

			if (filter.getQuantity() != null) {
				sql += " AND g.quantity=" + filter.getQuantity();
			}
			if (filter.getCustomerName() != null && !filter.getCustomerName().isEmpty()) {
				sql += " AND (c.cust_name like '%"+filter.getCustomerName()+"%' or c.last_name like '%"+filter.getCustomerName()+"%')";
			}
			if (filter.getUsername() != null && !filter.getUsername().isEmpty()) {
				sql += " AND c.user_id like '%"+filter.getUsername()+"%'";
			}
			
			if (filter.getCreatedDateStr() != null && !filter.getCreatedDateStr().isEmpty()) {
				if (Util.extractDateElement(filter.getCreatedDateStr(), "DAY") > 0) {
					sql += " AND DATEPART(day, g.created_date) = "
							+ Util.extractDateElement(filter.getCreatedDateStr(), "DAY") + " ";
				}
				if (Util.extractDateElement(filter.getCreatedDateStr(), "MONTH") > 0) {
					sql += " AND DATEPART(month, g.created_date) = "
							+ Util.extractDateElement(filter.getCreatedDateStr(), "MONTH") + " ";
				}
				if (Util.extractDateElement(filter.getCreatedDateStr(), "YEAR") > 0) {
					sql += " AND DATEPART(year, g.created_date) = "
							+ Util.extractDateElement(filter.getCreatedDateStr(), "YEAR") + " ";
				}
			}
			if (filter.getUpdatedDate()!= null && !filter.getUpdatedDate().isEmpty()) {
				if (Util.extractDateElement(filter.getUpdatedDate(), "DAY") > 0) {
					sql += " AND DATEPART(day, g.updated_date) = "
							+ Util.extractDateElement(filter.getUpdatedDate(), "DAY") + " ";
				}
				if (Util.extractDateElement(filter.getUpdatedDate(), "MONTH") > 0) {
					sql += " AND DATEPART(month, g.updated_date) = "
							+ Util.extractDateElement(filter.getUpdatedDate(), "MONTH") + " ";
				}
				if (Util.extractDateElement(filter.getUpdatedDate(), "YEAR") > 0) {
					sql += " AND DATEPART(year, g.updated_date) = "
							+ Util.extractDateElement(filter.getUpdatedDate(), "YEAR") + " ";
				}
			}

			sql += " order by g.id desc";

			giftCardSession = GiftCardOrderDAO.getGiftCardOrderSession();
			if (giftCardSession == null || !giftCardSession.isOpen() || !giftCardSession.isConnected()) {
				giftCardSession = getSession();				
				GiftCardOrderDAO.setGiftCardOrderSession(giftCardSession);
			}

			SQLQuery query = giftCardSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("custId", Hibernate.INTEGER);
			query.addScalar("cardId", Hibernate.INTEGER);
			query.addScalar("cardTitle", Hibernate.STRING);
			query.addScalar("cardDescription", Hibernate.STRING);
			query.addScalar("quantity", Hibernate.INTEGER);
			query.addScalar("redeemedRewards", Hibernate.DOUBLE);
			query.addScalar("originalCost", Hibernate.DOUBLE);
			query.addScalar("orderType", Hibernate.STRING);
			query.addScalar("barcode", Hibernate.STRING);
			query.addScalar("phone", Hibernate.STRING);
			query.addScalar("shipAddrId", Hibernate.INTEGER);
			query.addScalar("isEmailSent", Hibernate.BOOLEAN);
			query.addScalar("isOrderFulfilled", Hibernate.BOOLEAN);
			query.addScalar("cardFileUrl", Hibernate.STRING);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);
			query.addScalar("updatedBy", Hibernate.STRING);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("firstName", Hibernate.STRING);
			query.addScalar("lastName", Hibernate.STRING);
			query.addScalar("userId", Hibernate.STRING);
			query.addScalar("isRedeemed", Hibernate.BOOLEAN);
			

			contestsList = query.setResultTransformer(Transformers.aliasToBean(GiftCardOrders.class)).list();
		} catch (Exception e) {
			if (giftCardSession != null && giftCardSession.isOpen()) {
				giftCardSession.close();
			}
			e.printStackTrace();
		}
		return contestsList;
	}	
	
	
	@Override
	public GiftCardOrders getGiftCardOrdersById(Integer orderId) throws Exception {
		return findSingle("FROM GiftCardOrders WHERE id=?",new Object[]{orderId});
	}
	
	
	@Override
	public List<GiftCardOrders> getGiftCardOrdersByStatus(String status)  {
		return find("FROM GiftCardOrders WHERE status=?",new Object[]{status});
	}
	
	
	
	
	
	
	

}
