package com.rtw.tracker.dao.implementation;

import java.util.ArrayList;
import java.util.Collection;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.datas.AffiliateCashRewardHistory;
import com.rtw.tracker.utils.GridHeaderFilters;

public class AffiliateCashRewardHistoryDAO extends HibernateDAO<Integer, AffiliateCashRewardHistory> implements com.rtw.tracker.dao.services.AffiliateCashRewardHistoryDAO{

	public Collection<AffiliateCashRewardHistory> getHistoryByUserId(Integer userId, GridHeaderFilters filter){
		Collection<AffiliateCashRewardHistory> historyList = new ArrayList<AffiliateCashRewardHistory>();
		try{
			String sqlQuery = "From AffiliateCashRewardHistory where userId = "+userId+" "; 
			
			if(filter.getCustomerOrderId() != null){
				sqlQuery += " AND order.id = "+filter.getCustomerOrderId()+" ";
			}
			if(filter.getCustomerName() != null){
				sqlQuery += " AND customer.customerName like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getLastName() != null){
				sqlQuery += " AND customer.lastName like '%"+filter.getLastName()+"%' ";
			}
			if(filter.getEventName() != null){
				sqlQuery += " AND order.eventName like '%"+filter.getEventName()+"%' ";
			}
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					sqlQuery += " AND DATEPART(day,order.eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					sqlQuery += " AND DATEPART(month,order.eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					sqlQuery += " AND DATEPART(year,order.eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventTimeStr() != null){
				if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
					sqlQuery += " AND DATEPART(hour,order.eventTime) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
					sqlQuery += " AND DATEPART(minute,order.eventTime) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
				}
			}
			if(filter.getQuantity() != null){
				sqlQuery += " AND order.qty = "+filter.getQuantity()+" ";
			}
			if(filter.getOrderTotal() != null){
				sqlQuery += " AND orderTotal = "+filter.getOrderTotal()+" ";
			}
			if(filter.getCashCredited() != null){
				sqlQuery += " AND creditedCash = "+filter.getCashCredited()+" ";
			}
			if(filter.getStatus() != null){
				sqlQuery += " AND rewardStatus like '%"+filter.getStatus()+"%' ";
			}
			if(filter.getReferrerCode() != null){
				sqlQuery += " AND promoCode like '%"+filter.getReferrerCode()+"%' ";
			}
			/*if(filter.getInternalNotes() != null){
				sqlQuery += " AND paymentNote like '%"+filter.getInternalNotes()+"%' ";
			}*/
			
			return find(sqlQuery);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return historyList;
	}
	
	public Integer getHistoryCountByUserId(Integer UserId, GridHeaderFilters filter){
		Integer count = 0;
		try{
			String sqlQuery = "select count(*) as cnt from affiliate_cash_reward_history a with(nolock) "+
				"left join customer_order co with(nolock) on co.id = a.customer_order_id "+
				"left join customer c with(nolock) on c.id = a.customer_id where 1 = 1";
			
			if(UserId != null && UserId > 0){
				sqlQuery += " AND a.user_id = "+UserId;
			}			
			if(filter.getCustomerOrderId() != null){
				sqlQuery += " AND co.id = "+filter.getCustomerOrderId()+" ";
			}
			if(filter.getCustomerName() != null){
				sqlQuery += " AND c.cust_name like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getLastName() != null){
				sqlQuery += " AND c.last_name like '%"+filter.getLastName()+"%' ";
			}
			if(filter.getEventName() != null){
				sqlQuery += " AND co.event_name like '%"+filter.getEventName()+"%' ";
			}
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					sqlQuery += " AND DATEPART(day,co.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					sqlQuery += " AND DATEPART(month,co.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					sqlQuery += " AND DATEPART(year,co.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventTimeStr() != null){
				if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
					sqlQuery += " AND DATEPART(hour,co.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
					sqlQuery += " AND DATEPART(minute,co.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
				}
			}
			if(filter.getQuantity() != null){
				sqlQuery += " AND co.quantity = "+filter.getQuantity()+" ";
			}
			if(filter.getOrderTotal() != null){
				sqlQuery += " AND a.order_total = "+filter.getOrderTotal()+" ";
			}
			if(filter.getCashCredited() != null){
				sqlQuery += " AND a.credited_cash = "+filter.getCashCredited()+" ";
			}
			if(filter.getStatus() != null){
				sqlQuery += " AND a.status like '%"+filter.getStatus()+"%' ";
			}
			if(filter.getReferrerCode() != null){
				sqlQuery += " AND a.promo_code like '%"+filter.getReferrerCode()+"%' ";
			}
			/*if(filter.getInternalNotes() != null){
				sqlQuery += " AND a.payment_note like '%"+filter.getInternalNotes()+"%' ";
			}*/
			
			Session session = getSession();
			SQLQuery query = session.createSQLQuery(sqlQuery);
			count = Integer.parseInt(String.valueOf(query.uniqueResult()));
		}catch(Exception e){
			e.printStackTrace();
		}
		return count;
	}
	
	public AffiliateCashRewardHistory getHistoryByUserIdAndOrderId(Integer userId, Integer orderId){
		return findSingle("From AffiliateCashRewardHistory where userId = ? and order.id = ?", new Object[]{userId, orderId});
	}
	
	public AffiliateCashRewardHistory getHistoryByOrderId(Integer orderId){
		return findSingle("From AffiliateCashRewardHistory where order.id = ?", new Object[]{orderId});
	}
}
