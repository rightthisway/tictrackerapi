package com.rtw.tracker.dao.implementation;

import com.rtw.tracker.datas.LoyalFanParentCategoryImage;

public class LoyalFanParentCategoryImageDAO extends HibernateDAO<Integer, LoyalFanParentCategoryImage> implements com.rtw.tracker.dao.services.LoyalFanParentCategoryImageDAO{

	public LoyalFanParentCategoryImage getLoyalFanImageByParentCategory(Integer parentCategoryId){
		return findSingle("FROM LoyalFanParentCategoryImage where status=? AND category.id=?",new Object[]{"ACTIVE",parentCategoryId});
	}
}
