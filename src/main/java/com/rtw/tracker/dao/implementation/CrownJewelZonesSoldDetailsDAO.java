package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.ArtistImage;
import com.rtw.tracker.datas.CrownJewelZonesSoldDetails;

public class CrownJewelZonesSoldDetailsDAO extends HibernateDAO<Integer, CrownJewelZonesSoldDetails> implements com.rtw.tracker.dao.services.CrownJewelZonesSoldDetailsDAO{
	
	public List<CrownJewelZonesSoldDetails> getAllTeamZonesByLeagueId(Integer leagueId) throws Exception {
		return find("FROM CrownJewelZonesSoldDetails where leagueId=?", new Object[] {leagueId});	
	}
	
	public List<CrownJewelZonesSoldDetails> getAllTeamZonesByTeamId(Integer teamId) throws Exception {
		return find("FROM CrownJewelZonesSoldDetails where teamId=?", new Object[] {teamId});	
	}

	public void updateDeleteStatusForLeague(Integer leagueId) throws Exception {
		bulkUpdate("UPDATE CrownJewelZonesSoldDetails SET status='DELETED' WHERE leagueId=?",new Object[]{leagueId});
	}
	
	public void updateDeleteStatusForTeam(Integer teamId) throws Exception {
		bulkUpdate("UPDATE CrownJewelZonesSoldDetails SET status='DELETED' WHERE teamId=?",new Object[]{teamId});
	}
	
	public void updateActiveStatusForTeam(Integer teamId) throws Exception {
		bulkUpdate("UPDATE CrownJewelZonesSoldDetails SET status='ACTIVE' WHERE teamId=?",new Object[]{teamId});
	}
}
