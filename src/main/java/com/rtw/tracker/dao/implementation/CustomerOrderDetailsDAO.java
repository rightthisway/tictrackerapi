package com.rtw.tracker.dao.implementation;

import com.rtw.tracker.datas.CustomerOrderDetails;

public class CustomerOrderDetailsDAO extends HibernateDAO<Integer, CustomerOrderDetails> implements com.rtw.tracker.dao.services.CustomerOrderDetaislDAO{

	@Override
	public CustomerOrderDetails getCustomerOrderDetailsByOrderId(Integer orderId) {
		return findSingle("FROM CustomerOrderDetails WHERE orderId=?", new Object[]{orderId});
	}

}
