package com.rtw.tracker.dao.implementation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.rtw.tracker.datas.Customer;
import com.rtw.tracker.datas.ProductType;
import com.rtw.tracker.utils.GridHeaderFilters;

public class CustomerDAO extends HibernateDAO<Integer, Customer> implements com.rtw.tracker.dao.services.CustomerDAO{

	@Override
	public Customer checkCustomerExists(String username) {
		return findSingle("FROM Customer WHERE userName = ?", new Object[]{username});
	}

	public List<Customer> getCustomerByEmail(String email){
		return find("FROM Customer WHERE email = ?", new Object[]{email});
	}
	
	public List<Customer> getCustomerByUserId(String userId){
		return find("FROM Customer WHERE userId = ?", new Object[]{userId});
	}
	
	public Customer getCustByUserId(String userId){
		return findSingle("FROM Customer WHERE userId = ?", new Object[]{userId});
	}
	
	public List<Customer> getAllRTFCustomer(){
		return find("FROM Customer WHERE productType = ?", new Object[]{ProductType.REWARDTHEFAN});
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Collection<Customer> getAllCustomers() {
		Collection<Customer> customerList = new ArrayList<Customer>();
		try{
			Session session = getSession();
			Query query = session.createQuery("From Customer");
			customerList = query.list();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return customerList;
	}
	
	public int updateNotes(String notes,int customerId) throws Exception {
		Session session = null;
		int count = 0;		
		try {
			String query = "UPDATE customer SET notes = '"+notes+"' WHERE id = "+customerId;
			session = getSessionFactory().openSession();
			Query sqlQuery = session.createSQLQuery(query);
			count = sqlQuery.executeUpdate();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return count;
	}
	
	/*
	@Override
	public Integer getTotalCustomerCount() {
		int count = 0;
		String sqlquery = null;
		try{
			Session session = getSession();
			sqlquery = "select count(*) as cnt from customer c left join customer_address ca on (ca.customer_id = c.id and ca.address_type='BILLING_ADDRESS') "+
			"left join country ct on (ct.id=ca.country) left join state s on (s.id=ca.state) WHERE 1=1";
			Query query = session.createSQLQuery(sqlquery);
			count = Integer.valueOf((query.uniqueResult()).toString());
			return count;  
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	*/
	
	public Integer getTotalCustomerCount(Integer customerId,String customerName,String productType,Integer brokerId,GridHeaderFilters filter) {
		int count = 0;		
		String Sqlquery = "select count(*) as cnt from customer c with(nolock) " +
					"left join cust_loyalty_reward_info cl with(nolock) on cl.cust_id=c.id " +
					"left join customer_address ca with(nolock) on(ca.customer_id = c.id and ca.address_type='BILLING_ADDRESS')" +
					"left join country ct with(nolock) on(ct.id=ca.country)" +
					"left join state s with(nolock) on(s.id=ca.state) WHERE c.broker_id="+brokerId+" ";
		try{
			Session session = getSession();
			
			if(customerId!= null && customerId > 0){
				Sqlquery += " AND c.id = "+customerId+"";
			}
			if(customerName != null && !customerName.isEmpty()){
				if(customerName.contains(" ")){
					String customerNameArr[] = customerName.split(" ");
					if(customerNameArr.length>1){
						Sqlquery += " AND c.cust_name like '%"+customerNameArr[0]+"%' AND c.last_name like '%"+customerNameArr[1]+"%' ";
					}
				}else{
					Sqlquery += " AND (c.cust_name like '%"+customerName+"%' OR c.last_name like '%"+customerName+"%') ";
				}
			}
			if(filter.getText1()!= null){
				if(filter.getText1().equalsIgnoreCase("BLOCKED")){
					Sqlquery +=" AND c.is_blocked = 1 ";
				}else if(filter.getText1().equalsIgnoreCase("ACTIVE")){
					Sqlquery +=" AND c.is_blocked = 0 ";
				}
			}
			if(productType!=null && !productType.isEmpty()){
				Sqlquery +=" AND c.product_type like '"+productType+"' ";
			}
			if(filter.getCustomerId() != null && filter.getCustomerId() > 0){
				Sqlquery += " AND c.id = "+filter.getCustomerId() +" ";
			}
			if(filter.getRewardPoints() != null){
				Sqlquery += " AND cl.active_reward_points = "+filter.getRewardPoints() +" ";
			}
			if(filter.getCustomerType() != null && !filter.getCustomerType().isEmpty()){
				Sqlquery += " AND c.customer_type like '%"+filter.getCustomerType()+"%'";
			}
			if(filter.getCustomerName() != null){
				Sqlquery += " AND c.cust_name like '%"+filter.getCustomerName()+"%'";
			}
			if(filter.getLastName() != null){
				Sqlquery += " AND c.last_name like '%"+filter.getLastName()+"%'";
			}
			if(filter.getProductType() != null){
				Sqlquery +=" AND c.product_type like '%"+filter.getProductType()+"%'";
			}
			if(filter.getSignupType() != null){
				Sqlquery +=" AND c.signup_type like '%"+filter.getSignupType()+"%'";
			}
			if(filter.getIsClient() != null){
				Sqlquery +=" AND c.is_client= "+filter.getIsClient()+"";
			}
			if(filter.getIsBroker() != null){
				Sqlquery +=" AND c.is_broker= "+filter.getIsBroker()+"";
			}
			if(filter.getCustomerStatus() != null){
				Sqlquery +=" AND c.customer_level like '%"+filter.getCustomerStatus()+"%'";
			}
			if(filter.getCompanyName() != null){
				Sqlquery +=" AND c.company_name like '%"+filter.getCompanyName()+"%'";
			}
			if(filter.getEmail() != null){
				Sqlquery += " AND c.email like '%"+filter.getEmail()+"%'";	
			}
			if(filter.getAddressLine1() != null){
				Sqlquery += " AND ca.address_line1 like '%"+filter.getAddressLine1()+"%'";
			}
			if(filter.getAddressLine2() != null){
				Sqlquery += " AND ca.address_line2 like '%"+filter.getAddressLine2()+"%'";
			}
			if(filter.getCity() != null){
				Sqlquery += " AND ca.city like '%"+filter.getCity()+"%'";
			}
			if(filter.getState() != null){
				Sqlquery += " AND s.name like '%"+filter.getState()+"%'";
			}
			if(filter.getCountry() != null){
				Sqlquery += " AND ct.country_name like '%"+filter.getCountry()+"%'";
			}
			if(filter.getZipCode() != null){
				Sqlquery += " AND ca.zip_code like '%"+filter.getZipCode()+"%'";
			}
			if(filter.getPhone() != null){
				Sqlquery += " AND c.phone like '%"+filter.getPhone()+"%'";
			}
			if(filter.getReferrerCode() != null){
				Sqlquery += " AND c.referrer_code like '%"+filter.getReferrerCode()+"%'";
			}
			if(filter.getUsername() != null && !filter.getUsername().isEmpty()){
				Sqlquery += " AND c.user_id like '%"+filter.getUsername()+"%' ";
			}
			
			/*
			if(searchBy!=null && !searchBy.isEmpty() && searchValue!=null && !searchValue.isEmpty())
			{
				if(searchBy.equalsIgnoreCase("Name") || searchBy.equalsIgnoreCase("customerName")){
					Sqlquery += "AND cust_name like '%"+searchValue+"%'";
				}else if(searchBy.equalsIgnoreCase("Email")){
					Sqlquery += "AND email ='"+searchValue+"'";
				}else if(searchBy.equalsIgnoreCase("CompanyName")){
					Sqlquery += "AND company_name like '%"+searchValue+"%'";
				}else if(searchBy.equalsIgnoreCase("CustomerType")){
					Sqlquery += "AND customer_type like '%"+searchValue+"%'";
				}
			}			
			if(customerType!=null && !customerType.isEmpty())
			{	 
				if(customerType.equalsIgnoreCase("client")){
					Sqlquery +=" AND is_client=1 ";
				}else if(customerType.equalsIgnoreCase("broker")){
					Sqlquery +=" AND is_broker=1 ";
				}
			}
			if(productType!=null && !productType.isEmpty())
			{
				if(productType.equalsIgnoreCase("REWARDTHEFAN")){
					Sqlquery +=" AND product_type='REWARDTHEFAN'";
				}else if(productType.equalsIgnoreCase("RTW")){
					Sqlquery +=" AND product_type='RTW'";
				}else if (productType.equalsIgnoreCase("RTW2")){
					Sqlquery +=" AND product_type='RTW2'";
				}
			}*/	
			System.out.println("Count Customers: "+Sqlquery);
			Query query = session.createSQLQuery(Sqlquery);
			count = Integer.valueOf((query.uniqueResult()).toString());
			return count;
			}
			catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Customer> getRTWCustomerWithoutReferralCode(){
		return find("FROM Customer WHERE (productType='RTW' OR productType='RTW2') AND referrerCode is NULL");
	}

	@Override
	public List<Customer> getCustomerByEmailUserIdOrFirstNameLastName(String email) {
		return find("FROM Customer WHERE productType = ? and isOptVerified = ? AND (customerName like ? or lastName like ? or email like ? or userId like ?)", new Object[]{ProductType.REWARDTHEFAN,true,"%"+email+"%","%"+email+"%","%"+email+"%","%"+email+"%"});
	}

}
