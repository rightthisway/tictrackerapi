package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.datas.FanclubAbuseReported;
import com.rtw.tracker.utils.GridHeaderFilters;

public class FanclubAbuseReportedDAO extends HibernateDAO<Integer, FanclubAbuseReported> implements com.rtw.tracker.dao.services.FanclubAbuseReportedDAO{

	
	private static Session fanClubSession;

	public static Session getFanClubSession() {
		return fanClubSession;
	}

	public static void setFanClubSession(Session fanClubSession) {
		FanclubAbuseReportedDAO.fanClubSession = fanClubSession;
	}
	
	
	@Override
	public List<FanclubAbuseReported> getFanClubAbuseReportedData(String status, GridHeaderFilters filter,SharedProperty prop) {
		List<FanclubAbuseReported> abuseList = null;
		try {

			StringBuffer sql = new StringBuffer();
			sql.append("select fa.id as id,fa.customer_id as customerId,fa.fanclub_id as fanclubId,fa.abuse_id as abuseId,ab.abuse_text as abuseType,");
			sql.append("fa.created_user_id as createdUserId,fa.updated_user_id as updatedUserId,fa.created_date as createdDate,fa.updated_date as updatedDate,");
			sql.append("fc.title as title,fc.description as description,fc.poster_url as posterUrl,fc.no_of_members as count,fc.fanclub_status as status,");
			sql.append("fcust.user_id as userId,fcust.phone as phone, fcust.email as email,c.user_id as reportedByUserId,c.phone as reportedByPhone,c.email as reportedByEmail ");
			sql.append("from fanclub_customer_reported_abuse_on_media fa with(NOLOCK) ");
			sql.append("join "+prop.getDatabasAlias()+"customer as c with(NOLOCK) on fa.customer_id=c.id ");
			sql.append("join fanclub_mst fc with(NOLOCK) on fa.fanclub_id=fc.id ");
			sql.append("join "+prop.getDatabasAlias()+"customer as fcust with(NOLOCK) on fc.customer_id = fcust.id ");
			sql.append("join mst_static_abuse_options ab with(NOLOCK) on fa.abuse_id =ab.id ");
			//sql.append("WHERE fc.fanclub_status like '"+status+"' ");
			sql.append("WHERE 1=1 ");

			if (filter.getId() != null) {
				sql.append(" AND fc.id =" + filter.getId());
			}
			if (filter.getArtistName() != null && !filter.getArtistName().isEmpty()) {
				sql.append(" AND fc.title like '%" + filter.getArtistName() + "%' ");
			}
			if (filter.getStatus() != null && !filter.getStatus().isEmpty()) {
				sql.append(" AND fc.fanclub_status like '%" + filter.getStatus() + "%' ");
			}
			if (filter.getEventDescription() != null && !filter.getEventDescription().isEmpty()) {
				sql.append(" AND fc.description like '%" + filter.getEventDescription() + "%' ");
			}
			if (filter.getCreatedBy() != null && !filter.getCreatedBy().isEmpty()) {
				sql.append(" AND fa.created_user_id  like '%" + filter.getCreatedBy() + "%' ");
			}
			if (filter.getUpdatedBy() != null && !filter.getUpdatedBy().isEmpty()) {
				sql.append(" AND fa.updated_user_id  like '%" + filter.getUpdatedBy() + "%' ");
			}
			if (filter.getUsername() != null && !filter.getUsername().isEmpty()) {
				sql.append(" AND fcust.user_id like '%" + filter.getUsername() + "%' ");
			}
			if (filter.getEmail() != null && !filter.getEmail().isEmpty()) {
				sql.append(" AND fcust.email like '%" + filter.getEmail() + "%' ");
			}
			if (filter.getPhone() != null && !filter.getPhone().isEmpty()) {
				sql.append(" AND fcust.phone like '%" + filter.getPhone() + "%' ");
			}
			if (filter.getText1() != null && !filter.getText1().isEmpty()) {
				sql.append(" AND c.user_id like '%" + filter.getText1() + "%' ");
			}
			if (filter.getText2() != null && !filter.getText2().isEmpty()) {
				sql.append(" AND c.email like '%" + filter.getText2() + "%' ");
			}
			if (filter.getText3() != null && !filter.getText3().isEmpty()) {
				sql.append(" AND c.phone like '%" + filter.getText3() + "%' ");
			}
			if (filter.getText4() != null && !filter.getText4().isEmpty()) {
				sql.append(" AND ab.abuse_text like '%" + filter.getText4() + "%' ");
			}
			if (filter.getPartipantCount() != null) {
				sql.append(" AND fc.no_of_members =" + filter.getPartipantCount());
			}
			

			if (filter.getCreatedDateStr() != null) {
				if (Util.extractDateElement(filter.getCreatedDateStr(), "DAY") > 0) {
					sql.append(" AND DATEPART(day, fa.created_date) = "
							+ Util.extractDateElement(filter.getCreatedDateStr(), "DAY") + " ");
				}
				if (Util.extractDateElement(filter.getCreatedDateStr(), "MONTH") > 0) {
					sql.append(" AND DATEPART(month, fa.created_date) = "
							+ Util.extractDateElement(filter.getCreatedDateStr(), "MONTH") + " ");
				}
				if (Util.extractDateElement(filter.getCreatedDateStr(), "YEAR") > 0) {
					sql.append(" AND DATEPART(year, fa.created_date) = "
							+ Util.extractDateElement(filter.getCreatedDateStr(), "YEAR") + " ");
				}
			}

			if (filter.getLastUpdatedDateStr() != null) {
				if (Util.extractDateElement(filter.getLastUpdatedDateStr(), "DAY") > 0) {
					sql.append(" AND DATEPART(day, fa.updated_date) = "
							+ Util.extractDateElement(filter.getLastUpdatedDateStr(), "DAY") + " ");
				}
				if (Util.extractDateElement(filter.getCreatedDateStr(), "MONTH") > 0) {
					sql.append(" AND DATEPART(month, fa.updated_date) = "
							+ Util.extractDateElement(filter.getLastUpdatedDateStr(), "MONTH") + " ");
				}
				if (Util.extractDateElement(filter.getCreatedDateStr(), "YEAR") > 0) {
					sql.append(" AND DATEPART(year, fa.updated_date) = "
							+ Util.extractDateElement(filter.getLastUpdatedDateStr(), "YEAR") + " ");
				}
			}

			fanClubSession = FanclubAbuseReportedDAO.getFanClubSession();
			if (fanClubSession == null || !fanClubSession.isOpen() || !fanClubSession.isConnected()) {
				fanClubSession = getSession();
				FanclubAbuseReportedDAO.setFanClubSession(fanClubSession);
			}
			SQLQuery query = fanClubSession.createSQLQuery(sql.toString());
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("customerId", Hibernate.INTEGER);
			query.addScalar("fanclubId", Hibernate.INTEGER);
			query.addScalar("abuseType", Hibernate.STRING);
			query.addScalar("createdUserId", Hibernate.STRING);
			query.addScalar("updatedUserId", Hibernate.STRING);
			query.addScalar("abuseId", Hibernate.INTEGER);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);
			query.addScalar("userId", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("phone", Hibernate.STRING);
			query.addScalar("reportedByUserId", Hibernate.STRING);
			query.addScalar("reportedByEmail", Hibernate.STRING);
			query.addScalar("reportedByPhone", Hibernate.STRING);
			query.addScalar("title", Hibernate.STRING);
			query.addScalar("description", Hibernate.STRING);
			query.addScalar("posterUrl", Hibernate.STRING);
			query.addScalar("count", Hibernate.INTEGER);
			query.addScalar("status", Hibernate.STRING);

			abuseList = query.setResultTransformer(Transformers.aliasToBean(FanclubAbuseReported.class)).list();
		
		} catch (Exception e) {
			if (fanClubSession != null && fanClubSession.isOpen()) {
				fanClubSession.close();
			}
			e.printStackTrace();
		}
		return abuseList;
	}

	@Override
	public List<FanclubAbuseReported> getFanClubPostAbuseReportedData(String status, GridHeaderFilters filter,SharedProperty prop) {

		List<FanclubAbuseReported> abuseList = null;
		try {

			StringBuffer sql = new StringBuffer();
			sql.append("select fa.id as id,fa.customer_id as customerId,fa.fanclub_posts_id as postId,fa.abuse_id as abuseId,ab.abuse_text as abuseType,");
			sql.append("fa.created_user_id as createdUserId,fa.updated_user_id as updatedUserId,fa.created_date as createdDate,fa.updated_date as updatedDate,");
			sql.append("fp.posts_text as title,fp.no_of_likes as count,fp.posts_status status,");
			sql.append("fcust.user_id as userId,fcust.phone as phone, fcust.email as email,c.user_id as reportedByUserId,c.phone as reportedByPhone,c.email as reportedByEmail ");
			sql.append("from fanclub_customer_reported_abuse_on_media fa with(NOLOCK) ");
			sql.append("join "+prop.getDatabasAlias()+"customer as c with(NOLOCK) on fa.customer_id=c.id ");
			sql.append("join fanclub_posts fp with(NOLOCK) on fa.fanclub_posts_id = fp.id ");
			sql.append("join "+prop.getDatabasAlias()+"customer as fcust with(NOLOCK) on fp.customer_id = fcust.id ");
			sql.append("join mst_static_abuse_options ab with(NOLOCK) on fa.abuse_id =ab.id ");
			//sql.append("WHERE fp.posts_status like '"+status+"' ");
			sql.append("WHERE 1=1 ");

			if (filter.getId() != null) {
				sql.append(" AND fp.id =" + filter.getId());
			}
			if (filter.getArtistName() != null && !filter.getArtistName().isEmpty()) {
				sql.append(" AND fp.posts_text like '%" + filter.getArtistName() + "%' ");
			}
			if (filter.getStatus() != null && !filter.getStatus().isEmpty()) {
				sql.append(" AND fp.posts_status like '%" + filter.getStatus() + "%' ");
			}
			if (filter.getCreatedBy() != null && !filter.getCreatedBy().isEmpty()) {
				sql.append(" AND fa.created_user_id  like '%" + filter.getCreatedBy() + "%' ");
			}
			if (filter.getUpdatedBy() != null && !filter.getUpdatedBy().isEmpty()) {
				sql.append(" AND fa.updated_user_id  like '%" + filter.getUpdatedBy() + "%' ");
			}
			if (filter.getUsername() != null && !filter.getUsername().isEmpty()) {
				sql.append(" AND fcust.user_id like '%" + filter.getUsername() + "%' ");
			}
			if (filter.getEmail() != null && !filter.getEmail().isEmpty()) {
				sql.append(" AND fcust.email like '%" + filter.getEmail() + "%' ");
			}
			if (filter.getPhone() != null && !filter.getPhone().isEmpty()) {
				sql.append(" AND fcust.phone like '%" + filter.getPhone() + "%' ");
			}
			if (filter.getText1() != null && !filter.getText1().isEmpty()) {
				sql.append(" AND c.user_id like '%" + filter.getText1() + "%' ");
			}
			if (filter.getText2() != null && !filter.getText2().isEmpty()) {
				sql.append(" AND c.email like '%" + filter.getText2() + "%' ");
			}
			if (filter.getText3() != null && !filter.getText3().isEmpty()) {
				sql.append(" AND c.phone like '%" + filter.getText3() + "%' ");
			}
			if (filter.getText4() != null && !filter.getText4().isEmpty()) {
				sql.append(" AND ab.abuse_text like '%" + filter.getText4() + "%' ");
			}
			if (filter.getPartipantCount() != null) {
				sql.append(" AND fp.no_of_likes =" + filter.getPartipantCount());
			}

			if (filter.getCreatedDateStr() != null) {
				if (Util.extractDateElement(filter.getCreatedDateStr(), "DAY") > 0) {
					sql.append(" AND DATEPART(day, fa.created_date) = "
							+ Util.extractDateElement(filter.getCreatedDateStr(), "DAY") + " ");
				}
				if (Util.extractDateElement(filter.getCreatedDateStr(), "MONTH") > 0) {
					sql.append(" AND DATEPART(month, fa.created_date) = "
							+ Util.extractDateElement(filter.getCreatedDateStr(), "MONTH") + " ");
				}
				if (Util.extractDateElement(filter.getCreatedDateStr(), "YEAR") > 0) {
					sql.append(" AND DATEPART(year, fa.created_date) = "
							+ Util.extractDateElement(filter.getCreatedDateStr(), "YEAR") + " ");
				}
			}

			if (filter.getLastUpdatedDateStr() != null) {
				if (Util.extractDateElement(filter.getLastUpdatedDateStr(), "DAY") > 0) {
					sql.append(" AND DATEPART(day, fa.updated_date) = "
							+ Util.extractDateElement(filter.getLastUpdatedDateStr(), "DAY") + " ");
				}
				if (Util.extractDateElement(filter.getCreatedDateStr(), "MONTH") > 0) {
					sql.append(" AND DATEPART(month, fa.updated_date) = "
							+ Util.extractDateElement(filter.getLastUpdatedDateStr(), "MONTH") + " ");
				}
				if (Util.extractDateElement(filter.getCreatedDateStr(), "YEAR") > 0) {
					sql.append(" AND DATEPART(year, fa.updated_date) = "
							+ Util.extractDateElement(filter.getLastUpdatedDateStr(), "YEAR") + " ");
				}
			}

			fanClubSession = FanclubAbuseReportedDAO.getFanClubSession();
			if (fanClubSession == null || !fanClubSession.isOpen() || !fanClubSession.isConnected()) {
				fanClubSession = getSession();
				FanclubAbuseReportedDAO.setFanClubSession(fanClubSession);
			}
			SQLQuery query = fanClubSession.createSQLQuery(sql.toString());
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("customerId", Hibernate.INTEGER);
			query.addScalar("postId", Hibernate.INTEGER);
			query.addScalar("abuseType", Hibernate.STRING);
			query.addScalar("createdUserId", Hibernate.STRING);
			query.addScalar("updatedUserId", Hibernate.STRING);
			query.addScalar("abuseId", Hibernate.INTEGER);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);
			query.addScalar("userId", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("phone", Hibernate.STRING);
			query.addScalar("reportedByUserId", Hibernate.STRING);
			query.addScalar("reportedByEmail", Hibernate.STRING);
			query.addScalar("reportedByPhone", Hibernate.STRING);
			query.addScalar("title", Hibernate.STRING);
			query.addScalar("count", Hibernate.INTEGER);
			query.addScalar("status", Hibernate.STRING);

			abuseList = query.setResultTransformer(Transformers.aliasToBean(FanclubAbuseReported.class)).list();
		
		} catch (Exception e) {
			if (fanClubSession != null && fanClubSession.isOpen()) {
				fanClubSession.close();
			}
			e.printStackTrace();
		}
		return abuseList;
	
	}

	@Override
	public List<FanclubAbuseReported> getFanClubEventAbuseReportedData(String status, GridHeaderFilters filter,SharedProperty prop) {
		List<FanclubAbuseReported> abuseList = null;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("select fa.id as id,fa.customer_id as customerId,fa.fanclub_event_id as eventId,fa.abuse_id as abuseId,ab.abuse_text as abuseType,");
			sql.append("fa.created_user_id as createdUserId,fa.updated_user_id as updatedUserId,fa.created_date as createdDate,fa.updated_date as updatedDate,");
			sql.append("fe.event_name as title,fe.venue_address as description,fe.no_of_interested as count,fe.event_status status,fe.event_date as date,fe.poster_url as posterUrl,");
			sql.append("fcust.user_id as userId,fcust.phone as phone, fcust.email as email,c.user_id as reportedByUserId,c.phone as reportedByPhone,c.email as reportedByEmail ");
			sql.append("from fanclub_customer_reported_abuse_on_media fa with(NOLOCK) ");
			sql.append("join "+prop.getDatabasAlias()+"customer as c with(NOLOCK) on fa.customer_id=c.id ");
			sql.append("join fanclub_events_mst fe with(NOLOCK) on fa.fanclub_event_id=fe.id ");
			sql.append("join "+prop.getDatabasAlias()+"customer as fcust with(NOLOCK) on fe.customer_id = fcust.id ");
			sql.append("join mst_static_abuse_options ab with(NOLOCK) on fa.abuse_id =ab.id ");
			//sql.append("WHERE fe.event_status like '"+status+"' ");
			sql.append("WHERE 1=1 ");

			if (filter.getId() != null) {
				sql.append(" AND fe.id =" + filter.getId());
			}
			if (filter.getArtistName() != null && !filter.getArtistName().isEmpty()) {
				sql.append(" AND fe.event_name like '%" + filter.getArtistName() + "%' ");
			}
			if (filter.getEventDescription() != null && !filter.getEventDescription().isEmpty()) {
				sql.append(" AND fe.venue_address like '%" + filter.getEventDescription() + "%' ");
			}
			if (filter.getStatus() != null && !filter.getStatus().isEmpty()) {
				sql.append(" AND fe.event_status like '%" + filter.getStatus() + "%' ");
			}
			if (filter.getCreatedBy() != null && !filter.getCreatedBy().isEmpty()) {
				sql.append(" AND fa.created_user_id  like '%" + filter.getCreatedBy() + "%' ");
			}
			if (filter.getUpdatedBy() != null && !filter.getUpdatedBy().isEmpty()) {
				sql.append(" AND fa.updated_user_id  like '%" + filter.getUpdatedBy() + "%' ");
			}
			if (filter.getUsername() != null && !filter.getUsername().isEmpty()) {
				sql.append(" AND fcust.user_id like '%" + filter.getUsername() + "%' ");
			}
			if (filter.getEmail() != null && !filter.getEmail().isEmpty()) {
				sql.append(" AND fcust.email like '%" + filter.getEmail() + "%' ");
			}
			if (filter.getPhone() != null && !filter.getPhone().isEmpty()) {
				sql.append(" AND fcust.phone like '%" + filter.getPhone() + "%' ");
			}
			if (filter.getText1() != null && !filter.getText1().isEmpty()) {
				sql.append(" AND c.user_id like '%" + filter.getText1() + "%' ");
			}
			if (filter.getText2() != null && !filter.getText2().isEmpty()) {
				sql.append(" AND c.email like '%" + filter.getText2() + "%' ");
			}
			if (filter.getText3() != null && !filter.getText3().isEmpty()) {
				sql.append(" AND c.phone like '%" + filter.getText3() + "%' ");
			}
			if (filter.getText4() != null && !filter.getText4().isEmpty()) {
				sql.append(" AND ab.abuse_text like '%" + filter.getText4() + "%' ");
			}
			if (filter.getPartipantCount() != null) {
				sql.append(" AND fe.no_of_interested =" + filter.getPartipantCount());
			}

			if (filter.getCreatedDateStr() != null) {
				if (Util.extractDateElement(filter.getCreatedDateStr(), "DAY") > 0) {
					sql.append(" AND DATEPART(day, fa.created_date) = "
							+ Util.extractDateElement(filter.getCreatedDateStr(), "DAY") + " ");
				}
				if (Util.extractDateElement(filter.getCreatedDateStr(), "MONTH") > 0) {
					sql.append(" AND DATEPART(month, fa.created_date) = "
							+ Util.extractDateElement(filter.getCreatedDateStr(), "MONTH") + " ");
				}
				if (Util.extractDateElement(filter.getCreatedDateStr(), "YEAR") > 0) {
					sql.append(" AND DATEPART(year, fa.created_date) = "
							+ Util.extractDateElement(filter.getCreatedDateStr(), "YEAR") + " ");
				}
			}

			if (filter.getLastUpdatedDateStr() != null) {
				if (Util.extractDateElement(filter.getLastUpdatedDateStr(), "DAY") > 0) {
					sql.append(" AND DATEPART(day, fa.updated_date) = "
							+ Util.extractDateElement(filter.getLastUpdatedDateStr(), "DAY") + " ");
				}
				if (Util.extractDateElement(filter.getCreatedDateStr(), "MONTH") > 0) {
					sql.append(" AND DATEPART(month, fa.updated_date) = "
							+ Util.extractDateElement(filter.getLastUpdatedDateStr(), "MONTH") + " ");
				}
				if (Util.extractDateElement(filter.getCreatedDateStr(), "YEAR") > 0) {
					sql.append(" AND DATEPART(year, fa.updated_date) = "
							+ Util.extractDateElement(filter.getLastUpdatedDateStr(), "YEAR") + " ");
				}
			}
			
			if (filter.getEventDateStr() != null) {
				if (Util.extractDateElement(filter.getEventDateStr(), "DAY") > 0) {
					sql.append(" AND DATEPART(day, fe.event_date) = "
							+ Util.extractDateElement(filter.getEventDateStr(), "DAY") + " ");
				}
				if (Util.extractDateElement(filter.getEventDateStr(), "MONTH") > 0) {
					sql.append(" AND DATEPART(month, fe.event_date) = "
							+ Util.extractDateElement(filter.getEventDateStr(), "MONTH") + " ");
				}
				if (Util.extractDateElement(filter.getEventDateStr(), "YEAR") > 0) {
					sql.append(" AND DATEPART(year, fe.event_date) = "
							+ Util.extractDateElement(filter.getEventDateStr(), "YEAR") + " ");
				}
			}

			fanClubSession = FanclubAbuseReportedDAO.getFanClubSession();
			if (fanClubSession == null || !fanClubSession.isOpen() || !fanClubSession.isConnected()) {
				fanClubSession = getSession();
				FanclubAbuseReportedDAO.setFanClubSession(fanClubSession);
			}
			SQLQuery query = fanClubSession.createSQLQuery(sql.toString());
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("customerId", Hibernate.INTEGER);
			query.addScalar("eventId", Hibernate.INTEGER);
			query.addScalar("abuseType", Hibernate.STRING);
			query.addScalar("createdUserId", Hibernate.STRING);
			query.addScalar("updatedUserId", Hibernate.STRING);
			query.addScalar("abuseId", Hibernate.INTEGER);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("date", Hibernate.TIMESTAMP);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);
			query.addScalar("userId", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("phone", Hibernate.STRING);
			query.addScalar("reportedByUserId", Hibernate.STRING);
			query.addScalar("reportedByEmail", Hibernate.STRING);
			query.addScalar("reportedByPhone", Hibernate.STRING);
			query.addScalar("title", Hibernate.STRING);
			query.addScalar("description", Hibernate.STRING);
			query.addScalar("posterUrl", Hibernate.STRING);
			query.addScalar("count", Hibernate.INTEGER);
			query.addScalar("status", Hibernate.STRING);

			abuseList = query.setResultTransformer(Transformers.aliasToBean(FanclubAbuseReported.class)).list();
		} catch (Exception e) {
			if (fanClubSession != null && fanClubSession.isOpen()) {
				fanClubSession.close();
			}
			e.printStackTrace();
		}
		return abuseList;
	}

	@Override
	public List<FanclubAbuseReported> getFanClubVideoAbuseReportedData(String status, GridHeaderFilters filter,SharedProperty prop) {
		List<FanclubAbuseReported> abuseList = null;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("select fa.id as id,fa.customer_id as customerId,fa.fanclub_video_id as videoId,fa.abuse_id as abuseId,ab.abuse_text as abuseType,");
			sql.append("fa.created_user_id as createdUserId,fa.updated_user_id as updatedUserId,fa.created_date as createdDate,fa.updated_date as updatedDate,");
			sql.append("fv.video_title as title,fv.video_description as description,fv.no_of_likes as count,fv.video_status status,fv.video_url as videoUrl,fv.video_thumbnail_url as posterUrl,");
			sql.append("fcust.user_id as userId,fcust.phone as phone, fcust.email as email,c.user_id as reportedByUserId,c.phone as reportedByPhone,c.email as reportedByEmail ");
			sql.append("from fanclub_customer_reported_abuse_on_media fa with(NOLOCK) ");
			sql.append("join "+prop.getDatabasAlias()+"customer as c with(NOLOCK) on fa.customer_id=c.id ");
			sql.append("join fanclub_videos_mst fv with(NOLOCK) on fa.fanclub_video_id=fv.id ");
			sql.append("join "+prop.getDatabasAlias()+"customer as fcust with(NOLOCK) on fv.customer_id = fcust.id ");
			sql.append("join mst_static_abuse_options ab with(NOLOCK) on fa.abuse_id =ab.id ");
			//sql.append("WHERE fv.video_status like '"+status+"' ");
			sql.append("WHERE 1=1 ");

			if (filter.getId() != null) {
				sql.append(" AND fv.id =" + filter.getId());
			}
			if (filter.getArtistName() != null && !filter.getArtistName().isEmpty()) {
				sql.append(" AND fv.video_title like '%" + filter.getArtistName() + "%' ");
			}
			if (filter.getEventDescription() != null && !filter.getEventDescription().isEmpty()) {
				sql.append(" AND fv.video_description like '%" + filter.getEventDescription() + "%' ");
			}
			if (filter.getStatus() != null && !filter.getStatus().isEmpty()) {
				sql.append(" AND fv.video_status like '%" + filter.getStatus() + "%' ");
			}
			if (filter.getCreatedBy() != null && !filter.getCreatedBy().isEmpty()) {
				sql.append(" AND fa.created_user_id  like '%" + filter.getCreatedBy() + "%' ");
			}
			if (filter.getUpdatedBy() != null && !filter.getUpdatedBy().isEmpty()) {
				sql.append(" AND fa.updated_user_id  like '%" + filter.getUpdatedBy() + "%' ");
			}
			if (filter.getUsername() != null && !filter.getUsername().isEmpty()) {
				sql.append(" AND fcust.user_id like '%" + filter.getUsername() + "%' ");
			}
			if (filter.getEmail() != null && !filter.getEmail().isEmpty()) {
				sql.append(" AND fcust.email like '%" + filter.getEmail() + "%' ");
			}
			if (filter.getPhone() != null && !filter.getPhone().isEmpty()) {
				sql.append(" AND fcust.phone like '%" + filter.getPhone() + "%' ");
			}
			if (filter.getText1() != null && !filter.getText1().isEmpty()) {
				sql.append(" AND c.user_id like '%" + filter.getText1() + "%' ");
			}
			if (filter.getText2() != null && !filter.getText2().isEmpty()) {
				sql.append(" AND c.email like '%" + filter.getText2() + "%' ");
			}
			if (filter.getText3() != null && !filter.getText3().isEmpty()) {
				sql.append(" AND c.phone like '%" + filter.getText3() + "%' ");
			}
			if (filter.getText4() != null && !filter.getText4().isEmpty()) {
				sql.append(" AND ab.abuse_text like '%" + filter.getText4() + "%' ");
			}
			if (filter.getPartipantCount() != null) {
				sql.append(" AND fv.no_of_likes =" + filter.getPartipantCount());
			}

			if (filter.getCreatedDateStr() != null) {
				if (Util.extractDateElement(filter.getCreatedDateStr(), "DAY") > 0) {
					sql.append(" AND DATEPART(day, fa.created_date) = "
							+ Util.extractDateElement(filter.getCreatedDateStr(), "DAY") + " ");
				}
				if (Util.extractDateElement(filter.getCreatedDateStr(), "MONTH") > 0) {
					sql.append(" AND DATEPART(month, fa.created_date) = "
							+ Util.extractDateElement(filter.getCreatedDateStr(), "MONTH") + " ");
				}
				if (Util.extractDateElement(filter.getCreatedDateStr(), "YEAR") > 0) {
					sql.append(" AND DATEPART(year, fa.created_date) = "
							+ Util.extractDateElement(filter.getCreatedDateStr(), "YEAR") + " ");
				}
			}

			if (filter.getLastUpdatedDateStr() != null) {
				if (Util.extractDateElement(filter.getLastUpdatedDateStr(), "DAY") > 0) {
					sql.append(" AND DATEPART(day, fa.updated_date) = "
							+ Util.extractDateElement(filter.getLastUpdatedDateStr(), "DAY") + " ");
				}
				if (Util.extractDateElement(filter.getCreatedDateStr(), "MONTH") > 0) {
					sql.append(" AND DATEPART(month, fa.updated_date) = "
							+ Util.extractDateElement(filter.getLastUpdatedDateStr(), "MONTH") + " ");
				}
				if (Util.extractDateElement(filter.getCreatedDateStr(), "YEAR") > 0) {
					sql.append(" AND DATEPART(year, fa.updated_date) = "
							+ Util.extractDateElement(filter.getLastUpdatedDateStr(), "YEAR") + " ");
				}
			}

			fanClubSession = FanclubAbuseReportedDAO.getFanClubSession();
			if (fanClubSession == null || !fanClubSession.isOpen() || !fanClubSession.isConnected()) {
				fanClubSession = getSession();
				FanclubAbuseReportedDAO.setFanClubSession(fanClubSession);
			}
			SQLQuery query = fanClubSession.createSQLQuery(sql.toString());
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("customerId", Hibernate.INTEGER);
			query.addScalar("videoId", Hibernate.INTEGER);
			query.addScalar("abuseType", Hibernate.STRING);
			query.addScalar("createdUserId", Hibernate.STRING);
			query.addScalar("updatedUserId", Hibernate.STRING);
			query.addScalar("abuseId", Hibernate.INTEGER);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);
			query.addScalar("userId", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("phone", Hibernate.STRING);
			query.addScalar("reportedByUserId", Hibernate.STRING);
			query.addScalar("reportedByEmail", Hibernate.STRING);
			query.addScalar("reportedByPhone", Hibernate.STRING);
			query.addScalar("title", Hibernate.STRING);
			query.addScalar("description", Hibernate.STRING);
			query.addScalar("posterUrl", Hibernate.STRING);
			query.addScalar("videoUrl", Hibernate.STRING);
			query.addScalar("count", Hibernate.INTEGER);
			query.addScalar("status", Hibernate.STRING);

			abuseList = query.setResultTransformer(Transformers.aliasToBean(FanclubAbuseReported.class)).list();
		} catch (Exception e) {
			if (fanClubSession != null && fanClubSession.isOpen()) {
				fanClubSession.close();
			}
			e.printStackTrace();
		}
		return abuseList;
	}

	@Override
	public List<FanclubAbuseReported> getFanClubCommentsAbuseReportedData(String status, GridHeaderFilters filter,SharedProperty prop) {
		List<FanclubAbuseReported> abuseList = null;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("select fa.id as id,fa.customer_id as customerId,fa.fanclub_comments_id as commentId,fa.abuse_id as abuseId,ab.abuse_text as abuseType,");
			sql.append("fa.created_user_id as createdUserId,fa.updated_user_id as updatedUserId,fa.created_date as createdDate,fa.updated_date as updatedDate,");
			sql.append("fmc.cust_comment_text as title,fmc.cust_comment_image_url as imageUrl,fmc.comment_status status,fmc.cust_comment_media_url as videoUrl,fmc.cust_comment_poster_url as posterUrl,");
			sql.append("fcust.user_id as userId,fcust.phone as phone, fcust.email as email,c.user_id as reportedByUserId,c.phone as reportedByPhone,c.email as reportedByEmail ");
			sql.append("from fanclub_customer_reported_abuse_on_media fa with(NOLOCK) ");
			sql.append("join "+prop.getDatabasAlias()+"customer as c with(NOLOCK) on fa.customer_id=c.id ");
			sql.append("join fanclub_customer_comments_on_media fmc with(NOLOCK) on fa.fanclub_comments_id=fmc.id ");
			sql.append("join "+prop.getDatabasAlias()+"customer as fcust with(NOLOCK) on fmc.customer_id = fcust.id ");
			sql.append("join mst_static_abuse_options ab with(NOLOCK) on fa.abuse_id =ab.id ");
			//sql.append("WHERE fmc.comment_status like '"+status+"' ");
			sql.append("WHERE 1=1 ");

			if (filter.getId() != null) {
				sql.append(" AND fmc.id =" + filter.getId());
			}
			if (filter.getArtistName() != null && !filter.getArtistName().isEmpty()) {
				sql.append(" AND fmc.cust_comment_text like '%" + filter.getArtistName() + "%' ");
			}
			if (filter.getStatus() != null && !filter.getStatus().isEmpty()) {
				sql.append(" AND fmc.comment_status like '%" + filter.getStatus() + "%' ");
			}
			if (filter.getCreatedBy() != null && !filter.getCreatedBy().isEmpty()) {
				sql.append(" AND fa.created_user_id  like '%" + filter.getCreatedBy() + "%' ");
			}
			if (filter.getUpdatedBy() != null && !filter.getUpdatedBy().isEmpty()) {
				sql.append(" AND fa.updated_user_id  like '%" + filter.getUpdatedBy() + "%' ");
			}
			if (filter.getUsername() != null && !filter.getUsername().isEmpty()) {
				sql.append(" AND fcust.user_id like '%" + filter.getUsername() + "%' ");
			}
			if (filter.getEmail() != null && !filter.getEmail().isEmpty()) {
				sql.append(" AND fcust.email like '%" + filter.getEmail() + "%' ");
			}
			if (filter.getPhone() != null && !filter.getPhone().isEmpty()) {
				sql.append(" AND fcust.phone like '%" + filter.getPhone() + "%' ");
			}
			if (filter.getText1() != null && !filter.getText1().isEmpty()) {
				sql.append(" AND c.user_id like '%" + filter.getText1() + "%' ");
			}
			if (filter.getText2() != null && !filter.getText2().isEmpty()) {
				sql.append(" AND c.email like '%" + filter.getText2() + "%' ");
			}
			if (filter.getText3() != null && !filter.getText3().isEmpty()) {
				sql.append(" AND c.phone like '%" + filter.getText3() + "%' ");
			}
			if (filter.getText4() != null && !filter.getText4().isEmpty()) {
				sql.append(" AND ab.abuse_text like '%" + filter.getText4() + "%' ");
			}
			

			if (filter.getCreatedDateStr() != null) {
				if (Util.extractDateElement(filter.getCreatedDateStr(), "DAY") > 0) {
					sql.append(" AND DATEPART(day, fa.created_date) = "
							+ Util.extractDateElement(filter.getCreatedDateStr(), "DAY") + " ");
				}
				if (Util.extractDateElement(filter.getCreatedDateStr(), "MONTH") > 0) {
					sql.append(" AND DATEPART(month, fa.created_date) = "
							+ Util.extractDateElement(filter.getCreatedDateStr(), "MONTH") + " ");
				}
				if (Util.extractDateElement(filter.getCreatedDateStr(), "YEAR") > 0) {
					sql.append(" AND DATEPART(year, fa.created_date) = "
							+ Util.extractDateElement(filter.getCreatedDateStr(), "YEAR") + " ");
				}
			}

			if (filter.getLastUpdatedDateStr() != null) {
				if (Util.extractDateElement(filter.getLastUpdatedDateStr(), "DAY") > 0) {
					sql.append(" AND DATEPART(day, fa.updated_date) = "
							+ Util.extractDateElement(filter.getLastUpdatedDateStr(), "DAY") + " ");
				}
				if (Util.extractDateElement(filter.getCreatedDateStr(), "MONTH") > 0) {
					sql.append(" AND DATEPART(month, fa.updated_date) = "
							+ Util.extractDateElement(filter.getLastUpdatedDateStr(), "MONTH") + " ");
				}
				if (Util.extractDateElement(filter.getCreatedDateStr(), "YEAR") > 0) {
					sql.append(" AND DATEPART(year, fa.updated_date) = "
							+ Util.extractDateElement(filter.getLastUpdatedDateStr(), "YEAR") + " ");
				}
			}

			fanClubSession = FanclubAbuseReportedDAO.getFanClubSession();
			if (fanClubSession == null || !fanClubSession.isOpen() || !fanClubSession.isConnected()) {
				fanClubSession = getSession();
				FanclubAbuseReportedDAO.setFanClubSession(fanClubSession);
			}
			SQLQuery query = fanClubSession.createSQLQuery(sql.toString());
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("customerId", Hibernate.INTEGER);
			query.addScalar("commentId", Hibernate.INTEGER);
			query.addScalar("abuseType", Hibernate.STRING);
			query.addScalar("createdUserId", Hibernate.STRING);
			query.addScalar("updatedUserId", Hibernate.STRING);
			query.addScalar("abuseId", Hibernate.INTEGER);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			//query.addScalar("date", Hibernate.TIMESTAMP);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);
			query.addScalar("userId", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("phone", Hibernate.STRING);
			query.addScalar("reportedByUserId", Hibernate.STRING);
			query.addScalar("reportedByEmail", Hibernate.STRING);
			query.addScalar("reportedByPhone", Hibernate.STRING);
			query.addScalar("title", Hibernate.STRING);
			query.addScalar("videoUrl", Hibernate.STRING);
			query.addScalar("posterUrl", Hibernate.STRING);
			query.addScalar("imageUrl", Hibernate.STRING);
			query.addScalar("status", Hibernate.STRING);

			abuseList = query.setResultTransformer(Transformers.aliasToBean(FanclubAbuseReported.class)).list();
		} catch (Exception e) {
			if (fanClubSession != null && fanClubSession.isOpen()) {
				fanClubSession.close();
			}
			e.printStackTrace();
		}
		return abuseList;
	}

	
}
