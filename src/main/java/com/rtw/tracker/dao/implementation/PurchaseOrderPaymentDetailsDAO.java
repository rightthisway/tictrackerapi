package com.rtw.tracker.dao.implementation;

import com.rtw.tracker.datas.PurchaseOrderPaymentDetails;

public class PurchaseOrderPaymentDetailsDAO extends HibernateDAO<Integer, PurchaseOrderPaymentDetails> implements com.rtw.tracker.dao.services.PurchaseOrderPaymentDetailsDAO{

	@Override
	public PurchaseOrderPaymentDetails getPaymentDetailsByPOId(Integer poId)throws Exception {
		return findSingle("FROM PurchaseOrderPaymentDetails WHERE purchaseOrderId=?", new Object[]{poId});
	}

}
