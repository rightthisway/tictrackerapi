package com.rtw.tracker.dao.implementation;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.EnumType;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.datas.OpenOrders;
import com.rtw.tracker.datas.SeatGeekOrderStatus;
import com.rtw.tracker.datas.SeatGeekOrders;
import com.rtw.tracker.fedex.Recipient;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.PaginationUtil;

public class SeatGeekOrdersDAO extends HibernateDAO<Integer, SeatGeekOrders> implements com.rtw.tracker.dao.services.SeatGeekOrdersDAO{
	
	public SeatGeekOrders getSeatGeekOrderByOrderId(String orderId) throws Exception {
		return findSingle("FROM SeatGeekOrders where orderId=?", new Object[]{orderId});
	}
	public List<SeatGeekOrders> getAllOpenSeatGeekOrders() throws Exception {
		return find("FROM SeatGeekOrders where status=?", new Object[]{SeatGeekOrderStatus.created});
	}
	public List<SeatGeekOrders> getAllOpenSeatGeekOrdersForAutoconfirm() throws Exception {
		return find("FROM SeatGeekOrders where status in('created','submitted')", new Object[]{});
	}
	
	/*public List<SeatGeekOrders> getTicketUploadedOrders() throws Exception {
		return find("select sg FROM SeatGeekOrders sg,CustomerOrder co,Invoice i " +
				" where status=? and sg.id=co.secondaryOrderId and co.secondaryOrdertype=? and co.id=i.customerOrderId" +
				" and i.isRealTixUploaded='YES'", new Object[]{SeatGeekOrderStatus.confirmed});
	}*/
	public List<SeatGeekOrders> getAllOpenSeatGeekOrdersByStatus(String status) throws Exception {
		
		String query = "FROM SeatGeekOrders WHERE 1=1 ";
		List<Object> param = new ArrayList<Object>();
		
		if(status == null) {
			//query += " AND (status=? or status=?)";
			//param.add(SeatGeekOrderStatus.failed);	
			//param.add(SeatGeekOrderStatus.created);
		} else if(status.equals("created")) {
			query += " AND status=? ";
			param.add(SeatGeekOrderStatus.created);
		} else if(status.equals("failed")) {
			query += " AND status=?";
			param.add(SeatGeekOrderStatus.failed);	
		} else if(status.equals("confirmed")) {
			query += " AND status=?";
			param.add(SeatGeekOrderStatus.confirmed);	
		} else if(status.equals("fulfilled")) {
			query += " AND status=?";
			param.add(SeatGeekOrderStatus.fulfilled);	
		} else if(status.equals("denied")) {
			query += " AND status=?";
			param.add(SeatGeekOrderStatus.denied);	
		}
		/*if (startDate != null) {
			query += " AND e.localDate>=?";
			param.add(startDate);
		} 
		if (endDate != null) {
			query += " AND e.localDate<=?";
			param.add(endDate);								
		} */
		return find(query, param.toArray());	
	}
	
	public List<SeatGeekOrders> getSeatGeekOrdersList(String fromDate, String toDate,String statusStr,GridHeaderFilters filter,String pageNo) throws Exception {
		Session session =null;
		Integer startFrom = 0;
		try {
			Properties paramSgOrderStatus = new Properties();
			paramSgOrderStatus.put("enumClass", "com.rtw.tracker.datas.SeatGeekOrderStatus");
			paramSgOrderStatus.put("type", "12"); /*type 12 instructs to use the String representation of enum value*/
			
			String qry = "select sg.id as id,sg.order_id as orderId,sg.cat_ticket_id as catTicketId,sg.section as section,sg.row as row,sg.quantity as quantity," +
					" sg.total_payment as totalPaymentAmt,sg.total_sale as totalSaleAmt,sg.created_date as createdDate,sg.last_updated as lastUpdatedDate," +
					" sg.status as status,sg.instant_download as instantDownload,sg.event_date as eventDate,sg.event_time as eventTime," +
					" sg.event_name as eventName,sg.venue_name as venueName,sg.source as source,sg.event_id as eventId,sg.order_date as orderDate,sg.ticket_sold_price as ticketSoldPrice," +
					" sg.delivery_method as deliveryMethod,sg.total_amt as totalAmt,sg.sub_total_amt as subtotalAmt,sg.fees_amt as feesAmt," +
					" sg.accepted_by as acceptedBy,sg.rejected_by as rejectedBy,sg.last_updated_by as lastUpdatedBy,sg.seat_geek_file_names as seatGeekFileNames," +
					" co.id as ticTrackerOrderId,i.id as ticTrackerInvoiceId" +
					" from seatgeek_orders sg with(nolock) " +
					" left join customer_order co with(nolock) on co.secondary_order_id=sg.order_id and co.secondary_order_type='SEATGEEK'" +
					" left join invoice i with(nolock) on i.order_id=co.id" +
					" where 1=1 ";
			/*
			if(orderNo!=null && !orderNo.isEmpty()){
				qry += " AND sg.order_id like '%"+orderNo+"%'";
			}
			*/
			if(fromDate!=null && !fromDate.isEmpty()){
				qry = qry+" AND sg.order_date >='"+fromDate+"' ";
			}
			if(toDate!=null && !toDate.isEmpty()){
				qry = qry+" AND sg.order_date <='"+toDate+"' ";
			}
			if(statusStr!=null && !statusStr.isEmpty()){
				qry += " AND sg.status in("+statusStr+")";
			}
			if(filter.getSeatgeekOrderId() != null){
				qry += " AND sg.order_id = '"+filter.getSeatgeekOrderId()+"' ";
			}
			if(filter.getOrderDateStr() != null){
				if(Util.extractDateElement(filter.getOrderDateStr(),"DAY") > 0){
					qry += " AND DATEPART(day,sg.order_date) = "+Util.extractDateElement(filter.getOrderDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getOrderDateStr(),"MONTH") > 0){
					qry += " AND DATEPART(month,sg.order_date) = "+Util.extractDateElement(filter.getOrderDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getOrderDateStr(),"YEAR") > 0){
					qry += " AND DATEPART(year,sg.order_date) = "+Util.extractDateElement(filter.getOrderDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventName() != null){
				qry += " AND sg.event_name like '%"+filter.getEventName()+"%' ";
			}
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					qry += " AND DATEPART(day,sg.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					qry += " AND DATEPART(month,sg.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					qry += " AND DATEPART(year,sg.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventTimeStr() != null){
				if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
					qry += " AND DATEPART(hour,sg.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
					qry += " AND DATEPART(minute,sg.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
				}
			}
			if(filter.getVenueName() != null){
				qry += " AND sg.venue_name like '%"+filter.getVenueName()+"%' ";
			}
			if(filter.getSection() != null){
				qry += " AND sg.section like '%"+filter.getSection()+"%' ";
			}
			if(filter.getRow() != null){
				qry += " AND sg.row like '%"+filter.getRow()+"%' ";
			}
			if(filter.getQuantity() != null){
				qry += " AND sg.quantity = "+filter.getQuantity()+" ";
			}
			if(filter.getTotalSaleAmt() != null){
				qry += " AND sg.total_sale = "+filter.getTotalSaleAmt()+" ";
			}
			if(filter.getTotalPaymentAmt() != null){
				qry += " AND sg.total_payment = "+filter.getTotalPaymentAmt()+" ";
			}
			if(filter.getStatus() != null){
				qry += " AND sg.status like '%"+filter.getStatus()+"%' "; 
			}
			if(filter.getLastUpdatedDateStr() != null){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					qry += " AND DATEPART(day,sg.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
					qry += " AND DATEPART(month,sg.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
					qry += " AND DATEPART(year,sg.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getCustomerOrderId() != null){
				qry += " AND co.id = "+filter.getCustomerOrderId()+" ";
			}
			if(filter.getInvoiceId() != null){
				qry += " AND i.id = "+filter.getInvoiceId()+" ";
			}
			qry += " order by sg.created_date desc";
			session = getSessionFactory().openSession();
			
			SQLQuery sqlQuery = session.createSQLQuery(qry);
			
			sqlQuery.addScalar("status", Hibernate.custom(EnumType.class,paramSgOrderStatus));
			sqlQuery.addScalar("id", Hibernate.INTEGER);
			sqlQuery.addScalar("orderId", Hibernate.STRING);
			sqlQuery.addScalar("catTicketId", Hibernate.INTEGER);
			sqlQuery.addScalar("section", Hibernate.STRING);
			sqlQuery.addScalar("row", Hibernate.STRING);
			sqlQuery.addScalar("quantity", Hibernate.INTEGER);
			sqlQuery.addScalar("totalPaymentAmt", Hibernate.DOUBLE);
			sqlQuery.addScalar("totalSaleAmt", Hibernate.DOUBLE);
			sqlQuery.addScalar("createdDate", Hibernate.TIMESTAMP);
			sqlQuery.addScalar("lastUpdatedDate", Hibernate.TIMESTAMP);
			sqlQuery.addScalar("instantDownload", Hibernate.INTEGER);
			sqlQuery.addScalar("eventName", Hibernate.STRING);
			sqlQuery.addScalar("eventDate", Hibernate.STRING);
			sqlQuery.addScalar("eventTime", Hibernate.STRING);
			sqlQuery.addScalar("venueName", Hibernate.STRING);
			sqlQuery.addScalar("source", Hibernate.STRING);
			sqlQuery.addScalar("eventId", Hibernate.INTEGER);
			sqlQuery.addScalar("orderDate", Hibernate.TIMESTAMP);
			sqlQuery.addScalar("ticketSoldPrice", Hibernate.DOUBLE);
			sqlQuery.addScalar("deliveryMethod", Hibernate.STRING);
			sqlQuery.addScalar("totalAmt", Hibernate.DOUBLE);
			sqlQuery.addScalar("subtotalAmt", Hibernate.DOUBLE);
			sqlQuery.addScalar("feesAmt", Hibernate.DOUBLE);
			sqlQuery.addScalar("acceptedBy", Hibernate.STRING);
			sqlQuery.addScalar("rejectedBy", Hibernate.STRING);
			sqlQuery.addScalar("lastUpdatedBy", Hibernate.STRING);
			sqlQuery.addScalar("seatGeekFileNames", Hibernate.STRING);
			sqlQuery.addScalar("ticTrackerOrderId", Hibernate.INTEGER);
			 sqlQuery.addScalar("ticTrackerInvoiceId", Hibernate.INTEGER);
			 startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
			List<SeatGeekOrders> sgOrdersList = sqlQuery.setResultTransformer(Transformers.aliasToBean(SeatGeekOrders.class)).setFirstResult(startFrom).setMaxResults(PaginationUtil.PAGESIZE).list();
			return sgOrdersList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	
	public List<SeatGeekOrders> getSeatGeekOrdersListToExport(String orderNo,String fromDate, String toDate,String statusStr,GridHeaderFilters filter) throws Exception {
		Session session =null;
		try {
			Properties paramSgOrderStatus = new Properties();
			paramSgOrderStatus.put("enumClass", "com.rtw.tracker.datas.SeatGeekOrderStatus");
			paramSgOrderStatus.put("type", "12"); /*type 12 instructs to use the String representation of enum value*/
			
			String qry = "select sg.id as id,sg.order_id as orderId,sg.cat_ticket_id as catTicketId,sg.section as section,sg.row as row,sg.quantity as quantity," +
					" sg.total_payment as totalPaymentAmt,sg.total_sale as totalSaleAmt,sg.created_date as createdDate,sg.last_updated as lastUpdatedDate," +
					" sg.status as status,sg.instant_download as instantDownload,sg.event_date as eventDate,sg.event_time as eventTime," +
					" sg.event_name as eventName,sg.venue_name as venueName,sg.source as source,sg.event_id as eventId,sg.order_date as orderDate,sg.ticket_sold_price as ticketSoldPrice," +
					" sg.delivery_method as deliveryMethod,sg.total_amt as totalAmt,sg.sub_total_amt as subtotalAmt,sg.fees_amt as feesAmt," +
					" sg.accepted_by as acceptedBy,sg.rejected_by as rejectedBy,sg.last_updated_by as lastUpdatedBy,sg.seat_geek_file_names as seatGeekFileNames," +
					" co.id as ticTrackerOrderId,i.id as ticTrackerInvoiceId" +
					" from seatgeek_orders sg with(nolock) " +
					" left join customer_order co with(nolock) on co.secondary_order_id=sg.order_id and co.secondary_order_type='SEATGEEK'" +
					" left join invoice i with(nolock) on i.order_id=co.id" +
					" where 1=1 ";
			if(orderNo!=null && !orderNo.isEmpty()){
				qry += " AND sg.order_id like '%"+orderNo+"%'";
			}
			if(fromDate!=null && !fromDate.isEmpty()){
				qry = qry+" AND sg.order_date >='"+fromDate+"' ";
			}
			if(toDate!=null && !toDate.isEmpty()){
				qry = qry+" AND sg.order_date <='"+toDate+"' ";
			}
			if(statusStr!=null && !statusStr.isEmpty()){
				qry += " AND sg.status in("+statusStr+")";
			}
			if(filter.getSeatgeekOrderId() != null){
				qry += " AND sg.order_id = '"+filter.getSeatgeekOrderId()+"' ";
			}
			if(filter.getOrderDateStr() != null){
				if(Util.extractDateElement(filter.getOrderDateStr(),"DAY") > 0){
					qry += " AND DATEPART(day,sg.order_date) = "+Util.extractDateElement(filter.getOrderDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getOrderDateStr(),"MONTH") > 0){
					qry += " AND DATEPART(month,sg.order_date) = "+Util.extractDateElement(filter.getOrderDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getOrderDateStr(),"YEAR") > 0){
					qry += " AND DATEPART(year,sg.order_date) = "+Util.extractDateElement(filter.getOrderDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventName() != null){
				qry += " AND sg.event_name like '%"+filter.getEventName()+"%' ";
			}
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					qry += " AND DATEPART(day,sg.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					qry += " AND DATEPART(month,sg.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					qry += " AND DATEPART(year,sg.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventTimeStr() != null){
				if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
					qry += " AND DATEPART(hour,sg.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
					qry += " AND DATEPART(minute,sg.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
				}
			}
			if(filter.getVenueName() != null){
				qry += " AND sg.venue_name like '%"+filter.getVenueName()+"%' ";
			}
			if(filter.getSection() != null){
				qry += " AND sg.section like '%"+filter.getSection()+"%' ";
			}
			if(filter.getRow() != null){
				qry += " AND sg.row like '%"+filter.getRow()+"%' ";
			}
			if(filter.getQuantity() != null){
				qry += " AND sg.quantity = "+filter.getQuantity()+" ";
			}
			if(filter.getTotalSaleAmt() != null){
				qry += " AND sg.total_sale = "+filter.getTotalSaleAmt()+" ";
			}
			if(filter.getTotalPaymentAmt() != null){
				qry += " AND sg.total_payment = "+filter.getTotalPaymentAmt()+" ";
			}
			if(filter.getStatus() != null){
				qry += " AND sg.status like '%"+filter.getStatus()+"%' "; 
			}
			if(filter.getLastUpdatedDateStr() != null){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					qry += " AND DATEPART(day,sg.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
					qry += " AND DATEPART(month,sg.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
					qry += " AND DATEPART(year,sg.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getCustomerOrderId() != null){
				qry += " AND co.id = "+filter.getCustomerOrderId()+" ";
			}
			if(filter.getInvoiceId() != null){
				qry += " AND i.id = "+filter.getInvoiceId()+" ";
			}
			qry += " order by sg.created_date desc";
			session = getSessionFactory().openSession();
			
			SQLQuery sqlQuery = session.createSQLQuery(qry);
			
			sqlQuery.addScalar("status", Hibernate.custom(EnumType.class,paramSgOrderStatus));
			sqlQuery.addScalar("id", Hibernate.INTEGER);
			sqlQuery.addScalar("orderId", Hibernate.STRING);
			sqlQuery.addScalar("catTicketId", Hibernate.INTEGER);
			sqlQuery.addScalar("section", Hibernate.STRING);
			sqlQuery.addScalar("row", Hibernate.STRING);
			sqlQuery.addScalar("quantity", Hibernate.INTEGER);
			sqlQuery.addScalar("totalPaymentAmt", Hibernate.DOUBLE);
			sqlQuery.addScalar("totalSaleAmt", Hibernate.DOUBLE);
			sqlQuery.addScalar("createdDate", Hibernate.TIMESTAMP);
			sqlQuery.addScalar("lastUpdatedDate", Hibernate.TIMESTAMP);
			sqlQuery.addScalar("instantDownload", Hibernate.INTEGER);
			sqlQuery.addScalar("eventName", Hibernate.STRING);
			sqlQuery.addScalar("eventDate", Hibernate.STRING);
			sqlQuery.addScalar("eventTime", Hibernate.STRING);
			sqlQuery.addScalar("venueName", Hibernate.STRING);
			sqlQuery.addScalar("source", Hibernate.STRING);
			sqlQuery.addScalar("eventId", Hibernate.INTEGER);
			sqlQuery.addScalar("orderDate", Hibernate.TIMESTAMP);
			sqlQuery.addScalar("ticketSoldPrice", Hibernate.DOUBLE);
			sqlQuery.addScalar("deliveryMethod", Hibernate.STRING);
			sqlQuery.addScalar("totalAmt", Hibernate.DOUBLE);
			sqlQuery.addScalar("subtotalAmt", Hibernate.DOUBLE);
			sqlQuery.addScalar("feesAmt", Hibernate.DOUBLE);
			sqlQuery.addScalar("acceptedBy", Hibernate.STRING);
			sqlQuery.addScalar("rejectedBy", Hibernate.STRING);
			sqlQuery.addScalar("lastUpdatedBy", Hibernate.STRING);
			sqlQuery.addScalar("seatGeekFileNames", Hibernate.STRING);
			sqlQuery.addScalar("ticTrackerOrderId", Hibernate.INTEGER);
			 sqlQuery.addScalar("ticTrackerInvoiceId", Hibernate.INTEGER);			 
			List<SeatGeekOrders> sgOrdersList = sqlQuery.setResultTransformer(Transformers.aliasToBean(SeatGeekOrders.class)).list();
			return sgOrdersList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	
	public Integer getSeatGeekOrdersListCount(String fromDate, String toDate,String statusStr,GridHeaderFilters filter) throws Exception {
		Session session =null;
		Integer count = 0;
		try {
			Properties paramSgOrderStatus = new Properties();
			paramSgOrderStatus.put("enumClass", "com.rtw.tracker.datas.SeatGeekOrderStatus");
			paramSgOrderStatus.put("type", "12"); /*type 12 instructs to use the String representation of enum value*/
			
			String qry = "select count(*) as cnt from seatgeek_orders sg with(nolock) " +
					" left join customer_order co with(nolock) on co.secondary_order_id=sg.order_id and co.secondary_order_type='SEATGEEK'" +
					" left join invoice i with(nolock) on i.order_id=co.id" +
					" where 1=1 ";
			/*
			if(orderNo!=null && !orderNo.isEmpty()){
				qry += " AND sg.order_id like '%"+orderNo+"%'";
			}
			*/
			if(fromDate!=null && !fromDate.isEmpty()){
				qry = qry+" AND sg.order_date >='"+fromDate+"' ";
			}
			if(toDate!=null && !toDate.isEmpty()){
				qry = qry+" AND sg.order_date <='"+toDate+"' ";
			}
			if(statusStr!=null && !statusStr.isEmpty()){
				qry += " AND sg.status in("+statusStr+")";
			}
			if(filter.getSeatgeekOrderId() != null){
				qry += " AND sg.order_id = '"+filter.getSeatgeekOrderId()+"' ";
			}
			if(filter.getOrderDateStr() != null){
				if(Util.extractDateElement(filter.getOrderDateStr(),"DAY") > 0){
					qry += " AND DATEPART(day,sg.order_date) = "+Util.extractDateElement(filter.getOrderDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getOrderDateStr(),"MONTH") > 0){
					qry += " AND DATEPART(month,sg.order_date) = "+Util.extractDateElement(filter.getOrderDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getOrderDateStr(),"YEAR") > 0){
					qry += " AND DATEPART(year,sg.order_date) = "+Util.extractDateElement(filter.getOrderDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventName() != null){
				qry += " AND sg.event_name like '%"+filter.getEventName()+"%' ";
			}
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					qry += " AND DATEPART(day,sg.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					qry += " AND DATEPART(month,sg.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					qry += " AND DATEPART(year,sg.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventTimeStr() != null){
				if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
					qry += " AND DATEPART(hour,sg.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
					qry += " AND DATEPART(minute,sg.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
				}
			}
			if(filter.getVenueName() != null){
				qry += " AND sg.venue_name like '%"+filter.getVenueName()+"%' ";
			}
			if(filter.getSection() != null){
				qry += " AND sg.section like '%"+filter.getSection()+"%' ";
			}
			if(filter.getRow() != null){
				qry += " AND sg.row like '%"+filter.getRow()+"%' ";
			}
			if(filter.getQuantity() != null){
				qry += " AND sg.quantity = "+filter.getQuantity()+" ";
			}
			if(filter.getTotalSaleAmt() != null){
				qry += " AND sg.total_sale = "+filter.getTotalSaleAmt()+" ";
			}
			if(filter.getTotalPaymentAmt() != null){
				qry += " AND sg.total_payment = "+filter.getTotalPaymentAmt()+" ";
			}
			if(filter.getStatus() != null){
				qry += " AND sg.status like '%"+filter.getStatus()+"%' "; 
			}
			if(filter.getLastUpdatedDateStr() != null){
				if(filter.getLastUpdatedDateStr() != null){
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
						qry += " AND DATEPART(day,sg.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
						qry += " AND DATEPART(month,sg.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
						qry += " AND DATEPART(year,sg.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
					}
				}
			}
			if(filter.getCustomerOrderId() != null){
				qry += " AND co.id = "+filter.getCustomerOrderId()+" ";
			}
			if(filter.getInvoiceId() != null){
				qry += " AND i.id = "+filter.getInvoiceId()+" ";
			}
			session = getSessionFactory().openSession();			
			SQLQuery sqlQuery = session.createSQLQuery(qry);
			count = Integer.parseInt(String.valueOf(sqlQuery.uniqueResult()));			
			return count;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	public List<Object[]> getTicketUploadedOrders() throws Exception {
		Session session = null;
		List<Object[]> list = new ArrayList<Object[]>();
		try {
			session = getSessionFactory().openSession();

			String query = "select sg.order_id as orderId,co.id as customerOrderId,i.id as invoiceId,sg.status " +
					" from seatgeek_orders sg with(nolock) " +
					" inner join customer_order co with(nolock) on co.secondary_order_id=sg.order_id and co.secondary_order_type='SEATGEEK'" +
					" inner join invoice i with(nolock) on i.order_id=co.id" +
					" where sg.status='confirmed' and i.upload_to_exchange=1";

			SQLQuery querys = session.createSQLQuery(query.toString());
			list = querys.list();

		} catch (Exception e) {
			System.out.println("The Exception is" + e);
			throw e;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return list;
	}
	
}
