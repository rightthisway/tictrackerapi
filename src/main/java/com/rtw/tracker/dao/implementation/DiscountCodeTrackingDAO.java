package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.ApplicationPlatform;
import com.rtw.tracker.datas.DiscountCodeTracking;

public class DiscountCodeTrackingDAO extends HibernateDAO<Integer, DiscountCodeTracking> implements com.rtw.tracker.dao.services.DiscountCodeTrackingDAO{
	
	public DiscountCodeTracking getTrackingByCustomerIdByTicketId(Integer cId, Integer eId,Integer tId,String sessionId,ApplicationPlatform platForm){
		try{
			return findSingle("FROM DiscountCodeTracking WHERE customerId = ? and eventId = ? and catTicketGroupId=? " +
					"and sessionId=? and platform=? ", new Object[]{cId,eId,tId,sessionId,platForm});
		}catch (Exception e) {
			e.printStackTrace();
		}
		return new DiscountCodeTracking();
	}

	public List<DiscountCodeTracking> getTrackingByCustomerId(Integer customerId){
		return find("FROM DiscountCodeTracking WHERE customerId = ?", new Object[]{customerId});
	}
	
}
