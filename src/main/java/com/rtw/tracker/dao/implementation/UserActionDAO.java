package com.rtw.tracker.dao.implementation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.datas.UserAction;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.PaginationUtil;

public class UserActionDAO extends HibernateDAO<Integer, UserAction> implements com.rtw.tracker.dao.services.UserActionDAO{

	public Collection<UserAction> getAllActionsByUserId(Integer userId){
		return find("From UserAction where trackerUser.id = ? order by timeStamp desc", new Object[]{userId});
	}
	
	public Collection<UserAction> getAllActionsByUserIdAndDateRange(Integer userId, String fromDate, String toDate, GridHeaderFilters filter, String pageNo){
		Collection<UserAction> userActionList = new ArrayList<UserAction>();
		Session session = null;
		Integer startFrom = 0;
		
		try
		{
			String sqlQuery = "select id as id, user_id as userId, username as userName, action as action, "+
				"ip_address as ipAddress, message as message, time_stamp as timeStamp, data_id as dataId "+
				"from tracker_user_action with(nolock) where 1=1";
			
			if(userId != null){
				sqlQuery += " AND user_id = "+userId;
			}
			if(fromDate != null){
				sqlQuery += " AND time_stamp >= '"+fromDate+"'"; 
			}
			if(toDate != null){
				sqlQuery += " AND time_stamp <= '"+toDate+"'"; 
			}
			if(filter.getUsername() != null){
				sqlQuery += " AND username like '%"+filter.getUsername()+"%' ";
			}
			if(filter.getUserAction() != null){
				sqlQuery += " AND action like '%"+filter.getUserAction()+"%' ";
			}
			if(filter.getClientIPAddress() != null){
				sqlQuery += " AND ip_address like '%"+filter.getClientIPAddress()+"%' ";
			}
			if(filter.getMessage() != null){
				sqlQuery += " AND message like '%"+filter.getMessage()+"%' ";
			}
			if(filter.getLastUpdatedDateStr() != null){
				String dateTimeStr = filter.getLastUpdatedDateStr();
				if(dateTimeStr.contains(" ")){
					String dateTimeArr[] = dateTimeStr.split(" ");
					if(Util.extractDateElement(dateTimeArr[0],"DAY") > 0){
						sqlQuery += " AND DATEPART(day,time_stamp) = "+Util.extractDateElement(dateTimeArr[0],"DAY")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,time_stamp) = "+Util.extractDateElement(dateTimeArr[0],"MONTH")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,time_stamp) = "+Util.extractDateElement(dateTimeArr[0],"YEAR")+" ";
					}
					if(dateTimeArr.length>1){
						String dateTime = dateTimeArr[1];
						if(dateTimeArr.length > 2){
							if(dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()){
								dateTime += " "+dateTimeArr[2];
							}
						}
						if(Util.extractDateElement(dateTime,"HOUR") > 0){
							sqlQuery += " AND DATEPART(hour,time_stamp) = "+Util.extractDateElement(dateTime,"HOUR")+" ";
						}
						if(Util.extractDateElement(dateTime,"MINUTE") > 0){
							sqlQuery += " AND DATEPART(minute,time_stamp) = "+Util.extractDateElement(dateTime,"MINUTE")+" ";
						}
					}
				}else{
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
						sqlQuery += " AND DATEPART(day,time_stamp) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,time_stamp) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,time_stamp) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
					}
				}	
			}
			sqlQuery += " order by time_stamp desc";
			session = getSessionFactory().openSession();
			
			SQLQuery sqlQury = session.createSQLQuery(sqlQuery);
			sqlQury.addScalar("id", Hibernate.INTEGER);
			sqlQury.addScalar("userId", Hibernate.INTEGER);
			sqlQury.addScalar("userName", Hibernate.STRING);
			sqlQury.addScalar("action", Hibernate.STRING);
			sqlQury.addScalar("ipAddress", Hibernate.STRING);
			sqlQury.addScalar("message", Hibernate.STRING);
			sqlQury.addScalar("timeStamp", Hibernate.TIMESTAMP);
			sqlQury.addScalar("dataId", Hibernate.INTEGER);
			startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
			userActionList = sqlQury.setResultTransformer(Transformers.aliasToBean(UserAction.class)).setFirstResult(startFrom).setMaxResults(PaginationUtil.PAGESIZE).list();
			//return find(sqlQuery);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return userActionList;
	}
	
	public Integer getAllActionsByUserIdAndDateRangeCount(Integer userId, String fromDate, String toDate, GridHeaderFilters filter){
		Integer count = 0;
		Session session = null;
		
		try
		{
			String sqlQuery = "select count(*) as cnt "+
				"from tracker_user_action with(nolock) where 1=1";
			
			if(userId != null){
				sqlQuery += " AND user_id = "+userId;
			}
			if(fromDate != null){
				sqlQuery += " AND time_stamp >= '"+fromDate+"'"; 
			}
			if(toDate != null){
				sqlQuery += " AND time_stamp <= '"+toDate+"'"; 
			}
			if(filter.getUsername() != null){
				sqlQuery += " AND username like '%"+filter.getUsername()+"%' ";
			}
			if(filter.getUserAction() != null){
				sqlQuery += " AND action like '%"+filter.getUserAction()+"%' ";
			}
			if(filter.getClientIPAddress() != null){
				sqlQuery += " AND ip_address like '%"+filter.getClientIPAddress()+"%' ";
			}
			if(filter.getMessage() != null){
				sqlQuery += " AND message like '%"+filter.getMessage()+"%' ";
			}
			if(filter.getLastUpdatedDateStr() != null){
				String dateTimeStr = filter.getLastUpdatedDateStr();
				if(dateTimeStr.contains(" ")){
					String dateTimeArr[] = dateTimeStr.split(" ");
					if(Util.extractDateElement(dateTimeArr[0],"DAY") > 0){
						sqlQuery += " AND DATEPART(day,time_stamp) = "+Util.extractDateElement(dateTimeArr[0],"DAY")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,time_stamp) = "+Util.extractDateElement(dateTimeArr[0],"MONTH")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,time_stamp) = "+Util.extractDateElement(dateTimeArr[0],"YEAR")+" ";
					}
					if(dateTimeArr.length>1){
						String dateTime = dateTimeArr[1];
						if(dateTimeArr.length > 2){
							if(dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()){
								dateTime += " "+dateTimeArr[2];
							}
						}
						if(Util.extractDateElement(dateTime,"HOUR") > 0){
							sqlQuery += " AND DATEPART(hour,time_stamp) = "+Util.extractDateElement(dateTime,"HOUR")+" ";
						}
						if(Util.extractDateElement(dateTime,"MINUTE") > 0){
							sqlQuery += " AND DATEPART(minute,time_stamp) = "+Util.extractDateElement(dateTime,"MINUTE")+" ";
						}
					}
				}else{
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
						sqlQuery += " AND DATEPART(day,time_stamp) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,time_stamp) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,time_stamp) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
					}
				}	
			}
			//sqlQuery += " order by time_stamp desc";
			session = getSessionFactory().openSession();
			
			SQLQuery sqlQury = session.createSQLQuery(sqlQuery);
			count = Integer.parseInt(String.valueOf(sqlQury.uniqueResult()));
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return count;
	}
}
