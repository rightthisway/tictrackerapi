package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.datas.RtfGiftCard;
import com.rtw.tracker.pojos.GiftCardAutoComplete;
import com.rtw.tracker.pojos.GiftCardPojo;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.GridSortingUtil;

public class RtfGiftCardDAO extends HibernateDAO<Integer, RtfGiftCard> implements com.rtw.tracker.dao.services.RtfGiftCardDAO{

	
	private static Session genericSession;
	
	public static Session getGenericSession() {
		return genericSession;
	}

	public static void setGenericSession(Session genericSession) {
		RtfGiftCardDAO.genericSession = genericSession;
	}

	public RtfGiftCard getActiveRtfGiftCardById(Integer giftCardId) {
		return findSingle("FROM RtfGiftCard WHERE id=? AND status=? ",new Object[]{giftCardId,Boolean.TRUE});
		
	}
	/*public Collection<RtfGiftCard> filterByName(String pattern) {
		return find("FROM RtfGiftCard WHERE cardType='CONTEST' and status=? AND title LIKE ? ORDER BY title ASC",new Object[]{Boolean.TRUE,"%"+pattern+"%"});
	}*/

	public List<GiftCardPojo> getAllGiftCards(GridHeaderFilters  filter,String status){
		List<GiftCardPojo> cards = null;
		try {
			String sql = "select gc.id as id,gc.title as title,gc.description as description,gcv.card_amount as amount,gcv.max_quantity as maxQuantity,gc.reward_points_conv_rate as pointsConversionRate,"+
					"gcv.remaining_quantity as remainingQty, gcv.used_quantity as usedQuantity,gcv.max_qty_threshold as maxQtyThreshold,gc.card_status as cardStatus,"+
					"gc.created_date as createdDate,gc.updated_date as updatedDate,gc.created_by as createdBy,gc.updated_by as updatedBy, gb.id as brandId,gb.name as name,"+
					"gc.conversion_type as conversionType,gc.conversion_Rate as conversionRate,gc.image_path as imageUrl,gc.card_type as cardType, gc.start_date as startDate, gc.end_date as endDate "+
					"from gift_cards gc join gift_card_value gcv on gc.id=gcv.card_id left join gift_card_brand gb on gb.id=gc.gift_card_brand_id  "+
					"WHERE gc.status=1 AND gc.card_status like '"+status+"'";
			
			if(filter.getId()!=null){
				sql += " AND gc.id = "+filter.getId();
			}
			if(status!=null && status.equalsIgnoreCase("EXPIRED")){
				sql += " AND gcv.status=0 ";
			}else{
				sql += " AND gcv.status=1 ";
			}
			if(filter.getContestName()!=null && !filter.getContestName().isEmpty()){
				sql += " AND gc.title like '%"+filter.getContestName()+"%'";
			}
			if(filter.getEventDescription()!=null && !filter.getEventDescription().isEmpty()){
				sql += " AND gc.description like '%"+filter.getEventDescription()+"%'";
			}
			if(filter.getText2()!=null && !filter.getText2().isEmpty()){
				sql += " AND gc.conversion_type like '%"+filter.getText2()+"%'";
			}
			if(filter.getCategory()!=null && !filter.getCategory().isEmpty()){
				sql += " AND gc.card_type like '%"+filter.getCategory()+"%'";
			}
			if(filter.getTaxAmount()!=null){
				sql += " AND gc.conversion_Rate = "+filter.getTaxAmount();
			}
			if(filter.getInvoiceTotal()!=null){
				sql += " AND gc.reward_points_conv_rate = "+filter.getInvoiceTotal();
			}
			if(filter.getMinimumPurchaseAmount()!=null ){
				sql += " AND gcv.card_amount = "+filter.getMinimumPurchaseAmount();
			}
			if(filter.getEventTixQty()!=null){
				sql += " AND gcv.max_quantity = "+filter.getEventTixQty();
			}
			if(filter.getQuantity()!=null){
				sql += " AND gcv.remaining_quantity = "+filter.getQuantity();
			}
			if(filter.getZoneTixQty()!=null){
				sql += " AND gcv.used_quantity = "+filter.getZoneTixQty();
			}
			if(filter.getTicketWinnerThreshhold()!=null){
				sql += " AND gcv.max_qty_threshold = "+filter.getTicketWinnerThreshhold();
			}
			if(filter.getCreatedBy()!=null){
				sql += " AND gc.created_by like '%"+filter.getCreatedBy()+"%'";
			}
			if(filter.getUpdatedBy()!=null){
				sql += " AND gc.updated_by like '%"+filter.getUpdatedBy()+"%'";
			}
			if(filter.getStatus()!=null && !filter.getStatus().isEmpty()){
				sql += " AND gc.cardStatus like '%"+filter.getStatus()+"%'";
			}
			if(filter.getText3()!=null ){
				if(Util.extractDateElement(filter.getText3(),"DAY") > 0){
					sql += " AND DATEPART(day, gc.start_date) = "+Util.extractDateElement(filter.getText3(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getText3(),"MONTH") > 0){
					sql += " AND DATEPART(month, gc.start_date) = "+Util.extractDateElement(filter.getText3(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getText3(),"YEAR") > 0){
					sql += " AND DATEPART(year, gc.start_date) = "+Util.extractDateElement(filter.getText3(),"YEAR")+" ";
				}
			}
			if(filter.getText4()!=null ){
				if(Util.extractDateElement(filter.getText4(),"DAY") > 0){
					sql += " AND DATEPART(day, gc.end_date) = "+Util.extractDateElement(filter.getText4(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getText4(),"MONTH") > 0){
					sql += " AND DATEPART(month, gc.end_date) = "+Util.extractDateElement(filter.getText4(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getText4(),"YEAR") > 0){
					sql += " AND DATEPART(year, gc.end_date) = "+Util.extractDateElement(filter.getText4(),"YEAR")+" ";
				}
			}
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day, gc.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month, gc.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year, gc.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getUpdatedDate()!=null ){
				if(Util.extractDateElement(filter.getUpdatedDate(),"DAY") > 0){
					sql += " AND DATEPART(day, gc.updated_date) = "+Util.extractDateElement(filter.getUpdatedDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"MONTH") > 0){
					sql += " AND DATEPART(month, gc.updated_date) = "+Util.extractDateElement(filter.getUpdatedDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"YEAR") > 0){
					sql += " AND DATEPART(year, gc.updated_date) = "+Util.extractDateElement(filter.getUpdatedDate(),"YEAR")+" ";
				}
			}
			String sortingSql = GridSortingUtil.getGiftCardsSortingSql(filter);
			if(sortingSql!=null && !sortingSql.isEmpty()){
				sql += sortingSql;
			}else{
				sql +=  " Order by  gc.id desc";
			}
			
			genericSession = RtfGiftCardDAO.getGenericSession();
			if(genericSession==null || !genericSession.isOpen() || !genericSession.isConnected()){
				genericSession = getSession();
				RtfGiftCardDAO.setGenericSession(genericSession);
			}
			SQLQuery query = genericSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("title", Hibernate.STRING);
			query.addScalar("brandId", Hibernate.INTEGER);
			query.addScalar("name", Hibernate.STRING);
			query.addScalar("description", Hibernate.STRING);
			query.addScalar("imageUrl", Hibernate.STRING);
			query.addScalar("conversionType", Hibernate.STRING);
			query.addScalar("conversionRate", Hibernate.DOUBLE);
			query.addScalar("pointsConversionRate", Hibernate.DOUBLE);
			query.addScalar("cardStatus", Hibernate.STRING);
			query.addScalar("cardType", Hibernate.STRING);
			query.addScalar("amount", Hibernate.DOUBLE);
			query.addScalar("maxQuantity", Hibernate.INTEGER);
			query.addScalar("usedQuantity", Hibernate.INTEGER);
			query.addScalar("remainingQty", Hibernate.INTEGER);			
			query.addScalar("maxQtyThreshold", Hibernate.INTEGER);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);
			query.addScalar("updatedBy", Hibernate.STRING);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);	
			query.addScalar("createdBy", Hibernate.STRING);	
			query.addScalar("startDate", Hibernate.TIMESTAMP);	
			query.addScalar("endDate", Hibernate.TIMESTAMP);	
			
			cards = query.setResultTransformer(Transformers.aliasToBean(GiftCardPojo.class)).list();
			
		} catch (Exception e) {
			if(genericSession != null && genericSession.isOpen()){
				genericSession.close();
			}
			e.printStackTrace(); 
		}
		return cards;
	}
	
	public List<GiftCardAutoComplete> filterByName(String pattern) {
		List<GiftCardAutoComplete> cards = null;
		try {
			String sql = "select gv.id as cardValueId,card_id as cardId,gc.title as title,gv.card_amount as cardAmount,gv.remaining_quantity as qty " +
					" from gift_cards gc" + 
					" inner join gift_card_value gv on gv.card_id=gc.id" + 
					" where gc.status=1 and card_type='CONTEST' and gc.card_status='ACTIVE' and gv.status=1" + 
					" and gv.remaining_quantity>0 and gc.title like '%"+pattern+"%'" + 
					" order by gc.title,gv.card_amount,gv.remaining_quantity" + 
					"";
			
			
			genericSession = RtfGiftCardDAO.getGenericSession();
			if(genericSession==null || !genericSession.isOpen() || !genericSession.isConnected()){
				genericSession = getSession();
				RtfGiftCardDAO.setGenericSession(genericSession);
			}
			SQLQuery query = genericSession.createSQLQuery(sql);
			query.addScalar("cardValueId", Hibernate.INTEGER);
			query.addScalar("cardId", Hibernate.INTEGER);
			query.addScalar("title", Hibernate.STRING);
			query.addScalar("cardAmount", Hibernate.DOUBLE);
			query.addScalar("qty", Hibernate.INTEGER);						
			
			cards = query.setResultTransformer(Transformers.aliasToBean(GiftCardAutoComplete.class)).list();
			
		} catch (Exception e) {
			if(genericSession != null && genericSession.isOpen()){
				genericSession.close();
			}
			e.printStackTrace(); 
		}
		return cards;
	}
	

	@Override
	public List<RtfGiftCard> getAllActiveGiftCards() {
		return find("FROM RtfGiftCard WHERE status=? AND cardStatus=? ",new Object[]{Boolean.TRUE,"ACTIVE"});
	}
}
