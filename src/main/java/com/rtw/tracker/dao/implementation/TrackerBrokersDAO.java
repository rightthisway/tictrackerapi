package com.rtw.tracker.dao.implementation;

import java.util.Collection;
import java.util.List;

import com.rtw.tracker.datas.TrackerBrokers;

public class TrackerBrokersDAO extends HibernateDAO<Integer, TrackerBrokers> implements com.rtw.tracker.dao.services.TrackerBrokersDAO{

	public TrackerBrokers getBrokerByName(String companyName) throws Exception{
		return findSingle("From TrackerBrokers where companyName = ?" ,new Object[]{companyName});
	}
	
	public Collection<TrackerBrokers> getBrokersByName(String companyName) throws Exception{
		return find("From TrackerBrokers where companyName like ?" ,new Object[]{"%"+companyName+"%"});
	}
}
