package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.Cards;

public class CardsDAO extends HibernateDAO<Integer, Cards> implements com.rtw.tracker.dao.services.CardsDAO {
	
	public Cards getCardsByCardPosition(Integer productId,Integer desktopPosition,Integer mobilePosition) throws Exception {
		return findSingle("FROM Cards WHERE productId=? and desktopPosition=? and mobilePosition=?", new Object[]{productId,desktopPosition,mobilePosition});
	}
	
	public List<Cards> getAllCardsByProductId(Integer productId) throws Exception {
		return find("FROM Cards WHERE productId=? order by desktopPosition,mobilePosition", new Object[]{productId});
	}
	public void updateCardImageUrl(Cards card) throws Exception {
		bulkUpdate("UPDATE Cards SET imageFileUrl=? WHERE id=?",new Object[]{card.getImageFileUrl(),card.getId()});
	}
	public Cards getExistingCardsByCardPostionsAndCardId(Integer productId,Integer cardId,Integer desktopPosition,Integer mobilePosition) throws Exception {
		if(cardId == null) {
			return findSingle("FROM Cards WHERE productId=? and (desktopPosition=? or mobilePosition=?) ", new Object[]{productId,desktopPosition,mobilePosition});	
		} else {
			return findSingle("FROM Cards WHERE productId=? and (desktopPosition=? or mobilePosition=?)  and id<>? ", new Object[]{productId,desktopPosition,mobilePosition,cardId});
		}
		
	}
}
