package com.rtw.tracker.dao.implementation;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.rtw.tracker.datas.PollingContCategoryMapper;
import com.rtw.tracker.datas.TicketStatus;


public class PollingContestCategoryMapperDAO extends HibernateDAO<Integer, PollingContCategoryMapper>
		implements com.rtw.tracker.dao.services.PollingContestCategoryMapperDAO {

	private static Session pollingSession = null;

	public static Session getPollingSession() {
		return PollingContestCategoryMapperDAO.pollingSession;
	}

	public static void setPollingSession(Session pollingSession) {
		PollingContestCategoryMapperDAO.pollingSession = pollingSession;
	}

	@Override
	public List<PollingContCategoryMapper> getPollingCategoriesMappedForContestId(Integer id) {
		List<PollingContCategoryMapper> pollingContCategoryMapper = new ArrayList<PollingContCategoryMapper>();
		try {
						
			pollingSession = PollingContestCategoryMapperDAO.getPollingSession();
			if (pollingSession == null || !pollingSession.isOpen() || !pollingSession.isConnected()) {
				pollingSession = getSession();				
				PollingContestCategoryMapperDAO.setPollingSession(pollingSession);
			}
			String hql = " from PollingContCategoryMapper p where p.contestId = :contestId";
			pollingContCategoryMapper = (List<PollingContCategoryMapper>) pollingSession.createQuery(hql)
                    .setInteger("contestId", id).list();
                    			
		} catch (Exception e) {
			if (pollingSession != null && pollingSession.isOpen()) {
				pollingSession.close();
			}
			e.printStackTrace();
		}
		return pollingContCategoryMapper;

	}
	
	@Override
	public List<PollingContCategoryMapper> getPollingCategoryByContest(String contestId, String status) {
		List<PollingContCategoryMapper> pollingContCategoryList =  new ArrayList<PollingContCategoryMapper>();
		try 
		{	
			StringBuilder sb = new StringBuilder(); 			
			sb.append("select pc.id as categoryId,pc.title as categoryName,ps.name as sponsorName,pc.polling_type as pollingType " );
			sb.append(" from polling_category pc " );
			sb.append(" join polling_contest_category_mapper pcm on pc.id=pcm.category_id " );
			sb.append(" left join polling_sponsors ps on pc.sponsor_id=ps.id " );
			sb.append(" where pcm.contest_id = "+contestId );			
			pollingSession = PollingContestDAO.getPollingSession();
			if (pollingSession == null || !pollingSession.isOpen() || !pollingSession.isConnected()) {
				pollingSession = getSession();				
				PollingContestDAO.setPollingSession(pollingSession);
			}
			SQLQuery query = pollingSession.createSQLQuery(sb.toString());
			List<Object[]> rows = query.list();
			
			for(Object[] row : rows){
				PollingContCategoryMapper pollingContCategoryMapper = new PollingContCategoryMapper();
				pollingContCategoryMapper.setCategoryId(Integer.parseInt(row[0].toString()));
				pollingContCategoryMapper.setCategoryName(row[1].toString());
				pollingContCategoryMapper.setSponsorName((row[2] == null) ? "" : row[2].toString());
				pollingContCategoryMapper.setPollingType(row[3].toString());
//				System.out.println(pollingContCategoryMapper);
				
				pollingContCategoryList.add(pollingContCategoryMapper);
			}
			
			
		} catch (Exception e) {
			if (pollingSession != null && pollingSession.isOpen()) {
				pollingSession.close();
			}
			e.printStackTrace();
		}
		return pollingContCategoryList;
	}
	
	
	@Override
	public List<PollingContCategoryMapper> getPollingCategoryBySelection(String contestId, String status) {
		List<PollingContCategoryMapper> pollingContCategoryList =  new ArrayList<PollingContCategoryMapper>();
		try 
		{	
			
			StringBuilder sbc = new StringBuilder(); 
			sbc.append("select pcat.id , pcat.title  ,psp.name , pcat.polling_type  from polling_category pcat  " );
			sbc.append("left join polling_sponsors psp on pcat.sponsor_id=psp.id  where pcat.status = 'ACTIVE' AND pcat.id not in( " );
			sbc.append("select category_id  from polling_contest_category_mapper " );
			sbc.append("where status='ACTIVE' AND contest_id = "+contestId +")" );			
			pollingSession = PollingContestDAO.getPollingSession();
			if (pollingSession == null || !pollingSession.isOpen() || !pollingSession.isConnected()) {
				pollingSession = getSession();				
				PollingContestDAO.setPollingSession(pollingSession);
			}
			
			SQLQuery sql = pollingSession.createSQLQuery(sbc.toString());
			List<Object[]> rowsc = sql.list();
			for(Object[] row : rowsc){
				PollingContCategoryMapper pollingContCategoryMapper = new PollingContCategoryMapper();
				pollingContCategoryMapper.setCategoryId(Integer.parseInt(row[0].toString()));
				pollingContCategoryMapper.setCategoryName(row[1].toString());
				pollingContCategoryMapper.setSponsorName((row[2] == null) ? "" : row[2].toString());
				pollingContCategoryMapper.setPollingType(row[3].toString());
				pollingContCategoryMapper.setSelected(false);
				
				pollingContCategoryList.add(pollingContCategoryMapper);
			}
			
		} catch (Exception e) {
			if (pollingSession != null && pollingSession.isOpen()) {
				pollingSession.close();
			}
			e.printStackTrace();
		}
		return pollingContCategoryList;
	}
	
	@Override
	public void deletePollingContCategoryMapper(Integer contestId, Integer categoryId) {
		bulkUpdate("DELETE FROM PollingContCategoryMapper WHERE contestId=? AND categoryId =?",new Object[]{contestId,categoryId});
	}
}