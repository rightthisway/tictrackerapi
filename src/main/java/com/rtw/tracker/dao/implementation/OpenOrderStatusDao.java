package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.OpenOrderStatus;
import com.rtw.tracker.datas.ProductType;

public class OpenOrderStatusDao extends HibernateDAO<Integer, OpenOrderStatus> implements com.rtw.tracker.dao.services.OpenOrderStatusDao{

	/*@Override
	public List<OpenOrderStatus> getSoldTicketDetailsByEventIdAndArtistIdAndVenueIdAndInvoiceDate(String eventId,
			String artistId,String venueId, Date startDate, Date endDate,ProfitLossSign profitLossSign,String productType) {
		try {
			String query ="FROM OpenOrderStatus WHERE status='ACTIVE' ";
			List<Object> params = new ArrayList<Object>();
			
			if(eventId!=null && !eventId.isEmpty()){
				query = query+" AND tmatEventId = ? ";
				params.add(Integer.parseInt(eventId));
			}else {
				if(venueId!=null && !venueId.isEmpty()){
					query = query+" AND venueId = ? ";
					params.add(Integer.parseInt(venueId));
				}else if(artistId!=null && !artistId.isEmpty()){
					query = query+" AND tmatEventId in (SELECT id from EventDetails WHERE artistId = ? )";
					params.add(Integer.parseInt(artistId));
				}
			}
				
			
			if(startDate!=null){
				query = query+" AND invoiceDate >= ? ";
				params.add(startDate);
			}
			if(endDate!=null){
				query = query+" AND invoiceDate <= ? ";
				params.add(endDate);
			}
			
			if(productType!=null && !productType.equalsIgnoreCase("ALL")){
				query += " AND productType = ? ";
				params.add(productType);
			}
			
			if(null !=profitLossSign ){
				
				switch (profitLossSign) {
					case POSITIVE:
						query = query+" AND profitAndLoss > 0 ";
						break;
					case NEGATIVE:
						query = query+" AND profitAndLoss < 0 ";
						break;
					case ZERO:
						query = query+" AND profitAndLoss = 0 ";
						break;
					case ALL:
						query = query+"";
						break;
					default:
						break;
				}
			}

			 query = query + "ORDER BY invoiceDate desc"; 
			 System.out.println(query);
			return find(query, params.toArray());

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}*/
	 
	@Override
	public List<OpenOrderStatus> getAllActiveOpenOrders() {
		return find("FROM OpenOrderStatus WHERE status = 'ACTIVE' ");
	}
	
	@Override
	public List<OpenOrderStatus> getAllActiveOpenOrdersByProduct(ProductType productType) {
		return find("FROM OpenOrderStatus WHERE status = 'ACTIVE' AND productType=?",new Object[]{productType.toString()});
	}
	
	@Override
	public List<OpenOrderStatus> getRTFAllActiveOpenOrders() {
		return find("FROM OpenOrderStatus WHERE status = 'ACTIVE' AND brokerId is null");
	}

	@Override
	public List<OpenOrderStatus> getRTFActiveOrderByInvoiceId(Integer invoiceId, String productType) {
		return find("FROM OpenOrderStatus WHERE status = 'ACTIVE' AND productType=? AND invoiceNo=?",new Object[]{productType, invoiceId});
	}
}
