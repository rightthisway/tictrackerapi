package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.datas.ReferralContest;
import com.rtw.tracker.datas.ReferralContestParticipants;
import com.rtw.tracker.utils.GridHeaderFilters;

public class ReferralContestParticipantsDAO extends HibernateDAO<Integer, ReferralContestParticipants> implements com.rtw.tracker.dao.services.ReferralContestParticipantsDAO{

	public List<ReferralContestParticipants> getParticipantsByContestId(Integer contestId)throws Exception{
		return find("FROM ReferralContestParticipants WHERE contestId = ?", new Object[]{contestId});
	}
	
	public List<ReferralContestParticipants> getParticipantsByCustomerId(Integer customerId)throws Exception{
		return find("FROM ReferralContestParticipants WHERE customerId = ?", new Object[]{customerId});
	}
	
	
	public List<ReferralContestParticipants> getParticipantsListByContestId(Integer contestId, GridHeaderFilters filter)throws Exception{
		Session participantSession = null;
		List<ReferralContestParticipants> participantList = null;		
		try {
			String sql = "select par.id as id, par.contest_id as contestId, par.customer_id as customerId, "+
				"par.purchase_customer_id as purchaseCustomerId, par.referral_code as referralCode, "+
				"par.created_date as createdDate, CONCAT(c.cust_name,' ',c.last_name) AS customerName, "+
				"c.email as customerEmail, CONCAT(cus.cust_name,' ',cus.last_name) AS purchaseCustomerName, "+
				"cus.email as purchaseCustomerEmail "+
				"from referral_contest_participants par with(nolock) "+
				"inner join customer c with(nolock) on c.id = par.customer_id "+
				"inner join customer cus with(nolock) on cus.id = par.purchase_customer_id where 1=1 "; 
				
			if(contestId != null){
				sql += " AND par.contest_id = "+contestId;
			}
			if(filter.getContestId()!=null){
				sql += " AND par.contest_id = "+filter.getContestId();
			}
			if(filter.getCustomerId()!=null){
				sql += " AND par.customer_id = "+filter.getCustomerId();
			}
			if(filter.getPurchaseCustomerId()!=null){
				sql += " AND par.purchase_customer_id = "+filter.getPurchaseCustomerId();
			}
			if(filter.getReferrerCode()!=null && !filter.getReferrerCode().isEmpty()){
				sql += " AND par.referral_code like '%"+filter.getReferrerCode()+"%' ";
			}
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,par.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,par.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,par.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getCustomerName()!=null && !filter.getCustomerName().isEmpty()){
				if(filter.getCustomerName().contains(" ")){
					String names[] = filter.getCustomerName().split(" ");
					sql += " AND (c.cust_name like '%"+names[0]+"%' AND c.last_name like '%"+names[1]+"%') ";
				}else{
					sql += " AND (c.cust_name like '%"+filter.getCustomerName()+"%' OR c.last_name like '%"+filter.getCustomerName()+"%') ";
				}
			}
			if(filter.getEmail()!=null && !filter.getEmail().isEmpty()){
				sql += " AND c.email like '%"+filter.getEmail()+"%' ";
			}
			if(filter.getPurchaseCustomerName()!=null && !filter.getPurchaseCustomerName().isEmpty()){
				if(filter.getPurchaseCustomerName().contains(" ")){
					String names[] = filter.getPurchaseCustomerName().split(" ");
					sql += " AND (cus.cust_name like '%"+names[0]+"%' AND cus.last_name like '%"+names[1]+"%') ";
				}else{
					sql += " AND (cus.cust_name like '%"+filter.getPurchaseCustomerName()+"%' OR cus.last_name like '%"+filter.getPurchaseCustomerName()+"%') ";
				}
			}
			if(filter.getPurchaseCustomerEmail()!=null && !filter.getPurchaseCustomerEmail().isEmpty()){
				sql += " AND cus.email like '%"+filter.getPurchaseCustomerEmail()+"%' ";
			}
			
			participantSession = getSession();
			SQLQuery query = participantSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("contestId", Hibernate.INTEGER);
			query.addScalar("customerId", Hibernate.INTEGER);
			query.addScalar("purchaseCustomerId", Hibernate.INTEGER);
			query.addScalar("referralCode", Hibernate.STRING);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("customerName", Hibernate.STRING);
			query.addScalar("customerEmail", Hibernate.STRING);
			query.addScalar("purchaseCustomerName", Hibernate.STRING);
			query.addScalar("purchaseCustomerEmail", Hibernate.STRING);
			
			participantList = query.setResultTransformer(Transformers.aliasToBean(ReferralContestParticipants.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return participantList;
	}
	
}
