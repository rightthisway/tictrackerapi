package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.CustomerLoyaltyTracking;

public class CustomerLoyaltyTrackingDAO extends HibernateDAO<Integer, CustomerLoyaltyTracking> implements com.rtw.tracker.dao.services.CustomerLoyaltyTrackingDAO{

	public List<CustomerLoyaltyTracking> getTrackingByCustomerId(Integer customerId){
		return find("FROM CustomerLoyaltyTracking WHERE customerId = ?", (new Object[]{customerId}));
	}
	
}
