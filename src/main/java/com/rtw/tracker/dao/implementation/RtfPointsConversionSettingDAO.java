package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.datas.RtfPointsConversionSetting;
import com.rtw.tracker.utils.GridHeaderFilters;

public class RtfPointsConversionSettingDAO extends HibernateDAO<Integer, RtfPointsConversionSetting> implements com.rtw.tracker.dao.services.RtfPointsConversionSettingDAO{

	private static Session pointsSession;
	public static Session getPointsSession() {
		return pointsSession;
	}

	public static void setPointsSession(Session pointsSession) {
		RtfPointsConversionSettingDAO.pointsSession = pointsSession;
	}

	@Override
	public List<RtfPointsConversionSetting> getAllRtfPOintsSettingsByStatus(GridHeaderFilters filter, String status) {
		List<RtfPointsConversionSetting> list = null;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("select id as id, power_up_type as powerUpType, qty as qty, rtf_points as rtfPoints, created_date as createdDate,status as status,");
			sql.append("updated_Date as updatedDate, created_by as createdBy, update_by as updatedBy from rtf_points_conversion_setting ");
			sql.append("where 1=1 ");
			
			if(status!=null && !status.isEmpty()){
				if(status.equalsIgnoreCase("ACTIVE")){
					sql.append(" AND status=1");
				}else if(status.equalsIgnoreCase("INACTIVE")){
					sql.append(" AND status=0");
				}
			}
			if(filter.getCount()!= null){
				sql.append(" AND qty ="+filter.getCount());
			}
			if(filter.getPointWinnerCount()!= null){
				sql.append(" AND rtf_points ="+filter.getPointWinnerCount());
			}
			if(filter.getText1()!=null && !filter.getText1().isEmpty()){
				sql.append(" AND power_up_type like  '%"+filter.getText1()+"%' ");
			}
			if(filter.getStatus()!=null && !filter.getStatus().isEmpty()){
				if(filter.getStatus().equalsIgnoreCase("ACTIVE")){
					sql.append(" AND status=1");
				}else if(filter.getStatus().equalsIgnoreCase("INACTIVE")){
					sql.append(" AND status=0");
				}
			}
			if(filter.getCreatedBy()!=null && !filter.getCreatedBy().isEmpty()){
				sql.append(" AND created_by like  '%"+filter.getCreatedBy()+"%' ");
			}
			if(filter.getUpdatedBy()!=null && !filter.getUpdatedBy().isEmpty()){
				sql.append(" AND update_by like  '%"+filter.getUpdatedBy()+"%' ");
			}
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql.append(" AND DATEPART(day, created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql.append(" AND DATEPART(month, created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql.append(" AND DATEPART(year, created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ");
				}
			}
			if(filter.getUpdatedDate()!=null ){
				if(Util.extractDateElement(filter.getUpdatedDate(),"DAY") > 0){
					sql.append(" AND DATEPART(day,updated_date) = "+Util.extractDateElement(filter.getUpdatedDate(),"DAY")+" ");
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"MONTH") > 0){
					sql.append(" AND DATEPART(month, updated_date) = "+Util.extractDateElement(filter.getUpdatedDate(),"MONTH")+" ");
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"YEAR") > 0){
					sql.append(" AND DATEPART(year, updated_date) = "+Util.extractDateElement(filter.getUpdatedDate(),"YEAR")+" ");
				}
			}
			
			sql.append(" ORDER BY power_up_type,qty");
			
			pointsSession = RtfPointsConversionSettingDAO.getPointsSession();
			if(pointsSession==null || !pointsSession.isOpen() || !pointsSession.isConnected()){
				pointsSession = getSession();
				RtfPointsConversionSettingDAO.setPointsSession(pointsSession);
			}
			SQLQuery query = pointsSession.createSQLQuery(sql.toString());
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("powerUpType", Hibernate.STRING);
			query.addScalar("qty", Hibernate.INTEGER);
			query.addScalar("rtfPoints", Hibernate.INTEGER);
			query.addScalar("status", Hibernate.BOOLEAN);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("updatedBy", Hibernate.STRING);
			
			list = query.setResultTransformer(Transformers.aliasToBean(RtfPointsConversionSetting.class)).list();
		} catch (Exception e) {
			if(pointsSession != null && pointsSession.isOpen()){
				pointsSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<RtfPointsConversionSetting> getRtfPointsSettingByTypeAndQty(String type, Integer qty) {
		return find("FROM RtfPointsConversionSetting WHERE status=? and powerUpType=? AND qty=?",new Object[]{true,type,qty});
	}

}
