package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.dao.implementation.HibernateDAO;
import com.rtw.tracker.datas.FanClub;
import com.rtw.tracker.datas.SuperFanLevels;
import com.rtw.tracker.utils.GridHeaderFilters;

public class FanClubDAO extends HibernateDAO<Integer, FanClub> implements com.rtw.tracker.dao.services.FanClubDAO {

	private static Session fanClubSession;

	public static Session getFanClubSession() {
		return fanClubSession;
	}

	public static void setFanClubSession(Session fanClubSession) {
		FanClubDAO.fanClubSession = fanClubSession;
	}

	@Override
	public List<FanClub> getAllFabClubs(GridHeaderFilters filter, String status, SharedProperty prop) {
		List<FanClub> list = null;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append(
					"SELECT  fm.id as id, fm.title as title, fm.description as description, fm.poster_url as imageUrl,");
			sql.append(
					"fm.category_id as categoryId,fm.customer_id as customerId, fm.created_user_id as createdBy, fm.updated_user_id as updatedBy,");
			sql.append(
					"fm.fanclub_status as status,fm.no_of_members as memberCount, fm.created_date as createdDate, fm.updated_date as updatedDate,");
			sql.append("c.user_id as userId, c.email as email, c.phone as phone, pc.category_name as category ");
			sql.append("FROM fanclub_mst fm with(nolock)  ");
			sql.append("left join " + prop.getDatabasAlias() + "customer c with(nolock)  on fm.customer_id = c.id ");
			sql.append(
					"left join  " + prop.getDatabasAlias() + "polling_video_categories pc with(nolock)  on fm.category_id = pc.id ");
			sql.append("WHERE fm.fanclub_status like '"+status+"' ");

			if (filter.getId() != null) {
				sql.append(" AND fm.id =" + filter.getId());
			}
			if (filter.getArtistName() != null && !filter.getArtistName().isEmpty()) {
				sql.append(" AND fm.title like '%" + filter.getArtistName() + "%' ");
			}
			if (filter.getStatus() != null && !filter.getStatus().isEmpty()) {
				sql.append(" AND fm.fanclub_status like '%" + filter.getStatus() + "%' ");
			}
			if (filter.getEventDescription() != null && !filter.getEventDescription().isEmpty()) {
				sql.append(" AND fm.description like '%" + filter.getEventDescription() + "%' ");
			}
			if (filter.getCreatedBy() != null && !filter.getCreatedBy().isEmpty()) {
				sql.append(" AND fm.created_user_id  like '%" + filter.getCreatedBy() + "%' ");
			}
			if (filter.getUpdatedBy() != null && !filter.getUpdatedBy().isEmpty()) {
				sql.append(" AND fm.updated_user_id  like '%" + filter.getUpdatedBy() + "%' ");
			}
			if (filter.getUsername() != null && !filter.getUsername().isEmpty()) {
				sql.append(" AND c.user_id like '%" + filter.getUsername() + "%' ");
			}
			if (filter.getEmail() != null && !filter.getEmail().isEmpty()) {
				sql.append(" AND c.email like '%" + filter.getEmail() + "%' ");
			}
			if (filter.getPhone() != null && !filter.getPhone().isEmpty()) {
				sql.append(" AND c.phone like '%" + filter.getPhone() + "%' ");
			}
			if (filter.getCategory() != null && !filter.getCategory().isEmpty()) {
				sql.append(" AND pc.category_name like '%" + filter.getCategory() + "%' ");
			}
			if (filter.getPartipantCount() != null) {
				sql.append(" AND fm.no_of_members =" + filter.getPartipantCount());
			}

			if (filter.getCreatedDateStr() != null) {
				if (Util.extractDateElement(filter.getCreatedDateStr(), "DAY") > 0) {
					sql.append(" AND DATEPART(day, fm.created_date) = "
							+ Util.extractDateElement(filter.getCreatedDateStr(), "DAY") + " ");
				}
				if (Util.extractDateElement(filter.getCreatedDateStr(), "MONTH") > 0) {
					sql.append(" AND DATEPART(month, fm.created_date) = "
							+ Util.extractDateElement(filter.getCreatedDateStr(), "MONTH") + " ");
				}
				if (Util.extractDateElement(filter.getCreatedDateStr(), "YEAR") > 0) {
					sql.append(" AND DATEPART(year, fm.created_date) = "
							+ Util.extractDateElement(filter.getCreatedDateStr(), "YEAR") + " ");
				}
			}

			if (filter.getLastUpdatedDateStr() != null) {
				if (Util.extractDateElement(filter.getLastUpdatedDateStr(), "DAY") > 0) {
					sql.append(" AND DATEPART(day, fm.updated_date) = "
							+ Util.extractDateElement(filter.getLastUpdatedDateStr(), "DAY") + " ");
				}
				if (Util.extractDateElement(filter.getCreatedDateStr(), "MONTH") > 0) {
					sql.append(" AND DATEPART(month, fm.updated_date) = "
							+ Util.extractDateElement(filter.getLastUpdatedDateStr(), "MONTH") + " ");
				}
				if (Util.extractDateElement(filter.getCreatedDateStr(), "YEAR") > 0) {
					sql.append(" AND DATEPART(year, fm.updated_date) = "
							+ Util.extractDateElement(filter.getLastUpdatedDateStr(), "YEAR") + " ");
				}
			}

			fanClubSession = FanClubDAO.getFanClubSession();
			if (fanClubSession == null || !fanClubSession.isOpen() || !fanClubSession.isConnected()) {
				fanClubSession = getSession();
				FanClubDAO.setFanClubSession(fanClubSession);
			}
			SQLQuery query = fanClubSession.createSQLQuery(sql.toString());
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("title", Hibernate.STRING);
			query.addScalar("description", Hibernate.STRING);
			query.addScalar("customerId", Hibernate.INTEGER);
			query.addScalar("categoryId", Hibernate.INTEGER);
			query.addScalar("imageUrl", Hibernate.STRING);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("updatedBy", Hibernate.STRING);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("memberCount", Hibernate.INTEGER);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);
			query.addScalar("userId", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("phone", Hibernate.STRING);
			query.addScalar("category", Hibernate.STRING);

			list = query.setResultTransformer(Transformers.aliasToBean(FanClub.class)).list();
		} catch (Exception e) {
			if (fanClubSession != null && fanClubSession.isOpen()) {
				fanClubSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<FanClub> getPopularFanClubList(GridHeaderFilters filter, SharedProperty prop) {
		List<FanClub> list = null;		
		try {
			String sql = "select top(7)  t.id as id , t.title as title,"
					+ "t.description as description, t.poster_url as imageUrl , "
					+ " t.category_id as categoryId, t.no_of_members as memberCount from fanclub_mst t with(nolock)  "
					+ " where t.fanclub_status = 'ACTIVE'	  ORDER BY t.NO_OF_MEMBERS DESC ";
			fanClubSession = FanClubDAO.getFanClubSession();
			if (fanClubSession == null || !fanClubSession.isOpen() || !fanClubSession.isConnected()) {
				fanClubSession = getSession();
				FanClubDAO.setFanClubSession(fanClubSession);
			}

			SQLQuery query = fanClubSession.createSQLQuery(sql.toString());
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("title", Hibernate.STRING);
			query.addScalar("description", Hibernate.STRING);
			query.addScalar("imageUrl", Hibernate.STRING);
			query.addScalar("categoryId", Hibernate.INTEGER);
			query.addScalar("memberCount", Hibernate.INTEGER);

			list = query.setResultTransformer(Transformers.aliasToBean(FanClub.class)).list();
		} catch (Exception e) {
			if (fanClubSession != null && fanClubSession.isOpen()) {
				fanClubSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}

}
