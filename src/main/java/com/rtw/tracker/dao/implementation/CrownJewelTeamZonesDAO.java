package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.CrownJewelTeamZones;
import com.rtw.tracker.datas.CrownJewelTeams;

public class CrownJewelTeamZonesDAO extends HibernateDAO<Integer, CrownJewelTeamZones> implements com.rtw.tracker.dao.services.CrownJewelTeamZonesDAO{

	public List<CrownJewelTeamZones> getAllTeamZonesByLeagueId(Integer leagueId) throws Exception {
		return find("SELECT tz FROM CrownJewelTeamZones tz,CrownJewelTeams te where te.id=tz.teamId and te.leagueId=?", new Object[] {leagueId});	
	}
	public List<CrownJewelTeamZones> getAllTeamZonesByTeamId(Integer teamId) throws Exception {
		return find("FROM CrownJewelTeamZones where teamId=?", new Object[] {teamId});	
	}
	public List<CrownJewelTeamZones> getAllActiveTeamZonesByLeagueId(Integer leagueId) throws Exception {
		return find("SELECT tz FROM CrownJewelTeamZones tz,CrownJewelTeams te where te.id=tz.teamId and tz.status='ACTIVE' and te.leagueId=?", new Object[] {leagueId});	
	}
	public List<CrownJewelTeamZones> getAllActiveTeamZonesByTeamId(Integer teamId) throws Exception {
		return find("FROM CrownJewelTeamZones where status='ACTIVE' and teamId=?", new Object[] {teamId});	
	}
	public List<CrownJewelTeamZones> getAllActiveTeamZonesByLeagueIdAndCity(Integer leagueId,String city) throws Exception {
		return find("SELECT tz FROM CrownJewelTeamZones tz,CrownJewelTeams te where te.id=tz.teamId " +
				" and tz.status='ACTIVE' and te.leagueId=? and te.event.city=?", new Object[] {leagueId,city});	
	}
	@Override
	public CrownJewelTeamZones getActiveZoneByTeamAndZoneDescription(Integer teamId,String zone){
			return findSingle("FROM CrownJewelTeamZones where teamId=? and zone=?", new Object[] {teamId,zone});
	}
}

