package com.rtw.tracker.dao.implementation;


import java.util.List;

import com.rtw.tracker.datas.RTFPromoType;

public class RTFPromoTypeDAO extends HibernateDAO<Integer, RTFPromoType> implements com.rtw.tracker.dao.services.RTFPromoTypeDAO{

	public List<RTFPromoType> getPromoTypeFromOfferId(Integer id){
		return find("FROM RTFPromoType where promoOfferId = ?", new Object[]{id});
	}
}
