package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.datas.SuperFanLevels;
import com.rtw.tracker.utils.GridHeaderFilters;

public class SuperFanLevelsDAO extends HibernateDAO<Integer, SuperFanLevels> implements com.rtw.tracker.dao.services.SuperFanLevelsDAO{

	
	private static Session superfanSession;
	public static Session getSuperfanSession() {
		return superfanSession;
	}

	public static void setSuperfanSession(Session superfanSession) {
		SuperFanLevelsDAO.superfanSession = superfanSession;
	}



	@Override
	public List<SuperFanLevels> getAllActiveSuperFanLevels(GridHeaderFilters filter, String status) {
		List<SuperFanLevels> list =null;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("select id as id, sf_star_from as starsFrom,sf_star_to as starsTo, level_no as levelNo, level_title as title, status as status,");
			sql.append("created_date as createdDate, updated_date as updatedDate, created_by as createdBy, updated_by as updatedBy ");
			sql.append("from quiz_super_fan_level_config WHERE status like '"+status+"' ");
			
			
			if(filter.getCount()!=null){
				sql.append(" AND sf_star_from ="+filter.getCount());
			}
			if(filter.getEventCount()!=null){
				sql.append(" AND level_no ="+filter.getEventCount());
			}
			if(filter.getNoOfTixCount()!=null){
				sql.append(" AND sf_star_to ="+filter.getNoOfTixCount());
			}
			
			if(filter.getText1()!=null && !filter.getText1().isEmpty()){
				sql.append(" AND level_title like '%"+filter.getText1()+"%' ");
			}
			if(filter.getCreatedBy()!=null && !filter.getCreatedBy().isEmpty()){
				sql.append(" AND created_by like '%"+filter.getCreatedBy()+"%' ");
			}
			if(filter.getUpdatedBy()!=null && !filter.getUpdatedBy().isEmpty()){
				sql.append(" AND updated_by like '%"+filter.getUpdatedBy()+"%' ");
			}
			
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql.append(" AND DATEPART(day, created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql.append(" AND DATEPART(month, created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql.append(" AND DATEPART(year, created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ");
				}
			}
			
			if(filter.getLastUpdatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					sql.append(" AND DATEPART(day, updated_date) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql.append(" AND DATEPART(month, updated_date) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql.append(" AND DATEPART(year, updated_date) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ");
				}
			}
			
			superfanSession = SuperFanLevelsDAO.getSuperfanSession();
			if(superfanSession==null || !superfanSession.isOpen() || !superfanSession.isConnected()){
				superfanSession = getSession();
				SuperFanLevelsDAO.setSuperfanSession(superfanSession);
			}
			SQLQuery query = superfanSession.createSQLQuery(sql.toString());
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("starsFrom", Hibernate.INTEGER);
			query.addScalar("starsTo", Hibernate.INTEGER);
			query.addScalar("levelNo", Hibernate.INTEGER);
			query.addScalar("title", Hibernate.STRING);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("updatedBy", Hibernate.STRING);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);
			list = query.setResultTransformer(Transformers.aliasToBean(SuperFanLevels.class)).list();
		} catch (Exception e) {
			if(superfanSession != null && superfanSession.isOpen()){
				superfanSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public SuperFanLevels getSuperFanLevelByLeveNo(Integer levelNo) {
		return findSingle("FROM SuperFanLevels WHERE levelNo=? AND status=? ",new Object[]{levelNo,"ACTIVE"});
	}

}
