package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.State;

public class StateDAO extends HibernateDAO<Integer, State> implements com.rtw.tracker.dao.services.StateDAO{
	
	
	/**
	 *  method to get State by country id
	 * @param countryId, Id of a country
	 * @return list of states
	 */
	public List<State> getAllStateByCountryId(Integer countryId) {
		return find("from State  where countryId= ? order by name", new Object[]{countryId});
	}
	/**
	 *  method to get State by  id
	 * @param id, own id
	 * @return State
	 */
	public State getStateById(Integer id) {
		return findSingle("from State  where id= ? ", new Object[]{id});
	}

	
	public Integer getStateByName(String name) {
		State state = findSingle("from State  where name = ?", new Object[]{name});
		return state!=null?state.getId():0;
	}
	
	public List<State> getAllStateByCountryIds(String countryId) {
		return find("from State  where countryId in("+countryId+") order by name");
	}
	
	public List<State> getAllStateForUSA() {
		return find("from State  where countryId=217 and id between 1 and 51 order by name");
	}
	
	public State getStateByShortDesc(String shortDesc, Integer countryId){
		return findSingle("from State where shortDesc = ? and countryId = ?",new Object[]{shortDesc, countryId});
	}
}
