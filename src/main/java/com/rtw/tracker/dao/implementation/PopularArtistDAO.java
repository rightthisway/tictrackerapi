package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.PopularArtist;

public class PopularArtistDAO extends HibernateDAO<Integer, PopularArtist> implements com.rtw.tracker.dao.services.PopularArtistDAO{


	/*public Collection<PopularArtist> getAllActivePopularArtistByProductId(Integer productId) throws Exception {
		return find("FROM PopularArtist WHERE status='ACTIVE' and productId=? ",new Object[]{productId});
	}*/
	public Integer getAllActivePopularArtistCountByProductId(Integer productId) throws Exception {

		List list = find("SELECT count(id) FROM PopularArtist WHERE status='ACTIVE' and productId=?",new Object[]{productId});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();		
	}
}
