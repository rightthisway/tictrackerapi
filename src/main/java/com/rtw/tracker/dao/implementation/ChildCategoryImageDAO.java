package com.rtw.tracker.dao.implementation;

import com.rtw.tracker.datas.ChildCategoryImage;

public class ChildCategoryImageDAO extends HibernateDAO<Integer, ChildCategoryImage> implements com.rtw.tracker.dao.services.ChildCategoryImageDAO {
	
	public ChildCategoryImage getChildCategoryImageByChildCategoryId(Integer childCategoryId) throws Exception { 
		return findSingle("From ChildCategoryImage where childCategory.id=?", new Object[]{childCategoryId});		
	}
}
