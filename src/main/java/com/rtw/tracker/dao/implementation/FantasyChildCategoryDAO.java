package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.FantasyChildCategory;

public class FantasyChildCategoryDAO extends HibernateDAO<Integer, FantasyChildCategory> implements com.rtw.tracker.dao.services.FantasyChildCategoryDAO{

	public List<FantasyChildCategory> getAll(){
		return find("FROM FantasyChildCategory order by name asc");
	}
	
	public FantasyChildCategory getChildCategoryByName(String name){
		 return findSingle("FROM FantasyChildCategory where name = ?", new Object[]{name});
	}
	
	public FantasyChildCategory getChildCategoryById(Integer id){
		return findSingle("FROM FantasyChildCategory where id = ?", new Object[]{id});
	}
	
	public List<FantasyChildCategory> getChildCategoryByParentId(Integer id){
		return find("FROM FantasyChildCategory where parentId = ?", new Object[]{id});
	}
	
}
