package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.CrownJewelLeagues;

public class CrownJewelLeaguesDAO extends HibernateDAO<Integer, CrownJewelLeagues> implements com.rtw.tracker.dao.services.CrownJewelLeaguesDAO{

	public List<CrownJewelLeagues> getActiveLeaguesByGrandChildId(Integer grandChildId) throws Exception {		
		return find("FROM CrownJewelLeagues WHERE status='ACTIVE' and grandChildId=? order by name",new Object[]{grandChildId});
	}
	

	@Override
	public List<CrownJewelLeagues> getAllActiveLeagues() throws Exception {
		return find("FROM CrownJewelLeagues WHERE status='ACTIVE'");
	}
}
