package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.datas.PollingVideoCategory;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.GridSortingUtil;

public class PollingVideoCategoryDAO extends HibernateDAO<Integer, PollingVideoCategory>
		implements com.rtw.tracker.dao.services.PollingVideoCategoryDAO {

	private static Session pollingSession = null;

	public static Session getPollingSession() {
		return PollingVideoCategoryDAO.pollingSession;
	}

	public static void setPollingSession(Session pollingSession) {
		PollingVideoCategoryDAO.pollingSession = pollingSession;
	}

	@Override
	public List<PollingVideoCategory> getAllPollingVideoCategory(GridHeaderFilters filter, String status) {
		List<PollingVideoCategory> pollingVideoCategoryList = null;
		try {
			
			StringBuilder sb = new StringBuilder(); 			
			sb.append("select g.id as id,image_url as imageUrl, " );
			sb.append(" g.category_name as categoryName , g.description as description,g.icon_url as iconUrl, " );
			sb.append(" g.sequence_num as sequenceNum ,g.image_url as imageUrl, g.updated_by as updatedBy , g.updated_date as updatedDate,  " );		
			sb.append(" created_date as createdDate,created_by as createdBy from polling_video_categories g WHERE 1=1" );
			if(filter.getId() !=null){
				sb.append(" AND id="+filter.getId());
			}
			if(filter.getEventDescription()!=null){
				sb.append(" AND description like '%"+filter.getEventDescription()+"%'");
			}
			if(filter.getUrl()!=null){
				sb.append(" AND image_url like '%"+filter.getUrl()+"%'");
			}
			if(filter.getCategory()!=null){
				sb.append(" AND category_name like '%"+filter.getCategory()+"%'");
			}
			if(filter.getCreatedBy()!=null){
				sb.append(" AND created_by like '%"+filter.getCreatedBy()+"%'");
			}
			
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sb.append(" AND DATEPART(day, created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sb.append(" AND DATEPART(month,created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sb.append(" AND DATEPART(year,created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ");
				}
			}
			
			String sortingSql = GridSortingUtil.getPollingVideoCategorySortingSql(filter);
			if(sortingSql!=null && !sortingSql.isEmpty()){
				sb.append(" "+sortingSql);
			}else{
				sb.append(" order by g.id desc");
			}
			pollingSession = PollingVideoCategoryDAO.getPollingSession();
			if (pollingSession == null || !pollingSession.isOpen() || !pollingSession.isConnected()) {
				pollingSession = getSession();				
				PollingVideoCategoryDAO.setPollingSession(pollingSession);
			}
			
			SQLQuery query = pollingSession.createSQLQuery(sb.toString());
			query.addScalar("id", Hibernate.INTEGER);			
			query.addScalar("categoryName", Hibernate.STRING);
			query.addScalar("description", Hibernate.STRING);
			query.addScalar("sequenceNum", Hibernate.INTEGER);
			query.addScalar("imageUrl", Hibernate.STRING);
			query.addScalar("iconUrl", Hibernate.STRING);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);
			query.addScalar("updatedBy", Hibernate.STRING);

			pollingVideoCategoryList = query.setResultTransformer(Transformers.aliasToBean(PollingVideoCategory.class)).list();
		} catch (Exception e) {
			if (pollingSession != null && pollingSession.isOpen()) {
				pollingSession.close();
			}
			e.printStackTrace();
		}
		return pollingVideoCategoryList;

	}

	@Override
	public Integer getMaxSeqPollingCategory() {
		int count = 0;
		try 
		{	
			String sql = "select ISNULL(MAX(sequence_num), 0)  from polling_video_categories ";			
			pollingSession = PollingContestDAO.getPollingSession();
			if (pollingSession == null || !pollingSession.isOpen() || !pollingSession.isConnected()) {
				pollingSession = getSession();				
				PollingContestDAO.setPollingSession(pollingSession);
			}
			Query query = pollingSession.createSQLQuery(sql);
			count = Integer.valueOf((query.uniqueResult()).toString());
		} catch (Exception e) {
			if (pollingSession != null && pollingSession.isOpen()) {
				pollingSession.close();
			}
			e.printStackTrace();
		}
		return count;
	}
	
	@Override
	public Integer getVideoInventoryLinkedToCategory(String catId) {
		int count = 0;
		try 
		{	
			String sql = "select count(*)  from polling_video_inventory where category_id = '"+catId+"'";			
			pollingSession = PollingContestDAO.getPollingSession();
			if (pollingSession == null || !pollingSession.isOpen() || !pollingSession.isConnected()) {
				pollingSession = getSession();				
				PollingContestDAO.setPollingSession(pollingSession);
			}
			Query query = pollingSession.createSQLQuery(sql);
			count = Integer.valueOf((query.uniqueResult()).toString());
		} catch (Exception e) {
			if (pollingSession != null && pollingSession.isOpen()) {
				pollingSession.close();
			}
			e.printStackTrace();
		}
		return count;
	}
	
	@Override
	public PollingVideoCategory getPollingVideoCategoryById(Integer id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PollingVideoCategory savePollingVideoCategory(PollingVideoCategory pollingVideoCategory) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	

	@Override
	public List<PollingVideoCategory> getPopularVideoChannels(GridHeaderFilters filter) {
		List<PollingVideoCategory> pollingVideoCategoryList = null;
		try {
			
			StringBuilder sb = new StringBuilder(); 			
			sb.append("select g.id as id,image_url as imageUrl,g.icon_url as iconUrl," );
			sb.append(" g.category_name as categoryName  " );					
			sb.append("  from polling_video_categories g order by g.sequence_num " );
			
			pollingSession = PollingVideoCategoryDAO.getPollingSession();
			if (pollingSession == null || !pollingSession.isOpen() || !pollingSession.isConnected()) {
				pollingSession = getSession();				
				PollingVideoCategoryDAO.setPollingSession(pollingSession);
			}
			
			SQLQuery query = pollingSession.createSQLQuery(sb.toString());
			query.addScalar("id", Hibernate.INTEGER);			
			query.addScalar("categoryName", Hibernate.STRING);			
			query.addScalar("imageUrl", Hibernate.STRING);
			query.addScalar("iconUrl", Hibernate.STRING);
			pollingVideoCategoryList = query.setResultTransformer(Transformers.aliasToBean(PollingVideoCategory.class)).list();
		} catch (Exception e) {
			if (pollingSession != null && pollingSession.isOpen()) {
				pollingSession.close();
			}
			e.printStackTrace();
		}
		return pollingVideoCategoryList;

	}

	
	
	
	
	
	
	
	
	
	
	
	
}