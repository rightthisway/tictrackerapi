package com.rtw.tracker.dao.implementation;

import com.rtw.tracker.datas.ArtistImage;

public class ArtistImageDAO extends HibernateDAO<Integer, ArtistImage> implements com.rtw.tracker.dao.services.ArtistImageDAO {
	
	public ArtistImage getArtistImageByArtistId(Integer artistId) throws Exception { 
		return findSingle("From ArtistImage where artist.id=?", new Object[]{artistId});		
	}
	
	public void updateArtistImageUrl(ArtistImage artistImage) throws Exception {
		bulkUpdate("UPDATE ArtistImage SET imageFileUrl=? WHERE id=?",new Object[]{artistImage.getImageFileUrl(),artistImage.getId()});
	}
}
