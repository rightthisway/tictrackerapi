package com.rtw.tracker.dao.implementation;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.rtf.ecomerce.data.RTFSellerDetails;
import com.rtf.ecomerce.utils.GridHeaderFilters;
import com.rtw.tmat.utils.Util;
import com.rtw.tracker.datas.CustomerOrder;
import com.rtw.tracker.enums.SecondaryOrderType;

public class CustomerOrderDAO extends HibernateDAO<Integer, CustomerOrder> implements com.rtw.tracker.dao.services.CustomerOrderDAO{

	@Override
	public List<CustomerOrder> getCustomerOrderByIds(List<Integer> ids)throws Exception {
		return getHibernateTemplate().findByNamedParam("FROM CustomerOrder WHERE id in(:ids)", "ids", ids);
	}

	
	public CustomerOrder getCustomerOrderBySeatGeekOrderId(String seatGeekOrderId)throws Exception {
		return findSingle("FROM CustomerOrder WHERE secondaryOrderId=? and secondaryOrdertype=?", new Object[] {seatGeekOrderId,SecondaryOrderType.SEATGEEK});
	}
	
	public CustomerOrder getCustomerOrderByTransactionId(String transactionId)throws Exception {
		return findSingle("FROM CustomerOrder WHERE primaryTransactionId = ? OR secondaryTransactionId=? OR thirdTransactionId=?", new Object[] {transactionId,transactionId,transactionId});
	}
	
	public CustomerOrder getCustomerOrderByIdAndBrokerId(Integer id, Integer brokerId)throws Exception{
		return findSingle("FROM CustomerOrder WHERE id = ? AND brokerId = ?", new Object[]{id, brokerId});
	}
	
	@SuppressWarnings("unchecked")
	public List<RTFSellerDetails> getAllRTFSellerDetails(GridHeaderFilters filter,String status){
		List<RTFSellerDetails> rewardList = new ArrayList<RTFSellerDetails>();
		try{
			String sqlQuery = "From CustomerOrder where status='"+status+"' ";
			
			if(filter.getfName() != null){
				sqlQuery += " AND fName like '%"+filter.getfName()+"%' ";
			}
			if(filter.getlName() != null){
				sqlQuery += " AND lName like '%"+filter.getlName()+"%' ";
			}
			if(filter.getCompanyName() != null){
				sqlQuery += " AND compName like '%"+filter.getCompanyName()+"%' ";	
			}
			if(filter.getEmail() != null){
				sqlQuery += " AND email like '% "+filter.getEmail()+"%' ";
			}
			if(filter.getPhone() != null){
				sqlQuery += " AND phone like '% "+filter.getPhone()+"%' ";
			}
			if(filter.getAddressLine1() != null){
				sqlQuery += " AND addLine1 like '% "+filter.getAddressLine1()+"%' ";	
			}
			if(filter.getAddressLine2() != null){
				sqlQuery += " AND addLine2 like '% "+filter.getAddressLine2()+"%' ";
			}
			if(filter.getCity() != null){
				sqlQuery += " AND city like '% "+filter.getCity()+"%' ";
			}
			if(filter.getState() != null){
				sqlQuery += " AND state like '% "+filter.getState()+"%' ";	
			}
			if(filter.getCountry() != null){
				sqlQuery += " AND country like '% "+filter.getCountry()+"%' ";	
			}
			if(filter.getZipCode() != null){
				sqlQuery += " AND pincode like '% "+filter.getZipCode()+"%' ";	
			}
			if(filter.getStatus() != null){
				sqlQuery += " AND status like '% "+filter.getStatus()+"%' ";	
			}
			if(filter.getLastUpdatedBy() != null){
				sqlQuery += " AND updBy like '% "+filter.getLastUpdatedBy()+"%' ";	
			}
			if(filter.getLastUpdatedDateStr() != null){
				String dateTimeStr = filter.getLastUpdatedDateStr();
				if(dateTimeStr.contains(" ")){
					String dateTimeArr[] = dateTimeStr.split(" ");
					if(Util.extractDateElement(dateTimeArr[0],"DAY") > 0){
						sqlQuery += " AND DATEPART(day,updDate) = "+Util.extractDateElement(dateTimeArr[0],"DAY")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,updDate) = "+Util.extractDateElement(dateTimeArr[0],"MONTH")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,updDate) = "+Util.extractDateElement(dateTimeArr[0],"YEAR")+" ";
					}
					if(dateTimeArr.length>1){
						String dateTime = dateTimeArr[1];
						if(dateTimeArr.length > 2){
							if(dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()){
								dateTime += " "+dateTimeArr[2];
							}
						}
						if(Util.extractDateElement(dateTime,"HOUR") > 0){
							sqlQuery += " AND DATEPART(hour,updDate) = "+Util.extractDateElement(dateTime,"HOUR")+" ";
						}
						if(Util.extractDateElement(dateTime,"MINUTE") > 0){
							sqlQuery += " AND DATEPART(minute,updDate) = "+Util.extractDateElement(dateTime,"MINUTE")+" ";
						}
					}
				}else{
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
						sqlQuery += " AND DATEPART(day,updDate) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,updDate) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,updDate) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
					}
				}
			}
			
			return find(sqlQuery);
		}catch(Exception e){
			e.printStackTrace();
		}
		return rewardList;
	}
	
	public Integer getAllRTFSellerDetailsCount(GridHeaderFilters filter,String status){
		Integer count = 0;
		try{
			String sqlQuery = "select count(*) as cnt from rtf_seller_dtls with(nolock) where sler_status='"+status+"' ";

			if(filter.getfName() != null){
				sqlQuery += " AND first_name like '%"+filter.getfName()+"%' ";
			}
			if(filter.getlName() != null){
				sqlQuery += " AND last_name like '%"+filter.getlName()+"%' ";
			}
			if(filter.getCompanyName() != null){
				sqlQuery += " AND comp_name like '%"+filter.getCompanyName()+"%' ";	
			}
			if(filter.getEmail() != null){
				sqlQuery += " AND email like '% "+filter.getEmail()+"%' ";
			}
			if(filter.getPhone() != null){
				sqlQuery += " AND phone like '% "+filter.getPhone()+"%' ";
			}
			if(filter.getAddressLine1() != null){
				sqlQuery += " AND addr_line_one like '% "+filter.getAddressLine1()+"%' ";	
			}
			if(filter.getAddressLine2() != null){
				sqlQuery += " AND addr_line_two like '% "+filter.getAddressLine2()+"%' ";
			}
			if(filter.getCity() != null){
				sqlQuery += " AND city like '% "+filter.getCity()+"%' ";
			}
			if(filter.getState() != null){
				sqlQuery += " AND state like '% "+filter.getState()+"%' ";	
			}
			if(filter.getCountry() != null){
				sqlQuery += " AND country like '% "+filter.getCountry()+"%' ";	
			}
			if(filter.getZipCode() != null){
				sqlQuery += " AND pincode like '% "+filter.getZipCode()+"%' ";	
			}
			if(filter.getStatus() != null){
				sqlQuery += " AND sler_status like '% "+filter.getStatus()+"%' ";	
			}
			if(filter.getLastUpdatedBy() != null){
				sqlQuery += " AND lupd_by like '% "+filter.getLastUpdatedBy()+"%' ";	
			}
			if(filter.getLastUpdatedDateStr() != null){
				String dateTimeStr = filter.getLastUpdatedDateStr();
				if(dateTimeStr.contains(" ")){
					String dateTimeArr[] = dateTimeStr.split(" ");
					if(Util.extractDateElement(dateTimeArr[0],"DAY") > 0){
						sqlQuery += " AND DATEPART(day,lupd_date) = "+Util.extractDateElement(dateTimeArr[0],"DAY")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,lupd_date) = "+Util.extractDateElement(dateTimeArr[0],"MONTH")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,lupd_date) = "+Util.extractDateElement(dateTimeArr[0],"YEAR")+" ";
					}
					if(dateTimeArr.length>1){
						String dateTime = dateTimeArr[1];
						if(dateTimeArr.length > 2){
							if(dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()){
								dateTime += " "+dateTimeArr[2];
							}
						}
						if(Util.extractDateElement(dateTime,"HOUR") > 0){
							sqlQuery += " AND DATEPART(hour,lupd_date) = "+Util.extractDateElement(dateTime,"HOUR")+" ";
						}
						if(Util.extractDateElement(dateTime,"MINUTE") > 0){
							sqlQuery += " AND DATEPART(minute,lupd_date) = "+Util.extractDateElement(dateTime,"MINUTE")+" ";
						}
					}
				}else{
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
						sqlQuery += " AND DATEPART(day,lupd_date) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,lupd_date) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,lupd_date) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
					}
				}
			}
			
			Session session = getSession();
			SQLQuery query = session.createSQLQuery(sqlQuery);
			count = Integer.parseInt(String.valueOf(query.uniqueResult()));
		}catch(Exception e){
			e.printStackTrace();
		}
		return count;
	}
}