package com.rtw.tracker.dao.implementation;

import com.rtw.tmat.dao.implementation.ReportDAO;
import com.rtw.tracker.dao.services.ContestConfigSettingsDAO;
import com.rtw.tracker.dao.services.ContestsDAO;
import com.rtw.tracker.dao.services.FanClubPostCommentsDAO;
import com.rtw.tracker.dao.services.ContestEventsDAO;
import com.rtw.tracker.dao.services.ContestEventRequestDAO;
import com.rtw.tracker.dao.services.QuestionBankDAO;

public class QuizDAORegistry {

	private static ContestsDAO contestsDAO;
	private static ContestQuestionsDAO contestQuestionsDAO;
	private static ContestConfigSettingsDAO contestConfigSettingsDAO;
	private static ContestEventsDAO contestEventsDAO;
	private static ContestEventRequestDAO contestEventRequestDAO;
	private static QuestionBankDAO questionBankDAO;
	private static ContestPromocodeDAO contestPromocodeDAO;
	private static CustomerContestQuestionDAO customerContestQuestionDAO;
	private static PreContestChecklistDAO contestChecklistDAO;
	private static PreContestChecklistTrackingDAO preContestChecklistTrackingDAO;
	private static ContestGrandWinnersDAO contestGrandWinnersDAO;
	private static ReportDAO reportDAO;
	private static CustomerProfileQuestionDAO customerProfileQuestionDAO;
	private static RtfRewardConfigInfoDAO rtfRewardConfigInfoDAO;
	private static RtfPointsConversionSettingDAO rtfPointsConversionSettingDAO;
	private static CustomerCommentDAO customerCommentDAO;
	private static CustomerReportedMediaDAO customerReportedMediaDAO;
	private static SuperFanLevelsDAO superFanLevelsDAO;
	
	private static FanClubDAO fanClubDAO;
	private static FanClubMemberDAO fanClubMemberDAO;
	private static FanClubPostDAO fanClubPostDAO;
	private static FanClubPostCommentsDAO fanClubPostCommentsDAO;
	private static FanClubEventDAO fanClubEventDAO;
	private static FanClubVideoDAO fanClubVideoDAO;
	private static FanclubAbuseReportedDAO fanclubAbuseReportedDAO;
	
	public static final ContestsDAO getContestsDAO() {
		return contestsDAO;
	}
	public final void setContestsDAO(ContestsDAO contestsDAO) {
		QuizDAORegistry.contestsDAO = contestsDAO;
	}

	public static final ContestQuestionsDAO getContestQuestionsDAO() {
		return contestQuestionsDAO;
	}
	public final void setContestQuestionsDAO(
			ContestQuestionsDAO contestQuestionsDAO) {
		QuizDAORegistry.contestQuestionsDAO = contestQuestionsDAO;
	}
	public static final ContestConfigSettingsDAO getContestConfigSettingsDAO() {
		return contestConfigSettingsDAO;
	}
	public final void setContestConfigSettingsDAO(
			ContestConfigSettingsDAO contestConfigSettingsDAO) {
		QuizDAORegistry.contestConfigSettingsDAO = contestConfigSettingsDAO;
	}
	public static ContestEventsDAO getContestEventsDAO() {
		return contestEventsDAO;
	}
	public static void setContestEventsDAO(ContestEventsDAO contestEventsDAO) {
		QuizDAORegistry.contestEventsDAO = contestEventsDAO;
	}
	public static final ContestEventRequestDAO getContestEventRequestDAO() {
		return contestEventRequestDAO;
	}
	public final void setContestEventRequestDAO(
			ContestEventRequestDAO contestEventRequestDAO) {
		QuizDAORegistry.contestEventRequestDAO = contestEventRequestDAO;
	}
	public static final QuestionBankDAO getQuestionBankDAO() {
		return questionBankDAO;
	}
	public final void setQuestionBankDAO(QuestionBankDAO questionBankDAO) {
		QuizDAORegistry.questionBankDAO = questionBankDAO;
	}
	
	public static ContestPromocodeDAO getContestPromocodeDAO() {
		return contestPromocodeDAO;
	}
	public final void setContestPromocodeDAO(
			ContestPromocodeDAO contestPromocodeDAO) {
		QuizDAORegistry.contestPromocodeDAO = contestPromocodeDAO;
	}
	
	public static CustomerContestQuestionDAO getCustomerContestQuestionDAO() {
		return customerContestQuestionDAO;
	}
	public final void setCustomerContestQuestionDAO(
			CustomerContestQuestionDAO customerContestQuestionDAO) {
		QuizDAORegistry.customerContestQuestionDAO = customerContestQuestionDAO;
	}
	public static PreContestChecklistDAO getContestChecklistDAO() {
		return contestChecklistDAO;
	}
	public final void setContestChecklistDAO(PreContestChecklistDAO contestChecklistDAO) {
		QuizDAORegistry.contestChecklistDAO = contestChecklistDAO;
	}
	public static PreContestChecklistTrackingDAO getPreContestChecklistTrackingDAO() {
		return preContestChecklistTrackingDAO;
	}
	public final void setPreContestChecklistTrackingDAO(PreContestChecklistTrackingDAO preContestChecklistTrackingDAO) {
		QuizDAORegistry.preContestChecklistTrackingDAO = preContestChecklistTrackingDAO;
	}
	public static ContestGrandWinnersDAO getContestGrandWinnersDAO() {
		return contestGrandWinnersDAO;
	}
	public final void setContestGrandWinnersDAO(ContestGrandWinnersDAO contestGrandWinnersDAO) {
		QuizDAORegistry.contestGrandWinnersDAO = contestGrandWinnersDAO;
	}
	public static ReportDAO getReportDAO() {
		return reportDAO;
	}
	public final void setReportDAO(ReportDAO reportDAO) {
		QuizDAORegistry.reportDAO = reportDAO;
	}
	public static CustomerProfileQuestionDAO getCustomerProfileQuestionDAO() {
		return customerProfileQuestionDAO;
	}
	public final void setCustomerProfileQuestionDAO(CustomerProfileQuestionDAO customerProfileQuestionDAO) {
		QuizDAORegistry.customerProfileQuestionDAO = customerProfileQuestionDAO;
	}
	public static RtfRewardConfigInfoDAO getRtfRewardConfigInfoDAO() {
		return rtfRewardConfigInfoDAO;
	}
	public final void setRtfRewardConfigInfoDAO(RtfRewardConfigInfoDAO rtfRewardConfigInfoDAO) {
		QuizDAORegistry.rtfRewardConfigInfoDAO = rtfRewardConfigInfoDAO;
	}
	public static RtfPointsConversionSettingDAO getRtfPointsConversionSettingDAO() {
		return rtfPointsConversionSettingDAO;
	}
	public final void setRtfPointsConversionSettingDAO(RtfPointsConversionSettingDAO rtfPointsConversionSettingDAO) {
		QuizDAORegistry.rtfPointsConversionSettingDAO = rtfPointsConversionSettingDAO;
	}
	public static CustomerCommentDAO getCustomerCommentDAO() {
		return customerCommentDAO;
	}
	public final void setCustomerCommentDAO(CustomerCommentDAO customerCommentDAO) {
		QuizDAORegistry.customerCommentDAO = customerCommentDAO;
	}
	public static CustomerReportedMediaDAO getCustomerReportedMediaDAO() {
		return customerReportedMediaDAO;
	}
	public final void setCustomerReportedMediaDAO(CustomerReportedMediaDAO customerReportedMediaDAO) {
		QuizDAORegistry.customerReportedMediaDAO = customerReportedMediaDAO;
	}
	public static SuperFanLevelsDAO getSuperFanLevelsDAO() {
		return superFanLevelsDAO;
	}
	public final void setSuperFanLevelsDAO(SuperFanLevelsDAO superFanLevelsDAO) {
		QuizDAORegistry.superFanLevelsDAO = superFanLevelsDAO;
	}
	public static FanClubDAO getFanClubDAO() {
		return fanClubDAO;
	}
	public final void setFanClubDAO(FanClubDAO fanClubDAO) {
		QuizDAORegistry.fanClubDAO = fanClubDAO;
	}
	public static FanClubMemberDAO getFanClubMemberDAO() {
		return fanClubMemberDAO;
	}
	public final void setFanClubMemberDAO(FanClubMemberDAO fanClubMemberDAO) {
		QuizDAORegistry.fanClubMemberDAO = fanClubMemberDAO;
	}
	public static FanClubPostDAO getFanClubPostDAO() {
		return fanClubPostDAO;
	}
	public final void setFanClubPostDAO(FanClubPostDAO fanClubPostDAO) {
		QuizDAORegistry.fanClubPostDAO = fanClubPostDAO;
	}
	
	public static FanClubPostCommentsDAO getFanClubPostCommentsDAO() {
		return fanClubPostCommentsDAO;
	}
	public final void setFanClubPostCommentsDAO(FanClubPostCommentsDAO fanClubPostCommentsDAO) {
		QuizDAORegistry.fanClubPostCommentsDAO = fanClubPostCommentsDAO;
	}
	public static FanClubEventDAO getFanClubEventDAO() {
		return fanClubEventDAO;
	}
	public final void setFanClubEventDAO(FanClubEventDAO fanClubEventDAO) {
		QuizDAORegistry.fanClubEventDAO = fanClubEventDAO;
	}
	public static FanClubVideoDAO getFanClubVideoDAO() {
		return fanClubVideoDAO;
	}
	public final void setFanClubVideoDAO(FanClubVideoDAO fanClubVideoDAO) {
		QuizDAORegistry.fanClubVideoDAO = fanClubVideoDAO;
	}
	public static FanclubAbuseReportedDAO getFanclubAbuseReportedDAO() {
		return fanclubAbuseReportedDAO;
	}
	public final void setFanclubAbuseReportedDAO(FanclubAbuseReportedDAO fanclubAbuseReportedDAO) {
		QuizDAORegistry.fanclubAbuseReportedDAO = fanclubAbuseReportedDAO;
	}
	
	
	
	 
}
