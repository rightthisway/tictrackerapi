package com.rtw.tracker.dao.implementation;

import com.rtw.tracker.datas.ShippingMethod;

public class ShippingMethodDAO extends HibernateDAO<Integer, ShippingMethod> implements com.rtw.tracker.dao.services.ShippingMethodDAO{

	@Override
	public ShippingMethod getShippingMethodById(Integer id) {
		return findSingle("FROM ShippingMethod WHERE id=?", new Object[]{id});
	}

	@Override
	public ShippingMethod getShippingMethodByName(String name) {
		return findSingle("FROM ShippingMethod WHERE name=?", new Object[]{name});
	}

	

}
