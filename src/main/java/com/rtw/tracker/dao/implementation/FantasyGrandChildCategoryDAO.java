package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.FantasyGrandChildCategory;

public class FantasyGrandChildCategoryDAO extends HibernateDAO<Integer, FantasyGrandChildCategory> implements com.rtw.tracker.dao.services.FantasyGrandChildCategoryDAO{
	
	public List<FantasyGrandChildCategory> getAll(){
		return find("FROM FantasyGrandChildCategory order by name asc");
	}
	
	public List<FantasyGrandChildCategory> getAllExceptNameAsCollege(){
		return find("FROM FantasyGrandChildCategory where name not like '%COLLEGE%' order by name asc");
	}
	
	public FantasyGrandChildCategory getGrandChildCategoryByName(String name){
		return findSingle("FROM FantasyGrandChildCategory where name = ?",new Object []{name});	
	}
	
	public FantasyGrandChildCategory getGrandChildCategoryById(Integer id){
		return findSingle("FROM FantasyGrandChildCategory where id = ?",new Object []{id});	
	}
	
	public List<FantasyGrandChildCategory> getGrandChildsByChildCategoryId(Integer childCategoryId){
		return find("FROM FantasyGrandChildCategory  WHERE childId=? ORDER BY name", new Object[]{childCategoryId});
	}
}
