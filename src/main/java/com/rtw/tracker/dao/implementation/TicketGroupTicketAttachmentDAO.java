package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.TicketGroupTicketAttachment;
import com.rtw.tracker.enums.FileType;

public class TicketGroupTicketAttachmentDAO extends HibernateDAO<Integer, TicketGroupTicketAttachment> implements com.rtw.tracker.dao.services.TicketGroupTicketAttachmentDAO{

	public TicketGroupTicketAttachment getTicketByTicketGroupId(Integer ticketGroupId){
		return findSingle("FROM TicketGroupTicketAttachment where ticketGroupId=? order by type,position",new Object[]{ticketGroupId});
	}
	
	public List<TicketGroupTicketAttachment> getTicketAttachmentByTicketGroupId(Integer ticketGroupId) {
		return find("FROM TicketGroupTicketAttachment where ticketGroupId=? order by type,position",new Object[]{ticketGroupId});
	}
	
	public List<TicketGroupTicketAttachment> getAttachmentByTicketGroupAndFileType(Integer ticketGroupId,FileType fileType) {
		try {
			return find("FROM TicketGroupTicketAttachment where ticketGroupId=? AND type = ? order by position", new Object[]{ticketGroupId,fileType});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	public TicketGroupTicketAttachment getAttachmentByTicketGroupFileTypeAndPosition(Integer ticketGroupId,FileType fileType,Integer posotion) {
		try {
			return findSingle("FROM TicketGroupTicketAttachment where ticketGroupId=? AND type = ? and position=?", new Object[]{ticketGroupId,fileType,posotion});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}