package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.datas.CustomerProfileQuestion;
import com.rtw.tracker.datas.RtfRewardConfigInfo;
import com.rtw.tracker.utils.GridHeaderFilters;

public class RtfRewardConfigInfoDAO extends HibernateDAO<Integer, RtfRewardConfigInfo> implements com.rtw.tracker.dao.services.RtfRewardConfigInfoDAO{

	
	private static Session rewardConfigSession = null;;
	public static Session getRewardConfigSession() {
		return rewardConfigSession;
	}

	public static void setRewardConfigSession(Session rewardConfigSession) {
		RtfRewardConfigInfoDAO.rewardConfigSession = rewardConfigSession;
	}



	@Override
	public List<RtfRewardConfigInfo> getAllRtfRewardConfigByStatus(GridHeaderFilters filter, String status) {
		List<RtfRewardConfigInfo> list =null;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("select id as id, reward_title as mediaDescription, no_of_lives as lives, no_of_magic_wand as magicWands, no_of_sf_stars as sfStars,");
			sql.append("rtf_points as rtfPoints, reward_dollars as rwdDollars, status as status, active_date as activeDate, action_type as actionType,");
			sql.append("created_date as createdDate, updated_date as updatedDate, created_by as createdBy, update_by as updatedBy,");
			sql.append("max_rewards_per_day as maxRewardsPerDay, batch_size_per_day as batchSize, min_vid_play_time as minVideoPlayTime, min_reward_interval as videoRewardInterval ");
			sql.append("from rtf_reward_config where is_display=1 ");
			
			if(status!=null && !status.isEmpty()){
				if(status.equalsIgnoreCase("ACTIVE")){
					sql.append(" AND status=1");
				}else if(status.equalsIgnoreCase("INACTIVE")){
					sql.append(" AND status=0");
				}
			}
			if(filter.getContestId() !=null){
				sql.append(" AND max_rewards_per_day ="+filter.getContestId());
			}
			if(filter.getBrokerId() !=null){
				sql.append(" AND batch_size_per_day ="+filter.getBrokerId());
			}
			if(filter.getId() !=null){
				sql.append(" AND min_vid_play_time ="+filter.getId());
			}
			if(filter.getEventId() !=null){
				sql.append(" AND min_reward_interval ="+filter.getEventId());
			}
			if(filter.getEventDescription()!=null && !filter.getEventDescription().isEmpty()){
				sql.append(" AND reward_title like  '%"+filter.getEventDescription()+"%' ");
			}
			if(filter.getCount()!= null){
				sql.append(" AND no_of_lives ="+filter.getCount());
			}
			if(filter.getEventCount()!= null){
				sql.append(" AND no_of_magic_wand ="+filter.getEventCount());
			}
			if(filter.getNoOfTixCount()!= null){
				sql.append(" AND no_of_sf_stars ="+filter.getNoOfTixCount());
			}
			if(filter.getPointWinnerCount()!= null){
				sql.append(" AND rtf_points ="+filter.getPointWinnerCount());
			}
			if(filter.getRewardPoints()!= null){
				sql.append(" AND reward_dollars ="+filter.getRewardPoints());
			}
			if(filter.getText1()!=null && !filter.getText1().isEmpty()){
				sql.append(" AND action_type like  '%"+filter.getText1()+"%' ");
			}
			if(filter.getStatus()!=null && !filter.getStatus().isEmpty()){
				if(filter.getStatus().equalsIgnoreCase("ACTIVE")){
					sql.append(" AND status=1");
				}else if(filter.getStatus().equalsIgnoreCase("INACTIVE")){
					sql.append(" AND status=0");
				}
			}
			if(filter.getCreatedBy()!=null && !filter.getCreatedBy().isEmpty()){
				sql.append(" AND created_by like  '%"+filter.getCreatedBy()+"%' ");
			}
			if(filter.getUpdatedBy()!=null && !filter.getUpdatedBy().isEmpty()){
				sql.append(" AND update_by like  '%"+filter.getUpdatedBy()+"%' ");
			}
			if(filter.getStartDate()!=null){
				if(Util.extractDateElement(filter.getStartDate(),"DAY") > 0){
					sql.append(" AND DATEPART(day, active_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ");
				}
				if(Util.extractDateElement(filter.getStartDate(),"MONTH") > 0){
					sql.append(" AND DATEPART(month, active_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ");
				}
				if(Util.extractDateElement(filter.getStartDate(),"YEAR") > 0){
					sql.append(" AND DATEPART(year, active_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ");
				}
			}
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql.append(" AND DATEPART(day, created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql.append(" AND DATEPART(month, created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql.append(" AND DATEPART(year, created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ");
				}
			}
			if(filter.getUpdatedDate()!=null ){
				if(Util.extractDateElement(filter.getUpdatedDate(),"DAY") > 0){
					sql.append(" AND DATEPART(day,updated_date) = "+Util.extractDateElement(filter.getUpdatedDate(),"DAY")+" ");
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"MONTH") > 0){
					sql.append(" AND DATEPART(month, updated_date) = "+Util.extractDateElement(filter.getUpdatedDate(),"MONTH")+" ");
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"YEAR") > 0){
					sql.append(" AND DATEPART(year, updated_date) = "+Util.extractDateElement(filter.getUpdatedDate(),"YEAR")+" ");
				}
			}
			
			rewardConfigSession = RtfRewardConfigInfoDAO.getRewardConfigSession();
			if(rewardConfigSession==null || !rewardConfigSession.isOpen() || !rewardConfigSession.isConnected()){
				rewardConfigSession = getSession();
				RtfRewardConfigInfoDAO.setRewardConfigSession(rewardConfigSession);
			}
			SQLQuery query = rewardConfigSession.createSQLQuery(sql.toString());
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("mediaDescription", Hibernate.STRING);
			query.addScalar("lives", Hibernate.INTEGER);
			query.addScalar("magicWands", Hibernate.INTEGER);
			query.addScalar("sfStars", Hibernate.INTEGER);
			query.addScalar("rtfPoints", Hibernate.INTEGER);
			query.addScalar("rwdDollars", Hibernate.DOUBLE);
			query.addScalar("status", Hibernate.BOOLEAN);
			query.addScalar("activeDate", Hibernate.TIMESTAMP);
			query.addScalar("actionType", Hibernate.STRING);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("updatedBy", Hibernate.STRING);
			query.addScalar("maxRewardsPerDay", Hibernate.INTEGER);
			query.addScalar("batchSize", Hibernate.INTEGER);
			query.addScalar("minVideoPlayTime", Hibernate.INTEGER);
			query.addScalar("videoRewardInterval", Hibernate.INTEGER);
			
			list = query.setResultTransformer(Transformers.aliasToBean(RtfRewardConfigInfo.class)).list();
		} catch (Exception e) {
			if(rewardConfigSession != null && rewardConfigSession.isOpen()){
				rewardConfigSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<RtfRewardConfigInfo> getRtfRewardConfigByRewardType(String rewardType) {
		return find("FROM RtfRewardConfigInfo Where status=? AND actionType=?",new Object[]{true,rewardType});
	}
	

}
