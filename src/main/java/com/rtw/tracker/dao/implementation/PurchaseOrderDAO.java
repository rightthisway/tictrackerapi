package com.rtw.tracker.dao.implementation;

import java.util.ArrayList;
import java.util.Collection;

import org.hibernate.Query;
import org.hibernate.Session;

import com.rtw.tracker.datas.PurchaseOrder;

public class PurchaseOrderDAO extends HibernateDAO<Integer, PurchaseOrder> implements com.rtw.tracker.dao.services.PurchaseOrderDAO{

	@SuppressWarnings("unchecked")
	@Override
	public Collection<PurchaseOrder> fetchPO() {
		Collection<PurchaseOrder> poList = new ArrayList<PurchaseOrder>();
		try {
			Session session = getSession();
			Query query = session.createQuery("From PurchaseOrder");
			poList = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return poList;
	}

	public PurchaseOrder getPurchaseOrderByIdAndBrokerId(Integer id, Integer brokerId){
		return findSingle("FROM PurchaseOrder WHERE id = ? AND brokerId = ?", new Object[]{id, brokerId});
	}
}
