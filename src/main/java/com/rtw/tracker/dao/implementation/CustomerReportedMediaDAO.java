package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.datas.CustomerReportedMedia;
import com.rtw.tracker.utils.GridHeaderFilters;

public class CustomerReportedMediaDAO extends HibernateDAO<Integer, CustomerReportedMedia> implements com.rtw.tracker.dao.services.CustomerReportedMediaDAO{

	private static Session reportedMediaSession;
	
	public static Session getReportedMediaSession() {
		return reportedMediaSession;
	}

	public static void setReportedMediaSession(Session reportedMediaSession) {
		CustomerReportedMediaDAO.reportedMediaSession = reportedMediaSession;
	}



	@Override
	public List<CustomerReportedMedia> getAllReportedComment(GridHeaderFilters filter,String status,SharedProperty prop) {
		List<CustomerReportedMedia> list =null;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("select a.id as id,a.customer_id as customerId,a.media_id as mediaId,a.abuse_id as abuseId,a.created_date as createdDate,a.comment_id as commentId,");
			sql.append("c.user_id as reportedByUserId,c.email as reportedByEmail,c.phone as reportedByPhone,ao.abuse_text as abuseType,");
			sql.append("c1.user_id as userId,c1.email as email,c1.phone as phone,");
			sql.append("co.cust_comment_text as title,co.cust_comment_media_url as mediaUrl,co.cust_comment_image_url as imageUrl,");
			sql.append("co.block_status as blockStatus,co.block_date as blockDate,co.block_by as blockBy,co.hide_comment as hideStatus,co.hide_by as hideBy,co.hide_date as hideDate ");
			sql.append("from customer_reported_abuse_on_media a with(NOLOCK) left join "+prop.getDatabasAlias()+"customer c with(NOLOCK) on a.customer_id=c.id ");
			sql.append("join mst_static_abuse_options ao with(NOLOCK) on ao.id=a.abuse_id ");
			sql.append("left join customer_comments_on_media co with(NOLOCK) on a.comment_id=co.id ");
			sql.append("left join "+prop.getDatabasAlias()+"customer c1 with(NOLOCK) on co.customer_id=c1.id ");
			sql.append("where a.comment_id  is not null and a.comment_id > 0 ");
			
			if(status!=null && !status.isEmpty()){
				if(status.equalsIgnoreCase("ACTIVE")){
					sql.append(" AND (co.block_status is null or co.block_status =0) AND (co.hide_comment is null or co.hide_comment =0) ");
				}else if(status.equalsIgnoreCase("BLOCKED")){
					sql.append(" AND (co.block_status =1 OR co.hide_comment =1) ");
				}
			}
			
			if(filter.getText1()!=null && !filter.getText1().isEmpty()){
				sql.append(" AND c.user_id like  '%"+filter.getText1()+"%' ");
			}
			if(filter.getText2()!=null && !filter.getText2().isEmpty()){
				sql.append(" AND c.email like  '%"+filter.getText2()+"%' ");
			}
			if(filter.getText3()!=null && !filter.getText3().isEmpty()){
				sql.append(" AND c.phone like  '%"+filter.getText3()+"%' ");
			}
			
			if(filter.getActionType()!=null && !filter.getActionType().isEmpty()){
				sql.append(" AND ao.abuse_text like  '%"+filter.getActionType()+"%' ");
			}
			
			if(filter.getUsername()!=null && !filter.getUsername().isEmpty()){
				sql.append(" AND c1.user_id like  '%"+filter.getUsername()+"%' ");
			}
			if(filter.getEmail()!=null && !filter.getEmail().isEmpty()){
				sql.append(" AND c1.email like  '%"+filter.getEmail()+"%' ");
			}
			if(filter.getPhone()!=null && !filter.getPhone().isEmpty()){
				sql.append(" AND c1.phone like  '%"+filter.getPhone()+"%' ");
			}
			
			if(filter.getText4()!=null && !filter.getText4().isEmpty()){
				sql.append(" AND co.cust_comment_text like  '%"+filter.getText4()+"%' ");
			}
				
			if(filter.getUpdatedBy()!=null && !filter.getUpdatedBy().isEmpty()){
				sql.append(" AND co.block_by like  '%"+filter.getUpdatedBy()+"%' ");
			}
			if(filter.getCreatedBy()!=null && !filter.getCreatedBy().isEmpty()){
				sql.append(" AND co.hide_by like  '%"+filter.getCreatedBy()+"%' ");
			}
			
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql.append(" AND DATEPART(day, a.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql.append(" AND DATEPART(month, a.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql.append(" AND DATEPART(year, a.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ");
				}
			}
			
			if(filter.getLastUpdatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					sql.append(" AND DATEPART(day, co.block_date) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql.append(" AND DATEPART(month, co.block_date) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql.append(" AND DATEPART(year, co.block_date) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ");
				}
			}
			
			if(filter.getStartDate()!=null ){
				if(Util.extractDateElement(filter.getStartDate(),"DAY") > 0){
					sql.append(" AND DATEPART(day, co.hide_date) = "+Util.extractDateElement(filter.getStartDate(),"DAY")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql.append(" AND DATEPART(month, co.hide_date) = "+Util.extractDateElement(filter.getStartDate(),"MONTH")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql.append(" AND DATEPART(year, co.hide_date) = "+Util.extractDateElement(filter.getStartDate(),"YEAR")+" ");
				}
			}
			
			reportedMediaSession = CustomerReportedMediaDAO.getReportedMediaSession();
			if(reportedMediaSession==null || !reportedMediaSession.isOpen() || !reportedMediaSession.isConnected()){
				reportedMediaSession = getSession();
				CustomerReportedMediaDAO.setReportedMediaSession(reportedMediaSession);
			}
			SQLQuery query = reportedMediaSession.createSQLQuery(sql.toString());
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("customerId", Hibernate.INTEGER);
			query.addScalar("mediaId", Hibernate.INTEGER);
			query.addScalar("abuseId", Hibernate.INTEGER);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("commentId", Hibernate.INTEGER);
			query.addScalar("reportedByUserId", Hibernate.STRING);
			query.addScalar("reportedByEmail", Hibernate.STRING);
			query.addScalar("reportedByPhone", Hibernate.STRING);
			query.addScalar("abuseType", Hibernate.STRING);
			query.addScalar("userId", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("phone", Hibernate.STRING);
			query.addScalar("title", Hibernate.STRING);
			query.addScalar("mediaUrl", Hibernate.STRING);
			query.addScalar("imageUrl", Hibernate.STRING);
			query.addScalar("blockStatus", Hibernate.BOOLEAN);
			query.addScalar("blockDate", Hibernate.TIMESTAMP);
			query.addScalar("blockBy", Hibernate.STRING);
			query.addScalar("hideStatus", Hibernate.BOOLEAN);
			query.addScalar("hideBy", Hibernate.STRING);
			query.addScalar("hideDate", Hibernate.TIMESTAMP);
			list = query.setResultTransformer(Transformers.aliasToBean(CustomerReportedMedia.class)).list();
		} catch (Exception e) {
			if(reportedMediaSession != null && reportedMediaSession.isOpen()){
				reportedMediaSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<CustomerReportedMedia> getAllReportedMedia(GridHeaderFilters filter,String status, SharedProperty prop) {
		List<CustomerReportedMedia> list =null;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("select a.id as id,a.customer_id as customerId,a.media_id as mediaId,a.abuse_id as abuseId,a.created_date as createdDate,");
			sql.append("c.user_id as reportedByUserId,c.email as reportedByEmail,c.phone as reportedByPhone,ao.abuse_text as abuseType,");
			sql.append("c1.user_id as userId,c1.email as email,c1.phone as phone,");
			sql.append("pi.block_status as blockStatus,pi.block_date as blockDate,pi.block_by as blockBy,pi.hide_media as hideStatus,pi.hide_by as hideBy,pi.hide_date as hideDate,");
			sql.append("pi.video_url as mediaUrl,pi.title as title,pi.video_description as description ");
			sql.append("from customer_reported_abuse_on_media a with(NOLOCK) ");
			sql.append("left join "+prop.getDatabasAlias()+"customer c with(NOLOCK) on a.customer_id=c.id ");
			sql.append("join mst_static_abuse_options ao with(NOLOCK) on ao.id=a.abuse_id ");
			sql.append("left join "+prop.getDatabasAlias()+"polling_video_inventory pi with(NOLOCK) on a.media_id = pi.id ");
			sql.append("left join "+prop.getDatabasAlias()+"polling_customer_video_uploads pci with(NOLOCK) on pi.cust_video_id=pci.id ");
			sql.append("left join "+prop.getDatabasAlias()+"customer c1 with(NOLOCK) on pci.cust_id=c1.id ");
			sql.append("where a.media_id  is not null and a.media_id > 0 and (a.comment_id  is  null or a.comment_id <= 0) ");
			
			if(status!=null && !status.isEmpty()){
				if(status.equalsIgnoreCase("ACTIVE")){
					sql.append(" AND (pi.block_status is null or pi.block_status =0) ");
				}else if(status.equalsIgnoreCase("BLOCKED")){
					sql.append(" AND pi.block_status =1 ");
				}
			}
			
			if(filter.getText1()!=null && !filter.getText1().isEmpty()){
				sql.append(" AND c.user_id like  '%"+filter.getText1()+"%' ");
			}
			if(filter.getText2()!=null && !filter.getText2().isEmpty()){
				sql.append(" AND c.email like  '%"+filter.getText2()+"%' ");
			}
			if(filter.getText3()!=null && !filter.getText3().isEmpty()){
				sql.append(" AND c.phone like  '%"+filter.getText3()+"%' ");
			}
			
			if(filter.getActionType()!=null && !filter.getActionType().isEmpty()){
				sql.append(" AND ao.abuse_text like  '%"+filter.getActionType()+"%' ");
			}
			
			if(filter.getUsername()!=null && !filter.getUsername().isEmpty()){
				sql.append(" AND c1.user_id like  '%"+filter.getUsername()+"%' ");
			}
			if(filter.getEmail()!=null && !filter.getEmail().isEmpty()){
				sql.append(" AND c1.email like  '%"+filter.getEmail()+"%' ");
			}
			if(filter.getPhone()!=null && !filter.getPhone().isEmpty()){
				sql.append(" AND c1.phone like  '%"+filter.getPhone()+"%' ");
			}
			
			if(filter.getText4()!=null && !filter.getText4().isEmpty()){
				sql.append(" AND pi.title like  '%"+filter.getText4()+"%' ");
			}
			if(filter.getEventDescription()!=null && !filter.getEventDescription().isEmpty()){
				sql.append(" AND pi.description like  '%"+filter.getEventDescription()+"%' ");
			}
			
			if(filter.getUpdatedBy()!=null && !filter.getUpdatedBy().isEmpty()){
				sql.append(" AND pi.block_by like  '%"+filter.getUpdatedBy()+"%' ");
			}
			if(filter.getCreatedBy()!=null && !filter.getCreatedBy().isEmpty()){
				sql.append(" AND pi.hide_by like  '%"+filter.getCreatedBy()+"%' ");
			}
			
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql.append(" AND DATEPART(day, a.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql.append(" AND DATEPART(month, a.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql.append(" AND DATEPART(year, a.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ");
				}
			}
			
			if(filter.getLastUpdatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					sql.append(" AND DATEPART(day, pci.block_date) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql.append(" AND DATEPART(month, pci.block_date) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql.append(" AND DATEPART(year, pci.block_date) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ");
				}
			}
			
			reportedMediaSession = CustomerReportedMediaDAO.getReportedMediaSession();
			if(reportedMediaSession==null || !reportedMediaSession.isOpen() || !reportedMediaSession.isConnected()){
				reportedMediaSession = getSession();
				CustomerReportedMediaDAO.setReportedMediaSession(reportedMediaSession);
			}
			SQLQuery query = reportedMediaSession.createSQLQuery(sql.toString());
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("customerId", Hibernate.INTEGER);
			query.addScalar("mediaId", Hibernate.INTEGER);
			query.addScalar("abuseId", Hibernate.INTEGER);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("reportedByUserId", Hibernate.STRING);
			query.addScalar("reportedByEmail", Hibernate.STRING);
			query.addScalar("reportedByPhone", Hibernate.STRING);
			query.addScalar("abuseType", Hibernate.STRING);
			query.addScalar("userId", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("phone", Hibernate.STRING);
			query.addScalar("title", Hibernate.STRING);
			query.addScalar("mediaUrl", Hibernate.STRING);
			query.addScalar("description", Hibernate.STRING);
			query.addScalar("blockStatus", Hibernate.BOOLEAN);
			query.addScalar("blockDate", Hibernate.TIMESTAMP);
			query.addScalar("blockBy", Hibernate.STRING);
			query.addScalar("hideStatus", Hibernate.BOOLEAN);
			query.addScalar("hideBy", Hibernate.STRING);
			query.addScalar("hideDate", Hibernate.TIMESTAMP);
			list = query.setResultTransformer(Transformers.aliasToBean(CustomerReportedMedia.class)).list();
		} catch (Exception e) {
			if(reportedMediaSession != null && reportedMediaSession.isOpen()){
				reportedMediaSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}

}
