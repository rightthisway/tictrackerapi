package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.datas.GiftCardBrand;
import com.rtw.tracker.utils.GridHeaderFilters;

public class GiftCardBrandDAO extends HibernateDAO<Integer, GiftCardBrand> implements com.rtw.tracker.dao.services.GiftCardBrandDAO{

	private static Session genericSession;
	
	public static Session getGenericSession() {
		return genericSession;
	}

	public static void setGenericSession(Session genericSession) {
		GiftCardBrandDAO.genericSession = genericSession;
	}
	
	
	public List<GiftCardBrand> getAllGiftCardBrands(GridHeaderFilters filter,String gbStatus) {
		List<GiftCardBrand> brands = null;
		try {
			String sql = "select id as id,name as name,description as description,status as status,"
					+ "image_url as fileName,created_date as createdDate,updated_date as updatedDate,created_by as createdBy,"
					+ "updated_by as updatedBy,out_of_sotck_image_url as outOfStockFileName,diplay_out_of_stock as displayOutOfStock  from gift_card_brand "
					+ "WHERE status like '"+gbStatus+"' " ;
			
			if(filter.getContestName()!= null && !filter.getContestName().isEmpty()){
				sql += " AND name like '%"+filter.getContestName()+"%' ";
			}
			if(filter.getEventDescription() != null && !filter.getEventDescription().isEmpty()){
				sql += " AND description like '%"+filter.getEventDescription()+"%' ";
			}
			/*if(filter.getQuantity()!= null){
				sql += " AND qty_limit ="+filter.getQuantity();
			}*/
			if(filter.getCreatedBy()!= null && !filter.getCreatedBy().isEmpty()){
				sql += " AND created_by like '%"+filter.getCreatedBy()+"%' ";
			}
			if(filter.getUpdatedBy()!= null && !filter.getUpdatedBy().isEmpty()){
				sql += " AND updated_by like '%"+filter.getUpdatedBy()+"%' ";
			}
			if(filter.getText1()!= null && !filter.getUpdatedBy().isEmpty()){
				if(filter.getText1().equalsIgnoreCase("YES")){
					sql += " AND displayOutOfStock=1 ";
				}else{
					sql += " AND displayOutOfStock=1 ";
				}
			}
			
			
			
			if (filter.getCreatedDateStr() != null && !filter.getCreatedDateStr().isEmpty()) {
				if (Util.extractDateElement(filter.getCreatedDateStr(), "DAY") > 0) {
					sql += " AND DATEPART(day, created_date) = "
							+ Util.extractDateElement(filter.getCreatedDateStr(), "DAY") + " ";
				}
				if (Util.extractDateElement(filter.getCreatedDateStr(), "MONTH") > 0) {
					sql += " AND DATEPART(month, created_date) = "
							+ Util.extractDateElement(filter.getCreatedDateStr(), "MONTH") + " ";
				}
				if (Util.extractDateElement(filter.getCreatedDateStr(), "YEAR") > 0) {
					sql += " AND DATEPART(year, created_date) = "
							+ Util.extractDateElement(filter.getCreatedDateStr(), "YEAR") + " ";
				}
			}
			if (filter.getUpdatedDate()!= null && !filter.getUpdatedDate().isEmpty()) {
				if (Util.extractDateElement(filter.getUpdatedDate(), "DAY") > 0) {
					sql += " AND DATEPART(day, updated_date) = "
							+ Util.extractDateElement(filter.getUpdatedDate(), "DAY") + " ";
				}
				if (Util.extractDateElement(filter.getUpdatedDate(), "MONTH") > 0) {
					sql += " AND DATEPART(month, updated_date) = "
							+ Util.extractDateElement(filter.getUpdatedDate(), "MONTH") + " ";
				}
				if (Util.extractDateElement(filter.getUpdatedDate(), "YEAR") > 0) {
					sql += " AND DATEPART(year, updated_date) = "
							+ Util.extractDateElement(filter.getUpdatedDate(), "YEAR") + " ";
				}
			}
			
			genericSession = GiftCardBrandDAO.getGenericSession();
			if(genericSession==null || !genericSession.isOpen() || !genericSession.isConnected()){
				genericSession = getSession();
				GiftCardBrandDAO.setGenericSession(genericSession);
			}
			SQLQuery query = genericSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("name", Hibernate.STRING);
			query.addScalar("description", Hibernate.STRING);
			//query.addScalar("quantity", Hibernate.INTEGER);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("updatedBy", Hibernate.STRING);
			query.addScalar("fileName", Hibernate.STRING);
			query.addScalar("outOfStockFileName", Hibernate.STRING);
			query.addScalar("displayOutOfStock", Hibernate.BOOLEAN);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);
			
			brands = query.setResultTransformer(Transformers.aliasToBean(GiftCardBrand.class)).list();
			
		} catch (Exception e) {
			if(genericSession != null && genericSession.isOpen()){
				genericSession.close();
			}
			e.printStackTrace(); 
		}
		return brands;
	}
}
