package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.datas.PreContestChecklist;
import com.rtw.tracker.utils.GridHeaderFilters;

public class PreContestChecklistDAO extends HibernateDAO<Integer, PreContestChecklist> implements com.rtw.tracker.dao.services.PreContestChecklistDAO{

	
	private static Session contestSession = null;
	public static Session getContestSession() {
		return PreContestChecklistDAO.contestSession;
	}

	public static void setContestSession(Session contestSession) {
		PreContestChecklistDAO.contestSession = contestSession;
	}
	
	
	@Override
	public PreContestChecklist getPreContestChecklistByContestId(Integer contestId) {
	  return findSingle("FROM PreContestChecklist WHERE contestId=? ",new Object[]{contestId});
	}
	
	@Override
	public List<PreContestChecklist> getAllActivePreContestChecklist(Integer contestId) {
		 return find("FROM PreContestChecklist WHERE (contestId is null or contestId=?) and status=?",new Object[]{contestId,"ACTIVE"});
	}
	
	@Override
	public List<PreContestChecklist> getAllActivePreContestChecklistByType(Integer contestId,String type) {
		 return find("FROM PreContestChecklist WHERE (contestId is null or contestId=?) and status=? AND type=?",new Object[]{contestId,"ACTIVE",type});
	}

	@Override
	public List<PreContestChecklist> getPreContestChecklistbyFilters(String status, GridHeaderFilters filter,SharedProperty property,String type) {
		List<PreContestChecklist> performers = null;
		try {
			String sql = "select pcc.id as id,pcc.contest_id as contestId,pcc.contest_date as contestDateTime,pcc.reward_dollars_prize as "+ 
					"rewardDollarsPrize,pcc.grand_prize_tickets as grandWinnerTicketPrize,pcc.contest_name as contestName,pcc.extended_name as extendedName, "+
					"pcc.grand_prize_winners as grandPrizeWinner,pcc.category_name as categoryName,pcc.category_type as categoryType,pcc.category_id as "+ 
					"categoryId,pcc.price_per_ticket as pricePerTicket,pcc.promo_ref_name as promoCategoryName,pcc.promo_ref_type as promoCategoryType,pcc.promo_ref_id as "+ 
					"promoCategoryId,pcc.grand_prize_event as grandPrizeEvent,pcc.zone as zone,pcc.question_size as questionSize,"+
					"pcc.discount_code as discountCode,pcc.discount_percentage as discountPercentage,pcc.host_name as hostName,pcc.producer_name as "+
					"producerName,pcc.director_name as directorName,pcc.contest_category as contestCategory,pcc.participant_star as participantStars,pcc.referral_star as referralStars,"+
					"pcc.technical_director_name as technicalDirectorName,pcc.backend_operator_name as backendOperatorName,pcc.is_question_entered as "+ 
					"isQuestionEntered,pcc.is_question_verified as isQuestionVerified, "+
					"pcc.question_entered_by as questionEnteredBy,pcc.question_verified_by as questionVerifiedBy,pcc.reward_dollars_per_question as "+ 
					"rewardDollarsPerQuestion, "+
					"pcc.approved_by as approvedBy,pcc.updated_date as updatedDate,pcc.status as status,pcc.created_by as createdBy,pcc.created_date as "+ 
					"createdDate,pcc.updated_by as updatedBy, "+
					"tu.username as questionEnteredByName,tu1.username as questionVerifiedByName,tu2.username as approvedByName "+
					"from pre_contest_checklist pcc left join "+property.getDatabasAlias()+"tracker_users tu on tu.id=pcc.question_entered_by "+
					"left join "+property.getDatabasAlias()+"tracker_users tu1 on tu1.id=pcc.question_verified_by "+
					"left join "+property.getDatabasAlias()+"tracker_users tu2 on tu2.id=pcc.approved_by WHERE pcc.type='"+type+"' ";
			
			if(status!=null && !status.isEmpty()){
				sql += " AND pcc.status like '"+status+"' ";
			}
			if(filter.getContestId()!=null){
				sql += " AND pcc.contest_id ="+filter.getContestId();
			}
			if(filter.getContestName()!=null){
				sql += " AND pcc.contest_name like '%"+filter.getContestName()+"%'";
			}
			if(filter.getExternalNotes()!=null){
				sql += " AND pcc.extended_name like '%"+filter.getExternalNotes()+"%'";
			}
			if(filter.getEventName()!=null){
				sql += " AND pcc.grand_prize_event like '%"+filter.getEventName()+"%'";
			}
			if(filter.getZone()!=null){
				sql += " AND pcc.zone like '%"+filter.getZone()+"%'";
			}
			if(filter.getCount()!=null){
				sql += " AND pcc.question_size ="+filter.getCount();
			}
			if(filter.getParentCategoryName()!=null){
				sql += " AND pcc.contest_category like '%"+filter.getParentCategoryName()+"%'";
			}
			if(filter.getPartipantCount()!=null){
				sql += " AND pcc.participant_star ="+filter.getPartipantCount();
			}
			if(filter.getWinnerCount()!=null){
				sql += " AND pcc.referral_star ="+filter.getWinnerCount();
			}
			if(filter.getStartDate()!=null ){
				if(Util.extractDateElement(filter.getStartDate(),"DAY") > 0){
					sql += " AND DATEPART(day,pcc.contest_date) = "+Util.extractDateElement(filter.getStartDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getStartDate(),"MONTH") > 0){
					sql += " AND DATEPART(month,pcc.contest_date) = "+Util.extractDateElement(filter.getStartDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getStartDate(),"YEAR") > 0){
					sql += " AND DATEPART(year,pcc.contest_date) = "+Util.extractDateElement(filter.getStartDate(),"YEAR")+" ";
				}
			}
			if(filter.getRewardPoints()!=null){
				sql += " AND pcc.reward_dollars_prize ="+filter.getRewardPoints();
			}
			if(filter.getFreeTicketPerWinner()!=null){
				sql += " AND pcc.grand_prize_tickets ="+filter.getFreeTicketPerWinner();
			}
			if(filter.getWinnerCount()!=null){
				sql += " AND pcc.grand_prize_winners ="+filter.getWinnerCount();
			}
			if(filter.getText1()!=null){
				sql += " AND pcc.category_type like '"+filter.getText1()+"'";
			}
			if(filter.getCategory()!=null){
				sql += " AND pcc.category_name like '"+filter.getCategory()+"'";
			}
			if(filter.getPromoType()!=null){
				sql += " AND pcc.promo_ref_type like '"+filter.getPromoType()+"'";
			}
			if(filter.getPromoReferenceName()!=null){
				sql += " AND pcc.promo_ref_name like '"+filter.getPromoReferenceName()+"'";
			}
			if(filter.getPrice()!=null){
				sql += " AND pcc.price_per_ticket ="+filter.getPrice();
			}
			if(filter.getPromoCode()!=null){
				sql += " AND pcc.discount_code like '"+filter.getPromoCode()+"'";
			}
			if(filter.getDiscountCouponPrice()!=null){
				sql += " AND pcc.discount_percentage ="+filter.getDiscountCouponPrice();
			}
			if(filter.getText2()!=null){
				sql += " AND pcc.host_name like '"+filter.getText2()+"'";
			}
			if(filter.getText3()!=null){
				sql += " AND pcc.producer_name like '"+filter.getText3()+"'";
			}
			if(filter.getText4()!=null){
				sql += " AND pcc.director_name like '"+filter.getText4()+"'";
			}
			if(filter.getText5()!=null){
				sql += " AND pcc.technical_director_name like '"+filter.getText5()+"'";
			}
			if(filter.getText6()!=null){
				sql += " AND pcc.backend_operator_name like '"+filter.getText6()+"'";
			}
			if(filter.getText7()!=null){
				if(filter.getText7().equalsIgnoreCase("Yes")){
					sql += " AND pcc.is_question_entered =1";
				}else if(filter.getText7().equalsIgnoreCase("No")){
					sql += " AND pcc.is_question_entered =0";
				}
			}
			if(filter.getText8()!=null){
				if(filter.getText8().equalsIgnoreCase("Yes")){
					sql += " AND pcc.is_question_verified =1";
				}else if(filter.getText8().equalsIgnoreCase("No")){
					sql += " AND pcc.is_question_verified =0";
				}
			}
			if(filter.getUsername()!=null){
				sql += " AND tu1.username like '"+filter.getUsername()+"'";
			}
			if(filter.getPointsPerWinner()!=null){
				sql += " AND pcc.reward_dollars_per_question ="+filter.getPointsPerWinner();
			}
			if(filter.getLastUpdatedBy()!=null){
				sql += " AND tu2.username like '"+filter.getLastUpdatedBy()+"'";
			}
			if(filter.getUpdatedBy()!=null){
				sql += " AND pcc.updated_by like '"+filter.getUpdatedBy()+"'";
			}
			if(filter.getText9()!=null){
				sql += " AND tu.username like '"+filter.getText9()+"'";
			}
			if(filter.getCreatedBy()!=null){
				sql += " AND pcc.created_by like '"+filter.getCreatedBy()+"'";
			}
			if(filter.getStatus()!=null){
				sql += " AND pcc.status like '"+filter.getStatus()+"'";
			}
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,pcc.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,pcc.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,pcc.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getUpdatedDate()!=null ){
				if(Util.extractDateElement(filter.getUpdatedDate(),"DAY") > 0){
					sql += " AND DATEPART(day,pcc.updated_date) = "+Util.extractDateElement(filter.getUpdatedDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"MONTH") > 0){
					sql += " AND DATEPART(month,pcc.updated_date) = "+Util.extractDateElement(filter.getUpdatedDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"YEAR") > 0){
					sql += " AND DATEPART(year,pcc.updated_date) = "+Util.extractDateElement(filter.getUpdatedDate(),"YEAR")+" ";
				}
			}
			
			contestSession = PreContestChecklistDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				PreContestChecklistDAO.setContestSession(contestSession);
			}
			
			SQLQuery query = contestSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("contestId", Hibernate.INTEGER);
			query.addScalar("contestName", Hibernate.STRING);
			query.addScalar("extendedName", Hibernate.STRING);
			query.addScalar("zone", Hibernate.STRING);
			query.addScalar("grandPrizeEvent", Hibernate.STRING);
			query.addScalar("questionSize", Hibernate.INTEGER);
			query.addScalar("contestDateTime", Hibernate.TIMESTAMP);
			query.addScalar("rewardDollarsPrize", Hibernate.DOUBLE);
			query.addScalar("grandWinnerTicketPrize", Hibernate.INTEGER);
			query.addScalar("grandPrizeWinner", Hibernate.INTEGER);						
			query.addScalar("categoryType", Hibernate.STRING);
			query.addScalar("categoryName", Hibernate.STRING);
			query.addScalar("categoryId", Hibernate.INTEGER);
			query.addScalar("promoCategoryType", Hibernate.STRING);
			query.addScalar("promoCategoryName", Hibernate.STRING);
			query.addScalar("promoCategoryId", Hibernate.INTEGER);
			query.addScalar("pricePerTicket", Hibernate.DOUBLE);
			query.addScalar("discountCode", Hibernate.STRING);
			query.addScalar("discountPercentage", Hibernate.DOUBLE);
			query.addScalar("hostName", Hibernate.STRING);
			query.addScalar("producerName", Hibernate.STRING);	
			query.addScalar("directorName", Hibernate.STRING);	
			query.addScalar("technicalDirectorName", Hibernate.STRING);			
			query.addScalar("backendOperatorName", Hibernate.STRING);
			query.addScalar("isQuestionEntered", Hibernate.BOOLEAN);
			query.addScalar("isQuestionVerified", Hibernate.BOOLEAN);
			query.addScalar("questionEnteredBy", Hibernate.INTEGER);			
			query.addScalar("questionVerifiedBy", Hibernate.INTEGER);
			query.addScalar("questionEnteredByName", Hibernate.STRING);			
			query.addScalar("questionVerifiedByName", Hibernate.STRING);
			query.addScalar("rewardDollarsPerQuestion", Hibernate.DOUBLE);
			query.addScalar("approvedBy", Hibernate.INTEGER);
			query.addScalar("approvedByName", Hibernate.STRING);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);
			query.addScalar("updatedBy", Hibernate.STRING);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("contestCategory", Hibernate.STRING);
			query.addScalar("participantStars", Hibernate.INTEGER);
			query.addScalar("referralStars", Hibernate.INTEGER);
			performers = query.setResultTransformer(Transformers.aliasToBean(PreContestChecklist.class)).list();
			
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return performers;
	}

}
