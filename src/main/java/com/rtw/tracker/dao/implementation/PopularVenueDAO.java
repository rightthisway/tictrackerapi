package com.rtw.tracker.dao.implementation;

import java.util.Collection;
import java.util.List;

import com.rtw.tracker.datas.PopularGrandChildCategory;
import com.rtw.tracker.datas.PopularVenue;

public class PopularVenueDAO extends HibernateDAO<Integer, PopularVenue> implements com.rtw.tracker.dao.services.PopularVenueDAO{


	public Collection<PopularVenue> getAllActivePopularVenueByProductId(Integer productId) throws Exception {
		return find("FROM PopularVenue WHERE status='ACTIVE' and productId=? ",new Object[]{productId});
	}
	public Integer getAllActivePopularVenueCountByProductId(Integer productId) throws Exception {

		List list = find("SELECT count(id) FROM PopularVenue WHERE status='ACTIVE' and productId=?",new Object[]{productId});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();		
	}
}
