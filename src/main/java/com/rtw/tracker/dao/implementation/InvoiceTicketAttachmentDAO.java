package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.InvoiceTicketAttachment;
import com.rtw.tracker.enums.FileType;


public class InvoiceTicketAttachmentDAO extends HibernateDAO<Integer, InvoiceTicketAttachment> implements com.rtw.tracker.dao.services.InvoiceTicketAttachmentDAO{

	@Override
	public List<InvoiceTicketAttachment> getTicketAttachmentByInvoiceId(Integer invoiceId) {
		return find("FROM InvoiceTicketAttachment where invoiceId=? order by type,position",new Object[]{invoiceId});
	}
	

	@Override
	public List<InvoiceTicketAttachment> getAttachmentByInvoiceAndFileType(Integer invoiceId, FileType fileType) {
		try {
			return find("FROM InvoiceTicketAttachment where invoiceId=? AND type = ? order by position", new Object[]{invoiceId,fileType});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public InvoiceTicketAttachment getAttachmentByInvoiceFileTypeAndPosition(Integer invoiceId, FileType fileType, Integer posotion) {
		try {
			return findSingle("FROM InvoiceTicketAttachment where invoiceId=? AND type = ? and position=?", new Object[]{invoiceId,fileType,posotion});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<InvoiceTicketAttachment> getAttachmentByFileName(String fileName){
		try {
			return find("FROM InvoiceTicketAttachment where fileOriginalName = ? order by position", new Object[]{fileName});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
