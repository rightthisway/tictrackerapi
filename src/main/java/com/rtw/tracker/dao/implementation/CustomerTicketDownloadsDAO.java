package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.dao.implementation.HibernateDAO;
import com.rtw.tracker.datas.CustomerTicketDownloads;

public class CustomerTicketDownloadsDAO extends HibernateDAO<Integer, CustomerTicketDownloads> implements com.rtw.tracker.dao.services.CustomerTicketDownloadsDAO{

	public CustomerTicketDownloads getCustomerTicketDownloadsById(Integer id){
		return findSingle("FROM CustomerTicketDownloads Where id = ?" ,new Object[]{id});
	}
	
	public List<CustomerTicketDownloads> getCustomerTicketDownloadsByInvoiceId(Integer invoiceId){
		return find("FROM CustomerTicketDownloads Where invoiceId = ?" ,new Object[]{invoiceId});
	}
	
	public List<CustomerTicketDownloads> getCustomerTicketDownloadsByCustomerId(Integer customerId){
		return find("FROM CustomerTicketDownloads Where customerId = ?" ,new Object[]{customerId});
	}
}