package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.RtfGiftCardQuantity;

public class RtfGiftCardQuantityDAO extends HibernateDAO<Integer, RtfGiftCardQuantity> implements com.rtw.tracker.dao.services.RtfGiftCardQuantityDAO{

	@Override
	public List<RtfGiftCardQuantity> getAllGiftCardQuantityByCardId(Integer cardId) {
		return find("FROM RtfGiftCardQuantity where cardId=? and status=?",new Object[]{cardId,true});
	}
	
	@Override
	public List<RtfGiftCardQuantity> getExpiredGiftCardQuantityByCardId(Integer cardId) {
		return find("FROM RtfGiftCardQuantity where cardId=? and status=?",new Object[]{cardId,false});
	}

}
