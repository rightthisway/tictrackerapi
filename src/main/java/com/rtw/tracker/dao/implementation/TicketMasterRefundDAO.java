package com.rtw.tracker.dao.implementation;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tracker.datas.Contests;
import com.rtw.tracker.datas.TicketMasterRefund;

public class TicketMasterRefundDAO extends HibernateDAO<Integer, TicketMasterRefund> implements com.rtw.tracker.dao.services.TicketMasterRefundDAO{

	private static Session tmSession = null;
	
	public static Session getTmSession() {
		return TicketMasterRefundDAO.tmSession;
	}

	public static void setTmSession(Session tmSession) {
		TicketMasterRefundDAO.tmSession = tmSession;
	}



	@Override
	public List<String> getAllUniqueUsers() {
		List<String> userList = new ArrayList<String>();
		String sql = "Select b.name as Name "+
				"From ZZZ_tmp_tm_refunds a with(nolock) "+
				"inner join ZZZ_tmp_tm_refunds_KNOWN_emails b with(nolock) on a.id = b.id "+
				"where b.nameCategory = 'All Our Cards' "+
				"group by   b.name "+
				"order by   b.name ";
		try {
			tmSession = TicketMasterRefundDAO.getTmSession();
			if(tmSession==null || !tmSession.isOpen() || !tmSession.isConnected()){
				tmSession = getSession();
				TicketMasterRefundDAO.setTmSession(tmSession);
			}
			SQLQuery query = tmSession.createSQLQuery(sql);
			List<String> list = query.list();
			for(String ob : list){
				userList.add(ob);
			}
		} catch (Exception e) {
			e.printStackTrace();
			if(tmSession != null && tmSession.isOpen()){
				tmSession.close();
			}
		}
		return userList;
	}

	@Override
	public List<TicketMasterRefund> getAllTMRecordsByUserName(String userName) {
		List<TicketMasterRefund> list = new ArrayList<TicketMasterRefund>();
		/*String sql = "Select b.name as name, b.checkEmail as verifiedEmail, a.NameOnCC as ccName "+
				", a.TicketCost as ticketCost, c.RefundReceived as refReceived "+
				", a.InvoiceDate as invoiceDate, a.PODate as poDate, a.POId as poId "+
				", a.Event as eventName, a.EventDate as eventDate, a.Venue as venue, a.TicketQty as ticketQty "+
				", a.RefundQty as refundQty, a.RefundCost as refundCost, a.Section as section, a.Row as row "+
				", a.InternalTicketNotes as internalNotes, a.email as email, a.OLDemail as oldEmail "+ 
				", a.ErrorsFixedbyHand as error,c.MergedRecs as mergRec "+
				"From ZZZ_tmp_tm_refunds a with(nolock) "+
				"inner join ZZZ_tmp_tm_refunds_KNOWN_emails b with(nolock) on a.id = b.id "+
				"left join ZZZ_tmp_tm_refunds_KNOWN_emails_upload_mapped c with(nolock) on b.name = c.name "+ 
				"and b.checkEmail = c.checkEmail and b.nameCategory = c.nameCategory "+
				"where b.nameCategory = 'All Our Cards' "+
				"and b.name = '"+userName+"' "+
				"and a.InvoiceDate = c.InvoiceDate and a.TicketCost = c.RefundDue "+
				"and a.POId = c.POId and a.NameOnCC = c.NameOnCC "+
				"and a.PODate = c.PODate and a.Vendor = c.Vendor "+
				"and a.Event = c.Event and a.EventDate = c.EventDate "+
				"and a.Venue = c.Venue and a.TicketQty = c.TicketQty "+
				"and a.RefundQty = c.RefundQty and a.RefundCost = c.RefundCost "+
				"and a.Section = c.Section and a.Row = c.Row "+
				"and IIF(a.InternalTicketNotes IS NULL, 'InternalTicketNotes',a.InternalTicketNotes) = IIF(c.InternalTicketNotes IS NULL, "+ "'InternalTicketNotes',c.InternalTicketNotes) "+
				"and IIF(a.email IS NULL, 'emailemail',a.email) = IIF(c.email IS NULL, 'emailemail',c.email) "+
				"and IIF(a.OLDemail IS NULL, 'OLDemailOLDemail', a.OLDemail ) = IIF(c.OLDemail IS NULL, 'OLDemailOLDemail', c.OLDemail ) "+
				"and IIF(a.ErrorsFixedbyHand IS NULL, 'ErrorsFixedbyHand', a.ErrorsFixedbyHand ) = IIF(c.ErrorsFixedbyHand IS NULL, "+ "'ErrorsFixedbyHand', c.ErrorsFixedbyHand ) "+ 
				"order by b.name, convert(datetime, a.PODate),a.TicketCost, c.MergedRecs";*/
		try {
			tmSession = TicketMasterRefundDAO.getTmSession();
			if(tmSession==null || !tmSession.isOpen() || !tmSession.isConnected()){
				tmSession = getSession();
				TicketMasterRefundDAO.setTmSession(tmSession);
			}
			//SQLQuery query = tmSession.createSQLQuery(sql);
			CallableStatement st = tmSession.connection().prepareCall("{call sp_zzz_tm_refunds_perName_return_received(?)}");
			st.setString(1, userName);
			ResultSet resultSet = st.executeQuery();
			System.out.println(resultSet.getFetchSize());
			TicketMasterRefund tg = null;
			while(resultSet.next()){
				tg = new TicketMasterRefund();
				tg.setName(resultSet.getString("name"));
				tg.setVerifiedEmail(resultSet.getString("checkEmail"));
				tg.setTicketCost(resultSet.getDouble("RefundDue"));
				tg.setRefReceived(resultSet.getDouble("RefundReceived"));
				tg.setCcName(resultSet.getString("NameOnCC"));
				tg.setInvoiceDate(resultSet.getString("InvoiceDate"));
				tg.setPoDate(resultSet.getString("PODate"));
				tg.setPoId(resultSet.getString("POId"));
				tg.setEventName(resultSet.getString("Event"));
				tg.setEventDate(resultSet.getString("EventDate"));
				tg.setVenue(resultSet.getString("Venue"));
				tg.setTicketQty(resultSet.getInt("TicketQty"));
				tg.setRefundQty(resultSet.getInt("RefundQty"));
				tg.setRefundCost(resultSet.getDouble("RefundCost"));
				tg.setSection(resultSet.getString("Section"));
				tg.setRow(resultSet.getString("Row"));
				tg.setVendor(resultSet.getString("Vendor"));
				tg.setInternalNotes(resultSet.getString("InternalTicketNotes"));
				tg.setEmail(resultSet.getString("email"));
				tg.setOldEmail(resultSet.getString("OLDemail"));
				tg.setError(resultSet.getString("ErrorsFixedbyHand"));
				tg.setMergRec(resultSet.getInt("MergedRecs"));
				list.add(tg);
			}
			
			/*query.addScalar("name", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("verifiedEmail", Hibernate.STRING);
			query.addScalar("ticketCost", Hibernate.DOUBLE);
			query.addScalar("refReceived", Hibernate.DOUBLE);
			query.addScalar("ccName", Hibernate.STRING);
			query.addScalar("invoiceDate", Hibernate.STRING);
			query.addScalar("poDate", Hibernate.STRING);
			query.addScalar("poId", Hibernate.STRING);
			query.addScalar("eventName", Hibernate.STRING);
			query.addScalar("eventDate", Hibernate.STRING);
			query.addScalar("venue", Hibernate.STRING);
			query.addScalar("ticketQty", Hibernate.INTEGER);
			query.addScalar("refundQty", Hibernate.INTEGER);
			query.addScalar("refundCost", Hibernate.DOUBLE);
			query.addScalar("section", Hibernate.STRING);
			query.addScalar("row", Hibernate.STRING);
			query.addScalar("internalNotes", Hibernate.STRING);
			query.addScalar("oldEmail", Hibernate.STRING);
			query.addScalar("error", Hibernate.STRING);
			query.addScalar("mergRec", Hibernate.INTEGER);
			list = query.setResultTransformer(Transformers.aliasToBean(TicketMasterRefund.class)).list();*/
		} catch (Exception e) {
			e.printStackTrace();
			if(tmSession != null && tmSession.isOpen()){
				tmSession.close();
			}
		}
		return list;
	}

	@Override
	public List<Object[]> getTMRefundByCCUsers() {
		List<Object[]> list = null;
		String sql = "select a.nameCategory, a.NameCCHolder as name "+
				", b.totalCost as RefundsDue "+
				", IIF(a.Credits IS NULL,0, a.Credits) as Credits "+
				", a.CCBalance as CCBalance "+
				", a.CreditBalance as CreditBalance "+
				", CONVERT(DECIMAL(10,2),(a.Credits/b.totalCost)*100) as PercentageRefundsCredited "+
				"from ZZZ_tmp_tm_refunds_SUMMARY_Balance a "+
				"inner join ZZZ_tmp_tm_refunds_SUMMARY_Totals b on a.NameCCHolder = b.ccName and a.nameCategory = b.nameCategory "+
				"where a.nameCategory = 'All Our Cards' "+
				"order by NameCCHolder ";
		try {
			tmSession = TicketMasterRefundDAO.getTmSession();
			if(tmSession==null || !tmSession.isOpen() || !tmSession.isConnected()){
				tmSession = getSession();
				TicketMasterRefundDAO.setTmSession(tmSession);
			}
			SQLQuery query = tmSession.createSQLQuery(sql);
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
			if(tmSession != null && tmSession.isOpen()){
				tmSession.close();
			}
		}
		return list;
	}
	
	
	
	
	
	
	
	
	
	

}
