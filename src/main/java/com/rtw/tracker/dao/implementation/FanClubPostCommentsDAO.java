package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.datas.FanClubPostComments;
import com.rtw.tracker.utils.GridHeaderFilters;

public class FanClubPostCommentsDAO extends HibernateDAO<Integer, FanClubPostComments> implements com.rtw.tracker.dao.services.FanClubPostCommentsDAO{

	
	private static Session fanClubSession;
	
	public static Session getFanClubSession() {
		return fanClubSession;
	}
	public static void setFanClubSession(Session fanClubSession) {
		FanClubPostCommentsDAO.fanClubSession = fanClubSession;
	}
	@Override
	public List<FanClubPostComments> getAllFanClubPostComments(GridHeaderFilters filter, String status,String commentType, Integer commentTypeId,SharedProperty prop) {
		List<FanClubPostComments> list =null;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("select pc.id as id,pc.fanclub_posts_id as postId,pc.fanclub_video_id as videoId,pc.fanclub_event_id as eventId,pc.customer_id as customerId,pc.cust_comment_text as commentText,");
			sql.append("pc.cust_comment_media_url as mediaUrl,pc.cust_comment_poster_url as posterUrl,pc.cust_comment_image_url as imageUrl,");
			sql.append("pc.created_user_id as createdBy, pc.updated_user_id as updatedBy,pc.comment_status as commentStatus,pc.block_reason as blockReason,");
			sql.append("pc.created_date as createdDate, pc.updated_date as updatedDate,");
			sql.append("c.user_id as userId, c.email as email, c.phone as phone ");
			sql.append("from fanclub_customer_comments_on_media pc with(nolock) ");
			sql.append("left join "+prop.getDatabasAlias()+"customer c with(nolock) on pc.customer_id = c.id ");
			sql.append("WHERE pc.comment_status='"+status+"'");
			
			if(commentType.equals("POST")) {
				sql.append(" AND pc.fanclub_posts_id ="+commentTypeId);
			} else if (commentType.equals("VIDEO")) {
				sql.append(" AND pc.fanclub_video_id ="+commentTypeId);
			}
			
			if(filter.getArtistName()!=null && !filter.getArtistName().isEmpty()){
				sql.append(" AND pc.cust_comment_text like '%"+filter.getArtistName()+"%' ");
			}
			if(filter.getCreatedBy()!=null && !filter.getCreatedBy().isEmpty()){
				sql.append(" AND pc.created_user_id  like '%"+filter.getCreatedBy()+"%' ");
			}
			if(filter.getUpdatedBy()!=null && !filter.getUpdatedBy().isEmpty()){
				sql.append(" AND pc.updated_user_id  like '%"+filter.getUpdatedBy()+"%' ");
			}
			
			if(filter.getStatus()!=null && !filter.getStatus().isEmpty()){
				sql.append(" AND pc.comment_status  like '%"+filter.getStatus()+"%' ");
			}
			if(filter.getUsername()!=null && !filter.getUsername().isEmpty()){
				sql.append(" AND c.user_id like '%"+filter.getUsername()+"%' ");
			}
			if(filter.getEmail()!=null && !filter.getEmail().isEmpty()){
				sql.append(" AND c.email like '%"+filter.getEmail()+"%' ");
			}
			if(filter.getPhone()!=null && !filter.getPhone().isEmpty()){
				sql.append(" AND c.phone like '%"+filter.getPhone()+"%' ");
			}
			if(filter.getText1()!=null && !filter.getText1().isEmpty()){
				sql.append(" AND pc.block_reason like '%"+filter.getText1()+"%' ");
			}
			
			
			
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql.append(" AND DATEPART(day, pc.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql.append(" AND DATEPART(month, pc.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql.append(" AND DATEPART(year, pc.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ");
				}
			}
			
			if(filter.getLastUpdatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					sql.append(" AND DATEPART(day, pc.updated_date) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql.append(" AND DATEPART(month, pc.updated_date) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql.append(" AND DATEPART(year, pc.updated_date) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ");
				}
			}
			
			fanClubSession = FanClubPostCommentsDAO.getFanClubSession();
			if(fanClubSession==null || !fanClubSession.isOpen() || !fanClubSession.isConnected()){
				fanClubSession = getSession();
				FanClubPostCommentsDAO.setFanClubSession(fanClubSession);
			}
			SQLQuery query = fanClubSession.createSQLQuery(sql.toString());
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("postId", Hibernate.INTEGER);
			query.addScalar("videoId", Hibernate.INTEGER);
			query.addScalar("eventId", Hibernate.INTEGER);
			query.addScalar("commentText", Hibernate.STRING);
			query.addScalar("customerId", Hibernate.INTEGER);
			query.addScalar("imageUrl", Hibernate.STRING);
			query.addScalar("mediaUrl", Hibernate.STRING);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("updatedBy", Hibernate.STRING);
			query.addScalar("commentStatus", Hibernate.STRING);
			query.addScalar("posterUrl", Hibernate.STRING);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);
			query.addScalar("userId", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("phone", Hibernate.STRING);
			query.addScalar("blockReason", Hibernate.STRING);
			
			list = query.setResultTransformer(Transformers.aliasToBean(FanClubPostComments.class)).list();
		} catch (Exception e) {
			if(fanClubSession != null && fanClubSession.isOpen()){
				fanClubSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
}
