package com.rtw.tracker.dao.implementation;

import java.util.Collection;

import com.rtw.tracker.datas.Venue;

public class VenueDAO extends HibernateDAO<Integer, Venue> implements com.rtw.tracker.dao.services.VenueDAO{

	@Override
	public Collection<Venue> getAll() {
		return find("FROM Venue");
	}
	
	@Override
	public Collection<Venue> filterByName(String venueName) {
		return find("FROM Venue WHERE building like ? or city like ? or country like ?",new Object[]{"%"+venueName+"%","%"+venueName+"%","%"+venueName+"%"});
	}
}
