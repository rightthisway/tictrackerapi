package com.rtw.tracker.dao.implementation;

import com.rtw.tracker.datas.ManualFedexGeneration;

public class ManualFedexGenerationDAO extends HibernateDAO<Integer, ManualFedexGeneration> implements com.rtw.tracker.dao.services.ManualFedexGenerationDAO{

	public ManualFedexGeneration getManualFedexByTrackingNo(String trackingNo){
		return findSingle("FROM ManualFedexGeneration WHERE trackingNumber=?",new Object[]{trackingNo});
	}
}
