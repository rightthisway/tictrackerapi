package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.UserPreference;

public class UserPreferenceDAO extends HibernateDAO<Integer, UserPreference> implements com.rtw.tracker.dao.services.UserPreferenceDAO{

	@Override
	public UserPreference getPreferenceByUserNameandGridName(String userName,String gridName) {
		return findSingle("FROM UserPreference WHERE userName=? AND gridName=?",new Object[]{userName,gridName});
	}
	
	@Override
	public List<UserPreference> getPreferenceByUserName(String userName) {
		return find("FROM UserPreference WHERE userName=?",new Object[]{userName});
	}

}
