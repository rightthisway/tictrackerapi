package com.rtw.tracker.dao.implementation;

import com.rtw.tracker.datas.POSEvent;

public class POSEventDAO extends HibernateDAO<Integer, POSEvent> implements com.rtw.tracker.dao.services.POSEventDAO{

	public POSEvent getPOSEventByEventID(Integer eventID) throws Exception{
		return findSingle("FROM POSEvent WHERE eventId=?", new Object[]{eventID});
	}
}
