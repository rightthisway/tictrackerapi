package com.rtw.tracker.dao.implementation;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.rtw.tmat.enums.EventStatus;
import com.rtw.tracker.datas.AddressType;
import com.rtw.tracker.datas.CustomerAddress;

public class CustomerAddressDAO extends HibernateDAO<Integer, CustomerAddress> implements com.rtw.tracker.dao.services.CustomerAddressDAO{

	@Override
	public CustomerAddress getCustomerAddressInfo(Integer customerId) {
		
		return findSingle("From CustomerAddress Where customerId = ? and addressType = ?", new Object[]{customerId,AddressType.BILLING_ADDRESS});
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CustomerAddress> getShippingAddressInfo(Integer customerId) {
		List<CustomerAddress> shippingAddressList = new ArrayList<CustomerAddress>();
		try{
			Session session = getSession();
			Query query = session.createQuery("From CustomerAddress Where customerId = :customerId and addressType = :addressType and status = :status ")
							.setParameter("customerId", customerId)
							.setParameter("addressType", AddressType.SHIPPING_ADDRESS)
							.setParameter("status", EventStatus.ACTIVE);
			shippingAddressList = query.list();
			return shippingAddressList;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<CustomerAddress> getShippingAndOtherAddresInfo(Integer customerId) {
		List<CustomerAddress> shippingAddressList = new ArrayList<CustomerAddress>();
		try{
			Session session = getSession();
			Query query = session.createQuery("From CustomerAddress Where customerId = :customerId and addressType != :addressType and status = :status ")
							.setParameter("customerId", customerId)
							.setParameter("addressType", AddressType.BILLING_ADDRESS)
							.setParameter("status", EventStatus.ACTIVE);
			shippingAddressList = query.list();
			return shippingAddressList;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CustomerAddress> updatedShippingAddress(Integer customerId) {
		List<CustomerAddress> shippingAddressList = new ArrayList<CustomerAddress>();
		try{
			Session session = getSession();
			Query query = session.createQuery("From CustomerAddress Where customerId = :customerId and addressType = :addressType and status = :status order by id desc")
							.setParameter("customerId", customerId)
							.setParameter("addressType", AddressType.SHIPPING_ADDRESS)
							.setParameter("status", EventStatus.ACTIVE);
							/*.setParameter("currentDate", new Date())*/
			shippingAddressList = query.setMaxResults(1).list();
			return shippingAddressList;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Integer getShippingAddressCount(Integer customerId) {
		int count = 0;
		try {
			Session session = getSession();
			count = ((Long) session.createQuery("select count(*) from CustomerAddress where customerId = :customerId and addressType = :addressType and status = :status ")
							.setParameter("customerId", customerId)
							.setParameter("addressType", AddressType.SHIPPING_ADDRESS)
							.setParameter("status", EventStatus.ACTIVE)
							.uniqueResult())
							.intValue();
			return count;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public CustomerAddress getCustomerShippingAddresByAddresId(Integer shipAddrId) {
		
		return findSingle("From CustomerAddress Where id = ? ", new Object[]{shipAddrId});
	}

}
