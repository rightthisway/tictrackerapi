package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.datas.ContestPromocode;
import com.rtw.tracker.utils.GridHeaderFilters;

public class ContestPromocodeDAO extends HibernateDAO<Integer, ContestPromocode> implements com.rtw.tracker.dao.services.ContestPromocodeDAO{

	//private final String databseName = "ZonesApiPlatformV2.dbo.";
	private static Session contestPromoSession;
	
	
	public static Session getContestSession() {
		return ContestPromocodeDAO.contestPromoSession;
	}
	public static void setContestSession(Session contestSession) {
		ContestPromocodeDAO.contestPromoSession = contestSession;
	}
	
	public List<ContestPromocode> getAllContestPromocodes(Integer id, GridHeaderFilters filter,String type,String status,SharedProperty sharedProperty){
		List<ContestPromocode> contestsList = null;		
		try {
			String sql = "select c.id as id,c.customer_id as customerId,c.promo_reward_id as promoRewardId,c.promo_name as promoName,c.promo_type as promoType,"+
						"c.promotional_code as promoCode,c.earn_life_count as earnLifeCount,c.free_tix_count as freeTixCount,c.discount_percentage as discountPercentage,"+
						"c.promo_ref_id as promoRefId,c.promo_ref_type as promoRefType,c.promo_ref_name as promoRefName,c.promo_expiry_date as promoExpiryDate,"+
						"c.promo_offer_id as promoOfferId,c.status as status,c.created_date as createdDate,c.created_by as createdBy,c.updated_date as updatedDate,"+
						"c.updated_by as updatedBy,c.apply_date as applyDate,co.user_id as userId,co.email as email from customer_promo_reward_details c join "+sharedProperty.getDatabasAlias()+"customer co" +
								" on c.customer_id=co.id WHERE c.promo_type='REWARD_FREE_TIX' ";
			
			if(id != null  && id > 0){
				sql += " AND c.id="+id;
			}
			if(type!=null && !type.isEmpty()){
				sql += " AND c.promo_type ='"+type+"' ";
			}
			if(filter.getPromoCode()!=null && !filter.getPromoCode().isEmpty()){
				sql += " AND c.promo_name like '%"+filter.getPromoCode()+"%' ";
			}
			if(filter.getCustomerName()!=null && !filter.getCustomerName().isEmpty()){
				sql += " AND co.user_id like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getPromoType()!=null && !filter.getPromoType().isEmpty()){
				sql += " AND c.promo_type like '%"+filter.getPromoType()+"%' ";
			}
			if(filter.getPromoReferenceName()!=null && !filter.getPromoReferenceName().isEmpty()){
				sql += " AND c.promo_ref_name like '%"+filter.getPromoReferenceName()+"%' ";
			}
			if(filter.getStatus()!=null && !filter.getStatus().isEmpty()){
				sql += " AND c.status like '%"+filter.getStatus()+"%' ";
			}
			if(filter.getCreatedBy()!=null && !filter.getCreatedBy().isEmpty()){
				sql += " AND c.created_by like '%"+filter.getCreatedBy()+"%' ";
			}
			if(filter.getUpdatedBy()!=null && !filter.getUpdatedBy().isEmpty()){
				sql += " AND c.updated_by like '%"+filter.getUpdatedBy()+"%' ";
			}
			if(filter.getTicketQty()!=null){
				sql += " AND c.free_tix_count = "+filter.getTicketQty();
			}
			
			if(filter.getEndDate()!=null ){
				if(Util.extractDateElement(filter.getEndDate(),"DAY") > 0){
					sql += " AND DATEPART(day, c.promo_expiry_date) = "+Util.extractDateElement(filter.getEndDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEndDate(),"MONTH") > 0){
					sql += " AND DATEPART(month, c.promo_expiry_date) = "+Util.extractDateElement(filter.getEndDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEndDate(),"YEAR") > 0){
					sql += " AND DATEPART(year, c.promo_expiry_date) = "+Util.extractDateElement(filter.getEndDate(),"YEAR")+" ";
				}
			}
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day, c.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month, c.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year, c.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getUpdatedDate()!=null ){
				if(Util.extractDateElement(filter.getUpdatedDate(),"DAY") > 0){
					sql += " AND DATEPART(day, c.updated_date) = "+Util.extractDateElement(filter.getUpdatedDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"MONTH") > 0){
					sql += " AND DATEPART(month, c.updated_date) = "+Util.extractDateElement(filter.getUpdatedDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"YEAR") > 0){
					sql += " AND DATEPART(year, c.updated_date) = "+Util.extractDateElement(filter.getUpdatedDate(),"YEAR")+" ";
				}
			}
			
			sql += " order by c.id desc";
			contestPromoSession = ContestPromocodeDAO.getContestSession();
			if(contestPromoSession==null || !contestPromoSession.isOpen() || !contestPromoSession.isConnected()){
				contestPromoSession = getSession();
				ContestPromocodeDAO.setContestSession(contestPromoSession);
			}
			SQLQuery query = contestPromoSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("customerId", Hibernate.INTEGER);
			query.addScalar("promoRewardId", Hibernate.INTEGER);
			query.addScalar("promoName", Hibernate.STRING);
			query.addScalar("promoType", Hibernate.STRING);
			query.addScalar("promoCode", Hibernate.STRING);						
			//query.addScalar("pointsPerWinner", Hibernate.DOUBLE);
			query.addScalar("earnLifeCount", Hibernate.INTEGER);
			query.addScalar("freeTixCount", Hibernate.INTEGER);
			query.addScalar("discountPercentage", Hibernate.DOUBLE);
			query.addScalar("promoRefId", Hibernate.INTEGER);
			query.addScalar("promoRefType", Hibernate.STRING);
			query.addScalar("promoRefName", Hibernate.STRING);
			query.addScalar("promoExpiryDate", Hibernate.TIMESTAMP);	
			query.addScalar("promoOfferId", Hibernate.INTEGER);	
			query.addScalar("status", Hibernate.STRING);			
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);
			query.addScalar("updatedBy", Hibernate.STRING);	
			query.addScalar("userId", Hibernate.STRING);	
			query.addScalar("email", Hibernate.STRING);	
			query.addScalar("applyDate", Hibernate.TIMESTAMP);
			contestsList = query.setResultTransformer(Transformers.aliasToBean(ContestPromocode.class)).list();
		} catch (Exception e) {
			if(contestPromoSession != null && contestPromoSession.isOpen()){
				contestPromoSession.close();
			}
			e.printStackTrace();
		}
		return contestsList;
	}
	
	
	
	
}
