package com.rtw.tracker.dao.implementation;

import com.rtw.tracker.datas.RTFCustomerPromotionalOffer;

public class RTFCustomerPromotionalOfferDAO extends HibernateDAO<Integer, RTFCustomerPromotionalOffer> implements com.rtw.tracker.dao.services.RTFCustomerPromotionalOfferDAO{

	public RTFCustomerPromotionalOffer getPromoOfferCustomerId(Integer customerId){
		return findSingle("from RTFCustomerPromotionalOffer where customerId=? ", new Object[]{customerId});
	}
	
	public void updatePromotionaOfferOrderCount(Integer offerId) {
		try {
			String sql = "update RTFCustomerPromotionalOffer set noOfOrders=noOfOrders + 1 , status=?  where id = ?";
			bulkUpdate(sql, new Object[]{"REDEEMED",offerId});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
