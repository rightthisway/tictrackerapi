package com.rtw.tracker.dao.implementation;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.rtw.tracker.datas.AffiliatePromoCodeHistory;

public class AffiliatePromoCodeHistoryDAO extends HibernateDAO<Integer, AffiliatePromoCodeHistory> implements com.rtw.tracker.dao.services.AffiliatePromoCodeHistoryDAO{

	public AffiliatePromoCodeHistory getActiveAffiliatePromoCode(Integer userId) throws Exception{
		return findSingle("From AffiliatePromoCodeHistory where status = ? and userId=?" ,new Object[]{"ACTIVE",userId});
	}
	
	@SuppressWarnings("unchecked")
	public List<AffiliatePromoCodeHistory> getAllHistoryByUserId(Integer userId) throws Exception{
		return find("From AffiliatePromoCodeHistory where userId=? order by id desc" ,new Object[]{userId});
	}
	
	public void updateAllActiveHistoryByUserId(Integer userId) throws Exception{
		bulkUpdate("UPDATE AffiliatePromoCodeHistory set status = ?, updateDate = ? where status = ? and userId=?" ,new Object[]{"DELETED",new Date(),"ACTIVE",userId});
	}
	
	public Collection<AffiliatePromoCodeHistory> getAllActivePromoCodes()throws Exception{
		return find("From AffiliatePromoCodeHistory where status = ?", new Object[]{"ACTIVE"});
	}
	
	public AffiliatePromoCodeHistory checkActiveAffiliatePromoCode(String promoCode) throws Exception{
		return findSingle("From AffiliatePromoCodeHistory where status = ? and promoCode = ?" ,new Object[]{"ACTIVE",promoCode});
	}
}
