package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tracker.datas.PollingCategory;
import com.rtw.tracker.datas.PollingSponsor;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.GridSortingUtil;

public class PollingCategoryDAO extends HibernateDAO<Integer, PollingCategory>
		implements com.rtw.tracker.dao.services.PollingCategoryDAO {

	private static Session pollingSession = null;

	public static Session getPollingSession() {
		return PollingCategoryDAO.pollingSession;
	}

	public static void setPollingSession(Session pollingSession) {
		PollingCategoryDAO.pollingSession = pollingSession;
	}

	@Override
	public List<PollingCategory> getAllPollingCategory(Integer id, GridHeaderFilters filter, String status) {
		List<PollingCategory> pollingCategoryList = null;
		try {
			
			StringBuilder sb = new StringBuilder(); 			
			sb.append("select g.id as id, " );
			sb.append(" g.title as title , " );
			sb.append(" g.sponsor_id as sponsorId , " );
			sb.append(" ps.name as sponsorName , " );
			sb.append(" g.polling_type as pollingType , " );			
			sb.append(" g.status as status , " );
			sb.append(" g.created_by as createdBy , " );
			sb.append(" g.created_date as createdDate , " );
			sb.append(" g.updated_by as updatedBy , " );			
			sb.append(" g.updated_date as updatedDate  from polling_category g with(nolock) left "
					+ "join polling_sponsors ps with(nolock) on ps.id = g.sponsor_id where g.status = '"+status+"'  " );
			String sortingSql = GridSortingUtil.getPollingCategorySortingSql(filter);
			if(sortingSql!=null && !sortingSql.isEmpty()){
				sb.append(" "+sortingSql);
			}else{
				sb.append(" order by g.id desc");
			}
			pollingSession = PollingCategoryDAO.getPollingSession();
			if (pollingSession == null || !pollingSession.isOpen() || !pollingSession.isConnected()) {
				pollingSession = getSession();				
				PollingCategoryDAO.setPollingSession(pollingSession);
			}
			
			SQLQuery query = pollingSession.createSQLQuery(sb.toString());
			query.addScalar("id", Hibernate.INTEGER);			
			query.addScalar("title", Hibernate.STRING);
			query.addScalar("sponsorId", Hibernate.INTEGER);
			query.addScalar("sponsorName", Hibernate.STRING);
			query.addScalar("pollingType", Hibernate.STRING);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("updatedBy", Hibernate.STRING);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);

			pollingCategoryList = query.setResultTransformer(Transformers.aliasToBean(PollingCategory.class)).list();
		} catch (Exception e) {
			if (pollingSession != null && pollingSession.isOpen()) {
				pollingSession.close();
			}
			e.printStackTrace();
		}
		return pollingCategoryList;

	}

	@Override
	public PollingCategory getPollingCategoryById(Integer id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PollingCategory savePollingCategory(PollingCategory pollingCategory) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
}