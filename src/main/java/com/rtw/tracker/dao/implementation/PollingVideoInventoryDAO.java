package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.datas.Contests;
import com.rtw.tracker.datas.PollingVideoInventory;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.GridSortingUtil;

public class PollingVideoInventoryDAO extends HibernateDAO<Integer, PollingVideoInventory> implements com.rtw.tracker.dao.services.PollingVideoInventoryDAO{

	
private static Session staticSession = null;
	
	public static Session getStaticSession() {
		return PollingVideoInventoryDAO.staticSession;
	}

	public static void setStaticSession(Session staticSession) {
		PollingVideoInventoryDAO.staticSession = staticSession;
	}

	@Override
	public List<PollingVideoInventory> getAllPollingVideoINventories(GridHeaderFilters filter, String status) {
		
		List<PollingVideoInventory> list = null;
		StringBuilder sql = new StringBuilder();
		sql.append( "select pi.id as id,pi.video_url as videoUrl,pc.category_name as category,pi.title as title,pi.video_description as description,ps.name as sponsorName,");
		sql.append( " pi.status as status,pi.is_default as isDefault, pi.created_date as createdDate,pi.created_by as createdBy,pi.poster_url as posterUrl	");
		sql.append( " from polling_video_inventory pi with(nolock)  join polling_video_categories pc with(nolock) on ");
		sql.append( " pi.category_id = pc.id left join polling_sponsors ps on pi.sponsor_id=ps.id WHERE 1=1  and  (pi.is_deleted is null or pi.is_deleted='0')");
		try {
			
			if(filter.getId() !=null){
				sql.append( " AND id="+filter.getId()+"");
			}
			if(filter.getEventDescription()!=null){
				sql.append( "  AND title like '%"+filter.getEventDescription()+"%'");
			}
			if(filter.getArtistName()!=null){
				sql.append( "  AND pi.video_description like '%"+filter.getArtistName()+"%'");
			}
			if(filter.getUrl()!=null){
				sql.append( "  AND video_url like '%"+filter.getUrl()+"%'");
			}
			if(filter.getCategory()!=null){
				sql.append( "  AND category like '%"+filter.getCategory()+"%'");
			}
			if(filter.getText1()!=null){
				sql.append( "  AND ps.name like '%"+filter.getText1()+"%'");
			}
			if(filter.getStatus()!=null){
				sql.append( "  AND pi.status ="+filter.getIsDefault()+"");
			}
			if(filter.getCreatedBy()!=null){
				sql.append( "  AND created_by like '%"+filter.getCreatedBy()+"%'");
			}
			if(filter.getIsDefault()!=null){
				sql.append( "  AND is_default ="+filter.getIsDefault()+"");
			}
			if (status != null && !status.isEmpty() && !status.equalsIgnoreCase("ACTIVE")) {
				sql.append(" and pi.status = 0");
			}
			else if (status != null && !status.isEmpty() && !status.equalsIgnoreCase("PASSIVE")) {
				sql.append(" and pi.status = 1");
			}
				
			
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql.append( "  AND DATEPART(day, created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql.append( "  AND DATEPART(month,created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql.append( "  AND DATEPART(year,created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ");
				}
			}
			String sortingSql = GridSortingUtil.getRTFVideosSortingSql(filter);
			if(sortingSql!=null && !sortingSql.isEmpty()){
				sql.append(" "+sortingSql);
			}else{
				sql.append(" Order by  id desc ");
			}			
			staticSession = PollingVideoInventoryDAO.getStaticSession();
			if(staticSession==null || !staticSession.isOpen() || !staticSession.isConnected()){
				staticSession = getSession();
				PollingVideoInventoryDAO.setStaticSession(staticSession);
			}
			
			SQLQuery query = staticSession.createSQLQuery(sql.toString());
			
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("title", Hibernate.STRING);
			query.addScalar("description", Hibernate.STRING);
			query.addScalar("videoUrl", Hibernate.STRING);
			query.addScalar("category", Hibernate.STRING);
			query.addScalar("status", Hibernate.BOOLEAN);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("sponsorName", Hibernate.STRING);
			query.addScalar("isDefault", Hibernate.BOOLEAN);
			query.addScalar("posterUrl", Hibernate.STRING);
			
			
			list = query.setResultTransformer(Transformers.aliasToBean(PollingVideoInventory.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
			if(staticSession != null && staticSession.isOpen()){
				staticSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	
	@Override
	public boolean deleteRtfVideos(String ids) {
		try 
		{	
			String sql = "update  polling_video_inventory set is_deleted = 1,status=0 where id in ("+ids+" )";			
			staticSession = PollingVideoInventoryDAO.getStaticSession();
			if (staticSession == null || !staticSession.isOpen() || !staticSession.isConnected()) {
				staticSession = getSession();
				PollingVideoInventoryDAO.setStaticSession(staticSession);
			}
			Query query = staticSession.createSQLQuery(sql);
			query.executeUpdate();
		} catch (Exception e) {
			if (staticSession != null && staticSession.isOpen()) {
				staticSession.close();
			}
			e.printStackTrace();
			return false;
		}
		/*finally{
			staticSession.close();
		}*/
		return true;
	}

	@Override
	public PollingVideoInventory getVideoByUrl(String url) {
		return findSingle("FROM PollingVideoInventory WHERE status=? AND isDeleted=? and videoUrl like ?",new Object[]{true,0,url});
	}
}
