package com.rtw.tracker.dao.implementation;

import java.util.ArrayList;
import java.util.List;

import com.rtw.tracker.datas.ChildCategory;
import com.rtw.tracker.datas.ParentCategory;
/**
 * class having db related methods for parent category of a tour 
 * @author hamin
 *
 */ 
public class ParentCategoryDAO extends HibernateDAO<Integer,ParentCategory> implements
com.rtw.tracker.dao.services.ParentCategoryDAO {

	/**
	 * method to get all parent categories
	 * @return list of parent categories
	 */
	public List<ParentCategory> getAll() {
		System.out.println("getAll() of ParentCategoryDAO is called");
		return find("FROM ParentCategory order by name asc");	 
	}
	/**
	 * method to get parent category  by its name
	 * @param name, its name
	 * @return  parent category
	 */
	public ParentCategory getParentCategoryByName(String parent) {
	  return findSingle("FROM ParentCategory where name = ?", new Object[]{parent});	 
		
	}
	/**
	 * method to get parent category  by its id
	 * @param id, own id
	 * @return  parent category
	 */
	public ParentCategory getParentCategoryById(Integer id) {
		  return findSingle("FROM ParentCategory where id = ?", new Object[]{id});	 
			
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getDistinctParentCategoryNmaes() {
		  
		return getHibernateTemplate().find("SELECT distinct name FROM ParentCategory");
		
	
	}
	public List<ParentCategory> getParentCategoriesByName(String name) {
		String hql="FROM ParentCategory WHERE name like ? order by name";
		List<Object> parameters = new ArrayList<Object>();
		parameters.add("%" + name + "%");
		return find(hql, parameters.toArray());
	}
	
	public List<ParentCategory> getSportConcertTheater() {
		String hql="FROM ParentCategory WHERE id in(1,2,3) order by name";
		List<Object> parameters = new ArrayList<Object>();
		return find(hql);
	}

}
