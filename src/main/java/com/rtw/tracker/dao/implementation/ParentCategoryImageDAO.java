package com.rtw.tracker.dao.implementation;

import com.rtw.tracker.datas.ParentCategoryImage;

public class ParentCategoryImageDAO extends HibernateDAO<Integer, ParentCategoryImage> implements com.rtw.tracker.dao.services.ParentCategoryImageDAO {
	
	public ParentCategoryImage getParentCategoryImageByParentCategoryId(Integer parentCategoryId) throws Exception { 
		return findSingle("From ParentCategoryImage where parentCategory.id=?", new Object[]{parentCategoryId});		
	}
}
