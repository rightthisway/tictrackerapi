package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.FavouriteEvents;

public class FavouriteEventsDAO extends HibernateDAO<Integer, FavouriteEvents> implements com.rtw.tracker.dao.services.FavouriteEventsDAO{

	public FavouriteEvents getFavouriteEventsById(Integer eventId){
		return findSingle("FROM FavouriteEvents WHERE eventId = ? ", 
				new Object[]{eventId});
	}
	
	public FavouriteEvents getFavouriteEventsByEventAndCustomerId(Integer eventId,Integer customerId) {
		return findSingle("FROM FavouriteEvents WHERE eventId = ? and customerId = ? ", 
				new Object[]{eventId,customerId});
	}	
	
	public List<FavouriteEvents> getAllActiveFavouriteEventsByCustomerId(Integer customerId) {
		return find("from FavouriteEvents where customerId=? and status='ACTIVE' ", new Object[]{customerId});
				
	}
	
	public List<FavouriteEvents> getAllActiveFavouriteEvents() {
		return find("from FavouriteEvents where status='ACTIVE'");
				
	}
	
	
	public List<FavouriteEvents> getAllUnFavouritedEventsByCustomerId(Integer customerId) {
		return find("from FavouriteEvents where customerId=? and status='DELETED' ", new Object[]{customerId});
				
	}
}
