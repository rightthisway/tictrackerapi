package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.datas.ReferralContest;
import com.rtw.tracker.utils.GridHeaderFilters;

public class ReferralContestDAO extends HibernateDAO<Integer, ReferralContest> implements com.rtw.tracker.dao.services.ReferralContestDAO{

	public List<ReferralContest> getAllContests(Integer id, GridHeaderFilters filter){
		Session contestSession = null;
		List<ReferralContest> contestList = null;		
		try {
			String sql = "select con.id as id, con.name as name, con.min_purchase_amount as minimumPurchaseAmount, "+
				"con.start_date as startDate, con.end_date as endDate, con.created_by as createdBy, con.updated_by as updatedBy, "+
				"con.created_date as createdDate, con.updated_date as updatedDate, con.status as status, " +
				"con.contest_status as contestStatus,con.artist_id as artistId,con.artist_name artistName,a.name as artistNameStr " +
				"from referral_contest con with(nolock) "+
				"join artist a with(nolock) on con.artist_id=a.id WHERE con.status=1 ";
				
			if(id != null){
				sql += " AND con.id = "+id;
			}
			if(filter.getContestName()!=null && !filter.getContestName().isEmpty()){
				sql += " AND con.name like '"+filter.getContestName()+"' ";
			}
			if(filter.getMinimumPurchaseAmount()!=null){
				sql += " AND con.min_purchase_amount = "+filter.getMinimumPurchaseAmount();
			}
			if(filter.getStatus()!=null && !filter.getStatus().trim().isEmpty()){
				sql += " AND con.contest_status like '"+filter.getStatus()+"' ";
			}
			if(filter.getArtistName()!=null && !filter.getArtistName().trim().isEmpty()){
				sql += " AND con.artist_name like '"+filter.getArtistName()+"' ";
			}
			
			if(filter.getStartDate()!=null ){
				if(Util.extractDateElement(filter.getStartDate(),"DAY") > 0){
					sql += " AND DATEPART(day,con.start_date) = "+Util.extractDateElement(filter.getStartDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getStartDate(),"MONTH") > 0){
					sql += " AND DATEPART(month,con.start_date) = "+Util.extractDateElement(filter.getStartDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getStartDate(),"YEAR") > 0){
					sql += " AND DATEPART(year,con.start_date) = "+Util.extractDateElement(filter.getStartDate(),"YEAR")+" ";
				}
			}
			if(filter.getEndDate()!=null ){
				if(Util.extractDateElement(filter.getEndDate(),"DAY") > 0){
					sql += " AND DATEPART(day,con.end_date) = "+Util.extractDateElement(filter.getEndDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEndDate(),"MONTH") > 0){
					sql += " AND DATEPART(month,con.end_date) = "+Util.extractDateElement(filter.getEndDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEndDate(),"YEAR") > 0){
					sql += " AND DATEPART(year,con.end_date) = "+Util.extractDateElement(filter.getEndDate(),"YEAR")+" ";
				}
			}		
			
			if(filter.getCreatedBy()!=null && !filter.getCreatedBy().isEmpty()){
				sql += " AND con.created_by like '"+filter.getCreatedBy()+"' ";
			}
			if(filter.getLastUpdatedBy()!=null && !filter.getLastUpdatedBy().isEmpty()){
				sql += " AND con.updated_by like '"+filter.getLastUpdatedBy()+"' ";
			}
			
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,con.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,con.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,con.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getLastUpdatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,con.updated_date) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,con.updated_date) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,con.updated_date) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
				}
			}
			
			contestSession = getSession();
			SQLQuery query = contestSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("name", Hibernate.STRING);
			query.addScalar("minimumPurchaseAmount", Hibernate.DOUBLE);
			query.addScalar("startDate", Hibernate.TIMESTAMP);
			query.addScalar("endDate", Hibernate.TIMESTAMP);						
			query.addScalar("status", Hibernate.BOOLEAN);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("updatedBy", Hibernate.STRING);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);
			query.addScalar("contestStatus", Hibernate.STRING);
			query.addScalar("artistId", Hibernate.INTEGER);
			query.addScalar("artistName", Hibernate.STRING);
			query.addScalar("artistNameStr", Hibernate.STRING);
			contestList = query.setResultTransformer(Transformers.aliasToBean(ReferralContest.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return contestList;
	}
	

	@Override
	public List<ReferralContest> getAllActiveContests() {
		return find("FROM ReferralContest WHERE status=? and contestStatus=?",new Object[]{true,"ACTIVE"});
	}
}
