package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.CrownJewelCategoryTeams;

public class CrownJewelCategoryTeamsDAO extends HibernateDAO<Integer, CrownJewelCategoryTeams> implements com.rtw.tracker.dao.services.CrownJewelCategoryTeamsDAO{

	@Override
	public List<CrownJewelCategoryTeams> getCategoryTeamsByGrandChild(Integer grandChildId) {
		return find("FROM CrownJewelCategoryTeams where status=? AND grandChildId=?",new Object[]{"ACTIVE",grandChildId});
	}

	@Override
	public List<CrownJewelCategoryTeams> getCategoryTeamsByGrandChildandName(Integer grandChildId, String name) {
		return find("FROM CrownJewelCategoryTeams where status=? and name like ? AND grandChildId=?",new Object[]{"ACTIVE","%"+name+"%",grandChildId});
	}

	@Override
	public List<CrownJewelCategoryTeams> getCategoryTeamsByName(String name) {
		return find("FROM CrownJewelCategoryTeams where status=? AND name like ?",new Object[]{"ACTIVE","%"+name+"%"});
	}

}
