package com.rtw.tracker.dao.implementation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.rtw.tracker.datas.EventDetails;
import com.rtw.tracker.datas.EventStatus;
import com.rtw.tracker.datas.PopularEvents;

public class PopularEventsDAO extends HibernateDAO<Integer, PopularEvents> implements com.rtw.tracker.dao.services.PopularEventsDAO{


	public Collection<PopularEvents> getAllActivePopularEventsByProductId(Integer productId) throws Exception {
		return find("FROM PopularEvents WHERE status='ACTIVE' and productId=? ",new Object[]{productId});
	}
	public Integer getAllActivePopularEventsCountByProductId(Integer productId) throws Exception {

		List list = find("SELECT count(id) FROM PopularEvents WHERE status='ACTIVE' and productId=?",new Object[]{productId});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();		
	}
	
	/*public Collection<PopularEvents> getAllActiveEventsByFilter(Integer productId,String searchColumn,String searchValue) throws Exception {
		String query = "FROM PopularEvents WHERE status=? and productId=? ";
		List<Object> paramList = new ArrayList<Object>();
		
		paramList.add(EventStatus.ACTIVE);
		paramList.add(productId);
		if(searchColumn !=null && !searchColumn.isEmpty()){
			query += " AND "+searchColumn +" like ?  ";
			paramList.add("%"+searchValue+"%");
		}
		
		return find(query+" ORDER BY event_name, event_date, event_time Asc", paramList.toArray());
		
	}*/
}
