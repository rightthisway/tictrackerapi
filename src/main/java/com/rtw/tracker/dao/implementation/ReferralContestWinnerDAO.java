package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tracker.datas.ReferralContest;
import com.rtw.tracker.datas.ReferralContestWinner;

public class ReferralContestWinnerDAO extends HibernateDAO<Integer,ReferralContestWinner> implements com.rtw.tracker.dao.services.ReferralContestWinnerDAO{

	@Override
	public ReferralContestWinner getContestWinner(Integer contestId) {
		return findSingle("FROM ReferralContestWinner WHERE contestId=?",new Object[]{contestId});
	}

	@Override
	public ReferralContestWinner getContestWinnerDetails(Integer contestId) {
		Session winnerSession = null;
		ReferralContestWinner winner = null;
		try {
			String sql = "select rcw.id as id,rcp.id as participantId,rcp.contest_id as contestId,c.id as winnerCustomerId," +
					"c.cust_name+' '+c.last_name as winnerName,rc.name as contestName,rc.start_date as startDate,rc.end_date as endDate," +
					"c.email as winnerEmail,rcw.created_by as createdBy,rcw.created_On as createdOn,rcw.is_email_sent as isEmailed,rcw.city as city " +
					"from referral_contest_winner rcw with(nolock) " +
					"join referral_contest_participants rcp with(nolock) on rcw.participant_id=rcp.id " +
					"join customer c with(nolock) on rcp.customer_id=c.id " +
					"join referral_contest rc with(nolock) on rc.id=rcw.contest_id where rcp.contest_id="+contestId;
			winnerSession = getSession();
			SQLQuery query = winnerSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("participantId", Hibernate.INTEGER);
			query.addScalar("contestId", Hibernate.INTEGER);
			query.addScalar("winnerCustomerId", Hibernate.INTEGER);
			query.addScalar("winnerName", Hibernate.STRING);						
			query.addScalar("winnerEmail", Hibernate.STRING);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("createdOn", Hibernate.TIMESTAMP);
			query.addScalar("contestName", Hibernate.STRING);
			query.addScalar("startDate", Hibernate.TIMESTAMP);
			query.addScalar("endDate", Hibernate.TIMESTAMP);
			query.addScalar("isEmailed", Hibernate.BOOLEAN);
			query.addScalar("city", Hibernate.STRING);
			List<ReferralContestWinner> list = query.setResultTransformer(Transformers.aliasToBean(ReferralContestWinner.class)).list();
			if(!list.isEmpty()){
				winner=list.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return winner;
		
	}

	
}
