package com.rtw.tracker.dao.implementation;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tracker.datas.Customers;
import com.rtw.tracker.datas.Event;
import com.rtw.tracker.datas.EventStatus;

public class EventDAO extends HibernateDAO<Integer, Event> implements com.rtw.tracker.dao.services.EventDAO {

	public Event getEventById(Integer evnetId) throws Exception {
		return findSingle("FROM Event WHERE eventId=? ", new Object[]{evnetId});
	}
	
	public List<Event> getAllActiveEvents() {
		List<Event> events = null;
		try {
			events =  find("FROM Event WHERE status = ? ", new Object[]{1});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return events;
	}
	
	public List<Event> getEventByVenueId(Integer id) {
		List<Event> events = null;
		try {
			events =  find("FROM Event WHERE eventStatus = ? AND  venueId = ? ORDER BY eventName, eventDate, eventTime Asc", new Object[]{EventStatus.ACTIVE,id});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return events;
	}
	
	public List<Object[]> getEventsNotInRTF() {
		List<Object[]> events = null;
		Session session = null;
		try {
			String SQLQuery = "select EventID, EventLastUpdateDate, "+
				"ArtistName, EventName, "+
				"EventDate, EventTime, "+ 
				"Venue, VenueCity, VenueState, VenueCountry, "+ 
				"ParentCategoryName, ChildCategoryName, GrandChildCategoryName, "+ 
				"VenueCategoryGroupName, VenueMap "+		
				"from rep_EVENTS_IN_TMAT_NOTIN_RTF_report_qry_vw with(nolock) "+
				"order by EDate, ETime ";
			
			/*StringBuilder queryString = new StringBuilder();
			queryString.append("select e.id,e.last_update,a.name,e.name,CONVERT(VARCHAR(19),e.event_date,101) as EventDate,CONVERT(VARCHAR(19),e.event_time,108) as EventTime,e.event_status,v.building,v.city,v.state,v.country,t.name,c.name,gc.name, ");
			queryString.append("e.venue_category_id from   [10.0.1.51].[tmatprodscale].[dbo].[event] e inner join [10.0.1.51].[tmatprodscale].[dbo].[venue] v ");
			queryString.append("on v.id=e.venue_id inner join [10.0.1.51].[tmatprodscale].[dbo].[artist] a on a.id=e.artist_id inner join [10.0.1.51].[tmatprodscale].[dbo].[grand_child_tour_category] gc on gc.id=a.grand_child_category_id inner join ");
			queryString.append("[10.0.1.51].[tmatprodscale].[dbo].[child_tour_category] c on c.id=gc.child_category_id inner join ");
			queryString.append("[10.0.1.51].[tmatprodscale].[dbo].[tour_category] t on t.id=c.tour_category_id where e.event_status='ACTIVE' and ");
			queryString.append("e.id not in (select event_id from event_details where reward_thefan_enabled=1 and venu_map=1 and status=1) and v.country in ('US','USA','CANADA') AND ");
			queryString.append("(e.name='hotel room' or e.name like 'hotel room'+' %' or e.name like '% '+'hotel room' or e.name like '% '+'hotel room'+' %' or "); 
			queryString.append("e.name='pre-party' or e.name like 'pre-party'+' %' or e.name like '% '+'pre-party' or e.name like '% '+'pre-party'+' %' or ");
			queryString.append("e.name='party' or e.name like 'party'+' %' or e.name like '% '+'party' or e.name like '% '+'party'+' %' or ");
			queryString.append("e.name='packages' or e.name like 'packages'+' %' or e.name like '% '+'packages' or e.name like '% '+'packages'+' %' or ");
			queryString.append("e.name='parking' or e.name like 'parking'+' %' or e.name like '% '+'parking' or e.name like '% '+'parking'+' %' or ");
			queryString.append("e.name='pass' or e.name like 'pass'+' %' or e.name like '% '+'pass' or e.name like '% '+'pass'+' %' or ");
			queryString.append("e.name='passes' or e.name like 'passes'+' %' or e.name like '% '+'passes' or e.name like '% '+'passes %' or ");
			queryString.append("e.name='group pass' or e.name like 'group pass'+' %' or e.name like '% '+'group pass' or e.name like '% '+'group pass'+' %' or ");
			queryString.append("e.name='hotel' or e.name like 'hotel'+' %' or e.name like '% '+'hotel' or e.name like '% '+'hotel'+' %' or ");
			queryString.append("e.name='package' or e.name like 'package'+' %' or e.name like '% '+'package' or e.name like '% '+'package'+' %' or ");
			queryString.append("e.name='test' or e.name like 'test'+' %' or e.name like '% '+'test' or e.name like '% '+'test'+' %' or ");
			queryString.append("e.name='testing' or e.name like 'testing'+' %' or e.name like '% '+'testing' or e.name like '% '+'testing'+' %' or ");
			queryString.append("e.name='2 day' or e.name like '2 day'+' %' or e.name like '% '+'2 day' or e.name like '% '+'2 day'+' %' or ");
			queryString.append("e.name='2 days' or e.name like '2 days'+' %' or e.name like '% '+'2 days' or e.name like '% '+'2 days'+' %' or ");
			queryString.append("e.name='3 day' or e.name like '3 day'+' %' or e.name like '% '+'3 day' or e.name like '% '+'3 day'+' %' or ");
			queryString.append("e.name='3 days' or e.name like '3 days'+' %' or e.name like '% '+'3 days' or e.name like '% '+'3 days'+' %' or ");
			queryString.append("e.name='4 day' or e.name like '4 day'+' %' or e.name like '% '+'4 day' or e.name like '% '+'4 day'+' %' or ");
			queryString.append("e.name='4 days' or e.name like '4 days'+' %' or e.name like '% '+'4 days' or e.name like '% '+'4 days'+' %' or ");
			queryString.append("e.name='5 day' or e.name like '5 day'+' %' or e.name like '% '+'5 day' or e.name like '% '+'5 day'+' %' or ");
			queryString.append("e.name='5 days' or e.name like '5 days'+' %' or e.name like '% '+'5 days' or e.name like '% '+'5 days'+' %' or ");
			queryString.append("e.name='multiple day' or e.name like 'multiple day'+' %' or e.name like '% '+'multiple day' or e.name like '% '+'multiple day'+' %' or ");
			queryString.append("e.name='multiple days' or e.name like 'multiple days'+' %' or e.name like '% '+'multiple days' or e.name like '% '+'multiple days'+' %') ");*/
						
			session = getSession();
			Query query = session.createSQLQuery(SQLQuery);
			events = query.list();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return events;
	}

	/*public List<Object[]> getRTFUnsoldInventory(){
		List<Object[]> tickets = null;
		Session session = null;
		try {
			StringBuilder queryString = new StringBuilder();
			queryString.append("select distinct e.id,a.name,e.name,CONVERT(VARCHAR(19),e.event_date,101) as EventDate,CONVERT(VARCHAR(19),e.event_time,108) as EventTime,v.building,v.city,tg.section,tg.row,t.seat_number, ");
			queryString.append("tg.quantity,tg.quantity,t.retail_price,t.face_price,t.cost,t.wholesale_price,tg.internal_notes, ");
			queryString.append("t.on_hand,tg.instant_download,tg.stock_type,p.id,c.company_name from ticket t inner join ticket_group tg on t.ticket_group_id=tg.id inner join event e ");
			queryString.append("on e.id=tg.event_id inner join artist a on a.id=e.artist_id inner join venue v on v.id=e.venue_id ");
			queryString.append("inner join purchase_order p on p.id=tg.purchase_order_id inner join customer c on c.id=p.customer_id ");
			queryString.append("where (t.ticket_status = 'ACTIVE' or t.ticket_status='ONHOLD') and (t.invoice_id is null or t.invoice_id =0)");
			session = getSession();
			Query query = session.createSQLQuery(queryString.toString());
			tickets = query.list();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tickets;
	}*/
	
	public List<Object[]> getUnsoldInventoryReport(String startDate,String endDate){
		List<Object[]> tickets = null;
		Session session = null;
		try {
			String SQLQuery = "select PurchaseOrderNo, PurchaseOrderDate, EventId, EventName, EventDate, EventTime, "+ 
				"VenueName, VenueCity, VenueState, VenueCountry, Section, Row, Seat, "+
				"OriginalTicketCount, RemainingTicketCount, RetailPrice, WholesalePrice, "+ 
				"FacePrice, Cost, TotalCost, InternalNotes, ShippingMethod, "+
				"ClientBrokerCompanyName, ClientBrokerName "+
				"from rep_RTF_UnSold_Inventory_report_qry_vw with(nolock) "+
				"where EDate > '"+startDate+"' and EDate < '"+endDate+"' "+ 
				"order by EDate, ETime, PODate ";
			
			/*StringBuilder queryString = new StringBuilder();
			queryString.append("select distinct a.name as artistName,e.name as eventName,e.event_date,e.event_time, ");
			queryString.append("CONCAT(v.building,', '+v.city+', '+v.state+', '+v.country) AS venue,po.id,po.csr,tg.section,tg.row, ");
			queryString.append("COUNT(t.ticket_group_id) as qty,CONVERT(DECIMAL(10,2),t.retail_price) as retailPrice,");
			queryString.append("CONVERT(DECIMAL(10,2),t.wholesale_price) as wholesalePrice,");
			queryString.append("CONVERT(DECIMAL(10,2),(t.retail_price * COUNT(t.ticket_group_id))) as totalRetailPrice,");
			queryString.append("CONVERT(DECIMAL(10,2),(t.wholesale_price * COUNT(t.ticket_group_id))) as totalWholeSalePrice,tg.broadcast ");
			queryString.append("from ticket_group tg inner join ticket t on tg.id=t.ticket_group_id ");
			queryString.append("inner join event e on e.id=tg.event_id inner join venue v on v.id=e.venue_id inner join artist a on ");
			queryString.append("a.id=e.artist_id inner join purchase_order po on po.id=t.purchase_order_id where CAST(e.event_date AS DATETIME)+CAST(e.event_time AS DATETIME) >= GETDATE() and ");
			queryString.append("t.invoice_id is null and (e.event_date > '"+startDate+"' and e.event_date < '"+endDate+"') group by a.name,e.name, ");
			queryString.append("e.event_date,e.event_time,tg.section,tg.row,v.building,v.city,v.country,v.state,t.retail_price, ");
			queryString.append("t.wholesale_price,tg.broadcast,po.id,po.csr order by e.event_date,e.event_time");*/
			
			session = getSession();
			Query query = session.createSQLQuery(SQLQuery);
			tickets = query.list();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tickets;
	}
	
	public List<Object[]> getUnsoldCategoryTicketsReport(String startDate,String endDate){
		List<Object[]> tickets = null;
		Session session = null;
		try {
			String SQLQuery = "select EventID, EventName, EventDate, EventTime, "+
				"VenueName, VenueCity, VenueState, VenueCountry, "+
				"Section, Row, TicketQuantity, RetailPrice, WholesalePrice, "+
				"TotalRetailPrice, TotalWholeSalePrice , BroadCast "+
				"from rep_RTF_UnSold_Category_Ticket_report_qry_vw with(nolock) "+
				"where EDate > '"+startDate+"' and EDate < '"+endDate+"' "+
				"order by EDate, ETime ";
			
			/*StringBuilder queryString = new StringBuilder();
			queryString.append("select distinct a.name as artistName,e.name as eventName,e.event_date,e.event_time, ");
			queryString.append("CONCAT(v.building,', '+v.city+', '+v.state+', '+v.country) AS venue,ctg.section_range,ctg.row_range, ");
			queryString.append("COUNT(ct.category_ticket_group_id) as qty, ");
			queryString.append("CONVERT(DECIMAL(10,2),ctg.retail_price) as retailPrice,");
			queryString.append("CONVERT(DECIMAL(10,2),ctg.wholesale_price) as wholesalePrice,");
			queryString.append("CONVERT(DECIMAL(10,2),(ctg.retail_price * COUNT(ct.category_ticket_group_id))) as totalRetailPrice,");
			queryString.append("CONVERT(DECIMAL(10,2),(ctg.wholesale_price * COUNT(ct.category_ticket_group_id))) as totalWholeSalePrice,");
			queryString.append("ctg.broadcast from category_ticket_group ctg inner join category_ticket ct on ctg.id=ct.category_ticket_group_id ");
			queryString.append("inner join event e on e.id=ctg.event_id inner join venue v on v.id=e.venue_id inner join artist a on ");
			queryString.append("a.id=e.artist_id where CAST(e.event_date AS DATETIME)+CAST(e.event_time AS DATETIME) >= GETDATE() and ");
			queryString.append("ct.invoice_id is null and (e.event_date > '"+startDate+"' and e.event_date < '"+endDate+"') group by a.name,e.name, ");
			queryString.append("e.event_date,e.event_time,ctg.section_range,ctg.row_range,v.building,v.city,v.country, ");
			queryString.append("v.state,ctg.retail_price,ctg.wholesale_price,ctg.broadcast order by e.event_date,e.event_time");*/
			
			session = getSession();
			Query query = session.createSQLQuery(SQLQuery);
			tickets = query.list();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tickets;
	}
	
	public List<Object[]> getInvoiceReport(String startDate,String endDate){
		List<Object[]> tickets = null;
		Session session = null;
		try {
			String SQLQuery = "select InvoiceNo, InvoiceCreatedDate, InvoiceCreatedBy, asInvoiceLastUpdatedDate, InvoiceLastUpdatedBy, "+
				"InvoiceType, DeliveryMethod, InvoiceStatus, InternalNotes, "+
				"customerName, CustomerOrderNo, InvoiceTotal, InvoiceTicketCount, "+
				"realtix_map, is_real_tix_upload, realTixDelivered "+
				"from rep_RTF_Invoice_report_qry_vw with(nolock) "+
				"where InvCreatedDate >= '"+startDate+"' and InvCreatedDate <= '"+endDate+"' "+
				"order by InvCreatedDate ";
			
			/*StringBuilder queryString = new StringBuilder();
			queryString.append("select i.id,i.created_date,i.created_by,i.last_updated,i.last_updated_by,i.invoice_type, ");
			queryString.append("sm.name as  'Delivery Method',i.status,i.internal_note,CONCAT(c.cust_name,', ',c.last_name) as customerName,i.order_id, ");
			queryString.append("CONVERT(DECIMAL(10,2),i.invoice_total) as invoiceTotal,i.ticket_count,");
			queryString.append("i.realtix_map,i.is_real_tix_upload,(CASE WHEN i.realtix_delivered = 'true' THEN 'YES' ELSE 'NO' END) as realTixDelivered ");
			queryString.append("from invoice i inner join customer c on i.customer_id=c.id inner join ");
			queryString.append("shipping_method sm on sm.id=i.shipping_method_id where i.created_date >= '"+startDate+"' and i.created_date <= '"+endDate+"' order by i.created_date");*/
			
			session = getSession();
			Query query = session.createSQLQuery(SQLQuery);
			tickets = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tickets;
	}
	
	public List<Object[]> getPurchaseOrderReport(String startDate,String endDate){
		List<Object[]> tickets = null;
		Session session = null;
		try {
			String SQLQuery = "select PurchaseOrderNo, PurchaseOrderCreatedDate, PurchaseOrderCreatedBy, "+
				"PurchaseOrderType, DeliveryMethod, InternalNotes, "+
				"CustomerName, PurchaseOrderTotal, PurchaseOrderTicketCount, "+
				"ExternalPurchaseOrderNo "+
				"from rep_RTF_PO_report_qry_vw with(nolock) "+
				"where POCreatedDate >= '"+startDate+"' and POCreatedDate <= '"+endDate+"' "+
				"order by POCreatedDate";
			
			/*StringBuilder queryString = new StringBuilder();
			queryString.append("select p.id,p.created_time,p.created_by,p.purchase_order_type,sm.name as  'Delivery Method', ");
			queryString.append("p.internal_notes,c.cust_name,CONVERT(DECIMAL(10,2),p.po_total) as poTotal, ");
			queryString.append("p.ticket_count,p.external_po_no ");
			queryString.append("from purchase_order p inner join customer c on p.customer_id=c.id inner join ");
			queryString.append("shipping_method sm on sm.id=p.shipping_method_id where ");
			queryString.append("p.created_time >= '"+startDate+"' and p.created_time <= '"+endDate+"' order by p.created_time");*/
			
			session = getSession();
			Query query = session.createSQLQuery(SQLQuery);
			tickets = query.list();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tickets;
	}
	
	public List<Object[]> getRTFFilledReport(){
		List<Object[]> tickets = null;
		Session session = null;
		try {
			String SQLQuery = "select InvoiceNo, InvoiceCreatedDate, CustomerOrderNo, CustomerOrderCreatedDate, "+
				"PurchaseOrderNo, PurchaseOrderCreatedDate, "+
				"EventCategory, EventName, EventDate, EventTime, Venue, SectionRange, RowRange, "+ 
				"InvoiceTicketQuantity, SoldTicketQuantity, "+
				"InvoiceTotal, TicketPrice, SoldPrice, ActualSoldPrice, TicketCost, TotalTicketCost, "+ 
				"TotalSoldPrice, Fees, NetSoldPrice, ActualProfitOrLoss, GrossMargin, "+
				"CustomerName, eMail, Phone, BillingAddress, ShippingAddress "+
				"from rep_RTF_Filled_PO_report_qry_vw with(nolock) "+
				"where EDate > GETDATE() "+
				"order by InvCreatedDate, PORCreatedDate, EDate, ETime, InvoiceNo, PurchaseOrderNo ";
			
			/*StringBuilder queryString = new StringBuilder();			
			queryString.append("select distinct a.name,e.name,CONVERT(VARCHAR(19),e.event_date,101) as EventDate,CONVERT(VARCHAR(19),e.event_time,108) as EventTime, ");
			queryString.append("v.building,ctg.id,ct.invoice_id,po.id,ctg.section_range,ctg.row_range,count(ct.category_ticket_group_id) as categoryTicketGRoupId, ");
			queryString.append("CONVERT(DECIMAL(10,2),MAX(ct.actual_price)) as actual_price,");
			queryString.append("CONVERT(DECIMAL(10,2),ctg.retail_price) as retailPrice,");
			queryString.append("CONVERT(DECIMAL(10,2),ctg.wholesale_price) as wholesalePrice,");
			queryString.append("ctg.internal_notes,'RTW' as broker,i.created_date,po.created_time, ");
			queryString.append("c1.cust_name,c1.email,c1.phone,CONCAT(cod.bl_address1,', ',cod.bl_address2,', ',cod.bl_city,', ',");
			queryString.append("cod.bl_state_name,', ',cod.bl_country_name) as billingAddress,CONCAT(cod.sh_address1,', ',cod.sh_address2,");
			queryString.append("', ',cod.sh_city,', ',cod.sh_state_name,', ',cod.sh_country_name) as shippingAddress ");
			queryString.append("from category_ticket ct inner join category_ticket_group ctg on ct.category_ticket_group_id=");
			queryString.append("ctg.id inner join ticket t on t.id=ct.ticket_id inner join invoice i on i.id=ct.invoice_id inner join ");
			queryString.append("purchase_order po on po.id=t.purchase_order_id inner join event e on e.id=ctg.event_id inner join ");
			queryString.append("artist a on a.id=e.artist_id inner join venue v on v.id=e.venue_id inner join parent_category pc on ");
			queryString.append("pc.id=e.parent_category_id left outer join customer c on c.id=po.customer_id left outer join ");
			queryString.append("customer c1 on c1.id=i.customer_id left outer join customer_order co on co.category_ticket_group_id=ctg.id ");
			queryString.append("left outer join customer_order_details cod on cod.order_id=co.id ");
			queryString.append("where (ct.invoice_id IS NOT NULL AND ct.invoice_id <> 0) and ");
			queryString.append("(ct.ticket_id IS NOT NULL AND ct.ticket_id <> 0) and i.status <> 'VOIDED' and e.event_date > GETDATE() ");
			queryString.append("group by a.name,ct.category_ticket_group_id,ctg.id,ct.category_ticket_group_id, ");
			queryString.append("ct.invoice_id,ctg.section_range,ctg.row_range,ctg.retail_price,ctg.wholesale_price, ");
			queryString.append("ctg.internal_notes,e.name,e.event_date,e.event_time,v.building,i.created_date,po.created_time,po.id,");
			queryString.append("c1.cust_name,c1.email,c1.phone,cod.bl_address1,cod.bl_address2,cod.bl_city,cod.bl_country_name,");
			queryString.append("cod.bl_state_name,cod.sh_address1,cod.sh_address2,cod.sh_city,cod.sh_country_name,cod.sh_state_name");
			*/
			session = getSession();
			Query query = session.createSQLQuery(SQLQuery);
			tickets = query.list();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tickets;
	}
		
	public List<Object[]> getRTFUnFilledReport(){
		List<Object[]> tickets = null;
		Session session = null;
		try {
			String SQLQuery = "select InvoiceNo, InvoiceCreatedDate, "+
				"EventCategory, EventName, EventDate, EventTime, Venue, Section, Row, SectionRange, RowRange, "+ 
				"InvoiceTicketQuantity, SoldTicketQuantity, "+
				"InvoiceTotal, TicketPrice, SoldPrice, ActualSoldPrice, "+ 
				"TotalSoldPrice, Fees, NetSoldPrice, "+
				"CustomerName, eMail, Phone, BillingAddress, ShippingAddress "+	
				"from rep_RTF_UNFilled_report_qry_vw with(nolock) "+
				"where EDate > GETDATE() "+
				"order by InvCreatedDate, EDate, ETime ";
			
			/*StringBuilder queryString = new StringBuilder();
			queryString.append("select distinct a.name,e.name,CONVERT(VARCHAR(19),e.event_date,101) as EventDate,CONVERT(VARCHAR(19),e.event_time,108) as EventTime, ");
			queryString.append("v.building,ctg.id,ct.invoice_id,ctg.section_range,ctg.row_range,count(ct.category_ticket_group_id) as categoryTicketGRoupId, ");
			queryString.append("CONVERT(DECIMAL(10,2),MAX(ct.actual_price)) as actual_price,");
			queryString.append("CONVERT(DECIMAL(10,2),ctg.retail_price) as retailPrice,");
			queryString.append("CONVERT(DECIMAL(10,2),ctg.wholesale_price) as wholesalePrice,");
			queryString.append("ctg.internal_notes,'RTW' as broker,i.created_date, ");
			queryString.append("c1.cust_name,c1.email,c1.phone,CONCAT(cod.bl_address1,', ',cod.bl_address2,', ',cod.bl_city,', ',");
			queryString.append("cod.bl_state_name,', ',cod.bl_country_name) as billingAddress,CONCAT(cod.sh_address1,', ',cod.sh_address2,");
			queryString.append("', ',cod.sh_city,', ',cod.sh_state_name,', ',cod.sh_country_name) as shippingAddress ");
			queryString.append("from category_ticket ct inner join category_ticket_group ctg on ct.category_ticket_group_id= ");
			queryString.append("ctg.id inner join invoice i on i.id=ct.invoice_id inner join event e on e.id=ctg.event_id inner join ");
			queryString.append("artist a on a.id=e.artist_id inner join venue v on v.id=e.venue_id left outer join ");
			queryString.append("customer c1 on c1.id=i.customer_id left outer join customer_order co on co.category_ticket_group_id=ctg.id ");
			queryString.append("left outer join customer_order_details cod on cod.order_id=co.id ");
			queryString.append("where (ctg.invoice_id IS NOT NULL AND ctg.invoice_id <> 0) and ");
			queryString.append("(ct.ticket_id IS NULL OR ct.ticket_id = 0) and i.status <> 'VOIDED' and e.event_date > GETDATE() ");
			queryString.append("group by a.name,ct.category_ticket_group_id,ctg.id,ct.category_ticket_group_id, ");
			queryString.append("ct.invoice_id,ctg.section_range,ctg.row_range,ctg.retail_price,ctg.wholesale_price, ");
			queryString.append("ctg.internal_notes,e.name,e.event_date,e.event_time,v.building,i.created_date,");
			queryString.append("c1.cust_name,c1.email,c1.phone,cod.bl_address1,cod.bl_address2,cod.bl_city,cod.bl_country_name,");
			queryString.append("cod.bl_state_name,cod.sh_address1,cod.sh_address2,cod.sh_city,cod.sh_country_name,cod.sh_state_name");*/
			
			session = getSession();			
			Query query = session.createSQLQuery(SQLQuery);
			tickets = query.list();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tickets;
	}
	
	public List<Object[]> getNext15DaysUnsentOrders(){
		List<Object[]> tickets = null;
		Session session = null;
		try {
			String SQLQuery = "select EventID, EventName, EventDate, EventTime, "+
				"VenueName, VenueCity, VenueState, VenueCountry, "+
				"Section, Row, TicketQuantity, "+
				"InvoiceNo, InvoiceCreatedBy, InvoiceCreatedDate, "+ 
				"PurchaseOrderNo, PurchaseOrderCreatedDate, ShippingType, "+ 
				"VendorCompany, VendorEMail, "+
				"CustomerName, CustomerEmail "+ 
				"from rep_RTF_Next_FifteenDays_UnSent_report_qry_vw with(nolock) "+
				"order by EDate, ETime, InvCreatedDate, POCreatedDate ";
			
			/*StringBuilder queryString = new StringBuilder();
			queryString.append("select distinct co.event_name,CONVERT(VARCHAR(19),co.event_date,101) as EventDate,CONVERT(VARCHAR(19),co.event_time,108) as EventTime,co.venue_name,co.venue_city,co.venue_state,co.venue_country,");
			queryString.append("co.section,co.row,co.quantity,t.on_hand,ctg.shipping_method,t.invoice_id,i.created_by,i.created_date,");
			queryString.append("t.purchase_order_id,po.created_time,c.company_name,c.email from customer_order co inner join category_ticket_group ");
			queryString.append("ctg on ctg.id=co.category_ticket_group_id inner join category_ticket ct on ct.category_ticket_group_id=ctg.id ");
			queryString.append("inner join ticket t on t.id=ct.ticket_id inner join purchase_order po on po.id=t.purchase_order_id inner join ");
			queryString.append("invoice i on i.id=t.invoice_id left outer join customer c on c.id=po.pos_buyer_broker_customer_id  where i.realTix_map='Yes' AND i.is_real_tix_upload='Yes' and  ");
			queryString.append("i.status='Completed' AND (i.id in(select invoice_id from customer_ticket_downloads) OR i.upload_to_exchange=1)");*/
			
			session = getSession();
			Query query = session.createSQLQuery(SQLQuery);
			tickets = query.list();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tickets;
	}
	
	public List<Object[]> getUnFilledShortsWithPossibleLongInventory(){
		List<Object[]> tickets = null;
		Session session = null;
		try {
			String SQLQuery = "select EventId, EventName, EventDate, EventTime, ArtistName, VenueName, "+
					"ShortCategoryTicketGroupId, ShortInvoiceNo,  ShortSection, "+ 
					"ShortRow, ShortTicketQuantity, ShortSoldPrice, "+
					"LongTicketGroupId, LongPurchaseOrderNo, LongSection, Longrow, "+ 
					"LongTicketQuantity, LongRetailPrice, LongWholesalePrice, "+ 
					"LongFacePrice, LongCost "+ 
					"from rep_RTF_UnFilled_Shorts_With_Possible_Long_Inventory_Reports_qry_vw with(nolock) "+
					"order by EDate, ETime ";
			
			/*StringBuilder queryString = new StringBuilder();
			queryString.append("select e.id as eventId,a.name as artistName,e.name as eventName,CONVERT(VARCHAR(19),e.event_date,101) as EventDate,CONVERT(VARCHAR(19),e.event_time,108) ");
			queryString.append("as EventTime,v.building,ctg.id as categoryTicketGroupId,ctg.section_range,ctg.row_range,count(ct.category_ticket_group_id) as ordered_qty, ");
			queryString.append("ctg.sold_price,j.id as ticketGroupId,j.section,j.row,j.long_qty,");
			queryString.append("CONVERT(DECIMAL(10,2),MAX(j.retail_price)) as retailPrice,");
			queryString.append("CONVERT(DECIMAL(10,2),j.wholesale_price) as wholesalePrice,");
			queryString.append("CONVERT(DECIMAL(10,2),j.face_price) as facePrice,");
			queryString.append("CONVERT(DECIMAL(10,2),j.cost) as cost ");
			queryString.append("from category_ticket ct inner join category_ticket_group ctg on ct.category_ticket_group_id= ");
			queryString.append("ctg.id inner join invoice i on i.id=ct.invoice_id inner join event e on e.id=ctg.event_id inner join ");
			queryString.append("artist a on a.id=e.artist_id inner join venue v on v.id=e.venue_id left outer join ");
			queryString.append("customer c1 on c1.id=i.customer_id left outer join customer_address ca on ca.customer_id=c1.id ");
			queryString.append("left outer join state s on s.id=ca.state left outer join country co on co.id=ca.country right outer join ");
			queryString.append("(select tg.id,tg.event_id,count(t.ticket_group_id) as long_qty,tg.section,tg.row,t.retail_price, ");
			queryString.append("t.wholesale_price,t.face_price,t.cost from ticket_group tg inner join ticket t on ");
			queryString.append("t.ticket_group_id=tg.id where (t.invoice_id is null or t.invoice_id = 0) group by tg.event_id, ");
			queryString.append("t.ticket_group_id,tg.section,tg.row,t.retail_price,t.wholesale_price,t.face_price,t.cost,tg.id) as j ");
			queryString.append("on j.event_id=e.id where (ctg.invoice_id IS NOT NULL AND ctg.invoice_id <> 0) and ");
			queryString.append("(ct.ticket_id IS NULL OR ct.ticket_id = 0) and i.status <> 'VOIDED' and e.event_date > GETDATE() ");
			queryString.append("group by a.name,e.name,e.event_date,e.event_time,v.building,ctg.section_range,ctg.row_range, ");
			queryString.append("ct.category_ticket_group_id,j.section,j.row,j.long_qty,ct.sold_price,ctg.sold_price,j.id, ");
			queryString.append("j.retail_price,j.wholesale_price,j.face_price,j.cost,e.id,ctg.id order by e.id,e.event_date,e.event_time asc");*/
			
			session = getSession();
			Query query = session.createSQLQuery(SQLQuery);
			tickets = query.list();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tickets;
	}
	
	public List<Object[]> getEventsStatistics() {
		List<Object[]> eventsStistics = null;
		Session session = null;
		try {
			StringBuilder queryString = new StringBuilder();
			queryString.append("select count(parent_category_id),parent_category_id from event with(nolock) ");
			queryString.append("where reward_thefan_enabled=1 and status=1 group by parent_category_id order by parent_category_id");
			session = getSession();
			Query query = session.createSQLQuery(queryString.toString());
			eventsStistics = query.list();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return eventsStistics;
	}
	
	public List<Object[]> getRTFSalesReportInTickTracker(){
		List<Object[]> tickets = null;
		Session session = null;
		try {
			StringBuilder queryString = new StringBuilder();
			queryString.append("select i.id as InvoiceNo,co.id as OrderNo,co.created_date,co.event_name as EventName, ");
			queryString.append("CONVERT(VARCHAR(19),co.event_date,101) as EventDate, ");
			queryString.append("CASE WHEN co.event_time is not null THEN ");
			queryString.append("LTRIM(RIGHT(CONVERT(VARCHAR(20), co.event_time, 100), 7)) "); 
			queryString.append("WHEN co.event_time is null THEN 'TBD' END as EventTime, ");
			queryString.append("co.venue_name as VenueName,co.venue_city as VenueCity,co.section as Zone,co.quantity as Quantity, ");
			queryString.append("co.actual_section as Section,co.row as Row,co.seat_details as Seat,CONCAT(c.cust_name,' ',c.last_name) as CustomerName , c.email Email, ");
			queryString.append("co.order_total as OrderTotal,co.order_type as OrderType,co.platform as Platform ");
			queryString.append("from customer_order co with(nolock) inner join customer c with(nolock) on c.id=co.customer_id and (c.is_bounce <> 1  and  c.is_unsub <> 1 ) ");
			queryString.append("inner join invoice i with(nolock) on i.order_id = co.id  where co.platform  in ('TICK_TRACKER') ");
			queryString.append("order by i.id asc");
			
			session = getSession();
			Query query = session.createSQLQuery(queryString.toString());
			tickets = query.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return tickets;
	}
	
	public List<Object[]> getRTFSalesReportNotInTickTracker(){
		List<Object[]> tickets = null;
		Session session = null;
		try {
			StringBuilder queryString = new StringBuilder();
			queryString.append("select i.id as InvoiceNo,co.id as OrderNo,co.created_date,co.event_name as EventName, ");
			queryString.append("CONVERT(VARCHAR(19),co.event_date,101) as EventDate, ");
			queryString.append("CASE WHEN co.event_time is not null THEN ");
			queryString.append("LTRIM(RIGHT(CONVERT(VARCHAR(20), co.event_time, 100), 7)) "); 
			queryString.append("WHEN co.event_time is null THEN 'TBD' END as EventTime, ");
			queryString.append("co.venue_name as VenueName,co.venue_city as VenueCity,co.section as Zone,co.quantity as Quantity, ");
			queryString.append("co.actual_section as Section,co.row as Row,co.seat_details as Seat,CONCAT(c.cust_name,' ',c.last_name) as CustomerName , c.email Email, ");
			queryString.append("co.order_total as OrderTotal,co.order_type as OrderType,co.platform as Platform ");
			queryString.append("from customer_order co with(nolock) inner join customer c with(nolock) on c.id=co.customer_id and (c.is_bounce <> 1  and  c.is_unsub <> 1 ) ");
			queryString.append("inner join invoice i with(nolock) on i.order_id = co.id  where co.platform NOT in ('TICK_TRACKER') ");
			queryString.append("order by i.id asc");
			
			session = getSession();
			Query query = session.createSQLQuery(queryString.toString());
			tickets = query.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return tickets;
	}
	
	public List<Object[]> getRTFSalesReport(){
		List<Object[]> tickets = null;
		Session session = null;
		try {
			String SQLQuery = "select InvoiceNo, InvoiceCreatedDate, InvoiceStatus, "+
				"CustomerOrderNo, CustomerOrderCreatedDate, "+
				"PurchaseOrderNo, PurchaseOrderCreatedDate, "+
				"EventName, EventDate, EventTime, "+
				"VenueName, VenueCity, Zone, "+ 
				"Quantity, Section, Row, Seat, "+
				"CustomerName, CustomerEmail, "+ 
				"ListingPrice, PromotionalCodeDiscAmount, LoyalfanDiscountAmt, "+
				"OrderTotal, Cost, NetTotal, ProfitLoss, "+
				"OrderType, Platform, PaymentMethod, "+
				"PromotionOfferUsed, promoCode, promotionalDiscount, LoyalFanDiscount, "+
				"ReferralCode, ReferralEmail,DiscountAmount,PercentageDiscountAmount "+ 
				"from rep_RTF_Sales_report_qry_vw with(nolock) "+
				"order by InvCreatedDate, COCreatedDate, POCreatedDate ";
			
			/*StringBuilder queryString = new StringBuilder();
			queryString.append("select invoice_id,ticket_group_id,sum(cost) as TicketCost,purchase_order_id into #tempinvoicecost from ticket ");
			queryString.append("where invoice_id is not null  group by invoice_id,ticket_group_id,purchase_order_id; ");
			queryString.append("select i.id as InvoiceNo,i.status as InvoiceStatus,co.id as OrderNo,co.created_date,ic.purchase_order_id,p.created_time, co.event_name as EventName,CONVERT(VARCHAR(19), ");
			queryString.append("co.event_date,101) as EventDate, CASE WHEN co.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), co.event_time, 100), 7))  ");
			queryString.append("WHEN co.event_time is null THEN 'TBD' END as EventTime, co.venue_name as VenueName,co.venue_city as VenueCity,co.section as Zone,co.quantity as Quantity, ");
			queryString.append("co.actual_section as Section, co.row as Row,co.seat_details as Seat,CONCAT(c.cust_name,' ',c.last_name) as CustomerName ,c.email Email, ");
			queryString.append("co.original_order_total as listingPrice,pro.additional_discount_amount as promotionalCodeDiscAmount, IIF(co.order_type='LOYALFAN', "); 
			queryString.append("co.discount_amt-isnull(pro.additional_discount_amount,0),NULL) as loyalfanDiscountAmt, co.order_total as OrderTotal,  ");
			queryString.append("CASE WHEN ic.TicketCost is not null THEN ic.TicketCost else 0 END as Cost, round((co.order_total - co.order_total*3/100),2) as NetTotal, ");
			queryString.append("case when ic.TicketCost is null then 0 else round(((co.order_total - co.order_total*3/100) - ");
			queryString.append("CASE WHEN ic.TicketCost is not null THEN ic.TicketCost else 0 END),2) end as ProfitLoss, co.order_type as OrderType,co.platform as Platform, ");
			queryString.append("IIF(co.primary_payment_method is null or co.primary_payment_method='NULL',' ',co.primary_payment_method)+IIF(co.secondary_payment_method is null or ");
			queryString.append("co.secondary_payment_method='NULL',' ',','+co.secondary_payment_method)+IIF(co.third_payment_method is null or ");
			queryString.append("co.third_payment_method='NULL',' ',','+co.third_payment_method) As paymentMethod, ");
			queryString.append("case when pro.id is not null and (pro.offer_type='NORMAL_PROMO' or pro.offer_type='CUSTOMER_PROMO') then 'Yes' else 'No' end as PromotionOfferUsed, ");
			queryString.append("IIF(pro.offer_type='NORMAL_PROMO' or pro.offer_type='CUSTOMER_PROMO',pro.promo_code,'') as promoCode, ");
			queryString.append("CASE WHEN (pro.offer_type='NORMAL_PROMO' or pro.offer_type='CUSTOMER_PROMO') AND pro.is_flat_discount=0 THEN replace(convert(varchar(10),pro.additional_discount_conv*100),'.00','')+'%' ");
			queryString.append("ELSE CASE WHEN (pro.offer_type='NORMAL_PROMO' or pro.offer_type='CUSTOMER_PROMO') AND pro.is_flat_discount=1 THEN convert(varchar(10),pro.additional_discount_conv) ELSE '' END END AS promotionalDiscount, ");	
			queryString.append("IIF(co.order_type='LOYALFAN','10%','') as loyalFanDiscount, ");
			queryString.append("IIF(pro.offer_type='CUSTOMER_REFERAL_DISCOUNT_CODE',pro.promo_code,'') AS referralCode, ");
			queryString.append("IIF(pro.offer_type='CUSTOMER_REFERAL_DISCOUNT_CODE',cust.email,'') AS referralEmail from customer_order co ");
			queryString.append("inner join customer c on c.id=co.customer_id and (c.is_bounce <> 1  and  c.is_unsub <> 1 and c.is_test_account <> 1 )  ");
			queryString.append("inner join invoice i on i.order_id = co.id  ");
			queryString.append("left join rtf_promotional_order_tracking_new pro on pro.order_id=co.id and pro.status='COMPLETED' ");
			queryString.append("left join customer cust on pro.referrer_cust_id=cust.id ");
			queryString.append("left join #tempinvoicecost ic on ic.invoice_id = i.id left join purchase_order p  on ic.purchase_order_id = p.id ");
			queryString.append("order by i.id asc ");*/
			
			session = getSession();
			Query query = session.createSQLQuery(SQLQuery);
			tickets = query.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return tickets;
	}
	
	public List<Object[]> getRTFCustomerOrderForWebOrdersReport(){
		List<Object[]> customerOrders = null;
		Session session = null;
		try {
			String queryString = "select a.customerId, a.customerName, a.email, sum(a.totalOrders) as TotalNoOfOrders, "+
				"sum(a.loyalFanOrders) as LoyalFanOrders,sum(a.regularOrder) as RegularOrders from "+ 
				"( select co.customer_id as customerId, concat(c.cust_name,' ',c.last_name) as customerName, c.email as email, "+
				"count(co.order_type) as totalOrders, count(co.order_type) as loyalFanOrders, 0 as regularOrder "+
				"from customer c with(nolock) left join customer_order co with(nolock) on c.id= co.customer_id and co.order_type='LOYALFAN' and co.platform not like 'TICK_TRACKER' "+
				"where c.is_test_account =0 and c.product_type='REWARDTHEFAN' group by co.customer_id,c.cust_name,c.last_name,c.email "+
				"union all "+
				"select co.customer_id as customerId, concat(c.cust_name,' ',c.last_name) as customerName, c.email as email, "+ 
				"count(co.order_type) as totalOrders, 0 as loyalFanOrders, count(co.order_type) as regularOrder "+
				"from customer c with(nolock) left join customer_order co with(nolock) on c.id= co.customer_id and co.order_type='REGULAR' and co.platform not like 'TICK_TRACKER' "+
				"where c.is_test_account =0 and c.product_type='REWARDTHEFAN' group by co.customer_id,c.cust_name,c.last_name,c.email) a group by a.customerId,a.customerName,a.email order by TotalNoOfOrders desc ";
							
			session = getSession();
			Query query = session.createSQLQuery(queryString);
			customerOrders = query.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return customerOrders;
	}
	
	
	public List<Object[]> getRTFCustomerOrderForPhoneOrdersReport(){
		List<Object[]> customerOrders = null;
		Session session = null;
		try {
			String queryString = "select a.customerId, a.customerName, a.email, sum(a.totalOrders) as TotalNoOfOrders, "+
				"sum(a.loyalFanOrders) as LoyalFanOrders,sum(a.regularOrder) as RegularOrders from "+ 
				"( select co.customer_id as customerId, concat(c.cust_name,' ',c.last_name) as customerName, c.email as email, "+
				"count(co.order_type) as totalOrders, count(co.order_type) as loyalFanOrders, 0 as regularOrder "+
				"from customer_order co with(nolock) inner join customer c with(nolock) on c.id= co.customer_id and co.order_type='LOYALFAN' and co.platform like 'TICK_TRACKER' "+
				"where c.is_test_account =0 group by co.customer_id,c.cust_name,c.last_name,c.email "+
				"union all "+
				"select co.customer_id as customerId, concat(c.cust_name,' ',c.last_name) as customerName, c.email as email, "+ 
				"count(co.order_type) as totalOrders, 0 as loyalFanOrders, count(co.order_type) as regularOrder "+
				"from customer_order co with(nolock) inner join customer c with(nolock) on c.id= co.customer_id and co.order_type='REGULAR' and co.platform like 'TICK_TRACKER' "+
				"where c.is_test_account =0 group by co.customer_id,c.cust_name,c.last_name,c.email) a group by a.customerId,a.customerName,a.email order by TotalNoOfOrders desc ";
							
			session = getSession();
			Query query = session.createSQLQuery(queryString);
			customerOrders = query.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return customerOrders;
	}
	
	
	public List<Object[]> getRTFCustomerOrdersDuplicationsReport(){
		List<Object[]> repeatedCustomers = null;
		Session session = null;
		try {
			String queryString = "select noOfUsers, noOfOrders, (noOfUsers*100.0 / totalRTFUsers ) as '% of Users' "+
				"from ( select count(1) as noOfUsers, noOfOrders, totalRTFUsers "+
				"from ( select c.id as users,  count(co.customer_id) as noOfOrders, rtfC.totalRTFUsers "+
                "from customer c with(nolock) "+
	            "left join customer_order co with(nolock) on c.id = co.customer_id and co.status = 'ACTIVE' "+
	            "left join invoice i with(nolock) on co.id = i.order_id and i.status <> 'VOIDED' "+
	            "outer apply ( select count(1) as totalRTFUsers from customer with(nolock) where product_type ='REWARDTHEFAN'  and is_test_account = 0) rtfC "+
	            "where c.product_type ='REWARDTHEFAN'  and c.is_test_account = 0 "+ 
	            "group by c.id, rtfC.totalRTFUsers "+
	            ") qry "+
	            "group by qry.noOfOrders, qry.totalRTFUsers"+
	            ") mQry "+
	            "order by mQry.noOfOrders desc ";
			
			session = getSession();
			Query query = session.createSQLQuery(queryString);
			repeatedCustomers = query.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return repeatedCustomers;
	}
	
	public List<Object[]> getRTFChannelWiseCustomerOrdersReport(){
		List<Object[]> repeatedCustomers = null;
		Session session = null;
		try {
			/*String queryString = "select noOfUsers, noOfOrders, Channel, (noOfUsers*100.0 / totalRTFUsers ) as '% of Users' "+
				"from ( select count(1) as noOfUsers, noOfOrders, platform as Channel, totalRTFUsers "+
				"from ( select c.id as users,  count(co.customer_id) as noOfOrders, co.platform, rtfC.totalRTFUsers "+
                "from customer c "+
	            "left join customer_order co on c.id = co.customer_id and co.status = 'ACTIVE' "+
	            "left join invoice i on co.id = i.order_id and i.status != 'VOIDED' "+
	            "outer apply ( select count(1) as totalRTFUsers from customer where product_type ='REWARDTHEFAN'  and is_test_account = 0) rtfC "+
	            "where c.product_type ='REWARDTHEFAN'  and c.is_test_account = 0 "+ 
	            "group by c.id, co.platform, rtfC.totalRTFUsers "+
	            ") qry "+
	            "group by qry.noOfOrders, qry.platform, qry.totalRTFUsers "+
	            ") mQry "+
	            "order by mQry.noOfOrders desc ";*/
			String queryString ="select noOfUsers, noOfOrders, Channel, (noOfUsers*100.0 / totalRTFUsers ) as '% of Users' " +
					"from ( select count(users) as noOfUsers, sum(noOfOrders) as noOfOrders, platform as Channel, totalRTFUsers " +
					"from ( select c.id as users,  count(co.customer_id) as noOfOrders, co.platform, rtfC.totalRTFUsers " +
					"from customer c with(nolock) " +
					"left join customer_order co with(nolock) on c.id = co.customer_id and co.status = 'ACTIVE' " +
					"left join invoice i with(nolock) on co.id = i.order_id and i.status != 'VOIDED' " +
					"outer apply ( select count(1) as totalRTFUsers from customer with(nolock) " +
					"where product_type ='REWARDTHEFAN'  and is_test_account = 0) rtfC " +
					"where c.product_type ='REWARDTHEFAN'  and c.is_test_account = 0 " +
					"group by c.id, co.platform, rtfC.totalRTFUsers " +
					") qry " +
					"group by qry.platform, qry.totalRTFUsers " +
					") mQry " +
					"order by mQry.noOfOrders desc";				
			session = getSession();
			Query query = session.createSQLQuery(queryString);
			repeatedCustomers = query.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return repeatedCustomers;
	}
	
	
	@Override
	public Collection<Event> filterByName(String pattern) {
		return find("FROM Event WHERE status=? AND eventName LIKE ? and eventDate > ? ORDER BY eventName, eventDate, eventTime",new Object[]{"%"+pattern+"%",new Date()});
	}	
	
	
	public List<Customers> getClientMasterReport(){
		List<Customers> clientMasters = null;
		Session session = null;
		try {
			String queryString = "select distinct c.id as customerId, c.cust_name as firstName, c.last_name as lastName, c.email as customerEmail, "+
				"c.company_name as companyName, c.product_type as productType, ca.address_line1 as addressLine1, ca.address_line2 as addressLine2, "+
				"ca.city as city, s.name as state, co.country_name as country, ca.zip_code as zipCode, ca.phone1 as phone "+
				"from customer c with(nolock) "+ 
				"left join customer_address ca with(nolock) on c.id=ca.customer_id and ca.address_type='BILLING_ADDRESS' "+
				"and ca.id in(select max(id) from customer_address cusAdd with(nolock) where cusAdd.customer_id=c.id and cusAdd.address_type='BILLING_ADDRESS') "+
				"left join country co with(nolock) on ca.country=co.id "+
				"left join state s with(nolock) on s.id=ca.state "+
				"where c.is_test_account=0 and c.is_bounce=0 and c.is_unsub=0 "+
				"order by c.id desc ";
			
			session = getSession();
			Query query = session.createSQLQuery(queryString);
			clientMasters = query.setResultTransformer(Transformers.aliasToBean(Customers.class)).list(); //query.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return clientMasters;
	}
	
	public List<Customers> getUnsubscribedClientMasterReport(){
		List<Customers> unsubscribedClientMasters = null;
		Session session = null;
		try {
			String queryString = "select distinct c.id as customerId, c.cust_name as firstName, c.last_name as lastName, c.email as customerEmail, "+
				"c.company_name as companyName, c.product_type as productType, ca.address_line1 as addressLine1, ca.address_line2 as addressLine2, "+
				"ca.city as city, s.name as state, co.country_name as country, ca.zip_code as zipCode, ca.phone1 as phone "+
				"from customer c with(nolock) "+ 
				"inner join unsubscribed_email ue with(nolock) on ue.email=c.email "+
				"left join customer_address ca with(nolock) on c.id=ca.customer_id and ca.address_type='BILLING_ADDRESS' "+
				"and ca.id in(select max(id) from customer_address cusAdd with(nolock) where cusAdd.customer_id=c.id and cusAdd.address_type='BILLING_ADDRESS') "+
				"left join country co with(nolock) on ca.country=co.id "+
				"left join state s with(nolock) on s.id=ca.state "+
				"where c.is_test_account=0 "+
				"order by c.id desc ";
							
			session = getSession();
			Query query = session.createSQLQuery(queryString);
			unsubscribedClientMasters = query.setResultTransformer(Transformers.aliasToBean(Customers.class)).list();  //query.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return unsubscribedClientMasters;
	}
	
	public List<Object[]> getPurchasedMasterReport(){
		List<Object[]> purchasedMasters = null;
		Session session = null;
		try {			
			String queryString = "select distinct c.id,c.cust_name,c.last_name,c.email,c.company_name, "+
				"c.product_type,ca.address_line1,ca.address_line2,ca.city,s.name,co.country_name,ca.zip_code,ca.phone1 "+
				"from customer c with(nolock) "+ 
				"inner join customer_order o with(nolock) on c.id=o.customer_id "+
				"left join customer_address ca with(nolock) on c.id=ca.customer_id and ca.address_type='BILLING_ADDRESS' "+
				"and ca.id in(select max(id) from customer_address cusAdd with(nolock) where cusAdd.customer_id=c.id and cusAdd.address_type='BILLING_ADDRESS') "+
				"left join country co with(nolock) on ca.country=co.id "+
				"left join state s with(nolock) on s.id=ca.state "+
				"left join invoice i with(nolock) on c.id = i.customer_id "+
				"where i.status <> 'VOIDED' "+
				"and c.is_test_account=0 and c.is_unsub = 0 "+
				"order by c.id desc ";
							
			session = getSession();
			Query query = session.createSQLQuery(queryString);
			purchasedMasters = query.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return purchasedMasters;
	}
	
	public List<Object[]> getRTFRegisteredButnotPurchasedReport(){
		List<Object[]> registerButNotPurchased = null;
		Session session = null;
		try {	
			String queryString = "select distinct c.id,c.cust_name,c.last_name,c.email,c.company_name, "+
				"c.product_type,ca.address_line1,ca.address_line2,ca.city,s.name,co.country_name,ca.zip_code,ca.phone1 "+
				"from customer c with(nolock) "+ 
				"left join invoice i with(nolock) on c.id=i.customer_id "+
				"left join customer_address ca with(nolock) on c.id=ca.customer_id and ca.address_type='BILLING_ADDRESS' "+
				"and ca.id in(select max(id) from customer_address cusAdd with(nolock) where cusAdd.customer_id=c.id and cusAdd.address_type='BILLING_ADDRESS') "+
				"left join country co with(nolock) on ca.country=co.id "+
				"left join state s with(nolock) on s.id=ca.state "+
				"where i.id is null and c.product_type='REWARDTHEFAN' and c.is_test_account=0 and c.is_unsub=0 "+
				"order by c.id desc ";
							
			session = getSession();
			Query query = session.createSQLQuery(queryString);
			registerButNotPurchased = query.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return registerButNotPurchased;
	}
	
	public List<Object[]> getRTFCombinedPurchasedNonPurchasedReport(){
		List<Object[]> purchasedNonPurchased = null;
		Session session = null;
		try {
			String queryString = "select distinct c.id,c.cust_name,c.last_name,c.email,c.company_name, "+
				"c.product_type,ca.address_line1,ca.address_line2,ca.city,s.name,co.country_name,ca.zip_code,ca.phone1 from customer c with(nolock) "+ 
				"left join customer_address ca with(nolock) on c.id=ca.customer_id and ca.address_type='BILLING_ADDRESS' "+
				"and ca.id in(select max(id) from customer_address cusAdd with(nolock) where cusAdd.customer_id=c.id and cusAdd.address_type='BILLING_ADDRESS') "+
				"left join country co with(nolock) on ca.country=co.id "+
				"left join state s with(nolock) on s.id=ca.state "+
				"where c.product_type='REWARDTHEFAN' AND c.is_test_account=0 and c.is_bounce=0 and c.is_unsub=0 order by c.id desc ";
							
			session = getSession();
			Query query = session.createSQLQuery(queryString);
			purchasedNonPurchased = query.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return purchasedNonPurchased;
	}
	
	public List<Object[]> getRTFLoyalFanReport(){
		List<Object[]> loyalFans = null;
		Session session = null;
		try {
			String queryString = "select distinct c.id, c.cust_name, c.last_name, c.email, c.company_name, "+
				"c.product_type, ca.address_line1, ca.address_line2, ca.city, s.name, co.country_name, "+
				"ca.zip_code, ca.phone1, lf.loyal_fan_type, lf.loyal_name, "+
				"CONVERT(VARCHAR(10), lf.start_date, 101) + ' ' +CONVERT(VARCHAR(10), lf.start_date, 108) as startDate, "+
				"CONVERT(VARCHAR(10), lf.end_date, 101) + ' ' +CONVERT(VARCHAR(10), lf.end_date, 108) as endDate, "+
				"lf.Ticket_purchased "+
				"from customer c with(nolock) "+ 
				"inner join customer_loyal_fan lf with(nolock) on lf.customer_id=c.id "+
				"left join customer_address ca with(nolock) on c.id=ca.customer_id and ca.address_type='BILLING_ADDRESS' "+
				"and ca.id in(select max(id) from customer_address cusAdd with(nolock) where cusAdd.customer_id=c.id and cusAdd.address_type='BILLING_ADDRESS') "+
				"left join country co with(nolock) on ca.country=co.id "+
				"left join state s with(nolock) on s.id=ca.state "+
				"where c.product_type='REWARDTHEFAN' AND c.is_test_account=0 and c.is_bounce=0 and c.is_unsub=0 "+
				"order by c.id desc ";
							
			session = getSession();
			Query query = session.createSQLQuery(queryString);
			loyalFans = query.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return loyalFans;
	}
	
	public List<Object[]> getRTFLoyalFanPurchasedReport(){
		List<Object[]> loyalFansPurchased = null;
		Session session = null;
		try {
			String queryString = "select distinct c.id, c.cust_name, c.last_name, c.email, c.company_name, "+
				"c.product_type, ca.address_line1, ca.address_line2, ca.city, s.name, co.country_name, "+
				"ca.zip_code, ca.phone1, lf.loyal_fan_type, lf.loyal_name, "+
				"CONVERT(VARCHAR(10), lf.start_date, 101) + ' ' +CONVERT(VARCHAR(10), lf.start_date, 108) as startDate, "+
				"CONVERT(VARCHAR(10), lf.end_date, 101) + ' ' +CONVERT(VARCHAR(10), lf.end_date, 108) as endDate "+
				"from customer c with(nolock) "+ 
				"inner join customer_loyal_fan lf with(nolock) on lf.customer_id=c.id "+
				"left join customer_address ca with(nolock) on c.id=ca.customer_id and ca.address_type='BILLING_ADDRESS' "+
				"and ca.id in(select max(id) from customer_address cusAdd with(nolock) where cusAdd.customer_id=c.id and cusAdd.address_type='BILLING_ADDRESS') "+
				"left join country co with(nolock) on ca.country=co.id "+
				"left join state s with(nolock) on s.id=ca.state "+
				"where c.product_type='REWARDTHEFAN' AND c.is_test_account=0 and c.is_bounce=0 and c.is_unsub=0 and lf.Ticket_purchased = 1 "+
				"order by c.id desc ";
							
			session = getSession();
			Query query = session.createSQLQuery(queryString);
			loyalFansPurchased = query.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return loyalFansPurchased;
	}
	
	public List<Object[]> getRTFLoyalFanNonPurchasedReport(){
		List<Object[]> loyalFansNotPurchased = null;
		Session session = null;
		try {
			String queryString = "select distinct c.id,c.cust_name,c.last_name,c.email,c.company_name, "+
				"c.product_type,ca.address_line1,ca.address_line2,ca.city,s.name,co.country_name,ca.zip_code,ca.phone1, "+
				"lf.loyal_fan_type,lf.loyal_name, "+
				"CONVERT(VARCHAR(10), lf.start_date, 101) + ' ' +CONVERT(VARCHAR(10), lf.start_date, 108) as startDate, "+
				"CONVERT(VARCHAR(10), lf.end_date, 101) + ' ' +CONVERT(VARCHAR(10), lf.end_date, 108) as endDate "+				
				"from customer c with(nolock) "+ 
				"inner join customer_loyal_fan lf with(nolock) on lf.customer_id=c.id "+
				"left join customer_address ca with(nolock) on c.id=ca.customer_id and ca.address_type='BILLING_ADDRESS' "+
				"and ca.id in(select max(id) from customer_address cusAdd with(nolock) where cusAdd.customer_id=c.id and cusAdd.address_type='BILLING_ADDRESS') "+
				"left join country co with(nolock) on ca.country=co.id "+
				"left join state s with(nolock) on s.id=ca.state "+
				"where c.product_type='REWARDTHEFAN' AND c.is_test_account=0 and c.is_bounce=0 and c.is_unsub=0 and lf.Ticket_purchased = 0 "+
				"order by c.id desc ";
							
			session = getSession();
			Query query = session.createSQLQuery(queryString);
			loyalFansNotPurchased = query.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return loyalFansNotPurchased;
	}
	
	public List<Object[]> getRTFFilledReportByInvoice(){
		List<Object[]> tickets = null;
		Session session = null;
		try {
			String SQLQuery = "select InvoiceNo, InvoiceCreatedDate, CustomerOrderNo, CustomerOrderCreatedDate, "+
				"PurchaseOrderNo, PurchaseOrderCreatedDate, "+
				"EventCategory, EventName, EventDate, EventTime, Venue, SectionRange, "+ 
				"InvoiceTicketQuantity, SoldTicketQuantity, "+
				"InvoiceTotal, TicketPrice, SoldPrice, ActualSoldPrice, TicketCost, TotalTicketCost, "+ 
				"TotalSoldPrice, Fees, NetSoldPrice, ActualProfitOrLoss, GrossMargin, "+
				"CustomerName, eMail, Phone, BillingAddress, ShippingAddress "+
				"from rep_RTF_Filled_INV_report_qry_vw with(nolock) "+
				"order by InvCreatedDate, InvoiceNo ";
			
			session = getSession();
			Query query = session.createSQLQuery(SQLQuery);
			tickets = query.list();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tickets;
	}
	
	public List<Object[]> getRTFFilledReportByPO(){
		List<Object[]> tickets = null;
		Session session = null;
		try {
			String SQLQuery = "select PurchaseOrderNo, PurchaseOrderCreatedDate, InvoiceNo, InvoiceCreatedDate, "+
				"CustomerOrderNo, CustomerOrderCreatedDate, "+
				"EventCategory, EventName, EventDate, EventTime, Venue, SectionRange, RowRange, "+ 
				"InvoiceTicketQuantity, SoldTicketQuantity, "+
				"InvoiceTotal, TicketPrice, SoldPrice, ActualSoldPrice, TicketCost, TotalTicketCost, "+ 
				"TotalSoldPrice, Fees, NetSoldPrice, ActualProfitOrLoss, GrossMargin, "+
				"CustomerName, eMail, Phone, BillingAddress, ShippingAddress "+
				"from rep_RTF_Filled_PO_report_qry_vw with(nolock) "+
				"order by PORCreatedDate, PurchaseOrderNo ";
			
			session = getSession();
			Query query = session.createSQLQuery(SQLQuery);
			tickets = query.list();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tickets;
	}
	
	public List<Object[]> getRTFUnFilledReportByInvoice(){
		List<Object[]> tickets = null;
		Session session = null;
		try {
			String SQLQuery = "select InvoiceNo, InvoiceCreatedDate, "+
				"EventCategory, EventName, EventDate, EventTime, Venue, Section, Row, SectionRange, RowRange, "+ 
				"InvoiceTicketQuantity, SoldTicketQuantity, "+
				"InvoiceTotal, TicketPrice, SoldPrice, ActualSoldPrice, "+ 
				"TotalSoldPrice, Fees, NetSoldPrice, "+
				"CustomerName, eMail, Phone, BillingAddress, ShippingAddress "+	
				"from rep_RTF_UNFilled_report_qry_vw with(nolock) "+
				"order by InvCreatedDate ";
						
			session = getSession();			
			Query query = session.createSQLQuery(SQLQuery);
			tickets = query.list();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tickets;
	}
	
	public List<Object[]> getRTFMonthlyFilledReportByInvoice(){
		List<Object[]> monthlyFilledList = null;
		Session session = null;
		try {
			String SQLQuery = "select InvoiceYear, InvoiceMonth, "+
				"InvoiceNo, CustomerOrderNo, PurchaseOrderNo, "+
				"InvoiceTicketQuantity, SoldTicketQuantity, "+
				"InvoiceTotal, TicketPrice, SoldPrice, ActualSoldPrice, "+ 
				"TicketCost, TotalTicketCost, TotalSoldPrice, NetSoldPrice, "+ 
				"ActualProfitOrLoss, GrossMargin "+ 
				"from rep_RTF_Filled_Monthly_by_InvoiceDate_report with(nolock) "+
				"order by InvoiceYear, InvMonth ";
				
			session = getSession();			
			Query query = session.createSQLQuery(SQLQuery);
			monthlyFilledList = query.list();		
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return monthlyFilledList;
	}
	
	public List<Object[]> getRTFMonthlyFilledReportByPO(){
		List<Object[]> monthlyFilledList = null;
		Session session = null;
		try {
			String SQLQuery = "select PurchaseOrderYear, PurchaseOrderMonth, "+
				"PurchaseOrderNo, InvoiceNo, CustomerOrderNo, "+
				"InvoiceTicketQuantity, SoldTicketQuantity, "+ 
				"InvoiceTotal, TicketPrice, SoldPrice, ActualSoldPrice, "+ 
				"TicketCost, TotalTicketCost, TotalSoldPrice, NetSoldPrice, "+ 
				"ActualProfitOrLoss, GrossMargin "+ 
				"from rep_RTF_Filled_Monthly_by_PurchaseOrderDate_report with(nolock) "+
				"order by PurchaseOrderYear, POMonth ";
						
			session = getSession();			
			Query query = session.createSQLQuery(SQLQuery);
			monthlyFilledList = query.list();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return monthlyFilledList;
	}
	
	public List<Object[]> getRTFVoidedInvoiceReport(){
		List<Object[]> voidedInvoiceList = null;
		Session session = null;
		try {
			String SQLQuery = "select InvoiceNo, InvoiceCreatedDate, "+
				"CustomerOrderNo, CustomerOrderCreatedDate, "+
				"CustomerName, CustomerEmail, "+
				"EventName, EventDate, EventTime, "+ 
				"VenueName, VenueCity, Zone, "+ 
				"OrderTicketQuantity, ListingPrice, "+ 
				"OrderTotal, TicketPrice, SoldPrice, "+ 
				"OrderType, OrderPlatform, InvoiceVoidedDate, "+  
				"InvoiceTotal, InvoiceTicketQuantity, "+ 
				"InvoiceRefundAmount, OrderRefundAmount "+
				"from rep_RTF_Voided_Invoice_report_qry_vw with(nolock) "+
				"order by InvCreatedDate ";
						
			session = getSession();			
			Query query = session.createSQLQuery(SQLQuery);
			voidedInvoiceList = query.list();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return voidedInvoiceList;
	}
	
	public List<Object[]> getRTFSalesReportByReferral(){
		List<Object[]> tickets = null;
		Session session = null;
		try {
			String SQLQuery = "select InvoiceNo, InvoiceCreatedDate, InvoiceStatus, "+
				"CustomerOrderNo, CustomerOrderCreatedDate, "+
				"PurchaseOrderNo, PurchaseOrderCreatedDate, "+
				"EventName, EventDate, EventTime, "+
				"VenueName, VenueCity, Zone, "+ 
				"Quantity, Section, Row, Seat, "+
				"CustomerName, CustomerEmail, "+ 
				"ListingPrice, PromotionalCodeDiscAmount, LoyalfanDiscountAmt, "+
				"OrderTotal, Cost, NetTotal, ProfitLoss, "+
				"OrderType, Platform, PaymentMethod, "+
				"PromotionOfferUsed, promoCode, promotionalDiscount, LoyalFanDiscount, "+
				"ReferralCode, ReferralEmail "+ 
				"from rep_RTF_Sales_report_qry_vw with(nolock) "+
				"where ReferralCode is not null and ReferralCode <> '' "+
				"order by InvCreatedDate, COCreatedDate, POCreatedDate ";
			
			session = getSession();
			Query query = session.createSQLQuery(SQLQuery);
			tickets = query.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return tickets;
	}
	
	public List<Object[]> getRTFCustomerYearlySpentReport(String year){
		List<Object[]> customers = null;
		Session session = null;
		try {
			String SQLQuery = "select OrderYear, CustomerName, "+
				"NoOfOrders, TotalTicketQuantity, TotalOrderValue, "+ 
				"EarnedPoints, RedeemedPoints, "+
				"LoyalFanDiscount, AppDiscount, PromotionalDiscounts, "+				 
				"SpentOnCustomer, AverageSpentOnCustomer "+ 
				"from rep_RTF_SpentOnCustomer_Yearly_CustomerWise_report_qry_vw with(nolock) "+
				"where 1=1 ";
							
			if(year != null && !year.isEmpty() && !year.equalsIgnoreCase("ALL")){
				SQLQuery += " and OrderYear = '"+year+"' ";
			}
			SQLQuery += " order by 1,2 ";
			session = getSession();
			Query query = session.createSQLQuery(SQLQuery);
			customers = query.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return customers;
	}
	
	public List<Object[]> getRTFCustomerPlatformWiseReport(String year, String platform){
		List<Object[]> customers = null;
		Session session = null;
		try {
			String SQLQuery = "SELECT CustomerName"+
				  ",count(OrderNo) as NoOfOrders"+
				  ",sum(OrderQuantity) as TotalTicketQuantity"+
				  ",sum(OrderTotal) as TotalOrderValue"+
				  ",sum(EarnedPoints) as EarnedPoints"+
				  ",sum(RedeemedPoints) as RedeemedPoints"+
				  ",sum(LoyalFanDiscount) as LoyalFanDiscount"+
				  ",sum(AppDiscount) as AppDiscount"+
				  ",sum(PromotionalDiscount) as PromotionalDiscount"+
				  ",CONVERT(DECIMAL(10,2), (sum(RedeemedPoints)+sum(LoyalFanDiscount)+sum(AppDiscount)+sum(PromotionalDiscount))) as SpentOnCustomer"+
				  ",CONVERT(DECIMAL(10,2), ((sum(RedeemedPoints)+sum(LoyalFanDiscount)+sum(AppDiscount)+sum(PromotionalDiscount))/count(OrderNo)) ) as AverageSpentOnCustomer"+
				  " FROM rep_RTF_SpentOnCustomer_OrderWise_report_qry_tb with(nolock) "+
				  "where 1=1 ";

			if(year != null && !year.isEmpty() && !year.equalsIgnoreCase("ALL")){
				SQLQuery += " and OrderYear IN("+year+") ";
			}
			if(platform != null && !platform.isEmpty() && !platform.equalsIgnoreCase("ALL")){
				SQLQuery += " and Platform IN("+platform+") ";
			}
			SQLQuery += " group by CustomerNo, CustomerName ";
			SQLQuery += " order by CustomerName ";
			session = getSession();
			Query query = session.createSQLQuery(SQLQuery);
			customers = query.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return customers;
	}
	
	public List<Object[]> getRTFCustomerOrderwiseReport(String year, String platform){
		List<Object[]> customers = null;
		Session session = null;
		try {
			String SQLQuery = "select OrderYear, CustomerName, "+
				"OrderNo, OrderDate, OrderQuantity, OrderTotal, "+ 
				"EarnedPoints, RedeemedPoints, "+
				"LoyalFanDiscount, AppDiscount, PromotionalDiscount, "+				 
				"NULL as SpentOnCustomer, NULL as AverageSpentOnCustomer, "+
				"CustomerNo "+
				"from rep_RTF_SpentOnCustomer_OrderWise_report_qry_tb with(nolock) "+
				"where 1=1 ";
							
			if(year != null && !year.isEmpty() && !year.equalsIgnoreCase("ALL")){
				SQLQuery += " and OrderYear IN("+year+") ";
			}
			if(platform != null && !platform.isEmpty() && !platform.equalsIgnoreCase("ALL")){
				SQLQuery += " and Platform IN("+platform+") ";
			}
			SQLQuery += " order by 2,1,3 ";
			session = getSession();
			Query query = session.createSQLQuery(SQLQuery);
			customers = query.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return customers;
	}
	
	public List<Object[]> getRTFCustomerAcquisitionReport(String year, String platform){
		List<Object[]> customers = null;
		Session session = null;
		try {
			String SQLQuery = "SELECT CustomerName, "+
			  "count(OrderNo) as NoOfOrders, "+
			  "sum(OrderTotal) as TotalOrderValue, "+
			  "CONVERT(DECIMAL(10,2), (sum(RedeemedPoints)+sum(LoyalFanDiscount)+sum(AppDiscount)+sum(PromotionalDiscount))) as CustomerAcquisitionCost, "+
			  "CONVERT(DECIMAL(10,2), ((sum(RedeemedPoints)+sum(LoyalFanDiscount)+sum(AppDiscount)+sum(PromotionalDiscount))/count(OrderNo))) as AverageCostPerOrder "+		   
			  "from rep_RTF_SpentOnCustomer_OrderWise_report_qry_tb with(nolock) "+
			  "where 1=1 ";
							
			if(year != null && !year.isEmpty() && !year.equalsIgnoreCase("ALL")){
				SQLQuery += " and OrderYear IN("+year+") ";
			}
			if(platform != null && !platform.isEmpty() && !platform.equalsIgnoreCase("ALL")){
				SQLQuery += " and Platform IN("+platform+") ";
			}
			SQLQuery += " group by CustomerNo, CustomerName ";
			SQLQuery += " order by CustomerName ";
			session = getSession();
			Query query = session.createSQLQuery(SQLQuery);
			customers = query.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return customers;
	}
	
	public List<Object[]> getRTFCustomerAcquisitionSummaryReport(String year, String platform){
		List<Object[]> customers = null;
		Session session = null;
		try {
			String SQLQuery = "SELECT 0 as sqno, 'Average Order $' as heading, "+
				"CONVERT(DECIMAL(10,2), (sum(ao.TotalOrderValue)/count(1)) ) as AverageCostValue, "+ 
				"count(1) as cnt, sum(ao.TotalOrderValue) as SumOfAllValue "+
				"FROM ( SELECT CustomerName, sum(OrderTotal) as TotalOrderValue "+
	              "FROM rep_RTF_SpentOnCustomer_OrderWise_report_qry_tb with(nolock) "+
	              "where 1=1 ";
			
			if(year != null && !year.isEmpty() && !year.equalsIgnoreCase("ALL")){
				SQLQuery += " and OrderYear IN("+year+") ";
			}
			if(platform != null && !platform.isEmpty() && !platform.equalsIgnoreCase("ALL")){
				SQLQuery += " and Platform IN("+platform+") ";
			}
			
				SQLQuery += "group by CustomerNo, CustomerName "+
				       ") ao "+
				"UNION "+
				"SELECT ac.NoOfOrders as sqno, "+  
	              "'Average Cost for '+ CAST(ac.NoOfOrders as varchar(3))+ ' Orders' as heading, "+
	              "CONVERT(DECIMAL(10,2), (sum(ac.SpentOnCustomer)/(count(1) * ac.NoOfOrders)) ) as AverageCostValue, "+  
	              "(count(1) * ac.NoOfOrders) as cnt, sum(ac.SpentOnCustomer) as SumOfAllValue "+
				"FROM ( SELECT CustomerName, count(OrderNo) as NoOfOrders "+
	              ",CONVERT(DECIMAL(10,2), (sum(RedeemedPoints)+sum(LoyalFanDiscount)+sum(AppDiscount)+sum(PromotionalDiscount))) as SpentOnCustomer "+
	              "FROM rep_RTF_SpentOnCustomer_OrderWise_report_qry_tb with(nolock) "+
	              "where 1=1 ";
				
				if(year != null && !year.isEmpty() && !year.equalsIgnoreCase("ALL")){
					SQLQuery += " and OrderYear IN("+year+") ";
				}
				if(platform != null && !platform.isEmpty() && !platform.equalsIgnoreCase("ALL")){
					SQLQuery += " and Platform IN("+platform+") ";
				}
				
					SQLQuery += "group by CustomerNo, CustomerName "+
				       ") ac "+
				"GROUP BY ac.NoOfOrders "+
				"HAVING sum(ac.SpentOnCustomer) > 0 "+
				"ORDER BY 1";

			session = getSession();
			Query query = session.createSQLQuery(SQLQuery);
			customers = query.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return customers;
	}
	
	public List<Object[]> getRTFTMATSeatAllocationReport(String artistName){
		List<Object[]> seatAllocation = null;
		Session session = null;
		try {
			String SQLQuery = "select g.* from ("+
				"select distinct co.id as OrderId, "+
				"CONVERT(VARCHAR(10), co.created_date, 101) + ' ' +CONVERT(VARCHAR(10), co.created_date, 108) as OrderDate, "+
				"co.order_type as OrderType, co.event_name as EventName, "+
				"CONVERT(VARCHAR(19), co.event_date,101) as EventDate, "+
				"CASE WHEN co.event_time is not null THEN CONVERT(VARCHAR(20), co.event_time, 108) "+
				"WHEN co.event_time is null THEN 'TBD' "+ 
				"END as EventTime, "+
				"co.venue_name as VenueName, co.venue_city as VenueCity, co.venue_state as VenueState, co.venue_country as VenueCountry, "+
				"co.order_total as OrderTotal, "+
				"co.section as Zone, co.quantity as RTF_Quantity, t.section as TMAT_Section, t.row as TMAT_Row, "+
				"t.remaining_quantity as TMAT_Quantity, (t.current_price*co.quantity) as TMAT_Price, "+
				"t.site_id as SiteId, "+ 
				"CONVERT(VARCHAR(10), t.insertion_date, 101) + ' ' +CONVERT(VARCHAR(10), t.insertion_date, 108) as TMAT_Ticket_Created_Date, "+ 
				"CONVERT(VARCHAR(10), t.last_update, 101) + ' ' +CONVERT(VARCHAR(10), t.last_update, 108)  as TMAT_Ticket_Last_Updated, "+
				"case when t.ticket_status='DISABLED' then 'SOLD' else t.ticket_status end as TMAT_Ticket_Status "+
				"from customer_order co with(nolock) "+
				"inner join event e  with(nolock) on e.id=co.event_id "+
				"left join event_artist ea  with(nolock) on ea.event_id=e.id "+
				"left join artist a  with(nolock) on a.id=ea.artist_id "+
				"inner join category_ticket_group ctg  with(nolock) on ctg.id=co.category_ticket_group_id "+
				"inner join invoice i  with(nolock) on i.order_id=co.id "+
				"inner join [10.0.1.51].tmatprodscale.dbo.zone_tickets_category_ticket_Group tg  with(nolock) on tg.zone_tickets_ticket_group_id=co.category_ticket_group_id "+
				"inner join [10.0.1.51].tmatprodscale.dbo.ticket t  with(nolock) on t.id=tg.ticket_id "+
				"where is_long_sale = 0";
				
				if(artistName != null && !artistName.isEmpty()){				
					SQLQuery += " and (e.name like '%"+artistName+"%' or a.name like '%"+artistName+"%') ";
				}
				
				SQLQuery += "union "+ 
				
				"select distinct co.id as OrderId, "+
				"CONVERT(VARCHAR(10), co.created_date, 101) + ' ' +CONVERT(VARCHAR(10), co.created_date, 108) as OrderDate, "+
				"co.order_type as OrderType, co.event_name as EventName, "+
				"CONVERT(VARCHAR(19), co.event_date,101) as EventDate, "+
				"CASE WHEN co.event_time is not null THEN CONVERT(VARCHAR(20), co.event_time, 108) "+
				"WHEN co.event_time is null THEN 'TBD' "+ 
				"END as EventTime, "+
				"co.venue_name as VenueName, co.venue_city as VenueCity, co.venue_state as VenueState, co.venue_country as VenueCountry, "+
				"co.order_total as OrderTotal, "+
				"co.section as Zone, co.quantity as RTF_Quantity, t.section as TMAT_Section, t.row as TMAT_Row, "+
				"t.remaining_quantity as TMAT_Quantity, (t.current_price*co.quantity) as TMAT_Price, "+
				"t.site_id as SiteId, "+
				"CONVERT(VARCHAR(10), t.insertion_date, 101) + ' ' +CONVERT(VARCHAR(10), t.insertion_date, 108) as TMAT_Ticket_Created_Date, "+
				"CONVERT(VARCHAR(10), t.last_update, 101) + ' ' +CONVERT(VARCHAR(10), t.last_update, 108)  as TMAT_Ticket_Last_Updated, "+
				"case when t.ticket_status='DISABLED' then 'SOLD' else t.ticket_status end as TMAT_Ticket_Status "+
				"from customer_order co  with(nolock) "+
				"inner join event e  with(nolock) on e.id=co.event_id "+
				"left join event_artist ea  with(nolock) on ea.event_id=e.id "+
				"left join artist a  with(nolock) on a.id=ea.artist_id "+
				"inner join category_ticket_group ctg with(nolock) on ctg.id=co.category_ticket_group_id "+
				"inner join invoice i with(nolock) on i.order_id=co.id "+
				"inner join [10.0.1.51].tmatprodscale.dbo.zone_tickets_category_ticket_Group tg on tg.zone_tickets_ticket_group_id=co.category_ticket_group_id "+
				"inner join [10.0.1.51].tmatprodscale.dbo.historical_ticket t on t.id=tg.ticket_id "+
				"where is_long_sale = 0";
				
				if(artistName != null && !artistName.isEmpty()){				
					SQLQuery += " and (e.name like '%"+artistName+"%' or a.name like '%"+artistName+"%') ";
				}
				SQLQuery += ") as g ";
							
			
			SQLQuery += " order by g.orderDate";
			session = getSession();
			Query query = session.createSQLQuery(SQLQuery);
			seatAllocation = query.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return seatAllocation;
	}
}
