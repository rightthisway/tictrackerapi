package com.rtw.tracker.dao.implementation;

import com.rtw.tracker.datas.Product;

public class ProductDAO extends HibernateDAO<Integer, Product> implements com.rtw.tracker.dao.services.ProductDAO{


	public Product getProductByName(String productName) throws Exception { 
		return findSingle("From Product where name=?" ,new Object[]{productName});
	}
}
