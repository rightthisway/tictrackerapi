package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.CustomerLoyalFanAudit;

public class CustomerLoyalFanAuditDAO  extends HibernateDAO<Integer,CustomerLoyalFanAudit> implements com.rtw.tracker.dao.services.CustomerLoyalFanAuditDAO {
	
	public List<CustomerLoyalFanAudit> getAllByCustomerId(Integer customerId){
		return find("FROM CustomerLoyalFanAudit WHERE customerId = ?", new Object[]{customerId});
	}
	
}
