package com.rtw.tracker.dao.implementation;

import java.util.List;
import java.util.Properties;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.EnumType;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.datas.CustomerLoyaltyHistory;
import com.rtw.tracker.datas.RewardDetails;
import com.rtw.tracker.enums.OrderType;
import com.rtw.tracker.enums.RewardStatus;
import com.rtw.tracker.utils.GridHeaderFilters;

/**
 * class having db related methods for CustomerFavouriteEvent
 * @author Ulaganathan
 *
 */
public class CustomerLoyaltyHistoryDAO extends HibernateDAO<Integer, CustomerLoyaltyHistory> implements com.rtw.tracker.dao.services.CustomerLoyaltyHistoryDAO{

	@Override
	public CustomerLoyaltyHistory getCustomerLoyaltyByCustomerId(Integer customerId) {
		return findSingle("FROM CustomerLoyaltyHistory WHERE customerId=?", new Object[]{customerId});
	}

	@Override
	public CustomerLoyaltyHistory getCustomerLoyaltyByOrderId(Integer orderId) {
		return findSingle("FROM CustomerLoyaltyHistory WHERE orderId=? AND rewardStatus=?", new Object[]{orderId,RewardStatus.ACTIVE});
	}
	
	@Override
	public CustomerLoyaltyHistory getCustomerLoyaltyHistoryByOrderIdAndCustomerId(Integer orderId,Integer customerId) {
		return findSingle("FROM CustomerLoyaltyHistory WHERE orderId=? AND customerId=?", new Object[]{orderId,customerId});
	}
	
	@Override
	public CustomerLoyaltyHistory getCustomerGiftCardOrderLoyaltyHistoryByOrderIdAndCustomerId(Integer orderId,Integer customerId) {
		return findSingle("FROM CustomerLoyaltyHistory WHERE orderId=? AND customerId=? and orderType=?", new Object[]{orderId,customerId,OrderType.GIFTCARDORDER});
	}
	
	public CustomerLoyaltyHistory getCustomerLoyaltyByOrderIdAndStatus(Integer orderId){
		return findSingle("FROM CustomerLoyaltyHistory WHERE orderId=? AND (rewardStatus=? OR rewardStatus=?)", new Object[]{orderId,RewardStatus.ACTIVE,RewardStatus.PENDING});
	}
	
	public List<RewardDetails> getCustomerRewardsByRewardStatus(RewardStatus rewardStatus , Integer customerId, GridHeaderFilters filter){
		Session rewardSession = null;
		try {			
			Properties paramRewardStatus = new Properties();
			paramRewardStatus.put("enumClass", "com.rtw.tracker.enums.OrderType");
			paramRewardStatus.put("type", "12"); /*type 12 instructs to use the String representation of enum value*/
			
			String sql = "select rh.customer_id as customerId, rh.Order_id as orderNo, rh.points_earned as rewards, co.created_date as OrderDate, e.event_date as eventDate," +
			"e.event_time as eventTime, co.order_total as orderTotal, rh.order_type as orderType, CONVERT(VARCHAR(100),REPLACE(e.name,',','')) as eventName " +
			"from cust_loyalty_reward_history rh with(nolock) "+
			"inner join customer_order co with(nolock) on rh.Order_id=co.id " +
			"left join event e with(nolock) on e.id=co.event_id "+
			"where rh.status ='"+rewardStatus+"' " +
			"and rh.order_type in ('REGULAR','LOYALFAN','REFERRER') and co.order_type in ('REGULAR','LOYALFAN') and " +
			"rh.customer_id ="+customerId+" ";
			
			if(filter.getCustomerOrderId() != null){
				sql += " AND rh.Order_id = "+filter.getCustomerOrderId()+" ";
			}
			if(filter.getRewardPoints() != null){
				sql += " AND rh.points_earned = "+filter.getRewardPoints()+" ";
			}
			if(filter.getOrderTotal() != null){
				sql += " AND co.order_total = "+filter.getOrderTotal()+" ";
			}
			if(filter.getOrderType() != null){
				sql += " AND rh.order_type like '%"+filter.getOrderType()+"%' ";
			}
			if(filter.getOrderDateStr() != null){
				String dateTimeStr = filter.getOrderDateStr();
				if(dateTimeStr.contains(" ")){
					String dateTimeArr[] = dateTimeStr.split(" ");
					if(Util.extractDateElement(dateTimeArr[0],"DAY") > 0){
						sql += " AND DATEPART(day,co.created_date) = "+Util.extractDateElement(dateTimeArr[0],"DAY")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"MONTH") > 0){
						sql += " AND DATEPART(month,co.created_date) = "+Util.extractDateElement(dateTimeArr[0],"MONTH")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"YEAR") > 0){
						sql += " AND DATEPART(year,co.created_date) = "+Util.extractDateElement(dateTimeArr[0],"YEAR")+" ";
					}
					if(dateTimeArr.length>1){
						String dateTime = dateTimeArr[1];
						if(dateTimeArr.length > 2){
							if(dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()){
								dateTime += " "+dateTimeArr[2];
							}
						}
						if(Util.extractDateElement(dateTime,"HOUR") > 0){
							sql += " AND DATEPART(hour,co.created_date) = "+Util.extractDateElement(dateTime,"HOUR")+" ";
						}
						if(Util.extractDateElement(dateTime,"MINUTE") > 0){
							sql += " AND DATEPART(minute,co.created_date) = "+Util.extractDateElement(dateTime,"MINUTE")+" ";
						}
					}
				}else{
					if(Util.extractDateElement(filter.getOrderDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,co.created_date) = "+Util.extractDateElement(filter.getOrderDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getOrderDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,co.created_date) = "+Util.extractDateElement(filter.getOrderDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getOrderDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,co.created_date) = "+Util.extractDateElement(filter.getOrderDateStr(),"YEAR")+" ";
					}
				}
			}
			if(filter.getEventName() != null){
				sql += " AND e.name like '%"+filter.getEventName()+"%' ";
			}
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,e.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,e.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,e.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventTimeStr() != null){
				if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
					sql += " AND DATEPART(hour,e.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
					sql += " AND DATEPART(minute,e.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
				}
			}
			rewardSession = getSession();
			SQLQuery query = rewardSession.createSQLQuery(sql);
			query.addScalar("customerId", Hibernate.INTEGER);
			query.addScalar("orderNo", Hibernate.INTEGER);
			query.addScalar("rewards", Hibernate.DOUBLE);
			query.addScalar("OrderDate", Hibernate.DATE);
			query.addScalar("orderTotal", Hibernate.DOUBLE);
			query.addScalar("orderType", Hibernate.custom(EnumType.class,paramRewardStatus));
			query.addScalar("eventName", Hibernate.STRING);
			query.addScalar("eventDate", Hibernate.DATE);
			query.addScalar("eventTime", Hibernate.TIME);
			
			List<RewardDetails> finalList = query.setResultTransformer(Transformers.aliasToBean(RewardDetails.class)).list();
			return finalList;
		}catch (Exception e) {
			if(rewardSession != null && rewardSession.isOpen() ){
				rewardSession.close();
			}
			e.printStackTrace();
		}
		return null;
	}
	
	public Integer getCustomerRewardsByRewardStatusCount(RewardStatus rewardStatus , Integer customerId, GridHeaderFilters filter){
		Session rewardSession = null;
		Integer count = 0;
		try {
			String sql = "select count(*) as cnt " +
			"from cust_loyalty_reward_history rh with(nolock) "+
			"inner join customer_order co with(nolock) on rh.Order_id=co.id " +
			"left join event e with(nolock) on e.id=co.event_id "+
			"where rh.status='"+rewardStatus+"' " +
			"and rh.order_type in ('REGULAR','LOYALFAN','REFERRER') and co.order_type in ('REGULAR','LOYALFAN') and " +
			"rh.customer_id ="+customerId+" ";
					
			//sql = sql + " order by co.created_date desc ";

			if(filter.getCustomerOrderId() != null){
				sql += " AND rh.Order_id = "+filter.getCustomerOrderId()+" ";
			}
			if(filter.getRewardPoints() != null){
				sql += " AND rh.points_earned = "+filter.getRewardPoints()+" ";
			}
			if(filter.getOrderTotal() != null){
				sql += " AND co.order_total = "+filter.getOrderTotal()+" ";
			}
			if(filter.getOrderType() != null){
				sql += " AND rh.order_type like '%"+filter.getOrderType()+"%' ";
			}
			if(filter.getOrderDateStr() != null){
				String dateTimeStr = filter.getOrderDateStr();
				if(dateTimeStr.contains(" ")){
					String dateTimeArr[] = dateTimeStr.split(" ");
					if(Util.extractDateElement(dateTimeArr[0],"DAY") > 0){
						sql += " AND DATEPART(day,co.created_date) = "+Util.extractDateElement(dateTimeArr[0],"DAY")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"MONTH") > 0){
						sql += " AND DATEPART(month,co.created_date) = "+Util.extractDateElement(dateTimeArr[0],"MONTH")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"YEAR") > 0){
						sql += " AND DATEPART(year,co.created_date) = "+Util.extractDateElement(dateTimeArr[0],"YEAR")+" ";
					}
					if(dateTimeArr.length>1){
						String dateTime = dateTimeArr[1];
						if(dateTimeArr.length > 2){
							if(dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()){
								dateTime += " "+dateTimeArr[2];
							}
						}
						if(Util.extractDateElement(dateTime,"HOUR") > 0){
							sql += " AND DATEPART(hour,co.created_date) = "+Util.extractDateElement(dateTime,"HOUR")+" ";
						}
						if(Util.extractDateElement(dateTime,"MINUTE") > 0){
							sql += " AND DATEPART(minute,co.created_date) = "+Util.extractDateElement(dateTime,"MINUTE")+" ";
						}
					}
				}else{
					if(Util.extractDateElement(filter.getOrderDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,co.created_date) = "+Util.extractDateElement(filter.getOrderDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getOrderDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,co.created_date) = "+Util.extractDateElement(filter.getOrderDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getOrderDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,co.created_date) = "+Util.extractDateElement(filter.getOrderDateStr(),"YEAR")+" ";
					}
				}
			}
			if(filter.getEventName() != null){
				sql += " AND e.name like '%"+filter.getEventName()+"%' ";
			}
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,e.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,e.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,e.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventTimeStr() != null){
				if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
					sql += " AND DATEPART(hour,e.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
					sql += " AND DATEPART(minute,e.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
				}
			}
			rewardSession = getSession();
			SQLQuery query = rewardSession.createSQLQuery(sql);
			count = Integer.parseInt(String.valueOf(query.uniqueResult()));
			return count;
		}catch (Exception e) {
			if(rewardSession != null && rewardSession.isOpen() ){
				rewardSession.close();
			}
			e.printStackTrace();
		}
		return null;
	}
	
	
	public List<RewardDetails> getCustomerRedeemedRewardsByCustomerId(Integer customerId, GridHeaderFilters filter){
		Session rewardSession = null;
		try {
			Properties paramRewardStatus = new Properties();
			paramRewardStatus.put("enumClass", "com.rtw.tracker.enums.OrderType");
			paramRewardStatus.put("type", "12"); /*type 12 instructs to use the String representation of enum value*/
			
			String sql = "select rh.customer_id as customerId, rh.Order_id as orderNo, rh.points_spent as rewards, " +
			"co.created_date as OrderDate, e.event_date as eventDate," +
			"e.event_time as eventTime, co.order_total as orderTotal, rh.order_type as orderType, CONVERT(VARCHAR(100),REPLACE(e.name,',','')) as eventName " +
			"from cust_loyalty_reward_history rh with(nolock) "+
			"inner join customer_order co with(nolock) on rh.Order_id=co.id " +
			"left join event e with(nolock) on e.id=co.event_id "+
			"where rh.points_spent>0 and rh.customer_id ="+customerId+" ";
			
			if(filter.getCustomerOrderId() != null){
				sql += " AND rh.Order_id = "+filter.getCustomerOrderId()+" ";
			}
			if(filter.getRewardPoints() != null){
				sql += " AND rh.points_spent = "+filter.getRewardPoints()+" ";
			}
			if(filter.getOrderTotal() != null){
				sql += " AND co.order_total = "+filter.getOrderTotal()+" ";
			}
			if(filter.getOrderType() != null){
				sql += " AND rh.order_type like '%"+filter.getOrderType()+"%' ";
			}
			if(filter.getOrderDateStr() != null){
				String dateTimeStr = filter.getOrderDateStr();
				if(dateTimeStr.contains(" ")){
					String dateTimeArr[] = dateTimeStr.split(" ");
					if(Util.extractDateElement(dateTimeArr[0],"DAY") > 0){
						sql += " AND DATEPART(day,co.created_date) = "+Util.extractDateElement(dateTimeArr[0],"DAY")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"MONTH") > 0){
						sql += " AND DATEPART(month,co.created_date) = "+Util.extractDateElement(dateTimeArr[0],"MONTH")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"YEAR") > 0){
						sql += " AND DATEPART(year,co.created_date) = "+Util.extractDateElement(dateTimeArr[0],"YEAR")+" ";
					}
					if(dateTimeArr.length>1){
						String dateTime = dateTimeArr[1];
						if(dateTimeArr.length > 2){
							if(dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()){
								dateTime += " "+dateTimeArr[2];
							}
						}
						if(Util.extractDateElement(dateTime,"HOUR") > 0){
							sql += " AND DATEPART(hour,co.created_date) = "+Util.extractDateElement(dateTime,"HOUR")+" ";
						}
						if(Util.extractDateElement(dateTime,"MINUTE") > 0){
							sql += " AND DATEPART(minute,co.created_date) = "+Util.extractDateElement(dateTime,"MINUTE")+" ";
						}
					}
				}else{
					if(Util.extractDateElement(filter.getOrderDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,co.created_date) = "+Util.extractDateElement(filter.getOrderDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getOrderDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,co.created_date) = "+Util.extractDateElement(filter.getOrderDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getOrderDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,co.created_date) = "+Util.extractDateElement(filter.getOrderDateStr(),"YEAR")+" ";
					}
				}
			}
			if(filter.getEventName() != null){
				sql += " AND e.name like '%"+filter.getEventName()+"%' ";
			}
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,e.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,e.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,e.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventTimeStr() != null){
				if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
					sql += " AND DATEPART(hour,e.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
					sql += " AND DATEPART(minute,e.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
				}
			}
			rewardSession = getSession();
			SQLQuery query = rewardSession.createSQLQuery(sql);
			query.addScalar("customerId", Hibernate.INTEGER);
			query.addScalar("orderNo", Hibernate.INTEGER);
			query.addScalar("rewards", Hibernate.DOUBLE);
			query.addScalar("OrderDate", Hibernate.DATE);
			query.addScalar("orderTotal", Hibernate.DOUBLE);
			query.addScalar("orderType", Hibernate.custom(EnumType.class,paramRewardStatus));
			query.addScalar("eventName", Hibernate.STRING);
			query.addScalar("eventDate", Hibernate.DATE);
			query.addScalar("eventTime", Hibernate.TIME);
			
			List<RewardDetails> finalList = query.setResultTransformer(Transformers.aliasToBean(RewardDetails.class)).list();
			return finalList;
			
		}catch (Exception e) {
			if(rewardSession != null && rewardSession.isOpen() ){
				rewardSession.close();
			}
			e.printStackTrace();
		}
		return null;
	}
	
	public Integer getCustomerRedeemedRewardsByCustomerIdCount(Integer customerId, GridHeaderFilters filter){
		Session rewardSession = null;
		Integer count = 0;
		try {
			String sql = "select count(*) as cnt " +
			"from cust_loyalty_reward_history rh with(nolock) "+
			"inner join customer_order co with(nolock) on rh.Order_id=co.id " +
			"left join event e with(nolock) on e.id=co.event_id "+
			"where rh.points_spent>0 and rh.customer_id ="+customerId+" ";
				
			if(filter.getCustomerOrderId() != null){
				sql += " AND rh.Order_id = "+filter.getCustomerOrderId()+" ";
			}
			if(filter.getRewardPoints() != null){
				sql += " AND rh.points_spent = "+filter.getRewardPoints()+" ";
			}
			if(filter.getOrderTotal() != null){
				sql += " AND co.order_total = "+filter.getOrderTotal()+" ";
			}
			if(filter.getOrderType() != null){
				sql += " AND rh.order_type like '%"+filter.getOrderType()+"%' ";
			}
			if(filter.getOrderDateStr() != null){
				String dateTimeStr = filter.getOrderDateStr();
				if(dateTimeStr.contains(" ")){
					String dateTimeArr[] = dateTimeStr.split(" ");
					if(Util.extractDateElement(dateTimeArr[0],"DAY") > 0){
						sql += " AND DATEPART(day,co.created_date) = "+Util.extractDateElement(dateTimeArr[0],"DAY")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"MONTH") > 0){
						sql += " AND DATEPART(month,co.created_date) = "+Util.extractDateElement(dateTimeArr[0],"MONTH")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"YEAR") > 0){
						sql += " AND DATEPART(year,co.created_date) = "+Util.extractDateElement(dateTimeArr[0],"YEAR")+" ";
					}
					if(dateTimeArr.length>1){
						String dateTime = dateTimeArr[1];
						if(dateTimeArr.length > 2){
							if(dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()){
								dateTime += " "+dateTimeArr[2];
							}
						}
						if(Util.extractDateElement(dateTime,"HOUR") > 0){
							sql += " AND DATEPART(hour,co.created_date) = "+Util.extractDateElement(dateTime,"HOUR")+" ";
						}
						if(Util.extractDateElement(dateTime,"MINUTE") > 0){
							sql += " AND DATEPART(minute,co.created_date) = "+Util.extractDateElement(dateTime,"MINUTE")+" ";
						}
					}
				}else{
					if(Util.extractDateElement(filter.getOrderDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,co.created_date) = "+Util.extractDateElement(filter.getOrderDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getOrderDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,co.created_date) = "+Util.extractDateElement(filter.getOrderDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getOrderDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,co.created_date) = "+Util.extractDateElement(filter.getOrderDateStr(),"YEAR")+" ";
					}
				}
			}
			if(filter.getEventName() != null){
				sql += " AND e.name like '%"+filter.getEventName()+"%' ";
			}
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,e.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,e.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,e.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventTimeStr() != null){
				if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
					sql += " AND DATEPART(hour,e.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
					sql += " AND DATEPART(minute,e.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
				}
			}
			rewardSession = getSession();
			SQLQuery query = rewardSession.createSQLQuery(sql);
			count = Integer.parseInt(String.valueOf(query.uniqueResult()));
			return count;
		}catch (Exception e) {
			if(rewardSession != null && rewardSession.isOpen() ){
				rewardSession.close();
			}
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public CustomerLoyaltyHistory getActiveCustomerLoyaltyHistoryOfCrownJewelOrder(Integer orderId,Integer customerId) {
		return findSingle("FROM CustomerLoyaltyHistory WHERE orderId=? AND customerId=? AND orderType=? AND rewardStatus=?", new Object[]{orderId,customerId,OrderType.FANTASYTICKETS,RewardStatus.ACTIVE});
	}
	
	@Override
	public CustomerLoyaltyHistory getVoidCustomerLoyaltyHistoryOfCrownJewelOrder(Integer orderId,Integer customerId) {
		return findSingle("FROM CustomerLoyaltyHistory WHERE orderId=? AND customerId=? AND orderType=? AND rewardStatus=?", new Object[]{orderId,customerId,OrderType.FANTASYTICKETS,RewardStatus.VOIDED});
	}
	
	@Override
	public CustomerLoyaltyHistory getVoidRefundCustomerLoyaltyHistoryOfCrownJewelOrder(Integer orderId,Integer customerId) {
		return findSingle("FROM CustomerLoyaltyHistory WHERE orderId=? AND customerId=? AND orderType=? AND rewardStatus=?", new Object[]{orderId,customerId,OrderType.CROWNJEWEL_REFUND,RewardStatus.VOIDED});
	}
	
	@Override
	public CustomerLoyaltyHistory getActiveRefundCustomerLoyaltyHistoryOfCrownJewelOrder(Integer orderId,Integer customerId) {
		return findSingle("FROM CustomerLoyaltyHistory WHERE orderId=? AND customerId=? AND orderType=? AND rewardStatus=?", new Object[]{orderId,customerId,OrderType.CROWNJEWEL_REFUND,RewardStatus.ACTIVE});
	}
}
