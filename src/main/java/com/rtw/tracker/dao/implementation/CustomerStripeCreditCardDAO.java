package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.CustomerStripeCreditCard;

public class CustomerStripeCreditCardDAO extends HibernateDAO<Integer, CustomerStripeCreditCard> implements com.rtw.tracker.dao.services.CustomerStripeCreditCardDAO{

	@Override
	public List<CustomerStripeCreditCard> getCustomerStripCardDetailsByCustomerId(Integer customerId) throws Exception {
		return find("FROM CustomerStripeCreditCard WHERE customerId=?",new Object[]{customerId});
	}

}
