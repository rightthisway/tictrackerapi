package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.RTFPromoOffers;

public class RTFPromoOffersDAO extends HibernateDAO<Integer, RTFPromoOffers> implements com.rtw.tracker.dao.services.RTFPromoOffersDAO{

	public RTFPromoOffers getPromoCode(String promoCode){
		return findSingle("FROM RTFPromoOffers where promoCode = ? and status = ?",new Object[]{promoCode,"ENABLED"});
	}
	
	public List<RTFPromoOffers> getAllActiveOffers(){
		return find("FROM RTFPromoOffers where status = ?",new Object[]{"ENABLED"});
	}
	
}
