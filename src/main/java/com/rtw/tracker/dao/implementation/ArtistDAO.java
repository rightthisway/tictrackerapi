package com.rtw.tracker.dao.implementation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;

import com.rtw.tracker.datas.Artist;
import com.rtw.tracker.datas.ProductType;
import com.rtw.tracker.enums.ArtistStatus;

public class ArtistDAO extends HibernateDAO<Integer, Artist> implements com.rtw.tracker.dao.services.ArtistDAO{

	
public  List<Artist> getAllArtistBySearchKeyAndPageNumber(String searchKey,Integer pageNumber,Integer maxRows,ProductType productType,
		Map<Integer, Boolean> favArtistMap,Integer superFanArtistId) {
		
		
		String sql ="select distinct a.id,a.name FROM event e with(nolock) inner join artist a with(nolock) on e.artist_id=a.id " +
					"where a.name like '%"+searchKey+"%' ";
		
		switch (productType) {
			case REWARDTHEFAN:
				sql = sql+" and e.reward_thefan_enabled = 1 ";
				break;
			
			case MINICATS:
				sql = sql+" and e.minicats_enabled = 1 ";
				break;
				
			case VIPMINICATS:
				sql = sql+" and e.vipminicats_enabled = 1 ";
				break;
				
			case LASTROWMINICATS:
				sql = sql+" and e.lastrow_minicat_enabled = 1 ";
				break;
				
			case VIPLASTROWMINICATS:
				sql = sql+" and e.vip_lastrow_minicats_enabled = 1 ";
				break;
				
			case PRESALEZONETICKETS:
				sql = sql+" and e.presale_zonetickets_enabled = 1 ";
				break;
	
			default:
				break;
		}
		
		sql = sql+" order by a.name  OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY ";
		
		List<Artist> artists =new ArrayList<Artist>();
		Query query = null;
		Session session = null;
		try {
			session = getSessionFactory().openSession();
			query = session.createSQLQuery(sql);
			
			List<Object[]> result = (List<Object[]>)query.list();
			
			Artist artist = null;
			for (Object[] object : result) {
				artist= new Artist();
				Integer artistId = (Integer)object[0] ;
				Boolean isCustFavFlag = favArtistMap.get(artistId);
				if(null != isCustFavFlag && isCustFavFlag){
					artist.setCustFavFlag(true);
				}else{
					artist.setCustFavFlag(false);
				}
				if(artistId.equals(superFanArtistId) || superFanArtistId == artistId){
					artist.setCustSuperFanFlag(true);
				}
				artist.setId(artistId);
				artist.setName((String)object[1]);
				artists.add(artist);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		return artists;
	}
	/*public List<Artist> getAllArtistsByChildCategory(Integer childCategoryId)  throws Exception{
		return find("SELECT a FROM Artist a,GrandChildCategory gc WHERE a.grandChildCategory.id=gc.id AND gc.childCatId=? AND a.artistStatus=? " +
				" ORDER BY a.name", new Object[]{childCategoryId,ArtistStatus.ACTIVE});			
	}

	public List<Artist> getAllArtistsByGrandChildCategory(Integer grandChildCategoryId)  throws Exception{
		return find("FROM Artist WHERE grandChildCategory.id=? AND artistStatus=? " +
				" ORDER BY name", new Object[]{grandChildCategoryId,ArtistStatus.ACTIVE});			
	}*/
	public Collection<Artist> filterByName(String pattern) {
		return find("FROM Artist WHERE artistStatus=? AND name LIKE ? ORDER BY name ASC",new Object[]{ArtistStatus.ACTIVE,"%"+pattern+"%"});
		
	}
	@Override
	public Collection<Artist> getArtistByDisplayOnSearch() {
		return find("FROM Artist WHERE artistStatus=? AND isDisplay = ? ORDER BY name ASC",new Object[]{ArtistStatus.ACTIVE,true});
	}

}
