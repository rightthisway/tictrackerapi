package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.PayPalTracking;

public class PayPalTrackingDAO extends HibernateDAO<Integer, PayPalTracking> implements com.rtw.tracker.dao.services.PayPalTrackingDAO{

	public List<PayPalTracking> getTrackingByCustomerId(Integer customerId) throws Exception{
		return find("FROM PayPalTracking WHERE customerId=?",new Object[]{customerId});
	}
	
	public List<PayPalTracking> getTrackingByEventId(Integer eventId) throws Exception{
		return find("FROM PayPalTracking WHERE eventId=?",new Object[]{eventId});
	}
	
	public List<PayPalTracking> getTrackingByCategoryTicketGroupId(Integer catgoryTicketGroupId) throws Exception{
		return find("FROM PayPalTracking WHERE categoryTicketGroupId=?",new Object[]{catgoryTicketGroupId});
	}
	
	public List<PayPalTracking> getTrackingByPayPalTransactionId(String paypalTransactionId) throws Exception{
		return find("FROM PayPalTracking WHERE paypalTransactionId=?",new Object[]{paypalTransactionId});
	}
	
	public List<PayPalTracking> getTrackingByTransactionId(Integer transactionId) throws Exception{
		return find("FROM PayPalTracking WHERE transactionId=?",new Object[]{transactionId});
	}
	
}
