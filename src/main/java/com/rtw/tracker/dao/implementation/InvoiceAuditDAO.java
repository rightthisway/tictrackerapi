package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.InvoiceAudit;
import com.rtw.tracker.enums.InvoiceAuditAction;

public class InvoiceAuditDAO extends HibernateDAO<Integer, InvoiceAudit> implements  com.rtw.tracker.dao.services.InvoiceAuditDAO{

	@Override
	public List<InvoiceAudit> getInvoiceAuditByInvoiceId(Integer invoiceId) {
		return find("FROM InvoiceAudit WHERE invoiceId=? order by id desc",new Object[]{invoiceId});
	}

	@Override
	public List<InvoiceAudit> getInvoiceAuditByInvoiceIdAndAction(Integer invoiceId, InvoiceAuditAction action) {
		return find("FROM InvoiceAudit WHERE invoiceId=? AND action=?",new Object[]{invoiceId,action});
	}

}
