package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.datas.FanClub;
import com.rtw.tracker.datas.FanClubEvent;
import com.rtw.tracker.utils.GridHeaderFilters;

public class FanClubEventDAO extends HibernateDAO<Integer, FanClubEvent> implements com.rtw.tracker.dao.services.FanClubEventDAO{

	private static Session fanClubSession;
	
	public static Session getFanClubSession() {
		return fanClubSession;
	}
	public static void setFanClubSession(Session fanClubSession) {
		FanClubEventDAO.fanClubSession = fanClubSession;
	}
	@Override
	public List<FanClubEvent> getAllFanClubEvents(GridHeaderFilters filter, String status, Integer fanClubId,SharedProperty prop) {
		List<FanClubEvent> list =null;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT fe.id as id, fe.fanclub_mst_id as fanClubId, fe.customer_id as customerId, fe.created_user_id as createdBy,");
			sql.append("fe.updated_user_id as updatedBy, fe.event_name as eventName, fe.event_date as eventDate, fe.event_time as eventTime,");
			sql.append("fe.venue_address as venue, fe.event_status as status, fe.no_of_interested as interestedCount,fe.poster_url as eventImage,");
			sql.append("created_date as createdDate, updated_date as updatedDate,c.user_id as userId, c.email as email, c.phone as phone ");
			sql.append("FROM fanclub_events_mst fe with(nolock) ");
			sql.append("left join "+prop.getDatabasAlias()+"customer c with(nolock) on fe.customer_id = c.id ");
			sql.append("WHERE fe.fanclub_mst_id="+fanClubId);
			
			
			
			if(filter.getEventName()!=null && !filter.getEventName().isEmpty()){
				sql.append(" AND fe.event_name like '%"+filter.getEventName()+"%' ");
			}
			if(filter.getVenueName()!=null && !filter.getVenueName().isEmpty()){
				sql.append(" AND fe.venue_address like '%"+filter.getEventDescription()+"%' ");
			}
			if(filter.getStatus()!=null && !filter.getStatus().isEmpty()){
				sql.append(" AND fe.event_status  like '%"+filter.getStatus()+"%' ");
			}
			if(filter.getCreatedBy()!=null && !filter.getCreatedBy().isEmpty()){
				sql.append(" AND fe.created_user_id  like '%"+filter.getCreatedBy()+"%' ");
			}
			if(filter.getUpdatedBy()!=null && !filter.getUpdatedBy().isEmpty()){
				sql.append(" AND fe.updated_user_id  like '%"+filter.getUpdatedBy()+"%' ");
			}
			if(filter.getUsername()!=null && !filter.getUsername().isEmpty()){
				sql.append(" AND c.user_id like '%"+filter.getUsername()+"%' ");
			}
			if(filter.getEmail()!=null && !filter.getEmail().isEmpty()){
				sql.append(" AND c.email like '%"+filter.getEmail()+"%' ");
			}
			if(filter.getPhone()!=null && !filter.getPhone().isEmpty()){
				sql.append(" AND c.phone like '%"+filter.getPhone()+"%' ");
			}
			if(filter.getCount()!=null){
				sql.append(" AND fe.no_of_interested ="+filter.getCount());
			}
			
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql.append(" AND DATEPART(day, created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql.append(" AND DATEPART(month, created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql.append(" AND DATEPART(year, created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ");
				}
			}
			
			if(filter.getLastUpdatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					sql.append(" AND DATEPART(day, updated_date) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql.append(" AND DATEPART(month, updated_date) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql.append(" AND DATEPART(year, updated_date) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ");
				}
			}
			
			if(filter.getEventDateStr()!=null ){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					sql.append(" AND DATEPART(day, fe.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ");
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					sql.append(" AND DATEPART(month, fe.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ");
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					sql.append(" AND DATEPART(year, fe.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ");
				}
			}
			
			fanClubSession = FanClubEventDAO.getFanClubSession();
			if(fanClubSession==null || !fanClubSession.isOpen() || !fanClubSession.isConnected()){
				fanClubSession = getSession();
				FanClubEventDAO.setFanClubSession(fanClubSession);
			}
			SQLQuery query = fanClubSession.createSQLQuery(sql.toString());
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("eventName", Hibernate.STRING);
			query.addScalar("venue", Hibernate.STRING);
			query.addScalar("customerId", Hibernate.INTEGER);
			query.addScalar("fanClubId", Hibernate.INTEGER);
			query.addScalar("eventDate", Hibernate.TIMESTAMP);
			query.addScalar("eventTime", Hibernate.TIMESTAMP);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("updatedBy", Hibernate.STRING);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("interestedCount", Hibernate.INTEGER);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);
			query.addScalar("userId", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("phone", Hibernate.STRING);
			query.addScalar("eventImage", Hibernate.STRING);
			
			list = query.setResultTransformer(Transformers.aliasToBean(FanClubEvent.class)).list();
		} catch (Exception e) {
			if(fanClubSession != null && fanClubSession.isOpen()){
				fanClubSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
}
