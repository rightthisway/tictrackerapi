package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.datas.CrownJewelCustomerOrder;
import com.rtw.tracker.enums.CrownJewelOrderStatus;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.PaginationUtil;

public class CrownJewelCustomerOrderDAO extends HibernateDAO<Integer, CrownJewelCustomerOrder> implements com.rtw.tracker.dao.services.CrownJewelCustomerOrderDAO{

	@Override
	public List<CrownJewelCustomerOrder> getCronJewelOrdersByFilter(String orderId, String fromDate,String toDate, String parentCategory, String grandChildCategory, String teamName, String leagueName,String status,String pageNo, GridHeaderFilters filter)throws Exception {
		Integer startFrom = 0;
		
		/*String hql="Select id AS id,league_id AS leagueId,league_name AS leagueName,league_parent_type AS leagueParentType,team_id AS teamId," +
				"team_name AS teamName,league_city AS leagueCity,package_selected AS isPackageSelected,package_cost AS packageCost," +
				"package_notes AS packageNote,event_id AS eventId,ticket_group_id AS ticketGroupId,ticket_section AS ticketSelection," +
				"ticket_qty AS ticketQty,ticket_price AS ticketPrice,points_conversion AS pointConversion,ticket_points AS ticketPoints," +
				"package_points AS packagePoints,customer_id AS customerId,available_points AS availablePoints,remaining_points AS remainingPoints," +
				"status AS status,create_date AS createdDate,last_update AS lastUpdatedDate,accept_reject_reason AS acceptRejectReson," +
				"regular_order_id AS regularOrderId,required_points AS requiredPoints " +
				"FROM crownjewel_customer_orders WHERE 1=1 ";*/
		
		/*if(parentCategory!=null && !parentCategory.isEmpty()){
			hql += " AND league_parent_type='"+parentCategory+"' ";
		}*/
		String hql="Select fco.id AS id, fco.fantasy_event_name AS fantasyEventName, fco.team_id AS teamId, fco.team_name AS teamName, " +
			"fco.package_selected AS isPackageSelected, fco.package_cost AS packageCost, fco.package_notes AS packageNote, fco.event_id AS eventId, " +
			"fco.is_real_ticket as isRealTicket, fco.ticket_id AS ticketId, fco.zone AS ticketSelection, fco.ticket_qty AS ticketQty, " +
			"fco.ticket_price AS ticketPrice, fco.required_points AS requiredPoints, fco.customer_id AS customerId, fco.status AS status, " +			
			"fco.create_date AS createdDate, fco.last_update AS lastUpdatedDate, fco.accept_reject_reason AS acceptRejectReson," +
			"fco.regular_order_id AS regularOrderId, fco.team_zone_id AS teamZoneId, fco.venue_name AS venueName, fco.venue_city AS venueCity, " +
			"fco.venue_state AS venueState, fco.venue_country AS venueCountry, fco.platform AS platform, gc.name AS grandChildName,  "+
			"concat(c.cust_name,' ',c.last_name) AS customerName "+
			"FROM fantasy_customer_orders fco with(nolock) "+
			"inner join fantasy_grandchild_category gc with(nolock) on gc.id = fco.fantasy_grand_child_id "+
			"inner join customer c with(nolock) on c.id = fco.customer_id where 1=1 ";
		
		if(grandChildCategory != null && !grandChildCategory.isEmpty()){
			hql += " AND gc.name like '%"+grandChildCategory+"%' ";
		}
		if(status!=null && !status.isEmpty()){
			if(status.equals("Outstanding")){
				hql += " AND fco.status = '"+CrownJewelOrderStatus.OUTSTANDING+"'";
			}
			else if(status.equals("Approved")){
				hql += " AND fco.status = '"+CrownJewelOrderStatus.APPROVED+"'";
			}
			else if(status.equals("Rejected")){
				hql += " AND fco.status = '"+CrownJewelOrderStatus.REJECTED+"'";
			}
		}
		else{					
			hql += " AND (fco.status = 'OUTSTANDING' OR fco.status = 'REJECTED' OR fco.status = 'APPROVED')"; 				
		}
		
		if(orderId!=null && !orderId.isEmpty()){
			hql += " AND fco.id = "+orderId;
		}		
		if(fromDate!=null && !fromDate.isEmpty()){
			hql += " AND fco.create_date >= '"+fromDate+"'";
		}
		if(toDate!=null && !toDate.isEmpty()){
			hql += " AND fco.create_date <= '"+toDate+"'";
		}
		if(teamName!=null && !teamName.isEmpty()){
			hql += " AND fco.team_name like '%"+teamName+"%'";
		}
		if(leagueName!=null && !leagueName.isEmpty()){
			hql += " AND fco.fantasy_event_name like '%"+leagueName+"%'";
		}
		if(filter.getId() != null ){
			hql += " AND fco.id = "+filter.getId();
		}
		if(filter.getLeagueName() != null){
			hql += " AND fco.fantasy_event_name like '%"+filter.getLeagueName()+"%'";
		}
		if(filter.getTeamId() != null){
			hql += " AND fco.team_id = "+filter.getTeamId();
		}
		if(filter.getTeamName() != null){
			hql += " AND fco.team_name like '%"+filter.getTeamName()+"%'";
		}
		if(filter.getIsPackageSelected() != null){
			hql += " AND fco.package_selected = "+filter.getIsPackageSelected();
		}
		if(filter.getCost() != null){
			hql += " AND fco.package_cost = "+filter.getCost();
		}
		if(filter.getInternalNotes() != null){
			hql += " AND fco.package_notes like '%"+filter.getInternalNotes()+"%' ";
		}
		if(filter.getEventId() != null){
			hql += " AND fco.event_id = "+filter.getEventId();
		}
		if(filter.getTicketId() != null){
			hql += " AND fco.ticket_id = "+filter.getTicketId();
		}
		if(filter.getZone() != null){
			hql += " AND fco.zone like '%"+filter.getZone()+"%' ";
		}
		if(filter.getTicketQty() != null){
			hql += " AND fco.ticket_qty = "+filter.getTicketQty();
		}
		if(filter.getPrice() != null){
			hql += " AND fco.ticket_price = "+filter.getPrice();
		}
		if(filter.getRequirePoints() != null){
			hql += " AND fco.required_points = "+filter.getRequirePoints();	
		}
		if(filter.getCustomerId() != null){
			hql += " AND fco.customer_id = "+filter.getCustomerId();
		}
		if(filter.getStatus() != null){
			hql += " AND fco.status like '%"+filter.getStatus()+"%' ";
		}
		if(filter.getCreatedDateStr() != null){
			if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
				hql += " AND DATEPART(day, fco.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
			}
			if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
				hql += " AND DATEPART(month, fco.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
			}
			if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
				hql += " AND DATEPART(year, fco.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
			}
		}
		if(filter.getLastUpdatedDateStr() != null){
			if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
				hql += " AND DATEPART(day, fco.last_update) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
			}
			if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
				hql += " AND DATEPART(month, fco.last_update) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
			}
			if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
				hql += " AND DATEPART(year, fco.last_update) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
			}
		}
		if(filter.getCustomerOrderId() != null){
			hql += "AND fco.regular_order_id = "+filter.getCustomerOrderId();
		}
		if(filter.getTeamZoneId() != null){
			hql += " AND fco.team_zone_id = "+filter.getTeamZoneId();
		}
		if(filter.getVenueName() != null){
			hql += " AND fco.venue_name like '%"+filter.getVenueName()+"%' ";
		}
		if(filter.getCity() != null){
			hql += " AND fco.venue_city like '%"+filter.getCity()+"%' ";
		}
		if(filter.getState() != null){
			hql += " AND fco.venue_state like '%"+filter.getState()+"%' ";
		}
		if(filter.getCountry() != null){
			hql += " AND fco.venue_country like '%"+filter.getCountry()+"%' ";
		}
		if(filter.getPlatform() != null){
			hql += " AND fco.platform like '%"+filter.getPlatform()+"%' ";
		}
		if(filter.getGrandChildCategoryName() != null){
			hql += " AND gc.name like '%"+filter.getGrandChildCategoryName()+"%' ";
		}
		if(filter.getCustomerName() != null){
			hql += " AND (c.cust_name like '%"+filter.getCustomerName()+"%' OR c.last_name like '%"+filter.getCustomerName()+"%') ";
		}
		hql += " order by fco.id desc";
		
		startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
		SQLQuery query = getSession().createSQLQuery(hql);		
		query.addScalar("id", Hibernate.INTEGER);
		query.addScalar("fantasyEventName", Hibernate.STRING);
		query.addScalar("teamId", Hibernate.INTEGER);
		query.addScalar("teamName", Hibernate.STRING);
		query.addScalar("isPackageSelected", Hibernate.BOOLEAN);
		query.addScalar("packageCost", Hibernate.DOUBLE);
		query.addScalar("packageNote", Hibernate.STRING);
		query.addScalar("eventId", Hibernate.INTEGER);
		query.addScalar("isRealTicket", Hibernate.BOOLEAN);
		query.addScalar("ticketId", Hibernate.INTEGER);
		query.addScalar("ticketSelection", Hibernate.STRING);
		query.addScalar("ticketQty", Hibernate.INTEGER);
		query.addScalar("ticketPrice", Hibernate.DOUBLE);
		query.addScalar("requiredPoints", Hibernate.DOUBLE);
		query.addScalar("customerId", Hibernate.INTEGER);
		query.addScalar("status", Hibernate.STRING);
		query.addScalar("createdDate", Hibernate.TIMESTAMP);
		query.addScalar("lastUpdatedDate", Hibernate.TIMESTAMP);
		query.addScalar("acceptRejectReson", Hibernate.STRING);
		query.addScalar("regularOrderId", Hibernate.INTEGER);
		query.addScalar("teamZoneId", Hibernate.INTEGER);
		query.addScalar("venueName", Hibernate.STRING);
		query.addScalar("venueCity", Hibernate.STRING);
		query.addScalar("venueState", Hibernate.STRING);
		query.addScalar("venueCountry", Hibernate.STRING);
		query.addScalar("platform", Hibernate.STRING);
		query.addScalar("grandChildName", Hibernate.STRING);
		query.addScalar("customerName", Hibernate.STRING);
		return query.setResultTransformer(Transformers.aliasToBean(CrownJewelCustomerOrder.class)).setFirstResult(startFrom).setMaxResults(PaginationUtil.PAGESIZE).list();
		//return getSession().createSQLQuery(hql).setFirstResult(startFrom).setMaxResults(PaginationUtil.PAGESIZE).list();
	}
	
	@Override
	public List<CrownJewelCustomerOrder> getCrownJewelOrdersByTeamId(Integer teamId) throws Exception {
		return find("FROM CrownJewelCustomerOrder WHERE teamId=?",new Object[]{teamId});
	}

	@Override
	public List<CrownJewelCustomerOrder> getCrownJewelOrdersByLeagueId(Integer fantasyEventId) throws Exception {
		return find("FROM CrownJewelCustomerOrder WHERE fantasyEventId=?",new Object[]{fantasyEventId});
	}
	
	public List<CrownJewelCustomerOrder> getCrownJewelOrdersByFilterToExport(String orderId, String fromDate, String toDate, String parentCategory, String grandChildCategory, String teamName, String leagueName, String status, GridHeaderFilters filter)throws Exception {
		
		String hql="Select fco.id AS id, fco.fantasy_event_name AS fantasyEventName, fco.team_id AS teamId, fco.team_name AS teamName, " +
			"fco.package_selected AS isPackageSelected, fco.package_cost AS packageCost, fco.package_notes AS packageNote, fco.event_id AS eventId, " +
			"fco.is_real_ticket as isRealTicket, fco.ticket_id AS ticketId, fco.zone AS ticketSelection, fco.ticket_qty AS ticketQty, " +
			"fco.ticket_price AS ticketPrice, fco.required_points AS requiredPoints, fco.customer_id AS customerId, fco.status AS status, " +			
			"fco.create_date AS createdDate, fco.last_update AS lastUpdatedDate, fco.accept_reject_reason AS acceptRejectReson," +
			"fco.regular_order_id AS regularOrderId, fco.team_zone_id AS teamZoneId, fco.venue_name AS venueName, fco.venue_city AS venueCity, " +
			"fco.venue_state AS venueState, fco.venue_country AS venueCountry, fco.platform AS platform, gc.name AS grandChildName,  "+
			"concat(c.cust_name,' ',c.last_name) AS customerName "+
			"FROM fantasy_customer_orders fco with(nolock) "+
			"inner join fantasy_grandchild_category gc with(nolock) on gc.id = fco.fantasy_grand_child_id "+
			"inner join customer c with(nolock) on c.id = fco.customer_id where 1=1 ";
		
		/*if(parentCategory!=null && !parentCategory.isEmpty()){
			hql += " AND league_parent_type='"+parentCategory+"' ";
		}*/
		if(grandChildCategory != null && !grandChildCategory.isEmpty()){
			hql += " AND gc.name like '%"+grandChildCategory+"%' ";
		}
		if(status!=null && !status.isEmpty()){
			if(status.equals("Outstanding")){
				hql += " AND fco.status = '"+CrownJewelOrderStatus.OUTSTANDING+"'";
			}
			else if(status.equals("Approved")){
				hql += " AND fco.status = '"+CrownJewelOrderStatus.APPROVED+"'";
			}
			else if(status.equals("Rejected")){
				hql += " AND fco.status = '"+CrownJewelOrderStatus.REJECTED+"'";
			}
		}
		else{					
			hql += " AND (fco.status = 'OUTSTANDING' OR fco.status = 'REJECTED' OR fco.status = 'APPROVED')"; 				
		}
		
		if(orderId!=null && !orderId.isEmpty()){
			hql += " AND fco.id = "+orderId;
		}		
		if(fromDate!=null && !fromDate.isEmpty()){
			hql += " AND fco.create_date >= '"+fromDate+"'";
		}
		if(toDate!=null && !toDate.isEmpty()){
			hql += " AND fco.create_date <= '"+toDate+"'";
		}
		if(teamName!=null && !teamName.isEmpty()){
			hql += " AND fco.team_name like '%"+teamName+"%'";
		}
		if(leagueName!=null && !leagueName.isEmpty()){
			hql += " AND fco.fantasy_event_name like '%"+leagueName+"%'";
		}
		if(filter.getId() != null ){
			hql += " AND fco.id = "+filter.getId();
		}
		if(filter.getLeagueName() != null){
			hql += " AND fco.fantasy_event_name like '%"+filter.getLeagueName()+"%'";
		}
		if(filter.getTeamId() != null){
			hql += " AND fco.team_id = "+filter.getTeamId();
		}
		if(filter.getTeamName() != null){
			hql += " AND fco.team_name like '%"+filter.getTeamName()+"%'";
		}
		if(filter.getIsPackageSelected() != null){
			hql += " AND fco.package_selected = "+filter.getIsPackageSelected();
		}
		if(filter.getCost() != null){
			hql += " AND fco.package_cost = "+filter.getCost();
		}
		if(filter.getInternalNotes() != null){
			hql += " AND fco.package_notes like '%"+filter.getInternalNotes()+"%' ";
		}
		if(filter.getEventId() != null){
			hql += " AND fco.event_id = "+filter.getEventId();
		}
		if(filter.getTicketId() != null){
			hql += " AND fco.ticket_id = "+filter.getTicketId();
		}
		if(filter.getZone() != null){
			hql += " AND fco.zone like '%"+filter.getZone()+"%' ";
		}
		if(filter.getTicketQty() != null){
			hql += " AND fco.ticket_qty = "+filter.getTicketQty();
		}
		if(filter.getPrice() != null){
			hql += " AND fco.ticket_price = "+filter.getPrice();
		}
		if(filter.getRequirePoints() != null){
			hql += " AND fco.required_points = "+filter.getRequirePoints();	
		}
		if(filter.getCustomerId() != null){
			hql += " AND fco.customer_id = "+filter.getCustomerId();
		}
		if(filter.getStatus() != null){
			hql += " AND fco.status like '%"+filter.getStatus()+"%' ";
		}
		if(filter.getCreatedDateStr() != null){
			if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
				hql += " AND DATEPART(day, fco.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
			}
			if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
				hql += " AND DATEPART(month, fco.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
			}
			if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
				hql += " AND DATEPART(year, fco.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
			}
		}
		if(filter.getLastUpdatedDateStr() != null){
			if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
				hql += " AND DATEPART(day, fco.last_update) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
			}
			if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
				hql += " AND DATEPART(month, fco.last_update) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
			}
			if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
				hql += " AND DATEPART(year, fco.last_update) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
			}
		}
		if(filter.getCustomerOrderId() != null){
			hql += "AND fco.regular_order_id = "+filter.getCustomerOrderId();
		}
		if(filter.getTeamZoneId() != null){
			hql += " AND fco.team_zone_id = "+filter.getTeamZoneId();
		}
		if(filter.getVenueName() != null){
			hql += " AND fco.venue_name like '%"+filter.getVenueName()+"%' ";
		}
		if(filter.getCity() != null){
			hql += " AND fco.venue_city like '%"+filter.getCity()+"%' ";
		}
		if(filter.getState() != null){
			hql += " AND fco.venue_state like '%"+filter.getState()+"%' ";
		}
		if(filter.getCountry() != null){
			hql += " AND fco.venue_country like '%"+filter.getCountry()+"%' ";
		}
		if(filter.getPlatform() != null){
			hql += " AND fco.platform like '%"+filter.getPlatform()+"%' ";
		}
		if(filter.getGrandChildCategoryName() != null){
			hql += " AND gc.name like '%"+filter.getGrandChildCategoryName()+"%' ";
		}
		if(filter.getCustomerName() != null){
			hql += " AND (c.cust_name like '%"+filter.getCustomerName()+"%' OR c.last_name like '%"+filter.getCustomerName()+"%') ";
		}
		hql += " order by fco.id desc";
		
		SQLQuery query = getSession().createSQLQuery(hql);
		query.addScalar("id", Hibernate.INTEGER);
		query.addScalar("fantasyEventName", Hibernate.STRING);
		query.addScalar("teamId", Hibernate.INTEGER);
		query.addScalar("teamName", Hibernate.STRING);
		query.addScalar("isPackageSelected", Hibernate.BOOLEAN);
		query.addScalar("packageCost", Hibernate.DOUBLE);
		query.addScalar("packageNote", Hibernate.STRING);
		query.addScalar("eventId", Hibernate.INTEGER);
		query.addScalar("isRealTicket", Hibernate.BOOLEAN);
		query.addScalar("ticketId", Hibernate.INTEGER);
		query.addScalar("ticketSelection", Hibernate.STRING);
		query.addScalar("ticketQty", Hibernate.INTEGER);
		query.addScalar("ticketPrice", Hibernate.DOUBLE);
		query.addScalar("requiredPoints", Hibernate.DOUBLE);
		query.addScalar("customerId", Hibernate.INTEGER);
		query.addScalar("status", Hibernate.STRING);
		query.addScalar("createdDate", Hibernate.TIMESTAMP);
		query.addScalar("lastUpdatedDate", Hibernate.TIMESTAMP);
		query.addScalar("acceptRejectReson", Hibernate.STRING);
		query.addScalar("regularOrderId", Hibernate.INTEGER);
		query.addScalar("teamZoneId", Hibernate.INTEGER);
		query.addScalar("venueName", Hibernate.STRING);
		query.addScalar("venueCity", Hibernate.STRING);
		query.addScalar("venueState", Hibernate.STRING);
		query.addScalar("venueCountry", Hibernate.STRING);
		query.addScalar("platform", Hibernate.STRING);
		query.addScalar("grandChildName", Hibernate.STRING);
		query.addScalar("customerName", Hibernate.STRING);
		return query.setResultTransformer(Transformers.aliasToBean(CrownJewelCustomerOrder.class)).list();
		//return getSession().createSQLQuery(hql).setFirstResult(startFrom).setMaxResults(PaginationUtil.PAGESIZE).list();
	}
}
