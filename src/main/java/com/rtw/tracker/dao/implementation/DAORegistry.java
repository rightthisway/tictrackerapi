package com.rtw.tracker.dao.implementation;

import com.rtw.tracker.dao.services.AffiliateCashRewardDAO;
import com.rtw.tracker.dao.services.AffiliateCashRewardHistoryDAO;
import com.rtw.tracker.dao.services.AffiliatePromoCodeHistoryDAO;
import com.rtw.tracker.dao.services.AffiliateSettingDAO;
import com.rtw.tracker.dao.services.ArtistAuditDAO;
import com.rtw.tracker.dao.services.ArtistImageAuditDAO;
import com.rtw.tracker.dao.services.ArtistImageDAO;
import com.rtw.tracker.dao.services.AutoCatsLockedTicketsDAO;
import com.rtw.tracker.dao.services.CardsDAO;
import com.rtw.tracker.dao.services.CategoryTicketDAO;
import com.rtw.tracker.dao.services.CategoryTicketGroupDAO;
import com.rtw.tracker.dao.services.ChildCategoryImageAuditDAO;
import com.rtw.tracker.dao.services.ChildCategoryImageDAO;
import com.rtw.tracker.dao.services.ContestAffiliatesDAO;
import com.rtw.tracker.dao.services.CountryDAO;
import com.rtw.tracker.dao.services.CrownJewelCategoryTicketDAO;
import com.rtw.tracker.dao.services.CrownJewelCustomerOrderDAO;
import com.rtw.tracker.dao.services.CrownJewelLeaguesDAO;
import com.rtw.tracker.dao.services.CrownJewelTeamZonesAuditDAO;
import com.rtw.tracker.dao.services.CrownJewelTeamZonesDAO;
import com.rtw.tracker.dao.services.CrownJewelTeamsDAO;
import com.rtw.tracker.dao.services.CrownJewelZonesSoldDetailsDAO;
import com.rtw.tracker.dao.services.CustomerDAO;
import com.rtw.tracker.dao.services.CustomerLoyalFanAuditDAO;
import com.rtw.tracker.dao.services.CustomerLoyalFanDAO;
import com.rtw.tracker.dao.services.CustomerLoyaltyDAO;
import com.rtw.tracker.dao.services.CustomerLoyaltyTrackingDAO;
import com.rtw.tracker.dao.services.CustomerStripeCreditCardDAO;
import com.rtw.tracker.dao.services.CustomerTicketDownloadsDAO;
import com.rtw.tracker.dao.services.CustomerWalletTrackingDAO;
import com.rtw.tracker.dao.services.EmailBlastTrackingDAO;
import com.rtw.tracker.dao.services.EmailTemplateDAO;
import com.rtw.tracker.dao.services.EventAuditDAO;
import com.rtw.tracker.dao.services.EventDAO;
import com.rtw.tracker.dao.services.FantasyChildCategoryDAO;
import com.rtw.tracker.dao.services.FantasyGrandChildCategoryDAO;
import com.rtw.tracker.dao.services.FantasyParentCategoryDAO;
import com.rtw.tracker.dao.services.FavouriteEventsDAO;
import com.rtw.tracker.dao.services.GrandChildCategoryAuditDAO;
import com.rtw.tracker.dao.services.GrandChildCategoryImageAuditDAO;
import com.rtw.tracker.dao.services.GrandChildCategoryImageDAO;
import com.rtw.tracker.dao.services.InvoiceTicketAttachmentDAO;
import com.rtw.tracker.dao.services.LoyaltySettingsDAO;
import com.rtw.tracker.dao.services.ManualFedexGenerationDAO;
import com.rtw.tracker.dao.services.OpenOrderStatusDao;
import com.rtw.tracker.dao.services.OrderTicketGroupDetailsDAO;
import com.rtw.tracker.dao.services.POSCategoryTicketDAO;
import com.rtw.tracker.dao.services.POSCategoryTicketGroupDAO;
import com.rtw.tracker.dao.services.POSCustomerOrderDAO;
import com.rtw.tracker.dao.services.POSEventDAO;
import com.rtw.tracker.dao.services.POSInvoiceDAO;
import com.rtw.tracker.dao.services.POSPurchaseOrderDAO;
import com.rtw.tracker.dao.services.POSTicketDAO;
import com.rtw.tracker.dao.services.POSTicketGroupDAO;
import com.rtw.tracker.dao.services.ParentCategoryImageAuditDAO;
import com.rtw.tracker.dao.services.ParentCategoryImageDAO;
import com.rtw.tracker.dao.services.PayPalTrackingDAO;
import com.rtw.tracker.dao.services.PollingCategoryQuestionDAO;
import com.rtw.tracker.dao.services.PopularEventsDAO;
import com.rtw.tracker.dao.services.ProductDAO;
import com.rtw.tracker.dao.services.PromotionalEarnLifeOfferDAO;
import com.rtw.tracker.dao.services.PurchaseOrderPaymentDetailsDAO;
import com.rtw.tracker.dao.services.RTFCustomerPromotionalOfferDAO;
import com.rtw.tracker.dao.services.RTFPromoOffersDAO;
import com.rtw.tracker.dao.services.RTFPromoTypeDAO;
import com.rtw.tracker.dao.services.RTFPromotionalOfferTrackingDAO;
import com.rtw.tracker.dao.services.ReferralContestDAO;
import com.rtw.tracker.dao.services.ReferralContestParticipantsDAO;
import com.rtw.tracker.dao.services.ReferralContestWinnerDAO;
import com.rtw.tracker.dao.services.RoleDAO;
import com.rtw.tracker.dao.services.RtfCatsTicketDAO;
import com.rtw.tracker.dao.services.SeatGeekOrdersDAO;
import com.rtw.tracker.dao.services.ShippingMethodDAO;
import com.rtw.tracker.dao.services.ShowNearTermOptionDAO;
import com.rtw.tracker.dao.services.SoldTicketDetailDAO;
import com.rtw.tracker.dao.services.StateDAO;
import com.rtw.tracker.dao.services.TicketDAO;
import com.rtw.tracker.dao.services.TicketGroupTicketAttachmentDAO;
import com.rtw.tracker.dao.services.TrackerBrokersDAO;
import com.rtw.tracker.dao.services.TrackerUserDAO;
import com.rtw.tracker.dao.services.UserActionDAO;
import com.rtw.tracker.dao.services.UserPreferenceDAO;
import com.rtw.tracker.dao.services.VenueAuditDAO;
import com.rtw.tracker.fedex.FedExLabelLogDAO;
import com.rtw.tracker.fedex.PropertyDAO;

public class DAORegistry {
	
	private static TrackerUserDAO trackerUserDAO;
	private static RoleDAO roleDAO;
	private static UserActionDAO userActionDAO;
	private static QueryManagerDAO queryManagerDAO;
	private static CustomerDAO customerDAO;
	private static CustomerAddressDAO customerAddressDAO;
	private static CountryDAO countryDAO;
	private static StateDAO stateDAO;
	private static EventDetailsDAO eventDetailsDAO;
	private static PopularEventsDAO popularEventsDAO;
	private static CategoryTicketGroupDAO categoryTicketGroupDAO;
	private static PurchaseOrderDAO purchaseOrderDAO;
	private static PurchaseOrderPaymentDetailsDAO purchaseOrderPaymentDetailsDAO;
	private static TicketGroupDAO ticketGroupDAO;
	private static InvoiceDAO invoiceDAO;
	private static CustomerOrderDAO customerOrderDAO;
	private static GrandChildCategoryDAO grandChildCategoryDAO;
	private static ChildCategoryDAO childCategoryDAO;
	private static ParentCategoryDAO parentCategoryDAO;
	private static ArtistDAO artistDAO;
	private static VenueDAO venueDAO;
	private static PopularArtistDAO popularArtistDAO;
	private static PopularGrandChildCategoryDAO popularGrandChildCategoryDAO;
	private static PopularVenueDAO popularVenueDAO;
	private static ProductDAO productDAO;
	private static CardsDAO cardsDAO;
	private static CrownJewelTeamsDAO crownJewelTeamsDAO;
	private static CrownJewelTeamsAuditDAO crownJewelTeamsAuditDAO;
	private static CrownJewelLeaguesDAO crownJewelLeaguesDAO;
	private static CrownJewelLeaguesAuditDAO crownJewelLeaguesAuditDAO;
	private static CrownJewelCategoryTicketDAO crownJewelCategoryTicketDAO;
	private static CrownJewelTeamZonesDAO crownJewelTeamZonesDAO;
	private static CrownJewelCustomerOrderDAO crownJewelCustomerOrderDAO;
	private static CrownJewelTeamZonesAuditDAO crownJewelTeamZonesAuditDAO;
	private static CustomerOrderDetailsDAO customerOrderDetailsDAO;
	private static ArtistImageDAO artistImageDAO;
	private static ArtistImageAuditDAO artistImageAuditDAO;
	private static GrandChildCategoryImageDAO grandChildCategoryImageDAO;
	private static GrandChildCategoryImageAuditDAO grandChildCategoryImageAuditDAO;
	private static ChildCategoryImageDAO childCategoryImageDAO;
	private static ChildCategoryImageAuditDAO childCategoryImageAuditDAO;
	private static ParentCategoryImageDAO parentCategoryImageDAO;
	private static ParentCategoryImageAuditDAO parentCategoryImageAuditDAO;
	private static FantasyGrandChildCategoryImageDAO fantasyGrandChildCategoryImageDAO;
	private static LoyalFanParentCategoryImageDAO loyalFanParentCategoryImageDAO;
	private static OpenOrderStatusDao openOrderStatusDao;
	private static SoldTicketDetailDAO soldTicketDetailDAO;
	private static ShippingMethodDAO shippingMethodDAO;
	private static ShowNearTermOptionDAO showNearTermOptionDAO;
	private static UserPreferenceDAO userPreferenceDAO;
	private static SeatGeekOrdersDAO seatGeekOrdersDAO;
	private static InvoiceTicketAttachmentDAO invoiceTicketAttachmentDAO;
	private static CategoryTicketDAO categoryTicketDAO;
	private static TicketDAO ticketDAO;
	private static CustomerStripeCreditCardDAO customerStripeCreditCardDAO;
	private static InvoiceRefundDAO invoiceRefundDAO;
	private static InventoryHoldDAO inventoryHoldDAO;
	private static StripeDisputeDAO stripeDisputeDAO;
	private static AffiliateSettingDAO affiliateSettingDAO;
	
	//Loyalty Rewards
	private static LoyaltySettingsDAO loyaltySettingsDAO;
	private static CustomerLoyaltyDAO customerLoyaltyDAO;
	private static CustomerLoyaltyHistoryDAO customerLoyaltyHistoryDAO;
	//private static CustomerSuperFanDAO customerSuperFanDAO;
	private static CustomerLoyalFanDAO customerLoyalFanDAO;
	private static CustomerLoyalFanAuditDAO customerLoyalFanAuditDAO;
	private static RTFPromotionalOfferDAO rtfPromotionalOfferDAO;
	
	//Fedex
	private static PropertyDAO propertyDAO;
	private static FedExLabelLogDAO fedExLabelLogDAO;
	private static ManualFedexGenerationDAO manualFedexGenerationDAO;
	
	//Audit
	private static EventAuditDAO eventAuditDAO;
	private static ArtistAuditDAO artistAuditDAO;
	private static VenueAuditDAO venueAuditDAO;
	private static GrandChildCategoryAuditDAO grandChildCategoryAuditDAO;
	
	//DownloadTicket
	private static CustomerTicketDownloadsDAO customerTicketDownloadsDAO;
	
	//For Product Type RTW/RTW2
	private static POSCategoryTicketDAO posCategoryTicketDAO;
	private static POSCategoryTicketGroupDAO posCategoryTicketGroupDAO;
	private static POSTicketGroupDAO posTicketGroupDAO;
	private static POSTicketDAO posTicketDAO;
	private static POSPurchaseOrderDAO posPurchaseOrderDAO;
	private static POSCustomerOrderDAO posCustomerOrderDAO;
	private static POSInvoiceDAO posInvoiceDAO;
	private static POSEventDAO posEventDAO;
	
	private static CustomerWalletDAO customerWalletDAO;
	private static CustomerWalletHistoryDAO customerWalletHistoryDAO;
	
	private static PayPalTrackingDAO paypalTrackingDAO;
	
	private static EventDAO eventDAO;
	private static CrownJewelZonesSoldDetailsDAO crownJewelZonesSoldDetailsDAO;
	
	//Fantasy Tickets
	private static FantasyParentCategoryDAO fantasyParentCategoryDAO;
	private static FantasyChildCategoryDAO fantasyChildCategoryDAO;
	private static FantasyGrandChildCategoryDAO fantasyGrandChildCategoryDAO;
	private static CrownJewelCategoryTeamsDAO crownJewelCategoryTeamsDAO;
	private static CrownJewelCategoryTeamsAuditDAO crownJewelCategoryTeamsAuditDAO;
	private static InvoiceAuditDAO invoiceAuditDAO;
	
	private static FavouriteEventsDAO favouriteEventsDAO;
	private static TicketGroupTicketAttachmentDAO ticketGroupTicketAttachmentDAO;
	private static TrackerBrokersDAO trackerBrokersDAO;
	private static AffiliateCashRewardDAO affiliateCashRewardDAO;
	private static AffiliateCashRewardHistoryDAO affiliateCashRewardHistoryDAO;
	private static AffiliatePromoCodeHistoryDAO affiliatePromoCodeHistoryDAO;
	private static AutoCatsLockedTicketsDAO autoCatsLockedTicketsDAO;
	private static RTFPromoOffersDAO rtfPromoOffersDAO;
	private static RTFPromoTypeDAO rtfPromoTypeDAO;
	private static RTFCustomerPromotionalOfferDAO rtfCustomerPromotionalOfferDAO;
	private static RTFPromotionalOfferTrackingDAO rtfPromotionalOfferTrackingDAO;
	private static PromotionalEarnLifeOfferDAO promotionalEarnLifeOfferDAO;
		
	private static CustomerWalletTrackingDAO customerWalletTrackingDAO;
	private static CustomerLoyaltyTrackingDAO customerLoyaltyTrackingDAO;
	private static DiscountCodeTrackingDAO discountCodeTrackingDAO;
	private static EmailTemplateDAO emailTemplateDAO;
	private static EmailBlastTrackingDAO emailBlastTrackingDAO;
	private static OrderTicketGroupDetailsDAO orderTicketGroupDetailsDAO;		
	
	private static ReferralContestDAO referralContestDAO;
	private static ReferralContestParticipantsDAO referralContestParticipantsDAO;
	private static ReferralContestWinnerDAO referralContestWinnerDAO; 
	private static ContestAffiliatesDAO contestAffiliatesDAO;
	private static RtfCatsTicketDAO rtfCatsTicketDAO;
	
	private static RtfGiftCardDAO rtfGiftCardDAO;
	private static RtfGiftCardQuantityDAO rtfGiftCardQuantityDAO;
	private static GiftCardOrderDAO giftCardOrderDAO;
	private static GiftCardOrderAttachmentDAO giftCardOrderAttachmentDAO;
	private static GiftCardBrandDAO giftCardBrandDAO;

	
	// Polling Contest 
	private static PollingSponsorDAO pollingSponsorDAO;
	private static PollingCategoryDAO pollingCategoryDAO;
	private static PollingVideoCategoryDAO pollingVideoCategoryDAO;
	private static PollingCategoryQuestionDAO pollingCategoryQuestionDAO;
	private static PollingContestDAO pollingContestDAO;
	private static PollingRewardsDAO pollingRewardsDAO;
	private static PollingContestCategoryMapperDAO  pollingContestCategoryMapperDAO;
	private static PollingVideoInventoryDAO pollingVideoInventoryDAO;
	private static PollingCustomerVideoDAO pollingCustomerVideoDAO;
	private static TicketMasterRefundDAO ticketMasterRefundDAO;
	private static TicketMasterRefundKnownEmailDAO ticketMasterRefundKnownEmailDAO;
	private static TicketMasterRefundCCNotMatchedDAO ticketMasterRefundCCNotMatchedDAO;
	

	public static final ReferralContestDAO getReferralContestDAO() {
		return referralContestDAO;
	}

	public static void setReferralContestDAO(ReferralContestDAO referralContestDAO) {
		DAORegistry.referralContestDAO = referralContestDAO;
	}

	public static final ReferralContestParticipantsDAO getReferralContestParticipantsDAO() {
		return referralContestParticipantsDAO;
	}

	public static void setReferralContestParticipantsDAO(
			ReferralContestParticipantsDAO referralContestParticipantsDAO) {
		DAORegistry.referralContestParticipantsDAO = referralContestParticipantsDAO;
	}

	public static final ReferralContestWinnerDAO getReferralContestWinnerDAO() {
		return referralContestWinnerDAO;
	}

	public static void setReferralContestWinnerDAO(
			ReferralContestWinnerDAO referralContestWinnerDAO) {
		DAORegistry.referralContestWinnerDAO = referralContestWinnerDAO;
	}

	public static final OrderTicketGroupDetailsDAO getOrderTicketGroupDetailsDAO() {
		return orderTicketGroupDetailsDAO;
	}

	public static void setOrderTicketGroupDetailsDAO(
			OrderTicketGroupDetailsDAO orderTicketGroupDetailsDAO) {
		DAORegistry.orderTicketGroupDetailsDAO = orderTicketGroupDetailsDAO;
	}

	public static final ManualFedexGenerationDAO getManualFedexGenerationDAO() {
		return manualFedexGenerationDAO;
	}

	public static void setManualFedexGenerationDAO(
			ManualFedexGenerationDAO manualFedexGenerationDAO) {
		DAORegistry.manualFedexGenerationDAO = manualFedexGenerationDAO;
	}

	public static final EmailBlastTrackingDAO getEmailBlastTrackingDAO() {
		return emailBlastTrackingDAO;
	}

	public static void setEmailBlastTrackingDAO(
			EmailBlastTrackingDAO emailBlastTrackingDAO) {
		DAORegistry.emailBlastTrackingDAO = emailBlastTrackingDAO;
	}

	public static final EmailTemplateDAO getEmailTemplateDAO() {
		return emailTemplateDAO;
	}

	public static void setEmailTemplateDAO(EmailTemplateDAO emailTemplateDAO) {
		DAORegistry.emailTemplateDAO = emailTemplateDAO;
	}

	public static final DiscountCodeTrackingDAO getDiscountCodeTrackingDAO() {
		return discountCodeTrackingDAO;
	}

	public static void setDiscountCodeTrackingDAO(
			DiscountCodeTrackingDAO discountCodeTrackingDAO) {
		DAORegistry.discountCodeTrackingDAO = discountCodeTrackingDAO;
	}

	public static final CustomerWalletTrackingDAO getCustomerWalletTrackingDAO() {
		return customerWalletTrackingDAO;
	}

	public static void setCustomerWalletTrackingDAO(
			CustomerWalletTrackingDAO customerWalletTrackingDAO) {
		DAORegistry.customerWalletTrackingDAO = customerWalletTrackingDAO;
	}

	public static final CustomerLoyaltyTrackingDAO getCustomerLoyaltyTrackingDAO() {
		return customerLoyaltyTrackingDAO;
	}

	public static void setCustomerLoyaltyTrackingDAO(
			CustomerLoyaltyTrackingDAO customerLoyaltyTrackingDAO) {
		DAORegistry.customerLoyaltyTrackingDAO = customerLoyaltyTrackingDAO;
	}

	public static AffiliateSettingDAO getAffiliateSettingDAO() {
		return affiliateSettingDAO;
	}

	public final void setAffiliateSettingDAO(
			AffiliateSettingDAO affiliateSettingDAO) {
		DAORegistry.affiliateSettingDAO = affiliateSettingDAO;
	}
	
	public static final CustomerLoyalFanAuditDAO getCustomerLoyalFanAuditDAO() {
		return customerLoyalFanAuditDAO;
	}

	public static void setCustomerLoyalFanAuditDAO(
			CustomerLoyalFanAuditDAO customerLoyalFanAuditDAO) {
		DAORegistry.customerLoyalFanAuditDAO = customerLoyalFanAuditDAO;
	}

	public static final CustomerLoyalFanDAO getCustomerLoyalFanDAO() {
		return customerLoyalFanDAO;
	}

	public static void setCustomerLoyalFanDAO(
			CustomerLoyalFanDAO customerLoyalFanDAO) {
		DAORegistry.customerLoyalFanDAO = customerLoyalFanDAO;
	}

	public static final RTFPromotionalOfferTrackingDAO getRtfPromotionalOfferTrackingDAO() {
		return rtfPromotionalOfferTrackingDAO;
	}

	public static void setRtfPromotionalOfferTrackingDAO(
			RTFPromotionalOfferTrackingDAO rtfPromotionalOfferTrackingDAO) {
		DAORegistry.rtfPromotionalOfferTrackingDAO = rtfPromotionalOfferTrackingDAO;
	}

	public static final RTFCustomerPromotionalOfferDAO getRtfCustomerPromotionalOfferDAO() {
		return rtfCustomerPromotionalOfferDAO;
	}

	public static void setRtfCustomerPromotionalOfferDAO(
			RTFCustomerPromotionalOfferDAO rtfCustomerPromotionalOfferDAO) {
		DAORegistry.rtfCustomerPromotionalOfferDAO = rtfCustomerPromotionalOfferDAO;
	}

	public static final RTFPromoOffersDAO getRtfPromoOffersDAO() {
		return rtfPromoOffersDAO;
	}

	public static void setRtfPromoOffersDAO(RTFPromoOffersDAO rtfPromoOffersDAO) {
		DAORegistry.rtfPromoOffersDAO = rtfPromoOffersDAO;
	}

	public static final PromotionalEarnLifeOfferDAO getPromotionalEarnLifeOfferDAO() {
		return promotionalEarnLifeOfferDAO;
	}

	public static void setPromotionalEarnLifeOfferDAO(
			PromotionalEarnLifeOfferDAO promotionalEarnLifeOfferDAO) {
		DAORegistry.promotionalEarnLifeOfferDAO = promotionalEarnLifeOfferDAO;
	}

	public static final RTFPromoTypeDAO getRtfPromoTypeDAO() {
		return rtfPromoTypeDAO;
	}

	public static void setRtfPromoTypeDAO(RTFPromoTypeDAO rtfPromoTypeDAO) {
		DAORegistry.rtfPromoTypeDAO = rtfPromoTypeDAO;
	}

	public static final TicketGroupTicketAttachmentDAO getTicketGroupTicketAttachmentDAO() {
		return ticketGroupTicketAttachmentDAO;
	}

	public static void setTicketGroupTicketAttachmentDAO(
			TicketGroupTicketAttachmentDAO ticketGroupTicketAttachmentDAO) {
		DAORegistry.ticketGroupTicketAttachmentDAO = ticketGroupTicketAttachmentDAO;
	}

	public static final FavouriteEventsDAO getFavouriteEventsDAO() {
		return favouriteEventsDAO;
	}

	public static void setFavouriteEventsDAO(FavouriteEventsDAO favouriteEventsDAO) {
		DAORegistry.favouriteEventsDAO = favouriteEventsDAO;
	}

	public static final FantasyParentCategoryDAO getFantasyParentCategoryDAO() {
		return fantasyParentCategoryDAO;
	}

	public static void setFantasyParentCategoryDAO(
			FantasyParentCategoryDAO fantasyParentCategoryDAO) {
		DAORegistry.fantasyParentCategoryDAO = fantasyParentCategoryDAO;
	}

	public static final FantasyChildCategoryDAO getFantasyChildCategoryDAO() {
		return fantasyChildCategoryDAO;
	}

	public static void setFantasyChildCategoryDAO(
			FantasyChildCategoryDAO fantasyChildCategoryDAO) {
		DAORegistry.fantasyChildCategoryDAO = fantasyChildCategoryDAO;
	}

	public static final FantasyGrandChildCategoryDAO getFantasyGrandChildCategoryDAO() {
		return fantasyGrandChildCategoryDAO;
	}

	public static void setFantasyGrandChildCategoryDAO(
			FantasyGrandChildCategoryDAO fantasyGrandChildCategoryDAO) {
		DAORegistry.fantasyGrandChildCategoryDAO = fantasyGrandChildCategoryDAO;
	}

	public static final CrownJewelZonesSoldDetailsDAO getCrownJewelZonesSoldDetailsDAO() {
		return crownJewelZonesSoldDetailsDAO;
	}

	public static void setCrownJewelZonesSoldDetailsDAO(
			CrownJewelZonesSoldDetailsDAO crownJewelZonesSoldDetailsDAO) {
		DAORegistry.crownJewelZonesSoldDetailsDAO = crownJewelZonesSoldDetailsDAO;
	}

	public static final POSEventDAO getPosEventDAO() {
		return posEventDAO;
	}

	public static void setPosEventDAO(POSEventDAO posEventDAO) {
		DAORegistry.posEventDAO = posEventDAO;
	}

	public static final  POSCustomerOrderDAO getPosCustomerOrderDAO() {
		return posCustomerOrderDAO;
	}

	public static void setPosCustomerOrderDAO(
			POSCustomerOrderDAO posCustomerOrderDAO) {
		DAORegistry.posCustomerOrderDAO = posCustomerOrderDAO;
	}

	public static final EventDAO getEventDAO() {
		return eventDAO;
	}

	public static void setEventDAO(EventDAO eventDAO) {
		DAORegistry.eventDAO = eventDAO;
	}

	public static final PayPalTrackingDAO getPaypalTrackingDAO() {
		return paypalTrackingDAO;
	}

	public static void setPaypalTrackingDAO(PayPalTrackingDAO paypalTrackingDAO) {
		DAORegistry.paypalTrackingDAO = paypalTrackingDAO;
	}
	
	public static final CustomerWalletDAO getCustomerWalletDAO() {
		return customerWalletDAO;
	}

	public final void setCustomerWalletDAO(CustomerWalletDAO customerWalletDAO) {
		DAORegistry.customerWalletDAO = customerWalletDAO;
	}

	public static final CustomerWalletHistoryDAO getCustomerWalletHistoryDAO() {
		return customerWalletHistoryDAO;
	}

	public final void setCustomerWalletHistoryDAO(
			CustomerWalletHistoryDAO customerWalletHistoryDAO) {
		DAORegistry.customerWalletHistoryDAO = customerWalletHistoryDAO;
	}

	public static final POSPurchaseOrderDAO getPosPurchaseOrderDAO() {
		return posPurchaseOrderDAO;
	}

	public final void setPosPurchaseOrderDAO(
			POSPurchaseOrderDAO posPurchaseOrderDAO) {
		DAORegistry.posPurchaseOrderDAO = posPurchaseOrderDAO;
	}

	public static final POSCategoryTicketDAO getPosCategoryTicketDAO() {
		return posCategoryTicketDAO;
	}
	
	public static POSInvoiceDAO getPosInvoiceDAO() {
		return posInvoiceDAO;
	}

	public final void setPosInvoiceDAO(POSInvoiceDAO posInvoiceDAO) {
		DAORegistry.posInvoiceDAO = posInvoiceDAO;
	}

	public final void setPosCategoryTicketDAO(
			POSCategoryTicketDAO posCategoryTicketDAO) {
		DAORegistry.posCategoryTicketDAO = posCategoryTicketDAO;
	}

	public static final POSCategoryTicketGroupDAO getPosCategoryTicketGroupDAO() {
		return posCategoryTicketGroupDAO;
	}

	public final void setPosCategoryTicketGroupDAO(
			POSCategoryTicketGroupDAO posCategoryTicketGroupDAO) {
		DAORegistry.posCategoryTicketGroupDAO = posCategoryTicketGroupDAO;
	}

	public static final POSTicketGroupDAO getPosTicketGroupDAO() {
		return posTicketGroupDAO;
	}

	public final void setPosTicketGroupDAO(POSTicketGroupDAO posTicketGroupDAO) {
		DAORegistry.posTicketGroupDAO = posTicketGroupDAO;
	}

	public static final POSTicketDAO getPosTicketDAO() {
		return posTicketDAO;
	}

	public final void setPosTicketDAO(POSTicketDAO posTicketDAO) {
		DAORegistry.posTicketDAO = posTicketDAO;
	}

	public static final TrackerUserDAO getTrackerUserDAO() {
		return trackerUserDAO;
	}

	public final void setTrackerUserDAO(TrackerUserDAO trackerUserDAO) {
		DAORegistry.trackerUserDAO = trackerUserDAO;
	}

	public static RoleDAO getRoleDAO() {
		return roleDAO;
	}

	public final void setRoleDAO(RoleDAO roleDAO) {
		DAORegistry.roleDAO = roleDAO;
	}

	public static UserActionDAO getUserActionDAO() {
		return userActionDAO;
	}

	public final void setUserActionDAO(UserActionDAO userActionDAO) {
		DAORegistry.userActionDAO = userActionDAO;
	}

	public static QueryManagerDAO getQueryManagerDAO() {
		return queryManagerDAO;
	}

	public final void setQueryManagerDAO(QueryManagerDAO queryManagerDAO) {
		DAORegistry.queryManagerDAO = queryManagerDAO;
	}

	public static CustomerDAO getCustomerDAO() {
		return customerDAO;
	}

	public final void setCustomerDAO(CustomerDAO customerDAO) {
		DAORegistry.customerDAO = customerDAO;
	}

	public static CustomerAddressDAO getCustomerAddressDAO() {
		return customerAddressDAO;
	}

	public final void setCustomerAddressDAO(CustomerAddressDAO customerAddressDAO) {
		DAORegistry.customerAddressDAO = customerAddressDAO;
	}

	public static CountryDAO getCountryDAO() {
		return countryDAO;
	}

	public final void setCountryDAO(CountryDAO countryDAO) {
		DAORegistry.countryDAO = countryDAO;
	}

	public static StateDAO getStateDAO() {
		return stateDAO;
	}

	public final void setStateDAO(StateDAO stateDAO) {
		DAORegistry.stateDAO = stateDAO;
	}

	public static EventDetailsDAO getEventDetailsDAO() {
		return eventDetailsDAO;
	}

	public final void setEventDetailsDAO(EventDetailsDAO eventDetailsDAO) {
		DAORegistry.eventDetailsDAO = eventDetailsDAO;
	}

	public static CategoryTicketGroupDAO getCategoryTicketGroupDAO() {
		return categoryTicketGroupDAO;
	}

	public final void setCategoryTicketGroupDAO(
			CategoryTicketGroupDAO categoryTicketGroupDAO) {
		DAORegistry.categoryTicketGroupDAO = categoryTicketGroupDAO;
	}

	public static PurchaseOrderDAO getPurchaseOrderDAO() {
		return purchaseOrderDAO;
	}

	public static void setPurchaseOrderDAO(PurchaseOrderDAO purchaseOrderDAO) {
		DAORegistry.purchaseOrderDAO = purchaseOrderDAO;
	}
	
	public static PurchaseOrderPaymentDetailsDAO getPurchaseOrderPaymentDetailsDAO() {
		return purchaseOrderPaymentDetailsDAO;
	}

	public final void setPurchaseOrderPaymentDetailsDAO(
			PurchaseOrderPaymentDetailsDAO purchaseOrderPaymentDetailsDAO) {
		DAORegistry.purchaseOrderPaymentDetailsDAO = purchaseOrderPaymentDetailsDAO;
	}

	public static PopularEventsDAO getPopularEventsDAO() {
		return popularEventsDAO;
	}

	public final void setPopularEventsDAO(PopularEventsDAO popularEventsDAO) {
		DAORegistry.popularEventsDAO = popularEventsDAO;
	}

	public static TicketGroupDAO getTicketGroupDAO() {
		return ticketGroupDAO;
	}

	public static void setTicketGroupDAO(TicketGroupDAO ticketGroupDAO) {
		DAORegistry.ticketGroupDAO = ticketGroupDAO;
	}

	public static InvoiceDAO getInvoiceDAO() {
		return invoiceDAO;
	}

	public static void setInvoiceDAO(InvoiceDAO invoiceDAO) {
		DAORegistry.invoiceDAO = invoiceDAO;
	}

	public static GrandChildCategoryDAO getGrandChildCategoryDAO() {
		return grandChildCategoryDAO;
	}

	public final void setGrandChildCategoryDAO(
			GrandChildCategoryDAO grandChildCategoryDAO) {
		DAORegistry.grandChildCategoryDAO = grandChildCategoryDAO;
	}

	public static ChildCategoryDAO getChildCategoryDAO() {
		return childCategoryDAO;
	}

	public final void setChildCategoryDAO(ChildCategoryDAO childCategoryDAO) {
		DAORegistry.childCategoryDAO = childCategoryDAO;
	}

	public static ParentCategoryDAO getParentCategoryDAO() {
		return parentCategoryDAO;
	}

	public final void setParentCategoryDAO(ParentCategoryDAO parentCategoryDAO) {
		DAORegistry.parentCategoryDAO = parentCategoryDAO;
	}

	public static ArtistDAO getArtistDAO() {
		return artistDAO;
	}

	public final void setArtistDAO(ArtistDAO artistDAO) {
		DAORegistry.artistDAO = artistDAO;
	}

	public static VenueDAO getVenueDAO() {
		return venueDAO;
	}

	public final void setVenueDAO(VenueDAO venueDAO) {
		DAORegistry.venueDAO = venueDAO;
	}

	public static PopularArtistDAO getPopularArtistDAO() {
		return popularArtistDAO;
	}

	public final void setPopularArtistDAO(PopularArtistDAO popularArtistDAO) {
		DAORegistry.popularArtistDAO = popularArtistDAO;
	}

	public static PopularGrandChildCategoryDAO getPopularGrandChildCategoryDAO() {
		return popularGrandChildCategoryDAO;
	}

	public final void setPopularGrandChildCategoryDAO(
			PopularGrandChildCategoryDAO popularGrandChildCategoryDAO) {
		DAORegistry.popularGrandChildCategoryDAO = popularGrandChildCategoryDAO;
	}

	public static PopularVenueDAO getPopularVenueDAO() {
		return popularVenueDAO;
	}

	public final void setPopularVenueDAO(PopularVenueDAO popularVenueDAO) {
		DAORegistry.popularVenueDAO = popularVenueDAO;
	}

	public static ProductDAO getProductDAO() {
		return productDAO;
	}

	public final void setProductDAO(ProductDAO productDAO) {
		DAORegistry.productDAO = productDAO;
	}

	public static CardsDAO getCardsDAO() {
		return cardsDAO;
	}

	public final void setCardsDAO(CardsDAO cardsDAO) {
		DAORegistry.cardsDAO = cardsDAO;
	}

	public static CustomerOrderDAO getCustomerOrderDAO() {
		return customerOrderDAO;
	}

	public final void setCustomerOrderDAO(CustomerOrderDAO customerOrderDAO) {
		DAORegistry.customerOrderDAO = customerOrderDAO;
	}

	public static CrownJewelTeamsDAO getCrownJewelTeamsDAO() {
		return crownJewelTeamsDAO;
	}

	public final void setCrownJewelTeamsDAO(CrownJewelTeamsDAO crownJewelTeamsDAO) {
		DAORegistry.crownJewelTeamsDAO = crownJewelTeamsDAO;
	}

	public static CrownJewelTeamsAuditDAO getCrownJewelTeamsAuditDAO() {
		return crownJewelTeamsAuditDAO;
	}

	public final void setCrownJewelTeamsAuditDAO(
			CrownJewelTeamsAuditDAO crownJewelTeamsAuditDAO) {
		DAORegistry.crownJewelTeamsAuditDAO = crownJewelTeamsAuditDAO;
	}

	public static CrownJewelLeaguesDAO getCrownJewelLeaguesDAO() {
		return crownJewelLeaguesDAO;
	}

	public final void setCrownJewelLeaguesDAO(
			CrownJewelLeaguesDAO crownJewelLeaguesDAO) {
		DAORegistry.crownJewelLeaguesDAO = crownJewelLeaguesDAO;
	}

	public static CrownJewelLeaguesAuditDAO getCrownJewelLeaguesAuditDAO() {
		return crownJewelLeaguesAuditDAO;
	}

	public final void setCrownJewelLeaguesAuditDAO(
			CrownJewelLeaguesAuditDAO crownJewelLeaguesAuditDAO) {
		DAORegistry.crownJewelLeaguesAuditDAO = crownJewelLeaguesAuditDAO;
	}

	public static CrownJewelCategoryTicketDAO getCrownJewelCategoryTicketDAO() {
		return crownJewelCategoryTicketDAO;
	}

	public final void setCrownJewelCategoryTicketDAO(
			CrownJewelCategoryTicketDAO crownJewelCategoryTicketDAO) {
		DAORegistry.crownJewelCategoryTicketDAO = crownJewelCategoryTicketDAO;
	}

	public static CrownJewelTeamZonesDAO getCrownJewelTeamZonesDAO() {
		return crownJewelTeamZonesDAO;
	}

	public final void setCrownJewelTeamZonesDAO(
			CrownJewelTeamZonesDAO crownJewelTeamZonesDAO) {
		DAORegistry.crownJewelTeamZonesDAO = crownJewelTeamZonesDAO;
	}

	
	
	public static CrownJewelCustomerOrderDAO getCrownJewelCustomerOrderDAO() {
		return crownJewelCustomerOrderDAO;
	}

	public final void setCrownJewelCustomerOrderDAO(
			CrownJewelCustomerOrderDAO crownJewelCustomerOrderDAO) {
		DAORegistry.crownJewelCustomerOrderDAO = crownJewelCustomerOrderDAO;
	}

	public static CrownJewelTeamZonesAuditDAO getCrownJewelTeamZonesAuditDAO() {
		return crownJewelTeamZonesAuditDAO;
	}

	public final void setCrownJewelTeamZonesAuditDAO(
			CrownJewelTeamZonesAuditDAO crownJewelTeamZonesAuditDAO) {
		DAORegistry.crownJewelTeamZonesAuditDAO = crownJewelTeamZonesAuditDAO;
	}

	public static CustomerOrderDetailsDAO getCustomerOrderDetailsDAO() {
		return customerOrderDetailsDAO;
	}

	public final void setCustomerOrderDetailsDAO(
			CustomerOrderDetailsDAO customerOrderDetailsDAO) {
		DAORegistry.customerOrderDetailsDAO = customerOrderDetailsDAO;
	}

	public static ArtistImageDAO getArtistImageDAO() {
		return artistImageDAO;
	}

	public final void setArtistImageDAO(ArtistImageDAO artistImageDAO) {
		DAORegistry.artistImageDAO = artistImageDAO;
	}

	public static ArtistImageAuditDAO getArtistImageAuditDAO() {
		return artistImageAuditDAO;
	}

	public final void setArtistImageAuditDAO(
			ArtistImageAuditDAO artistImageAuditDAO) {
		DAORegistry.artistImageAuditDAO = artistImageAuditDAO;
	}

	public static GrandChildCategoryImageDAO getGrandChildCategoryImageDAO() {
		return grandChildCategoryImageDAO;
	}

	public final void setGrandChildCategoryImageDAO(
			GrandChildCategoryImageDAO grandChildCategoryImageDAO) {
		DAORegistry.grandChildCategoryImageDAO = grandChildCategoryImageDAO;
	}

	public static GrandChildCategoryImageAuditDAO getGrandChildCategoryImageAuditDAO() {
		return grandChildCategoryImageAuditDAO;
	}

	public final void setGrandChildCategoryImageAuditDAO(
			GrandChildCategoryImageAuditDAO grandChildCategoryImageAuditDAO) {
		DAORegistry.grandChildCategoryImageAuditDAO = grandChildCategoryImageAuditDAO;
	}

	public static ChildCategoryImageDAO getChildCategoryImageDAO() {
		return childCategoryImageDAO;
	}

	public final void setChildCategoryImageDAO(
			ChildCategoryImageDAO childCategoryImageDAO) {
		DAORegistry.childCategoryImageDAO = childCategoryImageDAO;
	}

	public static ChildCategoryImageAuditDAO getChildCategoryImageAuditDAO() {
		return childCategoryImageAuditDAO;
	}

	public final void setChildCategoryImageAuditDAO(
			ChildCategoryImageAuditDAO childCategoryImageAuditDAO) {
		DAORegistry.childCategoryImageAuditDAO = childCategoryImageAuditDAO;
	}

	public static ParentCategoryImageDAO getParentCategoryImageDAO() {
		return parentCategoryImageDAO;
	}

	public final void setParentCategoryImageDAO(
			ParentCategoryImageDAO parentCategoryImageDAO) {
		DAORegistry.parentCategoryImageDAO = parentCategoryImageDAO;
	}

	public static ParentCategoryImageAuditDAO getParentCategoryImageAuditDAO() {
		return parentCategoryImageAuditDAO;
	}

	public final void setParentCategoryImageAuditDAO(
			ParentCategoryImageAuditDAO parentCategoryImageAuditDAO) {
		DAORegistry.parentCategoryImageAuditDAO = parentCategoryImageAuditDAO;
	}

	public static FantasyGrandChildCategoryImageDAO getFantasyGrandChildCategoryImageDAO() {
		return fantasyGrandChildCategoryImageDAO;
	}

	public final void setFantasyGrandChildCategoryImageDAO(
			FantasyGrandChildCategoryImageDAO fantasyGrandChildCategoryImageDAO) {
		DAORegistry.fantasyGrandChildCategoryImageDAO = fantasyGrandChildCategoryImageDAO;
	}

	public static OpenOrderStatusDao getOpenOrderStatusDao() {
		return openOrderStatusDao;
	}

	public final void setOpenOrderStatusDao(OpenOrderStatusDao openOrderStatusDao) {
		DAORegistry.openOrderStatusDao = openOrderStatusDao;
	}

	public static SoldTicketDetailDAO getSoldTicketDetailDAO() {
		return soldTicketDetailDAO;
	}

	public final void setSoldTicketDetailDAO(
			SoldTicketDetailDAO soldTicketDetailDAO) {
		DAORegistry.soldTicketDetailDAO = soldTicketDetailDAO;
	}

	public static ShippingMethodDAO getShippingMethodDAO() {
		return shippingMethodDAO;
	}

	public final void setShippingMethodDAO(ShippingMethodDAO shippingMethodDAO) {
		DAORegistry.shippingMethodDAO = shippingMethodDAO;
	}

	public static ShowNearTermOptionDAO getShowNearTermOptionDAO() {
		return showNearTermOptionDAO;
	}

	public final void setShowNearTermOptionDAO(
			ShowNearTermOptionDAO showNearTermOptionDAO) {
		DAORegistry.showNearTermOptionDAO = showNearTermOptionDAO;
	}

	public static FedExLabelLogDAO getFedExLabelLogDAO() {
		return fedExLabelLogDAO;
	}

	public final void setFedExLabelLogDAO(
			FedExLabelLogDAO fedExLabelLogDAO) {
		DAORegistry.fedExLabelLogDAO = fedExLabelLogDAO;
	}

	public static LoyaltySettingsDAO getLoyaltySettingsDAO() {
		return loyaltySettingsDAO;
	}

	public final void setLoyaltySettingsDAO(LoyaltySettingsDAO loyaltySettingsDAO) {
		DAORegistry.loyaltySettingsDAO = loyaltySettingsDAO;
	}

	public static CustomerLoyaltyDAO getCustomerLoyaltyDAO() {
		return customerLoyaltyDAO;
	}

	public final void setCustomerLoyaltyDAO(CustomerLoyaltyDAO customerLoyaltyDAO) {
		DAORegistry.customerLoyaltyDAO = customerLoyaltyDAO;
	}

	public static PropertyDAO getPropertyDAO() {
		return propertyDAO;
	}

	public final void setPropertyDAO(PropertyDAO propertyDAO) {
		DAORegistry.propertyDAO = propertyDAO;
	}

	public static EventAuditDAO getEventAuditDAO() {
		return eventAuditDAO;
	}

	public final void setEventAuditDAO(EventAuditDAO eventAuditDAO) {
		DAORegistry.eventAuditDAO = eventAuditDAO;
	}

	public static ArtistAuditDAO getArtistAuditDAO() {
		return artistAuditDAO;
	}

	public final void setArtistAuditDAO(ArtistAuditDAO artistAuditDAO) {
		DAORegistry.artistAuditDAO = artistAuditDAO;
	}

	public static VenueAuditDAO getVenueAuditDAO() {
		return venueAuditDAO;
	}

	public final void setVenueAuditDAO(VenueAuditDAO venueAuditDAO) {
		DAORegistry.venueAuditDAO = venueAuditDAO;
	}

	public static GrandChildCategoryAuditDAO getGrandChildCategoryAuditDAO() {
		return grandChildCategoryAuditDAO;
	}

	public final void setGrandChildCategoryAuditDAO(
			GrandChildCategoryAuditDAO grandChildCategoryAuditDAO) {
		DAORegistry.grandChildCategoryAuditDAO = grandChildCategoryAuditDAO;
	}

	public static UserPreferenceDAO getUserPreferenceDAO() {
		return userPreferenceDAO;
	}

	public final void setUserPreferenceDAO(UserPreferenceDAO userPreferenceDAO) {
		DAORegistry.userPreferenceDAO = userPreferenceDAO;
	}

	public static SeatGeekOrdersDAO getSeatGeekOrdersDAO() {
		return seatGeekOrdersDAO;
	}

	public final void setSeatGeekOrdersDAO(SeatGeekOrdersDAO seatGeekOrdersDAO) {
		DAORegistry.seatGeekOrdersDAO = seatGeekOrdersDAO;
	}

	public static InvoiceTicketAttachmentDAO getInvoiceTicketAttachmentDAO() {
		return invoiceTicketAttachmentDAO;
	}

	public final void setInvoiceTicketAttachmentDAO(
			InvoiceTicketAttachmentDAO invoiceTicketAttachmentDAO) {
		DAORegistry.invoiceTicketAttachmentDAO = invoiceTicketAttachmentDAO;
	}

	public static CategoryTicketDAO getCategoryTicketDAO() {
		return categoryTicketDAO;
	}

	public final void setCategoryTicketDAO(CategoryTicketDAO categoryTicketDAO) {
		DAORegistry.categoryTicketDAO = categoryTicketDAO;
	}

	public static TicketDAO getTicketDAO() {
		return ticketDAO;
	}

	public final void setTicketDAO(TicketDAO ticketDAO) {
		DAORegistry.ticketDAO = ticketDAO;
	}

	public static CustomerStripeCreditCardDAO getCustomerStripeCreditCardDAO() {
		return customerStripeCreditCardDAO;
	}

	public final void setCustomerStripeCreditCardDAO(
			CustomerStripeCreditCardDAO customerStripeCreditCardDAO) {
		DAORegistry.customerStripeCreditCardDAO = customerStripeCreditCardDAO;
	}

	public static CustomerTicketDownloadsDAO getCustomerTicketDownloadsDAO() {
		return customerTicketDownloadsDAO;
	}

	public final void setCustomerTicketDownloadsDAO(
			CustomerTicketDownloadsDAO customerTicketDownloadsDAO) {
		DAORegistry.customerTicketDownloadsDAO = customerTicketDownloadsDAO;
	}

	public static InvoiceRefundDAO getInvoiceRefundDAO() {
		return invoiceRefundDAO;
	}

	public static void setInvoiceRefundDAO(InvoiceRefundDAO invoiceRefundDAO) {
		DAORegistry.invoiceRefundDAO = invoiceRefundDAO;
	}

	public static CustomerLoyaltyHistoryDAO getCustomerLoyaltyHistoryDAO() {
		return customerLoyaltyHistoryDAO;
	}

	public final void setCustomerLoyaltyHistoryDAO(
			CustomerLoyaltyHistoryDAO customerLoyaltyHistoryDAO) {
		DAORegistry.customerLoyaltyHistoryDAO = customerLoyaltyHistoryDAO;
	}

	/*public static CustomerSuperFanDAO getCustomerSuperFanDAO() {
		return customerSuperFanDAO;
	}

	public final void setCustomerSuperFanDAO(
			CustomerSuperFanDAO customerSuperFanDAO) {
		DAORegistry.customerSuperFanDAO = customerSuperFanDAO;
	}*/

	public static CrownJewelCategoryTeamsDAO getCrownJewelCategoryTeamsDAO() {
		return crownJewelCategoryTeamsDAO;
	}

	public final void setCrownJewelCategoryTeamsDAO(
			CrownJewelCategoryTeamsDAO crownJewelCategoryTeamsDAO) {
		DAORegistry.crownJewelCategoryTeamsDAO = crownJewelCategoryTeamsDAO;
	}

	
	public static CrownJewelCategoryTeamsAuditDAO getCrownJewelCategoryTeamsAuditDAO() {
		return crownJewelCategoryTeamsAuditDAO;
	}

	public final void setCrownJewelCategoryTeamsAuditDAO(
			CrownJewelCategoryTeamsAuditDAO crownJewelCategoryTeamsAuditDAO) {
		DAORegistry.crownJewelCategoryTeamsAuditDAO = crownJewelCategoryTeamsAuditDAO;
	}

	public static InvoiceAuditDAO getInvoiceAuditDAO() {
		return invoiceAuditDAO;
	}

	public final void setInvoiceAuditDAO(InvoiceAuditDAO invoiceAuditDAO) {
		DAORegistry.invoiceAuditDAO = invoiceAuditDAO;
	}

	public static InventoryHoldDAO getInventoryHoldDAO() {
		return inventoryHoldDAO;
	}

	public final void setInventoryHoldDAO(InventoryHoldDAO inventoryHoldDAO) {
		DAORegistry.inventoryHoldDAO = inventoryHoldDAO;
	}

	
	public static TrackerBrokersDAO getTrackerBrokersDAO() {
		return trackerBrokersDAO;
	}

	public final void setTrackerBrokersDAO(TrackerBrokersDAO trackerBrokersDAO) {
		DAORegistry.trackerBrokersDAO = trackerBrokersDAO;
	}

	public static AffiliateCashRewardDAO getAffiliateCashRewardDAO() {
		return affiliateCashRewardDAO;
	}

	public final void setAffiliateCashRewardDAO(
			AffiliateCashRewardDAO affiliateCashRewardDAO) {
		DAORegistry.affiliateCashRewardDAO = affiliateCashRewardDAO;
	}

	public static AffiliateCashRewardHistoryDAO getAffiliateCashRewardHistoryDAO() {
		return affiliateCashRewardHistoryDAO;
	}

	public final void setAffiliateCashRewardHistoryDAO(
			AffiliateCashRewardHistoryDAO affiliateCashRewardHistoryDAO) {
		DAORegistry.affiliateCashRewardHistoryDAO = affiliateCashRewardHistoryDAO;
	}

	public static AffiliatePromoCodeHistoryDAO getAffiliatePromoCodeHistoryDAO() {
		return affiliatePromoCodeHistoryDAO;
	}

	public final void setAffiliatePromoCodeHistoryDAO(
			AffiliatePromoCodeHistoryDAO affiliatePromoCodeHistoryDAO) {
		DAORegistry.affiliatePromoCodeHistoryDAO = affiliatePromoCodeHistoryDAO;
	}

	public static AutoCatsLockedTicketsDAO getAutoCatsLockedTicketsDAO() {
		return autoCatsLockedTicketsDAO;
	}

	public final void setAutoCatsLockedTicketsDAO(
			AutoCatsLockedTicketsDAO autoCatsLockedTicketsDAO) {
		DAORegistry.autoCatsLockedTicketsDAO = autoCatsLockedTicketsDAO;
	}

	public static RTFPromotionalOfferDAO getRtfPromotionalOfferDAO() {
		return rtfPromotionalOfferDAO;
	}

	public final void setRtfPromotionalOfferDAO(
			RTFPromotionalOfferDAO rtfPromotionalOfferDAO) {
		DAORegistry.rtfPromotionalOfferDAO = rtfPromotionalOfferDAO;
	}

	public static StripeDisputeDAO getStripeDisputeDAO() {
		return stripeDisputeDAO;
	}

	public final void setStripeDisputeDAO(StripeDisputeDAO stripeDisputeDAO) {
		DAORegistry.stripeDisputeDAO = stripeDisputeDAO;
	}

	public static LoyalFanParentCategoryImageDAO getLoyalFanParentCategoryImageDAO() {
		return loyalFanParentCategoryImageDAO;
	}

	public final void setLoyalFanParentCategoryImageDAO(
			LoyalFanParentCategoryImageDAO loyalFanParentCategoryImageDAO) {
		DAORegistry.loyalFanParentCategoryImageDAO = loyalFanParentCategoryImageDAO;
	}

	public static ContestAffiliatesDAO getContestAffiliatesDAO() {
		return contestAffiliatesDAO;
	}

	public final void setContestAffiliatesDAO(ContestAffiliatesDAO contestAffiliatesDAO) {
		DAORegistry.contestAffiliatesDAO = contestAffiliatesDAO;
	}

	public static RtfCatsTicketDAO getRtfCatsTicketDAO() {
		return rtfCatsTicketDAO;
	}

	public final void setRtfCatsTicketDAO(RtfCatsTicketDAO rtfCatsTicketDAO) {
		DAORegistry.rtfCatsTicketDAO = rtfCatsTicketDAO;
	}

	public static RtfGiftCardDAO getRtfGiftCardDAO() {
		return rtfGiftCardDAO;
	}

	public final void setRtfGiftCardDAO(RtfGiftCardDAO rtfGiftCardDAO) {
		DAORegistry.rtfGiftCardDAO = rtfGiftCardDAO;
	}

	public static RtfGiftCardQuantityDAO getRtfGiftCardQuantityDAO() {
		return rtfGiftCardQuantityDAO;
	}

	public final void setRtfGiftCardQuantityDAO(RtfGiftCardQuantityDAO rtfGiftCardQuantityDAO) {
		DAORegistry.rtfGiftCardQuantityDAO = rtfGiftCardQuantityDAO;
	}

	public static GiftCardOrderDAO getGiftCardOrderDAO() {
		return giftCardOrderDAO;
	}

	public final void setGiftCardOrderDAO(GiftCardOrderDAO giftCardOrderDAO) {
		DAORegistry.giftCardOrderDAO = giftCardOrderDAO;
	}

	public static GiftCardOrderAttachmentDAO getGiftCardOrderAttachmentDAO() {
		return giftCardOrderAttachmentDAO;
	}

	public final void setGiftCardOrderAttachmentDAO(GiftCardOrderAttachmentDAO giftCardOrderAttachmentDAO) {
		DAORegistry.giftCardOrderAttachmentDAO = giftCardOrderAttachmentDAO;
	}
	
	public static PollingSponsorDAO getPollingSponsorDAO() {
		return pollingSponsorDAO;
	}

	public final void setPollingSponsorDAO(PollingSponsorDAO pollingSponsorDAO) {
		DAORegistry.pollingSponsorDAO = pollingSponsorDAO;
	}
	public static PollingCategoryDAO getPollingCategoryDAO() {
		return pollingCategoryDAO;
	}
	
	public final void setPollingCategoryDAO(PollingCategoryDAO pollingCategoryDAO) {
		DAORegistry.pollingCategoryDAO = pollingCategoryDAO;
	}

	public static PollingVideoCategoryDAO getPollingVideoCategoryDAO() {
		return pollingVideoCategoryDAO;
	}
	
	public final void setPollingVideoCategoryDAO(PollingVideoCategoryDAO pollingVideoCategoryDAO) {
		DAORegistry.pollingVideoCategoryDAO = pollingVideoCategoryDAO;
	}
	
	public static PollingContestDAO getPollingContestDAO() {
		return pollingContestDAO;
	}

	public static void setPollingContestDAO(PollingContestDAO pollingContestDAO) {
		DAORegistry.pollingContestDAO = pollingContestDAO;
	}

	public static PollingRewardsDAO getPollingRewardsDAO() {
		return pollingRewardsDAO;
	}

	public static void setPollingRewardsDAO(PollingRewardsDAO pollingRewardsDAO) {
		DAORegistry.pollingRewardsDAO = pollingRewardsDAO;
	}

	public static PollingContestCategoryMapperDAO getPollingContestCategoryMapperDAO() {
		return pollingContestCategoryMapperDAO;
	}

	public static void setPollingContestCategoryMapperDAO(PollingContestCategoryMapperDAO pollingContestCategoryMapperDAO) {
		DAORegistry.pollingContestCategoryMapperDAO = pollingContestCategoryMapperDAO;
	}
	
	public static PollingCategoryQuestionDAO getPollingCategoryQuestionDAO() {
		return pollingCategoryQuestionDAO;
	}

	public final void setPollingCategoryQuestionDAO(PollingCategoryQuestionDAO pollingCategoryQuestionDAO) {
		DAORegistry.pollingCategoryQuestionDAO = pollingCategoryQuestionDAO;
	}
	
	public static PollingVideoInventoryDAO getPollingVideoInventoryDAO() {
		return pollingVideoInventoryDAO;
	}

	public final void setPollingVideoInventoryDAO(PollingVideoInventoryDAO pollingVideoInventoryDAO) {
		DAORegistry.pollingVideoInventoryDAO = pollingVideoInventoryDAO;
	}
	

	public static PollingCustomerVideoDAO getPollingCustomerVideoDAO() {
		return pollingCustomerVideoDAO;
	}

	public static void setPollingCustomerVideoDAO(PollingCustomerVideoDAO pollingCustomerVideoDAO) {
		DAORegistry.pollingCustomerVideoDAO = pollingCustomerVideoDAO;
	}

	public static GiftCardBrandDAO getGiftCardBrandDAO() {
		return giftCardBrandDAO;
	}

	public final void setGiftCardBrandDAO(GiftCardBrandDAO giftCardBrandDAO) {
		DAORegistry.giftCardBrandDAO = giftCardBrandDAO;
	}

	public static TicketMasterRefundDAO getTicketMasterRefundDAO() {
		return ticketMasterRefundDAO;
	}

	public final void setTicketMasterRefundDAO(TicketMasterRefundDAO ticketMasterRefundDAO) {
		DAORegistry.ticketMasterRefundDAO = ticketMasterRefundDAO;
	}

	public static TicketMasterRefundKnownEmailDAO getTicketMasterRefundKnownEmailDAO() {
		return ticketMasterRefundKnownEmailDAO;
	}

	public final void setTicketMasterRefundKnownEmailDAO(TicketMasterRefundKnownEmailDAO ticketMasterRefundKnownEmailDAO) {
		DAORegistry.ticketMasterRefundKnownEmailDAO = ticketMasterRefundKnownEmailDAO;
	}

	public static TicketMasterRefundCCNotMatchedDAO getTicketMasterRefundCCNotMatchedDAO() {
		return ticketMasterRefundCCNotMatchedDAO;
	}

	public final void setTicketMasterRefundCCNotMatchedDAO(
			TicketMasterRefundCCNotMatchedDAO ticketMasterRefundCCNotMatchedDAO) {
		DAORegistry.ticketMasterRefundCCNotMatchedDAO = ticketMasterRefundCCNotMatchedDAO;
	}
	
	
	
	
}