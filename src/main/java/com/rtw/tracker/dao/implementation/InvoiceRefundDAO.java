package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.InvoiceRefund;

public class InvoiceRefundDAO extends HibernateDAO<Integer, InvoiceRefund> implements com.rtw.tracker.dao.services.InvoiceRefundDAO{

	public InvoiceRefund getInvoiceRefundById(Integer id){
		return findSingle("FROM InvoiceRefund Where id = ?" ,new Object[]{id});
	}
	
	public InvoiceRefund getStripeRefundByIdandTrasactionId(String refundId,String transactionId){
		return findSingle("FROM InvoiceRefund Where refundType=? AND transactionId=? and refundId=?" ,new Object[]{"CREDITCARD",transactionId,refundId});
	}
	
	public InvoiceRefund getPaypalRefundByIdandTrasactionId(String refundId,String transactionId){
		return findSingle("FROM InvoiceRefund Where refundType=? AND transactionId=? and refundId=?" ,new Object[]{"PAYPAL",transactionId,refundId});
	}
	
	public List<InvoiceRefund> getInvoiceRefundByOrderId(Integer orderId){
		return find("FROM InvoiceRefund Where orderId = ?" ,new Object[]{orderId});
	}
	
	public List<InvoiceRefund> getAllStripeRefunds(){
		return find("FROM InvoiceRefund Where refundType=? order by id desc" ,new Object[]{"CREDITCARD"});
	}
	
	public List<InvoiceRefund> getAllPaypalRefunds(){
		return find("FROM InvoiceRefund Where refundType=? order by id desc" ,new Object[]{"PAYPAL"});
	}
	
	
	
}
