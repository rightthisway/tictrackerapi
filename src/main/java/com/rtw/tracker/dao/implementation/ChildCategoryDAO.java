package com.rtw.tracker.dao.implementation; 

import java.util.ArrayList;
import java.util.List;

import com.rtw.tracker.datas.ChildCategory;
/**
 * class having method related to Child Category 
 * @author hamin
 *
 */
public class ChildCategoryDAO extends HibernateDAO<Integer,ChildCategory> implements
 com.rtw.tracker.dao.services.ChildCategoryDAO {

 
	/**
	 * method to get List of child category  
	 @return List of ChildCategory 
	 */
	public List<ChildCategory> getAll() {
		return find("FROM ChildCategory order by name asc");	 
	}
	/**
	 * method to get child category using its name
	 * @param name, name of child category  
	 * @return ChildCategory
	 */	
	public ChildCategory getChildCategoryByName(String name) {
		 return findSingle("FROM ChildCategory where name = ?", new Object[]{name});	 
			
	}
	/**
	 * method to get child category using its id
	 * @param id, child category id
	 * @return ChildCategory
	 */
	public ChildCategory getChildCategoryById(Integer id) {
		  return findSingle("FROM ChildCategory where id = ?", new Object[]{id});	 
			
	}
	
	/**
	 * method to get child category using its parent category name
	 * @param parentCategoryName, parent category name
	 * @return ChildCategory
	 */
	public List<ChildCategory> getChildCategoryByParentCategoryName(
			String parentCategoryName) {
		return find("FROM ChildCategory WHERE parentCategory.name like ? order by name" ,new Object[]{parentCategoryName});
		
	}
	public List<ChildCategory> getAllChildCategoryByParentCategory(Integer parentCategoryId) throws Exception {
		  return find("FROM ChildCategory where parentCategory.id = ?", new Object[]{parentCategoryId});	 
			
	}
	public List<ChildCategory> getChildCategoriesByNameAndDisplayOnSearch(String name) {
		String hql="FROM ChildCategory WHERE name like ? AND isDisplay = ? order by name";
		List<Object> parameters = new ArrayList<Object>();
		parameters.add("%" + name + "%");
		parameters.add(true);
		return find(hql, parameters.toArray());
	}
	
	public List<ChildCategory> getAllChildCategoriesByNameAndDisplayOnSearch(String name) {
		String hql="FROM ChildCategory WHERE name like ? AND isDisplay = ? order by name";
		List<Object> parameters = new ArrayList<Object>();
		parameters.add(name);
		parameters.add(true);
		return find(hql, parameters.toArray());
	}
	
	@Override
	public List<ChildCategory> getAllChildCategoryByDisplayOnSearch()
			throws Exception {
		return find("FROM ChildCategory WHERE isDisplay=? order by name" ,new Object[]{true});
	}
	
	
}
