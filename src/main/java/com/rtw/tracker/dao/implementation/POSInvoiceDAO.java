package com.rtw.tracker.dao.implementation;

import com.rtw.tracker.datas.POSInvoice;

public class POSInvoiceDAO extends HibernateDAO<Integer, POSInvoice> implements com.rtw.tracker.dao.services.POSInvoiceDAO{

	@Override
	public POSInvoice getInvoiceByProductTypeAndId(Integer invoiceId,String productType) {
		return findSingle("FROM POSInvoice WHERE productType=? AND id=?",new Object[]{productType,invoiceId});
	}

}
