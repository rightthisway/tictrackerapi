package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.datas.ContestAffiliates;
import com.rtw.tracker.pojos.ContestAffiliateEarning;
import com.rtw.tracker.utils.GridHeaderFilters;

public class ContestAffiliatesDAO extends HibernateDAO<Integer, ContestAffiliates> implements com.rtw.tracker.dao.services.ContestAffiliatesDAO{

private static Session affiliateSession = null;
	
	public static Session getContestSession() {
		return ContestAffiliatesDAO.affiliateSession;
	}

	public static void setContestSession(Session contestSession) {
		ContestAffiliatesDAO.affiliateSession = contestSession;
	}

	@Override
	public List<ContestAffiliates> getAllContestAffiliates(String status, GridHeaderFilters filter) {
		List<ContestAffiliates> affiliates = null;
		try {
			String sql = "select a.id as id,a.customer_id as customerId,a.create_date as createdDate,a.effective_from_date as fromDate,a.effective_to_date as toDate,"+
					"a.no_of_lives_to_affiliate_customer as affiliateLives,a.no_of_lives_to_tireone_customer  as customerLives,a.reward_type as rewardType,"+
					"a.reward_value as rewardValue,a.status as status,a.updated_date as updatedDate,c.cust_name as firstName,c.last_name as lastName,"+
					"c.customer_type as customerType,c.email as email,c.phone as phone,c.user_id as userId,a.updated_by as updatedBy,a.created_by as createdBy,"+
					"a.active_cash_reward as activeCashReward,a.debited_cash_reward as debitedCashReward,a.total_cash_reward as totalCashReward,a.tireone_cust_super_fan_stars as customerSuperFanStars,"+
					"a.total_credited_reward_dollars as totalCreditedRewardDollar, a.voided_cash_reward as voidedCashReward,a.tireone_cust_reward_dollars as customerRewardDollars,"+
					"a.referral_code as referralCode from customer c join contest_affiliate_settings a on c.id=a.customer_id "+
					"where a.status='"+status+"' ";
			
			if(filter.getTicketWinnerCount()!=null){
				sql+= " AND a.no_of_lives_to_affiliate_customer="+filter.getTicketWinnerCount();
			}
			if(filter.getPointWinnerCount()!=null){
				sql+= " AND a.no_of_lives_to_tireone_customer="+filter.getPointWinnerCount();
			}
			if(filter.getText1()!=null){
				sql+= " AND a.reward_type like '%"+filter.getText1()+"%'";
			}
			if(filter.getText2()!=null){
				sql+= " AND a.active_cash_reward  = "+filter.getText2();
			}
			if(filter.getRewardPoints()!=null){
				sql+= " AND a.tireone_cust_reward_dollars  = "+filter.getRewardPoints();
			}
			if(filter.getCount()!=null){
				sql+= " AND a.tireone_cust_super_fan_stars  = "+filter.getCount();
			}
			if(filter.getText3()!=null){
				sql+= " AND a.debited_cash_reward = "+filter.getText3();
			}
			if(filter.getText4()!=null){
				sql+= " AND a.total_cash_reward = "+filter.getText4();
			}
			if(filter.getText5()!=null){
				sql+= " AND a.voided_cash_reward = "+filter.getText5();
			}
			if(filter.getText6()!=null){
				sql+= " AND a.total_credited_reward_dollars = "+filter.getText6();
			}
			if(filter.getPrice()!=null){
				sql+= " AND a.reward_value ="+filter.getPrice();
			}
			if(filter.getCustomerName()!=null){
				sql+= " AND c.cust_name like '%"+filter.getCustomerName()+"%'";
			}
			if(filter.getLastName()!=null){
				sql+= " AND c.last_name like '%"+filter.getLastName()+"%'";
			}
			if(filter.getEmail()!=null){
				sql+= " AND c.email like '%"+filter.getEmail()+"%'";
			}
			if(filter.getUsername()!=null){
				sql+= " AND c.user_id like '%"+filter.getUsername()+"%'";
			}
			if(filter.getPhone()!=null){
				sql+= " AND c.phone like '%"+filter.getPhone()+"%'";
			}
			if(filter.getCustomerType()!=null){
				sql+= " AND c.customer_type like '%"+filter.getCustomerType()+"%'";
			}
			if(filter.getReferrerCode()!=null){
				sql+= " AND a.referral_code like '%"+filter.getReferrerCode()+"%'";
			}
			if(filter.getCreatedBy()!=null){
				sql+= " AND a.created_by like '%"+filter.getCreatedBy()+"%'";
			}
			if(filter.getUpdatedBy()!=null){
				sql+= " AND a.updated_by like '%"+filter.getUpdatedBy()+"%'";
			}
			
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day, a.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month, a.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year, a.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getUpdatedDate()!=null ){
				if(Util.extractDateElement(filter.getUpdatedDate(),"DAY") > 0){
					sql += " AND DATEPART(day, a.updated_date) = "+Util.extractDateElement(filter.getUpdatedDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"MONTH") > 0){
					sql += " AND DATEPART(month, a.updated_date) = "+Util.extractDateElement(filter.getUpdatedDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"YEAR") > 0){
					sql += " AND DATEPART(year, a.updated_date) = "+Util.extractDateElement(filter.getUpdatedDate(),"YEAR")+" ";
				}
			}
			if(filter.getStartDate()!=null ){
				if(Util.extractDateElement(filter.getStartDate(),"DAY") > 0){
					sql += " AND DATEPART(day, a.effective_from_date) = "+Util.extractDateElement(filter.getStartDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getStartDate(),"MONTH") > 0){
					sql += " AND DATEPART(month, a.effective_from_date) = "+Util.extractDateElement(filter.getStartDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getStartDate(),"YEAR") > 0){
					sql += " AND DATEPART(year,a.effective_from_date) = "+Util.extractDateElement(filter.getStartDate(),"YEAR")+" ";
				}
			}
			if(filter.getEndDate()!=null){
				if(Util.extractDateElement(filter.getEndDate(),"DAY") > 0){
					sql += " AND DATEPART(day, a.effective_to_date) = "+Util.extractDateElement(filter.getEndDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEndDate(),"MONTH") > 0){
					sql += " AND DATEPART(month, a.effective_to_date) = "+Util.extractDateElement(filter.getEndDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEndDate(),"YEAR") > 0){
					sql += " AND DATEPART(year,a.effective_to_date) = "+Util.extractDateElement(filter.getEndDate(),"YEAR")+" ";
				}
			}
			
			
			affiliateSession = ContestAffiliatesDAO.getContestSession();
			if(affiliateSession==null || !affiliateSession.isOpen() || !affiliateSession.isConnected()){
				affiliateSession = getSession();
				ContestAffiliatesDAO.setContestSession(affiliateSession);
			}
			SQLQuery query = affiliateSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("customerId", Hibernate.INTEGER);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("fromDate", Hibernate.TIMESTAMP);
			query.addScalar("toDate", Hibernate.TIMESTAMP);
			query.addScalar("affiliateLives", Hibernate.INTEGER);						
			query.addScalar("customerLives", Hibernate.INTEGER);
			query.addScalar("rewardType", Hibernate.STRING);
			query.addScalar("rewardValue", Hibernate.DOUBLE);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("activeCashReward", Hibernate.DOUBLE);
			query.addScalar("debitedCashReward", Hibernate.DOUBLE);
			query.addScalar("totalCashReward", Hibernate.DOUBLE);
			query.addScalar("voidedCashReward", Hibernate.DOUBLE);
			query.addScalar("totalCreditedRewardDollar", Hibernate.DOUBLE);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);
			query.addScalar("firstName", Hibernate.STRING);
			query.addScalar("lastName", Hibernate.STRING);
			query.addScalar("customerType", Hibernate.STRING);	
			query.addScalar("email", Hibernate.STRING);	
			query.addScalar("phone", Hibernate.STRING);			
			query.addScalar("userId", Hibernate.STRING);
			query.addScalar("updatedBy", Hibernate.STRING);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("referralCode", Hibernate.STRING);
			query.addScalar("customerRewardDollars", Hibernate.DOUBLE);			
			query.addScalar("customerSuperFanStars", Hibernate.INTEGER);
			
			affiliates = query.setResultTransformer(Transformers.aliasToBean(ContestAffiliates.class)).list();
		} catch (Exception e) {
			if(affiliateSession != null && affiliateSession.isOpen()){
				affiliateSession.close();
			}
			e.printStackTrace();
		}
		return affiliates;
	}

	@Override
	public ContestAffiliates getContestAffiliateByCustomerId(Integer customerId) {
		return findSingle("FROM ContestAffiliates WHERE customerId=?",new Object[]{customerId});
	}
	
	@Override
	public List<ContestAffiliateEarning> getContestAffiliatePendingEarning(Integer customerId,GridHeaderFilters filter){
		List<ContestAffiliateEarning> earnings = null;
		try {
			String sql = "select rt.id as id,cc.cust_name as firstName,cc.last_name as lastName,cc.user_id as userId, "+
					"cc.email as email,cc.phone as phone,rt.created_datetime as createdDate, "+
					"cc.signup_date as signupDate,'PENDING' as status,cp.joinDatetime as playedDate "+
					"from RTFQuizMaster.dbo.customer_referral_tracking rt  "+
					"join customer c on rt.referral_customer_id=c.id "+
					"join customer cc on rt.customer_id=cc.id  "+
					"left join (select customer_id, min(join_datetime) as joinDatetime "+  
					"from RTFQuizMaster.dbo.contest_participants "+
					"group by customer_id ) cp on rt.customer_id = cp.customer_id "+
					"where rt.referral_customer_id="+customerId+" and rt.customer_id not in( "+
					"select tireone_customer_id from contest_affiliate_reward_history where affiliate_customer_id="+customerId+") ";
			
			if(filter.getCustomerName()!=null){
				sql+= " AND cc.cust_name like '%"+filter.getCustomerName()+"%'";
			}
			if(filter.getLastName()!=null){
				sql+= " AND cc.last_name like '%"+filter.getLastName()+"%'";
			}
			if(filter.getEmail()!=null){
				sql+= " AND cc.email like '%"+filter.getEmail()+"%'";
			}
			if(filter.getUsername()!=null){
				sql+= " AND cc.user_id like '%"+filter.getUsername()+"%'";
			}
			if(filter.getPhone()!=null){
				sql+= " AND cc.phone like '%"+filter.getPhone()+"%'";
			}
			
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day, rt.created_datetime) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month, rt.created_datetime) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year, rt.created_datetime) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventDateStr()!=null ){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,cc.signup_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,cc.signup_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,cc.signup_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getHittingDateStr()!=null ){
				if(Util.extractDateElement(filter.getHittingDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,cp.joinDatetime) = "+Util.extractDateElement(filter.getHittingDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getHittingDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,cp.joinDatetime) = "+Util.extractDateElement(filter.getHittingDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getHittingDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,cp.joinDatetime) = "+Util.extractDateElement(filter.getHittingDateStr(),"YEAR")+" ";
				}
			}
			
			
			affiliateSession = ContestAffiliatesDAO.getContestSession();
			if(affiliateSession==null || !affiliateSession.isOpen() || !affiliateSession.isConnected()){
				affiliateSession = getSession();
				ContestAffiliatesDAO.setContestSession(affiliateSession);
			}
			SQLQuery query = affiliateSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("firstName", Hibernate.STRING);
			query.addScalar("lastName", Hibernate.STRING);
			query.addScalar("userId", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("phone", Hibernate.STRING);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("signupDate", Hibernate.TIMESTAMP);
			query.addScalar("playedDate", Hibernate.TIMESTAMP);
			query.addScalar("status", Hibernate.STRING);
			
			earnings = query.setResultTransformer(Transformers.aliasToBean(ContestAffiliateEarning.class)).list();
		} catch (Exception e) {
			if(affiliateSession != null && affiliateSession.isOpen()){
				affiliateSession.close();
			}
			e.printStackTrace();
		}
		return earnings;
	}
	
	@Override
	public List<ContestAffiliateEarning> getContestAffiliateEarnings(Integer customerId,GridHeaderFilters filter){
		List<ContestAffiliateEarning> earnings = null;
		try {
			String sql = "select ar.id as id,c.user_id as affilateUserId,cc.cust_name as firstName,cc.last_name as lastName,"+
					"cc.user_id as userId,cc.email as email,cc.phone as phone,ar.reward_type as rewardType,"+
					"ar.lives_to_affiliate_customer as affiliateLives,ar.lives_to_tireone_customer as customerLives,"+
					"ar.cash_rewards as cashReward,ar.reward_dollars as rewardDollars,ar.create_date as createdDate,"+
					"ar.updated_date as updatedDate,ar.status as status,ass.referral_code as referralCode "+
					"from contest_affiliate_reward_history ar join customer c on ar.affiliate_customer_id=c.id "+
					"join customer cc on cc.id=tireone_customer_id join contest_affiliate_settings ass on ar.affiliate_customer_id=ass.customer_id "+
					" WHERE ar.affiliate_customer_id="+customerId;
			
			
			if(filter.getTicketWinnerCount()!=null){
				sql+= " AND ar.lives_to_affiliate_customer="+filter.getTicketWinnerCount();
			}
			if(filter.getPointWinnerCount()!=null){
				sql+= " AND ar.lives_to_tireone_customer="+filter.getPointWinnerCount();
			}
			if(filter.getText1()!=null){
				sql+= " AND ar.reward_type like '%"+filter.getText1()+"%'";
			}
			if(filter.getPrice()!=null){
				sql+= " AND ar.cash_rewards  = "+filter.getPrice();
			}
			if(filter.getText2()!=null){
				sql+= " AND ar.reward_dollars  = "+filter.getText2();
			}
			if(filter.getText3()!=null){
				sql+= " AND c.user_id like '%"+filter.getText3()+"%'";
			}
			if(filter.getCustomerName()!=null){
				sql+= " AND cc.cust_name like '%"+filter.getCustomerName()+"%'";
			}
			if(filter.getLastName()!=null){
				sql+= " AND cc.last_name like '%"+filter.getLastName()+"%'";
			}
			if(filter.getEmail()!=null){
				sql+= " AND cc.email like '%"+filter.getEmail()+"%'";
			}
			if(filter.getUsername()!=null){
				sql+= " AND cc.user_id like '%"+filter.getUsername()+"%'";
			}
			if(filter.getPhone()!=null){
				sql+= " AND cc.phone like '%"+filter.getPhone()+"%'";
			}
			if(filter.getReferrerCode()!=null){
				sql+= " AND ass.referral_code like '%"+filter.getReferrerCode()+"%'";
			}
			if(filter.getStatus()!=null){
				sql+= " AND ar.status like '%"+filter.getStatus()+"%'";
			}
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day, ar.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month, ar.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year, ar.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getUpdatedDate()!=null ){
				if(Util.extractDateElement(filter.getUpdatedDate(),"DAY") > 0){
					sql += " AND DATEPART(day, ar.updated_date) = "+Util.extractDateElement(filter.getUpdatedDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"MONTH") > 0){
					sql += " AND DATEPART(month, ar.updated_date) = "+Util.extractDateElement(filter.getUpdatedDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"YEAR") > 0){
					sql += " AND DATEPART(year, ar.updated_date) = "+Util.extractDateElement(filter.getUpdatedDate(),"YEAR")+" ";
				}
			}
			
			affiliateSession = ContestAffiliatesDAO.getContestSession();
			if(affiliateSession==null || !affiliateSession.isOpen() || !affiliateSession.isConnected()){
				affiliateSession = getSession();
				ContestAffiliatesDAO.setContestSession(affiliateSession);
			}
			SQLQuery query = affiliateSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("affilateUserId", Hibernate.STRING);
			query.addScalar("firstName", Hibernate.STRING);
			query.addScalar("lastName", Hibernate.STRING);
			query.addScalar("userId", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("phone", Hibernate.STRING);
			query.addScalar("rewardType", Hibernate.STRING);
			query.addScalar("affiliateLives", Hibernate.INTEGER);
			query.addScalar("customerLives", Hibernate.INTEGER);
			query.addScalar("cashReward", Hibernate.DOUBLE);
			query.addScalar("rewardDollars", Hibernate.DOUBLE);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("referralCode", Hibernate.STRING);
			
			
			earnings = query.setResultTransformer(Transformers.aliasToBean(ContestAffiliateEarning.class)).list();
		} catch (Exception e) {
			if(affiliateSession != null && affiliateSession.isOpen()){
				affiliateSession.close();
			}
			e.printStackTrace();
		}
		return earnings;
		
	}
}
