package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.datas.PollingCategoryQuestion;
import com.rtw.tracker.datas.PollingCustomerVideo;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.GridSortingUtil;

public class PollingCustomerVideoDAO extends HibernateDAO<Integer, PollingCustomerVideo>
		implements com.rtw.tracker.dao.services.PollingCustomerVideoDAO {

	private static Session pollingSession = null;

	public static Session getPollingSession() {
		return PollingCustomerVideoDAO.pollingSession;
	}

	public static void setPollingSession(Session pollingSession) {
		PollingCustomerVideoDAO.pollingSession = pollingSession;
	}
	
	public List<PollingCustomerVideo> getActiveVideos(){
		return find("FROM PollingCustomerVideo WHERE status='ACTIVE'");
	}
	
	
	@Override
	public List<PollingCustomerVideo> getActiveVideos(String fromDate, String toDate, String name, String videoType, String playType,GridHeaderFilters filter,SharedProperty sharedProperty) {
		List<PollingCustomerVideo> list = null;		
		try {
			String SQLQuery = "select pcv.id as id,video_url as videoUrl,cust_id as customerId,status ,is_winner_or_general as isWinnerOrGeneral,pcv.is_published as isPublished," + 
					"upload_date as uploadDate, c.user_id as userId, c.phone as phoneNo,c.email as emailId,CONCAT(c.cust_name,' ',c.last_name) as customerName " + 
					"from polling_customer_video_uploads pcv with(nolock) inner join customer c with(nolock) on c.id = pcv.cust_id  " + 
					"where 1=1  ";
			
			if(fromDate != null && !fromDate.isEmpty()){
				SQLQuery += " AND pcv.upload_date >= '"+fromDate+"' ";
			}
			if(toDate != null && !toDate.isEmpty()){
				SQLQuery += " AND pcv.upload_date <= '"+toDate+"' ";
			}			 
			if(name != null && !name.isEmpty()){
				SQLQuery += " AND (c.cust_name like '%"+name+"%' or c.email like '%"+name+"%') ";
			}
			
			if(videoType != null && !videoType.isEmpty()){
				if(videoType.equals("WINNER")) {
					SQLQuery += " AND pcv.is_winner_or_general = 1 ";
				}else if(videoType.equals("GENERAL UPLOAD")) {
					SQLQuery += " AND pcv.is_winner_or_general = 0 ";
				} 
			}
			
			if(playType != null && !playType.isEmpty()){
				if(playType.equals("PLAYED")) {
					SQLQuery += " AND pcv.status='PLAYED' ";
				}else if(playType.equals("NOTPLAYED")) {
					SQLQuery += " AND pcv.status='ACTIVE' ";
				} 
			}
			if(filter.getCustomerId() != null){
				SQLQuery += " AND c.customer_id = "+filter.getCustomerId();
			}
			if(filter.getText1() != null && !filter.getText1().isEmpty()){
				SQLQuery += " AND pcv.is_published = "+filter.getText1();
			}
			if(filter.getCustomerName() != null){
				SQLQuery += " AND c.cust_name like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getLastName() != null){
				SQLQuery += " AND c.last_name like '%"+filter.getLastName()+"%' ";
			}
			if(filter.getPhone() != null){
				SQLQuery += " AND c.phone like '%"+filter.getPhone()+"%' ";
			}
						
			String sortingSql = GridSortingUtil.getCustomerVideosSortingSql(filter);
			if(sortingSql!=null && !sortingSql.isEmpty()){
				SQLQuery += " "+sortingSql;
			}else{
				SQLQuery += " order by pcv.upload_date ";
			}
			pollingSession = PollingCustomerVideoDAO.getPollingSession();
			if(pollingSession==null || !pollingSession.isOpen() || !pollingSession.isConnected()){
				pollingSession = getSession();
				PollingCustomerVideoDAO.setPollingSession(pollingSession);
			}
			SQLQuery query = pollingSession.createSQLQuery(SQLQuery);

			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("customerId", Hibernate.INTEGER);
			query.addScalar("videoUrl", Hibernate.STRING);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("isWinnerOrGeneral", Hibernate.BOOLEAN);						
			query.addScalar("userId", Hibernate.STRING); 
			query.addScalar("phoneNo", Hibernate.STRING); 
			query.addScalar("customerName", Hibernate.STRING); 
			query.addScalar("uploadDate", Hibernate.TIMESTAMP);	
			query.addScalar("emailId", Hibernate.STRING);  
			query.addScalar("isPublished", Hibernate.BOOLEAN);  
			
			list = query.setResultTransformer(Transformers.aliasToBean(PollingCustomerVideo.class)).list();
		} catch (Exception e) {
			if(pollingSession != null && pollingSession.isOpen()){
				pollingSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
 
}