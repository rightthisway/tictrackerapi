package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tracker.datas.PollingSponsor;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.GridSortingUtil;
import com.rtw.tracker.pojos.SponsorAutoComplete;

public class PollingSponsorDAO extends HibernateDAO<Integer, PollingSponsor>
		implements com.rtw.tracker.dao.services.PollingSponsorDAO {

	private static Session pollingSponsorSession = null;

	public static Session getPollingSponsorSession() {
		return PollingSponsorDAO.pollingSponsorSession;
	}

	public static void setPollingSponsorSessionn(Session pollingSponsorSession) {
		PollingSponsorDAO.pollingSponsorSession = pollingSponsorSession;
	}

	@Override
	public List<PollingSponsor> getAllPollingSponsor(Integer id, GridHeaderFilters filter, String status) {
		List<PollingSponsor> pollingSponsorList = null;
		try {
			
			StringBuilder sb = new StringBuilder(); 			
			sb.append("select g.id as id, " );
			sb.append(" g.name as name , " );
			sb.append(" g.phone as phone , " );
			sb.append(" g.email as email , " );
			sb.append(" g.description as description , " );
			sb.append(" g.address as address , " );
			sb.append(" g.alt_phone as altPhone , " );
			sb.append(" g.contact_person as contactPerson , " );
			sb.append(" g.status as status , " );
			sb.append(" g.created_by as createdBy , " );
			sb.append(" g.created_date as createdDate , " );
			sb.append(" g.updated_by as updatedBy , " );		
			sb.append(" g.logo_path as imageUrl , " );
			sb.append(" g.updated_date as updatedDate  from polling_sponsors g where 1=1  " );
			
			if (status != null && !status.isEmpty() && !status.equalsIgnoreCase("ALL")) {
				sb.append(" and g.status = '"+status+"'");
			}
			String sortingSql = GridSortingUtil.getPollingSponsorsSortingSql(filter);
			if(sortingSql!=null && !sortingSql.isEmpty()){
				sb.append(" "+sortingSql);
			}else{
				sb.append(" order by g.id desc");
			}			

			pollingSponsorSession = PollingSponsorDAO.getPollingSponsorSession();
			if (pollingSponsorSession == null || !pollingSponsorSession.isOpen() || !pollingSponsorSession.isConnected()) {
				pollingSponsorSession = getSession();				
				PollingSponsorDAO.setPollingSponsorSessionn(pollingSponsorSession);
			}

			
			
			SQLQuery query = pollingSponsorSession.createSQLQuery(sb.toString());
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("name", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("phone", Hibernate.STRING);
			query.addScalar("description", Hibernate.STRING);
			query.addScalar("address", Hibernate.STRING);
			query.addScalar("altPhone", Hibernate.STRING);
			query.addScalar("contactPerson", Hibernate.STRING);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("updatedBy", Hibernate.STRING);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);
			query.addScalar("imageUrl", Hibernate.STRING);

			pollingSponsorList = query.setResultTransformer(Transformers.aliasToBean(PollingSponsor.class)).list();
		} catch (Exception e) {
			if (pollingSponsorSession != null && pollingSponsorSession.isOpen()) {
				pollingSponsorSession.close();
			}
			e.printStackTrace();
		}
		return pollingSponsorList;

	}

	@Override
	public PollingSponsor getPollingSponsorById(Integer id) throws Exception {
		return findSingle("FROM polling_sponsors WHERE id=?",new Object[]{id});
	}
	
	@Override
	public PollingSponsor savePollingSponsor(PollingSponsor pollingSponsor) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public List<SponsorAutoComplete> filterByName(String pattern) {
		List<SponsorAutoComplete> list = null;
		try {
			String sql = "select id as sponsorId,name as name from polling_sponsors with(nolock) where name like '%"+pattern+"%' "
					+ "and status='ACTIVE' order by name " ;
			pollingSponsorSession = PollingSponsorDAO.getPollingSponsorSession();
			if (pollingSponsorSession == null || !pollingSponsorSession.isOpen() || !pollingSponsorSession.isConnected()) {
				pollingSponsorSession = getSession();				
				PollingSponsorDAO.setPollingSponsorSessionn(pollingSponsorSession);
			}
			SQLQuery query = pollingSponsorSession.createSQLQuery(sql);
			query.addScalar("sponsorId", Hibernate.INTEGER);
			query.addScalar("name", Hibernate.STRING); 					
			
			list = query.setResultTransformer(Transformers.aliasToBean(SponsorAutoComplete.class)).list();
			
		} catch (Exception e) {
			if(pollingSponsorSession != null && pollingSponsorSession.isOpen()){
				pollingSponsorSession.close();
			}
			e.printStackTrace(); 
		}
		return list;
	}
	
	@Override
	public Integer getCategoryCountForSponsor(Integer pollSponId) {
		int count = 0;
		try 
		{	
			String sql = "select count(*) from polling_category where sponsor_id = '"+pollSponId+"'";			
			pollingSponsorSession = PollingSponsorDAO.getPollingSponsorSession();
			if (pollingSponsorSession == null || !pollingSponsorSession.isOpen() || !pollingSponsorSession.isConnected()) {
				pollingSponsorSession = getSession();				
				PollingSponsorDAO.setPollingSponsorSessionn(pollingSponsorSession);
			}
			Query query = pollingSponsorSession.createSQLQuery(sql);
			count = (Integer)query.uniqueResult();
		} catch (Exception e) {
			if (pollingSponsorSession != null && pollingSponsorSession.isOpen()) {
				pollingSponsorSession.close();
			}
			e.printStackTrace();
		}
		finally{
			pollingSponsorSession.close();
		}
		return count;
	}

}