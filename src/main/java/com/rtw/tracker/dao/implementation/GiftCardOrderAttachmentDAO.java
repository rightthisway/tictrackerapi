package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.GiftCardOrderAttachment;
import com.rtw.tracker.enums.FileType;

public class GiftCardOrderAttachmentDAO extends HibernateDAO<Integer, GiftCardOrderAttachment> implements com.rtw.tracker.dao.services.GiftCardOrderAttachmentDAO{

	@Override
	public List<GiftCardOrderAttachment> getOrderEGiftCardAttachments(Integer orderId) {
		return find("FROM GiftCardOrderAttachment WHERE orderId=? AND fileType=?",new Object[]{orderId,FileType.EGIFTCARD.toString()});
	}

	@Override
	public List<GiftCardOrderAttachment> getOrderBarcodeAttachments(Integer orderId) {
		return find("FROM GiftCardOrderAttachment WHERE orderId=?  AND fileType=?",new Object[]{orderId,FileType.BARCODE.toString()});
	}

	@Override
	public GiftCardOrderAttachment getOrderAttachmentByTypeAndPosition(Integer orderId, String fileType,Integer postion) {
		return findSingle("FROM GiftCardOrderAttachment WHERE orderId=?  AND fileType=? AND position=?",new Object[]{orderId,fileType,postion});
	}

	@Override
	public List<GiftCardOrderAttachment> getOrderAllAttachments(Integer orderId) {
		return find("FROM GiftCardOrderAttachment WHERE orderId=?",new Object[]{orderId});
	}
	
	@Override
	public List<GiftCardOrderAttachment> getUnProcessedAttachments() {
		return find("FROM GiftCardOrderAttachment WHERE filePath like ?",new Object[]{"%C:%"});
	}

}
