package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.EmailBlastTracking;

public class EmailBlastTrackingDAO extends HibernateDAO<Integer, EmailBlastTracking> implements com.rtw.tracker.dao.services.EmailBlastTrackingDAO{
	
	public List<EmailBlastTracking> getTrackingByTemplateId(Integer templateId){
		return find("FROM EmailBlastTracking WHERE templateId = ?", new Object[]{templateId});
	}

}
