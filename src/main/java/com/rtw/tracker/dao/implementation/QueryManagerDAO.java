package com.rtw.tracker.dao.implementation;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.hibernate.type.EnumType;

import com.rtw.tmat.enums.ProfitLossSign;
import com.rtw.tmat.utils.Util;
import com.rtw.tracker.datas.Artist;
import com.rtw.tracker.datas.CategoryTicketGroup;
import com.rtw.tracker.datas.CityAutoSearch;
import com.rtw.tracker.datas.Country;
import com.rtw.tracker.datas.CrownJewelTeamZones;
import com.rtw.tracker.datas.Customer;
import com.rtw.tracker.datas.CustomerInvoices;
import com.rtw.tracker.datas.Customers;
import com.rtw.tracker.datas.DiscountCodeTracking;
import com.rtw.tracker.datas.Event;
import com.rtw.tracker.datas.EventDetails;
import com.rtw.tracker.datas.FantasyChildCategory;
import com.rtw.tracker.datas.FantasyGrandChildCategory;
import com.rtw.tracker.datas.InvoiceRefund;
import com.rtw.tracker.datas.ManualFedexGeneration;
import com.rtw.tracker.datas.OpenOrderStatus;
import com.rtw.tracker.datas.OpenOrders;
import com.rtw.tracker.datas.PayPalTracking;
import com.rtw.tracker.datas.PaymentHistory;
import com.rtw.tracker.datas.PopularArtist;
import com.rtw.tracker.datas.ProductType;
import com.rtw.tracker.datas.PurchaseOrders;
import com.rtw.tracker.datas.RTFCustomerPromotionalOffer;
import com.rtw.tracker.datas.RTFPromoOffers;
import com.rtw.tracker.datas.RTFPromotionalOfferTracking;
import com.rtw.tracker.datas.RealTicketSectionDetails;
import com.rtw.tracker.datas.SoldTicketDetail;
import com.rtw.tracker.datas.State;
import com.rtw.tracker.datas.TicketGroup;
import com.rtw.tracker.datas.TicketMasterRefund;
import com.rtw.tracker.datas.TrackerBrokers;
import com.rtw.tracker.datas.Venue;
import com.rtw.tracker.datas.WebServiceTracking;
import com.rtw.tracker.enums.CrownJewelOrderStatus;
import com.rtw.tracker.enums.PaymentMethods;
import com.rtw.tracker.fedex.Recipient;
import com.rtw.tracker.pojos.RTFAPITracker;
import com.rtw.tracker.pojos.RtfConfigContestClusterNodes;
import com.rtw.tracker.pojos.ZoneEvent;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.GridSortingUtil;
import com.rtw.tracker.utils.PaginationUtil;

public class QueryManagerDAO {

	private static SessionFactory sessionFactory;
	private static Session staticSession = null;
	
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	
	public static Session getStaticSession() {
	   if(staticSession==null || !staticSession.isOpen() || !staticSession.isConnected()){
		   staticSession = getSessionFactory().openSession();
		   QueryManagerDAO.setStaticSession(staticSession);
	   }
	   return staticSession;
	}

	public static void setStaticSession(Session staticSession) {
		QueryManagerDAO.staticSession = staticSession;
	}
	

	public List<ZoneEvent> getPreSalePopularEvents(){
		Session session =null;
		try{
			StringBuilder sb = new StringBuilder();
			sb.append("select e.id as 'eventId',e.name as 'eventName',CASE WHEN e.event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), e.event_date, 101) END as 'eventDate', ");
			sb.append("CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7))WHEN e.event_time is null THEN 'TBD' END as 'eventTime', ");
			sb.append("a.name as'artistName',v.building as 'venueName',v.city as 'venueCity',v.state as 'venueState',v.country as 'venueCountry',e.presale_disc_zone_flag ");
			sb.append("as 'discountFlag' from event e with(nolock) inner join venue v with(nolock) on e.venue_id = v.id ");
			sb.append("inner join artist a with(nolock) on e.artist_id = a.id where e.id in ");
			sb.append("(select event_id from presale_zone_ticket_group with(nolock) where ticket_status = 1 group by event_id having count(*) > 20) ");
			sb.append("and e.presale_zonetickets_enabled = 1 and e.status = 1 order by e.event_date");
			
			session  = sessionFactory.openSession();
			Query query = session.createSQLQuery(sb.toString());
			List<ZoneEvent> list = query.setResultTransformer(Transformers.aliasToBean(com.rtw.tracker.pojos.ZoneEvent.class)).setFirstResult(0).setMaxResults(50).list();
			return list;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	public List<EventDetails> getArtistDetails(String pageNo) throws Exception {
		Session session =null;
		Integer startFrom = 0;
		try{
			String queryStr = "select distinct a.id as 'artistId',REPLACE(a.name,'\"','') as 'artistName',gc.name as 'grandChildCategoryName'," +
			" gc.id as 'grandChildCategoryId',cc.id as 'childCategoryId',cc.name as 'childCategoryName'," +
			" pc.id as 'parentCategoryId',pc.name as 'parentCategoryName',a.display_on_search AS displayOnSearch" +
			" FROM artist a with(nolock) " +
			" inner join artist_category_mapping ac with(nolock) on ac.artist_id=a.id"+
			" inner join grand_child_category gc with(nolock) on gc.id=ac.grand_child_category_id" +
			" inner join child_category cc with(nolock) on cc.id=ac.child_category_id " +
			" inner join parent_category pc with(nolock) on pc.id=ac.parent_category_id";
			session  = sessionFactory.openSession();
			Query query = session.createSQLQuery(queryStr);
			startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
			List<EventDetails> list = query.setResultTransformer(Transformers.aliasToBean(EventDetails.class)).setFirstResult(startFrom).setMaxResults(PaginationUtil.PAGESIZE).list();
			return list;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	public Integer getArtistDetailsCount() throws Exception {
		Session session =null;
		Integer count = 0;
		try{
			String queryStr = "select count(*) as cnt FROM artist a with(nolock) " +
			" inner join artist_category_mapping ac with(nolock) on ac.artist_id=a.id"+
			" inner join grand_child_category gc with(nolock) on gc.id=ac.grand_child_category_id" +
			" inner join child_category cc with(nolock) on cc.id=ac.child_category_id " +
			" inner join parent_category pc with(nolock) on pc.id=ac.parent_category_id";
			session  = sessionFactory.openSession();
			Query query = session.createSQLQuery(queryStr);
			count = Integer.parseInt(String.valueOf(query.uniqueResult()));	
			return count;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	public List<PopularArtist> getAllActivePopularArtistByProductId(Integer productId,GridHeaderFilters filter,String pageNo){
		Session session =null;
		Integer startFrom = 0;
		try {
			String queryStr = "select distinct a.id as id,a.artist_id as artistId,REPLACE(ar.name,'\"','') as artistName,gc.name as grandChildCategoryName,"+
				"cc.name as childCategoryName,pc.name as parentType,a.created_date AS createdDate,a.created_by AS createdBy "+
				"FROM popular_artist a with(nolock) "+
				"inner join artist ar with(nolock) on a.artist_id=ar.id "+
				"inner join artist_category_mapping ac with(nolock) on ac.artist_id=a.id "+
				"inner join grand_child_category gc with(nolock) on gc.id=ac.grand_child_category_id "+
				"inner join child_category cc with(nolock) on cc.id=ac.child_category_id "+
				"inner join parent_category pc with(nolock) on pc.id=ac.parent_category_id WHERE a.product_id="+productId;
			
			if(filter.getArtistName() != null){
				queryStr += " AND REPLACE(ar.name,'\"','') like '%"+filter.getArtistName()+"%' ";
			}
			if(filter.getGrandChildCategoryName() != null){
				queryStr += " AND gc.name like '%"+filter.getGrandChildCategoryName()+"%' ";
			}
			if(filter.getChildCategoryName() != null){
				queryStr += " AND cc.name like '%"+filter.getChildCategoryName()+"%' ";
			}
			if(filter.getParentCategoryName() != null){
				queryStr += " AND pc.name like '%"+filter.getParentCategoryName()+"%' ";
			}
			if(filter.getCreatedBy() != null){
				queryStr += " AND a.created_by like '%"+filter.getCreatedBy()+"%' ";
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					queryStr += " AND DATEPART(day,a.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					queryStr += " AND DATEPART(month,a.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					queryStr += " AND DATEPART(year,a.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			session  = sessionFactory.openSession();
			Query query = session.createSQLQuery(queryStr);
			startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
			List<PopularArtist> list = query.setResultTransformer(Transformers.aliasToBean(PopularArtist.class)).setFirstResult(startFrom).setMaxResults(PaginationUtil.PAGESIZE).list();
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	
	public Integer getAllActivePopularArtistByProductIdCount(Integer productId){
		Session session =null;
		Integer count = 0;
		try {
			String queryStr = "select  count(*) as cnt FROM popular_artist a with(nolock) "+
				"inner join artist ar with(nolock) on a.artist_id=ar.id "+
				"inner join artist_category_mapping ac with(nolock) on ac.artist_id=a.id "+
				"inner join grand_child_category gc with(nolock) on gc.id=ac.grand_child_category_id "+
				"inner join child_category cc with(nolock) on cc.id=ac.child_category_id "+
				"inner join parent_category pc with(nolock) on pc.id=ac.parent_category_id WHERE a.product_id="+productId;
			session  = sessionFactory.openSession();
			Query query = session.createSQLQuery(queryStr);
			count = Integer.parseInt(String.valueOf(query.uniqueResult()));	
			return count;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	public List<EventDetails> getGrandChildDetails() throws Exception {
		Session session =null;
		try{
			String queryStr = "select distinct gc.id as 'grandChildCategoryId',REPLACE(gc.name,'\"','') as 'grandChildCategoryName'," +
			" cc.id as 'childCategoryId',cc.name as 'childCategoryName'," +
			" pc.id as 'parentCategoryId',pc.name as 'parentCategoryName'" +
			" FROM  grand_child_category gc with(nolock) " +
			" inner join child_category cc with(nolock) on cc.id=gc.child_category_id " +
			" inner join parent_category pc with(nolock) on pc.id=cc.category_id ";
			session  = sessionFactory.openSession();
			Query query = session.createSQLQuery(queryStr);
			List<EventDetails> list = query.setResultTransformer(Transformers.aliasToBean(EventDetails.class)).list();
			return list;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	public List<EventDetails> getVenueDetails() throws Exception {
		Session session =null;
		try{
			String queryStr = "select distinct v.id as 'venueId',REPLACE(v.building,'\"','') as 'building',v.city as 'city', " +
			" v.state as 'state',v.country as 'country',REPLACE(v.postal_code,'\"','') as 'postalCode'" +
			" FROM venue v with(nolock) ";
			session  = sessionFactory.openSession();
			Query query = session.createSQLQuery(queryStr);
			List<EventDetails> list = query.setResultTransformer(Transformers.aliasToBean(EventDetails.class)).list();
			return list;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	public List<ZoneEvent> getCatTicketsPopularEvents(){
		Session session =null;
		try{
			StringBuilder sb = new StringBuilder();
			sb.append("select e.id as 'eventId',e.name as 'eventName',CASE WHEN e.event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), e.event_date, 101) END as 'eventDate', ");
			sb.append("CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7))WHEN e.event_time is null THEN 'TBD' END as 'eventTime', ");
			sb.append("a.name as'artistName',v.building as 'venueName',v.city as 'venueCity',v.state as 'venueState',v.country as 'venueCountry',e.discount_zone_flag ");
			sb.append("as 'discountFlag' from event e with(nolock) inner join venue v with(nolock) on e.venue_id = v.id ");
			sb.append("inner join artist a with(nolock) on e.artist_id = a.id where e.id in ");
			sb.append("(select event_id from minicat_category_ticket with(nolock) where status = 1 group by event_id having count(*) > 20) ");
			sb.append("and e.minicats_enabled = 1 and e.status = 1 order by e.event_date");
			session  = sessionFactory.openSession();
			Query query = session.createSQLQuery(sb.toString());
			List<ZoneEvent> list = query.setResultTransformer(Transformers.aliasToBean(com.rtw.tracker.pojos.ZoneEvent.class)).setFirstResult(0).setMaxResults(50).list();
			return list;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	public List<ZoneEvent> getFirstTenRowsPopularEvents(){
		Session session =null;
		try{
			StringBuilder sb = new StringBuilder();
			sb.append("select e.id as 'eventId',e.name as 'eventName',CASE WHEN e.event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), e.event_date, 101) END as 'eventDate', ");
			sb.append("CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7))WHEN e.event_time is null THEN 'TBD' END as 'eventTime', ");
			sb.append("a.name as'artistName',v.building as 'venueName',v.city as 'venueCity',v.state as 'venueState',v.country as 'venueCountry',e.discount_zone_flag ");
			sb.append("as 'discountFlag' from event e with(nolock) inner join venue v with(nolock) on e.venue_id = v.id ");
			sb.append("inner join artist a with(nolock) on e.artist_id = a.id where e.id in ");
			sb.append("(select event_id from vip_minicat_category_ticket with(nolock) where status = 1 group by event_id having count(*) > 20) ");
			sb.append("and e.vipminicats_enabled = 1 and e.status = 1 order by e.event_date");
			session  = sessionFactory.openSession();
			Query query = session.createSQLQuery(sb.toString());
			List<ZoneEvent> list = query.setResultTransformer(Transformers.aliasToBean(com.rtw.tracker.pojos.ZoneEvent.class)).setFirstResult(0).setMaxResults(50).list();
			return list;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	public List<ZoneEvent> getLastRowTicketsPopularEvents(){
		Session session =null;
		try{
			StringBuilder sb = new StringBuilder();
			sb.append("select e.id as 'eventId',e.name as 'eventName',CASE WHEN e.event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), e.event_date, 101) END as 'eventDate', ");
			sb.append("CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7))WHEN e.event_time is null THEN 'TBD' END as 'eventTime', ");
			sb.append("a.name as'artistName',v.building as 'venueName',v.city as 'venueCity',v.state as 'venueState',v.country as 'venueCountry',e.discount_zone_flag ");
			sb.append("as 'discountFlag' from event e with(nolock) inner join venue v with(nolock) on e.venue_id = v.id ");
			sb.append("inner join artist a with(nolock) on e.artist_id = a.id where e.id in ");
			sb.append("(select event_id from vip_lastrow_minicats_category_ticket with(nolock) where status = 1 group by event_id having count(*) > 20) ");
			sb.append("and e.lastrow_minicat_enabled = 1 and e.status = 1 order by e.event_date");
			session  = sessionFactory.openSession();
			Query query = session.createSQLQuery(sb.toString());
			List<ZoneEvent> list = query.setResultTransformer(Transformers.aliasToBean(com.rtw.tracker.pojos.ZoneEvent.class)).setFirstResult(0).setMaxResults(50).list();
			return list;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	/**
	 * Get all states
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<State> getStates(){
		Session session =null;
		try{
			session  = sessionFactory.openSession();
			List<State> stateList = session.createQuery("FROM State").list();
			return stateList;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	/**
	 * Get all countries
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Country> getCountries(){
		Session session =null;
		try{
			session  = sessionFactory.openSession();
			List<Country> countryList = session.createQuery("FROM Country").list();
			return countryList;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<State> getStatesForCountry(Integer countryId){
		Session session =null;
		try{
			session  = sessionFactory.openSession();
			List<State> stateList = session.createSQLQuery("select * from state s with(nolock) WHERE s.country_id = :countryId")
									.addEntity(State.class)
									.setParameter("countryId", countryId)
									.list();
			return stateList;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public Collection<Venue> getAllVenueDetails(String searchColumn,String searchValue){
		Session session =null;
		try{
			String sql = "select V.id AS id,V.building AS building,V.city AS city,V.state AS state,"+
				"V.country AS country,REPLACE(V.postal_code,'\"','') AS postalCode,Ev.ticketCount AS catTicketCount, "+
				"V.notes AS notes from  venue V with(nolock) "+
				"Left Join (select E.venue_id as venueId,count(C.id) as ticketCount from event_details E with(nolock) "+
				"left join category_ticket_group C with(nolock) "+
				"on E.event_id=C.event_Id where C.status='SOLD' group by E.venue_id) "+
				"Ev on Ev.venueId=V.id WHERE 1=1 ";
			
			List<Object> paramList = new ArrayList<Object>();
			
			if(searchColumn !=null && !searchColumn.isEmpty() && searchValue!=null && !searchValue.isEmpty() ){
				if(searchColumn.equals("venueName")) {
					sql += " AND V.building like :searchValue  ";
					paramList.add("%"+searchValue+"%");	
				} else if(searchColumn.equals("city")) {
					sql += " AND V.city like :searchValue  ";
					paramList.add("%"+searchValue+"%");	
				} else if(searchColumn.equals("state")) {
					sql += " AND V.state like :searchValue  ";
					paramList.add("%"+searchValue+"%");	
				} else if(searchColumn.equals("country")) {
					sql += " AND V.country like :searchValue  ";
					paramList.add("%"+searchValue+"%");	
				}
			}
			sql+=" ORDER BY V.building Asc";
			
			
			session  = sessionFactory.openSession();
			Query query = session.createSQLQuery(sql);
			if(searchColumn !=null && !searchColumn.isEmpty()&& searchValue!=null && !searchValue.isEmpty()){
				query.setParameter("searchValue","%"+searchValue+"%");
			}
			query.setResultTransformer(Transformers.aliasToBean(Venue.class));
			Collection<Venue> venueList = query.list();
			return venueList;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	public List<Integer> getCustomerIdToGenerateReferralCode(String pageNo){
		List<Integer> list = null;
		try{
			String sql = "select id from customer with(nolock) WHERE referrer_code is null or referrer_code='' order by id";
			Session staticSession  = getStaticSession();
			Query query = staticSession.createSQLQuery(sql);
			Integer startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
			list = query.setFirstResult(startFrom).setMaxResults(PaginationUtil.PAGESIZE).list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return list;
	}
	
	
	/**
	 * Get all customers and address
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<Customers> getCustomers(Integer customerId,String customerName,String productType,Integer brokerId,GridHeaderFilters filter,String pageNo){
		Session session =null;
		Integer startFrom=0;
		try{
			String CUSTOMER_LIST_SQL = "select c.id as customerId,c.customer_type as customerType, " +
					"c.cust_name as customerName, " +
					"c.last_name as lastName, " +
					"c.email as customerEmail, " +
					"c.product_type as productType, " +
					"c.signup_type as signupType, " +
					"c.customer_level as customerStatus, " +
					"c.company_name as companyName, "+
					"c.is_client as isClient, "+
					"c.is_broker as isBroker, "+
					"c.is_blocked as isBlocked, "+
					"ca.address_line1 as addressLine1, " +
					"ca.address_line2 as addressLine2, " +
					"ca.city as city, " +
					"ca.zip_code as zipCode, " +
					"c.phone as phone,c.user_id as userId, " +
					"c.broker_id as brokerId, " +
					"ct.country_name as country, " +
					"s.name as state,cl.active_reward_points as rewardPoints, "+
					"ca.id AS billingAddressId,"+
					"c.referrer_code as referrerCode from customer c with(nolock) " +
					"left join cust_loyalty_reward_info cl with(nolock) on cl.cust_id=c.id " +
					"left join customer_address ca with(nolock) on(ca.customer_id = c.id and ca.address_type='BILLING_ADDRESS')" +
					"left join country ct with(nolock) on(ct.id=ca.country)" +
					"left join state s with(nolock) on(s.id=ca.state) WHERE c.broker_id="+brokerId+" ";
				
				if(customerId!= null && customerId > 0){
					CUSTOMER_LIST_SQL += " AND c.id = "+customerId+"";
				}
				if(customerName != null && !customerName.isEmpty()){
					if(customerName.contains(" ")){
						String customerNameArr[] = customerName.split(" ");
						if(customerNameArr.length>1){
							CUSTOMER_LIST_SQL += " AND c.cust_name like '%"+customerNameArr[0]+"%' AND c.last_name like '%"+customerNameArr[1]+"%' ";
						}
					}else{
						CUSTOMER_LIST_SQL += " AND (c.cust_name like '%"+customerName+"%' OR c.last_name like '%"+customerName+"%') ";
					}
				}
				if(productType!=null && !productType.isEmpty()){
					CUSTOMER_LIST_SQL +=" AND c.product_type like '"+productType+"' ";
				}
				if(filter.getCustomerId() != null && filter.getCustomerId() > 0){
					CUSTOMER_LIST_SQL += " AND c.id = "+filter.getCustomerId() +" ";
				}
				if(filter.getRewardPoints() != null){
					CUSTOMER_LIST_SQL += " AND cl.active_reward_points = "+filter.getRewardPoints() +" ";
				}
				if(filter.getCustomerType() != null && !filter.getCustomerType().isEmpty()){
					CUSTOMER_LIST_SQL += " AND c.customer_type like '%"+filter.getCustomerType()+"%'";
				}
				if(filter.getCustomerName() != null){
					CUSTOMER_LIST_SQL += " AND c.cust_name like '%"+filter.getCustomerName()+"%'";
				}
				if(filter.getLastName() != null){
					CUSTOMER_LIST_SQL += " AND c.last_name like '%"+filter.getLastName()+"%'";
				}
				if(filter.getProductType() != null){
					CUSTOMER_LIST_SQL +=" AND c.product_type like '%"+filter.getProductType()+"%'";
				}
				if(filter.getText1()!= null){
					if(filter.getText1().equalsIgnoreCase("BLOCKED")){
						CUSTOMER_LIST_SQL +=" AND c.is_blocked = 1 ";
					}else if(filter.getText1().equalsIgnoreCase("ACTIVE")){
						CUSTOMER_LIST_SQL +=" AND c.is_blocked = 0 ";
					}
					
				}
				if(filter.getSignupType() != null){
					CUSTOMER_LIST_SQL +=" AND c.signup_type like '%"+filter.getSignupType()+"%'";
				}
				if(filter.getIsClient() != null){
					CUSTOMER_LIST_SQL +=" AND c.is_client= "+filter.getIsClient()+"";
				}
				if(filter.getIsBroker() != null){
					CUSTOMER_LIST_SQL +=" AND c.is_broker= "+filter.getIsBroker()+"";
				}
				if(filter.getCustomerStatus() != null){
					CUSTOMER_LIST_SQL +=" AND c.customer_level like '%"+filter.getCustomerStatus()+"%'";
				}
				if(filter.getCompanyName() != null){
					CUSTOMER_LIST_SQL +=" AND c.company_name like '%"+filter.getCompanyName()+"%'";
				}
				if(filter.getEmail() != null){
					CUSTOMER_LIST_SQL += " AND c.email like '%"+filter.getEmail()+"%'";	
				}
				if(filter.getAddressLine1() != null){
					CUSTOMER_LIST_SQL += " AND ca.address_line1 like '%"+filter.getAddressLine1()+"%'";
				}
				if(filter.getAddressLine2() != null){
					CUSTOMER_LIST_SQL += " AND ca.address_line2 like '%"+filter.getAddressLine2()+"%'";
				}
				if(filter.getCity() != null){
					CUSTOMER_LIST_SQL += " AND ca.city like '%"+filter.getCity()+"%'";
				}
				if(filter.getState() != null){
					CUSTOMER_LIST_SQL += " AND s.name like '%"+filter.getState()+"%'";
				}
				if(filter.getCountry() != null){
					CUSTOMER_LIST_SQL += " AND ct.country_name like '%"+filter.getCountry()+"%'";
				}
				if(filter.getZipCode() != null){
					CUSTOMER_LIST_SQL += " AND ca.zip_code like '%"+filter.getZipCode()+"%'";
				}
				if(filter.getPhone() != null){
					CUSTOMER_LIST_SQL += " AND c.phone like '%"+filter.getPhone()+"%'";
				}
				if(filter.getReferrerCode() != null){
					CUSTOMER_LIST_SQL += " AND c.referrer_code like '%"+filter.getReferrerCode()+"%'";
				}
				if(filter.getUsername() != null && !filter.getUsername().isEmpty()){
					CUSTOMER_LIST_SQL += " AND c.user_id like '%"+filter.getUsername()+"%'";
				}
				/*
				if(searchBy!=null && !searchBy.isEmpty() && searchValue!=null && !searchValue.isEmpty()){
					if(searchBy.equalsIgnoreCase("name")){
						CUSTOMER_LIST_SQL += " AND c.cust_name like '%"+searchValue+"%'";
					}else if(searchBy.equalsIgnoreCase("email")){
						CUSTOMER_LIST_SQL += " AND c.email='"+searchValue+"'";
					}else if(searchBy.equalsIgnoreCase("companyName")){
						CUSTOMER_LIST_SQL += " AND c.company_name like '%"+searchValue+"%'";
					}
					
				}
				
				if(customerType!=null && !customerType.isEmpty()){
					if(customerType.equalsIgnoreCase("client")){
						CUSTOMER_LIST_SQL +=" AND c.is_client=1 ";
					}else if(customerType.equalsIgnoreCase("broker")){
						CUSTOMER_LIST_SQL +=" AND c.is_broker=1 ";
					}
				}
				*/
				String sortingSql = GridSortingUtil.getCustomerSortingSql(filter);
				if(sortingSql!=null && !sortingSql.isEmpty()){
					CUSTOMER_LIST_SQL += sortingSql;
				}else{
					CUSTOMER_LIST_SQL += " ORDER BY c.id desc";
				}
				
			staticSession = getStaticSession();
			SQLQuery query = staticSession.createSQLQuery(CUSTOMER_LIST_SQL);
			query.addScalar("customerId", Hibernate.INTEGER);
			query.addScalar("brokerId", Hibernate.INTEGER);
			query.addScalar("customerType", Hibernate.STRING);
			query.addScalar("customerName", Hibernate.STRING);
			query.addScalar("lastName", Hibernate.STRING);
			query.addScalar("customerEmail", Hibernate.STRING);
			query.addScalar("productType", Hibernate.STRING);
			query.addScalar("signupType", Hibernate.STRING);
			query.addScalar("customerStatus", Hibernate.STRING);
			query.addScalar("companyName", Hibernate.STRING);
			query.addScalar("userId", Hibernate.STRING);
			query.addScalar("isClient", Hibernate.BOOLEAN);
			query.addScalar("isBroker", Hibernate.BOOLEAN);
			query.addScalar("isBlocked", Hibernate.BOOLEAN);
			query.addScalar("addressLine1", Hibernate.STRING);
			query.addScalar("addressLine2", Hibernate.STRING);
			query.addScalar("city", Hibernate.STRING);
			query.addScalar("zipCode", Hibernate.STRING);
			query.addScalar("phone", Hibernate.STRING);
			query.addScalar("country", Hibernate.STRING);
			query.addScalar("rewardPoints", Hibernate.DOUBLE);
			query.addScalar("billingAddressId", Hibernate.INTEGER);
			query.addScalar("referrerCode", Hibernate.STRING);
			startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
			Collection<Customers> customerList = query.setResultTransformer(Transformers.aliasToBean(Customers.class))
													.setFirstResult(startFrom)
													.setMaxResults(PaginationUtil.PAGESIZE)
													.list();
			return customerList;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Get all customers and address
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<Customers> getCustomersToExport(Integer id,String searchBy,String searchValue,String productType,Integer brokerId,GridHeaderFilters filter,String customerType){
		try{
			String CUSTOMER_LIST_SQL = "select c.id as customerId,c.customer_type as customerType, " +
					"c.cust_name as customerName, " +
					"c.last_name as lastName, " +
					"c.email as customerEmail, " +
					"c.product_type as productType, " +
					"c.signup_type as signupType, " +
					"c.customer_level as customerStatus, " +
					"c.company_name as companyName, "+
					"c.is_client as isClient, "+
					"c.is_broker as isBroker, "+
					"ca.address_line1 as addressLine1, " +
					"ca.address_line2 as addressLine2, " +
					"ca.city as city, " +
					"ca.zip_code as zipCode, " +
					"c.phone as phone, " +
					"c.broker_id as brokerId, " +
					"ct.country_name as country, " +
					"s.name as state, "+ 
					//"cl.active_reward_points as rewardPoints, "+
					"ca.id AS billingAddressId, "+
					"c.referrer_code as referrerCode from customer c with(nolock) " +
					"left join cust_loyalty_reward_info cl with(nolock) on cl.cust_id=c.id " +
					"left join customer_address ca with(nolock) on(ca.customer_id = c.id and ca.address_type='BILLING_ADDRESS')" +
					"left join country ct with(nolock) on(ct.id=ca.country)" +
					"left join state s with(nolock) on(s.id=ca.state) "+
					"WHERE c.broker_id="+brokerId+" ";				
			
				if(id!=null && id >0){
					CUSTOMER_LIST_SQL += " AND c.id="+id;
				}
				if(productType!=null && !productType.isEmpty()){
					CUSTOMER_LIST_SQL +=" AND c.product_type like '"+productType+"' ";
				}
				
				if(searchBy!=null && !searchBy.isEmpty() && searchValue!=null && !searchValue.isEmpty()){
					if(searchBy.equalsIgnoreCase("name")){
						CUSTOMER_LIST_SQL += " AND c.cust_name like '%"+searchValue+"%'";
					}else if(searchBy.equalsIgnoreCase("email")){
						CUSTOMER_LIST_SQL += " AND c.email='"+searchValue+"'";
					}else if(searchBy.equalsIgnoreCase("companyName")){
						CUSTOMER_LIST_SQL += " AND c.company_name like '%"+searchValue+"%'";
					}					
				}
				
				if(customerType!=null && !customerType.isEmpty()){
					if(customerType.equalsIgnoreCase("client")){
						CUSTOMER_LIST_SQL +=" AND c.is_client=1 ";
					}else if(customerType.equalsIgnoreCase("broker")){
						CUSTOMER_LIST_SQL +=" AND c.is_broker=1 ";
					}
				}
				
				if(filter.getCustomerId() != null && filter.getCustomerId() > 0){
					CUSTOMER_LIST_SQL += " AND c.id = "+filter.getCustomerId() +" ";
				}
				if(filter.getRewardPoints() != null){
					CUSTOMER_LIST_SQL += " AND cl.active_reward_points = "+filter.getRewardPoints() +" ";
				}
				if(filter.getCustomerType() != null && !filter.getCustomerType().isEmpty()){
					CUSTOMER_LIST_SQL += " AND c.customer_type like '%"+filter.getCustomerType()+"%'";
				}
				if(filter.getCustomerName() != null){
					CUSTOMER_LIST_SQL += " AND c.cust_name like '%"+filter.getCustomerName()+"%'";
				}
				if(filter.getLastName() != null){
					CUSTOMER_LIST_SQL += " AND c.last_name like '%"+filter.getLastName()+"%'";
				}
				if(filter.getProductType() != null){
					CUSTOMER_LIST_SQL +=" AND c.product_type like '%"+filter.getProductType()+"%'";
				}
				if(filter.getSignupType() != null){
					CUSTOMER_LIST_SQL +=" AND c.signup_type like '%"+filter.getSignupType()+"%'";
				}
				if(filter.getIsClient() != null){
					CUSTOMER_LIST_SQL +=" AND c.is_client= "+filter.getIsClient()+"";
				}
				if(filter.getIsBroker() != null){
					CUSTOMER_LIST_SQL +=" AND c.is_broker= "+filter.getIsBroker()+"";
				}
				if(filter.getCustomerStatus() != null){
					CUSTOMER_LIST_SQL +=" AND c.customer_level like '%"+filter.getCustomerStatus()+"%'";
				}
				if(filter.getCompanyName() != null){
					CUSTOMER_LIST_SQL +=" AND c.company_name like '%"+filter.getCompanyName()+"%'";
				}
				if(filter.getEmail() != null){
					CUSTOMER_LIST_SQL += " AND c.email like '%"+filter.getEmail()+"%'";	
				}
				if(filter.getAddressLine1() != null){
					CUSTOMER_LIST_SQL += " AND ca.address_line1 like '%"+filter.getAddressLine1()+"%'";
				}
				if(filter.getAddressLine2() != null){
					CUSTOMER_LIST_SQL += " AND ca.address_line2 like '%"+filter.getAddressLine2()+"%'";
				}
				if(filter.getCity() != null){
					CUSTOMER_LIST_SQL += " AND ca.city like '%"+filter.getCity()+"%'";
				}
				if(filter.getState() != null){
					CUSTOMER_LIST_SQL += " AND s.name like '%"+filter.getState()+"%'";
				}
				if(filter.getCountry() != null){
					CUSTOMER_LIST_SQL += " AND ct.country_name like '%"+filter.getCountry()+"%'";
				}
				if(filter.getZipCode() != null){
					CUSTOMER_LIST_SQL += " AND ca.zip_code like '%"+filter.getZipCode()+"%'";
				}
				if(filter.getPhone() != null){
					CUSTOMER_LIST_SQL += " AND c.phone like '%"+filter.getPhone()+"%'";
				}
				if(filter.getReferrerCode() != null){
					CUSTOMER_LIST_SQL += " AND c.referrer_code like '%"+filter.getReferrerCode()+"%'";
				}
			staticSession  = getStaticSession();
			Query query = staticSession.createSQLQuery(CUSTOMER_LIST_SQL);
			Collection<Customers> customerList = query.setResultTransformer(Transformers.aliasToBean(Customers.class)).list();
			return customerList;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public Collection<Customers> getClientReportData(String isMailSent,String customerCat,String customerType,String city,String state,String country,String venueId,
			String productType,String grandChildIds,String childIds,String artists,GridHeaderFilters filter,String pageNo,Boolean isPagination, String fromDate, String toDate){
		Integer startFrom=0;
		try{
			String CUSTOMER_LIST_SQL = "select distinct c.id as customerId,c.customer_type as customerType, " +
					"c.cust_name as customerName, " +
					"c.last_name as lastName, " +
					"c.email as customerEmail, " +
					"c.product_type as productType, " +
					"c.signup_type as signupType, " +
					"c.customer_level as customerStatus, " +
					"c.company_name as companyName, "+
					"c.is_client as isClient, "+
					"c.is_broker as isBroker, "+
					"ca.address_line1 as addressLine1, " +
					"ca.address_line2 as addressLine2, " +
					"ca.city as city, " +
					"ca.zip_code as zipCode, " +
					"c.phone as phone, " +
					"ct.country_name as country, " +
					"s.name as state, "+
					"ca.id AS billingAddressId," +
					"c.is_retail_cust as isRetailCust," +
					"c.is_mail_sent as isMailSent,"+
					"c.referrer_code as referrerCode from customer c with(nolock) " +
					"left join customer_order co with(nolock) on c.id=co.customer_id " +
					"left join event_details e with(nolock) on e.event_id=co.event_id " +
					"left join event_artist_details ae with(nolock) on e.event_id=ae.event_id " +
					"left join grand_child_category gcc with(nolock) on gcc.id=e.grand_child_category_id " +
					"left join child_category cc with(nolock) on cc.id=e.child_category_id " +
					"left join customer_address ca with(nolock) on(ca.customer_id = c.id and ca.address_type='BILLING_ADDRESS')" +
					"left join country ct with(nolock) on(ct.id=ca.country)" +
					"left join state s with(nolock) on(s.id=ca.state) "+
					"WHERE c.product_type like 'REWARDTHEFAN' AND c.is_bounce=0 AND c.is_unsub=0 and c.is_test_account=0 ";
				
				if(venueId!=null && !venueId.isEmpty()){
					if(venueId.endsWith(",")){
						venueId = venueId.substring(0,venueId.length()-1);
					}
					CUSTOMER_LIST_SQL += " AND e.building in("+venueId+") ";
				}
				if(customerType!=null && !customerType.isEmpty()){
					if(customerType.equalsIgnoreCase("CLIENT")){
						CUSTOMER_LIST_SQL += " AND c.is_client=1 ";
					}else if(customerType.equalsIgnoreCase("BROKER")){
						CUSTOMER_LIST_SQL += " AND c.is_broker=1 ";
					}
				}
				if(customerCat!=null && !customerCat.isEmpty()){
					if(customerCat.equalsIgnoreCase("RETAIL")){
						CUSTOMER_LIST_SQL += " AND c.is_retail_cust=1 ";
					}
				}
				if(filter.getRetailCustomer()!=null && !filter.getRetailCustomer().isEmpty()){
					if(filter.getRetailCustomer().equalsIgnoreCase("YES")){
						CUSTOMER_LIST_SQL += " AND c.is_retail_cust=1 ";
					}else{
						CUSTOMER_LIST_SQL += " AND c.is_retail_cust=0 ";
					}
				}
				if(isMailSent!=null && !isMailSent.isEmpty()){
					if(isMailSent.equalsIgnoreCase("YES")){
						CUSTOMER_LIST_SQL += " AND c.is_mail_sent=1 ";
					}else if(isMailSent.equalsIgnoreCase("NO")){
						CUSTOMER_LIST_SQL += " AND c.is_mail_sent=0 ";
					}
				}
				if(filter.getEmailSent()!=null && !filter.getEmailSent().isEmpty()){
					if(filter.getEmailSent().equalsIgnoreCase("YES")){
						CUSTOMER_LIST_SQL += " AND c.is_mail_sent=1 ";
					}else{
						CUSTOMER_LIST_SQL += " AND c.is_mail_sent=0 ";
					}
				}
				if(artists!=null && !artists.isEmpty()){
					if(artists.endsWith(",")){
						artists = artists.substring(0,artists.length()-1);
					}
					CUSTOMER_LIST_SQL += " AND ae.artist_name in("+artists+") ";
				}
				if(city != null && !city.isEmpty()){
					CUSTOMER_LIST_SQL += " AND ca.city in("+city+") ";
				}
				if(grandChildIds!=null && !grandChildIds.isEmpty()){
					if(grandChildIds.endsWith(",")){
						grandChildIds = grandChildIds.substring(0,grandChildIds.length()-1);
					}
					CUSTOMER_LIST_SQL += " AND gcc.tn_grand_child_category_id in("+grandChildIds+") ";
				}
				
				if(childIds!=null && !childIds.isEmpty()){
					if(childIds.endsWith(",")){
						childIds = childIds.substring(0,childIds.length()-1);
					}
					CUSTOMER_LIST_SQL += " AND cc.tn_child_category_id in("+childIds+") ";
				}
				
				if(state != null && !state.isEmpty()){
					if(state.endsWith(",")){
						state = state.substring(0,state.length()-1);
					}
					CUSTOMER_LIST_SQL += " AND s.id in("+state+") ";
				}
				if(country != null && !country.isEmpty()){
					if(country.endsWith(",")){
						country = country.substring(0,country.length()-1);
					}
					CUSTOMER_LIST_SQL += " AND ct.id in("+country+") ";
				}
				if(fromDate != null && !fromDate.isEmpty()){
					CUSTOMER_LIST_SQL += " AND c.last_updated_timestamp >= '"+fromDate+"' ";
				}
				if(toDate != null && !toDate.isEmpty()){
					CUSTOMER_LIST_SQL += " AND c.last_updated_timestamp <= '"+toDate+"' ";
				}
				if(filter.getCustomerType() != null && !filter.getCustomerType().isEmpty()){
					CUSTOMER_LIST_SQL += " AND c.customer_type like '%"+filter.getCustomerType()+"%'";
				}
				if(filter.getCustomerName() != null){
					CUSTOMER_LIST_SQL += " AND c.cust_name like '%"+filter.getCustomerName()+"%'";
				}
				if(filter.getLastName() != null){
					CUSTOMER_LIST_SQL += " AND c.last_name like '%"+filter.getLastName()+"%'";
				}
				if(filter.getProductType() != null){
					CUSTOMER_LIST_SQL +=" AND c.product_type like '%"+filter.getProductType()+"%'";
				}
				if(filter.getSignupType() != null){
					CUSTOMER_LIST_SQL +=" AND c.signup_type like '%"+filter.getSignupType()+"%'";
				}
				if(filter.getIsClient() != null){
					CUSTOMER_LIST_SQL +=" AND c.is_client= "+filter.getIsClient()+"";
				}
				if(filter.getIsBroker() != null){
					CUSTOMER_LIST_SQL +=" AND c.is_broker= "+filter.getIsBroker()+"";
				}
				if(filter.getCustomerStatus() != null){
					CUSTOMER_LIST_SQL +=" AND c.customer_level like '%"+filter.getCustomerStatus()+"%'";
				}
				if(filter.getCompanyName() != null){
					CUSTOMER_LIST_SQL +=" AND c.company_name like '%"+filter.getCompanyName()+"%'";
				}
				if(filter.getEmail() != null){
					CUSTOMER_LIST_SQL += " AND c.email like '%"+filter.getEmail()+"%'";	
				}
				if(filter.getAddressLine1() != null){
					CUSTOMER_LIST_SQL += " AND ca.address_line1 like '%"+filter.getAddressLine1()+"%'";
				}
				if(filter.getAddressLine2() != null){
					CUSTOMER_LIST_SQL += " AND ca.address_line2 like '%"+filter.getAddressLine2()+"%'";
				}
				if(filter.getCity() != null){
					CUSTOMER_LIST_SQL += " AND ca.city like '"+filter.getCity()+"'";
				}
				if(filter.getState() != null){
					CUSTOMER_LIST_SQL += " AND s.name like '"+filter.getState()+"'";
				}
				if(filter.getCountry() != null){
					CUSTOMER_LIST_SQL += " AND ct.country_name like '"+filter.getCountry()+"'";
				}
				if(filter.getZipCode() != null){
					CUSTOMER_LIST_SQL += " AND ca.zip_code like '"+filter.getZipCode()+"'";
				}
				if(filter.getPhone() != null){
					CUSTOMER_LIST_SQL += " AND c.phone like '"+filter.getPhone()+"'";
				}
				if(filter.getReferrerCode() != null){
					CUSTOMER_LIST_SQL += " AND c.referrer_code like '"+filter.getReferrerCode()+"'";
				}
				if(isPagination)
				{
					String sortingSql = GridSortingUtil.getCustomerSortingSql(filter);
					if(sortingSql!=null && !sortingSql.isEmpty()){
						CUSTOMER_LIST_SQL += sortingSql;
					}
				}
			staticSession  = getStaticSession();
			Query query = staticSession.createSQLQuery(CUSTOMER_LIST_SQL);
			Collection<Customers> customerList = null;
			if(isPagination){
				startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
				customerList = query.setResultTransformer(Transformers.aliasToBean(Customers.class))
														.setFirstResult(startFrom)
														.setMaxResults(PaginationUtil.PAGESIZE)
														.list();
			}else{
				customerList = query.setResultTransformer(Transformers.aliasToBean(Customers.class)).list();
			}
			
			return customerList;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public Collection<Customers> getClientReportDataRTWRTW2(String isMailSent,String customerCat,String customerType,String city,String state,String country,String venueId,
			String productType,String grandChildIds,String childIds,String artists,GridHeaderFilters filter,String pageNo,Boolean isPagination,String fromDate, String toDate){
		Integer startFrom=0;
		try{
			String CUSTOMER_LIST_SQL = "select distinct customer_id as customerId, " +
					"first_name as customerName, " +
					"last_name as lastName, " +
					"email as customerEmail, " +
					"product_type as productType, " +
					"company_name as companyName, "+
					"is_client as isClient, "+
					"is_broker as isBroker, "+
					"street_address1 as addressLine1, " +
					"street_address2 as addressLine2, " +
					"city as city, " +
					"postal_code as zipCode, " +
					"phone_number as phone, " +
					"country as country, " +
					"state as state, "+
					"is_mail_sent as isMailSent, " +
					"is_retail_cust as isRetailCust "+
					"from email_blast with(nolock) "+
					"where is_bounce=0 and is_unsub=0 ";
				
				if(productType!=null && !productType.isEmpty()){
					CUSTOMER_LIST_SQL +=" AND product_type IN("+productType+") ";
				}
				if(artists!=null && !artists.isEmpty()){
					if(artists.endsWith(",")){
						artists = artists.substring(0,artists.length()-1);
					}
					CUSTOMER_LIST_SQL += " AND artist_name in("+artists+") ";
				}
				if(customerCat!=null && !customerCat.isEmpty()){
					if(customerCat.equalsIgnoreCase("RETAIL")){
						CUSTOMER_LIST_SQL += " AND is_retail_cust=1 ";
					}
				}
				if(filter.getRetailCustomer()!=null && !filter.getRetailCustomer().isEmpty()){
					if(filter.getRetailCustomer().equalsIgnoreCase("YES")){
						CUSTOMER_LIST_SQL += " AND is_retail_cust=1";
					}else{
						CUSTOMER_LIST_SQL += " AND is_retail_cust=0";
					}
				}
				if(isMailSent!=null && !isMailSent.isEmpty()){
					if(isMailSent.equalsIgnoreCase("YES")){
						CUSTOMER_LIST_SQL += " AND is_mail_sent=1 ";
					}else if(isMailSent.equalsIgnoreCase("NO")){
						CUSTOMER_LIST_SQL += " AND is_mail_sent=0 ";
					}
				}
				if(filter.getEmailSent()!=null && !filter.getEmailSent().isEmpty()){
					if(filter.getEmailSent().equalsIgnoreCase("YES")){
						CUSTOMER_LIST_SQL += " AND is_mail_sent=1";
					}else{
						CUSTOMER_LIST_SQL += " AND is_mail_sent=0";
					}
				}
				
				if(customerType!=null && !customerType.isEmpty()){
					if(customerType.equalsIgnoreCase("CLIENT")){
						CUSTOMER_LIST_SQL += " AND is_client=1 ";
					}else if(customerType.equalsIgnoreCase("BROKER")){
						CUSTOMER_LIST_SQL += " AND is_broker=1 ";
					}
				}
				if(venueId!=null && !venueId.isEmpty()){
					if(venueId.endsWith(",")){
						venueId = venueId.substring(0,venueId.length()-1);
					}
					CUSTOMER_LIST_SQL += " AND venue_name in("+venueId+") ";
				}
				if(city != null && !city.isEmpty()){
					CUSTOMER_LIST_SQL += " AND city in("+city+") ";
				}
				if(grandChildIds!=null && !grandChildIds.isEmpty()){
					if(grandChildIds.endsWith(",")){
						grandChildIds = grandChildIds.substring(0,grandChildIds.length()-1);
					}
					CUSTOMER_LIST_SQL += " AND grandchild_category_id in("+grandChildIds+") ";
				}
				if(childIds!=null && !childIds.isEmpty()){
					if(childIds.endsWith(",")){
						childIds = childIds.substring(0,childIds.length()-1);
					}
					CUSTOMER_LIST_SQL += " AND child_category_id in("+childIds+") ";
				}
				if(state != null && !state.isEmpty()){
					if(state.endsWith(",")){
						state = state.substring(0,state.length()-1);
					}
					CUSTOMER_LIST_SQL += " AND state_id in("+state+") ";
				}
				if(country != null && !country.isEmpty()){
					if(country.endsWith(",")){
						country = country.substring(0,country.length()-1);
					}
					CUSTOMER_LIST_SQL += " AND country_id in("+country+") ";
				}
				if(fromDate != null && !fromDate.isEmpty()){
					CUSTOMER_LIST_SQL += " AND last_updated_timestamp >= '"+fromDate+"' ";
				}
				if(toDate != null && !toDate.isEmpty()){
					CUSTOMER_LIST_SQL += " AND last_updated_timestamp <= '"+toDate+"' ";
				}
				if(filter.getCustomerName() != null){
					CUSTOMER_LIST_SQL += " AND first_name like '%"+filter.getCustomerName()+"%'";
				}
				if(filter.getLastName() != null){
					CUSTOMER_LIST_SQL += " AND last_name like '%"+filter.getLastName()+"%'";
				}
				if(filter.getProductType() != null){
					CUSTOMER_LIST_SQL +=" AND product_type like '%"+filter.getProductType()+"%'";
				}
				if(filter.getIsClient() != null){
					CUSTOMER_LIST_SQL +=" AND is_client= "+filter.getIsClient()+"";
				}
				if(filter.getIsBroker() != null){
					CUSTOMER_LIST_SQL +=" AND is_broker= "+filter.getIsBroker()+"";
				}
				
				if(filter.getCompanyName() != null){
					CUSTOMER_LIST_SQL +=" AND company_name like '%"+filter.getCompanyName()+"%'";
				}
				if(filter.getEmail() != null){
					CUSTOMER_LIST_SQL += " AND email like '%"+filter.getEmail()+"%'";	
				}
				if(filter.getAddressLine1() != null){
					CUSTOMER_LIST_SQL += " AND street_address1 like '%"+filter.getAddressLine1()+"%'";
				}
				if(filter.getAddressLine2() != null){
					CUSTOMER_LIST_SQL += " AND street_address2 like '%"+filter.getAddressLine2()+"%'";
				}
				if(filter.getCity() != null){
					CUSTOMER_LIST_SQL += " AND city like '"+filter.getCity()+"'";
				}
				if(filter.getState() != null){
					CUSTOMER_LIST_SQL += " AND state like '"+filter.getState()+"'";
				}
				if(filter.getCountry() != null){
					CUSTOMER_LIST_SQL += " AND country like '"+filter.getCountry()+"'";
				}
				if(filter.getZipCode() != null){
					CUSTOMER_LIST_SQL += " AND postal_code like '"+filter.getZipCode()+"'";
				}
				if(filter.getPhone() != null){
					CUSTOMER_LIST_SQL += " AND phone_number like '"+filter.getPhone()+"'";
				}
			staticSession  = getStaticSession();
			SQLQuery query = staticSession.createSQLQuery(CUSTOMER_LIST_SQL);
			Collection<Customers> customerList = null;
			if(isPagination){
				startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
				customerList = query.setResultTransformer(Transformers.aliasToBean(Customers.class))
														.setFirstResult(startFrom)
														.setMaxResults(PaginationUtil.PAGESIZE)
														.list();
			}else{
				customerList = query.setResultTransformer(Transformers.aliasToBean(Customers.class)).list();
			}
			
			return customerList;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public Integer getClientReportDataCount(String isMailSent,String customerCat,String customerType,String city,String state,String country,String venueId,
			String productType,String grandChildIds,String childIds,String artists,GridHeaderFilters filter, String fromDate, String toDate){
		try{
			String CUSTOMER_LIST_SQL = "select count(distinct c.id) from customer c with(nolock) " +
					"left join customer_order co with(nolock) on c.id=co.customer_id "+
					"left join event_details e with(nolock) on e.event_id=co.event_id " +
					"left join event_artist_details ae with(nolock) on e.event_id=ae.event_id " +
					"left join grand_child_category gcc with(nolock) on gcc.id=e.grand_child_category_id " +
					"left join child_category cc with(nolock) on cc.id=e.child_category_id " +
					"left join customer_address ca with(nolock) on(ca.customer_id = c.id and ca.address_type='BILLING_ADDRESS')" +
					"left join country ct with(nolock) on(ct.id=ca.country)" +
					"left join state s with(nolock) on(s.id=ca.state) "+
					"WHERE c.product_type like 'REWARDTHEFAN' AND c.is_bounce=0 AND c.is_unsub=0 and c.is_test_account=0 ";
				
			if(venueId!=null && !venueId.isEmpty()){
				if(venueId.endsWith(",")){
					venueId = venueId.substring(0,venueId.length()-1);
				}
				CUSTOMER_LIST_SQL += " AND e.building in("+venueId+") ";
			}
			if(artists!=null && !artists.isEmpty()){
				if(artists.endsWith(",")){
					artists = artists.substring(0,artists.length()-1);
				}
				CUSTOMER_LIST_SQL += " AND ae.artist_name in("+artists+") ";
			}
			if(customerCat!=null && !customerCat.isEmpty()){
				if(customerCat.equalsIgnoreCase("RETAIL")){
					CUSTOMER_LIST_SQL += " AND c.is_retail_cust=1 ";
				}
			}
			if(filter.getRetailCustomer()!=null && !filter.getRetailCustomer().isEmpty()){
				if(filter.getRetailCustomer().equalsIgnoreCase("YES")){
					CUSTOMER_LIST_SQL += " AND c.is_retail_cust=1";
				}else{
					CUSTOMER_LIST_SQL += " AND c.is_retail_cust=0";
				}
			}
			if(isMailSent!=null && !isMailSent.isEmpty()){
				if(isMailSent.equalsIgnoreCase("YES")){
					CUSTOMER_LIST_SQL += " AND c.is_mail_sent=1 ";
				}else if(isMailSent.equalsIgnoreCase("NO")){
					CUSTOMER_LIST_SQL += " AND c.is_mail_sent=0 ";
				}
			}
			if(filter.getEmailSent()!=null && !filter.getEmailSent().isEmpty()){
				if(filter.getEmailSent().equalsIgnoreCase("YES")){
					CUSTOMER_LIST_SQL += " AND c.is_mail_sent=1";
				}else{
					CUSTOMER_LIST_SQL += " AND c.is_mail_sent=0";
				}
			}
			if(customerType!=null && !customerType.isEmpty()){
				if(customerType.equalsIgnoreCase("CLIENT")){
					CUSTOMER_LIST_SQL += " AND c.is_client=1 ";
				}else if(customerType.equalsIgnoreCase("BROKER")){
					CUSTOMER_LIST_SQL += " AND c.is_broker=1 ";
				}
			}
			if(city != null && !city.isEmpty()){
				CUSTOMER_LIST_SQL += " AND ca.city in("+city+") ";
			}
			if(grandChildIds!=null && !grandChildIds.isEmpty()){
				if(grandChildIds.endsWith(",")){
					grandChildIds = grandChildIds.substring(0,grandChildIds.length()-1);
				}
				CUSTOMER_LIST_SQL += " AND gcc.tn_grand_child_category_id in("+grandChildIds+") ";
			}
			if(childIds!=null && !childIds.isEmpty()){
				if(childIds.endsWith(",")){
					childIds = childIds.substring(0,childIds.length()-1);
				}
				CUSTOMER_LIST_SQL += " AND cc.tn_child_category_id in("+childIds+") ";
			}
			if(state != null && !state.isEmpty()){
				if(state.endsWith(",")){
					state = state.substring(0,state.length()-1);
				}
				CUSTOMER_LIST_SQL += " AND s.id in("+state+") ";
			}
			if(country != null && !country.isEmpty()){
				if(country.endsWith(",")){
					country = country.substring(0,country.length()-1);
				}
				CUSTOMER_LIST_SQL += " AND ct.id in("+country+") ";
			}
			if(fromDate != null && !fromDate.isEmpty()){
				CUSTOMER_LIST_SQL += " AND c.last_updated_timestamp >= '"+fromDate+"' ";
			}
			if(toDate != null && !toDate.isEmpty()){
				CUSTOMER_LIST_SQL += " AND c.last_updated_timestamp <= '"+toDate+"' ";
			}
			if(filter.getCustomerType() != null && !filter.getCustomerType().isEmpty()){
				CUSTOMER_LIST_SQL += " AND c.customer_type like '%"+filter.getCustomerType()+"%'";
			}
			if(filter.getCustomerName() != null){
				CUSTOMER_LIST_SQL += " AND c.cust_name like '%"+filter.getCustomerName()+"%'";
			}
			if(filter.getLastName() != null){
				CUSTOMER_LIST_SQL += " AND c.last_name like '%"+filter.getLastName()+"%'";
			}
			if(filter.getProductType() != null){
				CUSTOMER_LIST_SQL +=" AND c.product_type like '%"+filter.getProductType()+"%'";
			}
			if(filter.getSignupType() != null){
				CUSTOMER_LIST_SQL +=" AND c.signup_type like '%"+filter.getSignupType()+"%'";
			}
			if(filter.getIsClient() != null){
				CUSTOMER_LIST_SQL +=" AND c.is_client= "+filter.getIsClient()+"";
			}
			if(filter.getIsBroker() != null){
				CUSTOMER_LIST_SQL +=" AND c.is_broker= "+filter.getIsBroker()+"";
			}
			if(filter.getCustomerStatus() != null){
				CUSTOMER_LIST_SQL +=" AND c.customer_level like '%"+filter.getCustomerStatus()+"%'";
			}
			if(filter.getCompanyName() != null){
				CUSTOMER_LIST_SQL +=" AND c.company_name like '%"+filter.getCompanyName()+"%'";
			}
			if(filter.getEmail() != null){
				CUSTOMER_LIST_SQL += " AND c.email like '%"+filter.getEmail()+"%'";	
			}
			if(filter.getAddressLine1() != null){
				CUSTOMER_LIST_SQL += " AND ca.address_line1 like '%"+filter.getAddressLine1()+"%'";
			}
			if(filter.getAddressLine2() != null){
				CUSTOMER_LIST_SQL += " AND ca.address_line2 like '%"+filter.getAddressLine2()+"%'";
			}
			if(filter.getCity() != null){
				CUSTOMER_LIST_SQL += " AND ca.city like '"+filter.getCity()+"'";
			}
			if(filter.getState() != null){
				CUSTOMER_LIST_SQL += " AND s.name like '"+filter.getState()+"'";
			}
			if(filter.getCountry() != null){
				CUSTOMER_LIST_SQL += " AND ct.country_name like '"+filter.getCountry()+"'";
			}
			if(filter.getZipCode() != null){
				CUSTOMER_LIST_SQL += " AND ca.zip_code like '"+filter.getZipCode()+"'";
			}
			if(filter.getPhone() != null){
				CUSTOMER_LIST_SQL += " AND c.phone like '"+filter.getPhone()+"'";
			}
			if(filter.getReferrerCode() != null){
				CUSTOMER_LIST_SQL += " AND c.referrer_code like '%"+filter.getReferrerCode()+"%'";
			}
				
			staticSession  = getStaticSession();
			Query query = staticSession.createSQLQuery(CUSTOMER_LIST_SQL);
			Integer count = Integer.valueOf((query.uniqueResult()).toString());
			return count;
		}catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	
	public Integer getClientReportDataCountRTWRTW2(String isMailSent,String customerCat,String customerType,String city,String state,String country,String venueId,
			String productType,String grandChildIds,String childIds,String artists,GridHeaderFilters filter,String fromDate,String toDate){
		Session session =null;
		Integer count=0;
		try{
			String CUSTOMER_LIST_SQL = "select count(distinct email) from email_blast with(nolock) " +
					" WHERE is_bounce=0 AND is_unsub=0 ";
				
				if(productType!=null && !productType.isEmpty()){
					CUSTOMER_LIST_SQL +=" AND product_type IN("+productType+") ";
				}
				if(venueId!=null && !venueId.isEmpty()){
					if(venueId.endsWith(",")){
						venueId = venueId.substring(0,venueId.length()-1);
					}
					CUSTOMER_LIST_SQL += " AND venue_name in("+venueId+") ";
				}
				if(customerType!=null && !customerType.isEmpty()){
					if(customerType.equalsIgnoreCase("CLIENT")){
						CUSTOMER_LIST_SQL += " AND is_client=1 ";
					}else if(customerType.equalsIgnoreCase("BROKER")){
						CUSTOMER_LIST_SQL += " AND is_broker=1 ";
					}
				}
				if(customerCat!=null && !customerCat.isEmpty()){
					if(customerCat.equalsIgnoreCase("RETAIL")){
						CUSTOMER_LIST_SQL += " AND is_retail_cust=1 ";
					}
				}
				if(filter.getRetailCustomer()!=null && !filter.getRetailCustomer().isEmpty()){
					if(filter.getRetailCustomer().equalsIgnoreCase("YES")){
						CUSTOMER_LIST_SQL += " AND is_retail_cust=1";
					}else{
						CUSTOMER_LIST_SQL += " AND is_retail_cust=0";
					}
				}
				if(isMailSent!=null && !isMailSent.isEmpty()){
					if(isMailSent.equalsIgnoreCase("YES")){
						CUSTOMER_LIST_SQL += " AND is_mail_sent=1 ";
					}else if(isMailSent.equalsIgnoreCase("NO")){
						CUSTOMER_LIST_SQL += " AND is_mail_sent=0 ";
					}
				}
				if(filter.getEmailSent()!=null && !filter.getEmailSent().isEmpty()){
					if(filter.getEmailSent().equalsIgnoreCase("YES")){
						CUSTOMER_LIST_SQL += " AND is_mail_sent=1";
					}else{
						CUSTOMER_LIST_SQL += " AND is_mail_sent=0";
					}
				}
				if(artists!=null && !artists.isEmpty()){
					if(artists.endsWith(",")){
						artists = artists.substring(0,artists.length()-1);
					}
					CUSTOMER_LIST_SQL += " AND artist_name in("+artists+") ";
				}
				if(city != null && !city.isEmpty()){
					CUSTOMER_LIST_SQL += " AND city in("+city+") ";
				}
				if(grandChildIds!=null && !grandChildIds.isEmpty()){
					if(grandChildIds.endsWith(",")){
						grandChildIds = grandChildIds.substring(0,grandChildIds.length()-1);
					}
					CUSTOMER_LIST_SQL += " AND grandchild_category_id in("+grandChildIds+") ";
				}
				if(childIds!=null && !childIds.isEmpty()){
					if(childIds.endsWith(",")){
						childIds = childIds.substring(0,childIds.length()-1);
					}
					CUSTOMER_LIST_SQL += " AND child_category_id in("+childIds+") ";
				}
				if(state != null && !state.isEmpty()){
					if(state.endsWith(",")){
						state = state.substring(0,state.length()-1);
					}
					CUSTOMER_LIST_SQL += " AND state_id in("+state+") ";
				}
				if(country != null && !country.isEmpty()){
					if(country.endsWith(",")){
						country = country.substring(0,country.length()-1);
					}
					CUSTOMER_LIST_SQL += " AND country_id in("+country+") ";
				}
				if(fromDate != null && !fromDate.isEmpty()){
					CUSTOMER_LIST_SQL += " AND last_updated_timestamp >= '"+fromDate+"' ";
				}
				if(toDate != null && !toDate.isEmpty()){
					CUSTOMER_LIST_SQL += " AND last_updated_timestamp <= '"+toDate+"' ";
				}
				if(filter.getCustomerName() != null){
					CUSTOMER_LIST_SQL += " AND first_name like '%"+filter.getCustomerName()+"%'";
				}
				if(filter.getLastName() != null){
					CUSTOMER_LIST_SQL += " AND last_name like '%"+filter.getLastName()+"%'";
				}
				if(filter.getProductType() != null){
					CUSTOMER_LIST_SQL +=" AND product_type like '%"+filter.getProductType()+"%'";
				}
				if(filter.getIsClient() != null){
					CUSTOMER_LIST_SQL +=" AND is_client= "+filter.getIsClient()+"";
				}
				if(filter.getIsBroker() != null){
					CUSTOMER_LIST_SQL +=" AND is_broker= "+filter.getIsBroker()+"";
				}
				
				if(filter.getCompanyName() != null){
					CUSTOMER_LIST_SQL +=" AND company_name like '%"+filter.getCompanyName()+"%'";
				}
				if(filter.getEmail() != null){
					CUSTOMER_LIST_SQL += " AND email like '%"+filter.getEmail()+"%'";	
				}
				if(filter.getAddressLine1() != null){
					CUSTOMER_LIST_SQL += " AND street_address1 like '%"+filter.getAddressLine1()+"%'";
				}
				if(filter.getAddressLine2() != null){
					CUSTOMER_LIST_SQL += " AND street_address2 like '%"+filter.getAddressLine2()+"%'";
				}
				if(filter.getCity() != null){
					CUSTOMER_LIST_SQL += " AND city like '"+filter.getCity()+"'";
				}
				if(filter.getState() != null){
					CUSTOMER_LIST_SQL += " AND state like '"+filter.getState()+"'";
				}
				if(filter.getCountry() != null){
					CUSTOMER_LIST_SQL += " AND country like '"+filter.getCountry()+"'";
				}
				if(filter.getZipCode() != null){
					CUSTOMER_LIST_SQL += " AND postal_code like '"+filter.getZipCode()+"'";
				}
				if(filter.getPhone() != null){
					CUSTOMER_LIST_SQL += " AND phone_number like '"+filter.getPhone()+"'";
				}
				
				session  = getStaticSession();
				Query query = session.createSQLQuery(CUSTOMER_LIST_SQL);
				count = Integer.valueOf((query.uniqueResult()).toString());
				return count;
		}catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	
	/**
	 * List of purchase orders for REWARDTHEFAN
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<PurchaseOrders> purchaseOrdersList(String fromDate,String toDate,GridHeaderFilters filter,String poNo,String pageNo, Integer brokerId){
		Session session =null;
		Integer startFrom = 0;
		try {
			String SQL_PO_LIST = "select po.id as id,po.customer_id as customerId,'REWARDTHEFAN' as productType,c.cust_name as customerName,c.customer_type as customerType," +
					" po.created_time as created, po.last_updated as lastUpdated,datediff(dd,po.created_time,getdate()) as 'poAged',po.csr AS csr,po.purchase_order_type AS purchaseOrderType," +
					" po.created_by as createdBy,po.is_emailed as isEmailed,po.consignment_po_no as consignmentPoNo,po.transaction_office as transactionOffice," +
					" po.tracking_no as trackingNo,po.shipping_notes as shippingNotes,po.external_notes as externalNotes,po.internal_notes as internalNotes," +
					" po.po_total as poTotal,po.ticket_count as ticketQty,sm.name as shippingType,po.status as status,po.pos_product_type AS posProductType," +
					" t.event_id as eventId,t.event_name as eventName,t.event_date as eventDate,t.event_time as eventTime,po.pos_purchase_order_id AS posPOId,"+
					" t.quantity as eventTicketQty,"+
					" po.fedex_label_created as fedexLabelCreated, po.broker_id as brokerId "+
					" from purchase_order po with(nolock) " +
					" inner join customer c with(nolock) on(c.id=po.customer_id)" +
					" left join shipping_method sm with(nolock) on(po.shipping_method_id=sm.id)"+
					" left join ticket_group t with(nolock) on po.id=t.purchase_order_id"+
					//" left join event_details e on t.event_id=e.event_id"+
					" where po.status='ACTIVE'";
			
			if(fromDate!=null && !fromDate.isEmpty()){
				SQL_PO_LIST = SQL_PO_LIST+" AND po.created_time >='"+fromDate+"' ";
			}
			if(toDate!=null && !toDate.isEmpty()){
				SQL_PO_LIST = SQL_PO_LIST+" AND po.created_time <='"+toDate+"' ";
			}
			if(filter.getPurchaseOrderId() != null){
				SQL_PO_LIST += " AND po.id = "+filter.getPurchaseOrderId()+" ";				
			}
			if(filter.getCustomerName() != null){
				SQL_PO_LIST += " AND c.cust_name like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getCustomerType() != null){
				SQL_PO_LIST += " AND c.customer_type like '%"+filter.getCustomerType()+"%' ";	
			}
			if(filter.getPoTotal() != null){
				SQL_PO_LIST += " AND po.po_total = "+filter.getPoTotal()+" ";
			}
			if(filter.getPurchaseOrderType() != null){
				SQL_PO_LIST += " AND po.purchase_order_type like '%"+filter.getPurchaseOrderType()+"%' ";
			}
			if(filter.getTicketQty() != null){
				SQL_PO_LIST += " AND po.ticket_count = "+filter.getTicketQty()+" ";				
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					SQL_PO_LIST += " AND DATEPART(day,po.created_time) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					SQL_PO_LIST += " AND DATEPART(month,po.created_time) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					SQL_PO_LIST += " AND DATEPART(year,po.created_time) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getCreatedBy() != null){
				SQL_PO_LIST += " AND po.created_by like '%"+filter.getCreatedBy()+"%' ";
			}
			if(filter.getShippingType() != null){
				SQL_PO_LIST += " AND sm.name like '%"+filter.getShippingType()+"%' ";
			}
			if(filter.getStatus() != null){
				SQL_PO_LIST += " AND po.status like '%"+filter.getStatus()+"%' ";
			}
			if(filter.getProductType() != null){
				SQL_PO_LIST += " AND 'REWARDTHEFAN' like '%"+filter.getProductType()+"%' ";
			}
			if(filter.getCsr() != null){
				SQL_PO_LIST += " AND po.csr like '%"+filter.getCsr()+"%' ";
			}
			if(filter.getPoAged() != null){
				SQL_PO_LIST += " AND datediff(dd,po.created_time,getdate()) = "+filter.getPoAged()+" ";
			}
			if(filter.getLastUpdatedDateStr() != null){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					SQL_PO_LIST += " AND DATEPART(day,po.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
					SQL_PO_LIST += " AND DATEPART(month,po.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
					SQL_PO_LIST += " AND DATEPART(year,po.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getIsEmailed() != null){
				SQL_PO_LIST += " AND po.is_emailed = "+filter.getIsEmailed()+" ";
			}
			if(filter.getConsignmentPoNo() != null){
				SQL_PO_LIST += " AND po.consignment_po_no = '"+filter.getConsignmentPoNo()+"' ";
			}
			if(filter.getTransactionOffice() != null){
				SQL_PO_LIST += " AND po.transaction_office = '"+filter.getTransactionOffice()+"' ";
			}
			if(filter.getTrackingNo() != null){
				SQL_PO_LIST += " AND po.tracking_no = '"+filter.getTrackingNo()+"' ";
			}
			if(filter.getShippingNotes() != null){
				SQL_PO_LIST += " AND po.shipping_notes like '%"+filter.getShippingNotes()+"%' ";
			}
			if(filter.getExternalNotes() != null){
				SQL_PO_LIST += " AND po.external_notes like '%"+filter.getExternalNotes()+"%' ";
			}
			if(filter.getInternalNotes() != null){
				SQL_PO_LIST += " AND po.internal_notes like '%"+filter.getInternalNotes()+"%' ";
			}
			if(filter.getEventId() != null){
				SQL_PO_LIST += " AND t.event_id = "+filter.getEventId()+" ";
			}
			if(filter.getEventName() != null){
				SQL_PO_LIST += " AND t.event_name like '%"+filter.getEventName()+"%' ";
			}
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					SQL_PO_LIST += " AND DATEPART(day,t.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					SQL_PO_LIST += " AND DATEPART(month,t.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					SQL_PO_LIST += " AND DATEPART(year,t.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventTimeStr() != null){
				if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
					SQL_PO_LIST += " AND DATEPART(hour,t.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
					SQL_PO_LIST += " AND DATEPART(minute,t.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
				}
			}
			if(filter.getEventTixQty() != null){
				SQL_PO_LIST += " AND t.quantity = "+filter.getEventTixQty()+" ";
			}
			if(filter.getPosPOId() != null){
				if(filter.getPosPOId().equalsIgnoreCase("Yes")){
					SQL_PO_LIST += " AND po.pos_purchase_order_id is not null";
				}else if(filter.getPosPOId().equalsIgnoreCase("No")){
					SQL_PO_LIST += " AND po.pos_purchase_order_id is null";
				}
			}
			if(filter.getBrokerId() != null){
				SQL_PO_LIST += " AND po.broker_id = "+filter.getBrokerId()+" ";
			}
			if(poNo!=null && !poNo.isEmpty()){
				SQL_PO_LIST += " AND po.id="+poNo;
			}
			if(brokerId != null && brokerId > 0){
				SQL_PO_LIST += " AND po.broker_id = "+brokerId+" ";
			}
			/*
			if(csr!=null && !csr.isEmpty()){
				SQL_PO_LIST += " AND po.csr like '%"+csr+"%' ";
			}
			if(customer!=null && !customer.isEmpty()){
				SQL_PO_LIST += " AND c.cust_name like '%"+customer+"%'";
			}
			*/
			
			String sortingSql = GridSortingUtil.getPurchaseOrderSortingSql(filter);
			if(sortingSql!=null && !sortingSql.isEmpty()){
				SQL_PO_LIST += sortingSql;
			}else{
				SQL_PO_LIST += " ORDER BY po.id desc";
			}
			session  = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(SQL_PO_LIST);
			sqlQuery.addScalar("id", Hibernate.INTEGER);
			sqlQuery.addScalar("customerId", Hibernate.INTEGER);
			sqlQuery.addScalar("customerName", Hibernate.STRING);
			sqlQuery.addScalar("created", Hibernate.DATE);
			sqlQuery.addScalar("poTotal", Hibernate.DOUBLE);
			sqlQuery.addScalar("ticketQty", Hibernate.INTEGER);
			sqlQuery.addScalar("shippingType", Hibernate.STRING);
			sqlQuery.addScalar("status", Hibernate.STRING);
			sqlQuery.addScalar("lastUpdated", Hibernate.DATE);
			sqlQuery.addScalar("poAged", Hibernate.INTEGER);
			sqlQuery.addScalar("createdBy", Hibernate.STRING);
			sqlQuery.addScalar("isEmailed", Hibernate.BOOLEAN);
			sqlQuery.addScalar("consignmentPoNo", Hibernate.STRING);
			sqlQuery.addScalar("transactionOffice", Hibernate.STRING);
			sqlQuery.addScalar("trackingNo", Hibernate.STRING);
			sqlQuery.addScalar("shippingNotes", Hibernate.STRING);
			sqlQuery.addScalar("externalNotes", Hibernate.STRING);
			sqlQuery.addScalar("internalNotes", Hibernate.STRING);
			sqlQuery.addScalar("csr", Hibernate.STRING);
			sqlQuery.addScalar("eventTicketQty", Hibernate.INTEGER);
			sqlQuery.addScalar("eventId", Hibernate.INTEGER);
			sqlQuery.addScalar("eventName", Hibernate.STRING);
			sqlQuery.addScalar("eventDate", Hibernate.DATE);
			sqlQuery.addScalar("eventTime", Hibernate.TIME);
			sqlQuery.addScalar("productType", Hibernate.STRING);
			sqlQuery.addScalar("purchaseOrderType", Hibernate.STRING);
			sqlQuery.addScalar("posProductType", Hibernate.STRING);
			sqlQuery.addScalar("posPOId", Hibernate.INTEGER);
			sqlQuery.addScalar("fedexLabelCreated", Hibernate.BOOLEAN);
			sqlQuery.addScalar("brokerId", Hibernate.INTEGER);
			
			startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
			Collection<PurchaseOrders> poLIst = sqlQuery.setResultTransformer(Transformers.aliasToBean(PurchaseOrders.class)).setFirstResult(startFrom)
			.setMaxResults(PaginationUtil.PAGESIZE).list();
			return poLIst;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	
	/**
	 * Number of records in List of purchase orders for REWARDTHEFAN
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Integer purchaseOrdersListCount(String fromDate,String toDate,GridHeaderFilters filter,String poNo,Integer brokerId){
		int count = 0;
		Session session =null;
		try {
			String SQL_PO_LIST = "select count(*) as cnt"+
					" from purchase_order po with(nolock) " +
					" inner join customer c with(nolock) on(c.id=po.customer_id)" +
					" left join shipping_method sm with(nolock) on(po.shipping_method_id=sm.id)"+
					" left join ticket_group t with(nolock) on po.id=t.purchase_order_id"+
					" left join event_details e with(nolock) on t.event_id=e.event_id"+
					" where po.status='ACTIVE'";
			
			if(fromDate!=null && !fromDate.isEmpty()){
				SQL_PO_LIST = SQL_PO_LIST+" AND po.created_time >='"+fromDate+"' ";
			}
			if(toDate!=null && !toDate.isEmpty()){
				SQL_PO_LIST = SQL_PO_LIST+" AND po.created_time <='"+toDate+"' ";
			}
			if(filter.getPurchaseOrderId() != null){
				SQL_PO_LIST += " AND po.id = "+filter.getPurchaseOrderId()+" ";				
			}
			if(filter.getCustomerName() != null){
				SQL_PO_LIST += " AND c.cust_name like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getCustomerType() != null){
				SQL_PO_LIST += " AND c.customer_type like '%"+filter.getCustomerType()+"%' ";	
			}
			if(filter.getPoTotal() != null){
				SQL_PO_LIST += " AND po.po_total = "+filter.getPoTotal()+" ";
			}
			if(filter.getTicketQty() != null){
				SQL_PO_LIST += " AND po.ticket_count = "+filter.getTicketQty()+" ";				
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					SQL_PO_LIST += " AND DATEPART(day,po.created_time) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					SQL_PO_LIST += " AND DATEPART(month,po.created_time) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					SQL_PO_LIST += " AND DATEPART(year,po.created_time) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getCreatedBy() != null){
				SQL_PO_LIST += " AND po.created_by like '%"+filter.getCreatedBy()+"%' ";
			}
			if(filter.getShippingType() != null){
				SQL_PO_LIST += " AND sm.name like '%"+filter.getShippingType()+"%' ";
			}
			if(filter.getStatus() != null){
				SQL_PO_LIST += " AND po.status like '%"+filter.getStatus()+"%' ";
			}
			if(filter.getProductType() != null){
				SQL_PO_LIST += " AND 'REWARDTHEFAN' like '%"+filter.getProductType()+"%' ";
			}
			if(filter.getCsr() != null){
				SQL_PO_LIST += " AND po.csr like '%"+filter.getCsr()+"%' ";
			}
			if(filter.getPoAged() != null){
				SQL_PO_LIST += " AND datediff(dd,po.created_time,getdate()) = "+filter.getPoAged()+" ";
			}
			if(filter.getLastUpdatedDateStr() != null){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					SQL_PO_LIST += " AND DATEPART(day,po.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
					SQL_PO_LIST += " AND DATEPART(month,po.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
					SQL_PO_LIST += " AND DATEPART(year,po.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getIsEmailed() != null){
				SQL_PO_LIST += " AND po.is_emailed = "+filter.getIsEmailed()+" ";
			}
			if(filter.getConsignmentPoNo() != null){
				SQL_PO_LIST += " AND po.consignment_po_no = '"+filter.getConsignmentPoNo()+"' ";
			}
			if(filter.getTransactionOffice() != null){
				SQL_PO_LIST += " AND po.transaction_office = '"+filter.getTransactionOffice()+"' ";
			}
			if(filter.getTrackingNo() != null){
				SQL_PO_LIST += " AND po.tracking_no = '"+filter.getTrackingNo()+"' ";
			}
			if(filter.getShippingNotes() != null){
				SQL_PO_LIST += " AND po.shipping_notes like '%"+filter.getShippingNotes()+"%' ";
			}
			if(filter.getExternalNotes() != null){
				SQL_PO_LIST += " AND po.external_notes like '%"+filter.getExternalNotes()+"%' ";
			}
			if(filter.getInternalNotes() != null){
				SQL_PO_LIST += " AND po.internal_notes like '%"+filter.getInternalNotes()+"%' ";
			}
			if(filter.getEventId() != null){
				SQL_PO_LIST += " AND t.event_id = "+filter.getEventId()+" ";
			}
			if(filter.getEventName() != null){
				SQL_PO_LIST += " AND t.event_name like '%"+filter.getEventName()+"%' ";
			}
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					SQL_PO_LIST += " AND DATEPART(day,t.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					SQL_PO_LIST += " AND DATEPART(month,t.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					SQL_PO_LIST += " AND DATEPART(year,t.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventTimeStr() != null){
				if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
					SQL_PO_LIST += " AND DATEPART(hour,t.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
					SQL_PO_LIST += " AND DATEPART(minute,t.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
				}
			}
			if(filter.getPosPOId() != null){
				if(filter.getPosPOId().equalsIgnoreCase("Yes")){
					SQL_PO_LIST += " AND po.pos_purchase_order_id is not null";
				}else if(filter.getPosPOId().equalsIgnoreCase("No")){
					SQL_PO_LIST += " AND po.pos_purchase_order_id is null";
				}
			}
			if(filter.getBrokerId() != null){
				SQL_PO_LIST += " AND po.broker_id = "+filter.getBrokerId()+" ";
			}
			if(poNo!=null && !poNo.isEmpty()){
				SQL_PO_LIST += " AND po.id="+poNo;
			}
			if(brokerId != null && brokerId > 0){
				SQL_PO_LIST += " AND po.broker_id = "+brokerId+" ";
			}
			/*
			if(csr!=null && !csr.isEmpty()){
				SQL_PO_LIST += " AND po.csr like '%"+csr+"%' ";
			}
			if(customer!=null && !customer.isEmpty()){
				SQL_PO_LIST += " AND c.cust_name like '%"+customer+"%'";
			}
			*/
			session  = sessionFactory.openSession();
			
			Query query = session.createSQLQuery(SQL_PO_LIST);
			count = Integer.valueOf((query.uniqueResult()).toString());
			return count;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	
	/**
	 * List of purchase orders for REWARDTHEFAN
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<PurchaseOrders> purchaseOrdersListToExport(String poNo,String csr,String customer,String fromDate,String toDate,GridHeaderFilters filter,Integer brokerId){
		Session session =null;
		try {
			String SQL_PO_LIST = "select distinct po.id as id,po.customer_id as customerId,'REWARDTHEFAN' as productType,c.cust_name as customerName,c.customer_type as customerType," +
					" po.created_time as created, po.last_updated as lastUpdated,datediff(dd,po.created_time,getdate()) as 'poAged',po.csr AS csr," +
					" po.created_by as createdBy,po.is_emailed as isEmailed,po.consignment_po_no as consignmentPoNo,po.transaction_office as transactionOffice," +
					" po.tracking_no as trackingNo,po.shipping_notes as shippingNotes,po.external_notes as externalNotes,po.internal_notes as internalNotes," +
					" po.po_total as poTotal,po.ticket_count as ticketQty,sm.name as shippingType,po.status as status," +
					" t.event_id as eventId,t.event_name as eventName,t.event_date as eventDate,t.event_time as eventTime,"+
					" po.purchase_order_type AS purchaseOrderType,po.pos_product_type AS posProductType,po.pos_purchase_order_id AS posPOId,"+
					" t.quantity as eventTicketQty,po.broker_id as brokerId "+
					" from purchase_order po with(nolock) " +
					" inner join customer c with(nolock) on(c.id=po.customer_id)" +
					" left join shipping_method sm with(nolock) on(po.shipping_method_id=sm.id)"+
					" left join ticket_group t with(nolock) on po.id=t.purchase_order_id"+
					//" left join event_details e on t.event_id=e.event_id"+
					" where po.status='ACTIVE'";
			if(poNo!=null && !poNo.isEmpty()){
				SQL_PO_LIST += " AND po.id="+poNo;
			}
			if(fromDate!=null && !fromDate.isEmpty()){
				SQL_PO_LIST = SQL_PO_LIST+" AND po.created_time >='"+fromDate+"' ";
			}
			if(toDate!=null && !toDate.isEmpty()){
				SQL_PO_LIST = SQL_PO_LIST+" AND po.created_time <='"+toDate+"' ";
			}
			if(csr!=null && !csr.isEmpty()){
				SQL_PO_LIST += " AND po.csr like '%"+csr+"%' ";
			}
			if(customer!=null && !customer.isEmpty()){
				SQL_PO_LIST += " AND c.cust_name like '%"+customer+"%'";
			}
			if(brokerId != null && brokerId > 0){
				SQL_PO_LIST += " AND po.broker_id = "+brokerId+" ";
			}
			if(filter.getPurchaseOrderId() != null){
				SQL_PO_LIST += " AND po.id = "+filter.getPurchaseOrderId()+" ";				
			}
			if(filter.getCustomerName() != null){
				SQL_PO_LIST += " AND c.cust_name like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getCustomerType() != null){
				SQL_PO_LIST += " AND c.customer_type like '%"+filter.getCustomerType()+"%' ";	
			}
			if(filter.getPoTotal() != null){
				SQL_PO_LIST += " AND po.po_total = "+filter.getPoTotal()+" ";
			}
			if(filter.getPurchaseOrderType() != null){
				SQL_PO_LIST += " AND po.purchase_order_type like '%"+filter.getPurchaseOrderType()+"%' ";
			}
			if(filter.getTicketQty() != null){
				SQL_PO_LIST += " AND po.ticket_count = "+filter.getTicketQty()+" ";				
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					SQL_PO_LIST += " AND DATEPART(day,po.created_time) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					SQL_PO_LIST += " AND DATEPART(month,po.created_time) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					SQL_PO_LIST += " AND DATEPART(year,po.created_time) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getCreatedBy() != null){
				SQL_PO_LIST += " AND po.created_by like '%"+filter.getCreatedBy()+"%' ";
			}
			if(filter.getShippingType() != null){
				SQL_PO_LIST += " AND sm.name like '%"+filter.getShippingType()+"%' ";
			}
			if(filter.getStatus() != null){
				SQL_PO_LIST += " AND po.status like '%"+filter.getStatus()+"%' ";
			}
			if(filter.getProductType() != null){
				SQL_PO_LIST += " AND 'REWARDTHEFAN' like '%"+filter.getProductType()+"%' ";
			}
			if(filter.getCsr() != null){
				SQL_PO_LIST += " AND po.csr like '%"+filter.getCsr()+"%' ";
			}
			if(filter.getPoAged() != null){
				SQL_PO_LIST += " AND datediff(dd,po.created_time,getdate()) = "+filter.getPoAged()+" ";
			}
			if(filter.getLastUpdatedDateStr() != null){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					SQL_PO_LIST += " AND DATEPART(day,po.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
					SQL_PO_LIST += " AND DATEPART(month,po.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
					SQL_PO_LIST += " AND DATEPART(year,po.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getIsEmailed() != null){
				SQL_PO_LIST += " AND po.is_emailed = "+filter.getIsEmailed()+" ";
			}
			if(filter.getConsignmentPoNo() != null){
				SQL_PO_LIST += " AND po.consignment_po_no = '"+filter.getConsignmentPoNo()+"' ";
			}
			if(filter.getTransactionOffice() != null){
				SQL_PO_LIST += " AND po.transaction_office = '"+filter.getTransactionOffice()+"' ";
			}
			if(filter.getTrackingNo() != null){
				SQL_PO_LIST += " AND po.tracking_no = '"+filter.getTrackingNo()+"' ";
			}
			if(filter.getShippingNotes() != null){
				SQL_PO_LIST += " AND po.shipping_notes like '%"+filter.getShippingNotes()+"%' ";
			}
			if(filter.getExternalNotes() != null){
				SQL_PO_LIST += " AND po.external_notes like '%"+filter.getExternalNotes()+"%' ";
			}
			if(filter.getInternalNotes() != null){
				SQL_PO_LIST += " AND po.internal_notes like '%"+filter.getInternalNotes()+"%' ";
			}
			if(filter.getEventId() != null){
				SQL_PO_LIST += " AND t.event_id = "+filter.getEventId()+" ";
			}
			if(filter.getEventName() != null){
				SQL_PO_LIST += " AND t.event_name like '%"+filter.getEventName()+"%' ";
			}
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					SQL_PO_LIST += " AND DATEPART(day,t.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					SQL_PO_LIST += " AND DATEPART(month,t.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					SQL_PO_LIST += " AND DATEPART(year,t.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventTimeStr() != null){
				if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
					SQL_PO_LIST += " AND DATEPART(hour,t.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
					SQL_PO_LIST += " AND DATEPART(minute,t.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
				}
			}
			if(filter.getEventTixQty() != null){
				SQL_PO_LIST += " AND t.quantity = "+filter.getEventTixQty()+" ";
			}
			if(filter.getPosPOId() != null){
				if(filter.getPosPOId().equalsIgnoreCase("Yes")){
					SQL_PO_LIST += " AND po.pos_purchase_order_id is not null";
				}else if(filter.getPosPOId().equalsIgnoreCase("No")){
					SQL_PO_LIST += " AND po.pos_purchase_order_id is null";
				}
			}
			if(filter.getBrokerId() != null){
				SQL_PO_LIST += " AND po.broker_id = "+filter.getBrokerId()+" ";
			}
			SQL_PO_LIST += " order by po.id desc";
			session  = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(SQL_PO_LIST);
			sqlQuery.addScalar("id", Hibernate.INTEGER);
			sqlQuery.addScalar("customerId", Hibernate.INTEGER);
			sqlQuery.addScalar("customerName", Hibernate.STRING);
			sqlQuery.addScalar("created", Hibernate.DATE);
			sqlQuery.addScalar("poTotal", Hibernate.DOUBLE);
			sqlQuery.addScalar("ticketQty", Hibernate.INTEGER);
			sqlQuery.addScalar("shippingType", Hibernate.STRING);
			sqlQuery.addScalar("status", Hibernate.STRING);
			sqlQuery.addScalar("lastUpdated", Hibernate.DATE);
			sqlQuery.addScalar("poAged", Hibernate.INTEGER);
			sqlQuery.addScalar("createdBy", Hibernate.STRING);
			sqlQuery.addScalar("isEmailed", Hibernate.BOOLEAN);
			sqlQuery.addScalar("consignmentPoNo", Hibernate.STRING);
			sqlQuery.addScalar("transactionOffice", Hibernate.STRING);
			sqlQuery.addScalar("trackingNo", Hibernate.STRING);
			sqlQuery.addScalar("shippingNotes", Hibernate.STRING);
			sqlQuery.addScalar("externalNotes", Hibernate.STRING);
			sqlQuery.addScalar("internalNotes", Hibernate.STRING);
			sqlQuery.addScalar("csr", Hibernate.STRING);
			sqlQuery.addScalar("eventId", Hibernate.INTEGER);
			sqlQuery.addScalar("eventName", Hibernate.STRING);
			sqlQuery.addScalar("eventDate", Hibernate.DATE);
			sqlQuery.addScalar("eventTime", Hibernate.TIME);
			sqlQuery.addScalar("productType", Hibernate.STRING);
			sqlQuery.addScalar("purchaseOrderType", Hibernate.STRING);
			sqlQuery.addScalar("posProductType", Hibernate.STRING);
			sqlQuery.addScalar("posPOId", Hibernate.INTEGER);
			sqlQuery.addScalar("eventTicketQty", Hibernate.INTEGER);
			sqlQuery.addScalar("brokerId", Hibernate.INTEGER);
			Collection<PurchaseOrders> poLIst = sqlQuery.setResultTransformer(Transformers.aliasToBean(PurchaseOrders.class)).setMaxResults(65535).list();
			return poLIst;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	/**
	 * List of purchase orders for RTW/RTW2
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<PurchaseOrders> getRTWPurchaseOrdersList(String fromDate,String toDate,String productType,GridHeaderFilters filter,String poNo,Integer customerId,String pageNo){
		Session session =null;
		Integer startFrom = 0;
		try {
			String SQL_PO_LIST = "select distinct id, customerId, product_type as productType, customerName, customerType," +
					" created, lastUpdated, poAged, csr," +
					" createdBy, isEmailed, consignmentPoNo,transactionOffice," +
					" trackingNo, shippingNotes, externalNotes, internalNotes," +
					" poTotal, ticketQty, shippingType, status, eventTicketQty," +
					" eventId, eventName, eventDate, eventTime, eventTicketCost"+
					" from pos_purchase_order_view with(nolock) where 1=1";
			
			if(fromDate!=null && !fromDate.isEmpty()){
				SQL_PO_LIST = SQL_PO_LIST+" AND created >='"+fromDate+"' ";
			}
			if(toDate!=null && !toDate.isEmpty()){
				SQL_PO_LIST = SQL_PO_LIST+" AND created <='"+toDate+"' ";
			}
			if(productType != null && !productType.isEmpty()){
				SQL_PO_LIST += " AND LOWER(product_type) = LOWER('"+productType+"') ";
			}
			if(filter.getPurchaseOrderId() != null){
				SQL_PO_LIST += " AND id = "+filter.getPurchaseOrderId()+" ";				
			}
			if(filter.getCustomerName() != null){
				SQL_PO_LIST += " AND customerName like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getCustomerType() != null){
				SQL_PO_LIST += " AND customerType like '%"+filter.getCustomerType()+"%' ";	
			}
			if(filter.getPoTotal() != null){
				SQL_PO_LIST += " AND poTotal = "+filter.getPoTotal()+" ";
			}
			if(filter.getTicketQty() != null){
				SQL_PO_LIST += " AND ticketQty = "+filter.getTicketQty()+" ";				
			}
			if(filter.getEventTixQty() != null){
				SQL_PO_LIST += " AND eventTicketQty = "+filter.getTicketQty()+" ";				
			}
			if(filter.getEventTicketCost() != null){
				SQL_PO_LIST += " AND eventTicketCost = "+filter.getTicketQty()+" ";				
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					SQL_PO_LIST += " AND DATEPART(day,created) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					SQL_PO_LIST += " AND DATEPART(month,created) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					SQL_PO_LIST += " AND DATEPART(year,created) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getCreatedBy() != null){
				SQL_PO_LIST += " AND createdBy like '%"+filter.getCreatedBy()+"%' ";
			}
			if(filter.getShippingType() != null){
				SQL_PO_LIST += " AND shippingType like '%"+filter.getShippingType()+"%' ";
			}
			if(filter.getStatus() != null){
				SQL_PO_LIST += " AND status like '%"+filter.getStatus()+"%' ";
			}
			if(filter.getProductType() != null){
				SQL_PO_LIST += " AND product_type like '%"+filter.getProductType()+"%' ";
			}
			if(filter.getCsr() != null){
				SQL_PO_LIST += " AND csr like '%"+filter.getCsr()+"%' ";
			}
			if(filter.getPoAged() != null){
				SQL_PO_LIST += " AND poAged = "+filter.getPoAged()+" ";
			}
			if(filter.getLastUpdatedDateStr() != null){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					SQL_PO_LIST += " AND DATEPART(day,lastUpdated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
					SQL_PO_LIST += " AND DATEPART(month,lastUpdated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
					SQL_PO_LIST += " AND DATEPART(year,lastUpdated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getIsEmailed() != null){
				SQL_PO_LIST += " AND isEmailed = "+filter.getIsEmailed()+" ";
			}
			if(filter.getConsignmentPoNo() != null){
				SQL_PO_LIST += " AND consignmentPoNo = '"+filter.getConsignmentPoNo()+"' ";
			}
			if(filter.getTransactionOffice() != null){
				SQL_PO_LIST += " AND transactionOffice = '"+filter.getTransactionOffice()+"' ";
			}
			if(filter.getTrackingNo() != null){
				SQL_PO_LIST += " AND trackingNo = '"+filter.getTrackingNo()+"' ";
			}
			if(filter.getShippingNotes() != null){
				SQL_PO_LIST += " AND shippingNotes like '%"+filter.getShippingNotes()+"%' ";
			}
			if(filter.getExternalNotes() != null){
				SQL_PO_LIST += " AND externalNotes like '%"+filter.getExternalNotes()+"%' ";
			}
			if(filter.getInternalNotes() != null){
				SQL_PO_LIST += " AND internalNotes like '%"+filter.getInternalNotes()+"%' ";
			}
			if(filter.getEventId() != null){
				SQL_PO_LIST += " AND eventId = "+filter.getEventId()+" ";
			}
			if(filter.getEventName() != null){
				SQL_PO_LIST += " AND eventName like '%"+filter.getEventName()+"%' ";
			}
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					SQL_PO_LIST += " AND DATEPART(day,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					SQL_PO_LIST += " AND DATEPART(month,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					SQL_PO_LIST += " AND DATEPART(year,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventTimeStr() != null){
				if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
					SQL_PO_LIST += " AND DATEPART(hour,eventTime) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
					SQL_PO_LIST += " AND DATEPART(minute,eventTime) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
				}
			}
			if(poNo!=null && !poNo.isEmpty()){
				SQL_PO_LIST += " AND id="+poNo;
			}
			
			/*			
			if(csr!=null && !csr.isEmpty()){
				SQL_PO_LIST += " AND csr like '%"+csr+"%' ";
				//SQL_PO_LIST += " AND po.csr like '%"+csr+"%' ";
			}
			if(customer!=null && !customer.isEmpty()){
				SQL_PO_LIST += " AND customerName like '%"+customer+"%'";
				//SQL_PO_LIST += " AND c.cust_name like '%"+customer+"%'";
			}
			if(companyProduct!=null && !companyProduct.isEmpty()){
				if(companyProduct.equalsIgnoreCase("RTW")){
					SQL_PO_LIST += " AND product_type='RTW' ";
				}else if (companyProduct.equalsIgnoreCase("RTW2")){
					SQL_PO_LIST += " AND product_type='RTW2' ";
				}
			}
			*/
			if(customerId!=null){
				SQL_PO_LIST += " AND customerId="+customerId;
			}
			
			String sortingSql = GridSortingUtil.getRTWPurchaseOrderSortingSql(filter);
			if(sortingSql!=null && !sortingSql.isEmpty()){
				SQL_PO_LIST += sortingSql;
			}else{
				SQL_PO_LIST += " ORDER BY id desc";
			}
			
			session  = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(SQL_PO_LIST);
			sqlQuery.addScalar("id", Hibernate.INTEGER);
			sqlQuery.addScalar("customerId", Hibernate.INTEGER);
			sqlQuery.addScalar("customerName", Hibernate.STRING);
			sqlQuery.addScalar("created", Hibernate.DATE);
			sqlQuery.addScalar("poTotal", Hibernate.DOUBLE);
			sqlQuery.addScalar("ticketQty", Hibernate.INTEGER);
			sqlQuery.addScalar("eventTicketQty", Hibernate.INTEGER);
			sqlQuery.addScalar("eventTicketCost", Hibernate.DOUBLE);
			sqlQuery.addScalar("shippingType", Hibernate.STRING);
			sqlQuery.addScalar("status", Hibernate.STRING);
			sqlQuery.addScalar("lastUpdated", Hibernate.DATE);
			sqlQuery.addScalar("poAged", Hibernate.INTEGER);
			sqlQuery.addScalar("createdBy", Hibernate.STRING);
			sqlQuery.addScalar("isEmailed", Hibernate.BOOLEAN);
			sqlQuery.addScalar("consignmentPoNo", Hibernate.STRING);
			sqlQuery.addScalar("transactionOffice", Hibernate.STRING);
			sqlQuery.addScalar("trackingNo", Hibernate.STRING);
			sqlQuery.addScalar("shippingNotes", Hibernate.STRING);
			sqlQuery.addScalar("externalNotes", Hibernate.STRING);
			sqlQuery.addScalar("internalNotes", Hibernate.STRING);
			sqlQuery.addScalar("csr", Hibernate.STRING);
			sqlQuery.addScalar("eventId", Hibernate.INTEGER);
			sqlQuery.addScalar("eventName", Hibernate.STRING);
			sqlQuery.addScalar("eventDate", Hibernate.DATE);
			sqlQuery.addScalar("eventTime", Hibernate.TIME);
			sqlQuery.addScalar("productType", Hibernate.STRING);
			startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
			Collection<PurchaseOrders> poLIst = sqlQuery.setResultTransformer(Transformers.aliasToBean(PurchaseOrders.class)).setFirstResult(startFrom)
			.setMaxResults(PaginationUtil.PAGESIZE).list();
			return poLIst;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	
	
	/**
	 * List of purchase orders for RTW/RTW2
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<PurchaseOrders> getRTWPurchaseOrdersListByPoID(String poNo){
		Session session =null;
		try {
			String SQL_PO_LIST = "select distinct id, customerId, product_type as productType, customerName, customerType," +
					" created, lastUpdated, poAged, csr," +
					" createdBy, isEmailed, consignmentPoNo,transactionOffice," +
					" trackingNo, shippingNotes, externalNotes, internalNotes,eventTicketQty," +
					" poTotal, ticketQty, shippingType, status, eventTicketCost," +
					" eventId, eventName, eventDate, eventTime"+
					" from pos_purchase_order_view with(nolock) where 1=1";
			
			if(poNo!=null && !poNo.isEmpty()){
				SQL_PO_LIST += " AND id="+poNo+" ";
			}					
			SQL_PO_LIST += " order by id desc";
			
			session  = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(SQL_PO_LIST);
			sqlQuery.addScalar("id", Hibernate.INTEGER);
			sqlQuery.addScalar("customerId", Hibernate.INTEGER);
			sqlQuery.addScalar("customerName", Hibernate.STRING);
			sqlQuery.addScalar("created", Hibernate.DATE);
			sqlQuery.addScalar("poTotal", Hibernate.DOUBLE);
			sqlQuery.addScalar("ticketQty", Hibernate.INTEGER);
			sqlQuery.addScalar("eventTicketQty", Hibernate.INTEGER);
			sqlQuery.addScalar("eventTicketCost", Hibernate.DOUBLE);
			sqlQuery.addScalar("shippingType", Hibernate.STRING);
			sqlQuery.addScalar("status", Hibernate.STRING);
			sqlQuery.addScalar("lastUpdated", Hibernate.DATE);
			sqlQuery.addScalar("poAged", Hibernate.INTEGER);
			sqlQuery.addScalar("createdBy", Hibernate.STRING);
			sqlQuery.addScalar("isEmailed", Hibernate.BOOLEAN);
			sqlQuery.addScalar("consignmentPoNo", Hibernate.STRING);
			sqlQuery.addScalar("transactionOffice", Hibernate.STRING);
			sqlQuery.addScalar("trackingNo", Hibernate.STRING);
			sqlQuery.addScalar("shippingNotes", Hibernate.STRING);
			sqlQuery.addScalar("externalNotes", Hibernate.STRING);
			sqlQuery.addScalar("internalNotes", Hibernate.STRING);
			sqlQuery.addScalar("csr", Hibernate.STRING);
			sqlQuery.addScalar("eventId", Hibernate.INTEGER);
			sqlQuery.addScalar("eventName", Hibernate.STRING);
			sqlQuery.addScalar("eventDate", Hibernate.DATE);
			sqlQuery.addScalar("eventTime", Hibernate.TIME);
			sqlQuery.addScalar("productType", Hibernate.STRING);
			Collection<PurchaseOrders> poLIst = sqlQuery.setResultTransformer(Transformers.aliasToBean(PurchaseOrders.class)).list();
			return poLIst;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	/**
	 * List of purchase orders for RTW/RTW2
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<PurchaseOrders> getRTWPurchaseOrdersListToExport(String poNo,String csr,String customer,String fromDate,String toDate,String companyProduct,Integer customerId,GridHeaderFilters filter){
		Session session =null;
		try {
			String SQL_PO_LIST = "select distinct id, customerId, product_type as productType, customerName, customerType," +
					" created, lastUpdated, poAged, csr," +
					" createdBy, isEmailed, consignmentPoNo,transactionOffice," +
					" trackingNo, shippingNotes, externalNotes, internalNotes," +
					" poTotal, ticketQty, shippingType, status,eventTicketQty," +
					" eventId, eventName, eventDate, eventTime,eventTicketCost"+
					" from pos_purchase_order_view with(nolock) where 1=1";
			
			if(fromDate!=null && !fromDate.isEmpty()){
				SQL_PO_LIST = SQL_PO_LIST+" AND created >='"+fromDate+"' ";
			}
			if(toDate!=null && !toDate.isEmpty()){
				SQL_PO_LIST = SQL_PO_LIST+" AND created <='"+toDate+"' ";
			}
			if(poNo!=null && !poNo.isEmpty()){
				SQL_PO_LIST += " AND id="+poNo;
			}			
			if(csr!=null && !csr.isEmpty()){
				SQL_PO_LIST += " AND csr like '%"+csr+"%' ";
			}
			if(customer!=null && !customer.isEmpty()){
				SQL_PO_LIST += " AND customerName like '%"+customer+"%'";
			}
			if(companyProduct!=null && !companyProduct.isEmpty()){
				if(companyProduct.equalsIgnoreCase("RTW")){
					SQL_PO_LIST += " AND product_type='RTW' ";
				}else if (companyProduct.equalsIgnoreCase("RTW2")){
					SQL_PO_LIST += " AND product_type='RTW2' ";
				}
			}			
			if(customerId!=null){
				SQL_PO_LIST += " AND customerId="+customerId;
			}
			if(filter.getPurchaseOrderId() != null){
				SQL_PO_LIST += " AND id = "+filter.getPurchaseOrderId()+" ";				
			}
			if(filter.getCustomerName() != null){
				SQL_PO_LIST += " AND customerName like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getCustomerType() != null){
				SQL_PO_LIST += " AND customerType like '%"+filter.getCustomerType()+"%' ";	
			}
			if(filter.getPoTotal() != null){
				SQL_PO_LIST += " AND poTotal = "+filter.getPoTotal()+" ";
			}
			if(filter.getTicketQty() != null){
				SQL_PO_LIST += " AND ticketQty = "+filter.getTicketQty()+" ";				
			}
			if(filter.getEventTixQty() != null){
				SQL_PO_LIST += " AND eventTicketQty = "+filter.getTicketQty()+" ";				
			}
			if(filter.getEventTicketCost() != null){
				SQL_PO_LIST += " AND eventTicketCost = "+filter.getTicketQty()+" ";				
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					SQL_PO_LIST += " AND DATEPART(day,created) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					SQL_PO_LIST += " AND DATEPART(month,created) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					SQL_PO_LIST += " AND DATEPART(year,created) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getCreatedBy() != null){
				SQL_PO_LIST += " AND createdBy like '%"+filter.getCreatedBy()+"%' ";
			}
			if(filter.getShippingType() != null){
				SQL_PO_LIST += " AND shippingType like '%"+filter.getShippingType()+"%' ";
			}
			if(filter.getStatus() != null){
				SQL_PO_LIST += " AND status like '%"+filter.getStatus()+"%' ";
			}
			if(filter.getProductType() != null){
				SQL_PO_LIST += " AND product_type like '%"+filter.getProductType()+"%' ";
			}
			if(filter.getCsr() != null){
				SQL_PO_LIST += " AND csr like '%"+filter.getCsr()+"%' ";
			}
			if(filter.getPoAged() != null){
				SQL_PO_LIST += " AND poAged = "+filter.getPoAged()+" ";
			}
			if(filter.getLastUpdatedDateStr() != null){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					SQL_PO_LIST += " AND DATEPART(day,lastUpdated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
					SQL_PO_LIST += " AND DATEPART(month,lastUpdated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
					SQL_PO_LIST += " AND DATEPART(year,lastUpdated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getIsEmailed() != null){
				SQL_PO_LIST += " AND isEmailed = "+filter.getIsEmailed()+" ";
			}
			if(filter.getConsignmentPoNo() != null){
				SQL_PO_LIST += " AND consignmentPoNo = '"+filter.getConsignmentPoNo()+"' ";
			}
			if(filter.getTransactionOffice() != null){
				SQL_PO_LIST += " AND transactionOffice = '"+filter.getTransactionOffice()+"' ";
			}
			if(filter.getTrackingNo() != null){
				SQL_PO_LIST += " AND trackingNo = '"+filter.getTrackingNo()+"' ";
			}
			if(filter.getShippingNotes() != null){
				SQL_PO_LIST += " AND shippingNotes like '%"+filter.getShippingNotes()+"%' ";
			}
			if(filter.getExternalNotes() != null){
				SQL_PO_LIST += " AND externalNotes like '%"+filter.getExternalNotes()+"%' ";
			}
			if(filter.getInternalNotes() != null){
				SQL_PO_LIST += " AND internalNotes like '%"+filter.getInternalNotes()+"%' ";
			}
			if(filter.getEventId() != null){
				SQL_PO_LIST += " AND eventId = "+filter.getEventId()+" ";
			}
			if(filter.getEventName() != null){
				SQL_PO_LIST += " AND eventName like '%"+filter.getEventName()+"%' ";
			}
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					SQL_PO_LIST += " AND DATEPART(day,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					SQL_PO_LIST += " AND DATEPART(month,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					SQL_PO_LIST += " AND DATEPART(year,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventTimeStr() != null){
				if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
					SQL_PO_LIST += " AND DATEPART(hour,eventTime) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
					SQL_PO_LIST += " AND DATEPART(minute,eventTime) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
				}
			}
			
			SQL_PO_LIST += " order by id desc";
			
			session  = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(SQL_PO_LIST);
			sqlQuery.addScalar("id", Hibernate.INTEGER);
			sqlQuery.addScalar("customerId", Hibernate.INTEGER);
			sqlQuery.addScalar("customerName", Hibernate.STRING);
			sqlQuery.addScalar("created", Hibernate.DATE);
			sqlQuery.addScalar("poTotal", Hibernate.DOUBLE);
			sqlQuery.addScalar("ticketQty", Hibernate.INTEGER);
			sqlQuery.addScalar("eventTicketQty", Hibernate.INTEGER);
			sqlQuery.addScalar("eventTicketCost", Hibernate.DOUBLE);
			sqlQuery.addScalar("shippingType", Hibernate.STRING);
			sqlQuery.addScalar("status", Hibernate.STRING);
			sqlQuery.addScalar("lastUpdated", Hibernate.DATE);
			sqlQuery.addScalar("poAged", Hibernate.INTEGER);
			sqlQuery.addScalar("createdBy", Hibernate.STRING);
			sqlQuery.addScalar("isEmailed", Hibernate.BOOLEAN);
			sqlQuery.addScalar("consignmentPoNo", Hibernate.STRING);
			sqlQuery.addScalar("transactionOffice", Hibernate.STRING);
			sqlQuery.addScalar("trackingNo", Hibernate.STRING);
			sqlQuery.addScalar("shippingNotes", Hibernate.STRING);
			sqlQuery.addScalar("externalNotes", Hibernate.STRING);
			sqlQuery.addScalar("internalNotes", Hibernate.STRING);
			sqlQuery.addScalar("csr", Hibernate.STRING);
			sqlQuery.addScalar("eventId", Hibernate.INTEGER);
			sqlQuery.addScalar("eventName", Hibernate.STRING);
			sqlQuery.addScalar("eventDate", Hibernate.DATE);
			sqlQuery.addScalar("eventTime", Hibernate.TIME);
			sqlQuery.addScalar("productType", Hibernate.STRING);
			Collection<PurchaseOrders> poLIst = sqlQuery.setResultTransformer(Transformers.aliasToBean(PurchaseOrders.class)).setMaxResults(65535).list();
			return poLIst;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	
	/**
	 * Number of records in List of purchase orders for RTW/RTW2
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Integer getRTWPurchaseOrdersListCount(String fromDate,String toDate,String productType,GridHeaderFilters filter,String poNo,Integer customerId){
		int count = 0;
		Session session =null;
		try {
			String SQL_PO_LIST = "select count(*) as cnt"+
					" from pos_purchase_order_view with(nolock) where 1=1";
			
			if(fromDate!=null && !fromDate.isEmpty()){
				SQL_PO_LIST = SQL_PO_LIST+" AND created >='"+fromDate+"' ";
			}
			if(toDate!=null && !toDate.isEmpty()){
				SQL_PO_LIST = SQL_PO_LIST+" AND created <='"+toDate+"' ";
			}
			if(productType != null && !productType.isEmpty()){
				SQL_PO_LIST += " AND LOWER(product_type) = LOWER('"+productType+"') ";
			}
			if(filter.getPurchaseOrderId() != null){
				SQL_PO_LIST += " AND id = "+filter.getPurchaseOrderId()+" ";				
			}
			if(filter.getCustomerName() != null){
				SQL_PO_LIST += " AND customerName like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getCustomerType() != null){
				SQL_PO_LIST += " AND customerType like '%"+filter.getCustomerType()+"%' ";	
			}
			if(filter.getPoTotal() != null){
				SQL_PO_LIST += " AND poTotal = "+filter.getPoTotal()+" ";
			}
			if(filter.getTicketQty() != null){
				SQL_PO_LIST += " AND ticketQty = "+filter.getTicketQty()+" ";				
			}
			if(filter.getEventTixQty() != null){
				SQL_PO_LIST += " AND eventTicketQty = "+filter.getTicketQty()+" ";				
			}
			if(filter.getEventTicketCost() != null){
				SQL_PO_LIST += " AND eventTicketCost = "+filter.getTicketQty()+" ";				
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					SQL_PO_LIST += " AND DATEPART(day,created) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					SQL_PO_LIST += " AND DATEPART(month,created) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					SQL_PO_LIST += " AND DATEPART(year,created) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getCreatedBy() != null){
				SQL_PO_LIST += " AND createdBy like '%"+filter.getCreatedBy()+"%' ";
			}
			if(filter.getShippingType() != null){
				SQL_PO_LIST += " AND shippingType like '%"+filter.getShippingType()+"%' ";
			}
			if(filter.getStatus() != null){
				SQL_PO_LIST += " AND status like '%"+filter.getStatus()+"%' ";
			}
			if(filter.getProductType() != null){
				SQL_PO_LIST += " AND product_type like '%"+filter.getProductType()+"%' ";
			}
			if(filter.getCsr() != null){
				SQL_PO_LIST += " AND csr like '%"+filter.getCsr()+"%' ";
			}
			if(filter.getPoAged() != null){
				SQL_PO_LIST += " AND poAged = "+filter.getPoAged()+" ";
			}
			if(filter.getLastUpdatedDateStr() != null){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					SQL_PO_LIST += " AND DATEPART(day,lastUpdated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
					SQL_PO_LIST += " AND DATEPART(month,lastUpdated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
					SQL_PO_LIST += " AND DATEPART(year,lastUpdated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getIsEmailed() != null){
				SQL_PO_LIST += " AND isEmailed = "+filter.getIsEmailed()+" ";
			}
			if(filter.getConsignmentPoNo() != null){
				SQL_PO_LIST += " AND consignmentPoNo = '"+filter.getConsignmentPoNo()+"' ";
			}
			if(filter.getTransactionOffice() != null){
				SQL_PO_LIST += " AND transactionOffice = '"+filter.getTransactionOffice()+"' ";
			}
			if(filter.getTrackingNo() != null){
				SQL_PO_LIST += " AND trackingNo = '"+filter.getTrackingNo()+"' ";
			}
			if(filter.getShippingNotes() != null){
				SQL_PO_LIST += " AND shippingNotes like '%"+filter.getShippingNotes()+"%' ";
			}
			if(filter.getExternalNotes() != null){
				SQL_PO_LIST += " AND externalNotes like '%"+filter.getExternalNotes()+"%' ";
			}
			if(filter.getInternalNotes() != null){
				SQL_PO_LIST += " AND internalNotes like '%"+filter.getInternalNotes()+"%' ";
			}
			if(filter.getEventId() != null){
				SQL_PO_LIST += " AND eventId = "+filter.getEventId()+" ";
			}
			if(filter.getEventName() != null){
				SQL_PO_LIST += " AND eventName like '%"+filter.getEventName()+"%' ";
			}
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					SQL_PO_LIST += " AND DATEPART(day,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					SQL_PO_LIST += " AND DATEPART(month,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					SQL_PO_LIST += " AND DATEPART(year,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventTimeStr() != null){

				if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
					SQL_PO_LIST += " AND DATEPART(hour,eventTime) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
					SQL_PO_LIST += " AND DATEPART(minute,eventTime) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
				}
			}
			if(poNo!=null && !poNo.isEmpty()){
				SQL_PO_LIST += " AND id="+poNo;
			}
			/*			
			if(csr!=null && !csr.isEmpty()){
				SQL_PO_LIST += " AND csr like '%"+csr+"%' ";
			}
			if(customer!=null && !customer.isEmpty()){
				SQL_PO_LIST += " AND customerName like '%"+customer+"%'";
			}
			if(companyProduct!=null && !companyProduct.isEmpty()){
				if(companyProduct.equalsIgnoreCase("RTW")){
					SQL_PO_LIST += " AND product_type='RTW' ";
				}else if (companyProduct.equalsIgnoreCase("RTW2")){
					SQL_PO_LIST += " AND product_type='RTW2' ";
				}
			}
			*/
			if(customerId!=null){
				SQL_PO_LIST += " AND customerId="+customerId;
			}
			
			session  = sessionFactory.openSession();
			
			Query query = session.createSQLQuery(SQL_PO_LIST);
			count = Integer.valueOf((query.uniqueResult()).toString());
			return count;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public Collection<TicketGroup> getTicketGroupInfo(Integer id){
		Session session =null;
		try {
			String TICKET_GROUP_SQL = "select * from ticket_group with(nolock) where purchase_order_id = :id";
			session  = sessionFactory.openSession();
			Collection<TicketGroup> ticketGroupList = session.createSQLQuery(TICKET_GROUP_SQL)
													.addEntity(TicketGroup.class)
													.setParameter("id", id)
													.list();
			return ticketGroupList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public Collection<TicketGroup> getUnmappedTicketGroups(Integer brokerId, Integer eventId,GridHeaderFilters filter){
		Session session =null;
		try {
			String TICKET_GROUP_SQL = "select distinct tg.id AS id,tg.section AS section,tg.row AS row,count(t.id) as quantity,"+
			"min(convert(int,t.seat_number)) AS seatLow, max(convert(int,t.seat_number)) AS seatHigh, tg.price as price,"+
			"tg.invoice_id as invoiceId,tg.purchase_order_id as purchaseOrderId,tg.status AS status," +
			"tg.shipping_method_id AS shippingMethodId,tg.event_id AS eventId,tg.created_date as createdDate," +
			"tg.last_updated AS lastUpdated,tg.created_by AS createdBy,tg.on_hand As onHand, tg.broker_id AS brokerId, tg.broadcast as broadcast "+ 
			"from ticket_group tg with(nolock) "+
			"join ticket t with(nolock) on tg.id=t.ticket_group_id "+
			"left join shipping_method sm with(nolock) on tg.shipping_method_id=sm.id " +
			"where t.ticket_status='ACTIVE' and tg.event_id= "+eventId;
			
			if(brokerId != null && brokerId > 0){
				TICKET_GROUP_SQL += " AND tg.broker_id = "+brokerId+" ";	
			}
			if(filter.getSection() != null){				
				TICKET_GROUP_SQL += " AND tg.section like '%"+filter.getSection()+"%' ";
			}
			if(filter.getRow() != null){
				TICKET_GROUP_SQL += " AND tg.row like '"+filter.getRow()+"' ";
			}
			if(filter.getSeatLow() != null){
				TICKET_GROUP_SQL += " AND tg.seat_low like '"+filter.getSeatLow()+"' ";
			}
			if(filter.getSeatHigh() != null){
				TICKET_GROUP_SQL += " AND tg.seat_high like '"+filter.getSeatHigh()+"' ";
			}
			if(filter.getQuantity() != null){
				TICKET_GROUP_SQL += " AND tg.quantity = "+filter.getQuantity()+" ";
			}
			if(filter.getRetailPrice() != null){
				TICKET_GROUP_SQL += " AND tg.price = "+filter.getRetailPrice()+" ";
			}
			if(filter.getWholeSalePrice() != null){
				TICKET_GROUP_SQL += " AND tg.price = "+filter.getWholeSalePrice()+" ";
			}
			if(filter.getPrice() != null){
				TICKET_GROUP_SQL += " AND tg.price = "+filter.getPrice()+" ";
			}
			if(filter.getShippingMethod() != null){
				TICKET_GROUP_SQL += " AND tg.shipping_method like '%"+filter.getShippingMethod()+"%' ";
			}
			if(filter.getProductType() != null){
				TICKET_GROUP_SQL += " AND 'REWARDTHEFAN' like '%"+filter.getProductType()+"%' "; 
			}
			if(filter.getBrokerId() != null){
				TICKET_GROUP_SQL += " AND tg.broker_id = "+filter.getBrokerId()+" ";
			}
			if(filter.getBroadcast() != null){
				TICKET_GROUP_SQL += " AND tg.broadcast = "+filter.getBroadcast()+" ";
			}
			
			TICKET_GROUP_SQL += "group by tg.id,tg.section,tg.row,tg.seat_low,tg.seat_high,tg.price,tg.invoice_id,tg.purchase_order_id,tg.status," +
			"tg.shipping_method_id,tg.event_id,tg.created_date,tg.last_updated,tg.created_by,tg.on_hand,tg.broker_id,tg.broadcast having count(t.id) > 0";
			session  = getStaticSession();
			SQLQuery query = session.createSQLQuery(TICKET_GROUP_SQL);
			query.addScalar("id",Hibernate.INTEGER);
			query.addScalar("section",Hibernate.STRING);
			query.addScalar("row",Hibernate.STRING);
			query.addScalar("quantity",Hibernate.INTEGER);
			query.addScalar("eventId",Hibernate.INTEGER);
			query.addScalar("seatLow",Hibernate.STRING);
			query.addScalar("seatHigh",Hibernate.STRING);
			query.addScalar("shippingMethodId",Hibernate.INTEGER);
			query.addScalar("price",Hibernate.DOUBLE);
			query.addScalar("invoiceId",Hibernate.INTEGER);
			query.addScalar("purchaseOrderId",Hibernate.INTEGER);
			query.addScalar("status",Hibernate.STRING);
			query.addScalar("lastUpdated",Hibernate.TIMESTAMP);
			query.addScalar("createdBy",Hibernate.STRING);
			query.addScalar("onHand",Hibernate.STRING);
			query.addScalar("brokerId", Hibernate.INTEGER);
			query.addScalar("broadcast", Hibernate.BOOLEAN);
			
			Collection<TicketGroup> ticketGroupList = query.setResultTransformer(Transformers.aliasToBean(TicketGroup.class)).list();
			return ticketGroupList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public Collection<TicketGroup> getUnmappedTicketGroupsWithPagination(Integer brokerId, Integer eventId,GridHeaderFilters filter,String pageNo){
		Session session =null;
		Integer firstResult = null;
		try {
			String TICKET_GROUP_SQL = "select distinct tg.id AS id,tg.section AS section,tg.row AS row,count(t.id) as quantity,"+
			"min(convert(int,t.seat_number)) AS seatLow, max(convert(int,t.seat_number)) AS seatHigh, tg.price as price,"+
			"tg.invoice_id as invoiceId,tg.purchase_order_id as purchaseOrderId,tg.status AS status," +
			"tg.shipping_method_id AS shippingMethodId,tg.event_id AS eventId,tg.created_date as createdDate," +
			"tg.last_updated AS lastUpdated,tg.created_by AS createdBy,tg.on_hand As onHand, tg.broker_id AS brokerId, tg.broadcast as broadcast "+ 
			"from ticket_group tg with(nolock) "+
			"join ticket t with(nolock) on tg.id=t.ticket_group_id "+
			"left join shipping_method sm with(nolock) on tg.shipping_method_id=sm.id " +
			"where t.ticket_status='ACTIVE' and tg.event_id= "+eventId;
			
			if(brokerId != null && brokerId > 0){
				TICKET_GROUP_SQL += " AND tg.broker_id = "+brokerId+" ";	
			}
			if(filter.getSection() != null){				
				TICKET_GROUP_SQL += " AND tg.section like '%"+filter.getSection()+"%' ";
			}
			if(filter.getRow() != null){
				TICKET_GROUP_SQL += " AND tg.row like '"+filter.getRow()+"' ";
			}
			if(filter.getSeatLow() != null){
				TICKET_GROUP_SQL += " AND tg.seat_low like '"+filter.getSeatLow()+"' ";
			}
			if(filter.getSeatHigh() != null){
				TICKET_GROUP_SQL += " AND tg.seat_high like '"+filter.getSeatHigh()+"' ";
			}
			if(filter.getQuantity() != null){
				TICKET_GROUP_SQL += " AND tg.quantity = "+filter.getQuantity()+" ";
			}
			if(filter.getRetailPrice() != null){
				TICKET_GROUP_SQL += " AND tg.price = "+filter.getRetailPrice()+" ";
			}
			if(filter.getWholeSalePrice() != null){
				TICKET_GROUP_SQL += " AND tg.price = "+filter.getWholeSalePrice()+" ";
			}
			if(filter.getPrice() != null){
				TICKET_GROUP_SQL += " AND tg.price = "+filter.getPrice()+" ";
			}
			if(filter.getShippingMethod() != null){
				TICKET_GROUP_SQL += " AND tg.shipping_method like '%"+filter.getShippingMethod()+"%' ";
			}
			if(filter.getProductType() != null){
				TICKET_GROUP_SQL += " AND 'REWARDTHEFAN' like '%"+filter.getProductType()+"%' "; 
			}
			if(filter.getBrokerId() != null){
				TICKET_GROUP_SQL += " AND tg.broker_id = "+filter.getBrokerId()+" ";
			}
			if(filter.getBroadcast() != null){
				TICKET_GROUP_SQL += " AND tg.broadcast = "+filter.getBroadcast()+" ";
			}
			
			TICKET_GROUP_SQL += "group by tg.id,tg.section,tg.row,tg.seat_low,tg.seat_high,tg.price,tg.invoice_id,tg.purchase_order_id,tg.status," +
			"tg.shipping_method_id,tg.event_id,tg.created_date,tg.last_updated,tg.created_by,tg.on_hand,tg.broker_id,tg.broadcast having count(t.id) > 0";
			session  = getStaticSession();
			SQLQuery query = session.createSQLQuery(TICKET_GROUP_SQL);
			query.addScalar("id",Hibernate.INTEGER);
			query.addScalar("section",Hibernate.STRING);
			query.addScalar("row",Hibernate.STRING);
			query.addScalar("quantity",Hibernate.INTEGER);
			query.addScalar("eventId",Hibernate.INTEGER);
			query.addScalar("seatLow",Hibernate.STRING);
			query.addScalar("seatHigh",Hibernate.STRING);
			query.addScalar("shippingMethodId",Hibernate.INTEGER);
			query.addScalar("price",Hibernate.DOUBLE);
			query.addScalar("invoiceId",Hibernate.INTEGER);
			query.addScalar("purchaseOrderId",Hibernate.INTEGER);
			query.addScalar("status",Hibernate.STRING);
			query.addScalar("lastUpdated",Hibernate.TIMESTAMP);
			query.addScalar("createdBy",Hibernate.STRING);
			query.addScalar("onHand",Hibernate.STRING);
			query.addScalar("brokerId", Hibernate.INTEGER);
			query.addScalar("broadcast", Hibernate.BOOLEAN);
			
			firstResult = PaginationUtil.getNextPageStatFrom(pageNo);
			Collection<TicketGroup> ticketGroupList = query.setResultTransformer(Transformers.aliasToBean(TicketGroup.class)).setFirstResult(firstResult).setMaxResults(PaginationUtil.PAGESIZE).list();
			return ticketGroupList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	/**
	 * Get list of open orders
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<OpenOrders> getOpenOrderList(String fromDate, String toDate, GridHeaderFilters filter, String orderNo, String invoiceNo, String status, 
			String companyProduct,String pageNo, Integer brokerId,String invoiceType){
		Session session =null;
		Integer startFrom = 0;
		try {
			String OPEN_ORDERS_SQL = "select cust.id as customerId,cust.cust_name as customerName,cust.username as username," +
					"cust.customer_type as customerType, " +
					"co.product_type as productType, " +
					"inv.id as invoiceId ," +					
					" cust.company_name as custCompanyName,datediff(dd,inv.created_date,getdate()) as 'invoiceAged',co.is_invoice_email_sent as 'isInvoiceEmailSent'," +
					" voided_date as voidedDate, " +
					"inv.tracking_no as trackingNo, " +
					"sm.name as shippingMethod, " +
					"inv.shipping_method_id as shippingMethodId, " +
					"inv.order_id as customerOrderId, " +
					"inv.ticket_count as ticketCount, " +
					"inv.invoice_total as invoiceTotal, " +
					"inv.invoice_type as invoiceType, " +
					"ca.address_line1 as addressLine1, " +
					"ca.city as city, " +
					"ca.zip_code as zipCode, " +
					"cust.phone as phone, " +
				    "ct.country_name as country, " +
					"s.name as state, " + 
					"inv.created_date as createdDate, " + 
					"inv.created_by as createdBy, " + 
					"inv.csr as csr, " + 
					"inv.last_updated as lastUpdatedDate, " + 
					"inv.last_updated_by as lastUpdatedBy," + 
					"inv.status as status," +
					"co.secondary_order_type as secondaryOrderType,co.secondary_order_id as secondaryOrderId," +
					"inv.purchase_order_no as purchaseOrderNo," +
					"inv.fedex_label_created as fedexLabelCreated,"+
					"co.event_id as eventId,"+
					"co.event_name as eventName,"+
					"co.event_date as eventDate,"+
					"co.event_time as eventTime, "+
					"co.platform as platform, "+
					"inv.broker_id as brokerId, "+
					"co.order_type as orderType, "+
					"co.primary_payment_method as primaryPaymentMethod, "+
					"co.secondary_payment_method as secondaryPaymentMethod, "+
					"co.third_payment_method as thirdPaymentMethod, "+
					"iif(tic.invoice_id is NULL, 'No', 'Yes') as isPoMapped ," +
					"(inv.invoice_total - (co.primary_payment_amount + co.secondary_payment_amount + co.third_payment_amount)) as pendingAmount "+
					"from invoice inv with(nolock) " +
					" inner join customer_order co with(nolock) on co.id=inv.order_id " +
					"inner join customer cust with(nolock) on(cust.id=inv.customer_id) "+
					"left join customer_address ca with(nolock) on(ca.customer_id = cust.id and ca.address_type='BILLING_ADDRESS') " +
					"and ca.id in(select max(id) from customer_address cusAdd with(nolock) where cusAdd.customer_id=cust.id and cusAdd.address_type='BILLING_ADDRESS') "+
					"left join country ct with(nolock) on(ct.id=ca.country) "+
					"left join state s with(nolock) on(s.id=ca.state) "+
					"left join shipping_method sm with(nolock) on(inv.shipping_method_id=sm.id) " +
					"left join (select invoice_id, ticket_status "+
                       "from ticket "+ 
                       "where ticket_status='SOLD' and (purchase_order_id is not null or purchase_order_id <> '') "+
                       "group by invoice_id, ticket_status) tic "+ 
                       "on inv.id = tic.invoice_id and tic.ticket_status = 'SOLD' "+
					"where 1=1 ";
			
			if(invoiceType!=null){
				OPEN_ORDERS_SQL = OPEN_ORDERS_SQL+" AND inv.invoice_type like '"+invoiceType.toString()+"' ";
				if(status!=null && status.equalsIgnoreCase("Completed") && invoiceType.equalsIgnoreCase(PaymentMethods.ACCOUNT_RECIVABLE.toString())){
					//OPEN_ORDERS_SQL += " AND co.primary_payment_method <> '"+invoiceType+"' ";
					OPEN_ORDERS_SQL += " AND inv.status in('Outstanding','Completed') AND (inv.invoice_total - (co.primary_payment_amount + co.secondary_payment_amount + co.third_payment_amount) ) = 0";
				}else if(status!=null && status.equalsIgnoreCase("Outstanding") && invoiceType.equalsIgnoreCase(PaymentMethods.ACCOUNT_RECIVABLE.toString())){
					OPEN_ORDERS_SQL += " AND inv.status in('Outstanding','Completed') AND (inv.invoice_total - (co.primary_payment_amount + co.secondary_payment_amount + co.third_payment_amount) ) > 0";
				}else if(invoiceType.equalsIgnoreCase("CONTEST") && status!=null && !status.isEmpty()){
					OPEN_ORDERS_SQL += " AND inv.status='"+status+"'";
				}
			}else{
				if(status!=null && !status.isEmpty()){
					OPEN_ORDERS_SQL += " AND inv.status='"+status+"'";
				}
			}
			if(fromDate!=null && !fromDate.isEmpty()){
				OPEN_ORDERS_SQL = OPEN_ORDERS_SQL+" AND inv.created_date >='"+fromDate+"' ";
			}
			if(toDate!=null && !toDate.isEmpty()){
				OPEN_ORDERS_SQL = OPEN_ORDERS_SQL+" AND inv.created_date <='"+toDate+"' ";
			}
			if(filter.getInvoiceId() != null){
				OPEN_ORDERS_SQL += " AND inv.id = "+filter.getInvoiceId()+" ";
			}
			if(filter.getCustomerId() != null){
				OPEN_ORDERS_SQL += " AND cust.id = "+filter.getCustomerId()+" ";
			}
			if(filter.getCustomerName() != null){
				OPEN_ORDERS_SQL += " AND cust.cust_name like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getUsername() != null){
				OPEN_ORDERS_SQL += " AND cust.username like '%"+filter.getUsername()+"%' ";
			}
			if(filter.getPackageCost() != null){
				OPEN_ORDERS_SQL += " AND (inv.invoice_total - (co.primary_payment_amount + co.secondary_payment_amount + co.third_payment_amount) ) = "+filter.getPackageCost();
			}
			if(filter.getTicketQty() != null){
				OPEN_ORDERS_SQL += " AND inv.ticket_count = "+filter.getTicketQty()+" ";
			}
			if(filter.getInvoiceTotal() != null){
				OPEN_ORDERS_SQL += " AND inv.invoice_total = "+filter.getInvoiceTotal()+" ";
			}
			if(filter.getInvoiceAged() != null){
				OPEN_ORDERS_SQL += " AND datediff(dd,inv.created_date,getdate()) = "+filter.getInvoiceAged()+" ";
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(day,inv.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(month,inv.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(year,inv.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getCreatedBy() != null){
				OPEN_ORDERS_SQL += " AND inv.created_by like '%"+filter.getCreatedBy()+"%' ";
			}
			if(filter.getCsr() != null){
				OPEN_ORDERS_SQL += " AND inv.csr like '%"+filter.getCsr()+"%' ";
			}
			if(filter.getCustomerOrderId() != null){
				OPEN_ORDERS_SQL += " AND inv.order_id = "+filter.getCustomerOrderId()+" ";
			}
			if(filter.getShippingMethod() != null){
				OPEN_ORDERS_SQL += " AND sm.name like '%"+filter.getShippingMethod()+"%' ";
			}
			if(filter.getCustomerType() != null){
				OPEN_ORDERS_SQL += " AND cust.customer_type like '%"+filter.getCustomerType()+"%' ";
			}
			if(filter.getCompanyName() != null){
				OPEN_ORDERS_SQL += " AND cust.company_name like '%"+filter.getCompanyName()+"%' ";
			}
			if(filter.getProductType() != null){
				OPEN_ORDERS_SQL += " AND co.product_type like '%"+filter.getProductType()+"%' ";
			}
			if(filter.getAddressLine1() != null){
				OPEN_ORDERS_SQL += " AND ca.address_line1 like '%"+filter.getAddressLine1()+"%' ";
			}
			if(filter.getCity() != null){
				OPEN_ORDERS_SQL += " AND ca.city like '%"+filter.getCity()+"%' ";
			}
			if(filter.getState() != null){
				OPEN_ORDERS_SQL += " AND s.name like '%"+filter.getState()+"%' ";
			}
			if(filter.getCountry() != null){
				OPEN_ORDERS_SQL += " AND ct.country_name like '%"+filter.getCountry()+"%' ";
			}
			if(filter.getZipCode() != null){
				OPEN_ORDERS_SQL += " AND ca.zip_code like '%"+filter.getZipCode()+"%' ";
			}
			if(filter.getPhone() != null){
				OPEN_ORDERS_SQL += " AND cust.phone like '%"+filter.getPhone()+"%' ";
			}
			if(filter.getVoidedDateStr() != null){
				if(Util.extractDateElement(filter.getVoidedDateStr(),"DAY") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(day,voided_date) = "+Util.extractDateElement(filter.getVoidedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getVoidedDateStr(),"MONTH") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(month,voided_date) = "+Util.extractDateElement(filter.getVoidedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getVoidedDateStr(),"YEAR") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(year,voided_date) = "+Util.extractDateElement(filter.getVoidedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getStatus() != null){
				OPEN_ORDERS_SQL += " AND inv.status like '%"+filter.getStatus()+"%' ";
			}
			if(filter.getTrackingNo() != null){
				OPEN_ORDERS_SQL += " AND inv.tracking_no = '"+filter.getTrackingNo()+"' ";
			}
			if(filter.getEventId() != null){
				OPEN_ORDERS_SQL += " AND co.event_id = "+filter.getEventId()+" ";
			}
			if(filter.getEventName() != null){
				OPEN_ORDERS_SQL += " AND co.event_name like '%"+filter.getEventName()+"%' ";
			}
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(day,co.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(month,co.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(year,co.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventTimeStr() != null){
				if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(hour,co.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(minute,co.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
				}
			}
			if(filter.getSecondaryOrderType() != null){
				OPEN_ORDERS_SQL += " AND co.secondary_order_type like '%"+filter.getSecondaryOrderType()+"%' ";
			}
			if(filter.getSecondaryOrderId() != null){
				OPEN_ORDERS_SQL += " AND co.secondary_order_id = '"+filter.getSecondaryOrderId()+"' ";
			}
			if(filter.getLastUpdatedDateStr() != null){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(day,inv.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(month,inv.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(year,inv.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getLastUpdatedBy() != null){
				OPEN_ORDERS_SQL += " AND inv.last_updated_by like '%"+filter.getLastUpdatedBy()+"%' ";
			}
			if(filter.getBrokerId() != null){
				OPEN_ORDERS_SQL += " AND inv.broker_id = "+filter.getBrokerId()+" ";
			}
			if(filter.getPlatform() != null && !filter.getPlatform().isEmpty()){
				if(filter.getPlatform().toUpperCase().contains("WEB")){
					OPEN_ORDERS_SQL += " AND co.platform like '%DESKTOP_SITE%' ";
				}else if(filter.getPlatform().toUpperCase().contains("RETAIL")){
					OPEN_ORDERS_SQL += " AND co.platform like '%TICK_TRACKER%' ";
				}else{
					OPEN_ORDERS_SQL += " AND co.platform = '"+filter.getPlatform()+"' ";
				}
			}
			if(filter.getOrderType() != null && filter.getOrderType().equalsIgnoreCase("Yes")){
				OPEN_ORDERS_SQL += " AND co.order_type like 'LOYALFAN' ";
			}
			if(filter.getOrderType() != null && filter.getOrderType().equalsIgnoreCase("No")){
				OPEN_ORDERS_SQL += " AND co.order_type != 'LOYALFAN' ";
			}
			if(filter.getPrimaryPaymentMethod() != null){
				if((filter.getPrimaryPaymentMethod().toUpperCase().contains("BANK")) || (filter.getPrimaryPaymentMethod().toUpperCase().contains("CHECK"))){
					OPEN_ORDERS_SQL += " AND co.primary_payment_method like '%BANK_OR_CHEQUE%' ";
				}else{
					OPEN_ORDERS_SQL += " AND co.primary_payment_method like '%"+filter.getPrimaryPaymentMethod()+"%' ";					
				}
			}
			if(filter.getSecondaryPaymentMethod() != null){
				OPEN_ORDERS_SQL += " AND co.secondary_payment_method like '%"+filter.getSecondaryPaymentMethod()+"%' ";
			}
			if(filter.getThirdPaymentMethod() != null){
				OPEN_ORDERS_SQL += " AND co.third_payment_method like '%"+filter.getThirdPaymentMethod()+"%' ";
			}
			if(filter.getThirdPaymentMethod() != null){
				OPEN_ORDERS_SQL += " AND co.third_payment_method like '%"+filter.getThirdPaymentMethod()+"%' ";
			}
			if(filter.getIsPoMapped() != null){
				if(filter.getIsPoMapped().equalsIgnoreCase("YES")){
					OPEN_ORDERS_SQL += " AND tic.invoice_id is not null ";
				}
				if(filter.getIsPoMapped().equalsIgnoreCase("NO")){
					OPEN_ORDERS_SQL += " AND tic.invoice_id is null ";	
				}
			}
			if(companyProduct!=null && !companyProduct.isEmpty()){
				if(companyProduct.equalsIgnoreCase("REWARDTHEFAN")){
					OPEN_ORDERS_SQL += " AND co.product_type='REWARDTHEFAN' ";
				}else if (companyProduct.equalsIgnoreCase("SEATGEEK")){
					OPEN_ORDERS_SQL += " AND co.product_type='SEATGEEK' ";
				}
			}else{
				OPEN_ORDERS_SQL += " AND co.product_type='REWARDTHEFAN' ";
			}
			if(invoiceNo!=null && !invoiceNo.isEmpty()){
				OPEN_ORDERS_SQL += " AND inv.id="+invoiceNo;
			}
			if(orderNo!=null && !orderNo.isEmpty()){
				OPEN_ORDERS_SQL += " AND inv.order_id="+orderNo;
			}
			if(brokerId != null && brokerId > 0){
				OPEN_ORDERS_SQL += " AND inv.broker_id = "+brokerId+" ";
			}
			/*
			if(csr!=null && !csr.isEmpty()){
				OPEN_ORDERS_SQL += " AND inv.csr like '%"+csr+"%' ";
			}
			if(customer!=null && !customer.isEmpty()){
				OPEN_ORDERS_SQL += " AND cust.cust_name like '%"+customer+"%'";
			}
			if(externalOrderId!=null && !externalOrderId.isEmpty()){
				OPEN_ORDERS_SQL += " AND co.secondary_order_id='"+externalOrderId+"' ";
			}
			*/
			
			String sortingSql = GridSortingUtil.getInvoicesSortingSql(filter);
			if(sortingSql!=null && !sortingSql.isEmpty()){
				OPEN_ORDERS_SQL += sortingSql;
			}else{
				OPEN_ORDERS_SQL += " ORDER BY inv.id desc";
			}
			session  = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(OPEN_ORDERS_SQL);
			sqlQuery.addScalar("customerId", Hibernate.INTEGER);
			sqlQuery.addScalar("customerName", Hibernate.STRING);
			sqlQuery.addScalar("username", Hibernate.STRING);
			sqlQuery.addScalar("customerType", Hibernate.STRING);
			sqlQuery.addScalar("productType", Hibernate.STRING);
			sqlQuery.addScalar("invoiceId", Hibernate.INTEGER);
			sqlQuery.addScalar("customerOrderId", Hibernate.INTEGER);
			sqlQuery.addScalar("ticketCount", Hibernate.INTEGER);
			sqlQuery.addScalar("invoiceTotal", Hibernate.DOUBLE);
			sqlQuery.addScalar("addressLine1", Hibernate.STRING);
			sqlQuery.addScalar("city", Hibernate.STRING);
			sqlQuery.addScalar("phone", Hibernate.STRING);
			sqlQuery.addScalar("zipCode", Hibernate.STRING);
			sqlQuery.addScalar("country", Hibernate.STRING);
			sqlQuery.addScalar("state", Hibernate.STRING);
			sqlQuery.addScalar("createdDate", Hibernate.TIMESTAMP);
			sqlQuery.addScalar("createdBy", Hibernate.STRING);
			sqlQuery.addScalar("csr", Hibernate.STRING);
			sqlQuery.addScalar("platform", Hibernate.STRING);
			sqlQuery.addScalar("lastUpdatedDate", Hibernate.TIMESTAMP);
			sqlQuery.addScalar("lastUpdatedBy", Hibernate.STRING);
			sqlQuery.addScalar("trackingNo", Hibernate.STRING);
			sqlQuery.addScalar("voidedDate", Hibernate.TIMESTAMP);
			sqlQuery.addScalar("invoiceAged", Hibernate.INTEGER);
			sqlQuery.addScalar("isInvoiceEmailSent", Hibernate.BOOLEAN);
			sqlQuery.addScalar("custCompanyName", Hibernate.STRING);
			sqlQuery.addScalar("shippingMethod", Hibernate.STRING);
			sqlQuery.addScalar("shippingMethodId", Hibernate.INTEGER);
			sqlQuery.addScalar("status", Hibernate.STRING);
			 sqlQuery.addScalar("purchaseOrderNo", Hibernate.STRING);
			 sqlQuery.addScalar("fedexLabelCreated", Hibernate.BOOLEAN);
			 sqlQuery.addScalar("secondaryOrderType", Hibernate.STRING);
			 sqlQuery.addScalar("secondaryOrderId", Hibernate.STRING);
			 sqlQuery.addScalar("eventId", Hibernate.INTEGER);
			 sqlQuery.addScalar("eventName", Hibernate.STRING);
			 sqlQuery.addScalar("eventDate", Hibernate.DATE);
			 sqlQuery.addScalar("eventTime", Hibernate.TIME);
			 sqlQuery.addScalar("brokerId", Hibernate.INTEGER);
			 sqlQuery.addScalar("orderType", Hibernate.STRING);
			 sqlQuery.addScalar("primaryPaymentMethod", Hibernate.STRING);
			 sqlQuery.addScalar("secondaryPaymentMethod", Hibernate.STRING);
			 sqlQuery.addScalar("thirdPaymentMethod", Hibernate.STRING);
			 sqlQuery.addScalar("invoiceType", Hibernate.STRING);
			 sqlQuery.addScalar("isPoMapped", Hibernate.STRING);
			 sqlQuery.addScalar("pendingAmount", Hibernate.DOUBLE);
			 startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
			Collection<OpenOrders> openOrdersList = sqlQuery.setResultTransformer(Transformers.aliasToBean(OpenOrders.class)).setFirstResult(startFrom)
			.setMaxResults(PaginationUtil.PAGESIZE).list();
			return openOrdersList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	
	/**
	 * Get list of open orders
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<OpenOrders> getOpenOrderListToExport(String invoiceNo,String orderNo, String csr,String customer, String fromDate, 
			String toDate,String status,String companyProduct,String externalOrderId, GridHeaderFilters filter, Integer brokerId,String invoiceType){
		Session session =null;
		try {
			String OPEN_ORDERS_SQL = "select cust.id as customerId,cust.cust_name as customerName,cust.username as username," +
					"cust.customer_type as customerType, " +
					"co.product_type as productType, " +
					"inv.id as invoiceId ," +
					
					" cust.company_name as custCompanyName,datediff(dd,inv.created_date,getdate()) as 'invoiceAged',co.is_invoice_email_sent as 'isInvoiceEmailSent'," +
					" voided_date as voidedDate, " +
					"inv.tracking_no as trackingNo, " +
					"sm.name as shippingMethod, " +
					"inv.shipping_method_id as shippingMethodId, " +
					"inv.order_id as customerOrderId, " +
					"inv.ticket_count as ticketCount, " +
					"inv.invoice_total as invoiceTotal, " +
					"inv.invoice_type as invoiceType, " +
					"ca.address_line1 as addressLine1, " +
					"ca.city as city, " +
					"ca.zip_code as zipCode, " +
					"cust.phone as phone, " +
				    "ct.country_name as country, " +
					"s.name as state, " + 
					"inv.created_date as createdDate, " + 
					"inv.created_by as createdBy, " + 
					"inv.csr as csr, " + 
					"inv.last_updated as lastUpdatedDate, " + 
					"inv.last_updated_by as lastUpdatedBy," + 
					"inv.status as status," +
					"co.secondary_order_type as secondaryOrderType,co.secondary_order_id as secondaryOrderId," +
					"inv.purchase_order_no as purchaseOrderNo," +
					"co.event_id as eventId,"+
					"co.event_name as eventName,"+
					"co.event_date as eventDate,"+
					"co.event_time as eventTime, "+
					"inv.broker_id as brokerId, "+
					"co.order_type as orderType, "+
					"co.primary_payment_method as primaryPaymentMethod, "+
					"co.secondary_payment_method as secondaryPaymentMethod, "+
					"co.third_payment_method as thirdPaymentMethod, "+
					"iif(tic.invoice_id is NULL, 'No', 'Yes') as isPoMapped "+
					"from invoice inv with(nolock) " +
					" inner join customer_order co with(nolock) on co.id=inv.order_id " +
					"inner join customer cust with(nolock) on(cust.id=inv.customer_id) "+
					"left join customer_address ca with(nolock) on(ca.customer_id = cust.id and ca.address_type='BILLING_ADDRESS') "+
					"left join country ct with(nolock) on(ct.id=ca.country) "+
					"left join state s with(nolock) on(s.id=ca.state) "+
					"left join shipping_method sm with(nolock) on(inv.shipping_method_id=sm.id)"+
					"left join (select invoice_id, ticket_status "+
                    "from ticket "+ 
                    "where ticket_status='SOLD' and (purchase_order_id is not null or purchase_order_id <> '') "+
                    "group by invoice_id, ticket_status) tic "+ 
                    "on inv.id = tic.invoice_id and tic.ticket_status = 'SOLD' "+
					"where 1=1 ";
			
			if(invoiceType!=null){
				OPEN_ORDERS_SQL = OPEN_ORDERS_SQL+" AND inv.invoice_type like '"+invoiceType+"' ";
				if(status!=null && status.equalsIgnoreCase("Completed")){
					OPEN_ORDERS_SQL += " AND co.primary_payment_method <> '"+invoiceType+"' ";
				}else if(status!=null && status.equalsIgnoreCase("Outstanding")){
					OPEN_ORDERS_SQL += " AND co.primary_payment_method = '"+invoiceType+"' ";
				}
			}else{
				if(status!=null && !status.isEmpty()){
					OPEN_ORDERS_SQL += " AND inv.status='"+status+"'";
				}
			}
			if(invoiceNo!=null && !invoiceNo.isEmpty()){
				OPEN_ORDERS_SQL += " AND inv.id="+invoiceNo;
			}
			if(fromDate!=null && !fromDate.isEmpty()){
				OPEN_ORDERS_SQL = OPEN_ORDERS_SQL+" AND inv.created_date >='"+fromDate+"' ";
			}
			if(toDate!=null && !toDate.isEmpty()){
				OPEN_ORDERS_SQL = OPEN_ORDERS_SQL+" AND inv.created_date <='"+toDate+"' ";
			}
			if(csr!=null && !csr.isEmpty()){
				OPEN_ORDERS_SQL += " AND inv.csr like '%"+csr+"%' ";
			}
			if(customer!=null && !customer.isEmpty()){
				OPEN_ORDERS_SQL += " AND cust.cust_name like '%"+customer+"%'";
			}
			if(orderNo!=null && !orderNo.isEmpty()){
				OPEN_ORDERS_SQL += " AND inv.order_id="+orderNo;
			}
			/*if(status!=null && !status.isEmpty()){
				OPEN_ORDERS_SQL += " AND inv.status='"+status+"'";
			}*/
			if(externalOrderId!=null && !externalOrderId.isEmpty()){
				OPEN_ORDERS_SQL += " AND co.secondary_order_id='"+externalOrderId+"' ";
			}			
			if(companyProduct!=null && !companyProduct.isEmpty()){
				if(companyProduct.equalsIgnoreCase("REWARDTHEFAN")){
					OPEN_ORDERS_SQL += " AND co.product_type='REWARDTHEFAN' ";
				}else if (companyProduct.equalsIgnoreCase("SEATGEEK")){
					OPEN_ORDERS_SQL += " AND co.product_type='SEATGEEK' ";
				}
			}else{
				OPEN_ORDERS_SQL += " AND co.product_type='REWARDTHEFAN' ";
			}
			if(brokerId != null && brokerId > 0){
				OPEN_ORDERS_SQL += " AND inv.broker_id = "+brokerId+" ";
			}
			if(filter.getInvoiceId() != null){
				OPEN_ORDERS_SQL += " AND inv.id = "+filter.getInvoiceId()+" ";
			}
			if(filter.getCustomerId() != null){
				OPEN_ORDERS_SQL += " AND cust.id = "+filter.getCustomerId()+" ";
			}
			if(filter.getCustomerName() != null){
				OPEN_ORDERS_SQL += " AND cust.cust_name like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getUsername() != null){
				OPEN_ORDERS_SQL += " AND cust.username like '%"+filter.getUsername()+"%' ";
			}
			if(filter.getTicketQty() != null){
				OPEN_ORDERS_SQL += " AND inv.ticket_count = "+filter.getTicketQty()+" ";
			}
			if(filter.getInvoiceTotal() != null){
				OPEN_ORDERS_SQL += " AND inv.invoice_total = "+filter.getInvoiceTotal()+" ";
			}
			if(filter.getInvoiceAged() != null){
				OPEN_ORDERS_SQL += " AND datediff(dd,inv.created_date,getdate()) = "+filter.getInvoiceAged()+" ";
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(day,inv.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(month,inv.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(year,inv.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getCreatedBy() != null){
				OPEN_ORDERS_SQL += " AND inv.created_by like '%"+filter.getCreatedBy()+"%' ";
			}
			if(filter.getCsr() != null){
				OPEN_ORDERS_SQL += " AND inv.csr like '%"+filter.getCsr()+"%' ";
			}
			if(filter.getCustomerOrderId() != null){
				OPEN_ORDERS_SQL += " AND inv.order_id = "+filter.getCustomerOrderId()+" ";
			}
			if(filter.getShippingMethod() != null){
				OPEN_ORDERS_SQL += " AND sm.name like '%"+filter.getShippingMethod()+"%' ";
			}
			if(filter.getCustomerType() != null){
				OPEN_ORDERS_SQL += " AND cust.customer_type like '%"+filter.getCustomerType()+"%' ";
			}
			if(filter.getCompanyName() != null){
				OPEN_ORDERS_SQL += " AND cust.company_name like '%"+filter.getCompanyName()+"%' ";
			}
			if(filter.getProductType() != null){
				OPEN_ORDERS_SQL += " AND co.product_type like '%"+filter.getProductType()+"%' ";
			}
			if(filter.getAddressLine1() != null){
				OPEN_ORDERS_SQL += " AND ca.address_line1 like '%"+filter.getAddressLine1()+"%' ";
			}
			if(filter.getCity() != null){
				OPEN_ORDERS_SQL += " AND ca.city like '%"+filter.getCity()+"%' ";
			}
			if(filter.getState() != null){
				OPEN_ORDERS_SQL += " AND s.name like '%"+filter.getState()+"%' ";
			}
			if(filter.getCountry() != null){
				OPEN_ORDERS_SQL += " AND ct.country_name like '%"+filter.getCountry()+"%' ";
			}
			if(filter.getZipCode() != null){
				OPEN_ORDERS_SQL += " AND ca.zip_code like '%"+filter.getZipCode()+"%' ";
			}
			if(filter.getPhone() != null){
				OPEN_ORDERS_SQL += " AND cust.phone like '%"+filter.getPhone()+"%' ";
			}
			if(filter.getVoidedDateStr() != null){
				if(Util.extractDateElement(filter.getVoidedDateStr(),"DAY") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(day,voided_date) = "+Util.extractDateElement(filter.getVoidedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getVoidedDateStr(),"MONTH") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(month,voided_date) = "+Util.extractDateElement(filter.getVoidedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getVoidedDateStr(),"YEAR") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(year,voided_date) = "+Util.extractDateElement(filter.getVoidedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getStatus() != null){
				OPEN_ORDERS_SQL += " AND inv.status like '%"+filter.getStatus()+"%' ";
			}
			if(filter.getTrackingNo() != null){
				OPEN_ORDERS_SQL += " AND inv.tracking_no = '"+filter.getTrackingNo()+"' ";
			}
			if(filter.getEventId() != null){
				OPEN_ORDERS_SQL += " AND co.event_id = "+filter.getEventId()+" ";
			}
			if(filter.getEventName() != null){
				OPEN_ORDERS_SQL += " AND co.event_name like '%"+filter.getEventName()+"%' ";
			}
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(day,co.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(month,co.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(year,co.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventTimeStr() != null){
				if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(hour,co.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(minute,co.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
				}
			}
			if(filter.getSecondaryOrderType() != null){
				OPEN_ORDERS_SQL += " AND co.secondary_order_type like '%"+filter.getSecondaryOrderType()+"%' ";
			}
			if(filter.getSecondaryOrderId() != null){
				OPEN_ORDERS_SQL += " AND co.secondary_order_id = '"+filter.getSecondaryOrderId()+"' ";
			}
			if(filter.getLastUpdatedDateStr() != null){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(day,inv.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(month,inv.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(year,inv.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getLastUpdatedBy() != null){
				OPEN_ORDERS_SQL += " AND inv.last_updated_by like '%"+filter.getLastUpdatedBy()+"%' ";
			}
			if(filter.getBrokerId() != null){
				OPEN_ORDERS_SQL += " AND inv.broker_id = "+filter.getBrokerId()+" ";
			}
			if(filter.getOrderType() != null && filter.getOrderType().equalsIgnoreCase("Yes")){
				OPEN_ORDERS_SQL += " AND co.order_type like 'LOYALFAN' ";
			}
			if(filter.getOrderType() != null && filter.getOrderType().equalsIgnoreCase("No")){
				OPEN_ORDERS_SQL += " AND co.order_type != 'LOYALFAN' ";
			}
			if(filter.getPrimaryPaymentMethod() != null){
				OPEN_ORDERS_SQL += " AND co.primary_payment_method like '%"+filter.getPrimaryPaymentMethod()+"%' ";
			}
			if(filter.getSecondaryPaymentMethod() != null){
				OPEN_ORDERS_SQL += " AND co.secondary_payment_method like '%"+filter.getSecondaryPaymentMethod()+"%' ";
			}
			if(filter.getThirdPaymentMethod() != null){
				OPEN_ORDERS_SQL += " AND co.third_payment_method like '%"+filter.getThirdPaymentMethod()+"%' ";
			}
			if(filter.getIsPoMapped() != null){
				if(filter.getIsPoMapped().equalsIgnoreCase("YES")){
					OPEN_ORDERS_SQL += " AND tic.invoice_id is not null ";
				}
				if(filter.getIsPoMapped().equalsIgnoreCase("NO")){
					OPEN_ORDERS_SQL += " AND tic.invoice_id is null ";	
				}
			}
			OPEN_ORDERS_SQL += " order by inv.id desc";
			session  = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(OPEN_ORDERS_SQL);
			sqlQuery.addScalar("customerId", Hibernate.INTEGER);
			sqlQuery.addScalar("customerName", Hibernate.STRING);
			sqlQuery.addScalar("username", Hibernate.STRING);
			sqlQuery.addScalar("customerType", Hibernate.STRING);
			sqlQuery.addScalar("productType", Hibernate.STRING);
			sqlQuery.addScalar("invoiceId", Hibernate.INTEGER);
			sqlQuery.addScalar("customerOrderId", Hibernate.INTEGER);
			sqlQuery.addScalar("ticketCount", Hibernate.INTEGER);
			sqlQuery.addScalar("invoiceTotal", Hibernate.DOUBLE);
			sqlQuery.addScalar("addressLine1", Hibernate.STRING);
			sqlQuery.addScalar("city", Hibernate.STRING);
			sqlQuery.addScalar("phone", Hibernate.STRING);
			sqlQuery.addScalar("zipCode", Hibernate.STRING);
			sqlQuery.addScalar("country", Hibernate.STRING);
			sqlQuery.addScalar("state", Hibernate.STRING);
			sqlQuery.addScalar("createdDate", Hibernate.TIMESTAMP);
			sqlQuery.addScalar("createdBy", Hibernate.STRING);
			sqlQuery.addScalar("csr", Hibernate.STRING);
			sqlQuery.addScalar("lastUpdatedDate", Hibernate.TIMESTAMP);
			sqlQuery.addScalar("lastUpdatedBy", Hibernate.STRING);
			sqlQuery.addScalar("trackingNo", Hibernate.STRING);
			sqlQuery.addScalar("voidedDate", Hibernate.TIMESTAMP);
			sqlQuery.addScalar("invoiceAged", Hibernate.INTEGER);
			sqlQuery.addScalar("isInvoiceEmailSent", Hibernate.BOOLEAN);
			sqlQuery.addScalar("custCompanyName", Hibernate.STRING);
			sqlQuery.addScalar("shippingMethod", Hibernate.STRING);
			sqlQuery.addScalar("shippingMethodId", Hibernate.INTEGER);
			sqlQuery.addScalar("status", Hibernate.STRING);
			 sqlQuery.addScalar("purchaseOrderNo", Hibernate.STRING);
			 sqlQuery.addScalar("secondaryOrderType", Hibernate.STRING);
			 sqlQuery.addScalar("secondaryOrderId", Hibernate.STRING);
			 sqlQuery.addScalar("eventId", Hibernate.INTEGER);
			 sqlQuery.addScalar("eventName", Hibernate.STRING);
			 sqlQuery.addScalar("eventDate", Hibernate.DATE);
			 sqlQuery.addScalar("eventTime", Hibernate.TIME);
			 sqlQuery.addScalar("brokerId", Hibernate.INTEGER);
			 sqlQuery.addScalar("orderType", Hibernate.STRING);
			 sqlQuery.addScalar("primaryPaymentMethod", Hibernate.STRING);
			 sqlQuery.addScalar("secondaryPaymentMethod", Hibernate.STRING);
			 sqlQuery.addScalar("thirdPaymentMethod", Hibernate.STRING);
			 sqlQuery.addScalar("invoiceType", Hibernate.STRING);
			 sqlQuery.addScalar("isPoMapped", Hibernate.STRING);
			 
			Collection<OpenOrders> openOrdersList = sqlQuery.setResultTransformer(Transformers.aliasToBean(OpenOrders.class)).setMaxResults(65535).list();
			return openOrdersList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	
	/**
	 * Get Total number of records in list of open orders
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Integer getOpenOrderListCount(String fromDate, String toDate, GridHeaderFilters filter, String orderNo, 
			String invoiceNo, String status, String companyProduct, Integer brokerId,String invoiceType){
		int count = 0;
		Session session =null;
		try {
			String OPEN_ORDERS_SQL = "select count(*) as cnt "+
					"from invoice inv with(nolock) " +
					"inner join customer_order co with(nolock) on co.id=inv.order_id " +
					"inner join customer cust with(nolock) on(cust.id=inv.customer_id) "+
					"left join customer_address ca with(nolock) on(ca.customer_id = cust.id and ca.address_type='BILLING_ADDRESS') "+
					"and ca.id in(select max(id) from customer_address cusAdd with(nolock) where cusAdd.customer_id=cust.id and cusAdd.address_type='BILLING_ADDRESS') "+
					"left join country ct with(nolock) on(ct.id=ca.country) "+
					"left join state s with(nolock) on(s.id=ca.state) "+
					"left join shipping_method sm with(nolock) on(inv.shipping_method_id=sm.id) "+
					"left join (select invoice_id, ticket_status "+
                    "from ticket "+ 
                    "where ticket_status='SOLD' and (purchase_order_id is not null or purchase_order_id <> '') "+
                    "group by invoice_id, ticket_status) tic "+ 
                    "on inv.id = tic.invoice_id and tic.ticket_status = 'SOLD' "+
					"where 1=1 ";
			
			if(invoiceType!=null){
				OPEN_ORDERS_SQL = OPEN_ORDERS_SQL+" AND inv.invoice_type like '"+invoiceType.toString()+"' ";
				if(status!=null && status.equalsIgnoreCase("Completed") && invoiceType.equalsIgnoreCase(PaymentMethods.ACCOUNT_RECIVABLE.toString())){
					//OPEN_ORDERS_SQL += " AND co.primary_payment_method <> '"+invoiceType+"' ";
					OPEN_ORDERS_SQL += " AND inv.status in('Outstanding','Completed') AND (inv.invoice_total - (co.primary_payment_amount + co.secondary_payment_amount + co.third_payment_amount) ) = 0";
				}else if(status!=null && status.equalsIgnoreCase("Outstanding") && invoiceType.equalsIgnoreCase(PaymentMethods.ACCOUNT_RECIVABLE.toString())){
					OPEN_ORDERS_SQL += " AND inv.status in('Outstanding','Completed') AND (inv.invoice_total - (co.primary_payment_amount + co.secondary_payment_amount + co.third_payment_amount) ) > 0";
				}else if(invoiceType.equalsIgnoreCase("CONTEST") && status!=null && !status.isEmpty()){
					OPEN_ORDERS_SQL += " AND inv.status='"+status+"'";
				}
			}else{
				if(status!=null && !status.isEmpty()){
					OPEN_ORDERS_SQL += " AND inv.status='"+status+"'";
				}
			}
			if(fromDate!=null && !fromDate.isEmpty()){
				OPEN_ORDERS_SQL = OPEN_ORDERS_SQL+" AND inv.created_date >='"+fromDate+"' ";
			}
			if(toDate!=null && !toDate.isEmpty()){
				OPEN_ORDERS_SQL = OPEN_ORDERS_SQL+" AND inv.created_date <='"+toDate+"' ";
			}
			if(filter.getInvoiceId() != null){
				OPEN_ORDERS_SQL += " AND inv.id = "+filter.getInvoiceId()+" ";
			}
			if(filter.getCustomerId() != null){
				OPEN_ORDERS_SQL += " AND cust.id = "+filter.getCustomerId()+" ";
			}
			if(filter.getCustomerName() != null){
				OPEN_ORDERS_SQL += " AND cust.cust_name like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getUsername() != null){
				OPEN_ORDERS_SQL += " AND cust.username like '%"+filter.getUsername()+"%' ";
			}
			if(filter.getPackageCost() != null){
				OPEN_ORDERS_SQL += " AND (inv.invoice_total - (co.primary_payment_amount + co.secondary_payment_amount + co.third_payment_amount )) = "+filter.getPackageCost();
			}
			if(filter.getTicketQty() != null){
				OPEN_ORDERS_SQL += " AND inv.ticket_count = "+filter.getTicketQty()+" ";
			}
			if(filter.getPlatform() != null && !filter.getPlatform().isEmpty()){
				if(filter.getPlatform().toUpperCase().contains("WEB")){
					OPEN_ORDERS_SQL += " AND co.platform like '%DESKTOP_SITE%' ";
				}else if(filter.getPlatform().toUpperCase().contains("RETAIL")){
					OPEN_ORDERS_SQL += " AND co.platform like '%TICK_TRACKER%' ";
				}else{
					OPEN_ORDERS_SQL += " AND co.platform = '"+filter.getPlatform()+"' ";
				}
			}
			if(filter.getInvoiceTotal() != null){
				OPEN_ORDERS_SQL += " AND inv.invoice_total = "+filter.getInvoiceTotal()+" ";
			}
			if(filter.getInvoiceAged() != null){
				OPEN_ORDERS_SQL += " AND datediff(dd,inv.created_date,getdate()) = "+filter.getInvoiceAged()+" ";
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(day,inv.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(month,inv.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(year,inv.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getCreatedBy() != null){
				OPEN_ORDERS_SQL += " AND inv.created_by like '%"+filter.getCreatedBy()+"%' ";
			}
			if(filter.getCsr() != null){
				OPEN_ORDERS_SQL += " AND inv.csr like '%"+filter.getCsr()+"%' ";
			}
			if(filter.getCustomerOrderId() != null){
				OPEN_ORDERS_SQL += " AND inv.order_id = "+filter.getCustomerOrderId()+" ";
			}
			if(filter.getShippingMethod() != null){
				OPEN_ORDERS_SQL += " AND sm.name like '%"+filter.getShippingMethod()+"%' ";
			}
			if(filter.getCustomerType() != null){
				OPEN_ORDERS_SQL += " AND cust.customer_type like '%"+filter.getCustomerType()+"%' ";
			}
			if(filter.getCompanyName() != null){
				OPEN_ORDERS_SQL += " AND cust.company_name like '%"+filter.getCompanyName()+"%' ";
			}
			if(filter.getProductType() != null){
				OPEN_ORDERS_SQL += " AND co.product_type like '%"+filter.getProductType()+"%' ";
			}
			if(filter.getAddressLine1() != null){
				OPEN_ORDERS_SQL += " AND ca.address_line1 like '%"+filter.getAddressLine1()+"%' ";
			}
			if(filter.getCity() != null){
				OPEN_ORDERS_SQL += " AND ca.city like '%"+filter.getCity()+"%' ";
			}
			if(filter.getState() != null){
				OPEN_ORDERS_SQL += " AND s.name like '%"+filter.getState()+"%' ";
			}
			if(filter.getCountry() != null){
				OPEN_ORDERS_SQL += " AND ct.country_name like '%"+filter.getCountry()+"%' ";
			}
			if(filter.getZipCode() != null){
				OPEN_ORDERS_SQL += " AND ca.zip_code like '%"+filter.getZipCode()+"%' ";
			}
			if(filter.getPhone() != null){
				OPEN_ORDERS_SQL += " AND cust.phone like '%"+filter.getPhone()+"%' ";
			}
			if(filter.getVoidedDateStr() != null){
				if(Util.extractDateElement(filter.getVoidedDateStr(),"DAY") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(day,voided_date) = "+Util.extractDateElement(filter.getVoidedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getVoidedDateStr(),"MONTH") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(month,voided_date) = "+Util.extractDateElement(filter.getVoidedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getVoidedDateStr(),"YEAR") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(year,voided_date) = "+Util.extractDateElement(filter.getVoidedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getStatus() != null){
				OPEN_ORDERS_SQL += " AND inv.status like '%"+filter.getStatus()+"%' ";
			}
			if(filter.getTrackingNo() != null){
				OPEN_ORDERS_SQL += " AND inv.tracking_no = '"+filter.getTrackingNo()+"' ";
			}
			if(filter.getEventId() != null){
				OPEN_ORDERS_SQL += " AND co.event_id = "+filter.getEventId()+" ";
			}
			if(filter.getEventName() != null){
				OPEN_ORDERS_SQL += " AND co.event_name like '%"+filter.getEventName()+"%' ";
			}
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(day,co.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(month,co.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(year,co.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventTimeStr() != null){
				if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(hour,co.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(minute,co.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
				}
			}
			if(filter.getSecondaryOrderType() != null){
				OPEN_ORDERS_SQL += " AND co.secondary_order_type like '%"+filter.getSecondaryOrderType()+"%' ";
			}
			if(filter.getSecondaryOrderId() != null){
				OPEN_ORDERS_SQL += " AND co.secondary_order_id = '"+filter.getSecondaryOrderId()+"' ";
			}
			if(filter.getLastUpdatedDateStr() != null){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(day,inv.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(month,inv.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(year,inv.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getLastUpdatedBy() != null){
				OPEN_ORDERS_SQL += " AND inv.last_updated_by like '%"+filter.getLastUpdatedBy()+"%' ";
			}
			if(filter.getBrokerId() != null){
				OPEN_ORDERS_SQL += " AND inv.broker_id = "+filter.getBrokerId()+" ";
			}
			if(filter.getOrderType() != null && filter.getOrderType().equalsIgnoreCase("Yes")){
				OPEN_ORDERS_SQL += " AND co.order_type like 'LOYALFAN' ";
			}
			if(filter.getOrderType() != null && filter.getOrderType().equalsIgnoreCase("No")){
				OPEN_ORDERS_SQL += " AND co.order_type != 'LOYALFAN' ";
			}
			if(filter.getPrimaryPaymentMethod() != null){
				if((filter.getPrimaryPaymentMethod().toUpperCase().contains("BANK")) || (filter.getPrimaryPaymentMethod().toUpperCase().contains("CHECK"))){
					OPEN_ORDERS_SQL += " AND co.primary_payment_method like '%BANK_OR_CHEQUE%' ";
				}else{
					OPEN_ORDERS_SQL += " AND co.primary_payment_method like '%"+filter.getPrimaryPaymentMethod()+"%' ";					
				}
			}
			if(filter.getSecondaryPaymentMethod() != null){
				OPEN_ORDERS_SQL += " AND co.secondary_payment_method like '%"+filter.getSecondaryPaymentMethod()+"%' ";
			}
			if(filter.getThirdPaymentMethod() != null){
				OPEN_ORDERS_SQL += " AND co.third_payment_method like '%"+filter.getThirdPaymentMethod()+"%' ";
			}
			if(filter.getIsPoMapped() != null){
				if(filter.getIsPoMapped().equalsIgnoreCase("YES")){
					OPEN_ORDERS_SQL += " AND tic.invoice_id is not null ";
				}
				if(filter.getIsPoMapped().equalsIgnoreCase("NO")){
					OPEN_ORDERS_SQL += " AND tic.invoice_id is null ";	
				}
			}
			/*if(status!=null && !status.isEmpty()){
				OPEN_ORDERS_SQL += " AND inv.status='"+status+"'";
			}*/			
			if(companyProduct!=null && !companyProduct.isEmpty()){
				if(companyProduct.equalsIgnoreCase("REWARDTHEFAN")){
					OPEN_ORDERS_SQL += " AND co.product_type='REWARDTHEFAN' ";
				}else if (companyProduct.equalsIgnoreCase("SEATGEEK")){
					OPEN_ORDERS_SQL += " AND co.product_type='SEATGEEK' ";
				}
			}else{
				OPEN_ORDERS_SQL += " AND co.product_type='REWARDTHEFAN' ";
			}
			if(invoiceNo!=null && !invoiceNo.isEmpty()){
				OPEN_ORDERS_SQL += " AND inv.id="+invoiceNo;
			}
			if(orderNo!=null && !orderNo.isEmpty()){
				OPEN_ORDERS_SQL += " AND inv.order_id="+orderNo;
			}
			if(brokerId != null && brokerId > 0){
				OPEN_ORDERS_SQL += " AND inv.broker_id = "+brokerId+" ";
			}
			/*
			if(csr!=null && !csr.isEmpty()){
				OPEN_ORDERS_SQL += " AND inv.csr like '%"+csr+"%' ";
			}
			if(customer!=null && !customer.isEmpty()){
				OPEN_ORDERS_SQL += " AND cust.cust_name like '%"+customer+"%'";
			}
			if(externalOrderId!=null && !externalOrderId.isEmpty()){
				OPEN_ORDERS_SQL += " AND co.secondary_order_id='"+externalOrderId+"' ";
			}
			*/
			session  = sessionFactory.openSession();
			
			Query query = session.createSQLQuery(OPEN_ORDERS_SQL);
			count = Integer.valueOf((query.uniqueResult()).toString());
			return count;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	
	/**
	 * Get list of open orders RTW/RTW2
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<OpenOrders> getRTWOpenOrderList(String fromDate,String toDate,GridHeaderFilters filter,String orderNo,String invoiceNo,String status,String companyProduct,Integer customerId,String pageNo){
		Session session =null;
		Integer startFrom = 0;
		try {
			/*String OPEN_ORDERS_SQL = "select c.id as customerId,c.cust_name as customerName,c.username as username,c.customer_type as customerType,"+
					"i.product_type as productType, i.invoice_id as invoiceId ,c.company_name as custCompanyName,"+
					"datediff(dd,i.create_date,getdate()) as 'invoiceAged',i.isemailed as 'isInvoiceEmailSent', ctvi.create_date as voidedDate,"+
					"st.shipping_tracking_number as trackingNo, sm.name as shippingMethod, sm.id as shippingMethodId, i.ticket_request_id as customerOrderId,"+
					"i.invoice_total as invoiceTotal, ca.address_line1 as addressLine1, ca.city as city,"+
					"ca.zip_code as zipCode, c.phone as phone, ct.country_name as country, s.name as state, i.create_date as createdDate,"+
					"tu.username as createdBy,pis.invoice_status_desc as status,"+
					"co.event_id as eventId,co.event_name as eventName,co.event_datetime as eventDate,"+
					"co.event_datetime as eventTime"+
					" from pos_invoice i"+
					" left join customer c on c.id = i.customer_id"+
					" left join pos_invoice_status pis on i.invoice_status_id= pis.invoice_status_id"+
					" left join pos_shipping_tracking st on i.shipping_tracking_id = st.shipping_tracking_id"+
					" left join customer_address ca on (ca.customer_id = c.id and ca.address_type='BILLING_ADDRESS')"+
					" left join pos_category_ticket_voided_invoice ctvi on i.invoice_id = ctvi.invoice_id"+
					" left join pos_customer_order co on i.ticket_request_id = co.ticket_request_id"+
					" left join country ct on(ct.id=ca.country)"+ 
					" left join state s on(s.id=ca.state)"+
					" left join shipping_method sm on st.shipping_account_delivery_method_id = sm.id"+
					" left join tracker_users tu on i.system_user_id=tu.id"+			
					" where 1=1 ";*/
			
			String OPEN_ORDERS_SQL = "select customerId,customerName,username,customerType,"+
					"productType,invoiceId,custCompanyName,csr,"+
					"invoiceAged,isInvoiceEmailSent,voidedDate,"+
					"trackingNo,shippingMethod,shippingMethodId,customerOrderId,"+
					"invoiceTotal,addressLine1,city,ticketCount,"+
					"zipCode,phone,country,state,createdDate,"+
					"createdBy,status,"+
					"eventId,eventName,eventDate,"+
					"eventTime"+
					" from pos_invoice_view with(nolock) where 1=1";
						
			if(fromDate!=null && !fromDate.isEmpty()){
				OPEN_ORDERS_SQL = OPEN_ORDERS_SQL+" AND createdDate >='"+fromDate+"' ";
			}
			if(toDate!=null && !toDate.isEmpty()){
				OPEN_ORDERS_SQL = OPEN_ORDERS_SQL+" AND createdDate <='"+toDate+"' ";
			}
			if(filter.getInvoiceId() != null){
				OPEN_ORDERS_SQL += " AND invoiceId = "+filter.getInvoiceId()+" ";
			}
			if(filter.getCustomerId() != null){
				OPEN_ORDERS_SQL += " AND customerId = "+filter.getCustomerId()+" ";
			}
			if(filter.getCustomerName() != null){
				OPEN_ORDERS_SQL += " AND customerName like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getUsername() != null){
				OPEN_ORDERS_SQL += " AND username like '%"+filter.getUsername()+"%' ";
			}
			if(filter.getTicketQty() != null){
				OPEN_ORDERS_SQL += " AND ticketCount = "+filter.getTicketQty()+" ";
			}
			if(filter.getInvoiceTotal() != null){
				OPEN_ORDERS_SQL += " AND invoiceTotal = "+filter.getInvoiceTotal()+" ";
			}
			if(filter.getInvoiceAged() != null){
				OPEN_ORDERS_SQL += " AND invoiceAged = "+filter.getInvoiceAged()+" ";
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(day,createdDate) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(month,createdDate) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(year,createdDate) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getCreatedBy() != null){
				OPEN_ORDERS_SQL += " AND createdBy like '%"+filter.getCreatedBy()+"%' ";
			}
			if(filter.getCsr() != null){
				OPEN_ORDERS_SQL += " AND csr like '%"+filter.getCsr()+"%' ";
			}
			if(filter.getCustomerOrderId() != null){
				OPEN_ORDERS_SQL += " AND customerOrderId = "+filter.getCustomerOrderId()+" ";
			}
			if(filter.getShippingMethod() != null){
				OPEN_ORDERS_SQL += " AND shippingMethod like '%"+filter.getShippingMethod()+"%' ";
			}
			if(filter.getCustomerType() != null){
				OPEN_ORDERS_SQL += " AND customerType like '%"+filter.getCustomerType()+"%' ";
			}
			if(filter.getCompanyName() != null){
				OPEN_ORDERS_SQL += " AND custCompanyName like '%"+filter.getCompanyName()+"%' ";
			}
			if(filter.getProductType() != null){
				OPEN_ORDERS_SQL += " AND productType like '%"+filter.getProductType()+"%' ";
			}
			if(filter.getAddressLine1() != null){
				OPEN_ORDERS_SQL += " AND addressLine1 like '%"+filter.getAddressLine1()+"%' ";
			}
			if(filter.getCity() != null){
				OPEN_ORDERS_SQL += " AND city like '%"+filter.getCity()+"%' ";
			}
			if(filter.getState() != null){
				OPEN_ORDERS_SQL += " AND state like '%"+filter.getState()+"%' ";
			}
			if(filter.getCountry() != null){
				OPEN_ORDERS_SQL += " AND country like '%"+filter.getCountry()+"%' ";
			}
			if(filter.getZipCode() != null){
				OPEN_ORDERS_SQL += " AND zipCode like '%"+filter.getZipCode()+"%' ";
			}
			if(filter.getPhone() != null){
				OPEN_ORDERS_SQL += " AND phone like '%"+filter.getPhone()+"%' ";
			}
			if(filter.getVoidedDateStr() != null){
				if(Util.extractDateElement(filter.getVoidedDateStr(),"DAY") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(day,voidedDate) = "+Util.extractDateElement(filter.getVoidedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getVoidedDateStr(),"MONTH") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(month,voidedDate) = "+Util.extractDateElement(filter.getVoidedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getVoidedDateStr(),"YEAR") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(year,voidedDate) = "+Util.extractDateElement(filter.getVoidedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getStatus() != null){
				OPEN_ORDERS_SQL += " AND status like '%"+filter.getStatus()+"%' ";
			}
			if(filter.getTrackingNo() != null){
				OPEN_ORDERS_SQL += " AND trackingNo = '"+filter.getTrackingNo()+"' ";
			}
			if(filter.getEventId() != null){
				OPEN_ORDERS_SQL += " AND eventId = "+filter.getEventId()+" ";
			}
			if(filter.getEventName() != null){
				OPEN_ORDERS_SQL += " AND eventName like '%"+filter.getEventName()+"%' ";
			}
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(day,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(month,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(year,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventTimeStr() != null){
				if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(hour,eventTime) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(minute,eventTime) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
				}
			}
			if(status!=null && !status.isEmpty()){
				if(status.equalsIgnoreCase("Outstanding")){
					OPEN_ORDERS_SQL += " AND status='Outstanding'";
				}else if(status.equalsIgnoreCase("Completed")){
					OPEN_ORDERS_SQL += " AND status='Completed'";
				}else if(status.equalsIgnoreCase("Voided")){
					OPEN_ORDERS_SQL += " AND status='Voided'";
				}else if(status.equalsIgnoreCase("Disputed")){
					OPEN_ORDERS_SQL += " AND status='Disputed'";
				}
			}
			if(companyProduct!=null && !companyProduct.isEmpty()){
				if(companyProduct.equalsIgnoreCase("RTW")){
					OPEN_ORDERS_SQL += " AND productType='RTW' ";
				}else if (companyProduct.equalsIgnoreCase("RTW2")){
					OPEN_ORDERS_SQL += " AND productType='RTW2' ";
				}
			}
			if(invoiceNo!=null && !invoiceNo.isEmpty()){
				OPEN_ORDERS_SQL += " AND invoiceId="+invoiceNo;
			}
			if(orderNo!=null && !orderNo.isEmpty()){
				OPEN_ORDERS_SQL += " AND customerOrderId="+orderNo;
			}
			/*
			if(csr!=null && !csr.isEmpty()){
				OPEN_ORDERS_SQL += " AND csr like '%"+csr+"%' ";
			}
			if(customer!=null && !customer.isEmpty()){
				OPEN_ORDERS_SQL += " AND customerName like '%"+customer+"%'";
			}			
			if(externalOrderId!=null && !externalOrderId.isEmpty()){
			}
			*/
//			String sortingSql = GridSortingUtil.getInvoicesSortingSql(filter);
			
//			if(sortingSql!=null && !sortingSql.isEmpty()){
//				OPEN_ORDERS_SQL += sortingSql;
//			}else{
//				if(customerId!=null){
//					OPEN_ORDERS_SQL += " AND customerId="+customerId;
//				}
//				
//				OPEN_ORDERS_SQL += " order by invoiceId desc";
//			}
			String sortingSql = GridSortingUtil.getRTWInvoicesSortingSql(filter);
			if(sortingSql!=null && !sortingSql.isEmpty()){
				OPEN_ORDERS_SQL += sortingSql;
			}else{
				OPEN_ORDERS_SQL += " ORDER BY invoiceId desc";
			}
			
			session  = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(OPEN_ORDERS_SQL);
			sqlQuery.addScalar("customerId", Hibernate.INTEGER);
			sqlQuery.addScalar("customerName", Hibernate.STRING);
			sqlQuery.addScalar("username", Hibernate.STRING);
			sqlQuery.addScalar("customerType", Hibernate.STRING);
			sqlQuery.addScalar("productType", Hibernate.STRING);
			sqlQuery.addScalar("invoiceId", Hibernate.INTEGER);
			sqlQuery.addScalar("customerOrderId", Hibernate.INTEGER);
			sqlQuery.addScalar("ticketCount", Hibernate.INTEGER);
			sqlQuery.addScalar("invoiceTotal", Hibernate.DOUBLE);
			sqlQuery.addScalar("addressLine1", Hibernate.STRING);
			sqlQuery.addScalar("city", Hibernate.STRING);
			sqlQuery.addScalar("phone", Hibernate.STRING);
			sqlQuery.addScalar("zipCode", Hibernate.STRING);
			sqlQuery.addScalar("country", Hibernate.STRING);
			sqlQuery.addScalar("state", Hibernate.STRING);
			sqlQuery.addScalar("createdDate", Hibernate.TIMESTAMP);
			sqlQuery.addScalar("createdBy", Hibernate.STRING);
			sqlQuery.addScalar("csr", Hibernate.STRING);
			//sqlQuery.addScalar("lastUpdatedDate", Hibernate.TIMESTAMP);
			//sqlQuery.addScalar("lastUpdatedBy", Hibernate.STRING);
			sqlQuery.addScalar("trackingNo", Hibernate.STRING);
			sqlQuery.addScalar("voidedDate", Hibernate.TIMESTAMP);
			sqlQuery.addScalar("invoiceAged", Hibernate.INTEGER);
			sqlQuery.addScalar("isInvoiceEmailSent", Hibernate.BOOLEAN);
			sqlQuery.addScalar("custCompanyName", Hibernate.STRING);
			sqlQuery.addScalar("shippingMethod", Hibernate.STRING);
			sqlQuery.addScalar("shippingMethodId", Hibernate.INTEGER);
			sqlQuery.addScalar("status", Hibernate.STRING);
			//sqlQuery.addScalar("purchaseOrderNo", Hibernate.STRING);
			//sqlQuery.addScalar("secondaryOrderType", Hibernate.STRING);
			//sqlQuery.addScalar("secondaryOrderId", Hibernate.STRING);
			sqlQuery.addScalar("eventId", Hibernate.INTEGER);
			sqlQuery.addScalar("eventName", Hibernate.STRING);
			sqlQuery.addScalar("eventDate", Hibernate.DATE);
			sqlQuery.addScalar("eventTime", Hibernate.TIME);
			startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
			Collection<OpenOrders> openOrdersList = sqlQuery.setResultTransformer(Transformers.aliasToBean(OpenOrders.class)).
			setFirstResult(startFrom).setMaxResults(PaginationUtil.PAGESIZE).list();
			return openOrdersList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	
	/**
	 * Get list of open orders RTW/RTW2
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<OpenOrders> getRTWOpenOrderListByInvoiceID(String invoiceNo,String productType){
		Session session =null;
		try {
			String OPEN_ORDERS_SQL = "select customerId,customerName,username,customerType,"+
					"productType,invoiceId,custCompanyName,csr,"+
					"invoiceAged,isInvoiceEmailSent,voidedDate,"+
					"trackingNo,shippingMethod,shippingMethodId,customerOrderId,"+
					"invoiceTotal,addressLine1,city,ticketCount,"+
					"zipCode,phone,country,state,createdDate,"+
					"createdBy,status,ticketTotal,"+
					"eventId,eventName,eventDate,"+
					"eventTime"+
					" from pos_invoice_view with(nolock) where productType='"+productType+"' ";
			
			if(invoiceNo!=null && !invoiceNo.isEmpty()){
				OPEN_ORDERS_SQL += " AND invoiceId="+invoiceNo;
			}
			OPEN_ORDERS_SQL += " order by invoiceId desc";
			
			session  = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(OPEN_ORDERS_SQL);
			sqlQuery.addScalar("customerId", Hibernate.INTEGER);
			sqlQuery.addScalar("customerName", Hibernate.STRING);
			sqlQuery.addScalar("username", Hibernate.STRING);
			sqlQuery.addScalar("customerType", Hibernate.STRING);
			sqlQuery.addScalar("productType", Hibernate.STRING);
			sqlQuery.addScalar("invoiceId", Hibernate.INTEGER);
			sqlQuery.addScalar("customerOrderId", Hibernate.INTEGER);
			sqlQuery.addScalar("ticketCount", Hibernate.INTEGER);
			sqlQuery.addScalar("invoiceTotal", Hibernate.DOUBLE);
			sqlQuery.addScalar("addressLine1", Hibernate.STRING);
			sqlQuery.addScalar("city", Hibernate.STRING);
			sqlQuery.addScalar("phone", Hibernate.STRING);
			sqlQuery.addScalar("zipCode", Hibernate.STRING);
			sqlQuery.addScalar("country", Hibernate.STRING);
			sqlQuery.addScalar("state", Hibernate.STRING);
			sqlQuery.addScalar("createdDate", Hibernate.TIMESTAMP);
			sqlQuery.addScalar("createdBy", Hibernate.STRING);
			sqlQuery.addScalar("csr", Hibernate.STRING);
			//sqlQuery.addScalar("lastUpdatedDate", Hibernate.TIMESTAMP);
			//sqlQuery.addScalar("lastUpdatedBy", Hibernate.STRING);
			sqlQuery.addScalar("trackingNo", Hibernate.STRING);
			sqlQuery.addScalar("voidedDate", Hibernate.TIMESTAMP);
			sqlQuery.addScalar("invoiceAged", Hibernate.INTEGER);
			sqlQuery.addScalar("isInvoiceEmailSent", Hibernate.BOOLEAN);
			sqlQuery.addScalar("custCompanyName", Hibernate.STRING);
			sqlQuery.addScalar("shippingMethod", Hibernate.STRING);
			sqlQuery.addScalar("shippingMethodId", Hibernate.INTEGER);
			sqlQuery.addScalar("status", Hibernate.STRING);
			//sqlQuery.addScalar("purchaseOrderNo", Hibernate.STRING);
			//sqlQuery.addScalar("secondaryOrderType", Hibernate.STRING);
			//sqlQuery.addScalar("secondaryOrderId", Hibernate.STRING);
			sqlQuery.addScalar("eventId", Hibernate.INTEGER);
			sqlQuery.addScalar("eventName", Hibernate.STRING);
			sqlQuery.addScalar("eventDate", Hibernate.DATE);
			sqlQuery.addScalar("eventTime", Hibernate.TIME);
			sqlQuery.addScalar("ticketTotal", Hibernate.DOUBLE);
			Collection<OpenOrders> openOrdersList = sqlQuery.setResultTransformer(Transformers.aliasToBean(OpenOrders.class)).list();
			return openOrdersList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	/**
	 * Get list of open orders RTW/RTW2 To Export To Excel
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<OpenOrders> getRTWOpenOrderListToExport(String invoiceNo,String orderNo, String csr,String customer, String fromDate, 
			String toDate,String status,String companyProduct,String externalOrderId,Integer customerId,GridHeaderFilters filter){
		Session session =null;
		try {			
			String OPEN_ORDERS_SQL = "select customerId,customerName,username,customerType,"+
					"productType,invoiceId,custCompanyName,csr,"+
					"invoiceAged,isInvoiceEmailSent,voidedDate,"+
					"trackingNo,shippingMethod,shippingMethodId,customerOrderId,"+
					"invoiceTotal,addressLine1,city,ticketCount,"+
					"zipCode,phone,country,state,createdDate,"+
					"createdBy,status,"+
					"eventId,eventName,eventDate,"+
					"eventTime"+
					" from pos_invoice_view with(nolock) where 1=1";
			
			if(invoiceNo!=null && !invoiceNo.isEmpty()){
				OPEN_ORDERS_SQL += " AND invoiceId="+invoiceNo;
			}
			if(fromDate!=null && !fromDate.isEmpty()){
				OPEN_ORDERS_SQL = OPEN_ORDERS_SQL+" AND createdDate >='"+fromDate+"' ";
			}
			if(toDate!=null && !toDate.isEmpty()){
				OPEN_ORDERS_SQL = OPEN_ORDERS_SQL+" AND createdDate <='"+toDate+"' ";
			}
			if(csr!=null && !csr.isEmpty()){
				OPEN_ORDERS_SQL += " AND csr like '%"+csr+"%' ";
			}
			if(customer!=null && !customer.isEmpty()){
				OPEN_ORDERS_SQL += " AND customerName like '%"+customer+"%'";
			}
			if(orderNo!=null && !orderNo.isEmpty()){
				OPEN_ORDERS_SQL += " AND customerOrderId="+orderNo;
			}
			if(status!=null && !status.isEmpty()){
				if(status.equalsIgnoreCase("Outstanding")){
					OPEN_ORDERS_SQL += " AND status='Outstanding'";
				}else if(status.equalsIgnoreCase("Completed")){
					OPEN_ORDERS_SQL += " AND status='Completed'";
				}else if(status.equalsIgnoreCase("Voided")){
					OPEN_ORDERS_SQL += " AND status='Voided'";
				}
			}
			if(externalOrderId!=null && !externalOrderId.isEmpty()){
			}
			if(companyProduct!=null && !companyProduct.isEmpty()){
				if(companyProduct.equalsIgnoreCase("RTW")){
					OPEN_ORDERS_SQL += " AND productType='RTW' ";
				}else if (companyProduct.equalsIgnoreCase("RTW2")){
					OPEN_ORDERS_SQL += " AND productType='RTW2' ";
				}
			}
			if(customerId!=null){
				OPEN_ORDERS_SQL += " AND customerId="+customerId;
			}
			if(filter.getInvoiceId() != null){
				OPEN_ORDERS_SQL += " AND invoiceId = "+filter.getInvoiceId()+" ";
			}
			if(filter.getCustomerId() != null){
				OPEN_ORDERS_SQL += " AND customerId = "+filter.getCustomerId()+" ";
			}
			if(filter.getCustomerName() != null){
				OPEN_ORDERS_SQL += " AND customerName like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getUsername() != null){
				OPEN_ORDERS_SQL += " AND username like '%"+filter.getUsername()+"%' ";
			}
			if(filter.getTicketQty() != null){
				OPEN_ORDERS_SQL += " AND ticketCount = "+filter.getTicketQty()+" ";
			}
			if(filter.getInvoiceTotal() != null){
				OPEN_ORDERS_SQL += " AND invoiceTotal = "+filter.getInvoiceTotal()+" ";
			}
			if(filter.getInvoiceAged() != null){
				OPEN_ORDERS_SQL += " AND invoiceAged = "+filter.getInvoiceAged()+" ";
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(day,createdDate) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(month,createdDate) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(year,createdDate) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getCreatedBy() != null){
				OPEN_ORDERS_SQL += " AND createdBy like '%"+filter.getCreatedBy()+"%' ";
			}
			if(filter.getCsr() != null){
				OPEN_ORDERS_SQL += " AND csr like '%"+filter.getCsr()+"%' ";
			}
			if(filter.getCustomerOrderId() != null){
				OPEN_ORDERS_SQL += " AND customerOrderId = "+filter.getCustomerOrderId()+" ";
			}
			if(filter.getShippingMethod() != null){
				OPEN_ORDERS_SQL += " AND shippingMethod like '%"+filter.getShippingMethod()+"%' ";
			}
			if(filter.getCustomerType() != null){
				OPEN_ORDERS_SQL += " AND customerType like '%"+filter.getCustomerType()+"%' ";
			}
			if(filter.getCompanyName() != null){
				OPEN_ORDERS_SQL += " AND custCompanyName like '%"+filter.getCompanyName()+"%' ";
			}
			if(filter.getProductType() != null){
				OPEN_ORDERS_SQL += " AND productType like '%"+filter.getProductType()+"%' ";
			}
			if(filter.getAddressLine1() != null){
				OPEN_ORDERS_SQL += " AND addressLine1 like '%"+filter.getAddressLine1()+"%' ";
			}
			if(filter.getCity() != null){
				OPEN_ORDERS_SQL += " AND city like '%"+filter.getCity()+"%' ";
			}
			if(filter.getState() != null){
				OPEN_ORDERS_SQL += " AND state like '%"+filter.getState()+"%' ";
			}
			if(filter.getCountry() != null){
				OPEN_ORDERS_SQL += " AND country like '%"+filter.getCountry()+"%' ";
			}
			if(filter.getZipCode() != null){
				OPEN_ORDERS_SQL += " AND zipCode like '%"+filter.getZipCode()+"%' ";
			}
			if(filter.getPhone() != null){
				OPEN_ORDERS_SQL += " AND phone like '%"+filter.getPhone()+"%' ";
			}
			if(filter.getVoidedDateStr() != null){
				if(Util.extractDateElement(filter.getVoidedDateStr(),"DAY") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(day,voidedDate) = "+Util.extractDateElement(filter.getVoidedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getVoidedDateStr(),"MONTH") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(month,voidedDate) = "+Util.extractDateElement(filter.getVoidedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getVoidedDateStr(),"YEAR") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(year,voidedDate) = "+Util.extractDateElement(filter.getVoidedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getStatus() != null){
				OPEN_ORDERS_SQL += " AND status like '%"+filter.getStatus()+"%' ";
			}
			if(filter.getTrackingNo() != null){
				OPEN_ORDERS_SQL += " AND trackingNo = '"+filter.getTrackingNo()+"' ";
			}
			if(filter.getEventId() != null){
				OPEN_ORDERS_SQL += " AND eventId = "+filter.getEventId()+" ";
			}
			if(filter.getEventName() != null){
				OPEN_ORDERS_SQL += " AND eventName like '%"+filter.getEventName()+"%' ";
			}
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(day,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(month,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(year,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventTimeStr() != null){
				if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(hour,eventTime) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(minute,eventTime) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
				}
			}
			OPEN_ORDERS_SQL += " order by invoiceId desc";
			
			session  = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(OPEN_ORDERS_SQL);
			sqlQuery.addScalar("customerId", Hibernate.INTEGER);
			sqlQuery.addScalar("customerName", Hibernate.STRING);
			sqlQuery.addScalar("username", Hibernate.STRING);
			sqlQuery.addScalar("customerType", Hibernate.STRING);
			sqlQuery.addScalar("productType", Hibernate.STRING);
			sqlQuery.addScalar("invoiceId", Hibernate.INTEGER);
			sqlQuery.addScalar("customerOrderId", Hibernate.INTEGER);
			sqlQuery.addScalar("ticketCount", Hibernate.INTEGER);
			sqlQuery.addScalar("invoiceTotal", Hibernate.DOUBLE);
			sqlQuery.addScalar("addressLine1", Hibernate.STRING);
			sqlQuery.addScalar("city", Hibernate.STRING);
			sqlQuery.addScalar("phone", Hibernate.STRING);
			sqlQuery.addScalar("zipCode", Hibernate.STRING);
			sqlQuery.addScalar("country", Hibernate.STRING);
			sqlQuery.addScalar("state", Hibernate.STRING);
			sqlQuery.addScalar("createdDate", Hibernate.TIMESTAMP);
			sqlQuery.addScalar("createdBy", Hibernate.STRING);
			sqlQuery.addScalar("csr", Hibernate.STRING);
			//sqlQuery.addScalar("lastUpdatedDate", Hibernate.TIMESTAMP);
			//sqlQuery.addScalar("lastUpdatedBy", Hibernate.STRING);
			sqlQuery.addScalar("trackingNo", Hibernate.STRING);
			sqlQuery.addScalar("voidedDate", Hibernate.TIMESTAMP);
			sqlQuery.addScalar("invoiceAged", Hibernate.INTEGER);
			sqlQuery.addScalar("isInvoiceEmailSent", Hibernate.BOOLEAN);
			sqlQuery.addScalar("custCompanyName", Hibernate.STRING);
			sqlQuery.addScalar("shippingMethod", Hibernate.STRING);
			sqlQuery.addScalar("shippingMethodId", Hibernate.INTEGER);
			sqlQuery.addScalar("status", Hibernate.STRING);
			//sqlQuery.addScalar("purchaseOrderNo", Hibernate.STRING);
			//sqlQuery.addScalar("secondaryOrderType", Hibernate.STRING);
			//sqlQuery.addScalar("secondaryOrderId", Hibernate.STRING);
			sqlQuery.addScalar("eventId", Hibernate.INTEGER);
			sqlQuery.addScalar("eventName", Hibernate.STRING);
			sqlQuery.addScalar("eventDate", Hibernate.DATE);
			sqlQuery.addScalar("eventTime", Hibernate.TIME);
			Collection<OpenOrders> openOrdersList = sqlQuery.setResultTransformer(Transformers.aliasToBean(OpenOrders.class)).setMaxResults(65535).list();
			return openOrdersList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	/*
	@SuppressWarnings("unchecked")
	public List<CustomerAddress> getRTWCustomerAddressDetails(String invoiceId){
		Session session = null;
		try{
			String customerAddress_SQL = "select * from customer_address ca "+
						"join pos_customer_order pco on (ca.id = pco.billing_address_id or ca.id = pco.shipping_address_id)"+
						" join pos_invoice_view piv on pco.ticket_request_id = piv.customerOrderId"+
						" and piv.invoiceId = "+invoiceId;
			session  = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(customerAddress_SQL);
			List<CustomerAddress> CustomerAddressList = sqlQuery.setResultTransformer(Transformers.aliasToBean(CustomerAddress.class)).list();
			return CustomerAddressList;			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<CustomerAddress> getRTWRTW2POCustomerAddressDetails(Integer customerId, String productType){
		Session session = null;
		String customerAddress_SQL = "";
		try{ 
			if(productType.equalsIgnoreCase("RTW")){
				customerAddress_SQL = "select distinct id, address_line1,address_line2, address_type, city, country, state, zip_code, first_name, last_name, phone1,email, status from customer_address ca "+
						"left join pos_purchase_order po on ca.rtw_customer_id = po.client_broker_employee_id "+
						" where po.client_broker_employee_id = "+customerId+" and ca.address_type = 'MAIN_ADDRESS'";
			}else if(productType.equalsIgnoreCase("RTW2")){
				customerAddress_SQL = "select distinct id, address_line1,address_line2, address_type, city, country, state, zip_code, first_name, last_name, phone1,email, status from customer_address ca "+
					"left join pos_purchase_order po on ca.rtw2_customer_id = po.client_broker_employee_id "+
					" where po.client_broker_employee_id = "+customerId+" and ca.address_type = 'MAIN_ADDRESS'";
			}
			session  = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(customerAddress_SQL);
			List<CustomerAddress> CustomerAddressList = sqlQuery.setResultTransformer(Transformers.aliasToBean(CustomerAddress.class)).list();
			return CustomerAddressList;			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}*/
	/*
	 * Invoice - RTW/RTW2 - Total Count of records
	 */
	@SuppressWarnings("unchecked")
	public Integer getRTWOpenOrderListCount(String fromDate,String toDate,GridHeaderFilters filter,String orderNo, String invoiceNo,String status, String companyProduct,Integer customerId){
		int count = 0;		
		Session session =null;
		try {
			String OPEN_ORDERS_SQL = "select count(*) as cnt"+
					" from pos_invoice_view with(nolock) where 1=1";
						
			if(fromDate!=null && !fromDate.isEmpty()){
				OPEN_ORDERS_SQL = OPEN_ORDERS_SQL+" AND createdDate >='"+fromDate+"' ";
			}
			if(toDate!=null && !toDate.isEmpty()){
				OPEN_ORDERS_SQL = OPEN_ORDERS_SQL+" AND createdDate <='"+toDate+"' ";
			}
			if(filter.getInvoiceId() != null){
				OPEN_ORDERS_SQL += " AND invoiceId = "+filter.getInvoiceId()+" ";
			}
			if(filter.getCustomerId() != null){
				OPEN_ORDERS_SQL += " AND customerId = "+filter.getCustomerId()+" ";
			}
			if(filter.getCustomerName() != null){
				OPEN_ORDERS_SQL += " AND customerName like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getUsername() != null){
				OPEN_ORDERS_SQL += " AND username like '%"+filter.getUsername()+"%' ";
			}
			if(filter.getTicketQty() != null){
				OPEN_ORDERS_SQL += " AND ticketCount = "+filter.getTicketQty()+" ";
			}
			if(filter.getInvoiceTotal() != null){
				OPEN_ORDERS_SQL += " AND invoiceTotal = "+filter.getInvoiceTotal()+" ";
			}
			if(filter.getInvoiceAged() != null){
				OPEN_ORDERS_SQL += " AND invoiceAged = "+filter.getInvoiceAged()+" ";
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(day,createdDate) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(month,createdDate) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(year,createdDate) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getCreatedBy() != null){
				OPEN_ORDERS_SQL += " AND createdBy like '%"+filter.getCreatedBy()+"%' ";
			}
			if(filter.getCsr() != null){
				OPEN_ORDERS_SQL += " AND csr like '%"+filter.getCsr()+"%' ";
			}
			if(filter.getCustomerOrderId() != null){
				OPEN_ORDERS_SQL += " AND customerOrderId = "+filter.getCustomerOrderId()+" ";
			}
			if(filter.getShippingMethod() != null){
				OPEN_ORDERS_SQL += " AND shippingMethod like '%"+filter.getShippingMethod()+"%' ";
			}
			if(filter.getCustomerType() != null){
				OPEN_ORDERS_SQL += " AND customerType like '%"+filter.getCustomerType()+"%' ";
			}
			if(filter.getCompanyName() != null){
				OPEN_ORDERS_SQL += " AND custCompanyName like '%"+filter.getCompanyName()+"%' ";
			}
			if(filter.getProductType() != null){
				OPEN_ORDERS_SQL += " AND productType like '%"+filter.getProductType()+"%' ";
			}
			if(filter.getAddressLine1() != null){
				OPEN_ORDERS_SQL += " AND addressLine1 like '%"+filter.getAddressLine1()+"%' ";
			}
			if(filter.getCity() != null){
				OPEN_ORDERS_SQL += " AND city like '%"+filter.getCity()+"%' ";
			}
			if(filter.getState() != null){
				OPEN_ORDERS_SQL += " AND state like '%"+filter.getState()+"%' ";
			}
			if(filter.getCountry() != null){
				OPEN_ORDERS_SQL += " AND country like '%"+filter.getCountry()+"%' ";
			}
			if(filter.getZipCode() != null){
				OPEN_ORDERS_SQL += " AND zipCode like '%"+filter.getZipCode()+"%' ";
			}
			if(filter.getPhone() != null){
				OPEN_ORDERS_SQL += " AND phone like '%"+filter.getPhone()+"%' ";
			}
			if(filter.getVoidedDateStr() != null){
				if(Util.extractDateElement(filter.getVoidedDateStr(),"DAY") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(day,voidedDate) = "+Util.extractDateElement(filter.getVoidedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getVoidedDateStr(),"MONTH") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(month,voidedDate) = "+Util.extractDateElement(filter.getVoidedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getVoidedDateStr(),"YEAR") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(year,voidedDate) = "+Util.extractDateElement(filter.getVoidedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getStatus() != null){
				OPEN_ORDERS_SQL += " AND status like '%"+filter.getStatus()+"%' ";
			}
			if(filter.getTrackingNo() != null){
				OPEN_ORDERS_SQL += " AND trackingNo = '"+filter.getTrackingNo()+"' ";
			}
			if(filter.getEventId() != null){
				OPEN_ORDERS_SQL += " AND eventId = "+filter.getEventId()+" ";
			}
			if(filter.getEventName() != null){
				OPEN_ORDERS_SQL += " AND eventName like '%"+filter.getEventName()+"%' ";
			}
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(day,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(month,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(year,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventTimeStr() != null){
				if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(hour,eventTime) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
					OPEN_ORDERS_SQL += " AND DATEPART(minute,eventTime) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
				}
			}
			if(status!=null && !status.isEmpty()){
				if(status.equalsIgnoreCase("Outstanding")){
					OPEN_ORDERS_SQL += " AND status='Outstanding'";
				}else if(status.equalsIgnoreCase("Completed")){
					OPEN_ORDERS_SQL += " AND status='Completed'";
				}else if(status.equalsIgnoreCase("Voided")){
					OPEN_ORDERS_SQL += " AND status='Voided'";
				}else if(status.equalsIgnoreCase("Disputed")){
					OPEN_ORDERS_SQL += " AND status='Disputed'";
				}
			}
			if(companyProduct!=null && !companyProduct.isEmpty()){
				if(companyProduct.equalsIgnoreCase("RTW")){
					OPEN_ORDERS_SQL += " AND productType='RTW' ";
				}else if (companyProduct.equalsIgnoreCase("RTW2")){
					OPEN_ORDERS_SQL += " AND productType='RTW2' ";
				}
			}
			if(invoiceNo!=null && !invoiceNo.isEmpty()){
				OPEN_ORDERS_SQL += " AND invoiceId="+invoiceNo;
			}
			if(orderNo!=null && !orderNo.isEmpty()){
				OPEN_ORDERS_SQL += " AND customerOrderId="+orderNo;
			}
			/*
			if(csr!=null && !csr.isEmpty()){
				OPEN_ORDERS_SQL += " AND csr like '%"+csr+"%' ";
			}
			if(customer!=null && !customer.isEmpty()){
				OPEN_ORDERS_SQL += " AND customerName like '%"+customer+"%'";
			}
			if(externalOrderId!=null && !externalOrderId.isEmpty()){
			}
			*/
			if(customerId!=null){
				OPEN_ORDERS_SQL += " AND customerId="+customerId;
			}
			
			session  = sessionFactory.openSession();
			
			Query query = session.createSQLQuery(OPEN_ORDERS_SQL);
			count = Integer.valueOf((query.uniqueResult()).toString());
			return count;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	
	/**
	 * Get list of open orders
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<OpenOrders> getOpenOrderListByInvoice(Integer invoiceId, Integer brokerId){
		Session session =null;
		try {
			String OPEN_ORDERS_SQL = "select cust.id as customerId, cust.cust_name as customerName, cust.last_name as lastName,"+
					"cust.username as username," +
					"cust.customer_type as customerType, " +
					"cust.product_type as productType, " +
					"inv.id as invoiceId , " +
					"inv.order_id as customerOrderId, " +
					"inv.ticket_count as ticketCount, " +
					"inv.invoice_total as invoiceTotal, " +
					"ca.address_line1 as addressLine1, " +
					"ca.city as city, " +
					"ca.zip_code as zipCode, " +
					"cust.phone as phone, " +
				    "ct.country_name as country, " +
					"s.name as state, " +
					"e.event_id as eventId," +
					"e.event_name as eventName, " +
					"e.event_date as eventDate, " +
					"e.event_time as eventTime, " +
					"ctg.section as section, " +
					"ctg.row as row, " +
					"ctg.quantity as qty, " +
					"ctg.price as price, " +
					"ctg.sold_price as soldPrice, " +
					"ctg.row_range as rowRange, " +
					"ctg.section_range as sectionRange, " +
					"sm.name as shippingMethod, " +
					"inv.shipping_method_id as shippingMethodId, "+
					"inv.realtix_shipping_method as realTixShippingMethod,inv.realtix_map as realTixMap,inv.realtix_delivered as realTixDelivered " +
					"from invoice inv with(nolock) " +
					"inner join customer cust with(nolock) on(cust.id=inv.customer_id) "+
					"left join category_ticket_group ctg with(nolock) on(ctg.invoice_id=inv.id) "+
					"left join event_details e with(nolock) on(e.event_id=ctg.event_id) "+
					"left join customer_address ca with(nolock) on(ca.customer_id = cust.id and ca.address_type='BILLING_ADDRESS') "+
					"left join country ct with(nolock) on(ct.id=ca.country) "+
					"left join state s with(nolock) on(s.id=ca.state) "+
					"left join shipping_method sm with(nolock) on(inv.shipping_method_id=sm.id) "+
					"where inv.id = :invoiceId ";
			
			if(brokerId != null && brokerId > 0){
				OPEN_ORDERS_SQL += " AND inv.broker_id = "+brokerId;
			}
			session  = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(OPEN_ORDERS_SQL);
			sqlQuery.addScalar("customerId", Hibernate.INTEGER);
			sqlQuery.addScalar("customerName", Hibernate.STRING);
			sqlQuery.addScalar("lastName", Hibernate.STRING);
			sqlQuery.addScalar("username", Hibernate.STRING);
			sqlQuery.addScalar("customerType", Hibernate.STRING);
			sqlQuery.addScalar("productType", Hibernate.STRING);
			sqlQuery.addScalar("invoiceId", Hibernate.INTEGER);
			sqlQuery.addScalar("customerOrderId", Hibernate.INTEGER);
			sqlQuery.addScalar("ticketCount", Hibernate.INTEGER);
			sqlQuery.addScalar("invoiceTotal", Hibernate.DOUBLE);
			sqlQuery.addScalar("addressLine1", Hibernate.STRING);
			sqlQuery.addScalar("city", Hibernate.STRING);
			sqlQuery.addScalar("phone", Hibernate.STRING);
			sqlQuery.addScalar("zipCode", Hibernate.STRING);
			sqlQuery.addScalar("country", Hibernate.STRING);
			sqlQuery.addScalar("state", Hibernate.STRING);
			sqlQuery.addScalar("eventId", Hibernate.INTEGER);
			sqlQuery.addScalar("eventName", Hibernate.STRING);
			sqlQuery.addScalar("eventDate", Hibernate.DATE);
			sqlQuery.addScalar("eventTime", Hibernate.TIME);
			sqlQuery.addScalar("section", Hibernate.STRING);
			sqlQuery.addScalar("row", Hibernate.STRING);
			sqlQuery.addScalar("qty", Hibernate.INTEGER);
			sqlQuery.addScalar("price", Hibernate.DOUBLE);
			sqlQuery.addScalar("soldPrice", Hibernate.DOUBLE);
			sqlQuery.addScalar("rowRange", Hibernate.STRING);
			sqlQuery.addScalar("sectionRange", Hibernate.STRING);
			sqlQuery.addScalar("shippingMethod", Hibernate.STRING);
			sqlQuery.addScalar("shippingMethodId", Hibernate.INTEGER);
			sqlQuery.addScalar("realTixShippingMethod", Hibernate.STRING);
			sqlQuery.addScalar("realTixMap", Hibernate.STRING);
			sqlQuery.addScalar("realTixDelivered", Hibernate.BOOLEAN);
			
			List<OpenOrders> openOrdersList = sqlQuery.setResultTransformer(Transformers.aliasToBean(OpenOrders.class))
													.setParameter("invoiceId", invoiceId)
													.list();
			return openOrdersList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	/**
	 * Get list of closed orders
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<OpenOrders> getClosedOrdersList(){
		Session session =null;
		try {
			String OPEN_ORDERS_SQL = "select cust.id as customerId,cust.cust_name as customerName,cust.username as username," +
					"cust.customer_type as customerType, " +
					"cust.product_type as productType, " +
					"inv.id as invoiceId , " +
					"inv.order_id as customerOrderId, " +
					"inv.ticket_count as ticketCount, " +
					"inv.invoice_total as invoiceTotal, " +
					"ca.address_line1 as addressLine1, " +
					"ca.city as city, " +
					"ca.zip_code as zipCode, " +
					"cust.phone as phone, " +
				    "ct.country_name as country, " +
					"s.name as state, " +
					"e.event_id as eventId," +
					"e.event_name as eventName, " +
					"e.event_date as eventDate, " +
					"e.event_time as eventTime, " +
					"ctg.section as section, " +
					"ctg.row as row, " +
					"ctg.quantity as qty, " +
					"ctg.price as price, " +
					"ctg.row_range as rowRange, " +
					"ctg.section_range as sectionRange, " +
					"ctg.shipping_method as shippingMethod " +
					"from invoice inv with(nolock) " +
					"inner join customer cust with(nolock) on(cust.id=inv.customer_id) "+
					"inner join category_ticket_group ctg with(nolock) on(ctg.invoice_id=inv.id) "+
					"inner join event_details e with(nolock) on(e.event_id=ctg.event_id) "+
					"left join customer_address ca with(nolock) on(ca.customer_id = cust.id and ca.address_type='BILLING_ADDRESS') "+
					"left join country ct with(nolock) on(ct.id=ca.country) "+
					"left join state s with(nolock) on(s.id=ca.state) "+
					"where realtix_map = 'YES' and inv.status='ACTIVE'";
			session  = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(OPEN_ORDERS_SQL);
			sqlQuery.addScalar("customerId", Hibernate.INTEGER);
			sqlQuery.addScalar("customerName", Hibernate.STRING);
			sqlQuery.addScalar("username", Hibernate.STRING);
			sqlQuery.addScalar("customerType", Hibernate.STRING);
			sqlQuery.addScalar("productType", Hibernate.STRING);
			sqlQuery.addScalar("invoiceId", Hibernate.INTEGER);
			sqlQuery.addScalar("customerOrderId", Hibernate.INTEGER);
			sqlQuery.addScalar("ticketCount", Hibernate.INTEGER);
			sqlQuery.addScalar("invoiceTotal", Hibernate.DOUBLE);
			sqlQuery.addScalar("addressLine1", Hibernate.STRING);
			sqlQuery.addScalar("city", Hibernate.STRING);
			sqlQuery.addScalar("phone", Hibernate.STRING);
			sqlQuery.addScalar("zipCode", Hibernate.STRING);
			sqlQuery.addScalar("country", Hibernate.STRING);
			sqlQuery.addScalar("state", Hibernate.STRING);
			sqlQuery.addScalar("eventId", Hibernate.INTEGER);
			sqlQuery.addScalar("eventName", Hibernate.STRING);
			sqlQuery.addScalar("eventDate", Hibernate.DATE);
			sqlQuery.addScalar("eventTime", Hibernate.TIME);
			sqlQuery.addScalar("section", Hibernate.STRING);
			sqlQuery.addScalar("row", Hibernate.STRING);
			sqlQuery.addScalar("qty", Hibernate.INTEGER);
			sqlQuery.addScalar("price", Hibernate.DOUBLE);
			sqlQuery.addScalar("rowRange", Hibernate.STRING);
			sqlQuery.addScalar("sectionRange", Hibernate.STRING);
			sqlQuery.addScalar("shippingMethod", Hibernate.STRING);
			
			Collection<OpenOrders> openOrdersList = sqlQuery.setResultTransformer(Transformers.aliasToBean(OpenOrders.class))
													.list();
			return openOrdersList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	/**
	 * Get list of invoices for customer
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<CustomerInvoices> getInvoicesForCustomer(Integer customerId, GridHeaderFilters filter){
		Session session =null;
		try {
			
			String SQL_CUSTOMER_INVOICE = "select c.id as customerId,inv.id as invoiceId,inv.invoice_total as invoiceTotal," +
										//"inv.ext_po_no as extNo, " +
										"inv.invoice_type as invoiceType, " +
										"inv.created_date as createdDate, " +
										"inv.created_by as createdBy, " +
										"inv.ticket_count as ticketCount, "+
										"inv.status as status, "+
										"co.product_type as productType, "+
										"co.order_type as orderType "+
										"from customer c with(nolock) " +
										"inner join invoice inv with(nolock) on(inv.customer_id=c.id) " +
										"inner join customer_order co with(nolock) on co.id=inv.order_id "+
										"where inv.customer_id=:customerId";
			
			if(filter.getInvoiceId() != null){
				SQL_CUSTOMER_INVOICE += " AND inv.id = "+filter.getInvoiceId()+" ";
			}
			if(filter.getOrderType() != null && !filter.getOrderType().isEmpty()){
				SQL_CUSTOMER_INVOICE += " AND co.order_type = '"+filter.getOrderType()+"' ";
			}
			if(filter.getInvoiceTotal() != null){
				SQL_CUSTOMER_INVOICE += " AND inv.invoice_total = "+filter.getInvoiceTotal()+" ";
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					SQL_CUSTOMER_INVOICE += " AND DATEPART(day,inv.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					SQL_CUSTOMER_INVOICE += " AND DATEPART(month,inv.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					SQL_CUSTOMER_INVOICE += " AND DATEPART(year,inv.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getCreatedBy() != null){
				SQL_CUSTOMER_INVOICE += " AND inv.created_by like '%"+filter.getCreatedBy()+"%' ";
			}
			if(filter.getTicketQty() != null){
				SQL_CUSTOMER_INVOICE += " AND inv.ticket_count = "+filter.getTicketQty()+" ";
			}
			if(filter.getStatus() != null){
				SQL_CUSTOMER_INVOICE += " AND inv.status= '"+filter.getStatus()+"' ";
			}
			if(filter.getInvoiceType() != null){
				if(filter.getInvoiceType().equalsIgnoreCase("REGULAR")){
					SQL_CUSTOMER_INVOICE += " AND inv.invoice_type= 'INVOICE' ";
				}else{
					SQL_CUSTOMER_INVOICE += " AND inv.invoice_type= '"+filter.getInvoiceType()+"' ";
				}
			}
			if(filter.getProductType() != null){
				SQL_CUSTOMER_INVOICE += " AND co.product_type like '%"+filter.getProductType()+"%' ";
			}
			session  = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(SQL_CUSTOMER_INVOICE);
			sqlQuery.addScalar("customerId", Hibernate.INTEGER);
			sqlQuery.addScalar("invoiceId", Hibernate.INTEGER);
			sqlQuery.addScalar("invoiceTotal", Hibernate.DOUBLE);
			//sqlQuery.addScalar("extNo", Hibernate.INTEGER);
			sqlQuery.addScalar("invoiceType", Hibernate.STRING);
			sqlQuery.addScalar("createdDate", Hibernate.DATE);
			sqlQuery.addScalar("createdBy", Hibernate.STRING);
			sqlQuery.addScalar("ticketCount", Hibernate.INTEGER);
			sqlQuery.addScalar("productType", Hibernate.STRING);
			sqlQuery.addScalar("orderType", Hibernate.STRING);
			sqlQuery.addScalar("status", Hibernate.STRING);
			
			Collection<CustomerInvoices> custInvoicesList = sqlQuery.setResultTransformer(Transformers.aliasToBean(CustomerInvoices.class))
															.setParameter("customerId", customerId)
															.list();
			return custInvoicesList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	/**
	 * Get list of open orders REWARDTHEFAN Invoices for Customer
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<CustomerInvoices> getRTFInvoicesForCustomer(Integer customerId, String productType, GridHeaderFilters filter){
		Session session =null;
		try {
			String SQL_CUSTOMER_INVOICE = "select customerId, productType, invoiceId, invoiceTotal, ticketCount,createdDate, createdBy, status "+ 
						"from pos_invoice_view with(nolock) ";			
			
			if(customerId!=null){
				SQL_CUSTOMER_INVOICE += " WHERE customerId = "+customerId+" ";
			}
			if(productType!=null && !productType.isEmpty()){
				SQL_CUSTOMER_INVOICE += " AND productType = '"+productType+"' ";
			}
			if(filter.getInvoiceId() != null){
				SQL_CUSTOMER_INVOICE += " AND invoiceId= "+filter.getInvoiceId()+" ";
			}
			if(filter.getStatus() != null){
				SQL_CUSTOMER_INVOICE += " AND status= '"+filter.getStatus()+"' ";
			}
			if(filter.getInvoiceType() != null && !filter.getInvoiceType().equalsIgnoreCase("REGULAR")){
				SQL_CUSTOMER_INVOICE += " AND status= '"+filter.getInvoiceType()+"' ";
			}
			if(filter.getInvoiceTotal() != null){
				SQL_CUSTOMER_INVOICE += " AND invoiceTotal = "+filter.getInvoiceTotal()+" ";
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					SQL_CUSTOMER_INVOICE += " AND DATEPART(day,createdDate) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					SQL_CUSTOMER_INVOICE += " AND DATEPART(month,createdDate) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					SQL_CUSTOMER_INVOICE += " AND DATEPART(year,createdDate) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getCreatedBy() != null){
				SQL_CUSTOMER_INVOICE += " AND createdBy like '%"+filter.getCreatedBy()+"%' ";
			}
			if(filter.getTicketQty() != null){
				SQL_CUSTOMER_INVOICE += " AND ticketCount = "+filter.getTicketQty()+" ";
			}
			/*if(filter.getExternalNo() != null){
				SQL_CUSTOMER_INVOICE += " AND extNo = "+filter.getExternalNo()+" ";
			}*/
			if(filter.getProductType() != null && !filter.getProductType().isEmpty()){
				SQL_CUSTOMER_INVOICE += " AND productType like '%"+filter.getProductType()+"%' ";
			}
			SQL_CUSTOMER_INVOICE += " order by invoiceId";
			
			session  = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(SQL_CUSTOMER_INVOICE);
			sqlQuery.addScalar("customerId", Hibernate.INTEGER);
			sqlQuery.addScalar("invoiceId", Hibernate.INTEGER);
			sqlQuery.addScalar("invoiceTotal", Hibernate.DOUBLE);
			//sqlQuery.addScalar("extNo", Hibernate.INTEGER);
			sqlQuery.addScalar("createdDate", Hibernate.DATE);
			sqlQuery.addScalar("createdBy", Hibernate.STRING);
			sqlQuery.addScalar("ticketCount", Hibernate.INTEGER);
			sqlQuery.addScalar("productType", Hibernate.STRING);
			sqlQuery.addScalar("status", Hibernate.STRING);
			Collection<CustomerInvoices> custInvoicesList = sqlQuery.setResultTransformer(Transformers.aliasToBean(CustomerInvoices.class))
											.list();
			return custInvoicesList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	/**
	 * Get list of open orders RTW/RTW2 Invoices for Customer
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<CustomerInvoices> getRTWInvoicesForCustomer(Integer customerId, GridHeaderFilters filter){
		Session session =null;
		try {
			String SQL_CUSTOMER_INVOICE = "select * from (select c.id as customerId, co.product_type as productType, "+
						"inv.id as invoiceId, inv.invoice_total as invoiceTotal, co.order_type AS orderType, "+
						"inv.ticket_count as ticketCount, "+ 
						"inv.created_date as createdDate, inv.created_by as createdBy, inv.status as status from customer c WITH(NOLOCK) "+
						"inner join invoice inv WITH(NOLOCK) on(inv.customer_id=c.id) "+
						"inner join customer_order co WITH(NOLOCK) on co.id=inv.order_id where 1 = 1 "+
						"union all "+
						"select customerId, productType, invoiceId, invoiceTotal ,'' AS orderType,ticketCount,createdDate, createdBy, status "+ 
						"from pos_invoice_view WITH(NOLOCK) where 1=1) A ";				
			

			if(customerId!=null){
				SQL_CUSTOMER_INVOICE += " WHERE A.customerId = "+customerId+" ";
			}
			if(filter.getOrderType() != null){
				SQL_CUSTOMER_INVOICE += " AND co.order_type= '"+filter.getOrderType()+"' AND A.productType='1' ";
			}
			if(filter.getInvoiceId() != null){
				SQL_CUSTOMER_INVOICE += " AND A.invoiceId= "+filter.getInvoiceId()+" ";
			}
			if(filter.getInvoiceTotal() != null){
				SQL_CUSTOMER_INVOICE += " AND A.invoiceTotal = "+filter.getInvoiceTotal()+" ";
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					SQL_CUSTOMER_INVOICE += " AND DATEPART(day,A.createdDate) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					SQL_CUSTOMER_INVOICE += " AND DATEPART(month,A.createdDate) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					SQL_CUSTOMER_INVOICE += " AND DATEPART(year,A.createdDate) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getCreatedBy() != null){
				SQL_CUSTOMER_INVOICE += " AND A.createdBy like '%"+filter.getCreatedBy()+"%' ";
			}
			if(filter.getTicketQty() != null){
				SQL_CUSTOMER_INVOICE += " AND A.ticketCount = "+filter.getTicketQty()+" ";
			}
			/*if(filter.getExternalNo() != null){
				SQL_CUSTOMER_INVOICE += " AND A.extNo = "+filter.getExternalNo()+" ";
			}*/
			if(filter.getProductType() != null){
				SQL_CUSTOMER_INVOICE += " AND A.productType like '%"+filter.getProductType()+"%' ";
			}
			SQL_CUSTOMER_INVOICE += " order by A.invoiceId";
			
			session  = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(SQL_CUSTOMER_INVOICE);
			sqlQuery.addScalar("customerId", Hibernate.INTEGER);
			sqlQuery.addScalar("invoiceId", Hibernate.INTEGER);
			sqlQuery.addScalar("invoiceTotal", Hibernate.DOUBLE);
			//sqlQuery.addScalar("extNo", Hibernate.INTEGER);
			sqlQuery.addScalar("createdDate", Hibernate.DATE);
			sqlQuery.addScalar("createdBy", Hibernate.STRING);
			sqlQuery.addScalar("ticketCount", Hibernate.INTEGER);
			sqlQuery.addScalar("productType", Hibernate.STRING);
			sqlQuery.addScalar("orderType", Hibernate.STRING);
			sqlQuery.addScalar("status", Hibernate.STRING);
			
			Collection<CustomerInvoices> custInvoicesList = sqlQuery.setResultTransformer(Transformers.aliasToBean(CustomerInvoices.class))
											.list();
			return custInvoicesList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public Collection<PurchaseOrders> getPOForCustomer(Integer customerId, GridHeaderFilters filter){
		Session session =null;
		try {
			String SQL_PO_LIST = "select po.id as id, po.customer_id as customerId, c.cust_name as customerName, c.customer_type as customerType, "
					+ "po.created_time as created, "
					+ "po.po_total as poTotal, "
					+ "po.ticket_count as ticketQty, "
					+ "sm.name as shippingType, "
					+ "po.status as status, "
					+ "c.product_type as productType "
					+ "from purchase_order po with(nolock) "
					+ "inner join customer c with(nolock) on(c.id=po.customer_id) "
					+ "left join shipping_method sm with(nolock) on(po.shipping_method_id=sm.id) "
					+ "where po.status='ACTIVE' ";
			
			if(filter.getPurchaseOrderId() != null){
				SQL_PO_LIST += " AND po.id = "+filter.getPurchaseOrderId()+" ";
			}
			if(filter.getCustomerName() != null){
				SQL_PO_LIST += " AND c.cust_name like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getCustomerType() != null){
				SQL_PO_LIST += " AND c.customer_type like '%"+filter.getCustomerType()+"%' ";
			}
			if(filter.getPoTotal() != null){
				SQL_PO_LIST += " AND po.po_total = "+filter.getPoTotal()+" ";
			}
			if(filter.getTicketQty() != null){
				SQL_PO_LIST += " AND po.ticket_count = "+filter.getTicketQty()+" ";
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					SQL_PO_LIST += " AND DATEPART(day,po.created_time) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					SQL_PO_LIST += " AND DATEPART(month,po.created_time) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					SQL_PO_LIST += " AND DATEPART(year,po.created_time) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getShippingType() != null){
				SQL_PO_LIST += " AND sm.name like '%"+filter.getShippingType()+"%' ";
			}
			if(filter.getStatus() != null){
				SQL_PO_LIST += " AND po.status like '%"+filter.getStatus()+"%' ";
			}
			if(filter.getProductType() != null){
				SQL_PO_LIST += " AND c.product_type like '%"+filter.getProductType()+"%' ";	
			}
			if(customerId != null){
				SQL_PO_LIST += " AND c.id = "+customerId+" ";
			}
			session  = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(SQL_PO_LIST);
			sqlQuery.addScalar("id", Hibernate.INTEGER);
			sqlQuery.addScalar("customerId", Hibernate.INTEGER);
			sqlQuery.addScalar("customerName", Hibernate.STRING);
			sqlQuery.addScalar("created", Hibernate.DATE);
			sqlQuery.addScalar("poTotal", Hibernate.DOUBLE);
			sqlQuery.addScalar("ticketQty", Hibernate.INTEGER);
			sqlQuery.addScalar("shippingType", Hibernate.STRING);
			sqlQuery.addScalar("status", Hibernate.STRING);
			sqlQuery.addScalar("productType", Hibernate.STRING);
			Collection<PurchaseOrders> poListForCustomer = sqlQuery.setResultTransformer(Transformers.aliasToBean(PurchaseOrders.class))
												.list();
			return poListForCustomer;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	

	/*
	 * REWARDTHEFAN PO for Customer
	 */
	@SuppressWarnings("unchecked")
	public Collection<PurchaseOrders> getRTFPOForCustomer(Integer customerId, String productType, GridHeaderFilters filter){
		Session session =null;
		try {
			String SQL_PO_LIST = "select distinct id, customerId, customerName, customerType, created, poTotal, ticketQty, shippingType, product_type as productType, '' as status "+
					 "from pos_purchase_order_view with(nolock) ";
			
			if(customerId != null){
				SQL_PO_LIST += " WHERE customerId = "+customerId+" ";
			}
			if(productType != null && !productType.isEmpty()){
				SQL_PO_LIST += " AND product_type = '"+productType+"' ";	
			}
			if(filter.getPurchaseOrderId() != null){
				SQL_PO_LIST += " AND id = "+filter.getPurchaseOrderId()+" ";
			}
			if(filter.getCustomerName() != null){
				SQL_PO_LIST += " AND customerName like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getCustomerType() != null){
				SQL_PO_LIST += " AND customerType like '%"+filter.getCustomerType()+"%' ";
			}
			if(filter.getPoTotal() != null){
				SQL_PO_LIST += " AND poTotal = "+filter.getPoTotal()+" ";
			}
			if(filter.getTicketQty() != null){
				SQL_PO_LIST += " AND ticketQty = "+filter.getTicketQty()+" ";
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					SQL_PO_LIST += " AND DATEPART(day,created) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					SQL_PO_LIST += " AND DATEPART(month,created) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					SQL_PO_LIST += " AND DATEPART(year,created) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getShippingType() != null){
				SQL_PO_LIST += " AND shippingType like '%"+filter.getShippingType()+"%' ";
			}
			if(filter.getStatus() != null){
				SQL_PO_LIST += " AND status like '%"+filter.getStatus()+"%' ";
			}
			if(filter.getProductType() != null){
				SQL_PO_LIST += " AND product_type like '%"+filter.getProductType()+"%' ";	
			}
			
			session  = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(SQL_PO_LIST);
			sqlQuery.addScalar("id", Hibernate.INTEGER);
			sqlQuery.addScalar("customerId", Hibernate.INTEGER);
			sqlQuery.addScalar("customerName", Hibernate.STRING);
			sqlQuery.addScalar("created", Hibernate.DATE);
			sqlQuery.addScalar("poTotal", Hibernate.DOUBLE);
			sqlQuery.addScalar("ticketQty", Hibernate.INTEGER);
			sqlQuery.addScalar("shippingType", Hibernate.STRING);
			sqlQuery.addScalar("status", Hibernate.STRING);
			sqlQuery.addScalar("productType", Hibernate.STRING);
			Collection<PurchaseOrders> poListForCustomer = sqlQuery.setResultTransformer(Transformers.aliasToBean(PurchaseOrders.class))
												.list();
			return poListForCustomer;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	/*
	 * RTW/RTW2 PO for Customer
	 */
	@SuppressWarnings("unchecked")
	public Collection<PurchaseOrders> getRTWPOForCustomer(Integer customerId, GridHeaderFilters filter){
		Session session =null;
		try {
			String SQL_PO_LIST = "select * from (select po.id as id,po.customer_id as customerId,c.cust_name as customerName,c.customer_type as customerType, "+
					 "po.created_time as created, po.po_total as poTotal, po.ticket_count as ticketQty, sm.name as shippingType, "+ 
					 "c.product_type as productType, po.status as status "+ 
					 "from purchase_order po with(nolock) inner join customer c with(nolock) on(c.id=po.customer_id) "+ 
					 "left join shipping_method sm with(nolock) on(po.shipping_method_id=sm.id) where po.status ='ACTIVE' "+ 
					 "union all "+ 
					 "select distinct id, customerId, customerName, customerType, created, poTotal, ticketQty, shippingType, product_type as productType, '' as status "+
					 "from pos_purchase_order_view with(nolock) where 1=1) A ";
			
			if(customerId != null){
				SQL_PO_LIST += " WHERE A.customerId = "+customerId+" ";
			}
			if(filter.getPurchaseOrderId() != null){
				SQL_PO_LIST += " AND A.id = "+filter.getPurchaseOrderId()+" ";
			}
			if(filter.getCustomerName() != null){
				SQL_PO_LIST += " AND A.customerName like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getCustomerType() != null){
				SQL_PO_LIST += " AND A.customerType like '%"+filter.getCustomerType()+"%' ";
			}
			if(filter.getPoTotal() != null){
				SQL_PO_LIST += " AND A.poTotal = "+filter.getPoTotal()+" ";
			}
			if(filter.getTicketQty() != null){
				SQL_PO_LIST += " AND A.ticketQty = "+filter.getTicketQty()+" ";
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					SQL_PO_LIST += " AND DATEPART(day,A.created) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					SQL_PO_LIST += " AND DATEPART(month,A.created) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					SQL_PO_LIST += " AND DATEPART(year,A.created) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getShippingType() != null){
				SQL_PO_LIST += " AND A.shippingType like '%"+filter.getShippingType()+"%' ";
			}
			if(filter.getStatus() != null){
				SQL_PO_LIST += " AND A.status like '%"+filter.getStatus()+"%' ";
			}
			if(filter.getProductType() != null){
				SQL_PO_LIST += " AND A.productType like '%"+filter.getProductType()+"%' ";	
			}
			
			session  = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(SQL_PO_LIST);
			sqlQuery.addScalar("id", Hibernate.INTEGER);
			sqlQuery.addScalar("customerId", Hibernate.INTEGER);
			sqlQuery.addScalar("customerName", Hibernate.STRING);
			sqlQuery.addScalar("created", Hibernate.DATE);
			sqlQuery.addScalar("poTotal", Hibernate.DOUBLE);
			sqlQuery.addScalar("ticketQty", Hibernate.INTEGER);
			sqlQuery.addScalar("shippingType", Hibernate.STRING);
			sqlQuery.addScalar("status", Hibernate.STRING);
			sqlQuery.addScalar("productType", Hibernate.STRING);
			Collection<PurchaseOrders> poListForCustomer = sqlQuery.setResultTransformer(Transformers.aliasToBean(PurchaseOrders.class))
												.list();
			return poListForCustomer;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	public Collection<EventDetails> getAllActiveEventDetailsByBroker1(String searchColumn,String searchValue,String fromDate,String toDate,
			ProductType productType,Integer brokerId, GridHeaderFilters filter, String pageNo,boolean isPagination){
		Collection<EventDetails> eventDetails = null;
		Integer startFrom=0;
		try {
			String sqlQuery = "select e.id AS eventId,e.name AS eventName,e.event_date AS eventDate,e.event_time AS eventTime,e.venue_id AS venueId,"+
			   "v.building as building,v.city AS city,v.state AS state,v.country AS country,v.postal_code AS postalCode,gcc.name AS grandChildCategoryName," +
			   "cg.name AS childCategoryName,pc.name as parentCategoryName,ctg.noOfTixCount AS noOfTixCount,T.sold_count AS noOfTixSoldCount," +
			   "E.create_date AS eventCreationDate,E.last_update AS eventLastUpdatedDate,E.notes AS notes ";
			   
			
			if(searchColumn !=null && !searchColumn.isEmpty()){
				if(searchColumn.equalsIgnoreCase("artist_name")){
					sqlQuery += ",A.artist_name AS artistName ";
				}
			}
			sqlQuery += "from event e with(nolock) join venue v with(nolock) on e.venue_id=v.id " +
			   "join event_category ec with(nolock) on e.id=ec.event_id " +
			   "join grand_child_category gcc with(nolock) on ec.grand_child_category_id=gcc.id " +
			   "join child_category cg with(nolock) on ec.child_category_id=cg.id " +
			   "join parent_category pc with(nolock) on ec.parent_category_id=pc.id " +
			   "left join event_ticket_count_details t with(nolock) on e.id=t.event_id " +
			   "left join (select event_id, broker_id, count(quantity) as noOfTixCount,status " +
			   "from category_ticket_group with(nolock) group by event_id, broker_id,status) ctg " +
			   "on  (e.id=ctg.event_id and ctg.status='ACTIVE' AND ctg.broker_id="+brokerId+") ";
			
		   if(searchColumn !=null && !searchColumn.isEmpty()){
				if(searchColumn.equalsIgnoreCase("artist_name")){
					sqlQuery += "inner join event_artist_details A with(nolock) on E.id=A.event_id ";
				}
			}				   
			sqlQuery += " WHERE 1=1 ";
			if(fromDate != null && !fromDate.isEmpty() && toDate != null && !toDate.isEmpty()) {
				sqlQuery += " AND E.event_date>='"+fromDate+"' AND E.event_date<='"+toDate+"' ";
			}
			
			if(searchColumn !=null && !searchColumn.isEmpty() && searchValue!=null && !searchValue.isEmpty()){
				if(searchColumn.equalsIgnoreCase("artist_name")){
					sqlQuery += " AND A."+searchColumn +" like '%"+searchValue+"%' ";
				}else{
					sqlQuery += " AND E.name like '%"+searchValue+"%' ";
				}
			}
			
			if(filter.getEventId() != null){
				sqlQuery += " AND E.id = "+filter.getEventId()+" ";
			}
			if(filter.getEventName() != null){
				sqlQuery += " AND E.name like '%"+filter.getEventName()+"%' ";
			}
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					sqlQuery += " AND DATEPART(day,E.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					sqlQuery += " AND DATEPART(month,E.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					sqlQuery += " AND DATEPART(year,E.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventTimeStr() != null){
				if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
					sqlQuery += " AND DATEPART(hour,E.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
					sqlQuery += " AND DATEPART(minute,E.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
				}
			}
			if(filter.getVenueName() != null){
				sqlQuery += " AND v.building like '%"+filter.getVenueName()+"%' ";
			}
			if(filter.getVenueId() != null){
				sqlQuery += " AND E.venue_id = "+filter.getVenueId()+" ";
			}
			if(filter.getNoOfTixCount() != null){
				sqlQuery += " AND ctg.noOfTixCount = "+filter.getNoOfTixCount()+" ";
			}
			if(filter.getNoOfTixSoldCount() != null){
				sqlQuery += " AND T.sold_count = "+filter.getNoOfTixSoldCount()+" ";
			}
			if(filter.getCity() != null){
				sqlQuery += " AND v.city like '%"+filter.getCity()+"%' ";
			}
			if(filter.getState() != null){
				sqlQuery += " AND v.state like '%"+filter.getState()+"%' ";
			}
			if(filter.getCountry() != null){
				sqlQuery += " AND v.country like '%"+filter.getCountry()+"%' ";
			}
			if(filter.getGrandChildCategoryName() != null){
				sqlQuery += " AND gcc.name like '%"+filter.getGrandChildCategoryName()+"%' ";
			}
			if(filter.getChildCategoryName() != null){
				sqlQuery += " AND cg.name like '%"+filter.getChildCategoryName()+"%' ";
			}
			if(filter.getParentCategoryName() != null){
				sqlQuery += " AND pc.name like '%"+filter.getParentCategoryName()+"%' ";
			}
			if(filter.getInternalNotes() != null){
				sqlQuery += " AND E.notes like '%"+filter.getInternalNotes()+"%' ";
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sqlQuery += " AND DATEPART(day,E.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sqlQuery += " AND DATEPART(month,E.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sqlQuery += " AND DATEPART(year,E.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getLastUpdatedDateStr() != null){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					sqlQuery += " AND DATEPART(day,E.last_update) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
					sqlQuery += " AND DATEPART(month,E.last_update) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
					sqlQuery += " AND DATEPART(year,E.last_update) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
				}
			}
			sqlQuery +=" ORDER BY E.event_date, E.event_time Asc";
			staticSession  = getStaticSession();
			SQLQuery query = staticSession.createSQLQuery(
					sqlQuery);
			query.addScalar("eventId", Hibernate.INTEGER);
			query.addScalar("eventName", Hibernate.STRING);
			query.addScalar("eventDate", Hibernate.DATE);
			query.addScalar("eventTime", Hibernate.TIME);
			query.addScalar("venueId", Hibernate.INTEGER);
			query.addScalar("building", Hibernate.STRING);
			query.addScalar("city", Hibernate.STRING);
			query.addScalar("state", Hibernate.STRING);
			query.addScalar("country", Hibernate.STRING);
			query.addScalar("postalCode", Hibernate.STRING);
			
			if(searchColumn !=null && !searchColumn.isEmpty()){
				if(searchColumn.equalsIgnoreCase("artist_name")){
					query.addScalar("artistName", Hibernate.STRING);
				}
			}
			query.addScalar("grandChildCategoryName", Hibernate.STRING);
			query.addScalar("childCategoryName", Hibernate.STRING);
			query.addScalar("parentCategoryName", Hibernate.STRING);
			query.addScalar("noOfTixCount", Hibernate.INTEGER);
			query.addScalar("noOfTixSoldCount", Hibernate.INTEGER);
			query.addScalar("eventCreationDate", Hibernate.DATE);
			query.addScalar("eventLastUpdatedDate", Hibernate.DATE);
			query.addScalar("notes", Hibernate.STRING);
			if(isPagination){
				startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
				eventDetails = query.setResultTransformer(Transformers.aliasToBean(EventDetails.class)).setFirstResult(startFrom).setMaxResults(PaginationUtil.PAGESIZE).list();
			}else{
				eventDetails = query.setResultTransformer(Transformers.aliasToBean(EventDetails.class)).list();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return eventDetails;
	}
	
	public Integer getAllActiveEventDetailsCountByBroker1(String searchColumn,String searchValue,String fromDate,String toDate,
			ProductType productType,Integer brokerId, GridHeaderFilters filter){
		Collection<EventDetails> eventDetails = null;
		Integer startFrom=0;
		try {
			String sqlQuery = "select count(distinct e.id) "+
			"from event e with(nolock) join venue v with(nolock) on e.venue_id=v.id " +
			   "join event_category ec with(nolock) on e.id=ec.event_id " +
			   "join grand_child_category gcc with(nolock) on ec.grand_child_category_id=gcc.id " +
			   "join child_category cg with(nolock) on ec.child_category_id=cg.id " +
			   "join parent_category pc with(nolock) on ec.parent_category_id=pc.id " +
			   "left join event_ticket_count_details t with(nolock) on e.id=t.event_id " +
			   "left join (select event_id, broker_id, count(quantity) as noOfTixCount,status " +
			   "from category_ticket_group with(nolock) group by event_id, broker_id,status) ctg " +
			   "on  (e.id=ctg.event_id and ctg.status='ACTIVE' AND ctg.broker_id="+brokerId+") ";
			
		   if(searchColumn !=null && !searchColumn.isEmpty()){
				if(searchColumn.equalsIgnoreCase("artist_name")){
					sqlQuery += "inner join event_artist_details A with(nolock) on E.id=A.event_id ";
				}
			}				   
			sqlQuery += " WHERE 1=1 ";
			if(fromDate != null && !fromDate.isEmpty() && toDate != null && !toDate.isEmpty()) {
				sqlQuery += " AND E.event_date>='"+fromDate+"' AND E.event_date<='"+toDate+"' ";
			}
			
			if(searchColumn !=null && !searchColumn.isEmpty() && searchValue!=null && !searchValue.isEmpty()){
				if(searchColumn.equalsIgnoreCase("artist_name")){
					sqlQuery += " AND A."+searchColumn +" like '%"+searchValue+"%' ";
				}else{
					sqlQuery += " AND E.name like '%"+searchValue+"%' ";
				}
			}
			
			if(filter.getEventId() != null){
				sqlQuery += " AND E.id = "+filter.getEventId()+" ";
			}
			if(filter.getEventName() != null){
				sqlQuery += " AND E.name like '%"+filter.getEventName()+"%' ";
			}
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					sqlQuery += " AND DATEPART(day,E.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					sqlQuery += " AND DATEPART(month,E.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					sqlQuery += " AND DATEPART(year,E.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventTimeStr() != null){
				if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
					sqlQuery += " AND DATEPART(hour,E.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
					sqlQuery += " AND DATEPART(minute,E.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
				}
			}
			if(filter.getVenueName() != null){
				sqlQuery += " AND v.building like '%"+filter.getVenueName()+"%' ";
			}
			if(filter.getVenueId() != null){
				sqlQuery += " AND E.venue_id = "+filter.getVenueId()+" ";
			}
			if(filter.getNoOfTixCount() != null){
				sqlQuery += " AND ctg.noOfTixCount = "+filter.getNoOfTixCount()+" ";
			}
			if(filter.getNoOfTixSoldCount() != null){
				sqlQuery += " AND T.sold_count = "+filter.getNoOfTixSoldCount()+" ";
			}
			if(filter.getCity() != null){
				sqlQuery += " AND v.city like '%"+filter.getCity()+"%' ";
			}
			if(filter.getState() != null){
				sqlQuery += " AND v.state like '%"+filter.getState()+"%' ";
			}
			if(filter.getCountry() != null){
				sqlQuery += " AND v.country like '%"+filter.getCountry()+"%' ";
			}
			if(filter.getGrandChildCategoryName() != null){
				sqlQuery += " AND gcc.name like '%"+filter.getGrandChildCategoryName()+"%' ";
			}
			if(filter.getChildCategoryName() != null){
				sqlQuery += " AND cg.name like '%"+filter.getChildCategoryName()+"%' ";
			}
			if(filter.getParentCategoryName() != null){
				sqlQuery += " AND pc.name like '%"+filter.getParentCategoryName()+"%' ";
			}
			if(filter.getInternalNotes() != null){
				sqlQuery += " AND E.notes like '%"+filter.getInternalNotes()+"%' ";
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sqlQuery += " AND DATEPART(day,E.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sqlQuery += " AND DATEPART(month,E.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sqlQuery += " AND DATEPART(year,E.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getLastUpdatedDateStr() != null){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					sqlQuery += " AND DATEPART(day,E.last_update) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
					sqlQuery += " AND DATEPART(month,E.last_update) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
					sqlQuery += " AND DATEPART(year,E.last_update) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
				}
			}
			staticSession  = getStaticSession();
			SQLQuery query = staticSession.createSQLQuery(
					sqlQuery);
			Integer count =  (Integer)query.uniqueResult();
			return count;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
		
	}
	
	public Collection<EventDetails> getAllActiveEventDetailsByBroker(String searchColumn,String searchValue,String fromDate,String toDate,
			ProductType productType,Integer brokerId, GridHeaderFilters filter, String pageNo,boolean isPagination){
		Collection<EventDetails> eventDetails = null;
		Integer startFrom=0;
		try {
			String sqlQuery = "select distinct eventId AS eventId,eventName AS eventName,eventDate AS eventDate,eventTime AS eventTime,venueId AS venueId,building AS building,"+
			   "city As city,state AS state,country As country,postalCode AS postalCode,grandChildCategoryName AS grandChildCategoryName," +
			   "childCategoryName AS childCategoryName,parentCategoryName AS parentCategoryName,noOfTixCount AS noOfTixCount,noOfTixSoldCount AS noOfTixSoldCount," +
			   "eventCreationDate AS eventCreationDate,eventLastUpdatedDate AS eventLastUpdatedDate,notes AS notes from tiff_event_Details where 1=1 ";
			  
		   
			if(fromDate != null && !fromDate.isEmpty() && toDate != null && !toDate.isEmpty()) {
				sqlQuery += " AND eventDate >='"+fromDate+"' AND eventDate <='"+toDate+"' ";
			}
			
			if(searchColumn !=null && !searchColumn.isEmpty() && searchValue!=null && !searchValue.isEmpty()){
				if(searchColumn.equalsIgnoreCase("artist_name")){
					sqlQuery += " AND artistName like '%"+searchValue+"%' ";
				}/*else{
					sqlQuery += " AND E."+searchColumn +" like '%"+searchValue+"%' ";
				}*/
			}
			/*if(productType!=null){
				if(productType == ProductType.LASTROWMINICATS){
					sqlQuery += " AND E.lastrow_minicat_enabled=1";
				}else if(productType == ProductType.REWARDTHEFAN){
					sqlQuery += " AND E.reward_thefan_enabled=1";
				}else if(productType == ProductType.PRESALEZONETICKETS){
					sqlQuery += " AND E.presale_zonetickets_enable=1";
				}else if(productType == ProductType.ZONETICKETS){
					sqlQuery += " AND E.category_tickets_enabled=1";
				}else if(productType == ProductType.VIPMINICATS){
				}
			}*/
			if(filter.getEventId() != null){
				sqlQuery += " AND eventId = "+filter.getEventId()+" ";
			}
			if(filter.getEventName() != null){
				sqlQuery += " AND eventName like '%"+filter.getEventName()+"%' ";
			}
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					sqlQuery += " AND DATEPART(day,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					sqlQuery += " AND DATEPART(month,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					sqlQuery += " AND DATEPART(year,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventTimeStr() != null){
				if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
					sqlQuery += " AND DATEPART(hour,eventTime) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
					sqlQuery += " AND DATEPART(minute,eventTime) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
				}
			}
			if(filter.getVenueName() != null){
				sqlQuery += " AND building like '%"+filter.getVenueName()+"%' ";
			}
			if(filter.getVenueId() != null){
				sqlQuery += " AND venueId = "+filter.getVenueId()+" ";
			}
			if(filter.getNoOfTixCount() != null){
				sqlQuery += " AND noOfTixCount = "+filter.getNoOfTixCount()+" ";
			}
			if(filter.getNoOfTixSoldCount() != null){
				sqlQuery += " AND noOfTixSoldCount = "+filter.getNoOfTixSoldCount()+" ";
			}
			if(filter.getCity() != null){
				sqlQuery += " AND city like '%"+filter.getCity()+"%' ";
			}
			if(filter.getState() != null){
				sqlQuery += " AND state like '%"+filter.getState()+"%' ";
			}
			if(filter.getCountry() != null){
				sqlQuery += " AND country like '%"+filter.getCountry()+"%' ";
			}
			if(filter.getGrandChildCategoryName() != null){
				sqlQuery += " AND grandChildCategoryName like '%"+filter.getGrandChildCategoryName()+"%' ";
			}
			if(filter.getChildCategoryName() != null){
				sqlQuery += " AND childCategoryName like '%"+filter.getChildCategoryName()+"%' ";
			}
			if(filter.getParentCategoryName() != null){
				sqlQuery += " AND parentCategoryName like '%"+filter.getParentCategoryName()+"%' ";
			}
			if(filter.getInternalNotes() != null){
				sqlQuery += " AND notes like '%"+filter.getInternalNotes()+"%' ";
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sqlQuery += " AND DATEPART(day,eventCreationDate) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sqlQuery += " AND DATEPART(month,eventCreationDate) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sqlQuery += " AND DATEPART(year,eventCreationDate) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getLastUpdatedDateStr() != null){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					sqlQuery += " AND DATEPART(day,eventLastUpdatedDate) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
					sqlQuery += " AND DATEPART(month,eventLastUpdatedDate) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
					sqlQuery += " AND DATEPART(year,eventLastUpdatedDate) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
				}
			}
			

			String sortingSql = GridSortingUtil.getEventsSortingSql(filter);
			if(sortingSql!=null && !sortingSql.isEmpty()){
				sqlQuery += sortingSql;
			}else{
				sqlQuery +=" ORDER BY eventDate, eventTime Asc";
			}
				
			staticSession  = getStaticSession();
			SQLQuery query = staticSession.createSQLQuery(
					sqlQuery);
			query.addScalar("eventId", Hibernate.INTEGER);
			query.addScalar("eventName", Hibernate.STRING);
			query.addScalar("eventDate", Hibernate.DATE);
			query.addScalar("eventTime", Hibernate.TIME);
			query.addScalar("venueId", Hibernate.INTEGER);
			query.addScalar("building", Hibernate.STRING);
			query.addScalar("city", Hibernate.STRING);
			query.addScalar("state", Hibernate.STRING);
			query.addScalar("country", Hibernate.STRING);
			query.addScalar("postalCode", Hibernate.STRING);
			
			if(searchColumn !=null && !searchColumn.isEmpty()){
				if(searchColumn.equalsIgnoreCase("artist_name")){
					query.addScalar("artistName", Hibernate.STRING);
				}
			}
			query.addScalar("grandChildCategoryName", Hibernate.STRING);
			query.addScalar("childCategoryName", Hibernate.STRING);
			query.addScalar("parentCategoryName", Hibernate.STRING);
			query.addScalar("noOfTixCount", Hibernate.INTEGER);
			query.addScalar("noOfTixSoldCount", Hibernate.INTEGER);
			query.addScalar("eventCreationDate", Hibernate.DATE);
			query.addScalar("eventLastUpdatedDate", Hibernate.DATE);
			query.addScalar("notes", Hibernate.STRING);
			if(isPagination){
				startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
				eventDetails = query.setResultTransformer(Transformers.aliasToBean(EventDetails.class)).setFirstResult(startFrom).setMaxResults(PaginationUtil.PAGESIZE).list();
			}else{
				eventDetails = query.setResultTransformer(Transformers.aliasToBean(EventDetails.class)).list();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return eventDetails;
	}
	
	public Integer getAllActiveEventDetailsCountByBroker(String searchColumn,String searchValue,String fromDate,String toDate,
			ProductType productType,Integer brokerId, GridHeaderFilters filter){
		Integer count = 0;
		try {
			String sqlQuery = "select count(distinct eventId) from tiff_event_Details where 1=1 ";
					
			if(fromDate != null && !fromDate.isEmpty() && toDate != null && !toDate.isEmpty()) {
				sqlQuery += " AND eventDate >='"+fromDate+"' AND eventDate <='"+toDate+"' ";
			}
			if(filter.getEventId() != null){
				sqlQuery += " AND eventId = "+filter.getEventId()+" ";
			}
			if(filter.getEventName() != null){
				sqlQuery += " AND eventName like '%"+filter.getEventName()+"%' ";
			}
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					sqlQuery += " AND DATEPART(day,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					sqlQuery += " AND DATEPART(month,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					sqlQuery += " AND DATEPART(year,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventTimeStr() != null){
				if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
					sqlQuery += " AND DATEPART(hour,eventTime) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
					sqlQuery += " AND DATEPART(minute,eventTime) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
				}
			}
			if(filter.getVenueName() != null){
				sqlQuery += " AND building like '%"+filter.getVenueName()+"%' ";
			}
			if(filter.getVenueId() != null){
				sqlQuery += " AND venueId = "+filter.getVenueId()+" ";
			}
			if(filter.getNoOfTixCount() != null){
				sqlQuery += " AND noOfTixCount = "+filter.getNoOfTixCount()+" ";
			}
			if(filter.getNoOfTixSoldCount() != null){
				sqlQuery += " AND noOfTixSoldCount = "+filter.getNoOfTixSoldCount()+" ";
			}
			if(filter.getCity() != null){
				sqlQuery += " AND city like '%"+filter.getCity()+"%' ";
			}
			if(filter.getState() != null){
				sqlQuery += " AND state like '%"+filter.getState()+"%' ";
			}
			if(filter.getCountry() != null){
				sqlQuery += " AND country like '%"+filter.getCountry()+"%' ";
			}
			if(filter.getGrandChildCategoryName() != null){
				sqlQuery += " AND grandChildCategoryName like '%"+filter.getGrandChildCategoryName()+"%' ";
			}
			if(filter.getChildCategoryName() != null){
				sqlQuery += " AND childCategoryName like '%"+filter.getChildCategoryName()+"%' ";
			}
			if(filter.getParentCategoryName() != null){
				sqlQuery += " AND parentCategoryName like '%"+filter.getParentCategoryName()+"%' ";
			}
			if(filter.getInternalNotes() != null){
				sqlQuery += " AND notes like '%"+filter.getInternalNotes()+"%' ";
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sqlQuery += " AND DATEPART(day,eventCreationDate) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sqlQuery += " AND DATEPART(month,eventCreationDate) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sqlQuery += " AND DATEPART(year,eventCreationDate) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getLastUpdatedDateStr() != null){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					sqlQuery += " AND DATEPART(day,eventLastUpdatedDate) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
					sqlQuery += " AND DATEPART(month,eventLastUpdatedDate) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
					sqlQuery += " AND DATEPART(year,eventLastUpdatedDate) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
				}
			}
			//sqlQuery +=" ORDER BY E.event_date, E.event_time Asc";
			staticSession  = getStaticSession();
			SQLQuery query = staticSession.createSQLQuery(
					sqlQuery);
			count = (Integer)query.uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}
	
	
	
	public Collection<EventDetails> getAllActiveEventDetailsByFilter(String searchColumn,String searchValue,String fromDate,String toDate,
			ProductType productType, GridHeaderFilters filter, String pageNo){
		Collection<EventDetails> eventDetails = null;
		Integer startFrom=0;
		try {
			String sqlQuery = "SELECT distinct E.event_id AS eventId,E.event_name AS eventName,E.event_date AS eventDate,E.event_time AS eventTime,"+
					   "E.venue_id AS venueId,E.building AS building,E.city As city,E.state AS state,E.country As country,E.postal_code " +
					   "AS postalCode," +
					   "E.grand_child_category_name AS grandChildCategoryName,E.child_category_name AS childCategoryName," +
					   "E.parent_category_name AS parentCategoryName,T.active_count AS noOfTixCount,T.sold_count AS noOfTixSoldCount," +
					   "E.create_date AS eventCreationDate,E.last_update AS eventLastUpdatedDate,E.notes AS notes";
			
				if(searchColumn !=null && !searchColumn.isEmpty()){
					if(searchColumn.equalsIgnoreCase("artist_name")){
						sqlQuery += ", A.artist_name AS artistName ";
					}
				}
				sqlQuery += " from event_details E with(nolock) left join event_ticket_count_details T with(nolock) on E.event_id=T.event_id ";
				
			   if(searchColumn !=null && !searchColumn.isEmpty()){
					if(searchColumn.equalsIgnoreCase("artist_name")){
						sqlQuery += "inner join event_artist_details A with(nolock) on E.event_id=A.event_id ";
					}
				}				   
				sqlQuery += " WHERE E.status=1";
				   
				if(fromDate != null && !fromDate.isEmpty() && toDate != null && !toDate.isEmpty()) {
					sqlQuery += " AND E.event_date>='"+fromDate+"' AND E.event_date<='"+toDate+"' ";
				}
				
				if(searchColumn !=null && !searchColumn.isEmpty() && searchValue!=null && !searchValue.isEmpty()){
					if(searchColumn.equalsIgnoreCase("artist_name")){
						sqlQuery += " AND A."+searchColumn +" like '%"+searchValue+"%' ";
					}else{
						sqlQuery += " AND E."+searchColumn +" like '%"+searchValue+"%' ";
					}
				}
				if(productType!=null){
					if(productType == ProductType.LASTROWMINICATS){
						sqlQuery += " AND E.lastrow_minicat_enabled=1";
					}else if(productType == ProductType.REWARDTHEFAN){
						sqlQuery += " AND E.reward_thefan_enabled=1";
					}else if(productType == ProductType.PRESALEZONETICKETS){
						sqlQuery += " AND E.presale_zonetickets_enable=1";
					}else if(productType == ProductType.ZONETICKETS){
						sqlQuery += " AND E.category_tickets_enabled=1";
					}else if(productType == ProductType.VIPMINICATS){
					}
				}
				if(filter.getEventId() != null){
					sqlQuery += " AND E.event_id = "+filter.getEventId()+" ";
				}
				if(filter.getEventName() != null){
					sqlQuery += " AND E.event_name like '%"+filter.getEventName()+"%' ";
				}
				if(filter.getEventDateStr() != null){
					if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
						sqlQuery += " AND DATEPART(day,E.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,E.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,E.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getEventTimeStr() != null){
					if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
						sqlQuery += " AND DATEPART(hour,E.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
					}
					if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
						sqlQuery += " AND DATEPART(minute,E.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
					}
				}
				if(filter.getVenueName() != null){
					sqlQuery += " AND E.building like '%"+filter.getVenueName()+"%' ";
				}
				if(filter.getVenueId() != null){
					sqlQuery += " AND E.venue_id = "+filter.getVenueId()+" ";
				}
				if(filter.getNoOfTixCount() != null){
					sqlQuery += " AND T.active_count = "+filter.getNoOfTixCount()+" ";
				}
				if(filter.getNoOfTixSoldCount() != null){
					sqlQuery += " AND T.sold_count = "+filter.getNoOfTixSoldCount()+" ";
				}
				if(filter.getCity() != null){
					sqlQuery += " AND E.city like '%"+filter.getCity()+"%' ";
				}
				if(filter.getState() != null){
					sqlQuery += " AND E.state like '%"+filter.getState()+"%' ";
				}
				if(filter.getCountry() != null){
					sqlQuery += " AND E.country like '%"+filter.getCountry()+"%' ";
				}
				if(filter.getGrandChildCategoryName() != null){
					sqlQuery += " AND E.grand_child_category_name like '%"+filter.getGrandChildCategoryName()+"%' ";
				}
				if(filter.getChildCategoryName() != null){
					sqlQuery += " AND E.child_category_name like '%"+filter.getChildCategoryName()+"%' ";
				}
				if(filter.getParentCategoryName() != null){
					sqlQuery += " AND E.parent_category_name like '%"+filter.getParentCategoryName()+"%' ";
				}
				if(filter.getInternalNotes() != null){
					sqlQuery += " AND E.notes like '%"+filter.getInternalNotes()+"%' ";
				}
				if(filter.getCreatedDateStr() != null){
					if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
						sqlQuery += " AND DATEPART(day,E.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,E.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,E.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getLastUpdatedDateStr() != null){
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
						sqlQuery += " AND DATEPART(day,E.last_update) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,E.last_update) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,E.last_update) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
					}
				}
				sqlQuery +=" ORDER BY E.event_date, E.event_time Asc";
				staticSession  = getStaticSession();
				SQLQuery query = staticSession.createSQLQuery(
						sqlQuery);
				query.addScalar("eventId", Hibernate.INTEGER);
				query.addScalar("eventName", Hibernate.STRING);
				query.addScalar("eventDate", Hibernate.DATE);
				query.addScalar("eventTime", Hibernate.TIME);
				query.addScalar("venueId", Hibernate.INTEGER);
				query.addScalar("building", Hibernate.STRING);
				query.addScalar("city", Hibernate.STRING);
				query.addScalar("state", Hibernate.STRING);
				query.addScalar("country", Hibernate.STRING);
				query.addScalar("postalCode", Hibernate.STRING);
				
				if(searchColumn !=null && !searchColumn.isEmpty()){
					if(searchColumn.equalsIgnoreCase("artist_name")){
						query.addScalar("artistName", Hibernate.STRING);
					}
				}
				query.addScalar("grandChildCategoryName", Hibernate.STRING);
				query.addScalar("childCategoryName", Hibernate.STRING);
				query.addScalar("parentCategoryName", Hibernate.STRING);
				query.addScalar("noOfTixCount", Hibernate.INTEGER);
				query.addScalar("noOfTixSoldCount", Hibernate.INTEGER);
				query.addScalar("eventCreationDate", Hibernate.DATE);
				query.addScalar("eventLastUpdatedDate", Hibernate.DATE);
				query.addScalar("notes", Hibernate.STRING);
				startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
				eventDetails = query.setResultTransformer(Transformers.aliasToBean(EventDetails.class)).setFirstResult(startFrom).setMaxResults(PaginationUtil.PAGESIZE).list();
				
		} catch (Exception e) {
			e.printStackTrace();
		}
		return eventDetails;
	}
	
	
	public Collection<EventDetails> getAllActiveEventDetailsByFilterToExport(String searchColumn,String searchValue,String fromDate,String toDate,
			ProductType productType, GridHeaderFilters filter){
		Collection<EventDetails> eventDetails = null;
		Session session =null;
		try {
			String sqlQuery = "SELECT distinct E.event_id AS eventId,E.event_name AS eventName,E.event_date AS eventDate,E.event_time AS eventTime,"+
				   "E.venue_id AS venueId,E.building AS building,E.city As city,E.state AS state,E.country As country,E.postal_code " +
				   "AS postalCode, " +
				   "E.grand_child_category_name AS grandChildCategoryName,E.child_category_name AS childCategoryName," +
				   "E.parent_category_name AS parentCategoryName,T.active_count AS noOfTixCount,T.sold_count AS noOfTixSoldCount," +
				   "E.create_date AS eventCreationDate,E.last_update AS eventLastUpdatedDate,E.notes AS notes ";
				  /* "from event_details E with(nolock) inner join event_ticket_count_details T " +
				   "on E.event_id=T.event_id " +
				   "inner join event_artist_details A on E.event_id=A.event_id "+
				   "WHERE E.status=1";*/			

				if(searchColumn !=null && !searchColumn.isEmpty()){
					if(searchColumn.equalsIgnoreCase("artist_name")){
						sqlQuery += ", A.artist_name AS artistName ";
					}
				}
				sqlQuery += " from event_details E with(nolock) left join event_ticket_count_details T with(nolock) on E.event_id=T.event_id ";
				
			   if(searchColumn !=null && !searchColumn.isEmpty()){
					if(searchColumn.equalsIgnoreCase("artist_name")){
						sqlQuery += "inner join event_artist_details A with(nolock) on E.event_id=A.event_id ";
					}
				}				   
				sqlQuery += " WHERE E.status=1";
					  
				if(fromDate != null && !fromDate.isEmpty() && toDate != null && !toDate.isEmpty()) {
					sqlQuery += " AND E.event_date>='"+fromDate+"' AND E.event_date<='"+toDate+"' ";
				}
				
				if(searchColumn !=null && !searchColumn.isEmpty() && searchValue!=null && !searchValue.isEmpty()){
					if(searchColumn.equalsIgnoreCase("artist_name")){
						sqlQuery += " AND A."+searchColumn +" like '%"+searchValue+"%' ";
					}else{
						sqlQuery += " AND E."+searchColumn +" like '%"+searchValue+"%' ";
					}
				}
				if(productType!=null){
					if(productType == ProductType.LASTROWMINICATS){
						sqlQuery += " AND E.lastrow_minicat_enabled=1";
					}else if(productType == ProductType.REWARDTHEFAN){
						sqlQuery += " AND E.reward_thefan_enabled=1";
					}else if(productType == ProductType.PRESALEZONETICKETS){
						sqlQuery += " AND E.presale_zonetickets_enable=1";
					}else if(productType == ProductType.ZONETICKETS){
						sqlQuery += " AND E.category_tickets_enabled=1";
					}else if(productType == ProductType.VIPMINICATS){
					}
				}
				if(filter.getEventId() != null){
					sqlQuery += " AND E.event_id = "+filter.getEventId()+" ";
				}
				if(filter.getEventName() != null){
					sqlQuery += " AND E.event_name like '%"+filter.getEventName()+"%' ";
				}
				if(filter.getEventDateStr() != null){
					if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
						sqlQuery += " AND DATEPART(day,E.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,E.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,E.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getEventTimeStr() != null){
					if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
						sqlQuery += " AND DATEPART(hour,E.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
					}
					if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
						sqlQuery += " AND DATEPART(minute,E.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
					}
				}
				if(filter.getVenueName() != null){
					sqlQuery += " AND E.building like '%"+filter.getVenueName()+"%' ";
				}
				if(filter.getVenueId() != null){
					sqlQuery += " AND E.venue_id = "+filter.getVenueId()+" ";
				}
				if(filter.getNoOfTixCount() != null){
					sqlQuery += " AND T.active_count = "+filter.getNoOfTixCount()+" ";
				}
				if(filter.getNoOfTixSoldCount() != null){
					sqlQuery += " AND T.sold_count = "+filter.getNoOfTixSoldCount()+" ";
				}
				if(filter.getCity() != null){
					sqlQuery += " AND E.city like '%"+filter.getCity()+"%' ";
				}
				if(filter.getState() != null){
					sqlQuery += " AND E.state like '%"+filter.getState()+"%' ";
				}
				if(filter.getCountry() != null){
					sqlQuery += " AND E.country like '%"+filter.getCountry()+"%' ";
				}
				if(filter.getGrandChildCategoryName() != null){
					sqlQuery += " AND E.grand_child_category_name like '%"+filter.getGrandChildCategoryName()+"%' ";
				}
				if(filter.getChildCategoryName() != null){
					sqlQuery += " AND E.child_category_name like '%"+filter.getChildCategoryName()+"%' ";
				}
				if(filter.getParentCategoryName() != null){
					sqlQuery += " AND E.parent_category_name like '%"+filter.getParentCategoryName()+"%' ";
				}
				if(filter.getInternalNotes() != null){
					sqlQuery += " AND E.notes like '%"+filter.getInternalNotes()+"%' ";
				}
				if(filter.getCreatedDateStr() != null){
					if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
						sqlQuery += " AND DATEPART(day,E.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,E.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,E.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getLastUpdatedDateStr() != null){
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
						sqlQuery += " AND DATEPART(day,E.last_update) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,E.last_update) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,E.last_update) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
					}
				}
				sqlQuery +=" ORDER BY E.event_date, E.event_time Asc";
				session  = sessionFactory.openSession();
				SQLQuery query = session.createSQLQuery(
						sqlQuery);
				query.addScalar("eventId", Hibernate.INTEGER);
				query.addScalar("eventName", Hibernate.STRING);
				query.addScalar("eventDate", Hibernate.DATE);
				query.addScalar("eventTime", Hibernate.TIME);
				query.addScalar("venueId", Hibernate.INTEGER);
				query.addScalar("building", Hibernate.STRING);
				query.addScalar("city", Hibernate.STRING);
				query.addScalar("state", Hibernate.STRING);
				query.addScalar("country", Hibernate.STRING);
				query.addScalar("postalCode", Hibernate.STRING);
				if(searchColumn !=null && !searchColumn.isEmpty()){
					if(searchColumn.equalsIgnoreCase("artist_name")){
						query.addScalar("artistName", Hibernate.STRING);
					}
				}
				query.addScalar("grandChildCategoryName", Hibernate.STRING);
				query.addScalar("childCategoryName", Hibernate.STRING);
				query.addScalar("parentCategoryName", Hibernate.STRING);
				query.addScalar("noOfTixCount", Hibernate.INTEGER);
				query.addScalar("noOfTixSoldCount", Hibernate.INTEGER);
				query.addScalar("eventCreationDate", Hibernate.DATE);
				query.addScalar("eventLastUpdatedDate", Hibernate.DATE);
				query.addScalar("notes", Hibernate.STRING);
				eventDetails = query.setResultTransformer(Transformers.aliasToBean(EventDetails.class)).list();
				
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return eventDetails;
	}

	public Integer getEventDetailsByFilterCount(String searchColumn,String searchValue,String fromDate,String toDate,ProductType productType, GridHeaderFilters filter){
		Integer count = 0;
		Session session =null;
		try {
			String sqlQuery = "SELECT count(*) as cnt from event_details E with(nolock) inner join event_ticket_count_details T " +
				   "on E.event_id=T.event_id ";
				   
				   if(searchColumn !=null && !searchColumn.isEmpty()){
						if(searchColumn.equalsIgnoreCase("artist_name")){
							sqlQuery += "inner join event_artist_details A on E.event_id=A.event_id ";
						}
					}
				   
					sqlQuery +=  "WHERE E.status=1";
				   
				if(fromDate != null && !fromDate.isEmpty() && toDate != null && !toDate.isEmpty()) {
					sqlQuery += " AND E.event_date>='"+fromDate+"' AND E.event_date<='"+toDate+"' ";
				}
				
				if(searchColumn !=null && !searchColumn.isEmpty() && searchValue!=null && !searchValue.isEmpty()){
					if(searchColumn.equalsIgnoreCase("artist_name")){
						sqlQuery += " AND A."+searchColumn +" like '%"+searchValue+"%' ";
					}else{
						sqlQuery += " AND E."+searchColumn +" like '%"+searchValue+"%' ";
					}
				}
				if(productType!=null){
					if(productType == ProductType.LASTROWMINICATS){
						sqlQuery += " AND E.lastrow_minicat_enabled=1";
					}else if(productType == ProductType.REWARDTHEFAN){
						sqlQuery += " AND E.reward_thefan_enabled=1";
					}else if(productType == ProductType.PRESALEZONETICKETS){
						sqlQuery += " AND E.presale_zonetickets_enable=1";
					}else if(productType == ProductType.ZONETICKETS){
						sqlQuery += " AND E.category_tickets_enabled=1";
					}else if(productType == ProductType.VIPMINICATS){
					}
				}
				if(filter.getEventId() != null){
					sqlQuery += " AND E.event_id = "+filter.getEventId()+" ";
				}
				if(filter.getEventName() != null){
					sqlQuery += " AND E.event_name like '%"+filter.getEventName()+"%' ";
				}
				if(filter.getEventDateStr() != null){
					if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
						sqlQuery += " AND DATEPART(day,E.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,E.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,E.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getEventTimeStr() != null){
					if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
						sqlQuery += " AND DATEPART(hour,E.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
					}
					if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
						sqlQuery += " AND DATEPART(minute,E.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
					}
				}
				if(filter.getVenueName() != null){
					sqlQuery += " AND E.building like '%"+filter.getVenueName()+"%' ";
				}
				if(filter.getVenueId() != null){
					sqlQuery += " AND E.venue_id = "+filter.getVenueId()+" ";
				}
				if(filter.getNoOfTixCount() != null){
					sqlQuery += " AND T.active_count = "+filter.getNoOfTixCount()+" ";
				}
				if(filter.getNoOfTixSoldCount() != null){
					sqlQuery += " AND T.sold_count = "+filter.getNoOfTixSoldCount()+" ";
				}
				if(filter.getCity() != null){
					sqlQuery += " AND E.city like '%"+filter.getCity()+"%' ";
				}
				if(filter.getState() != null){
					sqlQuery += " AND E.state like '%"+filter.getState()+"%' ";
				}
				if(filter.getCountry() != null){
					sqlQuery += " AND E.country like '%"+filter.getCountry()+"%' ";
				}
				if(filter.getGrandChildCategoryName() != null){
					sqlQuery += " AND E.grand_child_category_name like '%"+filter.getGrandChildCategoryName()+"%' ";
				}
				if(filter.getChildCategoryName() != null){
					sqlQuery += " AND E.child_category_name like '%"+filter.getChildCategoryName()+"%' ";
				}
				if(filter.getParentCategoryName() != null){
					sqlQuery += " AND E.parent_category_name like '%"+filter.getParentCategoryName()+"%' ";
				}
				if(filter.getInternalNotes() != null){
					sqlQuery += " AND E.notes like '%"+filter.getInternalNotes()+"%' ";
				}
				if(filter.getCreatedDateStr() != null){
					if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
						sqlQuery += " AND DATEPART(day,E.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,E.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,E.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getLastUpdatedDateStr() != null){
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
						sqlQuery += " AND DATEPART(day,E.last_update) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,E.last_update) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,E.last_update) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
					}
				}
				session  = sessionFactory.openSession();
				Query query = session.createSQLQuery(sqlQuery);
				count =  (Integer) query.uniqueResult();
				} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return count;
	}

	public Collection<EventDetails> getAllActiveEventForPO(String searchColumn,String searchValue,String fromDate,String timeStr,
			GridHeaderFilters filter,String pageNo){
		Collection<EventDetails> eventDetails = null;
		Session session =null;
		Integer startFrom=0;
		try {
			String sqlQuery = "SELECT distinct E.event_id AS eventId,E.event_name AS eventName,E.event_date AS eventDate,E.event_time AS eventTime,"+
				   "E.venue_id AS venueId,E.building AS building,E.city As city,E.state AS state,E.country As country,E.postal_code " +
				   "AS postalCode,A.artist_name AS artistName," +
				   "E.grand_child_category_name AS grandChildCategoryName,E.child_category_name AS childCategoryName," +
				   "E.parent_category_name AS parentCategoryName," +
				   "E.create_date AS eventCreationDate,E.last_update AS eventLastUpdatedDate,E.notes AS notes " +
				   "from event_details E with(nolock) "+
				   "inner join event_artist_details A with(nolock) on E.event_id=A.event_id "+
				   "WHERE E.status=1";
				   
				if(fromDate != null && !fromDate.isEmpty()) {
					sqlQuery += " AND E.event_date>='"+fromDate+"' ";
				}
				if(timeStr != null && !timeStr.isEmpty()) {
					sqlQuery += " AND E.event_time>='1970-01-01 "+timeStr+"' ";
				}
				
				if(searchColumn !=null && !searchColumn.isEmpty() && searchValue!=null && !searchValue.isEmpty()){
					if(searchColumn.equalsIgnoreCase("artist_name")){
						sqlQuery += " AND A."+searchColumn +" like '%"+searchValue+"%' ";
					}else{
						sqlQuery += " AND E."+searchColumn +" like '%"+searchValue+"%' ";
					}
				}
				/*if(productType!=null){
					if(productType == ProductType.LASTROWMINICATS){
						sqlQuery += " AND E.lastrow_minicat_enabled=1";
					}else if(productType == ProductType.REWARDTHEFAN){
						sqlQuery += " AND E.reward_thefan_enabled=1";
					}else if(productType == ProductType.PRESALEZONETICKETS){
						sqlQuery += " AND E.presale_zonetickets_enable=1";
					}else if(productType == ProductType.ZONETICKETS){
						sqlQuery += " AND E.category_tickets_enabled=1";
					}else if(productType == ProductType.VIPMINICATS){
					}
				}*/
				if(filter.getEventId() != null){
					sqlQuery += " AND E.event_id = "+filter.getEventId()+" ";
				}
				if(filter.getEventName() != null){
					sqlQuery += " AND E.event_name like '%"+filter.getEventName()+"%' ";
				}
				if(filter.getEventDateStr() != null){
					if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
						sqlQuery += " AND DATEPART(day,E.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,E.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,E.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getEventTimeStr() != null){
					if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
						sqlQuery += " AND DATEPART(hour,E.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
					}
					if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
						sqlQuery += " AND DATEPART(minute,E.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
					}
				}
				if(filter.getVenueName() != null){
					sqlQuery += " AND E.building like '%"+filter.getVenueName()+"%' ";
				}
				if(filter.getVenueId() != null){
					sqlQuery += " AND E.venue_id = "+filter.getVenueId()+" ";
				}
				/*if(filter.getNoOfTixCount() != null){
					sqlQuery += " AND T.active_count = "+filter.getNoOfTixCount()+" ";
				}
				if(filter.getNoOfTixSoldCount() != null){
					sqlQuery += " AND T.sold_count = "+filter.getNoOfTixSoldCount()+" ";
				}*/
				if(filter.getCity() != null){
					sqlQuery += " AND E.city like '%"+filter.getCity()+"%' ";
				}
				if(filter.getState() != null){
					sqlQuery += " AND E.state like '%"+filter.getState()+"%' ";
				}
				if(filter.getCountry() != null){
					sqlQuery += " AND E.country like '%"+filter.getCountry()+"%' ";
				}
				if(filter.getGrandChildCategoryName() != null){
					sqlQuery += " AND E.grand_child_category_name like '%"+filter.getGrandChildCategoryName()+"%' ";
				}
				if(filter.getChildCategoryName() != null){
					sqlQuery += " AND E.child_category_name like '%"+filter.getChildCategoryName()+"%' ";
				}
				if(filter.getParentCategoryName() != null){
					sqlQuery += " AND E.parent_category_name like '%"+filter.getParentCategoryName()+"%' ";
				}
				if(filter.getInternalNotes() != null){
					sqlQuery += " AND E.notes like '%"+filter.getInternalNotes()+"%' ";
				}
				if(filter.getCreatedDateStr() != null){
					if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
						sqlQuery += " AND DATEPART(day,E.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY");
					}
					if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,E.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH");
					}
					if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,E.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR");
					}
				}
				if(filter.getLastUpdatedDateStr() != null){
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
						sqlQuery += " AND DATEPART(day,E.last_update) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY");
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,E.last_update) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH");
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,E.last_update) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR");
					}
				}
				sqlQuery +=" ORDER BY E.event_date, E.event_time Asc";
				session  = sessionFactory.openSession();
				SQLQuery query = session.createSQLQuery(
						sqlQuery);
				query.addScalar("eventId", Hibernate.INTEGER);
				query.addScalar("eventName", Hibernate.STRING);
				query.addScalar("eventDate", Hibernate.DATE);
				query.addScalar("eventTime", Hibernate.TIME);
				query.addScalar("venueId", Hibernate.INTEGER);
				query.addScalar("building", Hibernate.STRING);
				query.addScalar("city", Hibernate.STRING);
				query.addScalar("state", Hibernate.STRING);
				query.addScalar("country", Hibernate.STRING);
				query.addScalar("postalCode", Hibernate.STRING);
				query.addScalar("artistName", Hibernate.STRING);
				query.addScalar("grandChildCategoryName", Hibernate.STRING);
				query.addScalar("childCategoryName", Hibernate.STRING);
				query.addScalar("parentCategoryName", Hibernate.STRING);
				/*query.addScalar("noOfTixCount", Hibernate.INTEGER);
				query.addScalar("noOfTixSoldCount", Hibernate.INTEGER);*/
				query.addScalar("eventCreationDate", Hibernate.DATE);
				query.addScalar("eventLastUpdatedDate", Hibernate.DATE);
				query.addScalar("notes", Hibernate.STRING);
				startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
				eventDetails = query.setResultTransformer(Transformers.aliasToBean(EventDetails.class)).setFirstResult(startFrom).setMaxResults(PaginationUtil.PAGESIZE).list();
				
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return eventDetails;
	}
	
	public Integer getEventDetailsCountForPO(String searchColumn,String searchValue,String fromDate,String timeStr,GridHeaderFilters filter){
		Integer count = 0;
		Session session =null;
		try {
			String sqlQuery = "SELECT count(*) as cnt from event_details E with(nolock) "+
				   "inner join event_artist_details A with(nolock) on E.event_id=A.event_id "+
				   "WHERE E.status=1";
				   
				if(fromDate != null && !fromDate.isEmpty()) {
					sqlQuery += " AND E.event_date>='"+fromDate+"' ";
				}
				
				if(timeStr != null && !timeStr.isEmpty()) {
					sqlQuery += " AND E.event_time>='1970-01-01 "+timeStr+"' ";
				}
				if(searchColumn !=null && !searchColumn.isEmpty() && searchValue!=null && !searchValue.isEmpty()){
					if(searchColumn.equalsIgnoreCase("artist_name")){
						sqlQuery += " AND A."+searchColumn +" like '%"+searchValue+"%' ";
					}else{
						sqlQuery += " AND E."+searchColumn +" like '%"+searchValue+"%' ";
					}
				}
				/*if(productType!=null){
					if(productType == ProductType.LASTROWMINICATS){
						sqlQuery += " AND E.lastrow_minicat_enabled=1";
					}else if(productType == ProductType.REWARDTHEFAN){
						sqlQuery += " AND E.reward_thefan_enabled=1";
					}else if(productType == ProductType.PRESALEZONETICKETS){
						sqlQuery += " AND E.presale_zonetickets_enable=1";
					}else if(productType == ProductType.ZONETICKETS){
						sqlQuery += " AND E.category_tickets_enabled=1";
					}else if(productType == ProductType.VIPMINICATS){
					}
				}*/
				if(filter.getEventId() != null){
					sqlQuery += " AND E.event_id = "+filter.getEventId()+" ";
				}
				if(filter.getEventName() != null){
					sqlQuery += " AND E.event_name like '%"+filter.getEventName()+"%' ";
				}
				if(filter.getEventDateStr() != null){
					if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
						sqlQuery += " AND DATEPART(day,E.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,E.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,E.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getEventTimeStr() != null){
					if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
						sqlQuery += " AND DATEPART(hour,E.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
					}
					if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
						sqlQuery += " AND DATEPART(minute,E.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
					}
				}
				if(filter.getVenueName() != null){
					sqlQuery += " AND E.building like '%"+filter.getVenueName()+"%' ";
				}
				if(filter.getVenueId() != null){
					sqlQuery += " AND E.venue_id = "+filter.getVenueId()+" ";
				}
				/*if(filter.getNoOfTixCount() != null){
					sqlQuery += " AND T.active_count = "+filter.getNoOfTixCount()+" ";
				}
				if(filter.getNoOfTixSoldCount() != null){
					sqlQuery += " AND T.sold_count = "+filter.getNoOfTixSoldCount()+" ";
				}*/
				if(filter.getCity() != null){
					sqlQuery += " AND E.city like '%"+filter.getCity()+"%' ";
				}
				if(filter.getState() != null){
					sqlQuery += " AND E.state like '%"+filter.getState()+"%' ";
				}
				if(filter.getCountry() != null){
					sqlQuery += " AND E.country like '%"+filter.getCountry()+"%' ";
				}
				if(filter.getGrandChildCategoryName() != null){
					sqlQuery += " AND E.grand_child_category_name like '%"+filter.getGrandChildCategoryName()+"%' ";
				}
				if(filter.getChildCategoryName() != null){
					sqlQuery += " AND E.child_category_name like '%"+filter.getChildCategoryName()+"%' ";
				}
				if(filter.getParentCategoryName() != null){
					sqlQuery += " AND E.parent_category_name like '%"+filter.getParentCategoryName()+"%' ";
				}
				if(filter.getInternalNotes() != null){
					sqlQuery += " AND E.notes like '%"+filter.getInternalNotes()+"%' ";
				}
				if(filter.getCreatedDateStr() != null){
					if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
						sqlQuery += " AND DATEPART(day,E.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,E.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,E.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getLastUpdatedDateStr() != null){
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
						sqlQuery += " AND DATEPART(day,E.last_update) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,E.last_update) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,E.last_update) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
					}
				}
				session  = sessionFactory.openSession();
				Query query = session.createSQLQuery(sqlQuery);
				count =  (Integer) query.uniqueResult();
				} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return count;
	}
	
	
	public List<CrownJewelTeamZones> getCrownJewelTeamZonesByTeamId(Integer teamId) throws Exception {
		Session session =null;
		try{
			String queryStr = "select sum(ticket_qty) as soldTicketsCount,sum(ticket_points) as pointsRedeemed,replace(ticket_section,' Package','') as zone," +
					" team_id as teamId" +
					" from crownjewel_customer_orders with(nolock) where status='ACTIVE' and team_id=?" +
					" group by replace(ticket_section,' Package',''),team_id,league_id" +
					" ";
			session  = sessionFactory.openSession();
			Query query = session.createSQLQuery(queryStr);
			query.setInteger(0, teamId);
			
			List<CrownJewelTeamZones> list = query.setResultTransformer(Transformers.aliasToBean(CrownJewelTeamZones.class)).list();
			return list;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	public List<CrownJewelTeamZones> getCrownJewelTeamZonesByLeagueId(Integer leagueId) throws Exception {
		Session session =null;
		try{
			String queryStr = "select sum(ticket_qty) as soldTicketsCount,sum(ticket_points) as pointsRedeemed,replace(ticket_section,' Package','') as zone," +
					" team_id as teamId" +
					" from crownjewel_customer_orders with(nolock) where status='ACTIVE' and league_id=?" +
					" group by replace(ticket_section,' Package',''),team_id,league_id" +
					"";
			session  = sessionFactory.openSession();
			Query query = session.createSQLQuery(queryStr);
			query.setInteger(0, leagueId);
			
			List<CrownJewelTeamZones> list = query.setResultTransformer(Transformers.aliasToBean(CrownJewelTeamZones.class)).list();
			return list;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	public boolean saveEventNotes(Integer id,String notes){
		Session session =null;
		try {
			String query = "UPDATE event SET notes='"+notes+"' WHERE id="+id;
			session  = sessionFactory.openSession();
			session.createSQLQuery(query).executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return true;
	}
	
	public boolean saveVenueNotes(Integer id,String notes){
		Session session =null;
		try {
			String query = "UPDATE Venue SET notes='"+notes+"' WHERE id="+id;
			session  = sessionFactory.openSession();
			session.createSQLQuery(query).executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return true;
	}
	
	
	public List<OpenOrderStatus> getRTWRTW2FilledOrders(String eventIdStr, String artistIdStr,String venueIdStr, String fromDate,
			String toDate, ProfitLossSign sign,String orderType,String productType,GridHeaderFilters filter,String pageNo){
		List<OpenOrderStatus> orders= null;
		Session session =null;
		Integer startFrom = 0;
		try {
			String sql = "select invoiceNo as id,* from pos_filled_orders with(nolock) where status='"+orderType+"' ";
			if(eventIdStr!=null && !eventIdStr.isEmpty()){
				sql = sql+" AND tmatEventId ="+eventIdStr;
			}else {
				if(venueIdStr!=null && !venueIdStr.isEmpty()){
					sql = sql+" AND venueId ="+venueIdStr;
				}else if(artistIdStr!=null && !artistIdStr.isEmpty()){
					sql = sql+" AND tmatEventId in (SELECT event_id from event_artist_details with(nolock) WHERE artist_Id ="+artistIdStr+" )";
				}
			}
				
			if(fromDate!=null && !fromDate.isEmpty()){
				sql = sql+" AND invoiceDate >='"+fromDate+"'";
			}
			if(toDate!=null && !toDate.isEmpty()){
				sql = sql+" AND invoiceDate <='"+toDate+"'";
			}
			
			if(productType!=null && !productType.isEmpty()){
				if(productType.equalsIgnoreCase("RTW")){
					sql = sql+" AND productType='RTW' ";
				}else if (productType.equalsIgnoreCase("RTW2")){
					sql = sql+" AND producTtype='RTW2' ";
				}
			}
			
			if(filter.getInvoiceId() != null){
				sql += " AND invoiceNo = "+filter.getInvoiceId();
			}
			if(filter.getPurchaseOrderId() != null){
				sql += " AND poId = "+filter.getPurchaseOrderId();
			}
			if(filter.getCustomerOrderId() != null){
				sql += " AND ticketRequestId = "+filter.getCustomerOrderId();
			}
			if(filter.getInvoiceDateStr() != null){
				if(Util.extractDateElement(filter.getInvoiceDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,invoiceDate) = "+Util.extractDateElement(filter.getInvoiceDateStr(),"DAY");
				}
				if(Util.extractDateElement(filter.getInvoiceDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,invoiceDate) = "+Util.extractDateElement(filter.getInvoiceDateStr(),"MONTH");
				}
				if(Util.extractDateElement(filter.getInvoiceDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,invoiceDate) = "+Util.extractDateElement(filter.getInvoiceDateStr(),"YEAR");
				}
			}
			if(filter.getPurchaseOrderDateStr() != null){
				if(Util.extractDateElement(filter.getPurchaseOrderDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,poDate) = "+Util.extractDateElement(filter.getPurchaseOrderDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getPurchaseOrderDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,poDate) = "+Util.extractDateElement(filter.getPurchaseOrderDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getPurchaseOrderDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,poDate) = "+Util.extractDateElement(filter.getPurchaseOrderDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventName() != null){
				sql += " AND eventName like '%"+filter.getEventName()+"%' ";
			}
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY");
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH");
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR");
				}
			}
			if(filter.getEventTimeStr() != null){
				if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
					sql += " AND DATEPART(hour,eventTime) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR");
				}
				if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
					sql += " AND DATEPART(minute,eventTime) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE");
				}
			}
			if(filter.getVenueName() != null){
				sql += " AND venueName like '%"+filter.getVenueName()+"%' ";
			}
			if(filter.getProductType() != null){
				sql += " AND productType like '%"+filter.getProductType()+"%' ";
			}
			if(filter.getInternalNotes() != null){
				sql += " AND internalNotes like '%"+filter.getInternalNotes()+"%' ";
			}
			if(filter.getQuantity() != null){
				sql += " AND soldQty = "+filter.getQuantity()+" ";
			}
			if(filter.getSection() != null){
				sql += " AND section like '%"+filter.getSection()+"%' ";
			}
			if(filter.getRow() != null){
				sql += " AND row like '%"+filter.getRow()+"%' ";
			}
			if(filter.getActualSoldPrice() != null){
				sql += " AND actualSoldPrice = "+filter.getActualSoldPrice()+" ";
			}
			/*
			 if(null !=sign ){
				switch (sign) {
					case POSITIVE:
						sql = sql+" AND profitAndLoss > 0 ";
						break;
					case NEGATIVE:
						sql = sql+" AND profitAndLoss < 0 ";
						break;
					case ZERO:
						sql = sql+" AND profitAndLoss = 0 ";
						break;
					case ALL:
						sql = sql+" ";
						break;
					default:
						break;
				}
			}
			if(filter.getMarketPrice() != null){
				sql += " AND o.market_price = "+filter.getMarketPrice()+" ";
			}
			if(filter.getLastUpdatedPrice() != null){
				sql += " AND o.last_updated_price = "+filter.getLastUpdatedPrice()+" "; 	
			}
			if(filter.getSectionTixQty() != null){
				sql += " AND o.section_ticket_qty = "+filter.getSectionTixQty()+" ";
			}
			if(filter.getEventTixQty() != null){
				sql += " AND o.event_ticket_qty = "+filter.getEventTixQty()+" ";
			}
			if(filter.getProfitAndLoss() != null){
				sql += " AND o.profit_loss = "+filter.getProfitAndLoss()+" ";
			}
			if(filter.getPriceUpdateCount() != null){
				sql += " AND o.market_price_update_count = "+filter.getPriceUpdateCount()+" ";
			}
			if(filter.getSecondaryOrderType() != null){
				sql += " AND co.secondary_order_type like '%"+filter.getSecondaryOrderType()+"%' ";
			}
			if(filter.getSecondaryOrderId() != null){
				sql += " AND co.secondary_order_id = "+filter.getSecondaryOrderId()+" ";
			}
			if(filter.getLastUpdatedDateStr() != null){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,o.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,o.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,o.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
				}
			}*/
			if(filter.getShippingMethod() != null){
				sql += " AND shippingMethod like '%"+filter.getShippingMethod()+"%' ";
			}
			if(filter.getTrackingNo() != null){
				sql += " AND trackingNo = "+filter.getTrackingNo()+" ";
			}
			
			sql +=  " Order by invoiceNo desc";
			session  = sessionFactory.openSession();
			SQLQuery query = session.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("tmatEventId", Hibernate.INTEGER);
			query.addScalar("exEventId", Hibernate.INTEGER);
			//query.addScalar("sectionTixQty", Hibernate.INTEGER);
			//query.addScalar("eventTixQty", Hibernate.INTEGER);
			query.addScalar("ticketRequestId", Hibernate.INTEGER);
			query.addScalar("section", Hibernate.STRING);
			//query.addScalar("marketPrice", Hibernate.DOUBLE);
			//query.addScalar("lastUpdatedPrice", Hibernate.DOUBLE);
			//query.addScalar("lastCrawlTime", Hibernate.TIMESTAMP);
			//query.addScalar("createdDate", Hibernate.TIMESTAMP);
			//query.addScalar("lastUpdated", Hibernate.TIMESTAMP);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("poId", Hibernate.INTEGER);
			query.addScalar("poDate", Hibernate.TIMESTAMP);
			//query.addScalar("category", Hibernate.STRING);
			//query.addScalar("brokerId", Hibernate.INTEGER);
			query.addScalar("invoiceNo", Hibernate.INTEGER);
			query.addScalar("invoiceDate", Hibernate.TIMESTAMP);
			query.addScalar("venueId", Hibernate.INTEGER);
			query.addScalar("soldQty", Hibernate.INTEGER);
			query.addScalar("ticketGroupId", Hibernate.INTEGER);
			query.addScalar("brokerName", Hibernate.STRING);
			query.addScalar("invoiceType", Hibernate.STRING);
			query.addScalar("salesPerson", Hibernate.STRING);
			query.addScalar("eventName", Hibernate.STRING);
			query.addScalar("eventDate", Hibernate.DATE);
			query.addScalar("eventTime", Hibernate.TIME);
			query.addScalar("venueName", Hibernate.STRING);
			query.addScalar("venueCity", Hibernate.STRING);
			query.addScalar("venueState", Hibernate.STRING);
			query.addScalar("venueCountry", Hibernate.STRING);
			query.addScalar("internalNotes", Hibernate.STRING);
			query.addScalar("retailPrice", Hibernate.DOUBLE);
			query.addScalar("wholesalePrice", Hibernate.DOUBLE);
			query.addScalar("cost", Hibernate.DOUBLE);
			query.addScalar("actualSoldPrice", Hibernate.DOUBLE);
			query.addScalar("totalPrice", Hibernate.DOUBLE);
			query.addScalar("row", Hibernate.STRING);
			query.addScalar("profitAndLoss", Hibernate.DOUBLE);
			query.addScalar("priceUpdateCount", Hibernate.INTEGER);
			query.addScalar("onlinePrice", Hibernate.DOUBLE);
			query.addScalar("productType", Hibernate.STRING);
			query.addScalar("shippingMethod", Hibernate.STRING);
			query.addScalar("trackingNo", Hibernate.STRING);
			query.addScalar("orderId", Hibernate.INTEGER);
			startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
			orders = query.setResultTransformer(Transformers.aliasToBean(OpenOrderStatus.class)).setFirstResult(startFrom)
			.setMaxResults(PaginationUtil.PAGESIZE).list();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return orders;
	}
	
	
	public Integer getRTWRTW2FilledOrdersCount(String eventIdStr, String artistIdStr,String venueIdStr, String fromDate,
			String toDate, ProfitLossSign sign,String orderType,String productType,GridHeaderFilters filter){
		Session session =null;
		Integer count = 0;
		try {
			String sql = "select count(*) from pos_filled_orders with(nolock) where status='"+orderType+"' ";
			if(eventIdStr!=null && !eventIdStr.isEmpty()){
				sql = sql+" AND tmatEventId ="+eventIdStr;
			}else {
				if(venueIdStr!=null && !venueIdStr.isEmpty()){
					sql = sql+" AND venueId ="+venueIdStr;
				}else if(artistIdStr!=null && !artistIdStr.isEmpty()){
					sql = sql+" AND tmatEventId in (SELECT event_id from event_artist_details with(nolock) WHERE artist_Id ="+artistIdStr+" )";
				}
			}
				
			if(fromDate!=null && !fromDate.isEmpty()){
				sql = sql+" AND invoiceDate >='"+fromDate+"'";
			}
			if(toDate!=null && !toDate.isEmpty()){
				sql = sql+" AND invoiceDate <='"+toDate+"'";
			}
			if(productType!=null && !productType.isEmpty()){
				if(productType.equalsIgnoreCase("RTW")){
					sql = sql+" AND productType='RTW' ";
				}else if (productType.equalsIgnoreCase("RTW2")){
					sql = sql+" AND producTtype='RTW2' ";
				}
			}
			
			if(filter.getInvoiceId() != null){
				sql += " AND invoiceNo = "+filter.getInvoiceId();
			}
			if(filter.getPurchaseOrderId() != null){
				sql += " AND poId = "+filter.getPurchaseOrderId();
			}
			if(filter.getCustomerOrderId() != null){
				sql += " AND ticketRequestId = "+filter.getCustomerOrderId();
			}
			if(filter.getInvoiceDateStr() != null){
				if(Util.extractDateElement(filter.getInvoiceDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,invoiceDate) = "+Util.extractDateElement(filter.getInvoiceDateStr(),"DAY");
				}
				if(Util.extractDateElement(filter.getInvoiceDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,invoiceDate) = "+Util.extractDateElement(filter.getInvoiceDateStr(),"MONTH");
				}
				if(Util.extractDateElement(filter.getInvoiceDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,invoiceDate) = "+Util.extractDateElement(filter.getInvoiceDateStr(),"YEAR");
				}
			}
			if(filter.getPurchaseOrderDateStr() != null){
				if(Util.extractDateElement(filter.getPurchaseOrderDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,poDate) = "+Util.extractDateElement(filter.getPurchaseOrderDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getPurchaseOrderDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,poDate) = "+Util.extractDateElement(filter.getPurchaseOrderDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getPurchaseOrderDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,poDate) = "+Util.extractDateElement(filter.getPurchaseOrderDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventName() != null){
				sql += " AND eventName like '%"+filter.getEventName()+"%' ";
			}
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY");
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH");
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR");
				}
			}
			if(filter.getEventTimeStr() != null){
				if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
					sql += " AND DATEPART(hour,eventTime) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR");
				}
				if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
					sql += " AND DATEPART(minute,eventTime) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE");
				}
			}
			if(filter.getVenueName() != null){
				sql += " AND venueName like '%"+filter.getVenueName()+"%' ";
			}
			if(filter.getInternalNotes() != null){
				sql += " AND internalNotes like '%"+filter.getInternalNotes()+"%' ";
			}
			if(filter.getQuantity() != null){
				sql += " AND soldQty = "+filter.getQuantity()+" ";
			}
			if(filter.getSection() != null){
				sql += " AND section like '%"+filter.getSection()+"%' ";
			}
			if(filter.getRow() != null){
				sql += " AND row like '%"+filter.getRow()+"%' ";
			}
			if(filter.getActualSoldPrice() != null){
				sql += " AND actualSoldPrice = "+filter.getActualSoldPrice()+" ";
			}
			/*
			 if(null !=sign ){
				switch (sign) {
					case POSITIVE:
						sql = sql+" AND profitAndLoss > 0 ";
						break;
					case NEGATIVE:
						sql = sql+" AND profitAndLoss < 0 ";
						break;
					case ZERO:
						sql = sql+" AND profitAndLoss = 0 ";
						break;
					case ALL:
						sql = sql+" ";
						break;
					default:
						break;
				}
			}
			if(filter.getProductType() != null){
				sql += " AND productType like '%"+filter.getProductType()+"%' ";
			}
			if(filter.getMarketPrice() != null){
				sql += " AND o.market_price = "+filter.getMarketPrice()+" ";
			}
			if(filter.getLastUpdatedPrice() != null){
				sql += " AND o.last_updated_price = "+filter.getLastUpdatedPrice()+" "; 	
			}
			if(filter.getSectionTixQty() != null){
				sql += " AND o.section_ticket_qty = "+filter.getSectionTixQty()+" ";
			}
			if(filter.getEventTixQty() != null){
				sql += " AND o.event_ticket_qty = "+filter.getEventTixQty()+" ";
			}
			if(filter.getProfitAndLoss() != null){
				sql += " AND o.profit_loss = "+filter.getProfitAndLoss()+" ";
			}
			if(filter.getPriceUpdateCount() != null){
				sql += " AND o.market_price_update_count = "+filter.getPriceUpdateCount()+" ";
			}
			if(filter.getSecondaryOrderType() != null){
				sql += " AND co.secondary_order_type like '%"+filter.getSecondaryOrderType()+"%' ";
			}
			if(filter.getSecondaryOrderId() != null){
				sql += " AND co.secondary_order_id = "+filter.getSecondaryOrderId()+" ";
			}
			if(filter.getLastUpdatedDateStr() != null){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,o.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,o.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,o.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
				}
			}*/
			if(filter.getShippingMethod() != null){
				sql += " AND shippingMethod like '%"+filter.getShippingMethod()+"%' ";
			}
			if(filter.getTrackingNo() != null){
				sql += " AND trackingNo = "+filter.getTrackingNo()+" ";
			}
			
			session  = sessionFactory.openSession();
			SQLQuery query = session.createSQLQuery(sql);
			count = (Integer) query.uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return count;
	}
	
	
	public List<OpenOrderStatus> getOpenOrShipmentPendingOrClosedOrders(String eventIdStr, String artistIdStr,String venueIdStr, String fromDate,
			String toDate, ProfitLossSign sign,String orderType,String productType,GridHeaderFilters filter,boolean isCount,String pageNo,Integer brokerId){
		List<OpenOrderStatus> orders= null;
		Session session = null;
		Integer startFrom = 0;
		try {
			String sql = "select o.id AS id,o.event_id AS tmatEventId,o.tn_exchange_event_id AS exEventId,"+
				"o.section_ticket_qty AS sectionTixQty,o.event_ticket_qty AS eventTixQty,"+
				"o.market_price AS marketPrice,o.last_updated_price AS lastUpdatedPrice,"+
				"o.last_crawl_time AS lastCrawlTime,o.created_date AS createdDate,o.last_updated AS lastUpdated,"+
				"o.status AS status,o.category AS category,o.broker_id AS brokerId,o.ticket_request_id AS ticketRequestId,"+
				"o.invoice_no AS invoiceNo,o.invoice_date AS invoiceDate,o.venue_id AS venueId,co.secondary_order_id AS secondaryOrderId,"+
				"o.sold_qty AS soldQty,o.ticket_group_id AS ticketGroupId,o.broker_name AS brokerName,"+
				"o.invoice_type AS invoiceType,o.sales_person AS salesPerson,o.event_name AS eventName,"+
				"o.event_date AS eventDate,o.event_time AS eventTime,o.venue_name AS venueName,o.venue_city AS venueCity,"+
				"o.venue_state AS venueState,o.venue_country AS venueCountry,o.internal_notes AS internalNotes,"+
				"o.retail_price AS retailPrice,o.wholesale_price AS wholesalePrice,o.cost AS cost,co.secondary_order_type AS secondaryOrderType,"+
				"o.actual_sold_price AS actualSoldPrice,o.total_price AS totalPrice,sm.name AS shippingMethod,"+
				"o.profit_loss AS profitAndLoss,o.market_price_update_count AS priceUpdateCount,i.tracking_no AS trackingNo,"+
				"o.online_price AS onlinePrice,o.product_type AS productType,i.order_id AS orderId,CONCAT(cus.cust_name,' ',cus.last_name) AS customerName, "+
				"p.id as poId,p.created_time as poDate,o.zone as zone,o.zone_ticket_qty as zoneTixQty," +
				" o.zone_cheapest_price as zoneCheapestPrice,o.zone_ticket_count as zoneTixGroupCount, " +
				" o.net_total_sold_price as netTotalSoldPrice,o.net_sold_price as netSoldPrice, o.zone_total_price as zoneTotalPrice, " +
				" o.zone_profit_loss as zoneProfitAndLoss,o.zone_margin as zoneMargin,section_margin as sectionMargin, "+
				"cus.company_name as companyName, co.platform as platform,co.order_type as orderType, ";
				
				if(orderType.equalsIgnoreCase("OPEN") || orderType.equalsIgnoreCase("DISPUTED")){
					sql += "o.section AS section,o.row AS row ";
				}else if(orderType.equalsIgnoreCase("PENDING SHIPMENT")){
					sql += "otg.section AS section,otg.row AS row ";
				}else if(orderType.equalsIgnoreCase("PENDING RECIEPT")){
					sql += "otg.section AS section,otg.row AS row ";
				}else if(orderType.equalsIgnoreCase("CLOSED")){
					sql += "otg.section AS section,otg.row AS row ";
				}
				
				sql += " from tix_open_order_status o with(nolock) left join invoice i with(nolock) on o.invoice_no=i.id "+
				"left join customer_order co with(nolock) on co.id=i.order_id "+
				"left join customer cus with(nolock) on cus.id=i.customer_id "+
				"left join purchase_order p with(nolock) on i.purchase_order_no=p.id "+
				"left join shipping_method sm with(nolock) on(i.shipping_method_id=sm.id) ";
				
				if(orderType.equalsIgnoreCase("PENDING SHIPMENT")){
					sql += " left join order_ticket_group_details otg with(nolock) on(otg.order_id=co.id) ";
				}else if(orderType.equalsIgnoreCase("PENDING RECIEPT")){
					sql += " left join order_ticket_group_details otg with(nolock) on(otg.order_id=co.id) ";
				}else if(orderType.equalsIgnoreCase("CLOSED")){
					sql += " left join order_ticket_group_details otg with(nolock) on(otg.order_id=co.id) ";
				}
				sql += " WHERE o.status='ACTIVE' ";
				
				/*else if(orderType.equalsIgnoreCase("PENDING")){
					sql += " AND ((i.realTix_map='Yes' AND i.is_real_tix_upload='Yes' and i.status='Completed'"+
							" AND (i.id not in(select invoice_id from customer_ticket_downloads) AND i.upload_to_exchange=0)) OR i.fedex_label_created=1) ";
				}else*/ 
				if(orderType.equalsIgnoreCase("OPEN")){
					sql += " AND (i.is_real_tix_upload='No' OR i.is_real_tix_upload IS NULL) AND (i.fedex_label_created=0 OR i.fedex_label_created is NULL) " +
							"AND (i.realTix_map='No' or i.realTix_map is null) AND (i.status='Outstanding' OR i.status is NULL)";
				}else if(orderType.equalsIgnoreCase("PENDING SHIPMENT")){
					sql += " AND i.realTix_map='Yes' AND (i.is_real_tix_upload='No' or i.is_real_tix_upload is null) "+
					" AND (i.fedex_label_created=0 OR i.fedex_label_created is NULL) AND i.status='Outstanding' ";
				}else if(orderType.equalsIgnoreCase("PENDING RECIEPT")){
					sql += " AND ((i.realTix_map='Yes' AND i.is_real_tix_upload='Yes' and i.status='Completed'"+
					" AND (i.id not in(select invoice_id from customer_ticket_downloads) AND i.upload_to_exchange=0)) OR i.fedex_label_created=1) ";
				}else if(orderType.equalsIgnoreCase("CLOSED")){
					sql += " AND i.realTix_map='Yes' AND i.is_real_tix_upload='Yes' and i.status='Completed' " +
					"AND (i.id in(select invoice_id from customer_ticket_downloads) OR i.upload_to_exchange=1) ";
				}else if(orderType.equalsIgnoreCase("DISPUTED")){
					sql += " AND i.status='Disputed' ";
				}
				
				if(eventIdStr!=null && !eventIdStr.isEmpty()){
					sql = sql+" AND o.event_id ="+eventIdStr;
				}else {
					if(venueIdStr!=null && !venueIdStr.isEmpty()){
						sql = sql+" AND o.venue_id ="+venueIdStr;
					}else if(artistIdStr!=null && !artistIdStr.isEmpty()){
						sql = sql+" AND o.event_id in (SELECT event_id from event_artist_details WHERE artist_Id ="+artistIdStr+" )";
					}
				}
					
				if(fromDate!=null && !fromDate.isEmpty()){
					sql = sql+" AND o.invoice_Date >='"+fromDate+"'";
				}
				if(toDate!=null && !toDate.isEmpty()){
					sql = sql+" AND o.invoice_Date <='"+toDate+"'";
				}
				
				if(productType!=null && !productType.isEmpty()){
					if(productType.equalsIgnoreCase("REWARDTHEFAN")){
						sql = sql+" AND o.product_type='REWARDTHEFAN' ";
					}else if(productType.equalsIgnoreCase("RTW")){
						sql = sql+" AND o.product_type='RTW' ";
					}else if (productType.equalsIgnoreCase("RTW2")){
						sql = sql+" AND o.product_type='RTW2' ";
					}else if (productType.equalsIgnoreCase("SEATGEEK")){
						sql = sql+" AND o.product_type='SEATGEEK' ";
					}
				}
				
				if(null !=sign ){
					switch (sign) {
						case POSITIVE:
							sql = sql+" AND o.profit_loss > 0 ";
							break;
						case NEGATIVE:
							sql = sql+" AND o.profit_loss < 0 ";
							break;
						case ZERO:
							sql = sql+" AND o.profit_loss = 0 ";
							break;
						case ALL:
							sql = sql+" ";
							break;
						default:
							break;
					}
				}
				if(filter.getInvoiceId() != null){
					sql += " AND o.invoice_no = "+filter.getInvoiceId()+" ";
				}
				if(filter.getOrderType()!=null){
					sql += " AND co.order_type like '%"+filter.getOrderType()+"%' ";
				}
				if(filter.getCustomerOrderId() != null){
					if(productType.equalsIgnoreCase("REWARDTHEFAN")){
						sql += " AND i.order_id = "+filter.getCustomerOrderId()+" ";
					}else if((productType.equalsIgnoreCase("RTW")) || (productType.equalsIgnoreCase("RTW2"))){
						sql += " AND o.ticket_request_id = "+filter.getCustomerOrderId()+" ";
					}else{
						sql += " AND (i.order_id ="+filter.getCustomerOrderId()+" OR o.ticket_request_id ="+filter.getCustomerOrderId()+")";
					}
				}
				if(filter.getInvoiceDateStr() != null){
					if(Util.extractDateElement(filter.getInvoiceDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,o.invoice_date) = "+Util.extractDateElement(filter.getInvoiceDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getInvoiceDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,o.invoice_date) = "+Util.extractDateElement(filter.getInvoiceDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getInvoiceDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,o.invoice_date) = "+Util.extractDateElement(filter.getInvoiceDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getEventName() != null){
					sql += " AND o.event_name like '%"+filter.getEventName()+"%' ";
				}
				if(filter.getEventDateStr() != null){
					if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,o.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,o.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,o.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getEventTimeStr() != null){
					if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
						sql += " AND DATEPART(hour,o.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
					}
					if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
						sql += " AND DATEPART(minute,o.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
					}
				}
				if(filter.getVenueName() != null){
					sql += " AND o.venue_name like '%"+filter.getVenueName()+"%' ";
				}
				if(filter.getCity() != null){
					sql += " AND o.venue_city like '%"+filter.getCity()+"%' ";
				}
				if(filter.getState() != null){
					sql += " AND o.venue_state like '%"+filter.getState()+"%' ";
				}
				if(filter.getCountry() != null){
					sql += " AND o.venue_country like '%"+filter.getCountry()+"%' ";
				}
				if(filter.getProductType() != null){
					sql += " AND o.product_type like '%"+filter.getProductType()+"%' ";
				}
				if(filter.getInternalNotes() != null){
					sql += " AND o.internal_notes like '%"+filter.getInternalNotes()+"%' ";
				}
				if(filter.getQuantity() != null){
					sql += " AND o.sold_qty = "+filter.getQuantity()+" ";
				}
				if(filter.getSection() != null){
					if(orderType.equalsIgnoreCase("OPEN")  || orderType.equalsIgnoreCase("DISPUTED")){
						sql += " AND o.section like '%"+filter.getSection()+"%' ";
					}else if(orderType.equalsIgnoreCase("PENDING SHIPMENT")){
						sql += " AND otg.section like '%"+filter.getSection()+"%' ";
					}else if(orderType.equalsIgnoreCase("PENDING RECIEPT")){
						sql += " AND otg.section like '%"+filter.getSection()+"%' ";
					}else if(orderType.equalsIgnoreCase("CLOSED")){
						sql += " AND otg.section like '%"+filter.getSection()+"%' ";
					}					
				}
				if(filter.getRow() != null){
					if(orderType.equalsIgnoreCase("OPEN")  || orderType.equalsIgnoreCase("DISPUTED")){
						sql += " AND o.row like '%"+filter.getRow()+"%' ";
					}else if(orderType.equalsIgnoreCase("PENDING SHIPMENT")){
						sql += " AND otg.row like '%"+filter.getRow()+"%' ";
					}else if(orderType.equalsIgnoreCase("PENDING RECIEPT")){
						sql += " AND otg.row like '%"+filter.getRow()+"%' ";
					}else if(orderType.equalsIgnoreCase("CLOSED")){
						sql += " AND otg.row like '%"+filter.getRow()+"%' ";
					}					
				}
				if(filter.getActualSoldPrice() != null){
					sql += " AND o.actual_sold_price = "+filter.getActualSoldPrice()+" ";
				}
				if(filter.getMarketPrice() != null){
					sql += " AND o.market_price = "+filter.getMarketPrice()+" ";
				}
				if(filter.getLastUpdatedPrice() != null){
					sql += " AND o.last_updated_price = "+filter.getLastUpdatedPrice()+" "; 	
				}
				if(filter.getSectionTixQty() != null){
					sql += " AND o.section_ticket_qty = "+filter.getSectionTixQty()+" ";
				}
				if(filter.getEventTixQty() != null){
					sql += " AND o.event_ticket_qty = "+filter.getEventTixQty()+" ";
				}
				if(filter.getProfitAndLoss() != null){
					sql += " AND o.profit_loss = "+filter.getProfitAndLoss()+" ";
				}
				
				if(filter.getZone() != null){
					sql += " AND o.zone = "+filter.getZone()+" ";
				}
				if(filter.getZoneTixQty() != null){
					sql += " AND o.zone_ticket_qty = "+filter.getZoneTixQty()+" ";
				}
				if(filter.getZoneCheapestPrice() != null){
					sql += " AND o.zone_cheapest_price = "+filter.getZoneCheapestPrice()+" ";
				}
				if(filter.getZoneTixGroupCount() != null){
					sql += " AND o.zone_ticket_count = "+filter.getZoneTixGroupCount()+" ";
				}
				if(filter.getZoneTotalPrice() != null){
					sql += " AND o.zone_total_price = "+filter.getZoneTotalPrice()+" ";
				}
				if(filter.getZoneProfitAndLoss() != null){
					sql += " AND o.zone_profit_loss = "+filter.getZoneProfitAndLoss()+" ";
				}
				if(filter.getNetTotalSoldPrice() != null){
					sql += " AND o.net_total_sold_price = "+filter.getNetTotalSoldPrice()+" ";
				}
				if(filter.getNetSoldPrice() != null){
					sql += " AND o.net_sold_price = "+filter.getNetSoldPrice()+" ";
				}
				if(filter.getZoneMargin() != null){
					sql += " AND o.zone_margin = "+filter.getZoneMargin()+" ";
				}
				if(filter.getSectionMargin() != null){
					sql += " AND o.section_margin = "+filter.getSectionMargin()+" ";
				}
				
				if(filter.getPriceUpdateCount() != null){
					sql += " AND o.market_price_update_count = "+filter.getPriceUpdateCount()+" ";
				}
				if(filter.getShippingMethod() != null){
					sql += " AND sm.name like '%"+filter.getShippingMethod()+"%' ";
				}
				if(filter.getTrackingNo() != null){
					sql += " AND i.tracking_no = "+filter.getTrackingNo()+" ";
				}
				if(filter.getSecondaryOrderType() != null){
					sql += " AND co.secondary_order_type like '%"+filter.getSecondaryOrderType()+"%' ";
				}
				if(filter.getCustomerName()!=null && !filter.getCustomerName().isEmpty()){
					if(filter.getCustomerName().contains(" ")){
						String names[] = filter.getCustomerName().split(" ");
						sql += " AND (cus.cust_name like '%"+names[0]+"%' AND cus.last_name like '%"+names[1]+"%') ";
					}else{
						sql += " AND (cus.cust_name like '%"+filter.getCustomerName()+"%' OR cus.last_name like '%"+filter.getCustomerName()+"%') ";
					}
					
				}
				if(filter.getSecondaryOrderId() != null){
					sql += " AND co.secondary_order_id = "+filter.getSecondaryOrderId()+" ";
				}
				if(filter.getLastUpdatedDateStr() != null){
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,o.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,o.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,o.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getPurchaseOrderId() != null){
					sql += "AND p.id = "+filter.getPurchaseOrderId()+" ";
				}
				if(filter.getPurchaseOrderDateStr() != null){
					if(Util.extractDateElement(filter.getPurchaseOrderDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,p.created_time) = "+Util.extractDateElement(filter.getPurchaseOrderDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getPurchaseOrderDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,p.created_time) = "+Util.extractDateElement(filter.getPurchaseOrderDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getPurchaseOrderDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,p.created_time) = "+Util.extractDateElement(filter.getPurchaseOrderDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getBrokerId() != null){
					sql += " AND o.broker_id = "+filter.getBrokerId()+" ";
				}
				if(filter.getCompanyName() != null){
					sql += " AND cus.company_name like '%"+filter.getCompanyName()+"%' ";
				}
				if(filter.getPlatform() != null && !filter.getPlatform().isEmpty()){
					if(filter.getPlatform().toUpperCase().contains("WEB")){
						sql += " AND co.platform like '%DESKTOP_SITE%' ";
					}else if(filter.getPlatform().toUpperCase().contains("RETAIL")){
						sql += " AND co.platform like '%TICK_TRACKER%' ";
					}else{
						sql += " AND co.platform like '%"+filter.getPlatform()+"%' ";
					}
				}
				/*if(brokerId != null && brokerId > 0){
					sql += " AND o.broker_id = "+brokerId+" ";
				}*/
				/*
				if(filter.getTotalActualSoldPrice() != null){
					sql += " AND o.event_ticket_qty = "+filter.getTotalActualSoldPrice()+" ";
				}
				if(filter.getTotalMarketPrice() != null){
					sql += " AND o.event_ticket_qty = "+filter.getTotalMarketPrice()+" ";
				}
				if(filter.getPrice() != null){
					sql += " AND o.event_ticket_qty = "+filter.getPrice()+" ";	
				}
				if(filter.getDiscountCouponPrice() != null){
					sql += " AND o.event_ticket_qty = "+filter.getDiscountCouponPrice()+" ";
				}
				if(filter.getUrl() != null){
					sql += " AND o.event_ticket_qty = "+filter.getUrl()+" ";
				}
				*/	
				/*
				if(secondaryOrderId!=null && !secondaryOrderId.isEmpty()){
					sql = sql+" AND co.secondary_order_id='"+secondaryOrderId+"' ";
				}
				
				if(invoiceNo!=null && !invoiceNo.isEmpty()){
					sql = sql+" AND o.invoice_no ="+invoiceNo;
				}
				
				if(orderId != null && !orderId.isEmpty()){
					if(productType.equalsIgnoreCase("REWARDTHEFAN")){
						sql = sql+" AND i.order_id ="+orderId;
					}else if((productType.equalsIgnoreCase("RTW")) || (productType.equalsIgnoreCase("RTW2"))){
						sql = sql+" AND o.ticket_request_id ="+orderId;
					}else{
						sql = sql+" AND (i.order_id ="+orderId+" OR o.ticket_request_id ="+orderId+")";
					}
				}
				*/
				String sortingSql = GridSortingUtil.getOpenOrShipmentPendingOrClosedOrderSortingSql(filter,productType,orderType);
				if(sortingSql!=null && !sortingSql.isEmpty()){
					sql += sortingSql;
				}else{
					sql +=  " Order by  o.id desc";
				}
				
				session  = sessionFactory.openSession();
				SQLQuery query = session.createSQLQuery(sql);
			
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("tmatEventId", Hibernate.INTEGER);
			query.addScalar("exEventId", Hibernate.INTEGER);
			query.addScalar("sectionTixQty", Hibernate.INTEGER);
			query.addScalar("eventTixQty", Hibernate.INTEGER);
			query.addScalar("ticketRequestId", Hibernate.INTEGER);
			query.addScalar("section", Hibernate.STRING);
			query.addScalar("marketPrice", Hibernate.DOUBLE);
			query.addScalar("lastUpdatedPrice", Hibernate.DOUBLE);
			query.addScalar("lastCrawlTime", Hibernate.TIMESTAMP);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("lastUpdated", Hibernate.TIMESTAMP);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("category", Hibernate.STRING);
			query.addScalar("brokerId", Hibernate.INTEGER);
			query.addScalar("invoiceNo", Hibernate.INTEGER);
			query.addScalar("invoiceDate", Hibernate.TIMESTAMP);
			query.addScalar("venueId", Hibernate.INTEGER);
			query.addScalar("soldQty", Hibernate.INTEGER);
			query.addScalar("ticketGroupId", Hibernate.INTEGER);
			query.addScalar("brokerName", Hibernate.STRING);
			query.addScalar("invoiceType", Hibernate.STRING);
			query.addScalar("salesPerson", Hibernate.STRING);
			query.addScalar("eventName", Hibernate.STRING);
			query.addScalar("eventDate", Hibernate.DATE);
			query.addScalar("eventTime", Hibernate.TIME);
			query.addScalar("venueName", Hibernate.STRING);
			query.addScalar("venueCity", Hibernate.STRING);
			query.addScalar("venueState", Hibernate.STRING);
			query.addScalar("venueCountry", Hibernate.STRING);
			query.addScalar("internalNotes", Hibernate.STRING);
			query.addScalar("customerName", Hibernate.STRING);
			query.addScalar("retailPrice", Hibernate.DOUBLE);
			query.addScalar("wholesalePrice", Hibernate.DOUBLE);
			query.addScalar("cost", Hibernate.DOUBLE);
			query.addScalar("actualSoldPrice", Hibernate.DOUBLE);
			query.addScalar("totalPrice", Hibernate.DOUBLE);
			query.addScalar("row", Hibernate.STRING);
			query.addScalar("profitAndLoss", Hibernate.DOUBLE);
			query.addScalar("priceUpdateCount", Hibernate.INTEGER);
			query.addScalar("onlinePrice", Hibernate.DOUBLE);
			query.addScalar("productType", Hibernate.STRING);
			query.addScalar("poId", Hibernate.INTEGER);
			query.addScalar("poDate", Hibernate.TIMESTAMP);
			query.addScalar("shippingMethod", Hibernate.STRING);
			query.addScalar("trackingNo", Hibernate.STRING);
			query.addScalar("secondaryOrderType", Hibernate.STRING);
			query.addScalar("orderId", Hibernate.INTEGER);
			query.addScalar("secondaryOrderId", Hibernate.STRING);
			query.addScalar("zone", Hibernate.STRING);
			query.addScalar("zoneTixQty", Hibernate.INTEGER);
			query.addScalar("zoneCheapestPrice", Hibernate.DOUBLE);
			query.addScalar("zoneTixGroupCount", Hibernate.INTEGER);
			query.addScalar("netTotalSoldPrice", Hibernate.DOUBLE);
			query.addScalar("netSoldPrice", Hibernate.DOUBLE);
			query.addScalar("sectionMargin", Hibernate.DOUBLE);
			query.addScalar("zoneTotalPrice", Hibernate.DOUBLE);
			query.addScalar("zoneProfitAndLoss", Hibernate.DOUBLE);
			query.addScalar("zoneMargin", Hibernate.DOUBLE);
			query.addScalar("companyName", Hibernate.STRING);
			query.addScalar("platform", Hibernate.STRING);
			query.addScalar("orderType", Hibernate.STRING);
			
			if(orderType.equalsIgnoreCase("PENDING SHIPMENT")){
				isCount = true;
			}else if(orderType.equalsIgnoreCase("PENDING RECIEPT")){
				isCount = true;
			}else if(orderType.equalsIgnoreCase("CLOSED")){
				isCount = true;
			}
			
			if(isCount){
				orders = query.setResultTransformer(Transformers.aliasToBean(OpenOrderStatus.class)).list();
			}else{
				startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
				orders = query.setResultTransformer(Transformers.aliasToBean(OpenOrderStatus.class)).setFirstResult(startFrom)
				.setMaxResults(PaginationUtil.PAGESIZE).list();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return orders;
	}
	

	public List<OpenOrderStatus> getOpenOrShipmentPendingOrClosedOrdersToExport(String invoiceNo,String eventIdStr, String artistIdStr, 
			String venueIdStr, String fromDate,String toDate, ProfitLossSign sign,String orderType,String productType,String 
			secondaryOrderId,String orderId,GridHeaderFilters filter,Integer brokerId){
		List<OpenOrderStatus> orders= null;
		Session session =null;
		try {
			String sql = "select o.id AS id,o.event_id AS tmatEventId,o.tn_exchange_event_id AS exEventId,"+
				"o.section_ticket_qty AS sectionTixQty,o.event_ticket_qty AS eventTixQty,"+
				"o.market_price AS marketPrice,o.last_updated_price AS lastUpdatedPrice,"+
				"o.last_crawl_time AS lastCrawlTime,o.created_date AS createdDate,o.last_updated AS lastUpdated,"+
				"o.status AS status,o.category AS category,o.broker_id AS brokerId,o.ticket_request_id AS ticketRequestId,"+
				"o.invoice_no AS invoiceNo,o.invoice_date AS invoiceDate,o.venue_id AS venueId,co.secondary_order_id AS secondaryOrderId,"+
				"o.sold_qty AS soldQty,o.ticket_group_id AS ticketGroupId,o.broker_name AS brokerName,"+
				"o.invoice_type AS invoiceType,o.sales_person AS salesPerson,o.event_name AS eventName,"+
				"o.event_date AS eventDate,o.event_time AS eventTime,o.venue_name AS venueName,o.venue_city AS venueCity,"+
				"o.venue_state AS venueState,o.venue_country AS venueCountry,o.internal_notes AS internalNotes,"+
				"o.retail_price AS retailPrice,o.wholesale_price AS wholesalePrice,o.cost AS cost,co.secondary_order_type AS secondaryOrderType,"+
				"o.actual_sold_price AS actualSoldPrice,o.total_price AS totalPrice,sm.name AS shippingMethod,"+
				"o.profit_loss AS profitAndLoss,o.market_price_update_count AS priceUpdateCount,i.tracking_no AS trackingNo, "+
				"o.online_price AS onlinePrice,o.product_type AS productType,i.order_id AS orderId, " +
				" o.zone as zone,o.zone_ticket_qty as zoneTixQty,CONCAT(cus.cust_name,' ',cus.last_name) AS customerName," +
				" o.zone_cheapest_price as zoneCheapestPrice,o.zone_ticket_count as zoneTixGroupCount, " +
				" o.net_total_sold_price as netTotalSoldPrice,o.net_sold_price as netSoldPrice, o.zone_total_price as zoneTotalPrice, " +
				" o.zone_profit_loss as zoneProfitAndLoss,o.zone_margin as zoneMargin,section_margin as sectionMargin,  "+
				" p.id as poId,p.created_time as poDate, "+
				"cus.company_name as companyName, co.platform as platform,co.order_type as orderType, ";
				
				if(orderType.equalsIgnoreCase("OPEN")  || orderType.equalsIgnoreCase("DISPUTED")){
					sql += "o.section AS section,o.row AS row ";
				}else if(orderType.equalsIgnoreCase("PENDING SHIPMENT")){
					sql += "otg.section AS section,otg.row AS row ";
				}else if(orderType.equalsIgnoreCase("PENDING RECIEPT")){
					sql += "otg.section AS section,otg.row AS row ";
				}else if(orderType.equalsIgnoreCase("CLOSED")){
					sql += "otg.section AS section,otg.row AS row ";
				}
				
				sql += "from tix_open_order_status o with(nolock) left join invoice i with(nolock) on o.invoice_no=i.id "+
				"left join customer_order co with(nolock) on co.id=i.order_id "+
				"left join customer cus with(nolock) on cus.id=i.customer_id "+
				"left join purchase_order p with(nolock) on i.purchase_order_no=p.id "+
				"left join shipping_method sm with(nolock) on(i.shipping_method_id=sm.id) ";
				
				if(orderType.equalsIgnoreCase("PENDING SHIPMENT")){
					sql += " left join order_ticket_group_details otg with(nolock) on(otg.order_id=co.id) ";
				}else if(orderType.equalsIgnoreCase("PENDING RECIEPT")){
					sql += " left join order_ticket_group_details otg with(nolock) on(otg.order_id=co.id) ";
				}else if(orderType.equalsIgnoreCase("CLOSED")){
					sql += " left join order_ticket_group_details otg with(nolock) on(otg.order_id=co.id) ";
				}
				sql += "WHERE o.status='ACTIVE' ";
				
				if(orderType.equalsIgnoreCase("OPEN")){
					sql += " AND (i.is_real_tix_upload='No' OR i.is_real_tix_upload IS NULL) AND (i.fedex_label_created=0 OR i.fedex_label_created is NULL) " +
							"AND (i.realTix_map='No' or i.realTix_map is null) AND (i.status='Outstanding' OR i.status is NULL)";
				}else if(orderType.equalsIgnoreCase("PENDING SHIPMENT")){
					sql += " AND i.realTix_map='Yes' AND (i.is_real_tix_upload='No' or i.is_real_tix_upload is null) "+
					" AND (i.fedex_label_created=0 OR i.fedex_label_created is NULL) AND i.status='Outstanding' ";
				}else if(orderType.equalsIgnoreCase("PENDING RECIEPT")){
					sql += " AND ((i.realTix_map='Yes' AND i.is_real_tix_upload='Yes' and i.status='Completed'"+
					" AND (i.id not in(select invoice_id from customer_ticket_downloads) AND i.upload_to_exchange=0)) OR i.fedex_label_created=1) ";
				}else if(orderType.equalsIgnoreCase("CLOSED")){
					sql += " AND i.realTix_map='Yes' AND i.is_real_tix_upload='Yes' and i.status='Completed' " +
					"AND (i.id in(select invoice_id from customer_ticket_downloads) OR i.upload_to_exchange=1) ";
				}else if(orderType.equalsIgnoreCase("DISPUTED")){
					sql += " AND i.status='Disputed' ";
				}
				
				if(eventIdStr!=null && !eventIdStr.isEmpty()){
					sql = sql+" AND o.event_id ="+eventIdStr;
				}else {
					if(venueIdStr!=null && !venueIdStr.isEmpty()){
						sql = sql+" AND o.venue_id ="+venueIdStr;
					}else if(artistIdStr!=null && !artistIdStr.isEmpty()){
						sql = sql+" AND o.event_id in (SELECT event_id from event_artist_details WHERE artist_Id ="+artistIdStr+" )";
					}
				}
					
				if(fromDate!=null && !fromDate.isEmpty()){
					sql = sql+" AND o.invoice_Date >='"+fromDate+"'";
				}
				if(toDate!=null && !toDate.isEmpty()){
					sql = sql+" AND o.invoice_Date <='"+toDate+"'";
				}
				
				if(productType!=null && !productType.isEmpty()){
					if(productType.equalsIgnoreCase("REWARDTHEFAN")){
						sql = sql+" AND o.product_type='REWARDTHEFAN' ";
					}else if(productType.equalsIgnoreCase("RTW")){
						sql = sql+" AND o.product_type='RTW' ";
					}else if (productType.equalsIgnoreCase("RTW2")){
						sql = sql+" AND o.product_type='RTW2' ";
					}else if (productType.equalsIgnoreCase("SEATGEEK")){
						sql = sql+" AND o.product_type='SEATGEEK' ";
					}
				}
				if(secondaryOrderId!=null && !secondaryOrderId.isEmpty()){
					sql = sql+" AND co.secondary_order_id='"+secondaryOrderId+"' ";
				}
				
				if(invoiceNo!=null && !invoiceNo.isEmpty()){
					sql = sql+" AND o.invoice_no ="+invoiceNo;
				}
				
				if(orderId != null && !orderId.isEmpty()){
					if(productType.equalsIgnoreCase("REWARDTHEFAN")){
						sql = sql+" AND i.order_id ="+orderId;
					}else if((productType.equalsIgnoreCase("RTW")) || (productType.equalsIgnoreCase("RTW2"))){
						sql = sql+" AND o.ticket_request_id ="+orderId;
					}else{
						sql = sql+" AND (i.order_id ="+orderId+" OR o.ticket_request_id ="+orderId+")";
					}
				}
				
				if(null !=sign ){
					
					switch (sign) {
						case POSITIVE:
							sql = sql+" AND o.profit_loss > 0 ";
							break;
						case NEGATIVE:
							sql = sql+" AND o.profit_loss < 0 ";
							break;
						case ZERO:
							sql = sql+" AND o.profit_loss = 0 ";
							break;
						case ALL:
							sql = sql+" ";
							break;
						default:
							break;
					}
				}
				if(filter.getInvoiceId() != null){
					sql += " AND o.invoice_no = "+filter.getInvoiceId()+" ";
				}
				if(filter.getOrderType()!=null){
					sql += " AND co.order_type like '%"+filter.getOrderType()+"%' ";
				}
				if(filter.getCustomerOrderId() != null){
					if(productType.equalsIgnoreCase("REWARDTHEFAN")){
						sql += " AND i.order_id = "+filter.getCustomerOrderId()+" ";
					}else if((productType.equalsIgnoreCase("RTW")) || (productType.equalsIgnoreCase("RTW2"))){
						sql += " AND o.ticket_request_id = "+filter.getCustomerOrderId()+" ";
					}else{
						sql += " AND (i.order_id ="+filter.getCustomerOrderId()+" OR o.ticket_request_id ="+filter.getCustomerOrderId()+")";
					}
				}
				if(filter.getInvoiceDateStr() != null){
					if(Util.extractDateElement(filter.getInvoiceDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,o.invoice_date) = "+Util.extractDateElement(filter.getInvoiceDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getInvoiceDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,o.invoice_date) = "+Util.extractDateElement(filter.getInvoiceDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getInvoiceDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,o.invoice_date) = "+Util.extractDateElement(filter.getInvoiceDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getEventName() != null){
					sql += " AND o.event_name like '%"+filter.getEventName()+"%' ";
				}
				if(filter.getEventDateStr() != null){
					if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,o.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,o.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,o.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getEventTimeStr() != null){
					if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
						sql += " AND DATEPART(hour,o.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
					}
					if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
						sql += " AND DATEPART(minute,o.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
					}
				}
				if(filter.getVenueName() != null){
					sql += " AND o.venue_name like '%"+filter.getVenueName()+"%' ";
				}
				if(filter.getCity() != null){
					sql += " AND o.venue_city like '%"+filter.getCity()+"%' ";
				}
				if(filter.getState() != null){
					sql += " AND o.venue_state like '%"+filter.getState()+"%' ";
				}
				if(filter.getCountry() != null){
					sql += " AND o.venue_country like '%"+filter.getCountry()+"%' ";
				}
				if(filter.getProductType() != null){
					sql += " AND o.product_type like '%"+filter.getProductType()+"%' ";
				}
				if(filter.getInternalNotes() != null){
					sql += " AND o.internal_notes like '%"+filter.getInternalNotes()+"%' ";
				}
				if(filter.getQuantity() != null){
					sql += " AND o.sold_qty = "+filter.getQuantity()+" ";
				}
				if(filter.getSection() != null){
					if(orderType.equalsIgnoreCase("OPEN")  || orderType.equalsIgnoreCase("DISPUTED")){
						sql += " AND o.section like '%"+filter.getSection()+"%' ";
					}else if(orderType.equalsIgnoreCase("PENDING SHIPMENT")){
						sql += " AND otg.section like '%"+filter.getSection()+"%' ";
					}else if(orderType.equalsIgnoreCase("PENDING RECIEPT")){
						sql += " AND otg.section like '%"+filter.getSection()+"%' ";
					}else if(orderType.equalsIgnoreCase("CLOSED")){
						sql += " AND otg.section like '%"+filter.getSection()+"%' ";
					}					
				}
				if(filter.getRow() != null){
					if(orderType.equalsIgnoreCase("OPEN")  || orderType.equalsIgnoreCase("DISPUTED")){
						sql += " AND o.row like '%"+filter.getRow()+"%' ";
					}else if(orderType.equalsIgnoreCase("PENDING SHIPMENT")){
						sql += " AND otg.row like '%"+filter.getRow()+"%' ";
					}else if(orderType.equalsIgnoreCase("PENDING RECIEPT")){
						sql += " AND otg.row like '%"+filter.getRow()+"%' ";
					}else if(orderType.equalsIgnoreCase("CLOSED")){
						sql += " AND otg.row like '%"+filter.getRow()+"%' ";
					}					
				}
				if(filter.getActualSoldPrice() != null){
					sql += " AND o.actual_sold_price = "+filter.getActualSoldPrice()+" ";
				}
				if(filter.getMarketPrice() != null){
					sql += " AND o.market_price = "+filter.getMarketPrice()+" ";
				}
				if(filter.getLastUpdatedPrice() != null){
					sql += " AND o.last_updated_price = "+filter.getLastUpdatedPrice()+" "; 	
				}
				if(filter.getSectionTixQty() != null){
					sql += " AND o.section_ticket_qty = "+filter.getSectionTixQty()+" ";
				}
				if(filter.getEventTixQty() != null){
					sql += " AND o.event_ticket_qty = "+filter.getEventTixQty()+" ";
				}
				if(filter.getProfitAndLoss() != null){
					sql += " AND o.profit_loss = "+filter.getProfitAndLoss()+" ";
				}
				
				if(filter.getZone() != null){
					sql += " AND o.zone = "+filter.getZone()+" ";
				}
				if(filter.getZoneTixQty() != null){
					sql += " AND o.zone_ticket_qty = "+filter.getZoneTixQty()+" ";
				}
				if(filter.getZoneCheapestPrice() != null){
					sql += " AND o.zone_cheapest_price = "+filter.getZoneCheapestPrice()+" ";
				}
				if(filter.getZoneTixGroupCount() != null){
					sql += " AND o.zone_ticket_count = "+filter.getZoneTixGroupCount()+" ";
				}
				if(filter.getZoneTotalPrice() != null){
					sql += " AND o.zone_total_price = "+filter.getZoneTotalPrice()+" ";
				}
				if(filter.getZoneProfitAndLoss() != null){
					sql += " AND o.zone_profit_loss = "+filter.getZoneProfitAndLoss()+" ";
				}
				if(filter.getNetTotalSoldPrice() != null){
					sql += " AND o.net_total_sold_price = "+filter.getNetTotalSoldPrice()+" ";
				}
				if(filter.getNetSoldPrice() != null){
					sql += " AND o.net_sold_price = "+filter.getNetSoldPrice()+" ";
				}
				if(filter.getZoneMargin() != null){
					sql += " AND o.zone_margin = "+filter.getZoneMargin()+" ";
				}
				if(filter.getSectionMargin() != null){
					sql += " AND o.section_margin = "+filter.getSectionMargin()+" ";
				}
				if(filter.getPriceUpdateCount() != null){
					sql += " AND o.market_price_update_count = "+filter.getPriceUpdateCount()+" ";
				}
				if(filter.getShippingMethod() != null){
					sql += " AND sm.name like '%"+filter.getShippingMethod()+"%' ";
				}
				if(filter.getTrackingNo() != null){
					sql += " AND i.tracking_no = "+filter.getTrackingNo()+" ";
				}
				if(filter.getSecondaryOrderType() != null){
					sql += " AND co.secondary_order_type like '%"+filter.getSecondaryOrderType()+"%' ";
				}
				if(filter.getCustomerName()!=null && !filter.getCustomerName().isEmpty()){
					if(filter.getCustomerName().contains(" ")){
						String names[] = filter.getCustomerName().split(" ");
						sql += " AND (cus.cust_name like '%"+names[0]+"%' AND cus.last_name like '%"+names[1]+"%') ";
					}else{
						sql += " AND (cus.cust_name like '%"+filter.getCustomerName()+"%' OR cus.last_name like '%"+filter.getCustomerName()+"%') ";
					}
					
				}
				if(filter.getSecondaryOrderId() != null){
					sql += " AND co.secondary_order_id = "+filter.getSecondaryOrderId()+" ";
				}
				if(filter.getLastUpdatedDateStr() != null){
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,o.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,o.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,o.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getPurchaseOrderId() != null){
					sql += "AND p.id = "+filter.getPurchaseOrderId()+" ";
				}
				if(filter.getPurchaseOrderDateStr() != null){
					if(Util.extractDateElement(filter.getPurchaseOrderDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,p.created_time) = "+Util.extractDateElement(filter.getPurchaseOrderDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getPurchaseOrderDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,p.created_time) = "+Util.extractDateElement(filter.getPurchaseOrderDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getPurchaseOrderDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,p.created_time) = "+Util.extractDateElement(filter.getPurchaseOrderDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getBrokerId() != null){
					sql += " AND o.broker_id = "+filter.getBrokerId()+" ";
				}
				if(filter.getCompanyName() != null){
					sql += " AND cus.company_name like '%"+filter.getCompanyName()+"%' ";
				}
				if(filter.getPlatform() != null && !filter.getPlatform().isEmpty()){
					if(filter.getPlatform().toUpperCase().contains("WEB")){
						sql += " AND co.platform like '%DESKTOP_SITE%' ";
					}else if(filter.getPlatform().toUpperCase().contains("RETAIL")){
						sql += " AND co.platform like '%TICK_TRACKER%' ";
					}else{
						sql += " AND co.platform like '%"+filter.getPlatform()+"%' ";
					}
				}
				/*if(brokerId != null && brokerId > 0){
					sql += " AND o.broker_id = "+brokerId+" ";
				}*/
				sql +=  " Order by  o.id desc";
				session  = sessionFactory.openSession();
				SQLQuery query = session.createSQLQuery(sql);
			
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("tmatEventId", Hibernate.INTEGER);
			query.addScalar("exEventId", Hibernate.INTEGER);
			query.addScalar("sectionTixQty", Hibernate.INTEGER);
			query.addScalar("eventTixQty", Hibernate.INTEGER);
			query.addScalar("ticketRequestId", Hibernate.INTEGER);
			query.addScalar("section", Hibernate.STRING);
			query.addScalar("marketPrice", Hibernate.DOUBLE);
			query.addScalar("lastUpdatedPrice", Hibernate.DOUBLE);
			query.addScalar("lastCrawlTime", Hibernate.TIMESTAMP);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("lastUpdated", Hibernate.TIMESTAMP);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("category", Hibernate.STRING);
			query.addScalar("brokerId", Hibernate.INTEGER);
			query.addScalar("invoiceNo", Hibernate.INTEGER);
			query.addScalar("invoiceDate", Hibernate.TIMESTAMP);
			query.addScalar("venueId", Hibernate.INTEGER);
			query.addScalar("soldQty", Hibernate.INTEGER);
			query.addScalar("ticketGroupId", Hibernate.INTEGER);
			query.addScalar("brokerName", Hibernate.STRING);
			query.addScalar("customerName", Hibernate.STRING);
			query.addScalar("invoiceType", Hibernate.STRING);
			query.addScalar("salesPerson", Hibernate.STRING);
			query.addScalar("eventName", Hibernate.STRING);
			query.addScalar("eventDate", Hibernate.DATE);
			query.addScalar("eventTime", Hibernate.TIME);
			query.addScalar("venueName", Hibernate.STRING);
			query.addScalar("venueCity", Hibernate.STRING);
			query.addScalar("venueState", Hibernate.STRING);
			query.addScalar("venueCountry", Hibernate.STRING);
			query.addScalar("internalNotes", Hibernate.STRING);
			query.addScalar("retailPrice", Hibernate.DOUBLE);
			query.addScalar("wholesalePrice", Hibernate.DOUBLE);
			query.addScalar("cost", Hibernate.DOUBLE);
			query.addScalar("actualSoldPrice", Hibernate.DOUBLE);
			query.addScalar("totalPrice", Hibernate.DOUBLE);
			query.addScalar("row", Hibernate.STRING);
			query.addScalar("profitAndLoss", Hibernate.DOUBLE);
			query.addScalar("priceUpdateCount", Hibernate.INTEGER);
			query.addScalar("onlinePrice", Hibernate.DOUBLE);
			query.addScalar("productType", Hibernate.STRING);
			//query.addScalar("poId", Hibernate.INTEGER);
			//query.addScalar("poDate", Hibernate.TIMESTAMP);
			query.addScalar("shippingMethod", Hibernate.STRING);
			query.addScalar("trackingNo", Hibernate.STRING);
			query.addScalar("secondaryOrderType", Hibernate.STRING);
			query.addScalar("orderId", Hibernate.INTEGER);
			query.addScalar("secondaryOrderId", Hibernate.STRING);
			query.addScalar("zone", Hibernate.STRING);
			query.addScalar("zoneTixQty", Hibernate.INTEGER);
			query.addScalar("zoneCheapestPrice", Hibernate.DOUBLE);
			query.addScalar("zoneTixGroupCount", Hibernate.INTEGER);
			query.addScalar("netTotalSoldPrice", Hibernate.DOUBLE);
			query.addScalar("netSoldPrice", Hibernate.DOUBLE);
			query.addScalar("sectionMargin", Hibernate.DOUBLE);
			query.addScalar("zoneTotalPrice", Hibernate.DOUBLE);
			query.addScalar("zoneProfitAndLoss", Hibernate.DOUBLE);
			query.addScalar("zoneMargin", Hibernate.DOUBLE);
			query.addScalar("companyName", Hibernate.STRING);
			query.addScalar("platform", Hibernate.STRING);
			query.addScalar("orderType", Hibernate.STRING);
			
			orders = query.setResultTransformer(Transformers.aliasToBean(OpenOrderStatus.class)).setMaxResults(65535).list();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return orders;
	}
	
	public List<OpenOrderStatus> getRTWRTW2OrdersToExport(String invoiceNo,String eventIdStr, String artistIdStr, 
			String venueIdStr, String fromDate,String toDate, ProfitLossSign sign,String orderType,String productType,String 
			secondaryOrderId,String orderId,GridHeaderFilters filter){
		List<OpenOrderStatus> orders= null;
		Session session =null;
		try {
			String sql = "select invoiceNo as id,* from pos_filled_orders with(nolock) WHERE status='"+orderType+"' ";
				
				if(eventIdStr!=null && !eventIdStr.isEmpty()){
					sql = sql+" AND tmatEventId ="+eventIdStr;
				}else {
					if(venueIdStr!=null && !venueIdStr.isEmpty()){
						sql = sql+" AND venueId ="+venueIdStr;
					}else if(artistIdStr!=null && !artistIdStr.isEmpty()){
						sql = sql+" AND tmatEventId in (SELECT event_id from event_artist_details WHERE artist_Id ="+artistIdStr+" )";
					}
				}					
				if(fromDate!=null && !fromDate.isEmpty()){
					sql = sql+" AND invoiceDate >='"+fromDate+"'";
				}
				if(toDate!=null && !toDate.isEmpty()){
					sql = sql+" AND invoiceDate <='"+toDate+"'";
				}				
				if(productType!=null && !productType.isEmpty()){
					if(productType.equalsIgnoreCase("RTW")){
						sql = sql+" AND productType='RTW' ";
					}else if (productType.equalsIgnoreCase("RTW2")){
						sql = sql+" AND producTtype='RTW2' ";
					}
				}
				if(invoiceNo!=null && !invoiceNo.isEmpty()){
					sql = sql+" AND invoiceNo ="+invoiceNo;
				}				
				if(orderId != null && !orderId.isEmpty()){
					sql = sql+" AND ticketRequestId ="+orderId;
				}
				
				if(filter.getInvoiceId() != null){
					sql += " AND invoiceNo = "+filter.getInvoiceId();
				}
				if(filter.getPurchaseOrderId() != null){
					sql += " AND poId = "+filter.getPurchaseOrderId();
				}
				if(filter.getCustomerOrderId() != null){
					sql += " AND ticketRequestId = "+filter.getCustomerOrderId();
				}
				if(filter.getInvoiceDateStr() != null){
					if(Util.extractDateElement(filter.getInvoiceDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,invoiceDate) = "+Util.extractDateElement(filter.getInvoiceDateStr(),"DAY");
					}
					if(Util.extractDateElement(filter.getInvoiceDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,invoiceDate) = "+Util.extractDateElement(filter.getInvoiceDateStr(),"MONTH");
					}
					if(Util.extractDateElement(filter.getInvoiceDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,invoiceDate) = "+Util.extractDateElement(filter.getInvoiceDateStr(),"YEAR");
					}
				}
				if(filter.getPurchaseOrderDateStr() != null){
					if(Util.extractDateElement(filter.getPurchaseOrderDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,poDate) = "+Util.extractDateElement(filter.getPurchaseOrderDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getPurchaseOrderDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,poDate) = "+Util.extractDateElement(filter.getPurchaseOrderDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getPurchaseOrderDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,poDate) = "+Util.extractDateElement(filter.getPurchaseOrderDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getEventName() != null){
					sql += " AND eventName like '%"+filter.getEventName()+"%' ";
				}
				if(filter.getEventDateStr() != null){
					if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY");
					}
					if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH");
					}
					if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,eventDate) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR");
					}
				}
				if(filter.getEventTimeStr() != null){
					if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
						sql += " AND DATEPART(hour,eventTime) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR");
					}
					if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
						sql += " AND DATEPART(minute,eventTime) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE");
					}
				}
				if(filter.getVenueName() != null){
					sql += " AND venueName like '%"+filter.getVenueName()+"%' ";
				}
				if(filter.getCity() != null){
					sql += " AND venueCity like '%"+filter.getCity()+"%' ";
				}
				if(filter.getState() != null){
					sql += " AND venueState like '%"+filter.getState()+"%' ";
				}
				if(filter.getCountry() != null){
					sql += " AND venueCountry like '%"+filter.getCountry()+"%' ";
				}
				if(filter.getProductType() != null){
					sql += " AND productType like '%"+filter.getProductType()+"%' ";
				}
				if(filter.getInternalNotes() != null){
					sql += " AND internalNotes like '%"+filter.getInternalNotes()+"%' ";
				}
				if(filter.getQuantity() != null){
					sql += " AND soldQty = "+filter.getQuantity()+" ";
				}
				if(filter.getSection() != null){
					sql += " AND section like '%"+filter.getSection()+"%' ";
				}
				if(filter.getRow() != null){
					sql += " AND row like '%"+filter.getRow()+"%' ";
				}
				if(filter.getActualSoldPrice() != null){
					sql += " AND actualSoldPrice = "+filter.getActualSoldPrice()+" ";
				}
				/*if(null !=sign ){
					switch (sign) {
						case POSITIVE:
							sql = sql+" AND o.profit_loss > 0 ";
							break;
						case NEGATIVE:
							sql = sql+" AND o.profit_loss < 0 ";
							break;
						case ZERO:
							sql = sql+" AND o.profit_loss = 0 ";
							break;
						case ALL:
							sql = sql+" ";
							break;
						default:
							break;
					}
				}*/
				sql +=  " Order by ticketRequestId desc";
				session  = sessionFactory.openSession();
				SQLQuery query = session.createSQLQuery(sql);
			
				query.addScalar("id", Hibernate.INTEGER);
				query.addScalar("tmatEventId", Hibernate.INTEGER);
				query.addScalar("exEventId", Hibernate.INTEGER);
				//query.addScalar("sectionTixQty", Hibernate.INTEGER);
				//query.addScalar("eventTixQty", Hibernate.INTEGER);
				query.addScalar("ticketRequestId", Hibernate.INTEGER);
				query.addScalar("section", Hibernate.STRING);
				//query.addScalar("marketPrice", Hibernate.DOUBLE);
				//query.addScalar("lastUpdatedPrice", Hibernate.DOUBLE);
				//query.addScalar("lastCrawlTime", Hibernate.TIMESTAMP);
				//query.addScalar("createdDate", Hibernate.TIMESTAMP);
				//query.addScalar("lastUpdated", Hibernate.TIMESTAMP);
				query.addScalar("status", Hibernate.STRING);
				query.addScalar("poId", Hibernate.INTEGER);
				query.addScalar("poDate", Hibernate.TIMESTAMP);
				//query.addScalar("category", Hibernate.STRING);
				//query.addScalar("brokerId", Hibernate.INTEGER);
				query.addScalar("invoiceNo", Hibernate.INTEGER);
				query.addScalar("invoiceDate", Hibernate.TIMESTAMP);
				query.addScalar("venueId", Hibernate.INTEGER);
				query.addScalar("soldQty", Hibernate.INTEGER);
				query.addScalar("ticketGroupId", Hibernate.INTEGER);
				query.addScalar("brokerName", Hibernate.STRING);
				query.addScalar("invoiceType", Hibernate.STRING);
				query.addScalar("salesPerson", Hibernate.STRING);
				query.addScalar("eventName", Hibernate.STRING);
				query.addScalar("eventDate", Hibernate.DATE);
				query.addScalar("eventTime", Hibernate.TIME);
				query.addScalar("venueName", Hibernate.STRING);
				query.addScalar("venueCity", Hibernate.STRING);
				query.addScalar("venueState", Hibernate.STRING);
				query.addScalar("venueCountry", Hibernate.STRING);
				query.addScalar("internalNotes", Hibernate.STRING);
				query.addScalar("retailPrice", Hibernate.DOUBLE);
				query.addScalar("wholesalePrice", Hibernate.DOUBLE);
				query.addScalar("cost", Hibernate.DOUBLE);
				query.addScalar("actualSoldPrice", Hibernate.DOUBLE);
				query.addScalar("totalPrice", Hibernate.DOUBLE);
				query.addScalar("row", Hibernate.STRING);
				query.addScalar("profitAndLoss", Hibernate.DOUBLE);
				query.addScalar("priceUpdateCount", Hibernate.INTEGER);
				query.addScalar("onlinePrice", Hibernate.DOUBLE);
				query.addScalar("productType", Hibernate.STRING);
				query.addScalar("shippingMethod", Hibernate.STRING);
				query.addScalar("trackingNo", Hibernate.STRING);
				query.addScalar("orderId", Hibernate.INTEGER);
				orders = query.setResultTransformer(Transformers.aliasToBean(OpenOrderStatus.class)).setMaxResults(65535).list();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return orders;
	}

	public Integer getOpenOrShipmentPendingOrClosedOrdersCount(String eventIdStr, String artistIdStr, 
			String venueIdStr, String fromDate,String toDate, ProfitLossSign sign,String orderType,String productType,GridHeaderFilters filter,Integer brokerId){
		Session session =null;
		Integer count=0;
		try {
			String sql = "select count(o.id) "+
				"from tix_open_order_status o with(nolock) left join invoice i with(nolock) on o.invoice_no=i.id "+
				"left join customer_order co with(nolock) on co.id=i.order_id "+
				"left join customer cus with(nolock) on cus.id=i.customer_id "+
				"left join purchase_order p with(nolock) on i.purchase_order_no=p.id "+
				"left join shipping_method sm with(nolock) on(i.shipping_method_id=sm.id) ";
				
				if(orderType.equalsIgnoreCase("PENDING SHIPMENT")){
					sql += " left join order_ticket_group_details otg with(nolock) on(otg.order_id=co.id) ";
				}else if(orderType.equalsIgnoreCase("PENDING RECIEPT")){
					sql += " left join order_ticket_group_details otg with(nolock) on(otg.order_id=co.id) ";
				}else if(orderType.equalsIgnoreCase("CLOSED")){
					sql += " left join order_ticket_group_details otg with(nolock) on(otg.order_id=co.id) ";
				}
				sql += "WHERE o.status='ACTIVE' ";
				
				if(orderType.equalsIgnoreCase("OPEN")){
					sql += " AND (i.is_real_tix_upload='No' OR i.is_real_tix_upload IS NULL) AND (i.fedex_label_created=0 OR i.fedex_label_created is NULL) " +
							"AND (i.realTix_map='No' or i.realTix_map is null) AND (i.status='Outstanding' OR i.status is NULL)";
				}else if(orderType.equalsIgnoreCase("PENDING SHIPMENT")){
					sql += " AND i.realTix_map='Yes' AND (i.is_real_tix_upload='No' or i.is_real_tix_upload is null) "+
					" AND (i.fedex_label_created=0 OR i.fedex_label_created is NULL) AND i.status='Outstanding' ";
				}else if(orderType.equalsIgnoreCase("PENDING RECIEPT")){
					sql += " AND ((i.realTix_map='Yes' AND i.is_real_tix_upload='Yes' and i.status='Completed'"+
					" AND (i.id not in(select invoice_id from customer_ticket_downloads) AND i.upload_to_exchange=0)) OR i.fedex_label_created=1) ";
				}else if(orderType.equalsIgnoreCase("CLOSED")){
					sql += " AND i.realTix_map='Yes' AND i.is_real_tix_upload='Yes' and i.status='Completed' " +
					"AND (i.id in(select invoice_id from customer_ticket_downloads) OR i.upload_to_exchange=1) ";
				}else if(orderType.equalsIgnoreCase("DISPUTED")){
					sql += " AND i.status='Disputed' ";
				}
				
				if(eventIdStr!=null && !eventIdStr.isEmpty()){
					sql = sql+" AND o.event_id ="+eventIdStr;
				}else {
					if(venueIdStr!=null && !venueIdStr.isEmpty()){
						sql = sql+" AND o.venue_id ="+venueIdStr;
					}else if(artistIdStr!=null && !artistIdStr.isEmpty()){
						sql = sql+" AND o.event_id in (SELECT event_id from event_artist_details WHERE artist_Id ="+artistIdStr+" )";
					}
				}
					
				if(fromDate!=null && !fromDate.isEmpty()){
					sql = sql+" AND o.invoice_Date >='"+fromDate+"'";
				}
				if(toDate!=null && !toDate.isEmpty()){
					sql = sql+" AND o.invoice_Date <='"+toDate+"'";
				}
				
				if(productType!=null && !productType.isEmpty()){
					if(productType.equalsIgnoreCase("REWARDTHEFAN")){
						sql = sql+" AND o.product_type='REWARDTHEFAN' ";
					}else if(productType.equalsIgnoreCase("RTW")){
						sql = sql+" AND o.product_type='RTW' ";
					}else if (productType.equalsIgnoreCase("RTW2")){
						sql = sql+" AND o.product_type='RTW2' ";
					}else if (productType.equalsIgnoreCase("SEATGEEK")){
						sql = sql+" AND o.product_type='SEATGEEK' ";
					}
				}
				
				if(null !=sign ){
					
					switch (sign) {
						case POSITIVE:
							sql = sql+" AND o.profit_loss > 0 ";
							break;
						case NEGATIVE:
							sql = sql+" AND o.profit_loss < 0 ";
							break;
						case ZERO:
							sql = sql+" AND o.profit_loss = 0 ";
							break;
						case ALL:
							sql = sql+" ";
							break;
						default:
							break;
					}
				}
				if(filter.getInvoiceId() != null){
					sql += " AND o.invoice_no = "+filter.getInvoiceId()+" ";
				}
				if(filter.getOrderType()!=null){
					sql += " AND co.order_type like '%"+filter.getOrderType()+"%' ";
				}
				if(filter.getCustomerOrderId() != null){
					if(productType.equalsIgnoreCase("REWARDTHEFAN")){
						sql += " AND i.order_id = "+filter.getCustomerOrderId()+" ";
					}else if((productType.equalsIgnoreCase("RTW")) || (productType.equalsIgnoreCase("RTW2"))){
						sql += " AND o.ticket_request_id = "+filter.getCustomerOrderId()+" ";
					}else{
						sql += " AND (i.order_id ="+filter.getCustomerOrderId()+" OR o.ticket_request_id ="+filter.getCustomerOrderId()+")";
					}
				}
				if(filter.getInvoiceDateStr() != null){
					if(Util.extractDateElement(filter.getInvoiceDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,o.invoice_date) = "+Util.extractDateElement(filter.getInvoiceDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getInvoiceDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,o.invoice_date) = "+Util.extractDateElement(filter.getInvoiceDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getInvoiceDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,o.invoice_date) = "+Util.extractDateElement(filter.getInvoiceDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getEventName() != null){
					sql += " AND o.event_name like '%"+filter.getEventName()+"%' ";
				}
				if(filter.getCustomerName()!=null && !filter.getCustomerName().isEmpty()){
					if(filter.getCustomerName().trim().contains(" ")){
						String names[] = filter.getCustomerName().trim().split(" ");
						sql += " AND (cus.cust_name like '%"+names[0]+"%' AND cus.last_name like '%"+names[1]+"%') ";
					}else{
						sql += " AND (cus.cust_name like '%"+filter.getCustomerName()+"%' OR cus.last_name like '%"+filter.getCustomerName()+"%') ";
					}
					
				}
				if(filter.getEventDateStr() != null){
					if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,o.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,o.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,o.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getEventTimeStr() != null){
					if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
						sql += " AND DATEPART(hour,o.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
					}
					if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
						sql += " AND DATEPART(minute,o.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
					}
				}
				if(filter.getVenueName() != null){
					sql += " AND o.venue_name like '%"+filter.getVenueName()+"%' ";
				}
				if(filter.getCity() != null){
					sql += " AND o.venue_city like '%"+filter.getCity()+"%' ";
				}
				if(filter.getState() != null){
					sql += " AND o.venue_state like '%"+filter.getState()+"%' ";
				}
				if(filter.getCountry() != null){
					sql += " AND o.venue_country like '%"+filter.getCountry()+"%' ";
				}
				if(filter.getProductType() != null){
					sql += " AND o.product_type like '%"+filter.getProductType()+"%' ";
				}
				if(filter.getInternalNotes() != null){
					sql += " AND o.internal_notes like '%"+filter.getInternalNotes()+"%' ";
				}
				if(filter.getQuantity() != null){
					sql += " AND o.sold_qty = "+filter.getQuantity()+" ";
				}
				if(filter.getSection() != null){
					if(orderType.equalsIgnoreCase("OPEN")  || orderType.equalsIgnoreCase("DISPUTED")){
						sql += " AND o.section like '%"+filter.getSection()+"%' ";
					}else if(orderType.equalsIgnoreCase("PENDING SHIPMENT")){
						sql += " AND otg.section like '%"+filter.getSection()+"%' ";
					}else if(orderType.equalsIgnoreCase("PENDING RECIEPT")){
						sql += " AND otg.section like '%"+filter.getSection()+"%' ";
					}else if(orderType.equalsIgnoreCase("CLOSED")){
						sql += " AND otg.section like '%"+filter.getSection()+"%' ";
					}
				}
				if(filter.getRow() != null){
					if(orderType.equalsIgnoreCase("OPEN")  || orderType.equalsIgnoreCase("DISPUTED")){
						sql += " AND o.row like '%"+filter.getRow()+"%' ";
					}else if(orderType.equalsIgnoreCase("PENDING SHIPMENT")){
						sql += " AND otg.row like '%"+filter.getRow()+"%' ";
					}else if(orderType.equalsIgnoreCase("PENDING RECIEPT")){
						sql += " AND otg.row like '%"+filter.getRow()+"%' ";
					}else if(orderType.equalsIgnoreCase("CLOSED")){
						sql += " AND otg.row like '%"+filter.getRow()+"%' ";
					}
				}
				if(filter.getActualSoldPrice() != null){
					sql += " AND o.actual_sold_price = "+filter.getActualSoldPrice()+" ";
				}
				if(filter.getMarketPrice() != null){
					sql += " AND o.market_price = "+filter.getMarketPrice()+" ";
				}
				if(filter.getLastUpdatedPrice() != null){
					sql += " AND o.last_updated_price = "+filter.getLastUpdatedPrice()+" "; 	
				}
				if(filter.getSectionTixQty() != null){
					sql += " AND o.section_ticket_qty = "+filter.getSectionTixQty()+" ";
				}
				if(filter.getEventTixQty() != null){
					sql += " AND o.event_ticket_qty = "+filter.getEventTixQty()+" ";
				}
				if(filter.getProfitAndLoss() != null){
					sql += " AND o.profit_loss = "+filter.getProfitAndLoss()+" ";
				}
				if(filter.getZone() != null){
					sql += " AND o.zone = "+filter.getZone()+" ";
				}
				if(filter.getZoneTixQty() != null){
					sql += " AND o.zone_ticket_qty = "+filter.getZoneTixQty()+" ";
				}
				if(filter.getZoneCheapestPrice() != null){
					sql += " AND o.zone_cheapest_price = "+filter.getZoneCheapestPrice()+" ";
				}
				if(filter.getZoneTixGroupCount() != null){
					sql += " AND o.zone_ticket_count = "+filter.getZoneTixGroupCount()+" ";
				}
				if(filter.getZoneTotalPrice() != null){
					sql += " AND o.zone_total_price = "+filter.getZoneTotalPrice()+" ";
				}
				if(filter.getZoneProfitAndLoss() != null){
					sql += " AND o.zone_profit_loss = "+filter.getZoneProfitAndLoss()+" ";
				}
				if(filter.getNetTotalSoldPrice() != null){
					sql += " AND o.net_total_sold_price = "+filter.getNetTotalSoldPrice()+" ";
				}
				if(filter.getNetSoldPrice() != null){
					sql += " AND o.net_sold_price = "+filter.getNetSoldPrice()+" ";
				}
				if(filter.getZoneMargin() != null){
					sql += " AND o.zone_margin = "+filter.getZoneMargin()+" ";
				}
				if(filter.getSectionMargin() != null){
					sql += " AND o.section_margin = "+filter.getSectionMargin()+" ";
				}
				if(filter.getPriceUpdateCount() != null){
					sql += " AND o.market_price_update_count = "+filter.getPriceUpdateCount()+" ";
				}
				if(filter.getShippingMethod() != null){
					sql += " AND sm.name like '%"+filter.getShippingMethod()+"%' ";
				}
				if(filter.getTrackingNo() != null){
					sql += " AND i.tracking_no = "+filter.getTrackingNo()+" ";
				}
				if(filter.getSecondaryOrderType() != null){
					sql += " AND co.secondary_order_type like '%"+filter.getSecondaryOrderType()+"%' ";
				}
				if(filter.getSecondaryOrderId() != null){
					sql += " AND co.secondary_order_id = "+filter.getSecondaryOrderId()+" ";
				}
				if(filter.getLastUpdatedDateStr() != null){
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,o.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,o.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,o.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getPurchaseOrderId() != null){
					sql += "AND p.id = "+filter.getPurchaseOrderId()+" ";
				}
				if(filter.getPurchaseOrderDateStr() != null){
					if(Util.extractDateElement(filter.getPurchaseOrderDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,p.created_time) = "+Util.extractDateElement(filter.getPurchaseOrderDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getPurchaseOrderDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,p.created_time) = "+Util.extractDateElement(filter.getPurchaseOrderDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getPurchaseOrderDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,p.created_time) = "+Util.extractDateElement(filter.getPurchaseOrderDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getBrokerId() != null){
					sql += " AND o.broker_id = "+filter.getBrokerId()+" ";
				}
				if(filter.getCompanyName() != null){
					sql += " AND cus.company_name like '%"+filter.getCompanyName()+"%' ";
				}
				if(filter.getPlatform() != null && !filter.getPlatform().isEmpty()){
					if(filter.getPlatform().toUpperCase().contains("WEB")){
						sql += " AND co.platform like '%DESKTOP_SITE%' ";
					}else if(filter.getPlatform().toUpperCase().contains("RETAIL")){
						sql += " AND co.platform like '%TICK_TRACKER%' ";
					}else{
						sql += " AND co.platform like '%"+filter.getPlatform()+"%' ";
					}
				}
				/*if(brokerId != null && brokerId > 0){
					sql += " AND o.broker_id = "+brokerId+" ";
				}*/
				/*
				if(filter.getTotalActualSoldPrice() != null){
					sql += " AND o.event_ticket_qty = "+filter.getTotalActualSoldPrice()+" ";
				}
				if(filter.getTotalMarketPrice() != null){
					sql += " AND o.event_ticket_qty = "+filter.getTotalMarketPrice()+" ";
				}
				if(filter.getPrice() != null){
					sql += " AND o.event_ticket_qty = "+filter.getPrice()+" ";	
				}
				if(filter.getDiscountCouponPrice() != null){
					sql += " AND o.event_ticket_qty = "+filter.getDiscountCouponPrice()+" ";
				}
				if(filter.getUrl() != null){
					sql += " AND o.event_ticket_qty = "+filter.getUrl()+" ";
				}
				*/
				/*
				if(secondaryOrderId!=null && !secondaryOrderId.isEmpty()){
					sql = sql+" AND co.secondary_order_id='"+secondaryOrderId+"' ";
				}
				
				if(invoiceNo!=null && !invoiceNo.isEmpty()){
					sql = sql+" AND o.invoice_no ="+invoiceNo;
				}
				
				if(orderId != null && !orderId.isEmpty()){
					if(productType.equalsIgnoreCase("REWARDTHEFAN")){
						sql = sql+" AND i.order_id ="+orderId;
					}else if((productType.equalsIgnoreCase("RTW")) || (productType.equalsIgnoreCase("RTW2"))){
						sql = sql+" AND o.ticket_request_id ="+orderId;
					}else{
						sql = sql+" AND (i.order_id ="+orderId+" OR o.ticket_request_id ="+orderId+")";
					}
				}				
				*/
				session  = sessionFactory.openSession();
				SQLQuery query = session.createSQLQuery(sql);
				count = Integer.parseInt(String.valueOf(query.uniqueResult()));
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return count;
	}
	
		
	public List<OpenOrderStatus> getPassedOrders(String eventIdStr, String artistIdStr,String venueIdStr, String fromDate,
			String toDate, ProfitLossSign sign,String orderType,String productType,GridHeaderFilters filter,boolean isCount,String pageNo,Integer brokerId){
		List<OpenOrderStatus> orders= null;
		Session session =null;
		Integer startFrom = 0;
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date today = new Date();
		String toDayStr = dateFormat.format(today);
		try {
			String sql = "select o.id AS id,o.event_id AS tmatEventId,o.tn_exchange_event_id AS exEventId,"+
				"o.section_ticket_qty AS sectionTixQty,o.event_ticket_qty AS eventTixQty,"+
				"o.market_price AS marketPrice,o.last_updated_price AS lastUpdatedPrice,"+
				"o.last_crawl_time AS lastCrawlTime,o.created_date AS createdDate,o.last_updated AS lastUpdated,"+
				"o.status AS status,o.category AS category,o.broker_id AS brokerId,o.ticket_request_id AS ticketRequestId,"+
				"o.invoice_no AS invoiceNo,o.invoice_date AS invoiceDate,o.venue_id AS venueId,co.secondary_order_id AS secondaryOrderId,"+
				"o.sold_qty AS soldQty,o.ticket_group_id AS ticketGroupId,o.broker_name AS brokerName,"+
				"o.invoice_type AS invoiceType,o.sales_person AS salesPerson,o.event_name AS eventName,"+
				"o.event_date AS eventDate,o.event_time AS eventTime,o.venue_name AS venueName,o.venue_city AS venueCity,"+
				"o.venue_state AS venueState,o.venue_country AS venueCountry,o.internal_notes AS internalNotes,"+
				"o.retail_price AS retailPrice,o.wholesale_price AS wholesalePrice,o.cost AS cost,co.secondary_order_type AS secondaryOrderType,"+
				"o.actual_sold_price AS actualSoldPrice,o.total_price AS totalPrice,sm.name AS shippingMethod,"+
				"o.profit_loss AS profitAndLoss,o.market_price_update_count AS priceUpdateCount,i.tracking_no AS trackingNo,"+
				"o.online_price AS onlinePrice,o.product_type AS productType,i.order_id AS orderId,CONCAT(cus.cust_name,' ',cus.last_name) AS customerName, "+
				"p.id as poId,p.created_time as poDate,o.zone as zone,o.zone_ticket_qty as zoneTixQty," +
				" o.zone_cheapest_price as zoneCheapestPrice,o.zone_ticket_count as zoneTixGroupCount, " +
				" o.net_total_sold_price as netTotalSoldPrice,o.net_sold_price as netSoldPrice, o.zone_total_price as zoneTotalPrice, " +
				" o.zone_profit_loss as zoneProfitAndLoss,o.zone_margin as zoneMargin,section_margin as sectionMargin, "+
				"cus.company_name as companyName, co.platform as platform, "+
				"otg.section AS section,otg.row AS row "+
				" from tix_open_order_status o with(nolock) left join invoice i with(nolock) on o.invoice_no=i.id "+
				"left join customer_order co with(nolock) on co.id=i.order_id "+
				"left join customer cus with(nolock) on cus.id=i.customer_id "+
				"left join purchase_order p with(nolock) on i.purchase_order_no=p.id "+
				"left join shipping_method sm with(nolock) on(i.shipping_method_id=sm.id) "+
				"left join order_ticket_group_details otg with(nolock) on(otg.order_id=co.id) "+
				"WHERE o.status='DELETED' and i.status='Completed' "+
				"and o.event_date < '"+toDayStr+"' ";
				/*
				if(orderType.equalsIgnoreCase("DELETED")){
					sql += " AND i.realTix_map='Yes' AND i.is_real_tix_upload='Yes' ";
				}
				*/
				if(eventIdStr!=null && !eventIdStr.isEmpty()){
					sql = sql+" AND o.event_id ="+eventIdStr;
				}else {
					if(venueIdStr!=null && !venueIdStr.isEmpty()){
						sql = sql+" AND o.venue_id ="+venueIdStr;
					}else if(artistIdStr!=null && !artistIdStr.isEmpty()){
						sql = sql+" AND o.event_id in (SELECT event_id from event_artist_details WHERE artist_Id ="+artistIdStr+" )";
					}
				}
					
				if(fromDate!=null && !fromDate.isEmpty()){
					sql = sql+" AND o.invoice_Date >='"+fromDate+"'";
				}
				if(toDate!=null && !toDate.isEmpty()){
					sql = sql+" AND o.invoice_Date <='"+toDate+"'";
				}
				
				if(productType!=null && !productType.isEmpty()){
					if(productType.equalsIgnoreCase("REWARDTHEFAN")){
						sql = sql+" AND o.product_type='REWARDTHEFAN' ";
					}else if(productType.equalsIgnoreCase("RTW")){
						sql = sql+" AND o.product_type='RTW' ";
					}else if (productType.equalsIgnoreCase("RTW2")){
						sql = sql+" AND o.product_type='RTW2' ";
					}else if (productType.equalsIgnoreCase("SEATGEEK")){
						sql = sql+" AND o.product_type='SEATGEEK' ";
					}
				}
				
				if(null !=sign ){
					switch (sign) {
						case POSITIVE:
							sql = sql+" AND o.profit_loss > 0 ";
							break;
						case NEGATIVE:
							sql = sql+" AND o.profit_loss < 0 ";
							break;
						case ZERO:
							sql = sql+" AND o.profit_loss = 0 ";
							break;
						case ALL:
							sql = sql+" ";
							break;
						default:
							break;
					}
				}
				if(filter.getInvoiceId() != null){
					sql += " AND o.invoice_no = "+filter.getInvoiceId()+" ";
				}
				if(filter.getCustomerOrderId() != null){
					if(productType.equalsIgnoreCase("REWARDTHEFAN")){
						sql += " AND i.order_id = "+filter.getCustomerOrderId()+" ";
					}else if((productType.equalsIgnoreCase("RTW")) || (productType.equalsIgnoreCase("RTW2"))){
						sql += " AND o.ticket_request_id = "+filter.getCustomerOrderId()+" ";
					}else{
						sql += " AND (i.order_id ="+filter.getCustomerOrderId()+" OR o.ticket_request_id ="+filter.getCustomerOrderId()+")";
					}
				}
				if(filter.getInvoiceDateStr() != null){
					if(Util.extractDateElement(filter.getInvoiceDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,o.invoice_date) = "+Util.extractDateElement(filter.getInvoiceDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getInvoiceDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,o.invoice_date) = "+Util.extractDateElement(filter.getInvoiceDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getInvoiceDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,o.invoice_date) = "+Util.extractDateElement(filter.getInvoiceDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getEventName() != null){
					sql += " AND o.event_name like '%"+filter.getEventName()+"%' ";
				}
				if(filter.getEventDateStr() != null){
					if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,o.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,o.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,o.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getEventTimeStr() != null){
					if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
						sql += " AND DATEPART(hour,o.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
					}
					if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
						sql += " AND DATEPART(minute,o.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
					}
				}
				if(filter.getVenueName() != null){
					sql += " AND o.venue_name like '%"+filter.getVenueName()+"%' ";
				}
				if(filter.getCity() != null){
					sql += " AND o.venue_city like '%"+filter.getCity()+"%' ";
				}
				if(filter.getState() != null){
					sql += " AND o.venue_state like '%"+filter.getState()+"%' ";
				}
				if(filter.getCountry() != null){
					sql += " AND o.venue_country like '%"+filter.getCountry()+"%' ";
				}
				if(filter.getProductType() != null){
					sql += " AND o.product_type like '%"+filter.getProductType()+"%' ";
				}
				if(filter.getInternalNotes() != null){
					sql += " AND o.internal_notes like '%"+filter.getInternalNotes()+"%' ";
				}
				if(filter.getQuantity() != null){
					sql += " AND o.sold_qty = "+filter.getQuantity()+" ";
				}
				if(filter.getSection() != null){					
					sql += " AND otg.section like '%"+filter.getSection()+"%' ";
				}
				if(filter.getRow() != null){
					sql += " AND otg.row like '%"+filter.getRow()+"%' ";
				}
				if(filter.getActualSoldPrice() != null){
					sql += " AND o.actual_sold_price = "+filter.getActualSoldPrice()+" ";
				}
				if(filter.getMarketPrice() != null){
					sql += " AND o.market_price = "+filter.getMarketPrice()+" ";
				}
				if(filter.getLastUpdatedPrice() != null){
					sql += " AND o.last_updated_price = "+filter.getLastUpdatedPrice()+" "; 	
				}
				if(filter.getSectionTixQty() != null){
					sql += " AND o.section_ticket_qty = "+filter.getSectionTixQty()+" ";
				}
				if(filter.getEventTixQty() != null){
					sql += " AND o.event_ticket_qty = "+filter.getEventTixQty()+" ";
				}
				if(filter.getProfitAndLoss() != null){
					sql += " AND o.profit_loss = "+filter.getProfitAndLoss()+" ";
				}
				
				if(filter.getZone() != null){
					sql += " AND o.zone = "+filter.getZone()+" ";
				}
				if(filter.getZoneTixQty() != null){
					sql += " AND o.zone_ticket_qty = "+filter.getZoneTixQty()+" ";
				}
				if(filter.getZoneCheapestPrice() != null){
					sql += " AND o.zone_cheapest_price = "+filter.getZoneCheapestPrice()+" ";
				}
				if(filter.getZoneTixGroupCount() != null){
					sql += " AND o.zone_ticket_count = "+filter.getZoneTixGroupCount()+" ";
				}
				if(filter.getZoneTotalPrice() != null){
					sql += " AND o.zone_total_price = "+filter.getZoneTotalPrice()+" ";
				}
				if(filter.getZoneProfitAndLoss() != null){
					sql += " AND o.zone_profit_loss = "+filter.getZoneProfitAndLoss()+" ";
				}
				if(filter.getNetTotalSoldPrice() != null){
					sql += " AND o.net_total_sold_price = "+filter.getNetTotalSoldPrice()+" ";
				}
				if(filter.getNetSoldPrice() != null){
					sql += " AND o.net_sold_price = "+filter.getNetSoldPrice()+" ";
				}
				if(filter.getZoneMargin() != null){
					sql += " AND o.zone_margin = "+filter.getZoneMargin()+" ";
				}
				if(filter.getSectionMargin() != null){
					sql += " AND o.section_margin = "+filter.getSectionMargin()+" ";
				}
				
				if(filter.getPriceUpdateCount() != null){
					sql += " AND o.market_price_update_count = "+filter.getPriceUpdateCount()+" ";
				}
				if(filter.getShippingMethod() != null){
					sql += " AND sm.name like '%"+filter.getShippingMethod()+"%' ";
				}
				if(filter.getCustomerName()!=null && !filter.getCustomerName().isEmpty()){
					if(filter.getCustomerName().trim().contains(" ")){
						String names[] = filter.getCustomerName().trim().split(" ");
						sql += " AND (cus.cust_name like '%"+names[0]+"%' AND cus.last_name like '%"+names[1]+"%') ";
					}else{
						sql += " AND (cus.cust_name like '%"+filter.getCustomerName()+"%' OR cus.last_name like '%"+filter.getCustomerName()+"%') ";
					}
					
				}
				if(filter.getTrackingNo() != null){
					sql += " AND i.tracking_no = "+filter.getTrackingNo()+" ";
				}
				if(filter.getSecondaryOrderType() != null){
					sql += " AND co.secondary_order_type like '%"+filter.getSecondaryOrderType()+"%' ";
				}
				if(filter.getSecondaryOrderId() != null){
					sql += " AND co.secondary_order_id = "+filter.getSecondaryOrderId()+" ";
				}
				if(filter.getLastUpdatedDateStr() != null){
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,o.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,o.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,o.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getPurchaseOrderId() != null){
					sql += "AND p.id = "+filter.getPurchaseOrderId()+" ";
				}
				if(filter.getPurchaseOrderDateStr() != null){
					if(Util.extractDateElement(filter.getPurchaseOrderDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,p.created_time) = "+Util.extractDateElement(filter.getPurchaseOrderDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getPurchaseOrderDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,p.created_time) = "+Util.extractDateElement(filter.getPurchaseOrderDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getPurchaseOrderDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,p.created_time) = "+Util.extractDateElement(filter.getPurchaseOrderDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getBrokerId() != null){
					sql += " AND o.broker_id = "+filter.getBrokerId()+" ";
				}
				if(filter.getCompanyName() != null){
					sql += " AND cus.company_name like '%"+filter.getCompanyName()+"%' ";
				}
				if(filter.getPlatform() != null && !filter.getPlatform().isEmpty()){
					if(filter.getPlatform().toUpperCase().contains("WEB")){
						sql += " AND co.platform like '%DESKTOP_SITE%' ";
					}else if(filter.getPlatform().toUpperCase().contains("RETAIL")){
						sql += " AND co.platform like '%TICK_TRACKER%' ";
					}else{
						sql += " AND co.platform = '"+filter.getPlatform()+"' ";
					}
				}
				if(brokerId != null && brokerId > 0){
					sql += " AND o.broker_id = "+brokerId+" ";
				}
				/*
				if(filter.getTotalActualSoldPrice() != null){
					sql += " AND o.event_ticket_qty = "+filter.getTotalActualSoldPrice()+" ";
				}
				if(filter.getTotalMarketPrice() != null){
					sql += " AND o.event_ticket_qty = "+filter.getTotalMarketPrice()+" ";
				}
				if(filter.getPrice() != null){
					sql += " AND o.event_ticket_qty = "+filter.getPrice()+" ";	
				}
				if(filter.getDiscountCouponPrice() != null){
					sql += " AND o.event_ticket_qty = "+filter.getDiscountCouponPrice()+" ";
				}
				if(filter.getUrl() != null){
					sql += " AND o.event_ticket_qty = "+filter.getUrl()+" ";
				}
				*/	
				/*
				if(secondaryOrderId!=null && !secondaryOrderId.isEmpty()){
					sql = sql+" AND co.secondary_order_id='"+secondaryOrderId+"' ";
				}
				
				if(invoiceNo!=null && !invoiceNo.isEmpty()){
					sql = sql+" AND o.invoice_no ="+invoiceNo;
				}
				
				if(orderId != null && !orderId.isEmpty()){
					if(productType.equalsIgnoreCase("REWARDTHEFAN")){
						sql = sql+" AND i.order_id ="+orderId;
					}else if((productType.equalsIgnoreCase("RTW")) || (productType.equalsIgnoreCase("RTW2"))){
						sql = sql+" AND o.ticket_request_id ="+orderId;
					}else{
						sql = sql+" AND (i.order_id ="+orderId+" OR o.ticket_request_id ="+orderId+")";
					}
				}
				*/
				sql +=  " Order by  o.id desc";
				session  = sessionFactory.openSession();
				SQLQuery query = session.createSQLQuery(sql);
			
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("tmatEventId", Hibernate.INTEGER);
			query.addScalar("exEventId", Hibernate.INTEGER);
			query.addScalar("sectionTixQty", Hibernate.INTEGER);
			query.addScalar("eventTixQty", Hibernate.INTEGER);
			query.addScalar("ticketRequestId", Hibernate.INTEGER);
			query.addScalar("section", Hibernate.STRING);
			query.addScalar("marketPrice", Hibernate.DOUBLE);
			query.addScalar("lastUpdatedPrice", Hibernate.DOUBLE);
			query.addScalar("lastCrawlTime", Hibernate.TIMESTAMP);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("lastUpdated", Hibernate.TIMESTAMP);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("category", Hibernate.STRING);
			query.addScalar("brokerId", Hibernate.INTEGER);
			query.addScalar("invoiceNo", Hibernate.INTEGER);
			query.addScalar("invoiceDate", Hibernate.TIMESTAMP);
			query.addScalar("venueId", Hibernate.INTEGER);
			query.addScalar("soldQty", Hibernate.INTEGER);
			query.addScalar("ticketGroupId", Hibernate.INTEGER);
			query.addScalar("brokerName", Hibernate.STRING);
			query.addScalar("customerName", Hibernate.STRING);
			query.addScalar("invoiceType", Hibernate.STRING);
			query.addScalar("salesPerson", Hibernate.STRING);
			query.addScalar("eventName", Hibernate.STRING);
			query.addScalar("eventDate", Hibernate.DATE);
			query.addScalar("eventTime", Hibernate.TIME);
			query.addScalar("venueName", Hibernate.STRING);
			query.addScalar("venueCity", Hibernate.STRING);
			query.addScalar("venueState", Hibernate.STRING);
			query.addScalar("venueCountry", Hibernate.STRING);
			query.addScalar("internalNotes", Hibernate.STRING);
			query.addScalar("retailPrice", Hibernate.DOUBLE);
			query.addScalar("wholesalePrice", Hibernate.DOUBLE);
			query.addScalar("cost", Hibernate.DOUBLE);
			query.addScalar("actualSoldPrice", Hibernate.DOUBLE);
			query.addScalar("totalPrice", Hibernate.DOUBLE);
			query.addScalar("row", Hibernate.STRING);
			query.addScalar("profitAndLoss", Hibernate.DOUBLE);
			query.addScalar("priceUpdateCount", Hibernate.INTEGER);
			query.addScalar("onlinePrice", Hibernate.DOUBLE);
			query.addScalar("productType", Hibernate.STRING);
			query.addScalar("poId", Hibernate.INTEGER);
			query.addScalar("poDate", Hibernate.TIMESTAMP);
			query.addScalar("shippingMethod", Hibernate.STRING);
			query.addScalar("trackingNo", Hibernate.STRING);
			query.addScalar("secondaryOrderType", Hibernate.STRING);
			query.addScalar("orderId", Hibernate.INTEGER);
			query.addScalar("secondaryOrderId", Hibernate.STRING);
			query.addScalar("zone", Hibernate.STRING);
			query.addScalar("zoneTixQty", Hibernate.INTEGER);
			query.addScalar("zoneCheapestPrice", Hibernate.DOUBLE);
			query.addScalar("zoneTixGroupCount", Hibernate.INTEGER);
			query.addScalar("netTotalSoldPrice", Hibernate.DOUBLE);
			query.addScalar("netSoldPrice", Hibernate.DOUBLE);
			query.addScalar("sectionMargin", Hibernate.DOUBLE);
			query.addScalar("zoneTotalPrice", Hibernate.DOUBLE);
			query.addScalar("zoneProfitAndLoss", Hibernate.DOUBLE);
			query.addScalar("zoneMargin", Hibernate.DOUBLE);
			query.addScalar("companyName", Hibernate.STRING);
			query.addScalar("platform", Hibernate.STRING);
			
			if(isCount){
				orders = query.setResultTransformer(Transformers.aliasToBean(OpenOrderStatus.class)).list();
			}else{
				startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
				orders = query.setResultTransformer(Transformers.aliasToBean(OpenOrderStatus.class)).setFirstResult(startFrom)
				.setMaxResults(PaginationUtil.PAGESIZE).list();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return orders;
	}
		
	public Integer getPassedOrdersCount(String eventIdStr, String artistIdStr, 
			String venueIdStr, String fromDate,String toDate, ProfitLossSign sign,String orderType,String productType,GridHeaderFilters filter, Integer brokerId){
		Session session =null;
		Integer count=0;
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date today = new Date();
		String toDayStr = dateFormat.format(today);
		try {
			String sql = "select count(o.id) "+
				"from tix_open_order_status o with(nolock) left join invoice i with(nolock) on o.invoice_no=i.id "+
				"left join customer_order co with(nolock) on co.id=i.order_id "+
				"left join customer cus with(nolock) on cus.id=i.customer_id "+
				"left join purchase_order p with(nolock) on i.purchase_order_no=p.id "+
				"left join shipping_method sm with(nolock) on(i.shipping_method_id=sm.id) WHERE o.status='DELETED' and i.status='Completed' and o.event_date < '"+toDayStr+"' ";
				/*
				if(orderType.equalsIgnoreCase("DELETED")){
					sql += " AND i.realTix_map='Yes' AND i.is_real_tix_upload='Yes' ";
				}
				*/
				if(eventIdStr!=null && !eventIdStr.isEmpty()){
					sql = sql+" AND o.event_id ="+eventIdStr;
				}else {
					if(venueIdStr!=null && !venueIdStr.isEmpty()){
						sql = sql+" AND o.venue_id ="+venueIdStr;
					}else if(artistIdStr!=null && !artistIdStr.isEmpty()){
						sql = sql+" AND o.event_id in (SELECT event_id from event_artist_details WHERE artist_Id ="+artistIdStr+" )";
					}
				}
					
				if(fromDate!=null && !fromDate.isEmpty()){
					sql = sql+" AND o.invoice_Date >='"+fromDate+"'";
				}
				if(toDate!=null && !toDate.isEmpty()){
					sql = sql+" AND o.invoice_Date <='"+toDate+"'";
				}
				
				if(productType!=null && !productType.isEmpty()){
					if(productType.equalsIgnoreCase("REWARDTHEFAN")){
						sql = sql+" AND o.product_type='REWARDTHEFAN' ";
					}else if(productType.equalsIgnoreCase("RTW")){
						sql = sql+" AND o.product_type='RTW' ";
					}else if (productType.equalsIgnoreCase("RTW2")){
						sql = sql+" AND o.product_type='RTW2' ";
					}else if (productType.equalsIgnoreCase("SEATGEEK")){
						sql = sql+" AND o.product_type='SEATGEEK' ";
					}
				}
				
				if(null !=sign ){
					
					switch (sign) {
						case POSITIVE:
							sql = sql+" AND o.profit_loss > 0 ";
							break;
						case NEGATIVE:
							sql = sql+" AND o.profit_loss < 0 ";
							break;
						case ZERO:
							sql = sql+" AND o.profit_loss = 0 ";
							break;
						case ALL:
							sql = sql+" ";
							break;
						default:
							break;
					}
				}
				if(filter.getInvoiceId() != null){
					sql += " AND o.invoice_no = "+filter.getInvoiceId()+" ";
				}
				if(filter.getCustomerOrderId() != null){
					if(productType.equalsIgnoreCase("REWARDTHEFAN")){
						sql += " AND i.order_id = "+filter.getCustomerOrderId()+" ";
					}else if((productType.equalsIgnoreCase("RTW")) || (productType.equalsIgnoreCase("RTW2"))){
						sql += " AND o.ticket_request_id = "+filter.getCustomerOrderId()+" ";
					}else{
						sql += " AND (i.order_id ="+filter.getCustomerOrderId()+" OR o.ticket_request_id ="+filter.getCustomerOrderId()+")";
					}
				}
				if(filter.getInvoiceDateStr() != null){
					if(Util.extractDateElement(filter.getInvoiceDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,o.invoice_date) = "+Util.extractDateElement(filter.getInvoiceDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getInvoiceDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,o.invoice_date) = "+Util.extractDateElement(filter.getInvoiceDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getInvoiceDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,o.invoice_date) = "+Util.extractDateElement(filter.getInvoiceDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getCustomerName()!=null && !filter.getCustomerName().isEmpty()){
					if(filter.getCustomerName().trim().contains(" ")){
						String names[] = filter.getCustomerName().trim().split(" ");
						sql += " AND (cus.cust_name like '%"+names[0]+"%' AND cus.last_name like '%"+names[1]+"%') ";
					}else{
						sql += " AND (cus.cust_name like '%"+filter.getCustomerName()+"%' OR cus.last_name like '%"+filter.getCustomerName()+"%') ";
					}
					
				}
				if(filter.getEventName() != null){
					sql += " AND o.event_name like '%"+filter.getEventName()+"%' ";
				}
				if(filter.getEventDateStr() != null){
					if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,o.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,o.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,o.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getEventTimeStr() != null){
					if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
						sql += " AND DATEPART(hour,o.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
					}
					if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
						sql += " AND DATEPART(minute,o.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
					}
				}
				if(filter.getVenueName() != null){
					sql += " AND o.venue_name like '%"+filter.getVenueName()+"%' ";
				}
				if(filter.getCity() != null){
					sql += " AND o.venue_city like '%"+filter.getCity()+"%' ";
				}
				if(filter.getState() != null){
					sql += " AND o.venue_state like '%"+filter.getState()+"%' ";
				}
				if(filter.getCountry() != null){
					sql += " AND o.venue_country like '%"+filter.getCountry()+"%' ";
				}
				if(filter.getProductType() != null){
					sql += " AND o.product_type like '%"+filter.getProductType()+"%' ";
				}
				if(filter.getInternalNotes() != null){
					sql += " AND o.internal_notes like '%"+filter.getInternalNotes()+"%' ";
				}
				if(filter.getQuantity() != null){
					sql += " AND o.sold_qty = "+filter.getQuantity()+" ";
				}
				if(filter.getSection() != null){
					sql += " AND o.section like '%"+filter.getSection()+"%' ";
				}
				if(filter.getRow() != null){
					sql += " AND o.row like '%"+filter.getRow()+"%' ";
				}
				if(filter.getActualSoldPrice() != null){
					sql += " AND o.actual_sold_price = "+filter.getActualSoldPrice()+" ";
				}
				if(filter.getMarketPrice() != null){
					sql += " AND o.market_price = "+filter.getMarketPrice()+" ";
				}
				if(filter.getLastUpdatedPrice() != null){
					sql += " AND o.last_updated_price = "+filter.getLastUpdatedPrice()+" "; 	
				}
				if(filter.getSectionTixQty() != null){
					sql += " AND o.section_ticket_qty = "+filter.getSectionTixQty()+" ";
				}
				if(filter.getEventTixQty() != null){
					sql += " AND o.event_ticket_qty = "+filter.getEventTixQty()+" ";
				}
				if(filter.getProfitAndLoss() != null){
					sql += " AND o.profit_loss = "+filter.getProfitAndLoss()+" ";
				}
				if(filter.getZone() != null){
					sql += " AND o.zone = "+filter.getZone()+" ";
				}
				if(filter.getZoneTixQty() != null){
					sql += " AND o.zone_ticket_qty = "+filter.getZoneTixQty()+" ";
				}
				if(filter.getZoneCheapestPrice() != null){
					sql += " AND o.zone_cheapest_price = "+filter.getZoneCheapestPrice()+" ";
				}
				if(filter.getZoneTixGroupCount() != null){
					sql += " AND o.zone_ticket_count = "+filter.getZoneTixGroupCount()+" ";
				}
				if(filter.getZoneTotalPrice() != null){
					sql += " AND o.zone_total_price = "+filter.getZoneTotalPrice()+" ";
				}
				if(filter.getZoneProfitAndLoss() != null){
					sql += " AND o.zone_profit_loss = "+filter.getZoneProfitAndLoss()+" ";
				}
				if(filter.getNetTotalSoldPrice() != null){
					sql += " AND o.net_total_sold_price = "+filter.getNetTotalSoldPrice()+" ";
				}
				if(filter.getNetSoldPrice() != null){
					sql += " AND o.net_sold_price = "+filter.getNetSoldPrice()+" ";
				}
				if(filter.getZoneMargin() != null){
					sql += " AND o.zone_margin = "+filter.getZoneMargin()+" ";
				}
				if(filter.getSectionMargin() != null){
					sql += " AND o.section_margin = "+filter.getSectionMargin()+" ";
				}
				if(filter.getPriceUpdateCount() != null){
					sql += " AND o.market_price_update_count = "+filter.getPriceUpdateCount()+" ";
				}
				if(filter.getShippingMethod() != null){
					sql += " AND sm.name like '%"+filter.getShippingMethod()+"%' ";
				}
				if(filter.getTrackingNo() != null){
					sql += " AND i.tracking_no = "+filter.getTrackingNo()+" ";
				}
				if(filter.getSecondaryOrderType() != null){
					sql += " AND co.secondary_order_type like '%"+filter.getSecondaryOrderType()+"%' ";
				}
				if(filter.getSecondaryOrderId() != null){
					sql += " AND co.secondary_order_id = "+filter.getSecondaryOrderId()+" ";
				}
				if(filter.getLastUpdatedDateStr() != null){
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,o.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,o.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,o.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getPurchaseOrderId() != null){
					sql += "AND p.id = "+filter.getPurchaseOrderId()+" ";
				}
				if(filter.getPurchaseOrderDateStr() != null){
					if(Util.extractDateElement(filter.getPurchaseOrderDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,p.created_time) = "+Util.extractDateElement(filter.getPurchaseOrderDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getPurchaseOrderDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,p.created_time) = "+Util.extractDateElement(filter.getPurchaseOrderDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getPurchaseOrderDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,p.created_time) = "+Util.extractDateElement(filter.getPurchaseOrderDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getBrokerId() != null){
					sql += " AND o.broker_id = "+filter.getBrokerId()+" ";
				}
				if(filter.getCompanyName() != null){
					sql += " AND cus.company_name like '%"+filter.getCompanyName()+"%' ";
				}
				if(brokerId != null && brokerId > 0){
					sql += " AND o.broker_id = "+brokerId+" ";
				}
				/*
				if(filter.getTotalActualSoldPrice() != null){
					sql += " AND o.event_ticket_qty = "+filter.getTotalActualSoldPrice()+" ";
				}
				if(filter.getTotalMarketPrice() != null){
					sql += " AND o.event_ticket_qty = "+filter.getTotalMarketPrice()+" ";
				}
				if(filter.getPrice() != null){
					sql += " AND o.event_ticket_qty = "+filter.getPrice()+" ";	
				}
				if(filter.getDiscountCouponPrice() != null){
					sql += " AND o.event_ticket_qty = "+filter.getDiscountCouponPrice()+" ";
				}
				if(filter.getUrl() != null){
					sql += " AND o.event_ticket_qty = "+filter.getUrl()+" ";
				}
				*/
				/*
				if(secondaryOrderId!=null && !secondaryOrderId.isEmpty()){
					sql = sql+" AND co.secondary_order_id='"+secondaryOrderId+"' ";
				}
				
				if(invoiceNo!=null && !invoiceNo.isEmpty()){
					sql = sql+" AND o.invoice_no ="+invoiceNo;
				}
				
				if(orderId != null && !orderId.isEmpty()){
					if(productType.equalsIgnoreCase("REWARDTHEFAN")){
						sql = sql+" AND i.order_id ="+orderId;
					}else if((productType.equalsIgnoreCase("RTW")) || (productType.equalsIgnoreCase("RTW2"))){
						sql = sql+" AND o.ticket_request_id ="+orderId;
					}else{
						sql = sql+" AND (i.order_id ="+orderId+" OR o.ticket_request_id ="+orderId+")";
					}
				}				
				*/
				session  = sessionFactory.openSession();
				SQLQuery query = session.createSQLQuery(sql);
				count = Integer.parseInt(String.valueOf(query.uniqueResult()));
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return count;
	}
	
	public List<OpenOrderStatus> getPassedOrdersToExport(String invoiceNo,String eventIdStr, String artistIdStr, 
			String venueIdStr, String fromDate,String toDate, ProfitLossSign sign,String orderType,String productType,String 
			secondaryOrderId,String orderId,GridHeaderFilters filter,Integer brokerId){
		List<OpenOrderStatus> orders= null;
		Session session =null;
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date today = new Date();
		String toDayStr = dateFormat.format(today);
		try {
			String sql = "select o.id AS id,o.event_id AS tmatEventId,o.tn_exchange_event_id AS exEventId,"+
				"o.section_ticket_qty AS sectionTixQty,o.event_ticket_qty AS eventTixQty,"+
				"o.market_price AS marketPrice,o.last_updated_price AS lastUpdatedPrice,"+
				"o.last_crawl_time AS lastCrawlTime,o.created_date AS createdDate,o.last_updated AS lastUpdated,"+
				"o.status AS status,o.category AS category,o.broker_id AS brokerId,o.ticket_request_id AS ticketRequestId,"+
				"o.invoice_no AS invoiceNo,o.invoice_date AS invoiceDate,o.venue_id AS venueId,co.secondary_order_id AS secondaryOrderId,"+
				"o.sold_qty AS soldQty,o.ticket_group_id AS ticketGroupId,o.broker_name AS brokerName,"+
				"o.invoice_type AS invoiceType,o.sales_person AS salesPerson,o.event_name AS eventName,"+
				"o.event_date AS eventDate,o.event_time AS eventTime,o.venue_name AS venueName,o.venue_city AS venueCity,"+
				"o.venue_state AS venueState,o.venue_country AS venueCountry,o.internal_notes AS internalNotes,"+
				"o.retail_price AS retailPrice,o.wholesale_price AS wholesalePrice,o.cost AS cost,co.secondary_order_type AS secondaryOrderType,"+
				"o.actual_sold_price AS actualSoldPrice,o.total_price AS totalPrice,sm.name AS shippingMethod,"+
				"o.profit_loss AS profitAndLoss,o.market_price_update_count AS priceUpdateCount,i.tracking_no AS trackingNo, "+
				"o.online_price AS onlinePrice,o.product_type AS productType,i.order_id AS orderId,CONCAT(cus.cust_name,' ',cus.last_name) AS customerName, " +
				"p.id as poId,p.created_time as poDate,o.zone as zone,o.zone_ticket_qty as zoneTixQty," +
				" o.zone_cheapest_price as zoneCheapestPrice,o.zone_ticket_count as zoneTixGroupCount, " +
				" o.net_total_sold_price as netTotalSoldPrice,o.net_sold_price as netSoldPrice, o.zone_total_price as zoneTotalPrice, " +
				" o.zone_profit_loss as zoneProfitAndLoss,o.zone_margin as zoneMargin,section_margin as sectionMargin, "+
				"cus.company_name as companyName, co.platform as platform, "+
				"otg.section AS section,otg.row AS row "+
				"from tix_open_order_status o with(nolock) left join invoice i with(nolock) on o.invoice_no=i.id "+
				"left join customer_order co with(nolock) on co.id=i.order_id "+
				"left join customer cus with(nolock) on cus.id=i.customer_id "+
				"left join purchase_order p with(nolock) on i.purchase_order_no=p.id "+
				"left join shipping_method sm with(nolock) on(i.shipping_method_id=sm.id) "+
				"left join order_ticket_group_details otg with(nolock) on(otg.order_id=co.id) "+
				"WHERE o.status='DELETED' and i.status='Completed' "+
				"and o.event_date < '"+toDayStr+"' ";
				/*
				if(orderType.equalsIgnoreCase("DELETED")){
					sql += " AND i.realTix_map='Yes' AND i.is_real_tix_upload='Yes' ";
				}
				*/
				if(eventIdStr!=null && !eventIdStr.isEmpty()){
					sql = sql+" AND o.event_id ="+eventIdStr;
				}else {
					if(venueIdStr!=null && !venueIdStr.isEmpty()){
						sql = sql+" AND o.venue_id ="+venueIdStr;
					}else if(artistIdStr!=null && !artistIdStr.isEmpty()){
						sql = sql+" AND o.event_id in (SELECT event_id from event_artist_details WHERE artist_Id ="+artistIdStr+" )";
					}
				}
					
				if(fromDate!=null && !fromDate.isEmpty()){
					sql = sql+" AND o.invoice_Date >='"+fromDate+"'";
				}
				if(toDate!=null && !toDate.isEmpty()){
					sql = sql+" AND o.invoice_Date <='"+toDate+"'";
				}
				
				if(productType!=null && !productType.isEmpty()){
					if(productType.equalsIgnoreCase("REWARDTHEFAN")){
						sql = sql+" AND o.product_type='REWARDTHEFAN' ";
					}else if(productType.equalsIgnoreCase("RTW")){
						sql = sql+" AND o.product_type='RTW' ";
					}else if (productType.equalsIgnoreCase("RTW2")){
						sql = sql+" AND o.product_type='RTW2' ";
					}else if (productType.equalsIgnoreCase("SEATGEEK")){
						sql = sql+" AND o.product_type='SEATGEEK' ";
					}
				}
				if(secondaryOrderId!=null && !secondaryOrderId.isEmpty()){
					sql = sql+" AND co.secondary_order_id='"+secondaryOrderId+"' ";
				}
				
				if(invoiceNo!=null && !invoiceNo.isEmpty()){
					sql = sql+" AND o.invoice_no ="+invoiceNo;
				}
				
				if(orderId != null && !orderId.isEmpty()){
					if(productType.equalsIgnoreCase("REWARDTHEFAN")){
						sql = sql+" AND i.order_id ="+orderId;
					}else if((productType.equalsIgnoreCase("RTW")) || (productType.equalsIgnoreCase("RTW2"))){
						sql = sql+" AND o.ticket_request_id ="+orderId;
					}else{
						sql = sql+" AND (i.order_id ="+orderId+" OR o.ticket_request_id ="+orderId+")";
					}
				}
				
				if(null !=sign ){
					
					switch (sign) {
						case POSITIVE:
							sql = sql+" AND o.profit_loss > 0 ";
							break;
						case NEGATIVE:
							sql = sql+" AND o.profit_loss < 0 ";
							break;
						case ZERO:
							sql = sql+" AND o.profit_loss = 0 ";
							break;
						case ALL:
							sql = sql+" ";
							break;
						default:
							break;
					}
				}
				if(filter.getInvoiceId() != null){
					sql += " AND o.invoice_no = "+filter.getInvoiceId()+" ";
				}
				if(filter.getCustomerOrderId() != null){
					if(productType.equalsIgnoreCase("REWARDTHEFAN")){
						sql += " AND i.order_id = "+filter.getCustomerOrderId()+" ";
					}else if((productType.equalsIgnoreCase("RTW")) || (productType.equalsIgnoreCase("RTW2"))){
						sql += " AND o.ticket_request_id = "+filter.getCustomerOrderId()+" ";
					}else{
						sql += " AND (i.order_id ="+filter.getCustomerOrderId()+" OR o.ticket_request_id ="+filter.getCustomerOrderId()+")";
					}
				}
				if(filter.getInvoiceDateStr() != null){
					if(Util.extractDateElement(filter.getInvoiceDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,o.invoice_date) = "+Util.extractDateElement(filter.getInvoiceDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getInvoiceDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,o.invoice_date) = "+Util.extractDateElement(filter.getInvoiceDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getInvoiceDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,o.invoice_date) = "+Util.extractDateElement(filter.getInvoiceDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getEventName() != null){
					sql += " AND o.event_name like '%"+filter.getEventName()+"%' ";
				}
				if(filter.getEventDateStr() != null){
					if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,o.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,o.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,o.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getEventTimeStr() != null){
					if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
						sql += " AND DATEPART(hour,o.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
					}
					if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
						sql += " AND DATEPART(minute,o.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
					}
				}
				if(filter.getVenueName() != null){
					sql += " AND o.venue_name like '%"+filter.getVenueName()+"%' ";
				}
				if(filter.getCity() != null){
					sql += " AND o.venue_city like '%"+filter.getCity()+"%' ";
				}
				if(filter.getState() != null){
					sql += " AND o.venue_state like '%"+filter.getState()+"%' ";
				}
				if(filter.getCountry() != null){
					sql += " AND o.venue_country like '%"+filter.getCountry()+"%' ";
				}
				if(filter.getProductType() != null){
					sql += " AND o.product_type like '%"+filter.getProductType()+"%' ";
				}
				if(filter.getInternalNotes() != null){
					sql += " AND o.internal_notes like '%"+filter.getInternalNotes()+"%' ";
				}
				if(filter.getQuantity() != null){
					sql += " AND o.sold_qty = "+filter.getQuantity()+" ";
				}
				if(filter.getSection() != null){					
					sql += " AND otg.section like '%"+filter.getSection()+"%' ";
				}
				if(filter.getRow() != null){
					sql += " AND otg.row like '%"+filter.getRow()+"%' ";
				}
				if(filter.getActualSoldPrice() != null){
					sql += " AND o.actual_sold_price = "+filter.getActualSoldPrice()+" ";
				}
				if(filter.getMarketPrice() != null){
					sql += " AND o.market_price = "+filter.getMarketPrice()+" ";
				}
				if(filter.getLastUpdatedPrice() != null){
					sql += " AND o.last_updated_price = "+filter.getLastUpdatedPrice()+" "; 	
				}
				if(filter.getSectionTixQty() != null){
					sql += " AND o.section_ticket_qty = "+filter.getSectionTixQty()+" ";
				}
				if(filter.getEventTixQty() != null){
					sql += " AND o.event_ticket_qty = "+filter.getEventTixQty()+" ";
				}
				if(filter.getProfitAndLoss() != null){
					sql += " AND o.profit_loss = "+filter.getProfitAndLoss()+" ";
				}
				
				if(filter.getZone() != null){
					sql += " AND o.zone = "+filter.getZone()+" ";
				}
				if(filter.getZoneTixQty() != null){
					sql += " AND o.zone_ticket_qty = "+filter.getZoneTixQty()+" ";
				}
				if(filter.getZoneCheapestPrice() != null){
					sql += " AND o.zone_cheapest_price = "+filter.getZoneCheapestPrice()+" ";
				}
				if(filter.getZoneTixGroupCount() != null){
					sql += " AND o.zone_ticket_count = "+filter.getZoneTixGroupCount()+" ";
				}
				if(filter.getZoneTotalPrice() != null){
					sql += " AND o.zone_total_price = "+filter.getZoneTotalPrice()+" ";
				}
				if(filter.getZoneProfitAndLoss() != null){
					sql += " AND o.zone_profit_loss = "+filter.getZoneProfitAndLoss()+" ";
				}
				if(filter.getNetTotalSoldPrice() != null){
					sql += " AND o.net_total_sold_price = "+filter.getNetTotalSoldPrice()+" ";
				}
				if(filter.getNetSoldPrice() != null){
					sql += " AND o.net_sold_price = "+filter.getNetSoldPrice()+" ";
				}
				if(filter.getZoneMargin() != null){
					sql += " AND o.zone_margin = "+filter.getZoneMargin()+" ";
				}
				if(filter.getSectionMargin() != null){
					sql += " AND o.section_margin = "+filter.getSectionMargin()+" ";
				}
				
				if(filter.getPriceUpdateCount() != null){
					sql += " AND o.market_price_update_count = "+filter.getPriceUpdateCount()+" ";
				}
				if(filter.getShippingMethod() != null){
					sql += " AND sm.name like '%"+filter.getShippingMethod()+"%' ";
				}
				if(filter.getCustomerName()!=null && !filter.getCustomerName().isEmpty()){
					if(filter.getCustomerName().trim().contains(" ")){
						String names[] = filter.getCustomerName().trim().split(" ");
						sql += " AND (cus.cust_name like '%"+names[0]+"%' AND cus.last_name like '%"+names[1]+"%') ";
					}else{
						sql += " AND (cus.cust_name like '%"+filter.getCustomerName()+"%' OR cus.last_name like '%"+filter.getCustomerName()+"%') ";
					}
					
				}
				if(filter.getTrackingNo() != null){
					sql += " AND i.tracking_no = "+filter.getTrackingNo()+" ";
				}
				if(filter.getSecondaryOrderType() != null){
					sql += " AND co.secondary_order_type like '%"+filter.getSecondaryOrderType()+"%' ";
				}
				if(filter.getSecondaryOrderId() != null){
					sql += " AND co.secondary_order_id = "+filter.getSecondaryOrderId()+" ";
				}
				if(filter.getLastUpdatedDateStr() != null){
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,o.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,o.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,o.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getPurchaseOrderId() != null){
					sql += "AND p.id = "+filter.getPurchaseOrderId()+" ";
				}
				if(filter.getPurchaseOrderDateStr() != null){
					if(Util.extractDateElement(filter.getPurchaseOrderDateStr(),"DAY") > 0){
						sql += " AND DATEPART(day,p.created_time) = "+Util.extractDateElement(filter.getPurchaseOrderDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getPurchaseOrderDateStr(),"MONTH") > 0){
						sql += " AND DATEPART(month,p.created_time) = "+Util.extractDateElement(filter.getPurchaseOrderDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getPurchaseOrderDateStr(),"YEAR") > 0){
						sql += " AND DATEPART(year,p.created_time) = "+Util.extractDateElement(filter.getPurchaseOrderDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getBrokerId() != null){
					sql += " AND o.broker_id = "+filter.getBrokerId()+" ";
				}
				if(filter.getCompanyName() != null){
					sql += " AND cus.company_name like '%"+filter.getCompanyName()+"%' ";
				}
				if(filter.getPlatform() != null && !filter.getPlatform().isEmpty()){
					if(filter.getPlatform().toUpperCase().contains("WEB")){
						sql += " AND co.platform like '%DESKTOP_SITE%' ";
					}else if(filter.getPlatform().toUpperCase().contains("RETAIL")){
						sql += " AND co.platform like '%TICK_TRACKER%' ";
					}else{
						sql += " AND co.platform = '"+filter.getPlatform()+"' ";
					}
				}
				if(brokerId != null && brokerId > 0){
					sql += " AND o.broker_id = "+brokerId+" ";
				}
				sql +=  " Order by  o.id desc";
				session  = sessionFactory.openSession();
				SQLQuery query = session.createSQLQuery(sql);
			
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("tmatEventId", Hibernate.INTEGER);
			query.addScalar("exEventId", Hibernate.INTEGER);
			query.addScalar("sectionTixQty", Hibernate.INTEGER);
			query.addScalar("eventTixQty", Hibernate.INTEGER);
			query.addScalar("ticketRequestId", Hibernate.INTEGER);
			query.addScalar("section", Hibernate.STRING);
			query.addScalar("marketPrice", Hibernate.DOUBLE);
			query.addScalar("lastUpdatedPrice", Hibernate.DOUBLE);
			query.addScalar("lastCrawlTime", Hibernate.TIMESTAMP);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("lastUpdated", Hibernate.TIMESTAMP);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("category", Hibernate.STRING);
			query.addScalar("brokerId", Hibernate.INTEGER);
			query.addScalar("invoiceNo", Hibernate.INTEGER);
			query.addScalar("invoiceDate", Hibernate.TIMESTAMP);
			query.addScalar("venueId", Hibernate.INTEGER);
			query.addScalar("soldQty", Hibernate.INTEGER);
			query.addScalar("ticketGroupId", Hibernate.INTEGER);
			query.addScalar("brokerName", Hibernate.STRING);
			query.addScalar("customerName", Hibernate.STRING);
			query.addScalar("invoiceType", Hibernate.STRING);
			query.addScalar("salesPerson", Hibernate.STRING);
			query.addScalar("eventName", Hibernate.STRING);
			query.addScalar("eventDate", Hibernate.DATE);
			query.addScalar("eventTime", Hibernate.TIME);
			query.addScalar("venueName", Hibernate.STRING);
			query.addScalar("venueCity", Hibernate.STRING);
			query.addScalar("venueState", Hibernate.STRING);
			query.addScalar("venueCountry", Hibernate.STRING);
			query.addScalar("internalNotes", Hibernate.STRING);
			query.addScalar("retailPrice", Hibernate.DOUBLE);
			query.addScalar("wholesalePrice", Hibernate.DOUBLE);
			query.addScalar("cost", Hibernate.DOUBLE);
			query.addScalar("actualSoldPrice", Hibernate.DOUBLE);
			query.addScalar("totalPrice", Hibernate.DOUBLE);
			query.addScalar("row", Hibernate.STRING);
			query.addScalar("profitAndLoss", Hibernate.DOUBLE);
			query.addScalar("priceUpdateCount", Hibernate.INTEGER);
			query.addScalar("onlinePrice", Hibernate.DOUBLE);
			query.addScalar("productType", Hibernate.STRING);
			query.addScalar("poId", Hibernate.INTEGER);
			query.addScalar("poDate", Hibernate.TIMESTAMP);
			query.addScalar("shippingMethod", Hibernate.STRING);
			query.addScalar("trackingNo", Hibernate.STRING);
			query.addScalar("secondaryOrderType", Hibernate.STRING);
			query.addScalar("orderId", Hibernate.INTEGER);
			query.addScalar("secondaryOrderId", Hibernate.STRING);
			query.addScalar("zone", Hibernate.STRING);
			query.addScalar("zoneTixQty", Hibernate.INTEGER);
			query.addScalar("zoneCheapestPrice", Hibernate.DOUBLE);
			query.addScalar("zoneTixGroupCount", Hibernate.INTEGER);
			query.addScalar("netTotalSoldPrice", Hibernate.DOUBLE);
			query.addScalar("netSoldPrice", Hibernate.DOUBLE);
			query.addScalar("sectionMargin", Hibernate.DOUBLE);
			query.addScalar("zoneTotalPrice", Hibernate.DOUBLE);
			query.addScalar("zoneProfitAndLoss", Hibernate.DOUBLE);
			query.addScalar("zoneMargin", Hibernate.DOUBLE);
			query.addScalar("companyName", Hibernate.STRING);
			query.addScalar("platform", Hibernate.STRING);
			
			orders = query.setResultTransformer(Transformers.aliasToBean(OpenOrderStatus.class)).setMaxResults(65535).list();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return orders;
	}
	
	
	public Integer getEventCount(){
		Integer count=0;
		Session session =null;
		try {
			session  = sessionFactory.openSession();
			Query query =  session.createSQLQuery("SELECT count(*) from Event_Details with(nolock) WHERE status=1");
			count =  (Integer) query.uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return count;
	}
	
	public Collection<SoldTicketDetail> getAllSoldUnfilledNewTickets(){
		Collection<SoldTicketDetail> soldTicket = null;
		Session session =null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT s.ticket_group_id AS id,s.Invoice_no AS invoiceId,s.invoice_datetime AS invoiceDateTime,");
			sql.append("s.event_id AS tmatEventId,s.exchange_event_id AS exEventId,s.ticket_qty AS quantity,s.ticket_count As soldQty,");
			sql.append("s.event_name AS eventName,s.event_datetime AS eventDate,s.event_time AS eventTime,");
			sql.append("s.venue_id AS venueId,s.venue_name AS venueName,s.venue_city AS venueCity,s.state_name AS venueState,");
			sql.append("s.system_country_name AS venueCountry,s.section AS section,s.row AS row,s.retail_price AS retailPrice,");
			sql.append("s.cost AS cost,s.wholesale_price AS wholesalePrice,s.actual_sold_price AS actualSoldPrice,");
			sql.append("s.totalprice AS totalPrice,s.internal_notes As internalNotes,s.broker_id AS brokerId,");
			sql.append("s.purchase_order_id AS purchaseOrderId,s.purchase_order_date AS purchaseOrderDateTime,");
			sql.append("s.invoice_type AS invoiceType,s.Sales_Person AS salesPerson,s.product_type AS productType,");
			sql.append("s.realtix_map AS realTixMapped,s.is_sent AS isSent,s.ticket_request_id AS ticketRequestId ");
			sql.append("FROM sold_ticket_detail_vw s with(nolock) left join tix_open_order_status o with(nolock) ");
			sql.append("on(s.product_type = o.product_type and s.Invoice_no = o.invoice_no) ");
			sql.append(" where s.actual_sold_price>=0 and s.event_date>getdate() and (o.id is null or (o.status is not null and o.status!='ACTIVE')) ");
			
			session  = sessionFactory.openSession();
			SQLQuery query =  session.createSQLQuery(sql.toString());
			
			query.addScalar("id",Hibernate.INTEGER);
			query.addScalar("invoiceId",Hibernate.INTEGER);
			query.addScalar("invoiceDateTime",Hibernate.TIMESTAMP);
			query.addScalar("ticketRequestId",Hibernate.INTEGER);
			query.addScalar("tmatEventId", Hibernate.INTEGER);
			query.addScalar("quantity", Hibernate.INTEGER);
			query.addScalar("soldQty", Hibernate.INTEGER);
			query.addScalar("eventName", Hibernate.STRING);
			query.addScalar("eventDate", Hibernate.TIMESTAMP);
			query.addScalar("eventTime", Hibernate.TIME);
			query.addScalar("venueId", Hibernate.INTEGER);
			query.addScalar("venueName", Hibernate.STRING);
			query.addScalar("venueCity", Hibernate.STRING);
			query.addScalar("venueState", Hibernate.STRING);
			query.addScalar("venueCountry", Hibernate.STRING);
			query.addScalar("section", Hibernate.STRING);
			query.addScalar("row", Hibernate.STRING);
			query.addScalar("retailPrice", Hibernate.DOUBLE);
			query.addScalar("cost", Hibernate.DOUBLE);
			query.addScalar("wholesalePrice", Hibernate.DOUBLE);
			query.addScalar("actualSoldPrice", Hibernate.DOUBLE);
			query.addScalar("totalPrice", Hibernate.DOUBLE);
			query.addScalar("internalNotes", Hibernate.STRING);
			query.addScalar("brokerId", Hibernate.INTEGER);
			query.addScalar("purchaseOrderId", Hibernate.INTEGER);
			query.addScalar("purchaseOrderDateTime", Hibernate.TIMESTAMP);
			query.addScalar("invoiceType", Hibernate.STRING);
			query.addScalar("salesPerson", Hibernate.STRING);
			query.addScalar("productType", Hibernate.STRING);
			query.addScalar("realTixMapped", Hibernate.STRING);
			query.addScalar("isSent", Hibernate.BOOLEAN);
			soldTicket = query.setResultTransformer(Transformers.aliasToBean(SoldTicketDetail.class)).list();
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return soldTicket;
	}
	
	public List<PaymentHistory> getPaymentHistoryByInvoice(Integer invoiceId, Integer brokerId){
		Session session =null;
		List<PaymentHistory> paymentHistoryList = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("select o.primary_payment_method AS primaryPaymentMethod,o.secondary_payment_method AS secondaryPaymentMethod,");
			sql.append("o.third_payment_method AS thirdPaymentMethod,o.third_payment_amount AS thirdPayAmt,o.third_payment_transaction_id AS thirdTransactionId,");
			sql.append("o.primary_transaction_id AS primaryTransaction,o.secondary_transaction_id AS secondaryTransaction,");
			sql.append("o.primary_payment_amount AS primaryAmount,o.secondary_payment_amount AS secondaryAmount,");
			sql.append("o.created_date AS paidOn, scc.card_type as cardType,scc.card_last_four_digit AS creditCardNumber,i.csr AS csr ");
			sql.append("from invoice i with(nolock) left join customer_order o with(nolock) on i.order_id=o.id ");
			//sql.append("left join customer_order_credit_card cc with(nolock) on o.id=cc.order_id ");
			//sql.append("left join customer_card_info cci with(nolock) on cc.credit_card_id=cci.id where i.id="+invoiceId);
			sql.append("left join customer_order_credit_card_details cc with(nolock) on ");
			sql.append("(select case when o.primary_payment_method = 'CREDITCARD' then o.primary_transaction_id ");
			sql.append("when o.secondary_payment_method = 'CREDITCARD' then o.secondary_transaction_id ");
			sql.append("when o.third_payment_method = 'CREDITCARD' then o.third_payment_transaction_id end transactionId from customer_order o ");
			sql.append("where o.id = i.order_id) = cc.transaction_id ");
			sql.append("left join stripe_customer_cards scc with(nolock) on cc.credit_card_id=scc.id ");
			sql.append("where i.id="+invoiceId);
			
			if(brokerId != null && brokerId > 0){
				sql.append(" AND i.broker_id = "+brokerId);
			}
			session  = sessionFactory.openSession();
			SQLQuery query =  session.createSQLQuery(sql.toString());
			
			query.addScalar("primaryPaymentMethod",Hibernate.STRING);
			query.addScalar("secondaryPaymentMethod",Hibernate.STRING);
			query.addScalar("thirdPaymentMethod",Hibernate.STRING);
			query.addScalar("primaryAmount", Hibernate.DOUBLE);
			query.addScalar("secondaryAmount", Hibernate.DOUBLE);
			query.addScalar("thirdPayAmt", Hibernate.DOUBLE);
			query.addScalar("paidOn", Hibernate.TIMESTAMP);
			query.addScalar("cardType", Hibernate.STRING);
			query.addScalar("creditCardNumber", Hibernate.STRING);
			query.addScalar("csr", Hibernate.STRING);
			query.addScalar("primaryTransaction", Hibernate.STRING);
			query.addScalar("secondaryTransaction", Hibernate.STRING);
			query.addScalar("thirdTransactionId", Hibernate.STRING);
			paymentHistoryList = query.setResultTransformer(Transformers.aliasToBean(PaymentHistory.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return paymentHistoryList;
	}
	
	public List<PaymentHistory> getRTWPaymentHistoryByInvoice(Integer invoiceId, String productType){
		Session session =null;
		List<PaymentHistory> rtwpaymentHistoryList = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("select ip.invoice_id as invoiceId, ip.credit_card_number as creditCardNumber, ip.check_number as checkNumber, ip.amt as amount, ");
			sql.append("ip.notes as note, ip.create_date as paidOn, sys.system_user_fname as systemUser, ");
			sql.append("pt.payment_type_desc as paymentType, s.office_desc as transOffice ");
			sql.append("from pos_invoice_payment ip with(nolock) left join pos_system_user_office s with(nolock) on  ip.tran_office_id = s.office_id AND s.product_type='"+productType+"'");
			sql.append(" left join pos_payment_type pt with(nolock) on ip.payment_type_id = pt.payment_type_id ");
			sql.append("left join pos_system_users sys with(nolock) on ip.system_user_id = sys.system_user_id AND sys.product_type='"+productType+"'");
			sql.append(" where ip.invoice_id="+invoiceId+" AND ip.product_type='"+productType+"'");
			session  = sessionFactory.openSession();
			SQLQuery query =  session.createSQLQuery(sql.toString());
			
			query.addScalar("invoiceId",Hibernate.INTEGER);
			query.addScalar("creditCardNumber",Hibernate.STRING);
			query.addScalar("checkNumber",Hibernate.STRING);
			query.addScalar("amount", Hibernate.DOUBLE);
			query.addScalar("note", Hibernate.STRING);
			query.addScalar("paidOn", Hibernate.TIMESTAMP);
			query.addScalar("systemUser", Hibernate.STRING);
			query.addScalar("paymentType", Hibernate.STRING);
			query.addScalar("transOffice", Hibernate.STRING);
			rtwpaymentHistoryList = query.setResultTransformer(Transformers.aliasToBean(PaymentHistory.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return rtwpaymentHistoryList;
	}
	
	public List<PaymentHistory> getRTWPaymentDetailsByPOId(Integer poId, String productType){
		Session session =null;
		List<PaymentHistory> rtwpaymentHistoryList = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("select p.purchase_order_id as purchaseOrderId, p.credit_card_number as creditCardNumber, p.check_number as checkNumber, p.amt as amount, ");
			sql.append("p.notes as note, p.create_date as paidOn, sys.system_user_fname as systemUser, ");
			sql.append("pt.payment_type_desc as paymentType, s.office_desc as transOffice ");
			sql.append("from pos_purchase_order_payment p with(nolock) left join pos_system_user_office s with(nolock) on  p.tran_office_id = s.office_id AND s.product_type='"+productType+"'");
			sql.append(" left join pos_payment_type pt with(nolock) on p.payment_type_id = pt.payment_type_id ");
			sql.append("left join pos_system_users sys with(nolock) on p.system_user_id = sys.system_user_id AND sys.product_type='"+productType+"'");
			sql.append(" where p.purchase_order_id="+poId+" AND p.product_type='"+productType+"'");
			session  = sessionFactory.openSession();
			SQLQuery query =  session.createSQLQuery(sql.toString());
			
			query.addScalar("purchaseOrderId",Hibernate.INTEGER);
			query.addScalar("creditCardNumber",Hibernate.STRING);
			query.addScalar("checkNumber",Hibernate.STRING);
			query.addScalar("amount", Hibernate.DOUBLE);
			query.addScalar("note", Hibernate.STRING);
			query.addScalar("paidOn", Hibernate.TIMESTAMP);
			query.addScalar("systemUser", Hibernate.STRING);
			query.addScalar("paymentType", Hibernate.STRING);
			query.addScalar("transOffice", Hibernate.STRING);
			rtwpaymentHistoryList = query.setResultTransformer(Transformers.aliasToBean(PaymentHistory.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return rtwpaymentHistoryList;
	}
	/*public List<PurchaseOrders> getInvoiceRelatedPObyInvoice(Integer invoiceId){
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("select o.primary_payment_method,o.secondary_payment_method,o.primary_payment_amount,");
			sql.append("o.secondary_payment_amount,o.created_date,cci.card_last_four_digit,cci.card_type,i.created_by ");
			sql.append("from invoice i inner join customer_order o on i.order_id=o.id");
		} catch (Exception e) {
			
		}
	}*/
	
	
	public boolean deleteUserActions(Integer userId){
		Session session =null;
		try {
			String sql = "DELETE tracker_user_action WHERE user_id="+userId;
			session  = sessionFactory.openSession();
			SQLQuery query =  session.createSQLQuery(sql);
			query.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally{
			if(session!=null && session.isOpen()){
				session.close();
			}
		}
		return true;
	}

	public Collection<WebServiceTracking> getAllActionsByDateRangeAndServerIp(String fromDate, String toDate,GridHeaderFilters filter,String pageNo){
		Collection<WebServiceTracking> webServiceTracking = null;
		Session session = null;
		Integer startFrom = 0;
		try	{					
			String sqlQuery = "select id as id, customer_ip_address as customerIpAddress,platform as platform,"+
						"api_name as apiName,hit_date as hitDate,contest_id as contestId,customer_id as custId,"+
						"action_result as description, app_ver as appVersion "+
						"from web_service_tracking_new with(nolock) where platform <> 'TICK_TRACKER' ";
			
			if(fromDate != null || toDate != null){
				sqlQuery += " AND hit_date >= '"+fromDate+"' and hit_date <= '"+toDate+"'";
			}
			if(filter.getCustomerIpAddress() != null){
				sqlQuery += " AND customer_ip_address like '%"+filter.getCustomerIpAddress()+"%' ";
			}
			if(filter.getActionType() != null){
				sqlQuery += " AND api_name like '%"+filter.getActionType()+"%' ";
			}
			if(filter.getActionResult() != null){
				sqlQuery += " AND action_result like '%"+filter.getActionResult()+"%' ";
			}
			if(filter.getPlatform()!=null && !filter.getPlatform().isEmpty()){
				sqlQuery += " AND platform like '%"+filter.getPlatform()+"%' ";
			}
			if(filter.getCustomerId()!=null){
				sqlQuery += " AND customer_id = "+filter.getCustomerId();
			}
			if(filter.getContestId()!=null){
				sqlQuery += " AND contest_id = "+filter.getContestId();
			}
			if(filter.getText1()!=null && !filter.getText1().isEmpty()){
				sqlQuery += " AND action_result like '%"+filter.getText1()+"%' ";
			}
			if(filter.getText2()!=null && !filter.getText2().isEmpty()){
				sqlQuery += " AND app_ver like '%"+filter.getText2()+"%' ";
			}
			if(filter.getHittingDateStr() != null){
				if(Util.extractDateElement(filter.getHittingDateStr(),"DAY") > 0){
					sqlQuery += " AND DATEPART(day,hit_date) = "+Util.extractDateElement(filter.getHittingDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getHittingDateStr(),"MONTH") > 0){
					sqlQuery += " AND DATEPART(month,hit_date) = "+Util.extractDateElement(filter.getHittingDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getHittingDateStr(),"YEAR") > 0){
					sqlQuery += " AND DATEPART(year,hit_date) = "+Util.extractDateElement(filter.getHittingDateStr(),"YEAR")+" ";
				}
			}
			String sortingSql = GridSortingUtil.getAllActionsByDateRangeAndServerIpSql(filter);
			if(sortingSql!=null && !sortingSql.isEmpty()){
				sqlQuery += sortingSql;
			}else{
				sqlQuery += " order by id desc";
			}
			
			session  = sessionFactory.openSession();
			SQLQuery query = session.createSQLQuery(sqlQuery);
			
			query.addScalar("id",Hibernate.INTEGER);
			query.addScalar("customerIpAddress",Hibernate.STRING);
			query.addScalar("platform",Hibernate.STRING);
			query.addScalar("apiName",Hibernate.STRING);
			query.addScalar("hitDate",Hibernate.TIMESTAMP);
			query.addScalar("custId",Hibernate.INTEGER);
			query.addScalar("description",Hibernate.STRING);
			query.addScalar("contestId",Hibernate.INTEGER);
			query.addScalar("appVersion",Hibernate.STRING);
			
			startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
			webServiceTracking = query.setResultTransformer(Transformers.aliasToBean(WebServiceTracking.class)).setFirstResult(startFrom).setMaxResults(PaginationUtil.PAGESIZE).list();
			return webServiceTracking;
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return null;
	}
	
	public Integer getWebsiteIPSearchCount(String fromDate, String toDate,GridHeaderFilters filter){
		Session session = null;
		Integer count=0;
		try	{					
			String sqlQuery = "select count(*) as cnt from web_service_tracking_new with(nolock) where platform <> 'TICK_TRACKER' ";
			
			if(fromDate != null || toDate != null){
				sqlQuery += " AND hit_date >= '"+fromDate+"' and hit_date <= '"+toDate+"'";
			}
			if(filter.getCustomerIpAddress() != null){
				sqlQuery += " AND customer_ip_address like '%"+filter.getCustomerIpAddress()+"%' ";
			}
			if(filter.getActionType() != null){
				sqlQuery += " AND api_name like '%"+filter.getActionType()+"%' ";
			}
			if(filter.getActionResult() != null){
				sqlQuery += " AND action_result like '%"+filter.getActionResult()+"%' ";
			}
			if(filter.getPlatform()!=null && !filter.getPlatform().isEmpty()){
				sqlQuery += " AND platform like '%"+filter.getPlatform()+"%' ";
			}
			if(filter.getCustomerId()!=null){
				sqlQuery += " AND customer_id = "+filter.getCustomerId();
			}
			if(filter.getContestId()!=null){
				sqlQuery += " AND contest_id = "+filter.getContestId();
			}
			if(filter.getText1()!=null && !filter.getText1().isEmpty()){
				sqlQuery += " AND action_result like '%"+filter.getText1()+"%' ";
			}
			if(filter.getText2()!=null && !filter.getText2().isEmpty()){
				sqlQuery += " AND app_ver like '%"+filter.getText2()+"%' ";
			}
			if(filter.getHittingDateStr() != null){
				if(Util.extractDateElement(filter.getHittingDateStr(),"DAY") > 0){
					sqlQuery += " AND DATEPART(day,hit_date) = "+Util.extractDateElement(filter.getHittingDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getHittingDateStr(),"MONTH") > 0){
					sqlQuery += " AND DATEPART(month,hit_date) = "+Util.extractDateElement(filter.getHittingDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getHittingDateStr(),"YEAR") > 0){
					sqlQuery += " AND DATEPART(year,hit_date) = "+Util.extractDateElement(filter.getHittingDateStr(),"YEAR")+" ";
				}
			}
			session  = sessionFactory.openSession();
			SQLQuery query = session.createSQLQuery(sqlQuery);
			
			count =  (Integer) query.uniqueResult();
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return count;
	}
	
	
	
	
	public Collection<WebServiceTracking> getContestTracking(String fromDate, String toDate,GridHeaderFilters filter,String pageNo){
		Collection<WebServiceTracking> webServiceTracking = null;
		Session session = null;
		Integer startFrom = 0;
		try	{					
			String sqlQuery = "select id as id, cust_ip_addr as customerIpAddress, platform as platform,"+
							"api_name as apiName,start_date as hitDate, contest_id as contestId,cust_id as custId,"+
							"description as description, app_ver as appVersion "+
							"from web_service_tracking_cassandra with(nolock) where platform <> 'TICK_TRACKER'  ";
			
			if(fromDate != null || toDate != null){
				sqlQuery += " AND start_date >= '"+fromDate+"' and start_date <= '"+toDate+"'";
			}
			if(filter.getCustomerIpAddress() != null){
				sqlQuery += " AND cust_ip_addr like '%"+filter.getCustomerIpAddress()+"%' ";
			}
			if(filter.getActionType() != null){
				sqlQuery += " AND api_name like '%"+filter.getActionType()+"%' ";
			}
			if(filter.getActionResult() != null){
				sqlQuery += " AND description like '%"+filter.getActionResult()+"%' ";
			}
			if(filter.getPlatform()!=null && !filter.getPlatform().isEmpty()){
				sqlQuery += " AND platform like '%"+filter.getPlatform()+"%' ";
			}
			if(filter.getCustomerId()!=null){
				sqlQuery += " AND customer_id = "+filter.getCustomerId();
			}
			if(filter.getContestId()!=null){
				sqlQuery += " AND contest_id = "+filter.getContestId();
			}
			if(filter.getText1()!=null && !filter.getText1().isEmpty()){
				sqlQuery += " AND action_result like '%"+filter.getText1()+"%' ";
			}
			if(filter.getText2()!=null && !filter.getText2().isEmpty()){
				sqlQuery += " AND app_ver like '%"+filter.getText2()+"%' ";
			}
			if(filter.getHittingDateStr() != null){
				if(Util.extractDateElement(filter.getHittingDateStr(),"DAY") > 0){
					sqlQuery += " AND DATEPART(day,start_date) = "+Util.extractDateElement(filter.getHittingDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getHittingDateStr(),"MONTH") > 0){
					sqlQuery += " AND DATEPART(month,start_date) = "+Util.extractDateElement(filter.getHittingDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getHittingDateStr(),"YEAR") > 0){
					sqlQuery += " AND DATEPART(year,start_date) = "+Util.extractDateElement(filter.getHittingDateStr(),"YEAR")+" ";
				}
			}
			String sortingSql = GridSortingUtil.getContestTrackingSql(filter);
			if(sortingSql!=null && !sortingSql.isEmpty()){
				sqlQuery += sortingSql;
			}else{
				sqlQuery += " order by id desc";
			}
			
			session  = sessionFactory.openSession();
			SQLQuery query = session.createSQLQuery(sqlQuery);
			
			query.addScalar("id",Hibernate.INTEGER);
			query.addScalar("customerIpAddress",Hibernate.STRING);
			query.addScalar("platform",Hibernate.STRING);
			query.addScalar("apiName",Hibernate.STRING);
			query.addScalar("hitDate",Hibernate.TIMESTAMP);
			query.addScalar("custId",Hibernate.INTEGER);
			query.addScalar("description",Hibernate.STRING);
			query.addScalar("contestId",Hibernate.INTEGER);
			query.addScalar("appVersion",Hibernate.STRING);
			
			startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
			webServiceTracking = query.setResultTransformer(Transformers.aliasToBean(WebServiceTracking.class)).setFirstResult(startFrom).setMaxResults(PaginationUtil.PAGESIZE).list();
			return webServiceTracking;
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return null;
	}
	
	public Integer getContestTrackingCount(String fromDate, String toDate,GridHeaderFilters filter){
		Session session = null;
		Integer count=0;
		try	{					
			String sqlQuery = "select count(*) as cnt from web_service_tracking_cassandra with(nolock) where platform <> 'TICK_TRACKER' ";
			if(fromDate != null || toDate != null){
				sqlQuery += " AND start_date >= '"+fromDate+"' and start_date <= '"+toDate+"'";
			}
			if(filter.getCustomerIpAddress() != null){
				sqlQuery += " AND cust_ip_addr like '%"+filter.getCustomerIpAddress()+"%' ";
			}
			if(filter.getActionType() != null){
				sqlQuery += " AND api_name like '%"+filter.getActionType()+"%' ";
			}
			if(filter.getActionResult() != null){
				sqlQuery += " AND description like '%"+filter.getActionResult()+"%' ";
			}
			if(filter.getPlatform()!=null && !filter.getPlatform().isEmpty()){
				sqlQuery += " AND platform like '%"+filter.getPlatform()+"%' ";
			}
			if(filter.getCustomerId()!=null){
				sqlQuery += " AND customer_id = "+filter.getCustomerId();
			}
			if(filter.getContestId()!=null){
				sqlQuery += " AND contest_id = "+filter.getContestId();
			}
			if(filter.getText1()!=null && !filter.getText1().isEmpty()){
				sqlQuery += " AND action_result like '%"+filter.getText1()+"%' ";
			}
			if(filter.getText2()!=null && !filter.getText2().isEmpty()){
				sqlQuery += " AND app_ver like '%"+filter.getText2()+"%' ";
			}
			if(filter.getHittingDateStr() != null){
				if(Util.extractDateElement(filter.getHittingDateStr(),"DAY") > 0){
					sqlQuery += " AND DATEPART(day,start_date) = "+Util.extractDateElement(filter.getHittingDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getHittingDateStr(),"MONTH") > 0){
					sqlQuery += " AND DATEPART(month,start_date) = "+Util.extractDateElement(filter.getHittingDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getHittingDateStr(),"YEAR") > 0){
					sqlQuery += " AND DATEPART(year,start_date) = "+Util.extractDateElement(filter.getHittingDateStr(),"YEAR")+" ";
				}
			}
			session  = sessionFactory.openSession();
			SQLQuery query = session.createSQLQuery(sqlQuery);
			count =  (Integer) query.uniqueResult();
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return count;
	}
	
	
	
	
	
	public Integer getWebsiteIPCount(){
		Integer count=0;
		Session session =null;
		try {
			session  = sessionFactory.openSession();
			Query query =  session.createSQLQuery("SELECT count(*) from web_service_tracking with(nolock)");
			count =  (Integer) query.uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return count;
	}
	
	public List<Artist> getAllActiveByGrandchildOrChild(Integer grandChildId,Integer childId){
		Session session =null;
		try {
			String queryStr = "select distinct a.id as id,REPLACE(a.name,'\"','') as name "+
				"FROM artist a with(nolock) "+
				"inner join artist_category_mapping ac with(nolock) on ac.artist_id=a.id  WHERE a.artist_status=1 ";
			if(grandChildId!=null && grandChildId>0){
				queryStr += "AND ac.grand_child_category_id="+grandChildId;
			}
			if(childId!=null && childId>0){
				queryStr += "AND ac.child_category_id="+childId;
			}
			session  = sessionFactory.openSession();
			Query query = session.createSQLQuery(queryStr);
			List<Artist> list = query.setResultTransformer(Transformers.aliasToBean(Artist.class)).list();
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	
	public List<Artist> getAllActiveByGrandchildOrChildAndDisplayOnSearch(Integer grandChildId,Integer childId){
		Session session =null;
		try {
			String queryStr = "select distinct a.id as id,REPLACE(a.name,'\"','') as name "+
				"FROM artist a with(nolock) "+
				"inner join artist_category_mapping ac with(nolock) on ac.artist_id=a.id  WHERE a.artist_status=1 ";
			if(grandChildId!=null && grandChildId>0){
				queryStr += "AND ac.grand_child_category_id="+grandChildId;
			}
			if(childId!=null && childId>0){
				queryStr += "AND ac.child_category_id="+childId;
			}
			queryStr += "AND a.display_on_search="+1;
			session  = sessionFactory.openSession();
			Query query = session.createSQLQuery(queryStr);
			List<Artist> list = query.setResultTransformer(Transformers.aliasToBean(Artist.class)).list();
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	

	public Integer getCronJewelOrdersByFilterCount(String orderId,String fromDate, String toDate, String parentCategory, String grandChildCategory, String teamName, String leagueName,String status,GridHeaderFilters filter)throws Exception {
		Session session = null;
		Integer count = 0;
		try{
		String hql="select count(*) as cnt FROM fantasy_customer_orders fco with(nolock) "+
			"inner join fantasy_grandchild_category gc with(nolock) on gc.id = fco.fantasy_grand_child_id "+
			"inner join customer c with(nolock) on c.id = fco.customer_id where 1=1 ";
		
		/*if(parentCategory!=null && !parentCategory.isEmpty()){
			hql += " AND league_parent_type='"+parentCategory+"' ";
		}*/
		if(grandChildCategory != null && !grandChildCategory.isEmpty()){
			hql += " AND gc.name like '%"+grandChildCategory+"%' ";
		}
		if(status!=null && !status.isEmpty()){
			if(status.equals("Outstanding")){
				hql += " AND fco.status = '"+CrownJewelOrderStatus.OUTSTANDING+"'";
			}
			else if(status.equals("Approved")){
				hql += " AND fco.status = '"+CrownJewelOrderStatus.APPROVED+"'";
			}
			else if(status.equals("Rejected")){
				hql += " AND fco.status = '"+CrownJewelOrderStatus.REJECTED+"'";
			}
		}
		else{					
			hql += " AND (fco.status = 'OUTSTANDING' OR fco.status = 'REJECTED' OR fco.status = 'APPROVED')"; 				
		}
		
		if(orderId!=null && !orderId.isEmpty()){
			hql += " AND fco.id = "+orderId;
		}		
		if(fromDate!=null && !fromDate.isEmpty()){
			hql += " AND fco.create_date >= '"+fromDate+"'";
		}
		if(toDate!=null && !toDate.isEmpty()){
			hql += " AND fco.create_date <= '"+toDate+"'";
		}
		if(teamName!=null && !teamName.isEmpty()){
			hql += " AND fco.team_name like '%"+teamName+"%'";
		}
		if(leagueName!=null && !leagueName.isEmpty()){
			hql += " AND fco.fantasy_event_name like '%"+leagueName+"%'";
		}
		if(filter.getId() != null ){
			hql += " AND fco.id = "+filter.getId();
		}
		if(filter.getLeagueName() != null){
			hql += " AND fco.fantasy_event_name like '%"+filter.getLeagueName()+"%'";
		}
		if(filter.getTeamId() != null){
			hql += " AND fco.team_id = "+filter.getTeamId();
		}
		if(filter.getTeamName() != null){
			hql += " AND fco.team_name like '%"+filter.getTeamName()+"%'";
		}
		if(filter.getIsPackageSelected() != null){
			hql += " AND fco.package_selected = "+filter.getIsPackageSelected();
		}
		if(filter.getCost() != null){
			hql += " AND fco.package_cost = "+filter.getCost();
		}
		if(filter.getInternalNotes() != null){
			hql += " AND fco.package_notes like '%"+filter.getInternalNotes()+"%' ";
		}
		if(filter.getEventId() != null){
			hql += " AND fco.event_id = "+filter.getEventId();
		}
		if(filter.getTicketId() != null){
			hql += " AND fco.ticket_id = "+filter.getTicketId();
		}
		if(filter.getZone() != null){
			hql += " AND fco.zone like '%"+filter.getZone()+"%' ";
		}
		if(filter.getTicketQty() != null){
			hql += " AND fco.ticket_qty = "+filter.getTicketQty();
		}
		if(filter.getPrice() != null){
			hql += " AND fco.ticket_price = "+filter.getPrice();
		}
		if(filter.getRequirePoints() != null){
			hql += " AND fco.required_points = "+filter.getRequirePoints();	
		}
		if(filter.getCustomerId() != null){
			hql += " AND fco.customer_id = "+filter.getCustomerId();
		}
		if(filter.getStatus() != null){
			hql += " AND fco.status like '%"+filter.getStatus()+"%' ";
		}
		if(filter.getCreatedDateStr() != null){
			if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
				hql += " AND DATEPART(day, fco.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
			}
			if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
				hql += " AND DATEPART(month, fco.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
			}
			if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
				hql += " AND DATEPART(year, fco.create_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
			}
		}
		if(filter.getLastUpdatedDateStr() != null){
			if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
				hql += " AND DATEPART(day, fco.last_update) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
			}
			if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
				hql += " AND DATEPART(month, fco.last_update) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
			}
			if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
				hql += " AND DATEPART(year, fco.last_update) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
			}
		}
		if(filter.getCustomerOrderId() != null){
			hql += "AND fco.regular_order_id = "+filter.getCustomerOrderId();
		}
		if(filter.getTeamZoneId() != null){
			hql += " AND fco.team_zone_id = "+filter.getTeamZoneId();
		}
		if(filter.getVenueName() != null){
			hql += " AND fco.venue_name like '%"+filter.getVenueName()+"%' ";
		}
		if(filter.getCity() != null){
			hql += " AND fco.venue_city like '%"+filter.getCity()+"%' ";
		}
		if(filter.getState() != null){
			hql += " AND fco.venue_state like '%"+filter.getState()+"%' ";
		}
		if(filter.getCountry() != null){
			hql += " AND fco.venue_country like '%"+filter.getCountry()+"%' ";
		}
		if(filter.getPlatform() != null){
			hql += " AND fco.platform like '%"+filter.getPlatform()+"%' ";
		}
		if(filter.getGrandChildCategoryName() != null){
			hql += " AND gc.name like '%"+filter.getGrandChildCategoryName()+"%' ";
		}
		if(filter.getCustomerName() != null){
			hql += " AND (c.cust_name like '%"+filter.getCustomerName()+"%' OR c.last_name like '%"+filter.getCustomerName()+"%') ";
		}
		
		session  = sessionFactory.openSession();
		SQLQuery query = session.createSQLQuery(hql);
		count = Integer.parseInt(String.valueOf(query.uniqueResult()));		
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return count;
	}
	
	public List<PayPalTracking> getPayPalTracking(GridHeaderFilters filter,String pageNo){
		Session session = null;
		Integer startFrom = 0;
		try {
			String queryStr = "Select pt.customer_id as customerId,pt.event_id as eventId,pt.qty as quantity,pt.zone as zone,"+
								"CAST(pt.order_total AS DOUBLE PRECISION) as orderTotal,pt.order_type as orderType,pt.platform as platform,pt.status as status,"+
								"pt.paypal_transaction_id as paypalTransactionId,pt.transaction_id as transactionId,pt.created_date as createdDate,"+
								"pt.last_updated as lastUpdated,ptd.order_id as orderId,ptd.payment_id as paymentId,ptd.payer_email as payerEmail,"+
								"ptd.payer_f_name as payerFirstName,ptd.payer_l_name as payerLastName,ptd.payer_phone as payerPhone,"+
								"ptd.transaction_date as transactionDate"+
								" from paypal_tracking pt with(nolock) join paypal_transaction_details ptd with(nolock) on pt.transaction_id = ptd.id";
			/*
			if(orderId!=null && orderId>0){
				queryStr += " AND ptd.order_id = "+orderId+"";
			}
			if(customerName!=null && !customerName.isEmpty()){
				queryStr += " AND ptd.payer_f_name like '%"+customerName+"%'";
			}
			if(transactionStatus != null && !transactionStatus.isEmpty()){
				queryStr += " AND pt.status like '%"+transactionStatus+"%'";
			}
			*/
			if(filter.getCustomerId() != null){
				queryStr += " AND pt.customer_id = "+filter.getCustomerId()+" ";
			}
			if(filter.getCustomerName() != null){
				queryStr += " AND ptd.payer_f_name like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getLastName() != null){
				queryStr += " AND ptd.payer_l_name like '%"+filter.getLastName()+"%' ";
			}
			if(filter.getEmail() != null){
				queryStr += " AND ptd.payer_email like '%"+filter.getEmail()+"%' ";
			}
			if(filter.getPhone() != null){
				queryStr += " AND ptd.payer_phone like '%"+filter.getPhone()+"%' ";
			}
			if(filter.getCustomerOrderId() != null){
				queryStr += " AND ptd.order_id = "+filter.getCustomerOrderId()+" ";
			}
			if(filter.getOrderTotal() != null){
				queryStr += " AND CAST(pt.order_total AS DOUBLE PRECISION) = "+filter.getOrderTotal()+" ";
			}
			if(filter.getOrderType() != null){
				queryStr += " AND pt.order_type like '%"+filter.getOrderType()+"%' ";
			}
			if(filter.getEventId() != null){
				queryStr += " AND pt.event_id = "+filter.getEventId()+" ";
			}
			if(filter.getQuantity() != null){
				queryStr += " AND pt.qty = "+filter.getQuantity()+" ";
			}
			if(filter.getZone() != null){
				queryStr += " AND pt.zone like '%"+filter.getZone()+"%' ";
			}
			if(filter.getPaypalTransactionId() != null){
				queryStr += " AND pt.paypal_transaction_id like '%"+filter.getPaypalTransactionId()+"%' ";
			}
			if(filter.getTransactionId() != null){
				queryStr += " AND pt.transaction_id = "+filter.getTransactionId()+" ";
			}
			if(filter.getPaymentId() != null){
				queryStr += " AND ptd.payment_id like '%"+filter.getPaymentId()+"%' ";
			}
			if(filter.getPlatform() != null){
				queryStr += " AND pt.platform like '%"+filter.getPlatform()+"%' ";
			}
			if(filter.getStatus() != null){
				queryStr += " AND pt.status like '%"+filter.getStatus()+"%' ";
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					queryStr += " AND DATEPART(day,pt.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					queryStr += " AND DATEPART(month,pt.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					queryStr += " AND DATEPART(year,pt.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getLastUpdatedDateStr() != null){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					queryStr += " AND DATEPART(day,pt.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
					queryStr += " AND DATEPART(month,pt.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
					queryStr += " AND DATEPART(year,pt.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getTransactionDateStr() != null){
				if(Util.extractDateElement(filter.getTransactionDateStr(),"DAY") > 0){
					queryStr += " AND DATEPART(day,ptd.transaction_date) = "+Util.extractDateElement(filter.getTransactionDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getTransactionDateStr(),"MONTH") > 0){
					queryStr += " AND DATEPART(month,ptd.transaction_date) = "+Util.extractDateElement(filter.getTransactionDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getTransactionDateStr(),"YEAR") > 0){
					queryStr += " AND DATEPART(year,ptd.transaction_date) = "+Util.extractDateElement(filter.getTransactionDateStr(),"YEAR")+" ";
				}
			}
			String sortingSql = GridSortingUtil.getPayPalTrackingSortingSql(filter);
			if(sortingSql!=null && !sortingSql.isEmpty()){
				queryStr += sortingSql;
			}
				
			session  = sessionFactory.openSession();
			Query query = session.createSQLQuery(queryStr);
			startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
			List<PayPalTracking> list = query.setResultTransformer(Transformers.aliasToBean(PayPalTracking.class)).setFirstResult(startFrom).setMaxResults(PaginationUtil.PAGESIZE).list();
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	
	public Integer getPayPalTrackingCount(GridHeaderFilters filter){
		Session session = null;
		Integer count = 0;		
		try {
			String queryStr = "Select count(*) as cnt "+
								"from paypal_tracking pt with(nolock) join paypal_transaction_details ptd with(nolock) on pt.transaction_id = ptd.id";
			/*
			if(orderId!=null && orderId>0){
				queryStr += " AND ptd.order_id = "+orderId+"";
			}
			if(customerName!=null && !customerName.isEmpty()){
				queryStr += " AND ptd.payer_f_name like '%"+customerName+"%'";
			}
			if(transactionStatus != null && !transactionStatus.isEmpty()){
				queryStr += " AND pt.status like '%"+transactionStatus+"%'";
			}
			*/
			if(filter.getCustomerId() != null){
				queryStr += " AND pt.customer_id = "+filter.getCustomerId()+" ";
			}
			if(filter.getCustomerName() != null){
				queryStr += " AND ptd.payer_f_name like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getLastName() != null){
				queryStr += " AND ptd.payer_l_name like '%"+filter.getLastName()+"%' ";
			}
			if(filter.getEmail() != null){
				queryStr += " AND ptd.payer_email like '%"+filter.getEmail()+"%' ";
			}
			if(filter.getPhone() != null){
				queryStr += " AND ptd.payer_phone like '%"+filter.getPhone()+"%' ";
			}
			if(filter.getCustomerOrderId() != null){
				queryStr += " AND ptd.order_id = "+filter.getCustomerOrderId()+" ";
			}
			if(filter.getOrderTotal() != null){
				queryStr += " AND CAST(pt.order_total AS DOUBLE PRECISION) = "+filter.getOrderTotal()+" ";
			}
			if(filter.getOrderType() != null){
				queryStr += " AND pt.order_type like '%"+filter.getOrderType()+"%' ";
			}
			if(filter.getEventId() != null){
				queryStr += " AND pt.event_id = "+filter.getEventId()+" ";
			}
			if(filter.getQuantity() != null){
				queryStr += " AND pt.qty = "+filter.getQuantity()+" ";
			}
			if(filter.getZone() != null){
				queryStr += " AND pt.zone like '%"+filter.getZone()+"%' ";
			}
			if(filter.getPaypalTransactionId() != null){
				queryStr += " AND pt.paypal_transaction_id like '%"+filter.getPaypalTransactionId()+"%' ";
			}
			if(filter.getTransactionId() != null){
				queryStr += " AND pt.transaction_id = "+filter.getTransactionId()+" ";
			}
			if(filter.getPaymentId() != null){
				queryStr += " AND ptd.payment_id like '%"+filter.getPaymentId()+"%' ";
			}
			if(filter.getPlatform() != null){
				queryStr += " AND pt.platform like '%"+filter.getPlatform()+"%' ";
			}
			if(filter.getStatus() != null){
				queryStr += " AND pt.status like '%"+filter.getStatus()+"%' ";
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					queryStr += " AND DATEPART(day,pt.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					queryStr += " AND DATEPART(month,pt.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					queryStr += " AND DATEPART(year,pt.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getLastUpdatedDateStr() != null){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					queryStr += " AND DATEPART(day,pt.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
					queryStr += " AND DATEPART(month,pt.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
					queryStr += " AND DATEPART(year,pt.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getTransactionDateStr() != null){
				if(Util.extractDateElement(filter.getTransactionDateStr(),"DAY") > 0){
					queryStr += " AND DATEPART(day,ptd.transaction_date) = "+Util.extractDateElement(filter.getTransactionDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getTransactionDateStr(),"MONTH") > 0){
					queryStr += " AND DATEPART(month,ptd.transaction_date) = "+Util.extractDateElement(filter.getTransactionDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getTransactionDateStr(),"YEAR") > 0){
					queryStr += " AND DATEPART(year,ptd.transaction_date) = "+Util.extractDateElement(filter.getTransactionDateStr(),"YEAR")+" ";
				}
			}
			session  = sessionFactory.openSession();
			Query query = session.createSQLQuery(queryStr);
			count = Integer.parseInt(String.valueOf(query.uniqueResult()));
			return count;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	

	public List<Recipient> getRecipientListByPO(Integer shippingId, Integer purchaseOrderId) {
		List<Recipient> recipientList = new ArrayList<Recipient>();
		Session session = null;
		try {
			StringBuffer query = new StringBuffer("");
			List<Object[]> list = new ArrayList<Object[]>();

			session = getSessionFactory().openSession();

			query.append("select distinct ca.id AS id,ca.first_name AS name,ca.address_line1 as Add1,ca.address_line2 AS add2,ca.city AS city,");
			query.append("s.short_desc AS stateCode,co.short_desc AS countryCode,ca.zip_code AS pincode,ca.phone1 AS phone  ");
			query.append("from customer_address ca with(nolock) ");
			query.append("left join state s with(nolock) on ca.state = s.id ");
			query.append("left join country co with(nolock) on ca.country = co.id ");
			query.append("WHERE 1=1 ");

			if (shippingId != null)
				query.append(" and ca.id =" + shippingId);

			query.append(" order by ca.id");

			SQLQuery querys = session.createSQLQuery(query.toString());
			list = querys.list();

			if (list != null && list.size() > 0) {
				for (Object[] array : list) {
					Recipient recipient = new Recipient();
					recipient.setPurchaseOrderId(purchaseOrderId);
					recipient.setShippingId((Integer) array[0]);					
					recipient.setRecipientName((String) array[1]);
					recipient.setAddressStreet1((String) array[2]);
					recipient.setAddressStreet2((String) array[3]);
					recipient.setCity((String) array[4]);
					recipient.setStateProvinceCode((String) array[5]);
					recipient.setCountryCode((String) array[6]);
					recipient.setPostalCode((String) array[7]);
					recipient.setPhoneNumber((String) array[8]);
					recipientList.add(recipient);

				}
			}
		} catch (Exception e) {
			System.out.println("The Exception is" + e);
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return recipientList;
	}

	public List<PopularArtist> getAllArtist(GridHeaderFilters filter,String pageNo){
		List<Artist> artistList = new ArrayList<Artist>();
		Integer startFrom = 0;
		Session session = null;
		try{
			session = getSessionFactory().openSession();
			
			String queryStr = "select distinct ar.id as artistId,REPLACE(ar.name,'\"','') as artistName,gc.name as grandChildCategoryName,"+
				"cc.name as childCategoryName,pc.name as parentType, "+
				" ai.image_url as imageFileUrl,artistEvent.eventCount as eventCount "+
				"FROM artist ar with(nolock) "+
				"inner join artist_category_mapping ac with(nolock) on ac.artist_id=ar.id "+
				"inner join grand_child_category gc with(nolock) on gc.id=ac.grand_child_category_id "+
				"inner join child_category cc with(nolock) on cc.id=ac.child_category_id "+
				"inner join parent_category pc with(nolock) on pc.id=ac.parent_category_id "+
				"left join artist_image ai with(nolock) on ar.id = ai.artist_id " +
				"left join (select artist_id,count(event_id) as eventCount from event_artist with(nolock) group by artist_id) artistEvent " +
				"on artistEvent.artist_id=ar.id "+
				" where ar.display_on_search = 1 ";
			
			if(filter.getArtistName() != null){
				queryStr += " AND REPLACE(ar.name,'\"','') like '%"+filter.getArtistName()+"%' ";
			}
			if(filter.getGrandChildCategoryName() != null){
				queryStr += " AND gc.name like '%"+filter.getGrandChildCategoryName()+"%' ";
			}
			if(filter.getChildCategoryName() != null){
				queryStr += " AND cc.name like '%"+filter.getChildCategoryName()+"%' ";
			}
			if(filter.getParentCategoryName() != null){
				queryStr += " AND pc.name like '%"+filter.getParentCategoryName()+"%' ";
			}
			if(filter.getEventCount()!=null){
				queryStr += " AND artistEvent.eventCount = "+filter.getEventCount()+" ";
			}
			
			String sortingSql = GridSortingUtil.getAllArtistSortingSql(filter);
			if(sortingSql!=null && !sortingSql.isEmpty()){
				queryStr += sortingSql;
			}
			
			session  = sessionFactory.openSession();
			Query query = session.createSQLQuery(queryStr);
			startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
			List<PopularArtist> list = query.setResultTransformer(Transformers.aliasToBean(PopularArtist.class)).setFirstResult(startFrom).setMaxResults(PaginationUtil.PAGESIZE).list();
			return list;
		}catch(Exception e){
			System.out.println("The Exception is: "+ e);
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}	
	}
	
	public Integer getAllArtistCount(GridHeaderFilters filter){
		Integer count = 0;
		Session session = null;
		try{
			String SQLQuery = "select count(*) as cnt FROM artist ar with(nolock) "+
				"inner join artist_category_mapping ac with(nolock) on ac.artist_id=ar.id "+
				"inner join grand_child_category gc with(nolock) on gc.id=ac.grand_child_category_id "+
				"inner join child_category cc with(nolock) on cc.id=ac.child_category_id "+
				"inner join parent_category pc with(nolock) on pc.id=ac.parent_category_id "+
				"left join artist_image ai with(nolock) on ar.id = ai.artist_id "+
				"left join (select artist_id,count(event_id) as eventCount from event_artist with(nolock) group by artist_id) artistEvent " +
				"on artistEvent.artist_id=ar.id "+
				" where ar.display_on_search = 1 ";
			
			if(filter.getArtistName() != null){
				SQLQuery += " AND REPLACE(ar.name,'\"','') like '%"+filter.getArtistName()+"%' ";
			}
			if(filter.getGrandChildCategoryName() != null){
				SQLQuery += " AND gc.name like '%"+filter.getGrandChildCategoryName()+"%' ";
			}
			if(filter.getChildCategoryName() != null){
				SQLQuery += " AND cc.name like '%"+filter.getChildCategoryName()+"%' ";
			}
			if(filter.getParentCategoryName() != null){
				SQLQuery += " AND pc.name like '%"+filter.getParentCategoryName()+"%' ";
			}
			if(filter.getEventCount()!=null){
				SQLQuery += " AND artistEvent.eventCount = "+filter.getEventCount()+" ";
			}
			
			session  = sessionFactory.openSession();
			Query query = session.createSQLQuery(SQLQuery);
			count = Integer.parseInt(String.valueOf(query.uniqueResult()));
			return count;
			
		}catch(Exception e){
			System.out.println("The Exception is: "+ e);
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	public List<Event> getAllActiveEventWithVenue(String eventName){
		Session session = null;
		List<Event> events = null;
		Date today = new Date();
		String date = Util.formatDateToMonthDateYear(today);
		try {
			String SQLQuery = "select E.id AS eventId, E.name as eventName, E.event_date AS eventDate,E.event_time AS eventTime, "+
			"V.building as building,V.city AS city, V.state as state,V.country as country "+
			"from event E with(nolock) left join venue V with(nolock) on E.venue_id=V.id where E.status=1 ";
			//and E.event_date > '"+date+"' ";
			if(eventName!=null && !eventName.isEmpty()){
				SQLQuery += "AND E.name like '%"+eventName+"%' ";
			}
			SQLQuery += "order by E.event_date, E.event_time Asc";
			session  = getStaticSession();
			SQLQuery query = session.createSQLQuery(SQLQuery);
			query.addScalar("eventId", Hibernate.INTEGER);
			query.addScalar("eventName", Hibernate.STRING);
			query.addScalar("eventDate", Hibernate.DATE);
			query.addScalar("eventTime", Hibernate.TIME);
			query.addScalar("building", Hibernate.STRING);
			query.addScalar("city", Hibernate.STRING);
			query.addScalar("state", Hibernate.STRING);
			query.addScalar("country", Hibernate.STRING);
			events = query.setResultTransformer(Transformers.aliasToBean(Event.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return events;
	}
	
	
	public List<FantasyChildCategory> getChildCategoryDetails(GridHeaderFilters filter,String pageNo){
		Integer startFrom = 0;
		Session session = null;
		try{
			session = getSessionFactory().openSession();
			
			String queryStr = "select cc.id as id, cc.name as name, pc.name as parentCategoryName, "+
				"cc.last_updated as lastUpdated, cc.last_updated_by as lastUpdatedBy "+
				"FROM fantasy_child_category cc with(nolock) "+
				"left join fantasy_parent_category pc with(nolock) on cc.parent_id = pc.id "+
				" where 1 = 1";
			
			if(filter.getChildCategoryId() != null){
				queryStr += " AND cc.id = "+filter.getChildCategoryId()+" ";
			}
			if(filter.getChildCategoryName() != null){
				queryStr += " AND cc.name like '%"+filter.getChildCategoryName()+"%' ";
			}
			if(filter.getParentCategoryName() != null){
				queryStr += " AND pc.name like '%"+filter.getParentCategoryName()+"%' ";
			}
			if(filter.getLastUpdatedBy() != null){
				queryStr += " AND cc.last_updated_by like '%"+filter.getLastUpdatedBy()+"%' ";
			}
			if(filter.getLastUpdatedDateStr() != null){
				String dateTimeStr = filter.getLastUpdatedDateStr();
				if(dateTimeStr.contains(" ")){
					String dateTimeArr[] = dateTimeStr.split(" ");
					if(Util.extractDateElement(dateTimeArr[0],"DAY") > 0){
						queryStr += " AND DATEPART(day,cc.last_updated) = "+Util.extractDateElement(dateTimeArr[0],"DAY")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"MONTH") > 0){
						queryStr += " AND DATEPART(month,cc.last_updated) = "+Util.extractDateElement(dateTimeArr[0],"MONTH")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"YEAR") > 0){
						queryStr += " AND DATEPART(year,cc.last_updated) = "+Util.extractDateElement(dateTimeArr[0],"YEAR")+" ";
					}
					if(dateTimeArr.length>1){
						String dateTime = dateTimeArr[1];
						if(dateTimeArr.length > 2){
							if(dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()){
								dateTime += " "+dateTimeArr[2];
							}
						}
						if(Util.extractDateElement(dateTime,"HOUR") > 0){
							queryStr += " AND DATEPART(hour,cc.last_updated) = "+Util.extractDateElement(dateTime,"HOUR")+" ";
						}
						if(Util.extractDateElement(dateTime,"MINUTE") > 0){
							queryStr += " AND DATEPART(minute,cc.last_updated) = "+Util.extractDateElement(dateTime,"MINUTE")+" ";
						}
					}
				}else{
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
						queryStr += " AND DATEPART(day,cc.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
						queryStr += " AND DATEPART(month,cc.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
						queryStr += " AND DATEPART(year,cc.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
					}
				}
			}
			session  = sessionFactory.openSession();
			SQLQuery query = session.createSQLQuery(queryStr);
			startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
			List<FantasyChildCategory> list = query.setResultTransformer(Transformers.aliasToBean(FantasyChildCategory.class)).setFirstResult(startFrom).setMaxResults(PaginationUtil.PAGESIZE).list();
			return list;
		}catch(Exception e){
			System.out.println("The Exception is: "+ e);
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	public Integer getChildCategoryDetailsCount(GridHeaderFilters filter){
		Integer count = 0;
		Session session = null;
		try{
			session = getSessionFactory().openSession();
			
			String queryStr = "select count(*) as cnt "+
				"FROM fantasy_child_category cc with(nolock) "+
				"left join fantasy_parent_category pc with(nolock) on cc.parent_id = pc.id "+
				" where 1 = 1";
			
			if(filter.getChildCategoryId() != null){
				queryStr += " AND cc.id = "+filter.getChildCategoryId()+" ";
			}
			if(filter.getChildCategoryName() != null){
				queryStr += " AND cc.name like '%"+filter.getChildCategoryName()+"%' ";
			}
			if(filter.getParentCategoryName() != null){
				queryStr += " AND pc.name like '%"+filter.getParentCategoryName()+"%' ";
			}
			if(filter.getLastUpdatedBy() != null){
				queryStr += " AND cc.last_updated_by like '%"+filter.getLastUpdatedBy()+"%' ";
			}
			if(filter.getLastUpdatedDateStr() != null){
				String dateTimeStr = filter.getLastUpdatedDateStr();
				if(dateTimeStr.contains(" ")){
					String dateTimeArr[] = dateTimeStr.split(" ");
					if(Util.extractDateElement(dateTimeArr[0],"DAY") > 0){
						queryStr += " AND DATEPART(day,cc.last_updated) = "+Util.extractDateElement(dateTimeArr[0],"DAY")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"MONTH") > 0){
						queryStr += " AND DATEPART(month,cc.last_updated) = "+Util.extractDateElement(dateTimeArr[0],"MONTH")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"YEAR") > 0){
						queryStr += " AND DATEPART(year,cc.last_updated) = "+Util.extractDateElement(dateTimeArr[0],"YEAR")+" ";
					}
					if(dateTimeArr.length>1){
						String dateTime = dateTimeArr[1];
						if(dateTimeArr.length > 2){
							if(dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()){
								dateTime += " "+dateTimeArr[2];
							}
						}
						if(Util.extractDateElement(dateTime,"HOUR") > 0){
							queryStr += " AND DATEPART(hour,cc.last_updated) = "+Util.extractDateElement(dateTime,"HOUR")+" ";
						}
						if(Util.extractDateElement(dateTime,"MINUTE") > 0){
							queryStr += " AND DATEPART(minute,cc.last_updated) = "+Util.extractDateElement(dateTime,"MINUTE")+" ";
						}
					}
				}else{
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
						queryStr += " AND DATEPART(day,cc.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
						queryStr += " AND DATEPART(month,cc.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
						queryStr += " AND DATEPART(year,cc.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
					}
				}
			}
			session  = sessionFactory.openSession();
			Query query = session.createSQLQuery(queryStr);
			count = Integer.parseInt(String.valueOf(query.uniqueResult()));
			return count;
		}catch(Exception e){
			System.out.println("The Exception is: "+ e);
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	

	public List<FantasyGrandChildCategory> getGrandChildCategoryDetails(GridHeaderFilters filter,String pageNo){
		Integer startFrom = 0;
		Session session = null;
		try{
			session = getSessionFactory().openSession();
			
			String queryStr = "select gc.id as id, gc.name as name, pc.name as parentCategoryName, cc.name as childCategoryName, "+
				"gc.package_cost as packageCost, gc.last_updated as lastUpdated, gc.last_updated_by as lastUpdatedBy "+
				"FROM fantasy_grandchild_category gc with(nolock) "+
				"left join fantasy_child_category cc with(nolock) on gc.child_id = cc.id "+
				"left join fantasy_parent_category pc with(nolock) on cc.parent_id = pc.id "+
				" where 1 = 1";
			
			if(filter.getGrandChildCategoryId() != null){
				queryStr += " AND gc.id = "+filter.getGrandChildCategoryId()+" ";
			}
			if(filter.getGrandChildCategoryName() != null){
				queryStr += " AND gc.name like '%"+filter.getGrandChildCategoryName()+"%' ";
			}
			if(filter.getChildCategoryName() != null){
				queryStr += " AND cc.name like '%"+filter.getChildCategoryName()+"%' ";
			}
			if(filter.getParentCategoryName() != null){
				queryStr += " AND pc.name like '%"+filter.getParentCategoryName()+"%' ";
			}
			if(filter.getPackageCost() != null){
				queryStr += " AND gc.package_cost = "+filter.getPackageCost()+" ";
			}
			if(filter.getLastUpdatedBy() != null){
				queryStr += " AND gc.last_updated_by like '%"+filter.getLastUpdatedBy()+"%' ";
			}
			if(filter.getLastUpdatedDateStr() != null){				
				String dateTimeStr = filter.getLastUpdatedDateStr();
				if(dateTimeStr.contains(" ")){
					String dateTimeArr[] = dateTimeStr.split(" ");
					if(Util.extractDateElement(dateTimeArr[0],"DAY") > 0){
						queryStr += " AND DATEPART(day,gc.last_updated) = "+Util.extractDateElement(dateTimeArr[0],"DAY")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"MONTH") > 0){
						queryStr += " AND DATEPART(month,gc.last_updated) = "+Util.extractDateElement(dateTimeArr[0],"MONTH")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"YEAR") > 0){
						queryStr += " AND DATEPART(year,gc.last_updated) = "+Util.extractDateElement(dateTimeArr[0],"YEAR")+" ";
					}
					if(dateTimeArr.length>1){
						String dateTime = dateTimeArr[1];
						if(dateTimeArr.length > 2){
							if(dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()){
								dateTime += " "+dateTimeArr[2];
							}
						}
						if(Util.extractDateElement(dateTime,"HOUR") > 0){
							queryStr += " AND DATEPART(hour,gc.last_updated) = "+Util.extractDateElement(dateTime,"HOUR")+" ";
						}
						if(Util.extractDateElement(dateTime,"MINUTE") > 0){
							queryStr += " AND DATEPART(minute,gc.last_updated) = "+Util.extractDateElement(dateTime,"MINUTE")+" ";
						}
					}
				}else{
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
						queryStr += " AND DATEPART(day,gc.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
						queryStr += " AND DATEPART(month,gc.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
						queryStr += " AND DATEPART(year,gc.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
					}
				}
			}
			session  = sessionFactory.openSession();
			SQLQuery query = session.createSQLQuery(queryStr);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("name", Hibernate.STRING);
			query.addScalar("parentCategoryName", Hibernate.STRING);
			query.addScalar("childCategoryName", Hibernate.STRING);
			query.addScalar("packageCost", Hibernate.DOUBLE);
			query.addScalar("lastUpdated", Hibernate.TIMESTAMP);
			query.addScalar("lastUpdatedBy", Hibernate.STRING);
			
			startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
			List<FantasyGrandChildCategory> list = query.setResultTransformer(Transformers.aliasToBean(FantasyGrandChildCategory.class)).setFirstResult(startFrom).setMaxResults(PaginationUtil.PAGESIZE).list();
			return list;
		}catch(Exception e){
			System.out.println("The Exception is: "+ e);
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	public Integer getGrandChildCategoryDetailsCount(GridHeaderFilters filter){
		Integer count = 0;
		Session session = null;
		try{
			session = getSessionFactory().openSession();
			
			String queryStr = "select count(*) as cnt "+
			"FROM fantasy_grandchild_category gc with(nolock) "+
			"left join fantasy_child_category cc with(nolock) on gc.child_id = cc.id "+
			"left join fantasy_parent_category pc with(nolock) on cc.parent_id = pc.id "+
			" where 1 = 1";
		
			if(filter.getGrandChildCategoryId() != null){
				queryStr += " AND gc.id = "+filter.getGrandChildCategoryId()+" ";
			}
			if(filter.getGrandChildCategoryName() != null){
				queryStr += " AND gc.name like '%"+filter.getGrandChildCategoryName()+"%' ";
			}
			if(filter.getChildCategoryName() != null){
				queryStr += " AND cc.name like '%"+filter.getChildCategoryName()+"%' ";
			}
			if(filter.getParentCategoryName() != null){
				queryStr += " AND pc.name like '%"+filter.getParentCategoryName()+"%' ";
			}
			if(filter.getPackageCost() != null){
				queryStr += " AND gc.package_cost = "+filter.getPackageCost()+" ";
			}
			if(filter.getLastUpdatedBy() != null){
				queryStr += " AND gc.last_updated_by like '%"+filter.getLastUpdatedBy()+"%' ";
			}
			if(filter.getLastUpdatedDateStr() != null){				
				String dateTimeStr = filter.getLastUpdatedDateStr();
				if(dateTimeStr.contains(" ")){
					String dateTimeArr[] = dateTimeStr.split(" ");
					if(Util.extractDateElement(dateTimeArr[0],"DAY") > 0){
						queryStr += " AND DATEPART(day,gc.last_updated) = "+Util.extractDateElement(dateTimeArr[0],"DAY")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"MONTH") > 0){
						queryStr += " AND DATEPART(month,gc.last_updated) = "+Util.extractDateElement(dateTimeArr[0],"MONTH")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"YEAR") > 0){
						queryStr += " AND DATEPART(year,gc.last_updated) = "+Util.extractDateElement(dateTimeArr[0],"YEAR")+" ";
					}
					if(dateTimeArr.length>1){
						String dateTime = dateTimeArr[1];
						if(dateTimeArr.length > 2){
							if(dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()){
								dateTime += " "+dateTimeArr[2];
							}
						}
						if(Util.extractDateElement(dateTime,"HOUR") > 0){
							queryStr += " AND DATEPART(hour,gc.last_updated) = "+Util.extractDateElement(dateTime,"HOUR")+" ";
						}
						if(Util.extractDateElement(dateTime,"MINUTE") > 0){
							queryStr += " AND DATEPART(minute,gc.last_updated) = "+Util.extractDateElement(dateTime,"MINUTE")+" ";
						}
					}
				}else{
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
						queryStr += " AND DATEPART(day,gc.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
						queryStr += " AND DATEPART(month,gc.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
						queryStr += " AND DATEPART(year,gc.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
					}
				}
			}
			session  = sessionFactory.openSession();
			Query query = session.createSQLQuery(queryStr);
			count = Integer.parseInt(String.valueOf(query.uniqueResult()));
			return count;
		}catch(Exception e){
			System.out.println("The Exception is: "+ e);
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	
	
	public List<Event> getEventsByArtistAndCity(Integer artistId,String city){
		Integer count = 0;
		Session session = null;
		List<Event> events = null;
		try{
			session = getSessionFactory().openSession();
			
			String queryStr = "select e.id as eventId,e.name AS eventName, e.event_date As eventDate,e.event_time as eventTime, v.building as building,"+
					"v.city as city, v.state as state,v.country as country,a.artist_name as artistName "+
					"from event e with(nolock) left join venue v with(nolock) on e.venue_id=v.id left join event_artist_details a with(nolock) on a.event_id=e.id where 1=1 ";
		
			if(city != null && !city.isEmpty()){
				queryStr += " AND v.city like '%"+city+"%'";
			}
			if(artistId != null){
				queryStr += " AND a.artist_id="+artistId;
			}
			session  = sessionFactory.openSession();
			SQLQuery query = session.createSQLQuery(queryStr);
			query.addScalar("eventId", Hibernate.INTEGER);
			query.addScalar("eventName", Hibernate.STRING);
			query.addScalar("eventDate", Hibernate.DATE);
			query.addScalar("eventTime", Hibernate.TIME);
			query.addScalar("artistName", Hibernate.STRING);
			query.addScalar("building", Hibernate.STRING);
			query.addScalar("city", Hibernate.STRING);
			query.addScalar("state", Hibernate.STRING);
			query.addScalar("country", Hibernate.STRING);
			events = query.setResultTransformer(Transformers.aliasToBean(Event.class)).list();
		}catch(Exception e){
			System.out.println("The Exception is: "+ e);
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return events;
	}
	
	
	public List<Integer> getAllArtistIdsByEventId(Integer eventId){
		Session session = null;
		List<Integer> artistIds = new ArrayList<Integer>();
		try{
			session = getSessionFactory().openSession();
			String queryStr = "select artist_id from event_artist_details with(nolock) where event_id="+eventId;
			session  = sessionFactory.openSession();
			SQLQuery query = session.createSQLQuery(queryStr);
			artistIds =  query.list();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return artistIds;
	}
	
	public String getArtistNameByEventId(Integer eventId){
		Session session = null;
		String name = "";
		try{
			session = getSessionFactory().openSession();
			String queryStr = "select top 1 artist_name from event_artist_details with(nolock) where event_id="+eventId;
			session  = sessionFactory.openSession();
			SQLQuery query = session.createSQLQuery(queryStr);
			name =  (String) query.uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return name;
	}
	
	
	public Double getCheapestZonesPrice(Integer eventId,String section){
		Session session = null;
		BigDecimal price;
		try{
			session = getSessionFactory().openSession();
			String queryStr = "select top 1 min(zone_price) from crownjewel_category_ticket with(nolock) where status='ACTIVE'"+
					" AND event_id="+eventId+" AND section='"+section+"' group by section";
			session  = getStaticSession();
			SQLQuery query = session.createSQLQuery(queryStr);
			price =  (BigDecimal) query.uniqueResult();
			if(price!=null){
				return price.doubleValue();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public List<RealTicketSectionDetails> getRealTicketSectionDetailsByInvoiceId(Integer invoiceId){
		String sql = "select tg.id as ticketGroupId,tg.section AS section,tg.row as row,min(t.seat_number) As seatLow,max(t.seat_number) AS seatHigh " +
				"from ticket t with(nolock) join ticket_group tg with(nolock) on t.ticket_group_id=tg.id " +
				"where t.invoice_id="+invoiceId+" group by tg.id,tg.section,tg.row";
		List<RealTicketSectionDetails> list = null;
		try {
			staticSession = QueryManagerDAO.getStaticSession();
			Query query = staticSession.createSQLQuery(sql);
			list= query.setResultTransformer(Transformers.aliasToBean(RealTicketSectionDetails.class)).list();
		} catch (Exception e) {
			if(staticSession != null && staticSession.isOpen() ){
				staticSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	
	public boolean updateCustomerReferralCode(String sql){
		try {
			staticSession  = getStaticSession();
			SQLQuery query =  staticSession.createSQLQuery(sql);
			query.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public InvoiceRefund getLatestPaypalRefund(){
		String sql = "select id as id,order_id AS orderId,refund_id AS refundId,charge_id AS transactionId,amount AS amount,"+
		"reverted_used_points AS revertedUsedRewardPoints,reverted_earned_points AS revertedEarnedRewardPoints,reason AS reason,"+
		"status AS status,create_time AS createdTime,refund_type AS refundType,refunded_by AS refundedBy from invoice_refund with(nolock) " +
		"where refund_type='PAYPAL' order by create_time desc";
		try {
			staticSession = QueryManagerDAO.getStaticSession();
			SQLQuery query = staticSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("orderId", Hibernate.INTEGER);
			query.addScalar("amount", Hibernate.DOUBLE);
			query.addScalar("transactionId", Hibernate.STRING);
			query.addScalar("refundId", Hibernate.STRING);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("reason", Hibernate.STRING);
			query.addScalar("revertedUsedRewardPoints", Hibernate.DOUBLE);
			query.addScalar("revertedEarnedRewardPoints", Hibernate.DOUBLE);
			query.addScalar("createdTime", Hibernate.TIMESTAMP);
			query.addScalar("refundType", Hibernate.STRING);
			query.addScalar("refundedBy", Hibernate.STRING);
			List<InvoiceRefund> refundList = query.setResultTransformer(Transformers.aliasToBean(InvoiceRefund.class)).setMaxResults(1).list();
			if(!refundList.isEmpty()){
				return refundList.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public List<RTFPromoOffers> getDiscountCodes(Integer id, GridHeaderFilters filter){
		List<RTFPromoOffers> list = null;		
		try {
			String sql = "select rtf.id as id,rtf.promo_code as promoCode,rtf.discount as discountPer,rtf.start_date as startDate," +
					"rtf.end_date as endDate,rtf.max_orders as maxOrders,rtf.no_of_orders as noOfOrders," +
					"rtf.status as status,rtf.created_by as createdBy,rtf.created_date as createdDate," +
					"rtf.modified_by as modifiedBy,rtf.modified_time as modifiedDate,rtf.is_flat_discount as isFlatDiscount,"+
					"rtf.flat_offer_order_threshold as flatOfferOrderThreshold, rtf.mobile_discount as mobileDiscountPerc ";
			
			if(id != null){
				sql += ", rtfpt.promo_type as promoType, rtfpt.artist_id as artistId, rtfpt.venue_id as venueId, rtfpt.parent_id as parentId, rtfpt.child_id as childId, "+
					"rtfpt.grand_child_id as grandChildId, a.name as artistName, v.building as venueName, pc.name as parentCategory, cc.name as childCategory, gcc.name as grandChildCategory, "+
					"rtfpt.event_id as eventId, e.name as eventName ";
			}
		
			sql += " from rtf_promotional_offer_hdr rtf with(nolock) ";
			
			if(id != null){
				sql += "left join rtf_promotional_offer_dtl rtfpt with(nolock) on rtfpt.promo_offer_id = rtf.id " +
					"left join artist a with(nolock) on a.id=rtfpt.artist_id " +
					"left join venue v with(nolock) on v.id=rtfpt.venue_id " +
					"left join parent_category pc with(nolock) on pc.id=rtfpt.parent_id " +
					"left join child_category cc with(nolock) on cc.id=rtfpt.child_id " +
					"left join grand_child_category gcc with(nolock) on gcc.id=rtfpt.grand_child_id " +
					"left join event e with(nolock) on e.id = rtfpt.event_id ";
			}
			sql += " WHERE 1=1 ";
			
			if(id != null){
				sql += " AND rtf.id = "+id;
			}
			if(filter.getPromoCode()!=null && !filter.getPromoCode().isEmpty()){
				sql += " AND rtf.promo_code like '"+filter.getPromoCode()+"' ";
			}
			if(filter.getDiscountCouponPrice()!=null){
				sql += " AND rtf.discount = "+filter.getDiscountCouponPrice();
			}
			if(filter.getMobileDiscount() != null){
				sql += " AND rtf.mobile_discount = "+filter.getMobileDiscount(); 
			}
			if(filter.getStartDate()!=null ){
				if(Util.extractDateElement(filter.getStartDate(),"DAY") > 0){
					sql += " AND DATEPART(day,rtf.start_date) = "+Util.extractDateElement(filter.getStartDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getStartDate(),"MONTH") > 0){
					sql += " AND DATEPART(month,rtf.start_date) = "+Util.extractDateElement(filter.getStartDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getStartDate(),"YEAR") > 0){
					sql += " AND DATEPART(year,rtf.start_date) = "+Util.extractDateElement(filter.getStartDate(),"YEAR")+" ";
				}
				/*if(Util.extractDateElement(filter.getStartDate(),"HOUR") > 0){
					sql += " AND DATEPART(hour,rtf.start_date) = "+Util.extractDateElement(filter.getStartDate(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getStartDate(),"MINUTE") > 0){
					sql += " AND DATEPART(minute,rtf.start_date) = "+Util.extractDateElement(filter.getStartDate(),"MINUTE")+" ";
				}*/
			}
			if(filter.getEndDate()!=null ){
				if(Util.extractDateElement(filter.getEndDate(),"DAY") > 0){
					sql += " AND DATEPART(day,rtf.end_date) = "+Util.extractDateElement(filter.getEndDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEndDate(),"MONTH") > 0){
					sql += " AND DATEPART(month,rtf.end_date) = "+Util.extractDateElement(filter.getEndDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEndDate(),"YEAR") > 0){
					sql += " AND DATEPART(year,rtf.end_date) = "+Util.extractDateElement(filter.getEndDate(),"YEAR")+" ";
				}
				/*if(Util.extractDateElement(filter.getEndDate(),"HOUR") > 0){
					sql += " AND DATEPART(hour,rtf.end_date) = "+Util.extractDateElement(filter.getEndDate(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEndDate(),"MINUTE") > 0){
					sql += " AND DATEPART(minute,rtf.end_date) = "+Util.extractDateElement(filter.getEndDate(),"MINUTE")+" ";
				}*/
			}
			
			if(filter.getMaxOrders()!=null){
				sql += " AND rtf.max_orders = "+filter.getMaxOrders();
			}
			if(filter.getNoOfOrders()!=null){
				sql += " AND rtf.no_of_orders = "+filter.getNoOfOrders();
			}
			
			/*if(filter.getPromoType()!=null && !filter.getPromoType().isEmpty()){
				sql += " AND rtfpt.promo_type like '"+filter.getPromoType()+"' ";
			}
			if(filter.getArtistName()!=null && !filter.getArtistName().isEmpty()){
				sql += " AND a.name like '%"+filter.getArtistName()+"%' ";
			}
			if(filter.getVenueName()!=null && !filter.getVenueName().isEmpty()){
				sql += " AND v.building like '%"+filter.getVenueName()+"%' ";
			}
			if(filter.getParentCategoryName()!=null && !filter.getParentCategoryName().isEmpty()){
				sql += " AND pc.name like '%"+filter.getParentCategoryName()+"%' ";
			}
			if(filter.getChildCategoryName()!=null && !filter.getChildCategoryName().isEmpty()){
				sql += " AND cc.name like '"+filter.getChildCategoryName()+"' ";
			}
			if(filter.getGrandChildCategoryName()!=null && !filter.getGrandChildCategoryName().isEmpty()){
				sql += " AND gcc.name like '"+filter.getGrandChildCategoryName()+"' ";
			}*/
			if(filter.getStatus()!=null && !filter.getStatus().isEmpty()){
				sql += " AND rtf.status like '"+filter.getStatus()+"' ";
			}
			if(filter.getCreatedBy()!=null && !filter.getCreatedBy().isEmpty()){
				sql += " AND rtf.created_by like '"+filter.getCreatedBy()+"' ";
			}
			if(filter.getLastUpdatedBy()!=null && !filter.getLastUpdatedBy().isEmpty()){
				sql += " AND rtf.modified_by like '"+filter.getLastUpdatedBy()+"' ";
			}
			
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,rtf.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,rtf.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,rtf.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
				/*if(Util.extractDateElement(filter.getCreatedDateStr(),"HOUR") > 0){
					sql += " AND DATEPART(hour,rtf.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MINUTE") > 0){
					sql += " AND DATEPART(minute,rtf.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MINUTE")+" ";
				}*/
			}
			if(filter.getLastUpdatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,rtf.modified_time) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,rtf.modified_time) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,rtf.modified_time) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
				}
				/*if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"HOUR") > 0){
					sql += " AND DATEPART(hour,rtf.modified_time) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MINUTE") > 0){
					sql += " AND DATEPART(minute,rtf.modified_time) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MINUTE")+" ";
				}*/
			}
			
			staticSession = QueryManagerDAO.getStaticSession();
			SQLQuery query = staticSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("promoCode", Hibernate.STRING);
			query.addScalar("discountPer", Hibernate.DOUBLE);
			query.addScalar("startDate", Hibernate.TIMESTAMP);
			query.addScalar("endDate", Hibernate.TIMESTAMP);
			query.addScalar("maxOrders", Hibernate.INTEGER);
			query.addScalar("noOfOrders", Hibernate.INTEGER);			
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("modifiedBy", Hibernate.STRING);
			query.addScalar("modifiedDate", Hibernate.TIMESTAMP);			
			query.addScalar("isFlatDiscount", Hibernate.BOOLEAN);
			query.addScalar("flatOfferOrderThreshold", Hibernate.DOUBLE);
			query.addScalar("mobileDiscountPerc", Hibernate.DOUBLE);
		
			if(id != null){
				query.addScalar("promoType", Hibernate.STRING);
				query.addScalar("artistId", Hibernate.INTEGER);
				query.addScalar("venueId", Hibernate.INTEGER);
				query.addScalar("grandChildId", Hibernate.INTEGER);
				query.addScalar("childId", Hibernate.INTEGER);
				query.addScalar("parentId", Hibernate.INTEGER);
				query.addScalar("artistName", Hibernate.STRING);
				query.addScalar("venueName", Hibernate.STRING);
				query.addScalar("parentCategory", Hibernate.STRING);
				query.addScalar("childCategory", Hibernate.STRING);
				query.addScalar("grandChildCategory", Hibernate.STRING);
				query.addScalar("eventId", Hibernate.INTEGER);
				query.addScalar("eventName", Hibernate.STRING);
			}
			
			list = query.setResultTransformer(Transformers.aliasToBean(RTFPromoOffers.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	public List<RTFPromoOffers> getDiscountCodes(Integer id, GridHeaderFilters filter, String pageNo){
		List<RTFPromoOffers> list = null;	
		Integer startFrom = 0;
		try {
			String sql = "select rtf.id as id,rtf.promo_code as promoCode,rtf.discount as discountPer,rtf.start_date as startDate," +
					"rtf.end_date as endDate,rtf.max_orders as maxOrders,rtf.no_of_orders as noOfOrders," +
					"rtf.status as status,rtf.created_by as createdBy,rtf.created_date as createdDate," +
					"rtf.modified_by as modifiedBy,rtf.modified_time as modifiedDate,rtf.is_flat_discount as isFlatDiscount,"+
					"rtf.flat_offer_order_threshold as flatOfferOrderThreshold, rtf.mobile_discount as mobileDiscountPerc ";
			
			if(id != null){
				sql += ", rtfpt.promo_type as promoType, rtfpt.artist_id as artistId, rtfpt.venue_id as venueId, rtfpt.parent_id as parentId, rtfpt.child_id as childId, "+
					"rtfpt.grand_child_id as grandChildId, a.name as artistName, v.building as venueName, pc.name as parentCategory, cc.name as childCategory, gcc.name as grandChildCategory, "+
					"rtfpt.event_id as eventId, e.name as eventName ";
			}
		
			sql += " from rtf_promotional_offer_hdr rtf with(nolock) ";
			
			if(id != null){
				sql += "left join rtf_promotional_offer_dtl rtfpt with(nolock) on rtfpt.promo_offer_id = rtf.id " +
					"left join artist a with(nolock) on a.id=rtfpt.artist_id " +
					"left join venue v with(nolock) on v.id=rtfpt.venue_id " +
					"left join parent_category pc with(nolock) on pc.id=rtfpt.parent_id " +
					"left join child_category cc with(nolock) on cc.id=rtfpt.child_id " +
					"left join grand_child_category gcc with(nolock) on gcc.id=rtfpt.grand_child_id " +
					"left join event e with(nolock) on e.id = rtfpt.event_id ";
			}
			sql += " WHERE 1=1 ";
			
			if(id != null){
				sql += " AND rtf.id = "+id;
			}
			if(filter.getPromoCode()!=null && !filter.getPromoCode().isEmpty()){
				sql += " AND rtf.promo_code like '"+filter.getPromoCode()+"' ";
			}
			if(filter.getDiscountCouponPrice()!=null){
				sql += " AND rtf.discount = "+filter.getDiscountCouponPrice();
			}
			if(filter.getMobileDiscount() != null){
				sql += " AND rtf.mobile_discount = "+filter.getMobileDiscount(); 
			}
			if(filter.getStartDate()!=null ){
				if(Util.extractDateElement(filter.getStartDate(),"DAY") > 0){
					sql += " AND DATEPART(day,rtf.start_date) = "+Util.extractDateElement(filter.getStartDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getStartDate(),"MONTH") > 0){
					sql += " AND DATEPART(month,rtf.start_date) = "+Util.extractDateElement(filter.getStartDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getStartDate(),"YEAR") > 0){
					sql += " AND DATEPART(year,rtf.start_date) = "+Util.extractDateElement(filter.getStartDate(),"YEAR")+" ";
				}
				/*if(Util.extractDateElement(filter.getStartDate(),"HOUR") > 0){
					sql += " AND DATEPART(hour,rtf.start_date) = "+Util.extractDateElement(filter.getStartDate(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getStartDate(),"MINUTE") > 0){
					sql += " AND DATEPART(minute,rtf.start_date) = "+Util.extractDateElement(filter.getStartDate(),"MINUTE")+" ";
				}*/
			}
			if(filter.getEndDate()!=null ){
				if(Util.extractDateElement(filter.getEndDate(),"DAY") > 0){
					sql += " AND DATEPART(day,rtf.end_date) = "+Util.extractDateElement(filter.getEndDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEndDate(),"MONTH") > 0){
					sql += " AND DATEPART(month,rtf.end_date) = "+Util.extractDateElement(filter.getEndDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEndDate(),"YEAR") > 0){
					sql += " AND DATEPART(year,rtf.end_date) = "+Util.extractDateElement(filter.getEndDate(),"YEAR")+" ";
				}
				/*if(Util.extractDateElement(filter.getEndDate(),"HOUR") > 0){
					sql += " AND DATEPART(hour,rtf.end_date) = "+Util.extractDateElement(filter.getEndDate(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEndDate(),"MINUTE") > 0){
					sql += " AND DATEPART(minute,rtf.end_date) = "+Util.extractDateElement(filter.getEndDate(),"MINUTE")+" ";
				}*/
			}
			
			if(filter.getMaxOrders()!=null){
				sql += " AND rtf.max_orders = "+filter.getMaxOrders();
			}
			if(filter.getNoOfOrders()!=null){
				sql += " AND rtf.no_of_orders = "+filter.getNoOfOrders();
			}
			
			/*if(filter.getPromoType()!=null && !filter.getPromoType().isEmpty()){
				sql += " AND rtfpt.promo_type like '"+filter.getPromoType()+"' ";
			}
			if(filter.getArtistName()!=null && !filter.getArtistName().isEmpty()){
				sql += " AND a.name like '%"+filter.getArtistName()+"%' ";
			}
			if(filter.getVenueName()!=null && !filter.getVenueName().isEmpty()){
				sql += " AND v.building like '%"+filter.getVenueName()+"%' ";
			}
			if(filter.getParentCategoryName()!=null && !filter.getParentCategoryName().isEmpty()){
				sql += " AND pc.name like '%"+filter.getParentCategoryName()+"%' ";
			}
			if(filter.getChildCategoryName()!=null && !filter.getChildCategoryName().isEmpty()){
				sql += " AND cc.name like '"+filter.getChildCategoryName()+"' ";
			}
			if(filter.getGrandChildCategoryName()!=null && !filter.getGrandChildCategoryName().isEmpty()){
				sql += " AND gcc.name like '"+filter.getGrandChildCategoryName()+"' ";
			}*/
			if(filter.getStatus()!=null && !filter.getStatus().isEmpty()){
				sql += " AND rtf.status like '"+filter.getStatus()+"' ";
			}
			if(filter.getCreatedBy()!=null && !filter.getCreatedBy().isEmpty()){
				sql += " AND rtf.created_by like '"+filter.getCreatedBy()+"' ";
			}
			if(filter.getLastUpdatedBy()!=null && !filter.getLastUpdatedBy().isEmpty()){
				sql += " AND rtf.modified_by like '"+filter.getLastUpdatedBy()+"' ";
			}
			
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,rtf.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,rtf.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,rtf.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
				/*if(Util.extractDateElement(filter.getCreatedDateStr(),"HOUR") > 0){
					sql += " AND DATEPART(hour,rtf.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MINUTE") > 0){
					sql += " AND DATEPART(minute,rtf.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MINUTE")+" ";
				}*/
			}
			if(filter.getLastUpdatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,rtf.modified_time) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,rtf.modified_time) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,rtf.modified_time) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
				}
				/*if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"HOUR") > 0){
					sql += " AND DATEPART(hour,rtf.modified_time) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MINUTE") > 0){
					sql += " AND DATEPART(minute,rtf.modified_time) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MINUTE")+" ";
				}*/
			}
			String sortingSql = GridSortingUtil.getDiscountCodesSortingSql(filter);
			if(sortingSql!=null && !sortingSql.isEmpty()){
				sql += sortingSql;
			}
			
			staticSession = QueryManagerDAO.getStaticSession();
			SQLQuery query = staticSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("promoCode", Hibernate.STRING);
			query.addScalar("discountPer", Hibernate.DOUBLE);
			query.addScalar("startDate", Hibernate.TIMESTAMP);
			query.addScalar("endDate", Hibernate.TIMESTAMP);
			query.addScalar("maxOrders", Hibernate.INTEGER);
			query.addScalar("noOfOrders", Hibernate.INTEGER);			
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("modifiedBy", Hibernate.STRING);
			query.addScalar("modifiedDate", Hibernate.TIMESTAMP);			
			query.addScalar("isFlatDiscount", Hibernate.BOOLEAN);
			query.addScalar("flatOfferOrderThreshold", Hibernate.DOUBLE);
			query.addScalar("mobileDiscountPerc", Hibernate.DOUBLE);
		
			if(id != null){
				query.addScalar("promoType", Hibernate.STRING);
				query.addScalar("artistId", Hibernate.INTEGER);
				query.addScalar("venueId", Hibernate.INTEGER);
				query.addScalar("grandChildId", Hibernate.INTEGER);
				query.addScalar("childId", Hibernate.INTEGER);
				query.addScalar("parentId", Hibernate.INTEGER);
				query.addScalar("artistName", Hibernate.STRING);
				query.addScalar("venueName", Hibernate.STRING);
				query.addScalar("parentCategory", Hibernate.STRING);
				query.addScalar("childCategory", Hibernate.STRING);
				query.addScalar("grandChildCategory", Hibernate.STRING);
				query.addScalar("eventId", Hibernate.INTEGER);
				query.addScalar("eventName", Hibernate.STRING);
			}
			
			startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
			list = query.setResultTransformer(Transformers.aliasToBean(RTFPromoOffers.class)).setFirstResult(startFrom)
					.setMaxResults(PaginationUtil.PAGESIZE).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public Integer getDiscountCodesCount(Integer id, GridHeaderFilters filter){
		Integer count = 0;		
		try {
			String sql = "select count(*)  ";
		
			sql += " from rtf_promotional_offer_hdr rtf with(nolock) ";
			
			if(id != null){
				sql += "left join rtf_promotional_offer_dtl rtfpt with(nolock) on rtfpt.promo_offer_id = rtf.id " +
					"left join artist a with(nolock) on a.id=rtfpt.artist_id " +
					"left join venue v with(nolock) on v.id=rtfpt.venue_id " +
					"left join parent_category pc with(nolock) on pc.id=rtfpt.parent_id " +
					"left join child_category cc with(nolock) on cc.id=rtfpt.child_id " +
					"left join grand_child_category gcc with(nolock) on gcc.id=rtfpt.grand_child_id " +
					"left join event e with(nolock) on e.id = rtfpt.event_id ";
			}
			sql += " WHERE 1=1 ";
			
			if(id != null){
				sql += " AND rtf.id = "+id;
			}
			if(filter.getPromoCode()!=null && !filter.getPromoCode().isEmpty()){
				sql += " AND rtf.promo_code like '"+filter.getPromoCode()+"' ";
			}
			if(filter.getDiscountCouponPrice()!=null){
				sql += " AND rtf.discount = "+filter.getDiscountCouponPrice();
			}
			if(filter.getMobileDiscount() != null){
				sql += " AND rtf.mobile_discount = "+filter.getMobileDiscount(); 
			}
			if(filter.getStartDate()!=null ){
				if(Util.extractDateElement(filter.getStartDate(),"DAY") > 0){
					sql += " AND DATEPART(day,rtf.start_date) = "+Util.extractDateElement(filter.getStartDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getStartDate(),"MONTH") > 0){
					sql += " AND DATEPART(month,rtf.start_date) = "+Util.extractDateElement(filter.getStartDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getStartDate(),"YEAR") > 0){
					sql += " AND DATEPART(year,rtf.start_date) = "+Util.extractDateElement(filter.getStartDate(),"YEAR")+" ";
				}
			}
			if(filter.getEndDate()!=null ){
				if(Util.extractDateElement(filter.getEndDate(),"DAY") > 0){
					sql += " AND DATEPART(day,rtf.end_date) = "+Util.extractDateElement(filter.getEndDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEndDate(),"MONTH") > 0){
					sql += " AND DATEPART(month,rtf.end_date) = "+Util.extractDateElement(filter.getEndDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEndDate(),"YEAR") > 0){
					sql += " AND DATEPART(year,rtf.end_date) = "+Util.extractDateElement(filter.getEndDate(),"YEAR")+" ";
				}
			}
			
			if(filter.getMaxOrders()!=null){
				sql += " AND rtf.max_orders = "+filter.getMaxOrders();
			}
			if(filter.getNoOfOrders()!=null){
				sql += " AND rtf.no_of_orders = "+filter.getNoOfOrders();
			}
			
			
			if(filter.getStatus()!=null && !filter.getStatus().isEmpty()){
				sql += " AND rtf.status like '"+filter.getStatus()+"' ";
			}
			if(filter.getCreatedBy()!=null && !filter.getCreatedBy().isEmpty()){
				sql += " AND rtf.created_by like '"+filter.getCreatedBy()+"' ";
			}
			if(filter.getLastUpdatedBy()!=null && !filter.getLastUpdatedBy().isEmpty()){
				sql += " AND rtf.modified_by like '"+filter.getLastUpdatedBy()+"' ";
			}
			
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,rtf.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,rtf.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,rtf.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getLastUpdatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,rtf.modified_time) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,rtf.modified_time) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,rtf.modified_time) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
				}
			}
			
			staticSession = QueryManagerDAO.getStaticSession();
			SQLQuery query = staticSession.createSQLQuery(sql);
			count = Integer.parseInt(String.valueOf(query.uniqueResult()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}
	
	
	public List<CategoryTicketGroup> getLockedTicketDetails(Integer eventId, GridHeaderFilters filter){
		List<CategoryTicketGroup> lockedTicketDetails = null;
		Properties paramProductType = new Properties();
		paramProductType.put("enumClass", ProductType.class.getCanonicalName());
		paramProductType.put("type", "12"); /*type 12 instructs to use the String representation of enum value*/
		
		try{
			String queryStr = "select au.category_ticket_group_id as catgeoryTicketGroupId, au.ip_address as ipAddress, "+
				" au.platform as platform, au.creation_date as creationDate, au.lock_status as lockStatus, "+
				"c.section as section, c.row as row, c.quantity as quantity, "+
				"c.price as price, c.tax_amount as taxAmount, c.section_range as sectionRange, c.row_range as rowRange, "+
				"c.seat_low as seatLow, c.seat_high as seatHigh, c.external_notes as externalNotes, c.internal_notes as internalNotes, "+
				"c.near_term_display_option as nearTermDisplayOption, c.marketplace_notes as marketPlaceNotes, c.broadcast as broadcast, "+
				"c.product_type as producttype, c.shipping_method_id as shippingMethodId, c.max_showing as maxShowing, sm.name as shippingMethod "+
				" from autocats_locked_tickets au with(nolock) left join category_ticket_group c with(nolock) on c.id = au.category_ticket_group_id "+
				"left join shipping_method sm with(nolock) on c.shipping_method_id = sm.id "+
				" left join event ev with(nolock) on ev.id = au.eventId where 1 = 1 ";
			
			if(eventId != null && eventId > 0){
				queryStr += " AND ev.id = "+eventId;
			}
			if(filter.getSection() != null){				
				queryStr += " AND c.section like '%"+filter.getSection()+"%' ";
			}
			if(filter.getRow() != null){
				queryStr += " AND c.row like '%"+filter.getRow()+"%' ";
			}
			if(filter.getSeatLow() != null){
				queryStr += " AND c.seat_low like '"+filter.getSeatLow()+"' ";
			}
			if(filter.getSeatHigh() != null){
				queryStr += " AND c.seat_high like '"+filter.getSeatHigh()+"' ";
			}
			if(filter.getQuantity() != null){
				queryStr += " AND c.quantity = "+filter.getQuantity()+" ";
			}
			if(filter.getRetailPrice() != null){
				queryStr += " AND c.price = "+filter.getRetailPrice()+" ";
			}
			if(filter.getWholeSalePrice() != null){
				queryStr += " AND c.price = "+filter.getWholeSalePrice()+" ";
			}
			if(filter.getPrice() != null){
				queryStr += " AND c.price = "+filter.getPrice()+" ";
			}
			if(filter.getTaxAmount() != null){
				queryStr += " AND c.tax_amount = "+filter.getTaxAmount()+" ";
			}
			if(filter.getShippingMethod() != null){
				queryStr += " AND sm.name like '%"+filter.getShippingMethod()+"%' ";
			}
			if(filter.getNearTermDisplayOption() != null){
				queryStr += " AND c.near_term_display_option like '%"+filter.getNearTermDisplayOption()+"%' ";
			}
			if(filter.getSectionRange() != null){
				queryStr += " AND c.section_range like '%"+filter.getSectionRange()+"%' ";
			}
			if(filter.getRowRange() != null){
				queryStr += " AND c.row_range like '%"+filter.getRowRange()+"%' ";
			}
			if(filter.getExternalNotes() != null){
				queryStr += " AND c.external_notes like '%"+filter.getExternalNotes()+"%' ";
			}
			if(filter.getMarketPlaceNotes() != null){
				queryStr += " AND c.marketplace_notes like '%"+filter.getMarketPlaceNotes()+"%' ";
			}
			if(filter.getProductType() != null){
				queryStr += " AND c.product_type like '%"+filter.getProductType()+"%' "; 
			}
			if(filter.getCategoryTicketGroupId() != null && filter.getCategoryTicketGroupId() > 0){
				queryStr += " AND au.category_ticket_group_id = "+filter.getCategoryTicketGroupId()+" ";
			}
			if(filter.getCustomerIpAddress() != null){
				queryStr += " AND au.ip_address like '"+filter.getCustomerIpAddress()+"' ";
			}
			if(filter.getPlatform() != null){
				queryStr += " AND au.platform like '"+filter.getPlatform()+"' ";
			}
			if(filter.getCreatedDateStr() != null){
				String dateTimeStr = filter.getCreatedDateStr();
				if(dateTimeStr.contains(" ")){
					String dateTimeArr[] = dateTimeStr.split(" ");
					if(Util.extractDateElement(dateTimeArr[0],"DAY") > 0){
						queryStr += " AND DATEPART(day,au.creation_date) = "+Util.extractDateElement(dateTimeArr[0],"DAY")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"MONTH") > 0){
						queryStr += " AND DATEPART(month,au.creation_date) = "+Util.extractDateElement(dateTimeArr[0],"MONTH")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"YEAR") > 0){
						queryStr += " AND DATEPART(year,au.creation_date) = "+Util.extractDateElement(dateTimeArr[0],"YEAR")+" ";
					}
					if(dateTimeArr.length>1){
						String dateTime = dateTimeArr[1];
						if(dateTimeArr.length > 2){
							if(dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()){
								dateTime += " "+dateTimeArr[2];
							}
						}
						if(Util.extractDateElement(dateTime,"HOUR") > 0){
							queryStr += " AND DATEPART(hour,au.creation_date) = "+Util.extractDateElement(dateTime,"HOUR")+" ";
						}
						if(Util.extractDateElement(dateTime,"MINUTE") > 0){
							queryStr += " AND DATEPART(minute,au.creation_date) = "+Util.extractDateElement(dateTime,"MINUTE")+" ";
						}
					}
				}else{
					if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
						queryStr += " AND DATEPART(day,au.creation_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
						queryStr += " AND DATEPART(month,au.creation_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
						queryStr += " AND DATEPART(year,au.creation_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
					}
				}
			}
			if(filter.getStatus() != null){
				queryStr += " AND au.lock_status like '"+filter.getStatus()+"' ";
			}
			
			staticSession = QueryManagerDAO.getStaticSession();
			SQLQuery query = staticSession.createSQLQuery(queryStr);
			query.addScalar("section", Hibernate.STRING);
			query.addScalar("row", Hibernate.STRING);
			query.addScalar("quantity", Hibernate.INTEGER);
			query.addScalar("price", Hibernate.DOUBLE);
			query.addScalar("taxAmount", Hibernate.DOUBLE);
			query.addScalar("sectionRange", Hibernate.STRING);
			query.addScalar("rowRange", Hibernate.STRING);
			query.addScalar("seatLow", Hibernate.STRING);
			query.addScalar("seatHigh", Hibernate.STRING);
			query.addScalar("externalNotes", Hibernate.STRING);
			query.addScalar("internalNotes", Hibernate.STRING);
			query.addScalar("nearTermDisplayOption", Hibernate.STRING);
			query.addScalar("marketPlaceNotes", Hibernate.STRING);
			query.addScalar("broadcast", Hibernate.BOOLEAN);
			query.addScalar("producttype", Hibernate.custom(EnumType.class, paramProductType));
			query.addScalar("shippingMethodId", Hibernate.INTEGER);
			query.addScalar("maxShowing", Hibernate.INTEGER);
			query.addScalar("shippingMethod", Hibernate.STRING);
			query.addScalar("catgeoryTicketGroupId", Hibernate.INTEGER);
			query.addScalar("ipAddress", Hibernate.STRING);
			query.addScalar("platform", Hibernate.STRING);
			query.addScalar("creationDate", Hibernate.TIMESTAMP);
			lockedTicketDetails = query.setResultTransformer(Transformers.aliasToBean(CategoryTicketGroup.class)).setMaxResults(1).list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return lockedTicketDetails;
	}
	

	public Integer getLockedTicketDetailsCount(Integer eventId, GridHeaderFilters filter){		
		Integer count = 0;
		try{
			String queryStr = "select count(*) as cnt "+
				" from autocats_locked_tickets au with(nolock) left join category_ticket_group c with(nolock) on c.id = au.category_ticket_group_id "+
				"left join shipping_method sm with(nolock) on c.shipping_method_id = sm.id "+
				" left join event ev with(nolock) on ev.id = au.eventId where 1 = 1 ";
			
			if(eventId != null && eventId > 0){
				queryStr += " AND ev.id = "+eventId;
			}
			if(filter.getSection() != null){				
				queryStr += " AND c.section like '%"+filter.getSection()+"%' ";
			}
			if(filter.getRow() != null){
				queryStr += " AND c.row like '%"+filter.getRow()+"%' ";
			}
			if(filter.getSeatLow() != null){
				queryStr += " AND c.seat_low like '"+filter.getSeatLow()+"' ";
			}
			if(filter.getSeatHigh() != null){
				queryStr += " AND c.seat_high like '"+filter.getSeatHigh()+"' ";
			}
			if(filter.getQuantity() != null){
				queryStr += " AND c.quantity = "+filter.getQuantity()+" ";
			}
			if(filter.getRetailPrice() != null){
				queryStr += " AND c.price = "+filter.getRetailPrice()+" ";
			}
			if(filter.getWholeSalePrice() != null){
				queryStr += " AND c.price = "+filter.getWholeSalePrice()+" ";
			}
			if(filter.getPrice() != null){
				queryStr += " AND c.price = "+filter.getPrice()+" ";
			}
			if(filter.getTaxAmount() != null){
				queryStr += " AND c.tax_amount = "+filter.getTaxAmount()+" ";
			}
			if(filter.getShippingMethod() != null){
				queryStr += " AND sm.name like '%"+filter.getShippingMethod()+"%' ";
			}
			if(filter.getNearTermDisplayOption() != null){
				queryStr += " AND c.near_term_display_option like '%"+filter.getNearTermDisplayOption()+"%' ";
			}
			if(filter.getSectionRange() != null){
				queryStr += " AND c.section_range like '%"+filter.getSectionRange()+"%' ";
			}
			if(filter.getRowRange() != null){
				queryStr += " AND c.row_range like '%"+filter.getRowRange()+"%' ";
			}
			if(filter.getExternalNotes() != null){
				queryStr += " AND c.external_notes like '%"+filter.getExternalNotes()+"%' ";
			}
			if(filter.getMarketPlaceNotes() != null){
				queryStr += " AND c.marketplace_notes like '%"+filter.getMarketPlaceNotes()+"%' ";
			}
			if(filter.getProductType() != null){
				queryStr += " AND c.product_type like '%"+filter.getProductType()+"%' "; 
			}
			if(filter.getCategoryTicketGroupId() != null && filter.getCategoryTicketGroupId() > 0){
				queryStr += " AND au.category_ticket_group_id = "+filter.getCategoryTicketGroupId()+" ";
			}
			if(filter.getCustomerIpAddress() != null){
				queryStr += " AND au.ip_address like '"+filter.getCustomerIpAddress()+"' ";
			}
			if(filter.getPlatform() != null){
				queryStr += " AND au.platform like '"+filter.getPlatform()+"' ";
			}
			if(filter.getCreatedDateStr() != null){
				String dateTimeStr = filter.getCreatedDateStr();
				if(dateTimeStr.contains(" ")){
					String dateTimeArr[] = dateTimeStr.split(" ");
					if(Util.extractDateElement(dateTimeArr[0],"DAY") > 0){
						queryStr += " AND DATEPART(day,au.creation_date) = "+Util.extractDateElement(dateTimeArr[0],"DAY")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"MONTH") > 0){
						queryStr += " AND DATEPART(month,au.creation_date) = "+Util.extractDateElement(dateTimeArr[0],"MONTH")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"YEAR") > 0){
						queryStr += " AND DATEPART(year,au.creation_date) = "+Util.extractDateElement(dateTimeArr[0],"YEAR")+" ";
					}
					if(dateTimeArr.length>1){
						String dateTime = dateTimeArr[1];
						if(dateTimeArr.length > 2){
							if(dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()){
								dateTime += " "+dateTimeArr[2];
							}
						}
						if(Util.extractDateElement(dateTime,"HOUR") > 0){
							queryStr += " AND DATEPART(hour,au.creation_date) = "+Util.extractDateElement(dateTime,"HOUR")+" ";
						}
						if(Util.extractDateElement(dateTime,"MINUTE") > 0){
							queryStr += " AND DATEPART(minute,au.creation_date) = "+Util.extractDateElement(dateTime,"MINUTE")+" ";
						}
					}
				}else{
					if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
						queryStr += " AND DATEPART(day,au.creation_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
						queryStr += " AND DATEPART(month,au.creation_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
						queryStr += " AND DATEPART(year,au.creation_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
					}
				}
			}
			if(filter.getStatus() != null){
				queryStr += " AND au.lock_status like '"+filter.getStatus()+"' ";
			}
			
			staticSession = QueryManagerDAO.getStaticSession();
			SQLQuery query = staticSession.createSQLQuery(queryStr);
			count = Integer.parseInt(String.valueOf(query.uniqueResult()));
		}catch(Exception e){
			e.printStackTrace();
		}
		return count;
	}
	
	
	public List<TrackerBrokers> getAllActiveBrokers(){
		List<TrackerBrokers> brokers = null;
		try {
			String sql = "select b.id AS id,b.company_name AS companyName from tracker_brokers b with(nolock) " +
					"left join tracker_users t with(nolock) on t.broker_id=b.id where t.status=1";
			staticSession = getStaticSession();
			SQLQuery query = staticSession.createSQLQuery(sql);
			query.addScalar("id",Hibernate.INTEGER);
			query.addScalar("companyName",Hibernate.STRING);
			brokers = query.setResultTransformer(Transformers.aliasToBean(TrackerBrokers.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return brokers;
	}
	
	public List<Object[]> getRewardPointSummary(){
		List<Object[]> list = null;
		try {
			String sql = "select sum(cr.pending_rewards) as PendingPoints,"+
				"sum(cr.active_reward_points)  as ActivePoints from cust_loyalty_reward_info cr with(nolock) "+
				"inner join customer c with(nolock) on cr.cust_id=c.id "+
				"where c.is_test_account <> 1 AND c.is_bounce<> 1 and c.is_unsub <> 1";
			staticSession = getStaticSession();
			SQLQuery query = staticSession.createSQLQuery(sql);
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public List<PopularArtist> getAllArtistToExport(GridHeaderFilters filter){
		List<Artist> artistList = new ArrayList<Artist>();
		Integer startFrom = 0;
		Session session = null;
		try{
			session = getSessionFactory().openSession();
			
			String queryStr = "select distinct ar.id as artistId,REPLACE(ar.name,'\"','') as artistName,gc.name as grandChildCategoryName,"+
				"cc.name as childCategoryName,pc.name as parentType, "+
				" ai.image_url as imageFileUrl "+
				"FROM artist ar with(nolock) "+
				"inner join artist_category_mapping ac with(nolock) on ac.artist_id=ar.id "+
				"inner join grand_child_category gc with(nolock) on gc.id=ac.grand_child_category_id "+
				"inner join child_category cc with(nolock) on cc.id=ac.child_category_id "+
				"inner join parent_category pc with(nolock) on pc.id=ac.parent_category_id "+
				"left join artist_image ai with(nolock) on ar.id = ai.artist_id "+
				" where ar.display_on_search = 1 ";
			
			if(filter.getArtistName() != null){
				queryStr += " AND REPLACE(ar.name,'\"','') like '%"+filter.getArtistName()+"%' ";
			}
			if(filter.getGrandChildCategoryName() != null){
				queryStr += " AND gc.name like '%"+filter.getGrandChildCategoryName()+"%' ";
			}
			if(filter.getChildCategoryName() != null){
				queryStr += " AND cc.name like '%"+filter.getChildCategoryName()+"%' ";
			}
			if(filter.getParentCategoryName() != null){
				queryStr += " AND pc.name like '%"+filter.getParentCategoryName()+"%' ";
			}
			
			session  = sessionFactory.openSession();
			Query query = session.createSQLQuery(queryStr);
			List<PopularArtist> list = query.setResultTransformer(Transformers.aliasToBean(PopularArtist.class)).list();
			return list;
		}catch(Exception e){
			System.out.println("The Exception is: "+ e);
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}	
	}
	
	public List<PopularArtist> getAllActivePopularArtistByProductIdToExport(Integer productId,GridHeaderFilters filter){
		Session session =null;
		Integer startFrom = 0;
		try {
			String queryStr = "select distinct a.id as id,a.artist_id as artistId,REPLACE(ar.name,'\"','') as artistName,gc.name as grandChildCategoryName,"+
				"cc.name as childCategoryName,pc.name as parentType,a.created_date AS createdDate,a.created_by AS createdBy "+
				"FROM popular_artist a with(nolock) "+
				"inner join artist ar with(nolock) on a.artist_id=ar.id "+
				"inner join artist_category_mapping ac with(nolock) on ac.artist_id=a.id "+
				"inner join grand_child_category gc with(nolock) on gc.id=ac.grand_child_category_id "+
				"inner join child_category cc with(nolock) on cc.id=ac.child_category_id "+
				"inner join parent_category pc with(nolock) on pc.id=ac.parent_category_id WHERE a.product_id="+productId;
			
			if(filter.getArtistName() != null){
				queryStr += " AND REPLACE(ar.name,'\"','') like '%"+filter.getArtistName()+"%' ";
			}
			if(filter.getGrandChildCategoryName() != null){
				queryStr += " AND gc.name like '%"+filter.getGrandChildCategoryName()+"%' ";
			}
			if(filter.getChildCategoryName() != null){
				queryStr += " AND cc.name like '%"+filter.getChildCategoryName()+"%' ";
			}
			if(filter.getParentCategoryName() != null){
				queryStr += " AND pc.name like '%"+filter.getParentCategoryName()+"%' ";
			}
			if(filter.getCreatedBy() != null){
				queryStr += " AND a.created_by like '%"+filter.getCreatedBy()+"%' ";
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					queryStr += " AND DATEPART(day,a.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					queryStr += " AND DATEPART(month,a.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					queryStr += " AND DATEPART(year,a.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			session  = sessionFactory.openSession();
			Query query = session.createSQLQuery(queryStr);
			List<PopularArtist> list = query.setResultTransformer(Transformers.aliasToBean(PopularArtist.class)).list();
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	public List<FantasyChildCategory> getChildCategoryDetailsToExport(GridHeaderFilters filter){
		Integer startFrom = 0;
		Session session = null;
		try{
			session = getSessionFactory().openSession();
			
			String queryStr = "select cc.id as id, cc.name as name, pc.name as parentCategoryName, "+
				"cc.last_updated as lastUpdated, cc.last_updated_by as lastUpdatedBy "+
				"FROM fantasy_child_category cc with(nolock) "+
				"left join fantasy_parent_category pc with(nolock) on cc.parent_id = pc.id "+
				" where 1 = 1";
			
			if(filter.getChildCategoryId() != null){
				queryStr += " AND cc.id = "+filter.getChildCategoryId()+" ";
			}
			if(filter.getChildCategoryName() != null){
				queryStr += " AND cc.name like '%"+filter.getChildCategoryName()+"%' ";
			}
			if(filter.getParentCategoryName() != null){
				queryStr += " AND pc.name like '%"+filter.getParentCategoryName()+"%' ";
			}
			if(filter.getLastUpdatedBy() != null){
				queryStr += " AND cc.last_updated_by like '%"+filter.getLastUpdatedBy()+"%' ";
			}
			if(filter.getLastUpdatedDateStr() != null){
				String dateTimeStr = filter.getLastUpdatedDateStr();
				if(dateTimeStr.contains(" ")){
					String dateTimeArr[] = dateTimeStr.split(" ");
					if(Util.extractDateElement(dateTimeArr[0],"DAY") > 0){
						queryStr += " AND DATEPART(day,cc.last_updated) = "+Util.extractDateElement(dateTimeArr[0],"DAY")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"MONTH") > 0){
						queryStr += " AND DATEPART(month,cc.last_updated) = "+Util.extractDateElement(dateTimeArr[0],"MONTH")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"YEAR") > 0){
						queryStr += " AND DATEPART(year,cc.last_updated) = "+Util.extractDateElement(dateTimeArr[0],"YEAR")+" ";
					}
					if(dateTimeArr.length>1){
						String dateTime = dateTimeArr[1];
						if(dateTimeArr.length > 2){
							if(dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()){
								dateTime += " "+dateTimeArr[2];
							}
						}
						if(Util.extractDateElement(dateTime,"HOUR") > 0){
							queryStr += " AND DATEPART(hour,cc.last_updated) = "+Util.extractDateElement(dateTime,"HOUR")+" ";
						}
						if(Util.extractDateElement(dateTime,"MINUTE") > 0){
							queryStr += " AND DATEPART(minute,cc.last_updated) = "+Util.extractDateElement(dateTime,"MINUTE")+" ";
						}
					}
				}else{
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
						queryStr += " AND DATEPART(day,cc.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
						queryStr += " AND DATEPART(month,cc.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
						queryStr += " AND DATEPART(year,cc.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
					}
				}
			}
			session  = sessionFactory.openSession();
			SQLQuery query = session.createSQLQuery(queryStr);
			List<FantasyChildCategory> list = query.setResultTransformer(Transformers.aliasToBean(FantasyChildCategory.class)).list();
			return list;
		}catch(Exception e){
			System.out.println("The Exception is: "+ e);
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	public List<FantasyGrandChildCategory> getGrandChildCategoryDetailsToExport(GridHeaderFilters filter){
		Integer startFrom = 0;
		Session session = null;
		try{
			session = getSessionFactory().openSession();
			
			String queryStr = "select gc.id as id, gc.name as name, pc.name as parentCategoryName, cc.name as childCategoryName, "+
				"gc.package_cost as packageCost, gc.last_updated as lastUpdated, gc.last_updated_by as lastUpdatedBy "+
				"FROM fantasy_grandchild_category gc "+
				"left join fantasy_child_category cc on gc.child_id = cc.id "+
				"left join fantasy_parent_category pc on cc.parent_id = pc.id "+
				" where 1 = 1";
			
			if(filter.getGrandChildCategoryId() != null){
				queryStr += " AND gc.id = "+filter.getGrandChildCategoryId()+" ";
			}
			if(filter.getGrandChildCategoryName() != null){
				queryStr += " AND gc.name like '%"+filter.getGrandChildCategoryName()+"%' ";
			}
			if(filter.getChildCategoryName() != null){
				queryStr += " AND cc.name like '%"+filter.getChildCategoryName()+"%' ";
			}
			if(filter.getParentCategoryName() != null){
				queryStr += " AND pc.name like '%"+filter.getParentCategoryName()+"%' ";
			}
			if(filter.getPackageCost() != null){
				queryStr += " AND gc.package_cost = "+filter.getPackageCost()+" ";
			}
			if(filter.getLastUpdatedBy() != null){
				queryStr += " AND gc.last_updated_by like '%"+filter.getLastUpdatedBy()+"%' ";
			}
			if(filter.getLastUpdatedDateStr() != null){				
				String dateTimeStr = filter.getLastUpdatedDateStr();
				if(dateTimeStr.contains(" ")){
					String dateTimeArr[] = dateTimeStr.split(" ");
					if(Util.extractDateElement(dateTimeArr[0],"DAY") > 0){
						queryStr += " AND DATEPART(day,gc.last_updated) = "+Util.extractDateElement(dateTimeArr[0],"DAY")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"MONTH") > 0){
						queryStr += " AND DATEPART(month,gc.last_updated) = "+Util.extractDateElement(dateTimeArr[0],"MONTH")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"YEAR") > 0){
						queryStr += " AND DATEPART(year,gc.last_updated) = "+Util.extractDateElement(dateTimeArr[0],"YEAR")+" ";
					}
					if(dateTimeArr.length>1){
						String dateTime = dateTimeArr[1];
						if(dateTimeArr.length > 2){
							if(dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()){
								dateTime += " "+dateTimeArr[2];
							}
						}
						if(Util.extractDateElement(dateTime,"HOUR") > 0){
							queryStr += " AND DATEPART(hour,gc.last_updated) = "+Util.extractDateElement(dateTime,"HOUR")+" ";
						}
						if(Util.extractDateElement(dateTime,"MINUTE") > 0){
							queryStr += " AND DATEPART(minute,gc.last_updated) = "+Util.extractDateElement(dateTime,"MINUTE")+" ";
						}
					}
				}else{
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
						queryStr += " AND DATEPART(day,gc.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
						queryStr += " AND DATEPART(month,gc.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
						queryStr += " AND DATEPART(year,gc.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
					}
				}
			}
			session  = sessionFactory.openSession();
			SQLQuery query = session.createSQLQuery(queryStr);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("name", Hibernate.STRING);
			query.addScalar("parentCategoryName", Hibernate.STRING);
			query.addScalar("childCategoryName", Hibernate.STRING);
			query.addScalar("packageCost", Hibernate.DOUBLE);
			query.addScalar("lastUpdated", Hibernate.TIMESTAMP);
			query.addScalar("lastUpdatedBy", Hibernate.STRING);
			
			List<FantasyGrandChildCategory> list = query.setResultTransformer(Transformers.aliasToBean(FantasyGrandChildCategory.class)).list();
			return list;
		}catch(Exception e){
			System.out.println("The Exception is: "+ e);
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	

	public List<RTFPromoOffers> getDiscountCodesToExport(GridHeaderFilters filter){
		List<RTFPromoOffers> list = null;		
		try {
			String sql = "select rtf.id as id,rtf.promo_code as promoCode,rtf.discount as discountPer,rtf.start_date as startDate," +
			"rtf.end_date as endDate,rtf.max_orders as maxOrders,rtf.no_of_orders as noOfOrders," +
			"rtf.status as status,rtf.created_by as createdBy,rtf.created_date as createdDate," +
			"rtf.modified_by as modifiedBy,rtf.modified_time as modifiedDate,rtf.is_flat_discount as isFlatDiscount,"+
			"rtf.flat_offer_order_threshold as flatOfferOrderThreshold, rtf.mobile_discount as mobileDiscountPerc "+		
			"from rtf_promotional_offer_hdr rtf with(nolock) WHERE 1=1 ";
	
			/*if(id != null){
				sql += " AND rtf.id = "+id;
			}*/
			if(filter.getPromoCode()!=null && !filter.getPromoCode().isEmpty()){
				sql += " AND rtf.promo_code like '"+filter.getPromoCode()+"' ";
			}
			if(filter.getDiscountCouponPrice()!=null){
				sql += " AND rtf.discount = "+filter.getDiscountCouponPrice();
			}
			if(filter.getMobileDiscount() != null){
				sql += " AND rtf.mobile_discount = "+filter.getMobileDiscount();
			}
			if(filter.getStartDate()!=null ){
				if(Util.extractDateElement(filter.getStartDate(),"DAY") > 0){
					sql += " AND DATEPART(day,rtf.start_date) = "+Util.extractDateElement(filter.getStartDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getStartDate(),"MONTH") > 0){
					sql += " AND DATEPART(month,rtf.start_date) = "+Util.extractDateElement(filter.getStartDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getStartDate(),"YEAR") > 0){
					sql += " AND DATEPART(year,rtf.start_date) = "+Util.extractDateElement(filter.getStartDate(),"YEAR")+" ";
				}
				/*if(Util.extractDateElement(filter.getStartDate(),"HOUR") > 0){
					sql += " AND DATEPART(hour,rtf.start_date) = "+Util.extractDateElement(filter.getStartDate(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getStartDate(),"MINUTE") > 0){
					sql += " AND DATEPART(minute,rtf.start_date) = "+Util.extractDateElement(filter.getStartDate(),"MINUTE")+" ";
				}*/
			}
			if(filter.getEndDate()!=null ){
				if(Util.extractDateElement(filter.getEndDate(),"DAY") > 0){
					sql += " AND DATEPART(day,rtf.end_date) = "+Util.extractDateElement(filter.getEndDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEndDate(),"MONTH") > 0){
					sql += " AND DATEPART(month,rtf.end_date) = "+Util.extractDateElement(filter.getEndDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEndDate(),"YEAR") > 0){
					sql += " AND DATEPART(year,rtf.end_date) = "+Util.extractDateElement(filter.getEndDate(),"YEAR")+" ";
				}
				/*if(Util.extractDateElement(filter.getEndDate(),"HOUR") > 0){
					sql += " AND DATEPART(hour,rtf.end_date) = "+Util.extractDateElement(filter.getEndDate(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEndDate(),"MINUTE") > 0){
					sql += " AND DATEPART(minute,rtf.end_date) = "+Util.extractDateElement(filter.getEndDate(),"MINUTE")+" ";
				}*/
			}
			
			if(filter.getMaxOrders()!=null){
				sql += " AND rtf.max_orders = "+filter.getMaxOrders();
			}
			if(filter.getNoOfOrders()!=null){
				sql += " AND rtf.no_of_orders = "+filter.getNoOfOrders();
			}
			
			/*if(filter.getPromoType()!=null && !filter.getPromoType().isEmpty()){
				sql += " AND rtfpt.promo_type like '"+filter.getPromoType()+"' ";
			}
			if(filter.getArtistName()!=null && !filter.getArtistName().isEmpty()){
				sql += " AND a.name like '%"+filter.getArtistName()+"%' ";
			}
			if(filter.getVenueName()!=null && !filter.getVenueName().isEmpty()){
				sql += " AND v.building like '%"+filter.getVenueName()+"%' ";
			}
			if(filter.getParentCategoryName()!=null && !filter.getParentCategoryName().isEmpty()){
				sql += " AND pc.name like '%"+filter.getParentCategoryName()+"%' ";
			}
			if(filter.getChildCategoryName()!=null && !filter.getChildCategoryName().isEmpty()){
				sql += " AND cc.name like '"+filter.getChildCategoryName()+"' ";
			}
			if(filter.getGrandChildCategoryName()!=null && !filter.getGrandChildCategoryName().isEmpty()){
				sql += " AND gcc.name like '"+filter.getGrandChildCategoryName()+"' ";
			}*/
			if(filter.getStatus()!=null && !filter.getStatus().isEmpty()){
				sql += " AND rtf.status like '"+filter.getStatus()+"' ";
			}
			if(filter.getCreatedBy()!=null && !filter.getCreatedBy().isEmpty()){
				sql += " AND rtf.created_by like '"+filter.getCreatedBy()+"' ";
			}
			if(filter.getLastUpdatedBy()!=null && !filter.getLastUpdatedBy().isEmpty()){
				sql += " AND rtf.modified_by like '"+filter.getLastUpdatedBy()+"' ";
			}
			
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,rtf.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,rtf.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,rtf.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
				/*if(Util.extractDateElement(filter.getCreatedDateStr(),"HOUR") > 0){
					sql += " AND DATEPART(hour,rtf.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MINUTE") > 0){
					sql += " AND DATEPART(minute,rtf.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MINUTE")+" ";
				}*/
			}
			if(filter.getLastUpdatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,rtf.modified_time) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,rtf.modified_time) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,rtf.modified_time) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
				}
				/*if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"HOUR") > 0){
					sql += " AND DATEPART(hour,rtf.modified_time) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MINUTE") > 0){
					sql += " AND DATEPART(minute,rtf.modified_time) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MINUTE")+" ";
				}*/
			}
			
			staticSession = QueryManagerDAO.getStaticSession();
			SQLQuery query = staticSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("promoCode", Hibernate.STRING);
			query.addScalar("discountPer", Hibernate.DOUBLE);
			query.addScalar("startDate", Hibernate.TIMESTAMP);
			query.addScalar("endDate", Hibernate.TIMESTAMP);
			query.addScalar("maxOrders", Hibernate.INTEGER);
			query.addScalar("noOfOrders", Hibernate.INTEGER);
			/*query.addScalar("promoType", Hibernate.STRING);
			query.addScalar("artistId", Hibernate.INTEGER);
			query.addScalar("venueId", Hibernate.INTEGER);
			query.addScalar("grandChildId", Hibernate.INTEGER);
			query.addScalar("childId", Hibernate.INTEGER);
			query.addScalar("parentId", Hibernate.INTEGER);*/
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("modifiedBy", Hibernate.STRING);
			query.addScalar("modifiedDate", Hibernate.TIMESTAMP);
			/*query.addScalar("artistName", Hibernate.STRING);
			query.addScalar("venueName", Hibernate.STRING);
			query.addScalar("parentCategory", Hibernate.STRING);
			query.addScalar("childCategory", Hibernate.STRING);
			query.addScalar("grandChildCategory", Hibernate.STRING);*/
			query.addScalar("isFlatDiscount", Hibernate.BOOLEAN);
			query.addScalar("flatOfferOrderThreshold", Hibernate.DOUBLE);
			query.addScalar("mobileDiscountPerc", Hibernate.DOUBLE);
			list = query.setResultTransformer(Transformers.aliasToBean(RTFPromoOffers.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public Collection<WebServiceTracking> getWebsiteTrackingToExport(String fromDate, String toDate,GridHeaderFilters filter){
		Collection<WebServiceTracking> webServiceTracking = null;
		Session session = null;
		try	{					
			String sqlQuery = "select id as id, customer_ip_address as customerIpAddress,platform as platform,"+
						"api_name as apiName,hit_date as hitDate,contest_id as contestId,customer_id as custId,"+
						"action_result as description, app_ver as appVersion "+
						"from web_service_tracking_new with(nolock) where platform <> 'TICK_TRACKER' ";
			
			if(fromDate != null || toDate != null){
				sqlQuery += " AND hit_date >= '"+fromDate+"' and hit_date <= '"+toDate+"'";
			}
			if(filter.getCustomerIpAddress() != null){
				sqlQuery += " AND customer_ip_address like '%"+filter.getCustomerIpAddress()+"%' ";
			}
			if(filter.getActionType() != null){
				sqlQuery += " AND api_name like '"+filter.getActionType()+"' ";
			}
			if(filter.getActionResult() != null){
				sqlQuery += " AND action_result like '%"+filter.getActionResult()+"%' ";
			}
			if(filter.getPlatform()!=null && !filter.getPlatform().isEmpty()){
				sqlQuery += " AND platform like '%"+filter.getPlatform()+"%' ";
			}
			if(filter.getCustomerId()!=null){
				sqlQuery += " AND customer_id = "+filter.getCustomerId();
			}
			if(filter.getContestId()!=null){
				sqlQuery += " AND contest_id = "+filter.getContestId();
			}
			if(filter.getText1()!=null && !filter.getText1().isEmpty()){
				sqlQuery += " AND action_result like '%"+filter.getText1()+"%' ";
			}
			if(filter.getText2()!=null && !filter.getText2().isEmpty()){
				sqlQuery += " AND app_ver like '%"+filter.getText2()+"%' ";
			}
			if(filter.getHittingDateStr() != null){
				if(Util.extractDateElement(filter.getHittingDateStr(),"DAY") > 0){
					sqlQuery += " AND DATEPART(day,hit_date) = "+Util.extractDateElement(filter.getHittingDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getHittingDateStr(),"MONTH") > 0){
					sqlQuery += " AND DATEPART(month,hit_date) = "+Util.extractDateElement(filter.getHittingDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getHittingDateStr(),"YEAR") > 0){
					sqlQuery += " AND DATEPART(year,hit_date) = "+Util.extractDateElement(filter.getHittingDateStr(),"YEAR")+" ";
				}
			}
			sqlQuery += " order by id desc";
			session  = sessionFactory.openSession();
			SQLQuery query = session.createSQLQuery(sqlQuery);
			
			query.addScalar("id",Hibernate.INTEGER);
			query.addScalar("customerIpAddress",Hibernate.STRING);
			query.addScalar("platform",Hibernate.STRING);
			query.addScalar("apiName",Hibernate.STRING);
			query.addScalar("hitDate",Hibernate.TIMESTAMP);
			query.addScalar("custId",Hibernate.INTEGER);
			query.addScalar("description",Hibernate.STRING);
			query.addScalar("contestId",Hibernate.INTEGER);
			query.addScalar("appVersion",Hibernate.STRING);
			webServiceTracking = query.setResultTransformer(Transformers.aliasToBean(WebServiceTracking.class)).list();
			return webServiceTracking;
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return null;
	}
	
	
	public Collection<WebServiceTracking> getContestLogToExport(String fromDate, String toDate,GridHeaderFilters filter){
		Collection<WebServiceTracking> webServiceTracking = null;
		Session session = null;
		try	{					
			String sqlQuery = "select id as id, cust_ip_addr as customerIpAddress, platform as platform,"+
							"api_name as apiName,start_date as hitDate, contest_id as contestId,cust_id as custId,"+
							"description as description, app_ver as appVersion "+
							"from web_service_tracking_cassandra with(nolock) where platform <> 'TICK_TRACKER'  ";
			
			if(fromDate != null || toDate != null){
				sqlQuery += " AND start_date >= '"+fromDate+"' and start_date <= '"+toDate+"'";
			}
			if(filter.getCustomerIpAddress() != null){
				sqlQuery += " AND cust_ip_addr like '%"+filter.getCustomerIpAddress()+"%' ";
			}
			if(filter.getActionType() != null){
				sqlQuery += " AND api_name like '"+filter.getActionType()+"' ";
			}
			if(filter.getActionResult() != null){
				sqlQuery += " AND description like '%"+filter.getActionResult()+"%' ";
			}
			if(filter.getPlatform()!=null && !filter.getPlatform().isEmpty()){
				sqlQuery += " AND platform like '%"+filter.getPlatform()+"%' ";
			}
			if(filter.getCustomerId()!=null){
				sqlQuery += " AND customer_id = "+filter.getCustomerId();
			}
			if(filter.getContestId()!=null){
				sqlQuery += " AND contest_id = "+filter.getContestId();
			}
			if(filter.getText1()!=null && !filter.getText1().isEmpty()){
				sqlQuery += " AND action_result like '%"+filter.getText1()+"%' ";
			}
			if(filter.getText2()!=null && !filter.getText2().isEmpty()){
				sqlQuery += " AND app_ver like '%"+filter.getText2()+"%' ";
			}
			if(filter.getHittingDateStr() != null){
				if(Util.extractDateElement(filter.getHittingDateStr(),"DAY") > 0){
					sqlQuery += " AND DATEPART(day,start_date) = "+Util.extractDateElement(filter.getHittingDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getHittingDateStr(),"MONTH") > 0){
					sqlQuery += " AND DATEPART(month,start_date) = "+Util.extractDateElement(filter.getHittingDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getHittingDateStr(),"YEAR") > 0){
					sqlQuery += " AND DATEPART(year,start_date) = "+Util.extractDateElement(filter.getHittingDateStr(),"YEAR")+" ";
				}
			}
			sqlQuery += " order by id desc";
			session  = sessionFactory.openSession();
			SQLQuery query = session.createSQLQuery(sqlQuery);
			
			query.addScalar("id",Hibernate.INTEGER);
			query.addScalar("customerIpAddress",Hibernate.STRING);
			query.addScalar("platform",Hibernate.STRING);
			query.addScalar("apiName",Hibernate.STRING);
			query.addScalar("hitDate",Hibernate.TIMESTAMP);
			query.addScalar("custId",Hibernate.INTEGER);
			query.addScalar("description",Hibernate.STRING);
			query.addScalar("contestId",Hibernate.INTEGER);
			query.addScalar("appVersion",Hibernate.STRING);
			webServiceTracking = query.setResultTransformer(Transformers.aliasToBean(WebServiceTracking.class)).list();
			return webServiceTracking;
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return null;
	}
	
	
	public List<PayPalTracking> getPayPalTrackingToExport(GridHeaderFilters filter){
		Session session = null;
		Integer startFrom = 0;
		try {
			String queryStr = "Select pt.id as id,pt.customer_id as customerId,pt.event_id as eventId,pt.qty as quantity,pt.zone as zone,"+
					"CAST(pt.order_total AS DOUBLE PRECISION) as orderTotal,pt.order_type as orderType,pt.platform as platform,pt.status as status,"+
					"pt.paypal_transaction_id as paypalTransactionId,pt.transaction_id as transactionId,pt.created_date as createdDate,"+
					"pt.last_updated as lastUpdated,ptd.order_id as orderId,ptd.payment_id as paymentId,ptd.payer_email as payerEmail,"+
					"ptd.payer_f_name as payerFirstName,ptd.payer_l_name as payerLastName,ptd.payer_phone as payerPhone,"+
					"ptd.transaction_date as transactionDate"+
					" from paypal_tracking pt with(nolock) join paypal_transaction_details ptd with(nolock) on pt.transaction_id = ptd.id";
			/*
			if(orderId!=null && orderId>0){
				queryStr += " AND ptd.order_id = "+orderId+"";
			}
			if(customerName!=null && !customerName.isEmpty()){
				queryStr += " AND ptd.payer_f_name like '%"+customerName+"%'";
			}
			if(transactionStatus != null && !transactionStatus.isEmpty()){
				queryStr += " AND pt.status like '%"+transactionStatus+"%'";
			}
			*/
			if(filter.getCustomerId() != null){
				queryStr += " AND pt.customer_id = "+filter.getCustomerId()+" ";
			}
			if(filter.getCustomerName() != null){
				queryStr += " AND ptd.payer_f_name like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getLastName() != null){
				queryStr += " AND ptd.payer_l_name like '%"+filter.getLastName()+"%' ";
			}
			if(filter.getEmail() != null){
				queryStr += " AND ptd.payer_email like '%"+filter.getEmail()+"%' ";
			}
			if(filter.getPhone() != null){
				queryStr += " AND ptd.payer_phone like '%"+filter.getPhone()+"%' ";
			}
			if(filter.getCustomerOrderId() != null){
				queryStr += " AND ptd.order_id = "+filter.getCustomerOrderId()+" ";
			}
			if(filter.getOrderTotal() != null){
				queryStr += " AND CAST(pt.order_total AS DOUBLE PRECISION) = "+filter.getOrderTotal()+" ";
			}
			if(filter.getOrderType() != null){
				queryStr += " AND pt.order_type like '%"+filter.getOrderType()+"%' ";
			}
			if(filter.getEventId() != null){
				queryStr += " AND pt.event_id = "+filter.getEventId()+" ";
			}
			if(filter.getQuantity() != null){
				queryStr += " AND pt.qty = "+filter.getQuantity()+" ";
			}
			if(filter.getZone() != null){
				queryStr += " AND pt.zone like '%"+filter.getZone()+"%' ";
			}
			if(filter.getPaypalTransactionId() != null){
				queryStr += " AND pt.paypal_transaction_id like '%"+filter.getPaypalTransactionId()+"%' ";
			}
			if(filter.getTransactionId() != null){
				queryStr += " AND pt.transaction_id = "+filter.getTransactionId()+" ";
			}
			if(filter.getPaymentId() != null){
				queryStr += " AND ptd.payment_id like '%"+filter.getPaymentId()+"%' ";
			}
			if(filter.getPlatform() != null){
				queryStr += " AND pt.platform like '%"+filter.getPlatform()+"%' ";
			}
			if(filter.getStatus() != null){
				queryStr += " AND pt.status like '%"+filter.getStatus()+"%' ";
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					queryStr += " AND DATEPART(day,pt.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					queryStr += " AND DATEPART(month,pt.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					queryStr += " AND DATEPART(year,pt.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getLastUpdatedDateStr() != null){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					queryStr += " AND DATEPART(day,pt.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
					queryStr += " AND DATEPART(month,pt.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
					queryStr += " AND DATEPART(year,pt.last_updated) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getTransactionDateStr() != null){
				if(Util.extractDateElement(filter.getTransactionDateStr(),"DAY") > 0){
					queryStr += " AND DATEPART(day,ptd.transaction_date) = "+Util.extractDateElement(filter.getTransactionDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getTransactionDateStr(),"MONTH") > 0){
					queryStr += " AND DATEPART(month,ptd.transaction_date) = "+Util.extractDateElement(filter.getTransactionDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getTransactionDateStr(),"YEAR") > 0){
					queryStr += " AND DATEPART(year,ptd.transaction_date) = "+Util.extractDateElement(filter.getTransactionDateStr(),"YEAR")+" ";
				}
			}
			session  = sessionFactory.openSession();
			Query query = session.createSQLQuery(queryStr);
			List<PayPalTracking> list = query.setResultTransformer(Transformers.aliasToBean(PayPalTracking.class)).list();
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	

	public List<RTFCustomerPromotionalOffer> getCustomerPromotionalOffer(GridHeaderFilters filter, String pageNo){
		List<RTFCustomerPromotionalOffer> list = null;
		Integer startFrom = 0;
		try {
			String sql = "select rtfcp.id as id, rtfcp.customer_id as customerId, cus.cust_name as firstName, cus.last_name as lastName," +
					"cus.email as email, rtfcp.promo_code as promoCode, rtfcp.start_date as startDate," +
					"CASE WHEN rtfcp.is_flat_discount = 1 THEN concat('$',replace(rtfcp.discount,'.00','')) "+
					"WHEN rtfcp.is_flat_discount = 0 THEN concat(replace(rtfcp.discount,'.00',''),'%') END as discountStr, "+
					"rtfcp.end_date as endDate, rtfcp.status as status, rtfcp.created_date as createdDate, rtfcp.modified_time as modifiedDate "+
					"from rtf_customer_promotional_offers rtfcp with(nolock) " +
					"left join customer cus with(nolock) on cus.id = rtfcp.customer_id "+
					"WHERE 1=1 and rtfcp.status != 'REDEEMED' and cus.is_test_account <> 1 ";
						
			if(filter.getCustomerName() != null && !filter.getCustomerName().isEmpty()){
				sql += " AND cus.cust_name like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getLastName() != null && !filter.getLastName().isEmpty()){
				sql += " AND cus.last_name like '%"+filter.getLastName()+"%' ";
			}
			if(filter.getEmail() != null && !filter.getEmail().isEmpty()){
				sql += " AND cus.email like '%"+filter.getEmail()+"%' ";
			}
			if(filter.getPromoCode()!=null && !filter.getPromoCode().isEmpty()){
				sql += " AND rtfcp.promo_code like '"+filter.getPromoCode()+"' ";
			}
			if(filter.getDiscountCouponPrice()!=null){
				sql += " AND rtfcp.discount = "+filter.getDiscountCouponPrice();
			}
			if(filter.getStartDate()!=null ){
				if(Util.extractDateElement(filter.getStartDate(),"DAY") > 0){
					sql += " AND DATEPART(day,rtfcp.start_date) = "+Util.extractDateElement(filter.getStartDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getStartDate(),"MONTH") > 0){
					sql += " AND DATEPART(month,rtfcp.start_date) = "+Util.extractDateElement(filter.getStartDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getStartDate(),"YEAR") > 0){
					sql += " AND DATEPART(year,rtfcp.start_date) = "+Util.extractDateElement(filter.getStartDate(),"YEAR")+" ";
				}
			}
			if(filter.getEndDate()!=null ){
				if(Util.extractDateElement(filter.getEndDate(),"DAY") > 0){
					sql += " AND DATEPART(day,rtfcp.end_date) = "+Util.extractDateElement(filter.getEndDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEndDate(),"MONTH") > 0){
					sql += " AND DATEPART(month,rtfcp.end_date) = "+Util.extractDateElement(filter.getEndDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEndDate(),"YEAR") > 0){
					sql += " AND DATEPART(year,rtfcp.end_date) = "+Util.extractDateElement(filter.getEndDate(),"YEAR")+" ";
				}
			}
			if(filter.getStatus()!=null && !filter.getStatus().isEmpty()){
				sql += " AND rtfcp.status like '"+filter.getStatus()+"' ";
			}			
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,rtfcp.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,rtfcp.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,rtfcp.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getLastUpdatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,rtfcp.modified_time) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,rtfcp.modified_time) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,rtfcp.modified_time) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
				}
			}
			String sortingSql = GridSortingUtil.getCustomerPromotionalOfferSortingSql(filter);
			if(sortingSql!=null && !sortingSql.isEmpty()){
				sql += sortingSql;
			}else{
				sql += " order by rtfcp.created_date desc";
			}
			staticSession = QueryManagerDAO.getStaticSession();
			SQLQuery query = staticSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("customerId", Hibernate.INTEGER);
			query.addScalar("firstName", Hibernate.STRING);
			query.addScalar("lastName", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("promoCode", Hibernate.STRING);
			query.addScalar("discountStr", Hibernate.STRING);
			query.addScalar("startDate", Hibernate.TIMESTAMP);
			query.addScalar("endDate", Hibernate.TIMESTAMP);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("modifiedDate", Hibernate.TIMESTAMP);
			startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
			list = query.setResultTransformer(Transformers.aliasToBean(RTFCustomerPromotionalOffer.class)).setFirstResult(startFrom).setMaxResults(PaginationUtil.PAGESIZE).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public Integer getCustomerPromotionalOfferCount(GridHeaderFilters filter){
		Integer count = 0;
		try {
			String sql = "select count(*) from rtf_customer_promotional_offers rtfcp with(nolock) " +
					"left join customer cus with(nolock) on cus.id = rtfcp.customer_id "+
					"WHERE 1=1 and rtfcp.status != 'REDEEMED' and cus.is_test_account <> 1 ";
						
			if(filter.getCustomerName() != null && !filter.getCustomerName().isEmpty()){
				sql += " AND cus.cust_name like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getLastName() != null && !filter.getLastName().isEmpty()){
				sql += " AND cus.last_name like '%"+filter.getLastName()+"%' ";
			}
			if(filter.getEmail() != null && !filter.getEmail().isEmpty()){
				sql += " AND cus.email like '%"+filter.getEmail()+"%' ";
			}
			if(filter.getPromoCode()!=null && !filter.getPromoCode().isEmpty()){
				sql += " AND rtfcp.promo_code like '"+filter.getPromoCode()+"' ";
			}
			if(filter.getDiscountCouponPrice()!=null){
				sql += " AND rtfcp.discount = "+filter.getDiscountCouponPrice();
			}
			if(filter.getStartDate()!=null ){
				if(Util.extractDateElement(filter.getStartDate(),"DAY") > 0){
					sql += " AND DATEPART(day,rtfcp.start_date) = "+Util.extractDateElement(filter.getStartDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getStartDate(),"MONTH") > 0){
					sql += " AND DATEPART(month,rtfcp.start_date) = "+Util.extractDateElement(filter.getStartDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getStartDate(),"YEAR") > 0){
					sql += " AND DATEPART(year,rtfcp.start_date) = "+Util.extractDateElement(filter.getStartDate(),"YEAR")+" ";
				}
			}
			if(filter.getEndDate()!=null ){
				if(Util.extractDateElement(filter.getEndDate(),"DAY") > 0){
					sql += " AND DATEPART(day,rtfcp.end_date) = "+Util.extractDateElement(filter.getEndDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEndDate(),"MONTH") > 0){
					sql += " AND DATEPART(month,rtfcp.end_date) = "+Util.extractDateElement(filter.getEndDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEndDate(),"YEAR") > 0){
					sql += " AND DATEPART(year,rtfcp.end_date) = "+Util.extractDateElement(filter.getEndDate(),"YEAR")+" ";
				}
			}
			if(filter.getStatus()!=null && !filter.getStatus().isEmpty()){
				sql += " AND rtfcp.status like '"+filter.getStatus()+"' ";
			}			
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,rtfcp.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,rtfcp.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,rtfcp.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getLastUpdatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,rtfcp.modified_time) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,rtfcp.modified_time) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,rtfcp.modified_time) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
				}
			}
			
			staticSession = QueryManagerDAO.getStaticSession();
			SQLQuery query = staticSession.createSQLQuery(sql);			
			count = Integer.parseInt(String.valueOf(query.uniqueResult()));	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}
	
	
	public List<RTFCustomerPromotionalOffer> getCustomerPromotionalOfferToExport(GridHeaderFilters filter){
		List<RTFCustomerPromotionalOffer> list = null;
		try {
			String sql = "select rtfcp.id as id, rtfcp.customer_id as customerId, cus.cust_name as firstName, cus.last_name as lastName," +
					"cus.email as email, rtfcp.promo_code as promoCode, rtfcp.start_date as startDate," +
					"CASE WHEN rtfcp.is_flat_discount = 1 THEN concat('$',replace(rtfcp.discount,'.00','')) "+
					"WHEN rtfcp.is_flat_discount = 0 THEN concat(replace(rtfcp.discount,'.00',''),'%') END as discountStr, "+
					"rtfcp.end_date as endDate, rtfcp.status as status, rtfcp.created_date as createdDate, rtfcp.modified_time as modifiedDate "+
					"from rtf_customer_promotional_offers rtfcp with(nolock) " +
					"left join customer cus with(nolock) on cus.id = rtfcp.customer_id "+
					"WHERE 1=1 and rtfcp.status != 'REDEEMED' and cus.is_test_account <> 1 ";
			
			if(filter.getCustomerName() != null && !filter.getCustomerName().isEmpty()){
				sql += " AND cus.cust_name like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getLastName() != null && !filter.getLastName().isEmpty()){
				sql += " AND cus.last_name like '%"+filter.getLastName()+"%' ";
			}
			if(filter.getEmail() != null && !filter.getEmail().isEmpty()){
				sql += " AND cus.email like '%"+filter.getEmail()+"%' ";
			}
			if(filter.getPromoCode()!=null && !filter.getPromoCode().isEmpty()){
				sql += " AND rtfcp.promo_code like '"+filter.getPromoCode()+"' ";
			}
			if(filter.getDiscountCouponPrice()!=null){
				sql += " AND rtfcp.discount = "+filter.getDiscountCouponPrice();
			}
			if(filter.getStartDate()!=null ){
				if(Util.extractDateElement(filter.getStartDate(),"DAY") > 0){
					sql += " AND DATEPART(day,rtfcp.start_date) = "+Util.extractDateElement(filter.getStartDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getStartDate(),"MONTH") > 0){
					sql += " AND DATEPART(month,rtfcp.start_date) = "+Util.extractDateElement(filter.getStartDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getStartDate(),"YEAR") > 0){
					sql += " AND DATEPART(year,rtfcp.start_date) = "+Util.extractDateElement(filter.getStartDate(),"YEAR")+" ";
				}
			}
			if(filter.getEndDate()!=null ){
				if(Util.extractDateElement(filter.getEndDate(),"DAY") > 0){
					sql += " AND DATEPART(day,rtfcp.end_date) = "+Util.extractDateElement(filter.getEndDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEndDate(),"MONTH") > 0){
					sql += " AND DATEPART(month,rtfcp.end_date) = "+Util.extractDateElement(filter.getEndDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEndDate(),"YEAR") > 0){
					sql += " AND DATEPART(year,rtfcp.end_date) = "+Util.extractDateElement(filter.getEndDate(),"YEAR")+" ";
				}
			}
			if(filter.getStatus()!=null && !filter.getStatus().isEmpty()){
				sql += " AND rtfcp.status like '"+filter.getStatus()+"' ";
			}			
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,rtfcp.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,rtfcp.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,rtfcp.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getLastUpdatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,rtfcp.modified_time) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,rtfcp.modified_time) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,rtfcp.modified_time) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
				}
			}
			
			sql += " order by rtfcp.created_date desc";
			staticSession = QueryManagerDAO.getStaticSession();
			SQLQuery query = staticSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("customerId", Hibernate.INTEGER);
			query.addScalar("firstName", Hibernate.STRING);
			query.addScalar("lastName", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("promoCode", Hibernate.STRING);
			query.addScalar("discountStr", Hibernate.STRING);
			query.addScalar("startDate", Hibernate.TIMESTAMP);
			query.addScalar("endDate", Hibernate.TIMESTAMP);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("modifiedDate", Hibernate.TIMESTAMP);
			list = query.setResultTransformer(Transformers.aliasToBean(RTFCustomerPromotionalOffer.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public List<RTFPromotionalOfferTracking> getPromotionalOfferTracking(GridHeaderFilters filter, String pageNo){
		List<RTFPromotionalOfferTracking> list = null;
		Properties paramApplicationPlatform = new Properties();
		paramApplicationPlatform.put("enumClass", "com.rtw.tracker.datas.ApplicationPlatform");
		paramApplicationPlatform.put("type", "12"); /*type 12 instructs to use the String representation of enum value*/
		Integer startFrom = 0;
		try {
			String sql = "select rtfpt.id as id, cus.id as customerId, cus.cust_name as firstName, cus.last_name as lastName," +
					"cus.email as email, rtfpt.promo_code as promoCode, " +
					"CASE WHEN rtfpt.is_flat_discount = 1 THEN concat('$',replace(rtfpt.additional_discount_conv,'.00','')) "+
					"WHEN rtfpt.is_flat_discount = 0 THEN concat(replace(rtfpt.additional_discount_conv*100,'.00',''),'%') END as discount, "+
					"rtfpt.status as status, creq.created_time as createdDate, "+
					"rtfpt.order_id as orderId, creq.order_total as orderTotal, creq.quantity as quantity, creq.ip_address as ipAddress, "+
					"creq.primary_payment_method as primaryPaymentMethod, creq.secondary_payment_method as secondaryPaymentMethod, "+
					"creq.third_payment_method as thirdPaymentMethod, creq.platform as platForm "+
					"from rtf_promotional_order_tracking_new rtfpt with(nolock) "+
					"inner join customer cus with(nolock) on cus.id = rtfpt.customer_id "+
					"inner join customer_order_request creq with(nolock) on creq.order_id = rtfpt.order_id "+
					"WHERE 1=1 and rtfpt.status = 'COMPLETED' and cus.is_test_account <> 1 ";
					
			
			if(filter.getCustomerId() != null){
				sql += " AND cus.id = "+filter.getCustomerId(); 
			}
			if(filter.getCustomerName() != null && !filter.getCustomerName().isEmpty()){
				sql += " AND cus.cust_name like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getLastName() != null && !filter.getLastName().isEmpty()){
				sql += " AND cus.last_name like '%"+filter.getLastName()+"%' ";
			}
			if(filter.getEmail() != null && !filter.getEmail().isEmpty()){
				sql += " AND cus.email like '%"+filter.getEmail()+"%' ";
			}
			if(filter.getPromoCode()!=null && !filter.getPromoCode().isEmpty()){
				sql += " AND rtfpt.promo_code like '"+filter.getPromoCode()+"' ";
			}
			if(filter.getDiscountCouponPrice()!=null){
				sql += " AND rtfpt.discount = "+(filter.getDiscountCouponPrice()/100);
			}
			if(filter.getStatus()!=null && !filter.getStatus().isEmpty()){
				sql += " AND rtfpt.status like '"+filter.getStatus()+"' ";
			}			
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,creq.created_time) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,creq.created_time) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,creq.created_time) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getCustomerOrderId() != null){
				sql += " AND rtfpt.order_id = "+filter.getCustomerOrderId();
			}
			if(filter.getOrderTotal() != null){
				sql += " AND creq.order_total = "+filter.getOrderTotal();
			}
			if(filter.getQuantity() != null){
				sql += " AND creq.quantity = "+filter.getQuantity();
			}
			if(filter.getCustomerIpAddress() != null && !filter.getCustomerIpAddress().isEmpty()){				
				sql += " AND creq.ip_address like '%"+filter.getCustomerIpAddress()+"%' ";
			}
			if(filter.getPrimaryPaymentMethod() != null && !filter.getPrimaryPaymentMethod().isEmpty()){
				sql += " AND creq.primary_payment_method like '%"+filter.getPrimaryPaymentMethod()+"%' ";
			}
			if(filter.getSecondaryPaymentMethod() != null && !filter.getSecondaryPaymentMethod().isEmpty()){
				sql += " AND creq.secondary_payment_method like '%"+filter.getSecondaryPaymentMethod()+"%' ";
			}
			if(filter.getThirdPaymentMethod() != null && !filter.getThirdPaymentMethod().isEmpty()){
				sql += " AND creq.third_payment_method like '%"+filter.getThirdPaymentMethod()+"%' ";
			}
			if(filter.getPlatform() != null && !filter.getPlatform().isEmpty()){
				sql += " AND creq.platform like '%"+filter.getPlatform()+"%' ";
			}
			String sortingSql = GridSortingUtil.getPromotionalOfferTrackingSortingSql(filter);
			if(sortingSql!=null && !sortingSql.isEmpty()){
				sql += sortingSql;
			}else{
				sql += " order by rtfpt.created_date desc";
			}
			
			staticSession = QueryManagerDAO.getStaticSession();
			SQLQuery query = staticSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("customerId", Hibernate.INTEGER);
			query.addScalar("firstName", Hibernate.STRING);
			query.addScalar("lastName", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("promoCode", Hibernate.STRING);
			query.addScalar("discount", Hibernate.STRING);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("orderId", Hibernate.INTEGER);
			query.addScalar("orderTotal", Hibernate.DOUBLE);
			query.addScalar("quantity", Hibernate.INTEGER);
			query.addScalar("ipAddress", Hibernate.STRING);
			query.addScalar("primaryPaymentMethod", Hibernate.STRING);
			query.addScalar("secondaryPaymentMethod", Hibernate.STRING);
			query.addScalar("thirdPaymentMethod", Hibernate.STRING);
			query.addScalar("platForm", Hibernate.custom(EnumType.class, paramApplicationPlatform));
			startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
			list = query.setResultTransformer(Transformers.aliasToBean(RTFPromotionalOfferTracking.class)).setFirstResult(startFrom).setMaxResults(PaginationUtil.PAGESIZE).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public Integer getPromotionalOfferTrackingCount(GridHeaderFilters filter){
		Integer count = 0;
		try {
			String sql = "select count(*) "+
					"from rtf_promotional_order_tracking_new rtfpt with(nolock) "+
					"inner join customer cus with(nolock) on cus.id = rtfpt.customer_id "+
					"inner join customer_order_request creq with(nolock) on creq.order_id = rtfpt.order_id "+
					"WHERE 1=1 and rtfpt.status = 'COMPLETED' and cus.is_test_account <> 1 ";
					
			
			if(filter.getCustomerId() != null){
				sql += " AND cus.id = "+filter.getCustomerId(); 
			}
			if(filter.getCustomerName() != null && !filter.getCustomerName().isEmpty()){
				sql += " AND cus.cust_name like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getLastName() != null && !filter.getLastName().isEmpty()){
				sql += " AND cus.last_name like '%"+filter.getLastName()+"%' ";
			}
			if(filter.getEmail() != null && !filter.getEmail().isEmpty()){
				sql += " AND cus.email like '%"+filter.getEmail()+"%' ";
			}
			if(filter.getPromoCode()!=null && !filter.getPromoCode().isEmpty()){
				sql += " AND rtfpt.promo_code like '"+filter.getPromoCode()+"' ";
			}
			if(filter.getDiscountCouponPrice()!=null){
				sql += " AND rtfpt.discount = "+(filter.getDiscountCouponPrice()/100);
			}
			if(filter.getStatus()!=null && !filter.getStatus().isEmpty()){
				sql += " AND rtfpt.status like '"+filter.getStatus()+"' ";
			}			
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,creq.created_time) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,creq.created_time) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,creq.created_time) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getCustomerOrderId() != null){
				sql += " AND rtfpt.order_id = "+filter.getCustomerOrderId();
			}
			if(filter.getOrderTotal() != null){
				sql += " AND creq.order_total = "+filter.getOrderTotal();
			}
			if(filter.getQuantity() != null){
				sql += " AND creq.quantity = "+filter.getQuantity();
			}
			if(filter.getCustomerIpAddress() != null && !filter.getCustomerIpAddress().isEmpty()){				
				sql += " AND creq.ip_address like '%"+filter.getCustomerIpAddress()+"%' ";
			}
			if(filter.getPrimaryPaymentMethod() != null && !filter.getPrimaryPaymentMethod().isEmpty()){
				sql += " AND creq.primary_payment_method like '%"+filter.getPrimaryPaymentMethod()+"%' ";
			}
			if(filter.getSecondaryPaymentMethod() != null && !filter.getSecondaryPaymentMethod().isEmpty()){
				sql += " AND creq.secondary_payment_method like '%"+filter.getSecondaryPaymentMethod()+"%' ";
			}
			if(filter.getThirdPaymentMethod() != null && !filter.getThirdPaymentMethod().isEmpty()){
				sql += " AND creq.third_payment_method like '%"+filter.getThirdPaymentMethod()+"%' ";
			}
			if(filter.getPlatform() != null && !filter.getPlatform().isEmpty()){
				sql += " AND creq.platform like '%"+filter.getPlatform()+"%' ";
			}
			staticSession = QueryManagerDAO.getStaticSession();
			SQLQuery query = staticSession.createSQLQuery(sql);			
			count = Integer.parseInt(String.valueOf(query.uniqueResult()));	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}
	
	public List<RTFPromotionalOfferTracking> getPromotionalOfferTrackingToExport(GridHeaderFilters filter){
		List<RTFPromotionalOfferTracking> list = null;
		Properties paramApplicationPlatform = new Properties();
		paramApplicationPlatform.put("enumClass", "com.rtw.tracker.datas.ApplicationPlatform");
		paramApplicationPlatform.put("type", "12"); /*type 12 instructs to use the String representation of enum value*/
		
		try {
			String sql = "select rtfpt.id as id, cus.id as customerId, cus.cust_name as firstName, cus.last_name as lastName," +
					"cus.email as email, rtfpt.promo_code as promoCode, " +
					"CASE WHEN rtfpt.is_flat_discount = 1 THEN concat('$',replace(rtfpt.additional_discount_conv,'.00','')) "+
					"WHEN rtfpt.is_flat_discount = 0 THEN concat(replace(rtfpt.additional_discount_conv*100,'.00',''),'%') END as discount, "+
					"rtfpt.status as status, creq.created_time as createdDate, "+
					"rtfpt.order_id as orderId, creq.order_total as orderTotal, creq.quantity as quantity, creq.ip_address as ipAddress, "+
					"creq.primary_payment_method as primaryPaymentMethod, creq.secondary_payment_method as secondaryPaymentMethod, "+
					"creq.third_payment_method as thirdPaymentMethod, creq.platform as platForm "+
					"from rtf_promotional_order_tracking_new rtfpt with(nolock) "+
					"inner join customer cus with(nolock) on cus.id = rtfpt.customer_id "+
					"inner join customer_order_request creq with(nolock) on creq.order_id = rtfpt.order_id "+
					"WHERE 1=1 and rtfpt.status = 'COMPLETED' and cus.is_test_account <> 1 ";
					
			
			if(filter.getCustomerId() != null){
				sql += " AND cus.id = "+filter.getCustomerId(); 
			}
			if(filter.getCustomerName() != null && !filter.getCustomerName().isEmpty()){
				sql += " AND cus.cust_name like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getLastName() != null && !filter.getLastName().isEmpty()){
				sql += " AND cus.last_name like '%"+filter.getLastName()+"%' ";
			}
			if(filter.getEmail() != null && !filter.getEmail().isEmpty()){
				sql += " AND cus.email like '%"+filter.getEmail()+"%' ";
			}
			if(filter.getPromoCode()!=null && !filter.getPromoCode().isEmpty()){
				sql += " AND rtfpt.promo_code like '"+filter.getPromoCode()+"' ";
			}
			if(filter.getDiscountCouponPrice()!=null){
				sql += " AND rtfpt.discount = "+(filter.getDiscountCouponPrice()/100);
			}
			if(filter.getStatus()!=null && !filter.getStatus().isEmpty()){
				sql += " AND rtfpt.status like '"+filter.getStatus()+"' ";
			}			
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,creq.created_time) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,creq.created_time) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,creq.created_time) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getCustomerOrderId() != null){
				sql += " AND rtfpt.order_id = "+filter.getCustomerOrderId();
			}
			if(filter.getOrderTotal() != null){
				sql += " AND creq.order_total = "+filter.getOrderTotal();
			}
			if(filter.getQuantity() != null){
				sql += " AND creq.quantity = "+filter.getQuantity();
			}
			if(filter.getCustomerIpAddress() != null && !filter.getCustomerIpAddress().isEmpty()){				
				sql += " AND creq.ip_address like '%"+filter.getCustomerIpAddress()+"%' ";
			}
			if(filter.getPrimaryPaymentMethod() != null && !filter.getPrimaryPaymentMethod().isEmpty()){
				sql += " AND creq.primary_payment_method like '%"+filter.getPrimaryPaymentMethod()+"%' ";
			}
			if(filter.getSecondaryPaymentMethod() != null && !filter.getSecondaryPaymentMethod().isEmpty()){
				sql += " AND creq.secondary_payment_method like '%"+filter.getSecondaryPaymentMethod()+"%' ";
			}
			if(filter.getThirdPaymentMethod() != null && !filter.getThirdPaymentMethod().isEmpty()){
				sql += " AND creq.third_payment_method like '%"+filter.getThirdPaymentMethod()+"%' ";
			}
			if(filter.getPlatform() != null && !filter.getPlatform().isEmpty()){
				sql += " AND creq.platform like '%"+filter.getPlatform()+"%' ";
			}
			
			sql += " order by rtfpt.created_date desc";
			staticSession = QueryManagerDAO.getStaticSession();
			SQLQuery query = staticSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("customerId", Hibernate.INTEGER);
			query.addScalar("firstName", Hibernate.STRING);
			query.addScalar("lastName", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("promoCode", Hibernate.STRING);
			query.addScalar("discount", Hibernate.STRING);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("orderId", Hibernate.INTEGER);
			query.addScalar("orderTotal", Hibernate.DOUBLE);
			query.addScalar("quantity", Hibernate.INTEGER);
			query.addScalar("ipAddress", Hibernate.STRING);
			query.addScalar("primaryPaymentMethod", Hibernate.STRING);
			query.addScalar("secondaryPaymentMethod", Hibernate.STRING);
			query.addScalar("thirdPaymentMethod", Hibernate.STRING);
			query.addScalar("platForm", Hibernate.custom(EnumType.class, paramApplicationPlatform));
			list = query.setResultTransformer(Transformers.aliasToBean(RTFPromotionalOfferTracking.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	

	public Integer getCountryByShortDesc(String shortDesc){
		Session session =null;
		Integer countryId = 0;
		try{
			session  = sessionFactory.openSession();
			String sqlQuery = "select id FROM Country with(nolock) where short_desc like '%"+shortDesc+"%' ";
			SQLQuery query = session.createSQLQuery(sqlQuery);
			countryId = Integer.parseInt(String.valueOf(query.uniqueResult()));
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return countryId;
	}
	
	public List<RTFPromoOffers> getPromotionalOfferDetails(Integer id, GridHeaderFilters filter){
		List<RTFPromoOffers> list = null;		
		try {
			String sql = "select rtfpt.id as id, rtfpt.promo_type as promoType, rtfpt.artist_id as artistId," +
					"rtfpt.venue_id as venueId, rtfpt.grand_child_id as grandChildId, rtfpt.child_id as childId," +
					"a.name as artistName, v.building as venueName, pc.name as parentCategory, cc.name as childCategory, " +
					"gcc.name as grandChildCategory, rtfpt.parent_id as parentId, "+
					"rtfpt.event_id as eventId, e.name as eventName "+
					"from rtf_promotional_offer_dtl rtfpt with(nolock) " +
					"left join rtf_promotional_offer_hdr rtf with(nolock) on rtfpt.promo_offer_id = rtf.id " +
					"left join artist a with(nolock) on a.id=rtfpt.artist_id " +
					"left join venue v with(nolock) on v.id=rtfpt.venue_id " +
					"left join parent_category pc with(nolock) on pc.id=rtfpt.parent_id " +
					"left join child_category cc with(nolock) on cc.id=rtfpt.child_id " +
					"left join grand_child_category gcc with(nolock) on gcc.id=rtfpt.grand_child_id "+
					"left join event e with(nolock) on e.id = rtfpt.event_id WHERE 1=1 ";
			
			if(id != null){
				sql += " AND rtf.id = "+id;
			}			
			if(filter.getPromoType()!=null && !filter.getPromoType().isEmpty()){
				sql += " AND rtfpt.promo_type like '"+filter.getPromoType()+"' ";
			}
			if(filter.getArtistName()!=null && !filter.getArtistName().isEmpty()){
				sql += " AND a.name like '%"+filter.getArtistName()+"%' ";
			}
			if(filter.getVenueName()!=null && !filter.getVenueName().isEmpty()){
				sql += " AND v.building like '%"+filter.getVenueName()+"%' ";
			}
			if(filter.getParentCategoryName()!=null && !filter.getParentCategoryName().isEmpty()){
				sql += " AND pc.name like '%"+filter.getParentCategoryName()+"%' ";
			}
			if(filter.getChildCategoryName()!=null && !filter.getChildCategoryName().isEmpty()){
				sql += " AND cc.name like '"+filter.getChildCategoryName()+"' ";
			}
			if(filter.getGrandChildCategoryName()!=null && !filter.getGrandChildCategoryName().isEmpty()){
				sql += " AND gcc.name like '"+filter.getGrandChildCategoryName()+"' ";
			}
			if(filter.getEventName() !=null && !filter.getEventName().isEmpty()){
				sql += " AND e.name like '%"+filter.getEventName()+"%' ";
			}
			
			staticSession = QueryManagerDAO.getStaticSession();
			SQLQuery query = staticSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("promoType", Hibernate.STRING);
			query.addScalar("artistId", Hibernate.INTEGER);
			query.addScalar("venueId", Hibernate.INTEGER);
			query.addScalar("grandChildId", Hibernate.INTEGER);
			query.addScalar("childId", Hibernate.INTEGER);
			query.addScalar("parentId", Hibernate.INTEGER);
			query.addScalar("artistName", Hibernate.STRING);
			query.addScalar("venueName", Hibernate.STRING);
			query.addScalar("parentCategory", Hibernate.STRING);
			query.addScalar("childCategory", Hibernate.STRING);
			query.addScalar("grandChildCategory", Hibernate.STRING);
			query.addScalar("eventId", Hibernate.INTEGER);
			query.addScalar("eventName", Hibernate.STRING);
			list = query.setResultTransformer(Transformers.aliasToBean(RTFPromoOffers.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	
	public List<Event> getAllActiveEventDetailsWithVenue(String eventName){
		Session session = null;
		List<Event> events = null;
		try {
			String SQLQuery = "select E.event_id AS eventId, E.event_name as eventName, E.event_date AS eventDate,E.event_time AS eventTime, "+
				"E.building as building, E.city AS city, E.state as state, E.country as country "+
				"from event_details E with(nolock) where E.status=1 ";
				
				if(eventName!=null && !eventName.isEmpty()){
					SQLQuery += "AND E.event_name like '%"+eventName+"%' ";
				}
				SQLQuery += " order by E.event_date, E.event_time Asc";
				session  = getStaticSession();
				SQLQuery query = session.createSQLQuery(SQLQuery);
				query.addScalar("eventId", Hibernate.INTEGER);
				query.addScalar("eventName", Hibernate.STRING);
				query.addScalar("eventDate", Hibernate.DATE);
				query.addScalar("eventTime", Hibernate.TIME);
				query.addScalar("building", Hibernate.STRING);
				query.addScalar("city", Hibernate.STRING);
				query.addScalar("state", Hibernate.STRING);
				query.addScalar("country", Hibernate.STRING);
				events = query.setResultTransformer(Transformers.aliasToBean(Event.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return events;
	}
	
	
	public Collection<EventDetails> getAllActiveFavouriteEventDetailsByFilter(Integer customerId, GridHeaderFilters filter, String pageNo){
		Collection<EventDetails> eventDetails = null;
		Integer startFrom=0;
		try {
			String sqlQuery = "SELECT distinct E.id AS eventId, E.name AS eventName, E.event_date AS eventDate, "+
					   "E.event_time AS eventTime, E.venue_id AS venueId, V.building AS building, " +
					   "V.city As city, V.state AS state, V.country As country, V.postal_code AS postalCode, " +
					   "FE.insert_date AS eventCreationDate, FE.update_date AS eventLastUpdatedDate "+
					   " from event E with(nolock) left join cust_favorites_event_handler FE with(nolock) on FE.event_id=E.id "+
					   " left join venue V with(nolock) on E.venue_id=V.id "+
			   		   " WHERE E.status=1 ";
				
				if(customerId != null){
					sqlQuery += " AND FE.customer_id = "+customerId;
				}
				if(filter.getEventId() != null){
					sqlQuery += " AND E.id = "+filter.getEventId()+" ";
				}
				if(filter.getEventName() != null){
					sqlQuery += " AND E.name like '%"+filter.getEventName()+"%' ";
				}
				if(filter.getEventDateStr() != null){
					if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
						sqlQuery += " AND DATEPART(day,E.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,E.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,E.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getEventTimeStr() != null){
					if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
						sqlQuery += " AND DATEPART(hour,E.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
					}
					if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
						sqlQuery += " AND DATEPART(minute,E.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
					}
				}
				if(filter.getVenueName() != null){
					sqlQuery += " AND V.building like '%"+filter.getVenueName()+"%' ";
				}
				if(filter.getVenueId() != null){
					sqlQuery += " AND E.venue_id = "+filter.getVenueId()+" ";
				}
				if(filter.getCity() != null){
					sqlQuery += " AND V.city like '%"+filter.getCity()+"%' ";
				}
				if(filter.getState() != null){
					sqlQuery += " AND V.state like '%"+filter.getState()+"%' ";
				}
				if(filter.getCountry() != null){
					sqlQuery += " AND V.country like '%"+filter.getCountry()+"%' ";
				}
				if(filter.getCreatedDateStr() != null){
					if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
						sqlQuery += " AND DATEPART(day,FE.insert_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,FE.insert_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,FE.insert_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
					}
				}
				if(filter.getLastUpdatedDateStr() != null){
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
						sqlQuery += " AND DATEPART(day,FE.update_date) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,FE.update_date) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,FE.update_date) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
					}
				}
				sqlQuery +=" ORDER BY E.event_date, E.event_time Asc";
				staticSession  = getStaticSession();
				SQLQuery query = staticSession.createSQLQuery(sqlQuery);
				query.addScalar("eventId", Hibernate.INTEGER);
				query.addScalar("eventName", Hibernate.STRING);
				query.addScalar("eventDate", Hibernate.DATE);
				query.addScalar("eventTime", Hibernate.TIME);
				query.addScalar("venueId", Hibernate.INTEGER);
				query.addScalar("building", Hibernate.STRING);
				query.addScalar("city", Hibernate.STRING);
				query.addScalar("state", Hibernate.STRING);
				query.addScalar("country", Hibernate.STRING);
				query.addScalar("postalCode", Hibernate.STRING);
				query.addScalar("eventCreationDate", Hibernate.DATE);
				query.addScalar("eventLastUpdatedDate", Hibernate.DATE);
				startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
				eventDetails = query.setResultTransformer(Transformers.aliasToBean(EventDetails.class)).setFirstResult(startFrom).setMaxResults(PaginationUtil.PAGESIZE).list();
				
		} catch (Exception e) {
			e.printStackTrace();
		}
		return eventDetails;
	}
	
	/*public List<CityAutoSearch> getAllCityStateCountry(String name){
		Session session = null;
		List<CityAutoSearch> cityList = null;
		try {
			String SQLQuery = "select distinct c.city as city, c.state as state, c.state_full_name as stateName, "+
				"c.country as country, c.country_full_name as countryName "+
				"from city_auto_search c where 1=1 ";

			if(name!=null && !name.isEmpty()){
				SQLQuery += "AND (c.city like '%"+name+"%' OR c.state like '%"+name+"%' OR c.country like '%"+name+"%')";
			}
			
			session  = getStaticSession();
			SQLQuery query = session.createSQLQuery(SQLQuery);
			//query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("city", Hibernate.STRING);
			//query.addScalar("zipCode", Hibernate.STRING);
			query.addScalar("state", Hibernate.STRING);
			query.addScalar("stateName", Hibernate.STRING);
			query.addScalar("country", Hibernate.STRING);
			query.addScalar("countryName", Hibernate.STRING);
			cityList = query.setResultTransformer(Transformers.aliasToBean(CityAutoSearch.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cityList;
	}
	
	
	public List<CityAutoSearch> getCityStateCountry(Integer id, String cityName){
		Session session = null;
		List<CityAutoSearch> cityList = null;
		try {
			String SQLQuery = "select c.id AS id, c.city as city, c.zip_code as zipCode, c.state as state, "+
				"c.state_full_name as stateName, c.country as country, c.country_full_name as countryName "+
				"from city_auto_search c where 1=1 ";

			if(id!=null){
				SQLQuery += "AND c.id = "+id;
			}
			if(cityName != null && !cityName.isEmpty()){
				SQLQuery += " AND c.city = '"+cityName+"' "; 
			}
			session  = getStaticSession();
			SQLQuery query = session.createSQLQuery(SQLQuery);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("city", Hibernate.STRING);
			query.addScalar("zipCode", Hibernate.STRING);
			query.addScalar("state", Hibernate.STRING);
			query.addScalar("stateName", Hibernate.STRING);
			query.addScalar("country", Hibernate.STRING);
			query.addScalar("countryName", Hibernate.STRING);
			cityList = query.setResultTransformer(Transformers.aliasToBean(CityAutoSearch.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cityList;
	}*/
	
	public List<DiscountCodeTracking> getDiscountCodeTracking(String fromDate, String toDate, GridHeaderFilters filter, String pageNo){
		Session session = null;
		List<DiscountCodeTracking> discountCodeTrackingList = null;
		Integer startFrom = 0;
		try {			
			String SQLQuery = "select t.customer_id as customerId, c.cust_name as firstName, c.last_name as lastName, c.email as email, c.referrer_code as referrerCode, "+
				"isnull((select count(*) from track_discount_code_sharing where sharing_option='FACEBOOK' and customer_id=t.customer_id),0) as faceBookCnt, "+
				"isnull((select count(*) from track_discount_code_sharing where sharing_option='TWITTER' and customer_id=t.customer_id),0) as twitterCnt, "+
				"isnull((select count(*) from track_discount_code_sharing where sharing_option='LINKEDIN' and customer_id=t.customer_id),0) as linkedinCnt, "+
				"isnull((select count(*) from track_discount_code_sharing where sharing_option='WHATSAPP' and customer_id=t.customer_id),0) as whatsappCnt, "+
				"isnull((select count(*) from track_discount_code_sharing where sharing_option='ANDROIDMESSAGE' and customer_id=t.customer_id),0) as androidCnt, "+
				"isnull((select count(*) from track_discount_code_sharing where sharing_option='IMESSAGE' and customer_id=t.customer_id),0) as imessageCnt, "+
				"isnull((select count(*) from track_discount_code_sharing where sharing_option='GOOGLE' and customer_id=t.customer_id),0) as googleCnt, "+
				"isnull((select count(*) from track_discount_code_sharing where sharing_option='OUTLOOK' and customer_id=t.customer_id),0) as outlookCnt "+
				"from track_discount_code_sharing t with(nolock) "+
				"left join customer c with(nolock) on c.id = t.customer_id where c.is_test_Account<>1 ";				
			
			if(fromDate != null && !fromDate.isEmpty()){
				SQLQuery += " AND t.created_date >= '"+fromDate+"' ";
			}
			if(toDate != null && !toDate.isEmpty()){
				SQLQuery += " AND t.created_date <= '"+toDate+"' ";
			}
			if(filter.getCustomerId() != null){
				SQLQuery += " AND t.customer_id = "+filter.getCustomerId();
			}
			if(filter.getCustomerName() != null){
				SQLQuery += " AND c.cust_name like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getLastName() != null){
				SQLQuery += " AND c.last_name like '%"+filter.getLastName()+"%' ";
			}
			if(filter.getEmail() != null){
				SQLQuery += " AND c.email like '%"+filter.getEmail()+"%' ";
			}
			if(filter.getReferrerCode() != null){
				SQLQuery += " AND c.referrer_code like '%"+filter.getReferrerCode()+"%' ";
			}
			SQLQuery += " group by t.customer_id, c.cust_name, c.last_name, c.email, c.referrer_code ";
			session  = getStaticSession();
			SQLQuery query = session.createSQLQuery(SQLQuery);
			
			query.addScalar("customerId", Hibernate.INTEGER);			
			query.addScalar("firstName", Hibernate.STRING);
			query.addScalar("lastName", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("referrerCode", Hibernate.STRING);
			query.addScalar("faceBookCnt", Hibernate.INTEGER);
			query.addScalar("twitterCnt", Hibernate.INTEGER);
			query.addScalar("linkedinCnt", Hibernate.INTEGER);
			query.addScalar("whatsappCnt", Hibernate.INTEGER);
			query.addScalar("androidCnt", Hibernate.INTEGER);
			query.addScalar("imessageCnt", Hibernate.INTEGER);
			query.addScalar("googleCnt", Hibernate.INTEGER);
			query.addScalar("outlookCnt", Hibernate.INTEGER);
			
			startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
			discountCodeTrackingList = query.setResultTransformer(Transformers.aliasToBean(DiscountCodeTracking.class)).setFirstResult(startFrom).setMaxResults(PaginationUtil.PAGESIZE).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return discountCodeTrackingList;
	}
		
	public Integer getDiscountCodeTrackingCount(String fromDate, String toDate, GridHeaderFilters filter){
		Session session = null;		
		Integer count = 0;
		try {
			String SQLQuery = "select count(distinct t.customer_id) as cnt "+
			"from track_discount_code_sharing t with(nolock) "+
			"left join customer c with(nolock) on c.id = t.customer_id where c.is_test_Account<>1 ";
			
			if(fromDate != null && !fromDate.isEmpty()){
				SQLQuery += " AND t.created_date >= '"+fromDate+"' ";
			}
			if(toDate != null && !toDate.isEmpty()){
				SQLQuery += " AND t.created_date <= '"+toDate+"' ";
			}
			if(filter.getCustomerId() != null){
				SQLQuery += " AND t.customer_id = "+filter.getCustomerId();
			}
			if(filter.getCustomerName() != null){
				SQLQuery += " AND c.cust_name like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getLastName() != null){
				SQLQuery += " AND c.last_name like '%"+filter.getLastName()+"%' ";
			}
			if(filter.getEmail() != null){
				SQLQuery += " AND c.email like '%"+filter.getEmail()+"%' ";
			}
			if(filter.getReferrerCode() != null){
				SQLQuery += " AND c.referrer_code like '%"+filter.getReferrerCode()+"%' ";
			}
			
			session  = getStaticSession();
			SQLQuery query = session.createSQLQuery(SQLQuery);			
			count = Integer.parseInt(String.valueOf(query.uniqueResult()));	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}
	
	
	public List<DiscountCodeTracking> getDiscountCodeTrackingToExport(String fromDate, String toDate, GridHeaderFilters filter){
		Session session = null;
		List<DiscountCodeTracking> discountCodeTrackingList = null;
		
		try {			
			String SQLQuery = "select t.customer_id as customerId, c.cust_name as firstName, c.last_name as lastName, c.email as email, c.referrer_code as referrerCode, "+
				"isnull((select count(*) from track_discount_code_sharing where sharing_option='FACEBOOK' and customer_id=t.customer_id),0) as faceBookCnt, "+
				"isnull((select count(*) from track_discount_code_sharing where sharing_option='TWITTER' and customer_id=t.customer_id),0) as twitterCnt, "+
				"isnull((select count(*) from track_discount_code_sharing where sharing_option='LINKEDIN' and customer_id=t.customer_id),0) as linkedinCnt, "+
				"isnull((select count(*) from track_discount_code_sharing where sharing_option='WHATSAPP' and customer_id=t.customer_id),0) as whatsappCnt, "+
				"isnull((select count(*) from track_discount_code_sharing where sharing_option='ANDROIDMESSAGE' and customer_id=t.customer_id),0) as androidCnt, "+
				"isnull((select count(*) from track_discount_code_sharing where sharing_option='IMESSAGE' and customer_id=t.customer_id),0) as imessageCnt, "+
				"isnull((select count(*) from track_discount_code_sharing where sharing_option='GOOGLE' and customer_id=t.customer_id),0) as googleCnt, "+
				"isnull((select count(*) from track_discount_code_sharing where sharing_option='OUTLOOK' and customer_id=t.customer_id),0) as outlookCnt "+
				"from track_discount_code_sharing t with(nolock) "+
				"left join customer c with(nolock) on c.id = t.customer_id where c.is_test_Account<>1 ";				
					
			if(fromDate != null && !fromDate.isEmpty()){
				SQLQuery += " AND t.created_date >= '"+fromDate+"' ";
			}
			if(toDate != null && !toDate.isEmpty()){
				SQLQuery += " AND t.created_date <= '"+toDate+"' ";
			}
			if(filter.getCustomerId() != null){
				SQLQuery += " AND t.customer_id = "+filter.getCustomerId();
			}
			if(filter.getCustomerName() != null){
				SQLQuery += " AND c.cust_name like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getLastName() != null){
				SQLQuery += " AND c.last_name like '%"+filter.getLastName()+"%' ";
			}
			if(filter.getEmail() != null){
				SQLQuery += " AND c.email like '%"+filter.getEmail()+"%' ";
			}
			if(filter.getReferrerCode() != null){
				SQLQuery += " AND c.referrer_code like '%"+filter.getReferrerCode()+"%' ";
			}			
			SQLQuery += " group by t.customer_id, c.cust_name, c.last_name, c.email, c.referrer_code ";
			session  = getStaticSession();
			SQLQuery query = session.createSQLQuery(SQLQuery);
			
			query.addScalar("customerId", Hibernate.INTEGER);			
			query.addScalar("firstName", Hibernate.STRING);
			query.addScalar("lastName", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("referrerCode", Hibernate.STRING);
			query.addScalar("faceBookCnt", Hibernate.INTEGER);
			query.addScalar("twitterCnt", Hibernate.INTEGER);
			query.addScalar("linkedinCnt", Hibernate.INTEGER);
			query.addScalar("whatsappCnt", Hibernate.INTEGER);
			query.addScalar("androidCnt", Hibernate.INTEGER);
			query.addScalar("imessageCnt", Hibernate.INTEGER);
			query.addScalar("googleCnt", Hibernate.INTEGER);
			query.addScalar("outlookCnt", Hibernate.INTEGER);
						
			discountCodeTrackingList = query.setResultTransformer(Transformers.aliasToBean(DiscountCodeTracking.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return discountCodeTrackingList;
	}
	
	public boolean updateEmailBlastToMailSent(String email){
		try {
			String sql = "update email_blast set is_mail_sent=1 where email='"+email+"'";
			/*if(customer.getProductType().equals(ProductType.RTW)){
				sql += " AND customer_id="+customer.getRtwCustomerId();
				isUpdate =true;
			}else if(customer.getProductType().equals(ProductType.RTW2)){
				sql += " AND customer_id="+customer.getRtw2CustomerId();
				isUpdate =true;
			}else if(customer.getProductType().equals(ProductType.TIXCITY)){
				sql += " AND customer_id="+customer.getTixcityCustomerId();
				isUpdate =true;
			}*/
			
			SQLQuery query = getStaticSession().createSQLQuery(sql);
			query.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;		
	}
	
	public List<CityAutoSearch> getCityStateCountry(String zipCode){
		Session session = null;
		List<CityAutoSearch> cityList = null;
		try {
			String SQLQuery = "select c.id AS id, c.city as city, c.zip_code as zipCode, c.state as state, "+
				"c.state_full_name as stateName, c.country as country, cn.country_name as countryName, "+
				"cn.id as countryId, s.id as stateId "+
				"from city_auto_search c with(nolock) left join country cn with(nolock) on c.country = cn.short_desc "+
				"left join state s with(nolock) on c.state = s.short_desc and c.state_full_name = s.name where 1=1 ";
			
			if(zipCode != null && !zipCode.isEmpty()){
				SQLQuery += " AND c.zip_code = '"+zipCode+"' "; 
			}
			session  = getStaticSession();
			SQLQuery query = session.createSQLQuery(SQLQuery);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("city", Hibernate.STRING);
			query.addScalar("zipCode", Hibernate.STRING);
			query.addScalar("stateId", Hibernate.INTEGER);
			query.addScalar("state", Hibernate.STRING);
			query.addScalar("stateName", Hibernate.STRING);
			query.addScalar("countryId", Hibernate.INTEGER);
			query.addScalar("country", Hibernate.STRING);
			query.addScalar("countryName", Hibernate.STRING);
			cityList = query.setResultTransformer(Transformers.aliasToBean(CityAutoSearch.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cityList;
	}
	
	public List<ManualFedexGeneration> getManualFedexGeneration(GridHeaderFilters filter, String pageNo){
		Session session = null;
		List<ManualFedexGeneration> manualFedexList = null;
		Integer startFrom = 0;
		try {
			
			String SQLQuery = "select id as id, bl_first_name as blFirstName, bl_last_name as blLastName, "+
				"bl_company_name as blCompanyName, "+
				"bl_address1 as blAddress1, bl_address2 as blAddress2, bl_city as blCity, "+
				"bl_state as blState, bl_state_name as blStateName, bl_country as blCountry, "+
				"bl_country_name as blCountryName, bl_zipcode as blZipCode, bl_phone as blPhone, "+
				"sh_customer_name as shCustomerName, sh_company_name as shCompanyName, "+
				"sh_address1 as shAddress1, sh_address2 as shAddress2, sh_city as shCity, "+
				"sh_state as shState, sh_state_name as shStateName, sh_country as shCountry, "+
				"sh_country_name as shCountryName, sh_zipcode as shZipCode, sh_phone as shPhone, "+
				"service_type as serviceType, signature_type as signatureType, fedex_label_path as fedexLabelPath, "+ 
				"tracking_no as trackingNumber, created_date as createdDate, created_by as createdBy, "+
				"fedex_label_created as fedexLabelCreated "+
				"from manual_fedex_generation with(nolock) where 1 = 1 ";
			
			if(filter.getCustomerName() != null){
				SQLQuery += " AND bl_first_name like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getLastName() != null){
				SQLQuery += " AND bl_last_name like '%"+filter.getLastName()+"%' ";
			}
			if(filter.getCompanyName() != null){
				SQLQuery += " AND bl_company_name like '%"+filter.getCompanyName()+"%' ";
			}
			if(filter.getAddressLine1() != null){
				SQLQuery += " AND bl_address1 like '%"+filter.getAddressLine1()+"%' ";
			}
			if(filter.getAddressLine2() != null){
				SQLQuery += " AND bl_address2 like '%"+filter.getAddressLine2()+"%' ";
			}
			if(filter.getCity() != null){
				SQLQuery += " AND bl_city like '%"+filter.getCity()+"%' ";
			}
			if(filter.getState() != null){
				SQLQuery += " AND bl_state_name like '%"+filter.getState()+"%' ";
			}
			if(filter.getCountry() != null){
				SQLQuery += " AND bl_country_name like '%"+filter.getCountry()+"%' ";
			}
			if(filter.getZipCode() != null){
				SQLQuery += " AND bl_zipcode like '%"+filter.getZipCode()+"%' ";
			}
			if(filter.getPhone() != null){
				SQLQuery += " AND bl_phone like '%"+filter.getPhone()+"%' ";
			}
			if(filter.getShCustomerName() != null){
				SQLQuery += " AND sh_customer_name like '%"+filter.getShCustomerName()+"%' ";
			}
			if(filter.getShCompanyName() != null){
				SQLQuery += " AND sh_company_name like '%"+filter.getShCompanyName()+"%' ";
			}
			if(filter.getShAddressLine1() != null){
				SQLQuery += " AND sh_address1 like '%"+filter.getShAddressLine1()+"%' ";
			}
			if(filter.getShAddressLine2() != null){
				SQLQuery += " AND sh_address2 like '%"+filter.getShAddressLine2()+"%' ";
			}
			if(filter.getShCity() != null){
				SQLQuery += " AND sh_city like '%"+filter.getShCity()+"%' ";
			}
			if(filter.getShState() != null){
				SQLQuery += " AND sh_state_name like '%"+filter.getShState()+"%' ";
			}
			if(filter.getShCountry() != null){
				SQLQuery += " AND sh_country_name like '%"+filter.getShCountry()+"%' ";
			}
			if(filter.getShZipCode() != null){
				SQLQuery += " AND sh_zipcode like '%"+filter.getShZipCode()+"%' ";	
			}
			if(filter.getShPhone() != null){
				SQLQuery += " AND sh_phone like '%"+filter.getShPhone()+"%' ";	
			}
			if(filter.getServiceType() != null){
				SQLQuery += " AND service_type like '%"+filter.getServiceType()+"%' ";	
			}
			if(filter.getTrackingNo() != null){
				SQLQuery += " AND tracking_no like '%"+filter.getTrackingNo()+"%' ";	
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					SQLQuery += " AND DATEPART(day, created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					SQLQuery += " AND DATEPART(month, created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					SQLQuery += " AND DATEPART(year, created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getCreatedBy() != null){
				SQLQuery += " AND created_by like '%"+filter.getCreatedBy()+"%' ";
			}
			session  = getStaticSession();
			SQLQuery query = session.createSQLQuery(SQLQuery);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("blFirstName", Hibernate.STRING);
			query.addScalar("blLastName", Hibernate.STRING);
			query.addScalar("blCompanyName", Hibernate.STRING);
			query.addScalar("blAddress1", Hibernate.STRING);
			query.addScalar("blAddress2", Hibernate.STRING);
			query.addScalar("blCity", Hibernate.STRING);
			query.addScalar("blState", Hibernate.INTEGER);
			query.addScalar("blStateName", Hibernate.STRING);
			query.addScalar("blCountry", Hibernate.INTEGER);
			query.addScalar("blCountryName", Hibernate.STRING);
			query.addScalar("blZipCode", Hibernate.STRING);
			query.addScalar("blPhone", Hibernate.STRING);
			query.addScalar("shCustomerName", Hibernate.STRING);
			query.addScalar("shCompanyName", Hibernate.STRING);
			query.addScalar("shAddress1", Hibernate.STRING);
			query.addScalar("shAddress2", Hibernate.STRING);
			query.addScalar("shCity", Hibernate.STRING);
			query.addScalar("shState", Hibernate.INTEGER);
			query.addScalar("shStateName", Hibernate.STRING);
			query.addScalar("shCountry", Hibernate.INTEGER);
			query.addScalar("shCountryName", Hibernate.STRING);
			query.addScalar("shZipCode", Hibernate.STRING);
			query.addScalar("shPhone", Hibernate.STRING);
			query.addScalar("serviceType", Hibernate.STRING);
			query.addScalar("signatureType", Hibernate.STRING);
			query.addScalar("fedexLabelPath", Hibernate.STRING);
			query.addScalar("trackingNumber", Hibernate.STRING);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("fedexLabelCreated", Hibernate.BOOLEAN);
			startFrom = PaginationUtil.getNextPageStatFrom(pageNo);
			manualFedexList = query.setResultTransformer(Transformers.aliasToBean(ManualFedexGeneration.class)).setFirstResult(startFrom).setMaxResults(PaginationUtil.PAGESIZE).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return manualFedexList;
	}	
	
	public Integer getManualFedexGenerationCount(GridHeaderFilters filter){
		Session session = null;
		List<ManualFedexGeneration> manualFedexList = null;
		Integer count = 0;
		try {
			String SQLQuery = "select count(*) as cnt "+
				"from manual_fedex_generation with(nolock) where 1 = 1 ";
			
			if(filter.getCustomerName() != null){
				SQLQuery += " AND bl_first_name like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getLastName() != null){
				SQLQuery += " AND bl_last_name like '%"+filter.getLastName()+"%' ";
			}
			if(filter.getCompanyName() != null){
				SQLQuery += " AND bl_company_name like '%"+filter.getCompanyName()+"%' ";
			}
			if(filter.getAddressLine1() != null){
				SQLQuery += " AND bl_address1 like '%"+filter.getAddressLine1()+"%' ";
			}
			if(filter.getAddressLine2() != null){
				SQLQuery += " AND bl_address2 like '%"+filter.getAddressLine2()+"%' ";
			}
			if(filter.getCity() != null){
				SQLQuery += " AND bl_city like '%"+filter.getCity()+"%' ";
			}
			if(filter.getState() != null){
				SQLQuery += " AND bl_state_name like '%"+filter.getState()+"%' ";
			}
			if(filter.getCountry() != null){
				SQLQuery += " AND bl_country_name like '%"+filter.getCountry()+"%' ";
			}
			if(filter.getZipCode() != null){
				SQLQuery += " AND bl_zipcode like '%"+filter.getZipCode()+"%' ";
			}
			if(filter.getPhone() != null){
				SQLQuery += " AND bl_phone like '%"+filter.getPhone()+"%' ";
			}
			if(filter.getShCustomerName() != null){
				SQLQuery += " AND sh_customer_name like '%"+filter.getShCustomerName()+"%' ";
			}
			if(filter.getShCompanyName() != null){
				SQLQuery += " AND sh_company_name like '%"+filter.getShCompanyName()+"%' ";
			}
			if(filter.getShAddressLine1() != null){
				SQLQuery += " AND sh_address1 like '%"+filter.getShAddressLine1()+"%' ";
			}
			if(filter.getShAddressLine2() != null){
				SQLQuery += " AND sh_address2 like '%"+filter.getShAddressLine2()+"%' ";
			}
			if(filter.getShCity() != null){
				SQLQuery += " AND sh_city like '%"+filter.getShCity()+"%' ";
			}
			if(filter.getShState() != null){
				SQLQuery += " AND sh_state_name like '%"+filter.getShState()+"%' ";
			}
			if(filter.getShCountry() != null){
				SQLQuery += " AND sh_country_name like '%"+filter.getShCountry()+"%' ";
			}
			if(filter.getShZipCode() != null){
				SQLQuery += " AND sh_zipcode like '%"+filter.getShZipCode()+"%' ";	
			}
			if(filter.getShPhone() != null){
				SQLQuery += " AND sh_phone like '%"+filter.getShPhone()+"%' ";	
			}
			if(filter.getServiceType() != null){
				SQLQuery += " AND service_type like '%"+filter.getServiceType()+"%' ";	
			}
			if(filter.getTrackingNo() != null){
				SQLQuery += " AND tracking_no like '%"+filter.getTrackingNo()+"%' ";	
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					SQLQuery += " AND DATEPART(day, created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					SQLQuery += " AND DATEPART(month, created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					SQLQuery += " AND DATEPART(year, created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getCreatedBy() != null){
				SQLQuery += " AND created_by like '%"+filter.getCreatedBy()+"%' ";
			}
			
			session  = getStaticSession();
			SQLQuery query = session.createSQLQuery(SQLQuery);			
			count = Integer.parseInt(String.valueOf(query.uniqueResult()));	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}	
	
	public List<ManualFedexGeneration> getManualFedexGenerationToExport(GridHeaderFilters filter){
		Session session = null;
		List<ManualFedexGeneration> manualFedexList = null;
		try {
			
			String SQLQuery = "select id as id, bl_first_name as blFirstName, bl_last_name as blLastName, "+
				"bl_company_name as blCompanyName, "+
				"bl_address1 as blAddress1, bl_address2 as blAddress2, bl_city as blCity, "+
				"bl_state as blState, bl_state_name as blStateName, bl_country as blCountry, "+
				"bl_country_name as blCountryName, bl_zipcode as blZipCode, bl_phone as blPhone, "+
				"sh_customer_name as shCustomerName, sh_company_name as shCompanyName, "+
				"sh_address1 as shAddress1, sh_address2 as shAddress2, sh_city as shCity, "+
				"sh_state as shState, sh_state_name as shStateName, sh_country as shCountry, "+
				"sh_country_name as shCountryName, sh_zipcode as shZipCode, sh_phone as shPhone, "+
				"service_type as serviceType, signature_type as signatureType, fedex_label_path as fedexLabelPath, "+ 
				"tracking_no as trackingNumber, created_date as createdDate, created_by as createdBy, "+
				"fedex_label_created as fedexLabelCreated "+
				"from manual_fedex_generation with(nolock) where 1 = 1 ";
			
			if(filter.getCustomerName() != null){
				SQLQuery += " AND bl_first_name like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getLastName() != null){
				SQLQuery += " AND bl_last_name like '%"+filter.getLastName()+"%' ";
			}
			if(filter.getCompanyName() != null){
				SQLQuery += " AND bl_company_name like '%"+filter.getCompanyName()+"%' ";
			}
			if(filter.getAddressLine1() != null){
				SQLQuery += " AND bl_address1 like '%"+filter.getAddressLine1()+"%' ";
			}
			if(filter.getAddressLine2() != null){
				SQLQuery += " AND bl_address2 like '%"+filter.getAddressLine2()+"%' ";
			}
			if(filter.getCity() != null){
				SQLQuery += " AND bl_city like '%"+filter.getCity()+"%' ";
			}
			if(filter.getState() != null){
				SQLQuery += " AND bl_state_name like '%"+filter.getState()+"%' ";
			}
			if(filter.getCountry() != null){
				SQLQuery += " AND bl_country_name like '%"+filter.getCountry()+"%' ";
			}
			if(filter.getZipCode() != null){
				SQLQuery += " AND bl_zipcode like '%"+filter.getZipCode()+"%' ";
			}
			if(filter.getPhone() != null){
				SQLQuery += " AND bl_phone like '%"+filter.getPhone()+"%' ";
			}
			if(filter.getShCustomerName() != null){
				SQLQuery += " AND sh_customer_name like '%"+filter.getShCustomerName()+"%' ";
			}
			if(filter.getShCompanyName() != null){
				SQLQuery += " AND sh_company_name like '%"+filter.getShCompanyName()+"%' ";
			}
			if(filter.getShAddressLine1() != null){
				SQLQuery += " AND sh_address1 like '%"+filter.getShAddressLine1()+"%' ";
			}
			if(filter.getShAddressLine2() != null){
				SQLQuery += " AND sh_address2 like '%"+filter.getShAddressLine2()+"%' ";
			}
			if(filter.getShCity() != null){
				SQLQuery += " AND sh_city like '%"+filter.getShCity()+"%' ";
			}
			if(filter.getShState() != null){
				SQLQuery += " AND sh_state_name like '%"+filter.getShState()+"%' ";
			}
			if(filter.getShCountry() != null){
				SQLQuery += " AND sh_country_name like '%"+filter.getShCountry()+"%' ";
			}
			if(filter.getShZipCode() != null){
				SQLQuery += " AND sh_zipcode like '%"+filter.getShZipCode()+"%' ";	
			}
			if(filter.getShPhone() != null){
				SQLQuery += " AND sh_phone like '%"+filter.getShPhone()+"%' ";	
			}
			if(filter.getServiceType() != null){
				SQLQuery += " AND service_type like '%"+filter.getServiceType()+"%' ";	
			}
			if(filter.getTrackingNo() != null){
				SQLQuery += " AND tracking_no like '%"+filter.getTrackingNo()+"%' ";	
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					SQLQuery += " AND DATEPART(day, created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					SQLQuery += " AND DATEPART(month, created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					SQLQuery += " AND DATEPART(year, created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getCreatedBy() != null){
				SQLQuery += " AND created_by like '%"+filter.getCreatedBy()+"%' ";
			}
			session  = getStaticSession();
			SQLQuery query = session.createSQLQuery(SQLQuery);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("blFirstName", Hibernate.STRING);
			query.addScalar("blLastName", Hibernate.STRING);
			query.addScalar("blCompanyName", Hibernate.STRING);
			query.addScalar("blAddress1", Hibernate.STRING);
			query.addScalar("blAddress2", Hibernate.STRING);
			query.addScalar("blCity", Hibernate.STRING);
			query.addScalar("blState", Hibernate.INTEGER);
			query.addScalar("blStateName", Hibernate.STRING);
			query.addScalar("blCountry", Hibernate.INTEGER);
			query.addScalar("blCountryName", Hibernate.STRING);
			query.addScalar("blZipCode", Hibernate.STRING);
			query.addScalar("blPhone", Hibernate.STRING);
			query.addScalar("shCustomerName", Hibernate.STRING);
			query.addScalar("shCompanyName", Hibernate.STRING);
			query.addScalar("shAddress1", Hibernate.STRING);
			query.addScalar("shAddress2", Hibernate.STRING);
			query.addScalar("shCity", Hibernate.STRING);
			query.addScalar("shState", Hibernate.INTEGER);
			query.addScalar("shStateName", Hibernate.STRING);
			query.addScalar("shCountry", Hibernate.INTEGER);
			query.addScalar("shCountryName", Hibernate.STRING);
			query.addScalar("shZipCode", Hibernate.STRING);
			query.addScalar("shPhone", Hibernate.STRING);
			query.addScalar("serviceType", Hibernate.STRING);
			query.addScalar("signatureType", Hibernate.STRING);
			query.addScalar("fedexLabelPath", Hibernate.STRING);
			query.addScalar("trackingNumber", Hibernate.STRING);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("fedexLabelCreated", Hibernate.BOOLEAN);
			
			manualFedexList = query.setResultTransformer(Transformers.aliasToBean(ManualFedexGeneration.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return manualFedexList;
	}
	
	public List<String> getArtistCities(Integer artistId){
		Session session = null;
		List<String> cities = new ArrayList<String>();
		try{
			session = getStaticSession();
			String queryStr = "select distinct ed.city,ed.state,ed.country from event_details ed with(nolock) " +
					"join event_artist_details ead with(nolock) on ed.event_id=ead.event_id " +
					"where ed.status=1 and ed.country <> 'UK' and ead.artist_id="+artistId;
			session  = sessionFactory.openSession();
			SQLQuery query = session.createSQLQuery(queryStr);
			List<Object[]> list =   query.list();
			if(list!=null && !list.isEmpty()){
				String city = null;
				for(Object[] obj : list){
					city = obj[0]+","+obj[1]+","+obj[2];
					cities.add(city);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return cities;
	}
	
	public List<Object[]> getTNDEventSalesDetailReport() {
		List<Object[]> eventSales = null;
		Session session = null;
		try {
			String SQLQuery = "select EventId, EventName, EventDateStr, EventTimeStr, "+
				"Venue, City, State, Country, SalesCount, "+
				"RecentSaleDateStr "+ 
				"from rep_EVENTS_SALES_IN_LAST_THREE_DAYS_report_qry_vw with(nolock) "+
				"order by LatestOrderDate ";
			
			session = sessionFactory.openSession();
			Query query = session.createSQLQuery(SQLQuery);
			eventSales = query.list();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return eventSales;
	}
	
	
	public List<RTFAPITracker> getRtfApiLastUpdatedDate() {
		Session session = null;
		try {
			String SQLQuery = "select id as id,last_check as lastUpdated from rtf_api_running_status";
			session  = getStaticSession();
			SQLQuery query = session.createSQLQuery(SQLQuery);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("lastUpdated", Hibernate.TIMESTAMP);
			List<RTFAPITracker> tracker = query.setResultTransformer(Transformers.aliasToBean(RTFAPITracker.class)).list();
			return tracker;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Customer> getContestCustomers(String contestId){
		Session session = null;
		List<Customer> customers = null;
		try {
			String SQLQuery = "select distinct c.cust_name AS customerName,c.last_name AS lastName,c.user_id AS userId,c.email AS email from web_service_tracking_new wt join customer c "+ 
					"on wt.customer_id=c.id where wt.api_name like 'QUIZJOINCONTEST' and wt.contest_id='"+contestId+"' ";
			
			session  = getStaticSession();
			SQLQuery query = session.createSQLQuery(SQLQuery);
			query.addScalar("customerName", Hibernate.STRING);
			query.addScalar("lastName", Hibernate.STRING);
			query.addScalar("userId", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			
			customers = query.setResultTransformer(Transformers.aliasToBean(Customer.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return customers;
	}

	
	
	
	public Boolean removeTicketMasterRefundData(){
		Session session = null;
		Integer isDeleted = 0;
		try {
			String sql ="insert into ZZZ_tm_refunds_mstr(InvoiceDate,TicketCost,POId,NameOnCC,PODate,Vendor,Event,EventDate,Venue,"+
					" TicketQty,RefundQty,Section,Row,InternalTicketNotes,email,OLDemail,created_date,ErrorsFixedbyHand)"+
					" select InvoiceDate,TicketCost,POId,NameOnCC,PODate,Vendor,Event,EventDate,Venue,TicketQty,RefundQty,"+
					" Section,Row,InternalTicketNotes,email,OLDemail,created_date,ErrorsFixedbyHand from ZZZ_tmp_tm_refunds";
			session  = getStaticSession();
			SQLQuery query = session.createSQLQuery(sql);
			isDeleted = query.executeUpdate();
			if(isDeleted > 0){
				String SQLQuery = "truncate TABLE ZZZ_tmp_tm_refunds";
				
				session  = getStaticSession();
				SQLQuery query1 = session.createSQLQuery(SQLQuery);
				isDeleted = query1.executeUpdate();
				if(isDeleted > 0){
					return true;
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	
	
	public Boolean removeTicketMasterRefundKnownEmailData(){
		Session session = null;
		Integer isDeleted = 0;
		try {
			String sql ="INSERT INTO ZZZ_tmp_tm_refunds_KNOWN_emails_upload_history "+ "(id,name,checkEmail,nameCategory,InvoiceDate,RefundDue,RefundReceived,POId,NameOnCC,PODate,Vendor, "+
					"Event,EventDate,Venue,TicketQty,RefundQty,RefundCost,Section,Row,InternalTicketNotes,email,OLDemail, "+
					"ErrorsFixedbyHand,isMapped,MergedRecs) "+
					"select id,name,checkEmail,nameCategory,InvoiceDate,RefundDue,RefundReceived,POId,NameOnCC,PODate,Vendor, "+
					"Event,EventDate,Venue,TicketQty,RefundQty,RefundCost,Section,Row,InternalTicketNotes,email,OLDemail, "+
					"ErrorsFixedbyHand,isMapped,MergedRecs FROM ZZZ_tmp_tm_refunds_KNOWN_emails_upload ";
			session  = getStaticSession();
			SQLQuery query = session.createSQLQuery(sql);
			isDeleted = query.executeUpdate();
			if(isDeleted > 0){
				String SQLQuery = "truncate table ZZZ_tmp_tm_refunds_KNOWN_emails_upload";
				
				session  = getStaticSession();
				SQLQuery query1 = session.createSQLQuery(SQLQuery);
				isDeleted = query1.executeUpdate();
				if(isDeleted > 0){
					return true;
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	
	public Boolean removeTicketMasterRefundCCNotMatched(){
		Session session = null;
		Integer isDeleted = 0;
		try {
			String SQLQuery = "truncate table ZZZ_tmp_tm_refunds_CC_Not_Matched";
			
			session  = getStaticSession();
			SQLQuery query1 = session.createSQLQuery(SQLQuery);
			isDeleted = query1.executeUpdate();
			if(isDeleted > 0){
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	
	public List<RtfConfigContestClusterNodes> getRtfContestNodesUrls(){
		Session session = null;
		List<RtfConfigContestClusterNodes> urls = null;
		try {
			String SQLQuery = "select id as urlId,url as url,status as status from rtf_conf_contest_cluster_nodes where status=1 order by url";
			
			session  = getStaticSession();
			SQLQuery query = session.createSQLQuery(SQLQuery);
			query.addScalar("urlId", Hibernate.INTEGER);
			query.addScalar("url", Hibernate.STRING);
			query.addScalar("status", Hibernate.INTEGER);
			
			urls = query.setResultTransformer(Transformers.aliasToBean(RtfConfigContestClusterNodes.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return urls;
	}
	public List<TicketMasterRefund> getTmPurchaseRefundsAmounts() throws Exception {
		Session session = null;
		List<TicketMasterRefund> tgGroups = new ArrayList<TicketMasterRefund>();
		try{
			
			session = sessionFactory.openSession();
			PreparedStatement st = session.connection().prepareStatement("{call sp_zzz_tm_refunds}");
			ResultSet resultSet = st.executeQuery();
			System.out.println(resultSet.getFetchSize());
			TicketMasterRefund tg = null;
			while(resultSet.next()){
				tg = new TicketMasterRefund();
				tg.setCcName(resultSet.getString("Name"));
				tg.setRefundCost(resultSet.getDouble("Total Cost"));
				tg.setVenue(resultSet.getString("Title"));
				tgGroups.add(tg);
			}
			return tgGroups;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
	}
	
	public Boolean reconcileRefundData() throws Exception {
		Session session = null;
		try{
			session = sessionFactory.openSession();
			PreparedStatement st = session.connection().prepareStatement("{call sp_zzz_tm_refunds_received_mapped}");
			st.execute();
			/*if(resultSet.getInt(0) == 0){
				return true;
			}else{
				return false;
			}*/
			return true;
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return false;
	}
	
	
	public List<TicketMasterRefund> getTmPurchaseRefundsEmailList() throws Exception {
		Session session = null;
		List<TicketMasterRefund> tgGroups = new ArrayList<TicketMasterRefund>();
		try{
			
			session = sessionFactory.openSession();
			PreparedStatement st = session.connection().prepareStatement("{call sp_zzz_tmpCCLISTS}");
			ResultSet resultSet = st.executeQuery();
			System.out.println(resultSet.getFetchSize());
			TicketMasterRefund tg = null;
			while(resultSet.next()){
				tg = new TicketMasterRefund();
				tg.setCcName(resultSet.getString("name"));
				tg.setEmail(resultSet.getString("email"));
				tgGroups.add(tg);
			}
			return tgGroups;
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return tgGroups;
	}
	public List<TicketMasterRefund> getTmPurchaseRefundsUnknownData() throws Exception {
		Session session = null;
		List<TicketMasterRefund> list = new ArrayList<TicketMasterRefund>();
		try{
			
			session = sessionFactory.openSession();
			PreparedStatement st = session.connection().prepareStatement("{call sp_zzz_tm_refunds_unknown}");
			ResultSet resultSet = st.executeQuery();
			System.out.println(resultSet.getFetchSize());
			TicketMasterRefund obj = null;
			//InvoiceDate,TicketCost,PoId,NameOnCC,PODate,Vendor,Event,EventDate,Venue,TicketQty,RefundQty,RefundCost,Section,Row,InternalTicketNotes,email,OLDemail,name

			while(resultSet.next()){
				obj = new TicketMasterRefund();
				obj.setInvoiceDate(resultSet.getString("InvoiceDate"));
				obj.setTicketCost(resultSet.getDouble("TicketCost"));
				obj.setPoId(resultSet.getString("PoId"));
				obj.setCcName(resultSet.getString("NameOnCC"));
				obj.setPoDate(resultSet.getString("PODate"));
				obj.setVendor(resultSet.getString("Vendor"));
				obj.setEventName(resultSet.getString("Event"));
				obj.setEventDate(resultSet.getString("EventDate"));
				obj.setVenue(resultSet.getString("Venue"));
				obj.setTicketQty(resultSet.getInt("TicketQty"));
				obj.setRefundQty(resultSet.getInt("RefundQty"));
				obj.setRefundCost(resultSet.getDouble("RefundCost"));
				obj.setSection(resultSet.getString("Section"));
				obj.setRow(resultSet.getString("Row"));
				obj.setInternalNotes(resultSet.getString("InternalTicketNotes"));
				obj.setEmail(resultSet.getString("email"));
				obj.setOldEmail(resultSet.getString("OLDemail"));
				obj.setError(resultSet.getString("ErrorsFixedbyHand"));
				//ErrorsFixedbyHand
				list.add(obj);
			}
			return list;
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return list;
	}
	public Boolean saveTmCcHoldersRefunds(List<TicketMasterRefund> list,Date processDate)throws Exception {
		Session session = null;
		Integer isDeleted = 0;
		try {
			
			session  = getStaticSession();
			String SQLQuery = "delete from ZZZ_tm_cc_holders_refunds where processed_date=?";
			
			session  = getStaticSession();
			PreparedStatement st = session.connection().prepareStatement(SQLQuery);
			st.setDate(1, new java.sql.Date(new Date().getTime()));
			isDeleted = st.executeUpdate();
			//if(isDeleted > 0){
				//return true;
			//}
			
			String sql ="insert into ZZZ_tm_cc_holders_refunds(name,refund_amount,title,processed_date,created_date)"+
					" values(?,?,?,?,getdate())";
		
			 st = session.connection().prepareStatement(sql);
			
			for (TicketMasterRefund ticketMasterRefund : list) {
				st.setString(1, ticketMasterRefund.getCcName());
				st.setDouble(2, ticketMasterRefund.getRefundCost());
				st.setString(3, ticketMasterRefund.getVenue());
				//st.setTimestamp(4, new Timestamp(processDate.getTime()));
				st.setDate(4, new java.sql.Date(processDate.getTime()));
				st.addBatch();
			}
			int[] updCnt = st.executeBatch();
			System.out.println("update count ZZZ_tm_cc_holders_refunds : "+updCnt.length);
			
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return false;
	}
	public List<TicketMasterRefund> getAllTmCcHoldersRefunds() throws Exception {
		Session session = null;
		List<TicketMasterRefund> list = new ArrayList<TicketMasterRefund>();
		try{
			
			session = sessionFactory.openSession();
			PreparedStatement st = session.connection().prepareStatement("select * from  ZZZ_tm_cc_holders_refunds order by title,processed_date,name");
			ResultSet resultSet = st.executeQuery();
			System.out.println(resultSet.getFetchSize());
			TicketMasterRefund obj = null;
			//InvoiceDate,TicketCost,PoId,NameOnCC,PODate,Vendor,Event,EventDate,Venue,TicketQty,RefundQty,RefundCost,Section,Row,InternalTicketNotes,email,OLDemail,name

			while(resultSet.next()){
				obj = new TicketMasterRefund();
				obj.setCcName(resultSet.getString("name"));
				obj.setRefundCost(resultSet.getDouble("refund_amount"));
				obj.setVenue(resultSet.getString("title"));
				obj.setProcessdDate(new Date(resultSet.getDate("processed_date").getTime()));
				
				
				list.add(obj);
			}
			return list;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
	}
	public List<TicketMasterRefund> getTmDuplicateRefundsData() throws Exception {
		Session session = null;
		List<TicketMasterRefund> list = new ArrayList<TicketMasterRefund>();
		try{
			
			session = sessionFactory.openSession();
			PreparedStatement st = session.connection().prepareStatement("{call sp_zzz_tm_refunds_duplicate}");
			ResultSet resultSet = st.executeQuery();
			System.out.println(resultSet.getFetchSize());
			TicketMasterRefund obj = null;
			//InvoiceDate,TicketCost,PoId,NameOnCC,PODate,Vendor,Event,EventDate,Venue,TicketQty,RefundQty,RefundCost,Section,Row,InternalTicketNotes,email,OLDemail,name

			while(resultSet.next()){
				obj = new TicketMasterRefund();
				obj.setInvoiceDate(resultSet.getString("InvoiceDate"));
				obj.setTicketCost(resultSet.getDouble("TicketCost"));
				obj.setPoId(resultSet.getString("PoId"));
				obj.setCcName(resultSet.getString("NameOnCC"));
				obj.setPoDate(resultSet.getString("PODate"));
				obj.setVendor(resultSet.getString("Vendor"));
				obj.setEventName(resultSet.getString("Event"));
				obj.setEventDate(resultSet.getString("EventDate"));
				obj.setVenue(resultSet.getString("Venue"));
				obj.setTicketQty(resultSet.getInt("TicketQty"));
				obj.setRefundQty(resultSet.getInt("RefundQty"));
				obj.setRefundCost(resultSet.getDouble("RefundCost"));
				obj.setSection(resultSet.getString("Section"));
				obj.setRow(resultSet.getString("Row"));
				obj.setInternalNotes(resultSet.getString("InternalTicketNotes"));
				obj.setEmail(resultSet.getString("email"));
				obj.setOldEmail(resultSet.getString("OLDemail"));
				obj.setError(resultSet.getString("ErrorsFixedbyHand"));
				obj.setNoOfDuplicates(resultSet.getInt("No. Of Duplicates"));
				//ErrorsFixedbyHand
				list.add(obj);
			}
			return list;
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return list;
	}
}