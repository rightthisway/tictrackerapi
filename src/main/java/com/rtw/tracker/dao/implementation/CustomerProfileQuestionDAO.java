package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.datas.CustomerProfileQuestion;
import com.rtw.tracker.utils.GridHeaderFilters;

public class CustomerProfileQuestionDAO extends HibernateDAO<Integer, CustomerProfileQuestion> implements com.rtw.tracker.dao.services.CustomerProfileQuestionDAO{

	
	private static Session custProfileSession = null;;
	
	public static Session getCustProfileSession() {
		return CustomerProfileQuestionDAO.custProfileSession;
	}
	public static void setCustProfileSession(Session custProfileSession) {
		CustomerProfileQuestionDAO.custProfileSession = custProfileSession;
	}




	@Override
	public List<CustomerProfileQuestion> getAllCustomerQuestionByStatus(GridHeaderFilters filter, String status) {
		List<CustomerProfileQuestion> list = null;;
		try {
			String sql = "select id as id, question as question, qsl_no as serialNo, profile_type as profileType, points as points,"+
								"status as status, created_by as createdBy, updated_by as updatedBy, created_date as createdDate,"+
								"update_date as updatedDate from rtf_cust_profile_questions where status LIKE '"+status+"' ";
			if(filter.getText1()!=null && !filter.getText1().isEmpty()){
				sql += " AND question like '%"+filter.getText1()+"%' ";
			}
			if(filter.getCount()!=null ){
				sql += " AND qsl_no ="+ filter.getCount();
			}
			if(filter.getText2()!=null && !filter.getText2().isEmpty()){
				sql += " AND profile_type like '%"+filter.getText2()+"%' ";
			}
			if(filter.getWinnerCount()!=null){
				sql += " AND points ="+filter.getWinnerCount();
			}
			if(filter.getStatus()!=null && !filter.getStatus().isEmpty()){
				sql += " AND status like '%"+filter.getStatus()+"%' ";
			}
			if(filter.getActionType()!=null && !filter.getActionType().isEmpty()){
				sql += " AND question_type like '%"+filter.getActionType()+"%' ";
			}
			if(filter.getCreatedBy()!=null && !filter.getCreatedBy().isEmpty()){
				sql += " AND created_by like '"+filter.getCreatedBy()+"' ";
			}
			if(filter.getLastUpdatedBy()!=null && !filter.getLastUpdatedBy().isEmpty()){
				sql += " AND updated_by like '"+filter.getLastUpdatedBy()+"' ";
			}
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day, created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month, created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year, created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getUpdatedDate()!=null ){
				if(Util.extractDateElement(filter.getUpdatedDate(),"DAY") > 0){
					sql += " AND DATEPART(day,update_date) = "+Util.extractDateElement(filter.getUpdatedDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"MONTH") > 0){
					sql += " AND DATEPART(month, update_date) = "+Util.extractDateElement(filter.getUpdatedDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"YEAR") > 0){
					sql += " AND DATEPART(year, update_date) = "+Util.extractDateElement(filter.getUpdatedDate(),"YEAR")+" ";
				}
			}
			
			sql += " ORDER BY profile_type,qsl_no ASC";
			
			custProfileSession = CustomerProfileQuestionDAO.getCustProfileSession();
			if(custProfileSession==null || !custProfileSession.isOpen() || !custProfileSession.isConnected()){
				custProfileSession = getSession();
				CustomerProfileQuestionDAO.setCustProfileSession(custProfileSession);
			}
			SQLQuery query = custProfileSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("question", Hibernate.STRING);
			query.addScalar("serialNo", Hibernate.INTEGER);
			query.addScalar("profileType", Hibernate.STRING);
			query.addScalar("points", Hibernate.INTEGER);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("updatedBy", Hibernate.STRING);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);
			
			
			list = query.setResultTransformer(Transformers.aliasToBean(CustomerProfileQuestion.class)).list();
			
		} catch (Exception e) {
			if(custProfileSession != null && custProfileSession.isOpen()){
				custProfileSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	@Override
	public CustomerProfileQuestion getLastQuestion() {
		List<CustomerProfileQuestion> list =  find("FROM CustomerProfileQuestion WHERE profileType like 'FANDOM' and status like 'ACTIVE' ORDER BY serialNo");
		if(list.isEmpty()){
			return null;
		}
		return list.get(list.size()-1);
	}
	@Override
	public CustomerProfileQuestion getQuestionBySerialNo(Integer serialNo) {
		return findSingle("FROM CustomerProfileQuestion WHERE profileType like 'FANDOM' and status like 'ACTIVE' and serialNo=? ", new Object[]{serialNo});
	}

}
