package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.datas.FanClubPost;
import com.rtw.tracker.utils.GridHeaderFilters;

public class FanClubPostDAO extends HibernateDAO<Integer, FanClubPost> implements com.rtw.tracker.dao.services.FanClubPostDAO{

	
	private static Session fanClubSession;
	
	public static Session getFanClubSession() {
		return fanClubSession;
	}
	public static void setFanClubSession(Session fanClubSession) {
		FanClubPostDAO.fanClubSession = fanClubSession;
	}
	@Override
	public List<FanClubPost> getAllFanClubPosts(GridHeaderFilters filter, String status, Integer fanClubId,SharedProperty prop) {
		List<FanClubPost> list =null;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT fp.id as id, fp.fanclub_mst_id as fanClubId, fp.customer_id as customerId, fp.posts_text as postText,");
			sql.append("fp.posts_image_url as imageUrl, fp.posts_video_url as videoUrl, fp.posts_video_thumbnail_url as thumbnailUrl,");
			sql.append("fp.created_user_id as createdBy, fp.updated_user_id as updatedBy, fp.no_of_likes as likeCount,");
			sql.append("fp.posts_status as status, fp.created_date as createdDate, fp.updated_date as updatedDate,");
			sql.append("fp.created_updated_reason as updatReason,c.user_id as userId, c.email as email, c.phone as phone ");
			sql.append("FROM fanclub_posts fp with(nolock) ");
			sql.append("left join "+prop.getDatabasAlias()+"customer c with(nolock)  on fp.customer_id = c.id ");
			sql.append("WHERE fp.fanclub_mst_id ="+fanClubId);
			
			
			if(filter.getArtistName()!=null && !filter.getArtistName().isEmpty()){
				sql.append(" AND fp.posts_text like '%"+filter.getArtistName()+"%' ");
			}
			if(filter.getCreatedBy()!=null && !filter.getCreatedBy().isEmpty()){
				sql.append(" AND fp.created_user_id  like '%"+filter.getCreatedBy()+"%' ");
			}
			if(filter.getUpdatedBy()!=null && !filter.getUpdatedBy().isEmpty()){
				sql.append(" AND fp.updated_user_id  like '%"+filter.getUpdatedBy()+"%' ");
			}
			if(filter.getWinnerCount()!=null){
				sql.append(" AND fp.no_of_likes ="+filter.getWinnerCount());
			}
			if(filter.getStatus()!=null && !filter.getStatus().isEmpty()){
				sql.append(" AND fp.posts_status  like '%"+filter.getStatus()+"%' ");
			}
			if(filter.getUsername()!=null && !filter.getUsername().isEmpty()){
				sql.append(" AND c.user_id like '%"+filter.getUsername()+"%' ");
			}
			if(filter.getEmail()!=null && !filter.getEmail().isEmpty()){
				sql.append(" AND c.email like '%"+filter.getEmail()+"%' ");
			}
			if(filter.getPhone()!=null && !filter.getPhone().isEmpty()){
				sql.append(" AND c.phone like '%"+filter.getPhone()+"%' ");
			}
			if(filter.getText1()!=null && !filter.getText1().isEmpty()){
				sql.append(" AND fp.created_updated_reason like '%"+filter.getText1()+"%' ");
			}
			
			
			
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql.append(" AND DATEPART(day, fp.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql.append(" AND DATEPART(month, fp.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql.append(" AND DATEPART(year, fp.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ");
				}
			}
			
			if(filter.getLastUpdatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
					sql.append(" AND DATEPART(day, fp.updated_date) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql.append(" AND DATEPART(month, fp.updated_date) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ");
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql.append(" AND DATEPART(year, fp.updated_date) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ");
				}
			}
			
			fanClubSession = FanClubPostDAO.getFanClubSession();
			if(fanClubSession==null || !fanClubSession.isOpen() || !fanClubSession.isConnected()){
				fanClubSession = getSession();
				FanClubPostDAO.setFanClubSession(fanClubSession);
			}
			SQLQuery query = fanClubSession.createSQLQuery(sql.toString());
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("fanClubId", Hibernate.INTEGER);
			query.addScalar("postText", Hibernate.STRING);
			query.addScalar("customerId", Hibernate.INTEGER);
			query.addScalar("imageUrl", Hibernate.STRING);
			query.addScalar("videoUrl", Hibernate.STRING);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("updatedBy", Hibernate.STRING);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("thumbnailUrl", Hibernate.STRING);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);
			query.addScalar("userId", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("phone", Hibernate.STRING);
			query.addScalar("likeCount", Hibernate.INTEGER);
			query.addScalar("updatReason", Hibernate.STRING);
			
			
			list = query.setResultTransformer(Transformers.aliasToBean(FanClubPost.class)).list();
		} catch (Exception e) {
			if(fanClubSession != null && fanClubSession.isOpen()){
				fanClubSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
}
