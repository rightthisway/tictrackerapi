package com.rtw.tracker.dao.implementation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.datas.EventDetails;
import com.rtw.tracker.datas.EventStatus;
import com.rtw.tracker.datas.ProductType;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.PaginationUtil;

public class EventDetailsDAO extends HibernateDAO<Integer, EventDetails> implements com.rtw.tracker.dao.services.EventDetailsDAO {


	public EventDetails getEventById(Integer evnetId) throws Exception {
		return findSingle("FROM EventDetails WHERE eventId=? ", new Object[]{evnetId});
	}
	
	public Collection<EventDetails> getAllActiveEvents() throws Exception {
		return find("FROM EventDetails WHERE eventStatus=? ORDER BY event_name, event_date, event_time Asc", new Object[]{EventStatus.ACTIVE});
	}
	
	public Collection<EventDetails> getAllActiveEventsByFilter(String searchColumn,String searchValue,Date fromDate,Date toDate,ProductType productType) throws Exception {
		String query = "FROM EventDetails WHERE eventStatus=? ";
		List<Object> paramList = new ArrayList<Object>();
		
		paramList.add(EventStatus.ACTIVE);
		
		if(fromDate != null && toDate != null) {
			query += " AND eventDate>=? AND eventDate<=? ";
			paramList.add(fromDate);
			paramList.add(toDate);
		}
		
		if(searchColumn !=null && !searchColumn.isEmpty()){
			
			query += " AND "+searchColumn +" like ?  ";
			paramList.add("%"+searchValue+"%");
		}
		if(productType!=null){
			if(productType == ProductType.LASTROWMINICATS){
				query += " AND isLastRowMinicatsEnabled=?";
			}else if(productType == ProductType.REWARDTHEFAN){
				query += " AND isRewardTheFanEnabled=?";
			}else if(productType == ProductType.PRESALEZONETICKETS){
				query += " AND isPresaleZoneTicketEnabled=?";
			}else if(productType == ProductType.ZONETICKETS){
				query += " AND isCategoryTicketEnabled=?";
			}else if(productType == ProductType.VIPMINICATS){
				query += " AND isVipMinicatsEnabled=?";
			}
			paramList.add(true);
		}
		
		return find(query+" ORDER BY event_date, event_time Asc", paramList.toArray());
		
	}
	
	public Collection<EventDetails> getAllActiveArtistsByFilter(GridHeaderFilters filter,String pageNo) throws Exception {
		Integer firstResult = 0;
		String queryStr = "select distinct a.id as 'artistId',REPLACE(a.name,'\"','') as 'artistName',gc.name as 'grandChildCategoryName'," +
		" gc.id as 'grandChildCategoryId',cc.id as 'childCategoryId',cc.name as 'childCategoryName'," +
		" pc.id as 'parentCategoryId',pc.name as 'parentCategoryName',a.display_on_search AS displayOnSearch" +
		" FROM artist a with(nolock) " +
		" inner join artist_category_mapping ac with(nolock) on ac.artist_id=a.id"+
		" inner join grand_child_category gc with(nolock) on gc.id=ac.grand_child_category_id" +
		" inner join child_category cc with(nolock) on cc.id=ac.child_category_id " +
		" inner join parent_category pc with(nolock) on pc.id=ac.parent_category_id where a.artist_status=1 ";
		List<Object> paramList = new ArrayList<Object>();
		
			if(filter.getArtistName()!=null) {
				queryStr += " AND a.name like '%"+filter.getArtistName()+"%'";
			}
			if(filter.getGrandChildCategoryName()!=null) {
				queryStr += " AND gc.name like '%"+filter.getGrandChildCategoryName()+"%'";
			}
			if(filter.getChildCategoryName()!=null) {
				queryStr += " AND cc.name like '%"+filter.getChildCategoryName()+"%'";
			}
			if(filter.getParentCategoryName()!=null) {
				queryStr += " AND pc.name like '%"+filter.getParentCategoryName()+"%'";
			}
			if(filter.getVisibleOnSearch()!=null) {
				queryStr += " AND a.display_on_search="+filter.getVisibleOnSearch();
			}
		Query query = getSession().createSQLQuery(queryStr);
		firstResult = PaginationUtil.getNextPageStatFrom(pageNo);
		List<EventDetails> list = query.setResultTransformer(Transformers.aliasToBean(EventDetails.class)).setFirstResult(firstResult).setMaxResults(PaginationUtil.PAGESIZE).list();;
		return list;
		
	}
	
	public Integer getAllActiveArtistsTotalCount(GridHeaderFilters filter) throws Exception {
		Integer count = 0;
		String queryStr = "select count(*) "+
		" FROM artist a with(nolock) " +
		" inner join artist_category_mapping ac with(nolock) on ac.artist_id=a.id"+
		" inner join grand_child_category gc with(nolock) on gc.id=ac.grand_child_category_id" +
		" inner join child_category cc with(nolock) on cc.id=ac.child_category_id " +
		" inner join parent_category pc with(nolock) on pc.id=ac.parent_category_id where a.artist_status=1 ";
		List<Object> paramList = new ArrayList<Object>();
		
		if(filter.getArtistName()!=null) {
			queryStr += " AND a.name like '%"+filter.getArtistName()+"%'";
		}
		if(filter.getGrandChildCategoryName()!=null) {
			queryStr += " AND gc.name like '%"+filter.getGrandChildCategoryName()+"%'";
		}
		if(filter.getChildCategoryName()!=null) {
			queryStr += " AND cc.name like '%"+filter.getChildCategoryName()+"%'";
		}
		if(filter.getParentCategoryName()!=null) {
			queryStr += " AND pc.name like '%"+filter.getParentCategoryName()+"%'";
		}
		if(filter.getVisibleOnSearch()!=null) {
			queryStr += " AND a.display_on_search="+filter.getVisibleOnSearch();
		}
		Query query = getSession().createSQLQuery(queryStr);
		count = Integer.parseInt(String.valueOf(query.uniqueResult()));
		return count;
	}
	
	public Collection<EventDetails> getAllActiveGrandChildCategoryByFilter(String searchColumn,String searchValue) throws Exception {
		String queryStr = "select distinct grand_child_category_id as 'grandChildCategoryId',grand_child_category_name as 'grandChildCategoryName'," +
				" child_category_id as 'childCategoryId',child_category_name as 'childCategoryName'," +
				" parent_category_id as 'parentCategoryId',parent_category_name as 'parentCategoryName'" +
				" FROM event_details with(nolock) WHERE status=1 ";
		List<Object> paramList = new ArrayList<Object>();
		
		if(searchColumn !=null && !searchColumn.isEmpty()){
			if(searchColumn.equals("grandChildCategoryName")) {
				queryStr += " AND grand_child_category_name like :searchValue  ";
				paramList.add("%"+searchValue+"%");	
			} else if(searchColumn.equals("childCategoryName")) {
				queryStr += " AND child_category_name like :searchValue  ";
				paramList.add("%"+searchValue+"%");	
			} else if(searchColumn.equals("parentCategoryName")) {
				queryStr += " AND parent_category_name like :searchValue  ";
				paramList.add("%"+searchValue+"%");	
			}
		}
		queryStr+=" ORDER BY grand_child_category_name Asc";
		
		Query query = getSession().createSQLQuery(queryStr);
		if(searchColumn !=null && !searchColumn.isEmpty()){
			query.setParameter("searchValue","%"+searchValue+"%");
		}
		List<EventDetails> list = query.setResultTransformer(Transformers.aliasToBean(EventDetails.class)).list();
		return list;
		
	}
	
public Collection<EventDetails> filterByName(String pattern)throws Exception {
		
		String hql="FROM EventDetails WHERE eventStatus=? AND eventName LIKE ? ORDER BY eventName, eventDate, eventTime Asc";
		List<Object> parameters = new ArrayList<Object>();
		parameters.add(EventStatus.ACTIVE);
		parameters.add("%" + pattern + "%");
		return find(hql, parameters.toArray());
		
	}
	/*public List<EventDetails> getEventsByArtistId(Integer artistId)throws Exception {
		
		String hql="FROM EventDetails WHERE eventStatus=? AND artistId=? ORDER BY eventDate, eventTime,eventName Asc";
		List<Object> parameters = new ArrayList<Object>();
		parameters.add(EventStatus.ACTIVE);
		parameters.add(artistId);
		return find(hql, parameters.toArray());
		
	}*/
	public List<EventDetails> getEventsByArtistCity(Integer artistId,String city)throws Exception {
		List<EventDetails> eventDetails = null;
		String sql="SELECT distinct E.event_id AS eventId,E.event_name AS eventName,E.event_date AS eventDate,E.event_time AS eventTime,"+
				   "E.venue_id AS venueId,E.building AS building,E.city As city,E.state AS state,E.country As country,E.postal_code " +
				   "AS postalCode,A.artist_name AS artistName " +
				   "from event_details E with(nolock) inner join event_artist_details A with(nolock) on E.event_id=A.event_id "+
				   "WHERE E.status=1 ";
		try {
			if(artistId!=null && artistId>0){
				sql += "AND A.artist_id="+artistId;
			}
			if(city!=null && !city.isEmpty()){
				sql += " AND E.city='"+city+"'";
			}
			sql += " order by E.event_date,E.event_time,E.event_name Asc";
			SQLQuery query = getSession().createSQLQuery(sql);
			query.addScalar("eventId", Hibernate.INTEGER);
			query.addScalar("eventName", Hibernate.STRING);
			query.addScalar("eventDate", Hibernate.DATE);
			query.addScalar("eventTime", Hibernate.TIME);
			query.addScalar("venueId", Hibernate.INTEGER);
			query.addScalar("building", Hibernate.STRING);
			query.addScalar("city", Hibernate.STRING);
			query.addScalar("state", Hibernate.STRING);
			query.addScalar("country", Hibernate.STRING);
			query.addScalar("postalCode", Hibernate.STRING);
			query.addScalar("artistName", Hibernate.STRING);
			eventDetails = query.setResultTransformer(Transformers.aliasToBean(EventDetails.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return eventDetails;
		
	}
	public List<String> getAllCityByArtistId(Integer artistId)throws Exception {
		
		//String queryStr="select distinct concat(city,' - ',state) from event_details where status=1 and artist_id="+artistId+" ";
		String queryStr="select distinct city from event_details E with(nolock) join event_artist_details A with(nolock) on E.event_id=A.event_id where a.artist_id="+artistId+" ";
		Query query = getSession().createSQLQuery(queryStr);

		List<String> list = query.list();
		return list;
	}
	
	/*public Collection<EventDetails> getAllActiveVenueByFilter(String searchColumn,String searchValue) throws Exception {
		String queryStr = "select distinct venue_id as 'venueId',building as 'building'," +
				" city as 'city',state as 'state',country as 'country',postal_code as 'postalCode'" +
				" FROM event_details WHERE status=1 ";
		List<Object> paramList = new ArrayList<Object>();
		
		if(searchColumn !=null && !searchColumn.isEmpty()){
			if(searchColumn.equals("venueName")) {
				queryStr += " AND building like :searchValue  ";
				paramList.add("%"+searchValue+"%");	
			} else if(searchColumn.equals("city")) {
				queryStr += " AND city like :searchValue  ";
				paramList.add("%"+searchValue+"%");	
			} else if(searchColumn.equals("state")) {
				queryStr += " AND state like :searchValue  ";
				paramList.add("%"+searchValue+"%");	
			} else if(searchColumn.equals("country")) {
				queryStr += " AND country like :searchValue  ";
				paramList.add("%"+searchValue+"%");	
			}
		}
		queryStr+=" ORDER BY building Asc";
		
		Query query = getSession().createSQLQuery(queryStr);
		if(searchColumn !=null && !searchColumn.isEmpty()){
			query.setParameter("searchValue","%"+searchValue+"%");
		}
		List<EventDetails> list = query.setResultTransformer(Transformers.aliasToBean(EventDetails.class)).list();
		return list;
		
	}*/
	
	/*public Collection<EventDetails> getEventsByArtistId(Integer id) {
		Collection<EventDetails> events = null;
		try {
			events =  find("FROM EventDetails WHERE eventStatus = ? AND artistId = ? ORDER BY eventName, eventDate, eventTime Asc", new Object[]{EventStatus.ACTIVE,id});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return events;
	}*/
	
	public Collection<EventDetails> getEventsByVenueId(Integer id) {
		Collection<EventDetails> events = null;
		try {
			events =  find("FROM EventDetails WHERE eventStatus = ? AND  venueId = ? ORDER BY eventName, eventDate, eventTime Asc", new Object[]{EventStatus.ACTIVE,id});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return events;
	}
	
	public Integer getActiveEventIdByEventDateTimeVenue(String eventName,String venueName,String eventDate,String eventTime){
		Integer eventId=null;
		try {
			String time = Util.getParseDateWithTwelvwFourHourFormat(eventTime);
			eventDate += " 00:00";
			String sql = "select event_id from event_Details with(nolock) where status=1 AND event_name like '"+eventName+"' "+
			"AND event_date='"+eventDate+"' AND event_time='"+time+"' "+
			"AND building like '"+venueName+"' ";
			
			SQLQuery query = getSession().createSQLQuery(sql);
			eventId = (Integer) query.uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return eventId;
	}
	
	public Collection<EventDetails> getAllActiveArtistsByFilterToExport(GridHeaderFilters filter) throws Exception {
		Integer firstResult = 0;
		String queryStr = "select distinct a.id as 'artistId',REPLACE(a.name,'\"','') as 'artistName',gc.name as 'grandChildCategoryName'," +
		" gc.id as 'grandChildCategoryId',cc.id as 'childCategoryId',cc.name as 'childCategoryName'," +
		" pc.id as 'parentCategoryId',pc.name as 'parentCategoryName',a.display_on_search AS displayOnSearch" +
		" FROM artist a with(nolock) " +
		" inner join artist_category_mapping ac with(nolock) on ac.artist_id=a.id"+
		" inner join grand_child_category gc with(nolock) on gc.id=ac.grand_child_category_id" +
		" inner join child_category cc with(nolock) on cc.id=ac.child_category_id " +
		" inner join parent_category pc with(nolock) on pc.id=ac.parent_category_id where a.artist_status=1 ";
		List<Object> paramList = new ArrayList<Object>();
		
			if(filter.getArtistName()!=null) {
				queryStr += " AND a.name like '%"+filter.getArtistName()+"%'";
			}
			if(filter.getGrandChildCategoryName()!=null) {
				queryStr += " AND gc.name like '%"+filter.getGrandChildCategoryName()+"%'";
			}
			if(filter.getChildCategoryName()!=null) {
				queryStr += " AND cc.name like '%"+filter.getChildCategoryName()+"%'";
			}
			if(filter.getParentCategoryName()!=null) {
				queryStr += " AND pc.name like '%"+filter.getParentCategoryName()+"%'";
			}
			if(filter.getVisibleOnSearch()!=null) {
				queryStr += " AND a.display_on_search="+filter.getVisibleOnSearch();
			}
		Query query = getSession().createSQLQuery(queryStr);
		List<EventDetails> list = query.setResultTransformer(Transformers.aliasToBean(EventDetails.class)).list();;
		return list;
		
	}
	
}
