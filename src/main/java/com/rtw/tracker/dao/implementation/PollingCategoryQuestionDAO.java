package com.rtw.tracker.dao.implementation;

import java.util.List;
import java.util.Properties;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.EnumType;

import com.rtw.tmat.utils.Util;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.datas.PollingCategoryQuestion;
import com.rtw.tracker.enums.MiniJackpotType;
import com.rtw.tracker.utils.GridHeaderFilters;

public class PollingCategoryQuestionDAO extends HibernateDAO<Integer, PollingCategoryQuestion> implements com.rtw.tracker.dao.services.PollingCategoryQuestionDAO{

	private static Session contestSession = null;
	
	public static Session getContestSession() {
		return PollingCategoryQuestionDAO.contestSession;
	}

	public static void setContestSession(Session contestSession) {
		PollingCategoryQuestionDAO.contestSession = contestSession;
	}
	
	@Override
	public List<PollingCategoryQuestion> getPollingCategoryQuestion(GridHeaderFilters filter,Integer questionId,Integer categoryID,SharedProperty sharedProperty) {
		List<PollingCategoryQuestion> questionList = null;		
		try {
			String sql = "select cq.id as id,cq.poll_cat_id as categoryId ,cq.question as question,cq.option_a as optionA,cq.option_b as optionB," + 
					" cq.option_c as optionC, cq.answer as answer,cq.created_datetime as createdDate,cq.updated_datetime as updatedDate," + 
					" cq.created_by as createdBy,cq.updated_by as updatedBy " + 
					" from polling_question_bank cq with(nolock) WHERE 1=1 ";
				
			if(questionId!=null){
				sql += " AND cq.id ="+questionId;
			}
			if(categoryID!=null){
				sql += " AND cq.poll_cat_id ="+categoryID;
			}
			 
			if(filter.getText7()!=null && !filter.getText7().isEmpty()){
				sql += " AND cq.question like '%"+filter.getText7()+"%' ";
			}
			
			if(filter.getText1()!=null && !filter.getText1().isEmpty()){
				sql += " AND cq.option_a like '%"+filter.getText1()+"%' ";
			}
			if(filter.getText2()!=null && !filter.getText2().isEmpty()){
				sql += " AND cq.option_b like '%"+filter.getText2()+"%' ";
			}
			if(filter.getText3()!=null && !filter.getText3().isEmpty()){
				sql += " AND cq.option_c like '%"+filter.getText3()+"%' ";
			}
			 
			if(filter.getText5()!=null && !filter.getText5().isEmpty()){
				sql += " AND cq.answer like '%"+filter.getText5()+"%' ";
			}
			 
			if(filter.getCreatedBy()!=null && !filter.getCreatedBy().isEmpty()){
				sql += " AND cq.created_by like '"+filter.getCreatedBy()+"' ";
			}
			if(filter.getLastUpdatedBy()!=null && !filter.getLastUpdatedBy().isEmpty()){
				sql += " AND cq.updated_by like '"+filter.getLastUpdatedBy()+"' ";
			}
			 
			if(filter.getText11()!=null && !filter.getText11().isEmpty()){
				sql += " AND gc.title like '%"+filter.getText11()+"%' ";
			}
			
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,cq.created_datetime) = "+Util.
							extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,cq.created_datetime) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year, cq.created_datetime) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getUpdatedDate()!=null ){
				if(Util.extractDateElement(filter.getUpdatedDate(),"DAY") > 0){
					sql += " AND DATEPART(day, cq.updated_datetime) = "+Util.extractDateElement(filter.getUpdatedDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"MONTH") > 0){
					sql += " AND DATEPART(month, cq.updated_datetime) = "+Util.extractDateElement(filter.getUpdatedDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"YEAR") > 0){
					sql += " AND DATEPART(year, cq.updated_datetime) = "+Util.extractDateElement(filter.getUpdatedDate(),"YEAR")+" ";
				}
			}
			sql += " order by cq.id";
			
			contestSession = PollingCategoryQuestionDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				PollingCategoryQuestionDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			

			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("categoryId", Hibernate.INTEGER);
			query.addScalar("question", Hibernate.STRING);
			query.addScalar("optionA", Hibernate.STRING);
			query.addScalar("optionB", Hibernate.STRING);						
			query.addScalar("optionC", Hibernate.STRING); 
			query.addScalar("answer", Hibernate.STRING); 
			query.addScalar("createdDate", Hibernate.TIMESTAMP);	
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);	
			query.addScalar("createdBy", Hibernate.STRING);			
			query.addScalar("updatedBy", Hibernate.STRING);  
			questionList = query.setResultTransformer(Transformers.aliasToBean(PollingCategoryQuestion.class)).list();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return questionList;
	
		
	}

	/*@Override
	public PollingCategoryQuestion getPollingCategoryQuestionByNo(Integer contestId) {
		return findSingle("FROM PollingCategoryQuestion WHERE categoryId=? ",new Object[]{contestId});
	}*/

	@Override
	public Integer getQuestionCount(Integer categoryId) {
		Integer count =0;		
		try {
			String sql = "select count(id) from contest_questions with(nolock) where poll_cat_id="+categoryId;
			
			contestSession = PollingCategoryQuestionDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				PollingCategoryQuestionDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			
			count = (Integer) query.uniqueResult();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return count;	
	}

	@Override
	public PollingCategoryQuestion getContestQuestionByQuestion(Integer categoryId,String question){
		return findSingle("FROM PollingCategoryQuestion WHERE categoryId=? and question like ?",new Object[]{categoryId, "%"+question+"%"});
	}
	
	 
	/*@Override
	public List<PollingCategoryQuestion> getPollingCategoryQuestiontoReposition(Integer actualPosition,Integer contestId) {
		List<PollingCategoryQuestion> questionList = null;		
		try {
			String sql = "select id as id,contest_id as contestId,question_sl_no as serialNo,question as question,option_a as optionA,option_b as optionB,"+
						"option_c as optionC, answer as answer,created_datetime as createdDate,updated_datetime as updatedDate,question_reward as questionReward,"+
						"created_by as createdBy,updated_by as updatedBy,difficulty_level as difficultyLevel from contest_questions with(nolock) WHERE question_sl_no <> "+actualPosition;
				
			sql += " AND contest_id = "+contestId+" order by question_sl_no";
			
			contestSession = PollingCategoryQuestionDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				PollingCategoryQuestionDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("contestId", Hibernate.INTEGER);
			query.addScalar("serialNo", Hibernate.INTEGER);
			query.addScalar("question", Hibernate.STRING);
			query.addScalar("optionA", Hibernate.STRING);
			query.addScalar("optionB", Hibernate.STRING);						
			query.addScalar("optionC", Hibernate.STRING);
			query.addScalar("difficultyLevel", Hibernate.STRING);
			query.addScalar("answer", Hibernate.STRING);
			query.addScalar("questionReward", Hibernate.DOUBLE);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);	
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);	
			query.addScalar("createdBy", Hibernate.STRING);			
			query.addScalar("updatedBy", Hibernate.STRING);
			
			
			questionList = query.setResultTransformer(Transformers.aliasToBean(PollingCategoryQuestion.class)).list();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return questionList;
	
		
	}*/
}
