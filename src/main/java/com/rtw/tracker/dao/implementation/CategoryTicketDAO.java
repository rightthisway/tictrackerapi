package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.CategoryTicket;

public class CategoryTicketDAO extends HibernateDAO<Integer, CategoryTicket> implements com.rtw.tracker.dao.services.CategoryTicketDAO{

	@Override
	public List<CategoryTicket> getUnmappedCategoryTicketByInvoiceId(Integer invoiceId)throws Exception{
		return find("FROM CategoryTicket WHERE invoiceId=? AND ticketId IS NULL order by id", new Object[]{invoiceId});
		
	}

	@Override
	public List<CategoryTicket> getMappedCategoryTicketByInvoiceId(Integer invoiceId) throws Exception {
		return find("FROM CategoryTicket WHERE invoiceId=? AND ticketId IS NOT NULL order by id", new Object[]{invoiceId});
	}

	@Override
	public List<CategoryTicket> getCategoryTicketByCategoryTicketGroupId(Integer ticketGroupId) throws Exception {
		return find("FROM CategoryTicket WHERE categoryTicketGroupId=?", new Object[]{ticketGroupId});
	}
}
