package com.rtw.tracker.dao.implementation;

import java.util.ArrayList;
import java.util.List;

import com.rtw.tracker.datas.Artist;
import com.rtw.tracker.datas.GrandChildCategory;
import com.rtw.tracker.enums.ArtistStatus;
 
/**
 * class having db related methods for GrandChildCategory
 * @author hamin
 *
 */
public class GrandChildCategoryDAO extends HibernateDAO<Integer,GrandChildCategory> implements
com.rtw.tracker.dao.services.GrandChildCategoryDAO {

	/**
	 *  method to get all GrandChildCategories 
	 * @return List of GrandChildCategories
	 */
	public List<GrandChildCategory> getAll() {
		return find("FROM GrandChildCategory order by name asc");	 
	}
	/**
	 * method to get  GrandChildCategoriy by name 
	 * @param name , Grand Child Category name
	 * @return GrandChildCategory
	 */
	public  List<GrandChildCategory> getGrandChildsByChildCategoryId(Integer childCategoryId) {		 	
		return find("FROM GrandChildCategory gc where gc.childCategory.id = ? ORDER BY gc.priority",new Object []{childCategoryId});	 
	}
	/**
	 * method to get  GrandChildCategoriy by id 
	 * @param id , GrandChildCategory id
	 * @return GrandChildCategory
	 */
	public  GrandChildCategory  getGrandChildCategoryById(Integer id) {		 	
		return findSingle("FROM GrandChildCategory gc where gc.id = ?",new Object []{id});	 
	}
	/**
	 * method to get  GrandChildCategoriy by id 
	 * @param childCategoryId , childCategory id
	 * @return GrandChildCategory
	 */
	public  GrandChildCategory  getGrandChildCategoryByName(String grandChild) {		 	
		return findSingle("FROM GrandChildCategory gc where gc.name = ?",new Object []{grandChild});	 
	}
	public List<GrandChildCategory> getAllGrandChildCategoryByChildCategory(Integer childCategoryId)  throws Exception{
		return find("FROM GrandChildCategory  WHERE childCatId=? " +
				" ORDER BY name", new Object[]{childCategoryId});			
	}

	public List<GrandChildCategory> getAllGrandChildCategoryByParentCategory(Integer parentCategoryId)  throws Exception{
		return find("SELECT gc FROM GrandChildCategory gc,ChildCategory cc WHERE gc.childCatId=cc.id AND cc.parentCategory.id=? " +
				" ORDER BY gc.name", new Object[]{parentCategoryId});			
	}
	public List<GrandChildCategory> getGrandChildCategoriesByNameAndDisplayOnSearch(String name) {
		String hql="FROM GrandChildCategory WHERE name like ? AND isDisplay=? order by name";
		List<Object> parameters = new ArrayList<Object>();
		parameters.add("%" + name + "%");
		parameters.add(true);
		return find(hql, parameters.toArray());
	}
	
	public List<GrandChildCategory> getAllGrandChildCategoriesByNameAndDisplayOnSearch(String name) {
		String hql="FROM GrandChildCategory WHERE name like ? AND isDisplay=? order by name";
		List<Object> parameters = new ArrayList<Object>();
		parameters.add(name);
		parameters.add(true);
		return find(hql, parameters.toArray());
	}
	
	@Override
	public List<GrandChildCategory> getGrandChildsByDisplayOnSearch() {
		return find("FROM GrandChildCategory where isDisplay = ? order by name asc",new Object []{true});
	}
	
	

}
