package com.rtw.tracker.dao.implementation;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.EnumType;

import com.rtw.tmat.utils.Util;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.datas.Contests;
import com.rtw.tracker.datas.Customer;
import com.rtw.tracker.pojos.BotDetail;
import com.rtw.tracker.pojos.ContestWinner;
import com.rtw.tracker.utils.GridHeaderFilters;

public class ContestsDAO extends HibernateDAO<Integer, Contests> implements com.rtw.tracker.dao.services.ContestsDAO{

	//private String zonesApiDbUrl = "ZonesApiPlatformV2.dbo";
	private static Session contestSession = null;

	public static Session getContestSession() {
		return ContestsDAO.contestSession;
	}

	public static void setContestSession(Session contestSession) {
		ContestsDAO.contestSession = contestSession;
	}

	public List<Contests> getAllContests(Integer id, GridHeaderFilters filter,String type,String status,SharedProperty sharedProperty){
		List<Contests> contestsList = null;
		try {
			String sql = "select c.id as id, c.contest_name as contestName, c.contest_start_datetime as startDateTime," +
					" c.eligible_free_ticket_winners as ticketWinnerThreshhold, c.free_tickets_per_winner as freeTicketPerWinner, c.total_rewards as rewardPoints,"+
					" c.status as status,c.contest_category as contestCategory,c.participant_star as participantStars,c.referral_star as referralStars,"+
					" c.created_datetime as createdDate, c.updated_datetime as updatedDate, c.created_by as createdBy, c.updated_by as updatedBy," +
					" c.contest_mode as contestMode, c.contest_password as contestPassword, c.no_of_questions as questionSize,c.extended_name as extendedName," +
					" convert(varchar(2), datepart(hour,c.contest_start_datetime)) as hours, convert(varchar(2), datepart(minute,c.contest_start_datetime)) as minutes, "+
					" c.zone as zone, c.promo_ref_id as promoRefId, c.promo_ref_type as promoRefType, c.promo_ref_name as promoRefName, "+
					" c.promotional_code as promotionalCode, c.discount_percentage as discountPercentage,c.promo_expiry_date as promoExpiryDate, "+
					" rtfpo.id as rtfPromoOfferId, c.single_tix_price as singleTicketPrice,c.referral_rewards as referralRewards,c.participants_rtf_points as participantPoints,"+
					" c.participant_lives as participantLives,c.participant_rewards as participantRewards,c.contest_jackpot_type as contestJackpotType,"+
					" c.mega_jackpot as isMegaJackpot, c.no_of_winner_per_question as mjpWinnerPerQuestion,c.referral_rtf_points as referralPoints,"+
					" gift_card_value_id as giftCardId,gift_card_per_winner as giftCardPerWinner,gc.title as giftCardName,gv.card_amount as giftCardAmt,gv.remaining_quantity as giftCardQty " +
					" from contest c with(nolock) "+
					" left join "+sharedProperty.getDatabasAlias()+"rtf_promotional_offer_hdr rtfpo with(nolock) on rtfpo.id = c.promo_offer_id "+
					" left join "+sharedProperty.getDatabasAlias()+"gift_card_value gv with(nolock) on gv.id=c.gift_card_value_id" +
					" left join "+sharedProperty.getDatabasAlias()+"gift_cards gc with(nolock) on gc.id=gv.card_id" +
					" where c.contest_type like '"+type+"' ";

			if(id != null  && id > 0){
				sql += " AND c.id="+id;
			}
			if(status!=null && !status.isEmpty()){
				sql += " AND c.status in("+status+") ";
			}
			if(filter.getContestName()!=null && !filter.getContestName().isEmpty()){
				sql += " AND c.contest_name like '%"+filter.getContestName()+"%' ";
			}
			if(filter.getPartipantCount()!=null){
				sql += " AND c.participant_star="+filter.getPartipantCount();
			}
			if(filter.getWinnerCount()!=null){
				sql += " AND c.referral_star="+filter.getWinnerCount();
			}
			if(filter.getRetailPrice()!=null){
				sql += " AND c.referral_rewards="+filter.getRetailPrice();
			}
			if(filter.getInvoiceTotal()!=null){
				sql += " AND c.participant_rewards="+filter.getInvoiceTotal();
			}
			if(filter.getOrderId()!=null){
				sql += " AND c.participant_lives="+filter.getOrderId();
			}
			if(filter.getStartDate()!=null ){
				if(Util.extractDateElement(filter.getStartDate(),"DAY") > 0){
					sql += " AND DATEPART(day, c.contest_start_datetime) = "+Util.extractDateElement(filter.getStartDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getStartDate(),"MONTH") > 0){
					sql += " AND DATEPART(month, c.contest_start_datetime) = "+Util.extractDateElement(filter.getStartDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getStartDate(),"YEAR") > 0){
					sql += " AND DATEPART(year, c.contest_start_datetime) = "+Util.extractDateElement(filter.getStartDate(),"YEAR")+" ";
				}
			}


			if(filter.getText1() != null && !filter.getText1().isEmpty()){
				sql += " AND c.contest_mode like '"+filter.getText1()+"' ";
			}
			if(filter.getCount() != null){
				sql += " AND c.no_of_questions = "+filter.getCount();
			}
			if(filter.getCategory() != null && !filter.getCategory().isEmpty()){
				sql += " AND c.contest_category like '%"+filter.getCategory()+"%' ";
			}
			if(filter.getTicketWinnerThreshhold() != null){
				sql += " AND c.eligible_free_ticket_winners ="+filter.getTicketWinnerThreshhold();
			}
			if(filter.getFreeTicketPerWinner() != null){
				sql += " AND c.free_tickets_per_winner ="+filter.getFreeTicketPerWinner();
			}
			if(filter.getRewardPoints() != null){
				sql += " AND c.total_rewards ="+filter.getRewardPoints();
			}
			if(filter.getZone() != null){
				sql += " AND c.zone ="+filter.getZone();
			}
			if(filter.getPrice() != null){
				sql += " AND c.single_tix_price ="+filter.getPrice();
			}
			if(filter.getPromoReferenceName() != null){
				sql += " AND c.promo_ref_name like '%"+filter.getPromoReferenceName()+"%' ";
			}
			if(filter.getPromoType() != null){
				sql += " AND c.promo_ref_type like '%"+filter.getPromoType()+"%' ";
			}
			if(filter.getPromoCode() != null){
				sql += " AND c.promotional_code like '%"+filter.getPromoCode()+"%' ";
			}
			if(filter.getMobileDiscount() != null){
				sql += " AND c.discount_percentage ="+filter.getMobileDiscount();
			}
			if(filter.getStatus()!=null && !filter.getStatus().isEmpty()){
				sql += " AND c.status like '"+filter.getStatus()+"' ";
			}
			if(filter.getCreatedBy()!=null && !filter.getCreatedBy().isEmpty()){
				sql += " AND c.created_by like '"+filter.getCreatedBy()+"' ";
			}
			if(filter.getLastUpdatedBy()!=null && !filter.getLastUpdatedBy().isEmpty()){
				sql += " AND c.updated_by like '"+filter.getLastUpdatedBy()+"' ";
			}
			if(filter.getText2()!=null && !filter.getText2().isEmpty()){
					sql += " AND c.contest_jackpot_type like '%"+filter.getText2()+"%' ";
			}
			/*if(filter.getNoOfOrders()!=null ){
				sql += " AND c.no_of_winner_per_question="+filter.getNoOfOrders()+" ";
			}*/
			if(filter.getText3()!=null && !filter.getText3().isEmpty()){
				sql += " AND gc.title like '%"+filter.getText3()+"%' ";
			}
			if(filter.getText4()!=null && !filter.getText4().isEmpty()){
				try {
					sql += " AND c.gift_card_per_winner ="+filter.getText4().trim();
				} catch (Exception e) {
				}
			}


			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day, c.created_datetime) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month, c.created_datetime) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year, c.created_datetime) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getUpdatedDate()!=null ){
				if(Util.extractDateElement(filter.getUpdatedDate(),"DAY") > 0){
					sql += " AND DATEPART(day, c.updated_datetime) = "+Util.extractDateElement(filter.getUpdatedDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"MONTH") > 0){
					sql += " AND DATEPART(month, c.updated_datetime) = "+Util.extractDateElement(filter.getUpdatedDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"YEAR") > 0){
					sql += " AND DATEPART(year, c.updated_datetime) = "+Util.extractDateElement(filter.getUpdatedDate(),"YEAR")+" ";
				}
			}

			if(status !=null && status.contains("EXPIRED")){
				sql +=" order by c.contest_start_datetime desc";
			}else{
				sql +=" order by c.contest_start_datetime";
			}

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);

			Properties paramCreationType = new Properties();
			paramCreationType.put("enumClass", "com.rtw.tracker.enums.ContestJackpotType");
			paramCreationType.put("type", "12");

			query.addScalar("contestJackpotType", Hibernate.custom(EnumType.class,paramCreationType));

			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("contestName", Hibernate.STRING);
			query.addScalar("startDateTime", Hibernate.TIMESTAMP);
			query.addScalar("contestCategory", Hibernate.STRING);
			query.addScalar("ticketWinnerThreshhold", Hibernate.INTEGER);
			query.addScalar("freeTicketPerWinner", Hibernate.INTEGER);
			query.addScalar("extendedName", Hibernate.STRING);
			query.addScalar("rewardPoints", Hibernate.DOUBLE);
			query.addScalar("contestMode", Hibernate.STRING);
			query.addScalar("contestPassword", Hibernate.STRING);
			query.addScalar("questionSize", Hibernate.INTEGER);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("updatedBy", Hibernate.STRING);
			query.addScalar("minutes", Hibernate.STRING);
			query.addScalar("hours", Hibernate.STRING);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("zone", Hibernate.STRING);
			query.addScalar("promotionalCode", Hibernate.STRING);
			query.addScalar("discountPercentage", Hibernate.DOUBLE);
			query.addScalar("rtfPromoOfferId", Hibernate.INTEGER);
			query.addScalar("promoRefId", Hibernate.INTEGER);
			query.addScalar("promoRefType", Hibernate.STRING);
			query.addScalar("promoRefName", Hibernate.STRING);
			query.addScalar("promoExpiryDate", Hibernate.TIMESTAMP);
			query.addScalar("singleTicketPrice", Hibernate.DOUBLE);
			query.addScalar("participantStars", Hibernate.INTEGER);
			query.addScalar("referralStars", Hibernate.INTEGER);
			query.addScalar("referralRewards", Hibernate.DOUBLE);
			query.addScalar("participantLives", Hibernate.INTEGER);
			query.addScalar("participantRewards", Hibernate.DOUBLE);
			query.addScalar("participantPoints", Hibernate.INTEGER);
			query.addScalar("referralPoints", Hibernate.INTEGER);
			query.addScalar("giftCardId", Hibernate.INTEGER);
			query.addScalar("giftCardPerWinner", Hibernate.INTEGER);
			query.addScalar("giftCardName", Hibernate.STRING);
			query.addScalar("giftCardQty", Hibernate.INTEGER);
			query.addScalar("giftCardAmt", Hibernate.DOUBLE);


			contestsList = query.setResultTransformer(Transformers.aliasToBean(Contests.class)).list();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return contestsList;
	}

	@Override
	public List<Contests> getAllActiveContests() {
		return find("FROM Contests where status=?",new Object[]{"ACTIVE"});
	}

	@Override
	public List<Contests> getAllStartedContests() {
		return find("FROM Contests where status=?",new Object[]{"STARTED"});
	}

	@Override
	public Contests getTodaysContest(String type,String status) {
		List<Contests> contestsList = null;
		Date today = new Date();
		try {
			String sql = "select top 1 id as id,contest_name as contestName,contest_start_datetime as startDateTime," +
					"eligible_free_ticket_winners as ticketWinnerThreshhold,free_tickets_per_winner as freeTicketPerWinner,total_rewards as rewardPoints,"+
					"status as status,contest_category as contestCategory,participant_star as participantStars,referral_star as referralStars,"+
					"created_datetime as createdDate,updated_datetime as updatedDate,created_by as createdBy,updated_by as updatedBy,extended_name as extendedName," +
					"contest_mode as contestMode, no_of_questions as questionSize,last_question_no as lastQuestionNo,last_action as lastAction," +
					"convert(varchar(2), datepart(hour,contest_start_datetime)) as hours,convert(varchar(2), datepart(minute,contest_start_datetime)) as minutes,"+
					"zone as zone, promo_ref_id as promoRefId, promo_ref_type as promoRefType, promo_ref_name as promoRefName, "+
					"promotional_code as promotionalCode, discount_percentage as discountPercentage,referral_rewards as referralRewards,"+
					"participant_lives as participantLives,participant_rewards as participantRewards, "+
					"no_of_winner_per_question as mjpWinnerPerQuestion,contest_jackpot_type as contestJackpotType "+
					"from contest with(nolock) where status in("+status+") and contest_type like '"+type+"' ";

					sql += " AND DATEPART(day, contest_start_datetime) = "+Util.extractDateElement(Util.formatDateToMonthDateYear(today),"DAY")+" ";
					sql += " AND DATEPART(month,contest_start_datetime) = "+Util.extractDateElement(Util.formatDateToMonthDateYear(today),"MONTH")+" ";
					sql += " AND DATEPART(year,contest_start_datetime) = "+Util.extractDateElement(Util.formatDateToMonthDateYear(today),"YEAR")+" ";
					sql += " order by contest_start_datetime";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			Properties paramCreationType = new Properties();
			paramCreationType.put("enumClass", "com.rtw.tracker.enums.ContestJackpotType");
			paramCreationType.put("type", "12"); 

			query.addScalar("contestJackpotType", Hibernate.custom(EnumType.class,paramCreationType));
			
			
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("contestName", Hibernate.STRING);
			query.addScalar("startDateTime", Hibernate.TIMESTAMP);
			query.addScalar("ticketWinnerThreshhold", Hibernate.INTEGER);
			query.addScalar("freeTicketPerWinner", Hibernate.INTEGER);
			query.addScalar("extendedName", Hibernate.STRING);
			query.addScalar("rewardPoints", Hibernate.DOUBLE);
			query.addScalar("contestCategory", Hibernate.STRING);
			query.addScalar("contestMode", Hibernate.STRING);
			query.addScalar("questionSize", Hibernate.INTEGER);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("updatedBy", Hibernate.STRING);
			query.addScalar("minutes", Hibernate.STRING);
			query.addScalar("hours", Hibernate.STRING);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("zone", Hibernate.STRING);
			query.addScalar("promotionalCode", Hibernate.STRING);
			query.addScalar("discountPercentage", Hibernate.DOUBLE);
			query.addScalar("promoRefId", Hibernate.INTEGER);
			query.addScalar("promoRefType", Hibernate.STRING);
			query.addScalar("promoRefName", Hibernate.STRING);
			query.addScalar("lastQuestionNo", Hibernate.INTEGER);
			query.addScalar("lastAction", Hibernate.STRING);
			query.addScalar("participantStars", Hibernate.INTEGER);
			query.addScalar("referralStars", Hibernate.INTEGER);
			query.addScalar("referralRewards", Hibernate.DOUBLE);
			query.addScalar("participantLives", Hibernate.INTEGER);
			query.addScalar("participantRewards", Hibernate.DOUBLE);
			/*query.addScalar("isMegaJackpot", Hibernate.BOOLEAN);
			query.addScalar("mjpWinnerPerQuestion", Hibernate.INTEGER);*/
			contestsList = query.setResultTransformer(Transformers.aliasToBean(Contests.class)).list();
			if(!contestsList.isEmpty()){
				return contestsList.get(0);
			}
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return null;
	}


	public boolean resetContestDataById(Integer contestId,SharedProperty sharedProperty){
		String sql = null;
		try {



			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}

			sql = "update "+sharedProperty.getDatabasAlias()+"customer set quiz_cust_lives=quiz_cust_lives+1 where id in"+
				  " (select customer_id from customer_contest_answers where is_lifeline_used=1 and contest_id="+contestId+")";
			SQLQuery query3 = contestSession.createSQLQuery(sql);
			query3.executeUpdate();


			sql = "delete from customer_contest_answers where contest_id="+contestId;
			SQLQuery query = contestSession.createSQLQuery(sql);
			query.executeUpdate();

			sql = "delete from contest_winners where contest_id="+contestId;
			SQLQuery query1 = contestSession.createSQLQuery(sql);
			query1.executeUpdate();

			sql = "delete from contest_grand_winners where contest_id="+contestId;
			SQLQuery query2 = contestSession.createSQLQuery(sql);
			query2.executeUpdate();

			return true;

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
			return false;
		}
	}

	public Contests getContestByPromoOfferId(Integer promoOfferId){
		return findSingle("FROM Contests where rtfPromoOfferId=?",new Object[]{promoOfferId});
	}

	@Override
	public List<ContestWinner> getContestWinners(Integer contestId,GridHeaderFilters filter,SharedProperty sharedProperty) {
		List<ContestWinner> winners = null;
		try {
			String sql ="select cv.id as id,co.id as customerId,co.cust_name as custName,co.last_name as lastName,co.user_id as userId,co.email as email," +
					"cv.reward_points as rewardPoints,co.phone as phone " +
					"from contest c INNER join contest_winners cv ON c.id = cv.contest_id inner join " +
					sharedProperty.getDatabasAlias()+"customer co on cv.customer_id=co.id where c.id="+contestId;
			if(filter.getCustomerName()!=null && !filter.getCustomerName().isEmpty()){
				sql += " AND co.cust_name like '%"+filter.getCustomerName()+"%'";
			}
			if(filter.getLastName()!=null && !filter.getLastName().isEmpty()){
				sql += " AND co.last_name like '%"+filter.getLastName()+"%'";
			}
			if(filter.getUsername()!=null && !filter.getUsername().isEmpty()){
				sql += " AND co.user_id like '%"+filter.getUsername()+"%'";
			}
			if(filter.getEmail()!=null && !filter.getEmail().isEmpty()){
				sql += " AND co.email like '%"+filter.getEmail()+"%'";
			}
			if(filter.getPhone()!=null && !filter.getPhone().isEmpty()){
				sql += " AND co.phone like '%"+filter.getPhone()+"%'";
			}
			if(filter.getRewardPoints()!=null){
				sql += " AND cv.reward_points="+filter.getRewardPoints();
			}
			
			sql += " order by cv.id";
			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			query.addScalar("customerId", Hibernate.INTEGER);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("custName", Hibernate.STRING);
			query.addScalar("lastName", Hibernate.STRING);
			query.addScalar("userId", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("rewardPoints", Hibernate.DOUBLE);
			query.addScalar("phone", Hibernate.STRING);
			winners = query.setResultTransformer(Transformers.aliasToBean(ContestWinner.class)).list();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return winners;
	}

	@Override
	public List<ContestWinner> getContestGrandWinners(Integer contestId,GridHeaderFilters filter,SharedProperty sharedProperty) {
		List<ContestWinner> winners = null;
		try {
			String sql ="select cv.id as id,co.id as customerId,co.cust_name as custName,co.last_name as lastName,co.user_id as userId,co.email as email," +
					"co.phone as phone,cv.status as status,cv.order_id as orderId,cv.reward_tickets as rewardTickets,cv.expiry_date as expiryDate, " +
					"cv.created_Date as createdDate,cv.mini_jackpot_type as jackpotType,cv.winner_type as winnerType,cv.notes as notes "+
					"from contest c INNER join contest_grand_winners cv ON c.id = cv.contest_id inner join " +
					sharedProperty.getDatabasAlias()+"customer co on cv.customer_id=co.id where c.id="+contestId;
			if(filter.getCustomerName()!=null && !filter.getCustomerName().isEmpty()){
				sql += " AND co.cust_name like '%"+filter.getCustomerName()+"%'";
			}
			if(filter.getLastName()!=null && !filter.getLastName().isEmpty()){
				sql += " AND co.last_name like '%"+filter.getLastName()+"%'";
			}
			if(filter.getUsername()!=null && !filter.getUsername().isEmpty()){
				sql += " AND co.user_id like '%"+filter.getUsername()+"%'";
			}
			if(filter.getEmail()!=null && !filter.getEmail().isEmpty()){
				sql += " AND co.email like '%"+filter.getEmail()+"%'";
			}
			if(filter.getStatus()!=null && !filter.getStatus().isEmpty()){
				sql += " AND cv.status like '%"+filter.getStatus()+"%'";
			}
			if(filter.getText1()!=null && !filter.getText1().isEmpty()){
				sql += " AND cv.mini_jackpot_type like '%"+filter.getText1()+"%'";
			}
			if(filter.getText2()!=null && !filter.getText2().isEmpty()){
				sql += " AND cv.winner_type like '%"+filter.getText2()+"%'";
			}
			if(filter.getText3()!=null){
				sql += " AND cv.notes like '%"+filter.getText3()+"%'";
			}
			if(filter.getTicketQty()!=null){
				sql += " AND cv.reward_tickets="+filter.getTicketQty();
			}
			if(filter.getOrderId()!=null){
				sql += " AND cv.order_id="+filter.getOrderId();
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,cv.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,cv.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year,cv.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEndDate() != null){
				if(Util.extractDateElement(filter.getEndDate(),"DAY") > 0){
					sql += " AND DATEPART(day,cv.expiry_date) = "+Util.extractDateElement(filter.getEndDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEndDate(),"MONTH") > 0){
					sql += " AND DATEPART(month,cv.expiry_date) = "+Util.extractDateElement(filter.getEndDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEndDate(),"YEAR") > 0){
					sql += " AND DATEPART(year,cv.expiry_date) = "+Util.extractDateElement(filter.getEndDate(),"YEAR")+" ";
				}
			}
			
			
			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("customerId", Hibernate.INTEGER);
			query.addScalar("custName", Hibernate.STRING);
			query.addScalar("lastName", Hibernate.STRING);
			query.addScalar("userId", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("phone", Hibernate.STRING);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("jackpotType", Hibernate.STRING);
			query.addScalar("winnerType", Hibernate.STRING);
			query.addScalar("rewardTickets", Hibernate.INTEGER);
			query.addScalar("orderId", Hibernate.INTEGER);
			query.addScalar("notes", Hibernate.STRING);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("expiryDate", Hibernate.TIMESTAMP);
			
			winners = query.setResultTransformer(Transformers.aliasToBean(ContestWinner.class)).list();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return winners;
	}

	@Override
	public List<Customer> getContestCustomers(Integer contestId,SharedProperty sharedProperty) {
		List<Customer> customers = null;
		try {
			String SQLQuery = "select DISTINCT c.cust_name AS customerName,c.last_name AS lastName,c.user_id AS userId,c.email AS email,phone AS phone,c.signup_date as signupDate,"+
					"min(join_datetime) as joinDate from contest_participants wt join "+sharedProperty.getDatabasAlias()+"customer c "
					+ "on wt.customer_id=c.id where wt.contest_id="+contestId+" "
					+ "group by c.id,c.cust_name,c.last_name,c.user_id,c.email,c.phone,c.signup_date order by c.user_id";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(SQLQuery);
			query.addScalar("customerName", Hibernate.STRING);
			query.addScalar("lastName", Hibernate.STRING);
			query.addScalar("userId", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("phone", Hibernate.STRING);
			query.addScalar("signupDate", Hibernate.TIMESTAMP);
			query.addScalar("joinDate", Hibernate.TIMESTAMP);


			customers = query.setResultTransformer(Transformers.aliasToBean(Customer.class)).list();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return customers;
	}

	@Override
	public List<Object[]> getContestTicketCostReportData(String fromDate,String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			String sql = 	" SELECT  InvoiceNo,InvoiceCreatedDate,InvoiceStatus,CustomerOrderNo,CustomerOrderCreatedDate,PurchaseOrderNo,PurchaseOrderCreatedDate,EventName, "
							+" 		EventDate,EventTime,VenueName,VenueCity,Zone,Section,Row,Seat,CustomerName,CustomerEmail,phone,Quantity,BotsCustomer, Cost as ContestTicketCost,"
							+"		OrderType,Platform,PaymentMethod,InvCreatedDate,COCreatedDate,POCreatedDate "
							+"FROM (	SELECT i.id as InvoiceNo, "
							+"			CONVERT(VARCHAR(10), i.created_date, 101) + ' ' +CONVERT(VARCHAR(10), i.created_date, 108) as InvoiceCreatedDate, i.status as InvoiceStatus,"
							+"			co.id as CustomerOrderNo, CONVERT(VARCHAR(10), co.created_date, 101) + ' ' +CONVERT(VARCHAR(10), co.created_date, 108) as CustomerOrderCreatedDate, "
							+"			p.id as PurchaseOrderNo, CONVERT(VARCHAR(10), p.created_time, 101) + ' ' +CONVERT(VARCHAR(10), p.created_time, 108) as PurchaseOrderCreatedDate,"
							+"			CONVERT(VARCHAR(500),co.event_name) as EventName, CONVERT(VARCHAR(19), co.event_date,101) as EventDate,"
							+"			CASE WHEN co.event_time is not null THEN CONVERT(VARCHAR(20), co.event_time, 108) WHEN co.event_time is null THEN 'TBD' END as EventTime,"
							+"			co.venue_name as VenueName, co.venue_city as VenueCity, CONVERT(VARCHAR(50),co.section) as Zone, co.quantity as Quantity,"
							+"			co.actual_section as Section, CONVERT(VARCHAR(50),co.row) as Row, co.seat_details as Seat,"
							+"			CONVERT(VARCHAR(356), CONCAT(c.cust_name,' ',c.last_name)) as CustomerName, CONVERT(VARCHAR(255),c.email) as CustomerEmail,"
							+"			c.phone as phone, CONVERT(DECIMAL(10,2),co.order_total )  as OrderTotal,"
							+"			CONVERT(DECIMAL(10,2), CASE WHEN ic.TicketCost is not null THEN ic.TicketCost else 0 END ) as Cost,"
							+"			CONVERT(DECIMAL(10,2), CASE WHEN tc.InvoiceTicketCost is not null THEN tc.InvoiceTicketCost  else 0 END ) as TicketCost,"
							+"			CONVERT(VARCHAR(20),co.order_type) as OrderType, co.platform as Platform,"
							+"			CONVERT(VARCHAR(152),IIF(co.primary_payment_method is null or co.primary_payment_method='NULL' , ' ' , co.primary_payment_method)+"
							+"					IIF(co.secondary_payment_method is null or co.secondary_payment_method='NULL' , ' ' , ','+co.secondary_payment_method)+"
							+"						IIF(co.third_payment_method is null or co.third_payment_method='NULL' , ' ' , ','+co.third_payment_method)) As PaymentMethod, "
							+"			CONVERT(DECIMAL(10,2),(IIF(co.category_ticket_group_id > 0, (ctgc.t_price * co.quantity), tc.t_price) )  ) as ticketTotalPrice,"
							+"			CONVERT(DECIMAL(10,2),(IIF(co.category_ticket_group_id > 0, (ctgc.t_sold_price * co.quantity), tc.t_sold_price) ) ) as ticketTotalSoldPrice,"
							+"			IIF(cBots.CustomerId IS NULL, 'NO', 'YES') as BotsCustomer,"
							+"			i.created_date as InvCreatedDate,co.created_date as COCreatedDate,p.created_time as POCreatedDate "
							+"		FROM "+sharedProperty.getDatabasAlias()+"customer_order co "
							+"		INNER JOIN "+sharedProperty.getDatabasAlias()+"customer c on c.id=co.customer_id and c.is_test_account <> 1 "
							+"		INNER JOIN "+sharedProperty.getDatabasAlias()+"invoice i on i.order_id = co.id  and i.status <> 'VOIDED' "
							+"		LEFT JOIN "+sharedProperty.getDatabasAlias()+"rtf_promotional_order_tracking_new pro on pro.order_id=co.id and pro.status='COMPLETED' "
							+"		LEFT JOIN "+sharedProperty.getDatabasAlias()+"customer cust on pro.referrer_cust_id=cust.id "
							+"		OUTER APPLY (SELECT  invoice_id,sum(cost) as TicketCost, purchase_order_id "
							+"					FROM "+sharedProperty.getDatabasAlias()+"ticket where invoice_id is not null and invoice_id <> 0 and invoice_id = i.id "
							+"					GROUP BY invoice_id,purchase_order_id) ic "
							+"		OUTER APPLY (SELECT invoice_id,sum(cost) as InvoiceTicketCost, sum(retail_price) as t_price,sum(actual_sold_price) as t_sold_price "
							+"					FROM "+sharedProperty.getDatabasAlias()+"ticket where invoice_id is not null and invoice_id <> 0 and invoice_id = i.id group by invoice_id) tc "
							+"		OUTER APPLY (SELECT invoice_id,sum(cost) as InvoiceTicketCost, sum(price) as t_price, sum(sold_price) as t_sold_price "
							+"					FROM "+sharedProperty.getDatabasAlias()+"category_ticket_group where  invoice_id = i.id and id = co.category_ticket_group_id group by invoice_id) ctgc "
							+"		LEFT JOIN "+sharedProperty.getDatabasAlias()+"purchase_order p  on ic.purchase_order_id = p.id "
							+"		LEFT JOIN bots_customer cBots  on c.id = cBots.CustomerId "
							+"		WHERE co.order_type  in ('CONTEST') "
							+"		) salesRep "
							+"	WHERE 1=1 ";

					if(fromDate!=null && !fromDate.isEmpty()){
						fromDate += " 00:00:00.000";
						sql+= " AND salesRep.InvCreatedDate >= '"+fromDate+"' ";
					}
					if(toDate!=null && !toDate.isEmpty()){
						toDate += " 23:59:59:000";
						sql+= " AND salesRep.InvCreatedDate <= '"+toDate+"' ";
					}
					/*+ "where salesRep.InvCreatedDate between convert(datetime,'08/10/2018 00:00:00.000') and convert(datetime,'09/30/2018 00:00:00.000')"*/
					sql += "order by salesRep.InvoiceNo";


			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<Object[]> getContestWinnerInvoiceReportData(SharedProperty sharedProperty){
		List<Object[]> list = null;
		try {
			String sql = 	" 	SELECT  cc.id,cc.contest_name,cc.contest_start_datetime,c.cust_name,c.last_name,c.user_id,c.email,c.phone,"
							+"		IIF(cBots.CustomerId IS NULL, 'No', 'Yes') BotsCustomer, "
							+"		w.reward_tickets,w.status,w.order_id,co.event_name,co.event_Date,co.event_time,co.created_date "
							+"	FROM contest_grand_winners w "
							+"	INNER JOIN "+sharedProperty.getDatabasAlias()+"customer c on w.customer_id=c.id "
							+"	LEFT JOIN "+sharedProperty.getDatabasAlias()+"customer_order co on w.order_id=co.id "
							+"	INNER JOIN contest cc on cc.id=w.contest_id "
							+"	LEFT JOIN bots_customer cBots  on c.id = cBots.CustomerId"
							+"	WHERE cc.is_test_contest = 0  AND cc.status='EXPIRED' ORDER BY w.status,cc.contest_start_datetime";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;

	}

	@Override
	public List<Object[]> getContestRewardDollarsReportData(String fromDate,String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			String sql = 	"SELECT contestCustomerNo, contestCustomerName as contestParticipant "
							+"	,COUNT(contestNo) as amountOfGamesPlayed "
							+"	,SUM(contestRewardsTotal) as totalContestRewards "
							+"	,CONVERT(DECIMAL(12),SUM(questionsCPlayed)) as AmountOfQuestionsAnsweredOfAllGame "
							+"	,CONVERT(DECIMAL(12,2),SUM(contestARewards)) as AmountOfRewardDollarsEarnedFromQuestions "
							+"	,CONVERT(DECIMAL(12,2),SUM(contestJRewards)) as AmountOfRewardDollarsEarnedFromJackpot "
							+"	,CONVERT(DECIMAL(12),SUM(contestLLUsed)) as AmountOfLivesUsedPerGame "
							+"FROM (	SELECT contestNo, contestCustomerNo "
							+"				,IIF(contestCustomerName IS NOT NULL AND LTRIM(RTRIM(contestCustomerName)) <> '', contestCustomerName, contestCustomerUserID) as contestCustomerName "
							+"				,sum(contestAnswerRewards+contestJackpotRewards) as contestRewardsTotal "
							+"				,sum(questionsPlayed) as questionsCPlayed "
							+"		 		,sum(contestAnswerRewards) as contestARewards "
							+"				,sum(contestJackpotRewards) contestJRewards "
							+"				,sum(contestLifeLineUsed) as contestLLUsed "
							+"			FROM ( 	SELECT cca.contest_id as contestNo ,convert(int,cca.customer_id) as contestCustomerNo "
							+"						,cu.contest_customer_name as contestCustomerName,cu.contest_customer_user_id as contestCustomerUserID "
							+"						,convert(datetime,ct.contest_start_datetime) as contestDate,sum(convert(int,cca.is_correct_answer)) as questionsPlayed "
							+"						,iif(sum(cca.answer_rewards) is null,0,sum(cca.answer_rewards) ) as contestAnswerRewards "
							+"						,iif(sum(convert(int,cca.is_lifeline_used)) is null , 0, sum(convert(int,cca.is_lifeline_used)) ) as contestLifeLineUsed "
							+"						,0 as contestJackpotRewards "
							+"					FROM customer_contest_answers cca "
							+"					INNER JOIN contest ct on cca.contest_id = ct.id "
							+"					INNER JOIN contest_registered_customer cu on cca.customer_id = cu.contest_customer_id "
							+"					WHERE ct.status = 'EXPIRED'  ";
							if(fromDate!=null && !fromDate.isEmpty()){
								fromDate += " 00:00:00.000";
								sql+= " 		AND convert(datetime,ct.contest_start_datetime) >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								toDate += " 23:59:59:000";
								sql+= " 		AND convert(datetime,ct.contest_start_datetime) <= '"+toDate+"' ";
							}
							sql += "			AND ct.is_test_contest = 0 "
							+"					GROUP BY cca.contest_id, cca.customer_id, cu.contest_customer_name, "
							+"						cu.contest_customer_user_id, convert(datetime,ct.contest_start_datetime) "
							+"					UNION "
							+"					SELECT cca.contest_id as contestNo, convert(int,cca.customer_id) as contestCustomerNo "
							+"						,cu.contest_customer_name as contestCustomerName,cu.contest_customer_user_id as contestCustomerUserID "
							+"						,convert(datetime,ct.contest_start_datetime) as contestDate,0 as questionsPlayed "
							+"						,0 as contestAnswerRewards,0 as contestLifeLineUsed "
							+"						,iif(sum(cca.reward_points) is null,0,sum(cca.reward_points) ) as contestJackpotRewards "
							+"					FROM contest_winners cca "
							+"					INNER JOIN contest ct on cca.contest_id = ct.id "
							+"					INNER JOIN contest_registered_customer cu on cca.customer_id = cu.contest_customer_id  "
							+"					WHERE ct.status = 'EXPIRED'  ";
							if(fromDate!=null && !fromDate.isEmpty()){
								sql+= " 		AND convert(datetime,ct.contest_start_datetime) >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								sql+= " 		AND convert(datetime,ct.contest_start_datetime) <= '"+toDate+"' ";
							}
							sql += "			AND ct.is_test_contest = 0 "
							+"					GROUP BY cca.contest_id, cca.customer_id, cu.contest_customer_name, cu.contest_customer_user_id, "
							+"						convert(datetime,ct.contest_start_datetime) "
							+"				)cr GROUP BY contestNo, contestCustomerNo, IIF(contestCustomerName IS NOT NULL AND LTRIM(RTRIM(contestCustomerName)) <> '', contestCustomerName, contestCustomerUserID) "
							+"	) cau GROUP BY cau.contestCustomerNo, cau.contestCustomerName "
							+"ORDER BY cau.contestCustomerName ";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}


	public List<Object[]> getContesWiseRewardDollarsReportData(String fromDate,String toDate,SharedProperty sharedProperty){
		List<Object[]> list = null;
		try {
			String sql =	"	SELECT id,contestName,contestDate,sum(rewardDollars) "
							+"	FROM (	SELECT c.id as id,c.contest_name as contestName,c.contest_start_datetime as contestDate "
							+"				,sum(w.reward_points) as rewardDollars "
							+"			FROM contest c  "
							+"			INNER JOIN contest_winners w on c.id=w.contest_id "
							+"			INNER JOIN contest_registered_customer rc on w.customer_id = rc.contest_customer_id "
							+"			WHERE c.is_test_contest = 0 and c.status = 'EXPIRED' " ;
							if(fromDate!=null && !fromDate.isEmpty()){
								fromDate += " 00:00:00.000";
								sql+= " AND convert(datetime,c.contest_start_datetime) >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								toDate += " 23:59:59:000";
								sql+= " AND convert(datetime,c.contest_start_datetime) <= '"+toDate+"' ";
							}
							sql += "	GROUP BY c.id,c.contest_name,c.contest_start_datetime "
							+"			UNION  "
							+"			SELECT c.id as id,c.contest_name as contestName,c.contest_start_datetime as contestDate "
							+"				,sum(a.answer_rewards) as rewardDollars "
							+"			FROM contest c  "
							+"			INNER JOIN customer_contest_answers a on c.id=a.contest_id and a.is_correct_answer=1 "
							+"			INNER JOIN contest_registered_customer rc on a.customer_id = rc.contest_customer_id "
							+"			WHERE c.is_test_contest = 0 and c.status = 'EXPIRED' " ;
							if(fromDate!=null && !fromDate.isEmpty()){
								sql+= " AND convert(datetime,c.contest_start_datetime) >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								sql+= " AND convert(datetime,c.contest_start_datetime) <= '"+toDate+"' ";
							}
							sql += "	GROUP BY c.id,c.contest_name,c.contest_start_datetime "
							+"		) as reward "
							+"	GROUP BY reward.id,reward.contestName,reward.contestDate ORDER BY reward.id ";
			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}


	@Override
	public List<Object[]> getContestReportData(String fromDate,String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			String sql = "SELECT contestID "+
					",contestName "+
					",contestDate "+
					",contestTime "+
					",contestTotalNoOfQuestions "+
					",customerNo "+
					",customerName  "+
					",customerEmail "+
					",CustomerPhoneNumber "+
					",customerPhoneStateName "+
					",customerIPAddress "+
					",noOfQuestionsAttempted "+
					",noOfQuestionsAnswered "+
					",rewardPointsEarnedAtContest "+
					",rewardPointsEarnedBySharing "+
					",CONVERT(DECIMAL(12,2),rewardPointsEarnedAtContest+rewardPointsEarnedBySharing) as totalRewardPointsEarned "+
					",rewardTicketsEarned,lifeLineUsed,contestType,contestStartDateTime "+
					"from (SELECT c.id as contestID "+
					",c.[contest_name] as contestName "+
					",CONVERT(VARCHAR,c.[contest_start_datetime],101) as contestDate "+
					",iif(c.[contest_start_datetime] is not null, LTRIM(RIGHT(CONVERT(VARCHAR, c.[contest_start_datetime], 100), 7)), 'TBD') as contestTime "+
					",c.[no_of_questions] as contestTotalNoOfQuestions "+
					",cca.customer_id as customerNo "+
					",zc.[cust_name]+' '+ zc.[last_name] as customerName  "+
					",zc.[email] as customerEmail "+
					",REPLACE(REPLACE(zc.phone,'(',''),')','') as CustomerPhoneNumber "+
					",spc.state_name as customerPhoneStateName "+
					",ip.cust_ip_address as customerIPAddress "+
					",count(cca.[question_sl_no]) as noOfQuestionsAttempted "+
					",sum(iif(cca.[is_correct_answer]=1, 1, 0)) as noOfQuestionsAnswered "+
					",CONVERT(DECIMAL(12,2),sum(iif(cca.[answer_rewards] > 0, [answer_rewards], 0))) as rewardPointsEarnedAtContest "+
					",CONVERT(DECIMAL(12,2),iif(cw.[reward_points] > 0, cw.[reward_points], 0)) as rewardPointsEarnedBySharing "+
					",iif(cw.[reward_tickets] > 0, cw.[reward_tickets], 0) as rewardTicketsEarned "+
					",sum(iif(cca.[is_lifeline_used] > 0, [is_lifeline_used], 0)) as lifeLineUsed "+
					",c.[contest_type] as contestType "+
					",c.contest_start_datetime as contestStartDateTime "+
					"FROM customer_contest_answers cca WITH(NOWAIT) "+
					"left join contest_winners cw WITH(NOWAIT) on cca.contest_id = cw.contest_id and cca.customer_id = cw.customer_id "+
					"left join contest c WITH(NOWAIT) on cca.contest_id = c.id  "+
					"left join "+sharedProperty.getDatabasAlias()+"customer zc WITH(NOWAIT) on cca.customer_id = zc.id  "+
					"left join (select max(ip_address) as cust_ip_address, customer_id, "+
					"contest_id as contestCid from contest_participants WITH(NOWAIT) "+
					"where contest_id is not null "+
					"and customer_id is not null and  customer_id > 0 "+
					"group by contest_id, customer_id "+
					") ip on cca.customer_id = ip.customer_id and cca.contest_id = ip.contestCid "+
					"OUTER APPLY (select state_name from "+sharedProperty.getDatabasAlias()+"state_phonecodes  WITH(NOWAIT) "+
					"where CONVERT(varchar(4),phone_code) = LEFT(IIF(CHARINDEX('1',REPLACE(REPLACE(zc.phone,'(',''),')',''),1)=1, "+
					"RIGHT(REPLACE(REPLACE(zc.phone,'(',''),')',''), LEN(REPLACE(REPLACE(zc.phone,'(',''),')',''))-1), "+
					"REPLACE(REPLACE(zc.phone,'(',''),')','') ),3)) spc "+
					"where  zc.product_type='REWARDTHEFAN' and zc.is_test_account = 0 and c.is_test_contest = 0 and c.status = 'EXPIRED' ";

					if(fromDate!=null && !fromDate.isEmpty()){
						fromDate += " 00:00:00.000";
						sql+= " AND convert(datetime,c.contest_start_datetime) >= '"+fromDate+"' ";
					}
					if(toDate!=null && !toDate.isEmpty()){
						toDate += " 23:59:59:000";
						sql+= " AND convert(datetime,c.contest_start_datetime) <= '"+toDate+"' ";
					}

					sql += "and c.is_test_contest = 0 "+
					"group by c.id,c.[contest_name], c.[contest_start_datetime],c.[no_of_questions],c.[contest_type], "+
					"cca.[customer_id],cca.[contest_id],cw.[reward_points],cw.[reward_tickets] "+
					",zc.id,zc.[cust_name]+' '+ zc.[last_name] ,zc.[email],zc.phone, spc.state_name, ip.cust_ip_address "+
					") cd order by cd.contestStartDateTime, cd.contestID, cd.customerName";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<Object[]> getContestDetailedTrackingReportData(String fromDate,String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {

			String sql ="SELECT contestName,contestDate,contestTime,contestDay,contestWeekDay,contestMonth,contestYear,contestCategory"
						+"	,contestArtist,contestTotalNoOfQuestions,customerNo,customerName ,customerEmail,CustomerPhoneNumber"
						+"	,customerPhoneStateName,customerIPAddress,customerPlatform,customerSignUpDate,noOfQuestionsAttempted"
						+"	,noOfQuestionsAnswered,rewardPointsEarnedAtContest,rewardPointsEarnedBySharing"
						+"	,CONVERT(DECIMAL(12,2),rewardPointsEarnedAtContest+rewardPointsEarnedBySharing) as totalRewardPointsEarned"
						+"	,rewardTicketsEarned,lifeLineUsed,referralCustomerName,referralCustomerSignupDate,referralCustomerPhone"
						+"	,contestType,contestNo,contestStartDateTime "
						+"FROM (SELECT c.contest_name as contestName,CONVERT(VARCHAR,c.contest_start_datetime,101) as contestDate "
						+"			,iif(c.contest_start_datetime is not null, LTRIM(RIGHT(CONVERT(VARCHAR, c.contest_start_datetime, 100), 7)), 'TBD') as contestTime"
						+"			,DATENAME(DAY,c.contest_start_datetime) as contestDay"
						+"			,DATENAME(WEEKDAY,c.contest_start_datetime) as contestWeekDay"
						+"			,DATENAME(MONTH,c.contest_start_datetime) as contestMonth"
						+"			,DATENAME(YEAR,c.contest_start_datetime) as contestYear"
						+"			,c.promo_ref_type as contestCategory,c.promo_ref_name as contestArtist"
						+"			,c.no_of_questions as contestTotalNoOfQuestions,cca.customer_id as customerNo"
						+"			,zc.contest_customer_name as customerName ,zc.contest_customer_email as customerEmail"
						+"			,zc.contest_customer_phone as CustomerPhoneNumber"
						+"			,spc.state_name as customerPhoneStateName,ip.cust_ip_address as customerIPAddress"
						+"			,oc.application_platform as customerPlatform,zc.contest_customer_signup_date as customerSignUpDate"
						+"			,count(cca.question_sl_no) as noOfQuestionsAttempted"
						+"			,sum(iif(cca.is_correct_answer=1, 1, 0)) as noOfQuestionsAnswered"
						+"			,CONVERT(DECIMAL(12,2),sum(iif(cca.answer_rewards > 0, answer_rewards, 0))) as rewardPointsEarnedAtContest"
						+"			,CONVERT(DECIMAL(12,2),iif(cw.reward_points > 0, cw.reward_points, 0)) as rewardPointsEarnedBySharing"
						+"			,iif(cw.reward_tickets > 0, cw.reward_tickets, 0) as rewardTicketsEarned"
						+"			,sum(iif(cca.is_lifeline_used > 0, is_lifeline_used, 0)) as lifeLineUsed"
						+"			,crt.referral_customer_id as referralCustomerID,rc.contest_customer_name as referralCustomerName"
						+"			,rc.contest_customer_signup_date as referralCustomerSignupDate,rc.contest_customer_phone as referralCustomerPhone"
						+"			,c.contest_type as contestType,c.id as contestNo,c.contest_start_datetime as contestStartDateTime"
						+"	FROM customer_contest_answers cca"
						+"	INNER JOIN contest_registered_customer zc on cca.customer_id = zc.contest_customer_id"
						+"	LEFT JOIN contest_winners cw on cca.contest_id = cw.contest_id and cca.customer_id = cw.customer_id"
						+"	LEFT JOIN contest c on cca.contest_id = c.id "
						+"	LEFT JOIN "+sharedProperty.getDatabasAlias()+"customer oc on  cca.customer_id = oc.id"
						+"	LEFT JOIN (SELECT max(ip_address) as cust_ip_address, customer_id, contest_id as contestCid"
						+"				FROM contest_participants WITH(NOWAIT)"
						+"				WHERE contest_id is not null and customer_id is not null and  customer_id > 0"
						+"				GROUP BY contest_id, customer_id"
						+"				) ip on cca.customer_id = ip.customer_id and cca.contest_id = ip.contestCid"
						+"	OUTER APPLY ( SELECT customer_id, referral_customer_id FROM customer_referral_tracking "
						+"				  WHERE cca.customer_id = customer_id ) crt"
						+"	OUTER APPLY ( SELECT contest_customer_name, contest_customer_signup_date, contest_customer_phone "
						+"				  FROM contest_registered_customer "
						+"				  WHERE crt.referral_customer_id = contest_customer_id ) rc "
						+"	OUTER APPLY ( SELECT state_name from "+sharedProperty.getDatabasAlias()+"state_phonecodes "
						+"				  WHERE CONVERT(varchar(4),phone_code) = LEFT(zc.contest_customer_phone,3)	) spc "
						+"	WHERE  c.status = 'EXPIRED' ";
						if(fromDate!=null && !fromDate.isEmpty()){
							fromDate += " 00:00:00.000";
							sql+= " AND convert(datetime,c.contest_start_datetime) >= '"+fromDate+"' ";
						}
						if(toDate!=null && !toDate.isEmpty()){
							toDate += " 23:59:59:000";
							sql+= " AND convert(datetime,c.contest_start_datetime) <= '"+toDate+"' ";
						}
						sql += "AND c.is_test_contest = 0 "
						+"	GROUP BY c.id,c.contest_name, c.contest_start_datetime,c.no_of_questions,c.contest_type,cca.customer_id,cca.contest_id"
						+"	, cw.reward_points,cw.reward_tickets,zc.contest_customer_id,zc.contest_customer_name ,zc.contest_customer_email,zc.contest_customer_phone, spc.state_name"
						+"	, ip.cust_ip_address,c.promo_ref_type,c.promo_ref_name,oc.application_platform,zc.contest_customer_signup_date"
						+"	, crt.referral_customer_id, rc.contest_customer_name, rc.contest_customer_signup_date, rc.contest_customer_phone"
						+"	) cdtr order by cdtr.contestStartDateTime, cdtr.contestNo, cdtr.customerName";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<Object[]> getCustomerPurchaseRewardDollarsReportData(String fromDate,String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			String sql ="	SELECT CustomerName, CustomerEmail, CustomerPhone, PaymentMethod, OrderType , InvoiceNo, InvoiceCreatedDate, InvoiceStatus, CustomerOrderNo "
						+"		, CustomerOrderCreatedDate, PurchaseOrderNo, PurchaseOrderCreatedDate, EventName, EventDate, EventTime, VenueName, VenueCity, Zone "
						+"		, Quantity, Section, Row, Seat, ListingPrice, PromotionalCodeDiscAmount, LoyalfanDiscountAmt, OrderTotal, Cost, TicketCost "
						+"		, CONVERT(DECIMAL(10, 2), (((CHARGE_PAYMENT*3/100))+REWARDS)) as OtherCost  "
						+"		, CONVERT(DECIMAL(10, 2), RewardSpentAmount) as RewardAmountSpent  "
						+"		, CONVERT(DECIMAL(10, 2), BalanceRewardAmount) as RewardAmountBalance  "
						+"		, CONVERT(DECIMAL(10, 2), ((CHARGE_PAYMENT-(CHARGE_PAYMENT*3/100))+NO_CHARGE_PAYMENT)) as NetTotal  "
						+"		, CONVERT(DECIMAL(10, 2), (((CHARGE_PAYMENT-(CHARGE_PAYMENT*3/100))+NO_CHARGE_PAYMENT) - TicketCost )) as ProfitLoss  "
						+"		, Platform, PromotionOfferUsed, promoCode, promotionalDiscount, LoyalFanDiscount, ReferralCode, ReferralEmail  "
						+"		, CONVERT(DECIMAL(10, 2), (IIF(DiscountAmountCal > 0, DiscountAmountCal, 0)) ) as DiscountAmount  "
						+"		, CONVERT(INT, ( (IIF(DiscountAmountCal > 0, DiscountAmountCal, 0) /  "
						+"			IIF(ticketTotalPrice is null OR ticketTotalPrice=0, 1, ticketTotalPrice) ) * 100 ) ) as PercentageDiscountAmount  "
						+"		, InvCreatedDate, COCreatedDate, POCreatedDate  "
						+"	FROM (	SELECT i.id as InvoiceNo, i.status as InvoiceStatus, co.id as CustomerOrderNo, p.id as PurchaseOrderNo  "
						+"				, CONVERT(VARCHAR(10), i.created_date, 101) + ' ' +CONVERT(VARCHAR(10), i.created_date, 108) as InvoiceCreatedDate  "
						+"				, CONVERT(VARCHAR(10), co.created_date, 101) + ' ' +CONVERT(VARCHAR(10), co.created_date, 108) as CustomerOrderCreatedDate  "
						+"				, CONVERT(VARCHAR(10), p.created_time, 101) + ' ' +CONVERT(VARCHAR(10), p.created_time, 108) as PurchaseOrderCreatedDate  "
						+"				, CONVERT(VARCHAR(500), co.event_name) as EventName  "
						+"				, CONVERT(VARCHAR(19), co.event_date, 101) as EventDate  "
						+"				, CASE WHEN co.event_time is not null THEN CONVERT(VARCHAR(20), co.event_time, 108)  "
						+"					WHEN co.event_time is null THEN 'TBD' END as EventTime  "
						+"				, co.venue_name as VenueName, co.venue_city as VenueCity, CONVERT(VARCHAR(50), co.section) as Zone, co.quantity as Quantity "
						+"				, co.actual_section as Section, CONVERT(VARCHAR(50), co.row) as Row, co.seat_details as Seat "
						+"				, CONVERT(VARCHAR(356), CONCAT(c.cust_name, ' ', c.last_name)) as CustomerName  "
						+"				, CONVERT(VARCHAR(255), c.email) as CustomerEmail, c.phone as CustomerPhone "
						+"				, CONVERT(DECIMAL(10, 2), co.original_order_total) as ListingPrice "
						+"				, CONVERT(DECIMAL(10, 2), pro.additional_discount_amount) as PromotionalCodeDiscAmount "
						+"				, CONVERT(DECIMAL(10, 2), IIF(co.order_type='LOYALFAN', co.discount_amt-isnull(pro.additional_discount_amount, 0), NULL)) as LoyalfanDiscountAmt "
						+"				, CONVERT(DECIMAL(10, 2), co.order_total ) as OrderTotal "
						+"				, CONVERT(DECIMAL(10, 2), CASE WHEN ic.TicketCost is not null THEN ic.TicketCost else 0 END ) as Cost "
						+"				, CONVERT(DECIMAL(10, 2), CASE WHEN tc.InvoiceTicketCost is not null THEN tc.InvoiceTicketCost else 0 END ) as TicketCost "
						+"				, CONVERT(DECIMAL(10, 2), (IIF(co.primary_payment_method in ('PAYPAL', 'CREDITCARD', 'IPAY', 'GOOGLEPAY'), co.primary_payment_amount, 0) +  "
						+"					IIF(co.secondary_payment_method in ('PAYPAL', 'CREDITCARD', 'IPAY', 'GOOGLEPAY'), co.secondary_payment_amount, 0)+  "
						+"					IIF(co.third_payment_method in ('PAYPAL', 'CREDITCARD', 'IPAY', 'GOOGLEPAY'), co.third_payment_amount, 0))) as CHARGE_PAYMENT "
						+"				, CONVERT(DECIMAL(10, 2), (IIF(co.primary_payment_method in ('FULL_REWARDS', 'PARTIAL_REWARDS'), co.primary_payment_amount, 0) +  "
						+"					IIF(co.secondary_payment_method in ('FULL_REWARDS', 'PARTIAL_REWARDS'), co.secondary_payment_amount, 0)+  "
						+"					IIF(co.third_payment_method in ('FULL_REWARDS', 'PARTIAL_REWARDS'), co.third_payment_amount, 0))) as REWARDS "
						+"				, CONVERT(DECIMAL(10, 2), (IIF(co.primary_payment_method in ('ACCOUNT_RECIVABLE', 'BANK_OR_CHEQUE', 'CUSTOMER_WALLET'), co.primary_payment_amount, 0) +  "
						+"					IIF(co.secondary_payment_method in ('ACCOUNT_RECIVABLE', 'BANK_OR_CHEQUE', 'CUSTOMER_WALLET'), co.secondary_payment_amount, 0)+  "
						+"					IIF(co.third_payment_method in ('ACCOUNT_RECIVABLE', 'BANK_OR_CHEQUE', 'CUSTOMER_WALLET'), co.third_payment_amount, 0))) as NO_CHARGE_PAYMENT "
						+"				, CONVERT(VARCHAR(20), co.order_type) as OrderType, co.platform as Platform "
						+"				, CONVERT(VARCHAR(152), IIF(co.primary_payment_method is null OR co.primary_payment_method='NULL' , ' ' , co.primary_payment_method)+  "
						+"					IIF(co.secondary_payment_method is null OR co.secondary_payment_method='NULL' , ' ' , ', '+ co.secondary_payment_method)+  "
						+"					IIF(co.third_payment_method is null OR co.third_payment_method='NULL' , ' ' , ', '+ co.third_payment_method)) As PaymentMethod "
						+"				, CASE WHEN pro.id is not null AND (pro.offer_type='NORMAL_PROMO' OR pro.offer_type='CUSTOMER_PROMO') THEN 'Yes' ELSE 'No' END as PromotionOfferUsed "
						+"				, IIF(pro.offer_type='NORMAL_PROMO' OR pro.offer_type='CUSTOMER_PROMO', pro.promo_code, '') as promoCode "
						+"				, CASE WHEN (pro.offer_type='NORMAL_PROMO' OR pro.offer_type='CUSTOMER_PROMO')  "
						+"					AND pro.is_flat_discount=0 THEN replace(convert(varchar(10), pro.additional_discount_conv*100), '.00', '')+'%'  "
						+"					ELSE CASE WHEN (pro.offer_type='NORMAL_PROMO' OR pro.offer_type='CUSTOMER_PROMO')  "
						+"					AND pro.is_flat_discount=1 THEN convert(varchar(10), pro.additional_discount_conv) ELSE '' END END AS promotionalDiscount "
						+"				, IIF(co.order_type='LOYALFAN', '10%', '') as LoyalFanDiscount "
						+"				, IIF(pro.offer_type='CUSTOMER_REFERAL_DISCOUNT_CODE', pro.promo_code, '') AS ReferralCode "
						+"				, IIF(pro.offer_type='CUSTOMER_REFERAL_DISCOUNT_CODE', cust.email, '') AS ReferralEmail "
						+"				, IIF(clrh.reward_spent_amount is null, 0, clrh.reward_spent_amount) as RewardSpentAmount "
						+"				, IIF(clrh.balance_reward_amount is null, 0, clrh.balance_reward_amount) as BalanceRewardAmount "
						+"				, CONVERT(DECIMAL(10, 2), (IIF(co.category_ticket_group_id > 0, (ctgc.t_price * co.quantity), tc.t_price) ) ) as ticketTotalPrice "
						+"				, CONVERT(DECIMAL(10, 2), (IIF(co.category_ticket_group_id > 0, (ctgc.t_sold_price * co.quantity), tc.t_sold_price) ) ) as ticketTotalSoldPrice "
						+"				, CONVERT(DECIMAL(10, 2), (IIF(co.category_ticket_group_id > 0, (ctgc.t_price * co.quantity), tc.t_price) -  "
						+"					IIF(co.category_ticket_group_id > 0, (ctgc.t_sold_price * co.quantity), tc.t_sold_price) ) ) as DiscountAmountCal "
						+"				, i.created_date as InvCreatedDate, co.created_date as COCreatedDate, p.created_time as POCreatedDate  "
						+"			FROM  "+sharedProperty.getDatabasAlias()+"customer_order co  "
						+"			INNER JOIN  "+sharedProperty.getDatabasAlias()+"customer c on c.id=co.customer_id AND c.is_test_account = 0  "
						+"			INNER JOIN  "+sharedProperty.getDatabasAlias()+"invoice i on i.order_id = co.id AND i.status <> 'VOIDED'  "
						+"			LEFT JOIN  "+sharedProperty.getDatabasAlias()+"rtf_promotional_order_tracking_new pro on pro.order_id=co.id AND pro.status='COMPLETED'  "
						+"			LEFT JOIN  "+sharedProperty.getDatabasAlias()+"customer cust on pro.referrer_cust_id=cust.id  "
						+"			LEFT JOIN  "+sharedProperty.getDatabasAlias()+"cust_loyalty_reward_history clrh on co.id = clrh.Order_id  "
						+"			AND co.customer_id = clrh.customer_id AND clrh.status <> 'VOIDED'  "
						+"			OUTER APPLY (	SELECT invoice_id, sum(cost) as TicketCost, purchase_order_id  "
						+"							FROM  "+sharedProperty.getDatabasAlias()+"ticket WHERE invoice_id is not null AND invoice_id <> 0 AND invoice_id = i.id  "
						+"							GROUP BY invoice_id, purchase_order_id) ic  "
						+"			OUTER APPLY (	SELECT invoice_id, sum(cost) as InvoiceTicketCost, sum(retail_price) as t_price, sum(actual_sold_price) as t_sold_price  "
						+"							FROM  "+sharedProperty.getDatabasAlias()+"ticket WHERE invoice_id is not null AND invoice_id <> 0 AND invoice_id = i.id  "
						+"							GROUP BY invoice_id) tc  "
						+"			OUTER APPLY (	SELECT invoice_id, sum(cost) as InvoiceTicketCost, sum(price) as t_price,sum(sold_price) as t_sold_price  "
						+"							FROM  "+sharedProperty.getDatabasAlias()+"category_ticket_group WHERE invoice_id = i.id AND id = co.category_ticket_group_id  "
						+"							GROUP BY invoice_id) ctgc  "
						+"			LEFT JOIN  "+sharedProperty.getDatabasAlias()+"purchase_order p on ic.purchase_order_id = p.id  "
						+"			WHERE co.order_type not in ('CONTEST') AND (co.primary_payment_method in ('FULL_REWARDS', 'PARTIAL_REWARDS')  "
						+"				OR secondary_payment_method in ('FULL_REWARDS', 'PARTIAL_REWARDS') OR third_payment_method in ('FULL_REWARDS', 'PARTIAL_REWARDS'))  "
						+"				AND c.is_test_account = 0  "
						+"	) salesRep ORDER BY salesRep.InvCreatedDate, salesRep.CustomerName; ";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<Object[]> getContestCustomerRefferalPercentageReportData(String fromDate,String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			String sql ="SELECT noOfContestantsParticipated, noOfCustomersReferred, "+
					"CONVERT(DECIMAL(10,2),(CONVERT(DECIMAL(12,4),(noOfCustomersReferred *1.0/ noOfContestantsParticipated)) * 100)) "+
					"as '%Referrals' "+
					"FROM (SELECT CONVERT(int,MAX(noOfContestantParticipated)) as noOfContestantsParticipated,  "+
					"CONVERT(INT,MAX(noOfCustomerReferred)) as noOfCustomersReferred "+
					"FROM (SELECT count(1) as noOfContestantParticipated, 0 as noOfCustomerReferred "+
					"FROM (SELECT cp.[customer_id] as customerID "+
					",c.[cust_name]+ ' '+c.[last_name] as customerName,max(cp.join_datetime) as contestJoin "+
					"FROM contest co "+
					"inner join contest_participants cp on co.id = cp.contest_id "+
					"inner join "+sharedProperty.getDatabasAlias()+"customer c on  cp.customer_id = c.id "+
					"where c.is_test_account = 0 "+
					"and co.status = 'EXPIRED' ";

					if(fromDate!=null && !fromDate.isEmpty()){
						fromDate += " 00:00:00.000";
						sql+= " AND convert(datetime,co.contest_start_datetime) >= '"+fromDate+"' ";
					}
					if(toDate!=null && !toDate.isEmpty()){
						toDate += " 23:59:59:000";
						sql+= " AND convert(datetime,co.contest_start_datetime) <= '"+toDate+"' ";
					}

					sql += "and co.is_test_contest = 0 "+
					"group by cp.[customer_id],c.[cust_name]+ ' '+c.[last_name]  "+
					") conParti "+
					"UNION "+
					"SELECT 0 as noOfContestantParticipated, sum(noOfReferrals) as noOfCustomerReferred "+
					"FROM  ( SELECT 'Total Customer:' as BlankColumn, customerID, customerName,  "+
					"count(referralCustomerID) as noOfReferrals "+
					"FROM (SELECT customerID, customerName, referralCustomerID "+
					"FROM (SELECT cp.[customer_id] as customerID "+
					",c.[cust_name]+ ' '+c.[last_name] as customerName,cp.contest_id as contestID "+
					",max(cp.join_datetime) as contestJoin,crt.customer_id as referralCustomerID "+
					"FROM "+sharedProperty.getDatabasAlias()+"customer c "+
					"left join contest_participants cp on c.id = cp.customer_id "+
					"left join contest co on  co.id = cp.contest_id "+
					"left join customer_referral_tracking crt on c.id =crt.referral_customer_id "+
					"where c.is_test_account = 0 and c.product_type='REWARDTHEFAN' "+
					"and co.status = 'EXPIRED' ";

					if(fromDate!=null && !fromDate.isEmpty()){
						sql+= " AND convert(datetime,co.contest_start_datetime) >= '"+fromDate+"' ";
					}
					if(toDate!=null && !toDate.isEmpty()){
						sql+= " AND convert(datetime,co.contest_start_datetime) <= '"+toDate+"' ";
					}

					sql += "and co.is_test_contest = 0 "+
					"group by cp.[customer_id],c.[cust_name]+ ' '+c.[last_name] , cp.contest_id, crt.customer_id "+
					") conRefCus "+
					"group by conRefCus.customerID, conRefCus.customerName, conRefCus.referralCustomerID "+
					") cusRefCou "+
					"group by cusRefCou.customerID, cusRefCou.customerName "+
					") sumCusRef) maxCusVal) percentageRef";


			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}



	@Override
	public List<Object[]> getCustomerWiseRewardDollarsReportData(String fromDate,String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			String sql =	"	SELECT contestParticipant, noOfContestPlayed, totalContestRewards, averageContestRewards "
							+"	FROM (	SELECT ltrim(rtrim(contestCustomerName)) as contestParticipant  "
							+"				,COUNT(contestNo) as noOfContestPlayed,SUM(contestRewardsTotal) as totalContestRewards  "
							+"				,CONVERT(DECIMAL(12,3),AVG(contestRewardsTotal)) as averageContestRewards  "
							+"			FROM (	SELECT contestNo,contestCustomerNo,contestCustomerName,sum(contestRewards) as contestRewardsTotal  "
							+"					FROM (	SELECT cca.contest_id as contestNo,convert(int,cca.customer_id) as contestCustomerNo  "
							+"								,cu.contest_customer_name as contestCustomerName "
							+"								,convert(datetime,ct.contest_start_datetime)  as contestDate "
							+"								,iif(sum(cca.answer_rewards) is null,0,sum(cca.answer_rewards) ) as contestRewards  "
							+"							FROM customer_contest_answers cca  "
							+"							INNER JOIN contest ct on cca.contest_id = ct.id  "
							+"							INNER JOIN contest_registered_customer cu on cca.customer_id = cu.contest_customer_id "
							+"							WHERE ct.is_test_contest = 0 and ct.status = 'EXPIRED'  " ;
							if(fromDate!=null && !fromDate.isEmpty()){
								fromDate += " 00:00:00.000";
								sql+= " 				AND convert(datetime,ct.contest_start_datetime) >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								toDate += " 23:59:59:000";
								sql+= " 				AND convert(datetime,ct.contest_start_datetime) <= '"+toDate+"' ";
							}
							sql += "					GROUP BY cca.contest_id, cca.customer_id, cu.contest_customer_name,  "
							+"								convert(datetime,ct.contest_start_datetime)  "
							+"							UNION  "
							+"							SELECT cca.contest_id as contestNo,convert(int,cca.customer_id) as contestCustomerNo  "
							+"								,cu.contest_customer_name as contestCustomerName  "
							+"								,convert(datetime,ct.contest_start_datetime) as contestDate  "
							+"								,iif(sum(cca.reward_points) is null,0,sum(cca.reward_points) ) as contestRewards  "
							+"							FROM contest_winners cca  "
							+"							INNER JOIN contest ct on cca.contest_id = ct.id  "
							+"							INNER JOIN contest_registered_customer cu on cca.customer_id = cu.contest_customer_id "
							+"							WHERE ct.is_test_contest = 0 and ct.status = 'EXPIRED'  " ;
							if(fromDate!=null && !fromDate.isEmpty()){
								sql+= " 				AND convert(datetime,ct.contest_start_datetime) >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								sql+= " 				AND convert(datetime,ct.contest_start_datetime) <= '"+toDate+"' ";
							}
							sql += "					GROUP BY cca.contest_id, cca.customer_id, cu.contest_customer_name,  "
							+"								convert(datetime,ct.contest_start_datetime)  "
							+"						)cr GROUP BY contestNo, contestCustomerNo, contestCustomerName  "
							+"					) cau GROUP BY cau.contestCustomerNo, cau.contestCustomerName  "
							+"			) main ORDER BY main.contestParticipant ";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}



	@Override
	public List<Object[]> getCustomerReferredByReferredToReportDataOld(String fromDate,String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			String sql ="SELECT ReferredByCustomerIDa, ReferredByUserID, COUNT(ReferredToCustomerID) as ReferredCount "+
						"FROM (SELECT crt.referral_customer_id as ReferredByCustomerIDa, "+
						"c.user_id as ReferredByUserID,crt.customer_id as ReferredToCustomerID "+
						",ca.user_id as ReferredToUserID "+
						"FROM "+sharedProperty.getDatabasAlias()+"customer c "+
						"inner join customer_referral_tracking crt on c.id =crt.referral_customer_id  "+
						"inner join "+sharedProperty.getDatabasAlias()+"customer ca on crt.customer_id =ca.id "+
						"where 1=1  ";

						if(fromDate!=null && !fromDate.isEmpty()){
							fromDate += " 00:00:00.000";
							sql+= "and convert(datetime,crt.created_datetime) >= '"+fromDate+"' ";
						}
						if(toDate!=null && !toDate.isEmpty()){
							toDate += " 23:59:59:000";
							sql+= "and convert(datetime,crt.created_datetime) <= '"+toDate+"' ";
						}

						sql += "group by crt.referral_customer_id,c.user_id "+
						",crt.customer_id,ca.user_id) cnt "+
						"group by cnt.ReferredByCustomerIDa, cnt.ReferredByUserID "+
						"order by cnt.ReferredByUserID";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}


	@Override
	public List<Object[]> getCustomerReferredByReferredToReportData(String fromDate,String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			String sql ="select t.referral_customer_id as ReferreID, "+
					"c.user_id as ReferreUserID, t.customer_id as ReferToCustomerID,cc.user_id as ReferToCustomerUserID, "+
					"t.created_datetime as CodeAppliedDate,cc.signup_date as ReferToSignUpDate "+
					"from customer_referral_tracking t join "+sharedProperty.getDatabasAlias()+"customer c on c.id=t.referral_customer_id "+
					"join  "+sharedProperty.getDatabasAlias()+"customer cc on t.customer_id=cc.id where c.is_test_account=0 ";

					if(fromDate!=null && !fromDate.isEmpty()){
						fromDate += " 00:00:00.000";
						sql+= "and convert(datetime,t.created_datetime) >= '"+fromDate+"' ";
					}
					if(toDate!=null && !toDate.isEmpty()){
							toDate += " 23:59:59:000";
							sql+= "and convert(datetime,t.created_datetime) <= '"+toDate+"' ";
					}

					sql += "order by t.id desc";
			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}


	@Override
	public int getUniqueReferree(String fromDate,String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			String sql ="select distinct t.referral_customer_id as ReferreID "+
					"from customer_referral_tracking t join "+sharedProperty.getDatabasAlias()+"customer c on c.id=t.referral_customer_id "+
					"join "+sharedProperty.getDatabasAlias()+"customer cc on t.customer_id=cc.id  where c.is_test_account=0 ";

					if(fromDate!=null && !fromDate.isEmpty()){
						fromDate += " 00:00:00.000";
						sql+= "and convert(datetime,t.created_datetime) >= '"+fromDate+"' ";
					}
					if(toDate!=null && !toDate.isEmpty()){
							toDate += " 23:59:59:000";
							sql+= "and convert(datetime,t.created_datetime) <= '"+toDate+"' ";
					}

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list.size();
	}



	@Override
	public List<Object[]> getReferreRefferalsReportData(String fromDate,String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			String sql =	"	SELECT ReferreID, ReferreUserID, ReferToCustomerID, ReferToCustomerUserID, CodeAppliedDate, ReferToSignUpDate "
							+"	       , registeredCustomer, uniqueeReferree, totalReferrals, sheetDate "
							+"	FROM rtf_rep_referee_or_referrals_daily_report  with(nolock) "
							+"	WHERE 1=1 ";
							if(fromDate!=null && !fromDate.isEmpty()){
								fromDate += " 00:00:00.000";
								sql+= "and convert(datetime,CodeAppliedDate) >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
									toDate += " 23:59:59:000";
									sql+= "and convert(datetime,CodeAppliedDate) <= '"+toDate+"' ";
							}
							sql += "order by CodeAppliedDate desc";
			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}


	@Override
	public List<Object[]> getRegisteredCustomerReportData(String fromDate,String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			String sql ="SELECT * FROM contest_registered_customer WHERE 1=1 ";
					if(fromDate!=null && !fromDate.isEmpty()){
						fromDate += " 00:00:00.000";
						sql+= "AND CONVERT(datetime,contest_customer_signup_date) >= '"+fromDate+"' ";
					}
					if(toDate!=null && !toDate.isEmpty()){
						toDate += " 23:59:59:000";
						sql+= "AND CONVERT(datetime,contest_customer_signup_date) <= '"+toDate+"' ";
					}
					sql += "ORDER BY contest_customer_name";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}


	@Override
	public List<Object[]> getContestParticipantsAverageReportData(String fromDate,String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			String sql =	"	SELECT  customerID, customerName, count(referralCustomerID) as noOfReferrals "
							+"	FROM (	SELECT customerID, customerName, referralCustomerID "
							+"			FROM (	SELECT cp.customer_id as customerID, c.contest_customer_name as customerName "
							+"						, cp.contest_id as contestID, max(cp.join_datetime) as contestJoin "
							+"						, crt.customer_id as referralCustomerID "
							+"					FROM contest_registered_customer c "
							+"					LEFT JOIN contest_participants cp on c.contest_customer_id = cp.customer_id "
							+"					LEFT JOIN contest co on  co.id = cp.contest_id "
							+"					LEFT JOIN customer_referral_tracking crt on c.contest_customer_id = crt.referral_customer_id "
							+"					WHERE co.is_test_contest = 0 and co.status = 'EXPIRED' " ;
							if(fromDate!=null && !fromDate.isEmpty()){
								fromDate += " 00:00:00.000";
								sql+= " AND convert(datetime,co.contest_start_datetime) >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								toDate += " 23:59:59:000";
								sql+= " AND convert(datetime,co.contest_start_datetime) <= '"+toDate+"' ";
							}
							if(fromDate!=null && !fromDate.isEmpty()){
								sql+= " AND convert(datetime,crt.created_datetime) >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								sql+= " AND convert(datetime,crt.created_datetime) <= '"+toDate+"' ";
							}
							sql += "					GROUP BY cp.customer_id,c.contest_customer_name , cp.contest_id, crt.customer_id "
							+"			) conRefCus "
							+"			GROUP BY conRefCus.customerID, conRefCus.customerName, conRefCus.referralCustomerID "
							+"	) cusRefCou "
							+"	GROUP BY cusRefCou.customerID, cusRefCou.customerName ";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	
	
	
	@Override
	public Integer getRegisteredAndPlayedCustomerCount(String fromDate,String toDate,SharedProperty sharedProperty) {
		Integer count = 0;
		try {
			String sql =	"SELECT count(distinct CustomerNo) as partcipantsCount "+
					"FROM  (SELECT rc.contest_customer_id as CustomerNo, rc.contest_customer_name as CustomerName "+
					", rc.contest_customer_user_id as customerUserID, rc.contest_customer_phone as CustomerPhone "+
					", rc.contest_customer_email as CustomeerEmail "+
					", rc.contest_customer_signup_date as CustomerSignUpDate "+
					"FROM contest_registered_customer rc  with(nolock) "+
					"INNER JOIN "+sharedProperty.getDatabasAlias()+"customer cu  with(nolock) on cu.id = rc.contest_customer_id "+
					"INNER JOIN (SELECT ca.customer_id FROM customer_contest_answers ca with(nolock) "+
					"inner join contest coo with(nolock) on ca.contest_id = coo.id "+
					"WHERE coo.status <> 'DELETED'  and coo.status = 'EXPIRED' and coo.contest_name not like '%TEST%' ";
					
					if(fromDate!=null && !fromDate.isEmpty()){
						fromDate += " 00:00:00.000";
						sql+= " AND convert(datetime,coo.contest_start_datetime) >= '"+fromDate+"' ";
					}
					if(toDate!=null && !toDate.isEmpty()){
						toDate += " 23:59:59:000";
						sql+= " AND convert(datetime,coo.contest_start_datetime) <= '"+toDate+"' ";
					}
					
					sql += "GROUP BY ca.customer_id "+
					"UNION "+
					"SELECT cp.customer_id FROM contest_participants cp with(nolock) "+
					"inner join contest cod with(nolock) on cp.contest_id = cod.id "+
					"WHERE cod.status <> 'DELETED'  and cod.status = 'EXPIRED' and cod.contest_name not like '%TEST%' ";
					if(fromDate!=null && !fromDate.isEmpty()){
						sql+= " AND convert(datetime,cod.contest_start_datetime) >= '"+fromDate+"' ";
					}
					if(toDate!=null && !toDate.isEmpty()){
						sql+= " AND convert(datetime,cod.contest_start_datetime) <= '"+toDate+"' ";
					}
					
					sql += "GROUP BY cp.customer_id "+
					") pc on rc.contest_customer_id = pc.customer_id "+
					"WHERE 1 = 1 and cu.is_test_account = 0) mst";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			count = (Integer) query.uniqueResult();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return count;
	}
	
	
	@Override
	public List<Object[]> getRegisteredAndPlayedCustomers(String fromDate,String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			String sql =	"SELECT CustomerNo, CustomerName, customerUserID, CustomerPhone, CustomeerEmail "+
					"FROM (SELECT rc.contest_customer_id as CustomerNo, rc.contest_customer_name as CustomerName "+
					", rc.contest_customer_user_id as customerUserID, rc.contest_customer_phone as CustomerPhone "+
					", rc.contest_customer_email as CustomeerEmail "+
					", rc.contest_customer_signup_date as CustomerSignUpDate "+
					"FROM contest_registered_customer rc  with(nolock) "+
					"INNER JOIN "+sharedProperty.getDatabasAlias()+"customer cu  with(nolock) on cu.id = rc.contest_customer_id "+
					"INNER JOIN (	SELECT ca.customer_id FROM customer_contest_answers ca with(nolock) "+
					"inner join contest coo with(nolock) on ca.contest_id = coo.id "+
					"WHERE coo.status <> 'DELETED'  and coo.status = 'EXPIRED' and coo.contest_name not like '%TEST%' ";
					if(fromDate!=null && !fromDate.isEmpty()){
						fromDate += " 00:00:00.000";
						sql+= " AND convert(datetime,coo.contest_start_datetime) >= '"+fromDate+"' ";
					}
					if(toDate!=null && !toDate.isEmpty()){
						toDate += " 23:59:59:000";
						sql+= " AND convert(datetime,coo.contest_start_datetime) <= '"+toDate+"' ";
					}
					
					sql += "GROUP BY ca.customer_id "+
					"UNION "+
					"SELECT cp.customer_id FROM contest_participants cp with(nolock) "+
					"inner join contest cod with(nolock) on cp.contest_id = cod.id "+
					"WHERE cod.status <> 'DELETED'  and cod.status = 'EXPIRED' and cod.contest_name not like '%TEST%' ";
					if(fromDate!=null && !fromDate.isEmpty()){
						sql+= " AND convert(datetime,cod.contest_start_datetime) >= '"+fromDate+"' ";
					}
					if(toDate!=null && !toDate.isEmpty()){
						sql+= " AND convert(datetime,cod.contest_start_datetime) <= '"+toDate+"' ";
					}
					sql += "GROUP BY cp.customer_id "+
					") pc on rc.contest_customer_id = pc.customer_id "+
					"WHERE 1 = 1 "+
					"and cu.is_test_account = 0 "+
					") mst "+
					"order by mst.customerUserID";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	



	@Override
	public List<Object[]> getCustomerReferralAverageReportData(String fromDate,String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			String sql =	"	SELECT  customerID, customerName, count(referralCustomerID) as noOfReferrals "
							+"	FROM (	SELECT customerID, customerName, referralCustomerID "
							+"			FROM (	SELECT cp.customer_id as customerID, c.contest_customer_name as customerName "
							+"						, cp.contest_id as contestID, max(cp.join_datetime) as contestJoin "
							+"						, crt.customer_id as referralCustomerID "
							+"					FROM contest_registered_customer c "
							+"					LEFT JOIN contest_participants cp on c.contest_customer_id = cp.customer_id "
							+"					LEFT JOIN contest co on  co.id = cp.contest_id "
							+"					LEFT JOIN customer_referral_tracking crt on c.contest_customer_id = crt.referral_customer_id "
							+"					WHERE co.is_test_contest = 0 and co.status = 'EXPIRED' " ;
							if(fromDate!=null && !fromDate.isEmpty()){
								fromDate += " 00:00:00.000";
								sql+= " AND convert(datetime,co.contest_start_datetime) >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								toDate += " 23:59:59:000";
								sql+= " AND convert(datetime,co.contest_start_datetime) <= '"+toDate+"' ";
							}
							if(fromDate!=null && !fromDate.isEmpty()){
								sql+= " AND convert(datetime,crt.created_datetime) >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								sql+= " AND convert(datetime,crt.created_datetime) <= '"+toDate+"' ";
							}
							sql += "			GROUP BY cp.customer_id,c.contest_customer_name , cp.contest_id, crt.customer_id "
							+"			) conRefCus "
							+"			GROUP BY conRefCus.customerID, conRefCus.customerName, conRefCus.referralCustomerID "
							+"	) cusRefCou "
							+"	GROUP BY cusRefCou.customerID, cusRefCou.customerName ";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}




	@Override
	public List<Object[]> getRegisteredCustomerExcludingBotsReportData(String fromDate,String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			String sql ="select * from contest_registered_customer WHERE 1=1 AND contest_customer_id not in("+
					"select CustomerId from RTFQuizMaster.dbo.bots_customer group by CustomerId) " ;
					if(fromDate!=null && !fromDate.isEmpty()){
						fromDate += " 00:00:00.000";
						sql+= "and convert(datetime,contest_customer_signup_date) >= '"+fromDate+"' ";
					}
					if(toDate!=null && !toDate.isEmpty()){
						toDate += " 23:59:59:000";
						sql+= "and convert(datetime,contest_customer_signup_date) <= '"+toDate+"' ";
					}
					sql += "order by contest_customer_name";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}



	@Override
	public Integer getTotalContestCount(String fromDate,String toDate) {
		Integer count = 0;
		try {
			String sql ="select count(distinct id) contestcount from contest co "+
		    "where co.is_test_contest = 0 and co.status = 'EXPIRED' ";
			if(fromDate!=null && !fromDate.isEmpty()){
				fromDate += " 00:00:00.000";
				sql+= "and convert(datetime,co.contest_start_datetime) >= '"+fromDate+"' ";
			}
			if(toDate!=null && !toDate.isEmpty()){
				toDate += " 23:59:59:000";
				sql+= "and convert(datetime,co.contest_start_datetime) <= '"+toDate+"' ";
			}

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			count = (Integer) query.uniqueResult();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return count;
	}


	@Override
	public List<Object[]> getContestParticipantsAverageexcludingBotsReportData(String fromDate,String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			String sql ="SELECT  customerID, customerName, count(referralCustomerID) as noOfReferrals "+
					"FROM (SELECT customerID, customerName, referralCustomerID "+
					"FROM (SELECT cp.[customer_id] as customerID "+
								  ",c.contest_customer_name as customerName "+
								  ",cp.contest_id as contestID "+
								  ",max(cp.join_datetime) as contestJoin "+
								  ",crt.customer_id as referralCustomerID "+
					"FROM [RTFQuizMaster].[dbo].[contest_registered_customer] c "+
								  "left join contest_participants cp on c.contest_customer_id = cp.customer_id "+
								  "left join contest co on  co.id = cp.contest_id "+
								  "OUTER APPLY (select customer_id from  customer_referral_tracking  "+
												"where 1=1 ";
								if(fromDate!=null && !fromDate.isEmpty()){
									fromDate += " 00:00:00.000";
									sql+= "and convert(datetime,created_datetime) >= '"+fromDate+"' ";
								}
								if(toDate!=null && !toDate.isEmpty()){
									toDate += " 23:59:59:000";
									sql+= "and convert(datetime,created_datetime) <= '"+toDate+"' ";
								}


						sql += "and referral_customer_id = c.contest_customer_id "+
								"and customer_id not in(select CustomerId from RTFQuizMaster.dbo.bots_customer group by CustomerId)) crt " +
							    "where co.is_test_contest = 0 and co.status = 'EXPIRED' ";

						if(fromDate!=null && !fromDate.isEmpty()){
								sql+= "and convert(datetime,co.contest_start_datetime) >= '"+fromDate+"' ";
						}
						if(toDate!=null && !toDate.isEmpty()){
							sql+= "and convert(datetime,co.contest_start_datetime) <= '"+toDate+"' ";
						}

						sql += " group by cp.[customer_id],c.contest_customer_name , cp.contest_id, crt.customer_id "+
						") conRefCus "+
						"group by conRefCus.customerID, conRefCus.customerName, conRefCus.referralCustomerID "+
				") cusRefCou "+
				"group by cusRefCou.customerID, cusRefCou.customerName";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}



	@Override
	public List<Object[]> getCustomerReferralAverageExcludingBotsReportData(String fromDate,String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			String sql ="SELECT customerID, customerName, count(referralCustomerID) as noOfReferrals "+
					"FROM (SELECT customerID, customerName, referralCustomerID "+
							"FROM (SELECT cp.[customer_id] as customerID "+
							  ",c.contest_customer_name as customerName "+
							  ",cp.contest_id as contestID "+
							  ",max(cp.join_datetime) as contestJoin "+
							  ",crt.customer_id as referralCustomerID "+
						 " FROM contest_registered_customer c "+
							 " left join contest_participants cp on c.contest_customer_id = cp.customer_id "+
							  "left join contest co on  co.id = cp.contest_id "+
							  "left join customer_referral_tracking crt on c.contest_customer_id =crt.referral_customer_id "+
						  "where co.is_test_contest = 0 and  co.status = 'EXPIRED' ";

							if(fromDate!=null && !fromDate.isEmpty()){
								fromDate += " 00:00:00.000";
								sql+= "and convert(datetime,co.contest_start_datetime) >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								toDate += " 23:59:59:000";
								sql+= "and convert(datetime,co.contest_start_datetime) <= '"+toDate+"' ";
							}
							if(fromDate!=null && !fromDate.isEmpty()){
								sql+= "and convert(datetime,crt.created_datetime) >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								sql+= "and convert(datetime,crt.created_datetime) <= '"+toDate+"' ";
							}

						 sql +=  "and crt.customer_id not in(select CustomerId from RTFQuizMaster.dbo.bots_customer group by CustomerId) "+
								 "group by cp.[customer_id],c.contest_customer_name , cp.contest_id, crt.customer_id "+
								 ") conRefCus "+
								 "group by conRefCus.customerID, conRefCus.customerName, conRefCus.referralCustomerID "+
									") cusRefCou "+
							"group by cusRefCou.customerID, cusRefCou.customerName";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}





	@Override
	public List<Object[]> getQuestionWiseUserAnswerCountReportData(String fromDate, String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			String sql =	"	SELECT  [contestID], [contestName], [contestStartDateTime], [No.OfParticipantsParticipated], [No.OfParticipantsAttempted] "
							+"		  , [FirstQuestionAttempted], [FirstQuestionAnswered], [%FirstQuestionAttempted], [%FirstQuestionAnswered] "
							+"		  , [SecondQuestionAttempted], [SecondQuestionAnswered], [%SecondQuestionAttempted], [%SecondQuestionAnswered] "
							+"		  , [ThirdQuestionAttempted], [ThirdQuestionAnswered], [%ThirdQuestionAttempted], [%ThirdQuestionAnswered] "
							+"		  , [FourthQuestionAttempted], [FourthQuestionAnswered], [%FourthQuestionAttempted], [%FourthQuestionAnswered] "
							+"		  , [FivethQuestionAttempted], [FivethQuestionAnswered], [%FivethQuestionAttempted], [%FivethQuestionAnswered] "
							+"		  , [SixthQuestionAttempted], [SixthQuestionAnswered], [%SixthQuestionAttempted], [%SixthQuestionAnswered] "
							+"		  , [SeventhQuestionAttempted], [SeventhQuestionAnswered], [%SeventhQuestionAttempted], [%SeventhQuestionAnswered] "
							+"		  , [EighthQuestionAttempted], [EighthQuestionAnswered], [%EighthQuestionAttempted], [%EighthQuestionAnswered] "
							+"		  , [NinethQuestionAttempted], [NinethQuestionAnswered], [%NinethQuestionAttempted], [%NinethQuestionAnswered] "
							+"		  , [TenthQuestionAttempted], [TenthQuestionAnswered], [%TenthQuestionAttempted], [%TenthQuestionAnswered] "
							+"		  , [TotalQuestionAnswered], [TotalNoOfQuestionsAttempted], [Overall%Answered], [OriginalContestStartDatetime] "
							+"	FROM rtf_rep_Contest_Question_Wise_User_Answer_Count_Report with(nolock) "
							+"	WHERE 1=1 ";
							if(fromDate!=null && !fromDate.isEmpty()){
								fromDate += " 00:00:00.000";
								sql+= "	AND CONVERT(datetime,OriginalContestStartDatetime) >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								toDate += " 23:59:59:000";
								sql+= "	AND CONVERT(datetime,OriginalContestStartDatetime) <= '"+toDate+"' ";
							}
							sql += "ORDER BY OriginalContestStartDatetime";


			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}



	@Override
	public List<Object[]> getCustomerLivesEarnedByReferralReportData(String fromDate,String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			String sql =	"	SELECT crt.referral_customer_id as customerID, c.contest_customer_name as customerName "
							+"		, count( distinct crt.customer_id) as noOfLivesEarned_ByReferrals "
							+"	FROM contest_registered_customer c "
							+"	INNER JOIN customer_referral_tracking crt on c.contest_customer_id =crt.referral_customer_id "
							+"	WHERE 1=1 ";
							if(fromDate!=null && !fromDate.isEmpty()){
								fromDate += " 00:00:00.000";
								sql+= "and convert(datetime,crt.created_datetime) >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								toDate += " 23:59:59:000";
								sql+= "and convert(datetime,crt.created_datetime) <= '"+toDate+"' ";
							}
							sql += "	GROUP BY crt.referral_customer_id,c.contest_customer_name	 ORDER BY 3 DESC ";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}

	public List<Object[]> getCustomerLivesUsedContestWiseReportData(String fromDate,String toDate,SharedProperty sharedProperty){
		List<Object[]> list = null;
		try {
			String sql =	"	SELECT ContestID, ContestName, ContestDateString, [No.OfLivesUsedPerGame], AvailableCustomersWhoReferred "
                            +"        , [Average#ofLivesUsedByReferrPerGame], ContestDate "
                            +"  FROM  rtf_rep_Customer_Lives_Earned_Report_sheet_Referrer_Life_Used   "
                            +"  WHERE 1=1 ";
                            if(fromDate!=null && !fromDate.isEmpty()){
								fromDate += " 00:00:00.000";
								sql+= "and convert(datetime,ContestDate) >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								toDate += " 23:59:59:000";
								sql+= "and convert(datetime,ContestDate) <= '"+toDate+"' ";
							}
						sql += "  ORDER BY ContestDate, ContestID ";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}

	public List<Object[]> getCustomerLivesUsedAllReportData(String fromDate,String toDate,SharedProperty sharedProperty){
		List<Object[]> list = null;
		try {
			String sql ="	SELECT ContestID, ContestName, ContestDateString, [No.OfLivesUsedPerGame], AvailableCustomersWhoReferred "
                            +"        , [Average#ofLivesUsedByReferrPerGame], ContestDate "
                            +"  FROM  rtf_rep_Customer_Lives_Earned_Report_sheet_Referrer_Life_Used   "
                            +"  WHERE 1=1 ";
                            if(fromDate!=null && !fromDate.isEmpty()){
								fromDate += " 00:00:00.000";
								sql+= "and convert(datetime,ContestDate) >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								toDate += " 23:59:59:000";
								sql+= "and convert(datetime,ContestDate) <= '"+toDate+"' ";
							}
						sql += "  ORDER BY ContestDate, ContestID ";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}


	

	public List<Object[]> getCustomerLivesUsedPercentageBreackDownReportData(String fromDate,String toDate,SharedProperty sharedProperty){
		List<Object[]> list = null;
		try {
			String sql =	"	SELECT ContestID, ContestName, ContestDate, QuestionNo, NoOfCustomersWhoAttempted, NoOfLifeLineUsed, "
							+"	       [%OfCustomersUsedLifeLine], NoOfCustomersWhohadLIVES, NoOfLIVESAvailable, [%OfLifeLineUsed] , ContestDateDate "
							+"	FROM rtf_rep_Customer_Lives_Earned_Report_sheet_Lives_Used_Percentage_Breakdown "
							+"	WHERE 1=1 ";
							if(fromDate!=null && !fromDate.isEmpty()){
								fromDate += " 00:00:00.000";
								sql+= "and convert(datetime,ContestDateDate) >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								toDate += " 23:59:59:000";
								sql+= "and convert(datetime,ContestDateDate) <= '"+toDate+"' ";
							}
							sql += "	order by ContestDateDate, ContestID, QuestionNo";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	
	
	public List<Object[]> getCustomerLivesUsedBreackDownReportData(String fromDate,String toDate,SharedProperty sharedProperty){
		List<Object[]> list = null;
		try {
			String sql ="select co.id as ContestID,co.contest_name as ContestName,CONVERT(VARCHAR(35),co.contest_start_datetime) as ContestDate, "+
						"cca.question_sl_no as QuestionNo,count(cca.is_lifeline_used) as 'No.OfLifeLineUsed' "+
						"from customer_contest_answers cca "+
						"inner join contest co on  cca.contest_id= co.id "+
						"inner join contest_registered_customer cgc on cca.customer_id = cgc.contest_customer_id "+
						"where co.is_test_contest = 0 and co.status = 'EXPIRED' ";

						if(fromDate!=null && !fromDate.isEmpty()){
							fromDate += " 00:00:00.000";
							sql+= "and convert(datetime,co.contest_start_datetime) >= '"+fromDate+"' ";
						}
						if(toDate!=null && !toDate.isEmpty()){
							toDate += " 23:59:59:000";
							sql+= "and convert(datetime,co.contest_start_datetime) <= '"+toDate+"' ";
						}
			
						sql += "and cca.is_lifeline_used=1 "+
						"group by co.id, co.contest_name, co.contest_start_datetime, cca.question_sl_no "+
						"order by co.contest_start_datetime, co.id, cca.question_sl_no;";		

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();
			
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	
	
	public boolean copyContestToHostApp(Integer contestId){
		boolean isCopied = false;
		try {
			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			//Query query = contestSession.createSQLQuery("EXEC SP_load_Contest_data_from_Prod_to_Sanbox").setParameter("contestId", contestId);
			//query.list();
			isCopied = true;
		} catch (Exception e) {
			isCopied = false;
		}
		return isCopied;
	}

	@Override
	public List<Object[]> getAvgQuestionAsweredByCustomerReportData(String fromDate, String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			String sql =	"	SELECT CustomerID, CustomerName, CustomerUserID, CustomerEmail ,NoOfContestGamesPlayedByAnswering "
							+"			,TotalNoOfQuestionsFromPlayedGame,TotalNoOfQuestionsAnswered "
							+"			,CONVERT(DECIMAL(10,3),((TotalNoOfQuestionsAnswered * 1.0) / TotalNoOfQuestionsFromPlayedGame)*100 ) as AveragePercentageAnsweredByCustomer "
							+"	FROM (	SELECT CustomerID, CustomerName, CustomerUserID, CustomerEmail ,Count(distinct ContestID) as NoOfContestGamesPlayedByAnswering "
							+"				,sum(NoOfQuestions) as TotalNoOfQuestionsFromPlayedGame ,sum(NoOFQuestionsAnswered) as TotalNoOfQuestionsAnswered "
							+"			FROM (	SELECT cca.customer_id as customerID ,c.contest_customer_name as customerName,c.contest_customer_user_id as CustomerUserID "
							+"						,c.contest_customer_email as CustomerEmail,co.id as ContestID ,co.no_of_questions as NoOfQuestions "
							+"						,count(cca.is_correct_answer) as NoOFQuestionsAnswered "
							+"					FROM contest_registered_customer c "
							+"					INNER JOIN customer_contest_answers cca on c.contest_customer_id = cca.customer_id "
							+"					INNER JOIN contest co on  co.id = cca.contest_id "
							+"					WHERE co.is_test_contest = 0 and co.status = 'EXPIRED' " ;
							if(fromDate!=null && !fromDate.isEmpty()){
								fromDate += " 00:00:00.000";
								sql+= "			and convert(datetime,co.contest_start_datetime) >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								toDate += " 23:59:59:000";
								sql+= "			and convert(datetime,co.contest_start_datetime) <= '"+toDate+"' ";
							}
					sql += 	" 					and cca.is_correct_answer = 1 "
							+"					GROUP BY cca.customer_id,c.contest_customer_name ,c.contest_customer_user_id,  "
							+"					co.id,c.contest_customer_email, co.no_of_questions 		) cgp "
							+"			GROUP BY cgp.CustomerID, cgp.CustomerName, cgp.CustomerUserID, cgp.CustomerEmail "
							+"		) finalQ "
							+"	ORDER BY finalQ.NoOfContestGamesPlayedByAnswering desc, finalQ.CustomerID ;";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<Object[]> getAvgNoOfGamPlayedByCustomerReportData(String fromDate, String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			String sql =	"	SELECT CustomerID, CustomerName, CustomerUserID, CustomerEmail, NoOfContestGamesPlayed, AveragePercentageContestPlayedByCustomer "
							+"	FROM (	SELECT CustomerID, CustomerName, CustomerUserID, CustomerEmail, Count(distinct ContestID) as NoOfContestGamesPlayed, "
							+"				CONVERT(DECIMAL(10,3),((Count(distinct ContestID) * 1.0) / coCnt.contestcount)*100 ) as AveragePercentageContestPlayedByCustomer "
							+"			FROM  (	SELECT cp.[customer_id] as customerID,c.contest_customer_name as customerName "
							+"						,c.contest_customer_user_id as CustomerUserID,c.contest_customer_email as CustomerEmail "
							+"						,cp.contest_id as ContestID,max(cp.join_datetime) as contestJoin "
							+"					FROM contest_registered_customer c "
							+"					LEFT JOIN ( SELECT contest_id, customer_id, max(join_datetime) as join_datetime FROM contest_participants GROUP BY contest_id, customer_id "
							+"								UNION	 "
							+"								SELECT contest_id, customer_id, min(created_datetime) as join_datetime FROM customer_contest_answers GROUP BY contest_id, customer_id ) "
							+"								cp on c.contest_customer_id = cp.customer_id "
							+"					LEFT JOIN contest co on  co.id = cp.contest_id "
							+"					where co.is_test_contest = 0 and co.status = 'EXPIRED' ";
							if(fromDate!=null && !fromDate.isEmpty()){
								fromDate += " 00:00:00.000";
								sql+= "			and convert(datetime,co.contest_start_datetime) >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								toDate += " 23:59:59:000";
								sql+= "			and convert(datetime,co.contest_start_datetime) <= '"+toDate+"' ";
							}
					sql+= 	"					group by cp.[customer_id],c.contest_customer_name ,c.contest_customer_user_id, cp.contest_id "
							+"					,c.contest_customer_email	) cgp "
							+"					cross apply (	SELECT count(distinct id) contestcount from contest co  "
							+"									where co.is_test_contest = 0 and co.status = 'EXPIRED'  ";
							if(fromDate!=null && !fromDate.isEmpty()){
								sql+= "							and convert(datetime,co.contest_start_datetime) >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								sql+= "							and convert(datetime,co.contest_start_datetime) <= '"+toDate+"' ";
							}
					sql+= 	"									) coCnt "
							+"		group by cgp.CustomerID, cgp.CustomerName, cgp.CustomerUserID, cgp.CustomerEmail, coCnt.contestcount "
							+"		) finalQ "
							+"	order by finalQ.NoOfContestGamesPlayed desc ; ";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<Object[]> getCustomerRewardDollarsReportData(String fromDate, String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			String sql =	"	SELECT customerNo,CustomerName, customerUserID, customerPhone, customeerEmail, earnedPoints, contestPoints, "
							+"			redeemedPoints, pendingPoints, activePoints, manualPoints, totalPoints "
							+"	FROM rtf_rep_Customer_Reward_Dollars_Statistics_Report "
							+"	ORDER BY earnedPoints desc ;";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}


	@Override
	public Map<String, Object> getAllBotsInformation(SharedProperty sharedProperty) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Object[]> list = null;
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			String startDate= "2018-08-11 00:01:00", endDate = dateFormat.format(new Date());

			String sql =	"	SELECT CustomerId , "
							+"		CASE WHEN email like '%@%' THEN CONVERT(VARCHAR(150),email) WHEN email not like '%@%' THEN CONVERT(VARCHAR(150),concat(email,'@botsrtf.com')) END as Email , "
							+"		CONVERT(VARCHAR(50),UserId) as UserId, "
							+"		CONVERT(VARCHAR(15),SignUpPhone) as Phone, "
							+"		CONVERT(VARCHAR(20), SignUpDate, 100) as SignUpDate, "
							+"		isOTPVerified as OTPVerified, "
							+"		CONVERT(VARCHAR(50),SignUpPlatform) as Platform, "
							+"		CONVERT(VARCHAR(1000),SignUpDeviceid) as device_id, "
							+"		CONVERT(VARCHAR(150),ReferralCode) as ReferralCode, "
							+"		CASE WHEN device_id_str is not null THEN CONVERT(VARCHAR(2000),device_id_str) WHEN device_id_str is null THEN '' END as AllDevices, "
							+"		CASE WHEN ipAddress  is not null THEN CONVERT(VARCHAR(1000),ipAddress) WHEN ipAddress  is null THEN '' END as IPAddress "
							+"	FROM bots_customer ORDER BY SignUpDeviceid asc ";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

			Set<String> referralCodeList = new HashSet<String>();
			List<BotDetail> botList = new ArrayList<BotDetail>();

			BotDetail botDetail = null;
			String refCode = null;
			for (Object[] object : list) {
				try {
					botDetail = new BotDetail();
					botDetail.setCustomerId((Integer)object[0]);
					botDetail.setEmail((String)object[1]);
					botDetail.setUserId((String)object[2]);
					botDetail.setPhoneNo((String)object[3]);
					botDetail.setSignupDate((String)object[4]);

					Boolean otpVerified = (Boolean)object[5];

					botDetail.setOtpVerified(otpVerified?"YES":"NO");
					botDetail.setSignUpPlatform((String)object[6]);
					botDetail.setSignUpDeviceId((String)object[7]);

					if(null == object[8]) {
						refCode = "";
					}else {
						refCode = (String)object[8];
						referralCodeList.add(refCode);
					}
					botDetail.setReferrerCode(refCode);
					botDetail.setAllLoggedInDevices((String)object[9]);
					botDetail.setAllIPAddress((String)object[10]);
					botList.add(botDetail);

				}catch(Exception e) {
					e.printStackTrace();
				}
			}
			resultMap.put("BotList", botList);
			resultMap.put("ReferralCodeList", referralCodeList);

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return resultMap;
	}

	@Override
	public List<Object[]> getContestWiseFirstTimeCustomerReportData() {
		List<Object[]> list = null;
		try {
			String sql =	"	SELECT ContestID, ContestName, CONVERT(VARCHAR(35), ContestStartDateTimeOrg) ContestStartDateTime, "
							+"		FirstParticipantsCount, tp.totPartcipants as TotalParticipants, "
							+"		CONVERT(DECIMAL(4,2), ((CONVERT(DECIMAL(10,2),FirstParticipantsCount) *100)/CONVERT(DECIMAL(10,2),tp.totPartcipants))) as FirstParticipantsPercentage "
							+"		, cqc.SignUpCount , ContestStartDateTimeOrg "
							+"	FROM (	SELECT c.id ContestID, c.contest_name ContestName, c.contest_start_datetime ContestStartDateTimeOrg, count(cp.customer_id) as FirstParticipantsCount "
							+"			FROM contest_participants cp "
							+"			INNER JOIN contest c on cp.contest_id = c.id and c.is_test_contest = 0 "
							+"			and c.status='EXPIRED' and c.contest_start_datetime > '2018-08-10 00:00:00.000' "
							+"			INNER JOIN (	SELECT customer_id, min(join_datetime) fstJoinDatettime "
							+"							FROM contest_participants GROUP BY customer_id) cpf on cp.customer_id = cpf.customer_id and cp.join_datetime = cpf.fstJoinDatettime "
							+"							INNER JOIN contest_registered_customer cr on cp.customer_id = cr.contest_customer_id "
							+"							GROUP BY c.id, c.contest_name, c.contest_start_datetime		) main "
							+"	OUTER APPLY (	SELECT count(distinct cpc.customer_id) totPartcipants "
							+"					FROM contest_participants cpc "
							+"					INNER JOIN contest_registered_customer cv on cpc.customer_id = cv.contest_customer_id "
							+"					where cpc.contest_id = main.ContestID	) tp "
							+"	OUTER APPLY (	SELECT count(1) SignUpCount "
							+"					FROM contest_registered_customer c "
							+"					where c.contest_customer_signup_date >=  dateadd(second, 1, dateadd(day, datediff(day, 0, main.ContestStartDateTimeOrg), 0)) "
							+"					and c.contest_customer_signup_date < main.ContestStartDateTimeOrg) cqc "
							+"	ORDER BY main.ContestStartDateTimeOrg;";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<Object[]> getRegisteredUserReportData(String fromDate,String toDate,SharedProperty sharedProperty) {

		List<Object[]> list = null;
		try {
			String sql =	"	SELECT cust_name, last_name, user_id, email, phone, signup_Date "
							+"	FROM  "+sharedProperty.getDatabasAlias()+"customer c "
							+"	INNER JOIN contest_registered_customer rc on c.id = rc.contest_customer_id "
							+"	WHERE 1=1 ";
							if(fromDate!=null && !fromDate.isEmpty()){
								fromDate += " 00:00:00.000";
								sql+= "and signup_date >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								toDate += " 23:59:59:000";
								sql+= "and signup_date <= '"+toDate+"' ";
							}
					sql += "	ORDER BY c.signup_Date ";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;

	}

	@Override
	public List<Object[]> getRegisteredUserWhoHaveNotPlayedContestReportData(String fromDate,String toDate,SharedProperty sharedProperty) {

		List<Object[]> list = null;
		try {
			String sql =	"	SELECT cust_name, last_name, user_id, email, phone, signup_Date "
							+"	FROM  "+sharedProperty.getDatabasAlias()+"customer c "
							+"	INNER JOIN contest_registered_customer rc on c.id = rc.contest_customer_id "
							+"	WHERE c.id not in (SELECT customer_id FROM contest_participants GROUP BY customer_id) ";
							if(fromDate!=null && !fromDate.isEmpty()){
								fromDate += " 00:00:00.000";
								sql+= "and signup_date >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								toDate += " 23:59:59:000";
								sql+= "and signup_date <= '"+toDate+"' ";
							}
					sql += "	ORDER BY c.signup_Date ";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;

	}

	@Override
	public List<Object[]> getRegisteredUserWhoHavePlayedContestReportData(String fromDate,String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			String sql ="select distinct cu.id as CustomerNo "+
					", cu.cust_name +' '+cu.last_name as CustomerName "+
					", cu.user_id as CustomerUserID "+
					", cu.phone as CustomerPhone "+
					", cu.email as CustomeerEmail "+
					"from "+sharedProperty.getDatabasAlias()+"customer cu "+
					"where cu.is_test_account=0 and cu.product_type='REWARDTHEFAN' "+
					"and cu.id in (	select distinct customer_id from customer_contest_answers "+
					"where contest_id in (select id from contest co "+
					"where  co.status = 'EXPIRED' and co.is_test_contest = 0 ";

					if(fromDate!=null && !fromDate.isEmpty()){
						fromDate += " 00:00:00.000";
						sql+= "and co.contest_start_datetime >= '"+fromDate+"' ";
					}
					if(toDate!=null && !toDate.isEmpty()){
						toDate += " 23:59:59:000";
						sql+= "and co.contest_start_datetime <= '"+toDate+"' ";
					}

					sql += ")"+
					"UNION "+
					"select distinct customer_id from contest_participants "+
					"where contest_id in (select id from contest co "+
					"where  co.status = 'EXPIRED' and co.is_test_contest = 0 ";

					if(fromDate!=null && !fromDate.isEmpty()){
						sql+= "and co.contest_start_datetime >= '"+fromDate+"' ";
					}
					if(toDate!=null && !toDate.isEmpty()){
						sql+= "and co.contest_start_datetime <= '"+toDate+"' ";
					}

					sql+= "))"+
					"and  cu.id not in ( select customerid from bots_customer) "+
					"and cu.phone not like '%#%' "+
					"and cu.email not like '%#%' "+
					"order by cu.id";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<Object[]> getCustomerRewardDollarsLiabilityReportData(String fromDate,String toDate,SharedProperty sharedProperty) {	// Shiva Modified on 14 Oct 2019
		List<Object[]> list = null;
		try {
			
			// Shiva Commented the below SQL Query on 14 Oct 2019
			/* String sql =	"	SELECT CustomerNo, CustomerName, customerUserID, CustomerPhone, CustomeerEmail, ContestPoints "
							+"	FROM  (		SELECT CustomerNo, CustomerName, customerUserID, CustomerPhone, CustomeerEmail, "
							+"					( ContestPnts + ReferralPnts) as ContestPoints, SpentPoints "
							+"				FROM (	SELECT cu.id as CustomerNo ,cu.cust_name +' '+cu.last_name as CustomerName "
							+"							,cu.user_id as CustomerUserID,cu.phone as CustomerPhone,cu.email as CustomeerEmail "
							+"							,IIF(cp.contestPoints IS NULL, 0.00, cp.contestPoints) as ContestPnts "
							+"							,IIF(clri.total_referral_points IS NULL, 0.00, clri.total_referral_points ) as ReferralPnts "
							+"							,IIF(clrh.spent_points IS NULL, 0.00, clrh.spent_points) SpentPoints "
							+"						FROM contest_registered_customer rc "
							+"						INNER JOIN "+sharedProperty.getDatabasAlias()+"customer cu on cu.id = rc.contest_customer_id "
							+"						LEFT JOIN (		SELECT clrit.customer_id, sum(clrit.reward_points)  as contestPoints  "
							+"										FROM "+sharedProperty.getDatabasAlias()+"cust_loyalty_reward_info_tracking clrit "
							+"										INNER JOIN contest zco on clrit.contest_id = zco.id "
							+"										WHERE clrit.reward_type ='CONTEST' and clrit.created_by <> 'VOIDED' "
							+"										and zco.is_test_contest = 0  and zco.status = 'EXPIRED'  ";
							if(fromDate!=null && !fromDate.isEmpty()){
								fromDate += " 00:00:00.000";
								sql+= "								and zco.contest_start_datetime >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								toDate += " 23:59:59:000";
								sql+= "								and zco.contest_start_datetime <= '"+toDate+"' ";
							}
					sql += 	"										GROUP BY customer_id	) cp on cu.id = cp.customer_id "
							+"						LEFT JOIN "+sharedProperty.getDatabasAlias()+"cust_loyalty_reward_info clri on cu.id = clri.cust_id "
							+"						LEFT JOIN (	SELECT customer_id, sum(IIF(points_spent IS NULL, 0.00, points_spent)) as spent_points  "
							+"									FROM "+sharedProperty.getDatabasAlias()+"cust_loyalty_reward_history WHERE status<>'VOIDED'  "
							+"									GROUP BY customer_id	) clrh on cu.id = clrh.customer_id "
							+"						WHERE cu.id in (SELECT distinct customer_id FROM customer_contest_answers	"
							+"										UNION "
							+"										SELECT distinct customer_id FROM contest_participants) "
							+"					) main "
							+"			) mst "
							+"	WHERE ContestPoints > 0 "
							+"	ORDER BY ContestPoints desc , CustomerNo; ";
			*/
			String sql =	"	SELECT customerNo,CustomerName, customerUserID, customerPhone, customeerEmail, redeemedPoints, totalPoints "
							+"	FROM rtf_rep_Customer_Reward_Dollars_Statistics_Report "
							+"	ORDER BY totalPoints desc, customerNo ;";
			
			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}

	
	@Override
	public Map<String, Object> getPollingInformation(String fromDate, String toDate, SharedProperty sharedProperty) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Object[]> list = null;		
		try {			
			String sql = " select distinct rc.contest_customer_name as 'Customer Name' ," + 
					 	 " rc.contest_customer_phone as Phone , " + 
					 	 " rc.contest_customer_email as eMail , " +
					 	" rc.contest_customer_user_id as 'User ID' " +
						 " from " + sharedProperty.getDatabasAlias() + "polling_answer_validation av  " + 
						 " join contest_registered_customer rc on av.cust_id = rc.contest_customer_id " + 
						 " join " + sharedProperty.getDatabasAlias() + "polling_question_bank qb on av.question_id = qb.id " + 
						 " where av.user_ans is not null " +
						 " and av.created_date >= '"+fromDate+"'" +
						 " and av.created_date <= '"+toDate+"'" +						 
						 " order by rc.contest_customer_user_id";
			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();							
			/*
			 * list.stream().forEach((record) -> { pollingPlayedUserIdList.add((String)
			 * record[0]); });
			 */		
			resultMap.put("pollingDistinctUserIdList", list);
			
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		
		try {			
			String sql = "select distinct rc.contest_customer_name as 'Customer Name' , "
					+ " rc.contest_customer_phone as 'Phone' ,"
					+ " rc.contest_customer_email as 'eMail' ,  rc.contest_customer_user_id as 'User ID' "
					+ " from " + sharedProperty.getDatabasAlias() + "polling_video_played pvp "
					+ " inner join contest_registered_customer rc on pvp.cust_id = rc.contest_customer_id "
					+ " where 1=1 "
					+ " and pvp.play_date >= '"+fromDate+"'"
					+ " and pvp.play_date <= '"+toDate+"'"
					+ " order by rc.contest_customer_user_id " ;
		
//			String sql = " select distinct b.user_id as UserId from " +
//					 sharedProperty.getDatabasAlias() + "polling_video_played a " + 
//					" join " + sharedProperty.getDatabasAlias() + "customer b on  a.cust_id  = b.id   " + 
//					" join " + sharedProperty.getDatabasAlias() + "polling_video_inventory c on a.vid_id = c.id " + 
//					" where  b.is_test_account = 0 ";
			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();				
			resultMap.put("pollingVideoDistinctUserIdList", list);
			
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}	

		try {
			String sql = "select distinct rc.contest_customer_name as 'Customer Name' , "
					+ " rc.contest_customer_phone as 'Phone' ,"
					+ " rc.contest_customer_email as 'eMail' ,  rc.contest_customer_user_id as 'User ID' "
					+ ", CONVERT(VARCHAR(20),pvp.play_date,101) + ' ' + CONVERT(VARCHAR(20),pvp.play_date,108) as 'Video Played Date'"
					+ ", pvi.category as 'Video Category' "
					+ ", pvi.title as 'Video Title' "
					+ ", pvp.play_date"
					+ " from " + sharedProperty.getDatabasAlias() + " polling_video_played pvp "
					+ " inner join contest_registered_customer rc  on pvp.cust_id = rc.contest_customer_id "
					+ " left join " + sharedProperty.getDatabasAlias() + "polling_video_inventory pvi on pvp.vid_id = pvi.id"
					+ " where 1=1 "
					+ " and pvp.play_date >= '"+fromDate+"'"
					+ " and pvp.play_date <= '"+toDate+"'"
					+ " order by pvp.play_date, rc.contest_customer_user_id " ;
			
			
			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			List finalPlayList = new ArrayList();
			list = query.list();				
			resultMap.put("pollingUserVideoPlayListHistory", list);
			
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		
		try {						
//			String sql = " 	select a.cust_id , b.user_id ,  convert(varchar(10),a.reward_type) AS REWARD_TYPE , a.reward_qty, " +
//			" a.created_date from " + sharedProperty.getDatabasAlias() + "polling_answer_validation a  " + 
//			" join " + sharedProperty.getDatabasAlias() + "customer b on a.cust_id = b.id   " + 
//			" where b.is_test_account = 0 and a.reward_type is not null and a.reward_qty > 0   " ;						
//			contestSession = ContestsDAO.getContestSession();
			
			
			
			String sql = "select rc.contest_customer_name as 'Customer Name' "
					+ " , rc.contest_customer_phone as 'Phone' "
					+ ", rc.contest_customer_email as 'eMail' "
					+ ", rc.contest_customer_user_id as 'User ID' "
					+ ", qb.question as 'Polling Question' "
					+ ", qb.option_a as 'Option A' "
					+ ", qb.option_b as 'Option B' "
					+ ", qb.option_c as 'Option C'  , av.user_ans as 'Customer Answered Option' "
					+ ", convert(date, av.created_date ) as 'Polling Date' "
					+ ", CASE WHEN av.reward_type='sf' THEN 'Superfan Star' "
					+ " WHEN av.reward_type='mw' THEN 'Magic Wand' "
					+ " WHEN av.reward_type='lf' THEN 'Life' "
					+ " WHEN av.reward_type='ms' THEN 'NO REWARD' "
					+ " END as 'Reward Type'  , av.reward_qty as 'Reward Quantity' "
					+ " from "  + sharedProperty.getDatabasAlias() + "polling_answer_validation av "
					+ " inner join contest_registered_customer rc on av.cust_id = rc.contest_customer_id "
					+ " inner join "  + sharedProperty.getDatabasAlias() + "polling_question_bank qb on av.question_id = qb.id "
					+ " where 1=1  and av.user_ans is not null "
					+ " and av.created_date >= '"+fromDate+"'"
					+ " and av.created_date <= '"+toDate+"'";
			
			
			
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();
			resultMap.put("pollingUserAnswerRewardListHistory", list);
			
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}		
		return resultMap;
	}
	
	
	@Override
	public Map<String, Object> getNoOfGamesPlayedInformation(SharedProperty sharedProperty) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Object[]> list = null;		
		try {			
			String sql = "SELECT CustomerName, CustomerUserID, CustomerPhone, CustomerEmail, NoOfContestGamesPlayed ,lgd.lastGameDate "
					+ " FROM (	SELECT CustomerID, CustomerFirstName, CustomerLastName, CustomerPhone, "
					+ " CustomerName, CustomerUserID, CustomerEmail, Count(distinct ContestID) as NoOfContestGamesPlayed,  "
					+ " CONVERT(DECIMAL(10,3),((Count(distinct ContestID) * 1.0) / coCnt.contestcount)*100 ) as AveragePercentageContestPlayedByCustomer  "
					+ " From (	SELECT cp.customer_id as customerID,c.contest_customer_name as customerName "
					+ " ,c.contest_customer_user_id as CustomerUserID,c.contest_customer_email as CustomerEmail  "
					+ " ,cp.contest_id as ContestID	,cu.cust_name as CustomerFirstName, cu.last_name as CustomerLastName "
					+ " ,c.contest_customer_phone as CustomerPhone "
					+ " FROM contest_registered_customer c with(nolock)   "
					+ " LEFT JOIN contest_participants cp with(nolock) on c.contest_customer_id = cp.customer_id  "
					+ " LEFT JOIN contest co with(nolock) on  co.id = cp.contest_id "
					+ " LEFT JOIN "  + sharedProperty.getDatabasAlias() + "customer cu with(nolock) on  c.contest_customer_id = cu.id "
					+ " WHERE co.contest_name not like '%test%' and co.status <> 'DELETED' and  co.status = 'EXPIRED' "
					+ " and CONVERT(datetime,co.contest_start_datetime) >= '2018-08-10 00:00:00.000'  "
					+ " GROUP BY cp.customer_id,c.contest_customer_name ,c.contest_customer_user_id, cp.contest_id,c.contest_customer_email "
					+ " ,cu.cust_name, cu.last_name,c.contest_customer_phone  "
					+ " UNION "
					+ " SELECT cp.customer_id as customerID,c.contest_customer_name as customerName  "
					+ " ,c.contest_customer_user_id as CustomerUserID,c.contest_customer_email as CustomerEmail  "
					+ " ,cp.contest_id as ContestID	,cu.cust_name as CustomerFirstName, cu.last_name as CustomerLastName "
					+ " ,c.contest_customer_phone as CustomerPhone "
					+ " FROM contest_registered_customer c with(nolock) "
					+ " LEFT JOIN customer_contest_answers cp with(nolock) on c.contest_customer_id = cp.customer_id "
					+ " LEFT JOIN contest co with(nolock) on  co.id = cp.contest_id  "
					+ " LEFT JOIN "  + sharedProperty.getDatabasAlias() + "customer cu with(nolock) on  c.contest_customer_id = cu.id "
					+ " WHERE co.contest_name not like '%test%' and co.status <> 'DELETED' and  co.status = 'EXPIRED'  "
					+ " and CONVERT(datetime,co.contest_start_datetime) >= '2018-08-10 00:00:00.000'  "
					+ " GROUP BY cp.customer_id,c.contest_customer_name ,c.contest_customer_user_id, cp.contest_id,c.contest_customer_email "
					+ " ,cu.cust_name, cu.last_name,c.contest_customer_phone "
					+ ") cgp "
					+ " cross APPLY (	SELECT count(distinct id) contestcount FROM contest co with(nolock)  "
					+ " Where co.status <> 'DELETED'  and co.status = 'EXPIRED' and co.contest_name NOT LIKE '%TEST%'  "
					+ " and CONVERT(datetime,co.contest_start_datetime) >= '2018-08-10 00:00:00.000' "
					+ ") coCnt "
					+ " GROUP BY cgp.CustomerID, cgp.CustomerName, cgp.CustomerUserID, cgp.CustomerEmail, coCnt.contestcount "
					+ " , cgp.CustomerFirstName, cgp.CustomerLastName, cgp.CustomerPhone  "
					+ "  ) finalQ "
					+ " cross APPLY (	SELECT min(contest_start_datetime) as firstGameDate, max(contest_start_datetime) as lastGameDate  "
					+ " FROM contest c with(nolock) "
					+ " INNER JOIN contest_participants p with(nolock) on c.id = p.contest_id "
					+ " WHERE p.customer_id = finalQ.customerID and c.contest_name not like '%test%' and c.status <> 'DELETED' "
					+ " and  c.status = 'EXPIRED'  "
					+ " and CONVERT(datetime,c.contest_start_datetime) >= '2018-08-10 00:00:00.000' "
					+ " ) lgd "
					+ " ORDER BY finalQ.NoOfContestGamesPlayed desc, finalQ.CustomerID ; ";
			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();							
					
			resultMap.put("gamesPlayedList", list);
			
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		
			
		return resultMap;
	}
	
	
	@Override
	public Map<String, Object> getCustomerSuperFanStarsInformation(SharedProperty sharedProperty) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Object[]> list = null;		
		try {			
			String sql = "select c.contest_customer_name as CustomerName "
					+ " , c.contest_customer_user_id as CustomerUserID "
					+ " , c.contest_customer_phone as CustomerPhone "
					+ " , c.contest_customer_email as CustomerEmail "
					+ " , s.total_no_of_chances as AvailableStars "
					+ " from contest_registered_customer c with(nolock)  "
					+ " inner join  quiz_super_fan_stat s on c.contest_customer_id = s.customer_id "
					+ " order by s.total_no_of_chances desc ;";
			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();							
					
			resultMap.put("customerSuperFanStars", list);
			
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		
			
		return resultMap;
	}
	
	
	@Override
	public List<Object[]> getSuperFanStartsInformation(SharedProperty sharedProperty) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Object[]> list = null;		
		try {			
			String sql = "select 1 as orderSequence "
					+ " 		, '1 to 10'   as SuperFanStarsRange "
					+ "  		, count(distinct c.contest_customer_user_id) as NoOfCustomerCount "
					+ " 		, sum(s.total_no_of_chances) as TotalSuperFanStarsAvailability"
					+ " from contest_registered_customer c with(nolock) "
					+ " inner join  quiz_super_fan_stat s on c.contest_customer_id = s.customer_id"
					+ " where s.total_no_of_chances > 1 and s.total_no_of_chances <= 10 "
					+ " UNION "
					+ " select 2 as orderSequence "
					+ " 		,'11 to 20'   as SuperFanStarsRange "
					+ " 		, count(distinct c.contest_customer_user_id) as NoOfCustomerCount "
					+ "			, sum(s.total_no_of_chances) as TotalSuperFanStarsAvailability "
					+ " from contest_registered_customer c with(nolock) "
					+ " inner join  quiz_super_fan_stat s on c.contest_customer_id = s.customer_id "
					+ " where s.total_no_of_chances > 10 and s.total_no_of_chances <= 20 "
					+ " UNION "
					+ " select 3 as orderSequence "
					+ " 		,'21 to 30'   as SuperFanStarsRange "
					+ " 		, count(distinct c.contest_customer_user_id) as NoOfCustomerCount "
					+ "			, sum(s.total_no_of_chances) as TotalSuperFanStarsAvailability "
					+ " from contest_registered_customer c with(nolock)"
					+ " inner join  quiz_super_fan_stat s on c.contest_customer_id = s.customer_id "
					+ " where s.total_no_of_chances > 20 and s.total_no_of_chances <= 30 "
					+ " UNION "
					+ " select 4 as orderSequence "
					+ " 		,'31 to 40'   as SuperFanStarsRange "
					+ "	 		, count(distinct c.contest_customer_user_id) as NoOfCustomerCount "
					+ " 		, sum(s.total_no_of_chances) as TotalSuperFanStarsAvailability "
					+ " from contest_registered_customer c with(nolock) "
					+ " inner join  quiz_super_fan_stat s on c.contest_customer_id = s.customer_id "
					+ " where s.total_no_of_chances > 30 and s.total_no_of_chances <= 40 "
					+ " UNION "
					+ " select 5 as orderSequence "
					+ " 		,'41 to 50'   as SuperFanStarsRange "
					+ "			, count(distinct c.contest_customer_user_id) as NoOfCustomerCount "
					+ "			, sum(s.total_no_of_chances) as TotalSuperFanStarsAvailability "
					+ " from contest_registered_customer c with(nolock) "
					+ " inner join  quiz_super_fan_stat s on c.contest_customer_id = s.customer_id "
					+ " where s.total_no_of_chances > 40 and s.total_no_of_chances <= 50 "
					+ " UNION "
					+ " select 6 as orderSequence"
					+ "			,'> 50'       as SuperFanStarsRange "
					+ "			, count(distinct c.contest_customer_user_id) as NoOfCustomerCount "
					+ "			, sum(s.total_no_of_chances) as TotalSuperFanStarsAvailability "
					+ " from contest_registered_customer c with(nolock) "
					+ " inner join  quiz_super_fan_stat s on c.contest_customer_id = s.customer_id "
					+ " where s.total_no_of_chances > 50 "
					+ " order by 1 ;" ;
			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();							
					
			
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		
			
		return list;
	}
	
	
	
	@Override
	public Map<String, Object> getTicketCostInformation(String fromDate,String toDate,SharedProperty sharedProperty) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		if(fromDate!=null && !fromDate.isEmpty()){
			fromDate += " 00:00:00.000";
		}
		if(toDate!=null && !toDate.isEmpty()){
			toDate += " 23:59:59:000";
		}
		List<Object[]> list = null;		
		try {			
			String sql = " select yearNumber, monthNumber " + 
					 	 " , (LEFT(monthText,3) +' '+ yearText) MonthYear " + 
					 	 " , COUNT(InvoiceStatusCompleted) as Completed " +
					 	 " , COUNT(InvoiceStatusOutstanding) as OutStanding " +
						 " FROM   (      select datepart(month, InvCreatedDate) as monthNumber  " + 
					 	 " , datename(month, InvCreatedDate) as monthText " +
						 " , datepart(year, InvCreatedDate) as yearNumber "+
					 	 " , datename(Year, InvCreatedDate) as yearText "+
						 " , (CASE WHEN InvoiceStatus='Completed' THEN InvoiceStatus END) InvoiceStatusCompleted "+
					 	 " , (CASE WHEN InvoiceStatus='Outstanding' THEN InvoiceStatus END) InvoiceStatusOutstanding "+
						 " , Quantity "+
						 " from " + sharedProperty.getDatabasAlias() + "rep_RTF_Ticket_Cost_Report_qry_vw " +
						 " where InvCreatedDate between '"+fromDate+"'"+
						 " and "+
						 "'"+toDate+"'"+
						 " ) mst "+
						 " group by yearNumber, yearText, monthNumber, monthText "+
						 " order by yearNumber, monthNumber; ";
						 
					 	 
			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();							
			/*
			 * list.stream().forEach((record) -> { pollingPlayedUserIdList.add((String)
			 * record[0]); });
			 */		
			resultMap.put("MonthWiseInvoiceStatus", list);
			
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		
		try {			
			String sql = " select yearNumber, monthNumber, (LEFT(monthText,3)+' '+ yearText) MonthYear " + 
				 	 " , sum(ContestTicketCost) as TickectCost " + 
				 	 " FROM   (      select datepart(month, InvCreatedDate) as monthNumber " +
				 	 "  , datename(month, InvCreatedDate) as monthText " +
					 "  , datepart(year, InvCreatedDate) as yearNumber  " + 
				 	 " , datename(Year, InvCreatedDate) as yearText " +
					 " , ContestTicketCost "+
					 " from " + sharedProperty.getDatabasAlias() + "rep_RTF_Ticket_Cost_Report_qry_vw " +
					 " where InvCreatedDate between '"+fromDate+"'"+
					 " and "+
					 "'"+toDate+"'"+
					 " ) mst "+
					 " group by yearNumber, monthNumber, monthText, yearText "+
					 " order by yearNumber, monthNumber; ";
			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();				
			resultMap.put("MonthWiseTicketCost", list);
			
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}	

		try {
			String sql = " select yearNumber, monthNumber, (LEFT(monthText,3) +' '+ yearText) MonthYear " + 
				 	 " , sum(Quantity) as TickectQuantity " + 
				 	 " FROM   (      select datepart(month, InvCreatedDate) as monthNumber " +
				 	 "  , datename(month, InvCreatedDate) as monthText " +
					 "  , datepart(year, InvCreatedDate) as yearNumber  " + 
				 	 "  , datename(Year, InvCreatedDate) as yearText " +
					 " , Quantity "+
					 " from " + sharedProperty.getDatabasAlias() + "rep_RTF_Ticket_Cost_Report_qry_vw " +
					 " where InvCreatedDate between '"+fromDate+"'"+
					 " and "+
					 "'"+toDate+"'"+
					 " ) mst "+
					 " group by yearNumber, monthNumber, monthText, yearText "+
					 " order by yearNumber, monthNumber; ";
			
			
			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			List finalPlayList = new ArrayList();
			list = query.list();				
			resultMap.put("MonthWiseTicketQuantity", list);
			
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
			
		return resultMap;
	}
	
	@Override
	public Map<String, Object> getBotsCustomerInformation(String fromDate, String toDate, SharedProperty sharedProperty) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		
		List<Object[]> list = null;		
		try {			
			String sql = " select SignUpYearNo, SignUpMonthNo, LEFT(SignUpMonth,3)+' '+SignUpYear as month " + 
					 	 " 		, IOSUserCount, AndroidUserCount, TotalUserCount " + 
					 	 " from (	select datepart(Year,SignUpDate) as SignUpYearNo " +
					 	 "  		, datepart(month, SignUpDate) as SignUpMonthNo " +
						 "  		, datename(month, SignUpDate) as SignUpMonth  " + 
					 	 "  		, datename(YEAR, SignUpDate) as SignUpYear " +
						 "  		, count(case when SignUpPlatform='IOS' Then CustomerId End) as IOSUserCount "+
					 	 "  		, count(case when SignUpPlatform='ANDROID' Then CustomerId End) as AndroidUserCount "+
						 "  		, count(CustomerId) as TotalUserCount "+
					 	 " from bots_customer  with(nolock) " +
					 	 " where 1=1 "+
						 " AND CONVERT(datetime,signupdate) >= '"+ fromDate+"'" + 
						 " AND CONVERT(datetime,signupdate) <= '"+ toDate+"'" + 
						 "  group by datepart(Year,signupdate), datepart(month, SignUpDate) " +
						 "  		, datename(month, SignUpDate), datename(YEAR, SignUpDate) "+
						 " ) mst "+
						 " order by SignUpYearNo, SignUpMonthNo; ";
						 
					 	 
			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();							
			/*
			 * list.stream().forEach((record) -> { pollingPlayedUserIdList.add((String)
			 * record[0]); });
			 */		
			resultMap.put("MonthWiseBotsCust", list);
			
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		
		try {			
			String sql = " select  SignUpYear  , SignUpQUARTERNo, concat('Q', SignUpQUARTERNo, ' ', SignUpYear) as quarter , IOSUserCount, AndroidUserCount " + 
				 	 "  from ( select datepart(Year,SignUpDate) as SignUpYear" +
				 	 "  		, datepart(QUARTER, SignUpDate) as SignUpQUARTERNo " +
					 "  		, datename(QUARTER, SignUpDate) as SignUpQUARTER " +
				 	 "  		, count(case when SignUpPlatform='IOS' Then CustomerId End) as IOSUserCount " +
					 "  		, count(case when SignUpPlatform='ANDROID' Then CustomerId End) as AndroidUserCount "+
				 	 "  from bots_customer with(nolock) " +
					 "  where 1=1 "+
					 "  AND CONVERT(datetime,signupdate) >= '"+ fromDate+"'" + 
					 "  AND CONVERT(datetime,signupdate) <= '"+ toDate+"'" + 
					 "  group by datepart(Year,signupdate), datepart(QUARTER, SignUpDate), datename(QUARTER, SignUpDate) " +
					 "  ) mst "+
					 "  order by SignUpYear, SignUpQUARTER;  ";
			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();				
			resultMap.put("QuaterlyWiseBotsCust", list);
			
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
			
		return resultMap;
	}

	@Override
	public Map<String, Object> getContestParticipantInformation(SharedProperty sharedProperty) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		
		List<Object[]> list = null;		
		try {			
			String sql = " select ( datename(month, contestDate)+' '+datename(year, contestDate)) as Month " + 
					 	 " 		, convert(varchar(10),contestNo) +' - '+ convert(varchar(30),contestDate,100) as ContestIDDate " + 
					 	 " 		, count(customerNo) as NoOfParticipants " +
					 	 "  from rep_RTF_Contest_reward_dollar_Report_qry_vw " +
						 "  group by contestDate, contestNo,  " + 
					 	 "  		datepart(year, contestDate), datepart(month, contestDate) " +
						 "  order by contestDate ; ";
						 
					 	 
			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();							
			/*
			 * list.stream().forEach((record) -> { pollingPlayedUserIdList.add((String)
			 * record[0]); });
			 */		
			resultMap.put("contestParticipant", list);
			
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		
		/*try {			
			String sql = " select  SignUpYear  , SignUpQUARTERNo, concat('Q', SignUpQUARTERNo, ' ', SignUpYear) as quarter , IOSUserCount, AndroidUserCount " + 
				 	 "  from ( select datepart(Year,SignUpDate) as SignUpYear" +
				 	 "  		, datepart(QUARTER, SignUpDate) as SignUpQUARTERNo " +
					 "  		, datename(QUARTER, SignUpDate) as SignUpQUARTER " +
				 	 "  		, count(case when SignUpPlatform='IOS' Then CustomerId End) as IOSUserCount " +
					 "  		, count(case when SignUpPlatform='ANDROID' Then CustomerId End) as AndroidUserCount "+
				 	 "  from bots_customer "+
					 "  group by datepart(Year,signupdate), datepart(QUARTER, SignUpDate), datename(QUARTER, SignUpDate) " +
					 "  ) mst "+
					 "  order by SignUpYear, SignUpQUARTER;  ";
			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();				
			resultMap.put("contestRewardDollar", list);
			
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}*/
			
		return resultMap;
	}
	
	
	
	public Map<String, Object> getQueWiseUserAnswerCountInformation(String fromDate, String toDate, SharedProperty sharedProperty) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Object[]> list = null;	
		if(fromDate!=null && !fromDate.isEmpty()){
			fromDate += " 00:00:00.000";
		}
		if(toDate!=null && !toDate.isEmpty()){
			toDate += " 23:59:59:000";
		}
	
		try {			
			String sql = "select ( datename(month, OriginalContestStartDatetime)+' '+datename(year, OriginalContestStartDatetime)) as Month "
					+ " , convert(varchar(10),contestID) +' - '+ "
					+ " convert(varchar(30),OriginalContestStartDatetime,100) as ContestIDDate "
					+ " , [Overall%Answered] as PercentageAnswered "
					+ " , (100-[Overall%Answered]) as PercentageNotAnswered "
					+ " from rtf_rep_Contest_Question_Wise_User_Answer_Count_Report with(nolock) "
					+ " WHERE 1=1 "
					+ " AND CONVERT(datetime,OriginalContestStartDatetime) >= '"+ fromDate+"'"
					+ " AND CONVERT(datetime,OriginalContestStartDatetime) <= '"+ toDate+"'"
					+ " ORDER BY OriginalContestStartDatetime;";
			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();							
					
			resultMap.put("answeredForEachGame", list);
			
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		try {			
			String sql = "select ( datename(month, OriginalContestStartDatetime)+' '+datename(year, OriginalContestStartDatetime)) as Month "
					+ " 		, convert(varchar(10),contestID) +' - '+ "
					+ "   		convert(varchar(30),OriginalContestStartDatetime,100) as ContestIDDate  "
					+ " 	 	, [No.OfParticipantsParticipated] as Participants "
					+ " from RTFQuizMaster_Sanbox.dbo.rtf_rep_Contest_Question_Wise_User_Answer_Count_Report with(nolock) "
					+ " WHERE 1=1 "
					+ " AND CONVERT(datetime,OriginalContestStartDatetime) >= '"+ fromDate+"'"
					+ " AND CONVERT(datetime,OriginalContestStartDatetime) <= '"+ toDate+"'"
					+ " ORDER BY OriginalContestStartDatetime;";
			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();							
					
			resultMap.put("participantCount", list);
			
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
			
		return resultMap;
	}
	
	

	/*----// Shiva BEGINS of Adding New Report Controller Methods on 14 Oct 2019 //----*/

	
	@Override
	public List<Object[]> getFantasyFootballContestWinnersDetailsReport(String fromDate,String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {

			if(fromDate!=null && !fromDate.isEmpty()){
				fromDate += " 00:00:00.000";
			}
			if(toDate!=null && !toDate.isEmpty()){
				toDate += " 23:59:59:000";
			}

			String sql =	"  select o.id as ContestID, REPLACE(o.contest_name, '  ','') as ContestName, "	+
							"    o.contest_start_datetime as ContestDate, c.id as CustomerID, "	+
							"    c.user_id as UserID, c.email as UserEmail, w.reward_tickets as NoOfTicketsWon "	+
							"  from contest_grand_winners w "	+
							"  inner join " + sharedProperty.getDatabasAlias() + "customer c on w.customer_id = c.id "	+
							"  inner join contest o on w.contest_id = o.id "	+
							"  inner join	(	 select vw.customerNo,   vw.contestNo, REPLACE(co.contest_name, '  ', '') as contestName, "	+
							"                        co.contest_start_datetime "	+
							"                    from rtf_rep_Contest_Reward_Dollar_Report vw with(nolock) "	+
							"                    inner join contest_registered_customer crc with(nolock) on vw.customerNo = crc.contest_customer_id "	+
							"                    inner join " + sharedProperty.getDatabasAlias() + "customer c with(nolock) on crc.contest_customer_id = c.id "	+
							"                    inner join contest co with(nolock) on vw.contestNo = co.id  "	+
							"                    where  1=1 " 	;
							if(fromDate!=null && !fromDate.isEmpty()){
								sql+= "                    and contestDate >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								sql+= "                    and contestDate <= '"+toDate+"' ";
							}
					sql+= 	"                    and co.contest_name like '%fantasy%' and co.status = 'EXPIRED' and co.is_test_contest = 0 "	+
							"                    and datename(WEEKDAY,co.contest_start_datetime) in ('THURSDAY', 'SUNDAY') "	+
							"                    and convert(varchar(8),co.contest_start_datetime,108) in ('19:00:00','12:00:00') and c.is_test_account = 0 "	+
							"                ) fc on w.contest_id = fc.contestNo and w.customer_id = fc.customerNo "	+
							"  where 1=1 " ;
							if(fromDate!=null && !fromDate.isEmpty()){
								sql+= "  and o.contest_start_datetime >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								sql+= "  and o.contest_start_datetime <= '"+toDate+"' ";
							}
					sql+= 	"  and o.contest_name like '%football%' and w.reward_tickets is not null and c.is_test_account = 0  "+
							"  order by o.contest_start_datetime ";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	
	
	
	@Override
	public List<Object[]> getMonthlyCustomerParticipantsCountStatisticsForSheetOne(String fromDate,String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {

			if(fromDate!=null && !fromDate.isEmpty()){
				fromDate += " 00:00:00.000";
			}
			if(toDate!=null && !toDate.isEmpty()){
				toDate += " 23:59:59:000";
			}
			
			String sql =    "  select (monthText+' '+yearText) as MonthYearText  "  +
							"      , NoGamesPlayed as NoOfGamesPlayed  "  +
							"      , noOfParticipants as ParticipantCount  "  +
							"      , UniqueParticipantsCount as UniqueParticipantCount  "  +
							"      , regCusCntInMonth as NewUserParticipantCount  "  +
							"      , (IIF(    (UniqueParticipantsCount-regCusCntInMonth) > 0,  " +
							"            (UniqueParticipantsCount-regCusCntInMonth), 0) ) as OldUserParticipantsCount  "  +
							"      , CONVERT(decimal(10,2),(noOfParticipants*1.00/NoGamesPlayed*1.00)) as AverageViewership  "  +
							"	   , yearNumber, monthNumber  "  +
    						"  from (    select    datename(year, contestDate) as yearText  "  +
							"              , datepart(year, contestDate) as yearNumber  "  +
							"              , datepart(month, contestDate) as monthNumber  "  +
							"              , datename(month, contestDate) as monthText  "  +
							"              , count(distinct contestNo) as NoGamesPlayed  "  +
							"              , count(customerNo) as noOfParticipants  "  +
							"              , count(distinct customerNo) as UniqueParticipantsCount  "  +
							"            from rtf_rep_Contest_Reward_Dollar_Report vw with(nolock)  "  +  
							"            inner join contest_registered_customer rc on vw.customerNo = rc.contest_customer_id  "  +
							"            where 1=1  "  ;
							if(fromDate!=null && !fromDate.isEmpty()){
								sql+= "            and vw.contestDate >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								sql+= "            and vw.contestDate <= '"+toDate+"' ";
							}
					sql+=   "            group by datename(year, contestDate), datepart(year, contestDate)  "  +
							"            , datepart(month, contestDate), datename(month, contestDate)  "  +
							"      ) ma  "  +
							"  outer apply ( select count(distinct customerNo) regCusCntInMonth  "  +
							"                from rtf_rep_Contest_Reward_Dollar_Report a with(nolock)  "  +
							"                inner join contest_registered_customer rc on a.customerNo = rc.contest_customer_id  "  +
							"                where datepart(year, customerSignupDate) = ma.yearNumber  "  +
							"                and datepart(month, customerSignupDate) = ma.monthNumber  "  +
							"                group by datepart(year, customerSignupDate), datepart(month, customerSignupDate)  "  +
							"      ) nrcm  "  +
							"  order by yearNumber desc, monthNumber desc;" ;

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	

	
	@Override
	public List<Object[]> getMonthlyContestParticipantsCountStatisticsForSheetTwo(String fromDate,String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {

			if(fromDate!=null && !fromDate.isEmpty()){
				fromDate += " 00:00:00.000";
			}
			if(toDate!=null && !toDate.isEmpty()){
				toDate += " 23:59:59:000";
			}
			
			String sql =    "  select    ( datename(month, contestDate)+' '+datename(year, contestDate)) as MonthYearText  "  +
							"      , contestNo as ContestID  "  +
							"      , REPLACE(contestName, '  ', '') as ContestName  "  +
							"      , contestDate as ContestsDate  "  +
							"      , count(distinct customerNo) as NoOfParticipants  "  +
							"  from rtf_rep_Contest_Reward_Dollar_Report vw with(nolock)  "  +
							"  inner join contest_registered_customer rc on vw.customerNo = rc.contest_customer_id  "  +
							"  where  1=1  "  ;
							if(fromDate!=null && !fromDate.isEmpty()){
								sql+= "  and vw.contestDate >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								sql+= "  and vw.contestDate <= '"+toDate+"' ";
							}
					sql+=   "  group by contestDate, contestNo, contestName,  "  +
							"	        datepart(year, contestDate), datepart(month, contestDate),  "  +
							"	        datename(year, contestDate), datename(month, contestDate)  "  +
							"  order by contestDate desc;  "  ;

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	

	@Override
	public List<Object[]> getOutstandingContestOrdersToBeFulfilledReport(SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {

			
			String sql =    "  select  DATENAME(YEAR, co.created_date) as OrderYear,  "  +
							"       CONVERT(VARCHAR(10), co.created_date, 101) as OrderDate,  "  + 
							"       i.order_id as OrderNo, co.quantity  as OrderQuantity,  "  +
							"       i.invoice_total as OrderTotal, c.cust_name+ ' ' +c.last_name as CustomerName,  "  +
							"       c.User_id as CustomerUserID, c.email as CustomerEmailID,  "  +
							"       c.phone as CustomerPhone , co.event_name as EventName,   "  +
							"       ( CONVERT(VARCHAR(10), co.event_date, 101)+ ' '+iif(co.event_time is null, 'TBD'  "  +
							"         , CONVERT(VARCHAR(10), co.event_time, 108)) ) as EventDateTime  "  +
							"  from " + sharedProperty.getDatabasAlias() + "customer_order co with(NOLOCK)  "  +
							"  inner join " + sharedProperty.getDatabasAlias() + "customer c with(NOLOCK)  on c.id = co.customer_id  "  + 
							"  inner join " + sharedProperty.getDatabasAlias() + "invoice i with(NOLOCK) on co.id = i.order_id   "  +
							"  where co.status = 'ACTIVE'  and i.status = 'Outstanding'  and c.is_test_account = 0  "  +
							"  and co.order_type = 'CONTEST'  "  +
							"  and (CAST(convert(varchar(10),co.event_date,120) as datetime) +  "  + 
							"       CAST(CONVERT(VARCHAR(8),iif(co.event_time is null, '00:00:00', co.event_time),108) as dateTime))  "  + 
							"       between co.created_date and   "  +
							"          (CAST(convert(varchar(10),DATEADD(DAY,5,co.created_date),120) as datetime) +  "  + 
							"           CAST(CONVERT(VARCHAR(8),'23:59:59',108) as dateTime))  "  +
							"  and (CAST(convert(varchar(10),co.event_date,120) as datetime) +   "  +
							"       CAST(CONVERT(VARCHAR(8),iif(co.event_time is null, '00:00:00', co.event_time),108) as dateTime))  > getdate()  "  +
							"  and i.id not in  (select invoice_id from " + sharedProperty.getDatabasAlias() + "invoice_ticket_attachment)  "  +
							"  order by co.event_date, co.event_time, co.created_date;  "  ;

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	

	@Override
	public List<Object[]> getCustomerAverageTimeAccessingRTFTriviaReport(SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			
			String sql =	"  select rc.contest_customer_id as CustomerID  "  +
							"      , rc.contest_customer_user_id as UserID  "  +
							"      , CONVERT(DATE,rc.contest_customer_signup_date) as SignupDate  "  +
							"      , rc.no_of_days_accessed as NoOfDaysAccessed  "  +
							"      , rc.no_of_minutes_accessed as NoOfMinutesAccessed  "  +
							"      , rc.average_minutes_per_day as AverageMinutesperDay  "  +
							"      , rc.no_of_contest_participated as NoOfContestParticipated  "  +
							"      , rc.average_minutes_per_contest as AverageMinutesperContest  "  +
							"  from contest_registered_customer rc  "  +
							"  inner join " + sharedProperty.getDatabasAlias() + "customer c on rc.contest_customer_id = c.id  "  +
							"  where rc.no_of_days_accessed is not NULL and c.is_test_account=0  "  +
							"  order by rc.no_of_days_accessed desc, rc.contest_customer_id;" ;

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	

	@Override
	public List<Object[]> getMonthlyCustomerParticipantsCountBreakdownStatistics(SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			
			String sql =	 "    select cs.MonthText as MonthText      "  +
							 "        , cs.DownloadedRegisteredInTheMonth as RegisteredUsers      "  +
							 "        , cs.NoOfContestInTheMonth as NoOfShows      "  +
							 "        , CONVERT(DECIMAL(10,2),((cs.RegisteredMonthAndNotPartcipatedInTheMonth*1.00/cs.DownloadedRegisteredInTheMonth)*100)) as NeverPlayedViewedPercentage      "  +
							 "        , cs.RegisteredMonthAndNotPartcipatedInTheMonth as  NeverPlayedViewedCount      "  +
							 "        , CONVERT(DECIMAL(10,2),((cs.RegisteredMonthOneGameInMonthAndInactive*1.00/cs.DownloadedRegisteredInTheMonth)*100)) as OneShowThenInActivePercentage      "  +
							 "        , cs.RegisteredMonthOneGameInMonthAndInactive as OneShowThenInActiveCount      "  +
							 "        , CONVERT(DECIMAL(10,2),((cs.RegisteredMonthOneGameInMonthAndInactive*1.00/cs.DownloadedRegisteredInTheMonth)*100)) as OneShowPerMonthPercentage      "  +
							 "        , cs.RegisteredMonthOneGameInMonthAndInactive as OneShowPerMonthCount      "  +
							 "        , CONVERT(DECIMAL(10,2),((cs.RegisteredMonthOneGameInEveryWeek*1.00/cs.DownloadedRegisteredInTheMonth)*100)) as OneShowPerWeekPercentage      "  +
							 "        , (cs.RegisteredMonthOneGameInEveryWeek) as OneShowPerWeekCount      "  +
							 "        , CONVERT(DECIMAL(10,2),((0*1.00/cs.DownloadedRegisteredInTheMonth)*100)) as OneShowPerDayPercentage      "  +
							 "        , 0 as OneShowPerDayCount      "  +
							 "        , cs.yearNo, cs.monthNo      "  +
							 "    from    (    select csa.MonthText , csa.DownloadedRegisteredInTheMonth , csa.NoOfContestInTheMonth       "  +
							 "                    , csa.RegisteredMonthAndNotPartcipatedInTheMonth , csa.RegisteredMonthOneGameInMonthAndInactive       "  +
							 "                    , SUM(csa.RegisteredMonthOneGameInEveryWeek) as RegisteredMonthOneGameInEveryWeek      "  +
							 "                    , csa.yearNo, csa.monthNo      "  +
							 "                from ( select cs.MonthText , cs.DownloadedRegisteredInTheMonth , cs.NoOfContestInTheMonth       "  +
							 "                            , cs.RegisteredMonthAndNotPartcipatedInTheMonth , cs.RegisteredMonthOneGameInMonthAndInactive       "  +
							 "                            , cs.RegisteredMonthOneGameInEveryWeek, cs.yearNo, cs.monthNo      "  +
							 "                        from rtf_rep_ContestDateWise_Contest_and_Customer_Count_Statistics cs      "  +
							 "                        group by cs.MonthText, cs.DownloadedRegisteredInTheMonth, cs.NoOfContestInTheMonth      "  +
							 "                            , cs.RegisteredMonthAndNotPartcipatedInTheMonth, cs.RegisteredMonthOneGameInMonthAndInactive      "  +
							 "                            , cs.RegisteredMonthOneGameInEveryWeek , cs.yearNo, cs.monthNo      "  +
							 "                    ) csa       "  +
							 "                group by csa.MonthText, csa.DownloadedRegisteredInTheMonth, csa.NoOfContestInTheMonth      "  +
							 "                    , csa.RegisteredMonthAndNotPartcipatedInTheMonth, csa.RegisteredMonthOneGameInMonthAndInactive      "  +
							 "                    , csa.yearNo, csa.monthNo      "  +
							 "            ) cs      "  +
							 "    order by cs.yearNo desc, cs.monthNo desc;      "  ;


			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	

	
	@Override
	public List<Object[]> getRTFCustomerContestStatisticsforBusiness(SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			
			String sql =	"    SELECT 'NO OF REGISTRATIONS: ' as Heading, convert(varchar(50), convert(decimal(10,0),count(cu.contest_customer_id))) as Value     "  +
							"    FROM contest_registered_customer cu with(nolock)      "  +
							"    INNER JOIN " + sharedProperty.getDatabasAlias() + "customer oc with(nolock) on cu.contest_customer_id = oc.id     "  +
							"    WHERE cu.contest_customer_signup_date >= '2018-08-10 00:00:00.000' and oc.is_test_account = 0     "  +
							"    UNION     "  +
							"    SELECT 'REWARD THE FAN GAMES', convert(varchar(50), convert(decimal(10,0),count(distinct contestNo))) as Value      "  +
							"    FROM rtf_rep_Contest_Reward_Dollar_Report with(nolock)      "  +
							"    UNION     "  +
							"    SELECT 'NO OF PLAYERS WHO PLAYED', convert(varchar(50), convert(decimal(10,0),count(distinct customerNo))) as Value      "  +
							"    FROM rtf_rep_Contest_Reward_Dollar_Report  rd with(nolock)      "  +
							"    INNER JOIN " + sharedProperty.getDatabasAlias() + "customer oc with(nolock) on rd.customerNo = oc.id     "  +
							"    where oc.is_test_account = 0     "  +
							"    UNION     "  +
							"    SELECT 'PLAYERS WHO PLAYED MULTIPLE TIMES', convert(varchar(50), convert(decimal(10,0),count(customerNo))) as Value      "  +
							"    FROM    (    SELECT customerNo FROM    (         "  +
							"                SELECT customerNo, contestNo FROM rtf_rep_Contest_Reward_Dollar_Report rd with(nolock)     "  +
							"                INNER JOIN " + sharedProperty.getDatabasAlias() + "customer oc with(nolock) on rd.customerNo = oc.id     "  +
							"                where oc.is_test_account = 0      "  +
							"                GROUP BY customerNo, contestNo  ) a     "  +
							"                GROUP BY customerNo having count(1) > 2    ) b     "  +
							"    UNION     "  +
							"    SELECT 'PLAYERS WHO PLAY CONSISTENTLY', convert(varchar(50), convert(decimal(10,0),count(customerNo))) as Value      "  +
							"    FROM    (    SELECT customerNo FROM    (         "  +
							"                SELECT customerNo, contestNo FROM rtf_rep_Contest_Reward_Dollar_Report rd with(nolock)     "  +
							"                INNER JOIN " + sharedProperty.getDatabasAlias() + "customer oc with(nolock) on rd.customerNo = oc.id     "  +
							"                where oc.is_test_account = 0      "  +
							"                GROUP BY customerNo, contestNo  ) a     "  +
							"                GROUP BY customerNo having count(1) > 25    ) b     "  +
							"    UNION     "  +
							"    SELECT 'NO OF AVERAGE VIEWERS PER DAY (2019 8PM GAME)', convert(varchar(50), av2019.avgViewers) as Value      "  +
							"    FROM    (    SELECT convert(decimal(8,2), (a.userViewersCount*1.0)/(a.contestCnt*1.0)) avgViewers FROM    (         "  +
							"                SELECT count(customerNo) userViewersCount, count(distinct contestNo) contestCnt     "  +
							"                FROM rtf_rep_Contest_Reward_Dollar_Report rd with(nolock)     "  +
							"                INNER JOIN " + sharedProperty.getDatabasAlias() + "customer oc with(nolock) on rd.customerNo = oc.id     "  +
							"                where oc.is_test_account = 0      "  +
							"                and rd.contestDate >= CONVERT(datetime, '2019-01-01 00:00:00.000')      "  +
							"                AND convert(varchar(20), rd.contestDate, 108) = '20:00:00'  ) a     "  +
							"                    ) av2019     "  +
							"    UNION     "  +
							"    SELECT 'NO OF AVERAGE PLAYERS PER DAY (2019 8PM GAME)', convert(varchar(50), ap2019.avgPlayers) as Value      "  +
							"    FROM    (    SELECT convert(decimal(8,2), (a.userPlayerrsCount*1.0)/(a.contestCnt*1.0)) avgPlayers FROM    (         "  +
							"                SELECT count(customerNo) userPlayerrsCount, count(distinct contestNo) contestCnt     "  +
							"                FROM rtf_rep_Contest_Reward_Dollar_Report rd with(nolock)     "  +
							"                INNER JOIN " + sharedProperty.getDatabasAlias() + "customer oc with(nolock) on rd.customerNo = oc.id     "  +
							"                where oc.is_test_account = 0      "  +
							"                and rd.contestDate >= CONVERT(datetime, '2019-01-01 00:00:00.000')      "  +
							"                AND convert(varchar(20), rd.contestDate, 108) = '20:00:00'       "  +
							"                and rd.contestQuestionsAttempted > 0) a     "  +
							"                    ) ap2019     "  +
							"    UNION     "  +
							"    SELECT 'HIGHEST PLAYER COUNT IN PEAK GAME', convert(varchar(50), convert(decimal(10,0),mx.maxPlayers)) as Value      "  +
							"    FROM    (    SELECT max(userPlayerrsCount) maxPlayers FROM    (         "  +
							"                SELECT count(customerNo) userPlayerrsCount, contestNo     "  +
							"                FROM rtf_rep_Contest_Reward_Dollar_Report rd with(nolock)     "  +
							"                INNER JOIN " + sharedProperty.getDatabasAlias() + "customer oc with(nolock) on rd.customerNo = oc.id     "  +
							"                where oc.is_test_account = 0      "  +
							"                group by contestNo) a     "  +
							"                    ) mx     "  +
							"    UNION     "  +
							"    SELECT 'NO OF TIMES APP SHARED', convert(varchar(50), convert(decimal(10,0),sd.shared)) as Value      "  +
							"    FROM    (    SELECT  convert(decimal(10,0),count(customer_id)*2.5) as shared      "  +
							"                FROM customer_referral_tracking rt with(nolock)     "  +
							"                INNER JOIN contest_registered_customer rc with(nolock) on rt.referral_customer_id = rc.contest_customer_id         "  +
							"                INNER JOIN " + sharedProperty.getDatabasAlias() + "customer oc with(nolock) on rc.contest_customer_id = oc.id     "  +
							"                where oc.is_test_account = 0     "  +
							"                    ) sd     "  +
							"    UNION     "  +
							"    SELECT 'NO OF REFFERALS', convert(varchar(50), convert(decimal(10,0),count(customer_id))) as Value      "  +
							"    FROM customer_referral_tracking rt with(nolock)     "  +
							"    INNER JOIN contest_registered_customer rc with(nolock) on rt.referral_customer_id = rc.contest_customer_id         "  +
							"    INNER JOIN " + sharedProperty.getDatabasAlias() + "customer oc with(nolock) on rc.contest_customer_id = oc.id     "  +
							"    where oc.is_test_account = 0      "  +
							"    UNION     "  +
							"    SELECT 'NO OF WINNERS', convert(varchar(50), convert(decimal(10,0),sum(win.gwlottryWinners))) as Value      "  +
							"    FROM (    SELECT count(gw.customer_id) as gwlottryWinners, sum(gw.reward_tickets) as grandNoOfTickets,'REGULAR' as winType     "  +
							"            FROM contest_grand_winners  gw with(nolock)     "  +
							"            INNER JOIN contest co with(nolock) on gw.contest_id = co.id     "  +
							"            INNER JOIN contest_registered_customer rc with(nolock) on gw.customer_id = rc.contest_customer_id     "  +
							"            inner join " + sharedProperty.getDatabasAlias() + "customer c with(nolock) on rc.contest_customer_id = c.id     "  +
							"            WHERE co.status <> 'DELETED' and co.STATUS = 'EXPIRED' and co.contest_name not like '%test%'       "  +
							"            and co.is_test_contest = 0 and c.is_test_account = 0     "  +
							"            AND IIF(gw.winner_type IS NULL, 'REGULAR', gw.winner_type) = 'REGULAR'     "  +
							"            UNION     "  +
							"            select Count(gw.customer_id) as megalottryWinners, sum(gw.reward_tickets) as megaNoOfTickets, 'MEGAJACKPOT'     "  +
							"            FROM contest_grand_winners  gw with(nolock)     "  +
							"            INNER JOIN contest co with(nolock) on gw.contest_id = co.id     "  +
							"            INNER JOIN contest_registered_customer rc with(nolock) on gw.customer_id = rc.contest_customer_id     "  +
							"            inner join " + sharedProperty.getDatabasAlias() + "customer c with(nolock) on rc.contest_customer_id = c.id     "  +
							"            WHERE co.status <> 'DELETED' and co.STATUS = 'EXPIRED' and co.contest_name not like '%test%'       "  +
							"            and co.is_test_contest = 0 and c.is_test_account = 0     "  +
							"            AND gw.mini_jackpot_type = 'TICKET'  and gw.winner_type = 'MEGAJACKPOT'      "  +
							"            UNION     "  +
							"            select Count(gw.customer_id) as minilottryWinners, sum(gw.reward_tickets) as miniNoOfTickets, 'MINIJACKPOT'     "  +
							"            FROM contest_grand_winners  gw with(nolock)     "  +
							"            INNER JOIN contest co with(nolock) on gw.contest_id = co.id     "  +
							"            INNER JOIN contest_registered_customer rc with(nolock) on gw.customer_id = rc.contest_customer_id     "  +
							"            inner join " + sharedProperty.getDatabasAlias() + "customer c with(nolock) on rc.contest_customer_id = c.id     "  +
							"            WHERE co.status <> 'DELETED' and co.STATUS = 'EXPIRED' and co.contest_name not like '%test%'       "  +
							"            and co.is_test_contest = 0 and c.is_test_account = 0     "  +
							"            AND gw.mini_jackpot_type = 'TICKET'    and gw.winner_type = 'MINIJACKPOT'      "  +
							"            ) win     "  ;


			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	

	
	@Override
	public List<Object[]> getGiftCardPurchasedWinnersReport(String fromDate,String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {

			if(fromDate!=null && !fromDate.isEmpty()){
				fromDate += " 00:00:00.000";
			}
			if(toDate!=null && !toDate.isEmpty()){
				toDate += " 23:59:59:000";
			}

			String sql =	"    select gcd.CustomerID as CustomerID    "  +
							"      , gcd.CustomerFirsName as  FirstName    "  +
							"      , gcd.CustomerLastName as  LastName    "  +
							"      , gcd.CustomerUserID as  UserID    "  +
							"      , gcd.CustomereMail as  eMail    "  +
							"      , gcd.CustomerPhone as  Phone    "  +
							"      , gcd.CustomerSignUpDate as  SignUpDate    "  +
							"      , gcto.GiftCardTittle as GiftCardTittle    "  +
							"      , gcto.GCOrderQuantity as GiftCardOrderQuantity    "  +
							"      , qatm.cntContestNo as NoofContestPlayedtill9Questions    "  +
							"    from  (  select c.id as CustomerID, c.cust_name as CustomerFirsName , c.last_name as CustomerLastName    "  +
							"            , c.user_id as CustomerUserID, c.email as CustomereMail, c.phone as CustomerPhone    "  +
							"            , c.signup_date as CustomerSignUpDate, sum(gco.quantity) as OrderQuantity    "  +
							"          from contest_registered_customer rc    "  +
							"          inner join " + sharedProperty.getDatabasAlias() + "customer c on rc.contest_customer_id = c.id    "  +
							"          inner join " + sharedProperty.getDatabasAlias() + "giftcard_order gco on c.id = gco.customer_id    "  +
							"          left join " + sharedProperty.getDatabasAlias() + "gift_card_value gcv on gco.card_id = gcv.id    "  +
							"          left join " + sharedProperty.getDatabasAlias() + "gift_cards gc on gcv.card_id = gc.id    "  +
							"          where 1=1    "  ;
							if(fromDate!=null && !fromDate.isEmpty()){
								sql+= "  and gco.created_date >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								sql+= "  and gco.created_date <= '"+toDate+"' ";
							}
					sql+=   "          group by c.id, c.cust_name, c.last_name, c.user_id, c.email, c.phone, c.signup_date    "  +
							"        ) gcd    "  +
							"    outer apply (  select gct.CustomerID, STUFF ( ( SELECT distinct ', '+ gc.GiftCardTittle    "  +
							"              from (  select c.id as CustomerID, gco.card_title as GiftCardTittle, sum(gco.quantity) as OrderQuantity    "  +
							"                  from contest_registered_customer rc    "  +
							"                  inner join " + sharedProperty.getDatabasAlias() + "customer c on rc.contest_customer_id = c.id    "  +
							"                  inner join " + sharedProperty.getDatabasAlias() + "giftcard_order gco on c.id = gco.customer_id    "  +
							"                  left join " + sharedProperty.getDatabasAlias() + "gift_card_value gcv on gco.card_id = gcv.id    "  +
							"                  left join " + sharedProperty.getDatabasAlias() + "gift_cards gc on gcv.card_id = gc.id    "  +
							"                  where 1=1    "  ;
							if(fromDate!=null && !fromDate.isEmpty()){
								sql+= "  and gco.created_date >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								sql+= "  and gco.created_date <= '"+toDate+"' ";
							}
					sql+=   "                  group by c.id,  gco.card_title     "  +
							"                ) gc  where gc.CustomerID = gct.CustomerID    "  +
							"                for xml path('') ), 1, 2, '')  GiftCardTittle    "  +
							"              , sum(gct.OrderQuantity) as GCOrderQuantity    "  +
							"            from (  select c.id as CustomerID , gco.card_title as GiftCardTittle, sum(gco.quantity) as OrderQuantity    "  +
							"                from contest_registered_customer rc    "  +
							"                inner join " + sharedProperty.getDatabasAlias() + "customer c on rc.contest_customer_id = c.id    "  +
							"                inner join " + sharedProperty.getDatabasAlias() + "giftcard_order gco on c.id = gco.customer_id    "  +
							"                left join " + sharedProperty.getDatabasAlias() + "gift_card_value gcv on gco.card_id = gcv.id    "  +
							"                left join " + sharedProperty.getDatabasAlias() + "gift_cards gc on gcv.card_id = gc.id    "  +
							"                where 1=1    "  ;
							if(fromDate!=null && !fromDate.isEmpty()){
								sql+= "  and gco.created_date >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								sql+= "  and gco.created_date <= '"+toDate+"' ";
							}
					sql+=   "                group by c.id,  gco.card_title     "  +
							"              ) gct    "  +
							"            where gct.CustomerID = gcd.CustomerID group by gct.CustomerID    "  +
							"            ) gcto    "  +
							"    outer apply ( select count(distinct vw.contestNo) as cntContestNo     "  +
							"            from rtf_rep_Contest_Reward_Dollar_Report vw with(nolock)    "  +
							"            where 1=1    "  ;
							if(fromDate!=null && !fromDate.isEmpty()){
								sql+= "  and vw.contestDate >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								sql+= "  and vw.contestDate <= '"+toDate+"' ";
							}
					sql+=   "            and (vw.contestQuestionsAttempted >= 9  )    "  +
							"            and vw.customerNo = gcd.CustomerID) qAtm    "  +
							"    order by gcto.GCOrderQuantity desc, gcd.CustomerUserID;    "  ;


			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	
	
	@Override
	public List<Object[]> getPollingStatistics(SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			
			String sql =	"    select ma.TotalRegisteredUsers as TotalRegisteredUsers    "  +
							"        , psd.poolingStart as PollingStartDate    "  +
							"        , tpp.NoofTimesPollingPlayedCount as NoofTimesPollingPlayed    "  +
							"        , tpc.NoofPlayersPollingPlayedCount as UniquePlayersCount    "  +
							"        , CONVERT(decimal(5,2),(tpc.NoofPlayersPollingPlayedCount*1.0/ma.TotalRegisteredUsers*1.0)*100 ) as PercentageofUsersPlaying    "  +
							"        , app.avgPlayers AveragePlayersPerDay    "  +
							"        , avm.avgPlayersMonth AveragePlayersPerMonth    "  +
							"        , apcp.avgPoolingPlayed AveragePollingPlayedperDay    "  +
							"        , apcm.avgPoolingPlayedMonth AveragePollingPlayedperMonth    "  +
							"    from    (    select count( rc.contest_customer_id) as TotalRegisteredUsers    "  +
							"                from contest_registered_customer rc    "  +
							"                inner join " + sharedProperty.getDatabasAlias() + "customer c on rc.contest_customer_id = c.id    "  +
							"                where c.is_test_account=0    "  +
							"            ) ma    "  +
							"    cross apply ( select count(1) as NoofTimesPollingPlayedCount    "  +
							"                from " + sharedProperty.getDatabasAlias() + "polling_answer_validation av    "  +
							"                inner join contest_registered_customer rc on av.cust_id = rc.contest_customer_id    "  +
							"                inner join " + sharedProperty.getDatabasAlias() + "customer c on rc.contest_customer_id = c.id    "  +
							"                inner join " + sharedProperty.getDatabasAlias() + "polling_question_bank qb on av.question_id = qb.id    "  +
							"                where c.is_test_account=0    "  +
							"                 ) tpp    "  +
							"    cross apply ( select count(distinct av.cust_id ) as NoofPlayersPollingPlayedCount    "  +
							"                from " + sharedProperty.getDatabasAlias() + "polling_answer_validation av    "  +
							"                inner join contest_registered_customer rc on av.cust_id = rc.contest_customer_id    "  +
							"                inner join " + sharedProperty.getDatabasAlias() + "customer c on rc.contest_customer_id = c.id    "  +
							"                inner join " + sharedProperty.getDatabasAlias() + "polling_question_bank qb on av.question_id = qb.id    "  +
							"                where c.is_test_account=0    "  +
							"                 ) tpc    "  +
							"    cross apply ( select avg(cnt) as avgPlayers from (    "  +
							"                    select count(1) cnt, createDate from (    "  +
							"                    select av.cust_id, CONVERT(date,av.created_date) createDate    "  +
							"                                from " + sharedProperty.getDatabasAlias() + "polling_answer_validation av    "  +
							"                                inner join contest_registered_customer rc on av.cust_id = rc.contest_customer_id    "  +
							"                                inner join " + sharedProperty.getDatabasAlias() + "customer c on rc.contest_customer_id = c.id    "  +
							"                                inner join " + sharedProperty.getDatabasAlias() + "polling_question_bank qb on av.question_id = qb.id    "  +
							"                                where c.is_test_account=0    "  +
							"                    group by av.cust_id, CONVERT(date,av.created_date) ) mb    "  +
							"                    group by mb.createDate ) mc    "  +
							"                ) app    "  +
							"    cross apply ( select avg(cnt) as avgPlayersMonth from (    "  +
							"                    select count(1) cnt, mb.createdYear, mb.createdMonth from (    "  +
							"                    select av.cust_id, DATEPART(YEAR,av.created_date) createdYear, DATEPART(MONTH,av.created_date) createdMonth    "  +
							"                                from " + sharedProperty.getDatabasAlias() + "polling_answer_validation av    "  +
							"                                inner join contest_registered_customer rc on av.cust_id = rc.contest_customer_id    "  +
							"                                inner join " + sharedProperty.getDatabasAlias() + "customer c on rc.contest_customer_id = c.id    "  +
							"                                inner join " + sharedProperty.getDatabasAlias() + "polling_question_bank qb on av.question_id = qb.id    "  +
							"                                where c.is_test_account=0    "  +
							"                    group by av.cust_id, DATEPART(YEAR,av.created_date), DATEPART(MONTH,av.created_date) ) mb    "  +
							"                    group by mb.createdYear, mb.createdMonth ) mc    "  +
							"                ) avm    "  +
							"    cross apply ( select convert(date,min(av.created_date)) poolingStart    "  +
							"                    from " + sharedProperty.getDatabasAlias() + "polling_answer_validation av    "  +
							"                    inner join contest_registered_customer rc on av.cust_id = rc.contest_customer_id    "  +
							"                    inner join " + sharedProperty.getDatabasAlias() + "customer c on rc.contest_customer_id = c.id    "  +
							"                    inner join " + sharedProperty.getDatabasAlias() + "polling_question_bank qb on av.question_id = qb.id    "  +
							"                    where c.is_test_account=0    "  +
							"                ) psd    "  +
							"    cross apply ( select avg(cnt) as avgPoolingPlayed from (    "  +
							"                    select count(1) cnt, createDate from (    "  +
							"                    select av.cust_id, av.question_id, CONVERT(date,av.created_date) createDate    "  +
							"                                from " + sharedProperty.getDatabasAlias() + "polling_answer_validation av    "  +
							"                                inner join contest_registered_customer rc on av.cust_id = rc.contest_customer_id    "  +
							"                                inner join " + sharedProperty.getDatabasAlias() + "customer c on rc.contest_customer_id = c.id    "  +
							"                                inner join " + sharedProperty.getDatabasAlias() + "polling_question_bank qb on av.question_id = qb.id    "  +
							"                                where c.is_test_account=0    "  +
							"                    group by av.cust_id, av.question_id, CONVERT(date,av.created_date) ) mb    "  +
							"                    group by mb.createDate ) mc    "  +
							"                ) apcp    "  +
							"    cross apply ( select avg(cnt) as avgPoolingPlayedMonth from (    "  +
							"                    select count(1) cnt, createdYear, createdMonth from (    "  +
							"                    select av.cust_id, av.question_id, DATEPART(YEAR,av.created_date) createdYear, DATEPART(MONTH,av.created_date) createdMonth    "  +
							"                                from " + sharedProperty.getDatabasAlias() + "polling_answer_validation av    "  +
							"                                inner join contest_registered_customer rc on av.cust_id = rc.contest_customer_id    "  +
							"                                inner join " + sharedProperty.getDatabasAlias() + "customer c on rc.contest_customer_id = c.id    "  +
							"                                inner join " + sharedProperty.getDatabasAlias() + "polling_question_bank qb on av.question_id = qb.id    "  +
							"                                where c.is_test_account=0    "  +
							"                    group by av.cust_id, av.question_id, DATEPART(YEAR,av.created_date), DATEPART(MONTH,av.created_date) ) mb    "  +
							"                    group by createdYear, createdMonth ) mc    "  +
							"                ) apcm;    "  ;



			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	

	@Override
	public List<Object[]> getRTFTVStatistics(SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			
			String sql =	"    select ma.TotalRegisteredUsers as TotalRegisteredUser     "  +
							"        , psd.rtfTVStartDate as RTFTVStartDate     "  +
							"        , tpp.NoofTimesVideoPlayedCount as NoofTimesViewed     "  +
							"        , tpc.NoofPlayersVideoPlayedCount as UniqueViewersCount     "  +
							"        , CONVERT(decimal(5,2),(tpc.NoofPlayersVideoPlayedCount*1.0/ma.TotalRegisteredUsers*1.0)*100 ) as PercOfViewers     "  +
							"        , app.avgViewers as AverageViewersPerDay     "  +
							"        , avm.avgViewersMonth as AverageViewersPerMonth     "  +
							"        , apcp.avgVideoPlayed as AverageVideosPlayedperDay     "  +
							"        , apcm.avgVideoPlayedMonth as AverageVideosPlayedperMonth     "  +
							"        , upd.avgVideoUploadedDay as AverageVideosUploadedperDay     "  +
							"        , upm.avgVideoUploadedMonth as AverageVideosUploadedperMonth     "  +
							"    from    (    select count( rc.contest_customer_id) as TotalRegisteredUsers     "  +
							"            from  contest_registered_customer rc     "  +
							"            inner join " + sharedProperty.getDatabasAlias() + "customer c on rc.contest_customer_id = c.id     "  +
							"            where c.is_test_account=0     "  +
							"        ) ma     "  +
							"    cross apply ( select count(1) as NoofTimesVideoPlayedCount     "  +
							"            from (select id,cust_id,play_date from " + sharedProperty.getDatabasAlias() + "polling_video_played     "  +
							"                  union      "  +
							"                  select id,cust_id,play_date from " + sharedProperty.getDatabasAlias() + "polling_video_played_hist ) pvp     "  +
							"            inner join  contest_registered_customer rc on pvp.cust_id = rc.contest_customer_id     "  +
							"            inner join " + sharedProperty.getDatabasAlias() + "customer c on rc.contest_customer_id = c.id     "  +
							"            where c.is_test_account = 0     "  +
							"             ) tpp     "  +
							"    cross apply ( select count(distinct pvp.cust_id ) as NoofPlayersVideoPlayedCount     "  +
							"            from (select cust_id from " + sharedProperty.getDatabasAlias() + "polling_video_played     "  +
							"                  union      "  +
							"                  select cust_id from " + sharedProperty.getDatabasAlias() + "polling_video_played_hist ) pvp     "  +
							"            inner join  contest_registered_customer rc on pvp.cust_id = rc.contest_customer_id     "  +
							"            inner join " + sharedProperty.getDatabasAlias() + "customer c on rc.contest_customer_id = c.id     "  +
							"            where c.is_test_account = 0     "  +
							"             ) tpc     "  +
							"    cross apply ( select avg(cnt) as avgViewers from (     "  +
							"            select count(1) cnt, createDate from (     "  +
							"            select av.cust_id, CONVERT(date,av.play_date) createDate     "  +
							"            from (select id,cust_id,play_date from " + sharedProperty.getDatabasAlias() + "polling_video_played     "  +
							"                  union      "  +
							"                  select id,cust_id,play_date from " + sharedProperty.getDatabasAlias() + "polling_video_played_hist ) av     "  +
							"            inner join  contest_registered_customer rc on av.cust_id = rc.contest_customer_id     "  +
							"            inner join " + sharedProperty.getDatabasAlias() + "customer c on rc.contest_customer_id = c.id     "  +
							"            where c.is_test_account = 0     "  +
							"            group by av.cust_id, CONVERT(date,av.play_date) ) mb     "  +
							"            group by mb.createDate ) mc     "  +
							"            ) app     "  +
							"    cross apply ( select avg(cnt) as avgViewersMonth from (     "  +
							"            select count(1) cnt, mb.createdYear, mb.createdMonth from (     "  +
							"            select av.cust_id, DATEPART(YEAR,av.play_date) createdYear, DATEPART(MONTH,av.play_date) createdMonth     "  +
							"            from (select id,cust_id,play_date from " + sharedProperty.getDatabasAlias() + "polling_video_played     "  +
							"                  union      "  +
							"                  select id,cust_id,play_date from " + sharedProperty.getDatabasAlias() + "polling_video_played_hist ) av     "  +
							"            inner join  contest_registered_customer rc on av.cust_id = rc.contest_customer_id     "  +
							"            inner join " + sharedProperty.getDatabasAlias() + "customer c on rc.contest_customer_id = c.id     "  +
							"            where c.is_test_account = 0     "  +
							"            group by av.cust_id, DATEPART(YEAR,av.play_date), DATEPART(MONTH,av.play_date) ) mb     "  +
							"            group by mb.createdYear, mb.createdMonth ) mc     "  +
							"            ) avm     "  +
							"    cross apply ( select convert(date,min(av.play_date)) rtfTVStartDate     "  +
							"            from (select id,cust_id,play_date from " + sharedProperty.getDatabasAlias() + "polling_video_played     "  +
							"                  union      "  +
							"                  select id,cust_id,play_date from " + sharedProperty.getDatabasAlias() + "polling_video_played_hist ) av     "  +
							"            inner join  contest_registered_customer rc on av.cust_id = rc.contest_customer_id     "  +
							"            inner join " + sharedProperty.getDatabasAlias() + "customer c on rc.contest_customer_id = c.id     "  +
							"            where c.is_test_account = 0     "  +
							"            ) psd     "  +
							"    cross apply ( select avg(cnt) as avgVideoPlayed from (     "  +
							"            select count(1) cnt, createDate from (     "  +
							"            select av.cust_id, av.vid_id, CONVERT(date,av.play_date) createDate     "  +
							"            from (select id,cust_id,vid_id,play_date from " + sharedProperty.getDatabasAlias() + "polling_video_played     "  +
							"                  union      "  +
							"                  select id,cust_id,vid_id,play_date from " + sharedProperty.getDatabasAlias() + "polling_video_played_hist ) av     "  +
							"            inner join  contest_registered_customer rc on av.cust_id = rc.contest_customer_id     "  +
							"            inner join " + sharedProperty.getDatabasAlias() + "customer c on rc.contest_customer_id = c.id     "  +
							"            where c.is_test_account = 0     "  +
							"            group by av.cust_id, av.vid_id, CONVERT(date,av.play_date) ) mb     "  +
							"            group by mb.createDate ) mc     "  +
							"            ) apcp     "  +
							"    cross apply ( select avg(cnt) as avgVideoPlayedMonth from (     "  +
							"            select count(1) cnt, createdYear, createdMonth from (     "  +
							"            select av.cust_id, av.vid_id, DATEPART(YEAR,av.play_date) createdYear, DATEPART(MONTH,av.play_date) createdMonth     "  +
							"            from (select id,cust_id,vid_id,play_date from " + sharedProperty.getDatabasAlias() + "polling_video_played     "  +
							"                  union      "  +
							"                  select id,cust_id,vid_id,play_date from " + sharedProperty.getDatabasAlias() + "polling_video_played_hist ) av     "  +
							"            inner join  contest_registered_customer rc on av.cust_id = rc.contest_customer_id     "  +
							"            inner join " + sharedProperty.getDatabasAlias() + "customer c on rc.contest_customer_id = c.id     "  +
							"            where c.is_test_account = 0     "  +
							"            group by av.cust_id, av.vid_id, DATEPART(YEAR,av.play_date), DATEPART(MONTH,av.play_date) ) mb     "  +
							"            group by createdYear, createdMonth ) mc     "  +
							"            ) apcm     "  +
							"    cross apply ( select avg(cnt) as avgVideoUploadedDay from (     "  +
							"            select count(1) cnt, createDate from (     "  +
							"            select av.cust_id, CONVERT(date,av.upload_date) createDate     "  +
							"            from " + sharedProperty.getDatabasAlias() + "polling_customer_video_uploads av     "  +
							"            inner join  contest_registered_customer rc on av.cust_id = rc.contest_customer_id     "  +
							"            inner join " + sharedProperty.getDatabasAlias() + "customer c on rc.contest_customer_id = c.id     "  +
							"            where c.is_test_account = 0     "  +
							"            group by av.cust_id, CONVERT(date,av.upload_date) ) mb     "  +
							"            group by mb.createDate ) mc     "  +
							"            ) upd     "  +
							"    cross apply ( select avg(cnt) as avgVideoUploadedMonth from (     "  +
							"            select count(1) cnt, createdYear, createdMonth from (     "  +
							"            select av.cust_id, DATEPART(YEAR,av.upload_date) createdYear, DATEPART(MONTH,av.upload_date) createdMonth     "  +
							"            from " + sharedProperty.getDatabasAlias() + "polling_customer_video_uploads av     "  +
							"            inner join  contest_registered_customer rc on av.cust_id = rc.contest_customer_id     "  +
							"            inner join " + sharedProperty.getDatabasAlias() + "customer c on rc.contest_customer_id = c.id     "  +
							"            where c.is_test_account = 0     "  +
							"            group by av.cust_id, DATEPART(YEAR,av.upload_date), DATEPART(MONTH,av.upload_date) ) mb     "  +
							"            group by createdYear, createdMonth ) mc     "  +
							"            ) upm;     "    ;



			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	


	@Override
	public List<Object[]> getCustomerTimeSpentOnEachContest(String fromDate,String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {

			if(fromDate!=null && !fromDate.isEmpty()){
				fromDate += " 00:00:00.000";
			}
			if(toDate!=null && !toDate.isEmpty()){
				toDate += " 23:59:59:000";
			}

			String sql =	"    select contestNo as ContestID, REPLACE(contestName, '  ','') as ContestName     "  +
							"        , contestDate as ContestDate, customerNo as CustomerID     "  +
							"        , customerUserID as CustomerUserID, minsSpentInContest as MinutesSpentOnContest     "  +
							"    from rtf_rep_Contest_Reward_Dollar_Report     "  +
							"    where 1=1     "  ;
							if(fromDate!=null && !fromDate.isEmpty()){
								sql+= "  and contestDate >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								sql+= "  and contestDate <= '"+toDate+"' ";
							}
				sql+= 		"    order by contestDate desc, customerNo;     "  ;

							
					
			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	
	
	
	@Override
	public List<Object[]> getRTFCustomerAverageTimeTakenperContestStatistics(String fromDate,String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {

			if(fromDate!=null && !fromDate.isEmpty()){
				fromDate += " 00:00:00.000";
			}
			if(toDate!=null && !toDate.isEmpty()){
				toDate += " 23:59:59:000";
			}

			String sql =	"    select mst.contestNo as ContestID, mst.contestName as ContestName     "  +
							"        , mst.contestDate as ContestDate, mst.ParticipantsCount as OverallParticipantsCount     "  +
							"        , mst.OverallAverageMinutes as OverallAverageMinutesSpentOnContest     "  +
							"        , zf.pcZeroToFive as CountWhoSpent0to5Mins, zf.avgZeroToFive as AvgCountWhoSpent0to5Mins     "  +
							"        , ft.pcFiveToTen as CountWhoSpent5to10Mins, ft.avgFiveToTen as AvgCountWhoSpent5to10Mins     "  +
							"        , tf.pcTenToFifteen as CountWhoSpent10to15Mins, tf.avgTenToFifteen as AvgCountWhoSpent10to15Mins     "  +
							"        , fa.pcFifteenAbove as CountWhoSpent15Minsabove, fa.avgFifteenAbove as AvgCountWhoSpent15Minsabove     "  +
							"        , ze.pcZeroToEleven as CountWhoSpent0to11Mins, ze.avgZeroToEleven as AvgCountWhoSpent0to11Mins     "  +
							"        , ea.pcElevenAbove as CountWhoSpent11Minsabove, ea.avgElevenAbove as AvgCountWhoSpent11Minsabove     "  +
							"    From    (    select contestNo, contestName, contestDate      "  +
							"                    , count(customerNo) as ParticipantsCount     "  +
							"                    , CONVERT(DECIMAL(6,2), AVG(minsSpentInContest*1.00) ) as OverallAverageMinutes     "  +
							"                from  rtf_rep_Contest_Reward_Dollar_Report     "  +
							"                where 1=1     "  ;
							if(fromDate!=null && !fromDate.isEmpty()){
								sql+= "  and contestDate >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								sql+= "  and contestDate <= '"+toDate+"' ";
							}
				sql+= 		"                group by contestNo, contestName, contestDate     "  +
							"            ) mst     "  +
							"    outer apply (    select contestNo, contestName, contestDate      "  +
							"                        , count(customerNo) as pcZeroToFive     "  +
							"                        , CONVERT(DECIMAL(6,2), AVG(minsSpentInContest*1.00) ) as avgZeroToFive     "  +
							"                    from  rtf_rep_Contest_Reward_Dollar_Report     "  +
							"                    where 1=1 and minsSpentInContest >= 0 and minsSpentInContest <= 5     "  +
							"                    and contestNo = mst.contestNo     "  +
							"                    and contestDate = mst.contestDate     "  ;
							if(fromDate!=null && !fromDate.isEmpty()){
								sql+= "  and contestDate >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								sql+= "  and contestDate <= '"+toDate+"' ";
							}
				sql+= 		"                    group by contestNo, contestName, contestDate     "  +
							"                ) zf     "  +
							"    outer apply (    select contestNo, contestName, contestDate      "  +
							"                        , count(customerNo) as pcFiveToTen     "  +
							"                        , CONVERT(DECIMAL(6,2), AVG(minsSpentInContest*1.00) ) as avgFiveToTen     "  +
							"                    from  rtf_rep_Contest_Reward_Dollar_Report     "  +
							"                    where 1=1 and minsSpentInContest > 5 and minsSpentInContest <= 10     "  +
							"                    and contestNo = mst.contestNo     "  +
							"                    and contestDate = mst.contestDate     "  ;
							if(fromDate!=null && !fromDate.isEmpty()){
								sql+= "  and contestDate >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								sql+= "  and contestDate <= '"+toDate+"' ";
							}
				sql+= 		"                    group by contestNo, contestName, contestDate     "  +
							"                ) ft     "  +
							"    outer apply (    select contestNo, contestName, contestDate      "  +
							"                        , count(customerNo) as pcTenToFifteen     "  +
							"                        , CONVERT(DECIMAL(6,2), AVG(minsSpentInContest*1.00) ) as avgTenToFifteen     "  +
							"                    from  rtf_rep_Contest_Reward_Dollar_Report     "  +
							"                    where 1=1 and minsSpentInContest > 10 and minsSpentInContest <= 15     "  +
							"                    and contestNo = mst.contestNo     "  +
							"                    and contestDate = mst.contestDate     "  ;
							if(fromDate!=null && !fromDate.isEmpty()){
								sql+= "  and contestDate >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								sql+= "  and contestDate <= '"+toDate+"' ";
							}
				sql+= 		"                    group by contestNo, contestName, contestDate     "  +
							"                ) tf     "  +
							"    outer apply (    select contestNo, contestName, contestDate      "  +
							"                        , count(customerNo) as pcFifteenAbove     "  +
							"                        , CONVERT(DECIMAL(6,2), AVG(minsSpentInContest*1.00) ) as avgFifteenAbove     "  +
							"                    from  rtf_rep_Contest_Reward_Dollar_Report     "  +
							"                    where 1=1 and minsSpentInContest > 15      "  +
							"                    and contestNo = mst.contestNo     "  +
							"                    and contestDate = mst.contestDate     "  ;
							if(fromDate!=null && !fromDate.isEmpty()){
								sql+= "  and contestDate >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								sql+= "  and contestDate <= '"+toDate+"' ";
							}
				sql+= 		"                    group by contestNo, contestName, contestDate     "  +
							"                ) fa     "  +
							"    outer apply (    select contestNo, contestName, contestDate      "  +
							"                        , count(customerNo) as pcZeroToEleven     "  +
							"                        , CONVERT(DECIMAL(6,2), AVG(minsSpentInContest*1.00) ) as avgZeroToEleven     "  +
							"                    from  rtf_rep_Contest_Reward_Dollar_Report     "  +
							"                    where 1=1 and minsSpentInContest >= 0 and minsSpentInContest <= 11     "  +
							"                    and contestNo = mst.contestNo     "  +
							"                    and contestDate = mst.contestDate     "  ;
							if(fromDate!=null && !fromDate.isEmpty()){
								sql+= "  and contestDate >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								sql+= "  and contestDate <= '"+toDate+"' ";
							}
				sql+= 		"                    group by contestNo, contestName, contestDate     "  +
							"                ) ze     "  +
							"    outer apply (    select contestNo, contestName, contestDate      "  +
							"                        , count(customerNo) as pcElevenAbove     "  +
							"                        , CONVERT(DECIMAL(6,2), AVG(minsSpentInContest*1.00) ) as avgElevenAbove     "  +
							"                    from  rtf_rep_Contest_Reward_Dollar_Report     "  +
							"                    where 1=1 and minsSpentInContest > 11      "  +
							"                    and contestNo = mst.contestNo     "  +
							"                    and contestDate = mst.contestDate     "  ;
							if(fromDate!=null && !fromDate.isEmpty()){
								sql+= "  and contestDate >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								sql+= "  and contestDate <= '"+toDate+"' ";
							}
				sql+= 		"                    group by contestNo, contestName, contestDate     "  +
							"                ) ea     "  +
							"    order by mst.contestDate     "  ;

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	
	
	
	@Override
	public List<Object[]> getGrandMegaMINIJackpotTicketWinnerCount(String fromDate,String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {

			if(fromDate!=null && !fromDate.isEmpty()){
				fromDate += " 00:00:00.000";
			}
			if(toDate!=null && !toDate.isEmpty()){
				toDate += " 23:59:59:000";
			}

			String sql =	"    select nc.cntContestNo, gwlottryWinners, grandNoOfTickets, mg.megalottryWinners      "  +
							"        , mg.megaNoOfTickets, mi.minilottryWinners, mi.miniNoOfTickets      "  +
							"    from  ( SELECT count(gw.customer_id) as gwlottryWinners, sum(gw.reward_tickets) as grandNoOfTickets      "  +
							"            FROM  contest_grand_winners  gw with(nolock)     "  +
							"            INNER JOIN  contest co with(nolock) on gw.contest_id = co.id     "  +
							"            INNER JOIN  contest_registered_customer rc with(nolock) on gw.customer_id = rc.contest_customer_id     "  +
							"            inner join " + sharedProperty.getDatabasAlias() + "customer c with(nolock) on rc.contest_customer_id = c.id     "  +
							"            WHERE co.status <> 'DELETED' and co.STATUS = 'EXPIRED' and co.contest_name not like '%test%'       "  +
							"            and co.is_test_contest = 0 and c.is_test_account = 0  and gw.reward_tickets > 0   "  +
							"            AND IIF(gw.winner_type IS NULL, 'REGULAR', gw.winner_type) = 'REGULAR'     "  ;
							if(fromDate!=null && !fromDate.isEmpty()){
								sql+= "  and co.contest_start_datetime  >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								sql+= "  and co.contest_start_datetime  <= '"+toDate+"' ";
							}
				sql+= 		"           ) lw     "  +
							"    CROSS APPLY  (  select Count(gw.customer_id) as megalottryWinners, sum(gw.reward_tickets) as megaNoOfTickets     "  +
							"                    FROM  contest_grand_winners  gw with(nolock)     "  +
							"                    INNER JOIN  contest co with(nolock) on gw.contest_id = co.id     "  +
							"                    INNER JOIN  contest_registered_customer rc with(nolock) on gw.customer_id = rc.contest_customer_id     "  +
							"                    inner join " + sharedProperty.getDatabasAlias() + "customer c with(nolock) on rc.contest_customer_id = c.id     "  +
							"                    WHERE co.status <> 'DELETED' and co.STATUS = 'EXPIRED' and co.contest_name not like '%test%'       "  +
							"                    and co.is_test_contest = 0 and c.is_test_account = 0     "  +
							"                    AND gw.mini_jackpot_type = 'TICKET'  and gw.winner_type = 'MEGAJACKPOT'      "  ;
							if(fromDate!=null && !fromDate.isEmpty()){
								sql+= "  and co.contest_start_datetime  >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								sql+= "  and co.contest_start_datetime  <= '"+toDate+"' ";
							}
				sql+= 		"                  ) mg     "  +
							"    CROSS APPLY  (  select Count(gw.customer_id) as minilottryWinners, sum(gw.reward_tickets) as miniNoOfTickets     "  +
							"                    FROM  contest_grand_winners  gw with(nolock)     "  +
							"                    INNER JOIN  contest co with(nolock) on gw.contest_id = co.id     "  +
							"                    INNER JOIN  contest_registered_customer rc with(nolock) on gw.customer_id = rc.contest_customer_id     "  +
							"                    inner join " + sharedProperty.getDatabasAlias() + "customer c with(nolock) on rc.contest_customer_id = c.id     "  +
							"                    WHERE co.status <> 'DELETED' and co.STATUS = 'EXPIRED' and co.contest_name not like '%test%'       "  +
							"                    and co.is_test_contest = 0 and c.is_test_account = 0     "  +
							"                    AND gw.mini_jackpot_type = 'TICKET'    and gw.winner_type = 'MINIJACKPOT'      "  ;
							if(fromDate!=null && !fromDate.isEmpty()){
								sql+= "  and co.contest_start_datetime  >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								sql+= "  and co.contest_start_datetime  <= '"+toDate+"' ";
							}
				sql+= 		"                  ) mi     "  +
							"    CROSS APPLY (    select count(distinct vw.contestNo) as cntContestNo      "  +
							"                    from  rtf_rep_Contest_Reward_Dollar_Report vw with(nolock)     "  +
							"                    WHERE 1=1      "  ;
							if(fromDate!=null && !fromDate.isEmpty()){
								sql+= "  and vw.contestDate >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								sql+= "  and vw.contestDate <= '"+toDate+"' ";
							}
				sql+= 		"                ) nc     "  ;

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	
	
	
	@Override
	public List<Object[]> getCustomersUsingReferralCodesReport(String fromDate,String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {

			if(fromDate!=null && !fromDate.isEmpty()){
				fromDate += " 00:00:00.000";
			}
			if(toDate!=null && !toDate.isEmpty()){
				toDate += " 23:59:59:000";
			}

			String sql =	"    select c.user_id ,c.signup_date      "  +
							"        , UPPER(IIF(rt.referral_code is null, '', rt.referral_code)) as RefCodeUsed     "  +
							"    from " + sharedProperty.getDatabasAlias() + "customer c     "  +
							"    inner join customer_referral_tracking rt on c.id = rt.customer_id      "  +
							"        and rt.referral_customer_id in (select id from " + sharedProperty.getDatabasAlias() + "customer     "  +
							"                                        where 1=1     "  +
							"                                        and user_id in ('DFSKARMA', 'RTFBWAY')     "  +
							"                                        )     "  +
							"    where 1=1     "  ;
							if(fromDate!=null && !fromDate.isEmpty()){
								sql+= "  and c.signup_date >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								sql+= "  and c.signup_date <= '"+toDate+"' ";
							}
				sql+= 		"    order by rt.referral_code, c.signup_date;     "  ;


			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	
	
	@Override
	public List<Object[]> getGiftCardOrdersPendingFromRTFCount(SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			
			String sql =	"    select GiftCardTitle as 'Gift Card Title'    "  +
							"        , max(IIF(Amt$250 is null, 0, Amt$250)) as '$250', max(IIF(Amt$200 is null, 0, Amt$200)) as '$200'    "  +
							"        , max(IIF(Amt$100 is null, 0, Amt$100)) as '$100', max(IIF(Amt$50 is null, 0, Amt$50)) as '$50'    "  +
							"        , max(IIF(Amt$25 is null, 0, Amt$25)) as '$25', max(IIF(Amt$15 is null, 0, Amt$15)) as '$15'    "  +
							"        , max(IIF(Amt$10 is null, 0, Amt$10)) as '$10', max(IIF(Amt$5 is null, 0, Amt$5)) as '$5'    "  +
							"        , max(IIF(Amt$250 is null, 0, Amt$250)) + max(IIF(Amt$200 is null, 0, Amt$200)) +    "  +
							"          max(IIF(Amt$100 is null, 0, Amt$100)) + max(IIF(Amt$50 is null, 0, Amt$50)) +    "  +
							"          max(IIF(Amt$25 is null, 0, Amt$25)) + max(IIF(Amt$15 is null, 0, Amt$15)) +    "  +
							"          max(IIF(Amt$10 is null, 0, Amt$10)) + max(IIF(Amt$5 is null, 0, Amt$5)) as 'Total'    "  +
							"    from   (    select GiftCardTitle , GiftOrderStatus , IIF(ShippingAddressAvailable=1,'Y','N') 'CustomerAddressAvailable'    "  +
							"                       , case when GiftCardAmt='$250' then GiftOrderCount end as 'Amt$250'    "  +
							"                       , case when GiftCardAmt='$200' then GiftOrderCount end as 'Amt$200'    "  +
							"                       , case when GiftCardAmt='$100' then GiftOrderCount end as 'Amt$100'    "  +
							"                       , case when GiftCardAmt='$50' then GiftOrderCount end as 'Amt$50'    "  +
							"                       , case when GiftCardAmt='$25' then GiftOrderCount end as 'Amt$25'    "  +
							"                       , case when GiftCardAmt='$15' then GiftOrderCount end as 'Amt$15'    "  +
							"                       , case when GiftCardAmt='$10' then GiftOrderCount end as 'Amt$10'    "  +
							"                       , case when GiftCardAmt='$5' then GiftOrderCount end as 'Amt$5'    "  +
							"                from " + sharedProperty.getDatabasAlias() + "gift_card_order_status_statistics_vw a    "  +
							"            ) mst    "  +
							"    where 1=1 and GiftOrderStatus not in ( 'NOTAVAILABLE','VOIDED','COMPLETED' )    "  +
							"    and GiftOrderStatus = 'OUTSTANDING'    "  +
							"    group by GiftCardTitle, GiftOrderStatus, CustomerAddressAvailable    "  +
							"    order by GiftCardTitle;    "  ;

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	

	@Override
	public List<Object[]> getGiftCardOrdersPendingFromCustomerCount(SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			
			String sql =	"    select GiftCardTitle as 'Gift Card Title'    "  +
							"        , max(IIF(Amt$250 is null, 0, Amt$250)) as '$250', max(IIF(Amt$200 is null, 0, Amt$200)) as '$200'    "  +
							"        , max(IIF(Amt$100 is null, 0, Amt$100)) as '$100', max(IIF(Amt$50 is null, 0, Amt$50)) as '$50'    "  +
							"        , max(IIF(Amt$25 is null, 0, Amt$25)) as '$25', max(IIF(Amt$15 is null, 0, Amt$15)) as '$15'    "  +
							"        , max(IIF(Amt$10 is null, 0, Amt$10)) as '$10', max(IIF(Amt$5 is null, 0, Amt$5)) as '$5'    "  +
							"        , max(IIF(Amt$250 is null, 0, Amt$250)) + max(IIF(Amt$200 is null, 0, Amt$200)) +    "  +
							"          max(IIF(Amt$100 is null, 0, Amt$100)) + max(IIF(Amt$50 is null, 0, Amt$50)) +    "  +
							"          max(IIF(Amt$25 is null, 0, Amt$25)) + max(IIF(Amt$15 is null, 0, Amt$15)) +    "  +
							"          max(IIF(Amt$10 is null, 0, Amt$10)) + max(IIF(Amt$5 is null, 0, Amt$5)) as 'Total'    "  +
							"    from   (    select GiftCardTitle , GiftOrderStatus , IIF(ShippingAddressAvailable=1,'Y','N') 'CustomerAddressAvailable'    "  +
							"                       , case when GiftCardAmt='$250' then GiftOrderCount end as 'Amt$250'    "  +
							"                       , case when GiftCardAmt='$200' then GiftOrderCount end as 'Amt$200'    "  +
							"                       , case when GiftCardAmt='$100' then GiftOrderCount end as 'Amt$100'    "  +
							"                       , case when GiftCardAmt='$50' then GiftOrderCount end as 'Amt$50'    "  +
							"                       , case when GiftCardAmt='$25' then GiftOrderCount end as 'Amt$25'    "  +
							"                       , case when GiftCardAmt='$15' then GiftOrderCount end as 'Amt$15'    "  +
							"                       , case when GiftCardAmt='$10' then GiftOrderCount end as 'Amt$10'    "  +
							"                       , case when GiftCardAmt='$5' then GiftOrderCount end as 'Amt$5'    "  +
							"                from " + sharedProperty.getDatabasAlias() + "gift_card_order_status_statistics_vw a    "  +
							"            ) mst    "  +
							"    where 1=1 and GiftOrderStatus not in ( 'NOTAVAILABLE','VOIDED','COMPLETED' )    "  +
							"    and GiftOrderStatus = 'PENDING' and CustomerAddressAvailable='N'    "  +
							"    group by GiftCardTitle, GiftOrderStatus, CustomerAddressAvailable    "  +
							"    order by GiftCardTitle;    "  ;
			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	

	@Override
	public List<Object[]> getGiftCardOrdersOverallPendingCount(SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {
			
			String sql =	"    select gMst.GiftCardTitle as 'Gift Card Title'        "  +
							"        , sum(gMst.[$250]) as '$250', sum(gMst.[$200]) as '$200'        "  +
							"        , sum(gMst.[$100]) as '$100', sum(gMst.[$50]) as '$50'        "  +
							"        , sum(gMst.[$25]) as '$25', sum(gMst.[$15]) as '$15'        "  +
							"        , sum(gMst.[$10]) as '$10', sum(gMst.[$5]) as '$5'        "  +
							"        , sum(gMst.Total) as Total        "  +
							"    From (    select GiftCardTitle         "  +
							"                , max(IIF(Amt$250 is null, 0, Amt$250)) as '$250', max(IIF(Amt$200 is null, 0, Amt$200)) as '$200'        "  +
							"                , max(IIF(Amt$100 is null, 0, Amt$100)) as '$100', max(IIF(Amt$50 is null, 0, Amt$50)) as '$50'        "  +
							"                , max(IIF(Amt$25 is null, 0, Amt$25)) as '$25', max(IIF(Amt$15 is null, 0, Amt$15)) as '$15'        "  +
							"                , max(IIF(Amt$10 is null, 0, Amt$10)) as '$10', max(IIF(Amt$5 is null, 0, Amt$5)) as '$5'        "  +
							"                , max(IIF(Amt$250 is null, 0, Amt$250)) + max(IIF(Amt$200 is null, 0, Amt$200)) +        "  +
							"                  max(IIF(Amt$100 is null, 0, Amt$100)) + max(IIF(Amt$50 is null, 0, Amt$50)) +        "  +
							"                  max(IIF(Amt$25 is null, 0, Amt$25)) + max(IIF(Amt$15 is null, 0, Amt$15)) +        "  +
							"                  max(IIF(Amt$10 is null, 0, Amt$10)) + max(IIF(Amt$5 is null, 0, Amt$5)) as 'Total'        "  +
							"            from   (    select GiftCardTitle , GiftOrderStatus , IIF(ShippingAddressAvailable=1,'Y','N') 'CustomerAddressAvailable'        "  +
							"                               , case when GiftCardAmt='$250' then GiftOrderCount end as 'Amt$250'        "  +
							"                               , case when GiftCardAmt='$200' then GiftOrderCount end as 'Amt$200'        "  +
							"                               , case when GiftCardAmt='$100' then GiftOrderCount end as 'Amt$100'        "  +
							"                               , case when GiftCardAmt='$50' then GiftOrderCount end as 'Amt$50'        "  +
							"                               , case when GiftCardAmt='$25' then GiftOrderCount end as 'Amt$25'        "  +
							"                               , case when GiftCardAmt='$15' then GiftOrderCount end as 'Amt$15'        "  +
							"                               , case when GiftCardAmt='$10' then GiftOrderCount end as 'Amt$10'        "  +
							"                               , case when GiftCardAmt='$5' then GiftOrderCount end as 'Amt$5'        "  +
							"                        from " + sharedProperty.getDatabasAlias() + "gift_card_order_status_statistics_vw a        "  +
							"                    ) mst        "  +
							"            where 1=1 and GiftOrderStatus not in ( 'NOTAVAILABLE','VOIDED','COMPLETED' )        "  +
							"            group by GiftCardTitle, GiftOrderStatus, CustomerAddressAvailable        "  +
							"            ) gMst        "  +
							"    group by gMst.GiftCardTitle        "  +
							"    order by gMst.GiftCardTitle;        "  ;

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	

	@Override
	public List<Object[]> getGiftCardOrdersOutstandingDetails(String fromDate,String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {

			if(fromDate!=null && !fromDate.isEmpty()){
				fromDate += " 00:00:00.000";
			}
			if(toDate!=null && !toDate.isEmpty()){
				toDate += " 23:59:59:000";
			}

			String sql =	"    select c.id as customerID, ltrim(rtrim(c.cust_name)) + ' ' + ltrim(rtrim(c.last_name)) as customerName    "  +
							"        , c.user_id as userID, c.email, c.phone, gc.title as giftCardTitle, gco.id as gcOrderID    "  +
							"        , gco.created_date, gco.quantity as gcOrderQuantity, convert(decimal(8,2),gco.original_cost) as gcCost    "  +
							"    from " + sharedProperty.getDatabasAlias() + "gift_cards gc    "  +
							"    left join " + sharedProperty.getDatabasAlias() + "gift_card_value gcv on gc.id = gcv.card_id    "  +
							"    left join " + sharedProperty.getDatabasAlias() + "giftcard_order gco on gcv.id = gco.card_id    "  +
							"    left join " + sharedProperty.getDatabasAlias() + "giftcard_order_attachment gcoa on gco.id = gcoa.order_id    "  +
							"    left join " + sharedProperty.getDatabasAlias() + "customer c on gco.customer_id = c.id    "  +
							"    where c.is_test_account = 0 and gc.title not like '%test%' and c.is_test_account = 0     "  +
							"    and gco.status = 'OUTSTANDING'     "  ;

							if(fromDate!=null && !fromDate.isEmpty()){
								sql+= "  and gco.created_date >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								sql+= "  and gco.created_date <= '"+toDate+"' ";
							}
				sql+= 		"    order by gco.created_date;     "  ;

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	
	
	
	@Override
	public List<Object[]> getGiftCardOrdersPendingDetails(String fromDate,String toDate,SharedProperty sharedProperty) {
		List<Object[]> list = null;
		try {

			if(fromDate!=null && !fromDate.isEmpty()){
				fromDate += " 00:00:00.000";
			}
			if(toDate!=null && !toDate.isEmpty()){
				toDate += " 23:59:59:000";
			}

			String sql =	"    select c.id as customerID, ltrim(rtrim(c.cust_name)) + ' ' + ltrim(rtrim(c.last_name)) as customerName    "  +
							"        , c.user_id as userID, c.email, c.phone, gc.title as giftCardTitle, gco.id as gcOrderID    "  +
							"        , gco.created_date, gco.quantity as gcOrderQuantity, convert(decimal(8,2),gco.original_cost) as gcCost    "  +
							"    from " + sharedProperty.getDatabasAlias() + "gift_cards gc    "  +
							"    left join " + sharedProperty.getDatabasAlias() + "gift_card_value gcv on gc.id = gcv.card_id    "  +
							"    left join " + sharedProperty.getDatabasAlias() + "giftcard_order gco on gcv.id = gco.card_id    "  +
							"    left join " + sharedProperty.getDatabasAlias() + "giftcard_order_attachment gcoa on gco.id = gcoa.order_id    "  +
							"    left join " + sharedProperty.getDatabasAlias() + "customer c on gco.customer_id = c.id    "  +
							"    where c.is_test_account = 0 and gc.title not like '%test%' and c.is_test_account = 0     "  +
							"    and gco.status = 'PENDING' and gco.shipping_address_id < 0    "  ;
		
							if(fromDate!=null && !fromDate.isEmpty()){
								sql+= "  and gco.created_date >= '"+fromDate+"' ";
							}
							if(toDate!=null && !toDate.isEmpty()){
								sql+= "  and gco.created_date <= '"+toDate+"' ";
							}
				sql+= 		"    order by gco.created_date;     "  ;

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			list = query.list();

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	
	
	/*----// Shiva ENDS of Adding New Report Controller Methods on 24 Oct 2019 //----*/
	
}
