package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.datas.ContestPromocode;
import com.rtw.tracker.datas.CustomerContestQuestion;
import com.rtw.tracker.utils.GridHeaderFilters;



public class CustomerContestQuestionDAO extends HibernateDAO<Integer, CustomerContestQuestion> implements com.rtw.tracker.dao.services.CustomerContestQuestionDAO{

	
private static Session contestQuestionSession;
	
	
	public static Session getContestSession() {
		return CustomerContestQuestionDAO.contestQuestionSession;
	}

	public static void setContestSession(Session contestSession) {
		CustomerContestQuestionDAO.contestQuestionSession = contestSession;
	}
	
	
	
	@Override
	public List<CustomerContestQuestion> getAllCustomerQuestion(GridHeaderFilters filter, String status) {
		List<CustomerContestQuestion> questionList = null;
		try {
			String sql = "select id as id,question_text as questionText,option_a as optionA,option_b as optionB,option_c as optionC,answer as answer," +
			" created_date as createdDate,submitted_by as submittedBy,status as status from submitted_questions WHERE 1=1 ";
			
			if(status!=null && !status.isEmpty()){
				sql += " AND status like '"+status+"' ";
			}
			if(filter.getText7()!=null && !filter.getText7().isEmpty()){
				sql += " AND question_text like '%"+filter.getText7()+"%' ";
			}
			if(filter.getText1()!=null && !filter.getText1().isEmpty()){
				sql += " AND option_a like '%"+filter.getText1()+"%' ";
			}
			if(filter.getText2()!=null && !filter.getText2().isEmpty()){
				sql += " AND option_b like '%"+filter.getText2()+"%' ";
			}
			if(filter.getText3()!=null && !filter.getText3().isEmpty()){
				sql += " AND option_c like '%"+filter.getText3()+"%' ";
			}
			
			if(filter.getText5()!=null && !filter.getText5().isEmpty()){
				sql += " AND answer like '%"+filter.getText5()+"%' ";
			}
			if(filter.getStatus()!=null && !filter.getStatus().isEmpty()){
				sql += " AND status like '%"+filter.getStatus()+"%' ";
			}
			
			if(filter.getCreatedBy()!=null && !filter.getCreatedBy().isEmpty()){
				sql += " AND submitted_by like '"+filter.getCreatedBy()+"' ";
			}
			
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year, created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			sql += " order by id desc";
			contestQuestionSession = CustomerContestQuestionDAO.getContestSession();
			if(contestQuestionSession==null || !contestQuestionSession.isOpen() || !contestQuestionSession.isConnected()){
				contestQuestionSession = getSession();
				CustomerContestQuestionDAO.setContestSession(contestQuestionSession);
			}
			SQLQuery query = contestQuestionSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("questionText", Hibernate.STRING);
			query.addScalar("optionA", Hibernate.STRING);
			query.addScalar("optionB", Hibernate.STRING);
			query.addScalar("optionC", Hibernate.STRING);
			query.addScalar("answer", Hibernate.STRING);	
			query.addScalar("createdDate", Hibernate.TIMESTAMP);	
			query.addScalar("submittedBy", Hibernate.STRING);	
			query.addScalar("status", Hibernate.STRING);	
			questionList = query.setResultTransformer(Transformers.aliasToBean(CustomerContestQuestion.class)).list();
		} catch (Exception e) {
			if(contestQuestionSession != null && contestQuestionSession.isOpen()){
				contestQuestionSession.close();
			}
			e.printStackTrace();
		}
		return questionList;
	}

}
