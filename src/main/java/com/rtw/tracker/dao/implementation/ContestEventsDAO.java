package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.datas.ContestEvents;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.PaginationUtil;

public class ContestEventsDAO extends HibernateDAO<Integer, ContestEvents> implements com.rtw.tracker.dao.services.ContestEventsDAO{

	//private String zonesApiDbUrl = "ZonesApiPlatformV2.dbo";
	
	private static Session contestSession = null;
	
	public static Session getContestSession() {
		return ContestEventsDAO.contestSession;
	}
	public static void setContestSession(Session contestSession) {
		ContestEventsDAO.contestSession = contestSession;
	}
	
	@Override
	public List<ContestEvents> getAllEvents(GridHeaderFilters filter,Integer contestId, String pageNo,String zones,Double singleTixPrice,Integer requiredQty,
			SharedProperty sharedProperty,Boolean isPagination) {
		List<ContestEvents> eventList = null;
		Integer firstResult = 0;
		try {
			
			 
			
			String sql = "select distinct contestID as contestId, eventId as eventId, eventName as eventName, event_date as eventDate, event_time as eventTime, "
					+ "venueId as venueId,venueName as building from "
					+ ""+sharedProperty.getDatabasAlias()+"events_for_contest_winners_new e with(nolock) where e.contestID="+contestId+" and e.event_date > GETDATE() "
					+ "and eventId not in (select ce.event_id from contest_events ce with(nolock) where ce.contest_id = "+contestId+") ";
			
			if(contestId!=null){
				sql += " AND contestID ="+contestId;
			}
			if(filter.getEventId()!=null){
				sql += " AND eventId ="+filter.getEventId();
			}
			if(filter.getEventName()!=null && !filter.getEventName().isEmpty()){
				sql += " AND eventName like '%"+filter.getEventName()+"%'";
			}
			if(filter.getVenueId()!=null){
				sql += " AND venueId ="+filter.getVenueId();
			}
			if(filter.getVenueName()!=null && !filter.getVenueName().isEmpty()){
				sql += " AND venueName like '%"+filter.getVenueName()+"%'";
			}			
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day, event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month, event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year, event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventTimeStr() != null){
				if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
					sql += " AND DATEPART(hour, event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
					sql += " AND DATEPART(minute, event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
				}
			}
			sql += " order by eventId";
			
			System.out.println(sql);
			
			contestSession = ContestEventsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestEventsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			
			query.addScalar("contestId", Hibernate.INTEGER);
			query.addScalar("eventId", Hibernate.INTEGER);
			query.addScalar("eventName", Hibernate.STRING);
			query.addScalar("eventDate", Hibernate.DATE);
			query.addScalar("eventTime", Hibernate.TIME);
			query.addScalar("venueId", Hibernate.INTEGER);						
			query.addScalar("building", Hibernate.STRING);			
			
			if(isPagination){
				firstResult = PaginationUtil.getNextPageStatFrom(pageNo);
				eventList = query.setResultTransformer(Transformers.aliasToBean(ContestEvents.class)).setFirstResult(firstResult).setMaxResults(PaginationUtil.PAGESIZE).list();
			}else{
				eventList = query.setResultTransformer(Transformers.aliasToBean(ContestEvents.class)).list();
			}
			
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return eventList;
	}
	
	public static void main(String[] args) {
		ContestEventsDAO dao = new ContestEventsDAO();
		
		dao.getAllEvents(new GridHeaderFilters(), 183, null, null, null, null, null, true);
		
	}

	@Override
	public Integer getAllEventsCount(GridHeaderFilters filter,Integer contestId,String zones,Double singleTixPrice,Integer requiredQty,SharedProperty sharedProperty){
		Integer count =0;		
		try {		
			 
			
			String sql = "select count(*) as cnt from "
					+ ""+sharedProperty.getDatabasAlias()+"events_for_contest_winners_new e with(nolock) where e.contestID="+contestId+" and e.event_date > GETDATE() "
					+ "and eventId not in (select ce.event_id from contest_events ce with(nolock) where ce.contest_id = "+contestId+") ";
			
			if(contestId!=null){
				sql += " AND contestID ="+contestId;
			}
			if(filter.getEventId()!=null){
				sql += " AND eventId ="+filter.getEventId();
			}
			if(filter.getEventName()!=null && !filter.getEventName().isEmpty()){
				sql += " AND eventName like '%"+filter.getEventName()+"%'";
			}
			if(filter.getVenueId()!=null){
				sql += " AND venueId ="+filter.getVenueId();
			}
			if(filter.getVenueName()!=null && !filter.getVenueName().isEmpty()){
				sql += " AND venueName like '%"+filter.getVenueName()+"%'";
			}			
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day, event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month, event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year, event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventTimeStr() != null){
				if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
					sql += " AND DATEPART(hour, event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
					sql += " AND DATEPART(minute, event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
				}
			}
			
			
			contestSession = ContestEventsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestEventsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			
			count = (Integer) query.uniqueResult();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return count;
	}
	
	@Override
	public List<ContestEvents> getContestEvents(GridHeaderFilters filter, Integer contestId,SharedProperty sharedProperty) {
		List<ContestEvents> contestEventList = null;
		try {
			String sql = "select distinct ce.contest_id as contestId, e.event_id as eventId, e.event_name as eventName, "+
						" e.event_date as eventDate, e.event_time as eventTime, e.venue_id as venueId, "+
						" v.building as building, ce.updated_by as updatedBy, ce.updated_date as updatedDateTime "+
						" from "+sharedProperty.getDatabasAlias()+"events_rtf e with(nolock) "+
						" inner join contest_events ce with(nolock) on ce.event_id = e.event_id "+						
						" inner join "+sharedProperty.getDatabasAlias()+"venue v with(nolock) on e.venue_id = v.id "+
						" WHERE 1=1 ";
			
			if(contestId!=null){
				sql += " AND ce.contest_id ="+contestId;
			}
			if(filter.getEventId()!=null){
				sql += " AND e.event_id ="+filter.getEventId();
			}
			if(filter.getEventName()!=null && !filter.getEventName().isEmpty()){
				sql += " AND e.event_name like '%"+filter.getEventName()+"%'";
			}
			if(filter.getVenueId()!=null){
				sql += " AND e.venue_id ="+filter.getVenueId();
			}
			if(filter.getVenueName()!=null && !filter.getVenueName().isEmpty()){
				sql += " AND v.building like '%"+filter.getVenueName()+"%'";
			}			
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day, e.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month, e.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year, e.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getEventTimeStr() != null){
				if(Util.extractDateElement(filter.getEventTimeStr(),"HOUR") > 0){
					sql += " AND DATEPART(hour, e.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"HOUR")+" ";
				}
				if(Util.extractDateElement(filter.getEventTimeStr(),"MINUTE") > 0){
					sql += " AND DATEPART(minute, e.event_time) = "+Util.extractDateElement(filter.getEventTimeStr(),"MINUTE")+" ";
				}
			}
			if(filter.getUpdatedBy()!=null && !filter.getUpdatedBy().isEmpty()){
				sql += " AND ce.updated_by like '%"+filter.getUpdatedBy()+"%'";
			}
			if(filter.getUpdatedDate() != null){
				if(Util.extractDateElement(filter.getUpdatedDate(),"DAY") > 0){
					sql += " AND DATEPART(day, ce.updated_date) = "+Util.extractDateElement(filter.getUpdatedDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"MONTH") > 0){
					sql += " AND DATEPART(month, ce.updated_date) = "+Util.extractDateElement(filter.getUpdatedDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"YEAR") > 0){
					sql += " AND DATEPART(year, ce.updated_date) = "+Util.extractDateElement(filter.getUpdatedDate(),"YEAR")+" ";
				}
			}
			
			sql += " order by e.event_id";
			contestSession = ContestEventsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestEventsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			query.addScalar("contestId", Hibernate.INTEGER);
			query.addScalar("eventId", Hibernate.INTEGER);
			query.addScalar("eventName", Hibernate.STRING);
			query.addScalar("eventDate", Hibernate.DATE);
			query.addScalar("eventTime", Hibernate.TIME);
			query.addScalar("venueId", Hibernate.INTEGER);						
			query.addScalar("building", Hibernate.STRING);			
			query.addScalar("updatedBy", Hibernate.STRING);
			query.addScalar("updatedDateTime", Hibernate.TIMESTAMP);
			
			contestEventList = query.setResultTransformer(Transformers.aliasToBean(ContestEvents.class)).list();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return contestEventList;
	}
	
	@Override
	public Integer getContestEventCount(Integer contestId) {
		Integer count =0;		
		try {
			String sql = "select count(id) from contest_events with(nolock) where contest_id="+contestId;
			contestSession = ContestEventsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestEventsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			count = (Integer) query.uniqueResult();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return count;	
	}

	public void deleteByContestId(Integer contestId) {
		bulkUpdate("DELETE FROM ContestEvents WHERE contestId=?", new Object[]{contestId});
	}
	
	public void deleteByContestIdAndEventId(Integer contestId, String eventId) {
		bulkUpdate("DELETE FROM ContestEvents WHERE contestId=? AND eventId in("+eventId+") ", new Object[]{contestId});
	}
}
