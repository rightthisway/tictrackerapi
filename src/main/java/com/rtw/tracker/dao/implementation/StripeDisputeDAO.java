package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.StripeDispute;

public class StripeDisputeDAO extends HibernateDAO<Integer, StripeDispute> implements com.rtw.tracker.dao.services.StripeDisputeDAO{

	@Override
	public List<StripeDispute> getAllDisputeList() {
		return find("FROM StripeDispute");
	}

}
