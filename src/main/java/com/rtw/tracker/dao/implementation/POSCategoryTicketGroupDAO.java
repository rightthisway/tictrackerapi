package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.POSCategoryTicketGroup;

public class POSCategoryTicketGroupDAO extends HibernateDAO<Integer, POSCategoryTicketGroup> implements com.rtw.tracker.dao.services.POSCategoryTicketGroupDAO{
	
	public POSCategoryTicketGroup getPOSCategoryTicketGroupByEventId(Integer eventId) throws Exception{
		return findSingle("FROM POSCategoryTicketGroup WHERE eventId=? ", new Object[]{eventId});
	}

	public POSCategoryTicketGroup getPOSCategoryTicketGroupByCategoryTicketGroupId(Integer catTicketGroupId,String productType) throws Exception{
		return findSingle("FROM POSCategoryTicketGroup WHERE categoryTicketGroupId=? AND productType=? ", new Object[]{catTicketGroupId,productType});
	}
}
