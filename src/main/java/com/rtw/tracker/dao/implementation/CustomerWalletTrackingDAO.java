package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.CustomerWalletTracking;


public class CustomerWalletTrackingDAO extends HibernateDAO<Integer, CustomerWalletTracking> implements com.rtw.tracker.dao.services.CustomerWalletTrackingDAO{

	public List<CustomerWalletTracking> getCustomerWalletTrackingByCustomerId(Integer customerId){
		return find("FROM CustomerWalletTracking WHERE customerId=?",new Object[]{customerId});
	}
}
