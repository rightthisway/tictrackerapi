package com.rtw.tracker.dao.implementation;

import java.util.ArrayList;
import java.util.List;

import com.rtw.tracker.datas.FantasyParentCategory;

public class FantasyParentCategoryDAO extends HibernateDAO<Integer, FantasyParentCategory> implements com.rtw.tracker.dao.services.FantasyParentCategoryDAO{

	public List<FantasyParentCategory> getAll(){
		return find("FROM FantasyParentCategory");
	}
	
	public FantasyParentCategory getParentCategoryByName(String name){
		return findSingle("FROM FantasyParentCategory where name = ?", new Object[]{name});
	}
	
	public FantasyParentCategory getParentCategoryById(Integer id){
		return findSingle("FROM FantasyParentCategory where id = ?", new Object[]{id});
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getDistinctParentCategoryNames(){
		return getHibernateTemplate().find("SELECT distinct name FROM FantasyParentCategory");
	}
	
	public List<FantasyParentCategory> getParentCategoriesByName(String name){
		String hql="FROM FantasyParentCategory WHERE name like ? order by name";
		List<Object> parameters = new ArrayList<Object>();
		parameters.add("%" + name + "%");
		return find(hql, parameters.toArray());
	}
}
