package com.rtw.tracker.dao.implementation;

import com.rtw.tracker.datas.POSTicketGroup;

public class POSTicketGroupDAO extends HibernateDAO<Integer, POSTicketGroup> implements com.rtw.tracker.dao.services.POSTicketGroupDAO{

	public POSTicketGroup getPOSTicketGroupByTicketGroupId(Integer ticketGroupId,String productType) throws Exception{
		return findSingle("FROM POSTicketGroup WHERE ticketGroupId=? AND productType=?", new Object[]{ticketGroupId,productType});
	}
	
	public POSTicketGroup getPOSTicketGroupByTicketGroupIdWithPdt(Integer ticketGroupId, String productType) throws Exception{
		return findSingle("FROM POSTicketGroup WHERE ticketGroupId=? and productType=?", new Object[]{ticketGroupId, productType});
	}
	
	public POSTicketGroup getPOSTicketGroupByEventId(Integer eventId) throws Exception{
		return findSingle("FROM POSTicketGroup WHERE eventId=? ", new Object[]{eventId});
	}
}
