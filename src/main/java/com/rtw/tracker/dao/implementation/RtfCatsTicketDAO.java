package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.rtw.tmat.data.RewardthefanCategoryTicket;
import com.rtw.tracker.datas.RtfCatsTicket;
import com.rtw.tracker.datas.TicketStatus;

public class RtfCatsTicketDAO extends HibernateDAO<Integer, RtfCatsTicket> implements com.rtw.tracker.dao.services.RtfCatsTicketDAO{

	//private String zonesApiDbUrl = "ZonesApiPlatformV2.dbo";
	
	private static Session contestSession = null;
	
	public static Session getContestSession() {
		return RtfCatsTicketDAO.contestSession;
	}
	public static void setContestSession(Session contestSession) {
		RtfCatsTicketDAO.contestSession = contestSession;
	}
	
	public void deleteAllActiveRtfCatsListingsByEventId(Integer eventId) {
		//bulkUpdate("DELETE FROM RtfCatsTicket WHERE eventId=? and status=?", new Object[]{eventId,TicketStatus.ACTIVE});
		 Integer result=0;
		  Session session=null;
			try{
//				session = getSessionFactory().openSession();
				session = getSession();
				String sql = "DELETE FROM rtf_cat_tickets where event_id="+eventId+" and status='ACTIVE' ";
							  
					SQLQuery sqlQuery = session.createSQLQuery(sql);
					result += sqlQuery.executeUpdate();
				
			}catch (Exception e) {
				e.printStackTrace();
				//throw e;
			}finally{
				session.close();
			}
	}
	public Integer updateRtfCatsTicketByrtfCatsId(RewardthefanCategoryTicket rewardCatTicket)  throws Exception {
		String sql =" UPDATE rtf_cat_tickets  set price="+rewardCatTicket.getRtfPrice() +",tmat_price="+rewardCatTicket.getTmatPrice()+",last_updated=getdate()"+
			    " WHERE status='ACTIVE' AND id = "+rewardCatTicket.getRtfCatsId();
				//" AND (le.tn_broker_id > 0 OR le.vivid_broker_id > 0 OR le.scorebig_broker_id > 0)) ";
	     
		int result = 0;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			result = sqlQuery.executeUpdate();
			
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return result;
	}
	
	public Integer deleteRtfCatsTicketByrtfCats(List<RewardthefanCategoryTicket> catTixList) throws Exception{
		
		  Integer result=0;
		  Session session=null;
			try{
//				session = getSessionFactory().openSession();
				session = getSession();
				
				for (RewardthefanCategoryTicket rewardthefanCategoryTicket : catTixList) {
					String sql = "DELETE  FROM rtf_cat_tickets  where id="+rewardthefanCategoryTicket.getRtfCatsId()+" and status='ACTIVE' ";
							  
					SQLQuery sqlQuery = session.createSQLQuery(sql);
					result += sqlQuery.executeUpdate();
				}
				
				return result;
			}catch (Exception e) {
				e.printStackTrace();
				throw e;
			}finally{
				session.close();
			}
		}
	
}
