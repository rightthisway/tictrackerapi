package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.datas.PromotionalEarnLifeOffer;
import com.rtw.tracker.utils.GridHeaderFilters;

public class PromotionalEarnLifeOfferDAO extends HibernateDAO<Integer, PromotionalEarnLifeOffer> implements com.rtw.tracker.dao.services.PromotionalEarnLifeOfferDAO{

	
	private static Session earnLivesSession;
	
	
	public static Session getContestSession() {
		return PromotionalEarnLifeOfferDAO.earnLivesSession;
	}

	public static void setContestSession(Session earnLivesSession) {
		PromotionalEarnLifeOfferDAO.earnLivesSession = earnLivesSession;
	}
	
	public List<PromotionalEarnLifeOffer> getAllContestPromocodes(Integer id, GridHeaderFilters filter,String type,String status){
		List<PromotionalEarnLifeOffer> contestPromocodes = null;		
		try {
			String sql = "select id as id,promo_code as promocode,start_date as startDate,end_date as endDate,max_life_per_customer as maxLifePerCustomer," +
					"maximum_accumulate_threshold as maxAccumulateThreshold,cur_accumulated_count as curAccumulatedCount,status as status,created_date as createdDate," +
					"created_by as createdBy,modified_time as updatedDate,modified_by as updatedBy from rtf_promotional_earn_life_offers where 1=1 ";
			
			if(id != null  && id > 0){
				sql += " AND c.id="+id;
			}
			if(filter.getPromoCode() != null  && !filter.getPromoCode().isEmpty()){
				sql += " AND promo_code like '%"+filter.getPromoCode()+"%'";
			}
			if(filter.getStatus() != null  && !filter.getStatus().isEmpty()){
				sql += " AND status like '%"+filter.getStatus()+"%'";
			}
			if(filter.getCreatedBy() != null  && !filter.getCreatedBy().isEmpty()){
				sql += " AND created_by like '%"+filter.getCreatedBy()+"%'";
			}
			if(filter.getLastUpdatedBy() != null  && !filter.getLastUpdatedBy().isEmpty()){
				sql += " AND modified_by like '%"+filter.getLastUpdatedBy()+"%'";
			}
			if(filter.getTicketWinnerCount() != null ){
				sql += " AND max_life_per_customer = "+filter.getTicketWinnerCount();
			}
			if(filter.getTicketWinnerThreshhold() != null ){
				sql += " AND maximum_accumulate_threshold = "+filter.getTicketWinnerThreshhold();
			}
			if(filter.getPriceUpdateCount() != null ){
				sql += " AND cur_accumulated_count = "+filter.getPriceUpdateCount();
			}
			
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day, created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month, created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year, created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getUpdatedDate()!=null ){
				if(Util.extractDateElement(filter.getUpdatedDate(),"DAY") > 0){
					sql += " AND DATEPART(day, modified_time) = "+Util.extractDateElement(filter.getUpdatedDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"MONTH") > 0){
					sql += " AND DATEPART(month, modified_time) = "+Util.extractDateElement(filter.getUpdatedDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"YEAR") > 0){
					sql += " AND DATEPART(year, modified_time) = "+Util.extractDateElement(filter.getUpdatedDate(),"YEAR")+" ";
				}
			}
			if(filter.getStartDate()!=null ){
				if(Util.extractDateElement(filter.getStartDate(),"DAY") > 0){
					sql += " AND DATEPART(day, start_date) = "+Util.extractDateElement(filter.getStartDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getStartDate(),"MONTH") > 0){
					sql += " AND DATEPART(month, start_date) = "+Util.extractDateElement(filter.getStartDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getStartDate(),"YEAR") > 0){
					sql += " AND DATEPART(year, start_date) = "+Util.extractDateElement(filter.getStartDate(),"YEAR")+" ";
				}
			}
			if(filter.getEndDate()!=null ){
				if(Util.extractDateElement(filter.getEndDate(),"DAY") > 0){
					sql += " AND DATEPART(day, end_date) = "+Util.extractDateElement(filter.getEndDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEndDate(),"MONTH") > 0){
					sql += " AND DATEPART(month, end_date) = "+Util.extractDateElement(filter.getEndDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEndDate(),"YEAR") > 0){
					sql += " AND DATEPART(year, end_date) = "+Util.extractDateElement(filter.getEndDate(),"YEAR")+" ";
				}
			}
			sql += " order by id desc";
			earnLivesSession = PromotionalEarnLifeOfferDAO.getContestSession();
			if(earnLivesSession==null || !earnLivesSession.isOpen() || !earnLivesSession.isConnected()){
				earnLivesSession = getSession();
				PromotionalEarnLifeOfferDAO.setContestSession(earnLivesSession);
			}
			SQLQuery query = earnLivesSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("promoCode", Hibernate.STRING);
			query.addScalar("startDate", Hibernate.TIMESTAMP);
			query.addScalar("endDate", Hibernate.TIMESTAMP);
			query.addScalar("maxLifePerCustomer", Hibernate.INTEGER);
			query.addScalar("maxAccumulateThreshold", Hibernate.INTEGER);						
			query.addScalar("curAccumulatedCount", Hibernate.INTEGER);
			query.addScalar("status", Hibernate.STRING);			
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);
			query.addScalar("updatedBy", Hibernate.STRING);	
			contestPromocodes = query.setResultTransformer(Transformers.aliasToBean(PromotionalEarnLifeOffer.class)).list();
		}catch (Exception e) {
			if(earnLivesSession != null && earnLivesSession.isOpen()){
				earnLivesSession.close();
			}
			e.printStackTrace();
		}
		return contestPromocodes;
	}

	@Override
	public List<PromotionalEarnLifeOffer> getPromotionalEarLifeByCode(String promocode) {
		return find("FROM PromotionalEarnLifeOffer WHERE status=? AND promoCode=?",new Object[]{"ACTIVE",promocode});
	}
	
}
