package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.OrderTicketGroupDetails;

public class OrderTicketGroupDetailsDAO extends HibernateDAO<Integer, OrderTicketGroupDetails> implements com.rtw.tracker.dao.services.OrderTicketGroupDetailsDAO{

	@Override
	public List<OrderTicketGroupDetails> getOrderTicketGroupDetailsByOrderId(Integer orderId) {
		return find("FROM OrderTicketGroupDetails where orderId=?",new Object[]{orderId});
	}
}
