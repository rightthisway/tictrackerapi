package com.rtw.tracker.dao.implementation;

import com.rtw.tracker.datas.AffiliateSetting;

public class AffiliateSettingDAO extends HibernateDAO<Integer, AffiliateSetting> implements  com.rtw.tracker.dao.services.AffiliateSettingDAO{

	public AffiliateSetting getSettingByUser(Integer userId){
		return findSingle("FROM AffiliateSetting Where userId = ?", new Object[]{userId});
	}
}
