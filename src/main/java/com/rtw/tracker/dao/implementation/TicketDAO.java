package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.Ticket;
import com.rtw.tracker.datas.TicketStatus;


public class TicketDAO extends HibernateDAO<Integer, Ticket> implements com.rtw.tracker.dao.services.TicketDAO {


	@Override
	public List<Ticket> getAvailableTicketsByTicketGroupId(Integer ticketGroupId)throws Exception {
		return find("FROM Ticket WHERE ticketGroupId=? AND ticketStatus =? AND (invoiceId=0 OR invoiceId IS NULL) order by seatOrder",new Object[]{ticketGroupId,TicketStatus.ACTIVE});
	}
	
	public List<Ticket> getMappedTicketsByInvoiceId(Integer invoiceId) throws Exception{
		return find("FROM Ticket WHERE invoiceId=? AND ticketStatus =? order by ticketGroupId,seatOrder", new Object[]{invoiceId,TicketStatus.SOLD});
	}

	public List<Ticket> getMappedTicketsByInvoiceIdandTicketGroupId(Integer invoiceId,Integer ticketGroupId) throws Exception{
		return find("FROM Ticket WHERE invoiceId=? AND ticketGroupId=? AND ticketStatus =? order by ticketGroupId,seatOrder", new Object[]{invoiceId,ticketGroupId,TicketStatus.SOLD});
	}
	
	@Override
	public List<Ticket> getTIcketsByPOId(Integer poId) throws Exception {
		return find("FROM Ticket WHERE poId=?",new Object[]{poId});
	}

	@Override
	public List<Ticket> getTicketsByTicketGroupIds(List<Integer> groupIds)throws Exception {
		return getHibernateTemplate().findByNamedParam("FROM Ticket WHERE ticketGroupId in(:ids)", "ids", groupIds);
	}
	
	public List<Ticket> getTicketsByTicketIds(List<Integer> ids)throws Exception {
		return getHibernateTemplate().findByNamedParam("FROM Ticket WHERE id in(:ids)", "ids", ids);
	}

	public List<Ticket> getTicketsByTicketIdsAndStatus(List<Integer> ids)throws Exception {
		//return find("FROM Ticket WHERE id in ("+ ids +") AND ticketStatus = ? AND (invoiceId=0 OR invoiceId IS NULL) order by seatOrder", new Object[]{TicketStatus.ONHOLD});
		return getHibernateTemplate().findByNamedParam("FROM Ticket WHERE id in(:ids) AND ticketStatus = :status AND (invoiceId=0 OR invoiceId IS NULL) order by seatOrder", 
				new String[]{"ids","status"}, 
				new Object[]{ids,TicketStatus.ONHOLD});
	}
	
	@Override
	public List<Ticket> getAllTicketsByTicketGroupId(Integer ticketGroupId)throws Exception {
		return find("FROM Ticket WHERE ticketGroupId=? order by seatOrder",new Object[]{ticketGroupId});
	}

	@Override
	public List<Ticket> getMappedTicketByPOId(Integer poId) throws Exception {
		return find("FROM Ticket WHERE poId=? AND invoiceId>0 AND ticketStatus =?",new Object[]{poId,TicketStatus.SOLD});
	}

	@Override
	public void getAllTicketsToDeleteByTicketGroupId(Integer ticketGroupId)throws Exception {
		bulkUpdate("DELETE FROM Ticket WHERE ticketGroupId=?" , new Object[]{ticketGroupId});
	}
}
