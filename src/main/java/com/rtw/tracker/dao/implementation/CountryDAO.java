package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.Country;
import com.rtw.tracker.datas.State;

public class CountryDAO extends HibernateDAO<Integer, Country> implements
		com.rtw.tracker.dao.services.CountryDAO {
	/**
	 * method to get List of Countries
	 * 
	 * @return List of Countries
	 */
	public List<Country> getAllCountry() {

		return find("from Country  ", new Object[] {});

	}

	/**
	 * method to get Country by id
	 * 
	 * @param id
	 *            , company id
	 * @return
	 */
	public Country geCountryById(Integer id) {
		return findSingle("from Country where id=? ", new Object[] { id });
	}

	/**
	 * method to get id of US.
	 * 
	 * @return Id for US
	 */
	public Integer getCountryByName(String name) {

		Country country = findSingle("from Country  where name = ?",
				new Object[] { name });
		return country != null ? country.getId() : 0;

	}

	public List<Country> GetUSCANADA() {

		return find("from Country WHERE id in(217,38)");

	}

}
