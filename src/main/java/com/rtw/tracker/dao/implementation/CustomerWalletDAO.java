package com.rtw.tracker.dao.implementation;

import com.rtw.tracker.datas.CustomerWallet;

public class CustomerWalletDAO extends HibernateDAO<Integer, CustomerWallet> implements com.rtw.tracker.dao.services.CustomerWalletDAO{

	@Override
	public CustomerWallet getCustomerWalletByCustomerId(Integer customerId) {
		return findSingle("FROM CustomerWallet WHERE customerId=?",new Object[]{customerId});
	}

}
