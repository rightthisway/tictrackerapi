package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.RTFPromotionalOffer;


/**
 * class having db related methods for RTFPromotionalOffer
 * @author Ulaganathan
 *
 */
public class RTFPromotionalOfferDAO extends HibernateDAO<Integer, RTFPromotionalOffer> implements com.rtw.tracker.dao.services.RTFPromotionalOfferDAO{

	@Override
	public List<RTFPromotionalOffer> getAllActiveOffers() {
		return find("FROM RTFPromotionalOffer where status = ?",new Object[]{"ENABLED"});
	}
	
	/*public AffiliateCashReward getAffiliateByUserId(Integer userId){
		return findSingle("from AffiliateCashReward where userId=? ", new Object[]{userId});
	}*/
	
	/*public CustomerLoyalty getCustomerLoyaltyByCustomerId(Integer customerId){
		return findSingle("from CustomerLoyalty where customerId=? ", new Object[]{customerId});
	}
	
	public void updateCustomerLoyalty(Double pointsRemaining, Double latestSpendPoints, Double totalSpendPoints, Integer customerId) {
		
		try {
			String SQL_UPDATE_CUST_LOYALTY = "update CustomerLoyalty set activePointsAsDouble= ?, latestSpentPointsAsDouble=?, totalSpentPointsAsDouble = ? where customerId = ?";
			bulkUpdate(SQL_UPDATE_CUST_LOYALTY, new Object[]{pointsRemaining, latestSpendPoints, totalSpendPoints, customerId});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	public List<CustomerLoyalty> getAllCustomerLoyaltyWhichHasMorethanZeroActivePoints(){
		return find("from CustomerLoyalty where activePointsAsDouble > 0 order by  customerId");
	}
	
	public void updateCustomerLoyaltyByCustomer(Double earnPoints,  Integer customerId) {
		
		try {
			String SQL_UPDATE_CUST_LOYALTY = "update CustomerLoyalty set totalEarnedPointsAsDouble=totalEarnedPointsAsDouble + ?," +
					"latestEarnedPointsAsDouble = ?, activePointsAsDouble= activePointsAsDouble + ? , pendingPointsAsDouble = pendingPointsAsDouble - ?, lastUpdate=? where customerId = ?";
			bulkUpdate(SQL_UPDATE_CUST_LOYALTY, new Object[]{earnPoints, earnPoints, earnPoints,earnPoints,new Date(), customerId});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

}
