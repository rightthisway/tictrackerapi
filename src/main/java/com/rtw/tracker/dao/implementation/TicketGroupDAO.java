package com.rtw.tracker.dao.implementation;


import java.util.List;

import com.rtw.tracker.datas.TicketGroup;

public class TicketGroupDAO extends HibernateDAO<Integer, TicketGroup> implements com.rtw.tracker.dao.services.TicketGroupDAO{

	/*public List<TicketGroup> getAllTicketGroupsByInvoiceId(Integer invoiceId) throws Exception {
		return find("FROM TicketGroup WHERE invoiceId=?", new Object[]{invoiceId});
	}*/
	public TicketGroup getAllTicketGroupById(Integer id){
		return findSingle("FROM TicketGroup WHERE (invoiceId=0 OR invoiceId is NULL) AND status='ACTIVE' AND id = ?" ,new Object[]{id});
	}
	
	public TicketGroup getTicketGroupByIdAndBrokerId(Integer id, Integer brokerId){
		return findSingle("FROM TicketGroup WHERE (invoiceId=0 OR invoiceId is NULL) AND status='ACTIVE' AND id = ? AND brokerId= ?" ,new Object[]{id, brokerId});
	}
	
	@Override
	public List<TicketGroup> getAllTicketGroupsByEventId(Integer eventId) throws Exception {
		return find("FROM TicketGroup WHERE (invoiceId=0 OR invoiceId is NULL) AND status='ACTIVE' AND eventId=?",new Object[]{eventId});
	}
	
	@Override
	public List<TicketGroup> getAllTicketGroupsByEventAndBroker(Integer eventId,Integer brokerId) throws Exception {
		return find("FROM TicketGroup WHERE (invoiceId=0 OR invoiceId is NULL) AND status='ACTIVE' AND eventId=? AND brokerId=?",new Object[]{eventId,brokerId});
	}

	@Override
	public List<TicketGroup> getAllTicketGroupsByPOId(Integer poId)throws Exception {
		return find("FROM TicketGroup WHERE (status='ACTIVE' OR status is NULL) AND purchaseOrderId=?", new Object[]{poId});
	}
	
	public void updateBroadCastByEventId(Integer eventId, Boolean broadCast){
		bulkUpdate("UPDATE TicketGroup set broadcast = ? WHERE eventId=? and status='ACTIVE' ", new Object[]{broadCast,eventId});
	}
}
