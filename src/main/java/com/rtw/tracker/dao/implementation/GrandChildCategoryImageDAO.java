package com.rtw.tracker.dao.implementation;

import com.rtw.tracker.datas.GrandChildCategoryImage;

public class GrandChildCategoryImageDAO extends HibernateDAO<Integer, GrandChildCategoryImage> implements com.rtw.tracker.dao.services.GrandChildCategoryImageDAO {
	
	public GrandChildCategoryImage getGrandChildCategoryImageByGrandChildId(Integer grandChildId) throws Exception { 
		return findSingle("From GrandChildCategoryImage where grandChildCategory.id=?", new Object[]{grandChildId});		
	}
}
