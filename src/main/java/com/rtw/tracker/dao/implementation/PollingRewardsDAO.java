package com.rtw.tracker.dao.implementation;

import org.hibernate.Session;

import com.rtw.tracker.datas.PollingRewards;


public class PollingRewardsDAO extends HibernateDAO<Integer, PollingRewards>
		implements com.rtw.tracker.dao.services.PollingRewardsDAO {

	private static Session pollingSession = null;

	public static Session getPollingSession() {
		return PollingRewardsDAO.pollingSession;
	}

	public static void setPollingSession(Session pollingSession) {
		PollingRewardsDAO.pollingSession = pollingSession;
	}

	@Override
	public PollingRewards getPollingRewardsForContestId(Integer id) {
		PollingRewards pollingRewards = null;
		try {
						
			pollingSession = PollingRewardsDAO.getPollingSession();
			if (pollingSession == null || !pollingSession.isOpen() || !pollingSession.isConnected()) {
				pollingSession = getSession();				
				PollingRewardsDAO.setPollingSession(pollingSession);
			}
			String hql = " from PollingRewards p where p.contestId = :contestId";
			pollingRewards = (PollingRewards) pollingSession.createQuery(hql)
                    .setInteger("contestId", id)
                    .uniqueResult();
			// pollingRewards = (PollingRewards) pollingSession.get(PollingRewards.class, id);
		} catch (Exception e) {
			if (pollingSession != null && pollingSession.isOpen()) {
				pollingSession.close();
			}
			e.printStackTrace();
		}
		finally{
			pollingSession.close();
		}
		return pollingRewards;

	}
}