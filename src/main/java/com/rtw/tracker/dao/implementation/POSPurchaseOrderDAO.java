package com.rtw.tracker.dao.implementation;

import com.rtw.tracker.datas.POSPurchaseOrder;

public class POSPurchaseOrderDAO extends HibernateDAO<Integer, POSPurchaseOrder> implements com.rtw.tracker.dao.services.POSPurchaseOrderDAO{
	
	public POSPurchaseOrder getPOSPurchaseOrderDAOByPurchaseOrderId(Integer purchaseOrderId)throws Exception{
		return findSingle("FROM POSPurchaseOrder WHERE purchaseOrderId=? ", new Object[]{purchaseOrderId});
	}
	
	public POSPurchaseOrder getPOSPurchaseOrderDAOByPurchaseOrderId(Integer purchaseOrderId,String productType)throws Exception{
		return findSingle("FROM POSPurchaseOrder WHERE purchaseOrderId=? AND productType=?", new Object[]{purchaseOrderId, productType});
	}
}
