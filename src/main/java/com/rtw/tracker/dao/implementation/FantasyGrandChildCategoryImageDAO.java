package com.rtw.tracker.dao.implementation;

import com.rtw.tracker.datas.FantasyGrandChildCategoryImage;

public class FantasyGrandChildCategoryImageDAO extends HibernateDAO<Integer, FantasyGrandChildCategoryImage> implements com.rtw.tracker.dao.services.FantasyGrandChildCategoryImageDAO{

	@Override
	public FantasyGrandChildCategoryImage getFantasyGrandChildCategoryImageByGrandChildId(Integer fantasyGrandChildId) {
		return findSingle("From FantasyGrandChildCategoryImage where grandChildCategory.id=?", new Object[]{fantasyGrandChildId});
	}

}
