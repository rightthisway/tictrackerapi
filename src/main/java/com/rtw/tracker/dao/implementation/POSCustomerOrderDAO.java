package com.rtw.tracker.dao.implementation;

import com.rtw.tracker.datas.POSCustomerOrder;

public class POSCustomerOrderDAO extends HibernateDAO<Integer, POSCustomerOrder>  implements com.rtw.tracker.dao.services.POSCustomerOrderDAO{

	public POSCustomerOrder getPOSCustomerOrderByTicketRequestId(Integer ticketRequestId){
		return findSingle("FROM POSCustomerOrder WHERE ticketRequestId=?", new Object[]{ticketRequestId});
	}
}
