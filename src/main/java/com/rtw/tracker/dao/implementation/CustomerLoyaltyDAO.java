package com.rtw.tracker.dao.implementation;

import java.util.Date;
import java.util.List;

import com.rtw.tracker.datas.CustomerLoyalty;

/**
 * class having db related methods for CustomerFavouriteEvent
 * @author Ulaganathan
 *
 */
public class CustomerLoyaltyDAO extends HibernateDAO<Integer, CustomerLoyalty> implements com.rtw.tracker.dao.services.CustomerLoyaltyDAO{
	
	/*public List<CustomerLoyalty> getAllActiveFavouriteEventsByCustomerId(Integer customerId) {
		return find("from CustomerLoyalty where customerId=? and status='ACTIVE' ", new Object[]{customerId});
				
	}

	
	public void deleteFavouriteEventByEventIdAndCustomerId(Integer eventId,Integer customerId) {
		bulkUpdate("UPDATE CustomerLoyalty set status='DELETE' where customerId=? and eventId=?",new Object[]{customerId,eventId});
	}
	*/
	public CustomerLoyalty getCustomerLoyaltyByCustomerId(Integer customerId){
		return findSingle("from CustomerLoyalty where customerId=? ", new Object[]{customerId});
	}
	
	public void updateCustomerLoyalty(Integer pointsRemaining, Integer latestSpendPoints, Integer totalSpendPoints, Integer customerId) {
		
		try {
			String SQL_UPDATE_CUST_LOYALTY = "update CustomerLoyalty set activePoints= ?, latestSpentPoints=?, totalSpentPoints = ? where customerId = ?";
			bulkUpdate(SQL_UPDATE_CUST_LOYALTY, new Object[]{pointsRemaining, latestSpendPoints, totalSpendPoints, customerId});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	public List<CustomerLoyalty> getAllCustomerLoyaltyWhichHasMorethanZeroActivePoints(){
		return find("from CustomerLoyalty where activePoints > 0 order by  customerId");
	}
	
	public void updateCustomerLoyaltyByCustomer(Integer earnPoints,  Integer customerId) {
		
		try {
			String SQL_UPDATE_CUST_LOYALTY = "update CustomerLoyalty set totalEarnedPoints=totalEarnedPoints + ?," +
					"latestEarnedPoints = ?, activePoints= activePoints + ? , pendingPoints = pendingPoints - ?, lastUpdate=? where customerId = ?";
			bulkUpdate(SQL_UPDATE_CUST_LOYALTY, new Object[]{earnPoints, earnPoints, earnPoints,earnPoints,new Date(), customerId});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
