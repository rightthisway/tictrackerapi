package com.rtw.tracker.dao.implementation;

import java.util.ArrayList;
import java.util.Collection;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.datas.AffiliateCashReward;
import com.rtw.tracker.utils.GridHeaderFilters;

public class AffiliateCashRewardDAO extends HibernateDAO<Integer, AffiliateCashReward> implements com.rtw.tracker.dao.services.AffiliateCashRewardDAO{
	
	
	public AffiliateCashReward getAffiliateByUserId(Integer userId){
		return findSingle("from AffiliateCashReward where user.id=? ", new Object[]{userId});
	}

	@SuppressWarnings("unchecked")
	public Collection<AffiliateCashReward> getAllAffiliates(GridHeaderFilters filter){
		Collection<AffiliateCashReward> rewardList = new ArrayList<AffiliateCashReward>();
		try{
			String sqlQuery = "From AffiliateCashReward where 1=1";
			
			if(filter.getUserId() != null){
				sqlQuery += " AND user.id = "+filter.getUserId()+" ";
			}
			if(filter.getCustomerName() != null){
				sqlQuery += " AND user.firstName like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getLastName() != null){
				sqlQuery += " AND user.lastName like '%"+filter.getLastName()+"%' ";	
			}
			if(filter.getActiveCash() != null){
				sqlQuery += " AND activeCash = "+filter.getActiveCash()+" ";
			}
			if(filter.getPendingCash() != null){
				sqlQuery += " AND pendingCash = "+filter.getPendingCash()+" ";
			}
			if(filter.getLastCreditedCash() != null){
				sqlQuery += " AND lastCreditedCash = "+filter.getLastCreditedCash()+" ";	
			}
			if(filter.getLastDebitedCash() != null){
				sqlQuery += " AND lastDebitedCash = "+filter.getLastDebitedCash()+" ";
			}
			if(filter.getTotalCreditedCash() != null){
				sqlQuery += " AND totalCreditedCash = "+filter.getTotalCreditedCash()+" ";
			}
			if(filter.getTotalDebitedCash() != null){
				sqlQuery += " AND totalDebitedCash = "+filter.getTotalDebitedCash()+" ";	
			}
			if(filter.getLastUpdatedDateStr() != null){
				String dateTimeStr = filter.getLastUpdatedDateStr();
				if(dateTimeStr.contains(" ")){
					String dateTimeArr[] = dateTimeStr.split(" ");
					if(Util.extractDateElement(dateTimeArr[0],"DAY") > 0){
						sqlQuery += " AND DATEPART(day,lastUpdate) = "+Util.extractDateElement(dateTimeArr[0],"DAY")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,lastUpdate) = "+Util.extractDateElement(dateTimeArr[0],"MONTH")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,lastUpdate) = "+Util.extractDateElement(dateTimeArr[0],"YEAR")+" ";
					}
					if(dateTimeArr.length>1){
						String dateTime = dateTimeArr[1];
						if(dateTimeArr.length > 2){
							if(dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()){
								dateTime += " "+dateTimeArr[2];
							}
						}
						if(Util.extractDateElement(dateTime,"HOUR") > 0){
							sqlQuery += " AND DATEPART(hour,lastUpdate) = "+Util.extractDateElement(dateTime,"HOUR")+" ";
						}
						if(Util.extractDateElement(dateTime,"MINUTE") > 0){
							sqlQuery += " AND DATEPART(minute,lastUpdate) = "+Util.extractDateElement(dateTime,"MINUTE")+" ";
						}
					}
				}else{
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
						sqlQuery += " AND DATEPART(day,lastUpdate) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,lastUpdate) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,lastUpdate) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
					}
				}
			}
			
			return find(sqlQuery);
		}catch(Exception e){
			e.printStackTrace();
		}
		return rewardList;
	}
	
	public Integer getAllAffiliatesCount(GridHeaderFilters filter){
		Integer count = 0;
		try{
			String sqlQuery = "select count(*) as cnt from affiliate_cash_reward a with(nolock) "+
				" left join tracker_users tu with(nolock) on tu.id = a.user_id where 1 = 1";

			if(filter.getUserId() != null){
				sqlQuery += " AND a.user_id = "+filter.getUserId()+" ";
			}
			if(filter.getCustomerName() != null){
				sqlQuery += " AND tu.firstname like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getLastName() != null){
				sqlQuery += " AND tu.lastname like '%"+filter.getLastName()+"%' ";	
			}
			if(filter.getActiveCash() != null){
				sqlQuery += " AND a.active_cash = "+filter.getActiveCash()+" ";
			}
			if(filter.getPendingCash() != null){
				sqlQuery += " AND a.pending_cash = "+filter.getPendingCash()+" ";
			}
			if(filter.getLastCreditedCash() != null){
				sqlQuery += " AND a.last_credited_cash = "+filter.getLastCreditedCash()+" ";	
			}
			if(filter.getLastDebitedCash() != null){
				sqlQuery += " AND a.latest_debited_cash = "+filter.getLastDebitedCash()+" ";
			}
			if(filter.getTotalCreditedCash() != null){
				sqlQuery += " AND a.total_credited_cash = "+filter.getTotalCreditedCash()+" ";
			}
			if(filter.getTotalDebitedCash() != null){
				sqlQuery += " AND a.total_debited_cash = "+filter.getTotalDebitedCash()+" ";	
			}
			if(filter.getLastUpdatedDateStr() != null){
				String dateTimeStr = filter.getLastUpdatedDateStr();
				if(dateTimeStr.contains(" ")){
					String dateTimeArr[] = dateTimeStr.split(" ");
					if(Util.extractDateElement(dateTimeArr[0],"DAY") > 0){
						sqlQuery += " AND DATEPART(day,a.updated_time) = "+Util.extractDateElement(dateTimeArr[0],"DAY")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,a.updated_time) = "+Util.extractDateElement(dateTimeArr[0],"MONTH")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,a.updated_time) = "+Util.extractDateElement(dateTimeArr[0],"YEAR")+" ";
					}
					if(dateTimeArr.length>1){
						String dateTime = dateTimeArr[1];
						if(dateTimeArr.length > 2){
							if(dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()){
								dateTime += " "+dateTimeArr[2];
							}
						}
						if(Util.extractDateElement(dateTime,"HOUR") > 0){
							sqlQuery += " AND DATEPART(hour,a.updated_time) = "+Util.extractDateElement(dateTime,"HOUR")+" ";
						}
						if(Util.extractDateElement(dateTime,"MINUTE") > 0){
							sqlQuery += " AND DATEPART(minute,a.updated_time) = "+Util.extractDateElement(dateTime,"MINUTE")+" ";
						}
					}
				}else{
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY") > 0){
						sqlQuery += " AND DATEPART(day,a.updated_time) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH") > 0){
						sqlQuery += " AND DATEPART(month,a.updated_time) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR") > 0){
						sqlQuery += " AND DATEPART(year,a.updated_time) = "+Util.extractDateElement(filter.getLastUpdatedDateStr(),"YEAR")+" ";
					}
				}
			}
			
			Session session = getSession();
			SQLQuery query = session.createSQLQuery(sqlQuery);
			count = Integer.parseInt(String.valueOf(query.uniqueResult()));
		}catch(Exception e){
			e.printStackTrace();
		}
		return count;
	}
}
