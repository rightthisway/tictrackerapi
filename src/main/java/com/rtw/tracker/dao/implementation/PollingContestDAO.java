package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.datas.PollingContest;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.GridSortingUtil;

public class PollingContestDAO extends HibernateDAO<Integer, PollingContest>
		implements com.rtw.tracker.dao.services.PollingContestDAO {

	private static Session pollingSession = null;

	public static Session getPollingSession() {
		return PollingContestDAO.pollingSession;
	}

	public static void setPollingSession(Session pollingSession) {
		PollingContestDAO.pollingSession = pollingSession;
	}

	@Override
	public List<PollingContest> getAllPollingContest(Integer id, GridHeaderFilters filter, String status) {
		List<PollingContest> pollingContestList = null;
		try {
			
			StringBuilder sb = new StringBuilder(); 			
			sb.append("select g.id as id, " );
			sb.append(" g.title as title , " );
			sb.append(" g.description as description , " );
			sb.append(" g.start_date as startDate , " );
			sb.append(" g.end_date as endDate , " );
			sb.append(" g.polling_interval as pollingInterval ,  ");
			sb.append(" g.max_que_per_cust as maxQuePerCust , " );			
			sb.append(" g.status as status , " );
			sb.append(" g.created_by as createdBy , " );
			sb.append(" g.created_date as createdDate , " );
			sb.append(" g.updated_by as updatedBy , " );			
			sb.append(" g.updated_date as updatedDate  from polling_contest g where 1=1  " );
			
			if(filter.getId()!=null){
				sb.append(" AND g.id = "+filter.getId());
			}
			if(filter.getContestName()!=null && !filter.getContestName().isEmpty()){
				sb.append(" AND g.title like '%"+filter.getContestName()+"%'" );
			}
			if(filter.getEventDescription()!=null && !filter.getEventDescription().isEmpty()){
				sb.append(" AND g.description like '%"+filter.getEventDescription()+"%'" );
			}
			if(filter.getPollingInterval()!=null && !filter.getPollingInterval().isEmpty()){
				sb.append(" AND g.polling_interval like '%"+filter.getPollingInterval()+"%'" );
			}
			if(filter.getMaxQuePerCust()!=null && !filter.getMaxQuePerCust().isEmpty()){
				sb.append(" AND g.max_que_per_cust like '%"+filter.getMaxQuePerCust()+"%'" );
			}
			if (status != null && !status.isEmpty() && !status.equalsIgnoreCase("ALL")) {
				sb.append(" and g.status = '"+status+"'");
			}
			
			
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sb.append(" AND DATEPART(day, g.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" " );
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sb.append(" AND DATEPART(month, g.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" " );
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sb.append(" AND DATEPART(year, g.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" " );
				}
			}
			if(filter.getUpdatedDate()!=null ){
				if(Util.extractDateElement(filter.getUpdatedDate(),"DAY") > 0){
					sb.append(" AND DATEPART(day, g.updated_date) = "+Util.extractDateElement(filter.getUpdatedDate(),"DAY")+" " );
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"MONTH") > 0){
					sb.append(" AND DATEPART(month, g.updated_date) = "+Util.extractDateElement(filter.getUpdatedDate(),"MONTH")+" " );
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"YEAR") > 0){
					sb.append(" AND DATEPART(year, g.updated_date) = "+Util.extractDateElement(filter.getUpdatedDate(),"YEAR")+" " );
				}
			}
			String sortingSql = GridSortingUtil.getPollingContestSortingSql(filter);
			if(sortingSql!=null && !sortingSql.isEmpty()){
				sb.append(" "+sortingSql);
			}else{
				sb.append(" order by g.id desc");
			}
			pollingSession = PollingContestDAO.getPollingSession();
			if (pollingSession == null || !pollingSession.isOpen() || !pollingSession.isConnected()) {
				pollingSession = getSession();				
				PollingContestDAO.setPollingSession(pollingSession);
			}
			
			SQLQuery query = pollingSession.createSQLQuery(sb.toString());
			query.addScalar("id", Hibernate.INTEGER);			
			query.addScalar("title", Hibernate.STRING);
			query.addScalar("description", Hibernate.STRING);
			query.addScalar("startDate", Hibernate.TIMESTAMP);
			query.addScalar("endDate", Hibernate.TIMESTAMP);
			query.addScalar("pollingInterval", Hibernate.INTEGER);
			query.addScalar("maxQuePerCust", Hibernate.INTEGER);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("updatedBy", Hibernate.STRING);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);

			pollingContestList = query.setResultTransformer(Transformers.aliasToBean(PollingContest.class)).list();
		} catch (Exception e) {
			if (pollingSession != null && pollingSession.isOpen()) {
				pollingSession.close();
			}
			e.printStackTrace();
		}
		return pollingContestList;

	}

	@Override
	public boolean CheckActivePollingContest() {
		List<PollingContest> pollingContestList = null;
		try 
		{	
			pollingContestList =  find("from PollingContest where status like '%ACTIVE%'");;
			if(pollingContestList.size()>0)
			{
				return true;
			}
		} catch (Exception e) {
			if (pollingSession != null && pollingSession.isOpen()) {
				pollingSession.close();
			}
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public Integer getQuestCountForPolling(Integer pollId) {
		int count = 0;
		try 
		{	
			String sql = "select count(*) from polling_question_bank where poll_cat_id in"+
					"(select category_id from polling_contest_category_mapper where contest_id = '"+pollId+"')";			
			pollingSession = PollingContestDAO.getPollingSession();
			if (pollingSession == null || !pollingSession.isOpen() || !pollingSession.isConnected()) {
				pollingSession = getSession();				
				PollingContestDAO.setPollingSession(pollingSession);
			}
			Query query = pollingSession.createSQLQuery(sql);
			count = Integer.valueOf((query.uniqueResult()).toString());
		} catch (Exception e) {
			if (pollingSession != null && pollingSession.isOpen()) {
				pollingSession.close();
			}
			e.printStackTrace();
		}
		return count;
	}
	
	@Override
	public Integer getCategoryCountForPolling(Integer pollId) {
		int count = 0;
		try 
		{	
			String sql = "select count(*) from polling_contest_category_mapper where contest_id = '"+pollId+"'";			
			pollingSession = PollingContestDAO.getPollingSession();
			if (pollingSession == null || !pollingSession.isOpen() || !pollingSession.isConnected()) {
				pollingSession = getSession();				
				PollingContestDAO.setPollingSession(pollingSession);
			}
			Query query = pollingSession.createSQLQuery(sql);
			count = (Integer)query.uniqueResult();
		} catch (Exception e) {
			if (pollingSession != null && pollingSession.isOpen()) {
				pollingSession.close();
			}
			e.printStackTrace();
		}
		return count;
	}
	
	@Override
	public List<PollingContest> getActivePollingContestByCategoryId(Integer categoryId) throws Exception {
		List<PollingContest> pollingContestList = null;
		try {
			String sb =  "select pc.id, pc.status as status,pc.title from "
					+ "polling_contest_category_mapper ccm with(nolock) inner join polling_contest pc with(nolock) on pc.id = ccm.contest_id "
					+ "where ccm.category_id="+categoryId+" and pc.status='ACTIVE'";
			
			pollingSession = PollingContestDAO.getPollingSession();
			if (pollingSession == null || !pollingSession.isOpen() || !pollingSession.isConnected()) {
				pollingSession = getSession();				
				PollingContestDAO.setPollingSession(pollingSession);
			}
			SQLQuery query = pollingSession.createSQLQuery(sb.toString());
			query.addScalar("id", Hibernate.INTEGER);			
			query.addScalar("title", Hibernate.STRING); 
			query.addScalar("status", Hibernate.STRING); 
			pollingContestList = query.setResultTransformer(Transformers.aliasToBean(PollingContest.class)).list();
		} catch (Exception e) {
			if (pollingSession != null && pollingSession.isOpen()) {
				pollingSession.close();
			}
			e.printStackTrace();
		}
		return pollingContestList;
	}
	
	@Override
	public PollingContest getPollingContestById(Integer id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PollingContest savePollingContest(PollingContest pollingContest) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
}