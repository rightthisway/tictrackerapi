package com.rtw.tracker.dao.implementation;

import java.util.Date;
import java.util.List;

import com.rtw.tracker.datas.CrownJewelCategoryTicket;

public class CrownJewelCategoryTicketDAO extends HibernateDAO<Integer, CrownJewelCategoryTicket> implements com.rtw.tracker.dao.services.CrownJewelCategoryTicketDAO{

	public List<CrownJewelCategoryTicket> getAllActiveCrownjewelTicketsByEventId(Integer eventId) throws Exception{
		return find("FROM CrownJewelCategoryTicket where status='ACTIVE' and eventId=?", new Object[] {eventId});
	}
	public List<CrownJewelCategoryTicket> getAllCrownjewelTicketsByEventId(Integer eventId) throws Exception{
		return find("FROM CrownJewelCategoryTicket where eventId=?", new Object[] {eventId});
	}
	
	public void updateCrownJewelCategoryTicketstoDeletedByEventId(Integer eventId) throws Exception{
		 bulkUpdate("UPDATE CrownJewelCategoryTicket set status='DELETED',lastUpdated=? WHERE status='ACTIVE' and eventID=?", new Object[]{new Date(),eventId});
		}
}
