package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Query;

import com.rtw.tracker.datas.CrownJewelTeams;

public class CrownJewelTeamsDAO extends HibernateDAO<Integer, CrownJewelTeams> implements com.rtw.tracker.dao.services.CrownJewelTeamsDAO{

	public List<CrownJewelTeams> getAllActiveTeamsByLeagueId(Integer leagueId) throws Exception {
		return find("FROM CrownJewelTeams where status='ACTIVE' and leagueId=? order by name", new Object[] {leagueId});	
	}
	public List<CrownJewelTeams> getAllActiveTeamsByLeagueIdandCity(Integer leagueId,String city) throws Exception {
		return find("FROM CrownJewelTeams where status='ACTIVE' and leagueId=? and city=? order by name", new Object[] {leagueId,city});	
	}
	public List<CrownJewelTeams> getAllTeamsByLeagueId(Integer leagueId) throws Exception {
		return find("FROM CrownJewelTeams where leagueId=? order by name", new Object[] {leagueId});	
	}
	public List<String> getAllActiveCrownJewelTeamsCityByLeagueId(Integer leagueId)throws Exception {
		
		//String queryStr="select distinct concat(city,' - ',state) from event_details where status=1 and artist_id="+artistId+" ";
		String queryStr="select distinct e.city from crownjewel_teams cjt with(nolock) " +
				" inner join event_details e with(nolock) on e.event_id=cjt.event_id" +
				" where e.status=1 and cjt.status='ACTIVE' and cjt.league_id="+leagueId+" ";
		Query query = getSession().createSQLQuery(queryStr);

		List<String> list = query.list();
		return list;
	}
	@Override
	public List<CrownJewelTeams> getAllDeletedTeamsByLeagueId(Integer leagueId)throws Exception {
		return find("FROM CrownJewelTeams where status='DELETED' and leagueId=? order by name", new Object[] {leagueId});
	}
	@Override
	public List<CrownJewelTeams> getAllActiveTeamsByCategoryTeamId(Integer categoryTeamId) throws Exception {
		return find("FROM CrownJewelTeams where status='ACTIVE' and categoryTeamId=? order by name",new Object[]{categoryTeamId});
	}
}
