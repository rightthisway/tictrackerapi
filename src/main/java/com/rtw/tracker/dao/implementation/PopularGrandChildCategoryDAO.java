package com.rtw.tracker.dao.implementation;

import java.util.Collection;
import java.util.List;

import com.rtw.tracker.datas.PopularArtist;
import com.rtw.tracker.datas.PopularGrandChildCategory;

public class PopularGrandChildCategoryDAO extends HibernateDAO<Integer, PopularGrandChildCategory> implements com.rtw.tracker.dao.services.PopularGrandChildCategoryDAO{


	public Collection<PopularGrandChildCategory> getAllActivePopularGrandChildCategoryByProductId(Integer productId) throws Exception {
		return find("FROM PopularGrandChildCategory WHERE status='ACTIVE' and productId=? ",new Object[]{productId});
	}
	
	public Integer getAllActivePopularGrandChildCategoryCountByProductId(Integer productId) throws Exception {

		List list = find("SELECT count(id) FROM PopularGrandChildCategory WHERE status='ACTIVE' and productId=?",new Object[]{productId});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();		
	}
	
}
