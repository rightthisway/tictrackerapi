package com.rtw.tracker.dao.implementation;

import java.util.List;
import java.util.Properties;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.EnumType;
import org.hibernate.type.Type;

import com.rtw.tmat.utils.Util;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.datas.ContestQuestions;
import com.rtw.tracker.enums.MiniJackpotType;
import com.rtw.tracker.utils.GridHeaderFilters;

public class ContestQuestionsDAO extends HibernateDAO<Integer, ContestQuestions> implements com.rtw.tracker.dao.services.ContestQuestionsDAO{

	private static Session contestSession = null;
	
	public static Session getContestSession() {
		return ContestQuestionsDAO.contestSession;
	}

	public static void setContestSession(Session contestSession) {
		ContestQuestionsDAO.contestSession = contestSession;
	}
	
	@Override
	public List<ContestQuestions> getContestQuestions(GridHeaderFilters filter,Integer questionId,Integer contestId,SharedProperty sharedProperty) {
		List<ContestQuestions> questionList = null;		
		try {
			String sql = "select cq.id as id,cq.contest_id as contestId,cq.question_sl_no as serialNo,cq.question as question,cq.option_a as optionA,cq.option_b as optionB," + 
					" cq.option_c as optionC, cq.answer as answer,cq.created_datetime as createdDate,cq.updated_datetime as updatedDate,cq.question_reward as questionReward," + 
					" cq.created_by as createdBy,cq.updated_by as updatedBy,cq.difficulty_level as difficultyLevel, " + 
					" cq.mini_jackpot_type as miniJackpotType, cq.no_of_winner  as mjpNoOfWinner, cq.qty_per_winner  as mjpQtyPerWinner,cq.jackpot_gift_card_id  as mjpGiftCardId," +
					" gc.title as mjpGiftCardName,gv.card_amount as mjpGiftCardAmt,gv.remaining_quantity as mjpGiftCardQty " + 
					" from contest_questions cq with(nolock) " + 
					" left join "+sharedProperty.getDatabasAlias()+"gift_card_value gv with(nolock) on gv.id=cq.jackpot_gift_card_id" +
					" left join "+sharedProperty.getDatabasAlias()+"gift_cards gc with(nolock) on gc.id=gv.card_id" + 
					" WHERE 1=1 ";
				
			if(questionId!=null){
				sql += " AND cq.id ="+questionId;
			}
			if(contestId!=null){
				sql += " AND cq.contest_id ="+contestId;
			}
			if(filter.getNoOfOrders()!=null){
				sql += " AND cq.question_sl_no ="+filter.getNoOfOrders();
			}
			if(filter.getText7()!=null && !filter.getText7().isEmpty()){
				sql += " AND cq.question like '%"+filter.getText7()+"%' ";
			}
			if(filter.getCost()!=null){
				sql += " AND cq.question_reward = "+filter.getCost();
			}
			
			if(filter.getText1()!=null && !filter.getText1().isEmpty()){
				sql += " AND cq.option_a like '%"+filter.getText1()+"%' ";
			}
			if(filter.getText2()!=null && !filter.getText2().isEmpty()){
				sql += " AND cq.option_b like '%"+filter.getText2()+"%' ";
			}
			if(filter.getText3()!=null && !filter.getText3().isEmpty()){
				sql += " AND cq.option_c like '%"+filter.getText3()+"%' ";
			}
			/*if(filter.getText4()!=null && !filter.getText4().isEmpty()){
				sql += " AND option_d like '%"+filter.getText4()+"%' ";
			}*/
			if(filter.getText5()!=null && !filter.getText5().isEmpty()){
				sql += " AND cq.answer like '%"+filter.getText5()+"%' ";
			}
			if(filter.getText6()!=null && !filter.getText6().isEmpty()){
				sql += " AND cq.difficulty_level like '%"+filter.getText6()+"%' ";
			}
			if(filter.getCreatedBy()!=null && !filter.getCreatedBy().isEmpty()){
				sql += " AND cq.created_by like '"+filter.getCreatedBy()+"' ";
			}
			if(filter.getLastUpdatedBy()!=null && !filter.getLastUpdatedBy().isEmpty()){
				sql += " AND cq.updated_by like '"+filter.getLastUpdatedBy()+"' ";
			}
			
			if(filter.getText8()!=null && !filter.getText8().isEmpty()){
				sql += " AND cq.mini_jackpot_type like '%"+filter.getText8()+"%' ";
				
				System.out.println("miniJackpotType................"+filter.getText8()+": "+sql);
			}
			if(filter.getText9()!=null && !filter.getText9().isEmpty()){
				try {
					sql += " AND cq.no_of_winner ="+filter.getText9().trim();
				} catch (Exception e) {
				}
			}
			if(filter.getText10()!=null && !filter.getText10().isEmpty()){
				try {
					sql += " AND cq.qty_per_winner ="+filter.getText10().trim();
				} catch (Exception e) {
				}
			}
			if(filter.getText11()!=null && !filter.getText11().isEmpty()){
				sql += " AND gc.title like '%"+filter.getText11()+"%' ";
			}
			
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,cq.created_datetime) = "+Util.
							extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,cq.created_datetime) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year, cq.created_datetime) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getUpdatedDate()!=null ){
				if(Util.extractDateElement(filter.getUpdatedDate(),"DAY") > 0){
					sql += " AND DATEPART(day, cq.updated_datetime) = "+Util.extractDateElement(filter.getUpdatedDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"MONTH") > 0){
					sql += " AND DATEPART(month, cq.updated_datetime) = "+Util.extractDateElement(filter.getUpdatedDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"YEAR") > 0){
					sql += " AND DATEPART(year, cq.updated_datetime) = "+Util.extractDateElement(filter.getUpdatedDate(),"YEAR")+" ";
				}
			}
			sql += " order by cq.question_sl_no";
			
			contestSession = ContestQuestionsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestQuestionsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			
			Properties paramCreationType = new Properties();
			paramCreationType.put("enumClass", "com.rtw.tracker.enums.MiniJackpotType");
			paramCreationType.put("type", "12"); 

			query.addScalar("miniJackpotType", Hibernate.custom(EnumType.class,paramCreationType));
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("contestId", Hibernate.INTEGER);
			query.addScalar("serialNo", Hibernate.INTEGER);
			query.addScalar("question", Hibernate.STRING);
			query.addScalar("optionA", Hibernate.STRING);
			query.addScalar("optionB", Hibernate.STRING);						
			query.addScalar("optionC", Hibernate.STRING);
			query.addScalar("difficultyLevel", Hibernate.STRING);
			query.addScalar("answer", Hibernate.STRING);
			query.addScalar("questionReward", Hibernate.DOUBLE);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);	
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);	
			query.addScalar("createdBy", Hibernate.STRING);			
			query.addScalar("updatedBy", Hibernate.STRING);
			//query.addScalar("miniJackpotType", Hibernate.STRING);
			query.addScalar("mjpNoOfWinner", Hibernate.INTEGER);
			query.addScalar("mjpQtyPerWinner", Hibernate.DOUBLE);
			query.addScalar("mjpGiftCardId", Hibernate.INTEGER);
			query.addScalar("mjpGiftCardName", Hibernate.STRING);
			query.addScalar("mjpGiftCardQty", Hibernate.INTEGER);
			query.addScalar("mjpGiftCardAmt", Hibernate.DOUBLE);
			
			
			questionList = query.setResultTransformer(Transformers.aliasToBean(ContestQuestions.class)).list();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return questionList;
	
		
	}

	@Override
	public ContestQuestions getContestQuestionsByNo(Integer contestId,Integer questionNo) {
		return findSingle("FROM ContestQuestions WHERE contestId=? and serialNo=?",new Object[]{contestId,questionNo});
	}

	@Override
	public Integer getQuestionCount(Integer contestId) {
		Integer count =0;		
		try {
			String sql = "select count(id) from contest_questions with(nolock) where contest_id="+contestId;
			
			contestSession = ContestQuestionsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestQuestionsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			
			count = (Integer) query.uniqueResult();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return count;	
	}

	@Override
	public ContestQuestions getContestQuestionByQuestion(Integer contestId,String question){
		return findSingle("FROM ContestQuestions WHERE contestId=? and question like ?",new Object[]{contestId, "%"+question+"%"});
	}
	
	public List<ContestQuestions> getMiniJackpotQuestionsByContestId(Integer contestId){
		return find("FROM ContestQuestions WHERE contestId=? and miniJackpotType is NOT NULL AND miniJackpotType <> ?",new Object[]{contestId, MiniJackpotType.NONE});
	}
	
	@Override
	public List<ContestQuestions> getContestQuestionstoReposition(Integer actualPosition,Integer contestId) {
		List<ContestQuestions> questionList = null;		
		try {
			String sql = "select id as id,contest_id as contestId,question_sl_no as serialNo,question as question,option_a as optionA,option_b as optionB,"+
						"option_c as optionC, answer as answer,created_datetime as createdDate,updated_datetime as updatedDate,question_reward as questionReward,"+
						"created_by as createdBy,updated_by as updatedBy,difficulty_level as difficultyLevel from contest_questions with(nolock) WHERE question_sl_no <> "+actualPosition;
				
			sql += " AND contest_id = "+contestId+" order by question_sl_no";
			
			contestSession = ContestQuestionsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestQuestionsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("contestId", Hibernate.INTEGER);
			query.addScalar("serialNo", Hibernate.INTEGER);
			query.addScalar("question", Hibernate.STRING);
			query.addScalar("optionA", Hibernate.STRING);
			query.addScalar("optionB", Hibernate.STRING);						
			query.addScalar("optionC", Hibernate.STRING);
			query.addScalar("difficultyLevel", Hibernate.STRING);
			query.addScalar("answer", Hibernate.STRING);
			query.addScalar("questionReward", Hibernate.DOUBLE);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);	
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);	
			query.addScalar("createdBy", Hibernate.STRING);			
			query.addScalar("updatedBy", Hibernate.STRING);
			
			
			questionList = query.setResultTransformer(Transformers.aliasToBean(ContestQuestions.class)).list();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return questionList;
	
		
	}
}
