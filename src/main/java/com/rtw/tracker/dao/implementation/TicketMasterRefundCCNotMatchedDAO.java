package com.rtw.tracker.dao.implementation;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tracker.datas.TicketMasterRefund;
import com.rtw.tracker.datas.TicketMasterRefundCCNotMatched;

public class TicketMasterRefundCCNotMatchedDAO extends HibernateDAO<Integer, TicketMasterRefundCCNotMatched> implements com.rtw.tracker.dao.services.TicketMasterRefundCCNotMatchedDAO{

	
	private static Session tmSession = null;
	
	public static Session getTmSession() {
		return TicketMasterRefundCCNotMatchedDAO.tmSession;
	}

	public static void setTmSession(Session tmSession) {
		TicketMasterRefundCCNotMatchedDAO.tmSession = tmSession;
	}
	
	@Override
	public List<TicketMasterRefundCCNotMatched> getAllUnMatchedRefundsByUser(String userName) {

		List<TicketMasterRefundCCNotMatched> list = new ArrayList<TicketMasterRefundCCNotMatched>();
		/*String sql ="select id AS id,NotMatched as notMatched,CCRefundDate as ccRefundDate,CCName as ccName,CCHolderName as ccHolderName "+
				",CCAccount as ccAccount,CCRefundAmount as ccRefundAmount,CCExtendedDetails as ccExtendedDetails "+
				",CCStatementAppearance AS ccStatementAppearance,CCAddress as ccAddress,CCReference as ccReference "+
				",CCCategory AS ccCategory  "+
				"FROM ZZZ_tmp_tm_refunds_CC_Not_Matched WHERE MappedCCHolderName = '"+userName+"' ";*/
		try {
			tmSession = TicketMasterRefundCCNotMatchedDAO.getTmSession();
			if(tmSession==null || !tmSession.isOpen() || !tmSession.isConnected()){
				tmSession = getSession();
				TicketMasterRefundCCNotMatchedDAO.setTmSession(tmSession);
			}
			
			CallableStatement  st = tmSession.connection().prepareCall("{call sp_zzz_tm_refunds_perName_cc_statement(?)}");
			st.setString(1, userName);
			ResultSet resultSet = st.executeQuery();
			System.out.println(resultSet.getFetchSize());
			TicketMasterRefundCCNotMatched tg = null;
			while(resultSet.next()){
				tg = new TicketMasterRefundCCNotMatched();
				tg.setNotMatched(resultSet.getString("NotMatched"));
				tg.setCcRefundDate(resultSet.getString("CCRefundDate"));
				tg.setCcName(resultSet.getString("CCName"));
				tg.setCcHolderName(resultSet.getString("CCHolderName"));
				tg.setCcAccount(resultSet.getString("CCAccount"));
				tg.setCcRefundAmount(resultSet.getDouble("CCRefundAmount"));
				tg.setCcExtendedDetails(resultSet.getString("CCExtendedDetails"));
				tg.setCcStatementAppearance(resultSet.getString("CCStatementAppearance"));
				tg.setCcAddress(resultSet.getString("CCAddress"));
				tg.setCcReference(resultSet.getString("CCReference"));
				tg.setCcCategory(resultSet.getString("CCCategory"));
				list.add(tg);
			}
			
			
			/*SQLQuery query = tmSession.createSQLQuery(sql);
			
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("notMatched", Hibernate.STRING);
			query.addScalar("ccRefundDate", Hibernate.STRING);
			query.addScalar("ccName", Hibernate.STRING);
			query.addScalar("ccHolderName", Hibernate.STRING);
			query.addScalar("ccAccount", Hibernate.STRING);
			query.addScalar("ccRefundAmount", Hibernate.DOUBLE);
			query.addScalar("ccExtendedDetails", Hibernate.STRING);
			query.addScalar("ccStatementAppearance", Hibernate.STRING);
			query.addScalar("ccAddress", Hibernate.STRING);
			query.addScalar("ccReference", Hibernate.STRING);
			query.addScalar("ccCategory", Hibernate.STRING);
			list = query.setResultTransformer(Transformers.aliasToBean(TicketMasterRefundCCNotMatched.class)).list();*/
		} catch (Exception e) {
			e.printStackTrace();
			if(tmSession != null && tmSession.isOpen()){
				tmSession.close();
			}
		}
		return list;
	
	}

}
