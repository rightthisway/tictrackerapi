package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.datas.FanClub;
import com.rtw.tracker.datas.FanClubMember;
import com.rtw.tracker.utils.GridHeaderFilters;

public class FanClubMemberDAO extends HibernateDAO<Integer, FanClubMember> implements com.rtw.tracker.dao.services.FanClubMemberDAO{

	
	private static Session fanClubSession;
	
	public static Session getFanClubSession() {
		return fanClubSession;
	}
	public static void setFanClubSession(Session fanClubSession) {
		FanClubMemberDAO.fanClubSession = fanClubSession;
	}


	@Override
	public List<FanClubMember> getAllFanClubMembers(GridHeaderFilters filter, String status, Integer fanClubId,SharedProperty prop) {
		List<FanClubMember> list =null;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT fr.id as id, fr.fanclub_mst_id as fanClubId, fr.customer_id as customerId, fr.joined_date as joinedDate,");
			sql.append("fr.exit_date as exitDate, fr.member_status as status,");
			sql.append("c.user_id as userId, c.email as email, c.phone as phone ");
			sql.append("FROM fanclub_members fr with(nolock) ");
			sql.append("left join  "+prop.getDatabasAlias()+"customer c with(nolock)  on fr.customer_id = c.id ");
			sql.append("WHERE fr.fanclub_mst_id="+fanClubId);
			
			if(filter.getStatus()!=null  && !filter.getStatus().isEmpty()){
				sql.append(" AND fr.member_status like '"+filter.getStatus()+"' ");
			}
			if(filter.getUsername()!=null && !filter.getUsername().isEmpty()){
				sql.append(" AND c.user_id like '%"+filter.getUsername()+"%' ");
			}
			if(filter.getEmail()!=null && !filter.getEmail().isEmpty()){
				sql.append(" AND c.email like '%"+filter.getEmail()+"%' ");
			}
			if(filter.getPhone()!=null && !filter.getPhone().isEmpty()){
				sql.append(" AND c.phone like '%"+filter.getPhone()+"%' ");
			}
			
			
			if(filter.getStartDate()!=null ){
				if(Util.extractDateElement(filter.getStartDate(),"DAY") > 0){
					sql.append(" AND DATEPART(day,fr.joined_date) = "+Util.extractDateElement(filter.getStartDate(),"DAY")+" ");
				}
				if(Util.extractDateElement(filter.getStartDate(),"MONTH") > 0){
					sql.append(" AND DATEPART(month, fr.joined_date) = "+Util.extractDateElement(filter.getStartDate(),"MONTH")+" ");
				}
				if(Util.extractDateElement(filter.getStartDate(),"YEAR") > 0){
					sql.append(" AND DATEPART(year, fr.joined_date) = "+Util.extractDateElement(filter.getStartDate(),"YEAR")+" ");
				}
			}
			
			if(filter.getEndDate()!=null ){
				if(Util.extractDateElement(filter.getEndDate(),"DAY") > 0){
					sql.append(" AND DATEPART(day, fr.exit_date ) = "+Util.extractDateElement(filter.getEndDate(),"DAY")+" ");
				}
				if(Util.extractDateElement(filter.getEndDate(),"MONTH") > 0){
					sql.append(" AND DATEPART(month, fr.exit_date ) = "+Util.extractDateElement(filter.getEndDate(),"MONTH")+" ");
				}
				if(Util.extractDateElement(filter.getEndDate(),"YEAR") > 0){
					sql.append(" AND DATEPART(year,fr.exit_date ) = "+Util.extractDateElement(filter.getEndDate(),"YEAR")+" ");
				}
			}
			
			fanClubSession = FanClubMemberDAO.getFanClubSession();
			if(fanClubSession==null || !fanClubSession.isOpen() || !fanClubSession.isConnected()){
				fanClubSession = getSession();
				FanClubMemberDAO.setFanClubSession(fanClubSession);
			}
			SQLQuery query = fanClubSession.createSQLQuery(sql.toString());
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("fanClubId", Hibernate.INTEGER);
			query.addScalar("customerId", Hibernate.INTEGER);
			query.addScalar("joinedDate", Hibernate.TIMESTAMP);
			query.addScalar("exitDate", Hibernate.TIMESTAMP);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("userId", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("phone", Hibernate.STRING);
			
			list = query.setResultTransformer(Transformers.aliasToBean(FanClubMember.class)).list();
		} catch (Exception e) {
			if(fanClubSession != null && fanClubSession.isOpen()){
				fanClubSession.close();
			}
			e.printStackTrace();
		}
		return list;
	}

}
