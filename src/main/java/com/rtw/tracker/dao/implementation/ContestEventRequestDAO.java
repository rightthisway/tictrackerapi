package com.rtw.tracker.dao.implementation;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.datas.ContestEventRequest;
import com.rtw.tracker.utils.GridHeaderFilters;

public class ContestEventRequestDAO extends HibernateDAO<Integer, ContestEventRequest> implements com.rtw.tracker.dao.services.ContestEventRequestDAO{
	
	private static Session contestSession = null;
	
	public static Session getContestSession() {
		return ContestEventRequestDAO.contestSession;
	}

	public static void setContestSession(Session contestSession) {
		ContestEventRequestDAO.contestSession = contestSession;
	}
	
	@Override
	public List<ContestEventRequest> getContestEventRequests(GridHeaderFilters filter,Integer contestRequestId) {
		List<ContestEventRequest> eventRequestList = null;
		try {
			String sql = "select cer.id as id, cer.event_tittle as eventTitle, cer.event_date as eventDate, "+
						"cer.event_location as eventLocation, cer.event_description as eventDescription, "+
						"cer.social_media_link as socialMediaLink, cer.contact_name as contactName, "+
						"cer.contact_email as contactEmail, cer.contact_phone as contactPhone, "+
						"cer.created_date as createdDate, cer.created_by as createdBy, "+
						"cer.no_of_tickets as noOfTickets "+
						" from contest_event_request cer with(nolock) "+
						" WHERE 1=1 ";
			
			if(contestRequestId!=null){
				sql += " AND cer.id ="+contestRequestId;
			}
			if(filter.getEventName()!=null && !filter.getEventName().isEmpty()){
				sql += " AND cer.event_tittle like '%"+filter.getEventName()+"%'";
			}
			if(filter.getVenueName()!=null && !filter.getVenueName().isEmpty()){
				sql += " AND cer.event_location like '%"+filter.getVenueName()+"%'";
			}
			if(filter.getEventDescription()!=null && !filter.getEventDescription().isEmpty()){
				sql += " AND cer.event_description like '%"+filter.getEventDescription()+"%'";
			}						
			if(filter.getEventDateStr() != null){
				if(Util.extractDateElement(filter.getEventDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day, cer.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month, cer.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getEventDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year, cer.event_date) = "+Util.extractDateElement(filter.getEventDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getSocialMediaLink()!=null && !filter.getSocialMediaLink().isEmpty()){
				sql += " AND cer.social_media_link like '%"+filter.getSocialMediaLink()+"%'";
			}
			if(filter.getContactName()!=null && !filter.getContactName().isEmpty()){
				sql += " AND cer.contact_name like '%"+filter.getContactName()+"%'";
			}
			if(filter.getContactEmail()!=null && !filter.getContactEmail().isEmpty()){
				sql += " AND cer.contact_email like '%"+filter.getContactEmail()+"%'";
			}
			if(filter.getContactPhone()!=null && !filter.getContactPhone().isEmpty()){
				sql += " AND cer.contact_phone like '%"+filter.getContactPhone()+"%'";
			}
			if(filter.getCreatedDateStr() != null){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day, cer.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month, cer.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year, cer.created_date) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getCreatedBy()!=null && !filter.getCreatedBy().isEmpty()){
				sql += " AND cer.created_by like '%"+filter.getCreatedBy()+"%'";
			}
			if(filter.getNoOfTixCount()!=null){
				sql += " AND cer.no_of_tickets = "+filter.getNoOfTixCount();
			}
			sql += " order by cer.id";
			
			contestSession = ContestEventRequestDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestEventRequestDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("eventTitle", Hibernate.STRING);
			query.addScalar("eventDate", Hibernate.DATE);
			query.addScalar("eventLocation", Hibernate.STRING);
			query.addScalar("eventDescription", Hibernate.STRING);
			query.addScalar("socialMediaLink", Hibernate.STRING);						
			query.addScalar("contactName", Hibernate.STRING);
			query.addScalar("contactEmail", Hibernate.STRING);
			query.addScalar("contactPhone", Hibernate.STRING);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("noOfTickets", Hibernate.INTEGER);
			
			eventRequestList = query.setResultTransformer(Transformers.aliasToBean(ContestEventRequest.class)).list();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return eventRequestList;
	}

	@Override
	public Integer getContestRequestCount(Integer contestRequestId) {
		Session questionSession = null;
		Integer count =0;		
		try {
			String sql = "select count(id) from contest_event_request with(nolock) where contest_id="+contestRequestId;
			
			contestSession = ContestEventRequestDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestEventRequestDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			count = (Integer) query.uniqueResult();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return count;	
	}

}
