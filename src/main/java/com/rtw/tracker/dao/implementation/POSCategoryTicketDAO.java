package com.rtw.tracker.dao.implementation;

import java.util.List;

import com.rtw.tracker.datas.POSCategoryTicket;

public class POSCategoryTicketDAO extends HibernateDAO<Integer, POSCategoryTicket> implements com.rtw.tracker.dao.services.POSCategoryTicketDAO{
	
	public POSCategoryTicket getPOSCategoryTicketByCategoryTicketId(Integer categoryTicketId) throws Exception{
		return findSingle("FROM POSCategoryTicket WHERE categoryTicketId=? ", new Object[]{categoryTicketId});
	}
	
	public POSCategoryTicket getPOSCategoryTicketByCategoryTicketGroupId(Integer categoryTicketGroupId) throws Exception{
		return findSingle("FROM POSCategoryTicket WHERE categoryTicketGroupId=? ", new Object[]{categoryTicketGroupId});
	}
	
	public List<POSCategoryTicket> getPOSCategoryTicketByInvoiceId(Integer invoiceId,String productType) throws Exception{
		return find("FROM POSCategoryTicket WHERE invoiceId=? AND productType=? Order by categoryTicketGroupId", new Object[]{invoiceId,productType});
	}

}
