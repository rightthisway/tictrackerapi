package com.rtw.tracker.dao.implementation;

import com.rtw.tracker.datas.LoyaltySettings;

/**
 * class having db related methods for LoyaltySettings
 * @author Ulaganathan
 *
 */
public class LoyaltySettingsDAO extends HibernateDAO<Integer, LoyaltySettings> implements com.rtw.tracker.dao.services.LoyaltySettingsDAO{
	
	
	public LoyaltySettings getActivetLoyaltySettings() {
		return findSingle("from LoyaltySettings where status='ACTIVE' ", new Object[]{});
	}
	
}
