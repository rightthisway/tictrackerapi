package com.rtw.tracker.dao.implementation;

import java.util.Collection;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.EnumType;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.datas.InventoryHold;
import com.rtw.tracker.datas.RewardDetails;
import com.rtw.tracker.datas.TicketGroup;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.PaginationUtil;

public class InventoryHoldDAO extends HibernateDAO<Integer, InventoryHold> implements com.rtw.tracker.dao.services.InventoryHoldDAO{

	public List<InventoryHold> getAllByCustomerId(Integer customerId){
		return find("FROM InventoryHold WHERE customerId=? ", new Object[]{customerId});
	}
	
	public InventoryHold getAllByTicketId(Integer ticketId){
		return findSingle("FROM InventoryHold WHERE ticketIds=? ", new Object[]{ticketId});
	}
	
	@SuppressWarnings("unchecked")
	public Collection<InventoryHold> getAllActiveHoldTickets(){
		return find("FROM InventoryHold WHERE status = 'ACTIVE' ");
	}

	@Override
	public List<InventoryHold> getInventoryHoldByTicketGroupId(Integer ticketGroupId) {
		return find("FROM InventoryHold WHERE status = 'ACTIVE' AND ticketGroupId=?",new Object[]{ticketGroupId});
	}
	
	public List<InventoryHold> getAllHoldTickets(Integer brokerId, Integer eventId, GridHeaderFilters filter){
		Session holdTicketSession = null;
		List<InventoryHold> inventoryHoldList = null;
		try{
			String queryStr =  "select inv.customer_id as customerId, c.cust_name+' '+c.last_name as customerName, inv.sale_price as salePrice, inv.csr as cSR, "+ 
				"inv.expiration_date as expirationDate, inv.expiration_minutes as expirationMinutes, inv.shipping_method_id as shippingMethodId, "+
				"inv.hold_datetime as holdDateTime, inv.internal_note as internalNote, inv.external_note as externalNote, inv.ticket_ids as ticketIds, "+
				"inv.external_po as externalPo, inv.ticket_group_id as ticketGroupId, inv.status as status, sm.name as shippingMethod, "+
				"inv.broker_id as brokerId";
			
				if(eventId != null && eventId > 0){
					queryStr += ", tg.section as section, tg.row as row, tg.seat_low as seatLow, tg.seat_high as seatHigh ";
				}
				queryStr += " from inventory_hold inv with(nolock) "+ 
				"left join customer c with(nolock) on c.id = inv.customer_id "+
				"left join shipping_method sm with(nolock) on inv.shipping_method_id = sm.id ";
			
			if(eventId != null && eventId > 0){
				queryStr += " left join ticket_group tg with(nolock) on tg.id = inv.ticket_group_id left join event ev with(nolock) on ev.id = tg.event_id ";
			}
			queryStr += "where 1 = 1"; 
			
			if(brokerId != null && brokerId > 0){
				queryStr += " AND inv.broker_id = "+brokerId+" ";
			}
			if(eventId != null && eventId > 0){
				queryStr += " AND ev.id = "+eventId+" AND inv.status = 'ACTIVE' ";
			}
			if(filter.getCustomerId() != null && filter.getCustomerId() > 0){
				queryStr += " AND inv.customer_id = "+filter.getCustomerId()+" ";
			}
			if(filter.getCustomerName()!= null){
				queryStr += " AND (c.cust_name like '%"+filter.getCustomerName()+"%' OR c.last_name like '%"+filter.getCustomerName()+"%') ";
			}
			if(filter.getSalePrice() != null){
				queryStr += " AND inv.sale_price = "+filter.getSalePrice()+" ";
			}
			if(filter.getCsr() != null){
				queryStr += " AND inv.cSR like '%"+filter.getCsr()+"%' ";
			}
			if(filter.getExpirationDateStr() != null){
				String dateTimeStr = filter.getExpirationDateStr();
				if(dateTimeStr.contains(" ")){
					String dateTimeArr[] = dateTimeStr.split(" ");
					if(Util.extractDateElement(dateTimeArr[0],"DAY") > 0){
						queryStr += " AND DATEPART(day,inv.expiration_date) = "+Util.extractDateElement(dateTimeArr[0],"DAY")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"MONTH") > 0){
						queryStr += " AND DATEPART(month,inv.expiration_date) = "+Util.extractDateElement(dateTimeArr[0],"MONTH")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"YEAR") > 0){
						queryStr += " AND DATEPART(year,inv.expiration_date) = "+Util.extractDateElement(dateTimeArr[0],"YEAR")+" ";
					}
					if(dateTimeArr.length>1){
						String dateTime = dateTimeArr[1];
						if(dateTimeArr.length > 2){
							if(dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()){
								dateTime += " "+dateTimeArr[2];
							}
						}
						if(Util.extractDateElement(dateTime,"HOUR") > 0){
							queryStr += " AND DATEPART(hour,inv.expiration_date) = "+Util.extractDateElement(dateTime,"HOUR")+" ";
						}
						if(Util.extractDateElement(dateTime,"MINUTE") > 0){
							queryStr += " AND DATEPART(minute,inv.expiration_date) = "+Util.extractDateElement(dateTime,"MINUTE")+" ";
						}
					}
				}else{
					if(Util.extractDateElement(filter.getExpirationDateStr(),"DAY") > 0){
						queryStr += " AND DATEPART(day,inv.expiration_date) = "+Util.extractDateElement(filter.getExpirationDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getExpirationDateStr(),"MONTH") > 0){
						queryStr += " AND DATEPART(month,inv.expiration_date) = "+Util.extractDateElement(filter.getExpirationDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getExpirationDateStr(),"YEAR") > 0){
						queryStr += " AND DATEPART(year,inv.expiration_date) = "+Util.extractDateElement(filter.getExpirationDateStr(),"YEAR")+" ";
					}
				}
			}
			if(filter.getExpirationMinutes() != null && filter.getExpirationMinutes() > 0){
				queryStr += " AND inv.expiration_minutes = "+filter.getExpirationMinutes()+" ";
			}
			if(filter.getShippingMethodId() != null && filter.getShippingMethodId() > 0){
				queryStr += " AND inv.shipping_method_id = "+filter.getShippingMethodId()+" ";
			}
			if(filter.getShippingMethod() != null){
				queryStr += " AND sm.name like '%"+filter.getShippingMethod()+"%' ";
			}
			if(filter.getHoldDateStr() != null){
				String dateTimeStr = filter.getHoldDateStr();
				if(dateTimeStr.contains(" ")){
					String dateTimeArr[] = dateTimeStr.split(" ");
					if(Util.extractDateElement(dateTimeArr[0],"DAY") > 0){
						queryStr += " AND DATEPART(day,inv.hold_datetime) = "+Util.extractDateElement(dateTimeArr[0],"DAY")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"MONTH") > 0){
						queryStr += " AND DATEPART(month,inv.hold_datetime) = "+Util.extractDateElement(dateTimeArr[0],"MONTH")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"YEAR") > 0){
						queryStr += " AND DATEPART(year,inv.hold_datetime) = "+Util.extractDateElement(dateTimeArr[0],"YEAR")+" ";
					}
					if(dateTimeArr.length>1){
						String dateTime = dateTimeArr[1];
						if(dateTimeArr.length > 2){
							if(dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()){
								dateTime += " "+dateTimeArr[2];
							}
						}
						if(Util.extractDateElement(dateTime,"HOUR") > 0){
							queryStr += " AND DATEPART(hour,inv.hold_datetime) = "+Util.extractDateElement(dateTime,"HOUR")+" ";
						}
						if(Util.extractDateElement(dateTime,"MINUTE") > 0){
							queryStr += " AND DATEPART(minute,inv.hold_datetime) = "+Util.extractDateElement(dateTime,"MINUTE")+" ";
						}
					}
				}else{
					if(Util.extractDateElement(filter.getHoldDateStr(),"DAY") > 0){
						queryStr += " AND DATEPART(day,inv.hold_datetime) = "+Util.extractDateElement(filter.getHoldDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getHoldDateStr(),"MONTH") > 0){
						queryStr += " AND DATEPART(month,inv.hold_datetime) = "+Util.extractDateElement(filter.getHoldDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getHoldDateStr(),"YEAR") > 0){
						queryStr += " AND DATEPART(year,inv.hold_datetime) = "+Util.extractDateElement(filter.getHoldDateStr(),"YEAR")+" ";
					}
				}
			}
			if(filter.getInternalNotes() != null){
				queryStr += " AND inv.internal_note like '%"+filter.getInternalNotes()+"%' ";
			}
			if(filter.getExternalNotes() != null){
				queryStr += " AND inv.external_note like '%"+filter.getExternalNotes()+"%' ";
			}
			if(filter.getExternalNo() != null){
				queryStr += " AND inv.external_po like '%"+filter.getExternalNo()+"%' ";
			}
			if(filter.getTicketIdsStr() != null){				
				queryStr += "AND inv.ticket_ids like '%"+filter.getTicketIdsStr()+"%' ";
			}
			if(filter.getTicketGroupId() != null){
				queryStr += " AND inv.ticket_group_id = "+filter.getTicketGroupId()+" ";
			}
			if(filter.getStatus() != null){
				queryStr += " AND inv.status like '%"+filter.getStatus()+"%' ";
			}
			if(filter.getBrokerId() != null){
				queryStr += " AND inv.broker_id = "+filter.getBrokerId()+" ";
			}
			if(filter.getSection() != null){
				queryStr += " AND tg.section like '%"+filter.getSection()+"%' ";
			}
			if(filter.getRow() != null){
				queryStr += " AND tg.row like '%"+filter.getRow()+"%' ";
			}
			if(filter.getSeatLow() != null){
				queryStr += " AND tg.seat_low like '%"+filter.getSeatLow()+"%' ";
			}
			if(filter.getSeatHigh() != null){
				queryStr += " AND tg.seat_high like '%"+filter.getSeatHigh()+"%' ";
			}
			
			holdTicketSession = getSession();
			SQLQuery query = holdTicketSession.createSQLQuery(queryStr);
			query.addScalar("customerId", Hibernate.INTEGER);
			query.addScalar("customerName", Hibernate.STRING);
			query.addScalar("salePrice", Hibernate.DOUBLE);
			query.addScalar("cSR", Hibernate.STRING);
			query.addScalar("expirationDate", Hibernate.TIMESTAMP);
			query.addScalar("expirationMinutes",Hibernate.INTEGER);
			query.addScalar("shippingMethodId",Hibernate.INTEGER);
			query.addScalar("shippingMethod",Hibernate.STRING);
			query.addScalar("holdDateTime", Hibernate.TIMESTAMP);
			query.addScalar("internalNote", Hibernate.STRING);
			query.addScalar("externalNote", Hibernate.STRING);
			query.addScalar("ticketIds", Hibernate.STRING);
			query.addScalar("externalPo", Hibernate.STRING);
			query.addScalar("ticketGroupId", Hibernate.INTEGER);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("brokerId", Hibernate.INTEGER);
			if(eventId != null && eventId > 0){
				query.addScalar("section", Hibernate.STRING);
				query.addScalar("row", Hibernate.STRING);
				query.addScalar("seatLow", Hibernate.STRING);
				query.addScalar("seatHigh", Hibernate.STRING);
			}
			
			inventoryHoldList = query.setResultTransformer(Transformers.aliasToBean(InventoryHold.class)).list();
		}catch(Exception e){
			if(holdTicketSession != null && holdTicketSession.isOpen()){
				holdTicketSession.close();
			}
			e.printStackTrace();
		}
		return inventoryHoldList;
	}
	
	public List<InventoryHold> getAllHoldTicketsWithPagination(Integer brokerId, Integer eventId, GridHeaderFilters filter, String pageNo){
		Session holdTicketSession = null;
		List<InventoryHold> inventoryHoldList = null;
		Integer firstResult = null;
		try{
			String queryStr =  "select inv.id as id,inv.customer_id as customerId, c.cust_name+' '+c.last_name as customerName, inv.sale_price as salePrice, inv.csr as cSR, "+ 
				"inv.expiration_date as expirationDate, inv.expiration_minutes as expirationMinutes, inv.shipping_method_id as shippingMethodId, "+
				"inv.hold_datetime as holdDateTime, inv.internal_note as internalNote, inv.external_note as externalNote, inv.ticket_ids as ticketIds, "+
				"inv.external_po as externalPo, inv.ticket_group_id as ticketGroupId, inv.status as status, sm.name as shippingMethod, "+
				"inv.broker_id as brokerId";
			
				if(eventId != null && eventId > 0){
					queryStr += ", tg.section as section, tg.row as row, tg.seat_low as seatLow, tg.seat_high as seatHigh,tg.quantity as quantity ";
				}
				queryStr += " from inventory_hold inv with(nolock) "+ 
				"left join customer c with(nolock) on c.id = inv.customer_id "+
				"left join shipping_method sm with(nolock) on inv.shipping_method_id = sm.id ";
			
			if(eventId != null && eventId > 0){
				queryStr += " left join ticket_group tg with(nolock) on tg.id = inv.ticket_group_id left join event ev with(nolock) on ev.id = tg.event_id ";
			}
			queryStr += "where 1 = 1"; 
			
			if(brokerId != null && brokerId > 0){
				queryStr += " AND inv.broker_id = "+brokerId+" ";
			}
			if(eventId != null && eventId > 0){
				queryStr += " AND ev.id = "+eventId+" AND inv.status = 'ACTIVE' ";
			}
			if(filter.getCustomerId() != null && filter.getCustomerId() > 0){
				queryStr += " AND inv.customer_id = "+filter.getCustomerId()+" ";
			}
			if(filter.getCustomerName()!= null){
				queryStr += " AND (c.cust_name like '%"+filter.getCustomerName()+"%' OR c.last_name like '%"+filter.getCustomerName()+"%') ";
			}
			if(filter.getSalePrice() != null){
				queryStr += " AND inv.sale_price = "+filter.getSalePrice()+" ";
			}
			if(filter.getCsr() != null){
				queryStr += " AND inv.cSR like '%"+filter.getCsr()+"%' ";
			}
			if(filter.getExpirationDateStr() != null){
				String dateTimeStr = filter.getExpirationDateStr();
				if(dateTimeStr.contains(" ")){
					String dateTimeArr[] = dateTimeStr.split(" ");
					if(Util.extractDateElement(dateTimeArr[0],"DAY") > 0){
						queryStr += " AND DATEPART(day,inv.expiration_date) = "+Util.extractDateElement(dateTimeArr[0],"DAY")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"MONTH") > 0){
						queryStr += " AND DATEPART(month,inv.expiration_date) = "+Util.extractDateElement(dateTimeArr[0],"MONTH")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"YEAR") > 0){
						queryStr += " AND DATEPART(year,inv.expiration_date) = "+Util.extractDateElement(dateTimeArr[0],"YEAR")+" ";
					}
					if(dateTimeArr.length>1){
						String dateTime = dateTimeArr[1];
						if(dateTimeArr.length > 2){
							if(dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()){
								dateTime += " "+dateTimeArr[2];
							}
						}
						if(Util.extractDateElement(dateTime,"HOUR") > 0){
							queryStr += " AND DATEPART(hour,inv.expiration_date) = "+Util.extractDateElement(dateTime,"HOUR")+" ";
						}
						if(Util.extractDateElement(dateTime,"MINUTE") > 0){
							queryStr += " AND DATEPART(minute,inv.expiration_date) = "+Util.extractDateElement(dateTime,"MINUTE")+" ";
						}
					}
				}else{
					if(Util.extractDateElement(filter.getExpirationDateStr(),"DAY") > 0){
						queryStr += " AND DATEPART(day,inv.expiration_date) = "+Util.extractDateElement(filter.getExpirationDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getExpirationDateStr(),"MONTH") > 0){
						queryStr += " AND DATEPART(month,inv.expiration_date) = "+Util.extractDateElement(filter.getExpirationDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getExpirationDateStr(),"YEAR") > 0){
						queryStr += " AND DATEPART(year,inv.expiration_date) = "+Util.extractDateElement(filter.getExpirationDateStr(),"YEAR")+" ";
					}
				}
			}
			if(filter.getExpirationMinutes() != null && filter.getExpirationMinutes() > 0){
				queryStr += " AND inv.expiration_minutes = "+filter.getExpirationMinutes()+" ";
			}
			if(filter.getShippingMethodId() != null && filter.getShippingMethodId() > 0){
				queryStr += " AND inv.shipping_method_id = "+filter.getShippingMethodId()+" ";
			}
			if(filter.getShippingMethod() != null){
				queryStr += " AND sm.name like '%"+filter.getShippingMethod()+"%' ";
			}
			if(filter.getHoldDateStr() != null){
				String dateTimeStr = filter.getHoldDateStr();
				if(dateTimeStr.contains(" ")){
					String dateTimeArr[] = dateTimeStr.split(" ");
					if(Util.extractDateElement(dateTimeArr[0],"DAY") > 0){
						queryStr += " AND DATEPART(day,inv.hold_datetime) = "+Util.extractDateElement(dateTimeArr[0],"DAY")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"MONTH") > 0){
						queryStr += " AND DATEPART(month,inv.hold_datetime) = "+Util.extractDateElement(dateTimeArr[0],"MONTH")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"YEAR") > 0){
						queryStr += " AND DATEPART(year,inv.hold_datetime) = "+Util.extractDateElement(dateTimeArr[0],"YEAR")+" ";
					}
					if(dateTimeArr.length>1){
						String dateTime = dateTimeArr[1];
						if(dateTimeArr.length > 2){
							if(dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()){
								dateTime += " "+dateTimeArr[2];
							}
						}
						if(Util.extractDateElement(dateTime,"HOUR") > 0){
							queryStr += " AND DATEPART(hour,inv.hold_datetime) = "+Util.extractDateElement(dateTime,"HOUR")+" ";
						}
						if(Util.extractDateElement(dateTime,"MINUTE") > 0){
							queryStr += " AND DATEPART(minute,inv.hold_datetime) = "+Util.extractDateElement(dateTime,"MINUTE")+" ";
						}
					}
				}else{
					if(Util.extractDateElement(filter.getHoldDateStr(),"DAY") > 0){
						queryStr += " AND DATEPART(day,inv.hold_datetime) = "+Util.extractDateElement(filter.getHoldDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getHoldDateStr(),"MONTH") > 0){
						queryStr += " AND DATEPART(month,inv.hold_datetime) = "+Util.extractDateElement(filter.getHoldDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getHoldDateStr(),"YEAR") > 0){
						queryStr += " AND DATEPART(year,inv.hold_datetime) = "+Util.extractDateElement(filter.getHoldDateStr(),"YEAR")+" ";
					}
				}
			}
			if(filter.getInternalNotes() != null){
				queryStr += " AND inv.internal_note like '%"+filter.getInternalNotes()+"%' ";
			}
			if(filter.getExternalNotes() != null){
				queryStr += " AND inv.external_note like '%"+filter.getExternalNotes()+"%' ";
			}
			if(filter.getExternalNo() != null){
				queryStr += " AND inv.external_po like '%"+filter.getExternalNo()+"%' ";
			}
			if(filter.getTicketIdsStr() != null){				
				queryStr += "AND inv.ticket_ids like '%"+filter.getTicketIdsStr()+"%' ";
			}
			if(filter.getTicketGroupId() != null){
				queryStr += " AND inv.ticket_group_id = "+filter.getTicketGroupId()+" ";
			}
			if(filter.getStatus() != null){
				queryStr += " AND inv.status like '%"+filter.getStatus()+"%' ";
			}
			if(filter.getBrokerId() != null){
				queryStr += " AND inv.broker_id = "+filter.getBrokerId()+" ";
			}
			if(filter.getSection() != null){
				queryStr += " AND tg.section like '%"+filter.getSection()+"%' ";
			}
			if(filter.getRow() != null){
				queryStr += " AND tg.row like '%"+filter.getRow()+"%' ";
			}
			if(filter.getSeatLow() != null){
				queryStr += " AND tg.seat_low like '%"+filter.getSeatLow()+"%' ";
			}
			if(filter.getSeatHigh() != null){
				queryStr += " AND tg.seat_high like '%"+filter.getSeatHigh()+"%' ";
			}
			
			holdTicketSession = getSession();
			SQLQuery query = holdTicketSession.createSQLQuery(queryStr);
			query.addScalar("id", Hibernate.INTEGER);
			//query.addScalar("quantity", Hibernate.INTEGER);			
			query.addScalar("customerId", Hibernate.INTEGER);
			query.addScalar("customerName", Hibernate.STRING);
			query.addScalar("salePrice", Hibernate.DOUBLE);
			query.addScalar("cSR", Hibernate.STRING);
			query.addScalar("expirationDate", Hibernate.TIMESTAMP);
			query.addScalar("expirationMinutes",Hibernate.INTEGER);
			query.addScalar("shippingMethodId",Hibernate.INTEGER);
			query.addScalar("shippingMethod",Hibernate.STRING);
			query.addScalar("holdDateTime", Hibernate.TIMESTAMP);
			query.addScalar("internalNote", Hibernate.STRING);
			query.addScalar("externalNote", Hibernate.STRING);
			query.addScalar("ticketIds", Hibernate.STRING);
			query.addScalar("externalPo", Hibernate.STRING);
			query.addScalar("ticketGroupId", Hibernate.INTEGER);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("brokerId", Hibernate.INTEGER);
			if(eventId != null && eventId > 0){
				query.addScalar("section", Hibernate.STRING);
				query.addScalar("row", Hibernate.STRING);
				query.addScalar("seatLow", Hibernate.STRING);
				query.addScalar("seatHigh", Hibernate.STRING);
			}
			
			firstResult = PaginationUtil.getNextPageStatFrom(pageNo);			
			inventoryHoldList = query.setResultTransformer(Transformers.aliasToBean(InventoryHold.class)).setFirstResult(firstResult).setMaxResults(PaginationUtil.PAGESIZE).list();
		}catch(Exception e){
			if(holdTicketSession != null && holdTicketSession.isOpen()){
				holdTicketSession.close();
			}
			e.printStackTrace();
		}
		return inventoryHoldList;
	}

	public Integer getAllHoldTicketsCount(Integer brokerId, Integer eventId, GridHeaderFilters filter){
		Session holdTicketSession = null;
		Integer count = 0;
		try{
			String queryStr = "select count(*) as cnt "+
				"from inventory_hold inv with(nolock) left join customer c with(nolock) on c.id = inv.customer_id "+
				" left join shipping_method sm with(nolock) on inv.shipping_method_id = sm.id "; 
			
			if(eventId != null && eventId > 0){
				queryStr += " left join ticket_group tg with(nolock) on tg.id = inv.ticket_group_id left join event ev with(nolock) on ev.id = tg.event_id ";
			}
			queryStr += " where 1 = 1"; 
			
			if(brokerId != null && brokerId > 0){
				queryStr += " AND inv.broker_id = "+brokerId+" ";
			}
			if(eventId != null && eventId > 0){
				queryStr += " AND ev.id = "+eventId+" AND inv.status = 'ACTIVE' ";
			}
			if(filter.getCustomerId() != null && filter.getCustomerId() > 0){
				queryStr += " AND inv.customer_id = "+filter.getCustomerId()+" ";
			}
			if(filter.getCustomerName()!= null){
				queryStr += " AND (c.cust_name like '%"+filter.getCustomerName()+"%' OR c.last_name like '%"+filter.getCustomerName()+"%') ";
			}
			if(filter.getSalePrice() != null){
				queryStr += " AND inv.sale_price = "+filter.getSalePrice()+" ";
			}
			if(filter.getCsr() != null){
				queryStr += " AND inv.cSR like '%"+filter.getCsr()+"%' ";
			}
			if(filter.getExpirationDateStr() != null){
				String dateTimeStr = filter.getExpirationDateStr();
				if(dateTimeStr.contains(" ")){
					String dateTimeArr[] = dateTimeStr.split(" ");
					if(Util.extractDateElement(dateTimeArr[0],"DAY") > 0){
						queryStr += " AND DATEPART(day,inv.expiration_date) = "+Util.extractDateElement(dateTimeArr[0],"DAY")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"MONTH") > 0){
						queryStr += " AND DATEPART(month,inv.expiration_date) = "+Util.extractDateElement(dateTimeArr[0],"MONTH")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"YEAR") > 0){
						queryStr += " AND DATEPART(year,inv.expiration_date) = "+Util.extractDateElement(dateTimeArr[0],"YEAR")+" ";
					}
					if(dateTimeArr.length>1){
						String dateTime = dateTimeArr[1];
						if(dateTimeArr.length > 2){
							if(dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()){
								dateTime += " "+dateTimeArr[2];
							}
						}
						if(Util.extractDateElement(dateTime,"HOUR") > 0){
							queryStr += " AND DATEPART(hour,inv.expiration_date) = "+Util.extractDateElement(dateTime,"HOUR")+" ";
						}
						if(Util.extractDateElement(dateTime,"MINUTE") > 0){
							queryStr += " AND DATEPART(minute,inv.expiration_date) = "+Util.extractDateElement(dateTime,"MINUTE")+" ";
						}
					}
				}else{
					if(Util.extractDateElement(filter.getExpirationDateStr(),"DAY") > 0){
						queryStr += " AND DATEPART(day,inv.expiration_date) = "+Util.extractDateElement(filter.getExpirationDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getExpirationDateStr(),"MONTH") > 0){
						queryStr += " AND DATEPART(month,inv.expiration_date) = "+Util.extractDateElement(filter.getExpirationDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getExpirationDateStr(),"YEAR") > 0){
						queryStr += " AND DATEPART(year,inv.expiration_date) = "+Util.extractDateElement(filter.getExpirationDateStr(),"YEAR")+" ";
					}
				}
			}
			if(filter.getExpirationMinutes() != null && filter.getExpirationMinutes() > 0){
				queryStr += " AND inv.expiration_minutes = "+filter.getExpirationMinutes()+" ";
			}
			if(filter.getShippingMethodId() != null && filter.getShippingMethodId() > 0){
				queryStr += " AND inv.shipping_method_id = "+filter.getShippingMethodId()+" ";
			}
			if(filter.getShippingMethod() != null){
				queryStr += " AND sm.name like '%"+filter.getShippingMethod()+"%' ";
			}
			if(filter.getHoldDateStr() != null){
				String dateTimeStr = filter.getHoldDateStr();
				if(dateTimeStr.contains(" ")){
					String dateTimeArr[] = dateTimeStr.split(" ");
					if(Util.extractDateElement(dateTimeArr[0],"DAY") > 0){
						queryStr += " AND DATEPART(day,inv.hold_datetime) = "+Util.extractDateElement(dateTimeArr[0],"DAY")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"MONTH") > 0){
						queryStr += " AND DATEPART(month,inv.hold_datetime) = "+Util.extractDateElement(dateTimeArr[0],"MONTH")+" ";
					}
					if(Util.extractDateElement(dateTimeArr[0],"YEAR") > 0){
						queryStr += " AND DATEPART(year,inv.hold_datetime) = "+Util.extractDateElement(dateTimeArr[0],"YEAR")+" ";
					}
					if(dateTimeArr.length>1){
						String dateTime = dateTimeArr[1];
						if(dateTimeArr.length > 2){
							if(dateTimeArr[2] != null && !dateTimeArr[2].isEmpty()){
								dateTime += " "+dateTimeArr[2];
							}
						}
						if(Util.extractDateElement(dateTime,"HOUR") > 0){
							queryStr += " AND DATEPART(hour,inv.hold_datetime) = "+Util.extractDateElement(dateTime,"HOUR")+" ";
						}
						if(Util.extractDateElement(dateTime,"MINUTE") > 0){
							queryStr += " AND DATEPART(minute,inv.hold_datetime) = "+Util.extractDateElement(dateTime,"MINUTE")+" ";
						}
					}
				}else{
					if(Util.extractDateElement(filter.getHoldDateStr(),"DAY") > 0){
						queryStr += " AND DATEPART(day,inv.hold_datetime) = "+Util.extractDateElement(filter.getHoldDateStr(),"DAY")+" ";
					}
					if(Util.extractDateElement(filter.getHoldDateStr(),"MONTH") > 0){
						queryStr += " AND DATEPART(month,inv.hold_datetime) = "+Util.extractDateElement(filter.getHoldDateStr(),"MONTH")+" ";
					}
					if(Util.extractDateElement(filter.getHoldDateStr(),"YEAR") > 0){
						queryStr += " AND DATEPART(year,inv.hold_datetime) = "+Util.extractDateElement(filter.getHoldDateStr(),"YEAR")+" ";
					}
				}
			}
			if(filter.getInternalNotes() != null){
				queryStr += " AND inv.internal_note like '%"+filter.getInternalNotes()+"%' ";
			}
			if(filter.getExternalNotes() != null){
				queryStr += " AND inv.external_note like '%"+filter.getExternalNotes()+"%' ";
			}
			if(filter.getExternalNo() != null){
				queryStr += " AND inv.external_po like '%"+filter.getExternalNo()+"%' ";
			}
			if(filter.getTicketIdsStr() != null){				
				queryStr += "AND inv.ticket_ids like '%"+filter.getTicketIdsStr()+"%' ";
			}
			if(filter.getTicketGroupId() != null){
				queryStr += " AND inv.ticket_group_id = "+filter.getTicketGroupId()+" ";
			}
			if(filter.getStatus() != null){
				queryStr += " AND inv.status like '%"+filter.getStatus()+"%' ";
			}
			if(filter.getBrokerId() != null){
				queryStr += " AND inv.broker_id = "+filter.getBrokerId()+" ";
			}
			if(filter.getSection() != null){
				queryStr += " AND tg.section like '%"+filter.getSection()+"%' ";
			}
			if(filter.getRow() != null){
				queryStr += " AND tg.row like '%"+filter.getRow()+"%' ";
			}
			if(filter.getSeatLow() != null){
				queryStr += " AND tg.seat_low like '%"+filter.getSeatLow()+"%' ";
			}
			if(filter.getSeatHigh() != null){
				queryStr += " AND tg.seat_high like '%"+filter.getSeatHigh()+"%' ";
			}
			
			holdTicketSession = getSession();
			SQLQuery query = holdTicketSession.createSQLQuery(queryStr);
			count = Integer.parseInt(String.valueOf(query.uniqueResult()));
			return count;
		}catch(Exception e){
			if(holdTicketSession != null && holdTicketSession.isOpen()){
				holdTicketSession.close();
			}
			e.printStackTrace();			
		}
		return null;
	}
}
