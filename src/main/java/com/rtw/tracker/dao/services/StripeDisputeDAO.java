package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.StripeDispute;

public interface StripeDisputeDAO extends RootDAO<Integer, StripeDispute>{
	
	public List<StripeDispute> getAllDisputeList();
}
