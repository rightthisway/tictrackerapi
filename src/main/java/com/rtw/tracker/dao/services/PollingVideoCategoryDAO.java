package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.PollingCategory;
import com.rtw.tracker.datas.PollingVideoCategory;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface PollingVideoCategoryDAO extends RootDAO<Integer, PollingVideoCategory>{

	public List<PollingVideoCategory> getAllPollingVideoCategory(GridHeaderFilters filter, String status);
	
	public PollingVideoCategory getPollingVideoCategoryById(Integer id) throws Exception;

	public PollingVideoCategory savePollingVideoCategory(PollingVideoCategory pollingVideoCategory) throws Exception;

	public Integer getMaxSeqPollingCategory();

	public Integer getVideoInventoryLinkedToCategory(String catId);
	public List<PollingVideoCategory> getPopularVideoChannels(GridHeaderFilters filter);	
}