package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.CustomerProfileQuestion;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface CustomerProfileQuestionDAO extends RootDAO<Integer, CustomerProfileQuestion>{

	public List<CustomerProfileQuestion> getAllCustomerQuestionByStatus(GridHeaderFilters filter, String status);
	public CustomerProfileQuestion getLastQuestion();
	public CustomerProfileQuestion getQuestionBySerialNo(Integer serialNo);
}
