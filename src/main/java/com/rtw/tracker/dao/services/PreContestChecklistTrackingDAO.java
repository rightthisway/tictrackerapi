package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.datas.PreContestChecklistTracking;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface PreContestChecklistTrackingDAO extends RootDAO<Integer, PreContestChecklistTracking>{
	
	public List<PreContestChecklistTracking> getPreContestChecklistTrackingbyFilters(String status, GridHeaderFilters filter,SharedProperty property);

}
