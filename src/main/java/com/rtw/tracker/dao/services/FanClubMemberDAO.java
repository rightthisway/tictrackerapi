package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.dao.services.RootDAO;
import com.rtw.tracker.datas.FanClubMember;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface FanClubMemberDAO extends RootDAO<Integer, FanClubMember>{

	public List<FanClubMember> getAllFanClubMembers(GridHeaderFilters filter,String status,Integer fanClubId,SharedProperty prop);
}
