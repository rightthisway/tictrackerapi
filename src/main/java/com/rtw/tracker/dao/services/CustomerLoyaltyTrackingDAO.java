package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.CustomerLoyaltyTracking;

public interface CustomerLoyaltyTrackingDAO extends RootDAO<Integer, CustomerLoyaltyTracking>{
	
	public List<CustomerLoyaltyTracking> getTrackingByCustomerId(Integer customerId);

}
