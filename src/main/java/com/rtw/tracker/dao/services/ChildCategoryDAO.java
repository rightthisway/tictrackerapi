package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.ChildCategory;
 
/**
 * interface having method related to Child Category 
 * @author hamin
 *
 */
public interface ChildCategoryDAO extends RootDAO<Integer, ChildCategory>{
	/**
	 * method to get List of child category  
	 @return List of ChildCategory 
	 */
	public List<ChildCategory> getAll();
	/**
	 * method to get child category using its name
	 * @param name, name of child category  
	 * @return ChildCategory
	 */
	public ChildCategory getChildCategoryByName(String name);
	/**
	 * method to get child category using its id
	 * @param id, child category id
	 * @return ChildCategory
	 */
	public ChildCategory getChildCategoryById(
			Integer id);

	public List<ChildCategory> getChildCategoryByParentCategoryName(String parentCategoryName);
	/**
	 * 
	 * @param name
	 * @return
	 */
	public List<ChildCategory> getChildCategoriesByNameAndDisplayOnSearch(String name);
	public List<ChildCategory> getAllChildCategoryByParentCategory(Integer parentCategoryId) throws Exception;
	public List<ChildCategory> getAllChildCategoryByDisplayOnSearch() throws Exception;
	public List<ChildCategory> getAllChildCategoriesByNameAndDisplayOnSearch(String name);
}
