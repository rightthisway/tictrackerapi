package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.POSCategoryTicket;

public interface POSCategoryTicketDAO extends RootDAO<Integer, POSCategoryTicket>{
	
	public POSCategoryTicket getPOSCategoryTicketByCategoryTicketId(Integer categoryTicketId) throws Exception;
	public POSCategoryTicket getPOSCategoryTicketByCategoryTicketGroupId(Integer categoryTicketGroupId) throws Exception;
	public List<POSCategoryTicket> getPOSCategoryTicketByInvoiceId(Integer invoiceId,String productType) throws Exception;
	
}
