package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.FantasyGrandChildCategory;

public interface FantasyGrandChildCategoryDAO extends RootDAO<Integer, FantasyGrandChildCategory>{

	public List<FantasyGrandChildCategory> getAll();
	public List<FantasyGrandChildCategory> getAllExceptNameAsCollege();
	public FantasyGrandChildCategory getGrandChildCategoryByName(String name);
	public FantasyGrandChildCategory getGrandChildCategoryById(Integer id);
	public List<FantasyGrandChildCategory> getGrandChildsByChildCategoryId(Integer childCategoryId);
	
}
