package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.ApplicationPlatform;
import com.rtw.tracker.datas.RTFPromotionalOfferTracking;
import com.rtw.tracker.enums.PromotionalType;


public interface RTFPromotionalOfferTrackingDAO extends RootDAO<Integer, RTFPromotionalOfferTracking>{

	public RTFPromotionalOfferTracking getPromoTracking(ApplicationPlatform platForm,Integer promoOfferId,Integer customerId, String sessionId,
			Integer ticketGroupId, Integer eventId,String promoCode,Boolean isLongTicket,PromotionalType offerType);
	
	public RTFPromotionalOfferTracking getPromoTracking(ApplicationPlatform platForm,Integer customerId, String sessionId,
			Integer ticketGroupId, Integer eventId);
	
	public RTFPromotionalOfferTracking getPendingPromoTracking(ApplicationPlatform platForm,Integer customerId, String sessionId,
			Integer ticketGroupId, Integer eventId);
	
	public List<RTFPromotionalOfferTracking> getAllPendingPromoTracking();
	
	public RTFPromotionalOfferTracking getCompletedPromoTracking(Integer promoOfferId);
	
	public RTFPromotionalOfferTracking getCompletedPromoTrackingByOrderId(Integer promoOfferOrderId);
	
	public RTFPromotionalOfferTracking getPromoTrackingByOrderId(Integer promoOfferOrderId);
}
