package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.EmailBlastTracking;

public interface EmailBlastTrackingDAO extends RootDAO<Integer, EmailBlastTracking>{

	public List<EmailBlastTracking> getTrackingByTemplateId(Integer templateId);
	
}
