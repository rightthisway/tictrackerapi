package com.rtw.tracker.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtw.tracker.datas.InventoryHold;
import com.rtw.tracker.utils.GridHeaderFilters;


public interface InventoryHoldDAO extends RootDAO<Integer, InventoryHold>{

	public List<InventoryHold> getAllByCustomerId(Integer customerId);
	public InventoryHold getAllByTicketId(Integer ticketId);
	public Collection<InventoryHold> getAllActiveHoldTickets();
	public List<InventoryHold> getInventoryHoldByTicketGroupId(Integer ticketGroupId);
	public List<InventoryHold> getAllHoldTickets(Integer brokerId, Integer eventId, GridHeaderFilters filter);
	public List<InventoryHold> getAllHoldTicketsWithPagination(Integer brokerId, Integer eventId, GridHeaderFilters filter, String pageNo);
	public Integer getAllHoldTicketsCount(Integer brokerId, Integer eventId, GridHeaderFilters filter);
	
}
