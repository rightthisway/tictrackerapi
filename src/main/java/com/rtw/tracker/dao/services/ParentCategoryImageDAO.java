package com.rtw.tracker.dao.services;

import com.rtw.tracker.datas.ParentCategoryImage;

public interface ParentCategoryImageDAO extends RootDAO<Integer, ParentCategoryImage>{
	
	public ParentCategoryImage getParentCategoryImageByParentCategoryId(Integer parentCategoryId) throws Exception;
}
