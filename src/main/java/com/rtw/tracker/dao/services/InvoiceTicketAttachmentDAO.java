package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.InvoiceTicketAttachment;
import com.rtw.tracker.enums.FileType;



public interface InvoiceTicketAttachmentDAO extends RootDAO<Integer, InvoiceTicketAttachment>{
	public List<InvoiceTicketAttachment> getTicketAttachmentByInvoiceId(Integer invoiceId);
	public List<InvoiceTicketAttachment> getAttachmentByInvoiceAndFileType(Integer invoiceId,FileType fileType);
	public InvoiceTicketAttachment getAttachmentByInvoiceFileTypeAndPosition(Integer invoiceId,FileType fileType,Integer posotion);
	public List<InvoiceTicketAttachment> getAttachmentByFileName(String fileName);
}
