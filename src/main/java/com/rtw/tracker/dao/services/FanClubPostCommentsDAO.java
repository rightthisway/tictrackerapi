package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.dao.services.RootDAO;
import com.rtw.tracker.datas.FanClubPost;
import com.rtw.tracker.datas.FanClubPostComments;
import com.rtw.tracker.datas.FanClubVideo;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface FanClubPostCommentsDAO extends RootDAO<Integer, FanClubPostComments>{

	public List<FanClubPostComments> getAllFanClubPostComments(GridHeaderFilters filter, String status,String commentType, Integer commentTypeId,SharedProperty prop);
}
