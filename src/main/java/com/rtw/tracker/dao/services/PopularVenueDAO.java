package com.rtw.tracker.dao.services;

import java.util.Collection;

import com.rtw.tracker.datas.PopularEvents;
import com.rtw.tracker.datas.PopularVenue;

public interface PopularVenueDAO extends RootDAO<Integer, PopularVenue> {

	public Collection<PopularVenue> getAllActivePopularVenueByProductId(Integer productId) throws Exception;
	public Integer getAllActivePopularVenueCountByProductId(Integer productId) throws Exception;
}
