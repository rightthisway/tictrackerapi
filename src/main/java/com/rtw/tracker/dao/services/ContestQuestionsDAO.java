package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.datas.ContestQuestions;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface ContestQuestionsDAO extends RootDAO<Integer, ContestQuestions>{

	public List<ContestQuestions> getContestQuestions(GridHeaderFilters filter,Integer questionId,Integer contestId,SharedProperty sharedProperty);
	public ContestQuestions getContestQuestionsByNo(Integer contestId,Integer questionNo);
	public Integer getQuestionCount(Integer contestId);
	public ContestQuestions getContestQuestionByQuestion(Integer contestId,String question);
	public List<ContestQuestions> getContestQuestionstoReposition(Integer actualPosition,Integer contestId);
	public List<ContestQuestions> getMiniJackpotQuestionsByContestId(Integer contestId);
	
}
