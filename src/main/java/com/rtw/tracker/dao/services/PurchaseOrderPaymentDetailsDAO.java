package com.rtw.tracker.dao.services;

import com.rtw.tracker.datas.PurchaseOrderPaymentDetails;

public interface PurchaseOrderPaymentDetailsDAO extends RootDAO<Integer, PurchaseOrderPaymentDetails>{

	public PurchaseOrderPaymentDetails getPaymentDetailsByPOId(Integer poId)throws Exception;
}
