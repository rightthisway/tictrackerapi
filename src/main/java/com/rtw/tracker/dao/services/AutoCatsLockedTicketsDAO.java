package com.rtw.tracker.dao.services;

import com.rtw.tracker.datas.AutoCatsLockedTickets;

public interface AutoCatsLockedTicketsDAO extends RootDAO<Integer, AutoCatsLockedTickets>{

	public AutoCatsLockedTickets getLockedTicketsByEventId(Integer eventId);
	public AutoCatsLockedTickets getLockedTicketsByCategoryTicketGroupId(Integer catgeoryTicketGroupId);
	
}
