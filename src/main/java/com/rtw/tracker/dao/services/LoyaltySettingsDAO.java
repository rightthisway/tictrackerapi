package com.rtw.tracker.dao.services;

import com.rtw.tracker.datas.LoyaltySettings;
/**
 * interface having db related methods for LoyaltySettings
 * @author Ulaganathan
 *
 */
public interface LoyaltySettingsDAO extends RootDAO<Integer, LoyaltySettings>{
	
	
	public LoyaltySettings getActivetLoyaltySettings();
	
	
}
