package com.rtw.tracker.dao.services;

import com.rtw.tracker.datas.ShowNearTermOption;

public interface ShowNearTermOptionDAO extends RootDAO<Integer, ShowNearTermOption> {

}
