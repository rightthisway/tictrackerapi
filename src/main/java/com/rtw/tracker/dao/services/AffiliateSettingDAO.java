package com.rtw.tracker.dao.services;

import com.rtw.tracker.datas.AffiliateSetting;

public interface AffiliateSettingDAO extends RootDAO<Integer, AffiliateSetting>{
	
	public AffiliateSetting getSettingByUser(Integer userId);
}
