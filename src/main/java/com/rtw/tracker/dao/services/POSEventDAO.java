package com.rtw.tracker.dao.services;

import com.rtw.tracker.datas.POSEvent;

public interface POSEventDAO extends RootDAO<Integer, POSEvent>{

	public POSEvent getPOSEventByEventID(Integer eventID) throws Exception;
}
