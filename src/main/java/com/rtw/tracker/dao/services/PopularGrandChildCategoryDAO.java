package com.rtw.tracker.dao.services;

import java.util.Collection;

import com.rtw.tracker.datas.PopularGrandChildCategory;

public interface PopularGrandChildCategoryDAO extends RootDAO<Integer, PopularGrandChildCategory> {

	public Collection<PopularGrandChildCategory> getAllActivePopularGrandChildCategoryByProductId(Integer productId) throws Exception;
	public Integer getAllActivePopularGrandChildCategoryCountByProductId(Integer productId) throws Exception;
}
