package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.datas.PollingCategoryQuestion;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface PollingCategoryQuestionDAO extends RootDAO<Integer, PollingCategoryQuestion>{

	public List<PollingCategoryQuestion> getPollingCategoryQuestion(GridHeaderFilters filter,Integer questionId,Integer contestId,SharedProperty sharedProperty);
	/*public PollingCategoryQuestion getPollingCategoryQuestionByNo(Integer contestId,Integer questionNo);*/
	public Integer getQuestionCount(Integer contestId);
	public PollingCategoryQuestion getContestQuestionByQuestion(Integer contestId,String question);
	/*public List<PollingCategoryQuestion> getPollingCategoryQuestiontoReposition(Integer actualPosition,Integer contestId);*/
	
}
