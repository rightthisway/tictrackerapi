package com.rtw.tracker.dao.services;

import java.util.Collection;

import com.rtw.tracker.datas.AffiliateCashReward;
import com.rtw.tracker.utils.GridHeaderFilters;

/**
 * interface having db related methods for AffiliateCashReward
 * @author Ulaganathan
 *
 */
public interface AffiliateCashRewardDAO extends RootDAO<Integer, AffiliateCashReward>{
	
	public AffiliateCashReward getAffiliateByUserId(Integer userId);
	public Collection<AffiliateCashReward> getAllAffiliates(GridHeaderFilters filter);
	public Integer getAllAffiliatesCount(GridHeaderFilters filter);
	
}
