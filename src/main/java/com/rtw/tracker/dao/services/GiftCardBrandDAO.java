package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.GiftCardBrand;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface GiftCardBrandDAO  extends RootDAO<Integer, GiftCardBrand>{
	
	public List<GiftCardBrand> getAllGiftCardBrands(GridHeaderFilters filter,String gbStatus) ;

}
