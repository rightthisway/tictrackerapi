package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.OrderTicketGroupDetails;

public interface OrderTicketGroupDetailsDAO extends RootDAO<Integer,OrderTicketGroupDetails>{

	public List<OrderTicketGroupDetails> getOrderTicketGroupDetailsByOrderId(Integer orderId);
}
