package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.PollingContest;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface PollingContestDAO extends RootDAO<Integer, PollingContest>{

	public List<PollingContest> getAllPollingContest(Integer id, GridHeaderFilters filter, String status);
		
	public PollingContest getPollingContestById(Integer id) throws Exception;
	
	public PollingContest savePollingContest(PollingContest pollingSponsor) throws Exception;

	public boolean CheckActivePollingContest();

	public Integer getQuestCountForPolling(Integer pollId);

	public Integer getCategoryCountForPolling(Integer pollId);
	
	public List<PollingContest> getActivePollingContestByCategoryId(Integer categoryId) throws Exception;
}