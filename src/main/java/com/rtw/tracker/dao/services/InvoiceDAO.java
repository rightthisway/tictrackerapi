package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.Invoice;
import com.rtw.tracker.fedex.Recipient;

public interface InvoiceDAO extends RootDAO<Integer, Invoice>{

	//public int updateRealTixStatus(int invoiceId,String shippingMethod,Boolean isRealTixDelivered) throws Exception;

	int updateTixUploaded(int invoiceId) throws Exception;

	int updateBarCode(String barCode, int invoiceId) throws Exception;
	
	public List<Recipient> getRecipientListByInvoice(Integer invoiceId,
			boolean recreateLabel);
	public List<Integer> getInvoiceTocreateFedexLabel();
	public List<Invoice> getInvoiceByOrderIds(List<Integer> ids)throws Exception;
	public List<Invoice> getFedexUndeliveredInvoices()throws Exception;
	public Invoice getInvoiceByOrderId(Integer orderId)throws Exception;
	public List<Invoice> getInvoiceByTicketUpload()throws Exception;
	public Invoice getInvoiceByIdAndBrokerId(Integer id, Integer brokerId)throws Exception;
}
