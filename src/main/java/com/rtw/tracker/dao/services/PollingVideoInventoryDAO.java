package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.PollingVideoInventory;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface PollingVideoInventoryDAO extends RootDAO<Integer, PollingVideoInventory>{

	public List<PollingVideoInventory> getAllPollingVideoINventories(GridHeaderFilters filter, String status);
	public PollingVideoInventory getVideoByUrl(String url);
	public boolean deleteRtfVideos(String ids);
}
