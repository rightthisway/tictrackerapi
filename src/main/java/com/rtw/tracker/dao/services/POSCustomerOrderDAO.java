package com.rtw.tracker.dao.services;

import com.rtw.tracker.datas.POSCustomerOrder;

public interface POSCustomerOrderDAO extends RootDAO<Integer, POSCustomerOrder>{

	public POSCustomerOrder getPOSCustomerOrderByTicketRequestId(Integer ticketRequestId);
}
