package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.SuperFanLevels;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface SuperFanLevelsDAO extends RootDAO<Integer, SuperFanLevels>{

	public List<SuperFanLevels> getAllActiveSuperFanLevels(GridHeaderFilters filter, String status);
	public SuperFanLevels getSuperFanLevelByLeveNo(Integer levelNo);
}
