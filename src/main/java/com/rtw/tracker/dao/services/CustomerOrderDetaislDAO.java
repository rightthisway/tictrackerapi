package com.rtw.tracker.dao.services;

import com.rtw.tracker.dao.services.RootDAO;
import com.rtw.tracker.datas.CustomerOrderDetails;

public interface CustomerOrderDetaislDAO extends RootDAO<Integer, CustomerOrderDetails>{

	public CustomerOrderDetails getCustomerOrderDetailsByOrderId(Integer orderId);
}
