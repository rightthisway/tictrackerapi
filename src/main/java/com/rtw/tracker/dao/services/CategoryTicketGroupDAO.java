package com.rtw.tracker.dao.services;

import java.util.Collection;

import com.rtw.tracker.datas.CategoryTicketGroup;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface CategoryTicketGroupDAO extends RootDAO<Integer, CategoryTicketGroup> {

	public Collection<CategoryTicketGroup> getAllActiveCategoryTicketsbyEventId(Integer brokerId, Integer eventId, GridHeaderFilters filter, String pageNo) throws Exception;
	
	public CategoryTicketGroup getCategoryTicketByEventId(Integer eventId) throws Exception;
	
	public void updateCategoryTicket(int ticketId, int invoiceId,Double soldPrice) throws Exception;
	public void updateCategoryTicketGroupFields(Integer ticketId,String editColumnType,String value) throws Exception;
	public CategoryTicketGroup getActiveCategoryTicketById(Integer ticketId) throws Exception;
	public Collection<CategoryTicketGroup> getSoldTicketsByEventId(Integer eventId);
	public Collection<CategoryTicketGroup> getSoldTicketsSearchFilterByEventId(Integer eventId, Integer brokerId, GridHeaderFilters filter);
	public CategoryTicketGroup getCategoryTicketByInvoiceId(Integer invoiceId);
	public void updateBroadcastByEventId(Integer eventId, Boolean broadCast);
	public CategoryTicketGroup getDuplicateCategoryTicketGroupForBroker(String zone,Integer qty,Integer eventId,Integer brokerId);
	public CategoryTicketGroup getCategoryTicketByIdAndBrokerId(Integer id, Integer brokerId);
}
