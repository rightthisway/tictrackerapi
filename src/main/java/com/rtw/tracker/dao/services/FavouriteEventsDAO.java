package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.FavouriteEvents;


public interface FavouriteEventsDAO extends RootDAO<Integer, FavouriteEvents>{

	public FavouriteEvents getFavouriteEventsById(Integer eventId);	
	public FavouriteEvents getFavouriteEventsByEventAndCustomerId(Integer eventId,Integer customerId);		
	public List<FavouriteEvents> getAllActiveFavouriteEventsByCustomerId(Integer customerId);	
	public List<FavouriteEvents> getAllActiveFavouriteEvents();	
	public List<FavouriteEvents> getAllUnFavouritedEventsByCustomerId(Integer customerId);
	
}
