package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.dao.services.RootDAO;
import com.rtw.tracker.datas.CustomerTicketDownloads;

public interface CustomerTicketDownloadsDAO extends RootDAO<Integer, CustomerTicketDownloads> {
	
	public CustomerTicketDownloads getCustomerTicketDownloadsById(Integer id);
	public List<CustomerTicketDownloads> getCustomerTicketDownloadsByInvoiceId(Integer invoiceId);
	public List<CustomerTicketDownloads> getCustomerTicketDownloadsByCustomerId(Integer customerId);

}
