package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.InvoiceRefund;

public interface InvoiceRefundDAO extends RootDAO<Integer, InvoiceRefund>{

	public InvoiceRefund getInvoiceRefundById(Integer id);
	public List<InvoiceRefund> getAllStripeRefunds();
	public List<InvoiceRefund> getAllPaypalRefunds();
	public List<InvoiceRefund> getInvoiceRefundByOrderId(Integer orderId);
	public InvoiceRefund getStripeRefundByIdandTrasactionId(String refundId,String transactionId);
	public InvoiceRefund getPaypalRefundByIdandTrasactionId(String refundId,String transactionId);
}
