package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.GrandChildCategory;
/**
 * interface having db related methods for GrandChildCategory
 * @author hamin
 *
 */
public interface GrandChildCategoryDAO extends RootDAO<Integer, GrandChildCategory>{
	
	/**
	 *  method to get all GrandChildCategories 
	 * @return List of GrandChildCategories
	 */
	public List<GrandChildCategory> getAll();
	/**
	 * method to get  GrandChildCategoriy by name 
	 * @param name , Grand Child Category name
	 * @return GrandChildCategory
	 */
	public GrandChildCategory getGrandChildCategoryByName(
			String name);
	/**
	 * method to get  GrandChildCategoriy by id 
	 * @param id , GrandChildCategory id
	 * @return GrandChildCategory
	 */
	public GrandChildCategory getGrandChildCategoryById(
			Integer id);	
	/**
	 * method to get  GrandChildCategoriy by id 
	 * @param childCategoryId , childCategory id
	 * @return GrandChildCategory
	 */
	public List<GrandChildCategory> getGrandChildsByChildCategoryId(Integer childCategoryId);
	public List<GrandChildCategory> getGrandChildsByDisplayOnSearch();
	public List<GrandChildCategory> getGrandChildCategoriesByNameAndDisplayOnSearch(String name);
	public List<GrandChildCategory> getAllGrandChildCategoryByChildCategory(Integer childCategoryId)  throws Exception;
	public List<GrandChildCategory> getAllGrandChildCategoryByParentCategory(Integer parentCategoryId)  throws Exception;
	public List<GrandChildCategory> getAllGrandChildCategoriesByNameAndDisplayOnSearch(String name) ;
}
