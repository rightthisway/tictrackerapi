package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.Ticket;


public interface TicketDAO extends RootDAO<Integer, Ticket> {

	public List<Ticket> getAvailableTicketsByTicketGroupId(Integer ticketGroupId) throws Exception;
	public List<Ticket> getAllTicketsByTicketGroupId(Integer ticketGroupId) throws Exception;
	public List<Ticket> getMappedTicketsByInvoiceId(Integer invoiceId) throws Exception;
	public List<Ticket> getMappedTicketByPOId(Integer poId) throws Exception;
	public List<Ticket> getTIcketsByPOId(Integer poId) throws Exception;
	public List<Ticket> getTicketsByTicketGroupIds(List<Integer> groupIds) throws Exception;
	public List<Ticket> getTicketsByTicketIds(List<Integer> ids)throws Exception;
	public List<Ticket> getTicketsByTicketIdsAndStatus(List<Integer> ids)throws Exception;
	public void getAllTicketsToDeleteByTicketGroupId(Integer ticketGroupId)throws Exception;
	public List<Ticket> getMappedTicketsByInvoiceIdandTicketGroupId(Integer invoiceId,Integer ticketGroupId) throws Exception;
}
