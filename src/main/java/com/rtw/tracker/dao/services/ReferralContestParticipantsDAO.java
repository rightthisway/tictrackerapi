package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.ReferralContestParticipants;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface ReferralContestParticipantsDAO extends RootDAO<Integer, ReferralContestParticipants>{

	public List<ReferralContestParticipants> getParticipantsByContestId(Integer contestId)throws Exception;	
	public List<ReferralContestParticipants> getParticipantsByCustomerId(Integer customerId)throws Exception;
	public List<ReferralContestParticipants> getParticipantsListByContestId(Integer contestId, GridHeaderFilters filter)throws Exception;
	
}
