package com.rtw.tracker.dao.services;

import java.util.Collection;

import com.rtw.tracker.datas.PopularEvents;

public interface PopularEventsDAO extends RootDAO<Integer, PopularEvents> {

	public Collection<PopularEvents> getAllActivePopularEventsByProductId(Integer productId) throws Exception;
	public Integer getAllActivePopularEventsCountByProductId(Integer productId) throws Exception;
}
