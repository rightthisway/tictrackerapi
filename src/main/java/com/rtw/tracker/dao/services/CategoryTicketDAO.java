package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.CategoryTicket;

public interface CategoryTicketDAO extends RootDAO<Integer, CategoryTicket>{
	public List<CategoryTicket> getUnmappedCategoryTicketByInvoiceId(Integer invoiceId) throws Exception;
	public List<CategoryTicket> getMappedCategoryTicketByInvoiceId(Integer invoiceId) throws Exception;
	public List<CategoryTicket> getCategoryTicketByCategoryTicketGroupId(Integer ticketGroupId) throws Exception;
}
