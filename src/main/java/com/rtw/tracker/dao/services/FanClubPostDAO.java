package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.dao.services.RootDAO;
import com.rtw.tracker.datas.FanClubPost;
import com.rtw.tracker.datas.FanClubVideo;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface FanClubPostDAO extends RootDAO<Integer, FanClubPost>{

	public List<FanClubPost> getAllFanClubPosts(GridHeaderFilters filter,String status,Integer fanClubId,SharedProperty pro);
}
