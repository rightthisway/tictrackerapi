package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.GiftCardOrderAttachment;

public interface GiftCardOrderAttachmentDAO extends RootDAO<Integer, GiftCardOrderAttachment>{

	public List<GiftCardOrderAttachment> getOrderEGiftCardAttachments(Integer orderId); 
	public List<GiftCardOrderAttachment> getOrderBarcodeAttachments(Integer orderId);
	public List<GiftCardOrderAttachment> getOrderAllAttachments(Integer orderId);
	public GiftCardOrderAttachment getOrderAttachmentByTypeAndPosition(Integer orderId,String fileType,Integer postion);
	public List<GiftCardOrderAttachment> getUnProcessedAttachments();
}
