package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.CustomerContestQuestion;
import com.rtw.tracker.utils.GridHeaderFilters;



public interface CustomerContestQuestionDAO extends RootDAO<Integer, CustomerContestQuestion>{

	public List<CustomerContestQuestion> getAllCustomerQuestion(GridHeaderFilters filter, String status);
}
