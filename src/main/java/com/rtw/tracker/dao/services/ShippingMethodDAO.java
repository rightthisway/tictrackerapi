package com.rtw.tracker.dao.services;

import com.rtw.tracker.datas.ShippingMethod;

public interface ShippingMethodDAO extends RootDAO<Integer, ShippingMethod> {

	public ShippingMethod getShippingMethodById(Integer id);
	public ShippingMethod getShippingMethodByName(String name);
}
