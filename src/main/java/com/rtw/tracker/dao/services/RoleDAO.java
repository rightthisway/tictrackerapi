package com.rtw.tracker.dao.services;

import java.util.Collection;

import com.rtw.tracker.datas.Role;

public interface RoleDAO extends RootDAO<Integer, Role> {
	
	public Role getRoleByName(String roleName);
}
