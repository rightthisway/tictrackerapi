package com.rtw.tracker.dao.services;

import com.rtw.tracker.datas.ArtistImage;

public interface ArtistImageDAO extends RootDAO<Integer, ArtistImage>{
	
	public ArtistImage getArtistImageByArtistId(Integer artistId) throws Exception;
	public void updateArtistImageUrl(ArtistImage artistImage) throws Exception;
}
