package com.rtw.tracker.dao.services;

import com.rtw.tracker.datas.Product;

public interface ProductDAO extends RootDAO<Integer, Product> {

	public Product getProductByName(String productName) throws Exception;
}
