package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.TicketGroupTicketAttachment;
import com.rtw.tracker.enums.FileType;

public interface TicketGroupTicketAttachmentDAO extends RootDAO<Integer, TicketGroupTicketAttachment>{
	public TicketGroupTicketAttachment getTicketByTicketGroupId(Integer ticketGroupId);
	public List<TicketGroupTicketAttachment> getTicketAttachmentByTicketGroupId(Integer ticketGroupId);
	public List<TicketGroupTicketAttachment> getAttachmentByTicketGroupAndFileType(Integer ticketGroupId,FileType fileType);
	public TicketGroupTicketAttachment getAttachmentByTicketGroupFileTypeAndPosition(Integer ticketGroupId,FileType fileType,Integer posotion);
}
