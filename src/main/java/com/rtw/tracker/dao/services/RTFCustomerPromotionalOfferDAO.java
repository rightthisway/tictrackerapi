package com.rtw.tracker.dao.services;

import com.rtw.tracker.datas.RTFCustomerPromotionalOffer;

public interface RTFCustomerPromotionalOfferDAO extends RootDAO<Integer, RTFCustomerPromotionalOffer>{

	public RTFCustomerPromotionalOffer getPromoOfferCustomerId(Integer customerId);
	public void updatePromotionaOfferOrderCount(Integer offerId);
}
