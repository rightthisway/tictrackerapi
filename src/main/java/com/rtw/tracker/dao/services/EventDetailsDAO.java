package com.rtw.tracker.dao.services;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.rtw.tracker.datas.EventDetails;
import com.rtw.tracker.datas.ProductType;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface EventDetailsDAO extends RootDAO<Integer, EventDetails> {

	public Collection<EventDetails> getAllActiveEvents() throws Exception;
	public Collection<EventDetails> getAllActiveEventsByFilter(String searchColumn,String searchValue,Date fromDate,Date toDate,ProductType productType) throws Exception;
	public Collection<EventDetails> getAllActiveArtistsByFilter(GridHeaderFilters filter,String pageNo) throws Exception;
	public Collection<EventDetails> getAllActiveGrandChildCategoryByFilter(String searchColumn,String searchValue) throws Exception;
	//public Collection<EventDetails> getAllActiveVenueByFilter(String searchColumn,String searchValue) throws Exception;
	public Collection<EventDetails> filterByName(String eventName) throws Exception;
	public EventDetails getEventById(Integer evnetId) throws Exception;
	public List<String> getAllCityByArtistId(Integer artistId)throws Exception;
	//public List<EventDetails> getEventsByArtistId(Integer artistId)throws Exception;
	public List<EventDetails> getEventsByArtistCity(Integer artistId,String city)throws Exception;
	//public Collection<EventDetails> getEventsByArtistId(Integer id);
	public Collection<EventDetails> getEventsByVenueId(Integer id);
	public Integer getActiveEventIdByEventDateTimeVenue(String eventName,String venueName,String eventDate,String eventTime);
	public Collection<EventDetails> getAllActiveArtistsByFilterToExport(GridHeaderFilters filter) throws Exception;
	
}
