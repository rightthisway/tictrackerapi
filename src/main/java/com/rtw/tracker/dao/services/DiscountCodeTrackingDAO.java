package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.ApplicationPlatform;
import com.rtw.tracker.datas.DiscountCodeTracking;


public interface DiscountCodeTrackingDAO extends RootDAO<Integer, DiscountCodeTracking>{

	public DiscountCodeTracking getTrackingByCustomerIdByTicketId(Integer cId, Integer eId,Integer tId,String sessionId,ApplicationPlatform platForm);
	public List<DiscountCodeTracking> getTrackingByCustomerId(Integer customerId);
	
}
