package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.POSTicket;

public interface POSTicketDAO extends RootDAO<Integer, POSTicket>{
	
	public POSTicket getPOSTicketByTicketId(Integer ticketId) throws Exception;
	public POSTicket getPOSTicketByInvoiceId(Integer invoiceId) throws Exception;
	public List<POSTicket> getPOSTicketByPurchaseOrderId(Integer purchaseOrderId, String productType) throws Exception;
	public List<POSTicket> getMappedPOSTicketByPurchaseOrderId(Integer purchaseOrderId, String productType) throws Exception;
	public List<POSTicket> getMappedPOSTicketByInvoiceId(Integer invoiceId) throws Exception;
	public List<POSTicket> getMappedPOSTicketByInvoiceIdWithPdt(Integer invoiceId, String productType) throws Exception;
}

