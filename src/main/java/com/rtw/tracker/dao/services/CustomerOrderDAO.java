package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.CustomerOrder;

public interface CustomerOrderDAO extends RootDAO<Integer, CustomerOrder>{

	List<CustomerOrder> getCustomerOrderByIds(List<Integer> ids)throws Exception;
	public CustomerOrder getCustomerOrderBySeatGeekOrderId(String seatGeekOrderId)throws Exception;
	public CustomerOrder getCustomerOrderByTransactionId(String transactionId)throws Exception;
	public CustomerOrder getCustomerOrderByIdAndBrokerId(Integer id, Integer brokerId)throws Exception;
	
}
