package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.FantasyParentCategory;

public interface FantasyParentCategoryDAO extends RootDAO<Integer, FantasyParentCategory>{

	public List<FantasyParentCategory> getAll();
	public FantasyParentCategory getParentCategoryByName(String name);
	public FantasyParentCategory getParentCategoryById(Integer id);	
	public List<String> getDistinctParentCategoryNames();
	public List<FantasyParentCategory> getParentCategoriesByName(String name);
}
