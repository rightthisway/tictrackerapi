package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.CrownJewelLeagues;

public interface CrownJewelLeaguesDAO extends RootDAO<Integer, CrownJewelLeagues> {

	public List<CrownJewelLeagues> getAllActiveLeagues() throws Exception;
	public List<CrownJewelLeagues> getActiveLeaguesByGrandChildId(Integer grandChildId) throws Exception;
	
}
