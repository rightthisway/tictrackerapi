package com.rtw.tracker.dao.services;

import com.rtw.tracker.datas.CustomerWallet;

public interface CustomerWalletDAO extends RootDAO<Integer, CustomerWallet>{

	public CustomerWallet getCustomerWalletByCustomerId(Integer customerId);
}
