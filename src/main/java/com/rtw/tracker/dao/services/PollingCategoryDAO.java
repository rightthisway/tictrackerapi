package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.PollingCategory;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface PollingCategoryDAO extends RootDAO<Integer, PollingCategory>{

	public List<PollingCategory> getAllPollingCategory(Integer id, GridHeaderFilters filter, String status);
		
	public PollingCategory getPollingCategoryById(Integer id) throws Exception;
	
	public PollingCategory savePollingCategory(PollingCategory pollingSponsor) throws Exception;
	
}