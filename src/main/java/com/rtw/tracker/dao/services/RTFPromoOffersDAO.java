package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.RTFPromoOffers;

public interface RTFPromoOffersDAO extends RootDAO<Integer, RTFPromoOffers>{

	public RTFPromoOffers getPromoCode(String promoCode);
	public List<RTFPromoOffers> getAllActiveOffers();
}
