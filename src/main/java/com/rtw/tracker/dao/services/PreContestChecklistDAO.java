package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.datas.PreContestChecklist;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface PreContestChecklistDAO extends RootDAO<Integer, PreContestChecklist>{

	public PreContestChecklist getPreContestChecklistByContestId(Integer contestId);
	public  List<PreContestChecklist> getAllActivePreContestChecklist(Integer contestId);
	public List<PreContestChecklist> getAllActivePreContestChecklistByType(Integer contestId,String type) ;
	public List<PreContestChecklist> getPreContestChecklistbyFilters(String status,GridHeaderFilters filter,SharedProperty property,String type);
}
