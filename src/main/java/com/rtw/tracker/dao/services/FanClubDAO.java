package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.dao.services.RootDAO;
import com.rtw.tracker.datas.FanClub;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface FanClubDAO extends RootDAO<Integer, FanClub>{

	public List<FanClub> getAllFabClubs(GridHeaderFilters filter, String status,SharedProperty prop);

	public List<FanClub> getPopularFanClubList(GridHeaderFilters filter, SharedProperty prop);
}
