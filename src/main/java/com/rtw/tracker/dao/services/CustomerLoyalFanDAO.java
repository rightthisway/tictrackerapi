package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.CustomerLoyalFan;

 

public interface CustomerLoyalFanDAO extends RootDAO<Integer, CustomerLoyalFan>{
	
	public List<CustomerLoyalFan> getAll();
	
	public List<CustomerLoyalFan> getAllLoyalFansByCustomerId(Integer customerId);
	
	public CustomerLoyalFan getLoyalFansByCustomerIdByArtistId(Integer customerId,Integer artistId);
	public CustomerLoyalFan getActiveLoyalFanByCustomerId(Integer customerId);
}
