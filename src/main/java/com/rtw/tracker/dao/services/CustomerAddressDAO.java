package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.CustomerAddress;

public interface CustomerAddressDAO extends RootDAO<Integer, CustomerAddress>{

	public CustomerAddress getCustomerAddressInfo(Integer customerId);
	
	public List<CustomerAddress> getShippingAddressInfo(Integer customerId);
	
	public Integer getShippingAddressCount(Integer customerId);

	List<CustomerAddress> updatedShippingAddress(Integer customerId);
	
	public List<CustomerAddress> getShippingAndOtherAddresInfo(Integer customerId);
	
	public CustomerAddress getCustomerShippingAddresByAddresId(Integer shipAddrId);
}
