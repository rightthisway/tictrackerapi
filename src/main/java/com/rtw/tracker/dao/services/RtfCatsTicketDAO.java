package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tmat.data.RewardthefanCategoryTicket;
import com.rtw.tracker.datas.RtfCatsTicket;

public interface RtfCatsTicketDAO extends RootDAO<Integer, RtfCatsTicket>{

	public void deleteAllActiveRtfCatsListingsByEventId(Integer eventId);
	public Integer updateRtfCatsTicketByrtfCatsId(RewardthefanCategoryTicket rewardCatTicket)  throws Exception;
	public Integer deleteRtfCatsTicketByrtfCats(List<RewardthefanCategoryTicket> catTixList) throws Exception;
	
}
