package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.TicketMasterRefundCCNotMatched;

public interface TicketMasterRefundCCNotMatchedDAO extends RootDAO<Integer, TicketMasterRefundCCNotMatched>{

	public List<TicketMasterRefundCCNotMatched> getAllUnMatchedRefundsByUser(String userName);
}
