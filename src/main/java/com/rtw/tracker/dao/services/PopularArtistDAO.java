package com.rtw.tracker.dao.services;

import com.rtw.tracker.datas.PopularArtist;

public interface PopularArtistDAO extends RootDAO<Integer, PopularArtist> {

	//public Collection<PopularArtist> getAllActivePopularArtistByProductId(Integer productId) throws Exception;
	public Integer getAllActivePopularArtistCountByProductId(Integer productId) throws Exception;
}
