package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.dao.services.RootDAO;
import com.rtw.tracker.datas.FanClubVideo;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface FanClubVideoDAO extends RootDAO<Integer, FanClubVideo>{

	public List<FanClubVideo> getAllFanClubVideos(GridHeaderFilters filter, String status,Integer fanClubId,SharedProperty prop);
}
