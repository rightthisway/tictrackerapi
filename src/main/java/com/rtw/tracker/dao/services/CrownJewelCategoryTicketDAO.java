package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.CrownJewelCategoryTicket;


public interface CrownJewelCategoryTicketDAO extends RootDAO<Integer, CrownJewelCategoryTicket> {
	
	public List<CrownJewelCategoryTicket> getAllActiveCrownjewelTicketsByEventId(Integer eventId) throws Exception;
	public void updateCrownJewelCategoryTicketstoDeletedByEventId(Integer eventId) throws Exception;
	public List<CrownJewelCategoryTicket> getAllCrownjewelTicketsByEventId(Integer eventId) throws Exception;

}
