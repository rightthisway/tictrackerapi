package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.ContestEventRequest;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface ContestEventRequestDAO extends RootDAO<Integer, ContestEventRequest>{

	public List<ContestEventRequest> getContestEventRequests(GridHeaderFilters filter,Integer contestRequestId);
	public Integer getContestRequestCount(Integer contestRequestId);
	
}
