package com.rtw.tracker.dao.services;

import com.rtw.tracker.datas.PopularGrandChildCategoryAudit;

public interface GrandChildCategoryAuditDAO extends RootDAO<Integer, PopularGrandChildCategoryAudit>{

}
