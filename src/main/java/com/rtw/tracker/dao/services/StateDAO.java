package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.State;

public interface StateDAO extends RootDAO<Integer, State>{
	/**
	 *  method to get State by country id
	 * @param countryId, Id of a country
	 * @return list of states
	having db related methods */
	public List<State> getAllStateByCountryId(Integer countryId);
	/**
	 *  method to get State by  id
	 * @param id, own id
	 * @return State
	 */
	public State getStateById(Integer id);
	
	public Integer getStateByName(String name);
	public List<State> getAllStateByCountryIds(String countryId);
	public List<State> getAllStateForUSA();
	public State getStateByShortDesc(String shortDesc, Integer countryId);
}