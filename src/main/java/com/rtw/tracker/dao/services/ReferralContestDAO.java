package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.ReferralContest;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface ReferralContestDAO extends RootDAO<Integer, ReferralContest>{
	
	public List<ReferralContest> getAllContests(Integer id, GridHeaderFilters filter);
	public List<ReferralContest> getAllActiveContests();
	
}
