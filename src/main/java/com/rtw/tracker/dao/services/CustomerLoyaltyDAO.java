package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.CustomerLoyalty;
/**
 * interface having db related methods for CustomerFavouriteEvent
 * @author Ulaganathan
 *
 */
public interface CustomerLoyaltyDAO extends RootDAO<Integer, CustomerLoyalty>{
	
	public CustomerLoyalty getCustomerLoyaltyByCustomerId(Integer customerId);
	
	/*public void deleteFavouriteEventByEventIdAndCustomerId(Integer eventId,Integer customerId);
	public List<CustomerLoyalty> getAllActiveFavouriteEventsByCustomerId(Integer customerId);*/
	
	void updateCustomerLoyalty(Integer pointsRemaining, Integer latestSpendPoints, Integer totalSpendPoints, Integer customerId);
	public List<CustomerLoyalty> getAllCustomerLoyaltyWhichHasMorethanZeroActivePoints();
	public void updateCustomerLoyaltyByCustomer(Integer earnPoints,  Integer customerId);
}
