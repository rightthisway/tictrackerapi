package com.rtw.tracker.dao.services;

import com.rtw.tracker.datas.POSInvoice;

public interface POSInvoiceDAO extends RootDAO<Integer, POSInvoice>{

	public POSInvoice getInvoiceByProductTypeAndId(Integer invoiceId,String productType);
}
