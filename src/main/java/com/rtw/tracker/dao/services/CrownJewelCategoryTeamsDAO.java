package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.CrownJewelCategoryTeams;

public interface CrownJewelCategoryTeamsDAO extends RootDAO<Integer, CrownJewelCategoryTeams>{

	public List<CrownJewelCategoryTeams> getCategoryTeamsByGrandChild(Integer grandChildId);
	public List<CrownJewelCategoryTeams> getCategoryTeamsByGrandChildandName(Integer grandChildId,String name);
	public List<CrownJewelCategoryTeams> getCategoryTeamsByName(String name);
}
