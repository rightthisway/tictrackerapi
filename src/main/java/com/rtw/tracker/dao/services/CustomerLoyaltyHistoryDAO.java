package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.CustomerLoyaltyHistory;
import com.rtw.tracker.datas.RewardDetails;
import com.rtw.tracker.enums.RewardStatus;
import com.rtw.tracker.utils.GridHeaderFilters;

/**
 * interface having db related methods for CustomerFavouriteEvent
 * @author Ulaganathan
 *
 */
public interface CustomerLoyaltyHistoryDAO extends RootDAO<Integer, CustomerLoyaltyHistory>{
	
	public CustomerLoyaltyHistory getCustomerLoyaltyByCustomerId(Integer customerId);
	public CustomerLoyaltyHistory getCustomerLoyaltyByOrderId(Integer orderId);
	public CustomerLoyaltyHistory getCustomerLoyaltyByOrderIdAndStatus(Integer orderId);
	public List<RewardDetails> getCustomerRewardsByRewardStatus(RewardStatus rewardStatus , Integer customerId, GridHeaderFilters filter);
	public Integer getCustomerRewardsByRewardStatusCount(RewardStatus rewardStatus , Integer customerId, GridHeaderFilters filter);
	public List<RewardDetails> getCustomerRedeemedRewardsByCustomerId(Integer customerId, GridHeaderFilters filter);
	public Integer getCustomerRedeemedRewardsByCustomerIdCount(Integer customerId, GridHeaderFilters filter);
	public CustomerLoyaltyHistory getActiveCustomerLoyaltyHistoryOfCrownJewelOrder(Integer orderId,Integer customerId);
	public CustomerLoyaltyHistory getVoidRefundCustomerLoyaltyHistoryOfCrownJewelOrder(Integer orderId,Integer customerId);
	public CustomerLoyaltyHistory getActiveRefundCustomerLoyaltyHistoryOfCrownJewelOrder(Integer orderId,Integer customerId);
	public CustomerLoyaltyHistory getVoidCustomerLoyaltyHistoryOfCrownJewelOrder(Integer orderId,Integer customerId);
	public CustomerLoyaltyHistory getCustomerLoyaltyHistoryByOrderIdAndCustomerId(Integer orderId,Integer customerId);
	public CustomerLoyaltyHistory getCustomerGiftCardOrderLoyaltyHistoryByOrderIdAndCustomerId(Integer orderId,Integer customerId) ;
	
	
}
