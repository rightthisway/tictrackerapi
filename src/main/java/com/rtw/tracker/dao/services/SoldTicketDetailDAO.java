package com.rtw.tracker.dao.services;

import java.util.Date;
import java.util.List;

import com.rtw.tmat.enums.ProfitLossSign;
import com.rtw.tracker.datas.ProductType;
import com.rtw.tracker.datas.SoldTicketDetail;


public interface SoldTicketDetailDAO extends RootDAO<Integer, SoldTicketDetail> {
	public List<SoldTicketDetail> getAllSoldUnfilledTicketDetails();
	public List<SoldTicketDetail> getSoldTicketDetailsByEventId(Integer eventId);
	public List<SoldTicketDetail> getSoldTicketDetailsByBrokerId(Integer eventId);
	/*public List<SoldTicketDetail> getAllSoldUnfilledTicketDetailsByBrokerId(Integer brokerId);
	public List<SoldTicketDetail> getRTFAllSoldUnfilledTicketDetails();*/
	public List<SoldTicketDetail> getAllSoldUnfilledTicketDetailsByProduct(ProductType productType);
	public List<SoldTicketDetail> getSoldTicketDetailsByBrokerIdAndEventId(Integer brokerId,Integer eventId);
	public List<SoldTicketDetail> getSoldTicketDetailsByBrokerIdAndArtistIdAndEventIdAndBySalesDateRange(Integer brokerId,Integer artistId,Integer eventId,Date startDate,Date endDate);
	//public List<SoldTicketDetail> getAllSoldFilledTicketDetails(String eventId,String artistId,String venueId, Date startDate, Date endDate,ProfitLossSign profitLossSign);
}

