package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.PromotionalEarnLifeOffer;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface PromotionalEarnLifeOfferDAO extends RootDAO<Integer, PromotionalEarnLifeOffer>{

	public List<PromotionalEarnLifeOffer> getAllContestPromocodes(Integer id, GridHeaderFilters filter,String type,String status);
	public List<PromotionalEarnLifeOffer> getPromotionalEarLifeByCode(String promocode);
	
}
