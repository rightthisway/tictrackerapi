package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.datas.ContestEvents;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface ContestEventsDAO extends RootDAO<Integer, ContestEvents>{

	public List<ContestEvents> getAllEvents(GridHeaderFilters filter,Integer contestId, String pageNo,String zones,Double singleTixPrice,Integer requiredQty,
			SharedProperty sharedProperty,Boolean isPagination);
	public Integer getAllEventsCount(GridHeaderFilters filter,Integer contestId,String zones,Double singleTixPrice,Integer requiredQty,SharedProperty sharedProperty);
	public List<ContestEvents> getContestEvents(GridHeaderFilters filter,Integer contestId,SharedProperty sharedProperty);
	public Integer getContestEventCount(Integer contestId);
	public void deleteByContestId(Integer contestId);
	public void deleteByContestIdAndEventId(Integer contestId, String eventId);
	
}
