package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.RtfRewardConfigInfo;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface RtfRewardConfigInfoDAO extends RootDAO<Integer, RtfRewardConfigInfo>{

	List<RtfRewardConfigInfo> getAllRtfRewardConfigByStatus(GridHeaderFilters filter,String status);
	List<RtfRewardConfigInfo> getRtfRewardConfigByRewardType(String rewardType);
}
