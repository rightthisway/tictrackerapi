package com.rtw.tracker.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtw.tracker.datas.Customer;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface CustomerDAO extends RootDAO<Integer, Customer>{

	public List<Customer> getCustomerByUserId(String userId);
	public Collection<Customer> getAllCustomers();
	public Customer checkCustomerExists(String username);
	int updateNotes(String notes,int invoiceId) throws Exception;
	public List<Customer> getRTWCustomerWithoutReferralCode();
	public Integer getTotalCustomerCount(Integer customerId,String customerName,String productType,Integer brokerId,GridHeaderFilters filter);
	public List<Customer> getCustomerByEmail(String email);
	public List<Customer> getAllRTFCustomer();
	public List<Customer> getCustomerByEmailUserIdOrFirstNameLastName(String email);
	public Customer getCustByUserId(String userId);
}
