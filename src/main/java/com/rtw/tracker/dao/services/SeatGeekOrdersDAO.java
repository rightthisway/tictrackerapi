package com.rtw.tracker.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtw.tracker.datas.SeatGeekOrders;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface SeatGeekOrdersDAO extends RootDAO<Integer, SeatGeekOrders> {
	
	public SeatGeekOrders getSeatGeekOrderByOrderId(String orderId) throws Exception;
	public List<SeatGeekOrders> getAllOpenSeatGeekOrders() throws Exception;
	public List<SeatGeekOrders> getAllOpenSeatGeekOrdersByStatus(String status) throws Exception;
	public List<Object[]> getTicketUploadedOrders() throws Exception;
	public List<SeatGeekOrders> getSeatGeekOrdersList(String fromDate, String toDate,String statusStr,GridHeaderFilters filter,String pageNo) throws Exception ;
	public List<SeatGeekOrders> getSeatGeekOrdersListToExport(String orderNo,String fromDate, String toDate,String statusStr,GridHeaderFilters filter) throws Exception;
	public Integer getSeatGeekOrdersListCount(String fromDate, String toDate,String statusStr,GridHeaderFilters filter) throws Exception;
	public List<SeatGeekOrders> getAllOpenSeatGeekOrdersForAutoconfirm() throws Exception;

}
