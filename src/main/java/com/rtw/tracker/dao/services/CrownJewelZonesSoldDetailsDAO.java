package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.CrownJewelZonesSoldDetails;

public interface CrownJewelZonesSoldDetailsDAO extends RootDAO<Integer, CrownJewelZonesSoldDetails>{

	public List<CrownJewelZonesSoldDetails> getAllTeamZonesByLeagueId(Integer leagueId) throws Exception;
	public List<CrownJewelZonesSoldDetails> getAllTeamZonesByTeamId(Integer teamId) throws Exception;
	public void updateDeleteStatusForLeague(Integer leagueId) throws Exception ;
	public void updateDeleteStatusForTeam(Integer teamId) throws Exception;
	public void updateActiveStatusForTeam(Integer teamId) throws Exception;
	
}
