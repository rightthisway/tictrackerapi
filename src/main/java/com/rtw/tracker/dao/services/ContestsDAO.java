package com.rtw.tracker.dao.services;

import java.util.List;
import java.util.Map;

import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.dao.implementation.QuizDAORegistry;
import com.rtw.tracker.datas.Contests;
import com.rtw.tracker.datas.Customer;
import com.rtw.tracker.pojos.ContestWinner;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface ContestsDAO extends RootDAO<Integer, Contests>{

	public List<Contests> getAllContests(Integer id, GridHeaderFilters filter,String type,String status,SharedProperty sharedProperty);
	public List<Contests> getAllActiveContests();
	public Contests getTodaysContest(String type,String status);
	public boolean resetContestDataById(Integer contestId,SharedProperty sharedProperty);
	public Contests getContestByPromoOfferId(Integer promoOfferId);
	public List<Contests> getAllStartedContests();
	public List<ContestWinner> getContestWinners(Integer contestId,GridHeaderFilters filter,SharedProperty sharedProperty);
	public List<ContestWinner> getContestGrandWinners(Integer contestId,GridHeaderFilters filter,SharedProperty sharedProperty);
	public List<Customer> getContestCustomers(Integer contestId,SharedProperty sharedProperty);
	public boolean copyContestToHostApp(Integer contestId);
	
	
	public List<Object[]> getContestTicketCostReportData(String fromDate,String toDate,SharedProperty sharedProperty);
	public List<Object[]> getContestWinnerInvoiceReportData(SharedProperty sharedProperty);
	public List<Object[]> getContestRewardDollarsReportData(String fromDate,String toDate,SharedProperty sharedProperty);
	public List<Object[]> getContesWiseRewardDollarsReportData(String fromDate,String toDate,SharedProperty sharedProperty);
	public List<Object[]> getContestReportData(String fromDate,String toDate,SharedProperty sharedProperty);
	public List<Object[]> getContestDetailedTrackingReportData(String fromDate,String toDate,SharedProperty sharedProperty);
	public List<Object[]> getCustomerPurchaseRewardDollarsReportData(String fromDate,String toDate,SharedProperty sharedProperty);
	public List<Object[]> getContestCustomerRefferalPercentageReportData(String fromDate,String toDate,SharedProperty sharedProperty);
	public List<Object[]> getCustomerWiseRewardDollarsReportData(String fromDate,String toDate,SharedProperty sharedProperty);
	public List<Object[]> getCustomerReferredByReferredToReportDataOld(String fromDate,String toDate,SharedProperty sharedProperty);
	public List<Object[]> getRegisteredCustomerReportData(String fromDate,String toDate,SharedProperty sharedProperty);
	public List<Object[]> getContestParticipantsAverageReportData(String fromDate,String toDate,SharedProperty sharedProperty);
	public List<Object[]> getCustomerReferralAverageReportData(String fromDate,String toDate,SharedProperty sharedProperty);
	public List<Object[]> getQuestionWiseUserAnswerCountReportData(String fromDate,String toDate,SharedProperty sharedProperty);
	public List<Object[]> getCustomerLivesUsedContestWiseReportData(String fromDate,String toDate,SharedProperty sharedProperty);
	public List<Object[]> getCustomerLivesEarnedByReferralReportData(String fromDate,String toDate,SharedProperty sharedProperty);
	public List<Object[]> getCustomerLivesUsedAllReportData(String fromDate,String toDate,SharedProperty sharedProperty);
	public List<Object[]> getCustomerLivesUsedBreackDownReportData(String fromDate,String toDate,SharedProperty sharedProperty);
	public List<Object[]> getCustomerLivesUsedPercentageBreackDownReportData(String fromDate,String toDate,SharedProperty sharedProperty);
	public List<Object[]> getCustomerReferredByReferredToReportData(String fromDate,String toDate,SharedProperty sharedProperty);
	public int getUniqueReferree(String fromDate,String toDate,SharedProperty sharedProperty);
	public List<Object[]> getReferreRefferalsReportData(String fromDate,String toDate,SharedProperty sharedProperty);
	public List<Object[]> getAvgQuestionAsweredByCustomerReportData(String fromDate,String toDate,SharedProperty sharedProperty);
	public List<Object[]> getAvgNoOfGamPlayedByCustomerReportData(String fromDate,String toDate,SharedProperty sharedProperty);
	public List<Object[]> getCustomerRewardDollarsReportData(String fromDate,String toDate,SharedProperty sharedProperty);
	
	public List<Object[]> getRegisteredCustomerExcludingBotsReportData(String fromDate,String toDate,SharedProperty sharedProperty);
	public List<Object[]> getContestParticipantsAverageexcludingBotsReportData(String fromDate,String toDate,SharedProperty sharedProperty);
	public List<Object[]> getCustomerReferralAverageExcludingBotsReportData(String fromDate,String toDate,SharedProperty sharedProperty);
	public Integer getTotalContestCount(String fromDate,String toDate);
	public Map<String, Object> getAllBotsInformation(SharedProperty sharedProperty);
	public List<Object[]> getContestWiseFirstTimeCustomerReportData();
	public List<Object[]> getRegisteredUserReportData(String fromDate,String toDate,SharedProperty sharedProperty);
	public List<Object[]> getRegisteredUserWhoHaveNotPlayedContestReportData(String fromDate,String toDate,SharedProperty sharedProperty);
	public List<Object[]> getRegisteredUserWhoHavePlayedContestReportData(String fromDate,String toDate,SharedProperty sharedProperty);
	//public List<Object[]> getContestCustomerRewardDollarsReportData(String fromDate,String toDate,SharedProperty sharedProperty); // Shiva Modified on 14 Oct 2019
	public List<Object[]> getCustomerRewardDollarsLiabilityReportData(String fromDate,String toDate,SharedProperty sharedProperty); // Shiva Added on 14 Oct 2019
	public Map<String, Object> getPollingInformation(String fromDate, String toDate,SharedProperty sharedProperty);
	public Map<String, Object> getNoOfGamesPlayedInformation(SharedProperty sharedProperty);
	public Map<String, Object> getCustomerSuperFanStarsInformation(SharedProperty sharedProperty);
	public List<Object[]> getSuperFanStartsInformation(SharedProperty sharedProperty);
	public Map<String, Object> getTicketCostInformation(String fromDate, String toDate, SharedProperty sharedProperty);
	public Map<String, Object> getQueWiseUserAnswerCountInformation(String fromDate, String toDate, SharedProperty sharedProperty);	
	public Map<String, Object> getBotsCustomerInformation(String fromDate, String toDate, SharedProperty sharedProperty);
	public Map<String, Object> getContestParticipantInformation(SharedProperty sharedProperty);
	public List<Object[]> getRegisteredAndPlayedCustomers(String fromDate,String toDate,SharedProperty sharedProperty) ;
	public Integer getRegisteredAndPlayedCustomerCount(String fromDate,String toDate,SharedProperty sharedProperty);
	/*----// Shiva BEGINS of Adding New Report Controller Methods on 14 Oct 2019 //----*/
	public List<Object[]> getFantasyFootballContestWinnersDetailsReport(String fromDate, String toDate, SharedProperty sharedProperty);
	public List<Object[]> getMonthlyCustomerParticipantsCountStatisticsForSheetOne(String fromDate, String toDate, SharedProperty sharedProperty);
	public List<Object[]> getMonthlyContestParticipantsCountStatisticsForSheetTwo(String fromDate, String toDate, SharedProperty sharedProperty);
	public List<Object[]> getOutstandingContestOrdersToBeFulfilledReport(SharedProperty sharedProperty);
	public List<Object[]> getCustomerAverageTimeAccessingRTFTriviaReport(SharedProperty sharedProperty);
	public List<Object[]> getMonthlyCustomerParticipantsCountBreakdownStatistics(SharedProperty sharedProperty);
	public List<Object[]> getRTFCustomerContestStatisticsforBusiness(SharedProperty sharedProperty);
	public List<Object[]> getGiftCardPurchasedWinnersReport(String fromDate, String toDate, SharedProperty sharedProperty);
	public List<Object[]> getPollingStatistics(SharedProperty sharedProperty);
	public List<Object[]> getRTFTVStatistics(SharedProperty sharedProperty);
	public List<Object[]> getCustomerTimeSpentOnEachContest(String fromDate, String toDate, SharedProperty sharedProperty);
	public List<Object[]> getRTFCustomerAverageTimeTakenperContestStatistics(String fromDate, String toDate, SharedProperty sharedProperty);
	public List<Object[]> getGrandMegaMINIJackpotTicketWinnerCount(String fromDate, String toDate, SharedProperty sharedProperty);
	public List<Object[]> getCustomersUsingReferralCodesReport(String fromDate, String toDate, SharedProperty sharedProperty);
	public List<Object[]> getGiftCardOrdersPendingFromRTFCount(SharedProperty sharedProperty);
	public List<Object[]> getGiftCardOrdersPendingFromCustomerCount(SharedProperty sharedProperty);
	public List<Object[]> getGiftCardOrdersOverallPendingCount(SharedProperty sharedProperty);
	public List<Object[]> getGiftCardOrdersPendingDetails(String fromDate, String toDate, SharedProperty sharedProperty);
	public List<Object[]> getGiftCardOrdersOutstandingDetails(String fromDate, String toDate, SharedProperty sharedProperty);
	/*----// Shiva ENDS of Adding New Report Controller Methods on 24 Oct 2019 //----*/
}
