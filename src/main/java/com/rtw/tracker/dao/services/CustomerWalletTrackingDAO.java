package com.rtw.tracker.dao.services;


import java.util.List;

import com.rtw.tracker.datas.CustomerWalletTracking;

public interface CustomerWalletTrackingDAO extends RootDAO<Integer, CustomerWalletTracking>{

	public List<CustomerWalletTracking> getCustomerWalletTrackingByCustomerId(Integer customerId);
}
