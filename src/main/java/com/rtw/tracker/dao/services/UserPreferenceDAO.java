package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.UserPreference;

public interface UserPreferenceDAO extends RootDAO<Integer, UserPreference>{

	public UserPreference getPreferenceByUserNameandGridName(String userName,String gridName);
	public List<UserPreference> getPreferenceByUserName(String userName);
}
