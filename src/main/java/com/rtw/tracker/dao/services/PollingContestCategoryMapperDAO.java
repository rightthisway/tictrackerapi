package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.PollingContCategoryMapper;

public interface PollingContestCategoryMapperDAO {
	
	public List<PollingContCategoryMapper>  getPollingCategoriesMappedForContestId(Integer id);

	public List<PollingContCategoryMapper> getPollingCategoryByContest(String contestId, String status);

	public List<PollingContCategoryMapper> getPollingCategoryBySelection(
			String contestId, String status);

	public void deletePollingContCategoryMapper(Integer contestId, Integer categoryId);

}
