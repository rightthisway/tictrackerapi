package com.rtw.tracker.dao.services;

import java.util.Collection;
import java.util.Date;

import com.rtw.tracker.datas.UserAction;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface UserActionDAO extends RootDAO<Integer, UserAction> {

	public Collection<UserAction> getAllActionsByUserId(Integer userId);
	public Collection<UserAction> getAllActionsByUserIdAndDateRange(Integer userId, String fromDate, String toDate, GridHeaderFilters filter, String pageNo);
	public Integer getAllActionsByUserIdAndDateRangeCount(Integer userId, String fromDate, String toDate, GridHeaderFilters filter);
}
