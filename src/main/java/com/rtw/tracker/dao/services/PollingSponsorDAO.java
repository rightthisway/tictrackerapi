package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.PollingSponsor;
import com.rtw.tracker.pojos.SponsorAutoComplete;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface PollingSponsorDAO extends RootDAO<Integer, PollingSponsor>{

	public List<PollingSponsor> getAllPollingSponsor(Integer id, GridHeaderFilters filter, String status);
		
	public PollingSponsor getPollingSponsorById(Integer id) throws Exception;

	public PollingSponsor savePollingSponsor(PollingSponsor pollingSponsor) throws Exception;
	
	public List<SponsorAutoComplete> filterByName(String pattern);

	public Integer getCategoryCountForSponsor(Integer pollId);
	
}