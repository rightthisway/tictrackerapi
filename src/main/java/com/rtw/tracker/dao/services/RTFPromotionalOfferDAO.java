package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.RTFPromotionalOffer;

/**
 * interface having db related methods for RTFPromotionalOffer
 * @author Ulaganathan
 *
 */
public interface RTFPromotionalOfferDAO extends RootDAO<Integer, RTFPromotionalOffer>{
	
	/*public CustomerLoyalty getCustomerLoyaltyByCustomerId(Integer customerId);
	void updateCustomerLoyalty(Double pointsRemaining, Double latestSpendPoints, Double totalSpendPoints, Integer customerId);
	public List<CustomerLoyalty> getAllCustomerLoyaltyWhichHasMorethanZeroActivePoints();
	public void updateCustomerLoyaltyByCustomer(Double earnPoints,  Integer customerId);*/
	public List<RTFPromotionalOffer> getAllActiveOffers();
}
