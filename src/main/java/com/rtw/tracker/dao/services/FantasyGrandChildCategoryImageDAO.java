package com.rtw.tracker.dao.services;

import com.rtw.tracker.datas.FantasyGrandChildCategoryImage;

public interface FantasyGrandChildCategoryImageDAO extends RootDAO<Integer, FantasyGrandChildCategoryImage>{

	public FantasyGrandChildCategoryImage getFantasyGrandChildCategoryImageByGrandChildId(Integer fantasyGrandChildId);
}
