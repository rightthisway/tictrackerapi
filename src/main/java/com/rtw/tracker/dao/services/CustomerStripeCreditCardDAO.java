package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.CustomerStripeCreditCard;

public interface CustomerStripeCreditCardDAO extends RootDAO<Integer, CustomerStripeCreditCard>{

	public List<CustomerStripeCreditCard> getCustomerStripCardDetailsByCustomerId(Integer customerId)throws Exception;
}
