package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.RtfPointsConversionSetting;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface RtfPointsConversionSettingDAO extends RootDAO<Integer, RtfPointsConversionSetting> {

	public List<RtfPointsConversionSetting> getAllRtfPOintsSettingsByStatus(GridHeaderFilters filter,String status);
	public List<RtfPointsConversionSetting> getRtfPointsSettingByTypeAndQty(String type,Integer qty);
}
