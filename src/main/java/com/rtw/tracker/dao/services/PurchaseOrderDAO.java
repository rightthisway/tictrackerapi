package com.rtw.tracker.dao.services;

import java.util.Collection;

import com.rtw.tracker.datas.PurchaseOrder;

public interface PurchaseOrderDAO extends RootDAO<Integer, PurchaseOrder>{

	public Collection<PurchaseOrder> fetchPO();
	public PurchaseOrder getPurchaseOrderByIdAndBrokerId(Integer id, Integer brokerId);
}