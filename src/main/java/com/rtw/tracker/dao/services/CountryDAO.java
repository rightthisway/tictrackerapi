package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.Country;


public interface CountryDAO extends RootDAO<Integer, Country> {
	/**
	 * method to get List of Countries
	 * @return List of Countries
	 */
	public List<Country> getAllCountry();
	/**
	 * method to get Country by id
	 * @param id, company id
	 * @return
	 */
	public Country geCountryById(Integer id);
	public Integer getCountryByName(String name);
	public List<Country> GetUSCANADA();
}