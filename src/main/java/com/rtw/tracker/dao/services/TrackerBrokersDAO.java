package com.rtw.tracker.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtw.tracker.datas.TrackerBrokers;

public interface TrackerBrokersDAO extends RootDAO<Integer, TrackerBrokers>{

	public TrackerBrokers getBrokerByName(String companyName) throws Exception;
	public Collection<TrackerBrokers> getBrokersByName(String companyName) throws Exception;
}
