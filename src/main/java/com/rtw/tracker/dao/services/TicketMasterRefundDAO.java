package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.TicketMasterRefund;

public interface TicketMasterRefundDAO extends RootDAO<Integer, TicketMasterRefund>{
	
	
	public List<String> getAllUniqueUsers();
	public List<TicketMasterRefund> getAllTMRecordsByUserName(String userName);
	public List<Object[]> getTMRefundByCCUsers();
}
