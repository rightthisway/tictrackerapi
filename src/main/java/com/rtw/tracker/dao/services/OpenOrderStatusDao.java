package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.OpenOrderStatus;
import com.rtw.tracker.datas.ProductType;

public interface OpenOrderStatusDao extends RootDAO<Integer, OpenOrderStatus>{

	public List<OpenOrderStatus> getAllActiveOpenOrders();
	public List<OpenOrderStatus> getAllActiveOpenOrdersByProduct(ProductType productType);
	public List<OpenOrderStatus> getRTFAllActiveOpenOrders();
	public List<OpenOrderStatus> getRTFActiveOrderByInvoiceId(Integer invoiceId, String productType);
}
