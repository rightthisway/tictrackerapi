package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.datas.FanclubAbuseReported;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface FanclubAbuseReportedDAO extends RootDAO<Integer, FanclubAbuseReported>{

	List<FanclubAbuseReported> getFanClubAbuseReportedData(String status, GridHeaderFilters filter,SharedProperty prop);
	List<FanclubAbuseReported> getFanClubPostAbuseReportedData(String status, GridHeaderFilters filter,SharedProperty prop);
	List<FanclubAbuseReported> getFanClubEventAbuseReportedData(String status, GridHeaderFilters filter,SharedProperty prop);
	List<FanclubAbuseReported> getFanClubVideoAbuseReportedData(String status, GridHeaderFilters filter,SharedProperty prop);
	List<FanclubAbuseReported> getFanClubCommentsAbuseReportedData(String status, GridHeaderFilters filter,SharedProperty prop);
	
}
