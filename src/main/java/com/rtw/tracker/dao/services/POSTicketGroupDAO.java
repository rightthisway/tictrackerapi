package com.rtw.tracker.dao.services;

import com.rtw.tracker.datas.POSTicketGroup;

public interface POSTicketGroupDAO extends RootDAO<Integer, POSTicketGroup>{
	
	public POSTicketGroup getPOSTicketGroupByTicketGroupId(Integer ticketGroupId,String productType) throws Exception;
	public POSTicketGroup getPOSTicketGroupByTicketGroupIdWithPdt(Integer ticketGroupId, String productType) throws Exception;
	public POSTicketGroup getPOSTicketGroupByEventId(Integer eventId) throws Exception;

}
