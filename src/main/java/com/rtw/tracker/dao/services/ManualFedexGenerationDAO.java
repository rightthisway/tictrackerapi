package com.rtw.tracker.dao.services;

import com.rtw.tracker.datas.ManualFedexGeneration;

public interface ManualFedexGenerationDAO extends RootDAO<Integer, ManualFedexGeneration>{

	public ManualFedexGeneration getManualFedexByTrackingNo(String trackingNo);
}
