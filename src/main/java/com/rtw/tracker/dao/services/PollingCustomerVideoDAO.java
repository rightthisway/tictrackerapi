package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.datas.PollingCustomerVideo;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface PollingCustomerVideoDAO extends RootDAO<Integer, PollingCustomerVideo>{
	public List<PollingCustomerVideo> getActiveVideos();
	public List<PollingCustomerVideo> getActiveVideos(String fromDate, String toDate, String name, String videoType, String playType,GridHeaderFilters filter,SharedProperty sharedProperty);
}