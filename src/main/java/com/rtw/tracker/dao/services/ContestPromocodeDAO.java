package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.datas.ContestPromocode;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface ContestPromocodeDAO extends RootDAO<Integer, ContestPromocode>{
	
	public List<ContestPromocode> getAllContestPromocodes(Integer id, GridHeaderFilters filter,String type,String status,SharedProperty sharedProperty);

}
