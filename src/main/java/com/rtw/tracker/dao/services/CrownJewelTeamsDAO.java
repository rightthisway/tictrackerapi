package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.CrownJewelTeams;

public interface CrownJewelTeamsDAO extends RootDAO<Integer, CrownJewelTeams> {
	
	public List<CrownJewelTeams> getAllActiveTeamsByLeagueId(Integer leagueId) throws Exception;
	public List<CrownJewelTeams> getAllDeletedTeamsByLeagueId(Integer leagueId) throws Exception;
	public List<CrownJewelTeams> getAllActiveTeamsByLeagueIdandCity(Integer leagueId,String city) throws Exception;
	public List<CrownJewelTeams> getAllTeamsByLeagueId(Integer leagueId) throws Exception;
	public List<String> getAllActiveCrownJewelTeamsCityByLeagueId(Integer leagueId)throws Exception;
	public List<CrownJewelTeams> getAllActiveTeamsByCategoryTeamId(Integer categoryTeamId) throws Exception;

}
