package com.rtw.tracker.dao.services;

import com.rtw.tracker.datas.ReferralContestWinner;

public interface ReferralContestWinnerDAO extends RootDAO<Integer,ReferralContestWinner>{
	public ReferralContestWinner getContestWinner(Integer contestId);
	public ReferralContestWinner getContestWinnerDetails(Integer contestId);
}
