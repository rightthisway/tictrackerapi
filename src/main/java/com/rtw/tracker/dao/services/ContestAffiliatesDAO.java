package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.ContestAffiliates;
import com.rtw.tracker.pojos.ContestAffiliateEarning;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface ContestAffiliatesDAO extends RootDAO<Integer, ContestAffiliates> {

	public List<ContestAffiliates> getAllContestAffiliates(String status,GridHeaderFilters filter);
	public ContestAffiliates getContestAffiliateByCustomerId(Integer customerId);
	public List<ContestAffiliateEarning> getContestAffiliateEarnings(Integer customerId,GridHeaderFilters filter);
	public List<ContestAffiliateEarning> getContestAffiliatePendingEarning(Integer customerId,GridHeaderFilters filter);
}
