package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.RtfGiftCard;
import com.rtw.tracker.pojos.GiftCardAutoComplete;
import com.rtw.tracker.pojos.GiftCardPojo;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface RtfGiftCardDAO extends RootDAO<Integer, RtfGiftCard>{
	
	public RtfGiftCard getActiveRtfGiftCardById(Integer giftCardId);
	/*public Collection<RtfGiftCard> filterByName(String pattern);*/
	public List<GiftCardAutoComplete> filterByName(String pattern);
	public List<GiftCardPojo> getAllGiftCards(GridHeaderFilters  filter,String status);
	public List<RtfGiftCard> getAllActiveGiftCards();

}
