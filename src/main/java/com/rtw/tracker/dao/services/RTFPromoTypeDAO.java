package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.RTFPromoType;

public interface RTFPromoTypeDAO extends RootDAO<Integer, RTFPromoType>{

	public List<RTFPromoType> getPromoTypeFromOfferId(Integer id);
}
