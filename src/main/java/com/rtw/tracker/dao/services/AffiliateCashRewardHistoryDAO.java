package com.rtw.tracker.dao.services;

import java.util.Collection;

import com.rtw.tracker.datas.AffiliateCashRewardHistory;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface AffiliateCashRewardHistoryDAO extends RootDAO<Integer, AffiliateCashRewardHistory>{

	public Collection<AffiliateCashRewardHistory> getHistoryByUserId(Integer userId, GridHeaderFilters filter);
	public Integer getHistoryCountByUserId(Integer UserId, GridHeaderFilters filter);
	public AffiliateCashRewardHistory getHistoryByUserIdAndOrderId(Integer userId, Integer orderId);
	public AffiliateCashRewardHistory getHistoryByOrderId(Integer orderId);
	
}
