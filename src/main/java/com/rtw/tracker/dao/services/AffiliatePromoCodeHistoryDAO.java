package com.rtw.tracker.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtw.tracker.datas.AffiliatePromoCodeHistory;

public interface AffiliatePromoCodeHistoryDAO extends RootDAO<Integer, AffiliatePromoCodeHistory>{

	public AffiliatePromoCodeHistory getActiveAffiliatePromoCode(Integer userId) throws Exception;
	public List<AffiliatePromoCodeHistory> getAllHistoryByUserId(Integer userId) throws Exception;
	public void updateAllActiveHistoryByUserId(Integer userId) throws Exception;
	public Collection<AffiliatePromoCodeHistory> getAllActivePromoCodes()throws Exception;
	public AffiliatePromoCodeHistory checkActiveAffiliatePromoCode(String promoCode) throws Exception;
	
}
