package com.rtw.tracker.dao.services;

import com.rtw.tracker.datas.TicketMasterRefundKnownEmail;

public interface TicketMasterRefundKnownEmailDAO extends RootDAO<Integer, TicketMasterRefundKnownEmail>{

}
