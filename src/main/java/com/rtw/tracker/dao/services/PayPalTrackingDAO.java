package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.PayPalTracking;

public interface PayPalTrackingDAO extends RootDAO<Integer,PayPalTracking>{
	
	public List<PayPalTracking> getTrackingByCustomerId(Integer customerId) throws Exception;
	public List<PayPalTracking> getTrackingByEventId(Integer eventId) throws Exception;
	public List<PayPalTracking> getTrackingByCategoryTicketGroupId(Integer catgoryTicketGroupId) throws Exception;
	public List<PayPalTracking> getTrackingByPayPalTransactionId(String paypalTransactionId) throws Exception;
	public List<PayPalTracking> getTrackingByTransactionId(Integer transactionId) throws Exception;
	
}
