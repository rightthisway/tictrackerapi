package com.rtw.tracker.dao.services;

import java.util.Collection;

import com.rtw.tracker.datas.Venue;

public interface VenueDAO extends RootDAO<Integer, Venue> {

	public Collection<Venue> filterByName(String venueName);
}
