package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.CustomerLoyalFanAudit;

public interface CustomerLoyalFanAuditDAO extends RootDAO<Integer, CustomerLoyalFanAudit>{

	public List<CustomerLoyalFanAudit> getAllByCustomerId(Integer customerId);
	
}
