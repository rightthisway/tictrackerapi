package com.rtw.tracker.dao.services;

import java.util.List;


import com.rtw.tracker.datas.PollingRewards;


public interface PollingRewardsDAO extends RootDAO<Integer, PollingRewards>{

	public PollingRewards  getPollingRewardsForContestId(Integer id);
	
}