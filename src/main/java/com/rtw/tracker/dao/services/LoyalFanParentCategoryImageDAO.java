package com.rtw.tracker.dao.services;

import com.rtw.tracker.datas.LoyalFanParentCategoryImage;

public interface LoyalFanParentCategoryImageDAO extends RootDAO<Integer, LoyalFanParentCategoryImage>{

	public LoyalFanParentCategoryImage getLoyalFanImageByParentCategory(Integer parentCategoryId);
}
