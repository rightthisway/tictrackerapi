package com.rtw.tracker.dao.services;

import com.rtw.tracker.datas.POSPurchaseOrder;

public interface POSPurchaseOrderDAO extends RootDAO<Integer, POSPurchaseOrder>{

	public POSPurchaseOrder getPOSPurchaseOrderDAOByPurchaseOrderId(Integer purchaseOrderId)throws Exception;
	public POSPurchaseOrder getPOSPurchaseOrderDAOByPurchaseOrderId(Integer purchaseOrderId,String productType)throws Exception;
}
