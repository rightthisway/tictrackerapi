package com.rtw.tracker.dao.services;

import com.rtw.tracker.datas.ChildCategoryImage;

public interface ChildCategoryImageDAO extends RootDAO<Integer, ChildCategoryImage>{
	
	public ChildCategoryImage getChildCategoryImageByChildCategoryId(Integer childCategoryId) throws Exception;
}
