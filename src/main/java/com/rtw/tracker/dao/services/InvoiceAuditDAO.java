package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.InvoiceAudit;
import com.rtw.tracker.enums.InvoiceAuditAction;

public interface InvoiceAuditDAO extends RootDAO<Integer, InvoiceAudit>{

	public List<InvoiceAudit> getInvoiceAuditByInvoiceId(Integer InvoiceId);
	public List<InvoiceAudit> getInvoiceAuditByInvoiceIdAndAction(Integer InvoiceId,InvoiceAuditAction action);
}
