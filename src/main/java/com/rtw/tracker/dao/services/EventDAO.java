package com.rtw.tracker.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtw.tracker.datas.Customers;
import com.rtw.tracker.datas.Event;

public interface EventDAO extends RootDAO<Integer, Event>{

	public Event getEventById(Integer eventId) throws Exception;
	public List<Event> getEventByVenueId(Integer id);
	public Collection<Event> filterByName(String pattern);
	public List<Object[]> getEventsNotInRTF();
	//public List<Object[]> getRTFUnsoldInventory();
	public List<Object[]> getRTFUnFilledReport();
	public List<Object[]> getRTFFilledReport();
	public List<Object[]> getEventsStatistics();
	public List<Object[]> getNext15DaysUnsentOrders();
	public List<Object[]> getUnFilledShortsWithPossibleLongInventory();
	public List<Object[]> getUnsoldInventoryReport(String startDate,String endDate);
	public List<Object[]> getUnsoldCategoryTicketsReport(String startDate,String endDate);
	public List<Object[]> getInvoiceReport(String startDate,String endDate);
	public List<Object[]> getPurchaseOrderReport(String startDate,String endDate);
	public List<Object[]> getRTFSalesReportInTickTracker();
	public List<Object[]> getRTFSalesReportNotInTickTracker();
	public List<Object[]> getRTFSalesReport();
	public List<Object[]> getRTFCustomerOrderForWebOrdersReport();
	public List<Object[]> getRTFCustomerOrderForPhoneOrdersReport();
	public List<Object[]> getRTFCustomerOrdersDuplicationsReport();
	public List<Customers> getClientMasterReport();
	public List<Customers> getUnsubscribedClientMasterReport();
	public List<Object[]> getPurchasedMasterReport();
	public List<Object[]> getRTFRegisteredButnotPurchasedReport();
	public List<Object[]> getRTFCombinedPurchasedNonPurchasedReport();
	public List<Object[]> getRTFLoyalFanReport();
	public List<Object[]> getRTFLoyalFanPurchasedReport();
	public List<Object[]> getRTFLoyalFanNonPurchasedReport();
	public List<Object[]> getRTFChannelWiseCustomerOrdersReport();
	public List<Object[]> getRTFFilledReportByInvoice();
	public List<Object[]> getRTFFilledReportByPO();
	public List<Object[]> getRTFUnFilledReportByInvoice();
	public List<Object[]> getRTFMonthlyFilledReportByInvoice();
	public List<Object[]> getRTFMonthlyFilledReportByPO();
	public List<Object[]> getRTFVoidedInvoiceReport();
	public List<Object[]> getRTFSalesReportByReferral();
	public List<Object[]> getRTFCustomerYearlySpentReport(String year);
	public List<Object[]> getRTFCustomerPlatformWiseReport(String year, String platform);
	public List<Object[]> getRTFCustomerOrderwiseReport(String year, String platform);
	public List<Object[]> getRTFCustomerAcquisitionReport(String year, String platform);
	public List<Object[]> getRTFCustomerAcquisitionSummaryReport(String year, String platform);
	public List<Object[]> getRTFTMATSeatAllocationReport(String artistName); 
	public List<Event> getAllActiveEvents();
}
