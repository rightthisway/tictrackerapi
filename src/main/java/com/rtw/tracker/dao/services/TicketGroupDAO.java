package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.TicketGroup;

public interface TicketGroupDAO extends RootDAO<Integer, TicketGroup>{

	//public List<TicketGroup> getAllTicketGroupsByInvoiceId(Integer invoiceId) throws Exception;
	public TicketGroup getAllTicketGroupById(Integer id);
	public TicketGroup getTicketGroupByIdAndBrokerId(Integer id, Integer brokerId);
	public List<TicketGroup> getAllTicketGroupsByPOId(Integer poId) throws Exception;
	public List<TicketGroup> getAllTicketGroupsByEventId(Integer eventId) throws Exception;
	public List<TicketGroup> getAllTicketGroupsByEventAndBroker(Integer eventId,Integer brokerId) throws Exception;
	public void updateBroadCastByEventId(Integer eventId, Boolean broadCast);
}
