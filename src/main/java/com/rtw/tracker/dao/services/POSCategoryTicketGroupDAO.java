package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.POSCategoryTicketGroup;

public interface POSCategoryTicketGroupDAO extends RootDAO<Integer, POSCategoryTicketGroup>{
	
	public POSCategoryTicketGroup getPOSCategoryTicketGroupByEventId(Integer eventId) throws Exception;	
	public POSCategoryTicketGroup getPOSCategoryTicketGroupByCategoryTicketGroupId(Integer catTicketGroupId,String productType) throws Exception;	
	
}
