package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.CrownJewelTeamZones;

public interface CrownJewelTeamZonesDAO extends RootDAO<Integer, CrownJewelTeamZones> {
	
	public List<CrownJewelTeamZones> getAllTeamZonesByLeagueId(Integer leagueId) throws Exception;
	public List<CrownJewelTeamZones> getAllTeamZonesByTeamId(Integer teamId) throws Exception;
	public List<CrownJewelTeamZones> getAllActiveTeamZonesByLeagueId(Integer leagueId) throws Exception;
	public List<CrownJewelTeamZones> getAllActiveTeamZonesByTeamId(Integer teamId) throws Exception;
	public List<CrownJewelTeamZones> getAllActiveTeamZonesByLeagueIdAndCity(Integer leagueId,String city) throws Exception;
	public CrownJewelTeamZones getActiveZoneByTeamAndZoneDescription(Integer teamId,String zone)throws Exception;
}
