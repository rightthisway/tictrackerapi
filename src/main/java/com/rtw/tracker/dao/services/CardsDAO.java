package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.Cards;

public interface CardsDAO extends RootDAO<Integer, Cards>{
	
	public Cards getCardsByCardPosition(Integer productId,Integer desktopPosition,Integer mobilePosition) throws Exception;
	public List<Cards> getAllCardsByProductId(Integer productId) throws Exception;
	public Cards getExistingCardsByCardPostionsAndCardId(Integer productId,Integer cardId,Integer desktopPosition,Integer mobilePosition) throws Exception;
	public void updateCardImageUrl(Cards card) throws Exception;
}
