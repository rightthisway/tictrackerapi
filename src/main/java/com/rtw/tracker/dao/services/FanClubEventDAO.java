package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.dao.services.RootDAO;
import com.rtw.tracker.datas.FanClubEvent;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface FanClubEventDAO extends RootDAO<Integer, FanClubEvent>{

	public List<FanClubEvent> getAllFanClubEvents(GridHeaderFilters filter,String status,Integer fanClubId,SharedProperty prop);
}
