package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.RtfGiftCardQuantity;

public interface RtfGiftCardQuantityDAO extends RootDAO<Integer, RtfGiftCardQuantity>{

	public List<RtfGiftCardQuantity> getAllGiftCardQuantityByCardId(Integer cardId);
	public List<RtfGiftCardQuantity> getExpiredGiftCardQuantityByCardId(Integer cardId);
}
