package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.datas.GiftCardOrders;

import com.rtw.tracker.utils.GridHeaderFilters;

public interface GiftCardOrderDAO extends RootDAO<Integer, GiftCardOrders>{

	public List<GiftCardOrders> getAllGiftCardOrders(Integer id, GridHeaderFilters filter, String status);
	
	
	public GiftCardOrders getGiftCardOrdersById(Integer id) throws Exception;
	public List<GiftCardOrders> getGiftCardOrdersByStatus(String status);
	
}
