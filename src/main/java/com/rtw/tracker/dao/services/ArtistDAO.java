package com.rtw.tracker.dao.services;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.rtw.tracker.datas.Artist;
import com.rtw.tracker.datas.ProductType;
import com.rtw.tracker.enums.ArtistStatus;

public interface ArtistDAO extends RootDAO<Integer, Artist>{
	
	List<Artist> getAllArtistBySearchKeyAndPageNumber(String searchKey,Integer pageNumber,Integer maxRows,ProductType productType,
			Map<Integer, Boolean> favArtistMap,Integer superFanArtistId);

	public Collection<Artist> filterByName(String pattern);
	public Collection<Artist> getArtistByDisplayOnSearch();
	/*public List<Artist> getAllArtistsByChildCategory(Integer childCategoryId)  throws Exception;
	public List<Artist> getAllArtistsByGrandChildCategory(Integer grandChildCategoryId)  throws Exception;*/
}
