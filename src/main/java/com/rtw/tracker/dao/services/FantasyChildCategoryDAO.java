package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.FantasyChildCategory;

public interface FantasyChildCategoryDAO extends RootDAO<Integer, FantasyChildCategory>{
	
	public List<FantasyChildCategory> getAll();	
	public FantasyChildCategory getChildCategoryByName(String name);	
	public FantasyChildCategory getChildCategoryById(Integer id);
	public List<FantasyChildCategory> getChildCategoryByParentId(Integer id);
}
