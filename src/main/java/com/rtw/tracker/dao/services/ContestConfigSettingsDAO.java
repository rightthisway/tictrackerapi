package com.rtw.tracker.dao.services;

import java.util.List;

import com.rtw.tracker.datas.ContestConfigSettings;
import com.rtw.tracker.datas.ContestQuestions;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface ContestConfigSettingsDAO extends RootDAO<Integer, ContestConfigSettings>{

	public ContestConfigSettings getContestConfigSettings();
}
