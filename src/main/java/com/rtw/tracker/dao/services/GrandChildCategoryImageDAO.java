package com.rtw.tracker.dao.services;

import com.rtw.tracker.datas.GrandChildCategoryImage;

public interface GrandChildCategoryImageDAO extends RootDAO<Integer, GrandChildCategoryImage>{
	
	public GrandChildCategoryImage getGrandChildCategoryImageByGrandChildId(Integer grandChildId) throws Exception;
}
