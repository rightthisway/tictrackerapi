package com.rtw.tracker.dao.services;

import java.util.Date;
import java.util.List;

import com.rtw.tracker.datas.CrownJewelCustomerOrder;
import com.rtw.tracker.utils.GridHeaderFilters;

public interface CrownJewelCustomerOrderDAO extends RootDAO<Integer, CrownJewelCustomerOrder>{
	public List<CrownJewelCustomerOrder> getCronJewelOrdersByFilter(String orderId,String fromDate, String toDate,String parentCategory, String grandChildCategory, String teamName, String leagueName,String status,String pageNo,GridHeaderFilters filter)throws Exception;
	public List<CrownJewelCustomerOrder> getCrownJewelOrdersByTeamId(Integer teamId)throws Exception;
	public List<CrownJewelCustomerOrder> getCrownJewelOrdersByLeagueId(Integer leagueId)throws Exception;
	public List<CrownJewelCustomerOrder> getCrownJewelOrdersByFilterToExport(String orderId, String fromDate, String toDate, String parentCategory, String grandChildCategory, String teamName, String leagueName, String status, GridHeaderFilters filter)throws Exception;
	
}

