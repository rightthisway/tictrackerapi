package com.rtw.tracker.mail;

public class MailAttachment {
	private byte[] content;
	private String mimeType;
	private String fileName;
	private String filePath;
	
	public MailAttachment(byte[] content, String mimeType, String fileName) {
		this.content = content;
		this.mimeType = mimeType;
		this.fileName = fileName;
	}
	public MailAttachment(byte[] content, String mimeType, String fileName,String filePath) {
		this.content = content;
		this.mimeType = mimeType;
		this.fileName = fileName;
		this.filePath = filePath;
	}
	
	public byte[] getContent() {
		return content;
	}

	public String getMimeType() {
		return mimeType;
	}

	public String getFileName() {
		return fileName;
	}

	public String getFilePath() {
		return filePath;
	}

}