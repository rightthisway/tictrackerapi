package com.rtw.tracker.filters;

import java.io.IOException;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.TrackerUser;
import com.rtw.tracker.datas.UserAction;

public class UserActionLogFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpServletRequest = (HttpServletRequest)request;
		String path =httpServletRequest.getPathInfo();
		//System.out.println(path);
		if(path == null || path.contains("Logout")){
			// skip this and do not track this
		}else{
			TrackerUser trackerUser = (TrackerUser)httpServletRequest.getSession().getAttribute("trackerUser");
			if(trackerUser != null){
				logUserAction(trackerUser, path, httpServletRequest);
			}
		}
		chain.doFilter(request, response);
	}
	
	public boolean logUserAction(TrackerUser trackerUser, String path, HttpServletRequest httpServletRequest){
		try{
			UserAction userAction = new UserAction();
			userAction.setAction(path);
			userAction.setTimeStamp(new Date());
			//userAction.setTrackerUser(trackerUser);
			userAction.setUserId(trackerUser.getId());
			userAction.setIpAddress(httpServletRequest.getRemoteAddr());
			userAction.setUserName(trackerUser.getUserName());
			DAORegistry.getUserActionDAO().save(userAction);
			return true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

}
