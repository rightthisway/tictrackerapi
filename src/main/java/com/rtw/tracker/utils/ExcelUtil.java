package com.rtw.tracker.utils;

import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

public class ExcelUtil {

	public static void generateExcelHeaderRow(List<String> headerList, Sheet ssSheet){
		int j = 0;
		Row rowhead =   ssSheet.createRow((int)0);
		for(String header : headerList){
			rowhead.createCell((int) j).setCellValue(header);
		    j++;
		}
	}
	
	public static void generateExcelData(List<Object[]> dataList, Sheet ssSheet){
		int j = 0;
		int rowCount = 1;
		for(Object[] dataObj : dataList){
			Row rowhead =   ssSheet.createRow((int)rowCount);
			rowCount++;
			j = 0;
			for(Object data : dataObj){
				if(data != null){
					rowhead.createCell((int) j).setCellValue(data.toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
			    j++;
			}
		}
	}
}
