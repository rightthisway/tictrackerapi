package com.rtw.tracker.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.VerticalPositionMark;
import com.rtw.tmat.utils.Util;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.OrderTicketGroupDetails;
import com.rtw.tracker.datas.CategoryTicketGroup;
import com.rtw.tracker.datas.Customer;
import com.rtw.tracker.datas.CustomerAddress;
import com.rtw.tracker.datas.CustomerOrder;
import com.rtw.tracker.datas.CustomerOrderDetails;
import com.rtw.tracker.datas.Event;
import com.rtw.tracker.datas.Invoice;
import com.rtw.tracker.datas.OpenOrders;
import com.rtw.tracker.datas.POSCategoryTicketGroup;
import com.rtw.tracker.datas.POSCustomerOrder;
import com.rtw.tracker.datas.POSInvoice;
import com.rtw.tracker.datas.POSPurchaseOrder;
import com.rtw.tracker.datas.POSTicketGroup;
import com.rtw.tracker.datas.PurchaseOrder;
import com.rtw.tracker.datas.PurchaseOrders;
import com.rtw.tracker.datas.Ticket;
import com.rtw.tracker.datas.TicketGroup;
import com.rtw.tracker.datas.Venue;
import com.rtw.tracker.enums.PartialPaymentMethod;
import com.rtw.tracker.enums.PaymentMethod;

public class PDFUtil {

	// Set Font 
	/*
	static Font redFont = new Font(FontFamily.HELVETICA, 10, Font.BOLD,
			BaseColor.RED);
	static Font redBigFont = new Font(FontFamily.HELVETICA, 20, Font.BOLD,
			BaseColor.RED);
	static Font redBoldFont = new Font(FontFamily.HELVETICA, 16, Font.BOLD,
			BaseColor.RED);
	static Font boldFont = new Font(FontFamily.HELVETICA, 16, Font.BOLD,
			BaseColor.BLACK);
	static Font boldFontSmall = new Font(FontFamily.HELVETICA, 10, Font.BOLD,
			BaseColor.BLACK);
	static Font regularFont = new Font(FontFamily.COURIER, 8, Font.NORMAL,
			BaseColor.BLACK);
	static Font regularBoldFont = new Font(FontFamily.COURIER, 8, Font.BOLD,
			BaseColor.BLACK);
	static Font regularBoldFontTimesRoman = new Font(FontFamily.HELVETICA, 8,
			Font.BOLD, BaseColor.BLACK);
	*/
	
	static Font redFont = FontFactory.getFont("//windows//fonts//arial.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 8f, Font.NORMAL, BaseColor.RED);
	BaseFont baseFont = redFont.getBaseFont();
	static Font redBigFont = redFont;
	static Font redBoldFont = redFont;
	static Font boldFont = FontFactory.getFont("//windows//fonts//arial.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 8f, Font.NORMAL, BaseColor.BLACK);
	BaseFont baseFont1 = boldFont.getBaseFont();
	static Font boldFontSmall = boldFont;
	static Font regularFont = boldFont;
	static Font regularBoldFont = boldFont;
	static Font regularBoldFontTimesRoman = boldFont;
	
	static DateFormat dateTimeFormat = new SimpleDateFormat(
			"MM-dd-yyyy hh:mm aa");
	static DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	static DateFormat dateFormate3 = new SimpleDateFormat(
			"MMM-dd-yyyy hh:mm aa");

	public static Document generateInvoicePdf(Invoice invoice,
			CategoryTicketGroup catTicketGroup, List<OrderTicketGroupDetails> ticketGroupDetails,
			CustomerOrderDetails orderDetail,
			CustomerOrder order) {
		Document document = null;
		File file = null;
		FileOutputStream pdfFileout = null;
		try {

			document = new Document(PageSize.A4);
			file = new File(DAORegistry.getPropertyDAO()
					.get("tictracker.invoice.pdffile.path").getValue()
					+ "\\invoice_" + invoice.getId() + ".pdf");
			pdfFileout = new FileOutputStream(file);
			PdfWriter.getInstance(document, pdfFileout);
			document.open();

			// Generate Image in Pdf
			//Image img = Image.getInstance("C:\\Tomcat 7.0\\webapps\\ROOT\\resources\\images\\rtflogo1.png");
			// //prajendran local address
			Image img = Image.getInstance(DAORegistry.getPropertyDAO().get("rtf.logo.image.path").getValue());
			img.setAlignment(Image.LEFT | Image.TEXTWRAP);
			img.scalePercent(70);
			document.add(img);
			document.add(AddEmptyParagraph());
			document.add(AddEmptyParagraph());

			PdfPTable officeInfoTable = new PdfPTable(1);
			officeInfoTable.setHorizontalAlignment(Element.ALIGN_RIGHT);
			officeInfoTable.setWidthPercentage(35f);
			PdfPCell officeInfoCell1 = new PdfPCell(new Paragraph(
					"Reward The Fan", regularBoldFont));
			officeInfoCell1.setBorder(Rectangle.NO_BORDER);
			officeInfoTable.addCell(officeInfoCell1);

			PdfPCell officeInfoCell2 = new PdfPCell(new Paragraph(
					"10 Times Square", regularBoldFont));
			officeInfoCell2.setBorder(Rectangle.NO_BORDER);
			officeInfoTable.addCell(officeInfoCell2);

			PdfPCell officeInfoCell3 = new PdfPCell(new Paragraph(
					"3rd Floor", regularBoldFont));
			officeInfoCell3.setBorder(Rectangle.NO_BORDER);
			officeInfoTable.addCell(officeInfoCell3);

			PdfPCell officeInfoCell4 = new PdfPCell(new Paragraph(
					"New York NY 10018", regularBoldFont));
			officeInfoCell4.setBorder(Rectangle.NO_BORDER);
			officeInfoTable.addCell(officeInfoCell4);

			PdfPCell officeInfoCell5 = new PdfPCell(new Paragraph(
					"Phone:800-601-6100", regularBoldFont));
			officeInfoCell5.setBorder(Rectangle.NO_BORDER);
			officeInfoTable.addCell(officeInfoCell5);

			/*PdfPCell officeInfoCell6 = new PdfPCell(new Paragraph("Fax: ",
					regularBoldFont));
			officeInfoCell6.setBorder(Rectangle.NO_BORDER);
			officeInfoTable.addCell(officeInfoCell6);*/

			PdfPCell officeInfoCell7 = new PdfPCell(new Paragraph(
					"Email:sales@rewardthefan.com", regularBoldFont));
			officeInfoCell7.setBorder(Rectangle.NO_BORDER);
			officeInfoTable.addCell(officeInfoCell7);

			PdfPCell officeInfoCell8 = new PdfPCell(new Paragraph(
					"Sales Person: Web", regularFont));
			officeInfoCell8.setBorder(Rectangle.NO_BORDER);
			officeInfoCell8.setVerticalAlignment(Element.ALIGN_BOTTOM);
			officeInfoTable.addCell(officeInfoCell8);

			document.add(officeInfoTable);

			String invoiceType = "";
			if(!order.getPrimaryPaymentMethod().equals(PaymentMethod.ACCOUNT_RECIVABLE)){
				invoiceType = "PAID";
			}

			Paragraph invoiceTypeParagraph = new Paragraph();
			invoiceTypeParagraph.setAlignment(Element.ALIGN_CENTER);
			invoiceTypeParagraph.add(new Chunk(invoiceType, redBigFont));
			document.add(invoiceTypeParagraph);

			Integer reprint = 0;// MITUL
			if (reprint == 1) {
				Paragraph reprintParagraph = new Paragraph("Reprint         ",
						redFont);
				reprintParagraph.add(new Chunk("i n v o i c e #:"
						+ invoice.getId().toString(), boldFontSmall));
				reprintParagraph.setIndentationLeft(400);
				document.add(reprintParagraph);
			} else {
				Paragraph reprintParagraph = new Paragraph("i  n v o i c e #:"
						+ invoice.getId().toString(), boldFontSmall);
				reprintParagraph.setIndentationLeft(400);
				document.add(reprintParagraph);
			}

			Paragraph salesPersonParagraph = new Paragraph("Sales Date: "
					+ dateTimeFormat.format(invoice.getCreatedDate()),
					regularFont);
			document.add(salesPersonParagraph);
			
			document.add(AddEmptyParagraph());
			Customer customer = DAORegistry.getCustomerDAO().get(order.getCustomerId());
			float[] columnWidth = { 45f, 10f, 45f };
			PdfPTable billToTable = new PdfPTable(columnWidth);
			billToTable.setWidthPercentage(100f);
			billToTable.setHorizontalAlignment(Element.ALIGN_LEFT);
			PdfPCell billToTitleCell = new PdfPCell(new Paragraph("Bill To",
					boldFontSmall));
			billToTitleCell.setBorder(Rectangle.RIGHT | Rectangle.LEFT
					| Rectangle.TOP);
			billToTitleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			billToTitleCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			billToTable.addCell(billToTitleCell);
			billToTable.addCell(getCell("", regularFont));
			PdfPCell shipToTitleCell = new PdfPCell(new Paragraph("Ship To",
					boldFontSmall));
			shipToTitleCell.setBorder(Rectangle.RIGHT | Rectangle.LEFT
					| Rectangle.TOP);
			shipToTitleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			shipToTitleCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			billToTable.addCell(shipToTitleCell);

			billToTable.addCell(getCell((orderDetail.getBlFirstName()!=null ? orderDetail.getBlFirstName() : (orderDetail.getShFirstName()!=null ? orderDetail.getShFirstName() : "")) + " "
					+ (orderDetail.getBlLastName()!=null ? orderDetail.getBlLastName() : (orderDetail.getShLastName()!=null ? orderDetail.getShLastName() : "")), regularFont));
			billToTable.addCell(getCell("", regularFont));
			billToTable.addCell(getCell((orderDetail.getShFirstName()!=null ? orderDetail.getShFirstName() : "") + " "
					+ (orderDetail.getShLastName()!=null ? orderDetail.getShLastName() : ""), regularFont));
			if(customer.getCompanyName()!=null && !customer.getCompanyName().isEmpty()){
				billToTable.addCell(getCell(customer.getCompanyName(), regularFont));
				billToTable.addCell(getCell("", regularFont));
				billToTable.addCell(getCell(customer.getCompanyName(), regularFont));
			}
			billToTable.addCell(getCell((orderDetail.getBlAddress1()!=null ? orderDetail.getBlAddress1() : (orderDetail.getShAddress1()!=null ? orderDetail.getShAddress1() : "")),
					regularFont));
			billToTable.addCell(getCell("", regularFont));
			billToTable.addCell(getCell((orderDetail.getShAddress1()!=null ? orderDetail.getShAddress1() : ""),
					regularFont));
			if((orderDetail.getBlAddress2()!= null && !orderDetail.getBlAddress2().isEmpty()) 
					|| (orderDetail.getShAddress2()!=null && !orderDetail.getShAddress2().isEmpty())){
				billToTable.addCell(getCell((orderDetail.getBlAddress2()!=null ? orderDetail.getBlAddress2() : (orderDetail.getShAddress2()!=null ? orderDetail.getShAddress2() : "")),
						regularFont));
				billToTable.addCell(getCell("", regularFont));
			}
			
			if(orderDetail.getShAddress2()!=null && !orderDetail.getShAddress2().isEmpty()){
				billToTable.addCell(getCell((orderDetail.getShAddress2()!=null ? orderDetail.getShAddress2() : ""),
						regularFont));
			}
			billToTable.addCell(getCell((orderDetail.getBlCity()!=null ? orderDetail.getBlCity() : (orderDetail.getShCity()!=null ? orderDetail.getShCity() : "")) + ", "
							+ (orderDetail.getBlStateName()!=null ? orderDetail.getBlStateName() : (orderDetail.getShStateName()!=null ? orderDetail.getShStateName() : "")) + ", "
							+ (orderDetail.getBlZipCode()!=null ? orderDetail.getBlZipCode() : (orderDetail.getShZipCode()!=null ? orderDetail.getShZipCode() : "")), regularFont));
			billToTable.addCell(getCell("", regularFont));
			billToTable.addCell(getCell((orderDetail.getShCity()!=null ? orderDetail.getShCity() : "") + ", "
							+ (orderDetail.getShStateName()!=null ? orderDetail.getShStateName() : "") + ", "
							+ (orderDetail.getShZipCode()!=null ? orderDetail.getShZipCode() : ""), regularFont));
			billToTable.addCell(getCell((orderDetail.getBlCountryName()!=null ? orderDetail.getBlCountryName() : (orderDetail.getShCountryName()!=null ? orderDetail.getShCountryName() : "")),
					regularFont));
			billToTable.addCell(getCell("", regularFont));
			billToTable.addCell(getCell((orderDetail.getShCountryName()!=null ? orderDetail.getShCountryName() : ""),
					regularFont));
			billToTable.addCell(getBorderCell("Billing Phone: " + (orderDetail.getBlPhone1()!=null ? orderDetail.getBlPhone1() : (orderDetail.getShPhone1()!=null ? orderDetail.getShPhone1() : "")), 
					regularFont));
			billToTable.addCell(getCell("", regularFont));
			billToTable.addCell(getBorderCell("Shipping Phone: " + (orderDetail.getShPhone1()!=null ? orderDetail.getShPhone1() : ""),
							regularFont));
			if (invoice.getRealTixShippingMethod() != null
					&& !invoice.getRealTixShippingMethod().isEmpty()) {
				billToTable.addCell(getBorderCell("", regularFont));
				billToTable.addCell(getCell("", regularFont));
				billToTable.addCell(getBorderCell(
						invoice.getRealTixShippingMethod(), regularBoldFont));
			}
			if (invoice.getTrackingNo() != null
					&& !invoice.getTrackingNo().isEmpty()) {
				billToTable.addCell(getBorderCell("", regularFont));
				billToTable.addCell(getCell("", regularFont));
				billToTable.addCell(getBorderCell(
						"Tracking #: " + invoice.getTrackingNo(),
						regularBoldFont));
			}
			billToTable.addCell(getBorderCell("Customer Id: "+ invoice.getCustomerId(),regularFont));
			billToTable.addCell(getNoBorderCell(" ",regularFont));
			billToTable.addCell(getBorderCell(" ",regularFont));
			document.add(billToTable);

			document.add(AddEmptyParagraph());

			PdfPTable ticketInfoHeaderTable = new PdfPTable(7);
			ticketInfoHeaderTable.setWidthPercentage(100f);
			ticketInfoHeaderTable.addCell(getBottomBorderCell("QTY",
					regularBoldFont));
			ticketInfoHeaderTable.addCell(getBottomBorderCell("SEAT TYPE/OBS",
					regularBoldFont));
			ticketInfoHeaderTable.addCell(getBottomBorderCell("SECTION",
					regularBoldFont));
			ticketInfoHeaderTable.addCell(getBottomBorderCell("ROW",
					regularBoldFont));
			ticketInfoHeaderTable.addCell(getBottomBorderCell("SEATS",
					regularBoldFont));
			ticketInfoHeaderTable.addCell(getBottomBorderCell("SALE PRICE",
					regularBoldFont));
			ticketInfoHeaderTable.addCell(getBottomBorderCell("TOTAL",
					regularBoldFont));
			document.add(ticketInfoHeaderTable);

			StringBuilder eventInfo = new StringBuilder();
			eventInfo.append(order.getEventName());
			eventInfo.append(" ");
			eventInfo.append(dateFormat.format(order.getEventDate()));
			eventInfo.append(" ");
			eventInfo.append(order.getEventTimeStr());			
			eventInfo.append(" At ");
			eventInfo.append(order.getVenueName());
			eventInfo.append(" ");
			eventInfo.append(order.getVenueState());
			document.add(new Paragraph(eventInfo.toString(), regularBoldFont));

			String seat = "";
			PdfPTable ticketInfoTable = new PdfPTable(7);
			ticketInfoTable.setWidthPercentage(100f);

			if (ticketGroupDetails != null && !ticketGroupDetails.isEmpty()) {
				for(OrderTicketGroupDetails ticGroup : ticketGroupDetails){
					ticketInfoTable.addCell(getNoBorderCell(
							ticGroup.getQuantity(), regularFont));
					ticketInfoTable.addCell(getNoBorderCell("", regularFont));
					ticketInfoTable.addCell(getNoBorderCell(
							ticGroup.getSection(), regularFont));
					ticketInfoTable.addCell(getNoBorderCell(ticGroup.getRow(),
							regularFont));
					ticketInfoTable.addCell(getNoBorderCell(ticGroup.getSeat(), regularFont));
					ticketInfoTable.addCell(getNoBorderCell(
							Util.roundOffDouble(invoice.getInvoiceTotal()/invoice.getTicketCount()), regularFont));
					ticketInfoTable.addCell(getNoBorderCell(Util.roundOffDouble((invoice.getInvoiceTotal()/invoice.getTicketCount()) * ticGroup.getQuantity() ),
							regularFont));
				}
				document.add(ticketInfoTable);
			} else if(catTicketGroup != null){
				ticketInfoTable.addCell(getNoBorderCell(
						catTicketGroup.getSoldQuantity(), regularFont));
				ticketInfoTable.addCell(getNoBorderCell("", regularFont));
				ticketInfoTable.addCell(getNoBorderCell(
						catTicketGroup.getSection(), regularFont));
				ticketInfoTable.addCell(getNoBorderCell(
						catTicketGroup.getRow(), regularFont));
				if (catTicketGroup.getSeatLow() != null && !catTicketGroup.getSeatLow().isEmpty()) {
					seat = catTicketGroup.getSeatLow();
					if (catTicketGroup.getSeatHigh() != null && !catTicketGroup.getSeatHigh().isEmpty())
						seat += "-" + catTicketGroup.getSeatHigh();
				}
				ticketInfoTable.addCell(getNoBorderCell(seat, regularFont));
				ticketInfoTable.addCell(getNoBorderCell(
						Util.roundOffDouble(invoice.getInvoiceTotal()/invoice.getTicketCount()), regularFont));
				
				ticketInfoTable.addCell(getNoBorderCell(Util.roundOffDouble(invoice.getInvoiceTotal()), regularFont));
				document.add(ticketInfoTable);
			}else{
				ticketInfoTable.addCell(getNoBorderCell(
						order.getQty(), regularFont));
				ticketInfoTable.addCell(getNoBorderCell("", regularFont));
				ticketInfoTable.addCell(getNoBorderCell(
						order.getSection(), regularFont));
				ticketInfoTable.addCell(getNoBorderCell(
						(order.getRow()!=null ? order.getRow() : ""), regularFont));
				if (order.getSeatLow() != null) {
					seat = order.getSeatLow();
					if (order.getSeatHigh() != null)
						seat += "-" + order.getSeatHigh();
				}
				ticketInfoTable.addCell(getNoBorderCell(seat, regularFont));
				ticketInfoTable.addCell(getNoBorderCell(
						Util.roundOffDouble((invoice.getInvoiceTotal()/invoice.getTicketCount())), regularFont));
				ticketInfoTable.addCell(getNoBorderCell(Util.roundOffDouble(invoice.getInvoiceTotal()), regularFont));
				document.add(ticketInfoTable);
			}
			document.add(AddEmptyParagraph());

			float[] cellWidths = { 60f};
			PdfPTable paymentContactTable = new PdfPTable(cellWidths);
			paymentContactTable.setWidthPercentage(60f);
			paymentContactTable.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell paymentTitleCell = new PdfPCell(new Paragraph(
					"Payment Information", boldFontSmall));
			paymentTitleCell.setBorder(Rectangle.LEFT | Rectangle.TOP
					| Rectangle.RIGHT);
			paymentTitleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			paymentTitleCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			paymentContactTable.addCell(paymentTitleCell);

			String primaryPaymentMethod = "";
			String secondaryMethod = "";
			String thirdMethod = "";
			String primaryPaymentAmount = "";
			String secondaryPaymentAmount = "";
			String thirdPaymentAmount = "";
			if(order.getPrimaryPaymentMethod() != null && order.getPrimaryPaymentMethod().equals(PaymentMethod.BANK_OR_CHEQUE)){
				primaryPaymentMethod = "BANK_OR_CHECK";
				primaryPaymentAmount = order.getPrimaryPayAmt().toString();
			}else{
				primaryPaymentMethod = order.getPrimaryPaymentMethod().toString();
				primaryPaymentAmount = order.getPrimaryPayAmt().toString();
			}
			if(order.getSecondaryPaymentMethod() != null && !order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.NULL)){
				secondaryMethod = order.getSecondaryPaymentMethod().toString();
				secondaryPaymentAmount = order.getSecondaryPayAmt().toString();
			}
			if(order.getThirdPaymentMethod() != null && !order.getThirdPaymentMethod().equals(PartialPaymentMethod.NULL)){
				thirdMethod = order.getThirdPaymentMethod().toString();
				thirdPaymentAmount = order.getThirdPayAmt().toString();
			}
			
			
			PdfPCell cell1 = new PdfPCell();
			cell1.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
			cell1.addElement(new Paragraph("", boldFontSmall));

			PdfPCell cell3 = new PdfPCell();
			cell3.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
			cell3.addElement(new Paragraph("Payment Methods :", boldFontSmall));
			
			Chunk moveTextRight = new Chunk(new VerticalPositionMark());
			
			PdfPCell cell4 = new PdfPCell();
			cell4.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
			 
	        Paragraph p = new Paragraph(primaryPaymentMethod, regularBoldFont);
	        p.add(new Chunk(moveTextRight));
	        p.add(primaryPaymentAmount);
	        cell4.addElement(p);		        
			//cell4.addElement(new Paragraph(primaryPaymentMethod+"                  "+primaryPaymentAmount, regularBoldFont));
			
			PdfPCell cell5 = new PdfPCell();
			cell5.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
			
			Paragraph p1 = new Paragraph(secondaryMethod, regularBoldFont);
	        p1.add(new Chunk(moveTextRight));
	        p1.add(secondaryPaymentAmount);
	        cell5.addElement(p1);
			//cell5.addElement(new Paragraph(secondaryMethod+"                  "+secondaryPaymentAmount, regularBoldFont));
			
			PdfPCell cell6 = new PdfPCell();
			cell6.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
			
			Paragraph p2 = new Paragraph(thirdMethod, regularBoldFont);
	        p2.add(new Chunk(moveTextRight));
	        p2.add(thirdPaymentAmount);
	        cell6.addElement(p2);
			//cell6.addElement(new Paragraph(thirdMethod+"                  "+thirdPaymentAmount, regularBoldFont));
			
			
			
			paymentContactTable.addCell(cell1);
			paymentContactTable.addCell(cell3);
			paymentContactTable.addCell(cell4);
			paymentContactTable.addCell(cell5);
			paymentContactTable.addCell(cell6);

			/*PdfPCell contactTitleCell = new PdfPCell(new Paragraph(
					"Contact Information", boldFontSmall));
			contactTitleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			contactTitleCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			paymentContactTable.addCell(contactTitleCell);*/

			document.add(paymentContactTable);

			document.add(AddEmptyParagraph());
			
			
			float[] invoiceNoteWidth = { 60f};
			PdfPTable invoiceNote = new PdfPTable(invoiceNoteWidth);
			invoiceNote.setWidthPercentage(60f);
			invoiceNote.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell invoiceNoteTitle = new PdfPCell(new Paragraph(
					"Invoice Note", boldFontSmall));
			invoiceNoteTitle.setBorder(Rectangle.LEFT | Rectangle.TOP
					| Rectangle.RIGHT);
			invoiceNoteTitle.setHorizontalAlignment(Element.ALIGN_LEFT);
			invoiceNoteTitle.setBackgroundColor(BaseColor.LIGHT_GRAY);
			invoiceNote.addCell(invoiceNoteTitle);

			PdfPCell invoiceNote1 = new PdfPCell();
			invoiceNote1.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
			invoiceNote1.addElement(new Paragraph("", regularBoldFont));
			
			PdfPCell invoiceNote2 = new PdfPCell();
			invoiceNote2.setBorder(Rectangle.RIGHT | Rectangle.LEFT );
			invoiceNote2.addElement(new Paragraph(invoice.getInternalNote()!=null?invoice.getInternalNote():"", regularBoldFont));
			
			PdfPCell invoiceNote3 = new PdfPCell();
			invoiceNote3.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
			invoiceNote3.addElement(new Paragraph("", regularBoldFont));
			
			invoiceNote.addCell(invoiceNote1);
			invoiceNote.addCell(invoiceNote2);
			invoiceNote.addCell(invoiceNote3);

			/*PdfPCell contactTitleCell = new PdfPCell(new Paragraph(
					"Contact Information", boldFontSmall));
			contactTitleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			contactTitleCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			paymentContactTable.addCell(contactTitleCell);*/

			document.add(invoiceNote);
			
			document.add(AddEmptyParagraph());
			
			
			float[] shippingNoteWidth = { 60f};
			PdfPTable shippingNote = new PdfPTable(shippingNoteWidth);
			shippingNote.setWidthPercentage(60f);
			shippingNote.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell shippingNoteTitle = new PdfPCell(new Paragraph(
					"Shipping Note", boldFontSmall));
			shippingNoteTitle.setBorder(Rectangle.LEFT | Rectangle.TOP
					| Rectangle.RIGHT);
			shippingNoteTitle.setHorizontalAlignment(Element.ALIGN_LEFT);
			shippingNoteTitle.setBackgroundColor(BaseColor.LIGHT_GRAY);
			shippingNote.addCell(shippingNoteTitle);

			PdfPCell shippingNote1 = new PdfPCell();
			shippingNote1.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
			shippingNote1.addElement(new Paragraph("", regularBoldFont));
			
			PdfPCell shippingNote2 = new PdfPCell();
			shippingNote2.setBorder(Rectangle.RIGHT | Rectangle.LEFT );
			shippingNote2.addElement(new Paragraph("", regularBoldFont));
			
			PdfPCell shippingNote3 = new PdfPCell();
			shippingNote3.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
			shippingNote3.addElement(new Paragraph("", regularBoldFont));
			
			shippingNote.addCell(shippingNote1);
			shippingNote.addCell(shippingNote2);
			shippingNote.addCell(shippingNote3);
			document.add(shippingNote);
			
			
			document.add(AddEmptyParagraph());
			

			Double subTotal = null;
			String invoiceShipping = "0.0"; // MITUL
			Double invoiceTax = Util.getRoundedValue(order.getTaxes()!=null?order.getTaxes():0.0); // MITUL
			Double expenseInfo = 0.0; // MITUL
			if (invoice.getInvoiceTotal() != null) {
				subTotal = invoice.getInvoiceTotal();
			}
			if (subTotal != null) {
				if (invoiceShipping != null)
					subTotal = subTotal - Double.parseDouble(invoiceShipping);
				if (invoiceTax != null)
					subTotal = subTotal - invoiceTax;
				if (expenseInfo != null)
					subTotal = subTotal - expenseInfo;
			}
			float[] cellWidth = { 20f, 15f };
			PdfPTable expenseInfoTable = new PdfPTable(cellWidth);
			expenseInfoTable.setWidthPercentage(35f);
			expenseInfoTable.setHorizontalAlignment(Element.ALIGN_RIGHT);
			/*expenseInfoTable.addCell(getRigthAlignedNoBorderCell("SUBTOTAL: ",
					regularBoldFont));
			expenseInfoTable.addCell(getRigthAlignedBorderCell("$" + subTotal,
					regularFont));
			expenseInfoTable.addCell(getRigthAlignedNoBorderCell("SHIPPING: ",
					regularBoldFont));
			expenseInfoTable.addCell(getRigthAlignedBorderCell("$"
					+ invoiceShipping, regularFont));
			expenseInfoTable.addCell(getRigthAlignedNoBorderCell(
					"EXTERNAL PO: ", regularBoldFont));
			expenseInfoTable.addCell(getRigthAlignedBorderCell(
					invoice.getExtPONo(), regularFont));
			expenseInfoTable.addCell(getRigthAlignedNoBorderCell("FEES: ",
					regularBoldFont));
			expenseInfoTable.addCell(getRigthAlignedBorderCell(
					"$" + invoiceTax, regularFont));
			expenseInfoTable.addCell(getRigthAlignedNoBorderCell(
					"OTHER EXPENSES: ", regularBoldFont));
			expenseInfoTable.addCell(getRigthAlignedBorderCell("$"
					+ expenseInfo, regularFont));*/
			expenseInfoTable.addCell(getRigthAlignedNoBorderCell(
					"GRAND TOTAL: ", regularBoldFont));
			expenseInfoTable.addCell(getRigthAlignedBorderCell(
					"$" + Util.roundOffDouble(invoice.getInvoiceTotal()), regularBoldFont));
			/*expenseInfoTable.addCell(getRigthAlignedNoBorderCell(
					"BALANCE DUE: ", regularBoldFont));*/
			// expenseInfoTable.addCell(getRigthAlignedBorderCell(invoiceBalanceDue,
			// regularFont));
			document.add(expenseInfoTable);
			document.close();
			pdfFileout.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return document;
	}

	public static Document generateRTWInvoicePdf(Collection<OpenOrders> invoice,
			Map<Integer, List<POSCategoryTicketGroup>> categoryTicketMap,Map<Integer, List<POSTicketGroup>> ticketMap, CustomerAddress custBillingAddress,
			List<CustomerAddress> custShippingAddress, POSCustomerOrder order) {
		Document document = null;
		File file = null;
		FileOutputStream pdfFileout = null;
		String productType = invoice.iterator().next().getProductType();
		try {
			
			OpenOrders openOrderInvoice = null;
			Set<Integer> invoiceIds = new HashSet<Integer>();	

			for (OpenOrders openInvoice : invoice) {
				if (invoiceIds.add(openInvoice.getInvoiceId())) {
					openOrderInvoice = openInvoice;
				}
			}
			
			document = new Document(PageSize.A4);
			file = new File(DAORegistry.getPropertyDAO()
					.get("tictracker.invoice.pdffile.path").getValue()
					+ "\\invoice_" + openOrderInvoice.getInvoiceId() + ".pdf");
			pdfFileout = new FileOutputStream(file);
			PdfWriter.getInstance(document, pdfFileout);
			document.open();

			// Generate Image in Pdf
			//Image img = Image.getInstance("C:\\Tomcat 7.0\\webapps\\tictracker-0.1\\images\\bg-rtw.gif");
			// //prajendran local address
			Image img = Image.getInstance(DAORegistry.getPropertyDAO().get("rtw.logo.image.path").getValue());
			img.setAlignment(Image.LEFT | Image.TEXTWRAP);
			img.scalePercent(57);
			document.add(img);

			PdfPTable officeInfoTable = new PdfPTable(1);
			officeInfoTable.setHorizontalAlignment(Element.ALIGN_RIGHT);
			officeInfoTable.setWidthPercentage(50f);
			PdfPCell officeInfoCell1 = new PdfPCell(new Paragraph(
					"Right This Way", regularBoldFont));
			officeInfoCell1.setBorder(Rectangle.NO_BORDER);
			officeInfoTable.addCell(officeInfoCell1);

			PdfPCell officeInfoCell2 = new PdfPCell(new Paragraph(
					"1441 Broadway", regularBoldFont));
			officeInfoCell2.setBorder(Rectangle.NO_BORDER);
			officeInfoTable.addCell(officeInfoCell2);

			PdfPCell officeInfoCell3 = new PdfPCell(new Paragraph(
					"3rd Floor, Suite 3012", regularBoldFont));
			officeInfoCell3.setBorder(Rectangle.NO_BORDER);
			officeInfoTable.addCell(officeInfoCell3);

			PdfPCell officeInfoCell4 = new PdfPCell(new Paragraph(
					"New York, NY 10018", regularBoldFont));
			officeInfoCell4.setBorder(Rectangle.NO_BORDER);
			officeInfoTable.addCell(officeInfoCell4);

			PdfPCell officeInfoCell5 = new PdfPCell(new Paragraph(
					"Phone:(646)307-4277", regularBoldFont));
			officeInfoCell5.setBorder(Rectangle.NO_BORDER);
			officeInfoTable.addCell(officeInfoCell5);

			PdfPCell officeInfoCell6 = new PdfPCell(new Paragraph("Fax: ",
					regularBoldFont));
			officeInfoCell6.setBorder(Rectangle.NO_BORDER);
			officeInfoTable.addCell(officeInfoCell6);

			PdfPCell officeInfoCell7 = new PdfPCell(new Paragraph(
					"Email:sales@rightthisway.com", regularBoldFont));
			officeInfoCell7.setBorder(Rectangle.NO_BORDER);
			officeInfoTable.addCell(officeInfoCell7);

			PdfPCell officeInfoCell8 = new PdfPCell(new Paragraph(
					"Sales Person: Web",
					regularFont));
			officeInfoCell8.setBorder(Rectangle.NO_BORDER);
			officeInfoCell8.setVerticalAlignment(Element.ALIGN_BOTTOM);
			officeInfoTable.addCell(officeInfoCell8);
			document.add(officeInfoTable);
			String invoiceType = "";
			POSInvoice inv = DAORegistry.getPosInvoiceDAO().getInvoiceByProductTypeAndId(openOrderInvoice.getInvoiceId(), productType);
			if(inv.getInvoiceBalanceDue()==0.0){
				invoiceType = "PAID";
			}
			
			Paragraph invoiceTypeParagraph = new Paragraph();
			invoiceTypeParagraph.setAlignment(Element.ALIGN_CENTER);
			invoiceTypeParagraph.add(new Chunk(invoiceType, redBigFont));
			document.add(invoiceTypeParagraph);

			Integer reprint = 0;// Need to discuss.
			if (reprint == 1) {
				Paragraph reprintParagraph = new Paragraph("Reprint         ",
						redFont);
				reprintParagraph.add(new Chunk("i n v o i c e #:"
						+ openOrderInvoice.getInvoiceId().toString(),
						boldFontSmall));
				reprintParagraph.setIndentationLeft(400);
				document.add(reprintParagraph);
			} else {
				Paragraph reprintParagraph = new Paragraph("i  n v o i c e #:"
						+ openOrderInvoice.getInvoiceId().toString(),
						boldFontSmall);
				reprintParagraph.setIndentationLeft(400);
				document.add(reprintParagraph);
			}
			
			Paragraph salesPersonParagraph = new Paragraph("Sales Date: "
					+ dateTimeFormat.format(openOrderInvoice.getCreatedDate()),
					regularFont);
			document.add(salesPersonParagraph);

			document.add(AddEmptyParagraph());

			float[] columnWidth = { 45f, 10f, 45f };
			PdfPTable billToTable = new PdfPTable(columnWidth);
			billToTable.setWidthPercentage(100f);
			billToTable.setHorizontalAlignment(Element.ALIGN_LEFT);
			PdfPCell billToTitleCell = new PdfPCell(new Paragraph("Bill To",
					boldFontSmall));
			billToTitleCell.setBorder(Rectangle.RIGHT | Rectangle.LEFT
					| Rectangle.TOP);
			billToTitleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			billToTitleCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			billToTable.addCell(billToTitleCell);
			billToTable.addCell(getCell("", regularFont));
			PdfPCell shipToTitleCell = new PdfPCell(new Paragraph("Ship To",
					boldFontSmall));
			shipToTitleCell.setBorder(Rectangle.RIGHT | Rectangle.LEFT
					| Rectangle.TOP);
			shipToTitleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			shipToTitleCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			billToTable.addCell(shipToTitleCell);
			
			if(custBillingAddress != null || custShippingAddress != null){
				billToTable.addCell(getCell((custBillingAddress.getFirstName()!= null ? custBillingAddress.getFirstName() : (custShippingAddress.get(0).getFirstName()!=null ? custShippingAddress.get(0).getFirstName() : ""))+
						" " + (custBillingAddress.getLastName()!=null ? custBillingAddress.getLastName() : (custShippingAddress.get(0).getLastName()!=null ? custShippingAddress.get(0).getLastName() : "")),
						regularFont));
				billToTable.addCell(getCell("", regularFont));
				
				if(custShippingAddress != null && !custShippingAddress.isEmpty()){
					billToTable.addCell(getCell((custShippingAddress.get(0).getFirstName()!=null ? custShippingAddress.get(0).getFirstName() : (custBillingAddress.getFirstName()!= null ? custBillingAddress.getFirstName() : ""))
							+ " " + (custShippingAddress.get(0).getLastName()!=null ? custShippingAddress.get(0).getLastName() : (custBillingAddress.getLastName()!=null ? custBillingAddress.getLastName() : "")),
							regularFont));
				}else{
					billToTable.addCell(getCell((custBillingAddress.getFirstName()!= null ? custBillingAddress.getFirstName() : "") + " " + (custBillingAddress.getLastName()!=null ? custBillingAddress.getLastName() : ""), regularFont));
				}
				
				billToTable.addCell(getCell((custBillingAddress.getAddressLine1()!=null ? custBillingAddress.getAddressLine1() : (custShippingAddress.get(0).getAddressLine1()!=null ? custShippingAddress.get(0).getAddressLine1() : "")),
						regularFont));
				billToTable.addCell(getCell("", regularFont));
				
				if(custShippingAddress != null && !custShippingAddress.isEmpty()){
					billToTable.addCell(getCell((custShippingAddress.get(0).getAddressLine1()!=null ? custShippingAddress.get(0).getAddressLine1() : (custBillingAddress.getAddressLine1()!=null ? custBillingAddress.getAddressLine1() : "")),
						regularFont));
				}else{
					billToTable.addCell(getCell((custBillingAddress.getAddressLine1()!=null ? custBillingAddress.getAddressLine1() : ""), regularFont));
				}
				
				billToTable.addCell(getCell((custBillingAddress.getAddressLine2()!=null ? custBillingAddress.getAddressLine2() : ""),
						regularFont));
				billToTable.addCell(getCell("", regularFont));
				
				if(custShippingAddress != null && !custShippingAddress.isEmpty()){
					billToTable.addCell(getCell((custShippingAddress.get(0).getAddressLine2()!=null ? custShippingAddress.get(0).getAddressLine2() : ""),
						regularFont));
				}else{
					billToTable.addCell(getCell((custBillingAddress.getAddressLine2()!=null ? custBillingAddress.getAddressLine2() : ""), regularFont));
				}
				
				billToTable.addCell(getCell((custBillingAddress.getCity()!=null ? custBillingAddress.getCity(): (custShippingAddress.get(0).getCity()!=null ? custShippingAddress.get(0).getCity() : "")) + ", "
						+ (custBillingAddress.getStateName()!=null ? custBillingAddress.getStateName() : (custShippingAddress.get(0).getStateName()!=null ? custShippingAddress.get(0).getStateName() : "")) + ", "
						+ (custBillingAddress.getZipCode()!=null ? custBillingAddress.getZipCode(): (custShippingAddress.get(0).getZipCode()!=null ? custShippingAddress.get(0).getZipCode() : "")),
						regularFont));
				billToTable.addCell(getCell("", regularFont));
				
				if(custShippingAddress != null && !custShippingAddress.isEmpty()){
					billToTable.addCell(getCell((custShippingAddress.get(0).getCity()!=null ? custShippingAddress.get(0).getCity() : (custBillingAddress.getCity()!=null ? custBillingAddress.getCity(): "")) + ", "
							+ (custShippingAddress.get(0).getStateName()!=null ? custShippingAddress.get(0).getStateName() : (custBillingAddress.getStateName()!=null ? custBillingAddress.getStateName() : "")) + ", "
							+ (custShippingAddress.get(0).getZipCode()!=null ? custShippingAddress.get(0).getZipCode() : (custBillingAddress.getZipCode()!=null ? custBillingAddress.getZipCode(): "")),
							regularFont));
				}else{
					billToTable.addCell(getCell((custBillingAddress.getCity()!=null ? custBillingAddress.getCity(): "") + ", " + (custBillingAddress.getStateName()!=null ? custBillingAddress.getStateName() : "") + ", " + (custBillingAddress.getZipCode()!=null ? custBillingAddress.getZipCode(): ""), regularFont));
				}
				
				billToTable.addCell(getCell((custBillingAddress.getCountryName()!=null ? custBillingAddress.getCountryName(): (custShippingAddress.get(0).getCountryName()!=null ? custShippingAddress.get(0).getCountryName() : "")),
						regularFont));
				billToTable.addCell(getCell("", regularFont));
				
				if(custShippingAddress != null && !custShippingAddress.isEmpty()){
					billToTable.addCell(getCell((custShippingAddress.get(0).getCountryName()!=null ? custShippingAddress.get(0).getCountryName() : (custBillingAddress.getCountryName()!=null ? custBillingAddress.getCountryName(): "")),
						regularFont));
				}else{
					billToTable.addCell(getCell((custBillingAddress.getCountryName()!=null ? custBillingAddress.getCountryName(): ""), regularFont));
				}
				billToTable.addCell(getBorderCell("Billing Phone: "
						+ (custBillingAddress.getPhone1()!=null ? custBillingAddress.getPhone1() : (custShippingAddress.get(0).getPhone1()!=null ? custShippingAddress.get(0).getPhone1() : "")), regularFont));
				billToTable.addCell(getCell("", regularFont));
				if(custShippingAddress != null && !custShippingAddress.isEmpty()){
					billToTable.addCell(getBorderCell("Shipping Phone: "
						+ (custShippingAddress.get(0).getPhone1()!=null ? custShippingAddress.get(0).getPhone1() : (custBillingAddress.getPhone1()!=null ? custBillingAddress.getPhone1() : "")), regularFont));
				}else{
					billToTable.addCell(getBorderCell("Shipping Phone: "+ (custBillingAddress.getPhone1()!=null ? custBillingAddress.getPhone1() : ""), regularFont));
				}
				billToTable.addCell(getBorderCell("Customer ID: "+openOrderInvoice.getCustomerId(),regularFont));
				billToTable.addCell(getNoBorderCell(" ",regularFont));
				billToTable.addCell(getBorderCell(" ",regularFont));
			}else{
				billToTable.addCell(getCell(" ", regularFont));
				billToTable.addCell(getCell("", regularFont));
				billToTable.addCell(getCell(" ", regularFont));
				billToTable.addCell(getCell(" ",regularFont));
				billToTable.addCell(getCell("", regularFont));
				billToTable.addCell(getCell(" ",regularFont));
				billToTable.addCell(getCell(" ",regularFont));
				billToTable.addCell(getCell("", regularFont));
				billToTable.addCell(getCell(" ",regularFont));
				billToTable.addCell(getCell(" ", regularFont));
				billToTable.addCell(getCell("", regularFont));
				billToTable.addCell(getCell(" ", regularFont));
				billToTable.addCell(getCell(" ",regularFont));
				billToTable.addCell(getCell("", regularFont));
				billToTable.addCell(getCell(" ",regularFont));
				billToTable.addCell(getBorderCell("Billing Phone:  ", regularFont));
				billToTable.addCell(getCell("", regularFont));
				billToTable.addCell(getBorderCell("Shipping Phone: "+ " ", regularFont));
				billToTable.addCell(getBorderCell("Customer ID: "+openOrderInvoice.getCustomerId(),regularFont));
				billToTable.addCell(getNoBorderCell(" ",regularFont));
				billToTable.addCell(getBorderCell(" ",regularFont));
			}

			document.add(billToTable);

			document.add(AddEmptyParagraph());

			PdfPTable ticketInfoHeaderTable = new PdfPTable(9);
			ticketInfoHeaderTable.setWidthPercentage(100f);
			ticketInfoHeaderTable.addCell(getBottomBorderCell("ON HAND",
					regularBoldFont));
			ticketInfoHeaderTable.addCell(getBottomBorderCell("QTY",
					regularBoldFont));
			ticketInfoHeaderTable.addCell(getBottomBorderCell("SEAT TYPE/OBS",
					regularBoldFont));
			ticketInfoHeaderTable.addCell(getBottomBorderCell("PURCHASE ORDER ID",
					regularBoldFont));
			ticketInfoHeaderTable.addCell(getBottomBorderCell("SECTION",
					regularBoldFont));
			ticketInfoHeaderTable.addCell(getBottomBorderCell("ROW",
					regularBoldFont));
			ticketInfoHeaderTable.addCell(getBottomBorderCell("SEATS",
					regularBoldFont));
			ticketInfoHeaderTable.addCell(getBottomBorderCell("SALE PRICE",
					regularBoldFont));
			ticketInfoHeaderTable.addCell(getBottomBorderCell("TOTAL",
					regularBoldFont));
			document.add(ticketInfoHeaderTable);

			
			if(ticketMap!=null && !ticketMap.isEmpty()){
				for(Map.Entry<Integer,List<POSTicketGroup>> ticket :ticketMap.entrySet()){
					Integer eventId = ticket.getKey();
					OpenOrders openOrder = null;
					for(OpenOrders inv1 : invoice){
						if(inv1.getEventId().equals(eventId) || inv1.getEventId() == eventId){
							openOrder = inv1;
						}
					}
					StringBuilder eventInfo = new StringBuilder();
					eventInfo.append((openOrder.getEventName()!=null ? openOrder.getEventName() : ""));
					eventInfo.append(" ");
					eventInfo.append((openOrder.getEventDateStr()!=null ? openOrder.getEventTimeStr() : ""));
					//eventInfo.append(" ");
					//eventInfo.append((openOrderInvoice.getEventTimeStr()!=null ? openOrderInvoice.getEventTimeStr() : ""));
					//eventInfo.append(" At ");
					//eventInfo.append(order.getVenueName());
					//eventInfo.append(" ");
					//eventInfo.append(event.getState());
					document.add(new Paragraph(eventInfo.toString(), regularBoldFont));
					
					List<POSTicketGroup> ticketGroupList = ticket.getValue();
					for(POSTicketGroup ticketGroup:ticketGroupList){
						PdfPTable ticketInfoTable = new PdfPTable(9);
						ticketInfoTable.setWidthPercentage(100f);
						ticketInfoTable.addCell(getNoBorderCell(ticketGroup.getOnHand(), regularFont));
						ticketInfoTable.addCell(getNoBorderCell((ticketGroup.getMappedQty()!=null ? ticketGroup.getMappedQty() : ""), regularFont));
						ticketInfoTable.addCell(getNoBorderCell("", regularFont));
						ticketInfoTable.addCell(getNoBorderCell(ticketGroup.getPurchaseOrderId(), regularFont));
						ticketInfoTable.addCell(getNoBorderCell((ticketGroup.getSection()!=null ? ticketGroup.getSection() : ""), regularFont));
						ticketInfoTable.addCell(getNoBorderCell((ticketGroup.getRow()!=null ? ticketGroup.getRow() : ""),	regularFont));
						ticketInfoTable.addCell(getNoBorderCell(ticketGroup.getSeatNoString(), regularFont));
						ticketInfoTable.addCell(getNoBorderCell("$"+(ticketGroup.getActualSoldPrice()!=null ? ticketGroup.getActualSoldPrice() : "0"), regularFont));
						Double tgTotal = 0.00;
						if (ticketGroup.getActualSoldPrice() != null
								&& ticketGroup.getMappedQty() != null) {
							tgTotal = ticketGroup.getActualSoldPrice() * ticketGroup.getMappedQty();
						}
						ticketInfoTable.addCell(getNoBorderCell("$"+tgTotal,
								regularFont));
						document.add(ticketInfoTable);
					}
				}
			}else if(categoryTicketMap!=null && !categoryTicketMap.isEmpty()){
				for(Map.Entry<Integer,List<POSCategoryTicketGroup>> ticket :categoryTicketMap.entrySet()){
					Integer eventId = ticket.getKey();
					OpenOrders openOrder = null;
					for(OpenOrders inv1 : invoice){
						if(inv1.getEventId() == eventId){
							openOrder = inv1;
						}
					}
					StringBuilder eventInfo = new StringBuilder();
					eventInfo.append((openOrder.getEventName()!=null ? openOrder.getEventName() : ""));
					eventInfo.append(" ");
					eventInfo.append((openOrder.getEventDateStr()!=null ? openOrder.getEventTimeStr() : ""));
					//eventInfo.append(" ");
					//eventInfo.append((openOrderInvoice.getEventTimeStr()!=null ? openOrderInvoice.getEventTimeStr() : ""));
					//eventInfo.append(" At ");
					//eventInfo.append(order.getVenueName());
					//eventInfo.append(" ");
					//eventInfo.append(event.getState());
					document.add(new Paragraph(eventInfo.toString(), regularBoldFont));
					List<POSCategoryTicketGroup> ticketGroupList = ticket.getValue();
					for(POSCategoryTicketGroup categoryTicketGroup:ticketGroupList){
						PdfPTable ticketInfoTable = new PdfPTable(9);
						ticketInfoTable.setWidthPercentage(100f);
						ticketInfoTable.addCell(getNoBorderCell("", regularFont));
						ticketInfoTable.addCell(getNoBorderCell((categoryTicketGroup.getTicketCount()!=null ? categoryTicketGroup.getTicketCount() : ""), regularFont));
						ticketInfoTable.addCell(getNoBorderCell("", regularFont));
						ticketInfoTable.addCell(getNoBorderCell("", regularFont));
						ticketInfoTable.addCell(getNoBorderCell("", regularFont));
						ticketInfoTable.addCell(getNoBorderCell("",	regularFont));
						/*if (categoryTicketGroup.getMappedSeatLow() != null) {
							seat = categoryTicketGroup.getMappedSeatLow();
							if (categoryTicketGroup.getMappedSeatHigh() != null)
								seat += "-" + categoryTicketGroup.getMappedSeatHigh();
						}*/
						ticketInfoTable.addCell(getNoBorderCell("", regularFont));
						ticketInfoTable.addCell(getNoBorderCell("$"+(categoryTicketGroup.getActualPrice()!=null ? categoryTicketGroup.getActualPrice() : "0"), regularFont));
						Double tgTotal = 0.00;
						if (categoryTicketGroup.getActualPrice() != null
								&& categoryTicketGroup.getTicketCount() != null) {
							tgTotal = categoryTicketGroup.getActualPrice() * categoryTicketGroup.getTicketCount();
						}
						ticketInfoTable.addCell(getNoBorderCell("$"+tgTotal,
								regularFont));
						document.add(ticketInfoTable);
					}
				}
			}
			document.add(AddEmptyParagraph());
			document.add(AddEmptyParagraph());
			
			float[] cellWidths = { 65f, 5f, 30f };
			PdfPTable paymentContactTable = new PdfPTable(cellWidths);
			paymentContactTable.setWidthPercentage(100f);
			paymentContactTable.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell paymentTitleCell = new PdfPCell(new Paragraph(
					"Payment Information", boldFontSmall));
			paymentTitleCell.setBorder(Rectangle.LEFT | Rectangle.TOP
					| Rectangle.RIGHT);
			paymentTitleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			paymentTitleCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			paymentContactTable.addCell(paymentTitleCell);
			PdfPCell cell3 = new PdfPCell();
			cell3.setBorder(Rectangle.NO_BORDER);
			paymentContactTable.addCell(cell3);

			PdfPCell contactTitleCell = new PdfPCell(new Paragraph(
					"Contact Information", boldFontSmall));
			contactTitleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			contactTitleCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			paymentContactTable.addCell(contactTitleCell);

			paymentContactTable.addCell(getCell("", regularFont));
			paymentContactTable.addCell(getNoBorderCell("", regularFont));
			if(custBillingAddress!=null && custBillingAddress.getPhone1()!=null && !custBillingAddress.getPhone1().isEmpty()){
				paymentContactTable.addCell(getCell("Main : "+custBillingAddress.getPhone1(), regularFont));
			}else if(custShippingAddress!=null && !custShippingAddress.isEmpty() && !custShippingAddress.get(0).getPhone1().isEmpty()
					&& custShippingAddress.get(0).getPhone1()!=null){
				paymentContactTable.addCell(getCell("Main : "+custShippingAddress.get(0).getPhone1(), regularFont));
			}else{
				paymentContactTable.addCell(getCell("Main : ", regularFont));
			}
			paymentContactTable.addCell(getCell("", regularFont));
			paymentContactTable.addCell(getNoBorderCell("", regularFont));
			paymentContactTable.addCell(getCell("Fax : ", regularFont));
			paymentContactTable.addCell(PdfLRBBorderCell("", regularFont));
			paymentContactTable.addCell(getNoBorderCell("", regularFont));
			if(custBillingAddress!=null && custBillingAddress.getEmail()!=null && !custBillingAddress.getEmail().isEmpty()){
				paymentContactTable.addCell(PdfLRBBorderCell("Email : "+custBillingAddress.getPhone1(), regularFont));
			}else if(custShippingAddress!=null && !custShippingAddress.isEmpty() && !custShippingAddress.get(0).getEmail().isEmpty()
					&& custShippingAddress.get(0).getEmail()!=null){
				paymentContactTable.addCell(PdfLRBBorderCell("Email : "+custShippingAddress.get(0).getEmail(), regularFont));
			}else{
				paymentContactTable.addCell(PdfLRBBorderCell("Email : ", regularFont));
			}
			document.add(paymentContactTable);

			
			document.add(AddEmptyParagraph());
			

			
			
			document.add(AddEmptyParagraph());

			
			Double subTotal=openOrderInvoice.getInvoiceTotal();
			if (subTotal != null) {
				if (inv.getInvoiceShippingCharges() != null)
					subTotal = subTotal - inv.getInvoiceShippingCharges();
				if (inv.getInvoiceTax() != null)
					subTotal = subTotal - inv.getInvoiceTax();
				if (inv.getInvoiceExpense() != null)
					subTotal = subTotal - inv.getInvoiceExpense();
			}
			float[] cellWidth = {55f, 20f, 20f, 15f };
			PdfPTable expenseInfoTable = new PdfPTable(cellWidth);
			expenseInfoTable.setWidthPercentage(100f);
			expenseInfoTable.setHorizontalAlignment(Element.ALIGN_RIGHT);
			
			PdfPCell shippingTitleCell = new PdfPCell(new Paragraph(
					"Shipping Information", boldFontSmall));
			shippingTitleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			shippingTitleCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			expenseInfoTable.addCell(shippingTitleCell);
			expenseInfoTable.addCell(getNoBorderCell("",regularBoldFont));
			
			expenseInfoTable.addCell(getRigthAlignedNoBorderCell("SUBTOTAL: ",regularBoldFont));
			expenseInfoTable.addCell(getRigthAlignedBorderCell("$" +subTotal,regularFont));
			
			if (openOrderInvoice.getRealTixShippingMethod() != null
					&& !openOrderInvoice.getRealTixShippingMethod().isEmpty()) {
				expenseInfoTable.addCell(getBorderCell("Shipping Method: "+openOrderInvoice.getRealTixShippingMethod(),regularBoldFont));
				expenseInfoTable.addCell(getNoBorderCell("",regularBoldFont));
				expenseInfoTable.addCell(getRigthAlignedNoBorderCell("SHIPPING: ",
						regularBoldFont));
				expenseInfoTable.addCell(getRigthAlignedBorderCell("$"
						+ inv.getInvoiceShippingCharges(), regularFont));
			}else{
				expenseInfoTable.addCell(getBorderCell("",regularBoldFont));
				expenseInfoTable.addCell(getNoBorderCell("",regularBoldFont));
				expenseInfoTable.addCell(getRigthAlignedNoBorderCell("SHIPPING: ",
						regularBoldFont));
				expenseInfoTable.addCell(getRigthAlignedBorderCell("$"
						+ inv.getInvoiceShippingCharges(), regularFont));
			}
			if (openOrderInvoice.getTrackingNo() != null
					&& !openOrderInvoice.getTrackingNo().isEmpty()) {
				expenseInfoTable.addCell(getBorderCell("Tracking #: "+ openOrderInvoice.getTrackingNo(), regularBoldFont));
				expenseInfoTable.addCell(getNoBorderCell("",regularBoldFont));
				expenseInfoTable.addCell(getRigthAlignedNoBorderCell("TAX: ",regularBoldFont));
				expenseInfoTable.addCell(getRigthAlignedBorderCell("$" + inv.getInvoiceTax(), regularFont));
				
			}else{
				expenseInfoTable.addCell(getBorderCell("",regularBoldFont));
				expenseInfoTable.addCell(getNoBorderCell("",regularBoldFont));
				expenseInfoTable.addCell(getRigthAlignedNoBorderCell("TAX: ",regularBoldFont));
				expenseInfoTable.addCell(getRigthAlignedBorderCell("$" + inv.getInvoiceTax(), regularFont));
			}
			
			expenseInfoTable.addCell(getNoBorderCell("", regularBoldFont));
			expenseInfoTable.addCell(getNoBorderCell("",regularBoldFont));
			expenseInfoTable.addCell(getRigthAlignedNoBorderCell("OTHER EXPENSES: ", regularBoldFont));
			expenseInfoTable.addCell(getRigthAlignedBorderCell("$"+ inv.getInvoiceExpense(), regularFont));
			
			expenseInfoTable.addCell(getNoBorderCell("", regularBoldFont));
			expenseInfoTable.addCell(getNoBorderCell("",regularBoldFont));
			expenseInfoTable.addCell(getRigthAlignedNoBorderCell("GRAND TOTAL: ", regularBoldFont));
			expenseInfoTable.addCell(getRigthAlignedBorderCell("$"+ (openOrderInvoice.getInvoiceTotal()>0 ? openOrderInvoice.getInvoiceTotal() : 0.00), regularBoldFont));
			
			expenseInfoTable.addCell(getNoBorderCell("", regularBoldFont));
			expenseInfoTable.addCell(getNoBorderCell("",regularBoldFont));
			expenseInfoTable.addCell(getRigthAlignedNoBorderCell("BALANCE DUE: ", regularBoldFont));
			expenseInfoTable.addCell(getRigthAlignedBorderCell("$"+inv.getInvoiceBalanceDue(),regularFont));
			
			document.add(expenseInfoTable);
			document.close();
			pdfFileout.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return document;
	}

	public static Document generatePOPdf(PurchaseOrder po,
			List<TicketGroup> ticketGroups, Customer customer,
			CustomerAddress customerAddress) {
		Document document = null;
		File file = null;
		FileOutputStream pdfFileout = null;
		try {
			document = new Document(PageSize.A4);
			file = new File(DAORegistry.getPropertyDAO()
					.get("tictracker.po.pdffile.path").getValue()
					+ "\\PurchaseOrder_" + po.getId() + ".pdf");
			pdfFileout = new FileOutputStream(file);
			PdfWriter.getInstance(document, pdfFileout);
			document.open();

			// Generate Image in Pdf
			//Image img = Image.getInstance("C:\\Tomcat 7.0\\webapps\\tictracker-0.1\\images\\bg-rtw.gif");
			// //prajendran local address
			Image img = Image.getInstance(DAORegistry.getPropertyDAO().get("rtf.logo.image.path").getValue());
			img.setAlignment(Image.MIDDLE | Image.TEXTWRAP);
			img.scalePercent(70);
			document.add(img);
			document.add(AddEmptyParagraph());
			document.add(AddEmptyParagraph());
			PdfPTable officeInfoTable = new PdfPTable(1);
			officeInfoTable.setHorizontalAlignment(Element.ALIGN_RIGHT);
			officeInfoTable.setWidthPercentage(30f);
			PdfPCell officeInfoCell1 = new PdfPCell(new Paragraph(
					"Reward The Fan", regularBoldFont));
			officeInfoCell1.setBorder(Rectangle.NO_BORDER);
			officeInfoTable.addCell(officeInfoCell1);

			PdfPCell officeInfoCell2 = new PdfPCell(new Paragraph(
					"10 Times Square", regularBoldFont));
			officeInfoCell2.setBorder(Rectangle.NO_BORDER);
			officeInfoTable.addCell(officeInfoCell2);

			PdfPCell officeInfoCell3 = new PdfPCell(new Paragraph(
					"3rd Floor", regularBoldFont));
			officeInfoCell3.setBorder(Rectangle.NO_BORDER);
			officeInfoTable.addCell(officeInfoCell3);

			PdfPCell officeInfoCell4 = new PdfPCell(new Paragraph(
					"New York NY 10018", regularBoldFont));
			officeInfoCell4.setBorder(Rectangle.NO_BORDER);
			officeInfoTable.addCell(officeInfoCell4);

			PdfPCell officeInfoCell5 = new PdfPCell(new Paragraph(
					"Phone:800-601-6100", regularBoldFont));
			officeInfoCell5.setBorder(Rectangle.NO_BORDER);
			officeInfoTable.addCell(officeInfoCell5);

			/*PdfPCell officeInfoCell6 = new PdfPCell(new Paragraph("Fax: ",
					regularBoldFont));
			officeInfoCell6.setBorder(Rectangle.NO_BORDER);
			officeInfoTable.addCell(officeInfoCell6);*/

			PdfPCell officeInfoCell7 = new PdfPCell(new Paragraph(
					"Email:sales@rewardthefan.com", regularBoldFont));
			officeInfoCell7.setBorder(Rectangle.NO_BORDER);
			officeInfoTable.addCell(officeInfoCell7);

			document.add(officeInfoTable);

			float[] columnWidth = { 100f };
			PdfPTable poTable = new PdfPTable(columnWidth);
			poTable.setWidthPercentage(30f);
			poTable.setHorizontalAlignment(Element.ALIGN_LEFT);
			poTable.addCell(getNoBorderCell("Purchase Order", boldFont));
			poTable.addCell(getNoBorderCell("Active", redBoldFont));
			document.add(poTable);

			float[] poInfoColumnWidth = { 17f, 10f, 13f };
			PdfPTable poInfoTable = new PdfPTable(poInfoColumnWidth);
			poInfoTable.setWidthPercentage(40f);
			poInfoTable.setHorizontalAlignment(Element.ALIGN_RIGHT);
			poInfoTable.addCell(getBorderCell("Date", boldFontSmall));
			poInfoTable.addCell(getBorderCell("P.O NO.", boldFontSmall));
			poInfoTable.addCell(getBorderCell("External PO#.", boldFontSmall));
			PdfPCell poDateCell = new PdfPCell(new Paragraph(
					po.getCreateTimeStr(), regularBoldFont));
			PdfPCell poNoCell = new PdfPCell(new Paragraph(po.getId()
					.toString(), regularBoldFont));
			PdfPCell poExtNoCell = new PdfPCell(new Paragraph(
					(po.getExternalPONo() != null ? po.getExternalPONo() : ""),
					regularBoldFont));
			poInfoTable.addCell(poDateCell);
			poInfoTable.addCell(poNoCell);
			poInfoTable.addCell(poExtNoCell);
			document.add(poInfoTable);

			document.add(AddEmptyParagraph());
			document.add(AddEmptyParagraph());

			float[] vendorShipToColumn = { 40f, 20f, 40f };
			PdfPTable venderShipToTable = new PdfPTable(vendorShipToColumn);
			venderShipToTable.setWidthPercentage(100f);
			// venderShipToTable.setHorizontalAlignment(Element.ALIGN_LEFT);
			PdfPCell billToTitleCell = new PdfPCell(new Paragraph("Vendor",
					boldFontSmall));
			billToTitleCell.setBorder(Rectangle.BOX);
			billToTitleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			billToTitleCell.setBackgroundColor(BaseColor.LIGHT_GRAY);

			venderShipToTable.addCell(billToTitleCell);
			venderShipToTable.addCell(getNoBorderCell("", regularFont));

			PdfPCell shipToTitleCell = new PdfPCell(new Paragraph("Ship To",
					boldFontSmall));
			shipToTitleCell.setBorder(Rectangle.BOX);
			shipToTitleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			shipToTitleCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			venderShipToTable.addCell(shipToTitleCell);

			PdfPCell billtoCell1 = new PdfPCell(new Paragraph(
					(customerAddress.getFirstName()!=null ? customerAddress.getFirstName() : "") + " "
							+ (customerAddress.getLastName()!=null ? customerAddress.getLastName() : ""), regularBoldFont));
			billtoCell1.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
			venderShipToTable.addCell(billtoCell1);

			venderShipToTable.addCell(getNoBorderCell("", regularFont));

			PdfPCell shipToCell1 = new PdfPCell(new Paragraph("Reward The fan",
					regularBoldFont));
			shipToCell1.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
			venderShipToTable.addCell(shipToCell1);

			PdfPCell billtoCell2 = new PdfPCell(new Paragraph(
					(customerAddress.getAddressLine1()!=null ? customerAddress.getAddressLine1() : "") + ", "
							+ (customerAddress.getAddressLine2()!=null ? customerAddress.getAddressLine2() : ""),
					regularBoldFont));
			billtoCell2.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
			venderShipToTable.addCell(billtoCell2);

			venderShipToTable.addCell(getNoBorderCell("", regularFont));

			PdfPCell shipToCell2 = new PdfPCell(new Paragraph("10 Times Square",
					regularBoldFont));
			shipToCell2.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
			venderShipToTable.addCell(shipToCell2);

			PdfPCell billtoCell4 = new PdfPCell(new Paragraph(
					(customerAddress.getCity()!=null ? customerAddress.getCity() : "") + ", "
							+ (customerAddress.getStateName()!=null ? customerAddress.getStateName() : ""), regularBoldFont));
			billtoCell4.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
			venderShipToTable.addCell(billtoCell4);

			venderShipToTable.addCell(getNoBorderCell("", regularFont));

			PdfPCell shipToCell3 = new PdfPCell(new Paragraph(
					"3rd Floor", regularBoldFont));
			shipToCell3.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
			venderShipToTable.addCell(shipToCell3);

			PdfPCell billtoCell5 = new PdfPCell(new Paragraph(
					(customerAddress.getPhone1()!=null ? customerAddress.getPhone1() : ""), regularBoldFont));
			billtoCell5.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
			venderShipToTable.addCell(billtoCell5);

			venderShipToTable.addCell(getNoBorderCell("", regularFont));

			PdfPCell shipToCell4 = new PdfPCell(new Paragraph(
					"New York NY 10018", regularBoldFont));
			shipToCell4.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
			venderShipToTable.addCell(shipToCell4);

			PdfPCell billtoCell3 = new PdfPCell(new Paragraph(
					(customerAddress.getCountryName()!=null ? customerAddress.getCountryName() : ""), regularBoldFont));
			billtoCell3.setBorder(Rectangle.RIGHT | Rectangle.LEFT
					| Rectangle.BOTTOM);
			venderShipToTable.addCell(billtoCell3);

			venderShipToTable.addCell(getNoBorderCell("", regularFont));

			PdfPCell shipToCell5 = new PdfPCell(new Paragraph("800-601-6100",
					regularBoldFont));
			shipToCell5.setBorder(Rectangle.RIGHT | Rectangle.LEFT
					| Rectangle.BOTTOM);
			venderShipToTable.addCell(shipToCell5);
			document.add(venderShipToTable);

			document.add(AddEmptyParagraph());
			document.add(AddEmptyParagraph());
			document.add(AddEmptyParagraph());

			PdfPTable ticketInfoHeaderTable = new PdfPTable(9);
			ticketInfoHeaderTable.setWidthPercentage(100f);
			ticketInfoHeaderTable.addCell(getBottomBorderCell("QTY",
					regularBoldFont));
			ticketInfoHeaderTable.addCell(getBottomBorderCell("ONHAND",
					regularBoldFont));
			ticketInfoHeaderTable.addCell(getBottomBorderCell("SECTION",
					regularBoldFont));
			ticketInfoHeaderTable.addCell(getBottomBorderCell("ROW",
					regularBoldFont));
			ticketInfoHeaderTable.addCell(getBottomBorderCell("SEATS",
					regularBoldFont));
			ticketInfoHeaderTable.addCell(getBottomBorderCell("FACE",
					regularBoldFont));
			ticketInfoHeaderTable.addCell(getBottomBorderCell("COST",
					regularBoldFont));
			ticketInfoHeaderTable.addCell(getBottomBorderCell(
					"ETICKET ATACHED", regularBoldFont));
			ticketInfoHeaderTable.addCell(getBottomBorderCell("TOTAL",
					regularBoldFont));
			document.add(ticketInfoHeaderTable);

			for (TicketGroup ticket : ticketGroups) {
				Event event = DAORegistry.getEventDAO().get(ticket.getEventId());
				Venue venue = DAORegistry.getVenueDAO().get(event.getVenueId());
				/*EventDetails event = DAORegistry.getEventDetailsDAO().get(
						ticket.getEventId());*/
				StringBuilder eventInfo = new StringBuilder();
				eventInfo.append(event.getEventName());
				eventInfo.append(" ");
				eventInfo.append(dateFormat.format(event.getEventDate()));
				eventInfo.append(" ");
				eventInfo.append(event.getEventTimeStr());
				eventInfo.append(" At ");
				eventInfo.append(venue.getBuilding());
				eventInfo.append(" ");
				eventInfo.append(venue.getState());
				document.add(new Paragraph(eventInfo.toString(),
						regularBoldFont));

				String seat = "";
				PdfPTable ticketInfoTable = new PdfPTable(9);
				ticketInfoTable.setWidthPercentage(100f);
				ticketInfoTable.addCell(getNoBorderCell(ticket.getQuantity(),
						regularFont));
				ticketInfoTable.addCell(getNoBorderCell(
						ticket.getOnHand() != null ? ticket.getOnHand() : "",
						regularFont));
				ticketInfoTable.addCell(getNoBorderCell(ticket.getSection(),
						regularFont));
				ticketInfoTable.addCell(getNoBorderCell(ticket.getRow(),
						regularFont));
				if (ticket.getSeatLow() != null) {
					seat = ticket.getSeatLow();
					if (ticket.getSeatHigh() != null)
						seat += "-" + ticket.getSeatHigh();
				}
				ticketInfoTable.addCell(getNoBorderCell(seat, regularFont));
				ticketInfoTable.addCell(getNoBorderCell(
						String.valueOf(ticket.getPrice()), regularFont));
				ticketInfoTable.addCell(getNoBorderCell(String.valueOf(ticket.getPrice()), regularFont));
				ticketInfoTable.addCell(getNoBorderCell("", regularFont));
				Double tgTotal = null;
				if (ticket.getPrice() != null && ticket.getPrice() != null) {
					tgTotal = ticket.getPrice() * ticket.getQuantity();
				}
				ticketInfoTable.addCell(getNoBorderCell(tgTotal, regularFont));
				document.add(ticketInfoTable);

				document.add(AddEmptyParagraph());

			}

			document.add(AddEmptyParagraph());
			document.add(AddEmptyParagraph());

			float[] cellWidths = { 50f, 10f, 20f, 20f };
			PdfPTable paymentContactTable = new PdfPTable(cellWidths);
			paymentContactTable.setWidthPercentage(100f);
			paymentContactTable.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell paymentTitleCell = new PdfPCell(new Paragraph(
					"Payment Information", boldFontSmall));

			paymentTitleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			paymentTitleCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			paymentContactTable.addCell(paymentTitleCell);
			paymentContactTable.addCell(getNoBorderCell("", regularFont));
			paymentContactTable.addCell(getNoBorderCell("", regularFont));

			PdfPCell summeryCell = new PdfPCell(new Paragraph("Summery",
					boldFontSmall));
			summeryCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			summeryCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			summeryCell.setBorder(Rectangle.BOX);
			paymentContactTable.addCell(summeryCell);

			PdfPCell emtyCell1 = new PdfPCell(new Paragraph("", boldFontSmall));
			emtyCell1.setBorder(Rectangle.LEFT | Rectangle.RIGHT
					| Rectangle.BOTTOM);
			paymentContactTable.addCell(emtyCell1);
			paymentContactTable.addCell(getNoBorderCell("", regularFont));
			paymentContactTable.addCell(getNoBorderCell("Tickets",
					regularBoldFont));
			PdfPCell ticketCountCell1 = new PdfPCell(new Paragraph(po
					.getTicketCount().toString(), regularBoldFont));
			ticketCountCell1.setBorder(Rectangle.BOX);
			paymentContactTable.addCell(ticketCountCell1);

			paymentContactTable.addCell(getNoBorderCell("", regularFont));
			paymentContactTable.addCell(getNoBorderCell("", regularFont));
			paymentContactTable.addCell(getNoBorderCell("Total Ticket Cost:",
					regularBoldFont));
			PdfPCell poTotalCell1 = new PdfPCell(new Paragraph("$"
					+ po.getPoTotal().toString(), regularBoldFont));
			poTotalCell1.setBorder(Rectangle.BOX);
			poTotalCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			paymentContactTable.addCell(poTotalCell1);

			PdfPCell emtyCell3 = new PdfPCell(new Paragraph(
					"Purchase Order Notes", boldFontSmall));
			emtyCell3.setBorder(Rectangle.BOX);
			emtyCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
			emtyCell3.setBackgroundColor(BaseColor.LIGHT_GRAY);
			paymentContactTable.addCell(emtyCell3);
			paymentContactTable.addCell(getNoBorderCell("", regularFont));
			paymentContactTable
					.addCell(getNoBorderCell("Tax:", regularBoldFont));
			PdfPCell taxCell1 = new PdfPCell(new Paragraph("$0.00",
					regularBoldFont));
			taxCell1.setBorder(Rectangle.BOX);
			taxCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			paymentContactTable.addCell(taxCell1);

			PdfPCell emtyCell4 = new PdfPCell(new Paragraph(
					po.getInternalNotes() != null ? po.getInternalNotes() : "",
					regularFont));
			emtyCell4.setBorder(Rectangle.LEFT | Rectangle.RIGHT
					| Rectangle.BOTTOM);
			paymentContactTable.addCell(emtyCell4);
			paymentContactTable.addCell(getNoBorderCell("", regularFont));
			paymentContactTable.addCell(getNoBorderCell("Expenses:",
					regularBoldFont));
			PdfPCell expenseCell1 = new PdfPCell(new Paragraph("$0.00",
					regularBoldFont));
			expenseCell1.setBorder(Rectangle.BOX);
			expenseCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			paymentContactTable.addCell(expenseCell1);

			paymentContactTable.addCell(getNoBorderCell("", regularFont));
			paymentContactTable.addCell(getNoBorderCell("", regularFont));
			paymentContactTable.addCell(getNoBorderCell("Shipping:",
					regularBoldFont));
			PdfPCell shippingCell1 = new PdfPCell(new Paragraph("$0.00",
					regularBoldFont));
			shippingCell1.setBorder(Rectangle.TOP | Rectangle.RIGHT
					| Rectangle.LEFT);
			shippingCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			paymentContactTable.addCell(shippingCell1);

			PdfPCell emtyCell6 = new PdfPCell(new Paragraph("Shipping Notes",
					boldFontSmall));
			emtyCell6.setBorder(Rectangle.BOX);
			emtyCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
			emtyCell6.setBackgroundColor(BaseColor.LIGHT_GRAY);
			paymentContactTable.addCell(emtyCell6);
			paymentContactTable.addCell(getNoBorderCell("", regularFont));
			paymentContactTable.addCell(getNoBorderCell("PO Total:",
					regularBoldFont));
			PdfPCell totalCell1 = new PdfPCell(new Paragraph("$"+po.getPoTotal().toString(),
					regularBoldFont));
			totalCell1.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
			totalCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			paymentContactTable.addCell(totalCell1);

			PdfPCell emtyCell7 = new PdfPCell(new Paragraph("", boldFontSmall));
			emtyCell7.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
			paymentContactTable.addCell(emtyCell7);
			paymentContactTable.addCell(getNoBorderCell("", regularFont));
			paymentContactTable.addCell(getNoBorderCell("Total Paid:",
					regularBoldFont));
			PdfPCell totalPaidCell1 = new PdfPCell(new Paragraph("$"+po.getPoTotal().toString(),
					regularBoldFont));
			totalPaidCell1.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
			totalPaidCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			paymentContactTable.addCell(totalPaidCell1);

			PdfPCell emtyCell8 = new PdfPCell(new Paragraph("", boldFontSmall));
			emtyCell8.setBorder(Rectangle.LEFT | Rectangle.RIGHT
					| Rectangle.BOTTOM);
			paymentContactTable.addCell(emtyCell8);
			paymentContactTable.addCell(getNoBorderCell("", regularFont));
			paymentContactTable.addCell(getNoBorderCell("Balance Due:",
					regularBoldFont));
			PdfPCell balanceCell1 = new PdfPCell(new Paragraph("$0.00",
					regularBoldFont));
			balanceCell1.setBorder(Rectangle.BOX);
			balanceCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			paymentContactTable.addCell(balanceCell1);
			document.add(paymentContactTable);

			document.close();
			pdfFileout.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return document;
	}

	public static Document generateRTWPOPdf(Collection<PurchaseOrders> purchaseOrders,
			Map<Integer, List<POSTicketGroup>> ticketMap, CustomerAddress customerAddress) {
		Document document = null;
		File file = null;
		FileOutputStream pdfFileout = null;
		try {
			
			PurchaseOrders pos = null;
			Set<Integer> poID = new HashSet<Integer>();
			
			for(PurchaseOrders po: purchaseOrders){
				if(poID.add(po.getId())){
					pos = po;
				}
			}
			String productType = pos.getProductType();
			POSPurchaseOrder posPO = DAORegistry.getPosPurchaseOrderDAO().getPOSPurchaseOrderDAOByPurchaseOrderId(pos.getId(), productType);
			
			document = new Document(PageSize.A4);
			file = new File(DAORegistry.getPropertyDAO()
					.get("tictracker.po.pdffile.path").getValue()
					+ "\\PurchaseOrder_" + pos.getId() + ".pdf");
			pdfFileout = new FileOutputStream(file);
			PdfWriter.getInstance(document, pdfFileout);
			document.open();

			// Generate Image in Pdf
			//Image img = Image.getInstance("C:\\Tomcat 7.0\\webapps\\tictracker-0.1\\images\\bg-rtw.gif");
			// //prajendran local address
			Image img = Image.getInstance(DAORegistry.getPropertyDAO().get("rtw.logo.image.path").getValue());
			img.setAlignment(Image.LEFT | Image.TEXTWRAP);
			img.scalePercent(57);
			document.add(img);

			PdfPTable officeInfoTable = new PdfPTable(1);
			officeInfoTable.setHorizontalAlignment(Element.ALIGN_RIGHT);
			officeInfoTable.setWidthPercentage(50f);
			PdfPCell officeInfoCell1 = new PdfPCell(new Paragraph(
					"Right This Way", regularBoldFont));
			officeInfoCell1.setBorder(Rectangle.NO_BORDER);
			officeInfoTable.addCell(officeInfoCell1);

			PdfPCell officeInfoCell2 = new PdfPCell(new Paragraph(
					"1441 Broadway", regularBoldFont));
			officeInfoCell2.setBorder(Rectangle.NO_BORDER);
			officeInfoTable.addCell(officeInfoCell2);

			PdfPCell officeInfoCell3 = new PdfPCell(new Paragraph(
					"3rd Floor, Suite 3012", regularBoldFont));
			officeInfoCell3.setBorder(Rectangle.NO_BORDER);
			officeInfoTable.addCell(officeInfoCell3);

			PdfPCell officeInfoCell4 = new PdfPCell(new Paragraph(
					"New York, NY 10018", regularBoldFont));
			officeInfoCell4.setBorder(Rectangle.NO_BORDER);
			officeInfoTable.addCell(officeInfoCell4);

			PdfPCell officeInfoCell5 = new PdfPCell(new Paragraph(
					"Phone:(646)307-4277", regularBoldFont));
			officeInfoCell5.setBorder(Rectangle.NO_BORDER);
			officeInfoTable.addCell(officeInfoCell5);

			PdfPCell officeInfoCell6 = new PdfPCell(new Paragraph("Fax: ",
					regularBoldFont));
			officeInfoCell6.setBorder(Rectangle.NO_BORDER);
			officeInfoTable.addCell(officeInfoCell6);

			PdfPCell officeInfoCell7 = new PdfPCell(new Paragraph(
					"Email:sales@rightthisway.com", regularBoldFont));
			officeInfoCell7.setBorder(Rectangle.NO_BORDER);
			officeInfoTable.addCell(officeInfoCell7);

			document.add(officeInfoTable);

			float[] columnWidth = { 100f };
			PdfPTable poTable = new PdfPTable(columnWidth);
			poTable.setWidthPercentage(30f);
			poTable.setHorizontalAlignment(Element.ALIGN_LEFT);
			poTable.addCell(getNoBorderCell("Purchase Order", boldFont));
			poTable.addCell(getNoBorderCell("Active", redBoldFont));
			document.add(poTable);

			float[] poInfoColumnWidth = { 17f, 10f, 13f };
			PdfPTable poInfoTable = new PdfPTable(poInfoColumnWidth);
			poInfoTable.setWidthPercentage(40f);
			poInfoTable.setHorizontalAlignment(Element.ALIGN_RIGHT);
			poInfoTable.addCell(getBorderCell("Date", boldFontSmall));
			poInfoTable.addCell(getBorderCell("P.O NO.", boldFontSmall));
			poInfoTable.addCell(getBorderCell("External PO#.", boldFontSmall));
			PdfPCell poDateCell = new PdfPCell(new Paragraph(
					(pos.getCreatedDateStr() != null ? pos.getCreatedDateStr() : ""), regularBoldFont));
			PdfPCell poNoCell = new PdfPCell(new Paragraph(pos.getId()
					.toString(), regularBoldFont));
			PdfPCell poExtNoCell = new PdfPCell(new Paragraph(
					(pos.getConsignmentPoNo() != null ? pos.getConsignmentPoNo() : ""), regularBoldFont));
			poInfoTable.addCell(poDateCell);
			poInfoTable.addCell(poNoCell);
			poInfoTable.addCell(poExtNoCell);
			document.add(poInfoTable);

			document.add(AddEmptyParagraph());
			document.add(AddEmptyParagraph());

			float[] vendorShipToColumn = { 40f, 20f, 40f };
			PdfPTable venderShipToTable = new PdfPTable(vendorShipToColumn);
			venderShipToTable.setWidthPercentage(100f);
			// venderShipToTable.setHorizontalAlignment(Element.ALIGN_LEFT);
			PdfPCell billToTitleCell = new PdfPCell(new Paragraph("Vendor",
					boldFontSmall));
			billToTitleCell.setBorder(Rectangle.BOX);
			billToTitleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			billToTitleCell.setBackgroundColor(BaseColor.LIGHT_GRAY);

			venderShipToTable.addCell(billToTitleCell);
			venderShipToTable.addCell(getNoBorderCell("", regularFont));

			PdfPCell shipToTitleCell = new PdfPCell(new Paragraph("Ship To", boldFontSmall));
			shipToTitleCell.setBorder(Rectangle.BOX);
			shipToTitleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			shipToTitleCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			venderShipToTable.addCell(shipToTitleCell);

			PdfPCell billtoCell1 = new PdfPCell(new Paragraph((customerAddress.getFirstName() != null ? customerAddress.getFirstName() : "")+ " "
							+ (customerAddress.getLastName() != null ? customerAddress.getLastName() : ""), regularBoldFont));
			billtoCell1.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
			venderShipToTable.addCell(billtoCell1);

			venderShipToTable.addCell(getNoBorderCell("", regularFont));

			PdfPCell shipToCell1 = new PdfPCell(new Paragraph("Right This Way", regularBoldFont));
			shipToCell1.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
			venderShipToTable.addCell(shipToCell1);

			PdfPCell billtoCell2 = new PdfPCell(new Paragraph((customerAddress.getAddressLine1() != null ? customerAddress.getAddressLine1() : "")+ ", "
							+ (customerAddress.getAddressLine2() != null ? customerAddress.getAddressLine2() : ""), regularBoldFont));
			billtoCell2.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
			venderShipToTable.addCell(billtoCell2);

			venderShipToTable.addCell(getNoBorderCell("", regularFont));

			PdfPCell shipToCell2 = new PdfPCell(new Paragraph("1441 Broadway",
					regularBoldFont));
			shipToCell2.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
			venderShipToTable.addCell(shipToCell2);

			PdfPCell billtoCell4 = new PdfPCell(new Paragraph((customerAddress.getCity() != null ? customerAddress.getCity() : "")+ ", "
							+ (customerAddress.getStateName() != null ? customerAddress.getStateName() : ""), regularBoldFont));
			billtoCell4.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
			venderShipToTable.addCell(billtoCell4);

			venderShipToTable.addCell(getNoBorderCell("", regularFont));

			PdfPCell shipToCell3 = new PdfPCell(new Paragraph(
					"3rd Floor, Suite 3012", regularBoldFont));
			shipToCell3.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
			venderShipToTable.addCell(shipToCell3);

			PdfPCell billtoCell5 = new PdfPCell(new Paragraph(
					(customerAddress.getPhone1() != null ? customerAddress.getPhone1() : ""), regularBoldFont));
			billtoCell5.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
			venderShipToTable.addCell(billtoCell5);

			venderShipToTable.addCell(getNoBorderCell("", regularFont));

			PdfPCell shipToCell4 = new PdfPCell(new Paragraph(
					"New York, NY 10018", regularBoldFont));
			shipToCell4.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
			venderShipToTable.addCell(shipToCell4);

			PdfPCell billtoCell3 = new PdfPCell(new Paragraph(
					(customerAddress.getCountryName() != null ? customerAddress.getCountryName() : ""), regularBoldFont));
			billtoCell3.setBorder(Rectangle.RIGHT | Rectangle.LEFT
					| Rectangle.BOTTOM);
			venderShipToTable.addCell(billtoCell3);

			venderShipToTable.addCell(getNoBorderCell("", regularFont));

			PdfPCell shipToCell5 = new PdfPCell(new Paragraph("(646)307-4277",
					regularBoldFont));
			shipToCell5.setBorder(Rectangle.RIGHT | Rectangle.LEFT
					| Rectangle.BOTTOM);
			venderShipToTable.addCell(shipToCell5);
			document.add(venderShipToTable);

			document.add(AddEmptyParagraph());
			document.add(AddEmptyParagraph());
			document.add(AddEmptyParagraph());

			PdfPTable ticketInfoHeaderTable = new PdfPTable(9);
			ticketInfoHeaderTable.setWidthPercentage(100f);
			ticketInfoHeaderTable.addCell(getBottomBorderCell("QTY",
					regularBoldFont));
			ticketInfoHeaderTable.addCell(getBottomBorderCell("ONHAND",
					regularBoldFont));
			ticketInfoHeaderTable.addCell(getBottomBorderCell("SECTION",
					regularBoldFont));
			ticketInfoHeaderTable.addCell(getBottomBorderCell("ROW",
					regularBoldFont));
			ticketInfoHeaderTable.addCell(getBottomBorderCell("SEATS",
					regularBoldFont));
			ticketInfoHeaderTable.addCell(getBottomBorderCell("FACE",
					regularBoldFont));
			ticketInfoHeaderTable.addCell(getBottomBorderCell("COST",
					regularBoldFont));
			ticketInfoHeaderTable.addCell(getBottomBorderCell("NOTES", regularBoldFont));
			ticketInfoHeaderTable.addCell(getBottomBorderCell("TOTAL",
					regularBoldFont));
			document.add(ticketInfoHeaderTable);

			if(ticketMap!=null && !ticketMap.isEmpty()){
				for(Map.Entry<Integer,List<POSTicketGroup>> ticket :ticketMap.entrySet()){
					Integer eventId = ticket.getKey();
					PurchaseOrders purchaseOrder = null;
					for(PurchaseOrders po1 : purchaseOrders){
						if(po1.getEventId().equals(eventId) || po1.getEventId() == eventId){
							purchaseOrder = po1;
						}
					}
					StringBuilder eventInfo = new StringBuilder();
					eventInfo.append(purchaseOrder.getEventName() != null ? purchaseOrder.getEventName() : "");
					eventInfo.append(" ");
					eventInfo.append(dateFormat.format((purchaseOrder.getEventDate() != null ? purchaseOrder.getEventDate() : "")));
					eventInfo.append(" ");
					eventInfo.append(purchaseOrder.getEventTimeStr() != null ? purchaseOrder.getEventTimeStr() : "");
					//eventInfo.append(" At ");
					//eventInfo.append(event.getBuilding());
					//eventInfo.append(" ");
					//eventInfo.append(event.getState());
					document.add(new Paragraph(eventInfo.toString(),regularBoldFont));
					
					List<POSTicketGroup> ticketGroupList = ticket.getValue();
					for(POSTicketGroup ticketGroup:ticketGroupList){
						PdfPTable ticketInfoTable = new PdfPTable(9);
						ticketInfoTable.setWidthPercentage(100f);
						ticketInfoTable.addCell(getNoBorderCell((ticketGroup.getMappedQty() != null ? ticketGroup.getMappedQty() : ""),regularFont));
						ticketInfoTable.addCell(getNoBorderCell((ticketGroup.getOnHand() != null ? ticketGroup.getOnHand() : ""),regularFont));
						ticketInfoTable.addCell(getNoBorderCell((ticketGroup.getSection() != null ? ticketGroup.getSection() : ""),regularFont));
						ticketInfoTable.addCell(getNoBorderCell((ticketGroup.getRow() != null ? ticketGroup.getRow() : ""),regularFont));
						ticketInfoTable.addCell(getNoBorderCell(ticketGroup.getSeatNoString(), regularFont));
						ticketInfoTable.addCell(getNoBorderCell(String.valueOf(ticketGroup.getFacePrice() != null ? ticketGroup.getFacePrice() : "0"),regularFont));
						ticketInfoTable.addCell(getNoBorderCell(String.valueOf(ticketGroup.getActualSoldPrice() != null ? ticketGroup.getActualSoldPrice() : "0"),regularFont));
						ticketInfoTable.addCell(getNoBorderCell(ticketGroup.getNotes(), regularFont));
						Double tgTotal = 0.00;
						if (ticketGroup.getActualSoldPrice() != null
								&& ticketGroup.getMappedQty() != null) {
							tgTotal = ticketGroup.getActualSoldPrice() * ticketGroup.getMappedQty();
						}
						ticketInfoTable.addCell(getNoBorderCell(tgTotal, regularFont));
						document.add(ticketInfoTable);
					}
				}
			}

			document.add(AddEmptyParagraph());
			document.add(AddEmptyParagraph());

			float[] cellWidths = { 50f, 10f, 20f, 20f };
			PdfPTable paymentContactTable = new PdfPTable(cellWidths);
			paymentContactTable.setWidthPercentage(100f);
			paymentContactTable.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell paymentTitleCell = new PdfPCell(new Paragraph(
					"Payment Information", boldFontSmall));

			paymentTitleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			paymentTitleCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			paymentContactTable.addCell(paymentTitleCell);
			paymentContactTable.addCell(getNoBorderCell("", regularFont));
			paymentContactTable.addCell(getNoBorderCell("", regularFont));

			PdfPCell summeryCell = new PdfPCell(new Paragraph("Summary",
					boldFontSmall));
			summeryCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			summeryCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			summeryCell.setBorder(Rectangle.BOX);
			paymentContactTable.addCell(summeryCell);

			PdfPCell emtyCell1 = new PdfPCell(new Paragraph("", boldFontSmall));
			emtyCell1.setBorder(Rectangle.LEFT | Rectangle.RIGHT
					| Rectangle.BOTTOM);
			paymentContactTable.addCell(emtyCell1);
			paymentContactTable.addCell(getNoBorderCell("", regularFont));
			paymentContactTable.addCell(getNoBorderCell("Tickets",
					regularBoldFont));
			PdfPCell ticketCountCell1 = new PdfPCell(new Paragraph((pos.getTicketQty() != null ? pos.getTicketQty() : "").toString(), regularBoldFont));
			ticketCountCell1.setBorder(Rectangle.BOX);
			paymentContactTable.addCell(ticketCountCell1);

			paymentContactTable.addCell(getNoBorderCell("", regularFont));
			paymentContactTable.addCell(getNoBorderCell("", regularFont));
			paymentContactTable.addCell(getNoBorderCell("Total Ticket Cost:",
					regularBoldFont));
			PdfPCell poTotalCell1 = new PdfPCell(new Paragraph("$"+ (pos.getPoTotal() != null ? pos.getPoTotal() : "").toString(), regularBoldFont));
			poTotalCell1.setBorder(Rectangle.BOX);
			poTotalCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			paymentContactTable.addCell(poTotalCell1);

			PdfPCell emtyCell3 = new PdfPCell(new Paragraph(
					"Purchase Order Notes", boldFontSmall));
			emtyCell3.setBorder(Rectangle.BOX);
			emtyCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
			emtyCell3.setBackgroundColor(BaseColor.LIGHT_GRAY);
			paymentContactTable.addCell(emtyCell3);
			paymentContactTable.addCell(getNoBorderCell("", regularFont));
			paymentContactTable
					.addCell(getNoBorderCell("Tax:", regularBoldFont));
			PdfPCell taxCell1 = new PdfPCell(new Paragraph("$"+posPO.getTotalTaxes(),
					regularBoldFont));
			taxCell1.setBorder(Rectangle.BOX);
			taxCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			paymentContactTable.addCell(taxCell1);

			PdfPCell emtyCell4 = new PdfPCell(new Paragraph((pos.getExternalNotes() != null ? pos.getExternalNotes() : ""),
					regularFont));
			emtyCell4.setBorder(Rectangle.LEFT | Rectangle.RIGHT
					| Rectangle.BOTTOM);
			paymentContactTable.addCell(emtyCell4);
			paymentContactTable.addCell(getNoBorderCell("", regularFont));
			paymentContactTable.addCell(getNoBorderCell("Expenses:",
					regularBoldFont));
			PdfPCell expenseCell1 = new PdfPCell(new Paragraph("$"+posPO.getTotalExpenses(),
					regularBoldFont));
			expenseCell1.setBorder(Rectangle.BOX);
			expenseCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			paymentContactTable.addCell(expenseCell1);

			paymentContactTable.addCell(getNoBorderCell("", regularFont));
			paymentContactTable.addCell(getNoBorderCell("", regularFont));
			paymentContactTable.addCell(getNoBorderCell("Shipping:",
					regularBoldFont));
			PdfPCell shippingCell1 = new PdfPCell(new Paragraph("$"+posPO.getTotalShipping(),
					regularBoldFont));
			shippingCell1.setBorder(Rectangle.TOP | Rectangle.RIGHT
					| Rectangle.LEFT);
			shippingCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			paymentContactTable.addCell(shippingCell1);

			PdfPCell emtyCell6 = new PdfPCell(new Paragraph("Shipping Notes",
					boldFontSmall));
			emtyCell6.setBorder(Rectangle.BOX);
			emtyCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
			emtyCell6.setBackgroundColor(BaseColor.LIGHT_GRAY);
			paymentContactTable.addCell(emtyCell6);
			paymentContactTable.addCell(getNoBorderCell("", regularFont));
			paymentContactTable.addCell(getNoBorderCell("PO Total:",
					regularBoldFont));
			PdfPCell totalCell1 = new PdfPCell(new Paragraph("$"+posPO.getPoTotal(),
					regularBoldFont));
			totalCell1.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
			totalCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			paymentContactTable.addCell(totalCell1);

			PdfPCell emtyCell7 = new PdfPCell(new Paragraph("", boldFontSmall));
			emtyCell7.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
			paymentContactTable.addCell(emtyCell7);
			paymentContactTable.addCell(getNoBorderCell("", regularFont));
			paymentContactTable.addCell(getNoBorderCell("Balance Due:",
					regularBoldFont));
			PdfPCell totalPaidCell1 = new PdfPCell(new Paragraph("$"+posPO.getBalanceDue(),
					regularBoldFont));
			totalPaidCell1.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
			totalPaidCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			paymentContactTable.addCell(totalPaidCell1);

			PdfPCell emtyCell8 = new PdfPCell(new Paragraph("", boldFontSmall));
			emtyCell8.setBorder(Rectangle.LEFT | Rectangle.RIGHT
					| Rectangle.BOTTOM);
			paymentContactTable.addCell(emtyCell8);
			paymentContactTable.addCell(getNoBorderCell("", regularFont));
			/*paymentContactTable.addCell(getNoBorderCell("Balance Due:",
					regularBoldFont));
			PdfPCell balanceCell1 = new PdfPCell(new Paragraph("$"+posPO.getBalanceDue(),
					regularBoldFont));
			balanceCell1.setBorder(Rectangle.BOX);
			balanceCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			paymentContactTable.addCell(balanceCell1);*/
			document.add(paymentContactTable);

			document.close();
			pdfFileout.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return document;
	}

	private static PdfPCell getCell(Object obj, Font font) {
		String text = "";
		if (obj != null) {
			text = String.valueOf(obj);
		}
		PdfPCell cell = new PdfPCell(new Paragraph(text, font));
		cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		return cell;
	}

	private static PdfPCell getBorderCell(Object obj, Font font) {
		String text = "";
		if (obj != null) {
			text = String.valueOf(obj);
		}
		PdfPCell cell = new PdfPCell(new Paragraph(text, font));
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		return cell;
	}

	private static PdfPCell getRigthAlignedBorderCell(Object obj, Font font) {
		String text = "";
		if (obj != null) {
			text = String.valueOf(obj);
		}
		PdfPCell cell = new PdfPCell(new Paragraph(text, font));
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		return cell;
	}

	private static PdfPCell getRigthAlignedNoBorderCell(Object obj, Font font) {
		String text = "";
		if (obj != null) {
			text = String.valueOf(obj);
		}
		PdfPCell cell = new PdfPCell(new Paragraph(text, font));
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setBorder(Rectangle.NO_BORDER);
		return cell;
	}

	private static PdfPCell getBottomBorderCell(Object obj, Font font) {
		String text = "";
		if (obj != null) {
			text = String.valueOf(obj);
		}
		PdfPCell cell = new PdfPCell(new Paragraph(text, font));
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell.setBorder(Rectangle.BOTTOM);
		return cell;
	}

	private static PdfPCell getNoBorderCell(Object obj, Font font) {
		String text = "";
		if (obj != null) {
			text = String.valueOf(obj);
		}
		PdfPCell cell = new PdfPCell(new Paragraph(text, font));
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell.setBorder(Rectangle.NO_BORDER);
		return cell;
	}

	private static Paragraph AddEmptyParagraph() {
		Paragraph paragraph = new Paragraph("      ");
		paragraph.setLeading(10);
		return paragraph;

	}
	
	private static PdfPCell PdfLRBBorderCell(Object obj, Font font) {
		String text = "";
		if (obj != null) {
			text = String.valueOf(obj);
		}
		PdfPCell cell = new PdfPCell(new Paragraph(text, font));
		cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		return cell;
	}
}
