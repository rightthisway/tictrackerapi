package com.rtw.tracker.utils;

import org.springframework.beans.factory.InitializingBean;

public class URLUtil implements InitializingBean{
	
	
	private static String baseDirectory;
	
	public String getBaseDirectory() {
		return baseDirectory;
	}

	public void setBaseDirectory(String baseDirectory) {
		URLUtil.baseDirectory = baseDirectory;
	}
	
	
	public static String FIREBASE_CONFIG_FILE;
	public static String FIREBASE_WEB_CONFIG_FILE;
	public static String FIRESTORE_CONFIG_FILE;
	public static String CARDS_DIRECTORY;
	public static String ARTIST_DIRECTORY;
	public static String PARENT_DIRECTORY;
	public static String CHILD_DIRECTORY;
	public static String GRANDCHILD_DIRECTORY;
	public static String TICKET_DIRECTORY;
	public static String LOYALFAN_DIRECTORY;
	public static String FANTASY_DIRECTORY;
	public static String REPORT_TEMPLATE_DIRECTORY;
	public static String GIFTCARD_IMAGE_DIRECTORY;
	public static String EGIFTCARD_DIRECTORY;
	public static String RTFMEDIA_DIRECTORY;
	public static String RTFMEDIA_THUMBNAIL;
	public static String RTFCUSTMEDIA_DIRECTORY;
	public static String POLLING_SPONSOR_DIRECTORY;
	
	public static String FANCLUB_MEDIA;
	


	@Override
	public void afterPropertiesSet() throws Exception {
		URLUtil.FIREBASE_CONFIG_FILE = getBaseDirectory()+"FireBase/google-services.json";
		URLUtil.FIREBASE_WEB_CONFIG_FILE = getBaseDirectory()+"FireBase/google-services-web.json";
		//URLUtil.FIRESTORE_CONFIG_FILE = getBaseDirectory()+"FireBase/firestore-services.json";
		URLUtil.CARDS_DIRECTORY = getBaseDirectory()+"cards/";
		URLUtil.ARTIST_DIRECTORY = getBaseDirectory()+"ArtistImage/";
		URLUtil.PARENT_DIRECTORY = getBaseDirectory()+"ParentCategoryImage/";
		URLUtil.CHILD_DIRECTORY = getBaseDirectory()+"ChildCategoryImage/";
		URLUtil.GRANDCHILD_DIRECTORY = getBaseDirectory()+"GrandChildCategoryImage/";
		URLUtil.TICKET_DIRECTORY = getBaseDirectory()+"Tickets/";
		URLUtil.LOYALFAN_DIRECTORY = getBaseDirectory()+"LoyalFanParentCategoryImage/";
		URLUtil.FANTASY_DIRECTORY = getBaseDirectory()+"FantasyGrandChildCategoryImage/";
		URLUtil.REPORT_TEMPLATE_DIRECTORY = getBaseDirectory()+"reportTemplate/";
		URLUtil.GIFTCARD_IMAGE_DIRECTORY = getBaseDirectory()+"GiftCardImage/";
		URLUtil.EGIFTCARD_DIRECTORY = getBaseDirectory()+"EGiftCards/";
		URLUtil.RTFMEDIA_DIRECTORY = getBaseDirectory()+"RTFMedia/";
		URLUtil.RTFMEDIA_THUMBNAIL = getBaseDirectory()+"RTFMediaThumbnail/";
		URLUtil.RTFCUSTMEDIA_DIRECTORY = getBaseDirectory()+"TMPAWS/";
		URLUtil.POLLING_SPONSOR_DIRECTORY = getBaseDirectory()+"SponsorImages/";
		
		URLUtil.FANCLUB_MEDIA = getBaseDirectory()+"/fanclubmedia/";
		
		System.out.println(URLUtil.FIREBASE_CONFIG_FILE);
		
	}
	
}