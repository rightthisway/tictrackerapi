package com.rtw.tracker.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.context.SecurityContextHolder;

import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.EventDetails;
import com.rtw.tracker.datas.PopularEvents;
import com.rtw.tracker.datas.Product;




/**
 * Hold constant variables
 */

public final class EventUtil {
	
	
	public String addPopulerEvents(HttpServletRequest request) throws Exception {
		
		String returnMessage ="";
		try{
			Product product = DAORegistry.getProductDAO().getProductByName(Constants.Reward_The_Fan_Product);
			
			String action = request.getParameter("action");
			if(action != null && action.equals("create")){
				
				String eventIdStr = request.getParameter("eventIds");
				String[] eventIdsArr = null;
				if(eventIdStr!=null) {
					eventIdsArr = eventIdStr.split(",");
				}
				Collection<PopularEvents> popEventsDB = DAORegistry.getPopularEventsDAO().getAllActivePopularEventsByProductId(product.getId());
				Map<Integer,PopularEvents> popEventsMapDB = new HashMap<Integer, PopularEvents>();
				for (PopularEvents popularEvents : popEventsDB) {
					popEventsMapDB.put(popularEvents.getEventDetails().getEventId(), popularEvents);
				}
				if(eventIdsArr != null) {
					String username = SecurityContextHolder.getContext().getAuthentication().getName();
					List<PopularEvents> popularEventsList = new ArrayList<PopularEvents>();
					for (String eventStr : eventIdsArr) {
						Integer eventId= Integer.parseInt(eventStr);
						if(null != popEventsMapDB.get(eventId)) {
							continue;
						}
						EventDetails eventDetail = DAORegistry.getEventDetailsDAO().get(eventId);
						if(eventDetail == null) {
							continue;
						}
						PopularEvents popEvent = new PopularEvents();
						popEvent.setEventId(eventId);
						popEvent.setStatus("ACTIVE");
						popEvent.setCreatedDate(new Date());
						popEvent.setCreatedBy(username);
						popEvent.setProductId(product.getId());
						//popEvent.setEventDetails(eventDetail);
						
						popularEventsList.add(popEvent);
					}
					DAORegistry.getPopularEventsDAO().saveAll(popularEventsList);
				}
				
				returnMessage = "Popular Events added successfully";
				//map.put("successMessage", returnMessage);
			} else if(action != null && action.equals("delete")){
				String eventIdStr = request.getParameter("eventIds");
				String[] eventIdsArr = null;
				if(eventIdStr!=null) {
					eventIdsArr = eventIdStr.split(",");
				}
				Collection<PopularEvents> popEventsDB = DAORegistry.getPopularEventsDAO().getAllActivePopularEventsByProductId(product.getId());
				Map<Integer,PopularEvents> popEventsMapDB = new HashMap<Integer, PopularEvents>();
				for (PopularEvents popularEvents : popEventsDB) {
					popEventsMapDB.put(popularEvents.getEventDetails().getEventId(), popularEvents);
				}
				
				if(eventIdsArr != null) {
					List<PopularEvents> popularEventsList = new ArrayList<PopularEvents>();
					for (String eventStr : eventIdsArr) {
						Integer eventId= Integer.parseInt(eventStr);
						PopularEvents popularEvent = popEventsMapDB.get(eventId);
						if(null != popularEvent) {
							popularEventsList.add(popularEvent);
						}
						/*EventDetails eventDetail = DAORegistry.getEventDetailsDAO().get(eventId);
						if(eventDetail == null) {
							continue;
						}
						PopularEvents popEvent = new PopularEvents();
						popEvent.setEventId(eventId);
						popEvent.setStatus("ACTIVE");
						popEvent.setCreatedDate(new Date());
						popEvent.setCreatedBy("");
						//popEvent.setEventDetails(eventDetail);
						
						popularEventsList.add(popEvent);*/
					}
					DAORegistry.getPopularEventsDAO().deleteAll(popularEventsList);
				}
				
				returnMessage = "Popular Events Removed successfully";
			}
		}catch (Exception e) {
			e.printStackTrace();
			//map.put("errorMessage", "There is something wrong..Please Try Again.");
			returnMessage = "There is something wrong..Please Try Again.";
		}
		return returnMessage;
	}
}
