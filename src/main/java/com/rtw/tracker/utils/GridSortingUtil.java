package com.rtw.tracker.utils;

public class GridSortingUtil {

	// artist:aaa,grandChildCategory:bbb,childCategory:ccc,parentCategory:ddd,visibleInSearch:eee,
	public static String getManageArtistSearchHeaderFilters(String columnName)throws Exception{
		String sortingSql = null;
		try {
			if(columnName!=null && !columnName.isEmpty()){
				if(columnName.equalsIgnoreCase("artist")){
					sortingSql="";
				}else if(columnName.equalsIgnoreCase("grandChildCategory")){
					sortingSql="";
				}else if(columnName.equalsIgnoreCase("childCategory")){
					sortingSql="";
				}else if(columnName.equalsIgnoreCase("parentCategory")){
					sortingSql="";
				}else if(columnName.equalsIgnoreCase("visibleInSearch")){
					sortingSql="";
				}else if(columnName.equalsIgnoreCase("eventCount")){
					sortingSql="";
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return sortingSql;
	}
	
	// customerType:aaa,customerName:bbb,lastName:ccc,productType:ddd,signupType:eee,client:fff,broker:ggg,customerStatus:hhh,companyName:iii,email:jjj,
	// street1:kkk,city:lll,state:mmm,country:nnn,zip:ooo,phone:ppp,referrerCode:qqq.
	public static GridHeaderFilters getCustomerSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length ==2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("customerId")){
							filter.setCustomerId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("customerType")){
							filter.setCustomerType(value);
						}else if(name.equalsIgnoreCase("firstName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("customerName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("lastName")){
							filter.setLastName(value);
						}else if(name.equalsIgnoreCase("productType")){
							filter.setProductType(value);
						}else if(name.equalsIgnoreCase("signupType")){
							filter.setSignupType(value);
						}else if(name.equalsIgnoreCase("client")){
							if(value.toLowerCase().contains("y")){
								filter.setIsClient(1);
							}else{
								filter.setIsClient(0);
							}
						}else if(name.equalsIgnoreCase("broker")){
							if(value.toLowerCase().contains("y")){
								filter.setIsBroker(1);
							}else{
								filter.setIsBroker(0);
							}
						}else if(name.equalsIgnoreCase("customerStatus")){
							filter.setCustomerStatus(value);
						}else if(name.equalsIgnoreCase("companyName")){
							filter.setCompanyName(value);
						}else if(name.equalsIgnoreCase("email")){
							filter.setEmail(value);
						}else if(name.equalsIgnoreCase("street1")){
							filter.setAddressLine1(value);
						}else if(name.equalsIgnoreCase("street2")){
							filter.setAddressLine2(value);
						}else if(name.equalsIgnoreCase("city")){
							filter.setCity(value);
						}else if(name.equalsIgnoreCase("state")){
							filter.setState(value);
						}else if(name.equalsIgnoreCase("country")){
							filter.setCountry(value);
						}else if(name.equalsIgnoreCase("zip")){
							filter.setZipCode(value);
						}else if(name.equalsIgnoreCase("phone")){
							filter.setPhone(value);
						}else if(name.equalsIgnoreCase("referrerCode")){
							filter.setReferrerCode(value);
						}else if(name.equalsIgnoreCase("rewardPoints")){
							filter.setRewardPoints(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("emailSent")){
							filter.setEmailSent(value);
						}else if(name.equalsIgnoreCase("retailCust")){
							filter.setRetailCustomer(value);
						}						
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	// purchaseOrderId:aaa,customerName:bbb,customerType:ccc,poTotal:ddd,ticketQty:eee,created:fff,createdBy:ggg,shippingType:hhh,status:iii,productType:jjj,
	// csr:kkk,poAged:lll,lastUpdated:mmm,isEmailed:nnn,consignmentPoNo:ooo,transactionOffice:ppp,trackingNo:qqq,shippingNotes:rrr,externalNotes:sss,internalNotes:ttt,
	// eventId:uuu,eventName:vvv,eventDate:www,eventTime:xxx.
	public static GridHeaderFilters getPOSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >=2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("purchaseOrderId")){
							filter.setPurchaseOrderId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("id")){
							filter.setPurchaseOrderId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("customerName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("customerType")){
							filter.setCustomerType(value);
						}else if(name.equalsIgnoreCase("poTotal")){
							filter.setPoTotal(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("ticketQty")){
							filter.setTicketQty(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("created")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("shippingType")){
							filter.setShippingType(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("productType")){
							filter.setProductType(value);
						}else if(name.equalsIgnoreCase("csr")){
							filter.setCsr(value);
						}else if(name.equalsIgnoreCase("poAged")){
							filter.setPoAged(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("lastUpdated")){
							filter.setLastUpdatedDateStr(value);
						}else if(name.equalsIgnoreCase("isEmailed")){
							if(value.toLowerCase().contains("y")){
								filter.setIsEmailed(1);
							}else{
								filter.setIsEmailed(0);
							}
						}else if(name.equalsIgnoreCase("consignmentPoNo")){
							filter.setConsignmentPoNo(value);
						}else if(name.equalsIgnoreCase("transactionOffice")){
							filter.setTransactionOffice(value);
						}else if(name.equalsIgnoreCase("trackingNo")){
							filter.setTrackingNo(value);
						}else if(name.equalsIgnoreCase("shippingNotes")){
							filter.setShippingNotes(value);
						}else if(name.equalsIgnoreCase("externalNotes")){
							filter.setExternalNotes(value);
						}else if(name.equalsIgnoreCase("internalNotes")){
							filter.setInternalNotes(value);
						}else if(name.equalsIgnoreCase("eventId")){
							filter.setEventId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("eventName")){
							filter.setEventName(value);
						}else if(name.equalsIgnoreCase("eventDate")){
							filter.setEventDateStr(value);
						}else if(name.equalsIgnoreCase("eventTime")){
							filter.setEventTimeStr(value);
						}else if(name.equalsIgnoreCase("purchaseOrderType")){
							filter.setPurchaseOrderType(value);
						}else if(name.equalsIgnoreCase("eventTicketQty")){
							filter.setEventTixQty(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("eventTicketCost")){
							filter.setEventTicketCost(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("eventTicketQty")){
							filter.setEventTixQty(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("RTWPO")){
							filter.setPosPOId(value);
						}else if(name.equalsIgnoreCase("brokerId")){
							filter.setBrokerId(Integer.parseInt(value));
						}
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	// invId:aaa,customerId:bbb,customerName:ccc,username:ddd,ticketCount:eee,invoiceTotal:fff,invoiceAged:ggg,createdDate:hhh,createdBy:iii,csr:jjj,
	// orderId:kkk,shippingMethod:lll,customerType:mmm,companyName:nnn,productType:ooo,street1:ppp,city:qqq,state:rrr,country:sss,zipCode:ttt,
	// phone:uuu,secondaryOrderId:vvv,secondaryOrderType:www,voidedDate:xxx,lastUpdatedDate:yyy,lastUpdatedBy:zzz,
	// status:aaaa, trackingNo:bbbb, viewFedexLabel:cccc, eventId:dddd, eventName:eeee, eventDate:ffff, eventTime:gggg, extNp:hhhh.
	public static GridHeaderFilters getInvoiceSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >=2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("invId")){
							filter.setInvoiceId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("invoiceId")){
							filter.setInvoiceId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("customerId")){
							filter.setCustomerId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("customerName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("username")){
							filter.setUsername(value);
						}else if(name.equalsIgnoreCase("ticketCount")){
							filter.setTicketQty(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("invoiceTotal")){
							filter.setInvoiceTotal(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("invoiceAged")){
							filter.setInvoiceAged(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("csr")){
							filter.setCsr(value);
						}else if(name.equalsIgnoreCase("orderId")){
							filter.setCustomerOrderId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("shippingMethod")){
							filter.setShippingMethod(value);
						}else if(name.equalsIgnoreCase("customerType")){
							filter.setCustomerType(value);
						}else if(name.equalsIgnoreCase("companyName")){
							filter.setCompanyName(value);
						}else if(name.equalsIgnoreCase("productType")){
							filter.setProductType(value);
						}else if(name.equalsIgnoreCase("street1")){
							filter.setAddressLine1(value);
						}else if(name.equalsIgnoreCase("city")){
							filter.setCity(value);
						}else if(name.equalsIgnoreCase("state")){
							filter.setState(value);
						}else if(name.equalsIgnoreCase("country")){
							filter.setCountry(value);
						}else if(name.equalsIgnoreCase("zipCode")){
							filter.setZipCode(value);
						}else if(name.equalsIgnoreCase("phone")){
							filter.setPhone(value);
						}else if(name.equalsIgnoreCase("secondaryOrderId")){
							filter.setSecondaryOrderId(value);
						}else if(name.equalsIgnoreCase("secondaryOrderType")){
							filter.setSecondaryOrderType(value);
						}else if(name.equalsIgnoreCase("voidedDate")){
							filter.setVoidedDateStr(value);
						}else if(name.equalsIgnoreCase("lastUpdatedDate")){
							filter.setLastUpdatedDateStr(value);
						}else if(name.equalsIgnoreCase("lastUpdatedBy")){
							filter.setLastUpdatedBy(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("trackingNo")){
							filter.setTrackingNo(value);
						}else if(name.equalsIgnoreCase("viewFedexLabel")){
							filter.setViewFedexLabel(value);
						}else if(name.equalsIgnoreCase("eventId")){
							filter.setEventId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("eventName")){
							filter.setEventName(value);
						}else if(name.equalsIgnoreCase("eventDate")){
							filter.setEventDateStr(value);
						}else if(name.equalsIgnoreCase("eventTime")){
							filter.setEventTimeStr(value);
						}else if(name.equalsIgnoreCase("extNo")){
							filter.setExternalNo(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("orderType")){
							filter.setOrderType(value);
						}else if(name.equalsIgnoreCase("brokerId")){
							filter.setBrokerId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("loyalFan")){
							filter.setOrderType(value);
						}else if(name.equalsIgnoreCase("platform")){
							filter.setPlatform(value);
						}else if(name.equalsIgnoreCase("primaryPaymentMethod")){
							filter.setPrimaryPaymentMethod(value);
						}else if(name.equalsIgnoreCase("secondaryPaymentMethod")){
							filter.setSecondaryPaymentMethod(value);
						}else if(name.equalsIgnoreCase("thirdPaymentMethod")){
							filter.setThirdPaymentMethod(value);
						}else if(name.equalsIgnoreCase("isPOMapped")){
							filter.setIsPoMapped(value);
						}else if(name.equalsIgnoreCase("pendingAmount")){
							filter.setPackageCost(Double.parseDouble(value));
						}						
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	// orderId:aaa,orderDateTime:bbb,eventName:ccc,eventDate:ddd,eventTime:eee,venue:fff,section:ggg,row:hhh,quantity:iii,totalSale:jjj,
	// totalPayment:kkk,status:lll,lastUpdatedDate:mmm,ticTrackerOrderId:nnn,ticTrackerInvoiceId:ooo.
	public static GridHeaderFilters getSeatGeekSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >=2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("orderId")){
							filter.setSeatgeekOrderId(value);
						}else if(name.equalsIgnoreCase("orderDateTime")){
							filter.setOrderDateStr(value);
						}else if(name.equalsIgnoreCase("eventName")){
							filter.setEventName(value);
						}else if(name.equalsIgnoreCase("eventDate")){
							filter.setEventDateStr(value);
						}else if(name.equalsIgnoreCase("eventTime")){
							filter.setEventTimeStr(value);
						}else if(name.equalsIgnoreCase("venue")){
							filter.setVenueName(value);
						}else if(name.equalsIgnoreCase("section")){
							filter.setSection(value);
						}else if(name.equalsIgnoreCase("row")){
							filter.setRow(value);
						}else if(name.equalsIgnoreCase("quantity")){
							filter.setQuantity(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("totalSale")){
							filter.setTotalSaleAmt(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("totalPayment")){
							filter.setTotalPaymentAmt(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("lastUpdatedDate")){
							filter.setLastUpdatedDateStr(value);
						}else if(name.equalsIgnoreCase("ticTrackerOrderId")){
							filter.setCustomerOrderId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("ticTrackerInvoiceId")){
							filter.setInvoiceId(Integer.parseInt(value));
						}						
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}

	// invoiceId:aaa,orderId:bbb,invoiceDateTime:ccc,eventName:ddd,eventDate:eee,eventTime:fff,venue:ggg,productType:hhh,internalNotes:iii,quantity:jjj,
	// section:kkk,row:lll,soldPrice:mmm,marketPrice:nnn,lastUpdatedPrice:ooo,availableSectionTixCount:ppp,availableEventTixCount:qqq,totalSoldPrice:rrr,
	// totalMarketPrice:sss,PLValue:ttt,priceUpdatedCount:uuu,price:vvv,discountCoupenPrice:www,url:xxx,shippingMethod:yyy,trackingNo:zzz,
	// secondaryOrderType:aaaa, secondaryOrderId:bbbb, lastUpdated:cccc, eventId:dddd.
	public static String getOpenOrShipmentPendingOrClosedOrderSortingSql(GridHeaderFilters filter,String productType,String orderType){
		String sortingSql=null;
		String sortingCoulmn = filter.getSortingColumn();
		String sortingOrder = filter.getSortingOrder();
		try {
			if(sortingCoulmn!=null && !sortingCoulmn.isEmpty()){
				if(sortingCoulmn.equalsIgnoreCase("invoiceId")){
					sortingSql =" ORDER BY o.invoice_no "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("orderId")){
					if(productType.equalsIgnoreCase("REWARDTHEFAN")){
						sortingSql =" ORDER BY o.id  "+sortingOrder;
					}else{
						sortingSql =" ORDER BY o.ticket_request_id  "+sortingOrder;
					}
				}else if(sortingCoulmn.equalsIgnoreCase("invoiceDateTime")){
					sortingSql =" ORDER BY o.invoice_date "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("eventName")){
					sortingSql =" ORDER BY o.event_name "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("eventDate")){
					sortingSql =" ORDER BY o.event_date "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("eventTime")){
					sortingSql =" ORDER BY o.event_time "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("venue")){
					sortingSql =" ORDER BY o.venue_name "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("venueCity")){
					sortingSql =" ORDER BY o.venue_city "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("venueState")){
					sortingSql =" ORDER BY o.venue_state "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("venueCountry")){
					sortingSql =" ORDER BY o.venue_country "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("productType")){
					sortingSql =" ORDER BY o.product_type "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("internalNotes")){
					sortingSql =" ORDER BY o.internal_notes "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("quantity")){
					sortingSql =" ORDER BY o.sold_qty "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("section")){
					if(orderType.equalsIgnoreCase("OPEN")){
						sortingSql =" ORDER BY o.section "+sortingOrder;
					}else if(orderType.equalsIgnoreCase("PENDING SHIPMENT")){
						sortingSql =" ORDER BY otg.section "+sortingOrder;
					}else if(orderType.equalsIgnoreCase("PENDING RECIEPT")){
						sortingSql =" ORDER BY otg.section "+sortingOrder;
					}else if(orderType.equalsIgnoreCase("CLOSED")){
						sortingSql =" ORDER BY otg.section "+sortingOrder;
					}
				}else if(sortingCoulmn.equalsIgnoreCase("row")){
					if(orderType.equalsIgnoreCase("OPEN")){
						sortingSql =" ORDER BY o.row "+sortingOrder;
					}else if(orderType.equalsIgnoreCase("PENDING SHIPMENT")){
						sortingSql =" ORDER BY otg.row "+sortingOrder;
					}else if(orderType.equalsIgnoreCase("PENDING RECIEPT")){
						sortingSql =" ORDER BY otg.row "+sortingOrder;
					}else if(orderType.equalsIgnoreCase("CLOSED")){
						sortingSql =" ORDER BY otg.row "+sortingOrder;
					}
				}else if(sortingCoulmn.equalsIgnoreCase("soldPrice")){
					sortingSql =" ORDER BY o.actual_sold_price "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("marketPrice")){
					sortingSql =" ORDER BY o.market_price "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("lastUpdatedPrice")){
					sortingSql =" ORDER BY o.last_updated_price "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("availableSectionTixCount")){
					sortingSql =" ORDER BY o.section_ticket_qty "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("availableEventTixCount")){
					sortingSql =" ORDER BY o.event_ticket_qty "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("totalSoldPrice")){
					sortingSql =" ORDER BY o.net_total_sold_price "+sortingOrder; 
				}else if(sortingCoulmn.equalsIgnoreCase("PLValue")){
					sortingSql =" ORDER BY o.profit_loss "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("priceUpdatedCount")){
					sortingSql =" ORDER BY o.market_price_update_count "+sortingOrder;
				}/*else if(sortingCoulmn.equalsIgnoreCase("price")){
					sortingSql =" ORDER BY o.invoice_no "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("discountCoupenPrice")){
					sortingSql =" ORDER BY o.invoice_no "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("url")){
					sortingSql =" ORDER BY o.invoice_no "+sortingOrder;
				}*/else if(sortingCoulmn.equalsIgnoreCase("shippingMethod")){
					sortingSql =" ORDER BY sm.name "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("trackingNo")){
					sortingSql =" ORDER BY i.tracking_no "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("secondaryOrderType")){
					sortingSql =" ORDER BY co.secondary_order_type "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("secondaryOrderId")){
					sortingSql =" ORDER BY co.secondary_order_id "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("lastUpdated")){
					sortingSql =" ORDER BY o.last_updated "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("eventId")){
					sortingSql =" ORDER BY o.event_id "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("purchaseOrderId")){
					sortingSql =" ORDER BY p.id "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("purchaseOrderDateTime")){
					sortingSql =" ORDER BY p.created_time "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("zoneTixQty")){
					sortingSql =" ORDER BY o.zone_ticket_qty "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("zoneCheapestPrice")){
					sortingSql =" ORDER BY o.zone_cheapest_price "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("zoneTixGroupCount")){
					sortingSql =" ORDER BY o.zone_ticket_count "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("zoneTotalPrice")){
					sortingSql =" ORDER BY o.zone_total_price "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("zoneProfitAndLoss")){
					sortingSql =" ORDER BY o.zone_profit_loss "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("netTotalSoldPrice")){
					sortingSql =" ORDER BY o.net_total_sold_price "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("zoneMargin")){
					sortingSql =" ORDER BY o.zone_margin "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("sectionMargin")){
					sortingSql =" ORDER BY o.section_margin "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("netSoldPrice")){
					sortingSql =" ORDER BY o.net_sold_price "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("customerName")){
					sortingSql =" ORDER BY cus.cust_name "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("brokerId")){
					sortingSql =" ORDER BY o.broker_id "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("companyName")){
					sortingSql =" ORDER BY cus.company_name "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("platform")){
					sortingSql =" ORDER BY co.platform  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("zone")){
					sortingSql =" ORDER BY o.zone  "+sortingOrder;
				}						
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return sortingSql;
	}
	
	public static String getEventsSortingSql(GridHeaderFilters filter){
		String sortingSql=null;
		String sortingCoulmn = filter.getSortingColumn();
		String sortingOrder = filter.getSortingOrder();
		try {
			if(sortingCoulmn!=null && !sortingCoulmn.isEmpty())
			{
				if(sortingCoulmn.equalsIgnoreCase("eventTime")){
					sortingSql =" ORDER BY eventTime "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("eventDate")){
					sortingSql =" ORDER BY eventDate "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("eventMarketId")){
					sortingSql =" ORDER BY eventId  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("dayOfTheWeek")){
					sortingSql =" ORDER BY DATEPART(day,eventDate)  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("venue")){
					sortingSql =" ORDER BY building  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("venueId")){
					sortingSql =" ORDER BY venueId  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("noOfTix")){
					sortingSql =" ORDER BY noOfTixCount  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("noOfTixSold")){
					sortingSql =" ORDER BY noOfTixSoldCount  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("city")){
					sortingSql =" ORDER BY city "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("state")){
					sortingSql =" ORDER BY state "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("country")){
					sortingSql =" ORDER BY country  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("grandChildCategory")){
					sortingSql =" ORDER BY grandChildCategoryName like  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("childCategory")){
					sortingSql =" ORDER BY childCategory  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("parentCategory")){
					sortingSql =" ORDER BY parentCategory  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("eventCreated")){
					sortingSql =" ORDER BY eventCreationDate  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("eventLastUpdated")){
					sortingSql =" ORDER BY eventLastUpdated  "+sortingOrder;
			}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return sortingSql;
	}
	
	public static String getCustomerSortingSql(GridHeaderFilters filter){
		String sortingSql=null;
		String sortingCoulmn = filter.getSortingColumn();
		String sortingOrder = filter.getSortingOrder();
		try {
			if(sortingCoulmn!=null && !sortingCoulmn.isEmpty())
			{
				if(sortingCoulmn.equalsIgnoreCase("customerId")){
					sortingSql =" ORDER BY c.id "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("customerType")){
					sortingSql =" ORDER BY c.customer_type "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("firstName")){
					sortingSql =" ORDER BY c.cust_name  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("customerName")){
					sortingSql =" ORDER BY c.cust_name  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("lastName")){
					sortingSql =" ORDER BY c.last_name  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("userId")){
					sortingSql =" ORDER BY c.user_id  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("productType")){
					sortingSql =" ORDER BY c.product_type  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("signupType")){
					sortingSql =" ORDER BY c.signup_type  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("client")){
					sortingSql =" ORDER BY c.is_client "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("retailCust")){
					sortingSql =" ORDER BY c.is_retail_cust "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("broker")){
					sortingSql =" ORDER BY c.is_broker "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("customerStatus")){
					sortingSql =" ORDER BY c.customer_level  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("companyName")){
					sortingSql =" ORDER BY c.company_name  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("email")){
					sortingSql =" ORDER BY c.email  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("emailSent")){
					sortingSql =" ORDER BY c.is_mail_sent  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("street1")){
					sortingSql =" ORDER BY ca.address_line1  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("street2")){
					sortingSql =" ORDER BY ca.address_line2  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("city")){
					sortingSql =" ORDER BY ca.city  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("state")){
					sortingSql =" ORDER BY ca.city  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("country")){
					sortingSql =" ORDER BY ct.country_name  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("zip")){
					sortingSql =" ORDER BY ca.zip_code  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("phone")){
					sortingSql =" ORDER BY c.phone  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("referrerCode")){
					sortingSql =" ORDER BY c.referrer_code  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("rewardPoints")){
					sortingSql =" ORDER BY cl.active_reward_points   "+sortingOrder;
			}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return sortingSql;
	}
	
	
	public static String getPayPalTrackingSortingSql(GridHeaderFilters filter){
		String sortingSql=null;
		String sortingCoulmn = filter.getSortingColumn();
		String sortingOrder = filter.getSortingOrder();
		try {
			if(sortingCoulmn!=null && !sortingCoulmn.isEmpty())
			{
				if(sortingCoulmn.equalsIgnoreCase("customerId")){
					sortingSql =" ORDER BY pt.customer_id "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("firstName")){
					sortingSql =" ORDER BY ptd.payer_f_name  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("lastName")){
					sortingSql =" ORDER BY ptd.payer_l_name  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("email")){
					sortingSql =" ORDER BY ptd.payer_email  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("phone")){
					sortingSql =" ORDER BY ptd.payer_phone  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("orderId")){
					sortingSql =" ORDER BY ptd.order_id  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("orderTotal")){
					sortingSql =" ORDER BY pt.order_total "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("orderType")){
					sortingSql =" ORDER BY pt.order_type "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("eventId")){
					sortingSql =" ORDER BY pt.event_id "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("quantity")){
					sortingSql =" ORDER BY pt.qty  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("zone")){
					sortingSql =" ORDER BY pt.zone  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("paypalTransactionId")){
					sortingSql =" ORDER BY pt.paypal_transaction_id  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("transactionId")){
					sortingSql =" ORDER BY pt.transaction_id  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("paymentId")){
					sortingSql =" ORDER BY ptd.payment_id  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("platform")){
					sortingSql =" ORDER BY pt.platform  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("status")){
					sortingSql =" ORDER BY pt.status  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("createdDate")){
					sortingSql =" ORDER BY pt.created_date  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("lastUpdated")){
					sortingSql =" ORDER BY pt.last_updated  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("transactionDate")){
					sortingSql =" ORDER BY ptd.transaction_date  "+sortingOrder;	
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return sortingSql;
	}
	
	public static String getDiscountCodesSortingSql(GridHeaderFilters filter){
		String sortingSql=null;
		String sortingCoulmn = filter.getSortingColumn();
		String sortingOrder = filter.getSortingOrder();
		try {
			if(sortingCoulmn!=null && !sortingCoulmn.isEmpty())
			{
				if(sortingCoulmn.equalsIgnoreCase("promocode")){
					sortingSql =" ORDER BY rtf.promo_code "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("discountPerc")){
					sortingSql =" ORDER BY rtf.discount "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("mobileDiscountPerc")){
					sortingSql =" ORDER BY rtf.mobile_discount  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("startDate")){
					sortingSql =" ORDER BY rtf.start_date  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("endDate")){
					sortingSql =" ORDER BY rtf.end_date  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("maxOrder")){
					sortingSql =" ORDER BY rtf.max_orders  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("orders")){
					sortingSql =" ORDER BY rtf.no_of_orders  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("promoType")){
					sortingSql =" ORDER BY rtfpt.promo_type  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("artistId")){
					sortingSql =" ORDER BY rtfpt.artist_id "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("event")){
					sortingSql =" ORDER BY e.name "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("venue")){
					sortingSql =" ORDER BY v.building  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("parent")){
					sortingSql =" ORDER BY pc.name"+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("child")){
					sortingSql =" ORDER BY cc.name  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("grandChild")){
					sortingSql =" ORDER BY gcc.name  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("status")){
					sortingSql =" ORDER BY rtf.status  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("createdBy")){
					sortingSql =" ORDER BY rtf.created_by  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("createdDate")){
					sortingSql =" ORDER BY rtf.created_date  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("modifiedBy")){
					sortingSql =" ORDER BY rtf.modified_by  "+sortingOrder;				
			}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return sortingSql;
	}
	
	public static String getInvoicesSortingSql(GridHeaderFilters filter){
		String sortingSql=null;
		String sortingCoulmn = filter.getSortingColumn();
		String sortingOrder = filter.getSortingOrder();
		try {
			if(sortingCoulmn!=null && !sortingCoulmn.isEmpty())
			{
				if(sortingCoulmn.equalsIgnoreCase("invId")){
					sortingSql =" ORDER BY inv.id "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("customerId")){
					sortingSql =" ORDER BY cust.Id "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("customerName")){
					sortingSql =" ORDER BY cust.cust_name  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("companyName")){
					sortingSql =" ORDER BY cust.company_name  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("ticketCount")){
					sortingSql =" ORDER BY inv.ticket_count  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("invoiceAged")){
					sortingSql =" ORDER BY inv.created_date  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("eventName")){
					sortingSql =" ORDER BY co.event_name  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("eventDate")){
					sortingSql =" ORDER BY co.event_date  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("eventTime")){
					sortingSql =" ORDER BY co.event_time  "+sortingOrder;					 
				}else if(sortingCoulmn.equalsIgnoreCase("productType")){
					sortingSql =" ORDER BY co.product_type  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("brokerId")){
					sortingSql =" ORDER BY inv.broker_id  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("status")){
					sortingSql =" ORDER BY inv.status  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("lastUpdatedDate")){
					sortingSql =" ORDER BY inv.last_updated  "+sortingOrder;			
				}else if(sortingCoulmn.equalsIgnoreCase("lastUpdatedBy")){
					sortingSql =" ORDER BY inv.last_updated_by  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("createdDate")){
					sortingSql =" ORDER BY inv.created_date  "+sortingOrder;			
				}else if(sortingCoulmn.equalsIgnoreCase("createdBy")){
					sortingSql =" ORDER BY inv.created_by  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("trackingNo")){
					sortingSql =" ORDER BY inv.tracking_no  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("invoiceTotal")){
					sortingSql =" ORDER BY inv.invoice_total  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("voidedDate")){
					sortingSql =" ORDER BY voided_date  "+sortingOrder;
			}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return sortingSql;
	}
	
	public static String getRTWInvoicesSortingSql(GridHeaderFilters filter){
		String sortingSql=null;
		String sortingCoulmn = filter.getSortingColumn();
		String sortingOrder = filter.getSortingOrder();
		try {
			if(sortingCoulmn!=null && !sortingCoulmn.isEmpty())
			{
				if(sortingCoulmn.equalsIgnoreCase("invId")){
					sortingSql =" ORDER BY invoiceId "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("customerId")){
					sortingSql =" ORDER BY customerId "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("customerName")){
					sortingSql =" ORDER BY customerName  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("companyName")){
					sortingSql =" ORDER BY custCompanyName  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("ticketCount")){
					sortingSql =" ORDER BY ticketCount  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("invoiceAged")){
					sortingSql =" ORDER BY createdDate  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("eventName")){
					sortingSql =" ORDER BY eventName  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("eventDate")){
					sortingSql =" ORDER BY eventDate  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("eventTime")){
					sortingSql =" ORDER BY eventTime  "+sortingOrder;					 
				}else if(sortingCoulmn.equalsIgnoreCase("productType")){
					sortingSql =" ORDER BY productType  "+sortingOrder;	
				}else if(sortingCoulmn.equalsIgnoreCase("status")){
					sortingSql =" ORDER BY status  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("createdDate")){
					sortingSql =" ORDER BY createdDate  "+sortingOrder;			
				}else if(sortingCoulmn.equalsIgnoreCase("createdBy")){
					sortingSql =" ORDER BY createdBy  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("trackingNo")){
					sortingSql =" ORDER BY trackingNo  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("invoiceTotal")){
					sortingSql =" ORDER BY invoiceTotal  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("voidedDate")){
					sortingSql =" ORDER BY voidedDate  "+sortingOrder;
			}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return sortingSql;
	}
	
	public static String getPurchaseOrderSortingSql(GridHeaderFilters filter){
		String sortingSql=null;
		String sortingCoulmn = filter.getSortingColumn();
		String sortingOrder = filter.getSortingOrder();
		try {
			if(sortingCoulmn!=null && !sortingCoulmn.isEmpty())
			{
				if(sortingCoulmn.equalsIgnoreCase("purchaseOrderId")){
					sortingSql =" ORDER BY po.id "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("customerType")){
					sortingSql =" ORDER BY c.customer_type "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("customerName")){
					sortingSql =" ORDER BY c.cust_name  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("poTotal")){
					sortingSql =" ORDER BY po.po_total  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("ticketQty")){
					sortingSql =" ORDER BY po.ticket_count  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("created")){
					sortingSql =" ORDER BY po.created_time  "+sortingOrder;	
				}else if(sortingCoulmn.equalsIgnoreCase("createdBy")){
					sortingSql =" ORDER BY po.created_by  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("shippingType")){
					sortingSql =" ORDER BY sm.name  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("status")){
					sortingSql =" ORDER BY po.status  "+sortingOrder;					 
				}else if(sortingCoulmn.equalsIgnoreCase("csr")){
					sortingSql =" ORDER BY po.csr  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("poAged")){
					sortingSql =" ORDER BY datediff(dd,po.created_time,getdate())  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("eventId")){
					sortingSql =" ORDER BY t.event_id  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("evantName")){
					sortingSql =" ORDER BY t.event_name  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("eventDate")){
					sortingSql =" ORDER BY t.event_date  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("eventTime")){
					sortingSql =" ORDER BY t.event_time  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("brokerId")){
					sortingSql =" ORDER BY  po.broker_id  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("lastUpdated")){
					sortingSql =" ORDER BY po.last_updated  "+sortingOrder;	
				}else if(sortingCoulmn.equalsIgnoreCase("trackingNo")){
					sortingSql =" ORDER BY po.tracking_no  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("consignmentPoNo")){
					sortingSql =" ORDER BY po.consignment_po_no  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("purchaseOrderType")){
					sortingSql =" ORDER BY po.purchase_order_type  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("transactionOffice")){
					sortingSql =" ORDER BY po.transaction_office  "+sortingOrder;
			}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return sortingSql;
	}
	
	public static String getRTWPurchaseOrderSortingSql(GridHeaderFilters filter){
		String sortingSql=null;
		String sortingCoulmn = filter.getSortingColumn();
		String sortingOrder = filter.getSortingOrder();
		try {
			if(sortingCoulmn!=null && !sortingCoulmn.isEmpty())
			{
				if(sortingCoulmn.equalsIgnoreCase("purchaseOrderId")){
					sortingSql =" ORDER BY id "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("customerId")){
					sortingSql =" ORDER BY customerId "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("productType")){
					sortingSql =" ORDER BY product_type "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("customerName")){
					sortingSql =" ORDER BY customerName  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("customerType")){
					sortingSql =" ORDER BY customerType  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("poTotal")){
					sortingSql =" ORDER BY poTotal  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("eventTicketQty")){
					sortingSql =" ORDER BY eventTicketQty  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("ticketQty")){
					sortingSql =" ORDER BY ticketQty  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("created")){
					sortingSql =" ORDER BY created  "+sortingOrder;	
				}else if(sortingCoulmn.equalsIgnoreCase("createdBy")){
					sortingSql =" ORDER BY createdBy  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("shippingType")){
					sortingSql =" ORDER BY shippingType  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("status")){
					sortingSql =" ORDER BY status  "+sortingOrder;					 
				}else if(sortingCoulmn.equalsIgnoreCase("csr")){
					sortingSql =" ORDER BY csr  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("poAged")){
					sortingSql =" poAged  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("eventId")){
					sortingSql =" ORDER BY eventId  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("evantName")){
					sortingSql =" ORDER BY evantName  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("eventDate")){
					sortingSql =" ORDER BY eventDate  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("eventTime")){
					sortingSql =" ORDER BY eventTime  "+sortingOrder;
//				}else if(sortingCoulmn.equalsIgnoreCase("brokerId")){
//					sortingSql =" ORDER BY  po.broker_id  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("lastUpdated")){
					sortingSql =" ORDER BY lastUpdated  "+sortingOrder;	
				}else if(sortingCoulmn.equalsIgnoreCase("trackingNo")){
					sortingSql =" ORDER BY trackingNo  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("consignmentPoNo")){
					sortingSql =" ORDER BY consignmentPoNo  "+sortingOrder;
//				}else if(sortingCoulmn.equalsIgnoreCase("purchaseOrderType")){
//					sortingSql =" ORDER BY po.purchase_order_type  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("transactionOffice")){
					sortingSql =" ORDER BY transactionOffice  "+sortingOrder;
			}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return sortingSql;
	}
	
	public static String getGiftCardsSortingSql(GridHeaderFilters filter){
		String sortingSql=null;
		String sortingCoulmn = filter.getSortingColumn();
		String sortingOrder = filter.getSortingOrder();
		try {
			if(sortingCoulmn!=null && !sortingCoulmn.isEmpty())
			{
				if(sortingCoulmn.equalsIgnoreCase("giftCardId")){
					sortingSql =" ORDER BY gc.id "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("title")){
					sortingSql =" ORDER BY gc.title  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("description")){
					sortingSql =" ORDER BY gc.description  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("imageUrl")){
					sortingSql =" ORDER BY gc.imageUrl  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("conversionType")){
					sortingSql =" ORDER BY gc.conversion_type  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("conversionRate")){
					sortingSql =" ORDER BY gc.conversion_rate  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("status")){
					sortingSql =" ORDER BY gc.status  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("createdDate")){
					sortingSql =" ORDER BY gc.created_date  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("updatedDate")){
					sortingSql =" ORDER BY gc.updated_date  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("createdBy")){
					sortingSql =" ORDER BY gc.created_by  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("updatedBy")){
					sortingSql =" ORDER BY gc.updated_by  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("maxQtyThreshold")){
					sortingSql =" ORDER BY gc.max_qty_threshold  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("maxQuantity")){
					sortingSql =" ORDER BY gcv.max_quantity  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("amount")){
					sortingSql =" ORDER BY gcv.card_amount  "+sortingOrder;			
				}else if(sortingCoulmn.equalsIgnoreCase("remainingQty")){
					sortingSql =" ORDER BY gcv.remaining_quantity  "+sortingOrder;
			}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return sortingSql;
	}
	
	public static String getPromotionalOfferTrackingSortingSql(GridHeaderFilters filter){
		String sortingSql=null;
		String sortingCoulmn = filter.getSortingColumn();
		String sortingOrder = filter.getSortingOrder();
		try {
			if(sortingCoulmn!=null && !sortingCoulmn.isEmpty())
			{
				if(sortingCoulmn.equalsIgnoreCase("firstName")){
					sortingSql =" ORDER BY cus.cust_name "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("lastName")){
					sortingSql =" ORDER BY cus.last_name  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("customerId")){
					sortingSql =" ORDER BY cus.id  "+sortingOrder;	
				}else if(sortingCoulmn.equalsIgnoreCase("email")){
					sortingSql =" ORDER BY cus.email  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("discount")){
					sortingSql =" ORDER BY rtfpt.additional_discount_conv  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("status")){
					sortingSql =" ORDER BY rtfpt.status  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("createdDate")){
					sortingSql =" ORDER BY creq.created_time  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("promoCode")){
					sortingSql =" ORDER BY rtfpt.promo_code  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("primaryPaymentMethod")){
					sortingSql =" ORDER BY creq.primary_payment_method  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("secondaryPaymentMethod")){
					sortingSql =" ORDER BY creq.secondary_payment_method  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("thirdPaymentMethod")){
					sortingSql =" ORDER BY creq.third_payment_method  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("platform")){
					sortingSql =" ORDER BY creq.platform  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("ipAddress")){
					sortingSql =" ORDER BY creq.ip_address  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("quantity")){
					sortingSql =" ORDER BY creq.quantity  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("orderId")){
					sortingSql =" ORDER BY rtfpt.order_id  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("orderTotal")){
					sortingSql =" ORDER BY creq.order_total  "+sortingOrder;
			}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return sortingSql;
	}
	
	public static String getCustomerPromotionalOfferSortingSql(GridHeaderFilters filter){
		String sortingSql=null;
		String sortingCoulmn = filter.getSortingColumn();
		String sortingOrder = filter.getSortingOrder();
		try {
			if(sortingCoulmn!=null && !sortingCoulmn.isEmpty())
			{
				if(sortingCoulmn.equalsIgnoreCase("firstName")){
					sortingSql =" ORDER BY cus.cust_name "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("lastName")){
					sortingSql =" ORDER BY cus.last_name  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("email")){
					sortingSql =" ORDER BY cus.email  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("discount")){
					sortingSql =" ORDER BY rtfcp.discount  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("status")){
					sortingSql =" ORDER BY rtfcp.status  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("createdDate")){
					sortingSql =" ORDER BY rtfcp.created_date  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("promoCode")){
					sortingSql =" ORDER BY rtfcp.promo_code  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("endDate")){
					sortingSql =" ORDER BY rtfcp.end_date  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("startDate")){
					sortingSql =" ORDER BY rtfcp.start_date  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("modifiedDate")){
					sortingSql =" ORDER BY rtfcp.modified_time  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("ipAddress")){
					sortingSql =" ORDER BY creq.ip_address  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("quantity")){
					sortingSql =" ORDER BY creq.quantity  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("orderId")){
					sortingSql =" ORDER BY rtfpt.order_id  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("orderTotal")){
					sortingSql =" ORDER BY creq.order_total  "+sortingOrder;
			}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return sortingSql;
	}
	
	public static String getPollingSponsorsSortingSql(GridHeaderFilters filter){
		String sortingSql=null;
		String sortingCoulmn = filter.getSortingColumn();
		String sortingOrder = filter.getSortingOrder();
		try {
			if(sortingCoulmn!=null && !sortingCoulmn.isEmpty())
			{
				if(sortingCoulmn.equalsIgnoreCase("id")){
					sortingSql =" ORDER BY g.id "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("title")){
					sortingSql =" ORDER BY g.title  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("description")){
					sortingSql =" ORDER BY g.description  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("contactPerson")){
					sortingSql =" ORDER BY g.contactPerson  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("email")){
					sortingSql =" ORDER BY g.email  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("phone")){
					sortingSql =" ORDER BY g.phone  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("status")){
					sortingSql =" ORDER BY g.status  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("altPhone")){
					sortingSql =" ORDER BY g.altPhone  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("address")){
					sortingSql =" ORDER BY g.address  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("createdDate")){
					sortingSql =" ORDER BY g.created_date  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("updatedDate")){
					sortingSql =" ORDER BY g.updated_date  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("createdBy")){
					sortingSql =" ORDER BY g.created_by  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("updatedBy")){
					sortingSql =" ORDER BY g.updated_by  "+sortingOrder;
			}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return sortingSql;
	}
	
	public static String getPollingContestSortingSql(GridHeaderFilters filter){
		String sortingSql=null;
		String sortingCoulmn = filter.getSortingColumn();
		String sortingOrder = filter.getSortingOrder();
		try {
			if(sortingCoulmn!=null && !sortingCoulmn.isEmpty())
			{
				if(sortingCoulmn.equalsIgnoreCase("pollingId")){
					sortingSql =" ORDER BY g.id "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("title")){
					sortingSql =" ORDER BY g.title  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("description")){
					sortingSql =" ORDER BY g.description  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("endDate")){
					sortingSql =" ORDER BY g.end_date  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("startDate")){
					sortingSql =" ORDER BY g.start_date  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("status")){
					sortingSql =" ORDER BY g.status  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("pollingInterval")){
					sortingSql =" ORDER BY g.polling_interval  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("maxQuePerCust")){
					sortingSql =" ORDER BY g.max_que_per_cust  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("createdDate")){
					sortingSql =" ORDER BY g.created_date  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("updatedDate")){
					sortingSql =" ORDER BY g.updated_date  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("createdBy")){
					sortingSql =" ORDER BY g.created_by  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("updatedBy")){
					sortingSql =" ORDER BY g.updated_by  "+sortingOrder;
			}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return sortingSql;
	}
	
	public static String getPollingCategorySortingSql(GridHeaderFilters filter){
		String sortingSql=null;
		String sortingCoulmn = filter.getSortingColumn();
		String sortingOrder = filter.getSortingOrder();
		try {
			if(sortingCoulmn!=null && !sortingCoulmn.isEmpty())
			{
				if(sortingCoulmn.equalsIgnoreCase("id")){
					sortingSql =" ORDER BY g.id "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("title")){
					sortingSql =" ORDER BY g.title  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("sponsorName")){
					sortingSql =" ORDER BY ps.name  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("status")){
					sortingSql =" ORDER BY g.status  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("pollingType")){
					sortingSql =" ORDER BY g.polling_type  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("createdDate")){
					sortingSql =" ORDER BY g.created_date  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("updatedDate")){
					sortingSql =" ORDER BY g.updated_date  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("createdBy")){
					sortingSql =" ORDER BY g.created_by  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("updatedBy")){
					sortingSql =" ORDER BY g.updated_by  "+sortingOrder;
			}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return sortingSql;
	}
	
	public static String getPollingVideoCategorySortingSql(GridHeaderFilters filter){
		String sortingSql=null;
		String sortingCoulmn = filter.getSortingColumn();
		String sortingOrder = filter.getSortingOrder();
		try {
			if(sortingCoulmn!=null && !sortingCoulmn.isEmpty())
			{
				if(sortingCoulmn.equalsIgnoreCase("id")){
					sortingSql =" ORDER BY g.id "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("categoryName")){
					sortingSql =" ORDER BY g.category_name  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("description")){
					sortingSql =" ORDER BY g.description  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("sequenceNum")){
					sortingSql =" ORDER BY g.sequence_num  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("imageUrl")){
					sortingSql =" ORDER BY g.image_url  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("createdDate")){
					sortingSql =" ORDER BY g.created_date  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("updatedDate")){
					sortingSql =" ORDER BY g.updated_date  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("createdBy")){
					sortingSql =" ORDER BY g.created_by  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("updatedBy")){
					sortingSql =" ORDER BY g.updated_by  "+sortingOrder;
			}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return sortingSql;
	}
	public static String getRTFVideosSortingSql(GridHeaderFilters filter){
		String sortingSql=null;
		String sortingCoulmn = filter.getSortingColumn();
		String sortingOrder = filter.getSortingOrder();
		try {
			if(sortingCoulmn!=null && !sortingCoulmn.isEmpty())
			{
				if(sortingCoulmn.equalsIgnoreCase("isDefault")){
					sortingSql =" ORDER BY is_default "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("pollingVideoId")){
					sortingSql =" ORDER BY id "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("title")){
					sortingSql =" ORDER BY title  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("description")){
					sortingSql =" ORDER BY description  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("videoUrl")){
					sortingSql =" ORDER BY video_url  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("status")){
					sortingSql =" ORDER BY status  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("category")){
					sortingSql =" ORDER BY category  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("createdDate")){
					sortingSql =" ORDER BY created_date  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("createdBy")){
					sortingSql =" ORDER BY created_by  "+sortingOrder;
			}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return sortingSql;
	}
	
	public static String getCustomerVideosSortingSql(GridHeaderFilters filter){
		String sortingSql=null;
		String sortingCoulmn = filter.getSortingColumn();
		String sortingOrder = filter.getSortingOrder();
		try {
			if(sortingCoulmn!=null && !sortingCoulmn.isEmpty())
			{
				if(sortingCoulmn.equalsIgnoreCase("vedioId")){
					sortingSql =" ORDER BY pcv.id "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("customerName")){
					sortingSql =" ORDER BY c.cust_name  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("phone")){
					sortingSql =" ORDER BY c.phone  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("userId")){
					sortingSql =" ORDER BY customerId  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("status")){
					sortingSql =" ORDER BY pcv.status  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("videoType")){
					sortingSql =" ORDER BY category  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("playType")){
					sortingSql =" ORDER BY pcv.status  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("uploadDate")){
					sortingSql =" ORDER BY upload_date  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("fileName")){
					sortingSql =" ORDER BY video_url  "+sortingOrder;
			}
			}
		} catch (Exception e) {
			
			e.printStackTrace();			
		}
		return sortingSql;
	}
	
	public static String getContestTrackingSql(GridHeaderFilters filter){
		String sortingSql=null;
		String sortingCoulmn = filter.getSortingColumn();
		String sortingOrder = filter.getSortingOrder();
		try {
			if(sortingCoulmn!=null && !sortingCoulmn.isEmpty())
			{
				if(sortingCoulmn.equalsIgnoreCase("customerIpAddress")){
					sortingSql =" ORDER BY cust_ip_addr "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("platform")){
					sortingSql =" ORDER BY platform "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("apiName")){
					sortingSql =" ORDER BY api_name  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("hittingDate")){
					sortingSql =" ORDER BY start_date  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("description")){
					sortingSql =" ORDER BY description  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("custId")){
					sortingSql =" ORDER BY cust_id  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("contestId")){
					sortingSql =" ORDER BY contest_id  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("appVersion")){
					sortingSql =" ORDER BY app_ver "+sortingOrder;							
			}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return sortingSql;
	}
	public static String getAllActionsByDateRangeAndServerIpSql(GridHeaderFilters filter){
		String sortingSql=null;
		String sortingCoulmn = filter.getSortingColumn();
		String sortingOrder = filter.getSortingOrder();
		try {
			if(sortingCoulmn!=null && !sortingCoulmn.isEmpty())
			{
				if(sortingCoulmn.equalsIgnoreCase("customerIpAddress")){
					sortingSql =" ORDER BY customer_ip_address "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("platform")){
					sortingSql =" ORDER BY platform "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("apiName")){
					sortingSql =" ORDER BY api_name  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("hittingDate")){
					sortingSql =" ORDER BY hit_date  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("description")){
					sortingSql =" ORDER BY action_result  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("custId")){
					sortingSql =" ORDER BY customer_id  "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("contestId")){
					sortingSql =" ORDER BY contest_id  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("appVersion")){
					sortingSql =" ORDER BY app_ver "+sortingOrder;							
			}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return sortingSql;
	}
	public static String getAllArtistSortingSql(GridHeaderFilters filter){
		String sortingSql=null;
		String sortingCoulmn = filter.getSortingColumn();
		String sortingOrder = filter.getSortingOrder();
		try {
			if(sortingCoulmn!=null && !sortingCoulmn.isEmpty())
			{
				if(sortingCoulmn.equalsIgnoreCase("artistId")){
					sortingSql =" ORDER BY ar.id "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("artist")){
					sortingSql =" ORDER BY REPLACE(ar.name,'\"','') "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("grandChildCategory")){
					sortingSql =" ORDER BY gc.name "+sortingOrder;
				}else if(sortingCoulmn.equalsIgnoreCase("childCategory")){
					sortingSql =" ORDER BY cc.name  "+sortingOrder;					
				}else if(sortingCoulmn.equalsIgnoreCase("parentCategory")){
					sortingSql =" ORDER BY pc.name  "+sortingOrder;
			}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return sortingSql;
	}
	
	// eventMarketId:aaa,eventName:bbb,eventDate:ccc,eventTime:ddd,dayOfTheWeek:eee,venue:fff,venue:ggg,venueId:hhh,noOfTix:iii,noOfTixSold:jjj,
	// city:kkk,state:lll,country:mmm,grandChildCategory:nnn,childCategory:ooo,parentCategory:ppp,eventCreated:qqq,eventLastUpdated:rrr,
	// notes:sss.
	public static GridHeaderFilters getEventSearchHeaderFilters(String headerFilter)throws Exception{
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >=2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("eventMarketId")){
							filter.setEventId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("eventName")){
							filter.setEventName(value);
						}else if(name.equalsIgnoreCase("eventDate")){
							filter.setEventDateStr(value);
						}else if(name.equalsIgnoreCase("eventTime")){
							filter.setEventTimeStr(value);
						}else if(name.equalsIgnoreCase("dayOfTheWeek")){
							filter.setDayOfWeek(value);
						}else if(name.equalsIgnoreCase("venue")){
							filter.setVenueName(value);
						}else if(name.equalsIgnoreCase("venueId")){
							filter.setVenueId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("noOfTix")){
							filter.setNoOfTixCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("noOfTixSold")){
							filter.setNoOfTixSoldCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("city")){
							filter.setCity(value);
						}else if(name.equalsIgnoreCase("state")){
							filter.setState(value);
						}else if(name.equalsIgnoreCase("country")){
							filter.setCountry(value);
						}else if(name.equalsIgnoreCase("grandChildCategory")){
							filter.setGrandChildCategoryName(value);
						}else if(name.equalsIgnoreCase("childCategory")){
							filter.setChildCategoryName(value);
						}else if(name.equalsIgnoreCase("parentCategory")){
							filter.setParentCategoryName(value);
						}else if(name.equalsIgnoreCase("eventCreated")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("eventLastUpdated")){
							filter.setLastUpdatedDateStr(value);
						}else if(name.equalsIgnoreCase("notes")){
							filter.setInternalNotes(value);
						}						
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return filter;
	}
	
	// section:aaa,row:bbb,seatLow:ccc,seatHigh:ddd,quantity:eee,retailPrice:fff,wholeSalePrice:ggg,price:hhh,taxAmount:iii,facePrice:jjj,
	// cost:kkk,ticketType:lll,shippingMethod:mmm,nearTermDisplayOption:nnn,broadcast:ooo,sectionRange:ppp,rowRange:qqq,internalNotes:rrr,
	// externalNotes:sss,marketPlaceNotes:ttt,productType:uuu,eventName:vvv,eventDate:www,eventTime:xxx,venue:yyy.
	public static GridHeaderFilters getTicketSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){					
					String nameValue[] = param.split(":");					
					if(nameValue.length >=2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("invoiceId")){
							filter.setInvoiceId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("section")){
							filter.setSection(value);
						}else if(name.equalsIgnoreCase("row")){
							filter.setRow(value);
						}else if(name.equalsIgnoreCase("seatLow")){
							filter.setSeatLow(value);
						}else if(name.equalsIgnoreCase("seatHigh")){
							filter.setSeatHigh(value);
						}else if(name.equalsIgnoreCase("quantity")){
							filter.setQuantity(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("retailPrice")){
							filter.setRetailPrice(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("wholeSalePrice")){
							filter.setWholeSalePrice(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("price")){
							filter.setPrice(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("taxAmount")){
							filter.setTaxAmount(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("facePrice")){
							filter.setFacePrice(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("cost")){
							filter.setCost(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("ticketType")){
							filter.setTicketType(value);
						}else if(name.equalsIgnoreCase("shippingMethod")){
							filter.setShippingMethod(value);
						}else if(name.equalsIgnoreCase("nearTermDisplayOption")){
							filter.setNearTermDisplayOption(value);
						}else if(name.equalsIgnoreCase("broadcast")){
							if(value.toLowerCase().contains("y")){
								filter.setBroadcast(1);
							}else{
								filter.setBroadcast(0);
							}
						}else if(name.equalsIgnoreCase("sectionRange")){
							filter.setSectionRange(value);
						}else if(name.equalsIgnoreCase("rowRange")){
							filter.setRowRange(value);
						}else if(name.equalsIgnoreCase("internalNotes")){
							filter.setInternalNotes(value);
						}else if(name.equalsIgnoreCase("externalNotes")){
							filter.setExternalNotes(value);
						}else if(name.equalsIgnoreCase("marketPlaceNotes")){
							filter.setMarketPlaceNotes(value);
						}else if(name.equalsIgnoreCase("productType")){
							filter.setProductType(value);
						}else if(name.equalsIgnoreCase("eventName")){
							filter.setEventName(value);
						}else if(name.equalsIgnoreCase("eventDate")){
							filter.setEventDateStr(value);
						}else if(name.equalsIgnoreCase("eventTime")){
							filter.setEventTimeStr(value);
						}else if(name.equalsIgnoreCase("venue")){
							filter.setVenueName(value);
						}else if(name.equalsIgnoreCase("brokerId")){
							filter.setBrokerId(Integer.parseInt(value));
						}					
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	// artist:aaa,grandChildCategory:bbb,childCategory:ccc,parentCategory:ddd,createdDate:eee,createdBy:fff.
	public static GridHeaderFilters getPopularArtistSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length ==2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("artist")){
							filter.setArtistName(value);
						}else if(name.equalsIgnoreCase("grandChildCategory")){
							filter.setGrandChildCategoryName(value);
						}else if(name.equalsIgnoreCase("childCategory")){
							filter.setChildCategoryName(value);
						}else if(name.equalsIgnoreCase("parentCategory")){
							filter.setParentCategoryName(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}						
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	// customerIpAddress:aaa,webConfigId:bbb,actionType:ccc,hittingDate:ddd,actionResult:eee,url:fff,authenticatedHit:ggg.
	public static GridHeaderFilters getWebsiteTrackingSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length ==2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("customerIpAddress")){
							filter.setCustomerIpAddress(value);
						}else if(name.equalsIgnoreCase("webConfigId")){
							filter.setWebConfigId(value);
						}else if(name.equalsIgnoreCase("actionType")){
							filter.setActionType(value);
						}else if(name.equalsIgnoreCase("hittingDate")){
							filter.setHittingDateStr(value);
						}else if(name.equalsIgnoreCase("actionResult")){
							filter.setActionResult(value);
						}else if(name.equalsIgnoreCase("authenticatedHit")){
							filter.setAuthenticatedHit(value);
						}else if(name.equalsIgnoreCase("productType")){
							filter.setProductType(value);
						}						
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	// customerId:aaa,firstName:bbb,lastName:ccc,email:ddd,phone:eee,orderId:fff,orderTotal:ggg,orderType:hhh,eventId:iii,quantity:jjj,
	// zone:kkk,paypalTransactionId:lll,transactionId:mmm,paymentId:nnn,platform:ooo,status:ppp,createdDate:qqq,lastUpdated:rrr,
	// transactionDate:sss.
	public static GridHeaderFilters getPaypalSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length ==2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("customerId")){
							filter.setCustomerId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("firstName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("lastName")){
							filter.setLastName(value);
						}else if(name.equalsIgnoreCase("email")){
							filter.setEmail(value);
						}else if(name.equalsIgnoreCase("phone")){
							filter.setPhone(value);
						}else if(name.equalsIgnoreCase("orderId")){
							filter.setCustomerOrderId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("orderTotal")){
							filter.setOrderTotal(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("orderType")){
							filter.setOrderType(value);
						}else if(name.equalsIgnoreCase("eventId")){
							filter.setEventId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("quantity")){
							filter.setQuantity(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("zone")){
							filter.setZone(value);
						}else if(name.equalsIgnoreCase("paypalTransactionId")){
							filter.setPaypalTransactionId(value);
						}else if(name.equalsIgnoreCase("transactionId")){
							filter.setTransactionId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("paymentId")){
							filter.setPaymentId(value);
						}else if(name.equalsIgnoreCase("platform")){
							filter.setPlatform(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("lastUpdated")){
							filter.setLastUpdatedDateStr(value);
						}else if(name.equalsIgnoreCase("transactionDate")){
							filter.setTransactionDateStr(value);
						}						
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	// userName:aaa,firstName:bbb,lastName:ccc,email:ddd,phone:eee,role:fff.
	public static GridHeaderFilters getUserSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length ==2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("userName")){
							filter.setUsername(value);
						}else if(name.equalsIgnoreCase("firstName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("lastName")){
							filter.setLastName(value);
						}else if(name.equalsIgnoreCase("email")){
							filter.setEmail(value);
						}else if(name.equalsIgnoreCase("phone")){
							filter.setPhone(value);
						}else if(name.equalsIgnoreCase("role")){
							filter.setUserRole(value);
						}else if(name.equalsIgnoreCase("brokerId")){
							filter.setUserId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("companyName")){
							filter.setCompanyName(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("promotionalCode")){
							filter.setReferrerCode(value);
						}						
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	// id:aaa,childCategory:bbb,parentCategory:ccc,lastUpdatedBy:ddd,lastUpdated:eee
	public static GridHeaderFilters getFantasyChildCategorySearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("id")){
							filter.setChildCategoryId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("childCategory")){
							filter.setChildCategoryName(value);
						}else if(name.equalsIgnoreCase("parentCategory")){
							filter.setParentCategoryName(value);
						}else if(name.equalsIgnoreCase("lastUpdatedBy")){
							filter.setLastUpdatedBy(value);
						}else if(name.equalsIgnoreCase("lastUpdated")){
							filter.setLastUpdatedDateStr(value);
						}
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		return filter;
	}
	
	// id:aaa, grandChildCategory:bbb, childCategory:ccc,parentCategory:ddd,lastUpdatedBy:ddd,lastUpdated:eee
	public static GridHeaderFilters getFantasyGrandChildCategorySearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("id")){
							filter.setGrandChildCategoryId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("grandChildCategory")){
							filter.setGrandChildCategoryName(value);
						}else if(name.equalsIgnoreCase("childCategory")){
							filter.setChildCategoryName(value);
						}else if(name.equalsIgnoreCase("parentCategory")){
							filter.setParentCategoryName(value);
						}else if(name.equalsIgnoreCase("packageCost")){
							filter.setPackageCost(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("lastUpdatedBy")){
							filter.setLastUpdatedBy(value);
						}else if(name.equalsIgnoreCase("lastUpdated")){
							filter.setLastUpdatedDateStr(value);
						}
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	// orderNo:aaa, orderDate:bbb, orderType:ccc, rewardPoints:ddd, eventName:ddd, eventDate:eee, eventTime:fff.
	public static GridHeaderFilters getRewardPointsSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("orderNo")){
							filter.setCustomerOrderId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("orderDate")){
							filter.setOrderDateStr(value);
						}else if (name.equalsIgnoreCase("orderType")){
							filter.setOrderType(value);
						}else if(name.equalsIgnoreCase("rewardPoints")){
							filter.setRewardPoints(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("eventName")){
							filter.setEventName(value);
						}else if(name.equalsIgnoreCase("eventDate")){
							filter.setEventDateStr(value);
						}else if(name.equalsIgnoreCase("eventTime")){
							filter.setEventTimeStr(value);
						}
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	public static GridHeaderFilters getCrownJewelOrderSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >=2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("id")){
							filter.setId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("regularOrderId")){
							filter.setCustomerOrderId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("leagueName")){
							filter.setLeagueName(value);
						}else if(name.equalsIgnoreCase("teamName")){
							filter.setTeamName(value);
						}else if(name.equalsIgnoreCase("venue")){
							filter.setVenueName(value);
						}else if(name.equalsIgnoreCase("city")){
							filter.setCity(value);
						}else if(name.equalsIgnoreCase("state")){
							filter.setState(value);
						}else if(name.equalsIgnoreCase("country")){
							filter.setCountry(value);
						}else if(name.equalsIgnoreCase("isPackageSelected")){
							if(value.toLowerCase().contains("y")){
								filter.setIsPackageSelected(1);
							}else{
								filter.setIsPackageSelected(0);
							}
						}else if(name.equalsIgnoreCase("packageCost")){
							filter.setCost(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("packageNote")){
							filter.setInternalNotes(value);
						}else if(name.equalsIgnoreCase("eventId")){
							filter.setEventId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("ticketId")){
							filter.setTicketId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("zone")){
							filter.setZone(value);
						}else if(name.equalsIgnoreCase("ticketQty")){
							filter.setTicketQty(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("ticketPrice")){
							filter.setPrice(Double.parseDouble(value));
						}/*else if(name.equalsIgnoreCase("ticketPoints")){
							filter.setTicketPoints(Double.parseDouble(value));
						}*/else if(name.equalsIgnoreCase("requirePoints")){
							filter.setRequirePoints(Double.parseDouble(value));
						}/*else if(name.equalsIgnoreCase("packagePoints")){
							filter.setPackagePoints(Double.parseDouble(value));
						}*/else if(name.equalsIgnoreCase("customerName")){
							filter.setCustomerName(value);
						}/*else if(name.equalsIgnoreCase("remainingPoints")){
							filter.setRemainingPoints(Double.parseDouble(value));
						}*/else if(name.equalsIgnoreCase("grandChildCategoryName")){
							filter.setGrandChildCategoryName(value);
						}else if(name.equalsIgnoreCase("platform")){
							filter.setPlatform(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("lastUpdatedDate")){
							filter.setLastUpdatedDateStr(value);
						}						
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getHoldTicketsSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("customerId")){
							filter.setCustomerId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("customerName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("salePrice")){
							filter.setSalePrice(Double.parseDouble(value));
						}else if (name.equalsIgnoreCase("createdBy")){
							filter.setCsr(value);
						}else if(name.equalsIgnoreCase("expirationDate")){
							filter.setExpirationDateStr(value);
						}else if(name.equalsIgnoreCase("expirationMinutes")){
							filter.setExpirationMinutes(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("shippingMethodId")){
							filter.setShippingMethodId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("shippingMethod")){
							filter.setShippingMethod(value);
						}else if(name.equalsIgnoreCase("holdDate")){
							filter.setHoldDateStr(value);
						}else if(name.equalsIgnoreCase("internalNote")){
							filter.setInternalNotes(value);
						}else if(name.equalsIgnoreCase("externalNote")){
							filter.setExternalNotes(value);
						}else if(name.equalsIgnoreCase("ticketIds")){
							filter.setTicketIdsStr(value);
						}else if(name.equalsIgnoreCase("externalPo")){
							filter.setExternalNo(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("ticketGroupId")){
							filter.setTicketGroupId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("brokerId")){
							filter.setBrokerId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("section")){
							filter.setSection(value);
						}else if(name.equalsIgnoreCase("row")){
							filter.setRow(value);
						}else if(name.equalsIgnoreCase("seatLow")){
							filter.setSeatLow(value);
						}else if(name.equalsIgnoreCase("seatHigh")){
							filter.setSeatHigh(value);
						}
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	public static GridHeaderFilters getLockedTicketsSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("section")){
							filter.setSection(value);
						}else if(name.equalsIgnoreCase("row")){
							filter.setRow(value);
						}else if(name.equalsIgnoreCase("seatLow")){
							filter.setSeatLow(value);
						}else if(name.equalsIgnoreCase("seatHigh")){
							filter.setSeatHigh(value);
						}else if(name.equalsIgnoreCase("quantity")){
							filter.setQuantity(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("retailPrice")){
							filter.setRetailPrice(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("wholeSalePrice")){
							filter.setWholeSalePrice(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("price")){
							filter.setPrice(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("taxAmount")){
							filter.setTaxAmount(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("facePrice")){
							filter.setFacePrice(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("cost")){
							filter.setCost(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("ticketType")){
							filter.setTicketType(value);
						}else if(name.equalsIgnoreCase("shippingMethod")){
							filter.setShippingMethod(value);
						}else if(name.equalsIgnoreCase("nearTermDisplayOption")){
							filter.setNearTermDisplayOption(value);
						}else if(name.equalsIgnoreCase("broadcast")){
							if(value.toLowerCase().contains("y")){
								filter.setBroadcast(1);
							}else{
								filter.setBroadcast(0);
							}
						}else if(name.equalsIgnoreCase("sectionRange")){
							filter.setSectionRange(value);
						}else if(name.equalsIgnoreCase("rowRange")){
							filter.setRowRange(value);
						}else if(name.equalsIgnoreCase("internalNotes")){
							filter.setInternalNotes(value);
						}else if(name.equalsIgnoreCase("externalNotes")){
							filter.setExternalNotes(value);
						}else if(name.equalsIgnoreCase("marketPlaceNotes")){
							filter.setMarketPlaceNotes(value);
						}else if(name.equalsIgnoreCase("productType")){
							filter.setProductType(value);
						}else if(name.equalsIgnoreCase("eventName")){
							filter.setEventName(value);
						}else if(name.equalsIgnoreCase("eventDate")){
							filter.setEventDateStr(value);
						}else if(name.equalsIgnoreCase("eventTime")){
							filter.setEventTimeStr(value);
						}else if(name.equalsIgnoreCase("venue")){
							filter.setVenueName(value);
						}else if(name.equalsIgnoreCase("categoryTicketGroupId")){
							filter.setCategoryTicketGroupId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("ipAddress")){
							filter.setCustomerIpAddress(value);
						}else if (name.equalsIgnoreCase("platform")){
							filter.setPlatform(value);
						}else if(name.equalsIgnoreCase("creationDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("lockStatus")){
							filter.setStatus(value);
						}
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getAffiliatesSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("userId")){
							filter.setUserId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("userName")){
							filter.setUsername(value);
						}else if(name.equalsIgnoreCase("firstName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("lastName")){
							filter.setLastName(value);
						}else if(name.equalsIgnoreCase("email")){
							filter.setEmail(value);
						}else if(name.equalsIgnoreCase("phone")){
							filter.setPhone(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("promotionalCode")){
							filter.setReferrerCode(value);
						}else if(name.equalsIgnoreCase("activeCash")){
							filter.setActiveCash(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("pendingCash")){
							filter.setPendingCash(Double.valueOf(value));
						}/*else if(name.equalsIgnoreCase("lastCreditedCash")){
							filter.setLastCreditedCash(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("lastDebitedCash")){
							filter.setLastDebitedCash(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("totalCreditedCash")){
							filter.setTotalCreditedCash(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("totalDebitedCash")){
							filter.setTotalDebitedCash(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("lastUpdate")){
							filter.setLastUpdatedDateStr(value);
						}*/					
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getAffiliateReportSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("orderId")){
							filter.setCustomerOrderId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("firstName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("lastName")){
							filter.setLastName(value);
						}else if(name.equalsIgnoreCase("eventName")){
							filter.setEventName(value);
						}else if(name.equalsIgnoreCase("eventDate")){
							filter.setEventDateStr(value);
						}else if(name.equalsIgnoreCase("eventTime")){
							filter.setEventTimeStr(value);
						}else if(name.equalsIgnoreCase("ticketQty")){
							filter.setQuantity(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("orderTotal")){
							filter.setOrderTotal(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("cashCredit")){
							filter.setCashCredited(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("promoCode")){
							filter.setReferrerCode(value);
						}/*else if(name.equalsIgnoreCase("paymentNote")){
							filter.setInternalNotes(value);
						}*/					
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getRTFPromotionalOfferFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("promocode")){
							filter.setPromoCode(value);
						}else if(name.equalsIgnoreCase("discountPerc")){
							filter.setDiscountCouponPrice(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("mobileDiscountPerc")){
							filter.setMobileDiscount(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("startDate")){
							filter.setStartDate(value);
						}else if(name.equalsIgnoreCase("endDate")){
							filter.setEndDate(value);
						}else if(name.equalsIgnoreCase("maxOrder")){
							filter.setMaxOrders(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("orders")){
							filter.setNoOfOrders(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("promoType")){
							filter.setPromoType(value);
						}else if(name.equalsIgnoreCase("artist")){
							filter.setArtistName(value);
						}else if(name.equalsIgnoreCase("event")){
							filter.setEventName(value);
						}else if(name.equalsIgnoreCase("venue")){
							filter.setVenueName(value);
						}else if(name.equalsIgnoreCase("parent")){
							filter.setParentCategoryName(value);
						}else if(name.equalsIgnoreCase("child")){
							filter.setChildCategoryName(value);
						}else if(name.equalsIgnoreCase("grandChild")){
							filter.setGrandChildCategoryName(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("modifiedBy")){
							filter.setLastUpdatedBy(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	

	public static GridHeaderFilters getRTFCustomerPromotionalOfferFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("firstName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("lastName")){
							filter.setLastName(value);
						}else if(name.equalsIgnoreCase("email")){
							filter.setEmail(value);
						}else if(name.equalsIgnoreCase("promoCode")){
							filter.setPromoCode(value);
						}else if(name.equalsIgnoreCase("discount")){
							filter.setDiscountCouponPrice(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("startDate")){
							filter.setStartDate(value);
						}else if(name.equalsIgnoreCase("endDate")){
							filter.setEndDate(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("modifiedDate")){
							filter.setLastUpdatedDateStr(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	public static GridHeaderFilters getRTFPromotionalOfferTrackingFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("customerId")){
							filter.setCustomerId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("firstName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("lastName")){
							filter.setLastName(value);
						}else if(name.equalsIgnoreCase("email")){
							filter.setEmail(value);
						}else if(name.equalsIgnoreCase("promoCode")){
							filter.setPromoCode(value);
						}else if(name.equalsIgnoreCase("discount")){
							filter.setDiscountCouponPrice(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("orderId")){
							filter.setCustomerOrderId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("orderTotal")){
							filter.setOrderTotal(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("quantity")){
							filter.setQuantity(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("ipAddress")){
							filter.setCustomerIpAddress(value);
						}else if(name.equalsIgnoreCase("primaryPaymentMethod")){
							filter.setPrimaryPaymentMethod(value);
						}else if(name.equalsIgnoreCase("secondaryPaymentMethod")){
							filter.setSecondaryPaymentMethod(value);
						}else if(name.equalsIgnoreCase("thirdPaymentMethod")){
							filter.setThirdPaymentMethod(value);
						}else if(name.equalsIgnoreCase("platform")){
							filter.setPlatform(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getContestsFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("contestName")){
							filter.setContestName(value);
						}else if(name.equalsIgnoreCase("startDate")){
							filter.setStartDate(value);
						}else if(name.equalsIgnoreCase("partipantCount")){
							filter.setPartipantCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("winnerCount")){
							filter.setWinnerCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("ticketWinnerCount")){
							filter.setTicketWinnerCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("pointWinnerCount")){
							filter.setPointWinnerCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("ticketWinnerThreshhold")){
							filter.setTicketWinnerThreshhold(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("freeTicketPerWinner")){
							filter.setFreeTicketPerWinner(Integer.parseInt(value));
						}/*else if(name.equalsIgnoreCase("pointsPerWinner")){
							filter.setPointsPerWinner(Double.parseDouble(value));
						}*/
						else if(name.equalsIgnoreCase("rewardPoints")){
							filter.setRewardPoints(Double.parseDouble(value));
						}
						else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setUpdatedDate(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setUpdatedBy(value);
						}else if(name.equalsIgnoreCase("contestMode")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("questionSize")){
							filter.setCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("zone")){
							filter.setZone(value);
						}else if(name.equalsIgnoreCase("artistEventCategoryName")){
							filter.setPromoReferenceName(value);
						}else if(name.equalsIgnoreCase("artistEventCategoryType")){
							filter.setPromoType(value);
						}else if(name.equalsIgnoreCase("promotionalCode")){
							filter.setPromoCode(value);
						}else if(name.equalsIgnoreCase("discountPercentage")){
							filter.setMobileDiscount(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("singleTixPrice")){
							filter.setPrice(Double.parseDouble(value));
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	public static GridHeaderFilters getContestsWinnerFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("custName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("lastName")){
							filter.setLastName(value);
						}else if(name.equalsIgnoreCase("userId")){
							filter.setUsername(value);
						}else if(name.equalsIgnoreCase("email")){
							filter.setEmail(value);
						}else if(name.equalsIgnoreCase("rewardPoints")){
							filter.setRewardPoints(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("rewardRank")){
							filter.setUserId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("rewardTickets")){
							filter.setTicketQty(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("orderId")){
							filter.setOrderId(Integer.parseInt(value));
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	public static GridHeaderFilters getContestQuestionFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("question")){
							filter.setText7(value);
						}else if(name.equalsIgnoreCase("optionA")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("optionB")){
							filter.setText2(value);
						}else if(name.equalsIgnoreCase("optionC")){
							filter.setText3(value);
						}else if(name.equalsIgnoreCase("optionD")){
							filter.setText4(value);
						}else if(name.equalsIgnoreCase("answer")){
							filter.setText5(value);
						}else if(name.equalsIgnoreCase("difficultyLevel")){
							filter.setText6(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setUpdatedDate(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setLastUpdatedBy(value);
						}else if(name.equalsIgnoreCase("questionReward")){
							filter.setCost((Double.parseDouble(value)/100));
						}else if(name.equalsIgnoreCase("serialNo")){
							filter.setNoOfOrders(Integer.parseInt(value));
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getDiscountCodeTrackingFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						
						if(name.equalsIgnoreCase("customerId")){
							filter.setCustomerId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("firstName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("lastName")){
							filter.setLastName(value);
						}else if(name.equalsIgnoreCase("email")){
							filter.setEmail(value);
						}else if(name.equalsIgnoreCase("referrerCode")){
							filter.setReferrerCode(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getManualFedexGenerationFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("blFirstName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("blLastName")){
							filter.setLastName(value);
						}else if(name.equalsIgnoreCase("blCompanyName")){
							filter.setCompanyName(value);
						}else if(name.equalsIgnoreCase("blAddress1")){
							filter.setAddressLine1(value);
						}else if(name.equalsIgnoreCase("blAddress2")){
							filter.setAddressLine2(value);
						}else if(name.equalsIgnoreCase("blCity")){
							filter.setCity(value);
						}else if(name.equalsIgnoreCase("blStateName")){
							filter.setState(value);
						}else if(name.equalsIgnoreCase("blCountryName")){
							filter.setCountry(value);
						}else if(name.equalsIgnoreCase("blZipCode")){
							filter.setZipCode(value);
						}else if(name.equalsIgnoreCase("blPhone")){
							filter.setPhone(value);
						}else if(name.equalsIgnoreCase("shCustomerName")){
							filter.setShCustomerName(value);
						}else if(name.equalsIgnoreCase("shCompanyName")){
							filter.setShCompanyName(value);
						}else if(name.equalsIgnoreCase("shAddress1")){
							filter.setShAddressLine1(value);
						}else if(name.equalsIgnoreCase("shAddress2")){
							filter.setShAddressLine2(value);
						}else if(name.equalsIgnoreCase("shCity")){
							filter.setShCity(value);
						}else if(name.equalsIgnoreCase("shStateName")){
							filter.setShState(value);
						}else if(name.equalsIgnoreCase("shCountryName")){
							filter.setShCountry(value);
						}else if(name.equalsIgnoreCase("shZipCode")){
							filter.setShZipCode(value);
						}else if(name.equalsIgnoreCase("shPhone")){
							filter.setShPhone(value);
						}else if(name.equalsIgnoreCase("serviceType")){
							filter.setServiceType(value);
						}else if(name.equalsIgnoreCase("trackingNo")){
							filter.setTrackingNo(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getLotteriesFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("contestName")){
							filter.setContestName(value);
						}else if(name.equalsIgnoreCase("minPurchaseAmount")){
							filter.setMinimumPurchaseAmount(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("startDate")){
							filter.setStartDate(value);
						}else if(name.equalsIgnoreCase("endDate")){
							filter.setEndDate(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setLastUpdatedBy(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("artistName")){
							filter.setArtistName(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getParticipantsFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("participantContId")){
							filter.setContestId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("customerId")){
							filter.setCustomerId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("customerName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("customerEmail")){
							filter.setEmail(value);
						}else if(name.equalsIgnoreCase("purchaseCustomerId")){
							filter.setPurchaseCustomerId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("purchaseCustomerName")){
							filter.setPurchaseCustomerName(value);
						}else if(name.equalsIgnoreCase("purchaseCustomerEmail")){
							filter.setPurchaseCustomerEmail(value);
						}else if(name.equalsIgnoreCase("referralCode")){
							filter.setReferrerCode(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getUserAuditFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("userName")){
							filter.setUsername(value);
						}else if(name.equalsIgnoreCase("userAction")){
							filter.setUserAction(value);
						}else if(name.equalsIgnoreCase("clientIPAddress")){
							filter.setClientIPAddress(value);
						}else if(name.equalsIgnoreCase("message")){
							filter.setMessage(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setLastUpdatedDateStr(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getContestAllEventFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("contestId")){
							filter.setContestId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("eventId")){
							filter.setEventId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("eventName")){
							filter.setEventName(value);
						}else if(name.equalsIgnoreCase("eventDate")){
							filter.setEventDateStr(value);
						}else if(name.equalsIgnoreCase("eventTime")){
							filter.setEventTimeStr(value);
						}else if(name.equalsIgnoreCase("venueId")){
							filter.setVenueId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("venue")){
							filter.setVenueName(value);
						}				
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getContestEventFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("contestId")){
							filter.setContestId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("eventId")){
							filter.setEventId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("eventName")){
							filter.setEventName(value);
						}else if(name.equalsIgnoreCase("eventDate")){
							filter.setEventDateStr(value);
						}else if(name.equalsIgnoreCase("eventTime")){
							filter.setEventTimeStr(value);
						}else if(name.equalsIgnoreCase("venueId")){
							filter.setVenueId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("venue")){
							filter.setVenueName(value);
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setUpdatedBy(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setUpdatedDate(value);
						}						
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getContestEventRequestFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("eventName")){
							filter.setEventName(value);
						}else if(name.equalsIgnoreCase("venue")){
							filter.setVenueName(value);
						}else if(name.equalsIgnoreCase("eventDescription")){
							filter.setEventDescription(value);
						}else if(name.equalsIgnoreCase("eventDate")){
							filter.setEventDateStr(value);
						}else if(name.equalsIgnoreCase("socialMediaLink")){
							filter.setSocialMediaLink(value);
						}else if(name.equalsIgnoreCase("contactName")){
							filter.setContactName(value);
						}else if(name.equalsIgnoreCase("contactEmail")){
							filter.setContactEmail(value);
						}else if(name.equalsIgnoreCase("contactPhone")){
							filter.setContactPhone(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("noOfTickets")){
							filter.setNoOfTixCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getQuestionBankFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("question")){
							filter.setText7(value);
						}else if(name.equalsIgnoreCase("optionA")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("optionB")){
							filter.setText2(value);
						}else if(name.equalsIgnoreCase("optionC")){
							filter.setText3(value);
						}else if(name.equalsIgnoreCase("factChecked")){
							filter.setText4(value);
						}else if(name.equalsIgnoreCase("answer")){
							filter.setText5(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setUpdatedDate(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setLastUpdatedBy(value);
						}else if(name.equalsIgnoreCase("questionReward")){
							filter.setCost((Double.parseDouble(value)/100));
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("category")){
							filter.setCategory(value);
						}else if(name.equalsIgnoreCase("difficultyLevel")){
							filter.setText6(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	
	public static GridHeaderFilters getContestPromocodeFiter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("promoName")){
							filter.setPromoCode(value);
						}else if(name.equalsIgnoreCase("customerName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("noOfFreeTickets")){
							filter.setTicketQty(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("promoType")){
							filter.setPromoType(value);
						}else if(name.equalsIgnoreCase("promoTypeName")){
							filter.setPromoReferenceName(value);
						}else if(name.equalsIgnoreCase("expiryDate")){
							filter.setEndDate(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setUpdatedDate(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("modifiedBy")){
							filter.setLastUpdatedBy(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getContestPromoOfferFiter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("promoCode")){
							filter.setPromoCode(value);
						}else if(name.equalsIgnoreCase("startDate")){
							filter.setStartDate(value);
						}else if(name.equalsIgnoreCase("endDate")){
							filter.setEndDate(value);
						}else if(name.equalsIgnoreCase("maxLifePerCustomer")){
							filter.setTicketWinnerCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("maxAccumulateThreshold")){
							filter.setTicketWinnerThreshhold(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("curAccumulatedCount")){
							filter.setPriceUpdateCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setUpdatedDate(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setLastUpdatedBy(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	public static GridHeaderFilters getCustomerQuestionFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("question")){
							filter.setText7(value);
						}else if(name.equalsIgnoreCase("optionA")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("optionB")){
							filter.setText2(value);
						}else if(name.equalsIgnoreCase("optionC")){
							filter.setText3(value);
						}else if(name.equalsIgnoreCase("answer")){
							filter.setText5(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	private static boolean validateParamValues(String name,String value){
		if(name==null || name.isEmpty() || name.equalsIgnoreCase("undefined") ||
				value==null || value.isEmpty() || value.equalsIgnoreCase("undefined")){
			return false;
		}
		return true;	
	}
}
