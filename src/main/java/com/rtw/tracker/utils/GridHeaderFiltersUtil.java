package com.rtw.tracker.utils;

public class GridHeaderFiltersUtil {

	// artist:aaa,grandChildCategory:bbb,childCategory:ccc,parentCategory:ddd,visibleInSearch:eee,
	public static GridHeaderFilters getManageArtistSearchHeaderFilters(String headerFilter)throws Exception{
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length ==2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("artist")){
							filter.setArtistName(value);
						}else if(name.equalsIgnoreCase("grandChildCategory")){
							filter.setGrandChildCategoryName(value);
						}else if(name.equalsIgnoreCase("childCategory")){
							filter.setChildCategoryName(value);
						}else if(name.equalsIgnoreCase("parentCategory")){
							filter.setParentCategoryName(value);
						}else if(name.equalsIgnoreCase("visibleInSearch")){
							if(value.toLowerCase().contains("y")){
								filter.setVisibleOnSearch(1);
							}else{
								filter.setVisibleOnSearch(0);
							}
						}else if(name.equalsIgnoreCase("eventCount")){
							filter.setEventCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}
						
					}
					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return filter;
	}
	
	// customerType:aaa,customerName:bbb,lastName:ccc,productType:ddd,signupType:eee,client:fff,broker:ggg,customerStatus:hhh,companyName:iii,email:jjj,
	// street1:kkk,city:lll,state:mmm,country:nnn,zip:ooo,phone:ppp,referrerCode:qqq.
	public static GridHeaderFilters getCustomerSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length ==2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("customerId")){
							filter.setCustomerId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("customerType")){
							filter.setCustomerType(value);
						}else if(name.equalsIgnoreCase("firstName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("customerName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("lastName")){
							filter.setLastName(value);
						}else if(name.equalsIgnoreCase("userId")){
							filter.setUsername(value);
						}else if(name.equalsIgnoreCase("productType")){
							filter.setProductType(value);
						}else if(name.equalsIgnoreCase("signupType")){
							filter.setSignupType(value);
						}else if(name.equalsIgnoreCase("client")){
							if(value.toLowerCase().contains("y")){
								filter.setIsClient(1);
							}else{
								filter.setIsClient(0);
							}
						}else if(name.equalsIgnoreCase("broker")){
							if(value.toLowerCase().contains("y")){
								filter.setIsBroker(1);
							}else{
								filter.setIsBroker(0);
							}
						}else if(name.equalsIgnoreCase("customerStatus")){
							filter.setCustomerStatus(value);
						}else if(name.equalsIgnoreCase("companyName")){
							filter.setCompanyName(value);
						}else if(name.equalsIgnoreCase("email")){
							filter.setEmail(value);
						}else if(name.equalsIgnoreCase("street1")){
							filter.setAddressLine1(value);
						}else if(name.equalsIgnoreCase("street2")){
							filter.setAddressLine2(value);
						}else if(name.equalsIgnoreCase("city")){
							filter.setCity(value);
						}else if(name.equalsIgnoreCase("state")){
							filter.setState(value);
						}else if(name.equalsIgnoreCase("country")){
							filter.setCountry(value);
						}else if(name.equalsIgnoreCase("zip")){
							filter.setZipCode(value);
						}else if(name.equalsIgnoreCase("phone")){
							filter.setPhone(value);
						}else if(name.equalsIgnoreCase("referrerCode")){
							filter.setReferrerCode(value);
						}else if(name.equalsIgnoreCase("rewardPoints")){
							filter.setRewardPoints(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("emailSent")){
							filter.setEmailSent(value);
						}else if(name.equalsIgnoreCase("retailCust")){
							filter.setRetailCustomer(value);
						}else if(name.equalsIgnoreCase("isblocked")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}						
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	// purchaseOrderId:aaa,customerName:bbb,customerType:ccc,poTotal:ddd,ticketQty:eee,created:fff,createdBy:ggg,shippingType:hhh,status:iii,productType:jjj,
	// csr:kkk,poAged:lll,lastUpdated:mmm,isEmailed:nnn,consignmentPoNo:ooo,transactionOffice:ppp,trackingNo:qqq,shippingNotes:rrr,externalNotes:sss,internalNotes:ttt,
	// eventId:uuu,eventName:vvv,eventDate:www,eventTime:xxx.
	public static GridHeaderFilters getPOSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >=2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("purchaseOrderId")){
							filter.setPurchaseOrderId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("id")){
							filter.setPurchaseOrderId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("customerName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("customerType")){
							filter.setCustomerType(value);
						}else if(name.equalsIgnoreCase("poTotal")){
							filter.setPoTotal(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("ticketQty")){
							filter.setTicketQty(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("created")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("shippingType")){
							filter.setShippingType(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("productType")){
							filter.setProductType(value);
						}else if(name.equalsIgnoreCase("csr")){
							filter.setCsr(value);
						}else if(name.equalsIgnoreCase("poAged")){
							filter.setPoAged(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("lastUpdated")){
							filter.setLastUpdatedDateStr(value);
						}else if(name.equalsIgnoreCase("isEmailed")){
							if(value.toLowerCase().contains("y")){
								filter.setIsEmailed(1);
							}else{
								filter.setIsEmailed(0);
							}
						}else if(name.equalsIgnoreCase("consignmentPoNo")){
							filter.setConsignmentPoNo(value);
						}else if(name.equalsIgnoreCase("transactionOffice")){
							filter.setTransactionOffice(value);
						}else if(name.equalsIgnoreCase("trackingNo")){
							filter.setTrackingNo(value);
						}else if(name.equalsIgnoreCase("shippingNotes")){
							filter.setShippingNotes(value);
						}else if(name.equalsIgnoreCase("externalNotes")){
							filter.setExternalNotes(value);
						}else if(name.equalsIgnoreCase("internalNotes")){
							filter.setInternalNotes(value);
						}else if(name.equalsIgnoreCase("eventId")){
							filter.setEventId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("eventName")){
							filter.setEventName(value);
						}else if(name.equalsIgnoreCase("eventDate")){
							filter.setEventDateStr(value);
						}else if(name.equalsIgnoreCase("eventTime")){
							filter.setEventTimeStr(value);
						}else if(name.equalsIgnoreCase("purchaseOrderType")){
							filter.setPurchaseOrderType(value);
						}else if(name.equalsIgnoreCase("eventTicketQty")){
							filter.setEventTixQty(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("eventTicketCost")){
							filter.setEventTicketCost(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("eventTicketQty")){
							filter.setEventTixQty(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("RTWPO")){
							filter.setPosPOId(value);
						}else if(name.equalsIgnoreCase("brokerId")){
							filter.setBrokerId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	// invId:aaa,customerId:bbb,customerName:ccc,username:ddd,ticketCount:eee,invoiceTotal:fff,invoiceAged:ggg,createdDate:hhh,createdBy:iii,csr:jjj,
	// orderId:kkk,shippingMethod:lll,customerType:mmm,companyName:nnn,productType:ooo,street1:ppp,city:qqq,state:rrr,country:sss,zipCode:ttt,
	// phone:uuu,secondaryOrderId:vvv,secondaryOrderType:www,voidedDate:xxx,lastUpdatedDate:yyy,lastUpdatedBy:zzz,
	// status:aaaa, trackingNo:bbbb, viewFedexLabel:cccc, eventId:dddd, eventName:eeee, eventDate:ffff, eventTime:gggg, extNp:hhhh.
	public static GridHeaderFilters getInvoiceSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >=2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("invId")){
							filter.setInvoiceId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("invoiceId")){
							filter.setInvoiceId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("customerId")){
							filter.setCustomerId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("customerName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("username")){
							filter.setUsername(value);
						}else if(name.equalsIgnoreCase("ticketCount")){
							filter.setTicketQty(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("invoiceTotal")){
							filter.setInvoiceTotal(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("invoiceAged")){
							filter.setInvoiceAged(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("csr")){
							filter.setCsr(value);
						}else if(name.equalsIgnoreCase("orderId")){
							filter.setCustomerOrderId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("shippingMethod")){
							filter.setShippingMethod(value);
						}else if(name.equalsIgnoreCase("customerType")){
							filter.setCustomerType(value);
						}else if(name.equalsIgnoreCase("companyName")){
							filter.setCompanyName(value);
						}else if(name.equalsIgnoreCase("productType")){
							filter.setProductType(value);
						}else if(name.equalsIgnoreCase("street1")){
							filter.setAddressLine1(value);
						}else if(name.equalsIgnoreCase("city")){
							filter.setCity(value);
						}else if(name.equalsIgnoreCase("state")){
							filter.setState(value);
						}else if(name.equalsIgnoreCase("country")){
							filter.setCountry(value);
						}else if(name.equalsIgnoreCase("zipCode")){
							filter.setZipCode(value);
						}else if(name.equalsIgnoreCase("phone")){
							filter.setPhone(value);
						}else if(name.equalsIgnoreCase("secondaryOrderId")){
							filter.setSecondaryOrderId(value);
						}else if(name.equalsIgnoreCase("secondaryOrderType")){
							filter.setSecondaryOrderType(value);
						}else if(name.equalsIgnoreCase("voidedDate")){
							filter.setVoidedDateStr(value);
						}else if(name.equalsIgnoreCase("lastUpdatedDate")){
							filter.setLastUpdatedDateStr(value);
						}else if(name.equalsIgnoreCase("lastUpdatedBy")){
							filter.setLastUpdatedBy(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("trackingNo")){
							filter.setTrackingNo(value);
						}else if(name.equalsIgnoreCase("viewFedexLabel")){
							filter.setViewFedexLabel(value);
						}else if(name.equalsIgnoreCase("eventId")){
							filter.setEventId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("eventName")){
							filter.setEventName(value);
						}else if(name.equalsIgnoreCase("eventDate")){
							filter.setEventDateStr(value);
						}else if(name.equalsIgnoreCase("eventTime")){
							filter.setEventTimeStr(value);
						}else if(name.equalsIgnoreCase("extNo")){
							filter.setExternalNo(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("orderType")){
							filter.setOrderType(value);
						}else if(name.equalsIgnoreCase("brokerId")){
							filter.setBrokerId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("loyalFan")){
							filter.setOrderType(value);
						}else if(name.equalsIgnoreCase("platform")){
							filter.setPlatform(value);
						}else if(name.equalsIgnoreCase("primaryPaymentMethod")){
							filter.setPrimaryPaymentMethod(value);
						}else if(name.equalsIgnoreCase("secondaryPaymentMethod")){
							filter.setSecondaryPaymentMethod(value);
						}else if(name.equalsIgnoreCase("thirdPaymentMethod")){
							filter.setThirdPaymentMethod(value);
						}else if(name.equalsIgnoreCase("isPOMapped")){
							filter.setIsPoMapped(value);
						}else if(name.equalsIgnoreCase("pendingAmount")){
							filter.setPackageCost(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}						
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	// orderId:aaa,orderDateTime:bbb,eventName:ccc,eventDate:ddd,eventTime:eee,venue:fff,section:ggg,row:hhh,quantity:iii,totalSale:jjj,
	// totalPayment:kkk,status:lll,lastUpdatedDate:mmm,ticTrackerOrderId:nnn,ticTrackerInvoiceId:ooo.
	public static GridHeaderFilters getSeatGeekSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >=2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("orderId")){
							filter.setSeatgeekOrderId(value);
						}else if(name.equalsIgnoreCase("orderDateTime")){
							filter.setOrderDateStr(value);
						}else if(name.equalsIgnoreCase("eventName")){
							filter.setEventName(value);
						}else if(name.equalsIgnoreCase("eventDate")){
							filter.setEventDateStr(value);
						}else if(name.equalsIgnoreCase("eventTime")){
							filter.setEventTimeStr(value);
						}else if(name.equalsIgnoreCase("venue")){
							filter.setVenueName(value);
						}else if(name.equalsIgnoreCase("section")){
							filter.setSection(value);
						}else if(name.equalsIgnoreCase("row")){
							filter.setRow(value);
						}else if(name.equalsIgnoreCase("quantity")){
							filter.setQuantity(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("totalSale")){
							filter.setTotalSaleAmt(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("totalPayment")){
							filter.setTotalPaymentAmt(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("lastUpdatedDate")){
							filter.setLastUpdatedDateStr(value);
						}else if(name.equalsIgnoreCase("ticTrackerOrderId")){
							filter.setCustomerOrderId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("ticTrackerInvoiceId")){
							filter.setInvoiceId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}						
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}

	// invoiceId:aaa,orderId:bbb,invoiceDateTime:ccc,eventName:ddd,eventDate:eee,eventTime:fff,venue:ggg,productType:hhh,internalNotes:iii,quantity:jjj,
	// section:kkk,row:lll,soldPrice:mmm,marketPrice:nnn,lastUpdatedPrice:ooo,availableSectionTixCount:ppp,availableEventTixCount:qqq,totalSoldPrice:rrr,
	// totalMarketPrice:sss,PLValue:ttt,priceUpdatedCount:uuu,price:vvv,discountCoupenPrice:www,url:xxx,shippingMethod:yyy,trackingNo:zzz,
	// secondaryOrderType:aaaa, secondaryOrderId:bbbb, lastUpdated:cccc, eventId:dddd.
	public static GridHeaderFilters getOpenOrShipmentPendingOrClosedOrderSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >=2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("invoiceId")){
							filter.setInvoiceId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("orderId")){
							filter.setCustomerOrderId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("invoiceDateTime")){
							filter.setInvoiceDateStr(value);
						}else if(name.equalsIgnoreCase("eventName")){
							filter.setEventName(value);
						}else if(name.equalsIgnoreCase("eventDate")){
							filter.setEventDateStr(value);
						}else if(name.equalsIgnoreCase("eventTime")){
							filter.setEventTimeStr(value);
						}else if(name.equalsIgnoreCase("venue")){
							filter.setVenueName(value);
						}else if(name.equalsIgnoreCase("venueCity")){
							filter.setCity(value);
						}else if(name.equalsIgnoreCase("venueState")){
							filter.setState(value);
						}else if(name.equalsIgnoreCase("orderType")){
							filter.setOrderType(value);
						}else if(name.equalsIgnoreCase("venueCountry")){
							filter.setCountry(value);
						}else if(name.equalsIgnoreCase("productType")){
							filter.setProductType(value);
						}else if(name.equalsIgnoreCase("internalNotes")){
							filter.setInternalNotes(value);
						}else if(name.equalsIgnoreCase("quantity")){
							filter.setQuantity(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("section")){
							filter.setSection(value);
						}else if(name.equalsIgnoreCase("row")){
							filter.setRow(value);
						}else if(name.equalsIgnoreCase("soldPrice")){
							filter.setActualSoldPrice(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("marketPrice")){
							filter.setMarketPrice(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("lastUpdatedPrice")){
							filter.setLastUpdatedPrice(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("availableSectionTixCount")){
							filter.setSectionTixQty(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("availableEventTixCount")){
							filter.setEventTixQty(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("totalSoldPrice")){
							filter.setTotalActualSoldPrice(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("totalMarketPrice")){
							filter.setTotalMarketPrice(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("PLValue")){
							filter.setProfitAndLoss(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("priceUpdatedCount")){
							filter.setPriceUpdateCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("price")){
							filter.setPrice(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("discountCoupenPrice")){
							filter.setDiscountCouponPrice(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("url")){
							filter.setUrl(value);
						}else if(name.equalsIgnoreCase("shippingMethod")){
							filter.setShippingMethod(value);
						}else if(name.equalsIgnoreCase("trackingNo")){
							filter.setTrackingNo(value);
						}else if(name.equalsIgnoreCase("secondaryOrderType")){
							filter.setSecondaryOrderType(value);
						}else if(name.equalsIgnoreCase("secondaryOrderId")){
							filter.setSecondaryOrderId(value);
						}else if(name.equalsIgnoreCase("lastUpdated")){
							filter.setLastUpdatedDateStr(value);
						}else if(name.equalsIgnoreCase("eventId")){
							filter.setEventId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("purchaseOrderId")){
							filter.setPurchaseOrderId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("purchaseOrderDateTime")){
							filter.setPurchaseOrderDateStr(value);
						}else if(name.equalsIgnoreCase("zoneTixQty")){
							filter.setZoneTixQty(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("zoneCheapestPrice")){
							filter.setZoneCheapestPrice(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("zoneTixGroupCount")){
							filter.setZoneTixGroupCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("zoneTotalPrice")){
							filter.setZoneTotalPrice(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("zoneProfitAndLoss")){
							filter.setZoneProfitAndLoss(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("netTotalSoldPrice")){
							filter.setNetTotalSoldPrice(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("zoneMargin")){
							filter.setZoneMargin(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("sectionMargin")){
							filter.setSectionMargin(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("netSoldPrice")){
							filter.setNetSoldPrice(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("customerName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("brokerId")){
							filter.setBrokerId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("companyName")){
							filter.setCompanyName(value);
						}else if(name.equalsIgnoreCase("platform")){
							filter.setPlatform(value);
						}else if(name.equalsIgnoreCase("zone")){
							filter.setZone(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}					
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	// eventMarketId:aaa,eventName:bbb,eventDate:ccc,eventTime:ddd,dayOfTheWeek:eee,venue:fff,venue:ggg,venueId:hhh,noOfTix:iii,noOfTixSold:jjj,
	// city:kkk,state:lll,country:mmm,grandChildCategory:nnn,childCategory:ooo,parentCategory:ppp,eventCreated:qqq,eventLastUpdated:rrr,
	// notes:sss.
	public static GridHeaderFilters getEventSearchHeaderFilters(String headerFilter)throws Exception{
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >=2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("eventMarketId")){
							filter.setEventId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("eventName")){
							filter.setEventName(value);
						}else if(name.equalsIgnoreCase("eventDate")){
							filter.setEventDateStr(value);
						}else if(name.equalsIgnoreCase("eventTime")){
							filter.setEventTimeStr(value);
						}else if(name.equalsIgnoreCase("dayOfTheWeek")){
							filter.setDayOfWeek(value);
						}else if(name.equalsIgnoreCase("venue")){
							filter.setVenueName(value);
						}else if(name.equalsIgnoreCase("venueId")){
							filter.setVenueId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("noOfTix")){
							filter.setNoOfTixCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("noOfTixSold")){
							filter.setNoOfTixSoldCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("city")){
							filter.setCity(value);
						}else if(name.equalsIgnoreCase("state")){
							filter.setState(value);
						}else if(name.equalsIgnoreCase("country")){
							filter.setCountry(value);
						}else if(name.equalsIgnoreCase("grandChildCategory")){
							filter.setGrandChildCategoryName(value);
						}else if(name.equalsIgnoreCase("childCategory")){
							filter.setChildCategoryName(value);
						}else if(name.equalsIgnoreCase("parentCategory")){
							filter.setParentCategoryName(value);
						}else if(name.equalsIgnoreCase("eventCreated")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("eventLastUpdated")){
							filter.setLastUpdatedDateStr(value);
						}else if(name.equalsIgnoreCase("notes")){
							filter.setInternalNotes(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}						
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return filter;
	}
	
	// section:aaa,row:bbb,seatLow:ccc,seatHigh:ddd,quantity:eee,retailPrice:fff,wholeSalePrice:ggg,price:hhh,taxAmount:iii,facePrice:jjj,
	// cost:kkk,ticketType:lll,shippingMethod:mmm,nearTermDisplayOption:nnn,broadcast:ooo,sectionRange:ppp,rowRange:qqq,internalNotes:rrr,
	// externalNotes:sss,marketPlaceNotes:ttt,productType:uuu,eventName:vvv,eventDate:www,eventTime:xxx,venue:yyy.
	public static GridHeaderFilters getTicketSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){					
					String nameValue[] = param.split(":");					
					if(nameValue.length >=2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("invoiceId")){
							filter.setInvoiceId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("section")){
							filter.setSection(value);
						}else if(name.equalsIgnoreCase("row")){
							filter.setRow(value);
						}else if(name.equalsIgnoreCase("seatLow")){
							filter.setSeatLow(value);
						}else if(name.equalsIgnoreCase("seatHigh")){
							filter.setSeatHigh(value);
						}else if(name.equalsIgnoreCase("quantity")){
							filter.setQuantity(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("retailPrice")){
							filter.setRetailPrice(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("wholeSalePrice")){
							filter.setWholeSalePrice(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("price")){
							filter.setPrice(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("taxAmount")){
							filter.setTaxAmount(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("facePrice")){
							filter.setFacePrice(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("cost")){
							filter.setCost(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("ticketType")){
							filter.setTicketType(value);
						}else if(name.equalsIgnoreCase("shippingMethod")){
							filter.setShippingMethod(value);
						}else if(name.equalsIgnoreCase("nearTermDisplayOption")){
							filter.setNearTermDisplayOption(value);
						}else if(name.equalsIgnoreCase("broadcast")){
							if(value.toLowerCase().contains("y")){
								filter.setBroadcast(1);
							}else{
								filter.setBroadcast(0);
							}
						}else if(name.equalsIgnoreCase("sectionRange")){
							filter.setSectionRange(value);
						}else if(name.equalsIgnoreCase("rowRange")){
							filter.setRowRange(value);
						}else if(name.equalsIgnoreCase("internalNotes")){
							filter.setInternalNotes(value);
						}else if(name.equalsIgnoreCase("externalNotes")){
							filter.setExternalNotes(value);
						}else if(name.equalsIgnoreCase("marketPlaceNotes")){
							filter.setMarketPlaceNotes(value);
						}else if(name.equalsIgnoreCase("productType")){
							filter.setProductType(value);
						}else if(name.equalsIgnoreCase("eventName")){
							filter.setEventName(value);
						}else if(name.equalsIgnoreCase("eventDate")){
							filter.setEventDateStr(value);
						}else if(name.equalsIgnoreCase("eventTime")){
							filter.setEventTimeStr(value);
						}else if(name.equalsIgnoreCase("venue")){
							filter.setVenueName(value);
						}else if(name.equalsIgnoreCase("brokerId")){
							filter.setBrokerId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}					
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	// artist:aaa,grandChildCategory:bbb,childCategory:ccc,parentCategory:ddd,createdDate:eee,createdBy:fff.
	public static GridHeaderFilters getPopularArtistSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length ==2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("artist")){
							filter.setArtistName(value);
						}else if(name.equalsIgnoreCase("grandChildCategory")){
							filter.setGrandChildCategoryName(value);
						}else if(name.equalsIgnoreCase("childCategory")){
							filter.setChildCategoryName(value);
						}else if(name.equalsIgnoreCase("parentCategory")){
							filter.setParentCategoryName(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}						
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	// customerIpAddress:aaa,webConfigId:bbb,actionType:ccc,hittingDate:ddd,actionResult:eee,url:fff,authenticatedHit:ggg.
	public static GridHeaderFilters getWebsiteTrackingSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length ==2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("customerIpAddress")){
							filter.setCustomerIpAddress(value);
						}else if(name.equalsIgnoreCase("platform")){
							filter.setPlatform(value);
						}else if(name.equalsIgnoreCase("apiName")){
							filter.setActionType(value);
						}else if(name.equalsIgnoreCase("hittingDate")){
							filter.setHittingDateStr(value);
						}else if(name.equalsIgnoreCase("description")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("custId")){
							filter.setCustomerId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("contestId")){
							filter.setContestId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("appVersion")){
							filter.setText2(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}						
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	// customerId:aaa,firstName:bbb,lastName:ccc,email:ddd,phone:eee,orderId:fff,orderTotal:ggg,orderType:hhh,eventId:iii,quantity:jjj,
	// zone:kkk,paypalTransactionId:lll,transactionId:mmm,paymentId:nnn,platform:ooo,status:ppp,createdDate:qqq,lastUpdated:rrr,
	// transactionDate:sss.
	public static GridHeaderFilters getPaypalSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length ==2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("customerId")){
							filter.setCustomerId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("firstName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("lastName")){
							filter.setLastName(value);
						}else if(name.equalsIgnoreCase("email")){
							filter.setEmail(value);
						}else if(name.equalsIgnoreCase("phone")){
							filter.setPhone(value);
						}else if(name.equalsIgnoreCase("orderId")){
							filter.setCustomerOrderId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("orderTotal")){
							filter.setOrderTotal(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("orderType")){
							filter.setOrderType(value);
						}else if(name.equalsIgnoreCase("eventId")){
							filter.setEventId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("quantity")){
							filter.setQuantity(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("zone")){
							filter.setZone(value);
						}else if(name.equalsIgnoreCase("paypalTransactionId")){
							filter.setPaypalTransactionId(value);
						}else if(name.equalsIgnoreCase("transactionId")){
							filter.setTransactionId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("paymentId")){
							filter.setPaymentId(value);
						}else if(name.equalsIgnoreCase("platform")){
							filter.setPlatform(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("lastUpdated")){
							filter.setLastUpdatedDateStr(value);
						}else if(name.equalsIgnoreCase("transactionDate")){
							filter.setTransactionDateStr(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}					
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	// userName:aaa,firstName:bbb,lastName:ccc,email:ddd,phone:eee,role:fff.
	public static GridHeaderFilters getUserSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length ==2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("userName")){
							filter.setUsername(value);
						}else if(name.equalsIgnoreCase("firstName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("lastName")){
							filter.setLastName(value);
						}else if(name.equalsIgnoreCase("email")){
							filter.setEmail(value);
						}else if(name.equalsIgnoreCase("phone")){
							filter.setPhone(value);
						}else if(name.equalsIgnoreCase("role")){
							filter.setUserRole(value);
						}else if(name.equalsIgnoreCase("brokerId")){
							filter.setUserId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("companyName")){
							filter.setCompanyName(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("promotionalCode")){
							filter.setReferrerCode(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}						
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	// id:aaa,childCategory:bbb,parentCategory:ccc,lastUpdatedBy:ddd,lastUpdated:eee
	public static GridHeaderFilters getFantasyChildCategorySearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("id")){
							filter.setChildCategoryId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("childCategory")){
							filter.setChildCategoryName(value);
						}else if(name.equalsIgnoreCase("parentCategory")){
							filter.setParentCategoryName(value);
						}else if(name.equalsIgnoreCase("lastUpdatedBy")){
							filter.setLastUpdatedBy(value);
						}else if(name.equalsIgnoreCase("lastUpdated")){
							filter.setLastUpdatedDateStr(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		return filter;
	}
	
	// id:aaa, grandChildCategory:bbb, childCategory:ccc,parentCategory:ddd,lastUpdatedBy:ddd,lastUpdated:eee
	public static GridHeaderFilters getFantasyGrandChildCategorySearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("id")){
							filter.setGrandChildCategoryId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("grandChildCategory")){
							filter.setGrandChildCategoryName(value);
						}else if(name.equalsIgnoreCase("childCategory")){
							filter.setChildCategoryName(value);
						}else if(name.equalsIgnoreCase("parentCategory")){
							filter.setParentCategoryName(value);
						}else if(name.equalsIgnoreCase("packageCost")){
							filter.setPackageCost(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("lastUpdatedBy")){
							filter.setLastUpdatedBy(value);
						}else if(name.equalsIgnoreCase("lastUpdated")){
							filter.setLastUpdatedDateStr(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	// orderNo:aaa, orderDate:bbb, orderType:ccc, rewardPoints:ddd, eventName:ddd, eventDate:eee, eventTime:fff.
	public static GridHeaderFilters getRewardPointsSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("orderNo")){
							filter.setCustomerOrderId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("orderDate")){
							filter.setOrderDateStr(value);
						}else if (name.equalsIgnoreCase("orderType")){
							filter.setOrderType(value);
						}else if(name.equalsIgnoreCase("rewardPoints")){
							filter.setRewardPoints(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("eventName")){
							filter.setEventName(value);
						}else if(name.equalsIgnoreCase("eventDate")){
							filter.setEventDateStr(value);
						}else if(name.equalsIgnoreCase("eventTime")){
							filter.setEventTimeStr(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	public static GridHeaderFilters getCrownJewelOrderSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >=2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("id")){
							filter.setId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("regularOrderId")){
							filter.setCustomerOrderId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("leagueName")){
							filter.setLeagueName(value);
						}else if(name.equalsIgnoreCase("teamName")){
							filter.setTeamName(value);
						}else if(name.equalsIgnoreCase("venue")){
							filter.setVenueName(value);
						}else if(name.equalsIgnoreCase("city")){
							filter.setCity(value);
						}else if(name.equalsIgnoreCase("state")){
							filter.setState(value);
						}else if(name.equalsIgnoreCase("country")){
							filter.setCountry(value);
						}else if(name.equalsIgnoreCase("isPackageSelected")){
							if(value.toLowerCase().contains("y")){
								filter.setIsPackageSelected(1);
							}else{
								filter.setIsPackageSelected(0);
							}
						}else if(name.equalsIgnoreCase("packageCost")){
							filter.setCost(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("packageNote")){
							filter.setInternalNotes(value);
						}else if(name.equalsIgnoreCase("eventId")){
							filter.setEventId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("ticketId")){
							filter.setTicketId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("zone")){
							filter.setZone(value);
						}else if(name.equalsIgnoreCase("ticketQty")){
							filter.setTicketQty(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("ticketPrice")){
							filter.setPrice(Double.parseDouble(value));
						}/*else if(name.equalsIgnoreCase("ticketPoints")){
							filter.setTicketPoints(Double.parseDouble(value));
						}*/else if(name.equalsIgnoreCase("requirePoints")){
							filter.setRequirePoints(Double.parseDouble(value));
						}/*else if(name.equalsIgnoreCase("packagePoints")){
							filter.setPackagePoints(Double.parseDouble(value));
						}*/else if(name.equalsIgnoreCase("customerName")){
							filter.setCustomerName(value);
						}/*else if(name.equalsIgnoreCase("remainingPoints")){
							filter.setRemainingPoints(Double.parseDouble(value));
						}*/else if(name.equalsIgnoreCase("grandChildCategoryName")){
							filter.setGrandChildCategoryName(value);
						}else if(name.equalsIgnoreCase("platform")){
							filter.setPlatform(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("lastUpdatedDate")){
							filter.setLastUpdatedDateStr(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}						
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getHoldTicketsSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("customerId")){
							filter.setCustomerId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("customerName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("salePrice")){
							filter.setSalePrice(Double.parseDouble(value));
						}else if (name.equalsIgnoreCase("createdBy")){
							filter.setCsr(value);
						}else if(name.equalsIgnoreCase("expirationDate")){
							filter.setExpirationDateStr(value);
						}else if(name.equalsIgnoreCase("expirationMinutes")){
							filter.setExpirationMinutes(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("shippingMethodId")){
							filter.setShippingMethodId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("shippingMethod")){
							filter.setShippingMethod(value);
						}else if(name.equalsIgnoreCase("holdDate")){
							filter.setHoldDateStr(value);
						}else if(name.equalsIgnoreCase("internalNote")){
							filter.setInternalNotes(value);
						}else if(name.equalsIgnoreCase("externalNote")){
							filter.setExternalNotes(value);
						}else if(name.equalsIgnoreCase("ticketIds")){
							filter.setTicketIdsStr(value);
						}else if(name.equalsIgnoreCase("externalPo")){
							filter.setExternalNo(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("ticketGroupId")){
							filter.setTicketGroupId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("brokerId")){
							filter.setBrokerId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("section")){
							filter.setSection(value);
						}else if(name.equalsIgnoreCase("row")){
							filter.setRow(value);
						}else if(name.equalsIgnoreCase("seatLow")){
							filter.setSeatLow(value);
						}else if(name.equalsIgnoreCase("seatHigh")){
							filter.setSeatHigh(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	public static GridHeaderFilters getLockedTicketsSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("section")){
							filter.setSection(value);
						}else if(name.equalsIgnoreCase("row")){
							filter.setRow(value);
						}else if(name.equalsIgnoreCase("seatLow")){
							filter.setSeatLow(value);
						}else if(name.equalsIgnoreCase("seatHigh")){
							filter.setSeatHigh(value);
						}else if(name.equalsIgnoreCase("quantity")){
							filter.setQuantity(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("retailPrice")){
							filter.setRetailPrice(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("wholeSalePrice")){
							filter.setWholeSalePrice(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("price")){
							filter.setPrice(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("taxAmount")){
							filter.setTaxAmount(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("facePrice")){
							filter.setFacePrice(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("cost")){
							filter.setCost(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("ticketType")){
							filter.setTicketType(value);
						}else if(name.equalsIgnoreCase("shippingMethod")){
							filter.setShippingMethod(value);
						}else if(name.equalsIgnoreCase("nearTermDisplayOption")){
							filter.setNearTermDisplayOption(value);
						}else if(name.equalsIgnoreCase("broadcast")){
							if(value.toLowerCase().contains("y")){
								filter.setBroadcast(1);
							}else{
								filter.setBroadcast(0);
							}
						}else if(name.equalsIgnoreCase("sectionRange")){
							filter.setSectionRange(value);
						}else if(name.equalsIgnoreCase("rowRange")){
							filter.setRowRange(value);
						}else if(name.equalsIgnoreCase("internalNotes")){
							filter.setInternalNotes(value);
						}else if(name.equalsIgnoreCase("externalNotes")){
							filter.setExternalNotes(value);
						}else if(name.equalsIgnoreCase("marketPlaceNotes")){
							filter.setMarketPlaceNotes(value);
						}else if(name.equalsIgnoreCase("productType")){
							filter.setProductType(value);
						}else if(name.equalsIgnoreCase("eventName")){
							filter.setEventName(value);
						}else if(name.equalsIgnoreCase("eventDate")){
							filter.setEventDateStr(value);
						}else if(name.equalsIgnoreCase("eventTime")){
							filter.setEventTimeStr(value);
						}else if(name.equalsIgnoreCase("venue")){
							filter.setVenueName(value);
						}else if(name.equalsIgnoreCase("categoryTicketGroupId")){
							filter.setCategoryTicketGroupId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("ipAddress")){
							filter.setCustomerIpAddress(value);
						}else if (name.equalsIgnoreCase("platform")){
							filter.setPlatform(value);
						}else if(name.equalsIgnoreCase("creationDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("lockStatus")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getAffiliatesSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("userId")){
							filter.setUserId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("userName")){
							filter.setUsername(value);
						}else if(name.equalsIgnoreCase("firstName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("lastName")){
							filter.setLastName(value);
						}else if(name.equalsIgnoreCase("email")){
							filter.setEmail(value);
						}else if(name.equalsIgnoreCase("phone")){
							filter.setPhone(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("promotionalCode")){
							filter.setReferrerCode(value);
						}else if(name.equalsIgnoreCase("activeCash")){
							filter.setActiveCash(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("pendingCash")){
							filter.setPendingCash(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}/*else if(name.equalsIgnoreCase("lastCreditedCash")){
							filter.setLastCreditedCash(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("lastDebitedCash")){
							filter.setLastDebitedCash(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("totalCreditedCash")){
							filter.setTotalCreditedCash(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("totalDebitedCash")){
							filter.setTotalDebitedCash(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("lastUpdate")){
							filter.setLastUpdatedDateStr(value);
						}*/					
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getAffiliateReportSearchHeaderFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("orderId")){
							filter.setCustomerOrderId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("firstName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("lastName")){
							filter.setLastName(value);
						}else if(name.equalsIgnoreCase("eventName")){
							filter.setEventName(value);
						}else if(name.equalsIgnoreCase("eventDate")){
							filter.setEventDateStr(value);
						}else if(name.equalsIgnoreCase("eventTime")){
							filter.setEventTimeStr(value);
						}else if(name.equalsIgnoreCase("ticketQty")){
							filter.setQuantity(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("orderTotal")){
							filter.setOrderTotal(Double.valueOf(value));
						}else if(name.equalsIgnoreCase("cashCredit")){
							filter.setCashCredited(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("promoCode")){
							filter.setReferrerCode(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}/*else if(name.equalsIgnoreCase("paymentNote")){
							filter.setInternalNotes(value);
						}*/					
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getContestAffiliatesFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("customerType")){
							filter.setCustomerType(value);
						}else if(name.equalsIgnoreCase("firstName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("lastName")){
							filter.setLastName(value);
						}else if(name.equalsIgnoreCase("userId")){
							filter.setUsername(value);
						}else if(name.equalsIgnoreCase("email")){
							filter.setEmail(value);
						}else if(name.equalsIgnoreCase("phone")){
							filter.setPhone(value);
						}else if(name.equalsIgnoreCase("fromDate")){
							filter.setStartDate(value);
						}else if(name.equalsIgnoreCase("toDate")){
							filter.setEndDate(value);
						}else if(name.equalsIgnoreCase("rewardType")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("rewardValue")){
							filter.setPrice(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("customerLives")){
							filter.setPointWinnerCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("affiliateLives")){
							filter.setTicketWinnerCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("activeCashReward")){
							filter.setText2(value);
						}else if(name.equalsIgnoreCase("debitedCashReward")){
							filter.setText3(value);
						}else if(name.equalsIgnoreCase("totalCashReward")){
							filter.setText4(value);
						}else if(name.equalsIgnoreCase("voidedCashReward")){
							filter.setText5(value);
						}else if(name.equalsIgnoreCase("totalCreditedRewardDollar")){
							filter.setText6(value);
						}else if(name.equalsIgnoreCase("referralCode")){
							filter.setReferrerCode(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);;
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setUpdatedBy(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setUpdatedDate(value);
						}else if(name.equalsIgnoreCase("custRewardDollar")){
							filter.setRewardPoints(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("custSuperFanStar")){
							filter.setCount(Integer.parseInt(value));;
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}/*else if(name.equalsIgnoreCase("paymentNote")){
							filter.setInternalNotes(value);
						}*/					
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	public static GridHeaderFilters getContestAffiliateEarningFilters(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("firstName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("lastName")){
							filter.setLastName(value);
						}else if(name.equalsIgnoreCase("userId")){
							filter.setUsername(value);
						}else if(name.equalsIgnoreCase("email")){
							filter.setEmail(value);
						}else if(name.equalsIgnoreCase("phone")){
							filter.setPhone(value);
						}else if(name.equalsIgnoreCase("rewardType")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("rewardCash")){
							filter.setPrice(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("customerLives")){
							filter.setPointWinnerCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("affiliateLives")){
							filter.setTicketWinnerCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("rewardDollars")){
							filter.setText2(value);
						}else if(name.equalsIgnoreCase("referralCode")){
							filter.setReferrerCode(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);;
						}else if(name.equalsIgnoreCase("playedDate")){
							filter.setHittingDateStr(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setUpdatedDate(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("affilateUserId")){
							filter.setText3(value);
						}else if(name.equalsIgnoreCase("signupDate")){
							filter.setEventDateStr(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}/*else if(name.equalsIgnoreCase("paymentNote")){
							filter.setInternalNotes(value);
						}*/					
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	public static GridHeaderFilters getRTFPromotionalOfferFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("promocode")){
							filter.setPromoCode(value);
						}else if(name.equalsIgnoreCase("discountPerc")){
							filter.setDiscountCouponPrice(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("mobileDiscountPerc")){
							filter.setMobileDiscount(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("startDate")){
							filter.setStartDate(value);
						}else if(name.equalsIgnoreCase("endDate")){
							filter.setEndDate(value);
						}else if(name.equalsIgnoreCase("maxOrder")){
							filter.setMaxOrders(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("orders")){
							filter.setNoOfOrders(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("promoType")){
							filter.setPromoType(value);
						}else if(name.equalsIgnoreCase("artist")){
							filter.setArtistName(value);
						}else if(name.equalsIgnoreCase("event")){
							filter.setEventName(value);
						}else if(name.equalsIgnoreCase("venue")){
							filter.setVenueName(value);
						}else if(name.equalsIgnoreCase("parent")){
							filter.setParentCategoryName(value);
						}else if(name.equalsIgnoreCase("child")){
							filter.setChildCategoryName(value);
						}else if(name.equalsIgnoreCase("grandChild")){
							filter.setGrandChildCategoryName(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("modifiedBy")){
							filter.setLastUpdatedBy(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	

	public static GridHeaderFilters getRTFCustomerPromotionalOfferFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("firstName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("lastName")){
							filter.setLastName(value);
						}else if(name.equalsIgnoreCase("email")){
							filter.setEmail(value);
						}else if(name.equalsIgnoreCase("promoCode")){
							filter.setPromoCode(value);
						}else if(name.equalsIgnoreCase("discount")){
							filter.setDiscountCouponPrice(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("startDate")){
							filter.setStartDate(value);
						}else if(name.equalsIgnoreCase("endDate")){
							filter.setEndDate(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("modifiedDate")){
							filter.setLastUpdatedDateStr(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	public static GridHeaderFilters getRTFPromotionalOfferTrackingFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("customerId")){
							filter.setCustomerId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("firstName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("lastName")){
							filter.setLastName(value);
						}else if(name.equalsIgnoreCase("email")){
							filter.setEmail(value);
						}else if(name.equalsIgnoreCase("promoCode")){
							filter.setPromoCode(value);
						}else if(name.equalsIgnoreCase("discount")){
							filter.setDiscountCouponPrice(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("orderId")){
							filter.setCustomerOrderId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("orderTotal")){
							filter.setOrderTotal(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("quantity")){
							filter.setQuantity(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("ipAddress")){
							filter.setCustomerIpAddress(value);
						}else if(name.equalsIgnoreCase("primaryPaymentMethod")){
							filter.setPrimaryPaymentMethod(value);
						}else if(name.equalsIgnoreCase("secondaryPaymentMethod")){
							filter.setSecondaryPaymentMethod(value);
						}else if(name.equalsIgnoreCase("thirdPaymentMethod")){
							filter.setThirdPaymentMethod(value);
						}else if(name.equalsIgnoreCase("platform")){
							filter.setPlatform(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getContestsFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("contestName")){
							filter.setContestName(value);
						}else if(name.equalsIgnoreCase("startDate")){
							filter.setStartDate(value);
						}else if(name.equalsIgnoreCase("participantStar")){
							filter.setPartipantCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("referralStar")){
							filter.setWinnerCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("ticketWinnerThreshhold")){
							filter.setTicketWinnerThreshhold(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("freeTicketPerWinner")){
							filter.setFreeTicketPerWinner(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("referralReward")){
							filter.setRetailPrice(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("participantRewards")){
							filter.setInvoiceTotal(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("participantLives")){
							filter.setOrderId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("rewardPoints")){
							filter.setRewardPoints(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setUpdatedDate(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setUpdatedBy(value);
						}else if(name.equalsIgnoreCase("contestMode")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("questionSize")){
							filter.setCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("zone")){
							filter.setZone(value);
						}else if(name.equalsIgnoreCase("artistEventCategoryName")){
							filter.setPromoReferenceName(value);
						}else if(name.equalsIgnoreCase("artistEventCategoryType")){
							filter.setPromoType(value);
						}else if(name.equalsIgnoreCase("promotionalCode")){
							filter.setPromoCode(value);
						}else if(name.equalsIgnoreCase("discountPercentage")){
							filter.setMobileDiscount(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("singleTixPrice")){
							filter.setPrice(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("contestCategory")){
							filter.setCategory(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}else if(name.equalsIgnoreCase("contestJackpotType")){
							filter.setText2(value);
						}/*else if(name.equalsIgnoreCase("mjpWinnerPerQuestion")){
							filter.setNoOfOrders(Integer.parseInt(value));
						}*/else if(name.equalsIgnoreCase("giftCardName")){
							filter.setText3(value);
						}else if(name.equalsIgnoreCase("giftCardPerWinner")){
							filter.setText4(value);
						}
						
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	public static GridHeaderFilters getContestsWinnerFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("custName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("lastName")){
							filter.setLastName(value);
						}else if(name.equalsIgnoreCase("userId")){
							filter.setUsername(value);
						}else if(name.equalsIgnoreCase("email")){
							filter.setEmail(value);
						}else if(name.equalsIgnoreCase("rewardPoints")){
							filter.setRewardPoints(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("rewardRank")){
							filter.setUserId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("rewardTickets")){
							filter.setTicketQty(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("orderId")){
							filter.setOrderId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("phone")){
							filter.setPhone(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("jackpotType")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("winnerType")){
							filter.setText2(value);
						}else if(name.equalsIgnoreCase("notes")){
							filter.setText3(value);
						}else if(name.equalsIgnoreCase("createdDateStr")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("expiryDateStr")){
							filter.setEndDate(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	public static GridHeaderFilters getContestQuestionFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("question")){
							filter.setText7(value);
						}else if(name.equalsIgnoreCase("optionA")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("optionB")){
							filter.setText2(value);
						}else if(name.equalsIgnoreCase("optionC")){
							filter.setText3(value);
						}else if(name.equalsIgnoreCase("optionD")){
							filter.setText4(value);
						}else if(name.equalsIgnoreCase("answer")){
							filter.setText5(value);
						}else if(name.equalsIgnoreCase("difficultyLevel")){
							filter.setText6(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setUpdatedDate(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setLastUpdatedBy(value);
						}else if(name.equalsIgnoreCase("questionReward")){
							filter.setCost((Double.parseDouble(value)/100));
						}else if(name.equalsIgnoreCase("serialNo")){
							filter.setNoOfOrders(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						} else if(name.equalsIgnoreCase("miniJackpotType")){
							System.out.println("miniJackpotType................"+value);
							filter.setText8(value);
						} else if(name.equalsIgnoreCase("mjpNoOfWinner")){
							filter.setText9(value);
						} else if(name.equalsIgnoreCase("mjpQtyPerWinner")){
							filter.setText10(value);
						} else if(name.equalsIgnoreCase("mjpGiftCardName")){
							filter.setText11(value);
						}
						
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getDiscountCodeTrackingFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						
						if(name.equalsIgnoreCase("customerId")){
							filter.setCustomerId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("firstName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("lastName")){
							filter.setLastName(value);
						}else if(name.equalsIgnoreCase("email")){
							filter.setEmail(value);
						}else if(name.equalsIgnoreCase("referrerCode")){
							filter.setReferrerCode(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getManualFedexGenerationFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("blFirstName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("blLastName")){
							filter.setLastName(value);
						}else if(name.equalsIgnoreCase("blCompanyName")){
							filter.setCompanyName(value);
						}else if(name.equalsIgnoreCase("blAddress1")){
							filter.setAddressLine1(value);
						}else if(name.equalsIgnoreCase("blAddress2")){
							filter.setAddressLine2(value);
						}else if(name.equalsIgnoreCase("blCity")){
							filter.setCity(value);
						}else if(name.equalsIgnoreCase("blStateName")){
							filter.setState(value);
						}else if(name.equalsIgnoreCase("blCountryName")){
							filter.setCountry(value);
						}else if(name.equalsIgnoreCase("blZipCode")){
							filter.setZipCode(value);
						}else if(name.equalsIgnoreCase("blPhone")){
							filter.setPhone(value);
						}else if(name.equalsIgnoreCase("shCustomerName")){
							filter.setShCustomerName(value);
						}else if(name.equalsIgnoreCase("shCompanyName")){
							filter.setShCompanyName(value);
						}else if(name.equalsIgnoreCase("shAddress1")){
							filter.setShAddressLine1(value);
						}else if(name.equalsIgnoreCase("shAddress2")){
							filter.setShAddressLine2(value);
						}else if(name.equalsIgnoreCase("shCity")){
							filter.setShCity(value);
						}else if(name.equalsIgnoreCase("shStateName")){
							filter.setShState(value);
						}else if(name.equalsIgnoreCase("shCountryName")){
							filter.setShCountry(value);
						}else if(name.equalsIgnoreCase("shZipCode")){
							filter.setShZipCode(value);
						}else if(name.equalsIgnoreCase("shPhone")){
							filter.setShPhone(value);
						}else if(name.equalsIgnoreCase("serviceType")){
							filter.setServiceType(value);
						}else if(name.equalsIgnoreCase("trackingNo")){
							filter.setTrackingNo(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getLotteriesFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("contestName")){
							filter.setContestName(value);
						}else if(name.equalsIgnoreCase("minPurchaseAmount")){
							filter.setMinimumPurchaseAmount(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("startDate")){
							filter.setStartDate(value);
						}else if(name.equalsIgnoreCase("endDate")){
							filter.setEndDate(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setLastUpdatedBy(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("artistName")){
							filter.setArtistName(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getParticipantsFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("participantContId")){
							filter.setContestId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("customerId")){
							filter.setCustomerId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("customerName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("customerEmail")){
							filter.setEmail(value);
						}else if(name.equalsIgnoreCase("purchaseCustomerId")){
							filter.setPurchaseCustomerId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("purchaseCustomerName")){
							filter.setPurchaseCustomerName(value);
						}else if(name.equalsIgnoreCase("purchaseCustomerEmail")){
							filter.setPurchaseCustomerEmail(value);
						}else if(name.equalsIgnoreCase("referralCode")){
							filter.setReferrerCode(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getUserAuditFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("userName")){
							filter.setUsername(value);
						}else if(name.equalsIgnoreCase("userAction")){
							filter.setUserAction(value);
						}else if(name.equalsIgnoreCase("clientIPAddress")){
							filter.setClientIPAddress(value);
						}else if(name.equalsIgnoreCase("message")){
							filter.setMessage(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setLastUpdatedDateStr(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getContestAllEventFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("contestId")){
							filter.setContestId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("eventId")){
							filter.setEventId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("eventName")){
							filter.setEventName(value);
						}else if(name.equalsIgnoreCase("eventDate")){
							filter.setEventDateStr(value);
						}else if(name.equalsIgnoreCase("eventTime")){
							filter.setEventTimeStr(value);
						}else if(name.equalsIgnoreCase("venueId")){
							filter.setVenueId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("venue")){
							filter.setVenueName(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}				
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	public static GridHeaderFilters getRtfCatsEventFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						 if(name.equalsIgnoreCase("eventId")){
							filter.setEventId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("eventName")){
							filter.setEventName(value);
						}else if(name.equalsIgnoreCase("eventDate")){
							filter.setEventDateStr(value);
						}else if(name.equalsIgnoreCase("eventTime")){
							filter.setEventTimeStr(value);
						}else if(name.equalsIgnoreCase("venueId")){
							filter.setVenueId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("venue")){
							filter.setVenueName(value);
						}else if(name.equalsIgnoreCase("city")){
							filter.setCity(value);
						}else if(name.equalsIgnoreCase("state")){
							filter.setState(value);
						}else if(name.equalsIgnoreCase("country")){
							filter.setCountry(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}				
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	public static GridHeaderFilters getContestEventFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("contestId")){
							filter.setContestId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("eventId")){
							filter.setEventId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("eventName")){
							filter.setEventName(value);
						}else if(name.equalsIgnoreCase("eventDate")){
							filter.setEventDateStr(value);
						}else if(name.equalsIgnoreCase("eventTime")){
							filter.setEventTimeStr(value);
						}else if(name.equalsIgnoreCase("venueId")){
							filter.setVenueId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("venue")){
							filter.setVenueName(value);
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setUpdatedBy(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setUpdatedDate(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}						
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getContestEventRequestFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("eventName")){
							filter.setEventName(value);
						}else if(name.equalsIgnoreCase("venue")){
							filter.setVenueName(value);
						}else if(name.equalsIgnoreCase("eventDescription")){
							filter.setEventDescription(value);
						}else if(name.equalsIgnoreCase("eventDate")){
							filter.setEventDateStr(value);
						}else if(name.equalsIgnoreCase("socialMediaLink")){
							filter.setSocialMediaLink(value);
						}else if(name.equalsIgnoreCase("contactName")){
							filter.setContactName(value);
						}else if(name.equalsIgnoreCase("contactEmail")){
							filter.setContactEmail(value);
						}else if(name.equalsIgnoreCase("contactPhone")){
							filter.setContactPhone(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("noOfTickets")){
							filter.setNoOfTixCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getQuestionBankFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("question")){
							filter.setText7(value);
						}else if(name.equalsIgnoreCase("optionA")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("optionB")){
							filter.setText2(value);
						}else if(name.equalsIgnoreCase("optionC")){
							filter.setText3(value);
						}else if(name.equalsIgnoreCase("factChecked")){
							filter.setText4(value);
						}else if(name.equalsIgnoreCase("answer")){
							filter.setText5(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setUpdatedDate(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setLastUpdatedBy(value);
						}else if(name.equalsIgnoreCase("questionReward")){
							filter.setCost((Double.parseDouble(value)/100));
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("category")){
							filter.setCategory(value);
						}else if(name.equalsIgnoreCase("difficultyLevel")){
							filter.setText6(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	
	public static GridHeaderFilters getContestPromocodeFiter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("promoName")){
							filter.setPromoCode(value);
						}else if(name.equalsIgnoreCase("customerName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("noOfFreeTickets")){
							filter.setTicketQty(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("promoType")){
							filter.setPromoType(value);
						}else if(name.equalsIgnoreCase("promoTypeName")){
							filter.setPromoReferenceName(value);
						}else if(name.equalsIgnoreCase("expiryDate")){
							filter.setEndDate(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setUpdatedDate(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("modifiedBy")){
							filter.setLastUpdatedBy(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getContestPromoOfferFiter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("promoCode")){
							filter.setPromoCode(value);
						}else if(name.equalsIgnoreCase("startDate")){
							filter.setStartDate(value);
						}else if(name.equalsIgnoreCase("endDate")){
							filter.setEndDate(value);
						}else if(name.equalsIgnoreCase("maxLifePerCustomer")){
							filter.setTicketWinnerCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("maxAccumulateThreshold")){
							filter.setTicketWinnerThreshhold(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("curAccumulatedCount")){
							filter.setPriceUpdateCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setUpdatedDate(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setLastUpdatedBy(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	public static GridHeaderFilters getCustomerQuestionFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("question")){
							filter.setText7(value);
						}else if(name.equalsIgnoreCase("optionA")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("optionB")){
							filter.setText2(value);
						}else if(name.equalsIgnoreCase("optionC")){
							filter.setText3(value);
						}else if(name.equalsIgnoreCase("answer")){
							filter.setText5(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	
	public static GridHeaderFilters getPreHeaderContestChecklistFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("preContestId")){
							filter.setContestId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("contestName")){
							filter.setContestName(value);
						}else if(name.equalsIgnoreCase("extendedName")){
							filter.setExternalNotes(value);
						}else if(name.equalsIgnoreCase("zone")){
							filter.setZone(value);
						}else if(name.equalsIgnoreCase("grandPrizeEvent")){
							filter.setEventName(value);
						}else if(name.equalsIgnoreCase("questionSize")){
							filter.setCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("contestDateTimeStr")){
							filter.setStartDate(value);
						}else if(name.equalsIgnoreCase("rewardDollarsPrize")){
							filter.setRewardPoints(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("grandWinnerTicketPrize")){
							filter.setFreeTicketPerWinner(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("grandPrizeWinner")){
							filter.setWinnerCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("categoryType")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("contestCategory")){
							filter.setParentCategoryName(value);
						}else if(name.equalsIgnoreCase("participantStar")){
							filter.setPartipantCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("referralStar")){
							filter.setWinnerCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("categoryName")){
							filter.setCategory(value);
						}else if(name.equalsIgnoreCase("promoCategoryType")){
							filter.setPromoType(value);
						}else if(name.equalsIgnoreCase("promoCategoryName")){
							filter.setPromoReferenceName(value);;
						}else if(name.equalsIgnoreCase("pricePerTicket")){
							filter.setPrice(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("discountCode")){
							filter.setPromoCode(value);
						}else if(name.equalsIgnoreCase("discountPercentage")){
							filter.setDiscountCouponPrice(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("hostName")){
							filter.setText2(value);
						}else if(name.equalsIgnoreCase("producerName")){
							filter.setText3(value);
						}else if(name.equalsIgnoreCase("directorName")){
							filter.setText4(value);
						}else if(name.equalsIgnoreCase("technicalDirectorName")){
							filter.setText5(value);
						}else if(name.equalsIgnoreCase("backendOperatorName")){
							filter.setText6(value);
						}else if(name.equalsIgnoreCase("isQuestionEntered")){
							filter.setText7(value);
						}else if(name.equalsIgnoreCase("isQuestionVerified")){
							filter.setText8(value);;
						}else if(name.equalsIgnoreCase("questionEnteredBy")){
							filter.setText9(value);
						}else if(name.equalsIgnoreCase("questionVerifiedBy")){
							filter.setUsername(value);
						}else if(name.equalsIgnoreCase("rewardDollarsPerQuestion")){
							filter.setPointsPerWinner(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("approvedBy")){
							filter.setLastUpdatedBy(value);
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setUpdatedBy(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("createdDateStr")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("updatedDateStr")){
							filter.setUpdatedDate(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	
	public static GridHeaderFilters getGiftCardFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("giftCardId")){
							filter.setId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("title")){
							filter.setContestName(value);
						}else if(name.equalsIgnoreCase("description")){
							filter.setEventDescription(value);
						}else if(name.equalsIgnoreCase("imageUrl")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("conversionType")){
							filter.setText2(value);
						}else if(name.equalsIgnoreCase("conversionRate")){
							filter.setTaxAmount(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setUpdatedDate(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setUpdatedBy(value);
						}else if(name.equalsIgnoreCase("amount")){
							filter.setMinimumPurchaseAmount(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("maxQuantity")){
							filter.setEventTixQty(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("usedQuantity")){
							filter.setZoneTixQty(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("remainingQty")){
							filter.setQuantity(Integer.parseInt(value));;
						}else if(name.equalsIgnoreCase("maxQtyThreshold")){
							filter.setTicketWinnerThreshhold(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}else if(name.equalsIgnoreCase("startDate")){
							filter.setText3(value);
						}else if(name.equalsIgnoreCase("endDate")){
							filter.setText4(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	
	public static GridHeaderFilters getGiftCardBrandFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("giftCardBrandId")){
							filter.setId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("name")){
							filter.setContestName(value);
						}else if(name.equalsIgnoreCase("description")){
							filter.setEventDescription(value);
						}else if(name.equalsIgnoreCase("quantity")){
							filter.setQuantity(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setUpdatedDate(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("displayOutOfStockImage")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setUpdatedBy(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}					
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	
	
	
	public static GridHeaderFilters getPollingSponsorFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("id")){
							filter.setId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("title")){
							filter.setContestName(value);
						}else if(name.equalsIgnoreCase("description")){
							filter.setEventDescription(value);
						}else if(name.equalsIgnoreCase("email")){
							filter.setEmail(value);
						}else if(name.equalsIgnoreCase("contactPerson")){
							filter.setContactName(value);
						}else if(name.equalsIgnoreCase("phone")){
							filter.setPhone(value);
						}else if(name.equalsIgnoreCase("altPhone")){
							filter.setPhone(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setUpdatedDate(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setUpdatedBy(value);
						}else if(name.equalsIgnoreCase("address")){
							filter.setAddressLine1(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getPollingContestFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("pollingId")){
							filter.setId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("title")){
							filter.setContestName(value);
						}else if(name.equalsIgnoreCase("description")){
							filter.setEventDescription(value);
						}else if(name.equalsIgnoreCase("pollingInterval")){
							filter.setPollingInterval(value);
						}else if(name.equalsIgnoreCase("maxQuePerCust")){
							filter.setMaxQuePerCust(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setUpdatedDate(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setUpdatedBy(value);
						}else if(name.equalsIgnoreCase("startdate")){
							filter.setStartDate(value);
						}else if(name.equalsIgnoreCase("enddate")){
							filter.setEndDate(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getPollingCategoryFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("categoryId")){
							filter.setId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("title")){
							filter.setContestName(value);
						}else if(name.equalsIgnoreCase("pollingType")){
							filter.setEventDescription(value);
						}else if(name.equalsIgnoreCase("sponsor")){
							filter.setText1(value);
						} else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setUpdatedDate(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setUpdatedBy(value);
						}  else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}else if(name.equalsIgnoreCase("startDate")){
							filter.setText3(value);
						}else if(name.equalsIgnoreCase("endDate")){
							filter.setText4(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getPollingCategoryQuestionFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("question")){
							filter.setText7(value);
						}else if(name.equalsIgnoreCase("optionA")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("optionB")){
							filter.setText2(value);
						}else if(name.equalsIgnoreCase("optionC")){
							filter.setText3(value);
						}else if(name.equalsIgnoreCase("optionD")){
							filter.setText4(value);
						}else if(name.equalsIgnoreCase("answer")){
							filter.setText5(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setUpdatedDate(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setLastUpdatedBy(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}  
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getPollingVideoFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("id")){
							filter.setId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("title")){
							filter.setEventDescription(value);
						}else if(name.equalsIgnoreCase("description")){
							filter.setArtistName(value);
						}else if(name.equalsIgnoreCase("videoUrl")){
							filter.setUrl(value);
						}else if(name.equalsIgnoreCase("category")){
							filter.setCategory(value);
						}else if(name.equalsIgnoreCase("sponsorName")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("isDefault")){
							if(value.toLowerCase().contains("y")){
								filter.setIsEmailed(1);
							}else{
								filter.setIsEmailed(0);
							}
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getPollingVideoCategoryFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("id")){
							filter.setId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("description")){
							filter.setEventDescription(value);
						}else if(name.equalsIgnoreCase("imageUrl")){
							filter.setUrl(value);
						}else if(name.equalsIgnoreCase("categoryName")){
							filter.setCategory(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	public static GridHeaderFilters getPollingCustomerVideoFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("customerId")){
							filter.setCustomerId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("customerName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("phone")){
							filter.setPhone(value);
						}else if(name.equalsIgnoreCase("lastName")){
							filter.setLastName(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);						
						}else if(name.equalsIgnoreCase("published")){
							filter.setText1(value);					
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	public static GridHeaderFilters getGiftCardOrderFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("giftCardOrderId")){
							filter.setId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("cardTitle")){
							filter.setContestName(value);
						}else if(name.equalsIgnoreCase("cardDescription")){
							filter.setEventDescription(value);
						}else if(name.equalsIgnoreCase("isEmailSent")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("orderType")){
							filter.setText2(value);
						}else if(name.equalsIgnoreCase("remarks")){
							filter.setText3(value);
						}else if(name.equalsIgnoreCase("barcode")){
							filter.setText4(value);
						}else if(name.equalsIgnoreCase("isRedeemed")){
							filter.setText6(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setUpdatedDate(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setUpdatedBy(value);
						}else if(name.equalsIgnoreCase("redeemedRewards")){
							filter.setMinimumPurchaseAmount(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("quantity")){
							filter.setQuantity(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("customerName")){
							filter.setCustomerName(value);
						}else if(name.equalsIgnoreCase("userId")){
							filter.setUsername(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}					
	
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	
	public static GridHeaderFilters getCustomerProfileQuestions(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("question")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("serialNo")){
							filter.setCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("profileType")){
							filter.setText2(value);
						}else if(name.equalsIgnoreCase("points")){
							filter.setPointWinnerCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("questionType")){
							filter.setActionType(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setUpdatedDate(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setLastUpdatedBy(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						} 
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	
	public static GridHeaderFilters getRewardConfigFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("mediaDescription")){
							filter.setEventDescription(value);
						}else if(name.equalsIgnoreCase("lives")){
							filter.setCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("magicWands")){
							filter.setEventCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("points")){
							filter.setPointWinnerCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("sfStars")){
							filter.setNoOfTixCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("rtfPoints")){
							filter.setPointWinnerCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("rwdDollars")){
							filter.setRewardPoints(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("actionType")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("activeDate")){
							filter.setStartDate(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setUpdatedDate(value);
						}else if(name.equalsIgnoreCase("maxRewards")){
							filter.setContestId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("noOfLikes")){
							filter.setBrokerId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("minVideoPlayTime")){
							filter.setId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("videoRewardInterval")){
							filter.setEventId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setLastUpdatedBy(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						} 
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	
	public static GridHeaderFilters getRtfPointConversionSeettingsFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("qty")){
							filter.setCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("points")){
							filter.setPointWinnerCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("powerUpType")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setUpdatedDate(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setLastUpdatedBy(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						} 
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	
	public static GridHeaderFilters getReportedMediaFIlter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("reportedUserId")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("reportedEmail")){
							filter.setText2(value);;
						}else if(name.equalsIgnoreCase("reportedPhone")){
							filter.setText3(value);
						}else if(name.equalsIgnoreCase("userId")){
							filter.setUsername(value);
						}else if(name.equalsIgnoreCase("email")){
							filter.setEmail(value);
						}else if(name.equalsIgnoreCase("phone")){
							filter.setPhone(value);
						}else if(name.equalsIgnoreCase("abuseType")){
							filter.setActionType(value);
						}else if(name.equalsIgnoreCase("blockBy")){
							filter.setUpdatedBy(value);
						}else if(name.equalsIgnoreCase("hideBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("title")){
							filter.setText4(value);
						}else if(name.equalsIgnoreCase("description")){
							filter.setEventDescription(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("blockDate")){
							filter.setLastUpdatedDateStr(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						} 
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	
	
	public static GridHeaderFilters getReportedCommentFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("reportedUserId")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("reportedEmail")){
							filter.setText2(value);;
						}else if(name.equalsIgnoreCase("reportedPhone")){
							filter.setText3(value);
						}else if(name.equalsIgnoreCase("userId")){
							filter.setUsername(value);
						}else if(name.equalsIgnoreCase("email")){
							filter.setEmail(value);
						}else if(name.equalsIgnoreCase("phone")){
							filter.setPhone(value);
						}else if(name.equalsIgnoreCase("abuseType")){
							filter.setActionType(value);
						}else if(name.equalsIgnoreCase("blockBy")){
							filter.setUpdatedBy(value);
						}else if(name.equalsIgnoreCase("hideBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("commentText")){
							filter.setText4(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("blockDate")){
							filter.setLastUpdatedDateStr(value);
						}else if(name.equalsIgnoreCase("hideDate")){
							filter.setStartDate(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						} 
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	
	public static GridHeaderFilters getSuperfanStarsFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("starsFrom")){
							filter.setCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("starsTo")){
							filter.setNoOfTixCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("levelNo")){
							filter.setEventCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("title")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setLastUpdatedDateStr(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setUpdatedBy(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						} 
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	public static GridHeaderFilters getFanClubFIlter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("fanClubId")){
							filter.setId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("title")){
							filter.setArtistName(value);
						}else if(name.equalsIgnoreCase("postText")){
							filter.setArtistName(value);
						}else if(name.equalsIgnoreCase("description")){
							filter.setEventDescription(value);
						}else if(name.equalsIgnoreCase("category")){
							filter.setCategory(value);;
						}else if(name.equalsIgnoreCase("userId")){
							filter.setUsername(value);
						}else if(name.equalsIgnoreCase("email")){
							filter.setEmail(value);
						}else if(name.equalsIgnoreCase("phone")){
							filter.setPhone(value);;
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setLastUpdatedDateStr(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setUpdatedBy(value);
						}else if(name.equalsIgnoreCase("blockDate")){
							filter.setInvoiceDateStr(value);
						}else if(name.equalsIgnoreCase("blockBy")){
							filter.setLastUpdatedBy(value);
						}else if(name.equalsIgnoreCase("joinDate")){
							filter.setStartDate(value);
						}else if(name.equalsIgnoreCase("exitDate")){
							filter.setEndDate(value);
						}else if(name.equalsIgnoreCase("eventName")){
							filter.setEventName(value);
						}else if(name.equalsIgnoreCase("venue")){
							filter.setVenueName(value);
						}else if(name.equalsIgnoreCase("eventDate")){
							filter.setEventDateStr(value);
						}else if(name.equalsIgnoreCase("memberCount")){
							filter.setPartipantCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("interestedCount")){
							filter.setCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("likeCount")){
							filter.setWinnerCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("reason")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						} 
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	public static GridHeaderFilters getFanClubAbuseFIlter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("abuseCommentId") || name.equalsIgnoreCase("fanclubId")  || name.equalsIgnoreCase("abusePostId")  || 
								name.equalsIgnoreCase("abuseEventId") || name.equalsIgnoreCase("abuseVideoId") ){
							filter.setId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setLastUpdatedDateStr(value);
						}else if(name.equalsIgnoreCase("reportedByUserId")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("reportedByEmail")){
							filter.setText2(value);
						}else if(name.equalsIgnoreCase("reportedByPhone")){
							filter.setText3(value);
						}else if(name.equalsIgnoreCase("abuseType")){
							filter.setText4(value);
						}else if(name.equalsIgnoreCase("userId")){
							filter.setUsername(value);
						}else if(name.equalsIgnoreCase("email")){
							filter.setEmail(value);
						}else if(name.equalsIgnoreCase("phone")){
							filter.setPhone(value);
						}else if(name.equalsIgnoreCase("title")){
							filter.setArtistName(value);
						}else if(name.equalsIgnoreCase("description")){
							filter.setEventDescription(value);
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setUpdatedBy(value);
						}else if(name.equalsIgnoreCase("count")){
							filter.setPartipantCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						} 
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	public static GridHeaderFilters getFanClubPostCommentsFIlter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("postId")){
							filter.setId(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("commentText")){
							filter.setArtistName(value);
						}else if(name.equalsIgnoreCase("userId")){
							filter.setUsername(value);
						}else if(name.equalsIgnoreCase("email")){
							filter.setEmail(value);
						}else if(name.equalsIgnoreCase("phone")){
							filter.setPhone(value);;
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDateStr(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setLastUpdatedDateStr(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setUpdatedBy(value);
						}else if(name.equalsIgnoreCase("blockReason")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						} 
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	private static boolean validateParamValues(String name,String value){
		if(name==null || name.isEmpty() || name.equalsIgnoreCase("undefined") ||
				value==null || value.isEmpty() || value.equalsIgnoreCase("undefined")){
			return false;
		}
		return true;	
	}
}



