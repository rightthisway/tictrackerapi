package com.rtw.tracker.utils;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtw.tmat.utils.Util;
import com.rtw.tracker.pojos.DashboardInfo;

public class EndContestThread implements Runnable{

	private Integer contestId;
	HttpServletRequest request;
	private Boolean isEndedSuccessFully;
	private static Logger contestLog = LoggerFactory.getLogger(EndContestThread.class);
	
	public EndContestThread(Integer contestId,HttpServletRequest request,Boolean isEndedSuccessFully) {
		this.request = request;
		this.contestId = contestId;
		this.isEndedSuccessFully = isEndedSuccessFully;
	}
	
	@Override
	public void run() {
		Map<String, String> map1 = com.rtw.tmat.utils.Util.getParameterMapCass(request);
		map1.put("contestId",contestId.toString());
		map1.put("isEndedSuccessfully",isEndedSuccessFully.toString());
		
		try {
			contestLog.info("MIGRATION STARTED (ForceMigrationToSqlFromCass)");
			String data1 = Util.getObject(map1, com.rtw.tmat.utils.Constants.BASE_URL+com.rtw.tmat.utils.Constants.FORCE_MIGRATION_TO_SQL_FROM_CASS);
			Gson gson1 = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
			DashboardInfo dashInfo = gson1.fromJson(((JsonObject)jsonObject1.get("dashboardInfo")), DashboardInfo.class);
			if(dashInfo.getSts() == 0){
				contestLog.error("RTFAPI ERROR (ForceMigrationToSqlFromCass): "+isEndedSuccessFully+" :"+dashInfo.getErr().getDesc());
			}
			contestLog.info("MIGRATION COMPLETED (ForceMigrationToSqlFromCass)");
		} catch (Exception e) {
			contestLog.error("ERROR OCCURED WHILE CALLING END CONTEST THREAD: "+isEndedSuccessFully);
		    e.printStackTrace();
		}
	}

}
