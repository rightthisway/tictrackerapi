package com.rtw.tracker.utils;




/**
 * Hold constant variables
 */

public final class Constants {
	
	public static final String Reward_The_Fan_Product="RewardTheFan";
	public static final String Pre_Sale_Zone_Tickets_Product="PreSaleZoneTickets";
	public static final String Category_Ticekts_Product="CategoryTickets";
	public static final String First_Ten_Row_Tickets_Product="FirstTenRowTickets";
	public static final String Last_Ten_Row_Tickets_Product="LastTenRowTickets";
	
	/*public static final String RTW_cards_Path="C:/REWARDTHEFAN/cards/";
	public static final String CONTEST_REPORT_TEMPLATE_PATH="C:/REWARDTHEFAN/reportTemplate/";
	public static final String Artist_Image_Path="C:/REWARDTHEFAN/ArtistImage/";
	public static final String Real_ticket_Path="C:/REWARDTHEFAN/Tickets/";
	public static final String Grand_Child_Category_Image_Path="C:/REWARDTHEFAN/GrandChildCategoryImage/";
	public static final String Fantasy_Grand_Child_Category_Image_Path="C:/REWARDTHEFAN/FantasyGrandChildCategoryImage/";
	public static final String LoyalFan_Parent_Category_Image_Path="C:/REWARDTHEFAN/LoyalFanParentCategoryImage/";
	public static final String Child_Category__Image_Path="C:/REWARDTHEFAN/ChildCategoryImage/";
	public static final String Parent_category_Image_Path="C:/REWARDTHEFAN/ParentCategoryImage/";*/
	
	//TicTracker Reports Constants
	public static final String EVENTS_IN_TMAT_BUT_NOT_IN_RTF = "EventsInTmatButNotInRTF";
	public static final String POINTS_SUMMARY_REPORT = "PontsSummaryReport";
	public static final String RTF_EVENTS_STATISTICS = "RTFEventsStatistics";
	public static final String RTF_UNFILLED_REPORT = "RTFUnFilledReport";
	public static final String RTF_FILLED_REPORT = "RTFFilledReport";
	public static final String RTF_PROFIT_AND_LOSS_REPORT = "RTFProfitAndLossReport";
	public static final String RTF_ZONES_NOT_HAVING_COLORS = "RTFZonesNotHavingColorsReport";	
	public static final String RTF_NEXT_15_DAYS_UNSENT_ORDERS = "RTFNext15DaysUnsentReport";
	public static final String RTF_UNFILLED_SHORTS_WITH_POSSIBLE_LONG_INVENTORY = "RTFUnFilledShortsWithPossibleLongInventoryReport";
	public static final String RTF_UNSOLD_INVENTORY_REPORT = "RTFUnsoldInventoryReport";
	public static final String RTF_UNSOLD_CATEGORY_TICKET_REPORT = "RTFUnsoldCategoryTicketsReport";
	public static final String RTF_INVOICE_REPORT = "RTFInvoiceReport";
	public static final String RTF_PURCHASE_ORDER_REPORT = "RTFPurchaseOrderReport";
	public static final String RTF_SALES_REPORT = "RTFSalesReport";
	public static final String RTF_SALES_REPORT_BY_REFERRAL = "RTFSalesReportByReferral";
	public static final String RTF_CUSTOMER_SPENT_REPORT = "RTFCustomerSpentReport";
	public static final String RTF_CUSTOMER_ORDER_DETAILS_REPORT = "RTFCustomerOrderDetailsReport";
	public static final String RTF_REPEATED_ORDERS_REPORT = "RTFRepeatedOrdersReport";
	public static final String RTF_CHANNELWISE_ORDERS_REPORT = "RTFChannelWiseOrdersReport";
	public static final String CLIENT_MASTER_REPORT = "ClientMasterReport";
	public static final String UNSUBSCRIBED_CLIENT_MASTER_REPORT = "UnsubscribedClientMasterReport";
	public static final String PURCHASED_MASTER_REPORT = "PurchasedMasterReport";
	public static final String RTF_REGISTERED_BUT_NOT_PURCHASED_REPORT = "RTFRegisteredButNotPurchasedReport";
	public static final String RTF_COMBINED_PURCHASED_NONPURCHASED_REPORT = "RTFCombinedPurchasedNonPurchasedReport";
	public static final String RTF_LOYAL_FAN_REPORT = "RTFLoyalFanReport";
	public static final String RTF_LOYAL_FAN_PURCHASED_REPORT = "RTFLoyalFanPurchasedReport";
	public static final String RTF_LOYAL_FAN_NON_PURCHASED_REPORT = "RTFLoyalFanNonPurchasedReport";
	public static final String RTF_TMAT_SEAT_ALLOCATION_REPORT = "RTFTMATSeatAllocationReport";
	
	public static final String RTF_CONTEST_REPORT = "ContestReport";
	public static final String RTF_CONTEST_DETAIL_TACKING_REPORT = "ContestDetailTrackingReport";
	public static final String RTF_CONTEST_TICKET_COST_REPORT = "TicketCostReport";
	public static final String RTF_CONTEST_INVOICE_REPORT = "WinnerInvoiceReport";
	public static final String RTF_CUSTOMER_EARNED_CONTEST_REWARD_DOLLAR_REPORT = "CustomerEarnedContestRewardDollarsReport"; // Shiva Modified on 18th Oct 2019 - Constant matching the Report Name
	public static final String RTF_CUSTOMER_PURCHASE_REWARD_DOLLAR_REPORT = "CustomerPurchaseOnRewardDollarReport";
	public static final String RTF_CONTEST_CUSTOMER_REFFERAL_PERCENTAGE_REPORT = "ContestCustomerRefferalPercentageReport";
	public static final String RTF_CONTEST_CUSTOMER_REFFERAL_AVERAGE_REPORT = "ContestCustomerRefferalAerageReport";
	public static final String RTF_REWARD_DOLLAR_SPENT_ON_CONTEST_AND_CUSTOMER = "RewardDollarSpentOnContestAndCustomerReport";  // Shiva Modified on 18th Oct 2019 - Constant matching the Report Name
	public static final String RTF_CONTEST_REFERRED_BY_REFERRED_TO_REPORT = "CustomerReferredByReferredToReport";
	public static final String RTF_CONTEST_REFERRE_REFERRALS_REPORT = "CustomerReferreeRefferralsDailyReport";
	public static final String RTF_CONTEST_QUESTION_WISE_USERCOUNT = "ContestQuestionWiseUserAnswerCount";
	public static final String RTF_AVG_QUESTION_ASWERED_BY_CUSTOMER = "AverageNoOfQuestionAnsweredByCustomerReport";
	public static final String RTF_No_OF_GAME_PLAYED_BY_CUSTOMER = "AverageNoOfGamePlayedByCustomerReport";
	public static final String RTF_CUSTOMER_REWARD_DOLLARS_BREADOWN = "CustomerRewardDollarsBreakdownDetailedReport"; // Shiva Modified on 14 Oct 2019
	public static final String RTF_CONTEST_WISE_FIRST_TIME_CUSTOMER = "ContestWiseFirstTimeCustomerReport";
	public static final String RTF_CONTEST_LIVES_REPORT = "ContestCustomerEarnedAndUsedLivesReport";
	public static final String RTF_REGISTERED_NOT_PLAYED_CUSTOMERS = "RegisteredCustomersWhoDidNotPlayedContestReport";
	public static final String RTF_REGISTERED_PLAYED_CUSTOMERS = "RegisteredCustomersWhoPlayedContestReport";
	//public static final String RTF_CUSTOMER_CONTEST_REWARD_DOLLARS = "CustomersContestRewwardDollarsReport"; // Shiva Commented and added below new constant on 14 Oct 2019
	public static final String RTF_CUSTOMER_REWARD_DOLLARS_LIABILITY = "CustomersRewardDollarsLiabilityReport"; // Shiva Modified on 14 Oct 2019
	public static final String RTF_REGISTERED_CUSTOMERS = "RegisteredCustomersReport";
	public static final String RTF_CONTEST_CUSTOMER_REFFERAL_AVERAGE_REPORT_EXCLUDING_BOTS = "ContestCustomerRefferalAerageReport_ExcludingBOTS";  //Shiva added for existing report
	public static final String RTF_CUSTOMER_NO_OF_GAMES_PLAYED = "CustomerNoOfGamesPlayedReport";  //Shiva added for existing report
	public static final String RTF_MONTH_WISE_REGISTERED_CUSTOMER_COUNT = "MonthWiseRegisteredCustomerCountReport";  //Shiva added for existing report
	public static final String RTF_ACTIVE_INACTIVE_CUSTOMERS = "ActiveInactiveCustomersCountReport";  //Shiva added for existing report
	public static final String RTF_FANTASY_CONTEST_PARTICIPANTS_COUNT = "FantasyContestParticipantsCountReport";  //Shiva added for existing report
	public static final String RTF_CONTEST_WISE_AVERAGE_TIME_TAKEN = "ContestWiseAverageTimeTakenReport";  //Shiva added for existing report
	// Shiva BEGINS of Adding New Constants on 14 Oct 2019
	public static final String RTF_FANTASY_FOOTBALL_CONTEST_WINNERS = "FantasyFootballContestWinnersReport";
	public static final String RTF_MONTHLY_CUSTOMER_PARTICIPANTS_COUNT_STATISTICS = "MonthlyCustomerParticipantsCountStatisticsReport";
	public static final String RTF_OUTSTANDING_CONTEST_ORDERS_TOBE_FULFILLED = "OutstandingContestOrdersToBeFulfilledWithIn5DaysReport";
	public static final String RTF_CUSTOMER_AVERAGE_TIME_ACCESSING_RTF_TRIVIA = "CustomerAverageTimeAccessingRTFTriviaReport";
	public static final String RTF_MONTHLY_CUSTOMER_PARTICIPANTS_COUNT_BREAKDOWN_STATISTICS = "MonthlyCustomerParticipantsCountBreakdownStatisticsReport";
	public static final String RTF_CUSTOMER_CONTEST_STATISTICS_FOR_BUSINESS = "CustomerContestStatisticsForBusinessReport";
	public static final String RTF_GIFT_CARD_PURCHASED_WINNERS = "GiftCardPurchasedAndWinnersReport";
	public static final String RTF_POLLING_STATISTICS = "PollingStatisticsReport";
	public static final String RTF_TV_STATISTICS = "RTFTVStatisticsReport";
	public static final String RTF_CUSTOMER_TIME_SPENT_ON_EACH_CONTEST = "CustomerTimeSpentOnEachContestReport";
	public static final String RTF_CUSTOMER_AVERAGE_TIME_TAKEN_PER_CONTEST_STATISTICS = "CustomerAverageTimeTakenperContestStatisticsReport";
	public static final String RTF_GRAND_MEGA_MINI_JACKPOT_TICKET_WINNER_COUNT = "GrandMegaMINIJackpotTicketWinnerCountReport";
	public static final String RTF_CUSTOMERS_USING_REFERRAL_CODE = "CustomersUsingReferralCodeReport_DFSKARMAandRTFBWAY";
	public static final String RTF_GIFT_CARD_ORDERS_OUTSTANDING_PENDING_COUNT = "GiftCardOrdersOutstandingAndPendingCountReport";
	public static final String RTF_GIFT_CARD_ORDERS_OUTSTANDING_PENDING_DETAILED = "GiftCardOrdersOutstandingAndPendingDetailedReport";
	// Shiva ENDS of Adding New Constants on 23 Oct 2019
	
	
	public static final String CSV_EXTENSION = "csv";
	public static final String EXCEL_EXTENSION = "xlsx";
	public static final String NEW_LINE_DELIMITER = "\n";
	
}
