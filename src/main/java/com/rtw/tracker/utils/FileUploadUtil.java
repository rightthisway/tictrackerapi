package com.rtw.tracker.utils;

import static org.bytedeco.javacpp.opencv_core.cvSize;
import static org.bytedeco.javacpp.opencv_highgui.cvSaveImage;
import static org.bytedeco.javacpp.opencv_imgproc.cvResize;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.util.EntityUtils;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacv.FFmpegFrameGrabber;

import com.rtw.tracker.pojos.GenericResponseDTO;


public class FileUploadUtil {
	
	public static GenericResponseDTO uploadGCAssets(String assetType , File inFile , String apiBaseUrl )  {			
			
			//File inFile = new File("C:/REWARDTHEFAN/GiftCards/TMP/Chrysanthemum.jpg");
			FileInputStream fis = null;
			int statusCode = 0;
			GenericResponseDTO genResponseDTO = new GenericResponseDTO();
			try {
				if(inFile == null || inFile.equals(assetType) ) {					
					System.err.println("Please pass  Proper File to upload" );
					genResponseDTO.setStatus(0);
					genResponseDTO.setMessage("Please pass  Proper File to upload " );					
					return genResponseDTO;
				}
				if(assetType == null || assetType.isEmpty() ) {					
					System.err.println("Please pass the Asset Type to Identify if Order or card upload" );
					genResponseDTO.setStatus(0);
					genResponseDTO.setMessage("Please pass the Asset Type to Identify if Order or card upload- assetType - ["  + assetType + "]" );						
					return genResponseDTO;
				}
				if(apiBaseUrl == null || apiBaseUrl.isEmpty() ) {					
					System.err.println("Please pass the proper API Url  to upload File " );
					genResponseDTO.setStatus(0);
					genResponseDTO.setMessage("Please pass the proper API Url  to upload File " );					
					return genResponseDTO;
				}
				
				
				fis = new FileInputStream(inFile);
				DefaultHttpClient httpclient = new DefaultHttpClient(new BasicHttpParams());				
				// RTF API URL [ ?uploadPath=GCMD ] for Gift Card  or [ ?uploadPath=GCOD ] for Gift Card Order.				
				//HttpPost httppost = new HttpPost("http://127.0.0.1:7779/UploadGiftCards?uploadPath=" + assetType);
				HttpPost httppost = new HttpPost(apiBaseUrl+"UploadGiftCards?uploadPath=" + assetType);
				@SuppressWarnings("deprecation")
				MultipartEntity entity = new MultipartEntity();
				// set the file input stream and file name as arguments
				entity.addPart("file", new InputStreamBody(fis, inFile.getName()));
				httppost.setEntity(entity);	 				
				HttpResponse response = httpclient.execute(httppost);	 				
				statusCode = response.getStatusLine().getStatusCode();
				HttpEntity responseEntity = response.getEntity();
				String responseString = EntityUtils.toString(responseEntity, "UTF-8");	 				
				System.out.println("[" + statusCode + "] " + responseString);
				genResponseDTO.setStatus(1);
				genResponseDTO.setMessage(" File Uploaded to Server Successfully " );						
				
				
			} catch (ClientProtocolException e) {
				System.err.println("Unable to make connection to " + apiBaseUrl);
				e.printStackTrace();
				genResponseDTO.setStatus(0);
				genResponseDTO.setMessage("Unable to make connection to " + apiBaseUrl );
				
			} catch (IOException e) {
				System.err.println("Unable to read file" );
				e.printStackTrace();
				genResponseDTO.setStatus(0);
				genResponseDTO.setMessage("Unable to read file " );
				
			}catch (Exception e){
				System.err.println("Exception uploading file to API Server");
				e.printStackTrace();
				genResponseDTO.setStatus(0);
				genResponseDTO.setMessage("Exception uploading file to API Server" );
				
			}finally {
				try {
					if (fis != null) fis.close();
				} catch (IOException e) {}
			}
			return genResponseDTO;
		}
	
	
	public static String generateThumbnailImage(File file,String filename){
		String path =  null;
		try {
			if(file == null){
				return path;
			}
	        FFmpegFrameGrabber grabber = new FFmpegFrameGrabber(file.getPath());
	        grabber.start();
	        int i = 0;
	        opencv_core.IplImage img = null;
	        int max = 1;
	        if (max <= 0) {
	            max = Integer.MAX_VALUE;
	        }
	        while (i++ < max) {
	            opencv_core.IplImage source = grabber.grab();
	            if (img == null) {
	                opencv_core.CvSize size = cvSize(source.width() / 2, source.height() / 2);
	                img = opencv_core.IplImage.create(size, source.depth(), source.nChannels());
	            }
	            path = URLUtil.RTFMEDIA_THUMBNAIL+filename+".jpg";
	            cvResize(source, img);
	            cvSaveImage(path, img);
	        }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return path;
	}
	
	
	public static String getFileNameWithoutSpecialCharaters(String fileName){
		try {
			fileName = fileName.replaceAll("[\\\\/:*?\"<>|]", "");
			fileName = fileName.replaceAll(" ", "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileName;
	}
	
	public static String generateFileName(String ext){
		String fileName = "";
		try {
			fileName = "buss_"+new Date().getTime()+ext;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileName;
	}

}
