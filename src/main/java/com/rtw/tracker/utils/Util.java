package com.rtw.tracker.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.rtw.tmat.utils.CustomerCardInfo;
import com.rtw.tracker.datas.UserAction;
import com.rtw.tracker.datas.CategoryTicketGroup;
import com.rtw.tracker.datas.Event;
import com.rtw.tracker.datas.Venue;
import com.rtw.tracker.datas.InvoiceAudit;
import com.rtw.tracker.datas.InvoiceRefund;
import com.rtw.tracker.datas.CustomerTicketDownloads;
import com.rtw.tracker.datas.ShippingMethod;
import com.rtw.tracker.datas.TicketGroupTicketAttachment;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.CustomerAddress;
import com.rtw.tracker.datas.CustomerOrderDetails;
import com.rtw.tracker.datas.InvoiceTicketAttachment;
import com.rtw.tracker.datas.OpenOrders;
import com.rtw.tracker.datas.PaymentHistory;
import com.rtw.tracker.datas.PurchaseOrderPaymentDetails;
import com.rtw.tracker.datas.PurchaseOrders;
import com.rtw.tracker.datas.POSPurchaseOrder;
import com.rtw.tracker.datas.POSTicket;
import com.rtw.tracker.datas.PurchaseOrder;
import com.rtw.tracker.datas.ManualFedexGeneration;
import com.rtw.tracker.datas.AffiliateCashReward;
import com.rtw.tracker.datas.AffiliateCashRewardHistory;
import com.rtw.tracker.datas.AffiliateSetting;
import com.rtw.tracker.datas.ChildCategory;
import com.rtw.tracker.datas.CityAutoSearch;
import com.rtw.tracker.datas.Contests;
import com.rtw.tracker.datas.Country;
import com.rtw.tracker.datas.CrownJewelCustomerOrder;
import com.rtw.tracker.datas.Customer;
import com.rtw.tracker.datas.CustomerOrder;
import com.rtw.tracker.datas.Customers;
import com.rtw.tracker.datas.DiscountCodeTracking;
import com.rtw.tracker.datas.EmailTemplate;
import com.rtw.tracker.datas.EventDetails;
import com.rtw.tracker.datas.GrandChildCategory;
import com.rtw.tracker.datas.HoldTicketDTO;
import com.rtw.tracker.datas.OpenOrderStatus;
import com.rtw.tracker.datas.PopularArtist;
import com.rtw.tracker.datas.ProductType;
import com.rtw.tracker.datas.RTFCustomerPromotionalOffer;
import com.rtw.tracker.datas.RTFPromoOffers;
import com.rtw.tracker.datas.RTFPromotionalOfferTracking;
import com.rtw.tracker.datas.ReferralContest;
import com.rtw.tracker.datas.ReferralContestParticipants;
import com.rtw.tracker.datas.ReferralContestWinner;
import com.rtw.tracker.datas.RewardDetails;
import com.rtw.tracker.datas.Role;
import com.rtw.tracker.datas.SeatGeekOrders;
import com.rtw.tracker.datas.State;
import com.rtw.tracker.datas.Ticket;
import com.rtw.tracker.datas.TicketGroup;
import com.rtw.tracker.datas.TrackerBrokers;
import com.rtw.tracker.datas.TrackerUser;
import com.rtw.tracker.enums.InvoiceAuditAction;
import com.rtw.tracker.pojos.AffiliateSettingsCustomDTO;
import com.rtw.tracker.pojos.AffiliatesOrderDTO;
import com.rtw.tracker.pojos.CategoryTicketGroupListDTO;
import com.rtw.tracker.pojos.ChildCategoryDTO;
import com.rtw.tracker.pojos.CityStateCountryDTO;
import com.rtw.tracker.pojos.ContestsList;
import com.rtw.tracker.pojos.CountryDTO;
import com.rtw.tracker.pojos.CrownJewelCustomerOrderDTO;
import com.rtw.tracker.pojos.CustomerAddressDTO;
import com.rtw.tracker.pojos.CustomerCardInfoDTO;
import com.rtw.tracker.pojos.CustomerFavouriteEventDTO;
import com.rtw.tracker.pojos.CustomerOrderDTO;
import com.rtw.tracker.pojos.CustomerPromotionalOfferDTO;
import com.rtw.tracker.pojos.CustomerPromotionalOfferTrackingDTO;
import com.rtw.tracker.pojos.CustomerPromotionalOfferTrackingDetailsDTO;
import com.rtw.tracker.pojos.CustomerRewardPointsDTO;
import com.rtw.tracker.pojos.CustomerTicketsDownloadDTO;
import com.rtw.tracker.pojos.CustomersCustomDTO;
import com.rtw.tracker.pojos.DiscountCodeTrackingDTO;
import com.rtw.tracker.pojos.EditTrackerUserCustomDTO;
import com.rtw.tracker.pojos.EmailTemplateDTO;
import com.rtw.tracker.pojos.EventDetailsDTO;
import com.rtw.tracker.pojos.GrandChildCategoryDTO;
import com.rtw.tracker.pojos.InvoiceAuditDTO;
import com.rtw.tracker.pojos.InvoiceRefundListDTO;
import com.rtw.tracker.pojos.InvoiceTicketAttachmentDTO;
import com.rtw.tracker.pojos.ManageAffiliatesDTO;
import com.rtw.tracker.pojos.ManualFedexGenerationDTO;
import com.rtw.tracker.pojos.OpenOrdersDTO;
import com.rtw.tracker.pojos.POShippingMethodDTO;
import com.rtw.tracker.pojos.PaymentHistoryDTO;
import com.rtw.tracker.pojos.PopularArtistCustomDTO;
import com.rtw.tracker.pojos.PromoOfferDTO;
import com.rtw.tracker.pojos.PurchaseOrderDTO;
import com.rtw.tracker.pojos.PurchaseOrderPaymentDetailsDTO;
import com.rtw.tracker.pojos.ReferralContestListDTO;
import com.rtw.tracker.pojos.ReferralContestParticipantsListDTO;
import com.rtw.tracker.pojos.ReferralContestWinnerListDTO;
import com.rtw.tracker.pojos.RelatedInvoiceDTO;
import com.rtw.tracker.pojos.RelatedPODTO;
import com.rtw.tracker.pojos.RtfConfigContestClusterNodes;
import com.rtw.tracker.pojos.SeatGeekOrdersDTO;
import com.rtw.tracker.pojos.StateDTO;
import com.rtw.tracker.pojos.TicketGroupDTO;
import com.rtw.tracker.pojos.TrackerBrokerCustomDTO;
import com.rtw.tracker.pojos.TrackerUserCustomDTO;
import com.rtw.tracker.pojos.TrackerUserDTO;
import com.rtw.tracker.pojos.UserActionDTO;

public class Util {
	
	private static List<RtfConfigContestClusterNodes> rtfContestClusterNodesList = new ArrayList<RtfConfigContestClusterNodes>();
	public static Boolean isContestRunning= false;
	
	public static List<ManageAffiliatesDTO> getManageAffiliatesArray(Collection<TrackerUser> list,Map<Integer, AffiliateCashReward> cashRewardMapByUserId){
		ManageAffiliatesDTO manageAffiliatesDTO = null;
		List<ManageAffiliatesDTO> manageAffiliatesDTOList = new ArrayList<ManageAffiliatesDTO>();
		try {
			if(list!=null){
				for(TrackerUser user :list){
					manageAffiliatesDTO = new ManageAffiliatesDTO();
					manageAffiliatesDTO.setUserId(user.getId());
					manageAffiliatesDTO.setUserName(user.getUserName());
					manageAffiliatesDTO.setFirstName(user.getFirstName());
					manageAffiliatesDTO.setLastName(user.getLastName());
					manageAffiliatesDTO.setEmail(user.getEmail());
					manageAffiliatesDTO.setPhone(user.getPhone());
					manageAffiliatesDTO.setStatus(user.getStatus()==true?"ACTIVE":"DEACTIVE");
					manageAffiliatesDTO.setPromotionalCode(user.getPromotionalCode());
					manageAffiliatesDTO.setActiveCash(cashRewardMapByUserId.get(user.getId())!=null? cashRewardMapByUserId.get(user.getId()).getActiveCash(): 0.00);
					manageAffiliatesDTO.setPendingCash(cashRewardMapByUserId.get(user.getId())!=null? cashRewardMapByUserId.get(user.getId()).getPendingCash(): 0.00);
					manageAffiliatesDTOList.add(manageAffiliatesDTO);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return manageAffiliatesDTOList;
	}
	
	public static Collection<AffiliatesOrderDTO> getAffiliatesArray(Collection<AffiliateCashRewardHistory> list){
		AffiliatesOrderDTO affiliatesOrderDTO = null;
		Collection<AffiliatesOrderDTO> affiliatesOrderDTOs = new ArrayList<AffiliatesOrderDTO>();
		
		try {
			if(list!=null){
				for(AffiliateCashRewardHistory history :list){
					affiliatesOrderDTO = new AffiliatesOrderDTO();
					affiliatesOrderDTO.setOrderId(history.getOrder().getId());					
					affiliatesOrderDTO.setFirstName(history.getCustomer().getCustomerName());
					affiliatesOrderDTO.setLastName(history.getCustomer().getLastName());
					affiliatesOrderDTO.setEventName(history.getOrder().getEventName());
					affiliatesOrderDTO.setEventDate(history.getOrder().getEventDateStr());
					affiliatesOrderDTO.setEventTime(history.getOrder().getEventTimeStr());
					affiliatesOrderDTO.setTicketQty(history.getOrder().getQty());
					affiliatesOrderDTO.setOrderTotal(history.getOrderTotal());
					affiliatesOrderDTO.setCashCredit(history.getCreditedCash());
					affiliatesOrderDTO.setStatus(history.getRewardStatus());
					affiliatesOrderDTO.setPromoCode(history.getPromoCode());
					/*affiliatesOrderDTO.setPaymentNote(history.getPaymentNote());*/
					affiliatesOrderDTOs.add(affiliatesOrderDTO);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return affiliatesOrderDTOs;
	}
	
	public static List<HoldTicketDTO> getHoldTickets(List<Ticket> tickets,TicketGroup tg){
		HoldTicketDTO holdTicketDTO = null;
		List<HoldTicketDTO> holdTicketDTOs = new ArrayList<HoldTicketDTO>();
		try {
			if(tickets.isEmpty()){
				return holdTicketDTOs;
			}
			for(Ticket ticket : tickets){
				holdTicketDTO = new HoldTicketDTO();
				holdTicketDTO.setId(ticket.getId());
				holdTicketDTO.setCost(String.format("%.2f",ticket.getCost()!=null?ticket.getCost():0.00));
				holdTicketDTO.setActualSoldPrice(String.format("%.2f", ticket.getActualSoldPrice()));
				holdTicketDTO.setFacePrice(String.format("%.2f",ticket.getFacePrice()));
				holdTicketDTO.setWholeSalePrice(String.format("%.2f",ticket.getWholesalePrice()));
				holdTicketDTO.setRetailPrice(String.format("%.2f",ticket.getRetailPrice()));
				holdTicketDTO.setSection(tg.getSection());
				holdTicketDTO.setRow(tg.getRow());
				holdTicketDTO.setSeatNo(ticket.getSeatNo());
				holdTicketDTO.setStatus(ticket.getTicketStatus().toString());
				if(ticket.getOnHand()!=null && ticket.getOnHand()){
					holdTicketDTO.setOnHandStatus("Yes");
				}else{
					holdTicketDTO.setOnHandStatus("No");
				}
				holdTicketDTOs.add(holdTicketDTO);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return holdTicketDTOs;
	}
	
	public static List<TrackerUserCustomDTO> getMangeUsersObject(Collection<TrackerUser> list){
		TrackerUserCustomDTO trackerUserCustomDTO = null;
		List<TrackerUserCustomDTO> trackerUserCustomDTOs = new ArrayList<TrackerUserCustomDTO>(); 
		try {
			if(list!=null){			
				for(TrackerUser user : list){
					trackerUserCustomDTO = new TrackerUserCustomDTO();
					trackerUserCustomDTO.setId(user.getId());
					trackerUserCustomDTO.setUserName(user.getUserName());
					trackerUserCustomDTO.setFirstName(user.getFirstName());
					trackerUserCustomDTO.setLastName(user.getLastName());
					trackerUserCustomDTO.setEmail(user.getEmail());										
					trackerUserCustomDTO.setPhone(user.getPhone());
					if(user.getRoles().size() > 1){
						int i=1;
						String rol1 = "";
						String rol2 = "";
						String rol3 = "";
						for(Role role: user.getRoles()){
							if(i==1){
								rol1 = role.getName();
							}else if(i==2){
								rol2 = role.getName();
							}else if(i==3){
								rol3 = role.getName();
							}
							i++;
						}
						
						if(rol1.equalsIgnoreCase("ROLE_SUPER_ADMIN") || rol2.equalsIgnoreCase("ROLE_SUPER_ADMIN") || rol3.equalsIgnoreCase("ROLE_SUPER_ADMIN")){
							trackerUserCustomDTO.setRole("ROLE_SUPER_ADMIN");
						}else if(rol1.equalsIgnoreCase("ROLE_CONTEST") || rol2.equalsIgnoreCase("ROLE_CONTEST") || rol3.equalsIgnoreCase("ROLE_CONTEST")){
							trackerUserCustomDTO.setRole("ROLE_CONTEST");
						}else if(rol1.equalsIgnoreCase("ROLE_USER") || rol2.equalsIgnoreCase("ROLE_USER") || rol3.equalsIgnoreCase("ROLE_USER")){
							trackerUserCustomDTO.setRole("ROLE_USER");
						}else if(rol1.equalsIgnoreCase("ROLE_BROKER") || rol2.equalsIgnoreCase("ROLE_BROKER") || rol3.equalsIgnoreCase("ROLE_BROKER")){
							trackerUserCustomDTO.setRole("ROLE_BROKER");
						}else if(rol1.equalsIgnoreCase("ROLE_AFFILIATES") || rol2.equalsIgnoreCase("ROLE_AFFILIATES") || rol3.equalsIgnoreCase("ROLE_AFFILIATES")){
							trackerUserCustomDTO.setRole("ROLE_AFFILIATES");
						}
					}else{
						for(Role role: user.getRoles()){
							trackerUserCustomDTO.setRole(role.getName());
						}
					}
										
					trackerUserCustomDTO.setBrokerId(user.getBroker()!= null ? String.valueOf(user.getBroker().getId()): "");
					trackerUserCustomDTO.setCompanyName(user.getBroker()!= null ? user.getBroker().getCompanyName() : "");
					trackerUserCustomDTO.setStatus(user.getStatus()==true?"ACTIVE":"DEACTIVE");
					trackerUserCustomDTO.setPromotionalCode(user.getPromotionalCode());
					trackerUserCustomDTOs.add(trackerUserCustomDTO);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return trackerUserCustomDTOs;
	}
	
	public static EditTrackerUserCustomDTO getEditUsersObject(TrackerUser user){
		EditTrackerUserCustomDTO editTrackerUserCustomDTO = null;
		
		try{
			if(user!=null){
				editTrackerUserCustomDTO = new EditTrackerUserCustomDTO();
				editTrackerUserCustomDTO.setUserId(user.getId());
				editTrackerUserCustomDTO.setUserName(user.getUserName());
				editTrackerUserCustomDTO.setFirstName(user.getFirstName());
				editTrackerUserCustomDTO.setLastName(user.getLastName());
				editTrackerUserCustomDTO.setEmail(user.getEmail());
				editTrackerUserCustomDTO.setPhone(user.getPhone());
				editTrackerUserCustomDTO.setStatus(user.getStatus());
				editTrackerUserCustomDTO.setPromoCode(user.getPromotionalCode());
				editTrackerUserCustomDTO.setSellerId(user.getSellerId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return editTrackerUserCustomDTO;
	}
	
	public static TrackerBrokerCustomDTO getTrackerBrokerObject(TrackerBrokers trackerBrokers){
		TrackerBrokerCustomDTO trackerBrokerCustomDTO = null;
		
		try{
			if(trackerBrokers!=null){
				trackerBrokerCustomDTO = new TrackerBrokerCustomDTO();
				trackerBrokerCustomDTO.setBrokerId(trackerBrokers.getId());
				trackerBrokerCustomDTO.setBrokerCompany(trackerBrokers.getCompanyName());
				trackerBrokerCustomDTO.setBrokerServiceFees(trackerBrokers.getServiceFees());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return trackerBrokerCustomDTO;
	}
		
	public static List<PopularArtistCustomDTO> getAllArtistArray(List<PopularArtist> artistList){
		List<PopularArtistCustomDTO> PopularArtistCustomDTOList = new ArrayList<PopularArtistCustomDTO>();
		PopularArtistCustomDTO popularArtistCustomDTO = null;
		try {
			if(artistList!=null){
				for(PopularArtist artist : artistList){
					popularArtistCustomDTO = new PopularArtistCustomDTO();
					popularArtistCustomDTO.setArtistId(artist.getArtistId());
					popularArtistCustomDTO.setArtistName(artist.getArtistName());
					popularArtistCustomDTO.setGrandChildCategory(artist.getGrandChildCategoryName());
					popularArtistCustomDTO.setChildCategory(artist.getChildCategoryName());
					popularArtistCustomDTO.setParentCategory(artist.getParentType());
					popularArtistCustomDTO.setImageFileUrl(artist.getImageFileUrl());
					if(artist.getImageFileUrl() != null && artist.getImageFileUrl() != ""){
						popularArtistCustomDTO.setImgUploaded("Yes");
					}else{
						popularArtistCustomDTO.setImgUploaded("No");
					}
					popularArtistCustomDTO.setCreatedBy(artist.getCreatedBy());
					popularArtistCustomDTO.setCreatedDate(artist.getCreatedDateStr());
					if(artist.getEventCount()==null){
						popularArtistCustomDTO.setEventCount(0);
					}else{
						popularArtistCustomDTO.setEventCount(artist.getEventCount());
					}
					PopularArtistCustomDTOList.add(popularArtistCustomDTO);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PopularArtistCustomDTOList;
	}
	
	public static List<PopularArtist> getPopularArtists(Integer productId, GridHeaderFilters filter){

		List<PopularArtist> popularArtists = new ArrayList<PopularArtist>();
		try {
			List<PopularArtist> popArtists = DAORegistry.getQueryManagerDAO().getAllActivePopularArtistByProductId(productId,filter,null);
			Set<Integer> artistIds = new HashSet<Integer>();
			
			for(PopularArtist popArtist: popArtists){				
				if(artistIds.add(popArtist.getArtistId())){
					popularArtists.add(popArtist);
				}
			}	
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return popularArtists;
	}

	public static AffiliateSettingsCustomDTO getAffiliateObject(AffiliateSetting setting){
		AffiliateSettingsCustomDTO affiliateSettingCustomDTO = null;
		
		try{
			if(setting!=null){
				affiliateSettingCustomDTO = new AffiliateSettingsCustomDTO();
				affiliateSettingCustomDTO.setUserId(setting.getUserId());
				affiliateSettingCustomDTO.setCashDiscount(setting.getCashDiscount()*100);
				affiliateSettingCustomDTO.setCustomerDiscount(setting.getCustomerDiscount()*100);
				affiliateSettingCustomDTO.setPhoneCashDiscount(setting.getPhoneCashDiscount()*100);
				affiliateSettingCustomDTO.setPhoneCustomerDiscount(setting.getPhoneCustomerDiscount()*100);
				affiliateSettingCustomDTO.setIsRepeatBusiness(setting.getIsRepeatBusiness());
				affiliateSettingCustomDTO.setIsEarnRewardPoints(setting.getIsEarnRewardPoints());
				affiliateSettingCustomDTO.setLastUpdated(setting.getLastUpdated());
				affiliateSettingCustomDTO.setStatus(setting.getStatus());
				affiliateSettingCustomDTO.setUpdatedBy(setting.getUpdatedBy());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return affiliateSettingCustomDTO;
	}
	
	public static List<CrownJewelCustomerOrderDTO> getCrownJewelOrderArray(List<CrownJewelCustomerOrder> list){
		List<CrownJewelCustomerOrderDTO> CrownJewelCustomerOrderDTOList = new ArrayList<CrownJewelCustomerOrderDTO>();
		try {
			if(list!=null){
				CrownJewelCustomerOrderDTO crownJewelCustomerOrderDTO = null;			
				for(CrownJewelCustomerOrder order : list){
					crownJewelCustomerOrderDTO = new CrownJewelCustomerOrderDTO();
					crownJewelCustomerOrderDTO.setId(order.getId());
					crownJewelCustomerOrderDTO.setEventName(order.getFantasyEventName());
					crownJewelCustomerOrderDTO.setTeamId(order.getTeamId());
					crownJewelCustomerOrderDTO.setTeamName(order.getTeamName());
					if(order.getIsPackageSelected()){
						crownJewelCustomerOrderDTO.setIsPackageSelected("Yes");
					}else{
						crownJewelCustomerOrderDTO.setIsPackageSelected("No");
					}					
					crownJewelCustomerOrderDTO.setPackageCost(order.getPackageCost());
					crownJewelCustomerOrderDTO.setPackageNote(order.getPackageNote());
					crownJewelCustomerOrderDTO.setEventId(order.getEventId());
					if(order.getIsRealTicket()){
						crownJewelCustomerOrderDTO.setIsRealTicket("Yes");
					}else{
						crownJewelCustomerOrderDTO.setIsRealTicket("No");
					}
					crownJewelCustomerOrderDTO.setTicketId(order.getTicketId());
					crownJewelCustomerOrderDTO.setZone(order.getTicketSelection());
					crownJewelCustomerOrderDTO.setTicketQty(order.getTicketQty());
					crownJewelCustomerOrderDTO.setTicketPrice(order.getTicketPrice());
					crownJewelCustomerOrderDTO.setRequirePoints(order.getRequiredPoints());
					crownJewelCustomerOrderDTO.setCustomerId(order.getCustomerId());
					crownJewelCustomerOrderDTO.setCjStatus(order.getStatus());
					crownJewelCustomerOrderDTO.setCreatedDateStr(order.getCreatedDateStr());
					crownJewelCustomerOrderDTO.setLastUpdatedStr(order.getLastUpdatedStr());
					crownJewelCustomerOrderDTO.setAcceptRejectReason(order.getAcceptRejectReson()!=null?order.getAcceptRejectReson():"");
					crownJewelCustomerOrderDTO.setRegularOrderId(order.getRegularOrderId());
					crownJewelCustomerOrderDTO.setTeamZoneId(order.getTeamZoneId());
					crownJewelCustomerOrderDTO.setVenue(order.getVenueName());
					crownJewelCustomerOrderDTO.setCity(order.getVenueCity());
					crownJewelCustomerOrderDTO.setState(order.getVenueState());
					crownJewelCustomerOrderDTO.setCountry(order.getVenueCountry());
					crownJewelCustomerOrderDTO.setPlatform(order.getPlatform());
					crownJewelCustomerOrderDTO.setGrandChildCategoryName(order.getGrandChildName());
					crownJewelCustomerOrderDTO.setCustomerName(order.getCustomerName());
					CrownJewelCustomerOrderDTOList.add(crownJewelCustomerOrderDTO);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return CrownJewelCustomerOrderDTOList;
	}	

	public static List<CustomersCustomDTO> getCustomerArray(Collection<Customers> list){
		CustomersCustomDTO customerCustomDTO = null;
		List<CustomersCustomDTO> customerCustomDTOs = new ArrayList<CustomersCustomDTO>();		
		try {
			if(list!=null){
				for(Customers cust :list){
					customerCustomDTO = new CustomersCustomDTO();
					customerCustomDTO.setCustomerId(cust.getCustomerId());
					customerCustomDTO.setCustomerType(cust.getCustomerType());
					customerCustomDTO.setProductType(cust.getProductType());
					customerCustomDTO.setSignupType(cust.getSignupType());
					customerCustomDTO.setCustomerStatus(cust.getCustomerStatus());
					customerCustomDTO.setCompanyName(cust.getCompanyName());
					customerCustomDTO.setCustomerName(cust.getCustomerName());
					customerCustomDTO.setLastName(cust.getLastName());
					customerCustomDTO.setUserId(cust.getUserId());
					customerCustomDTO.setCustomerEmail(cust.getCustomerEmail());
					customerCustomDTO.setAddressLine1(cust.getAddressLine1());
					customerCustomDTO.setAddressLine2(cust.getAddressLine2());
					customerCustomDTO.setCity(cust.getCity());
					customerCustomDTO.setBillingAddressId(cust.getBillingAddressId());
					customerCustomDTO.setState(cust.getState());
					customerCustomDTO.setCountry(cust.getCountry());
					customerCustomDTO.setZipCode(cust.getZipCode());
					customerCustomDTO.setClient(cust.getIsClient()!=null?(cust.getIsClient()==true?"Yes":"No"):"No");
					customerCustomDTO.setBroker(cust.getIsBroker()!=null?(cust.getIsBroker()==true?"Yes":"No"):"No");
					customerCustomDTO.setPhone(cust.getPhone());
					customerCustomDTO.setIsBlocked(cust.getIsBlocked());
					customerCustomDTO.setReferrerCode(cust.getReferrerCode());
					customerCustomDTO.setRewardPoints(cust.getRewardPoints());
					if(cust.getIsMailSent()==null || cust.getIsMailSent() == false){
						customerCustomDTO.setEmailSent("No");
					}else{
						customerCustomDTO.setEmailSent("Yes");
					}					
					if(cust.getIsRetailCust()==null || cust.getIsRetailCust() == false){
						customerCustomDTO.setRetailCust("No");
					}else{
						customerCustomDTO.setRetailCust("Yes");
					}
					customerCustomDTOs.add(customerCustomDTO);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return customerCustomDTOs;
	}
	
	public static List<CountryDTO> getCountryArray(List<Country> list){
		CountryDTO countryDTO = null;
		List<CountryDTO> countryDTOs = new ArrayList<CountryDTO>();		
		try {
			if(list!=null){
				for(Country country :list){
					countryDTO = new CountryDTO();
					countryDTO.setId(country.getId());
					countryDTO.setName(country.getName());
					countryDTO.setDesc(country.getDesc());
					countryDTOs.add(countryDTO);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return countryDTOs;
	}

	public static List<StateDTO> getStateArray(List<State> list){
		StateDTO stateDTO = null;
		List<StateDTO> stateDTOs = new ArrayList<StateDTO>();		
		try {
			if(list!=null){
				for(State state :list){
					stateDTO = new StateDTO();
					stateDTO.setId(state.getId());
					stateDTO.setName(state.getName());
					stateDTO.setShortDesc(state.getShortDesc());
					stateDTOs.add(stateDTO);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return stateDTOs;
	}
	
	public static List<CustomerFavouriteEventDTO> getCustomerFavouriteEventArray(Collection<EventDetails> list){
		CustomerFavouriteEventDTO customerFavouriteEventDTO = null;
		List<CustomerFavouriteEventDTO> customerFavouriteEventDTOs = new ArrayList<CustomerFavouriteEventDTO>();		
		try {
			if(list!=null){
				for(EventDetails event :list){
					customerFavouriteEventDTO = new CustomerFavouriteEventDTO();
					customerFavouriteEventDTO.setEventId(event.getEventId());
					customerFavouriteEventDTO.setEventName(event.getEventName());
					customerFavouriteEventDTO.setEventDateStr(event.getEventDateStr());
					customerFavouriteEventDTO.setEventTimeStr(event.getEventTimeStr());
					customerFavouriteEventDTO.setDayOfWeek(event.getDayOfWeek());
					customerFavouriteEventDTO.setBuilding(event.getBuilding());
					customerFavouriteEventDTO.setVenueId(event.getVenueId());
					customerFavouriteEventDTO.setNoOfTixCount(event.getNoOfTixCount()==null?0:event.getNoOfTixCount());
					customerFavouriteEventDTO.setNoOfTixSoldCount(event.getNoOfTixSoldCount()==null?0:event.getNoOfTixSoldCount());
					customerFavouriteEventDTO.setCity(event.getCity());
					customerFavouriteEventDTO.setState(event.getState());
					customerFavouriteEventDTO.setCountry(event.getCountry());
					customerFavouriteEventDTO.setGrandChildCategoryName(event.getGrandChildCategoryName());
					customerFavouriteEventDTO.setChildCategoryName(event.getChildCategoryName());
					customerFavouriteEventDTO.setParentCategoryName(event.getParentCategoryName());
					customerFavouriteEventDTO.setEventCreationStr(event.getEventCreationStr());
					customerFavouriteEventDTO.setEventUpdatedStr(event.getEventUpdatedStr());
					customerFavouriteEventDTO.setNotes(event.getNotes());
					customerFavouriteEventDTOs.add(customerFavouriteEventDTO);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return customerFavouriteEventDTOs;
	}
	
	public static List<CustomerRewardPointsDTO> getCustomerRewardPointArray(List<RewardDetails> list){
		CustomerRewardPointsDTO customerRewardPointsDTO = null;
		List<CustomerRewardPointsDTO> customerRewardPointsDTOs = new ArrayList<CustomerRewardPointsDTO>();
		try{
			if(list != null){
				for(RewardDetails rewardDetail : list){
					customerRewardPointsDTO = new CustomerRewardPointsDTO();
					customerRewardPointsDTO.setOrderNo(rewardDetail.getOrderNo());
					customerRewardPointsDTO.setRewardPoint(String.format("%.2f", rewardDetail.getRewards()));
					customerRewardPointsDTO.setOrderDate(rewardDetail.getOrderDateStr());					
					customerRewardPointsDTO.setOrderTotal(String.format("%.2f", rewardDetail.getOrderTotal()));
					customerRewardPointsDTO.setOrderType(rewardDetail.getOrderType().toString());
					customerRewardPointsDTO.setEventName(rewardDetail.getEventName());
					customerRewardPointsDTO.setEventDate(rewardDetail.getEventDateStr());
					customerRewardPointsDTO.setEventTime(rewardDetail.getEventTimeStr());
					customerRewardPointsDTOs.add(customerRewardPointsDTO);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return customerRewardPointsDTOs;
	}
	
	public static List<CustomerPromotionalOfferDTO> getCustomerPromotionalOfferArray(List<RTFCustomerPromotionalOffer> list){
		CustomerPromotionalOfferDTO customerPromotionalOfferDTO = null;
		List<CustomerPromotionalOfferDTO> customerPromotionalOfferDTOs = new ArrayList<CustomerPromotionalOfferDTO>();
		try{
			if(list != null){
				for(RTFCustomerPromotionalOffer promoOffer : list){
					customerPromotionalOfferDTO = new CustomerPromotionalOfferDTO();
					customerPromotionalOfferDTO.setId(promoOffer.getId());
					customerPromotionalOfferDTO.setCustomerId(promoOffer.getCustomerId());
					customerPromotionalOfferDTO.setFirstName(promoOffer.getFirstName());
					customerPromotionalOfferDTO.setLastName(promoOffer.getLastName());
					customerPromotionalOfferDTO.setEmail(promoOffer.getEmail());
					customerPromotionalOfferDTO.setPromoCode(promoOffer.getPromoCode());
					customerPromotionalOfferDTO.setDiscount(promoOffer.getDiscountStr());
					customerPromotionalOfferDTO.setStartDate(promoOffer.getStartDateStr());
					customerPromotionalOfferDTO.setEndDate(promoOffer.getEndDateStr());					
					customerPromotionalOfferDTO.setStatus(promoOffer.getStatus());
					customerPromotionalOfferDTO.setCreatedDate(promoOffer.getCreatedDateStr());
					customerPromotionalOfferDTO.setModifiedDate(promoOffer.getModifiedDateStr());
					customerPromotionalOfferDTOs.add(customerPromotionalOfferDTO);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return customerPromotionalOfferDTOs;
	}
	
	public static List<CustomerPromotionalOfferTrackingDTO> getCustomerPromotionalOfferTrackingArray(List<RTFPromotionalOfferTracking> list){
		CustomerPromotionalOfferTrackingDTO customerPromotionalOfferTrackingDTO = null;
		List<CustomerPromotionalOfferTrackingDTO> customerPromotionalOfferTrackingDTOs = new ArrayList<CustomerPromotionalOfferTrackingDTO>();
		try{
			if(list != null){
				for(RTFPromotionalOfferTracking promoOffer : list){
					customerPromotionalOfferTrackingDTO = new CustomerPromotionalOfferTrackingDTO();
					customerPromotionalOfferTrackingDTO.setId(promoOffer.getId());
					customerPromotionalOfferTrackingDTO.setCustomerId(promoOffer.getCustomerId());
					customerPromotionalOfferTrackingDTO.setFirstName(promoOffer.getFirstName());
					customerPromotionalOfferTrackingDTO.setLastName(promoOffer.getLastName());
					customerPromotionalOfferTrackingDTO.setEmail(promoOffer.getEmail());
					customerPromotionalOfferTrackingDTO.setPromoCode(promoOffer.getPromoCode());
					customerPromotionalOfferTrackingDTO.setDiscount(promoOffer.getDiscount());
					customerPromotionalOfferTrackingDTO.setOrderId(promoOffer.getOrderId());
					customerPromotionalOfferTrackingDTO.setOrderTotal(String.format("%.2f", promoOffer.getOrderTotal()));
					customerPromotionalOfferTrackingDTO.setQuantity(promoOffer.getQuantity());
					customerPromotionalOfferTrackingDTO.setIpAddress(promoOffer.getIpAddress()!=null?promoOffer.getIpAddress():"");
					if(promoOffer.getPrimaryPaymentMethod() == null || promoOffer.getPrimaryPaymentMethod().equalsIgnoreCase("NULL")){
						customerPromotionalOfferTrackingDTO.setPrimaryPaymentMethod("");
					}else{
						customerPromotionalOfferTrackingDTO.setPrimaryPaymentMethod(promoOffer.getPrimaryPaymentMethod());
					}
					if(promoOffer.getSecondaryPaymentMethod() == null || promoOffer.getSecondaryPaymentMethod().equalsIgnoreCase("NULL")){
						customerPromotionalOfferTrackingDTO.setSecondaryPaymentMethod("");
					}else{
						customerPromotionalOfferTrackingDTO.setSecondaryPaymentMethod(promoOffer.getSecondaryPaymentMethod());
					}
					if(promoOffer.getThirdPaymentMethod() == null || promoOffer.getThirdPaymentMethod().equalsIgnoreCase("NULL")){
						customerPromotionalOfferTrackingDTO.setThirdPaymentMethod("");
					}else{
						customerPromotionalOfferTrackingDTO.setThirdPaymentMethod(promoOffer.getThirdPaymentMethod());
					}
					customerPromotionalOfferTrackingDTO.setPlatform(promoOffer.getPlatForm()!=null?promoOffer.getPlatForm().toString():"");
					customerPromotionalOfferTrackingDTO.setStatus(promoOffer.getStatus());
					customerPromotionalOfferTrackingDTO.setCreatedDate(promoOffer.getCreatedDateStr());
					customerPromotionalOfferTrackingDTOs.add(customerPromotionalOfferTrackingDTO);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return customerPromotionalOfferTrackingDTOs;
	}
	
	public static CustomerPromotionalOfferTrackingDetailsDTO getCustomerPromotionalOfferTrackingSummary(RTFPromotionalOfferTracking offerTracking, Customer customer, CustomerOrder customerOrder){
		CustomerPromotionalOfferTrackingDetailsDTO cusPromoOfferTrackingDetailsDTO = null;
		State state = null;
		Integer countryId = 0;
		try{
			if(offerTracking != null){				
				cusPromoOfferTrackingDetailsDTO = new CustomerPromotionalOfferTrackingDetailsDTO();
				cusPromoOfferTrackingDetailsDTO.setOrderId(offerTracking.getOrderId());
				cusPromoOfferTrackingDetailsDTO.setPromoCode(offerTracking.getPromoCode());
				cusPromoOfferTrackingDetailsDTO.setPlatform(offerTracking.getPlatForm().toString());
				cusPromoOfferTrackingDetailsDTO.setOrderTotal(customerOrder!=null?String.format("%.2f", customerOrder.getOrderTotal()):String.format("%.2f", 0.00));
				cusPromoOfferTrackingDetailsDTO.setOrderQuantity(customerOrder!=null?customerOrder.getQty():0);
				cusPromoOfferTrackingDetailsDTO.setEventName(customerOrder!=null?customerOrder.getEventName():"");
				cusPromoOfferTrackingDetailsDTO.setEventDate(customerOrder!=null?customerOrder.getEventDateStr():"");
				cusPromoOfferTrackingDetailsDTO.setEventTime(customerOrder!=null?customerOrder.getEventTimeStr():"");
				cusPromoOfferTrackingDetailsDTO.setVenue(customerOrder!=null?customerOrder.getVenueName():"");
				cusPromoOfferTrackingDetailsDTO.setVenueCity(customerOrder!=null?customerOrder.getVenueCity():"");
				if(customerOrder != null && customerOrder.getVenueState() != null){
		    		countryId = DAORegistry.getQueryManagerDAO().getCountryByShortDesc(customerOrder.getVenueCountry());
		    		state = DAORegistry.getStateDAO().getStateByShortDesc(customerOrder.getVenueState(), countryId);
		    		cusPromoOfferTrackingDetailsDTO.setVenueState(state.getName());
		    	}else{
		    		cusPromoOfferTrackingDetailsDTO.setVenueState("");
		    	}
				cusPromoOfferTrackingDetailsDTO.setFirstName(customer!=null?customer.getCustomerName():"");
				cusPromoOfferTrackingDetailsDTO.setLastName(customer!=null?customer.getLastName():"");
				cusPromoOfferTrackingDetailsDTO.setEmail(customer!=null?customer.getEmail():"");
				cusPromoOfferTrackingDetailsDTO.setReferrerCode(customer!=null?customer.getReferrerCode():"");
				cusPromoOfferTrackingDetailsDTO.setSignUpDate(customer!=null?customer.getSignupDateStr():"");
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return cusPromoOfferTrackingDetailsDTO;
	}
	
	public static List<DiscountCodeTrackingDTO> getDiscountCodeTrackingArray(List<DiscountCodeTracking> list){
		DiscountCodeTrackingDTO discountCodeTrackingDTO = null;
		List<DiscountCodeTrackingDTO> discountCodeTrackingDTOs = new ArrayList<DiscountCodeTrackingDTO>();
		try{
			if(list != null){
				for(DiscountCodeTracking discountCodeTracking : list){
					discountCodeTrackingDTO = new DiscountCodeTrackingDTO();
					discountCodeTrackingDTO.setCustomerId(discountCodeTracking.getCustomerId());
					discountCodeTrackingDTO.setFirstName(discountCodeTracking.getFirstName());
					discountCodeTrackingDTO.setLastName(discountCodeTracking.getLastName());
					discountCodeTrackingDTO.setEmail(discountCodeTracking.getEmail());
					discountCodeTrackingDTO.setReferrerCode(discountCodeTracking.getReferrerCode());
					discountCodeTrackingDTO.setFaceBookCnt(discountCodeTracking.getFaceBookCnt());
					discountCodeTrackingDTO.setTwitterCnt(discountCodeTracking.getTwitterCnt());
					discountCodeTrackingDTO.setLinkedinCnt(discountCodeTracking.getLinkedinCnt());
					discountCodeTrackingDTO.setWhatsappCnt(discountCodeTracking.getWhatsappCnt());					
					discountCodeTrackingDTO.setAndroidCnt(discountCodeTracking.getAndroidCnt());
					discountCodeTrackingDTO.setImessageCnt(discountCodeTracking.getImessageCnt());
					discountCodeTrackingDTO.setGoogleCnt(discountCodeTracking.getGoogleCnt());
					discountCodeTrackingDTO.setOutlookCnt(discountCodeTracking.getOutlookCnt());
					discountCodeTrackingDTOs.add(discountCodeTrackingDTO);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return discountCodeTrackingDTOs;
	}
	
	public static List<PromoOfferDTO> getPromotionalCodeArray(List<RTFPromoOffers> list){
		PromoOfferDTO promoOfferDTO = null;
		List<PromoOfferDTO> promoOfferDTOs = new ArrayList<PromoOfferDTO>();
		try{
			if(list != null){
				for(RTFPromoOffers promoCode : list){
					promoOfferDTO = new PromoOfferDTO();
					promoOfferDTO.setId(promoCode.getId());
					promoOfferDTO.setPromocode(promoCode.getPromoCode());
					promoOfferDTO.setDiscountPerc(promoCode.getDiscountPer());
					promoOfferDTO.setMobileDiscountPerc(promoCode.getMobileDiscountPerc());
					promoOfferDTO.setStartDate(promoCode.getStartDateStr());
					promoOfferDTO.setEndDate(promoCode.getEndDateStr());
					promoOfferDTO.setMaxOrder(promoCode.getMaxOrders());
					promoOfferDTO.setOrders(promoCode.getNoOfOrders());
					if(promoCode.getPromoType() != null && promoCode.getPromoType().equalsIgnoreCase("GRANDCHILD")){
						promoOfferDTO.setPromoType("GRAND");
					}else{
						promoOfferDTO.setPromoType(promoCode.getPromoType());
					}
					promoOfferDTO.setArtist(promoCode.getArtistName());
					promoOfferDTO.setVenue(promoCode.getVenueName());
					promoOfferDTO.setParent(promoCode.getParentCategory());
					promoOfferDTO.setChild(promoCode.getChildCategory());
					promoOfferDTO.setGrandChild(promoCode.getGrandChildCategory());
					promoOfferDTO.setArtistId(promoCode.getArtistId());
					promoOfferDTO.setVenueId(promoCode.getVenueId());
					promoOfferDTO.setParentId(promoCode.getParentId());
					promoOfferDTO.setChildId(promoCode.getChildId());
					promoOfferDTO.setGrandChildId(promoCode.getGrandChildId());
					promoOfferDTO.setEventId(promoCode.getEventId());
					promoOfferDTO.setEvent(promoCode.getEventName());
					promoOfferDTO.setStatus(promoCode.getStatus());
					promoOfferDTO.setCreatedBy(promoCode.getCreatedBy());
					promoOfferDTO.setCreatedDate(promoCode.getCreatedDateStr());
					promoOfferDTO.setModifiedBy(promoCode.getModifiedBy());
					promoOfferDTO.setModifiedDate(promoCode.getModifiedDateStr());
					String startDate[] = promoCode.getStartDateStr().split(" ");
					promoOfferDTO.setFromDate(startDate[0]);
					String endDate[] = promoCode.getEndDateStr().split(" ");
					promoOfferDTO.setToDate(endDate[0]);
					promoOfferDTO.setOrderThreshold((promoCode.getFlatOfferOrderThreshold()!=null?promoCode.getFlatOfferOrderThreshold().toString():""));
					if(promoCode.getIsFlatDiscount() != null && promoCode.getIsFlatDiscount()){
						promoOfferDTO.setIsFlatDiscount("Yes");
					}else{
						promoOfferDTO.setIsFlatDiscount("No");
					}
					promoOfferDTO.setIsAutoGenerate("No"); 
					promoOfferDTOs.add(promoOfferDTO);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return promoOfferDTOs;
	}
	

	public static List<ChildCategoryDTO> getChildCategoryArray(List<ChildCategory> list){
		ChildCategoryDTO childCategoryDTO = null;
		List<ChildCategoryDTO> childCategoryDTOs = new ArrayList<ChildCategoryDTO>();		
		try {
			if(list!=null){
				for(ChildCategory child :list){
					childCategoryDTO = new ChildCategoryDTO();
					childCategoryDTO.setId(child.getId());
					childCategoryDTO.setName(child.getName());
					childCategoryDTO.setTnId(child.getTnId());
					childCategoryDTOs.add(childCategoryDTO);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return childCategoryDTOs;
	}
	
	public static List<GrandChildCategoryDTO> getGrandChildCategoryArray(List<GrandChildCategory> list){
		GrandChildCategoryDTO grandChildCategoryDTO = null;
		List<GrandChildCategoryDTO> grandChildCategoryDTOs = new ArrayList<GrandChildCategoryDTO>();		
		try {
			if(list!=null){
				for(GrandChildCategory grandChild :list){
					grandChildCategoryDTO = new GrandChildCategoryDTO();
					grandChildCategoryDTO.setId(grandChild.getId());
					grandChildCategoryDTO.setName(grandChild.getName());
					grandChildCategoryDTO.setTnId(grandChild.getTnId());
					grandChildCategoryDTO.setTnName(grandChild.getTnName());
					grandChildCategoryDTOs.add(grandChildCategoryDTO);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return grandChildCategoryDTOs;
	}
	
	public static List<EmailTemplateDTO> getEmailTemplateArray(Collection<EmailTemplate> list){
		EmailTemplateDTO emailTemplateDTO = null;
		List<EmailTemplateDTO> emailTemplateDTOs = new ArrayList<EmailTemplateDTO>();		
		try {
			if(list!=null){
				for(EmailTemplate template :list){
					emailTemplateDTO = new EmailTemplateDTO();
					emailTemplateDTO.setId(template.getId());
					emailTemplateDTO.setName(template.getName());
					emailTemplateDTO.setBody(template.getBody());
					emailTemplateDTOs.add(emailTemplateDTO);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return emailTemplateDTOs;
	}
	
	public static CityStateCountryDTO getCityStateCountryValues(CityAutoSearch cityStateCountry){
		CityStateCountryDTO cityStateCountryDTO = null;				
		try {
			if(cityStateCountry!=null){				
				cityStateCountryDTO = new CityStateCountryDTO();
				cityStateCountryDTO.setId(cityStateCountry.getId());
				cityStateCountryDTO.setCity(cityStateCountry.getCity());
				cityStateCountryDTO.setStateId(cityStateCountry.getStateId());
				cityStateCountryDTO.setState(cityStateCountry.getState());
				cityStateCountryDTO.setStateName(cityStateCountry.getStateName());
				cityStateCountryDTO.setCountryId(cityStateCountry.getCountryId());
				cityStateCountryDTO.setCountry(cityStateCountry.getCountry());
				cityStateCountryDTO.setCountryName(cityStateCountry.getCountryName());					
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return cityStateCountryDTO;
	}
	
	public static List<EventDetailsDTO> getEventDetailsArray(Collection<EventDetails> list){
		EventDetailsDTO eventDetailsDTO = null;
		List<EventDetailsDTO> eventDetailsDTOs = new ArrayList<EventDetailsDTO>();		
		try {
			if(list!=null){
				for(EventDetails eventDetails :list){
					eventDetailsDTO = new EventDetailsDTO();
					eventDetailsDTO.setEventId(eventDetails.getEventId());
					eventDetailsDTO.setEventName(eventDetails.getEventName());
					eventDetailsDTO.setEventDateStr(eventDetails.getEventDateStr());
					eventDetailsDTO.setEventTimeStr(eventDetails.getEventTimeStr());
					eventDetailsDTO.setVenue(eventDetails.getBuilding());
					eventDetailsDTOs.add(eventDetailsDTO);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return eventDetailsDTOs;
	}
	
	public static List<OpenOrdersDTO> getOpenOrdersArray(Collection<OpenOrderStatus> list){
		OpenOrdersDTO openOrdersDTO = null;
		List<OpenOrdersDTO> openOrdersDTOs = new ArrayList<OpenOrdersDTO>();		
		try {
			if(list!=null){
				for(OpenOrderStatus order :list){
					openOrdersDTO = new OpenOrdersDTO();
					openOrdersDTO.setId(order.getId());
					openOrdersDTO.setInvoiceNo(order.getInvoiceNo());
					if(order.getProductType().equalsIgnoreCase(ProductType.REWARDTHEFAN.toString())){
						openOrdersDTO.setOrderId(order.getOrderId());
					}else{
						openOrdersDTO.setOrderId(order.getTicketRequestId());
					}
					openOrdersDTO.setInvoiceDateStr(order.getInvoiceDateStr());
					openOrdersDTO.setEventName(order.getEventName());
					openOrdersDTO.setEventDateStr(order.getEventDateStr());
					openOrdersDTO.setEventTimeStr(order.getEventTimeStr());
					openOrdersDTO.setVenueName(order.getVenueName());
					openOrdersDTO.setVenueCity(order.getVenueCity());
					openOrdersDTO.setVenueState(order.getVenueState());
					openOrdersDTO.setVenueCountry(order.getVenueCountry());					
					openOrdersDTO.setProductType(order.getProductType() );
					openOrdersDTO.setInternalNotes(order.getInternalNotes());
					openOrdersDTO.setSoldQty(order.getSoldQty());
					openOrdersDTO.setCustomerName(order.getCustomerName());
					openOrdersDTO.setZone(order.getZone());
					openOrdersDTO.setZoneTixQty(order.getZoneTixQty());
					openOrdersDTO.setOrderType(order.getOrderType());
					openOrdersDTO.setZoneCheapestPrice(order.getZoneCheapestPrice()!=null?String.format("%.2f",order.getZoneCheapestPrice()):"-");
					openOrdersDTO.setZoneTixGroupCount(order.getZoneTixGroupCount());
					openOrdersDTO.setSection(order.getSection());
					openOrdersDTO.setRow(order.getRow());
					openOrdersDTO.setActualSoldPrice(order.getActualSoldPrice()!=null?String.format("%.2f",order.getActualSoldPrice()):"-");
					openOrdersDTO.setMarketPrice(order.getMarketPrice()!=null?String.format("%.2f",order.getMarketPrice()):"-");
					openOrdersDTO.setLastUpdatedPrice(order.getLastUpdatedPrice()!=null?String.format("%.2f",order.getLastUpdatedPrice()):"-");
					openOrdersDTO.setSectionTixQty(order.getSectionTixQty());
					openOrdersDTO.setEventTixQty(order.getEventTixQty());
					openOrdersDTO.setTotalActualSoldPrice(order.getTotalActualSoldPrice()!=null?String.format("%.2f",order.getTotalActualSoldPrice()):"-");
					openOrdersDTO.setTotalMarketPrice(order.getTotalMarketPrice()!=null?String.format("%.2f",order.getTotalMarketPrice()):"-");
					openOrdersDTO.setProfitAndLoss(order.getProfitAndLoss()!=null?String.format("%.2f",order.getProfitAndLoss()):"-");
					openOrdersDTO.setNetTotalSoldPrice(order.getNetTotalSoldPrice()!=null?String.format("%.2f",order.getNetTotalSoldPrice()):"-");
					openOrdersDTO.setNetSoldPrice(order.getNetSoldPrice()!=null?String.format("%.2f",order.getNetSoldPrice()):"-");
					openOrdersDTO.setSectionMargin(order.getSectionMargin()!=null?String.format("%.2f",order.getSectionMargin()):"-");
					openOrdersDTO.setZoneTotalPrice(order.getZoneTotalPrice()!=null?String.format("%.2f",order.getZoneTotalPrice()):"-");
					openOrdersDTO.setZoneProfitAndLoss(order.getZoneProfitAndLoss()!=null?String.format("%.2f",order.getZoneProfitAndLoss()):"-");
					openOrdersDTO.setZoneMargin(order.getZoneMargin()!=null?String.format("%.2f",order.getZoneMargin()):"-");
					openOrdersDTO.setPriceUpdateCount(order.getPriceUpdateCount());
					openOrdersDTO.setPrice(order.getPrice()!=null?String.format("%.2f",order.getPrice()):"-");
					openOrdersDTO.setDiscountCouponPrice(order.getDiscountCouponPrice()!=null?String.format("%.2f", order.getDiscountCouponPrice()):"-");
					openOrdersDTO.setUrl(order.getUrl());
					openOrdersDTO.setShippingMethod(order.getShippingMethod());
					openOrdersDTO.setTrackingNo(order.getTrackingNo());
					openOrdersDTO.setSecondaryOrderType(order.getSecondaryOrderType());
					openOrdersDTO.setPoId(order.getPoId());
					openOrdersDTO.setPoDateStr(order.getPoDateStr());
					openOrdersDTO.setSecondaryOrderId(order.getSecondaryOrderId() );
					openOrdersDTO.setLastUpdateStr(order.getLastUpdateStr());
					openOrdersDTO.setTmatEventId(order.getTmatEventId());
					openOrdersDTO.setBrokerId(order.getBrokerId()!=null ?order.getBrokerId().toString():"");
					openOrdersDTO.setCompanyName(order.getCompanyName()!=null ? order.getCompanyName(): "");
					openOrdersDTO.setPlatform(order.getPlatform());
					if(order.getPlatform() != null && order.getPlatform().equalsIgnoreCase("DESKTOP_SITE")){
						openOrdersDTO.setPlatform("WEB_ORDER");
					}else if(order.getPlatform() != null && order.getPlatform().equalsIgnoreCase("TICK_TRACKER")){
						openOrdersDTO.setPlatform("RETAIL_ORDER");
					}
					openOrdersDTOs.add(openOrdersDTO);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return openOrdersDTOs;
	}
	
	public static List<ManualFedexGenerationDTO> getManualFedexGenerationArray(List<ManualFedexGeneration> list){
		ManualFedexGenerationDTO manualFedexGenerationDTO = null;
		List<ManualFedexGenerationDTO> manualFedexGenerationDTOs = new ArrayList<ManualFedexGenerationDTO>();		
		try {
			if(list!=null){
				for(ManualFedexGeneration manualFedex :list){
					manualFedexGenerationDTO = new ManualFedexGenerationDTO();
					manualFedexGenerationDTO.setId(manualFedex.getId());
					manualFedexGenerationDTO.setBlFirstName(manualFedex.getBlFirstName());
					manualFedexGenerationDTO.setBlLastName(manualFedex.getBlLastName());
					manualFedexGenerationDTO.setBlCompanyName(manualFedex.getBlCompanyName());
					manualFedexGenerationDTO.setBlAddress1(manualFedex.getBlAddress1());
					manualFedexGenerationDTO.setBlAddress2(manualFedex.getBlAddress2());
					manualFedexGenerationDTO.setBlCity(manualFedex.getBlCity());
					manualFedexGenerationDTO.setBlState(manualFedex.getBlState());
					manualFedexGenerationDTO.setBlStateName(manualFedex.getBlStateName());					
					manualFedexGenerationDTO.setBlCountry(manualFedex.getBlCountry());
					manualFedexGenerationDTO.setBlCountryName(manualFedex.getBlCountryName());
					manualFedexGenerationDTO.setBlZipCode(manualFedex.getBlZipCode());
					manualFedexGenerationDTO.setBlPhone(manualFedex.getBlPhone());
					manualFedexGenerationDTO.setShCustomerName(manualFedex.getShCustomerName());
					manualFedexGenerationDTO.setShCompanyName(manualFedex.getShCompanyName());
					manualFedexGenerationDTO.setShAddress1(manualFedex.getShAddress1());
					manualFedexGenerationDTO.setShAddress2(manualFedex.getShAddress2());
					manualFedexGenerationDTO.setShCity(manualFedex.getShCity());
					manualFedexGenerationDTO.setShState(manualFedex.getShState());
					manualFedexGenerationDTO.setShStateName(manualFedex.getShStateName());					
					manualFedexGenerationDTO.setShCountry(manualFedex.getShCountry());
					manualFedexGenerationDTO.setShCountryName(manualFedex.getShCountryName());
					manualFedexGenerationDTO.setShZipCode(manualFedex.getShZipCode());
					manualFedexGenerationDTO.setShPhone(manualFedex.getShPhone());
					manualFedexGenerationDTO.setServiceType(manualFedex.getServiceType());
					manualFedexGenerationDTO.setSignatureType(manualFedex.getSignatureType());
					manualFedexGenerationDTO.setFedexLabelPath(manualFedex.getFedexLabelPath());
					manualFedexGenerationDTO.setTrackingNo(manualFedex.getTrackingNumber());
					if(manualFedex.getFedexLabelCreated()!= null && manualFedex.getFedexLabelCreated()){
						manualFedexGenerationDTO.setFedexLabelCreated("Yes");
					}else{
						manualFedexGenerationDTO.setFedexLabelCreated("No");
					}
					manualFedexGenerationDTO.setCreatedDate(manualFedex.getCreatedDateStr());
					manualFedexGenerationDTO.setCreatedBy(manualFedex.getCreatedBy());
					manualFedexGenerationDTOs.add(manualFedexGenerationDTO);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return manualFedexGenerationDTOs;
	}
	
	public static List<RelatedPODTO> getRelatedPOArray(List<PurchaseOrder> purchaseOrderList, List<POSPurchaseOrder> posPurchaseOrderList){
		List<RelatedPODTO> relatedPODTOs = new ArrayList<RelatedPODTO>();
		try {
			if(purchaseOrderList!=null){
				RelatedPODTO relatedPODTO = null;
				for(PurchaseOrder purchaseOrder : purchaseOrderList){
					relatedPODTO = new RelatedPODTO();
					relatedPODTO.setId(purchaseOrder.getId());
					relatedPODTO.setPoTotal(String.format("%.2f", purchaseOrder.getPoTotal()));
					relatedPODTO.setTotalQuantity(purchaseOrder.getTotalQuantity());					
					relatedPODTO.setUsedQunatity(purchaseOrder.getUsedQunatity());
					relatedPODTO.setCustomerName(purchaseOrder.getCustomerName());
					relatedPODTO.setCsr("");
					relatedPODTO.setCreateTimeStr(purchaseOrder.getCreateTimeStr());
					relatedPODTO.setInternalNotes(purchaseOrder.getInternalNotes());
					relatedPODTO.setExternalNotes(purchaseOrder.getExternalNotes());
					relatedPODTOs.add(relatedPODTO);
				}
			}
			if(posPurchaseOrderList!=null){
				RelatedPODTO relatedPODTO = null;
				for(POSPurchaseOrder purchaseOrder : posPurchaseOrderList){
					relatedPODTO = new RelatedPODTO();
					relatedPODTO.setId(purchaseOrder.getPurchaseOrderId());
					relatedPODTO.setPoTotal(String.format("%.2f", purchaseOrder.getPoTotal()));
					relatedPODTO.setTotalQuantity(purchaseOrder.getTotalQuantity());					
					relatedPODTO.setUsedQunatity(purchaseOrder.getUsedQunatity());
					relatedPODTO.setCustomerName(purchaseOrder.getCustomerName());
					relatedPODTO.setCsr("");
					relatedPODTO.setCreateTimeStr(purchaseOrder.getCreateTimeStr());
					relatedPODTO.setInternalNotes(purchaseOrder.getNotes());
					relatedPODTO.setExternalNotes(purchaseOrder.getDisplayedNotes());
					relatedPODTOs.add(relatedPODTO);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return relatedPODTOs;
	}
	
	public static List<RelatedInvoiceDTO> getRelatedInvoicesArray(List<Ticket> ticketList, List<POSTicket> posTicketList){
		List<RelatedInvoiceDTO> relatedInvoiceDTOs = new ArrayList<RelatedInvoiceDTO>();
		try {
			if(ticketList!=null){
				RelatedInvoiceDTO relatedInvoiceDTO = null;
				for(Ticket ticket : ticketList){
					relatedInvoiceDTO = new RelatedInvoiceDTO();
					relatedInvoiceDTO.setId(ticket.getId());
					relatedInvoiceDTO.setInvoiceId(ticket.getInvoiceId());
					relatedInvoiceDTO.setEventName(ticket.getEventNameString());					
					relatedInvoiceDTO.setSection(ticket.getSection());
					relatedInvoiceDTO.setRow(ticket.getRow());
					relatedInvoiceDTO.setSeat(ticket.getSeatNo());
					relatedInvoiceDTO.setRetailPrice(ticket.getRetailPrice());
					relatedInvoiceDTO.setFacePrice(ticket.getFacePrice());
					relatedInvoiceDTO.setCost(ticket.getCost());
					relatedInvoiceDTO.setWholesalePrice(ticket.getWholesaleSalePrice());
					relatedInvoiceDTO.setActualSoldPrice(ticket.getActualSoldPrice());
					relatedInvoiceDTO.setPurchasedBy("");
					relatedInvoiceDTOs.add(relatedInvoiceDTO);
				}
			}
			if(posTicketList!=null){
				RelatedInvoiceDTO relatedInvoiceDTO = null;
				for(POSTicket posTicket : posTicketList){
					relatedInvoiceDTO = new RelatedInvoiceDTO();
					relatedInvoiceDTO.setId(posTicket.getTicketId());
					relatedInvoiceDTO.setInvoiceId(posTicket.getInvoiceId());
					relatedInvoiceDTO.setEventName("");					
					relatedInvoiceDTO.setSection(posTicket.getSection());
					relatedInvoiceDTO.setRow(posTicket.getRow());
					relatedInvoiceDTO.setSeat(posTicket.getSeatNumber());
					relatedInvoiceDTO.setRetailPrice(posTicket.getRetailPrice());
					relatedInvoiceDTO.setFacePrice(posTicket.getFacePrice());
					relatedInvoiceDTO.setCost(posTicket.getCost());
					relatedInvoiceDTO.setWholesalePrice(posTicket.getWholesalePrice());
					relatedInvoiceDTO.setActualSoldPrice(posTicket.getActualSoldPrice());
					relatedInvoiceDTO.setPurchasedBy("");
					relatedInvoiceDTOs.add(relatedInvoiceDTO);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return relatedInvoiceDTOs;
	}
	
	public static List<PurchaseOrderDTO> getPuchaseOrderArray(Collection<PurchaseOrders> list){
		PurchaseOrderDTO purchaseOrderDTO = null;
		List<PurchaseOrderDTO> purchaseOrderDTOs = new ArrayList<PurchaseOrderDTO>();
		try {
			if(list!=null){				
				for (PurchaseOrders order : list) {
					purchaseOrderDTO = new PurchaseOrderDTO();
					purchaseOrderDTO.setId(order.getId());
					purchaseOrderDTO.setCustomerId(order.getCustomerId());
					purchaseOrderDTO.setCustomerName(order.getCustomerName());					
					purchaseOrderDTO.setCustomerType(order.getCustomerType());
					purchaseOrderDTO.setPoTotal(String.format("%.2f", order.getPoTotal()));
					purchaseOrderDTO.setTicketQty(order.getTicketQty());
					purchaseOrderDTO.setEventTicketQty(order.getEventTicketQty());
					purchaseOrderDTO.setEventTicketCost(order.getEventTicketCost()!=null?String.format("%.2f",order.getEventTicketCost()):"");
					purchaseOrderDTO.setCreatedDateStr(order.getCreatedDateStr());
					purchaseOrderDTO.setCreatedBy(order.getCreatedBy());
					purchaseOrderDTO.setShippingType(order.getShippingType());
					purchaseOrderDTO.setStatus(order.getStatus());
					purchaseOrderDTO.setProductType(order.getProductType());
					purchaseOrderDTO.setLastUpdatedStr(order.getLastUpdatedStr());
					purchaseOrderDTO.setPoAged(order.getPoAged());
					purchaseOrderDTO.setCsr(order.getCsr());
					purchaseOrderDTO.setConsignmentPoNo(order.getConsignmentPoNo());
					purchaseOrderDTO.setPurchaseOrderType(order.getPurchaseOrderType());
					purchaseOrderDTO.setTransactionOffice(order.getTransactionOffice());
					purchaseOrderDTO.setTrackingNo(order.getTrackingNo());
					purchaseOrderDTO.setShippingNotes(order.getShippingNotes());
					purchaseOrderDTO.setExternalNotes(order.getExternalNotes());
					purchaseOrderDTO.setInternalNotes(order.getInternalNotes());
					purchaseOrderDTO.setEventId(order.getEventId());
					purchaseOrderDTO.setEventName(order.getEventName());
					purchaseOrderDTO.setEventDateStr(order.getEventDateStr());
					purchaseOrderDTO.setEventTimeStr(order.getEventTimeStr());
					purchaseOrderDTO.setPosProductType(order.getPosProductType());
					purchaseOrderDTO.setPosPOId(order.getPosPOId());
					if(order.getIsEmailed()!= null && order.getIsEmailed()){
						purchaseOrderDTO.setIsEmailed("Yes");
					}else{
						purchaseOrderDTO.setIsEmailed("No");
					}
					if(order.getFedexLabelCreated()!= null && order.getFedexLabelCreated()){
						purchaseOrderDTO.setFedexLabelCreated("Yes");
					}else{
						purchaseOrderDTO.setFedexLabelCreated("No");
					}
					purchaseOrderDTO.setBrokerId(order.getBrokerId() != null ? order.getBrokerId().toString() : "");
					purchaseOrderDTOs.add(purchaseOrderDTO);
				}
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return purchaseOrderDTOs;
	}
	
	public static List<TrackerUserDTO> getTrackerUsersObject(Collection<TrackerUser> list){
		TrackerUserDTO trackerUserDTO = null;
		List<TrackerUserDTO> trackerUserDTOs = new ArrayList<TrackerUserDTO>(); 
		try {
			if(list!=null){			
				for(TrackerUser user : list){
					trackerUserDTO = new TrackerUserDTO();
					trackerUserDTO.setId(user.getId());
					trackerUserDTO.setName(user.getUserName());
					trackerUserDTOs.add(trackerUserDTO);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return trackerUserDTOs;
	}
	
	public static List<PaymentHistoryDTO> getPaymentHistoryArray(List<PaymentHistory> paymentHistoryList, List<PaymentHistory> rtwPaymentHistoryList){
		List<PaymentHistoryDTO> paymentHistoryDTOs = new ArrayList<PaymentHistoryDTO>();
		try {
			if(paymentHistoryList!=null){
				PaymentHistoryDTO paymentHistoryDTO = null;
				for(PaymentHistory paymentHistory : paymentHistoryList){
					paymentHistoryDTO = new PaymentHistoryDTO();
					paymentHistoryDTO.setPrimaryPaymentMethod(paymentHistory.getPrimaryPaymentMethod());
					paymentHistoryDTO.setSecondaryPaymentMethod(paymentHistory.getSecondaryPaymentMethod());
					paymentHistoryDTO.setThirdPaymentMethod(paymentHistory.getThirdPaymentMethod()!=null?paymentHistory.getThirdPaymentMethod():"");					
					paymentHistoryDTO.setPrimaryAmount(paymentHistory.getPrimaryAmount());
					paymentHistoryDTO.setSecondaryAmount(paymentHistory.getSecondaryAmount());
					paymentHistoryDTO.setThirdPayAmt(paymentHistory.getThirdPayAmt());
					paymentHistoryDTO.setPaidOnStr(paymentHistory.getPaidOnStr());
					paymentHistoryDTO.setSecondaryTransaction(paymentHistory.getSecondaryTransaction());
					paymentHistoryDTO.setPrimaryTransaction(paymentHistory.getPrimaryTransaction());
					paymentHistoryDTO.setThirdTransactionId(paymentHistory.getThirdTransactionId());
					paymentHistoryDTO.setCsr(paymentHistory.getCsr());
					paymentHistoryDTO.setCardType(paymentHistory.getCardType());
					paymentHistoryDTO.setCreditCardNumber(paymentHistory.getCreditCardNumber());
					paymentHistoryDTOs.add(paymentHistoryDTO);
				}
			}
			if(rtwPaymentHistoryList!=null){
				PaymentHistoryDTO paymentHistoryDTO = null;
				for(PaymentHistory paymentHistory : rtwPaymentHistoryList){
					paymentHistoryDTO = new PaymentHistoryDTO();
					paymentHistoryDTO.setCreditCardNumber(paymentHistory.getCreditCardNumber());
					paymentHistoryDTO.setCheckNumber(paymentHistory.getCheckNumber());
					paymentHistoryDTO.setAmount(paymentHistory.getAmount());					
					paymentHistoryDTO.setNote(paymentHistory.getNote());
					paymentHistoryDTO.setPaidOnStr(paymentHistory.getPaidOnStr());
					paymentHistoryDTO.setSystemUser(paymentHistory.getSystemUser());
					paymentHistoryDTO.setPaymentType(paymentHistory.getPaymentType());
					paymentHistoryDTO.setTransOffice(paymentHistory.getTransOffice());
					paymentHistoryDTOs.add(paymentHistoryDTO);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return paymentHistoryDTOs;
	}
	
	public static List<PurchaseOrderPaymentDetailsDTO> getPOPaymentHistoryArray(List<PurchaseOrderPaymentDetails> paymentHistoryList){
		PurchaseOrderPaymentDetailsDTO purchaseOrderPaymentDetailsDTO = null;
		List<PurchaseOrderPaymentDetailsDTO> purchaseOrderPaymentDetailsDTOs = new ArrayList<PurchaseOrderPaymentDetailsDTO>();
		try {
			if(paymentHistoryList!=null){
				for(PurchaseOrderPaymentDetails paymentHistory : paymentHistoryList){
					purchaseOrderPaymentDetailsDTO = new PurchaseOrderPaymentDetailsDTO();
					purchaseOrderPaymentDetailsDTO.setId(paymentHistory.getId());
					purchaseOrderPaymentDetailsDTO.setName(paymentHistory.getName());
					purchaseOrderPaymentDetailsDTO.setPaymentType(paymentHistory.getPaymentType());					
					purchaseOrderPaymentDetailsDTO.setCardType(paymentHistory.getCardType());
					purchaseOrderPaymentDetailsDTO.setCardLastFourDigit(paymentHistory.getCardLastFourDigit());
					purchaseOrderPaymentDetailsDTO.setRoutingLastFourDigit(paymentHistory.getRoutingLastFourDigit());
					purchaseOrderPaymentDetailsDTO.setAccountLastFourDigit(paymentHistory.getAccountLastFourDigit());
					purchaseOrderPaymentDetailsDTO.setChequeNo(paymentHistory.getChequeNo());
					purchaseOrderPaymentDetailsDTO.setPaymentStatus(paymentHistory.getPaymentStatus());
					purchaseOrderPaymentDetailsDTO.setAmount(paymentHistory.getAmount());
					purchaseOrderPaymentDetailsDTO.setPaymentDate(paymentHistory.getPaymentDateStr());
					purchaseOrderPaymentDetailsDTO.setPaymentNote(paymentHistory.getPaymentNote());
					purchaseOrderPaymentDetailsDTOs.add(purchaseOrderPaymentDetailsDTO);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return purchaseOrderPaymentDetailsDTOs;
	}
	
	public static List<TicketGroupDTO> getPOTicketGroupArray(Collection<TicketGroup> ticketGroupList){
		TicketGroupDTO ticketGroupDTO = null;
		List<TicketGroupDTO> ticketGroupDTOs = new ArrayList<TicketGroupDTO>();
		try {
			if(ticketGroupList!=null){
				for(TicketGroup ticketGroup : ticketGroupList){
					ticketGroupDTO = new TicketGroupDTO();
					ticketGroupDTO.setId(ticketGroup.getId());
					ticketGroupDTO.setEventName(ticketGroup.getEventName());
					ticketGroupDTO.setEventDate(ticketGroup.getEventDateStr());
					ticketGroupDTO.setVenue(ticketGroup.getVenue());
					ticketGroupDTO.setSection(ticketGroup.getSection());
					ticketGroupDTO.setRow(ticketGroup.getRow());
					ticketGroupDTO.setSeatLow(ticketGroup.getSeatLow());
					ticketGroupDTO.setSeatHigh(ticketGroup.getSeatHigh());
					ticketGroupDTO.setQuantity(ticketGroup.getQuantity());
					ticketGroupDTO.setPrice(ticketGroup.getPriceStr());					
					ticketGroupDTO.setOnHand(ticketGroup.getOnHand());
					ticketGroupDTO.setInvoiceId(ticketGroup.getInvoiceId());
					
					ticketGroupDTO.setPurchaseOrderId(ticketGroup.getPurchaseOrderId());
					ticketGroupDTO.setAvailableSeatLow(ticketGroup.getAvailableSeatLow());
					ticketGroupDTO.setAvailableSeatHigh(ticketGroup.getAvailableSeatHigh());
					ticketGroupDTO.setAvailableQty(ticketGroup.getAvailableQty());
					ticketGroupDTO.setPriceStr(ticketGroup.getPriceStr());
					ticketGroupDTO.setCreatedBy(ticketGroup.getCreatedBy());
					
					ticketGroupDTO.setTicketGroupId(ticketGroup.getId());
					ticketGroupDTO.setTicketGroupSection(ticketGroup.getSection());
					ticketGroupDTO.setTicketGroupRow(ticketGroup.getRow());
					ticketGroupDTO.setTicketGroupMappedSeatLow(ticketGroup.getMappedSeatLow());
					ticketGroupDTO.setTicketGroupMappedSeatHigh(ticketGroup.getMappedSeatHigh());
					ticketGroupDTO.setTicketGroupMappedQty(ticketGroup.getMappedQty());
					ticketGroupDTO.setTicketGroupPrice(ticketGroup.getPriceStr());
					
					ticketGroupDTOs.add(ticketGroupDTO);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ticketGroupDTOs;
	}
	
	public static List<CustomerAddressDTO> getShippingAddressArray(List<CustomerAddress> list){
		CustomerAddressDTO customerAddressDTO = null;
		List<CustomerAddressDTO> customerAddressDTOs = new ArrayList<CustomerAddressDTO>();
		try {
			if(list!=null){				
				for(CustomerAddress customer : list){
					customerAddressDTO = new CustomerAddressDTO();
					customerAddressDTO.setId(customer.getId());
					customerAddressDTO.setFirstName(customer.getFirstName());
					customerAddressDTO.setLastName(customer.getLastName());
					customerAddressDTO.setAddressLine1(customer.getAddressLine1());
					customerAddressDTO.setAddressLine2(customer.getAddressLine2());										
					customerAddressDTO.setCountry(customer.getCountry().getName());
					customerAddressDTO.setState(customer.getState().getName());
					customerAddressDTO.setCity(customer.getCity());
					customerAddressDTO.setZipCode(customer.getZipCode());
					
					customerAddressDTO.setShId(customer.getId());
					customerAddressDTO.setStreet1(customer.getAddressLine1());
					customerAddressDTO.setStreet2(customer.getAddressLine2());
					customerAddressDTO.setPhone(customer.getPhone1());
					customerAddressDTO.setEmail(customer.getEmail());
					
					customerAddressDTOs.add(customerAddressDTO);
				}
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return customerAddressDTOs;
	}
	
	public static List<TicketGroupDTO> getPOUploadTicketGroupArray(Collection<TicketGroup> ticketGroupList){
		TicketGroupDTO ticketGroupDTO = null;
		List<TicketGroupDTO> ticketGroupDTOs = new ArrayList<TicketGroupDTO>();
		try {
			if(ticketGroupList!=null){
				for(TicketGroup ticketGroup : ticketGroupList){
					ticketGroupDTO = new TicketGroupDTO();
					ticketGroupDTO.setTgId(ticketGroup.getId());
					ticketGroupDTO.setTgSection(ticketGroup.getSection());
					ticketGroupDTO.setTgRow(ticketGroup.getRow());
					ticketGroupDTO.setTgSeatLow(ticketGroup.getSeatLow());
					ticketGroupDTO.setTgSeatHigh(ticketGroup.getSeatHigh());
					ticketGroupDTO.setTgQuantity(ticketGroup.getQuantity());
					ticketGroupDTO.setTgPrice(ticketGroup.getPriceStr());
					ticketGroupDTO.setTgEventId(ticketGroup.getEventId());
					ticketGroupDTO.setTgEventName(ticketGroup.getEventName());
					ticketGroupDTO.setTgEventDate(ticketGroup.getEventDateStr());
					ticketGroupDTO.setTgEventTime(ticketGroup.getEventTimeStr());
					if(ticketGroup.getInstantDownload() != null && ticketGroup.getInstantDownload()){
						ticketGroupDTO.setTgInstantDownload("Yes");
					}else{
						ticketGroupDTO.setTgInstantDownload("No");
					}
					ticketGroupDTO.setTgTicketAttachment(ticketGroup.getTicketGroupTicketAttachment());
					if(ticketGroup.getTicketGroupTicketAttachment() != null){
						TicketGroupTicketAttachment tgTktAtt = ticketGroup.getTicketGroupTicketAttachment();
						ticketGroupDTO.setTgrpId(tgTktAtt.getTicketGroupId());
						ticketGroupDTO.setTgrpType(tgTktAtt.getType().toString());
						ticketGroupDTO.setTgrpPosition(tgTktAtt.getPosition().toString());
						ticketGroupDTO.setTgrpFilePath(tgTktAtt.getFilePath());
						ticketGroupDTO.setTgrpFileName(tgTktAtt.getFileName());
					}
					ticketGroupDTOs.add(ticketGroupDTO);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ticketGroupDTOs;
	}
	
	public static PurchaseOrderDTO getPurchaseOrderObject(PurchaseOrder poInfo, Collection<ShippingMethod> shippingMethods){
		PurchaseOrderDTO purchaseOrderDTO = null;		
		try {
			if(poInfo!=null){
				purchaseOrderDTO = new PurchaseOrderDTO();
				purchaseOrderDTO.setPoId(poInfo.getId());
				purchaseOrderDTO.setPoTotal(poInfo.getPoTotal().toString());
				purchaseOrderDTO.setPoCsr(poInfo.getCsr());
				purchaseOrderDTO.setPoTicketCount(poInfo.getTicketCount());
				purchaseOrderDTO.setPoCreateTime(poInfo.getCreateTimeStr());
				purchaseOrderDTO.setPoCustomerId(poInfo.getCustomerId());
				
				ShippingMethod shippingMethod = DAORegistry.getShippingMethodDAO().getShippingMethodById(poInfo.getShippingMethodId());
				if(shippingMethod!=null){
					purchaseOrderDTO.setPoShippingMethod(shippingMethod.getName());
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return purchaseOrderDTO;
	}
	
	public static CustomersCustomDTO getPOCustomerObject(Customer customer){
		CustomersCustomDTO customerCustomDTO = null;		
		try {
			if(customer!=null){
				customerCustomDTO = new CustomersCustomDTO();
				customerCustomDTO.setCustomerId(customer.getId());
				customerCustomDTO.setCustomerName(customer.getCustomerName());
				customerCustomDTO.setLastName(customer.getLastName());
				customerCustomDTO.setCustomerEmail(customer.getEmail());
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return customerCustomDTO;
	}
	
	public static List<POShippingMethodDTO> getShippingMethodArray(Collection<ShippingMethod> list){
		POShippingMethodDTO poShippingMethodDTO = null;
		List<POShippingMethodDTO> poShippingMethodDTOs = new ArrayList<POShippingMethodDTO>();
		try {
			if(list!=null){				
				for(ShippingMethod shippingMethod : list){
					poShippingMethodDTO = new POShippingMethodDTO();
					poShippingMethodDTO.setShippingMethodId(shippingMethod.getId());
					poShippingMethodDTO.setShippingMethodName(shippingMethod.getName());
					poShippingMethodDTOs.add(poShippingMethodDTO);
				}
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return poShippingMethodDTOs;
	}
	
	public static PurchaseOrderPaymentDetailsDTO getPOPaymentHistoryObject(PurchaseOrderPaymentDetails paymentHistory){
		PurchaseOrderPaymentDetailsDTO purchaseOrderPaymentDetailsDTO = null;
		try {
			if(paymentHistory!=null){
				purchaseOrderPaymentDetailsDTO = new PurchaseOrderPaymentDetailsDTO();
				purchaseOrderPaymentDetailsDTO.setPaidName(paymentHistory.getName());
				purchaseOrderPaymentDetailsDTO.setPaymentType(paymentHistory.getPaymentType()!=null?paymentHistory.getPaymentType():"");					
				purchaseOrderPaymentDetailsDTO.setCardType(paymentHistory.getCardType());
				purchaseOrderPaymentDetailsDTO.setCardLastFourDigit(paymentHistory.getCardLastFourDigit());
				purchaseOrderPaymentDetailsDTO.setRoutingLastFourDigit(paymentHistory.getRoutingLastFourDigit());
				purchaseOrderPaymentDetailsDTO.setAccountLastFourDigit(paymentHistory.getAccountLastFourDigit());
				purchaseOrderPaymentDetailsDTO.setChequeNo(paymentHistory.getChequeNo());
				purchaseOrderPaymentDetailsDTO.setPaymentStatus(paymentHistory.getPaymentStatus());					
				purchaseOrderPaymentDetailsDTO.setPaymentDate(paymentHistory.getPaymentDateStr());
				purchaseOrderPaymentDetailsDTO.setPaymentNote(paymentHistory.getPaymentNote());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return purchaseOrderPaymentDetailsDTO;
	}
	
	public static PurchaseOrderDTO getPurchaseOrderObject(PurchaseOrder poInfo){
		PurchaseOrderDTO purchaseOrderDTO = null;		
		try {
			if(poInfo!=null){
				purchaseOrderDTO = new PurchaseOrderDTO();
				purchaseOrderDTO.setPoId(poInfo.getId());
				purchaseOrderDTO.setPoTotal(poInfo.getPoTotal().toString());
				purchaseOrderDTO.setPoTicketCount(poInfo.getTicketCount());
				purchaseOrderDTO.setPoCustomerId(poInfo.getCustomerId());
				purchaseOrderDTO.setPoTrackingNo(poInfo.getTrackingNo());
				purchaseOrderDTO.setPoExternalPONo(poInfo.getExternalPONo());
				purchaseOrderDTO.setPoConsignmentPONo(poInfo.getConsignmentPoNo());
				purchaseOrderDTO.setPoTransactionOffice(poInfo.getTransactionOffice());
				purchaseOrderDTO.setPoPurchaseOrderType(poInfo.getPurchaseOrderType());
				purchaseOrderDTO.setPoShippingMethod(poInfo.getShippingMethodId().toString());
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return purchaseOrderDTO;
	}
	
	public static CustomersCustomDTO getCustomerObject(Customer customerInfo){
		CustomersCustomDTO customersCustomDTO = null;		
		try {
			if(customerInfo!=null){
				customersCustomDTO = new CustomersCustomDTO();
				customersCustomDTO.setCustomerName(customerInfo.getCustomerName());
				customersCustomDTO.setUserName(customerInfo.getUserName());
				customersCustomDTO.setPhone(customerInfo.getPhone());
				customersCustomDTO.setCompanyName(customerInfo.getCompanyName() != null ? customerInfo.getCompanyName() : "");	
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return customersCustomDTO;
	}
	
	public static CustomersCustomDTO getCustomerAddressObject(CustomerAddress customerAddressInfo){
		CustomersCustomDTO customerCustomDTO = null;		
		try {
			if(customerAddressInfo!=null){
				customerCustomDTO = new CustomersCustomDTO();
				customerCustomDTO.setAddressLine1(customerAddressInfo.getAddressLine1());
				customerCustomDTO.setAddressLine2(customerAddressInfo.getAddressLine2());
				customerCustomDTO.setCity(customerAddressInfo.getCity());
				customerCustomDTO.setState(customerAddressInfo.getState().getName());
				customerCustomDTO.setCountry(customerAddressInfo.getCountry().getName());
				customerCustomDTO.setZipCode(customerAddressInfo.getZipCode());
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return customerCustomDTO;
	}
	
	public static CustomerAddressDTO getCustomerShipppingAddressObject(CustomerOrderDetails orderDetails){
		CustomerAddressDTO customerAddressDTO = null;		
		try {
			if(orderDetails!=null){
				customerAddressDTO = new CustomerAddressDTO();
				customerAddressDTO.setShipAddrOrderId(orderDetails.getOrderId());
				customerAddressDTO.setShipAddrFirstName(orderDetails.getShFirstName());
				customerAddressDTO.setShipAddrLastName(orderDetails.getShLastName());
				customerAddressDTO.setShipAddrAddressLine1(orderDetails.getShAddress1());
				customerAddressDTO.setShipAddrAddressLine2(orderDetails.getShAddress2());
				customerAddressDTO.setShipAddrEmail(orderDetails.getShEmail());	
				customerAddressDTO.setShipAddrPhone1(orderDetails.getShPhone1());
				customerAddressDTO.setShipAddrCountry(orderDetails.getShCountry());
				customerAddressDTO.setShipAddrCountryName(orderDetails.getShCountryName());
				customerAddressDTO.setShipAddrState(orderDetails.getShState());
				customerAddressDTO.setShipAddrStateName(orderDetails.getShStateName());
				customerAddressDTO.setShipAddrCity(orderDetails.getShCity());
				customerAddressDTO.setShipAddrZipCode(orderDetails.getShZipCode());	
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return customerAddressDTO;
	}
	
	public static CustomerAddressDTO getCustomerBillingAddressObject(CustomerOrderDetails orderDetails){
		CustomerAddressDTO customerAddressDTO = null;		
		try {
			if(orderDetails!=null){
				customerAddressDTO = new CustomerAddressDTO();
				customerAddressDTO.setBillAddrOrderId(orderDetails.getOrderId());
				customerAddressDTO.setBillAddrFirstName(orderDetails.getBlFirstName());
				customerAddressDTO.setBillAddrLastName(orderDetails.getBlLastName());
				customerAddressDTO.setBillAddrAddressLine1(orderDetails.getBlAddress1());
				customerAddressDTO.setBillAddrAddressLine2(orderDetails.getBlAddress2());
				customerAddressDTO.setBillAddrEmail(orderDetails.getBlEmail());	
				customerAddressDTO.setBillAddrPhone1(orderDetails.getBlPhone1());
				customerAddressDTO.setBillAddrCountry(orderDetails.getBlCountry());
				customerAddressDTO.setBillAddrCountryName(orderDetails.getBlCountryName());
				customerAddressDTO.setBillAddrState(orderDetails.getBlState());
				customerAddressDTO.setBillAddrStateName(orderDetails.getBlStateName());
				customerAddressDTO.setBillAddrCity(orderDetails.getBlCity());
				customerAddressDTO.setBillAddrZipCode(orderDetails.getBlZipCode());
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return customerAddressDTO;
	}
	
	public static List<CustomerTicketsDownloadDTO> getCustomerTicketsDownloadArray(List<CustomerTicketDownloads> list){
		CustomerTicketsDownloadDTO customerTicketsDownloadDTO = null;
		List<CustomerTicketsDownloadDTO> customerTicketsDownloadDTOs = new ArrayList<CustomerTicketsDownloadDTO>();
		try {
			if(list!=null){				
				for(CustomerTicketDownloads customerTicketDownload : list){
					customerTicketsDownloadDTO = new CustomerTicketsDownloadDTO();
					customerTicketsDownloadDTO.setId(customerTicketDownload.getId());
					customerTicketsDownloadDTO.setInvoiceId(customerTicketDownload.getInvoiceId());
					customerTicketsDownloadDTO.setCustomerId(customerTicketDownload.getCustomerId());
					customerTicketsDownloadDTO.setTicketType(customerTicketDownload.getTicketType());
					customerTicketsDownloadDTO.setTicketName(customerTicketDownload.getTicketName());
					customerTicketsDownloadDTO.setEmail(customerTicketDownload.getEmail());
					customerTicketsDownloadDTO.setIpAddress(customerTicketDownload.getIpAddress());
					customerTicketsDownloadDTO.setDownloadPlatform(customerTicketDownload.getDownloadPlatform());
					customerTicketsDownloadDTO.setDownloadDateTime(customerTicketDownload.getDownloadDateTimeStr());
					customerTicketsDownloadDTOs.add(customerTicketsDownloadDTO);
				}
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return customerTicketsDownloadDTOs;
	}
	
	public static List<InvoiceRefundListDTO> getInvoiceRefundArray(List<InvoiceRefund> list){
		InvoiceRefundListDTO invoiceRefundListDTO = null;
		List<InvoiceRefundListDTO> invoiceRefundListDTOs = new ArrayList<InvoiceRefundListDTO>();
		try {
			if(list!=null){				
				for(InvoiceRefund invoiceRefund : list){
					invoiceRefundListDTO = new InvoiceRefundListDTO();					
					invoiceRefundListDTO.setInvoiceRefundId(invoiceRefund.getId());
					invoiceRefundListDTO.setRefundType(invoiceRefund.getRefundType());
					invoiceRefundListDTO.setRefundOrderId(invoiceRefund.getOrderId());
					invoiceRefundListDTO.setRefundTransactionId(invoiceRefund.getTransactionId());
					invoiceRefundListDTO.setRefundId(invoiceRefund.getRefundId());
					invoiceRefundListDTO.setRefundAmount(String.format("%.2f", invoiceRefund.getAmount()));
					invoiceRefundListDTO.setRefundStatus(invoiceRefund.getStatus());
					invoiceRefundListDTO.setRefundReason(invoiceRefund.getReason());
					invoiceRefundListDTO.setRevertedUsedRewardPoints(String.format("%.2f", invoiceRefund.getRevertedUsedRewardPoints()));
					invoiceRefundListDTO.setRevertedEarnedRewardPoints(String.format("%.2f", invoiceRefund.getRevertedEarnedRewardPoints()));
					invoiceRefundListDTO.setRefundCreatedTime(invoiceRefund.getCreatedTimeStr());
					invoiceRefundListDTO.setRefundedBy(invoiceRefund.getRefundedBy());
					invoiceRefundListDTOs.add(invoiceRefundListDTO);
				}
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return invoiceRefundListDTOs;
	}
	
	public static List<OpenOrdersDTO> getInvoiceArray(Collection<OpenOrders> list){
		OpenOrdersDTO openOrdersDTO = null;
		List<OpenOrdersDTO> openOrdersDTOs = new ArrayList<OpenOrdersDTO>();		
		try {
			if(list!=null){
				for(OpenOrders order :list){
					openOrdersDTO = new OpenOrdersDTO();					
					openOrdersDTO.setProductType(order.getProductType());
					openOrdersDTO.setInvoiceId(order.getInvoiceId());
					openOrdersDTO.setCustomerId(order.getCustomerId());
					openOrdersDTO.setCustomerName(order.getCustomerName());
					openOrdersDTO.setUsername(order.getUsername());
					openOrdersDTO.setCustomerType(order.getCustomerType());
					openOrdersDTO.setCustCompanyName(order.getCustCompanyName());
					openOrdersDTO.setTicketCount(order.getTicketCount());
					openOrdersDTO.setInvoiceTotal(String.format("%.2f", order.getInvoiceTotal()));
					openOrdersDTO.setAddressLine1(order.getAddressLine1());
					openOrdersDTO.setCountry(order.getCountry());
					openOrdersDTO.setState(order.getState());
					openOrdersDTO.setCity(order.getCity());
					openOrdersDTO.setZipCode(order.getZipCode());
					openOrdersDTO.setPhone(order.getPhone());
					openOrdersDTO.setInvoiceAged(order.getInvoiceAged());
					openOrdersDTO.setCreatedDateStr(order.getCreatedDateStr());
					openOrdersDTO.setCreatedBy(order.getCreatedBy());
					openOrdersDTO.setCsr(order.getCsr());
					openOrdersDTO.setSecondaryOrderType(order.getSecondaryOrderType());
					openOrdersDTO.setSecondaryOrderId(order.getSecondaryOrderId());
					openOrdersDTO.setVoidedDateStr(order.getVoidedDateStr());
					openOrdersDTO.setIsInvoiceEmailSent(order.getIsInvoiceEmailSent().toString());
					openOrdersDTO.setPurchaseOrderNo(order.getPurchaseOrderNo());
					openOrdersDTO.setShippingMethod(order.getShippingMethod());
					openOrdersDTO.setTrackingNo(order.getTrackingNo());
					openOrdersDTO.setPlatform(order.getPlatform());
					if(order.getPlatform() != null && order.getPlatform().equalsIgnoreCase("DESKTOP_SITE")){
						openOrdersDTO.setPlatform("WEB_ORDER");
					}else if(order.getPlatform() != null && order.getPlatform().equalsIgnoreCase("TICK_TRACKER")){
						openOrdersDTO.setPlatform("RETAIL_ORDER");
					}					
					if(order.getFedexLabelCreated()!= null && order.getFedexLabelCreated()){
						openOrdersDTO.setFedexLabelCreated("Yes");
					}else{
						openOrdersDTO.setFedexLabelCreated("No");
					}					
					openOrdersDTO.setCustomerOrderId(order.getCustomerOrderId());
					openOrdersDTO.setLastUpdatedDateStr(order.getLastUpdatedDateStr());
					openOrdersDTO.setLastUpdatedBy(order.getLastUpdatedBy());
					openOrdersDTO.setStatus(order.getStatus());
					openOrdersDTO.setEventId(order.getEventId());
					openOrdersDTO.setEventName(order.getEventName());
					openOrdersDTO.setEventDateStr(order.getEventDateStr());
					openOrdersDTO.setEventTimeStr(order.getEventTimeStr());
					openOrdersDTO.setPendingAmount(String.format("%.2f",order.getPendingAmount()));
					openOrdersDTO.setBrokerId(order.getBrokerId() != null ? order.getBrokerId().toString() : "");
					if(order.getOrderType()!= null && order.getOrderType().equalsIgnoreCase("LOYALFAN")){
						openOrdersDTO.setLoyalFanOrder("Yes");
					}else{
						openOrdersDTO.setLoyalFanOrder("No");
					}
					if(order.getPrimaryPaymentMethod() != null && order.getPrimaryPaymentMethod().equalsIgnoreCase("BANK_OR_CHEQUE")){
						openOrdersDTO.setPrimaryPaymentMethod("BANK_OR_CHECK");
					}else{
						openOrdersDTO.setPrimaryPaymentMethod(order.getPrimaryPaymentMethod());
					}
					if(order.getSecondaryPaymentMethod() != null && !order.getSecondaryPaymentMethod().equalsIgnoreCase("NULL")){
						openOrdersDTO.setSecondaryPaymentMethod(order.getSecondaryPaymentMethod());
					}else{
						openOrdersDTO.setSecondaryPaymentMethod("");
					}
					if(order.getThirdPaymentMethod() != null && !order.getThirdPaymentMethod().equalsIgnoreCase("NULL")){
						openOrdersDTO.setThirdPaymentMethod(order.getThirdPaymentMethod());
					}else{
						openOrdersDTO.setThirdPaymentMethod("");
					}
					openOrdersDTO.setIsPoMapped(order.getIsPoMapped());
					openOrdersDTOs.add(openOrdersDTO);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return openOrdersDTOs;
	}
	
	public static CustomerOrderDTO getCustomerOrderObject(CustomerOrder order){
		CustomerOrderDTO customerOrderDTO = null;		
		try {
			if(order!=null){
				customerOrderDTO = new CustomerOrderDTO();
				customerOrderDTO.setId(order.getId());
				customerOrderDTO.setOrderTotal(order.getOrderTotal());
				customerOrderDTO.setOrderCreateDate(order.getCreatedDateStr());
				customerOrderDTO.setSection(order.getSection());
				customerOrderDTO.setQty(order.getQty());
				customerOrderDTO.setSeatLow(order.getSeatLow());
				customerOrderDTO.setSeatHigh(order.getSeatHigh());
				customerOrderDTO.setTicketPrice(order.getTicketPrice());
				customerOrderDTO.setSoldPrice(order.getSoldPrice());
				customerOrderDTO.setShippingMethod(order.getShippingMethod());
				customerOrderDTO.setEventId(order.getEventId());
				customerOrderDTO.setEventName(order.getEventName());
				customerOrderDTO.setEventDateStr(order.getEventDateStr());
				customerOrderDTO.setEventTimeStr(order.getEventTimeStr());
				customerOrderDTO.setVenueName(order.getVenueName());
				customerOrderDTO.setPrimaryPaymentMethod(order.getPrimaryPaymentMethod());
				customerOrderDTO.setPrimaryPayAmt(order.getPrimaryPayAmt());				
				customerOrderDTO.setSecondaryPaymentMethod(order.getSecondaryPaymentMethod());				
				customerOrderDTO.setSecondaryPayAmt(order.getSecondaryPayAmt());				
				customerOrderDTO.setThirdPaymentMethod(order.getThirdPaymentMethod());
				customerOrderDTO.setThirdPayAmt(order.getThirdPayAmt());
				
				customerOrderDTO.setOrderId(order.getId());
				customerOrderDTO.setPrimaryAvailableAmt(order.getPrimaryAvailableAmt());
				customerOrderDTO.setSecondaryAvailableAmt(order.getSecondaryAvailableAmt());
				customerOrderDTO.setThirdAvailableAmt(order.getThirdAvailableAmt());
				
				customerOrderDTO.setTicketGroupId(order.getCategoryTicketGroupId());
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return customerOrderDTO;
	}
	
	public static List<InvoiceAuditDTO> getInvoiceAuditArray(List<InvoiceAudit> auditList,List<CustomerTicketDownloads> customerTicketDownloads){
		List<InvoiceAuditDTO> invoiceAuditDTOs = new ArrayList<InvoiceAuditDTO>();
		try {
			if(auditList!=null && !auditList.isEmpty()){
				InvoiceAuditDTO invoiceAuditDTO = null;
				for(InvoiceAudit audit : auditList){
					invoiceAuditDTO = new InvoiceAuditDTO();
					invoiceAuditDTO.setId(audit.getId());
					invoiceAuditDTO.setInvoiceId(audit.getInvoiceId());
					invoiceAuditDTO.setAction(audit.getAction().toString());
					invoiceAuditDTO.setCreatedDate(audit.getCreatedDateStr());
					invoiceAuditDTO.setCreatedBy(audit.getCreateBy());
					invoiceAuditDTO.setNote(audit.getNote());
					invoiceAuditDTOs.add(invoiceAuditDTO);
				}
			}
			
			if(customerTicketDownloads!=null && !customerTicketDownloads.isEmpty()){
				InvoiceAuditDTO invoiceAuditDTO = null;
				for(CustomerTicketDownloads download : customerTicketDownloads){
					invoiceAuditDTO = new InvoiceAuditDTO();
					invoiceAuditDTO.setId(download.getId());
					invoiceAuditDTO.setInvoiceId(download.getInvoiceId());
					invoiceAuditDTO.setAction(InvoiceAuditAction.TICKET_DOWNLOADED.toString());
					invoiceAuditDTO.setCreatedDate(download.getDownloadDateTimeStr());
					invoiceAuditDTO.setCreatedBy(DAORegistry.getCustomerDAO().get(download.getCustomerId()).getUserName());
					invoiceAuditDTO.setNote(download.getTicketName());
					invoiceAuditDTOs.add(invoiceAuditDTO);
				}
			}			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return invoiceAuditDTOs;
	}
	
	public static OpenOrdersDTO getOpenOrderObject(OpenOrders openOrder, CustomerOrder customerOrder){
		OpenOrdersDTO openOrdersDTO = null;		
		try {
			if(openOrder!=null){
				openOrdersDTO = new OpenOrdersDTO();
				openOrdersDTO.setOpenOrderInvoiceId(openOrder.getInvoiceId());
				openOrdersDTO.setOpenOrderProductType(openOrder.getProductType());
				openOrdersDTO.setOpenOrderSection(openOrder.getSection());
				openOrdersDTO.setOpenOrderRow(openOrder.getRow());
				openOrdersDTO.setOpenOrderQuantity(openOrder.getQty());
				openOrdersDTO.setOpenOrderPrice(openOrder.getSoldPrice());
				openOrdersDTO.setOpenOrderShippingMethodId(openOrder.getShippingMethodId());
				openOrdersDTO.setOpenOrderShippingMethod(openOrder.getShippingMethod()!=null?openOrder.getShippingMethod():"");
				openOrdersDTO.setOpenOrderCustomerName(openOrder.getCustomerName()+" "+openOrder.getLastName());
				openOrdersDTO.setOpenOrderUserName(openOrder.getUsername()); //Email
				openOrdersDTO.setOpenOrderPhone(openOrder.getPhone());
				openOrdersDTO.setOpenOrderAddressLine1(openOrder.getAddressLine1());
				openOrdersDTO.setOpenOrderCountry(openOrder.getCountry());
				openOrdersDTO.setOpenOrderState(openOrder.getState());
				openOrdersDTO.setOpenOrderCity(openOrder.getCity());
				openOrdersDTO.setOpenOrderZipcode(openOrder.getZipCode());
				openOrdersDTO.setOpenOrderRealTixDelivered(openOrder.getRealTixDelivered());
				openOrdersDTO.setLongSale("NO");
				if(customerOrder.getIsLongSale()){
					openOrdersDTO.setLongSale("YES");
					openOrdersDTO.setOpenOrderQuantity(customerOrder.getQty());
					openOrdersDTO.setOpenOrderShippingMethod(customerOrder.getShippingMethod());
					openOrdersDTO.setOpenOrderShippingMethodId(customerOrder.getShippingMethodId());
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return openOrdersDTO;
	}
	
	public static List<InvoiceTicketAttachmentDTO> getETicketAttachmentArray(List<InvoiceTicketAttachment> list){
		InvoiceTicketAttachmentDTO invoiceTicketAttachmentDTO = null;
		List<InvoiceTicketAttachmentDTO> invoiceTicketAttachmentDTOs = new ArrayList<InvoiceTicketAttachmentDTO>();
		try {
			if(list!=null){				
				for(InvoiceTicketAttachment eticketAttachment : list){
					invoiceTicketAttachmentDTO = new InvoiceTicketAttachmentDTO();					
					invoiceTicketAttachmentDTO.setTicketAttachmentInvoiceId(eticketAttachment.getInvoiceId());
					invoiceTicketAttachmentDTO.setTicketAttachmentType(eticketAttachment.getType());
					invoiceTicketAttachmentDTO.setTicketAttachmentPosition(eticketAttachment.getPosition());
					invoiceTicketAttachmentDTO.setTicketAttachmentFileName(eticketAttachment.getFileName());
					invoiceTicketAttachmentDTOs.add(invoiceTicketAttachmentDTO);
				}
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return invoiceTicketAttachmentDTOs;
	}
	
	public static List<InvoiceTicketAttachmentDTO> getQRCodeAttachmentArray(List<InvoiceTicketAttachment> list){
		InvoiceTicketAttachmentDTO invoiceTicketAttachmentDTO = null;
		List<InvoiceTicketAttachmentDTO> invoiceTicketAttachmentDTOs = new ArrayList<InvoiceTicketAttachmentDTO>();
		try {
			if(list!=null){				
				for(InvoiceTicketAttachment qrcodeAttachment : list){
					invoiceTicketAttachmentDTO = new InvoiceTicketAttachmentDTO();					
					invoiceTicketAttachmentDTO.setQrcodeAttachmentInvoiceId(qrcodeAttachment.getInvoiceId());
					invoiceTicketAttachmentDTO.setQrcodeAttachmentType(qrcodeAttachment.getType());
					invoiceTicketAttachmentDTO.setQrcodeAttachmentPosition(qrcodeAttachment.getPosition());
					invoiceTicketAttachmentDTO.setQrcodeAttachmentFileName(qrcodeAttachment.getFileName());
					invoiceTicketAttachmentDTOs.add(invoiceTicketAttachmentDTO);
				}
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return invoiceTicketAttachmentDTOs;
	}
	
	public static List<InvoiceTicketAttachmentDTO> getBarCodeAttachmentArray(List<InvoiceTicketAttachment> list){
		InvoiceTicketAttachmentDTO invoiceTicketAttachmentDTO = null;
		List<InvoiceTicketAttachmentDTO> invoiceTicketAttachmentDTOs = new ArrayList<InvoiceTicketAttachmentDTO>();
		try {
			if(list!=null){				
				for(InvoiceTicketAttachment barcodeAttachment : list){
					invoiceTicketAttachmentDTO = new InvoiceTicketAttachmentDTO();					
					invoiceTicketAttachmentDTO.setBarcodeAttachmentInvoiceId(barcodeAttachment.getInvoiceId());
					invoiceTicketAttachmentDTO.setBarcodeAttachmentType(barcodeAttachment.getType());
					invoiceTicketAttachmentDTO.setBarcodeAttachmentPosition(barcodeAttachment.getPosition());
					invoiceTicketAttachmentDTO.setBarcodeAttachmentFileName(barcodeAttachment.getFileName());
					invoiceTicketAttachmentDTOs.add(invoiceTicketAttachmentDTO);
				}
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return invoiceTicketAttachmentDTOs;
	}
	
	public static List<CustomerCardInfoDTO> getCustomerCardInfoArray(List<CustomerCardInfo> list){
		CustomerCardInfoDTO customerCardInfoDTO = null;
		List<CustomerCardInfoDTO> customerCardInfoDTOs = new ArrayList<CustomerCardInfoDTO>();
		try {
			if(list!=null){				
				for(CustomerCardInfo customerCardInfo : list){
					customerCardInfoDTO = new CustomerCardInfoDTO();					
					customerCardInfoDTO.setCardId(customerCardInfo.getId());
					customerCardInfoDTO.setCardType(customerCardInfo.getCardType());
					customerCardInfoDTO.setCardNo(customerCardInfo.getCardNo());
					customerCardInfoDTOs.add(customerCardInfoDTO);
				}
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return customerCardInfoDTOs;
	}
	
	public static List<ReferralContestListDTO> getLotteriesFilterArray(List<ReferralContest> list){
		ReferralContestListDTO referralContestListDTO = null;
		List<ReferralContestListDTO> referralContestListDTOs = new ArrayList<ReferralContestListDTO>();
		try {
			if(list!=null){				
				for(ReferralContest contest : list){
					referralContestListDTO = new ReferralContestListDTO();					
					referralContestListDTO.setId(contest.getId());
					referralContestListDTO.setName(contest.getName());
					referralContestListDTO.setMinPurchaseAmt(contest.getMinimumPurchaseAmount());
					referralContestListDTO.setStartDate(contest.getStartDateStr());
					referralContestListDTO.setEndDate(contest.getEndDateStr());
					referralContestListDTO.setArtistName(contest.getArtistName());
					referralContestListDTO.setArtistNameStr(contest.getArtistNameStr());
					referralContestListDTO.setArtistId(contest.getArtistId());
					referralContestListDTO.setStatus(contest.getContestStatus());
					referralContestListDTO.setCreatedBy(contest.getCreatedBy());
					referralContestListDTO.setCreatedDate(contest.getCreatedDateStr());
					referralContestListDTO.setUpdatedBy(contest.getUpdatedBy());
					referralContestListDTO.setUpdatedDate(contest.getUpdatedDateStr());
					String startDate[] = contest.getStartDateStr().split(" ");
					referralContestListDTO.setFromDate(startDate[0]);
					String endDate[] = contest.getEndDateStr().split(" ");
					referralContestListDTO.setToDate(endDate[0]);
					referralContestListDTOs.add(referralContestListDTO);
				}
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return referralContestListDTOs;
	}
	
	public static List<ReferralContestParticipantsListDTO> getParticipantFilterArray(List<ReferralContestParticipants> list){
		ReferralContestParticipantsListDTO referralContestParticipantListDTO = null;
		List<ReferralContestParticipantsListDTO> referralContestParticipantListDTOs = new ArrayList<ReferralContestParticipantsListDTO>();
		try {
			if(list!=null){				
				for(ReferralContestParticipants contParticipant : list){
					referralContestParticipantListDTO = new ReferralContestParticipantsListDTO();					
					referralContestParticipantListDTO.setId(contParticipant.getId());
					referralContestParticipantListDTO.setContestId(contParticipant.getContestId());
					referralContestParticipantListDTO.setCustomerId(contParticipant.getCustomerId());
					referralContestParticipantListDTO.setCustomerName(contParticipant.getCustomerName());
					referralContestParticipantListDTO.setCustomerEmail(contParticipant.getCustomerEmail());
					referralContestParticipantListDTO.setPurchaseCustomerId(contParticipant.getPurchaseCustomerId());
					referralContestParticipantListDTO.setPurchaseCustomerName(contParticipant.getPurchaseCustomerName());
					referralContestParticipantListDTO.setPurchaseCustomerEmail(contParticipant.getPurchaseCustomerEmail());
					referralContestParticipantListDTO.setReferralCode(contParticipant.getReferralCode());
					referralContestParticipantListDTO.setCreatedDate(contParticipant.getCreatedDateStr());
					referralContestParticipantListDTOs.add(referralContestParticipantListDTO);
				}
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return referralContestParticipantListDTOs;
	}
	
	public static ReferralContestWinnerListDTO getReferralContestWinnerObject(ReferralContestWinner winner){
		ReferralContestWinnerListDTO contestWinnerListDTO = new ReferralContestWinnerListDTO();
		try {
			if(winner!=null){
				contestWinnerListDTO.setId(winner.getId());
				contestWinnerListDTO.setWinnerName(winner.getWinnerName());
				contestWinnerListDTO.setWinnerEmail(winner.getWinnerEmail());
				contestWinnerListDTO.setWinnerCustomerId(winner.getWinnerCustomerId());
				contestWinnerListDTO.setCreatedBy(winner.getCreatedBy());
				contestWinnerListDTO.setCreatedDate(winner.getCreatedOnStr());
				contestWinnerListDTO.setStartDate(winner.getStartDateStr());
				contestWinnerListDTO.setEndDate(winner.getEndDateStr());
				contestWinnerListDTO.setContestName(winner.getContestName());
				contestWinnerListDTO.setWinnerCity("N/A");
				if(winner.getCity() != null && !winner.getCity().isEmpty()){
					contestWinnerListDTO.setWinnerCity(winner.getCity());
				}
				contestWinnerListDTO.setIsEmailed("No");
				if(winner.getIsEmailed()){
					contestWinnerListDTO.setIsEmailed("Yes");
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return contestWinnerListDTO;
	}
		
	public static EventDetailsDTO getEventDetailsObject(Event event,Venue venue){
		EventDetailsDTO eventDetailsDTO = new EventDetailsDTO();
		try {
			if(event!=null){
				eventDetailsDTO.setEventId(event.getEventId());
				eventDetailsDTO.setEventName(event.getEventName());
				eventDetailsDTO.setEventDateStr(event.getEventDateStr());
				eventDetailsDTO.setEventTimeStr(event.getEventTimeStr());
				eventDetailsDTO.setBuilding(venue.getBuilding());
				eventDetailsDTO.setVenueId(event.getVenueId());
				eventDetailsDTO.setCity(venue.getCity());
				eventDetailsDTO.setState(venue.getState());
				eventDetailsDTO.setCountry(venue.getCountry());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return eventDetailsDTO;
	}
	
	public static List<CategoryTicketGroupListDTO> getCategoryTicketGroupArray(Collection<CategoryTicketGroup> list,Map<Integer, String> shippingMap,Event eventDtls){
		CategoryTicketGroupListDTO categoryTicketGroupListDTO = null;
		List<CategoryTicketGroupListDTO> categoryTicketGroupListDTOs = new ArrayList<CategoryTicketGroupListDTO>();
		try {
			if(list!=null){
				for (CategoryTicketGroup tx : list) {
					categoryTicketGroupListDTO = new CategoryTicketGroupListDTO();
					categoryTicketGroupListDTO.setId(tx.getId());
					categoryTicketGroupListDTO.setEventId(tx.getEventId());
					categoryTicketGroupListDTO.setSection(tx.getSection());
					categoryTicketGroupListDTO.setRow(tx.getRow()==null?"":tx.getRow());
					categoryTicketGroupListDTO.setQuantity(tx.getQuantity());
					categoryTicketGroupListDTO.setPrice(String.format("%.2f",tx.getPrice()));
					categoryTicketGroupListDTO.setTaxAmount(String.format("%.2f",tx.getTaxAmount()));
					categoryTicketGroupListDTO.setSectionRange(tx.getSectionRange());
					categoryTicketGroupListDTO.setRowRange(tx.getRowRange());
					categoryTicketGroupListDTO.setShippingMethod(shippingMap.get(tx.getShippingMethodId())==null?"":shippingMap.get(tx.getShippingMethodId()));
					categoryTicketGroupListDTO.setProductType(tx.getProducttype());
					categoryTicketGroupListDTO.setRetailPrice(String.format("%.2f",tx.getPrice()));//tx.getPrice());
					categoryTicketGroupListDTO.setWholeSalePrice(String.format("%.2f",tx.getPrice()));//tx.getWholeSalePrice());
					categoryTicketGroupListDTO.setFacePrice(String.format("%.2f",0.0));// tx.getFacePrice());
					categoryTicketGroupListDTO.setLoyalFanPrice(String.format("%.2f",tx.getLoyalFanPrice()));
					categoryTicketGroupListDTO.setCost(String.format("%.2f",0.0));//tx.getCost());
					categoryTicketGroupListDTO.setBroadcast(tx.getBroadcast()!=null?tx.getBroadcast():false);//tx.getBroadcast());
					categoryTicketGroupListDTO.setInternalNotes("ZTP");// tx.getInternalNotes());
					categoryTicketGroupListDTO.setExternalNotes(tx.getExternalNotes());
					categoryTicketGroupListDTO.setMarketPlaceNotes(tx.getMarketPlaceNotes());
					categoryTicketGroupListDTO.setMaxShowing(tx.getMaxShowing());
					categoryTicketGroupListDTO.setSeatLow(tx.getSeatLow());
					categoryTicketGroupListDTO.setSeatHigh(tx.getSeatHigh());
					categoryTicketGroupListDTO.setNearTermDisplayOption(tx.getNearTermDisplayOption());
					if(eventDtls!=null){
						categoryTicketGroupListDTO.setEventName(eventDtls.getEventName());
						categoryTicketGroupListDTO.setEventDate(eventDtls.getEventDateStr());
						categoryTicketGroupListDTO.setEventTime(eventDtls.getEventTimeStr());
						categoryTicketGroupListDTO.setVenue(eventDtls.getBuilding());
					}
					categoryTicketGroupListDTO.setBrokerId(tx.getBrokerId());
					categoryTicketGroupListDTO.setTicketType("Category");
					categoryTicketGroupListDTOs.add(categoryTicketGroupListDTO);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return categoryTicketGroupListDTOs;
	}
	
	public static List<TicketGroupDTO> getTicketGroupArray(Collection<TicketGroup> list,Map<Integer, String> shippingMap,Event eventDtls){
		TicketGroupDTO ticketGroupDTO = null;
		List<TicketGroupDTO> ticketGroupDTOs = new ArrayList<TicketGroupDTO>();
		try {
			if(list!=null){
				for (TicketGroup tx : list) {
					ticketGroupDTO = new TicketGroupDTO();					
					ticketGroupDTO.setId(tx.getId());
					ticketGroupDTO.setEventId(tx.getEventId());
					ticketGroupDTO.setSection(tx.getSection());
					ticketGroupDTO.setRow(tx.getRow()==null?"":tx.getRow());
					ticketGroupDTO.setQuantity(tx.getQuantity());
					ticketGroupDTO.setPrice(String.format("%.2f",tx.getPrice()));
					ticketGroupDTO.setTaxAmount(String.format("%.2f", 0.0));
					ticketGroupDTO.setSectionRange("");
					ticketGroupDTO.setRowRange("");
					ticketGroupDTO.setShippingMethod(shippingMap.get(tx.getShippingMethodId()));
					ticketGroupDTO.setProductType("REWARDTHEFAN");
					ticketGroupDTO.setRetailPrice(String.format("%.2f",tx.getPrice()));//tx.getPrice());
					ticketGroupDTO.setWholeSalePrice(String.format("%.2f",tx.getPrice()));//tx.getWholeSalePrice());
					ticketGroupDTO.setFacePrice(String.format("%.2f",0.0));// tx.getFacePrice());
					ticketGroupDTO.setLoyalFanPrice(String.format("%.2f",tx.getLoyalFanPrice()));
					ticketGroupDTO.setCost(String.format("%.2f",0.0));//tx.getCost());
					ticketGroupDTO.setBroadcast(tx.getBroadcast()!=null?tx.getBroadcast():false);//tx.getBroadcast());
					ticketGroupDTO.setInternalNotes("ZTP");// tx.getInternalNotes());
					ticketGroupDTO.setExternalNotes("");
					ticketGroupDTO.setMarketPlaceNotes("");
					ticketGroupDTO.setMaxShowing("");
					ticketGroupDTO.setSeatLow(tx.getSeatLow());
					ticketGroupDTO.setSeatHigh(tx.getSeatHigh());
					ticketGroupDTO.setNearTermDisplayOption("");
					if(eventDtls!=null){
						ticketGroupDTO.setEventName(eventDtls.getEventName());
						ticketGroupDTO.setEventDate(eventDtls.getEventDateStr());
						ticketGroupDTO.setEventTime(eventDtls.getEventTimeStr());
						ticketGroupDTO.setVenue(eventDtls.getBuilding());
					}
					ticketGroupDTO.setBrokerId(tx.getBrokerId());
					ticketGroupDTO.setTicketType("Real");
					ticketGroupDTOs.add(ticketGroupDTO);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return ticketGroupDTOs;
	}
	
	public static List<UserActionDTO> getUserActionsArray(Collection<UserAction> list){
		UserActionDTO userActionDTO = null;
		List<UserActionDTO> userActionDTOs = new ArrayList<UserActionDTO>();
		try {
			if(list!=null){			
				for(UserAction user : list){
					userActionDTO = new UserActionDTO();
					userActionDTO.setId(user.getId());
					userActionDTO.setUserName(user.getUserName());
					userActionDTO.setUserId(user.getUserId());
					userActionDTO.setUserAction(user.getAction());
					userActionDTO.setTimeStampStr(user.getTimeStampStr());										
					userActionDTO.setClientIPAddress(user.getIpAddress());
					userActionDTO.setUserMessage(user.getMessage());
					userActionDTOs.add(userActionDTO);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userActionDTOs;
	}
	
	public static List<SeatGeekOrdersDTO> getSeatGeekOrdersArray(List<SeatGeekOrders> list){
		SeatGeekOrdersDTO seatGeekOrdersDTO = null;
		List<SeatGeekOrdersDTO> seatGeekOrdersDTOs = new ArrayList<SeatGeekOrdersDTO>();
		try {
			if(list!=null){			
				for(SeatGeekOrders order : list){
					seatGeekOrdersDTO = new SeatGeekOrdersDTO();
					
					seatGeekOrdersDTO.setId(order.getId());
					seatGeekOrdersDTO.setOrderId(order.getOrderId());
					seatGeekOrdersDTO.setOrderDateStr(order.getOrderDateStr());
					seatGeekOrdersDTO.setEventName(order.getEventName());
					seatGeekOrdersDTO.setEventDateStr(order.getEventDateStr());										
					seatGeekOrdersDTO.setEventTimeStr( order.getEventTimeStr());
					seatGeekOrdersDTO.setVenueName( order.getVenueName());
					seatGeekOrdersDTO.setQuantity(order.getQuantity());
					seatGeekOrdersDTO.setSection(order.getSection());
					seatGeekOrdersDTO.setRow(order.getRow());
					seatGeekOrdersDTO.setTotalSaleAmt(String.format("%.2f", order.getTotalSaleAmt()));
					seatGeekOrdersDTO.setTotalPaymentAmt(String.format("%.2f", order.getTotalPaymentAmt()));
					seatGeekOrdersDTO.setStatus(order.getStatus());
					seatGeekOrdersDTO.setLastUpdatedDateStr(order.getLastUpdatedDateStr());
					seatGeekOrdersDTO.setTicTrackerOrderId(order.getTicTrackerOrderId());
					seatGeekOrdersDTO.setTicTrackerInvoiceId(order.getTicTrackerInvoiceId());
					seatGeekOrdersDTOs.add(seatGeekOrdersDTO);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return seatGeekOrdersDTOs;
	}
	
	public static List<RtfConfigContestClusterNodes> getContestServerNodeUrls(){
		try {
			if(rtfContestClusterNodesList.isEmpty()){
				rtfContestClusterNodesList = DAORegistry.getQueryManagerDAO().getRtfContestNodesUrls();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rtfContestClusterNodesList;
	}
	
	public static Date getDateWithTwentyFourHourFormat1(String datetime){
		Date result =null;
		DateFormat formatDateTime = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		try{
			if(datetime != null){
				result = formatDateTime.parse(datetime);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
}
