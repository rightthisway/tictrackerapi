package com.rtw.tracker.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;

public class HttpUtil {

	public static String executeHttpGetRequest(String host,String path,Map<String, String> httpHeaders,Map<String, String> httpParams) throws Exception{
		HttpClient httpClient = HttpClientBuilder.create().build();	
		URIBuilder builder = new URIBuilder();
		builder.setScheme("http").setHost(host).setPath(path);
			
		for(Map.Entry<String, String> entry :httpParams.entrySet()){
			builder.setParameter(entry.getKey(), entry.getValue());
		}
		
		URI uri = builder.build();
		HttpGet request = new HttpGet(uri);
		
		for(Map.Entry<String, String> entry :httpHeaders.entrySet()){
			request.setHeader(entry.getKey(), entry.getValue());
		}

		return getContentInString(httpClient.execute(request));
		
	}
	
	public static String executeHttpPostRequest(String url,Map<String, String> httpHeaders,Map<String, String> httpParams) throws Exception{
		HttpClient httpClient = HttpClientBuilder.create().build();	
		HttpPost request = new HttpPost(url);
		
		for(Map.Entry<String, String> entry :httpHeaders.entrySet()){
			request.setHeader(entry.getKey(), entry.getValue());
		}
		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();		
		for(Map.Entry<String, String> entry :httpParams.entrySet()){
			urlParameters.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
		}
		
		request.setEntity(new UrlEncodedFormEntity(urlParameters));
		return getContentInString(httpClient.execute(request));
	}
	
	public static String getContentInString(HttpResponse response) throws Exception{
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		StringBuilder result = new StringBuilder();
		String line = "";
		while ((line = bufferedReader.readLine()) != null) {
			result.append(line);
		}
		bufferedReader.close();
		return result.toString();
	}
	
}
