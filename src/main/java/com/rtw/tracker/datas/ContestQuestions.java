package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.enums.MiniJackpotType;


@Entity
@Table(name="contest_questions")
public class ContestQuestions implements Serializable{

	private Integer id;
	private Integer contestId;
	private Integer serialNo;
	private String question;
	private String optionA;
	private String optionB;
	private String optionC;
	//private String optionD;
	private String answer;
	private Date createdDate;
	private Date updatedDate;
	private String createdBy;
	private String updatedBy;
	private Double questionReward;
	private String difficultyLevel;
	
	private String createdDateTimeStr;
	private String updatedDateTimeStr;
	private String rewardPerQuestionStr;
	private String correctAnswer;
	private String wrongAnswer;
		
	private MiniJackpotType miniJackpotType;
	private Integer mjpNoOfWinner;
	private Double mjpQtyPerWinner;
	private Integer mjpGiftCardId;
	
	private Integer mjpGiftCardQty;
	private Double mjpGiftCardAmt;
	private String mjpGiftCardName;
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="contest_id")
	public Integer getContestId() {
		return contestId;
	}
	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}
	
	@Column(name="question_sl_no")
	public Integer getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(Integer serialNo) {
		this.serialNo = serialNo;
	}
	
	@Column(name="question")
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	
	@Column(name="option_a")
	public String getOptionA() {
		return optionA;
	}
	public void setOptionA(String optionA) {
		this.optionA = optionA;
	}
	
	@Column(name="option_b")
	public String getOptionB() {
		return optionB;
	}
	public void setOptionB(String optionB) {
		this.optionB = optionB;
	}
	
	@Column(name="option_c")
	public String getOptionC() {
		return optionC;
	}
	public void setOptionC(String optionC) {
		this.optionC = optionC;
	}
	
	/*@Column(name="option_d")
	public String getOptionD() {
		return optionD;
	}
	public void setOptionD(String optionD) {
		this.optionD = optionD;
	}*/
	
	@Column(name="answer")
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	@Column(name="created_datetime")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="updated_datetime")
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@Column(name="created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column(name="updated_by")
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column(name="question_reward")
	public Double getQuestionReward() {
		return questionReward;
	}
	public void setQuestionReward(Double questionReward) {
		this.questionReward = questionReward;
	}
	
	@Column(name="difficulty_level")
	public String getDifficultyLevel() {
		return difficultyLevel;
	}
	public void setDifficultyLevel(String difficultyLevel) {
		this.difficultyLevel = difficultyLevel;
	}
	
	@Column(name="mini_jackpot_type")
	@Enumerated(EnumType.ORDINAL)
	public MiniJackpotType getMiniJackpotType() {
		return miniJackpotType;
	}
	public void setMiniJackpotType(MiniJackpotType miniJackpotType) {
		this.miniJackpotType = miniJackpotType;
	}
	
	/*public void setMiniJackpotType(String miniJackpotType) {
		if(miniJackpotType != null) {
			this.miniJackpotType = MiniJackpotType.valueOf(miniJackpotType);
		}
		this.miniJackpotType = null;
		
	}*/
	
	@Column(name="no_of_winner")
	public Integer getMjpNoOfWinner() {
		return mjpNoOfWinner;
	}
	public void setMjpNoOfWinner(Integer mjpNoOfWinner) {
		this.mjpNoOfWinner = mjpNoOfWinner;
	}
	
	@Column(name="qty_per_winner")
	public Double getMjpQtyPerWinner() {
		return mjpQtyPerWinner;
	}
	public void setMjpQtyPerWinner(Double mjpQtyPerWinner) {
		this.mjpQtyPerWinner = mjpQtyPerWinner;
	}
	
	@Column(name="jackpot_gift_card_id")
	public Integer getMjpGiftCardId() {
		return mjpGiftCardId;
	}
	public void setMjpGiftCardId(Integer mjpGiftCardId) {
		this.mjpGiftCardId = mjpGiftCardId;
	}
	
	@Transient
	public String getMjpGiftCardName() {
		/*if(mjpGiftCardName != null) {
			return mjpGiftCardName;
		}
		if(mjpGiftCardId != null) {
			RtfGiftCard rtfGiftcard = DAORegistry.getRtfGiftCardDAO().get(mjpGiftCardId);
			if(rtfGiftcard != null) {
				mjpGiftCardName = rtfGiftcard.getTitle();
			}
		}*/
		return mjpGiftCardName;
	}
	public void setMjpGiftCardName(String mjpGiftCardName) {
		this.mjpGiftCardName = mjpGiftCardName;
	}
	@Transient
	public Integer getMjpGiftCardQty() {
		return mjpGiftCardQty;
	}
	public void setMjpGiftCardQty(Integer mjpGiftCardQty) {
		this.mjpGiftCardQty = mjpGiftCardQty;
	}
	@Transient
	public Double getMjpGiftCardAmt() {
		return mjpGiftCardAmt;
	}
	public void setMjpGiftCardAmt(Double mjpGiftCardAmt) {
		this.mjpGiftCardAmt = mjpGiftCardAmt;
	}
	@Transient
	public String getRewardPerQuestionStr() {
		if(questionReward != null){
			rewardPerQuestionStr = String.valueOf(Util.roundOffDouble(questionReward));
		}
		return rewardPerQuestionStr;
	}
	public void setRewardPerQuestionStr(String rewardPerQuestionStr) {
		this.rewardPerQuestionStr = rewardPerQuestionStr;
	}
	
	@Transient
	public String getCreatedDateTimeStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(createdDate);
	}
	public void setCreatedDateTimeStr(String createdDateTimeStr) {
		this.createdDateTimeStr = createdDateTimeStr;
	}
	
	@Transient
	public String getUpdatedDateTimeStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(updatedDate);
	}
	public void setUpdatedDateTimeStr(String updatedDateTimeStr) {
		this.updatedDateTimeStr = updatedDateTimeStr;
	}
	
	@Transient
	public String getCorrectAnswer() {
		return correctAnswer;
	}
	public void setCorrectAnswer(String correctAnswer) {
		this.correctAnswer = correctAnswer;
	}
	
	@Transient
	public String getWrongAnswer() {		
		return Util.getRandomWrongAnswer(correctAnswer);
	}
	public void setWrongAnswer(String wrongAnswer) {
		this.wrongAnswer = wrongAnswer;
	}
	
}
