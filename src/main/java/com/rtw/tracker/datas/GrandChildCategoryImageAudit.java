package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="grand_child_category_image_audit")
public class GrandChildCategoryImageAudit implements Serializable  {
	
	private Integer id;
	private String imageFileUrl;
	private String circleImageFileUrl;
	private String lastUpdatedBy;
	private Date lastUpdated;
	private Integer grandChildCategoryId;
	private String status;
	private String action;
	
	public GrandChildCategoryImageAudit() {
		//super();
	}
	public GrandChildCategoryImageAudit(GrandChildCategoryImage gcImage) {
		this.grandChildCategoryId=gcImage.getGrandChildCategory().getId();
		this.imageFileUrl=gcImage.getImageFileUrl();
		this.circleImageFileUrl=gcImage.getCircleImageFileUrl();
		this.lastUpdated=gcImage.getLastUpdated();
		this.lastUpdatedBy=gcImage.getLastUpdatedBy();
		this.status=gcImage.getStatus();
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="image_url")
	public String getImageFileUrl() {
		return imageFileUrl;
	}

	public void setImageFileUrl(String imageFileUrl) {
		this.imageFileUrl = imageFileUrl;
	}

	@Column(name="last_updated_by")
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	@Column(name="grand_child_category_id")
	public Integer getGrandChildCategoryId() {
		return grandChildCategoryId;
	}

	public void setGrandChildCategoryId(Integer grandChildCategoryId) {
		this.grandChildCategoryId = grandChildCategoryId;
	}

	@Column(name="action")
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	@Column(name="status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="circle_image_url")
	public String getCircleImageFileUrl() {
		return circleImageFileUrl;
	}
	public void setCircleImageFileUrl(String circleImageFileUrl) {
		this.circleImageFileUrl = circleImageFileUrl;
	}

	
}

