package com.rtw.tracker.datas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="venue")
public class Venue implements Serializable{
	protected Integer id;
	protected String building;
	protected String city;
	protected String state;
	protected String country;
	protected String postalCode;
	protected String notes;
	protected Integer catTicketCount;
	
	@Id
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="building")
	public String getBuilding() {
		return building;
	}

	public void setBuilding(String building) {
		this.building = building;
	}
	
	@Column(name="city")
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	@Column(name="state")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	@Column(name="country")
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}

	/*@Column(name="stubhub_id")
	public Integer getStubhubId() {
		return stubhubId;
	}

	public void setStubhubId(Integer stubhubId) {
		this.stubhubId = stubhubId;
	}*/
	
	@Transient
	public String getLocation() {
		String location = "";
		if (building != null) {
			location += building + ", ";
		}
		
		if (city != null) {
			location += city + ", ";
		}

		if (state != null) {
			location += state + ", ";
		}

		if (country != null) {
			location += country + ", ";
		}

		if (!location.isEmpty()) {
			location = location.substring(0, location.length() - 2);
		}
		
		return location;
	}
	
	
	@Column(name="postal_code")
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	
	@Column(name="notes")
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	@Transient
	public Integer getCatTicketCount() {
		return catTicketCount;
	}
	public void setCatTicketCount(Integer catTicketCount) {
		this.catTicketCount = catTicketCount;
	}
	
	
	
	
}
