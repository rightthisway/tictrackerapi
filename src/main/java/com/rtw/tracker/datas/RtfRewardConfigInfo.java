package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;

@Entity
@Table(name="rtf_reward_config")
public class RtfRewardConfigInfo implements Serializable{
	
	private Integer id;
	private String mediaDescription;
	private String actionType;
	private Integer sfStars;
	private Integer lives;
	private Integer magicWands;
	private Integer rtfPoints;
	private Double rwdDollars;
	private Boolean status;
	private Date activeDate;
	private Date createdDate;
	private Date updatedDate;
	private String createdBy;
	private String updatedBy;
	private Integer maxRewardsPerDay;
	private Integer batchSize;
	private Integer minVideoPlayTime;
	private Integer videoRewardInterval;
	private Boolean isDisplay;
	
	private String activeDateStr;
	private String createdDateStr;
	private String updatedDateStr;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="reward_title")
	public String getMediaDescription() {
		return mediaDescription;
	}
	public void setMediaDescription(String mediaDescription) {
		this.mediaDescription = mediaDescription;
	}
	
	@Column(name="action_type")
	public String getActionType() {
		return actionType;
	}
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}
	
	@Column(name="no_of_sf_stars")
	public Integer getSfStars() {
		return sfStars;
	}
	public void setSfStars(Integer sfStars) {
		this.sfStars = sfStars;
	}
	
	@Column(name="no_of_lives")
	public Integer getLives() {
		return lives;
	}
	public void setLives(Integer lives) {
		this.lives = lives;
	}
	
	@Column(name="no_of_magic_wand")
	public Integer getMagicWands() {
		return magicWands;
	}
	public void setMagicWands(Integer magicWands) {
		this.magicWands = magicWands;
	}
	
	@Column(name="rtf_points")
	public Integer getRtfPoints() {
		return rtfPoints;
	}
	public void setRtfPoints(Integer rtfPoints) {
		this.rtfPoints = rtfPoints;
	}
	
	@Column(name="reward_dollars")
	public Double getRwdDollars() {
		return rwdDollars;
	}
	public void setRwdDollars(Double rwdDollars) {
		this.rwdDollars = rwdDollars;
	}
	
	@Column(name="status")
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Column(name="active_date")
	public Date getActiveDate() {
		return activeDate;
	}
	public void setActiveDate(Date activeDate) {
		this.activeDate = activeDate;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="updated_date")
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@Column(name="created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column(name="update_by")
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	
	@Column(name="max_rewards_per_day")
	public Integer getMaxRewardsPerDay() {
		return maxRewardsPerDay;
	}
	public void setMaxRewardsPerDay(Integer maxRewardsPerDay) {
		this.maxRewardsPerDay = maxRewardsPerDay;
	}
	
	
	@Column(name="batch_size_per_day")
	public Integer getBatchSize() {
		return batchSize;
	}
	public void setBatchSize(Integer batchSize) {
		this.batchSize = batchSize;
	}
	
	@Column(name="min_vid_play_time")
	public Integer getMinVideoPlayTime() {
		return minVideoPlayTime;
	}
	public void setMinVideoPlayTime(Integer minVideoPlayTime) {
		this.minVideoPlayTime = minVideoPlayTime;
	}
	
	@Column(name="min_reward_interval")
	public Integer getVideoRewardInterval() {
		return videoRewardInterval;
	}
	public void setVideoRewardInterval(Integer videoRewardInterval) {
		this.videoRewardInterval = videoRewardInterval;
	}
	
	@Column(name="is_display")
	public Boolean getIsDisplay() {
		return isDisplay;
	}
	public void setIsDisplay(Boolean isDisplay) {
		this.isDisplay = isDisplay;
	}
	@Transient
	public String getActiveDateStr() {
		if(activeDate!=null){
			this.activeDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(activeDate);
		}
		return activeDateStr;
	}
	public void setActiveDateStr(String activeDateStr) {
		this.activeDateStr = activeDateStr;
	}
	
	@Transient
	public String getCreatedDateStr() {
		if(createdDate!=null){
			this.createdDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(createdDate);
		}
		return createdDateStr;
	}
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	
	@Transient
	public String getUpdatedDateStr() {
		if(updatedDate!=null){
			this.updatedDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(updatedDate);
		}
		return updatedDateStr;
	}
	public void setUpdatedDateStr(String updatedDateStr) {
		this.updatedDateStr = updatedDateStr;
	}
	
	

}
