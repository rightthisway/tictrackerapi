package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.map.annotate.JsonDeserialize;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.rtw.tmat.utils.Util;

@Entity
@Table(name="affiliate_promo_code_history")
public class AffiliatePromoCodeHistory implements Serializable{

	private Integer id;
	private Integer userId;
	private String promoCode;
	@JsonDeserialize(using = com.rtw.tracker.utils.CustomerDateAndTimeDeserialize.class)
	@JsonSerialize(using = com.rtw.tracker.utils.CustomerDateAndTimeSerializer.class)
	private Date createDate;
	@JsonDeserialize(using = com.rtw.tracker.utils.CustomerDateAndTimeDeserialize.class)
	@JsonSerialize(using = com.rtw.tracker.utils.CustomerDateAndTimeSerializer.class)
	private Date updateDate;
	private String status;
	@JsonDeserialize(using = com.rtw.tracker.utils.CustomerDateAndTimeDeserialize.class)
	@JsonSerialize(using = com.rtw.tracker.utils.CustomerDateAndTimeSerializer.class)
	private Date effectiveFromDate;
	@JsonDeserialize(using = com.rtw.tracker.utils.CustomerDateAndTimeDeserialize.class)
	@JsonSerialize(using = com.rtw.tracker.utils.CustomerDateAndTimeSerializer.class)
	private Date effectiveToDate;
	
	private String fromDateStr;
	private String toDateStr;
	
	@Id
	@GeneratedValue
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="user_id")
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	@Column(name="promotional_code")
	public String getPromoCode() {
		return promoCode;
	}
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}
	
	@Column(name="create_date")
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	@Column(name="updated_date")
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="effective_from_date")
	public Date getEffectiveFromDate() {
		return effectiveFromDate;
	}
	public void setEffectiveFromDate(Date effectiveFromDate) {
		this.effectiveFromDate = effectiveFromDate;
	}
	
	@Column(name="effective_to_date")
	public Date getEffectiveToDate() {
		return effectiveToDate;
	}
	public void setEffectiveToDate(Date effectiveToDate) {
		this.effectiveToDate = effectiveToDate;
	}	

	@Transient
	public String getFromDateStr() {
		if(effectiveFromDate!=null){
			fromDateStr = Util.formatDateTimeToMonthDateYearAndHourMinute(effectiveFromDate);
		}
		return fromDateStr;
	}
	public void setFromDateStr(String fromDateStr) {
		this.fromDateStr = fromDateStr;
	}
	
	@Transient
	public String getToDateStr() {
		if(effectiveToDate!=null){
			toDateStr = Util.formatDateTimeToMonthDateYearAndHourMinute(effectiveToDate);
		}
		return toDateStr;
	}
	public void setToDateStr(String toDateStr) {
		this.toDateStr = toDateStr;
	}
	
}
