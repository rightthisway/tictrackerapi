package com.rtw.tracker.datas;

import java.util.Date;

import com.rtw.tmat.utils.Util;

public class PaymentHistory {

	String primaryPaymentMethod;
	String secondaryPaymentMethod;
	String thirdPaymentMethod;
	Double primaryAmount;
	Double secondaryAmount;
	Double thirdPayAmt;
	Date paidOn;
	String csr;
	String paidOnStr;
	String primaryTransaction;
	String secondaryTransaction;
	String thirdTransactionId;
	String cardType;
	//For RTW - Invoice 
	String creditCardNumber;
	String checkNumber;
	Double amount;
	String systemUser;
	String paymentType;
	Integer invoiceId;
	String note;
	String transOffice;
	//For RTW - Purchase Order
	Integer purchaseOrderId;
	
	public Integer getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}
	public String getCreditCardNumber() {
		return creditCardNumber;
	}
	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}
	public String getCheckNumber() {
		return checkNumber;
	}
	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}
	public Double getAmount() {
		try{
			if(amount != null){
				return Util.getRoundedValue(amount);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}	
	public String getSystemUser() {
		return systemUser;
	}
	public void setSystemUser(String systemUser) {
		this.systemUser = systemUser;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public Integer getPurchaseOrderId() {
		return purchaseOrderId;
	}
	public void setPurchaseOrderId(Integer purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}
	public String getPrimaryPaymentMethod() {
		return primaryPaymentMethod;
	}
	public void setPrimaryPaymentMethod(String primaryPaymentMethod) {
		this.primaryPaymentMethod = primaryPaymentMethod;
	}
	public String getSecondaryPaymentMethod() {
		if(secondaryPaymentMethod==null || secondaryPaymentMethod.equalsIgnoreCase("NULL")){
			secondaryPaymentMethod="";
		}
		return secondaryPaymentMethod;
	}
	public void setSecondaryPaymentMethod(String secondaryPaymentMethod) {
		this.secondaryPaymentMethod = secondaryPaymentMethod;
	}
	public Double getPrimaryAmount() {
		try{
			if(primaryAmount != null){
				return Util.getRoundedValue(primaryAmount);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return primaryAmount;
	}
	public void setPrimaryAmount(Double primaryAmount) {
		this.primaryAmount = primaryAmount;
	}
	public Double getSecondaryAmount() {
		try{
			if(secondaryAmount != null){
				return Util.getRoundedValue(secondaryAmount);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return secondaryAmount;
	}
	public void setSecondaryAmount(Double secondaryAmount) {
		this.secondaryAmount = secondaryAmount;
	}
	public Date getPaidOn() {
		return paidOn;
	}
	public void setPaidOn(Date paidOn) {
		this.paidOn = paidOn;
	}
	
	public String getCsr() {
		return csr;
	}
	public void setCsr(String csr) {
		this.csr = csr;
	}
	
	public String getPaidOnStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(paidOn);
	}
	public void setPaidOnStr(String paidOnStr) {
		this.paidOnStr = paidOnStr;
	}
	public String getPrimaryTransaction() {
		return primaryTransaction;
	}
	public void setPrimaryTransaction(String primaryTransaction) {
		this.primaryTransaction = primaryTransaction;
	}
	public String getSecondaryTransaction() {
		return secondaryTransaction;
	}
	public void setSecondaryTransaction(String secondaryTransaction) {
		this.secondaryTransaction = secondaryTransaction;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getTransOffice() {
		return transOffice;
	}
	public void setTransOffice(String transOffice) {
		this.transOffice = transOffice;
	}
	public String getThirdPaymentMethod() {
		return thirdPaymentMethod;
	}
	public void setThirdPaymentMethod(String thirdPaymentMethod) {
		this.thirdPaymentMethod = thirdPaymentMethod;
	}
	public Double getThirdPayAmt() {
		return thirdPayAmt;
	}
	public void setThirdPayAmt(Double thirdPayAmt) {
		this.thirdPayAmt = thirdPayAmt;
	}
	public String getThirdTransactionId() {
		return thirdTransactionId;
	}
	public void setThirdTransactionId(String thirdTransactionId) {
		this.thirdTransactionId = thirdTransactionId;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	
	
	
}
