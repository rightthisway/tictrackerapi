package com.rtw.tracker.datas;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.dao.implementation.DAORegistry;

@Entity
@Table(name="popular_artist")
public class PopularArtist implements Serializable{
	
	/**
	 * serialVersionUID is added only to remove warning
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private Integer artistId;
	private Date createdDate;
	private String createdBy;
	private String status;
	private String createdDateStr;
	private Integer productId;
	private String grandChildCategoryName;
	private String childCategoryName;
	private String parentType;
	private String artistName;
	
	private String imageFileUrl;
	private MultipartFile file;
	private Integer eventCount;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	
	@Column(name="artist_id")
	public Integer getArtistId() {
		return artistId;
	}
	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Column(name="product_id")
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	
	@Transient
	public String getCreatedDateStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(getCreatedDate());
	}
	
	@Transient
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	
	@Transient
	public String getGrandChildCategoryName() {
		return grandChildCategoryName;
	}
	public void setGrandChildCategoryName(String grandChildCategoryName) {
		this.grandChildCategoryName = grandChildCategoryName;
	}
	
	@Transient
	public String getChildCategoryName() {
		return childCategoryName;
	}
	public void setChildCategoryName(String childCategoryName) {
		this.childCategoryName = childCategoryName;
	}
	
	@Transient
	public String getParentType() {
		return parentType;
	}
	public void setParentType(String parentType) {
		this.parentType = parentType;
	}
	
	@Transient
	public String getArtistName() {
		return artistName;
	}
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}	

	@Transient
	public String getImageFileUrl() {
		return imageFileUrl;
	}

	public void setImageFileUrl(String imageFileUrl) {
		this.imageFileUrl = imageFileUrl;
	}
		
	@Transient
	public MultipartFile getFile() {
		return file;
	}

	@Transient
	public void setFile(MultipartFile file) {
		this.file = file;
	}
	
	@Transient
	public Integer getEventCount() {
		return eventCount;
	}
	public void setEventCount(Integer eventCount) {
		this.eventCount = eventCount;
	}
	
	
	
	
}
