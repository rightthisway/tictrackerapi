package com.rtw.tracker.datas;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.rtw.tmat.utils.Util;
import com.rtw.tracker.datas.EventStatus;

@Entity
@Table(name="event_details")
public class EventDetails implements Serializable{
	
	/**
	 * serialVersionUID is added only to remove warning
	 */
	private static final long serialVersionUID = 1L;
	protected static DateFormat dayFormat = new SimpleDateFormat("EE");
	
	private Integer eventId;
	private String eventName;
	@JsonIgnore
	private Date eventDate;
	@JsonIgnore
	private Time eventTime;
	private Integer venueId;
	private String building;
	private String city;
	private String state;
	private String country;
	private String postalCode;
	private String venueCategoryName;
	private Integer venueCategoryId;
	private EventStatus eventStatus;
	private String artistName;
	private Integer artistId;
	private String grandChildCategoryName;
	private Integer grandChildCategoryId;
	private String childCategoryName;
	private Integer childCategoryId;
	private String parentCategoryName;
	private Integer parentCategoryId;

	private Boolean isRewardTheFanEnabled;
	private Boolean isCategoryTicketEnabled;
	private Boolean isVipMinicatsEnabled;
	
	private Boolean isLastRowMinicatsEnabled;
	private Boolean isPresaleZoneTicketEnabled;
	@JsonIgnore
	private Date eventCreationDate;
	@JsonIgnore
	private Date eventLastUpdatedDate;
	private String notes;
	private Boolean displayOnSearch;
	
	private String eventDateStr;
	private String eventTimeStr;
	private String eventCreationStr;
	private String eventUpdatedStr;
	private String dayOfWeek;
	private Integer noOfTixCount = new Integer(0);
	private Integer noOfTixSoldCount;
	
	
	@Id
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="event_name")
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	@Column(name="event_date", columnDefinition="DATE")
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	
	@Column(name="event_time")
	public Time getEventTime() {
		return eventTime;
	}
	public void setEventTime(Time eventTime) {
		this.eventTime = eventTime;
	}
	
	@Column(name="venue_id")
	public Integer getVenueId() {
		return venueId;
	}
	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}
	
	@Column(name="building")
	public String getBuilding() {
		return building;
	}
	public void setBuilding(String building) {
		this.building = building;
	}
	
	@Column(name="city")
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	@Column(name="state")
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	@Column(name="country")
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	@Column(name="postal_code")
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	
	@Column(name="venue_category_name")
	public String getVenueCategoryName() {
		return venueCategoryName;
	}
	public void setVenueCategoryName(String venueCategoryName) {
		this.venueCategoryName = venueCategoryName;
	}
	
	@Column(name="venue_category_id")
	public Integer getVenueCategoryId() {
		return venueCategoryId;
	}
	public void setVenueCategoryId(Integer venueCategoryId) {
		this.venueCategoryId = venueCategoryId;
	}
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name="status")
	public EventStatus getEventStatus() {
		return eventStatus;
	}
	public void setEventStatus(EventStatus eventStatus) {
		this.eventStatus = eventStatus;
	}
	
	@Column(name="artist_name")
	public String getArtistName() {
		return artistName;
	}
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
	
	@Column(name="artist_id")
	public Integer getArtistId() {
		return artistId;
	}
	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}
	
	@Column(name="grand_child_category_name")
	public String getGrandChildCategoryName() {
		return grandChildCategoryName;
	}
	public void setGrandChildCategoryName(String grandChildCategoryName) {
		this.grandChildCategoryName = grandChildCategoryName;
	}
	
	@Column(name="grand_child_category_id")
	public Integer getGrandChildCategoryId() {
		return grandChildCategoryId;
	}
	public void setGrandChildCategoryId(Integer grandChildCategoryId) {
		this.grandChildCategoryId = grandChildCategoryId;
	}
	
	@Column(name="child_category_name")
	public String getChildCategoryName() {
		return childCategoryName;
	}
	public void setChildCategoryName(String childCategoryName) {
		this.childCategoryName = childCategoryName;
	}
	
	@Column(name="child_category_id")
	public Integer getChildCategoryId() {
		return childCategoryId;
	}
	public void setChildCategoryId(Integer childCategoryId) {
		this.childCategoryId = childCategoryId;
	}
	
	@Column(name="parent_category_name")
	public String getParentCategoryName() {
		return parentCategoryName;
	}
	public void setParentCategoryName(String parentCategoryName) {
		this.parentCategoryName = parentCategoryName;
	}
	
	@Column(name="parent_category_id")
	public Integer getParentCategoryId() {
		return parentCategoryId;
	}
	public void setParentCategoryId(Integer parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}
	
	@Column(name="reward_thefan_enabled")
	public Boolean getIsRewardTheFanEnabled() {
		return isRewardTheFanEnabled;
	}
	public void setIsRewardTheFanEnabled(Boolean isRewardTheFanEnabled) {
		this.isRewardTheFanEnabled = isRewardTheFanEnabled;
	}
	
	@Column(name="category_tickets_enabled")
	public Boolean getIsCategoryTicketEnabled() {
		return isCategoryTicketEnabled;
	}
	public void setIsCategoryTicketEnabled(Boolean isCategoryTicketEnabled) {
		this.isCategoryTicketEnabled = isCategoryTicketEnabled;
	}
	
	@Column(name="vipminicats_enabled")
	public Boolean getIsVipMinicatsEnabled() {
		return isVipMinicatsEnabled;
	}
	public void setIsVipMinicatsEnabled(Boolean isVipMinicatsEnabled) {
		this.isVipMinicatsEnabled = isVipMinicatsEnabled;
	}
	
	@Column(name="lastrow_minicat_enabled")
	public Boolean getIsLastRowMinicatsEnabled() {
		return isLastRowMinicatsEnabled;
	}
	public void setIsLastRowMinicatsEnabled(Boolean isLastRowMinicatsEnabled) {
		this.isLastRowMinicatsEnabled = isLastRowMinicatsEnabled;
	}
	
	@Column(name="presale_zonetickets_enabled")
	public Boolean getIsPresaleZoneTicketEnabled() {
		return isPresaleZoneTicketEnabled;
	}
	public void setIsPresaleZoneTicketEnabled(Boolean isPresaleZoneTicketEnabled) {
		this.isPresaleZoneTicketEnabled = isPresaleZoneTicketEnabled;
	}
	
	@Column(name="create_date", columnDefinition="DATE")
	public Date getEventCreationDate() {
		return eventCreationDate;
	}
	public void setEventCreationDate(Date eventCreationDate) {
		this.eventCreationDate = eventCreationDate;
	}
	
	@Column(name="last_update", columnDefinition="DATE")
	public Date getEventLastUpdatedDate() {
		return eventLastUpdatedDate;
	}
	public void setEventLastUpdatedDate(Date eventLastUpdatedDate) {
		this.eventLastUpdatedDate = eventLastUpdatedDate;
	}
	
	@Column(name="notes")
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	
	@Transient
	public String getFormattedVenueDescription() {
		if (this.venueId == null) {
			return "n/a";
		}
		
		String[] tokens = {
			this.building,
			this.city,
			this.state,
			this.country				
		};
		
		String v = null;
		for (String token: tokens) {
			if (token == null || token.equals("null")) {
				continue;
			}
			
			if (v == null) {
				v = token;
			} else {
				v += ", " + token;
			}
		}
		return v;
	}
	@Transient
	public String getNameWithDateandVenue() {
		String fullName = eventName;
		
		if(eventDate != null || eventDateStr != null) {
			fullName = fullName + " " + getEventDateStr() +" "+getEventTimeStr()+" "+getFormattedVenueDescription();
		} else {
			fullName = fullName + ",TBD";
		}
		return fullName;
	}
	
	@Transient
	public String getEventDateTimeStr() {
		if(eventDateStr != null && eventTimeStr != null){
			return (getEventDateStr()+" "+getEventTimeStr());
		}else if(this.eventDate == null) {
			return "TBD";
		} else {
			return (getEventDateStr()+" "+getEventTimeStr());
		}
		
	}
	
	@Transient
	public String getEventDateStr() {
		if(eventDateStr != null){
			return eventDateStr;
		}else if(getEventDate()==null){
			return "TBD";
		}
		return Util.formatDateToMonthDateYear(getEventDate());
	}
	@Transient
	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}
	@Transient
	public String getEventCreationStr() {
		if(eventCreationStr != null){
			return eventCreationStr;
		}
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(getEventCreationDate());
	}
	@Transient
	public void setEventCreationStr(String eventCreationStr) {
		this.eventCreationStr = eventCreationStr;
	}
	
	@Transient
	public String getEventUpdatedStr() {
		if(eventUpdatedStr != null){
			return eventUpdatedStr;
		}
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(getEventLastUpdatedDate());
	}
	
	@Transient
	public void setEventUpdatedStr(String eventUpdatedStr) {
		this.eventUpdatedStr = eventUpdatedStr;
	}
	
	@Transient
	public String getEventTimeStr() {
		if(eventTimeStr != null){
			return eventTimeStr;
		}else if(getEventTime()==null){
			return "TBD";
		}
		return Util.formatTimeToHourMinutes(getEventTime());
	}
	
	@Transient
	public void setEventTimeStr(String eventTimeStr) {
		this.eventTimeStr = eventTimeStr;
	}
	
	@Transient
	public String getDayOfWeek() {
		if(dayOfWeek != null){
			return dayOfWeek;
		}else if(getEventDate()!=null){
			return dayFormat.format(getEventDate());
		}
		return "";
	}
	
	@Transient
	public void setDayOfWeek(String dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}
	@Transient
	public Integer getNoOfTixCount() {
		if(noOfTixCount == null)
			return 0;
		return noOfTixCount;
	}
	
	@Transient
	public void setNoOfTixCount(Integer noOfTixCount) {
		this.noOfTixCount = noOfTixCount;
	}
	@Transient
	public Integer getNoOfTixSoldCount() {
		return noOfTixSoldCount;
	}
	@Transient
	public void setNoOfTixSoldCount(Integer noOfTixSoldCount) {
		this.noOfTixSoldCount = noOfTixSoldCount;
	}
	
	@Transient
	public Boolean getDisplayOnSearch() {
		return displayOnSearch;
	}
	public void setDisplayOnSearch(Boolean displayOnSearch) {
		this.displayOnSearch = displayOnSearch;
	}
	
	
	
}
