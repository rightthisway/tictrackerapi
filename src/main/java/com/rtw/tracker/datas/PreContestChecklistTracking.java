package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;

@Entity
@Table(name="pre_contest_checklist_tracking")
public class PreContestChecklistTracking  implements Serializable {

	private Integer id;
	private Integer preContestId;
	private String contestName;
	private String extendedName;
	private Integer contestId;
	private Date contestDateTime;
	private Double rewardDollarsPrize;
	private Integer grandWinnerTicketPrize;
	private Integer grandPrizeWinner;
	private String grandPrizeEvent;
	private Integer questionSize;
	private String categoryType;
	private String categoryName;
	private Integer categoryId;
	private String promoCategoryType;
	private String promoCategoryName;
	private Integer promoCategoryId;
	private Double pricePerTicket;
	private String discountCode;
	private Double discountPercentage;
	private String hostName;
	private String producerName;
	private String directorName;
	private String technicalDirectorName;
	private String backendOperatorName;
	private Boolean isQuestionEntered;
	private Boolean isQuestionVerified;
	private Integer questionEnteredBy;
	private Integer questionVerifiedBy;
	private Double rewardDollarsPerQuestion;
	private String zone;
	private Integer approvedBy;
	private Date createdDate;
	private String createdBy;
	private String status;
	
	private String contestDateTimeStr;
	private String contestDateStr;
	private String minutes;
	private String hours;
	private String createdDateStr;
	private String approvedByName;
	private String questionEnteredByName;
	private String questionVerifiedByName;
	
	public PreContestChecklistTracking(){};
	
	public PreContestChecklistTracking(PreContestChecklist preContest) {
		this.preContestId = preContest.getId();
		this.contestId = preContest.getContestId();
		this.contestName=preContest.getContestName();
		this.extendedName=preContest.getExtendedName();
		this.zone=preContest.getZone();
		this.grandPrizeEvent = preContest.getGrandPrizeEvent();
		this.questionSize=preContest.getQuestionSize();
		this.contestDateTime = preContest.getContestDateTime();
		this.rewardDollarsPrize = preContest.getRewardDollarsPrize();
		this.grandWinnerTicketPrize = preContest.getGrandWinnerTicketPrize();
		this.grandPrizeWinner = preContest.getGrandPrizeWinner();
		this.categoryType = preContest.getCategoryType();
		this.categoryName = preContest.getCategoryName();
		this.categoryId = preContest.getCategoryId();
		this.promoCategoryType = preContest.getPromoCategoryType();
		this.promoCategoryName = preContest.getPromoCategoryName();
		this.promoCategoryId = preContest.getPromoCategoryId();
		this.pricePerTicket = preContest.getPricePerTicket();
		this.discountCode = preContest.getDiscountCode();
		this.discountPercentage = preContest.getDiscountPercentage();
		this.hostName = preContest.getHostName();
		this.producerName = preContest.getProducerName();
		this.directorName = preContest.getDirectorName();
		this.technicalDirectorName = preContest.getTechnicalDirectorName();
		this.backendOperatorName = preContest.getBackendOperatorName();
		this.isQuestionEntered = preContest.getIsQuestionEntered();
		this.isQuestionVerified = preContest.getIsQuestionVerified();
		this.questionEnteredBy = preContest.getQuestionEnteredBy();
		this.questionVerifiedBy = preContest.getQuestionVerifiedBy();
		this.rewardDollarsPerQuestion = preContest.getRewardDollarsPerQuestion();
		this.approvedBy = preContest.getApprovedBy();
		this.createdDate = new Date();
		this.createdBy = preContest.getUpdatedBy();
		this.status=preContest.getStatus();
	}
	
	@Id
	@GeneratedValue
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="contest_id")
	public Integer getContestId() {
		return contestId;
	}
	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}
	
	@Column(name="contest_name")
	public String getContestName() {
		return contestName;
	}
	public void setContestName(String contestName) {
		this.contestName = contestName;
	}
	
	@Column(name="extended_name")
	public String getExtendedName() {
		return extendedName;
	}
	public void setExtendedName(String extendedName) {
		this.extendedName = extendedName;
	}
	
	@Column(name="grand_prize_event")
	public String getGrandPrizeEvent() {
		return grandPrizeEvent;
	}
	public void setGrandPrizeEvent(String grandPrizeEvent) {
		this.grandPrizeEvent = grandPrizeEvent;
	}
	
	@Column(name="question_size")
	public Integer getQuestionSize() {
		return questionSize;
	}
	public void setQuestionSize(Integer questionSize) {
		this.questionSize = questionSize;
	}
	
	@Column(name="zone")
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	
	
	@Column(name="pre_contest_id")
	public Integer getPreContestId() {
		return preContestId;
	}
	public void setPreContestId(Integer preContestId) {
		this.preContestId = preContestId;
	}
	
	
	@Column(name="contest_date")
	public Date getContestDateTime() {
		return contestDateTime;
	}
	public void setContestDateTime(Date contestDateTime) {
		this.contestDateTime = contestDateTime;
	}
	
	@Column(name="reward_dollars_prize")
	public Double getRewardDollarsPrize() {
		return rewardDollarsPrize;
	}
	public void setRewardDollarsPrize(Double rewardDollarsPrize) {
		this.rewardDollarsPrize = rewardDollarsPrize;
	}
	
	@Column(name="grand_prize_tickets")
	public Integer getGrandWinnerTicketPrize() {
		return grandWinnerTicketPrize;
	}
	public void setGrandWinnerTicketPrize(Integer grandWinnerTicketPrize) {
		this.grandWinnerTicketPrize = grandWinnerTicketPrize;
	}
	
	@Column(name="grand_prize_winners")
	public Integer getGrandPrizeWinner() {
		return grandPrizeWinner;
	}
	public void setGrandPrizeWinner(Integer grandPrizeWinner) {
		this.grandPrizeWinner = grandPrizeWinner;
	}
	
	@Column(name="category_type")
	public String getCategoryType() {
		return categoryType;
	}
	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}
	
	@Column(name="category_name")
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	
	@Column(name="category_id")
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	
	@Column(name="price_per_ticket")
	public Double getPricePerTicket() {
		return pricePerTicket;
	}
	public void setPricePerTicket(Double pricePerTicket) {
		this.pricePerTicket = pricePerTicket;
	}
	
	@Column(name="discount_code")
	public String getDiscountCode() {
		return discountCode;
	}
	public void setDiscountCode(String discountCode) {
		this.discountCode = discountCode;
	}
	
	@Column(name="discount_percentage")
	public Double getDiscountPercentage() {
		return discountPercentage;
	}
	public void setDiscountPercentage(Double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}
	
	@Column(name="host_name")
	public String getHostName() {
		return hostName;
	}
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	
	@Column(name="producer_name")
	public String getProducerName() {
		return producerName;
	}
	public void setProducerName(String producerName) {
		this.producerName = producerName;
	}
	
	@Column(name="director_name")
	public String getDirectorName() {
		return directorName;
	}
	public void setDirectorName(String directorName) {
		this.directorName = directorName;
	}
	
	@Column(name="technical_director_name")
	public String getTechnicalDirectorName() {
		return technicalDirectorName;
	}
	public void setTechnicalDirectorName(String technicalDirectorName) {
		this.technicalDirectorName = technicalDirectorName;
	}
	
	@Column(name="backend_operator_name")
	public String getBackendOperatorName() {
		return backendOperatorName;
	}
	public void setBackendOperatorName(String backendOperatorName) {
		this.backendOperatorName = backendOperatorName;
	}
	
	@Column(name="is_question_entered")
	public Boolean getIsQuestionEntered() {
		return isQuestionEntered;
	}
	public void setIsQuestionEntered(Boolean isQuestionEntered) {
		this.isQuestionEntered = isQuestionEntered;
	}
	
	@Column(name="is_question_verified")
	public Boolean getIsQuestionVerified() {
		return isQuestionVerified;
	}
	public void setIsQuestionVerified(Boolean isQuestionVerified) {
		this.isQuestionVerified = isQuestionVerified;
	}
	
	@Column(name="question_entered_by")
	public Integer getQuestionEnteredBy() {
		return questionEnteredBy;
	}
	public void setQuestionEnteredBy(Integer questionEnteredBy) {
		this.questionEnteredBy = questionEnteredBy;
	}
	
	@Column(name="question_verified_by")
	public Integer getQuestionVerifiedBy() {
		return questionVerifiedBy;
	}
	public void setQuestionVerifiedBy(Integer questionVerifiedBy) {
		this.questionVerifiedBy = questionVerifiedBy;
	}
	
	@Column(name="reward_dollars_per_question")
	public Double getRewardDollarsPerQuestion() {
		return rewardDollarsPerQuestion;
	}
	public void setRewardDollarsPerQuestion(Double rewardDollarsPerQuestion) {
		this.rewardDollarsPerQuestion = rewardDollarsPerQuestion;
	}
	
	@Column(name="approved_by")
	public Integer getApprovedBy() {
		return approvedBy;
	}
	public void setApprovedBy(Integer approvedBy) {
		this.approvedBy = approvedBy;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	
	@Column(name="promo_ref_type")
	public String getPromoCategoryType() {
		return promoCategoryType;
	}
	public void setPromoCategoryType(String promoCategoryType) {
		this.promoCategoryType = promoCategoryType;
	}
	
	
	@Column(name="promo_ref_name")
	public String getPromoCategoryName() {
		return promoCategoryName;
	}
	public void setPromoCategoryName(String promoCategoryName) {
		this.promoCategoryName = promoCategoryName;
	}
	
	
	@Column(name="promo_ref_id")
	public Integer getPromoCategoryId() {
		return promoCategoryId;
	}
	public void setPromoCategoryId(Integer promoCategoryId) {
		this.promoCategoryId = promoCategoryId;
	}
	
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Transient
	public String getContestDateTimeStr() {
		this.contestDateTimeStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(contestDateTime);
		return contestDateTimeStr;
	}
	public void setContestDateTimeStr(String contestDateTimeStr) {
		this.contestDateTimeStr = contestDateTimeStr;
	}
	
	@Transient
	public String getCreatedDateStr() {
		this.createdDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(createdDate);
		return createdDateStr;
	}
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	
	@Transient
	public String getApprovedByName() {
		return approvedByName;
	}
	public void setApprovedByName(String approvedByName) {
		this.approvedByName = approvedByName;
	}
	
	@Transient
	public String getQuestionEnteredByName() {
		return questionEnteredByName;
	}
	public void setQuestionEnteredByName(String questionEnteredByName) {
		this.questionEnteredByName = questionEnteredByName;
	}
	
	@Transient
	public String getQuestionVerifiedByName() {
		return questionVerifiedByName;
	}
	public void setQuestionVerifiedByName(String questionVerifiedByName) {
		this.questionVerifiedByName = questionVerifiedByName;
	}
	
	@Transient
	public String getContestDateStr() {
		if(contestDateTime!=null){
			contestDateStr = Util.formatDateToMonthDateYear(contestDateTime);
		}
		 
		return contestDateStr;
	}
	public void setContestDateStr(String contestDateStr) {
		this.contestDateStr = contestDateStr;
	}
	
	@Transient
	public String getMinutes() {
		if(contestDateTime!=null){
			this.minutes = Util.getMinutesFromDate(contestDateTime);
		}
		return minutes;
	}
	public void setMinutes(String minutes) {
		this.minutes = minutes;
	}
	
	@Transient
	public String getHours() {
		if(contestDateTime!=null){
			this.hours = Util.getHourFromDate(contestDateTime);
		}
		return hours;
	}
	public void setHours(String hours) {
		this.hours = hours;
	}
	

}
