package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="customer_comments_on_media")
public class CustomerComment implements Serializable{
	
	private Integer id;
	private Integer customerId;
	private Integer mediaId;
	private String commentText;
	private String commentMediaUrl;
	private String commentPosterUrl;
	private String commentImageUrl;
	private Date createdDate;
	private Boolean hideComment;
	private Date hideDate;
	private String hideBy;
	private String hideReason;
	private Boolean bolckStatus;
	private Date blockDate;
	private String blockBy;
	private String blockReason;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Column(name="media_id")
	public Integer getMediaId() {
		return mediaId;
	}
	public void setMediaId(Integer mediaId) {
		this.mediaId = mediaId;
	}
	
	@Column(name="cust_comment_text")
	public String getCommentText() {
		return commentText;
	}
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}
	
	@Column(name="cust_comment_media_url")
	public String getCommentMediaUrl() {
		return commentMediaUrl;
	}
	public void setCommentMediaUrl(String commentMediaUrl) {
		this.commentMediaUrl = commentMediaUrl;
	}
	
	@Column(name="cust_comment_poster_url	")
	public String getCommentPosterUrl() {
		return commentPosterUrl;
	}
	public void setCommentPosterUrl(String commentPosterUrl) {
		this.commentPosterUrl = commentPosterUrl;
	}
	
	@Column(name="cust_comment_image_url")
	public String getCommentImageUrl() {
		return commentImageUrl;
	}
	public void setCommentImageUrl(String commentImageUrl) {
		this.commentImageUrl = commentImageUrl;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="hide_comment")
	public Boolean getHideComment() {
		return hideComment;
	}
	public void setHideComment(Boolean hideComment) {
		this.hideComment = hideComment;
	}
	
	@Column(name="hide_date")
	public Date getHideDate() {
		return hideDate;
	}
	public void setHideDate(Date hideDate) {
		this.hideDate = hideDate;
	}
	
	@Column(name="hide_by")
	public String getHideBy() {
		return hideBy;
	}
	public void setHideBy(String hideBy) {
		this.hideBy = hideBy;
	}
	
	@Column(name="hide_reason")
	public String getHideReason() {
		return hideReason;
	}
	public void setHideReason(String hideReason) {
		this.hideReason = hideReason;
	}
	
	@Column(name="block_status")
	public Boolean getBolckStatus() {
		return bolckStatus;
	}
	public void setBolckStatus(Boolean bolckStatus) {
		this.bolckStatus = bolckStatus;
	}
	
	@Column(name="block_date")
	public Date getBlockDate() {
		return blockDate;
	}
	public void setBlockDate(Date blockDate) {
		this.blockDate = blockDate;
	}
	
	@Column(name="block_by")
	public String getBlockBy() {
		return blockBy;
	}
	public void setBlockBy(String blockBy) {
		this.blockBy = blockBy;
	}
	
	@Column(name="block_reason")
	public String getBlockReason() {
		return blockReason;
	}
	public void setBlockReason(String blockReason) {
		this.blockReason = blockReason;
	}

}
