package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;

@Entity
@Table(name="referral_contest_participants")
public class ReferralContestParticipants implements Serializable{

	private Integer id;
	private Integer contestId;
	private Integer customerId;
	private Integer purchaseCustomerId;
	private String referralCode;
	private Date createdDate;
	private Boolean isEmailed;
	
	private String customerName;
	private String customerEmail;
	private String purchaseCustomerName;
	private String purchaseCustomerEmail;
	
	private String createdDateStr;

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="contest_id")
	public Integer getContestId() {
		return contestId;
	}
	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}

	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	@Column(name="purchase_customer_id")
	public Integer getPurchaseCustomerId() {
		return purchaseCustomerId;
	}
	public void setPurchaseCustomerId(Integer purchaseCustomerId) {
		this.purchaseCustomerId = purchaseCustomerId;
	}

	@Column(name="referral_code")
	public String getReferralCode() {
		return referralCode;
	}
	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}

	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="is_emailed")
	public Boolean getIsEmailed() {
		return isEmailed;
	}
	public void setIsEmailed(Boolean isEmailed) {
		this.isEmailed = isEmailed;
	}
	
	@Transient
	public String getCreatedDateStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(createdDate);
	}
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	
	@Transient
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	@Transient
	public String getCustomerEmail() {
		return customerEmail;
	}
	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}
	
	@Transient
	public String getPurchaseCustomerName() {
		return purchaseCustomerName;
	}
	public void setPurchaseCustomerName(String purchaseCustomerName) {
		this.purchaseCustomerName = purchaseCustomerName;
	}
	
	@Transient
	public String getPurchaseCustomerEmail() {
		return purchaseCustomerEmail;
	}
	public void setPurchaseCustomerEmail(String purchaseCustomerEmail) {
		this.purchaseCustomerEmail = purchaseCustomerEmail;
	}
	
	
}
