package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="pos_category_ticket")
public class POSCategoryTicket implements Serializable{
		
	private String productType;
	private Integer productId;
	private Integer categoryTicketId;
	private Integer categoryTicketGroupId;
	private Integer position;
	private Double actualPrice;
	private Integer invoiceId;
	private Integer ticketId;
	private Boolean processed;
	private Integer systemUserId;
	private Integer exchangeRequestId;
	private Date fillDate;
	private Double wholesalePriceAtTimeOfSale;
	
	public POSCategoryTicket(){
		
	}
	
	@Column(name = "product_type")
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	
	@Column(name = "product_id")
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	
	@Id
	@Column(name = "category_ticket_id")
	public Integer getCategoryTicketId() {
		return categoryTicketId;
	}
	public void setCategoryTicketId(Integer categoryTicketId) {
		this.categoryTicketId = categoryTicketId;
	}
	
	@Column(name = "category_ticket_group_id")
	public Integer getCategoryTicketGroupId() {
		return categoryTicketGroupId;
	}
	public void setCategoryTicketGroupId(Integer categoryTicketGroupId) {
		this.categoryTicketGroupId = categoryTicketGroupId;
	}
	
	@Column(name = "position")
	public Integer getPosition() {
		return position;
	}
	public void setPosition(Integer position) {
		this.position = position;
	}
	
	@Column(name = "actual_price")
	public Double getActualPrice() {
		return actualPrice;
	}
	public void setActualPrice(Double actualPrice) {
		this.actualPrice = actualPrice;
	}
	
	@Column(name = "invoice_id")
	public Integer getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}
	
	@Column(name = "ticket_id")
	public Integer getTicketId() {
		return ticketId;
	}
	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}
	
	@Column(name = "processed")
	public Boolean getProcessed() {
		return processed;
	}
	public void setProcessed(Boolean processed) {
		this.processed = processed;
	}
	
	@Column(name = "system_user_id")
	public Integer getSystemUserId() {
		return systemUserId;
	}
	public void setSystemUserId(Integer systemUserId) {
		this.systemUserId = systemUserId;
	}
	
	@Column(name = "exchange_request_id")
	public Integer getExchangeRequestId() {
		return exchangeRequestId;
	}
	public void setExchangeRequestId(Integer exchangeRequestId) {
		this.exchangeRequestId = exchangeRequestId;
	}
	
	@Column(name = "fill_date")
	public Date getFillDate() {
		return fillDate;
	}
	public void setFillDate(Date fillDate) {
		this.fillDate = fillDate;
	}
	
	@Column(name = "wholesale_price_at_time_of_sale")
	public Double getWholesalePriceAtTimeOfSale() {
		return wholesalePriceAtTimeOfSale;
	}
	public void setWholesalePriceAtTimeOfSale(Double wholesalePriceAtTimeOfSale) {
		this.wholesalePriceAtTimeOfSale = wholesalePriceAtTimeOfSale;
	}
	
}
