package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;


@Entity
@Table(name="crownjewel_category_teams")
public class CrownJewelCategoryTeams implements Serializable{

	private Integer id;
	private String name;
	private Double odds;
	private Date cutOffDate;
	private String lastUpdatedBy;
	private Date lastUpdated;
	private String status;
	private String lastUpdatedStr;
	private String fractionalOdd;
	private Integer markup;
	private String cutOffDateStr;
	private Integer grandChildId;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="odds")
	public Double getOdds() {
		return odds;
	}
	public void setOdds(Double odds) {
		this.odds = odds;
	}
	
	@Column(name="cutoff_date")
	public Date getCutOffDate() {
		return cutOffDate;
	}
	public void setCutOffDate(Date cutOffDate) {
		this.cutOffDate = cutOffDate;
	}
	
	@Column(name="last_updated_by")
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdateddBy) {
		this.lastUpdatedBy = lastUpdateddBy;
	}
	
	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="fractional_odds")
	public String getFractionalOdd() {
		return fractionalOdd;
	}
	public void setFractionalOdd(String fractionalOdd) {
		this.fractionalOdd = fractionalOdd;
	}
	
	@Column(name="markup")
	public Integer getMarkup() {
		return markup;
	}
	public void setMarkup(Integer markup) {
		this.markup = markup;
	}
	
	@Column(name="grand_child_id")
	public Integer getGrandChildId() {
		return grandChildId;
	}
	public void setGrandChildId(Integer grandChildId) {
		this.grandChildId = grandChildId;
	}
	
	@Transient
	public String getLastUpdatedStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(getLastUpdated());
	}
	public void setLastUpdatedStr(String lastUpdatedStr) {
		this.lastUpdatedStr = lastUpdatedStr;
	}
	
	@Transient
	public String getCutOffDateStr() {
		if(cutOffDate!=null){
			cutOffDateStr = Util.formatDateToMonthDateYear(cutOffDate);
		}
		return cutOffDateStr;
	}
	public void setCutOffDateStr(String cutOffDateStr) {
		this.cutOffDateStr = cutOffDateStr;
	}
	
	
	
}
