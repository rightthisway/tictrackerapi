package com.rtw.tracker.datas;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.dao.implementation.DAORegistry;

@Entity
@Table(name="crownjewel_team_zones_audit")
public class CrownJewelTeamZonesAudit implements Serializable{
	
	/**
	 * serialVersionUID is added only to remove warning
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private Integer teamId;
	private Integer eventId;
	private CrownJewelTeams team;
	private String zone;
	private Integer ticketsCount;
	private Integer soldTicketsCount=0;
	private Integer redeemedPoints;
	private String lastUpdateddBy;
	private Date lastUpdated;
	private String status;
	private String lastUpdatedStr;
	private EventDetails event;
	private Integer availableTixCount=0;
	private Integer pointsRedeemed=0;
	private String action;
	
	public CrownJewelTeamZonesAudit() {
		
	}
	public CrownJewelTeamZonesAudit(CrownJewelTeamZones cjTeamZone) {
		this.zone=cjTeamZone.getZone();
		this.eventId=cjTeamZone.getEventId();
		this.teamId=cjTeamZone.getTeamId();
		this.ticketsCount=cjTeamZone.getTicketsCount();
		//this.soldTicketsCount=cjTeamZone.getSoldTicketsCount();
		this.lastUpdated=cjTeamZone.getLastUpdated();
		this.lastUpdateddBy=cjTeamZone.getLastUpdateddBy();
		this.status=cjTeamZone.getStatus();
	}
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="team_id")
	public Integer getTeamId() {
		return teamId;
	}
	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="zone")
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	@Transient
	public Integer getSoldTicketsCount() {
		return soldTicketsCount;
	}
	public void setSoldTicketsCount(Integer soldTicketsCount) {
		this.soldTicketsCount = soldTicketsCount;
	}
	@Transient
	public Integer getRedeemedPoints() {
		return redeemedPoints;
	}
	public void setRedeemedPoints(Integer redeemedPoints) {
		this.redeemedPoints = redeemedPoints;
	}
	@Column(name="last_updated_by")
	public String getLastUpdateddBy() {
		return lastUpdateddBy;
	}
	public void setLastUpdateddBy(String lastUpdateddBy) {
		this.lastUpdateddBy = lastUpdateddBy;
	}
	
	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Column(name="tickets_count")
	public Integer getTicketsCount() {
		return ticketsCount;
	}
	public void setTicketsCount(Integer ticketsCount) {
		this.ticketsCount = ticketsCount;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Column(name="action")
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	@Transient
	public String getLastUpdatedStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(getLastUpdated());
	}
	
	@Transient
	public void setLastUpdatedStr(String lastUpdatedStr) {
		this.lastUpdatedStr = lastUpdatedStr;
	}
	@Transient
	public EventDetails getEvent() {
		if (eventId == null) {
			return null;
		}
		if (event == null) {
			event = DAORegistry.getEventDetailsDAO().get(eventId); 
		}
		return event;
	}
	@Transient
	public void setEvent(EventDetails event) {
		this.event = event;
	}
	@Transient
	public CrownJewelTeams getTeam() {
		if (teamId == null) {
			return null;
		}
		if (team == null) {
			team = DAORegistry.getCrownJewelTeamsDAO().get(teamId); 
		}
		return team;
	}
	@Transient
	public void setTeam(CrownJewelTeams team) {
		this.team = team;
	}
	@Transient
	public Integer getAvailableTixCount() {
		return availableTixCount;
	}
	@Transient
	public void setAvailableTixCount(Integer availableTixCount) {
		this.availableTixCount = availableTixCount;
	}
	@Transient
	public Integer getPointsRedeemed() {
		return pointsRedeemed;
	}
	@Transient
	public void setPointsRedeemed(Integer pointsRedeemed) {
		this.pointsRedeemed = pointsRedeemed;
	}
	
}
