package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.rtw.tracker.enums.OrderType;
import com.rtw.tracker.enums.RewardStatus;
import com.thoughtworks.xstream.annotations.XStreamAlias;


/**
 * represents customer entity
 * @author ulaganathan
 *
 */
@XStreamAlias("CustomerLoyaltyHistory")
@Entity
@Table(name="cust_loyalty_reward_history")
public class CustomerLoyaltyHistory  implements Serializable{
	
	private Integer id;
	private Integer customerId;
	private Integer orderId;
	private Double orderTotal;
	private Double pointsEarned;
	private Double pointsSpent;
	private Double rewardConversionRate;
	private Double rewardEarnAmount;
	private Double rewardSpentAmount;
	private Double initialRewardPoints;
	private Double balanceRewardPoints;
	private Double initialRewardAmount;
	private Double balanceRewardAmount;
	private Date createDate;
	private Date updatedDate;
	private Double dollarConversionRate;
	private OrderType orderType;
	private RewardStatus rewardStatus;
	@JsonIgnore
	private String eventDate;
	@JsonIgnore
	private String eventTime;
	
                                    
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id ,Id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Column(name="Order_id")
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	
	@Column(name="order_total")
	public Double getOrderTotal() {
		return orderTotal;
	}
	public void setOrderTotal(Double orderTotal) {
		this.orderTotal = orderTotal;
	}
	
	
	@Column(name="points_earned")
	public Double getPointsEarned() {
		return pointsEarned;
	}
	public void setPointsEarned(Double pointsEarned) {
		this.pointsEarned = pointsEarned;
	}
	
	@Column(name="points_spent")
	public Double getPointsSpent() {
		return pointsSpent;
	}
	public void setPointsSpent(Double pointsSpent) {
		this.pointsSpent = pointsSpent;
	}
	
	@Column(name="reward_conv_rate")
	public Double getRewardConversionRate() {
		return rewardConversionRate;
	}
	public void setRewardConversionRate(Double rewardConversionRate) {
		this.rewardConversionRate = rewardConversionRate;
	}
	
	@Column(name="reward_earn_amount")
	public Double getRewardEarnAmount() {
		return rewardEarnAmount;
	}
	public void setRewardEarnAmount(Double rewardEarnAmount) {
		this.rewardEarnAmount = rewardEarnAmount;
	}
	
	@Column(name="reward_spent_amount")
	public Double getRewardSpentAmount() {
		return rewardSpentAmount;
	}
	public void setRewardSpentAmount(Double rewardSpentAmount) {
		this.rewardSpentAmount = rewardSpentAmount;
	}
	
	@Column(name="initial_reward_points")
	public Double getInitialRewardPoints() {
		return initialRewardPoints;
	}
	public void setInitialRewardPoints(Double initialRewardPoints) {
		this.initialRewardPoints = initialRewardPoints;
	}
	
	@Column(name="balance_reward_points")
	public Double getBalanceRewardPoints() {
		return balanceRewardPoints;
	}
	public void setBalanceRewardPoints(Double balanceRewardPoints) {
		this.balanceRewardPoints = balanceRewardPoints;
	}
	
	@Column(name="initial_reward_amount")
	public Double getInitialRewardAmount() {
		return initialRewardAmount;
	}
	public void setInitialRewardAmount(Double initialRewardAmount) {
		this.initialRewardAmount = initialRewardAmount;
	}
	
	@Column(name="balance_reward_amount")
	public Double getBalanceRewardAmount() {
		return balanceRewardAmount;
	}
	public void setBalanceRewardAmount(Double balanceRewardAmount) {
		this.balanceRewardAmount = balanceRewardAmount;
	}
	
	
	
	@Column(name = "dollar_conv_rate")
	public Double getDollarConversionRate() {
		return dollarConversionRate;
	}
	public void setDollarConversionRate(Double dollarConversionRate) {
		this.dollarConversionRate = dollarConversionRate;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="order_type")
	public OrderType getOrderType() {
		return orderType;
	}
	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	public RewardStatus getRewardStatus() {
		return rewardStatus;
	}
	public void setRewardStatus(RewardStatus rewardStatus) {
		this.rewardStatus = rewardStatus;
	}
	
	@Column(name="updated_time")
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@Column(name="created_date")
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	@Transient
	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	
	@Transient
	public String getEventTime() {
		return eventTime;
	}
	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}
	
	
}
