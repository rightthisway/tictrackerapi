package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;

@Entity
@Table(name="fanclub_customer_reported_abuse_on_media")
public class FanclubAbuseReported implements Serializable{

	private Integer id;
	private Integer customerId;
	private Integer fanclubId;
	private Integer postId;
	private Integer videoId;
	private Integer eventId;
	private Integer commentId;
	private Integer abuseId;
	private String createdUserId;
	private String updatedUserId;
	private Date createdDate;
	private Date updatedDate;
	
	
	private String email;
	private String phone;
	private String userId;
	private String reportedByEmail;
	private String reportedByPhone;
	private String reportedByUserId;
	private String createdDateStr;
	private String updatedDateStr;
	
	private String title;
	private String description;
	private String posterUrl;
	private String videoUrl;
	private String imageUrl;
	private String abuseType;
	private String status;
	private Date date;
	private String dateStr;
	private Integer count;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Column(name="fanclub_id")
	public Integer getFanclubId() {
		return fanclubId;
	}
	public void setFanclubId(Integer fanclubId) {
		this.fanclubId = fanclubId;
	}
	
	@Column(name="fanclub_posts_id")
	public Integer getPostId() {
		return postId;
	}
	
	public void setPostId(Integer postId) {
		this.postId = postId;
	}
	
	@Column(name="fanclub_video_id")
	public Integer getVideoId() {
		return videoId;
	}
	public void setVideoId(Integer videoId) {
		this.videoId = videoId;
	}
	
	@Column(name="fanclub_event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="fanclub_comments_id")
	public Integer getCommentId() {
		return commentId;
	}
	public void setCommentId(Integer commentId) {
		this.commentId = commentId;
	}
	
	@Column(name="abuse_id")
	public Integer getAbuseId() {
		return abuseId;
	}
	public void setAbuseId(Integer abuseId) {
		this.abuseId = abuseId;
	}
	
	@Column(name="created_user_id")
	public String getCreatedUserId() {
		return createdUserId;
	}
	public void setCreatedUserId(String createdUserId) {
		this.createdUserId = createdUserId;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="updated_date")
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@Transient
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Transient
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@Transient
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	
	
	@Transient
	public String getCreatedDateStr() {
		if(createdDate!=null){
			this.createdDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(createdDate);
		}
		return createdDateStr;
	}
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	
	@Transient
	public String getUpdatedDateStr() {
		if(updatedDate!=null){
			this.updatedDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(updatedDate);
		}
		return updatedDateStr;
	}
	public void setUpdatedDateStr(String updatedDateStr) {
		this.updatedDateStr = updatedDateStr;
	}
	
	@Transient
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	@Transient
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Transient
	public String getPosterUrl() {
		return posterUrl;
	}
	public void setPosterUrl(String posterUrl) {
		this.posterUrl = posterUrl;
	}
	
	@Transient
	public String getVideoUrl() {
		return videoUrl;
	}
	public void setVideoUrl(String videoUrl) {
		this.videoUrl = videoUrl;
	}
	
	@Transient
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	
	@Transient
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Transient
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	@Transient
	public String getDateStr() {
		if(date!=null){
			this.dateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(date);
		}
		return dateStr;
	}
	public void setDateStr(String dateStr) {
		this.dateStr = dateStr;
	}
	
	@Transient
	public String getReportedByEmail() {
		return reportedByEmail;
	}
	public void setReportedByEmail(String reportedByEmail) {
		this.reportedByEmail = reportedByEmail;
	}
	
	@Transient
	public String getReportedByPhone() {
		return reportedByPhone;
	}
	public void setReportedByPhone(String reportedByPhone) {
		this.reportedByPhone = reportedByPhone;
	}
	
	@Transient
	public String getReportedByUserId() {
		return reportedByUserId;
	}
	public void setReportedByUserId(String reportedByUserId) {
		this.reportedByUserId = reportedByUserId;
	}
	
	@Transient
	public String getAbuseType() {
		return abuseType;
	}
	public void setAbuseType(String abuseType) {
		this.abuseType = abuseType;
	}
	
	@Transient
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	
	
	@Column(name="updated_user_id")
	public String getUpdatedUserId() {
		return updatedUserId;
	}
	public void setUpdatedUserId(String updatedUserId) {
		this.updatedUserId = updatedUserId;
	}
	
	
	
}
