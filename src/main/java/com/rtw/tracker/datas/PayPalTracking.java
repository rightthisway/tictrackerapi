package com.rtw.tracker.datas;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;

@Entity
@Table(name = "paypal_tracking")
public class PayPalTracking implements Serializable{

	private Integer id;
	private Integer customerId;
	private Integer eventId;
	private Integer categoryTicketGroupId;
	private Integer quantity;
	private String zone;
	private Double orderTotal;
	private String orderType;
	private String platform;
	private String status;
	private String paypalTransactionId;
	private String sessionId;
	private Integer transactionId;
	private Date createdDate;
	private Date lastUpdated;
	
	private Integer orderId;
	private String paymentId;
	private String payerEmail;
	private String payerFirstName;
	private String payerLastName;
	private String payerPhone;
	private String transactionDate;
	
	private String createdDateStr;
	private String lastUpdatedStr;
	private String transactionDateStr;
	
	public PayPalTracking(){
		
	}
	
	@Id
	@GeneratedValue
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="category_ticket_group_id")
	public Integer getCategoryTicketGroupId() {
		return categoryTicketGroupId;
	}
	public void setCategoryTicketGroupId(Integer categoryTicketGroupId) {
		this.categoryTicketGroupId = categoryTicketGroupId;
	}
	
	@Column(name="qty")
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	@Column(name="zone")
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	
	@Column(name="order_total")
	public Double getOrderTotal() {
		return orderTotal;
	}
	public void setOrderTotal(Double orderTotal) {
		this.orderTotal = orderTotal;
	}
	
	@Column(name="order_type")
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	
	@Column(name="platform")
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="paypal_transaction_id")
	public String getPaypalTransactionId() {
		return paypalTransactionId;
	}
	public void setPaypalTransactionId(String paypalTransactionId) {
		this.paypalTransactionId = paypalTransactionId;
	}
	
	@Column(name="session_id")
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	@Column(name="transaction_id")
	public Integer getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(Integer transactionId) {
		this.transactionId = transactionId;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Transient
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	
	@Transient
	public String getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
	
	@Transient
	public String getPayerEmail() {
		return payerEmail;
	}
	public void setPayerEmail(String payerEmail) {
		this.payerEmail = payerEmail;
	}
	
	@Transient
	public String getPayerFirstName() {
		return payerFirstName;
	}
	public void setPayerFirstName(String payerFirstName) {
		this.payerFirstName = payerFirstName;
	}
	
	@Transient
	public String getPayerLastName() {
		return payerLastName;
	}
	public void setPayerLastName(String payerLastName) {
		this.payerLastName = payerLastName;
	}
	
	@Transient
	public String getPayerPhone() {
		return payerPhone;
	}
	public void setPayerPhone(String payerPhone) {
		this.payerPhone = payerPhone;
	}
	
	@Transient
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	@Transient
	public String getCreatedDateStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(createdDate);
	}

	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}

	@Transient
	public String getLastUpdatedStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(lastUpdated);
	}

	public void setLastUpdatedStr(String lastUpdatedStr) {
		this.lastUpdatedStr = lastUpdatedStr;
	}	

	@Transient
	public String getTransactionDateStr() {
		Date transactionDt = null;
		if(transactionDate != null){
			String transactionDateArr[] = transactionDate.split("T");
			String date = transactionDateArr[0];
			String time = transactionDateArr[1].replace("Z", "");
			DateFormat formatDateTimeZone = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			try{
				transactionDt = formatDateTimeZone.parse(date+" "+time);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(transactionDt);
	}

	public void setTransactionDateStr(String transactionDateStr) {
		this.transactionDateStr = transactionDateStr;
	}

}
