package com.rtw.tracker.datas;

import java.io.Serializable;
import java.sql.Time;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;

@Entity
@Table(name="tix_open_order_status")
public class OpenOrderStatus implements Serializable {
	
	Integer id;
	Integer tmatEventId;
	Integer exEventId;
	Integer sectionTixQty;
	Integer eventTixQty;
	String section;
	String row;
	String category;
	Integer ticketRequestId;
	Double marketPrice; 
	Double onlinePrice; 
	Double lastUpdatedPrice;
	Double profitAndLoss;
	Date lastCrawlTime;
	Date createdDate;
	Date lastUpdated;
	String status;
	String customerName;
	Integer brokerId;
	Integer invoiceNo;
	Date invoiceDate;
	Integer venueId;
	Integer soldQty;
	Integer ticketGroupId;
	Integer priceUpdateCount;
	
	String brokerName;
	String invoiceType;
	String salesPerson;
	String eventName;
	Date eventDate;
	Time eventTime;
	String venueName;
	String venueCity;
	String venueState;
	String venueCountry;
	String internalNotes;
	Boolean isFocusRow;
	
	Double retailPrice; 
	Double wholesalePrice; 
	Double cost; 
	Double actualSoldPrice; 
	Double totalPrice;
	Double totalActualSoldPrice; 
	Double totalMarketPrice;
	String productType;
	
	String zone;
	Integer zoneTixQty;
	Double zoneCheapestPrice;
	Integer zoneTixGroupCount;
	Double zoneTotalPrice;
	Double zoneProfitAndLoss;
	Double netTotalSoldPrice;
	Double zoneMargin;
	Double sectionMargin;
	Double netSoldPrice;
	
	String eventDateStr;
	String eventTimeStr;
	String invoiceDateStr;
	Integer poId;
	Date poDate;
	String poDateStr;
	String lastUpdateStr;
	String shippingMethod;
	String trackingNo;
	Integer orderId;
	String secondaryOrderId;
	String secondaryOrderType;
	String orderType;

	String companyName;
	String platform;
	
	DecimalFormat df = new DecimalFormat("#.##");
	public static Integer TN_FEES = 12; 
	
	@Column(name="price", nullable=true)
	private Double price;
	
	@Column(name="discount_coupon_price", nullable=true)
	private Double discountCouponPrice;
	
	@Column(name="url", nullable=true)
	private String url;
	
	@Column(name="last_updated_price_date", nullable=true)
	private Date lastUpdatedPriceDate;
	
	/*@Column(name="is_price_updated", nullable =true)
	private boolean priceUpdated;*/
	
	public OpenOrderStatus(){
		
	}
	
	public OpenOrderStatus(SoldTicketDetail ticketDetail){
		setTicketGroupId(ticketDetail.getId());
		setBrokerId(ticketDetail.getBrokerId());
		//setBrokerName(ticketDetail.getBrokerName());
		setInvoiceNo(ticketDetail.getInvoiceId());
		setInvoiceDate(ticketDetail.getInvoiceDateTime());
		setInvoiceType(ticketDetail.getInvoiceType());
		setSalesPerson(ticketDetail.getSalesPerson());
		setExEventId(ticketDetail.getExEventId());
		setEventName(ticketDetail.getEventName());
		setEventDate(ticketDetail.getEventDate());
		setEventTime(ticketDetail.getEventTime());
		//setVenueId(venueId);
		setVenueName(ticketDetail.getVenueName());
		setVenueCity(ticketDetail.getVenueCity());
		setVenueState(ticketDetail.getVenueState());
		setVenueCountry(ticketDetail.getVenueCountry());
		setSection(ticketDetail.getSection());
		setRow(ticketDetail.getRow());
		setRetailPrice(ticketDetail.getRetailPrice());
		setWholesalePrice(ticketDetail.getWholesalePrice());
		setCost(ticketDetail.getCost());
		setActualSoldPrice(ticketDetail.getActualSoldPrice());
		setTotalPrice(ticketDetail.getTotalPrice());
		setInternalNotes(ticketDetail.getInternalNotes());
		setSoldQty(ticketDetail.getSoldQty());
		setProductType(ticketDetail.getProductType());
		setTicketRequestId(ticketDetail.getTicketRequestId());
		
		computeNetTotalSoldPrice();
		computeNetSoldPrice();
	}
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="event_id")
	public Integer getTmatEventId() {
		return tmatEventId;
	}

	public void setTmatEventId(Integer tmatEventId) {
		this.tmatEventId = tmatEventId;
	}
	
	@Column(name="tn_exchange_event_id")
	public Integer getExEventId() {
		return exEventId;
	}
	

	public void setExEventId(Integer exEventId) {
		this.exEventId = exEventId;
	}
	
	@Column(name="section_ticket_qty")
	public Integer getSectionTixQty() {
		return sectionTixQty;
	}
	public void setSectionTixQty(Integer sectionTixQty) {
		this.sectionTixQty = sectionTixQty;
	}
	
	
	@Column(name="event_ticket_qty")
	public Integer getEventTixQty() {
		return eventTixQty;
	}
	public void setEventTixQty(Integer eventTixQty) {
		this.eventTixQty = eventTixQty;
	}
	
	@Column(name="section")
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	
	@Column(name="market_price")
	public Double getMarketPrice() {
		return marketPrice;
	}
	public void setMarketPrice(Double marketPrice) {
		this.marketPrice = marketPrice;
	}
	
	@Column(name="ticket_request_id")
	public Integer getTicketRequestId() {
		return ticketRequestId;
	}
	public void setTicketRequestId(Integer ticketRequestId) {
		this.ticketRequestId = ticketRequestId;
	}
	
	@Column(name="last_updated_price")
	public Double getLastUpdatedPrice() {
		return lastUpdatedPrice;
	}
	public void setLastUpdatedPrice(Double lastUpdatedPrice) {
		this.lastUpdatedPrice = lastUpdatedPrice;
	}
	
	@Column(name="last_crawl_time")
	public Date getLastCrawlTime() {
		return lastCrawlTime;
	}
	public void setLastCrawlTime(Date lastCrawlTime) {
		this.lastCrawlTime = lastCrawlTime;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="category")
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
	
	@Column(name="broker_id")
	public Integer getBrokerId() {
		return brokerId;
	}
	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}
	
	@Column(name="invoice_no")
	public Integer getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(Integer invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	
	@Column(name="invoice_date")
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	
	@Column(name="venue_id")
	public Integer getVenueId() {
		return venueId;
	}
	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}
	
	@Column(name="sold_qty")
	public Integer getSoldQty() {
		return soldQty;
	}
	public void setSoldQty(Integer soldQty) {
		this.soldQty = soldQty;
	}
	
	@Column(name="ticket_group_id")
	public Integer getTicketGroupId() {
		return ticketGroupId;
	}
	public void setTicketGroupId(Integer ticketGroupId) {
		this.ticketGroupId = ticketGroupId;
	}
	
	@Column(name="broker_name")
	public String getBrokerName() {
		return brokerName;
	}
	public void setBrokerName(String brokerName) {
		this.brokerName = brokerName;
	}
	
	@Column(name="invoice_type")
	public String getInvoiceType() {
		return invoiceType;
	}
	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}
	
	@Column(name="sales_person")
	public String getSalesPerson() {
		return salesPerson;
	}
	public void setSalesPerson(String salesPerson) {
		this.salesPerson = salesPerson;
	}
	
	@Column(name="event_name")
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	@Column(name="event_date")
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	
	@Column(name="venue_name")
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	
	
	@Column(name="venue_city")
	public String getVenueCity() {
		return venueCity;
	}
	public void setVenueCity(String venueCity) {
		this.venueCity = venueCity;
	}
	
	@Column(name="venue_state")
	public String getVenueState() {
		return venueState;
	}
	public void setVenueState(String venueState) {
		this.venueState = venueState;
	}
	
	@Column(name="venue_country")
	public String getVenueCountry() {
		return venueCountry;
	}
	public void setVenueCountry(String venueCountry) {
		this.venueCountry = venueCountry;
	}
	
	@Column(name="internal_notes")
	public String getInternalNotes() {
		return internalNotes;
	}
	public void setInternalNotes(String internalNotes) {
		this.internalNotes = internalNotes;
	}
	
	@Column(name="retail_price")
	public Double getRetailPrice() {
		return retailPrice;
	}
	public void setRetailPrice(Double retailPrice) {
		this.retailPrice = retailPrice;
	}
	
	@Column(name="wholesale_price")
	public Double getWholesalePrice() {
		return wholesalePrice;
	}
	public void setWholesalePrice(Double wholesalePrice) {
		this.wholesalePrice = wholesalePrice;
	}
	
	@Column(name="cost")
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	
	@Column(name="actual_sold_price")
	public Double getActualSoldPrice() {
		return actualSoldPrice;
	}
	public void setActualSoldPrice(Double actualSoldPrice) {
		this.actualSoldPrice = actualSoldPrice;
	}
	
	@Column(name="total_price")
	public Double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	@Column(name="row")
	public String getRow() {
		return row;
	}

	public void setRow(String row) {
		this.row = row;
	}

	@Column(name="profit_loss")
	public Double getProfitAndLoss() {
		if(null != profitAndLoss){
			profitAndLoss = Double.valueOf(df.format(profitAndLoss));
		}
		return profitAndLoss;
	}

	public void setProfitAndLoss(Double profitAndLoss) {
		this.profitAndLoss = profitAndLoss;
	}
	
	
	@Transient
	public Double getTotalActualSoldPrice() {
		if(soldQty != null){
			totalActualSoldPrice = Double.valueOf(df.format(soldQty * actualSoldPrice));
		}
		return totalActualSoldPrice;
	}

	public void setTotalActualSoldPrice(Double totalActualSoldPrice) {
		this.totalActualSoldPrice = totalActualSoldPrice;
	}

	@Transient
	public Double getTotalMarketPrice() {
		if(soldQty != null && marketPrice!=null){
			totalMarketPrice = Double.valueOf(df.format(soldQty * marketPrice));
		}
		return totalMarketPrice;
	}

	public void setTotalMarketPrice(Double totalMarketPrice) {
		this.totalMarketPrice = totalMarketPrice;
	}

	@Transient
	public String getEventDateStr() {
		return Util.formatDateToMonthDateYear(eventDate);
	}

	
	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}

	@Transient
	public String getInvoiceDateStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(invoiceDate);
	}

	public void setInvoiceDateStr(String invoiceDateStr) {
		this.invoiceDateStr = invoiceDateStr;
	}

	@Column(name="market_price_update_count")
	public Integer getPriceUpdateCount() {
		return priceUpdateCount;
	}

	public void setPriceUpdateCount(Integer priceUpdateCount) {
		this.priceUpdateCount = priceUpdateCount;
	}

	@Transient
	public Boolean getIsFocusRow() {
		if(null != isFocusRow && !isFocusRow){
			isFocusRow = false;
		}
		return isFocusRow;
	}

	public void setIsFocusRow(Boolean isFocusRow) {
		this.isFocusRow = isFocusRow;
	}

	@Column(name="online_price")
	public Double getOnlinePrice() {
		return onlinePrice;
	}

	public void setOnlinePrice(Double onlinePrice) {
		this.onlinePrice = onlinePrice;
	}
	
	
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getDiscountCouponPrice() {
		return discountCouponPrice;
	}

	public void setDiscountCouponPrice(Double discountCouponPrice) {
		this.discountCouponPrice = discountCouponPrice;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Date getLastUpdatedPriceDate() {
		return lastUpdatedPriceDate;
	}

	public void setLastUpdatedPriceDate(Date lastUpdatedPriceDate) {
		this.lastUpdatedPriceDate = lastUpdatedPriceDate;
	}

	@Column(name = "product_type")
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}

	@Column(name="zone_ticket_qty")
	public Integer getZoneTixQty() {
		return zoneTixQty;
	}

	public void setZoneTixQty(Integer zoneTixQty) {
		this.zoneTixQty = zoneTixQty;
	}

	@Column(name="zone_cheapest_price")
	public Double getZoneCheapestPrice() {
		return zoneCheapestPrice;
	}

	public void setZoneCheapestPrice(Double zoneCheapestPrice) {
		this.zoneCheapestPrice = zoneCheapestPrice;
	}

	@Column(name="zone")
	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	@Column(name="zone_ticket_count")
	public Integer getZoneTixGroupCount() {
		return zoneTixGroupCount;
	}

	public void setZoneTixGroupCount(Integer zoneTixGroupCount) {
		this.zoneTixGroupCount = zoneTixGroupCount;
	}
	
	@Column(name="zone_total_price")
	public Double getZoneTotalPrice() {
		return zoneTotalPrice;
	}

	public void setZoneTotalPrice(Double zoneTotalPrice) {
		this.zoneTotalPrice = zoneTotalPrice;
	}

	@Column(name="zone_profit_loss")
	public Double getZoneProfitAndLoss() {
		return zoneProfitAndLoss;
	}

	public void setZoneProfitAndLoss(Double zoneProfitAndLoss) {
		this.zoneProfitAndLoss = zoneProfitAndLoss;
	}

	@Column(name="net_total_sold_price")
	public Double getNetTotalSoldPrice() {
		return netTotalSoldPrice;
	}

	public void setNetTotalSoldPrice(Double netTotalSoldPrice) {
		this.netTotalSoldPrice = netTotalSoldPrice;
	}

	@Column(name="net_sold_price")
	public Double getNetSoldPrice() {
		return netSoldPrice;
	}

	public void setNetSoldPrice(Double netSoldPrice) {
		this.netSoldPrice = netSoldPrice;
	}

	@Column(name="zone_margin")
	public Double getZoneMargin() {
		return zoneMargin;
	}

	public void setZoneMargin(Double zoneMargin) {
		this.zoneMargin = zoneMargin;
	}

	@Column(name="section_margin")
	public Double getSectionMargin() {
		return sectionMargin;
	}

	public void setSectionMargin(Double sectionMargin) {
		this.sectionMargin = sectionMargin;
	}
	
	@Transient
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	@Transient
	public Integer getPoId() {
		return poId;
	}

	@Column(name = "event_time")
	public Time getEventTime() {
		return eventTime;
	}

	public void setEventTime(Time eventTime) {
		this.eventTime = eventTime;
	}


	public void setPoId(Integer poId) {
		this.poId = poId;
	}

	@Transient
	public Date getPoDate() {
		return poDate;
	}

	public void setPoDate(Date poDate) {
		this.poDate = poDate;
	}

	@Transient
	public String getEventTimeStr() {
		return Util.formatTimeToHourMinutes(eventTime);
	}

	public void setEventTimeStr(String eventTimeStr) {
		this.eventTimeStr = eventTimeStr;
	}
	
	@Transient
	public String getPoDateStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(poDate);
	}

	public void setPoDateStr(String poDateStr) {
		this.poDateStr = poDateStr;
	}

	@Transient
	public String getLastUpdateStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(lastUpdated);
	}

	public void setLastUpdateStr(String lastUpdateStr) {
		this.lastUpdateStr = lastUpdateStr;
	}

	
	@Transient
	public String getShippingMethod() {
		return shippingMethod;
	}
	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	@Transient
	public String getTrackingNo() {
		return trackingNo;
	}

	public void setTrackingNo(String trackingNo) {
		this.trackingNo = trackingNo;
	}

	@Transient
	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	@Transient
	public String getSecondaryOrderId() {
		return secondaryOrderId;
	}

	public void setSecondaryOrderId(String secondaryOrderId) {
		this.secondaryOrderId = secondaryOrderId;
	}

	@Transient
	public String getSecondaryOrderType() {
		return secondaryOrderType;
	}

	public void setSecondaryOrderType(String secondaryOrderType) {
		this.secondaryOrderType = secondaryOrderType;
	}
	
	
	@Transient
	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	@Transient
	public void computeNetTotalSoldPrice() {
		
		
		Double price =  getTotalActualSoldPrice();
		if(price == null){
			price =0.0;
		}
		if(productType != null && (productType.equals("RTW") || productType.equals("RTW2"))) {
			netTotalSoldPrice = Double.valueOf(df.format(price - (price*TN_FEES/100)));
		} else {
			netTotalSoldPrice = price;
		}
	}
	@Transient
	public void computeNetSoldPrice() {
		
		
		Double price =  getActualSoldPrice();
		if(price == null){
			price =0.0;
		}
		if(productType != null && (productType.equals("RTW") || productType.equals("RTW2"))) {
			netSoldPrice = Double.valueOf(df.format(price - (price*TN_FEES/100)));
		} else {
			netSoldPrice = price;
		}
	}
	
	@Transient
	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@Transient
	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}
 
	
}
