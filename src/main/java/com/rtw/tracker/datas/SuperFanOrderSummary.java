package com.rtw.tracker.datas;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("SuperFanOrderSummary")
public class SuperFanOrderSummary {
	private Integer status;
	private com.rtw.tmat.utils.Error error;
	private String pointsNeeded;
	private String pointsNeededText;
	private String availablePoints;
	private String pointsRemaining;
	private String message;
	private CrownJewelCustomerOrder futureOrderConfirmation;
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public com.rtw.tmat.utils.Error getError() {
		return error;
	}
	public void setError(com.rtw.tmat.utils.Error error) {
		this.error = error;
	}
	public String getPointsNeeded() {
		return pointsNeeded;
	}
	public void setPointsNeeded(String pointsNeeded) {
		this.pointsNeeded = pointsNeeded;
	}
	public String getPointsRemaining() {
		return pointsRemaining;
	}
	public void setPointsRemaining(String pointsRemaining) {
		this.pointsRemaining = pointsRemaining;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public CrownJewelCustomerOrder getFutureOrderConfirmation() {
		return futureOrderConfirmation;
	}
	public void setFutureOrderConfirmation(
			CrownJewelCustomerOrder futureOrderConfirmation) {
		this.futureOrderConfirmation = futureOrderConfirmation;
	}
	public String getPointsNeededText() {
		return pointsNeededText;
	}
	public void setPointsNeededText(String pointsNeededText) {
		this.pointsNeededText = pointsNeededText;
	}
	public String getAvailablePoints() {
		return availablePoints;
	}
	public void setAvailablePoints(String availablePoints) {
		this.availablePoints = availablePoints;
	}
	
	
}
