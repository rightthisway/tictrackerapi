package com.rtw.tracker.datas;

import java.io.Serializable;
import java.sql.Time;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;

@Entity
@Table(name="sold_ticket_detail_vw")
public class SoldTicketDetail implements Serializable {
	
	Integer id;
	Integer invoiceId;
	Date invoiceDateTime;
	Integer tmatEventId;
	Integer exEventId;
	Integer quantity;
	Integer soldQty;
	Integer ticketRequestId;
	String eventName;
	Date eventDate;
	Time eventTime;
	Integer venueId;
	String venueName;
	String venueCity;
	String venueState;
	String venueCountry;
	String section;
	String row;
	Double retailPrice; 
	Double cost; 
	Double wholesalePrice; 
	Double actualSoldPrice; 
	Double totalPrice;
//	Double amount;
	String internalNotes;
	String clientBrokerCompanyName;
	Integer brokerId; 
	String clientName;
	String clientTypeDesc;
	Integer purchaseOrderId;
	Date purchaseOrderDateTime;
	String invoiceType;
	String salesPerson;
	String productType;
	String realTixMapped;
	Boolean isSent;
	
	String eventDateStr;
	String invoiceDateTimeStr;
	String purchaseOrderDateTimeStr;
	Double totalActualSoldPrice;
	
	DecimalFormat df = new DecimalFormat("#.##");
	
	@Column(name="purchase_order_id")
	public Integer getPurchaseOrderId() {
		return purchaseOrderId;
	}
	public void setPurchaseOrderId(Integer purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}
	
	@Column(name="purchase_order_date")
	public Date getPurchaseOrderDateTime() {
		return purchaseOrderDateTime;
	}
	public void setPurchaseOrderDateTime(Date purchaseOrderDateTime) {
		this.purchaseOrderDateTime = purchaseOrderDateTime;
	}
	
	@Id
	@Column(name="ticket_group_id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="broker_id")
	public Integer getBrokerId() {
		return brokerId;
	}
	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}
	
	
	@Column(name="event_id")
	public Integer getTmatEventId() {
		return tmatEventId;
	}
	public void setTmatEventId(Integer tmatEventId) {
		this.tmatEventId = tmatEventId;
	}
	
	@Column(name="Invoice_no")
	public Integer getInvoiceId() {
		return invoiceId;
	}
	
	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}
	
	@Column(name="ticket_qty")
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	@Column(name="ticket_count")
	public Integer getSoldQty() {
		return soldQty;
	}
	public void setSoldQty(Integer soldQty) {
		this.soldQty = soldQty;
	}
	
	
	@Column(name="ticket_request_id")
	public Integer getTicketRequestId() {
		return ticketRequestId;
	}
	public void setTicketRequestId(Integer ticketRequestId) {
		this.ticketRequestId = ticketRequestId;
	}
	@Column(name="event_name")
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	@Column(name="event_datetime",columnDefinition="DATE")
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	
	
	
	
	
	@Column(name="section")
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	
	@Column(name="event_time")
	public Time getEventTime() {
		return eventTime;
	}
	public void setEventTime(Time eventTime) {
		this.eventTime = eventTime;
	}
	
	
	@Column(name="row")
	public String getRow() {
		return row;
	}
	public void setRow(String row) {
		this.row = row;
	}
	
	
	@Column(name="retail_price")
	public Double getRetailPrice() {
		return retailPrice;
	}
	public void setRetailPrice(Double retailPrice) {
		this.retailPrice = retailPrice;
	}
	
	@Column(name="cost")
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	@Column(name="wholesale_price")
	public Double getWholesalePrice() {
		return wholesalePrice;
	}
	public void setWholesalePrice(Double wholesalePrice) {
		this.wholesalePrice = wholesalePrice;
	}
	
	@Column(name="actual_sold_price")
	public Double getActualSoldPrice() {
		return actualSoldPrice;
	}
	public void setActualSoldPrice(Double actualSoldPrice) {
		this.actualSoldPrice = actualSoldPrice;
	}
	
	@Column(name="totalprice")
	public Double getTotalPrice() {
		return totalPrice;
	}
	
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	@Column(name="internal_notes")
	public String getInternalNotes() {
		return internalNotes;
	}
	public void setInternalNotes(String internalNotes) {
		this.internalNotes = internalNotes;
	}
	
	@Column(name="invoice_datetime")
	public Date getInvoiceDateTime() {
		return invoiceDateTime;
	}
	public void setInvoiceDateTime(Date invoiceDateTime) {
		this.invoiceDateTime = invoiceDateTime;
	}

	@Column(name="venue_id")
	public Integer getVenueId() {
		return venueId;
	}
	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}
	
	@Column(name="venue_name")
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	
	@Column(name="venue_city")
	public String getVenueCity() {
		return venueCity;
	}
	public void setVenueCity(String venueCity) {
		this.venueCity = venueCity;
	}
	
	@Column(name="state_name")
	public String getVenueState() {
		return venueState;
	}
	public void setVenueState(String venueState) {
		this.venueState = venueState;
	}
	
	@Column(name="system_country_name")
	public String getVenueCountry() {
		return venueCountry;
	}
	public void setVenueCountry(String venueCountry) {
		this.venueCountry = venueCountry;
	}
	
	/*@Column(name="client_broker_type")
	public String getBrokerName() {
		return brokerName;
	}
	
	public void setBrokerName(String brokerName) {
		this.brokerName = brokerName;
	}*/
	
	@Column(name="invoice_type")
	public String getInvoiceType() {
		return invoiceType;
	}
	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}
	
	@Column(name="Sales_Person")
	public String getSalesPerson() {
		return salesPerson;
	}
	public void setSalesPerson(String salesPerson) {
		this.salesPerson = salesPerson;
	}
	@Column(name="exchange_event_id")
	public Integer getExEventId() {
		return exEventId;
	}
	public void setExEventId(Integer exEventId) {
		this.exEventId = exEventId;
	}
	
	@Column(name = "product_type")
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	
	
	@Column(name = "realtix_map")
	public String getRealTixMapped() {
		return realTixMapped;
	}
	public void setRealTixMapped(String realTixMapped) {
		this.realTixMapped = realTixMapped;
	}
	
	@Column(name = "is_sent")
	public Boolean getIsSent() {
		return isSent;
	}
	public void setIsSent(Boolean isSent) {
		this.isSent = isSent;
	}
	@Transient
	public String getEventDateStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(eventDate);
	}
	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}
	
	
	@Transient
	public String getInvoiceDateTimeStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(invoiceDateTime);
	}
	public void setInvoiceDateTimeStr(String invoiceDateTimeStr) {
		this.invoiceDateTimeStr = invoiceDateTimeStr;
	}
	
	
	
	@Transient
	public String getPurchaseOrderDateTimeStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(purchaseOrderDateTime);
	}
	public void setPurchaseOrderDateTimeStr(String purchaseOrderDateTimeStr) {
		this.purchaseOrderDateTimeStr = purchaseOrderDateTimeStr;
	}
	
	@Transient
	public Double getTotalActualSoldPrice() {
		if(quantity != null){
			totalActualSoldPrice = Double.valueOf(df.format(quantity * actualSoldPrice));
		}
		return totalActualSoldPrice;
	}

	public void setTotalActualSoldPrice(Double totalActualSoldPrice) {
		this.totalActualSoldPrice = totalActualSoldPrice;
	}
	

}
