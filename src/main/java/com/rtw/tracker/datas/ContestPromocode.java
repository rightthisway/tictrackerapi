package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;

@Entity
@Table(name = "customer_promo_reward_details")
public class ContestPromocode implements Serializable{

	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;
	
	@Column(name="customer_id")
	private Integer customerId;
	
	@Column(name="promo_reward_id")
	private Integer promoRewardId;
	
	@Column(name="promo_name")
	private String promoName;
	
	@Column(name="promo_type")
	private String promoType;
	
	@Column(name="promotional_code")
	private String promoCode;
	
	@Column(name="earn_life_count")
	private Integer earnLifeCount;
	
	@Column(name="free_tix_count")
	private Integer freeTixCount;
	
	@Column(name="discount_percentage")
	private Double discountPercentage;
	
	@Column(name="promo_ref_id")
	private Integer promoRefId;
	
	@Column(name="promo_ref_type")
	private String promoRefType;
	
	@Column(name="promo_ref_name")
	private String promoRefName;
	
	@Column(name="promo_expiry_date")
	private Date promoExpiryDate;
	
	@Column(name="promo_offer_id")
	private Integer promoOfferId;
	
	@Column(name="status")
	private String status;
	
	@Column(name="created_date")
	private Date createdDate;
	
	@Column(name="created_by")
	private String createdBy;
	
	@Column(name="updated_date")
	private Date updatedDate;
	
	@Column(name="updated_by")
	private String updatedBy;
	
	@Column(name="apply_date")
	private Date applyDate;
	
	
	@Transient
	private String expiryDateStr;
	
	@Transient
	private String expiryDateEditStr;
	
	@Transient
	private String createdDateStr;
	
	@Transient
	private String updatedDateStr;
	
	@Transient
	private String userId;
	
	@Transient
	private String email;
	
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public Integer getPromoRewardId() {
		return promoRewardId;
	}
	public void setPromoRewardId(Integer promoRewardId) {
		this.promoRewardId = promoRewardId;
	}
	public String getPromoName() {
		return promoName;
	}
	public void setPromoName(String promoName) {
		this.promoName = promoName;
	}
	public String getPromoType() {
		return promoType;
	}
	public void setPromoType(String promoType) {
		this.promoType = promoType;
	}
	public String getPromoCode() {
		return promoCode;
	}
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}
	public Integer getEarnLifeCount() {
		return earnLifeCount;
	}
	public void setEarnLifeCount(Integer earnLifeCount) {
		this.earnLifeCount = earnLifeCount;
	}
	public Integer getFreeTixCount() {
		return freeTixCount;
	}
	public void setFreeTixCount(Integer freeTixCount) {
		this.freeTixCount = freeTixCount;
	}
	public Double getDiscountPercentage() {
		return discountPercentage;
	}
	public void setDiscountPercentage(Double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}
	public Integer getPromoRefId() {
		return promoRefId;
	}
	public void setPromoRefId(Integer promoRefId) {
		this.promoRefId = promoRefId;
	}
	public String getPromoRefType() {
		return promoRefType;
	}
	public void setPromoRefType(String promoRefType) {
		this.promoRefType = promoRefType;
	}
	public String getPromoRefName() {
		return promoRefName;
	}
	public void setPromoRefName(String promoRefName) {
		this.promoRefName = promoRefName;
	}
	public Date getPromoexpiryDate() {
		return promoExpiryDate;
	}
	public void setPromoexpiryDate(Date promoexpiryDate) {
		this.promoExpiryDate = promoexpiryDate;
	}
	public Integer getPromoOfferId() {
		return promoOfferId;
	}
	public void setPromoOfferId(Integer promoOfferId) {
		this.promoOfferId = promoOfferId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getApplyDate() {
		return applyDate;
	}
	public void setApplyDate(Date applyDate) {
		this.applyDate = applyDate;
	}
	public Date getPromoExpiryDate() {
		return promoExpiryDate;
	}
	public void setPromoExpiryDate(Date promoExpiryDate) {
		this.promoExpiryDate = promoExpiryDate;
	}
	
	
	
	public String getExpiryDateStr() {
		if(promoExpiryDate != null){
			this.expiryDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(promoExpiryDate);
		}
		return this.expiryDateStr;
	}
	public void setExpiryDateStr(String expiryDateStr) {
		this.expiryDateStr = expiryDateStr;
	}
	
	
	
	public String getExpiryDateEditStr() {
		if(promoExpiryDate!=null){
			expiryDateEditStr = Util.formatDateToMonthDateYear(promoExpiryDate);
		}
		return expiryDateEditStr;
	}
	public void setExpiryDateEditStr(String expiryDateEditStr) {
		this.expiryDateEditStr = expiryDateEditStr;
	}
	
	
	public String getCreatedDateStr() {
		if(createdDate != null){
			this.createdDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(createdDate);
		}
		return createdDateStr;
	}
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	
	
	public String getUpdatedDateStr() {
		if(updatedDate!=null){
			this.updatedDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(updatedDate);
		}
		return updatedDateStr;
	}
	public void setUpdatedDateStr(String updatedDateStr) {
		this.updatedDateStr = updatedDateStr;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
}
