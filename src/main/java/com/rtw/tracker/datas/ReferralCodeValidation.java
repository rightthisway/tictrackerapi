package com.rtw.tracker.datas;

import com.rtw.tracker.enums.CodeType;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("ReferralCodeValidation")
public class ReferralCodeValidation {
	private Integer status;
	private com.rtw.tmat.utils.Error error; 
	private Integer customerId;
	private String referralCode; 
	private String message;
	private String rewardPoints;
	private Double rewardPointsAsDouble;
	private Double rewardConv;
	private Double discountConv;
	private CodeType codeType;
	private Boolean isFlatDiscount;
	private Integer promoTrackingId;
	private Integer affPromoTrackingId;
	
	public com.rtw.tmat.utils.Error getError() {
		return error;
	}
	public void setError(com.rtw.tmat.utils.Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getReferralCode() {
		return referralCode;
	}
	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getRewardPoints() {
		return rewardPoints;
	}
	public void setRewardPoints(String rewardPoints) {
		this.rewardPoints = rewardPoints;
	}
	public Double getRewardPointsAsDouble() {
		return rewardPointsAsDouble;
	}
	public void setRewardPointsAsDouble(Double rewardPointsAsDouble) {
		this.rewardPointsAsDouble = rewardPointsAsDouble;
	}
	public Double getRewardConv() {
		return rewardConv;
	}
	public void setRewardConv(Double rewardConv) {
		this.rewardConv = rewardConv;
	}
	public Double getDiscountConv() {
		return discountConv;
	}
	public void setDiscountConv(Double discountConv) {
		this.discountConv = discountConv;
	}
	public CodeType getCodeType() {
		return codeType;
	}
	public void setCodeType(CodeType codeType) {
		this.codeType = codeType;
	}
	public Boolean getIsFlatDiscount() {
		return isFlatDiscount;
	}
	public void setIsFlatDiscount(Boolean isFlatDiscount) {
		this.isFlatDiscount = isFlatDiscount;
	}
	public Integer getPromoTrackingId() {
		return promoTrackingId;
	}
	public void setPromoTrackingId(Integer promoTrackingId) {
		this.promoTrackingId = promoTrackingId;
	}
	public Integer getAffPromoTrackingId() {
		return affPromoTrackingId;
	}
	public void setAffPromoTrackingId(Integer affPromoTrackingId) {
		this.affPromoTrackingId = affPromoTrackingId;
	}
	
}
