package com.rtw.tracker.datas;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("CrownJewelTicketList")
public class CrownJewelTicketList {
	
	private Integer status;
	private com.rtw.tmat.utils.Error error; 
	private Integer leagueId;
	private String leagueName;
	private String leagueCity;
	private Integer teamId;
	private String teamName;
	private Integer customerId;
	private String availableRewardPoints;
	private String pendingPoints;
	private Double availableRewardPointsAsDouble;
	private Double pendingPointsAsDouble;
	private String svgWebViewUrl;
	private String svgText;
	private String svgMapPath;
	private Event event;
	private List<CrownJewelTicketMap> sectionTicketsMap;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public com.rtw.tmat.utils.Error getError() {
		return error;
	}
	public void setError(com.rtw.tmat.utils.Error error) {
		this.error = error;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getAvailableRewardPoints() {
		return availableRewardPoints;
	}
	public void setAvailableRewardPoints(String availableRewardPoints) {
		this.availableRewardPoints = availableRewardPoints;
	}
	public Integer getLeagueId() {
		return leagueId;
	}
	public void setLeagueId(Integer leagueId) {
		this.leagueId = leagueId;
	}
	public String getLeagueName() {
		if(null == leagueName ){
			leagueName="";
		}
		return leagueName;
	}
	public void setLeagueName(String leagueName) {
		this.leagueName = leagueName;
	}
	public Integer getTeamId() {
		return teamId;
	}
	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}
	public String getTeamName() {
		if(null == teamName ){
			teamName="";
		}
		return teamName;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	
	public String getLeagueCity() {
		if(null == leagueCity ){
			leagueCity="";
		}
		return leagueCity;
	}
	public void setLeagueCity(String leagueCity) {
		this.leagueCity = leagueCity;
	}
	public Event getEvent() {
		return event;
	}
	public void setEvent(Event event) {
		this.event = event;
	}
	public List<CrownJewelTicketMap> getSectionTicketsMap() {
		return sectionTicketsMap;
	}
	public void setSectionTicketsMap(List<CrownJewelTicketMap> sectionTicketsMap) {
		this.sectionTicketsMap = sectionTicketsMap;
	}
	public String getSvgWebViewUrl() {
		return svgWebViewUrl;
	}
	public void setSvgWebViewUrl(String svgWebViewUrl) {
		this.svgWebViewUrl = svgWebViewUrl;
	}
	public String getSvgText() {
		return svgText;
	}
	public void setSvgText(String svgText) {
		this.svgText = svgText;
	}
	public String getSvgMapPath() {
		return svgMapPath;
	}
	public void setSvgMapPath(String svgMapPath) {
		this.svgMapPath = svgMapPath;
	}
	public String getPendingPoints() {
		return pendingPoints;
	}
	public void setPendingPoints(String pendingPoints) {
		this.pendingPoints = pendingPoints;
	}
	public Double getAvailableRewardPointsAsDouble() {
		return availableRewardPointsAsDouble;
	}
	public void setAvailableRewardPointsAsDouble(
			Double availableRewardPointsAsDouble) {
		this.availableRewardPointsAsDouble = availableRewardPointsAsDouble;
	}
	public Double getPendingPointsAsDouble() {
		return pendingPointsAsDouble;
	}
	public void setPendingPointsAsDouble(Double pendingPointsAsDouble) {
		this.pendingPointsAsDouble = pendingPointsAsDouble;
	}
	
	
}
