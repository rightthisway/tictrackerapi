package com.rtw.tracker.datas;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.enums.CrownJewelOrderStatus;


@Entity
@Table(name = "fantasy_customer_orders")
public class CustomerFantasyOrder implements Serializable{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@JsonIgnore
	@Column(name = "fantasy_grand_child_id")
	private Integer fantasyGrandChildId;
	
	@Column(name = "fantasy_event_id") 
	private Integer fantasyEventId;
	
	@Column(name="fantasy_event_name")
	private String fantasyEventName;
	
	@JsonIgnore
	@Column(name="team_id")
	private Integer teamId;
	
	@Column(name="team_name")
	private String teamName;
	
	@Column(name="package_selected")
	private Boolean isPackageSelected;
	
	@JsonIgnore
	@Column(name="package_cost")
	private Double packageCost;
	
	@Column(name="package_notes")
	private String packageNotesAsHtml;
	
	@JsonIgnore
	@Column(name="event_id")
	private Integer tmatEventId;
	
	@JsonIgnore
	@Column(name="is_real_ticket")
	private Boolean isRealTix;
	
	@JsonIgnore
	@Column(name="ticket_id")
	private Integer ticketId;
	
	@Column(name="zone")
	private String zone;
	
	@Column(name="ticket_qty")
	private Integer quantity;
	
	@JsonIgnore
	@Column(name="ticket_price")
	private Double ticketPrice;
	
	@JsonIgnore
	@Column(name="required_points")
	private Double requiredPoints;
	
	@Column(name="customer_id")
	private Integer customerId;
	
	@JsonIgnore
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	private CrownJewelOrderStatus status;
	
	@JsonIgnore
	@Column(name="create_date")
	private Date createDate;
	
	@JsonIgnore
	@Column(name="last_update")
	private Date lastUpdate;
	
	
	@Column(name="regular_order_id")
	private Integer regularOrderId;
	
	@JsonIgnore
	@Column(name="team_zone_id")
	private Integer teamZoneId;
	
	@Column(name="venue_name")
	private String venueName;
	
	@Column(name="venue_city")
	private String city;
	
	@Column(name="venue_state")
	private String state;
	
	@Column(name="venue_country")
	private String country;
	
	@JsonIgnore
	@Transient
	private Event event;
	@Transient
	private String fantasyOrderText;
	@Transient
	private String deliveryInfo;
	@Transient
	private String buttonOption;
	@Transient
	private Boolean showInfoButton;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getFantasyGrandChildId() {
		return fantasyGrandChildId;
	}
	public void setFantasyGrandChildId(Integer fantasyGrandChildId) {
		this.fantasyGrandChildId = fantasyGrandChildId;
	}
	public Integer getFantasyEventId() {
		return fantasyEventId;
	}
	public void setFantasyEventId(Integer fantasyEventId) {
		this.fantasyEventId = fantasyEventId;
	}
	public String getFantasyEventName() {
		return fantasyEventName;
	}
	public void setFantasyEventName(String fantasyEventName) {
		this.fantasyEventName = fantasyEventName;
	}
	public Integer getTeamId() {
		return teamId;
	}
	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}
	public String getTeamName() {
		return teamName;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	public Boolean getIsPackageSelected() {
		return isPackageSelected;
	}
	public void setIsPackageSelected(Boolean isPackageSelected) {
		this.isPackageSelected = isPackageSelected;
	}
	public Double getPackageCost() {
		return packageCost;
	}
	public void setPackageCost(Double packageCost) {
		this.packageCost = packageCost;
	}
	public String getPackageNotesAsHtml() {
		return packageNotesAsHtml;
	}
	public void setPackageNotesAsHtml(String packageNotesAsHtml) {
		this.packageNotesAsHtml = packageNotesAsHtml;
	}
	public Integer getTmatEventId() {
		return tmatEventId;
	}
	public void setTmatEventId(Integer tmatEventId) {
		this.tmatEventId = tmatEventId;
	}
	public Boolean getIsRealTix() {
		return isRealTix;
	}
	public void setIsRealTix(Boolean isRealTix) {
		this.isRealTix = isRealTix;
	}
	public Integer getTicketId() {
		return ticketId;
	}
	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Double getTicketPrice() {
		return ticketPrice;
	}
	public void setTicketPrice(Double ticketPrice) {
		this.ticketPrice = ticketPrice;
	}
	public Double getRequiredPoints() {
		return requiredPoints;
	}
	public void setRequiredPoints(Double requiredPoints) {
		this.requiredPoints = requiredPoints;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public CrownJewelOrderStatus getStatus() {
		return status;
	}
	public void setStatus(CrownJewelOrderStatus status) {
		this.status = status;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public Integer getRegularOrderId() {
		return regularOrderId;
	}
	public void setRegularOrderId(Integer regularOrderId) {
		this.regularOrderId = regularOrderId;
	}
	public Integer getTeamZoneId() {
		return teamZoneId;
	}
	public void setTeamZoneId(Integer teamZoneId) {
		this.teamZoneId = teamZoneId;
	}
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Event getEvent() {
		return event;
	}
	public void setEvent(Event event) {
		this.event = event;
	}
	public String getFantasyOrderText() {
		return fantasyOrderText;
	}
	public void setFantasyOrderText(String fantasyOrderText) {
		this.fantasyOrderText = fantasyOrderText;
	}
	public String getDeliveryInfo() {
		return deliveryInfo;
	}
	public void setDeliveryInfo(String deliveryInfo) {
		this.deliveryInfo = deliveryInfo;
	}
	public String getButtonOption() {
		return buttonOption;
	}
	public void setButtonOption(String buttonOption) {
		this.buttonOption = buttonOption;
	}
	public Boolean getShowInfoButton() {
		return showInfoButton;
	}
	public void setShowInfoButton(Boolean showInfoButton) {
		this.showInfoButton = showInfoButton;
	}

	

}
