package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.rtw.tmat.utils.Util;
import com.rtw.tracker.datas.TicketStatus;

@Entity
@Table(name="ticket")
public class Ticket implements Serializable{

	private Integer id;
	private String seatNo;
	private Double cost;
	private Double facePrice;
	private Double retailPrice;
	private Integer seatOrder;
	private Double wholesalePrice;
	private Double actualSoldPrice;
	private String generalDescription;
	private Boolean status;
	@JsonSerialize(using = com.rtw.tracker.utils.CustomerDateAndTimeSerializer.class)
	private Date createdDate;
	@JsonSerialize(using = com.rtw.tracker.utils.CustomerDateAndTimeSerializer.class)
	private Date expectedArrivalDate;
	private Integer ticketGroupId;
	private String userName;
	private String ticketOnhandStatus;
	private Integer invoiceId;
	private Integer poId;
	private String origanalSection;
	private Double wholesaleSalePrice;
	private TicketStatus ticketStatus;
	private Boolean onHand;
	private Date onHandDate;
	@JsonSerialize(using = com.rtw.tracker.utils.CustomerDateAndTimeSerializer.class)
	private String eventNameString;
	private String section; 
	private String row;
		
	public Ticket(){
		
	}
	
	public Ticket(TicketGroup ticketGroup){
		this.cost = ticketGroup.getPrice();
		this.facePrice = ticketGroup.getPrice();
		this.retailPrice = ticketGroup.getPrice();
		this.wholesalePrice = ticketGroup.getPrice(); 
		this.actualSoldPrice = ticketGroup.getPrice();
		this.generalDescription = ticketGroup.getNotes();
		this.status = Boolean.TRUE;
		this.createdDate = new Date();
		this.expectedArrivalDate = null;
		this.ticketGroupId = ticketGroup.getId();
		this.ticketOnhandStatus = null;
		this.invoiceId = ticketGroup.getInvoiceId();
		this.poId = ticketGroup.getPurchaseOrderId();
		this.origanalSection = ticketGroup.getSection();
		this.wholesaleSalePrice = ticketGroup.getPrice();
	}
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="seat_number")
	public String getSeatNo() {
		return seatNo;
	}
	public void setSeatNo(String seatNo) {
		this.seatNo = seatNo;
	}
	
	@Column(name="cost")
	public Double getCost() {
		try{
			if(cost != null){
				return Util.getRoundedValue(cost);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	
	@Column(name="face_price")
	public Double getFacePrice() {
		try{
			if(facePrice != null){
				return Util.getRoundedValue(facePrice);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return facePrice;
	}
	public void setFacePrice(Double facePrice) {
		this.facePrice = facePrice;
	}
	
	@Column(name="retail_price")
	public Double getRetailPrice() {
		try{
			if(retailPrice != null){
				return Util.getRoundedValue(retailPrice);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return retailPrice;
	}
	public void setRetailPrice(Double retailPrice) {
		this.retailPrice = retailPrice;
	}
	
	@Column(name="seat_order")
	public Integer getSeatOrder() {
		return seatOrder;
	}
	public void setSeatOrder(Integer seatOrder) {
		this.seatOrder = seatOrder;
	}
	
	@Column(name="wholesale_price")
	public Double getWholesalePrice() {
		try{
			if(wholesalePrice != null){
				return Util.getRoundedValue(wholesalePrice);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return wholesalePrice;
	}
	public void setWholesalePrice(Double wholesalePrice) {
		this.wholesalePrice = wholesalePrice;
	}
	
	@Column(name="actual_sold_price")
	public Double getActualSoldPrice() {
		try{
			if(actualSoldPrice != null){
				return Util.getRoundedValue(actualSoldPrice);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return actualSoldPrice;
	}
	public void setActualSoldPrice(Double actualSoldPrice) {
		this.actualSoldPrice = actualSoldPrice;
	}
	
	@Column(name="general_desc")
	public String getGeneralDescription() {
		return generalDescription;
	}
	public void setGeneralDescription(String generalDesription) {
		this.generalDescription = generalDesription;
	}
	
	@Column(name="status")
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Column(name="create_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="expected_arrival_date")
	public Date getExpectedArrivalDate() {
		return expectedArrivalDate;
	}
	public void setExpectedArrivalDate(Date expectedArrivalDate) {
		this.expectedArrivalDate = expectedArrivalDate;
	}
	
	@Column(name="ticket_group_id")
	public Integer getTicketGroupId() {
		return ticketGroupId;
	}
	public void setTicketGroupId(Integer ticketGroupId) {
		this.ticketGroupId = ticketGroupId;
	}
	
	@Column(name="username")
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	/*@Column(name="ticket_on_hand_status")
	public String getTicketOnhandStatus() {
		return ticketOnhandStatus;
	}
	public void setTicketOnhandStatus(String ticketOnhandStatus) {
		this.ticketOnhandStatus = ticketOnhandStatus;
	}*/
	
	@Column(name="invoice_id")
	public Integer getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}
	
	@Column(name="purchase_order_id")
	public Integer getPoId() {
		return poId;
	}
	public void setPoId(Integer poId) {
		this.poId = poId;
	}
	
	@Column(name="original_section")
	public String getOriganalSection() {
		return origanalSection;
	}
	public void setOriganalSection(String origanalSection) {
		this.origanalSection = origanalSection;
	}
	
	@Column(name="wholesale_price_at_time_of_sale")
	public Double getWholesaleSalePrice() {
		try{
			if(wholesaleSalePrice != null){
				return Util.getRoundedValue(wholesaleSalePrice);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return wholesaleSalePrice;
	}
	public void setWholesaleSalePrice(Double wholesaleSalePrice) {
		this.wholesaleSalePrice = wholesaleSalePrice;
	}
	
	
	@Column(name="ticket_status")
	@Enumerated(EnumType.STRING)
	public TicketStatus getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(TicketStatus ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	@Column(name="on_hand")
	public Boolean getOnHand() {
		return onHand;
	}

	public void setOnHand(Boolean onHand) {
		this.onHand = onHand;
	}

	@Column(name="on_hand_date")
	public Date getOnHandDate() {
		return onHandDate;
	}

	public void setOnHandDate(Date onHandDate) {
		this.onHandDate = onHandDate;
	}

	@Transient
	public String getEventNameString() {
		return eventNameString;
	}

	public void setEventNameString(String eventNameString) {
		this.eventNameString = eventNameString;
	}

	@Transient
	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	@Transient
	public String getRow() {
		return row;
	}

	public void setRow(String row) {
		this.row = row;
	}
	
	

}
