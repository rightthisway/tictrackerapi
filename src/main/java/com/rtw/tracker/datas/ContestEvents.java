package com.rtw.tracker.datas;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.rtw.tmat.utils.Util;

@Entity
@Table(name="contest_events")
public class ContestEvents implements Serializable{

	private Integer id;
	private Integer contestId;
	private Integer eventId;
	private String eventName;
	@JsonIgnore
	private Date eventDate;
	@JsonIgnore
	private Time eventTime;
	private Integer venueId;
	private String building;
	private String updatedBy;
	private Date updatedDateTime;
	
	private String eventDateStr;
	private String eventTimeStr;
	private String updatedDateTimeStr;
	
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="contest_id")
	public Integer getContestId() {
		return contestId;
	}
	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	@Column(name="updated_by")
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column(name="updated_date")
	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}
	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}
	
	@Transient
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	@Transient
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	
	@Transient
	public Time getEventTime() {
		return eventTime;
	}
	public void setEventTime(Time eventTime) {
		this.eventTime = eventTime;
	}
	
	@Transient
	public Integer getVenueId() {
		return venueId;
	}
	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}
	
	@Transient
	public String getBuilding() {
		return building;
	}
	public void setBuilding(String building) {
		this.building = building;
	}
	
	@Transient
	public String getEventDateStr() {
		if(eventDateStr != null){
			return eventDateStr;
		}else if(getEventDate()==null){
			return "TBD";
		}
		return Util.formatDateToMonthDateYear(getEventDate());
	}
	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}
	
	@Transient
	public String getEventTimeStr() {
		if(eventTimeStr != null){
			return eventTimeStr;
		}else if(getEventTime()==null){
			return "TBD";
		}
		return Util.formatTimeToHourMinutes(getEventTime());
	}
	public void setEventTimeStr(String eventTimeStr) {
		this.eventTimeStr = eventTimeStr;
	}
	
	@Transient
	public String getUpdatedDateTimeStr() {
		if(updatedDateTimeStr != null){
			return updatedDateTimeStr;
		}
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(getUpdatedDateTime());
	}
	public void setUpdatedDateTimeStr(String updatedDateTimeStr) {
		this.updatedDateTimeStr = updatedDateTimeStr;
	}
		
}
