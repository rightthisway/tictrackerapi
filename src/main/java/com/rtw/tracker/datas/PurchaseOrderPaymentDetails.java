package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;
@Entity
@Table(name= "purchase_order_payment")
public class PurchaseOrderPaymentDetails  implements Serializable{

	private Integer id;
	private Integer purchaseOrderId;
	private String name;
	private String paymentType;
	private String cardType;
	private String cardLastFourDigit;
	private String routingLastFourDigit;
	private String accountLastFourDigit;
	private String chequeNo;
	private String paymentStatus;
	private Double amount;
	private Date paymentDate;
	private String paymentNote;
	
	private String paymentDateStr;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="purchase_order_id")
	public Integer getPurchaseOrderId() {
		return purchaseOrderId;
	}
	public void setPurchaseOrderId(Integer purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}
	
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="payment_type")
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	
	@Column(name="card_type")
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	
	@Column(name="card_last_four_digit")
	public String getCardLastFourDigit() {
		return cardLastFourDigit;
	}
	public void setCardLastFourDigit(String cardLastFourDigit) {
		this.cardLastFourDigit = cardLastFourDigit;
	}
	
	@Column(name="routing_last_four_digit")
	public String getRoutingLastFourDigit() {
		return routingLastFourDigit;
	}
	public void setRoutingLastFourDigit(String routingLastFourDigit) {
		this.routingLastFourDigit = routingLastFourDigit;
	}
	
	@Column(name="account_last_four_digit")
	public String getAccountLastFourDigit() {
		return accountLastFourDigit;
	}
	public void setAccountLastFourDigit(String accountLastFourDigit) {
		this.accountLastFourDigit = accountLastFourDigit;
	}
	
	@Column(name="cheque_no")
	public String getChequeNo() {
		return chequeNo;
	}
	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}
	
	@Column(name="payment_status")
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	
	@Column(name="payment_amount")
	public Double getAmount() {
		try{
			if(amount != null){
				return Util.getRoundedValue(amount);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	@Column(name="payment_date")
	public Date getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}
	
	@Column(name="payment_note")
	public String getPaymentNote() {
		return paymentNote;
	}
	public void setPaymentNote(String paymentNote) {
		this.paymentNote = paymentNote;
	}
	
	@Transient
	public String getPaymentDateStr() {
		return Util.formatDateToMonthDateYear(paymentDate);
	}
	public void setPaymentDateStr(String paymentDateStr) {
		this.paymentDateStr = paymentDateStr;
	}
	
	
}
