package com.rtw.tracker.datas;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.enums.CrownJewelOrderStatus;

@Entity
@Table(name="fantasy_customer_orders")
public class CrownJewelCustomerOrder implements Serializable{

	private Integer id;
	private Integer fantasyGrandChildId;
	private Integer fantasyEventId;
	private String fantasyEventName;
	
	/*private Integer leagueId;
	private String leagueName;
	private String leagueParentType;
	private String leagueCity;
	private Integer ticketGroupId;
	private Double pointConversion;
	private Double ticketPoints;
	private Double packagePoints;
	private Double availablePoints;
	private Double remainingPoints;*/
	
	private Integer teamId;
	private String teamName;
	
	private Boolean isPackageSelected;
	private Double packageCost;
	private String packageNote;
	private Integer eventId;
	private Boolean isRealTicket;
	private Integer ticketId;
	
	private String ticketSelection;
	private Integer ticketQty;
	private Double ticketPrice;	
	private Double requiredPoints;
	private Integer customerId;	
	private String status;
	private Date createdDate;
	private Date lastUpdatedDate;
	private String acceptRejectReson;
	private Integer regularOrderId;
	private Integer teamZoneId;
	private String venueName;
	private String venueCity;
	private String venueState;
	private String venueCountry;
	private String grandChildName;
	private String platform;
	private String createdDateStr;
	private String lastUpdatedStr;
	@Transient
	private String customerName;

	
	@Id
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	/*
	@Column(name="league_id")
	public Integer getLeagueId() {
		return leagueId;
	}
	public void setLeagueId(Integer leagueId) {
		this.leagueId = leagueId;
	}
	
	@Column(name="league_name")
	public String getLeagueName() {
		return leagueName;
	}
	public void setLeagueName(String leagueName) {
		this.leagueName = leagueName;
	}
	
	@Column(name="league_parent_type")
	public String getLeagueParentType() {
		return leagueParentType;
	}
	public void setLeagueParentType(String leagueParentType) {
		this.leagueParentType = leagueParentType;
	}
	
	@Column(name="league_city")
	public String getLeagueCity() {
		return leagueCity;
	}
	public void setLeagueCity(String leagueCity) {
		this.leagueCity = leagueCity;
	}

	@Column(name="ticket_group_id")
	public Integer getTicketGroupId() {
		return ticketGroupId;
	}
	public void setTicketGroupId(Integer ticketGroupId) {
		this.ticketGroupId = ticketGroupId;
	}
	
	@Column(name="points_conversion")
	public Double getPointConversion() {
		return pointConversion;
	}
	public void setPointConversion(Double pointConversion) {
		this.pointConversion = pointConversion;
	}
	
	@Column(name="ticket_points")
	public Double getTicketPoints() {
		return ticketPoints;
	}
	public void setTicketPoints(Double ticketPoints) {
		this.ticketPoints = ticketPoints;
	}
	
	
	@Column(name="package_points")
	public Double getPackagePoints() {
		return packagePoints;
	}
	public void setPackagePoints(Double packagePoints) {
		this.packagePoints = packagePoints;
	}
	
	@Column(name="available_points")
	public Double getAvailablePoints() {
		return availablePoints;
	}
	public void setAvailablePoints(Double availablePoints) {
		this.availablePoints = availablePoints;
	}
	
	@Column(name="remaining_points")
	public Double getRemainingPoints() {
		return remainingPoints;
	}
	public void setRemainingPoints(Double remainingPoints) {
		this.remainingPoints = remainingPoints;
	}
	*/
	
	@Column(name="team_id")
	public Integer getTeamId() {
		return teamId;
	}
	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}
	
	@Column(name="team_name")
	public String getTeamName() {
		return teamName;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	
	@Column(name="package_selected")
	public Boolean getIsPackageSelected() {
		return isPackageSelected;
	}
	public void setIsPackageSelected(Boolean isPackageSelected) {
		this.isPackageSelected = isPackageSelected;
	}
	
	@Column(name="package_cost")
	public Double getPackageCost() {
		return packageCost;
	}
	public void setPackageCost(Double packageCost) {
		this.packageCost = packageCost;
	}
	
	@Column(name="package_notes")
	public String getPackageNote() {
		return packageNote;
	}
	public void setPackageNote(String packageNote) {
		this.packageNote = packageNote;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="zone")
	public String getTicketSelection() {
		return ticketSelection;
	}
	public void setTicketSelection(String ticketSelection) {
		this.ticketSelection = ticketSelection;
	}
	
	@Column(name="ticket_qty")
	public Integer getTicketQty() {
		return ticketQty;
	}
	public void setTicketQty(Integer ticketQty) {
		this.ticketQty = ticketQty;
	}
	
	@Column(name="ticket_price")
	public Double getTicketPrice() {
		return ticketPrice;
	}
	public void setTicketPrice(Double ticketPrice) {
		this.ticketPrice = ticketPrice;
	}
	
	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
		
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	@Column(name="create_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="last_update")
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	
	@Column(name="accept_reject_reason")
	public String getAcceptRejectReson() {
		return acceptRejectReson;
	}
	public void setAcceptRejectReson(String acceptRejectReson) {
		this.acceptRejectReson = acceptRejectReson;
	}
	
	@Column(name="regular_order_id")
	public Integer getRegularOrderId() {
		return regularOrderId;
	}
	public void setRegularOrderId(Integer regularOrderId) {
		this.regularOrderId = regularOrderId;
	}
	
	@Column(name="required_points")
	public Double getRequiredPoints() {
		return requiredPoints;
	}

	public void setRequiredPoints(Double requiredPoints) {
		this.requiredPoints = requiredPoints;
	}
	
	@Column(name="fantasy_grand_child_id")
	public Integer getFantasyGrandChildId() {
		return fantasyGrandChildId;
	}
	public void setFantasyGrandChildId(Integer fantasyGrandChildId) {
		this.fantasyGrandChildId = fantasyGrandChildId;
	}
	
	@Column(name="fantasy_event_id")
	public Integer getFantasyEventId() {
		return fantasyEventId;
	}
	public void setFantasyEventId(Integer fantasyEventId) {
		this.fantasyEventId = fantasyEventId;
	}
	
	@Column(name="fantasy_event_name")
	public String getFantasyEventName() {
		return fantasyEventName;
	}
	public void setFantasyEventName(String fantasyEventName) {
		this.fantasyEventName = fantasyEventName;
	}
	
	@Column(name="is_real_ticket")
	public Boolean getIsRealTicket() {
		return isRealTicket;
	}
	public void setIsRealTicket(Boolean isRealTicket) {
		this.isRealTicket = isRealTicket;
	}
	
	@Column(name="ticket_id")
	public Integer getTicketId() {
		return ticketId;
	}
	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}
	
	@Column(name="team_zone_id")	
	public Integer getTeamZoneId() {
		return teamZoneId;
	}
	public void setTeamZoneId(Integer teamZoneId) {
		this.teamZoneId = teamZoneId;
	}
	
	@Column(name="venue_name")
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	
	@Column(name="venue_city")
	public String getVenueCity() {
		return venueCity;
	}
	public void setVenueCity(String venueCity) {
		this.venueCity = venueCity;
	}
	
	@Column(name="venue_state")
	public String getVenueState() {
		return venueState;
	}
	public void setVenueState(String venueState) {
		this.venueState = venueState;
	}
	
	@Column(name="venue_country")
	public String getVenueCountry() {
		return venueCountry;
	}
	public void setVenueCountry(String venueCountry) {
		this.venueCountry = venueCountry;
	}
	
	@Column(name="grand_child_name")
	public String getGrandChildName() {
		return grandChildName;
	}
	public void setGrandChildName(String grandChildName) {
		this.grandChildName = grandChildName;
	}
	
	@Column(name="platform")
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	
	@Transient
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	@Transient
	public String getCreatedDateStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(createdDate);
	}
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	
	@Transient
	public String getLastUpdatedStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(lastUpdatedDate);
	}
	public void setLastUpdatedStr(String lastUpdatedStr) {
		this.lastUpdatedStr = lastUpdatedStr;
	}
	
	
}

