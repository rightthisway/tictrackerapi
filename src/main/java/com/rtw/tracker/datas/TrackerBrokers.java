package com.rtw.tracker.datas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tracker_brokers")
public class TrackerBrokers implements Serializable{

	private Integer id;
	private String companyName;
	private Double serviceFees;
	
	public TrackerBrokers(){}
	
	@Id
	@GeneratedValue
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="company_name")
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@Column(name="service_fees")
	public Double getServiceFees() {
		return serviceFees;
	}
	public void setServiceFees(Double serviceFees) {
		this.serviceFees = serviceFees;
	}
	
	
}
