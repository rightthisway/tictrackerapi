package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class FedexTracking implements Serializable{

	private String responseType;
	private Integer responseCode;
	private String trackingNo;
	private String CarrierCode;
	private String serviceType;
	private String serviceDescription;
	private String customerRefrence;
	private String shipmentId;
	private Date createdDate; 
	private Date actualDeliveryDate;
	private Date commitedDeliveryDate;
	private Integer deliveryAttempt;
	private String chargeType;
	private Double chargeAmount;
	private String dayOfWeek;
	
	private FedexAddress shipmentAddress;
	private FedexAddress originAddress;
	private FedexAddress destinationAddress;
	private FedexAddress actualAddress;
	private String anciliaryReasonDecription;
	
	List<FedexTrackingEvent> eventList;

	public String getResponseType() {
		return responseType;
	}

	public void setResponseType(String responseType) {
		this.responseType = responseType;
	}

	public Integer getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(Integer responseCode) {
		this.responseCode = responseCode;
	}

	public String getTrackingNo() {
		return trackingNo;
	}

	public void setTrackingNo(String trackingNo) {
		this.trackingNo = trackingNo;
	}

	public String getCarrierCode() {
		return CarrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		CarrierCode = carrierCode;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getServiceDescription() {
		return serviceDescription;
	}

	public void setServiceDescription(String serviceDescription) {
		this.serviceDescription = serviceDescription;
	}

	public String getCustomerRefrence() {
		return customerRefrence;
	}

	public void setCustomerRefrence(String customerRefrence) {
		this.customerRefrence = customerRefrence;
	}

	public String getShipmentId() {
		return shipmentId;
	}

	public void setShipmentId(String shipmentId) {
		this.shipmentId = shipmentId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getActualDeliveryDate() {
		return actualDeliveryDate;
	}

	public void setActualDeliveryDate(Date actualDeliveryDate) {
		this.actualDeliveryDate = actualDeliveryDate;
	}

	public Date getCommitedDeliveryDate() {
		return commitedDeliveryDate;
	}

	public void setCommitedDeliveryDate(Date commitedDeliveryDate) {
		this.commitedDeliveryDate = commitedDeliveryDate;
	}

	public Integer getDeliveryAttempt() {
		return deliveryAttempt;
	}

	public void setDeliveryAttempt(Integer deliveryAttempt) {
		this.deliveryAttempt = deliveryAttempt;
	}

	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	public Double getChargeAmount() {
		return chargeAmount;
	}

	public void setChargeAmount(Double chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	public String getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(String dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	public FedexAddress getShipmentAddress() {
		return shipmentAddress;
	}

	public void setShipmentAddress(FedexAddress shipmentAddress) {
		this.shipmentAddress = shipmentAddress;
	}

	public FedexAddress getOriginAddress() {
		return originAddress;
	}

	public void setOriginAddress(FedexAddress originAddress) {
		this.originAddress = originAddress;
	}

	public FedexAddress getDestinationAddress() {
		return destinationAddress;
	}

	public void setDestinationAddress(FedexAddress destinationAddress) {
		this.destinationAddress = destinationAddress;
	}

	public FedexAddress getActualAddress() {
		return actualAddress;
	}

	public void setActualAddress(FedexAddress actualAddress) {
		this.actualAddress = actualAddress;
	}

	public String getAnciliaryReasonDecription() {
		return anciliaryReasonDecription;
	}

	public void setAnciliaryReasonDecription(String anciliaryReasonDecription) {
		this.anciliaryReasonDecription = anciliaryReasonDecription;
	}

	public List<FedexTrackingEvent> getEventList() {
		return eventList;
	}

	public void setEventList(List<FedexTrackingEvent> eventList) {
		this.eventList = eventList;
	}
	
	
}
