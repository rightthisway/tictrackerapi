package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.map.annotate.JsonDeserialize;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.rtw.tmat.utils.Util;
import com.rtw.tracker.dao.implementation.DAORegistry;

@Entity
@Table(name="fantasy_event_teams")
public class CrownJewelTeams implements Serializable{
	
	/**
	 * serialVersionUID is added only to remove warning
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private Integer leagueId;
	private CrownJewelLeagues crownJewelLeagues;
	private String name;
	private Double odds;
	@JsonDeserialize(using = com.rtw.tracker.utils.CustomerDateAndTimeDeserialize.class)
	@JsonSerialize(using = com.rtw.tracker.utils.CustomerDateAndTimeSerializer.class)
	private Date cutOffDate;
	private Integer ticketsCount;
	/*private Boolean packageApplicable;
	private Double packageCost;
	private String packageNotes;*/
	private String lastUpdateddBy;
	@JsonDeserialize(using = com.rtw.tracker.utils.CustomerDateAndTimeDeserialize.class)
	@JsonSerialize(using = com.rtw.tracker.utils.CustomerDateAndTimeSerializer.class)
	private Date lastUpdated;
	private String status;
	private String lastUpdatedStr;
	//private EventDetails event;
	//private String city;
	private Integer eventId;
	private Integer ordersCount;
	private String fractionalOdd;
	private Integer markup;
	private Integer categoryTeamId;
	
	private String cutOffDateStr;
	
	private List<CrownJewelTeamZones> cjTeamZones;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="fantasy_event_id")
	public Integer getLeagueId() {
		return leagueId;
	}
	public void setLeagueId(Integer leagueId) {
		this.leagueId = leagueId;
	}
	@Transient
	public CrownJewelLeagues getCrownJewelLeagues() {
		if(leagueId == null) {
			return null;
		}
		if(crownJewelLeagues == null) {
			crownJewelLeagues = DAORegistry.getCrownJewelLeaguesDAO().get(leagueId);
		}
		
		return crownJewelLeagues;
	}
	public void setCrownJewelLeagues(CrownJewelLeagues crownJewelLeagues) {
		this.crownJewelLeagues = crownJewelLeagues;
	}
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Column(name="last_updated_by")
	public String getLastUpdateddBy() {
		return lastUpdateddBy;
	}
	public void setLastUpdateddBy(String lastUpdateddBy) {
		this.lastUpdateddBy = lastUpdateddBy;
	}
	
	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Column(name="odds")
	public Double getOdds() {
		return odds;
	}
	public void setOdds(Double odds) {
		this.odds = odds;
	}
	
	@Column(name="tickets_count")
	public Integer getTicketsCount() {
		return ticketsCount;
	}
	public void setTicketsCount(Integer ticketsCount) {
		this.ticketsCount = ticketsCount;
	}
	
	/*@Column(name="package_applicable")
	public Boolean getPackageApplicable() {
		return packageApplicable;
	}
	public void setPackageApplicable(Boolean packageApplicable) {
		this.packageApplicable = packageApplicable;
	}
	
	@Column(name="package_cost")
	public Double getPackageCost() {
		return packageCost;
	}
	public void setPackageCost(Double packageCost) {
		this.packageCost = packageCost;
	}
	
	@Column(name="package_notes")
	public String getPackageNotes() {
		return packageNotes;
	}
	
	public void setPackageNotes(String packageNotes) {
		this.packageNotes = packageNotes;
	}*/
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	/*@ManyToOne
	@JoinColumn(name="event_id")
	public EventDetails getEvent() {
		return event;
	}
	public void setEvent(EventDetails event) {
		this.event = event;
	}
	
	@Column(name="city")
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}*/
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	@Column(name="cutoff_date")
	public Date getCutOffDate() {
		return cutOffDate;
	}
	public void setCutOffDate(Date cutOffDate) {
		this.cutOffDate = cutOffDate;
	}
	
	@Column(name="fractional_odd")
	public String getFractionalOdd() {
		return fractionalOdd;
	}
	public void setFractionalOdd(String fractionalOdd) {
		this.fractionalOdd = fractionalOdd;
	}
	
	@Column(name="markup")
	public Integer getMarkup() {
		return markup;
	}
	public void setMarkup(Integer markup) {
		this.markup = markup;
	}
	
	@Column(name="category_team_id")
	public Integer getCategoryTeamId() {
		return categoryTeamId;
	}
	public void setCategoryTeamId(Integer categoryTeamId) {
		this.categoryTeamId = categoryTeamId;
	}
	@Transient
	public String getCutOffDateStr() {
		if(cutOffDate!=null){
			cutOffDateStr = Util.formatDateToMonthDateYear(cutOffDate);
		}
		return cutOffDateStr;
	}
	public void setCutOffDateStr(String cutOffDateStr) {
		this.cutOffDateStr = cutOffDateStr;
	}
	@Transient
	public String getLastUpdatedStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(getLastUpdated());
	}
	
	@Transient
	public void setLastUpdatedStr(String lastUpdatedStr) {
		this.lastUpdatedStr = lastUpdatedStr;
	}
	
	/*@Transient
	public List<EventDetails> getEventsByArtistCity() {
		if(event != null) {
			try {
				return DAORegistry.getEventDetailsDAO().getEventsByArtistCity(event.getArtistId(), event.getCity());
			} catch(Exception e) {
				
			}
		}
		return null;
	}*/
	
	@Transient
	public List<CrownJewelTeamZones> getCjTeamZones() {
		return cjTeamZones;
	}
	public void setCjTeamZones(List<CrownJewelTeamZones> cjTeamZones) {
		this.cjTeamZones = cjTeamZones;
	}
	
	@Transient
	public Integer getOrdersCount() {
		return ordersCount;
	}
	public void setOrdersCount(Integer ordersCount) {
		this.ordersCount = ordersCount;
	}
	
	
	
}
