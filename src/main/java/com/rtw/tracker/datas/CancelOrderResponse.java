package com.rtw.tracker.datas;



public class CancelOrderResponse {

	private Integer status;
	private com.rtw.tmat.utils.Error error;
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public com.rtw.tmat.utils.Error getError() {
		return error;
	}
	public void setError(com.rtw.tmat.utils.Error error) {
		this.error = error;
	}
	
}
