package com.rtw.tracker.datas;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.dao.implementation.DAORegistry;

@Entity
@Table(name="crownjewel_teams_audit")
public class CrownJewelTeamsAudit implements Serializable{
	
	/**
	 * serialVersionUID is added only to remove warning
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private Integer leagueId;
	private CrownJewelLeagues crownJewelLeagues;
	private String name;
	private Double odds;
	private Integer ticketsCount;
	/*private Boolean packageApplicable;
	private Double packageCost;
	private String packageNotes;*/
	private String lastUpdateddBy;
	private Date lastUpdated;
	private String status;
	private String lastUpdatedStr;
	private String action;
	private Integer eventId;
	//private String city;
	private Integer markup;
	
	
	public CrownJewelTeamsAudit(){
		
	}
	
	public CrownJewelTeamsAudit(CrownJewelTeams crownJewelTeam){
		this.leagueId=crownJewelTeam.getLeagueId();
		this.name=crownJewelTeam.getName();
		this.odds=crownJewelTeam.getOdds();
		this.ticketsCount=crownJewelTeam.getTicketsCount();
		this.lastUpdated=crownJewelTeam.getLastUpdated();
		this.lastUpdateddBy=crownJewelTeam.getLastUpdateddBy();
		this.status=crownJewelTeam.getStatus();
		this.markup = crownJewelTeam.getMarkup();
		this.eventId=crownJewelTeam.getEventId();
	}
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="league_id")
	public Integer getLeagueId() {
		return leagueId;
	}
	public void setLeagueId(Integer leagueId) {
		this.leagueId = leagueId;
	}
	@Transient
	public CrownJewelLeagues getCrownJewelLeagues() {
		if(leagueId == null) {
			return null;
		}
		if(crownJewelLeagues == null) {
			crownJewelLeagues = DAORegistry.getCrownJewelLeaguesDAO().get(leagueId);
		}
		
		return crownJewelLeagues;
	}
	public void setCrownJewelLeagues(CrownJewelLeagues crownJewelLeagues) {
		this.crownJewelLeagues = crownJewelLeagues;
	}
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="last_updated_by")
	public String getLastUpdateddBy() {
		return lastUpdateddBy;
	}
	public void setLastUpdateddBy(String lastUpdateddBy) {
		this.lastUpdateddBy = lastUpdateddBy;
	}
	
	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="action")
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	
	@Column(name="odds")
	public Double getOdds() {
		return odds;
	}

	public void setOdds(Double odds) {
		this.odds = odds;
	}

	@Column(name="tickets_count")
	public Integer getTicketsCount() {
		return ticketsCount;
	}

	public void setTicketsCount(Integer ticketsCount) {
		this.ticketsCount = ticketsCount;
	}

	/*@Column(name="package_applicable")
	public Boolean getPackageApplicable() {
		return packageApplicable;
	}

	public void setPackageApplicable(Boolean packageApplicable) {
		this.packageApplicable = packageApplicable;
	}

	@Column(name="package_cost")
	public Double getPackageCost() {
		return packageCost;
	}

	public void setPackageCost(Double packageCost) {
		this.packageCost = packageCost;
	}

	@Column(name="package_notes")
	public String getPackageNotes() {
		return packageNotes;
	}

	public void setPackageNotes(String packageNotes) {
		this.packageNotes = packageNotes;
	}*/

	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	/*@Column(name="city")
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}*/
	
	@Column(name="markup")
	public Integer getMarkup() {
		return markup;
	}

	public void setMarkup(Integer markup) {
		this.markup = markup;
	}

	@Transient
	public String getLastUpdatedStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(getLastUpdated());
	}
	
	@Transient
	public void setLastUpdatedStr(String lastUpdatedStr) {
		this.lastUpdatedStr = lastUpdatedStr;
	}
	

}
