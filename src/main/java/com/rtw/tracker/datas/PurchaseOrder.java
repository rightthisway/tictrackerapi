package com.rtw.tracker.datas;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.datas.POStatus;

/**
 * Entity class for PurchaseOrder
 * @author dthiyagarajan
 *
 */
@Entity
@Table(name= "purchase_order")
public class PurchaseOrder implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;
	private Integer customerId;
	private Double poTotal;
	private POStatus status;
	private String shippingType;
	private Integer shippingMethodId;
	private String externalPONo;
	private String createdBy;
	private Integer ticketCount;
	private Date createdTime;
	private Date lastUpdated;
	private Date voidDate;
	private Boolean isEmailed;
	private String consignmentPoNo;
	private String transactionOffice;
	private String trackingNo;
	private String csr;
	private String shippingNotes;
	private String externalNotes;
	private String internalNotes;
	private Integer totalQuantity;
	private Integer usedQunatity;
	private String customerName;
	private String purchaseOrderType;
		
	private String createTimeStr;
	
	private Boolean fedexLabelCreated;
	private Integer brokerId;
	
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	@Column(name="po_total")
	public Double getPoTotal() {
		try{
			if(poTotal != null){
				return Util.getRoundedValue(poTotal);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return poTotal;
	}

	public void setPoTotal(Double poTotal) {
		this.poTotal = poTotal;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="status")
	public POStatus getStatus() {
		return status;
	}

	public void setStatus(POStatus status) {
		this.status = status;
	}

	@Transient
	public String getShippingType() {
		return shippingType;
	}

	public void setShippingType(String shippingType) {
		this.shippingType = shippingType;
	}

	
	@Column(name="created_by")
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name="ticket_count")
	public Integer getTicketCount() {
		return ticketCount;
	}

	public void setTicketCount(Integer ticketCount) {
		this.ticketCount = ticketCount;
	}

	@Column(name="external_po_no")
	public String getExternalPONo() {
		return externalPONo;
	}

	public void setExternalPONo(String externalPONo) {
		this.externalPONo = externalPONo;
	}

	@Column(name="created_time")
	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	@Column(name="is_emailed")
	public Boolean getIsEmailed() {
		return isEmailed;
	}

	public void setIsEmailed(Boolean isEmailed) {
		this.isEmailed = isEmailed;
	}

	@Column(name="consignment_po_no")
	public String getConsignmentPoNo() {
		return consignmentPoNo;
	}

	public void setConsignmentPoNo(String consignmentPoNo) {
		this.consignmentPoNo = consignmentPoNo;
	}

	@Column(name="transaction_office")
	public String getTransactionOffice() {
		return transactionOffice;
	}

	public void setTransactionOffice(String transactionOffice) {
		this.transactionOffice = transactionOffice;
	}

	@Column(name="tracking_no")
	public String getTrackingNo() {
		return trackingNo;
	}

	public void setTrackingNo(String trackingNo) {
		this.trackingNo = trackingNo;
	}

	@Column(name="shipping_notes")
	public String getShippingNotes() {
		return shippingNotes;
	}

	public void setShippingNotes(String shippingNotes) {
		this.shippingNotes = shippingNotes;
	}

	@Column(name="external_notes")
	public String getExternalNotes() {
		return externalNotes;
	}

	public void setExternalNotes(String externalNotes) {
		this.externalNotes = externalNotes;
	}

	@Column(name="internal_notes")
	public String getInternalNotes() {
		return internalNotes;
	}

	public void setInternalNotes(String internalNotes) {
		this.internalNotes = internalNotes;
	}

	@Column(name="void_date")
	public Date getVoidDate() {
		return voidDate;
	}

	public void setVoidDate(Date voidDate) {
		this.voidDate = voidDate;
	}
	
	
	@Column(name="csr")
	public String getCsr() {
		return csr;
	}

	public void setCsr(String csr) {
		this.csr = csr;
	}

	@Column(name="shipping_method_id")
	public Integer getShippingMethodId() {
		return shippingMethodId;
	}

	public void setShippingMethodId(Integer shippingMethodId) {
		this.shippingMethodId = shippingMethodId;
	}
	
	@Column(name="purchase_order_type")
	public String getPurchaseOrderType() {
		return purchaseOrderType;
	}

	public void setPurchaseOrderType(String purchaseOrderType) {
		this.purchaseOrderType = purchaseOrderType;
	}

	@Transient
	public String getCreateTimeStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(createdTime);
	}

	public void setCreateTimeStr(String createTimeStr) {
		this.createTimeStr = createTimeStr;
	}

	@Transient
	public Integer getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(Integer totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	@Transient
	public Integer getUsedQunatity() {
		return usedQunatity;
	}

	public void setUsedQunatity(Integer usedQunatity) {
		this.usedQunatity = usedQunatity;
	}

	@Transient
	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	@Column(name="fedex_label_created")
	public Boolean getFedexLabelCreated() {
		return fedexLabelCreated;
	}

	public void setFedexLabelCreated(Boolean fedexLabelCreated) {
		this.fedexLabelCreated = fedexLabelCreated;
	}

	@Column(name="broker_id")
	public Integer getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}

	
}