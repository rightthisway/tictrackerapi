package com.rtw.tracker.datas;

import java.util.Date;

import com.rtw.tmat.utils.Util;

public class WebServiceTracking{

	private Integer id;
	private String customerIpAddress;
	private String platform;
	private String apiName;
	private Date hitDate;
	private Integer custId;
	private String description;
	private Integer contestId;
	private String appVersion;
	private String hitDateStr;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCustomerIpAddress() {
		return customerIpAddress;
	}
	public void setCustomerIpAddress(String customerIpAddress) {
		this.customerIpAddress = customerIpAddress;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	public String getApiName() {
		return apiName;
	}
	public void setApiName(String apiName) {
		this.apiName = apiName;
	}
	public Date getHitDate() {
		return hitDate;
	}
	public void setHitDate(Date hitDate) {
		this.hitDate = hitDate;
	}
	public Integer getCustId() {
		return custId;
	}
	public void setCustId(Integer custId) {
		this.custId = custId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getContestId() {
		return contestId;
	}
	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}
	public String getAppVersion() {
		return appVersion;
	}
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}
	public String getHitDateStr() {
		if(hitDate!=null){
			hitDateStr = Util.getTimeStamp(hitDate);
		}
		return hitDateStr;
	}
	public void setHitDateStr(String hitDateStr) {
		this.hitDateStr = hitDateStr;
	}
	
	
	
}