package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * class to represent a country 
 * @author Ulaganathan
 *
 */
@Entity
@Table(name="Loyalty_Settings")
public class LoyaltySettings implements Serializable {
	
	private Integer id;
	private Double rewardConversion;
	private Double dollerConversion;
	private Date fromDate;
	private Date toDate;
	private Date updatedTime;
	private String status;
	private String referralRewardCreditType;
	private Double contestCustReferralRewardPerc;
	/**
	 * 
	 * @return id
	 */
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id ,Id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="Reward_Conversion")
	public Double getRewardConversion() {
		return rewardConversion;
	}
	public void setRewardConversion(Double rewardConversion) {
		this.rewardConversion = rewardConversion;
	}
	
	@Column(name="Dollar_Conversion")
	public Double getDollerConversion() {
		return dollerConversion;
	}
	public void setDollerConversion(Double dollerConversion) {
		this.dollerConversion = dollerConversion;
	}
	
	@Column(name="From_Date")
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	
	@Column(name="To_Date")
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="updated_time")
	public Date getUpdatedTime() {
		return updatedTime;
	}
	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}

	@Column(name="contest_cust_referral_reward_perc")
	public Double getContestCustReferralRewardPerc() {
		return contestCustReferralRewardPerc;
	}
	public void setContestCustReferralRewardPerc(Double contestCustReferralRewardPerc) {
		this.contestCustReferralRewardPerc = contestCustReferralRewardPerc;
	}
	
	@Column(name="referral_reward_credit_type")
	public String getReferralRewardCreditType() {
		return referralRewardCreditType;
	}
	public void setReferralRewardCreditType(String referralRewardCreditType) {
		this.referralRewardCreditType = referralRewardCreditType;
	}
}
