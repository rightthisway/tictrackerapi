package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;

@Entity
@Table(name="pos_ticket_group")
public class POSTicketGroup implements Serializable{
	
	private String productType;
	private Integer productId;
	private Integer ticketGroupId;
	private Integer ticketGroupExposureTypeId;
	private Date ArrivedOnDate;
	private Integer originalTicketCount;
	private Integer remainingTicketCount;
	private Double retailPrice;
	private Double facePrice;
	private Double cost;
	private Double wholesalePrice;
	private String row;
	private String section;
	private String notes;
	private Date createDate;
	private Integer systemUserId;
	private Integer eventId;
	private Boolean statusId;
	private Integer ticketGroupSeatingTypeId;
	private Integer ticketGroupTypeId;
	private Integer clientBrokerId;
	private Integer clientBrokerEmployeeId;
	private String internalNotes;
	private Date actualPurchaseDate;
	private Integer highSeat;
	private Integer lowSeat;
	private Integer autoProcessWebRequests;
	private Boolean taxExempt;
	private Boolean ebayAuction;
	private String ebayAuctionNumber;
	private Date updateDatetime;
	private String notOnHandMessage;
	private Integer ticketGroupCodeId;
	private Integer showTndNontnd;
	private Integer officeId;
	private Boolean isDropSale;
	private Integer modSystemUserId;
	private Integer autoProcessUseLowSeats;
	//private Date rowVersion;
	private Integer unbroadcastDaysBeforeEvent;
	private Integer ticketGroupStockTypeId;
	private Integer shippigMethodSpecialId;
	private Boolean showNearTermOptionId;
	private Boolean tgNoteGrandFathered;
	private Date priceUpdateDatetiime;
	private Date eticketAttachDatetime;
	private Integer eticketAttachSystemUserId;
	private Boolean eticketInstantDownload;
	private Boolean eticketsStoredLocally;
	private Boolean overrideInstantDownloadPause;
	//private Boolean mergeRowVersion;
	private Integer splitRuleId;
	private Double lastWholesalePrice;
	private Integer wholesalePriceModSystemUserId;
	private String canonicalSection;
	private Date canonicalSectionGenerateDate;
	private String standardSectionTime;
	private Date barcodeAddedDatetime;
	private Integer barcodeAddedSystemUserId;
	
	private Double actualSoldPrice;
	private Integer purchaseOrderId;
	private Integer mappedQty;
	private String mappedSeatLow;
	private String mappedSeatHigh;
	private String onHand;
	private String seatNoString;
	
	public POSTicketGroup(){
		
	}
	
	@Column(name = "product_type")
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	
	@Column(name = "product_id")
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	
	@Id
	@Column(name = "ticket_group_id")
	public Integer getTicketGroupId() {
		return ticketGroupId;
	}
	public void setTicketGroupId(Integer ticketGroupId) {
		this.ticketGroupId = ticketGroupId;
	}
	
	@Column(name = "ticket_group_exposure_type_id")
	public Integer getTicketGroupExposureTypeId() {
		return ticketGroupExposureTypeId;
	}
	public void setTicketGroupExposureTypeId(Integer ticketGroupExposureTypeId) {
		this.ticketGroupExposureTypeId = ticketGroupExposureTypeId;
	}
	
	@Column(name = "arrived_on_date")
	public Date getArrivedOnDate() {
		return ArrivedOnDate;
	}
	public void setArrivedOnDate(Date arrivedOnDate) {
		ArrivedOnDate = arrivedOnDate;
	}
	
	@Column(name = "original_ticket_count")
	public Integer getOriginalTicketCount() {
		return originalTicketCount;
	}
	public void setOriginalTicketCount(Integer originalTicketCount) {
		this.originalTicketCount = originalTicketCount;
	}
	
	@Column(name = "remaining_ticket_count")
	public Integer getRemainingTicketCount() {
		return remainingTicketCount;
	}
	public void setRemainingTicketCount(Integer remainingTicketCount) {
		this.remainingTicketCount = remainingTicketCount;
	}
	
	@Column(name = "retail_price")
	public Double getRetailPrice() {
		try{
			if(retailPrice != null){
				return Util.getRoundedValue(retailPrice);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return retailPrice;
	}
	public void setRetailPrice(Double retailPrice) {
		this.retailPrice = retailPrice;
	}
	
	@Column(name = "face_price")
	public Double getFacePrice() {
		try{
			if(facePrice != null){
				return Util.getRoundedValue(facePrice);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return facePrice;
	}
	public void setFacePrice(Double facePrice) {
		this.facePrice = facePrice;
	}
	
	@Column(name = "cost")
	public Double getCost() {
		try{
			if(cost != null){
				return Util.getRoundedValue(cost);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	
	@Column(name = "wholesale_price")
	public Double getWholesalePrice() {
		try{
			if(wholesalePrice != null){
				return Util.getRoundedValue(wholesalePrice);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return wholesalePrice;
	}
	public void setWholesalePrice(Double wholesalePrice) {
		this.wholesalePrice = wholesalePrice;
	}
	
	@Column(name = "row")
	public String getRow() {
		return row;
	}
	public void setRow(String row) {
		this.row = row;
	}
	
	@Column(name = "section")
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	
	@Column(name = "notes")
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	@Column(name = "create_date")
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	@Column(name = "system_user_id")
	public Integer getSystemUserId() {
		return systemUserId;
	}
	public void setSystemUserId(Integer systemUserId) {
		this.systemUserId = systemUserId;
	}
	
	@Column(name = "event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name = "status_id")
	public Boolean getStatusId() {
		return statusId;
	}
	public void setStatusId(Boolean statusId) {
		this.statusId = statusId;
	}
	
	@Column(name = "ticket_group_seating_type_id")
	public Integer getTicketGroupSeatingTypeId() {
		return ticketGroupSeatingTypeId;
	}
	public void setTicketGroupSeatingTypeId(Integer ticketGroupSeatingTypeId) {
		this.ticketGroupSeatingTypeId = ticketGroupSeatingTypeId;
	}
	
	@Column(name = "ticket_group_type_id")
	public Integer getTicketGroupTypeId() {
		return ticketGroupTypeId;
	}
	public void setTicketGroupTypeId(Integer ticketGroupTypeId) {
		this.ticketGroupTypeId = ticketGroupTypeId;
	}
	
	@Column(name = "client_broker_id")
	public Integer getClientBrokerId() {
		return clientBrokerId;
	}
	public void setClientBrokerId(Integer clientBrokerId) {
		this.clientBrokerId = clientBrokerId;
	}
	
	@Column(name = "client_broker_employee_id")
	public Integer getClientBrokerEmployeeId() {
		return clientBrokerEmployeeId;
	}
	public void setClientBrokerEmployeeId(Integer clientBrokerEmployeeId) {
		this.clientBrokerEmployeeId = clientBrokerEmployeeId;
	}
	
	@Column(name = "internal_notes")
	public String getInternalNotes() {
		return internalNotes;
	}
	public void setInternalNotes(String internalNotes) {
		this.internalNotes = internalNotes;
	}
	
	@Column(name = "actual_purchase_date")
	public Date getActualPurchaseDate() {
		return actualPurchaseDate;
	}
	public void setActualPurchaseDate(Date actualPurchaseDate) {
		this.actualPurchaseDate = actualPurchaseDate;
	}
	
	@Column(name = "high_seat")
	public Integer getHighSeat() {
		return highSeat;
	}
	public void setHighSeat(Integer highSeat) {
		this.highSeat = highSeat;
	}
	
	@Column(name = "low_seat")
	public Integer getLowSeat() {
		return lowSeat;
	}
	public void setLowSeat(Integer lowSeat) {
		this.lowSeat = lowSeat;
	}
	
	@Column(name = "auto_process_web_requests")
	public Integer getAutoProcessWebRequests() {
		return autoProcessWebRequests;
	}
	public void setAutoProcessWebRequests(Integer autoProcessWebRequests) {
		this.autoProcessWebRequests = autoProcessWebRequests;
	}
	
	@Column(name = "tax_exempt")
	public Boolean getTaxExempt() {
		return taxExempt;
	}
	public void setTaxExempt(Boolean taxExempt) {
		this.taxExempt = taxExempt;
	}
	
	@Column(name = "ebay_auction")
	public Boolean getEbayAuction() {
		return ebayAuction;
	}
	public void setEbayAuction(Boolean ebayAuction) {
		this.ebayAuction = ebayAuction;
	}
	
	@Column(name = "ebay_auction_number")
	public String getEbayAuctionNumber() {
		return ebayAuctionNumber;
	}
	public void setEbayAuctionNumber(String ebayAuctionNumber) {
		this.ebayAuctionNumber = ebayAuctionNumber;
	}
	
	@Column(name = "update_datetime")
	public Date getUpdateDatetime() {
		return updateDatetime;
	}
	public void setUpdateDatetime(Date updateDatetime) {
		this.updateDatetime = updateDatetime;
	}
	
	@Column(name = "not_on_hand_message")
	public String getNotOnHandMessage() {
		return notOnHandMessage;
	}
	public void setNotOnHandMessage(String notOnHandMessage) {
		this.notOnHandMessage = notOnHandMessage;
	}
	
	@Column(name = "ticket_group_code_id")
	public Integer getTicketGroupCodeId() {
		return ticketGroupCodeId;
	}
	public void setTicketGroupCodeId(Integer ticketGroupCodeId) {
		this.ticketGroupCodeId = ticketGroupCodeId;
	}
	
	@Column(name = "show_tnd_nontnd")
	public Integer getShowTndNontnd() {
		return showTndNontnd;
	}
	public void setShowTndNontnd(Integer showTndNontnd) {
		this.showTndNontnd = showTndNontnd;
	}
	
	@Column(name = "office_id")
	public Integer getOfficeId() {
		return officeId;
	}
	public void setOfficeId(Integer officeId) {
		this.officeId = officeId;
	}
	
	@Column(name = "isDropSale")
	public Boolean getIsDropSale() {
		return isDropSale;
	}
	public void setIsDropSale(Boolean isDropSale) {
		this.isDropSale = isDropSale;
	}
	
	@Column(name = "mod_system_user_id")
	public Integer getModSystemUserId() {
		return modSystemUserId;
	}
	public void setModSystemUserId(Integer modSystemUserId) {
		this.modSystemUserId = modSystemUserId;
	}
	
	@Column(name = "auto_process_use_low_seats")
	public Integer getAutoProcessUseLowSeats() {
		return autoProcessUseLowSeats;
	}
	public void setAutoProcessUseLowSeats(Integer autoProcessUseLowSeats) {
		this.autoProcessUseLowSeats = autoProcessUseLowSeats;
	}
	/*
	@Column(name = "row_version")
	public Date getRowVersion() {
		return rowVersion;
	}
	public void setRowVersion(Date rowVersion) {
		this.rowVersion = rowVersion;
	}
	*/
	@Column(name = "unbroadcast_days_before_event")
	public Integer getUnbroadcastDaysBeforeEvent() {
		return unbroadcastDaysBeforeEvent;
	}
	public void setUnbroadcastDaysBeforeEvent(Integer unbroadcastDaysBeforeEvent) {
		this.unbroadcastDaysBeforeEvent = unbroadcastDaysBeforeEvent;
	}
	
	@Column(name = "ticket_group_stock_type_id")
	public Integer getTicketGroupStockTypeId() {
		return ticketGroupStockTypeId;
	}
	public void setTicketGroupStockTypeId(Integer ticketGroupStockTypeId) {
		this.ticketGroupStockTypeId = ticketGroupStockTypeId;
	}
	
	@Column(name = "shipping_method_special_id")
	public Integer getShippigMethodSpecialId() {
		return shippigMethodSpecialId;
	}
	public void setShippigMethodSpecialId(Integer shippigMethodSpecialId) {
		this.shippigMethodSpecialId = shippigMethodSpecialId;
	}
	
	@Column(name = "show_near_term_option_id")
	public Boolean getShowNearTermOptionId() {
		return showNearTermOptionId;
	}
	public void setShowNearTermOptionId(Boolean showNearTermOptionId) {
		this.showNearTermOptionId = showNearTermOptionId;
	}
	
	@Column(name = "tg_note_grandfathered")
	public Boolean getTgNoteGrandFathered() {
		return tgNoteGrandFathered;
	}
	public void setTgNoteGrandFathered(Boolean tgNoteGrandFathered) {
		this.tgNoteGrandFathered = tgNoteGrandFathered;
	}
	
	@Column(name = "price_update_datetime")
	public Date getPriceUpdateDatetiime() {
		return priceUpdateDatetiime;
	}
	public void setPriceUpdateDatetiime(Date priceUpdateDatetiime) {
		this.priceUpdateDatetiime = priceUpdateDatetiime;
	}
	
	@Column(name = "eticket_attach_datetime")
	public Date getEticketAttachDatetime() {
		return eticketAttachDatetime;
	}
	public void setEticketAttachDatetime(Date eticketAttachDatetime) {
		this.eticketAttachDatetime = eticketAttachDatetime;
	}
	
	@Column(name = "eticket_attach_system_user_id")
	public Integer getEticketAttachSystemUserId() {
		return eticketAttachSystemUserId;
	}
	public void setEticketAttachSystemUserId(Integer eticketAttachSystemUserId) {
		this.eticketAttachSystemUserId = eticketAttachSystemUserId;
	}
	
	@Column(name = "eticket_instant_download")
	public Boolean getEticketInstantDownload() {
		return eticketInstantDownload;
	}
	public void setEticketInstantDownload(Boolean eticketInstantDownload) {
		this.eticketInstantDownload = eticketInstantDownload;
	}
	
	@Column(name = "etickets_stored_locally")
	public Boolean getEticketsStoredLocally() {
		return eticketsStoredLocally;
	}
	public void setEticketsStoredLocally(Boolean eticketsStoredLocally) {
		this.eticketsStoredLocally = eticketsStoredLocally;
	}
	
	@Column(name = "override_instant_download_pause")
	public Boolean getOverrideInstantDownloadPause() {
		return overrideInstantDownloadPause;
	}
	public void setOverrideInstantDownloadPause(Boolean overrideInstantDownloadPause) {
		this.overrideInstantDownloadPause = overrideInstantDownloadPause;
	}
	/*
	@Column(name = "merge_row_version")
	public Boolean getMergeRowVersion() {
		return mergeRowVersion;
	}
	public void setMergeRowVersion(Boolean mergeRowVersion) {
		this.mergeRowVersion = mergeRowVersion;
	}
	*/
	
	@Column(name = "split_rule_id")
	public Integer getSplitRuleId() {
		return splitRuleId;
	}
	public void setSplitRuleId(Integer splitRuleId) {
		this.splitRuleId = splitRuleId;
	}
	
	@Column(name = "last_wholesale_price")
	public Double getLastWholesalePrice() {
		try{
			if(lastWholesalePrice != null){
				return Util.getRoundedValue(lastWholesalePrice);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return lastWholesalePrice;
	}
	public void setLastWholesalePrice(Double lastWholesalePrice) {
		this.lastWholesalePrice = lastWholesalePrice;
	}
	
	@Column(name = "wholesale_price_mod_system_user_id")
	public Integer getWholesalePriceModSystemUserId() {
		return wholesalePriceModSystemUserId;
	}
	public void setWholesalePriceModSystemUserId(
			Integer wholesalePriceModSystemUserId) {
		this.wholesalePriceModSystemUserId = wholesalePriceModSystemUserId;
	}
	
	@Column(name = "canonical_section")
	public String getCanonicalSection() {
		return canonicalSection;
	}
	public void setCanonicalSection(String canonicalSection) {
		this.canonicalSection = canonicalSection;
	}
	
	@Column(name = "canonical_section_generate_date")
	public Date getCanonicalSectionGenerateDate() {
		return canonicalSectionGenerateDate;
	}
	public void setCanonicalSectionGenerateDate(Date canonicalSectionGenerateDate) {
		this.canonicalSectionGenerateDate = canonicalSectionGenerateDate;
	}
	
	@Column(name = "standard_section_name")
	public String getStandardSectionTime() {
		return standardSectionTime;
	}
	public void setStandardSectionTime(String standardSectionTime) {
		this.standardSectionTime = standardSectionTime;
	}
	
	@Column(name = "barcode_added_datetime")
	public Date getBarcodeAddedDatetime() {
		return barcodeAddedDatetime;
	}
	public void setBarcodeAddedDatetime(Date barcodeAddedDatetime) {
		this.barcodeAddedDatetime = barcodeAddedDatetime;
	}
	
	@Column(name = "barcode_added_system_user_id")
	public Integer getBarcodeAddedSystemUserId() {
		return barcodeAddedSystemUserId;
	}
	public void setBarcodeAddedSystemUserId(Integer barcodeAddedSystemUserId) {
		this.barcodeAddedSystemUserId = barcodeAddedSystemUserId;
	}
	
	@Transient
	public Integer getMappedQty() {
		return mappedQty;
	}

	public void setMappedQty(Integer mappedQty) {
		this.mappedQty = mappedQty;
	}

	public void setMappedSeatHigh(String mappedSeatHigh) {
		this.mappedSeatHigh = mappedSeatHigh;
	}

	@Transient
	public Double getActualSoldPrice() {
		try{
			if(actualSoldPrice != null){
				return Util.getRoundedValue(actualSoldPrice);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return actualSoldPrice;
	}

	public void setActualSoldPrice(Double actualSoldPrice) {
		this.actualSoldPrice = actualSoldPrice;
	}
	@Transient
	public Integer getPurchaseOrderId() {
		return purchaseOrderId;
	}

	public void setPurchaseOrderId(Integer purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}
	@Transient
	public String getOnHand() {
		return onHand;
	}

	public void setOnHand(String onHand) {
		this.onHand = onHand;
	}
	@Transient
	public String getSeatNoString() {
		return seatNoString;
	}

	public void setSeatNoString(String seatNoString) {
		this.seatNoString = seatNoString;
	}
	
	
	
	

}
