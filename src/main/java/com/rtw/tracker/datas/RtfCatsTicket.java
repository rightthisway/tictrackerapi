package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.rtw.tmat.data.RewardthefanCategoryTicket;
import com.rtw.tmat.utils.TicketUtil;

@Entity
@Table(name="rtf_cat_tickets")
public class RtfCatsTicket implements Serializable{
	
	private Integer id;
	private Integer eId;
	private Integer qty;
	private Double price;
	@JsonIgnore
	private TicketStatus status;
	@JsonIgnore
	private Date createdDate;
	@JsonIgnore
	private Date lastUpdatedDate;
	@JsonIgnore
	private Integer invoiceId;
	@JsonIgnore
	private Boolean broadcast;
	/*@JsonIgnore
	private Integer brokerId;*/
	
	private Double tmatPrice;
		
	
	public RtfCatsTicket() {}
	public RtfCatsTicket(RewardthefanCategoryTicket rewardCatTicket) {
		this.eId = rewardCatTicket.getEventId();
		this.qty = rewardCatTicket.getQuantity();
		this.price = rewardCatTicket.getRtfPrice();
		this.tmatPrice = rewardCatTicket.getTmatPrice();
	}
	
		
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="event_id")
	public Integer geteId() {
		return eId;
	}
	public void seteId(Integer eId) {
		this.eId = eId;
	}
	
	@Column(name="qty")
	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	
	@Column(name="price")
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	
	@Column(name="status")
	@Enumerated(EnumType.STRING)
	public TicketStatus getStatus() {
		return status;
	}
	public void setStatus(TicketStatus status) {
		this.status = status;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="last_updated")
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	
	@Column(name="invoice_id")
	public Integer getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}
	
	@Column(name="broadcast")
	public Boolean getBroadcast() {
		return broadcast;
	}
	public void setBroadcast(Boolean broadcast) {
		this.broadcast = broadcast;
	}
	
	@Column(name="tmat_price")
	public Double getTmatPrice() {
		return tmatPrice;
	}
	public void setTmatPrice(Double tmatPrice) {
		this.tmatPrice = tmatPrice;
	}
	
	/*
	@Column(name="broker_id")
	public Integer getBrokerId() {
		return brokerId;
	}
	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}*/
	
}