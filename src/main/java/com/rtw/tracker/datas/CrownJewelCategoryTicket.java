package com.rtw.tracker.datas;



import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.data.Category;
import com.rtw.tmat.data.Ticket;
import com.rtw.tmat.enums.TicketDeliveryType;


@Entity
@Table(name="crownjewel_category_ticket")
public class CrownJewelCategoryTicket implements Serializable{
	
	private Integer id;
	private String section;
	private Integer quantity;
	private Double zonePrice;
	private String sectionRange;
	private String rowRange;
	private Date createdDate;
	private Date lastUpdatedDate;
	private String status;
	private String shippingMethod;
	private Integer eventId;
	
	
	
	@Id
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="section")
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	
	@Column(name="quantity")
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	
	@Column(name="zone_price")
	public Double getZonePrice() {
		return zonePrice;
	}
	public void setZonePrice(Double zonePrice) {
		this.zonePrice = zonePrice;
	}
	
	@Column(name="section_range")
	public String getSectionRange() {
		return sectionRange;
	}
	public void setSectionRange(String sectionRange) {
		this.sectionRange = sectionRange;
	}
	
	@Column(name="row_range")
	public String getRowRange() {
		return rowRange;
	}
	public void setRowRange(String rowRange) {
		this.rowRange = rowRange;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="last_updated")
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	@Column(name="shipping_method")
	public String getShippingMethod() {
		return shippingMethod;
	}
	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}
	
	
}

