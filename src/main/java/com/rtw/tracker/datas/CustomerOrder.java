package com.rtw.tracker.datas;

import java.io.Serializable;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.rtw.tmat.utils.Util;
import com.rtw.tracker.enums.OrderType;
import com.rtw.tracker.enums.PartialPaymentMethod;
import com.rtw.tracker.enums.PaymentMethod;
import com.rtw.tracker.enums.SecondaryOrderType;

@Entity
@Table(name = "customer_order")
public class CustomerOrder implements Serializable{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "customer_id", nullable=false)
	private Integer customerId;
	
	@Column(name = "quantity")
	private Integer qty;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "section")
	private String section;
	
	@Column(name = "row")
	private String row;
	
	@Column(name = "seat_low")
	private String seatLow;
	
	@Column(name = "seat_high")
	private String seatHigh;
	
	@Column(name = "event_id")
	private Integer eventId;
	
	@Column(name = "event_name")
	private String eventName;
	
	@JsonIgnore
	@Column(name = "event_date")
	private Date eventDate;
	
	@JsonIgnore
	@Column(name = "event_time")
	private Time eventTime;
	
	@Column(name = "venue_id")
	private Integer venueId;
	
	@Column(name = "venue_name")
	private String venueName;
	
	@Column(name = "ticket_price")
	private Double ticketPrice;
	
	@Column(name = "sold_price")
	private Double soldPrice;
	
	@Column(name = "order_total")
	private Double orderTotal;
	
	@Column(name = "discount_amt")
	private Double discountAmt;
	
	@Column(name = "ticket_group_notes")
	private String tixGroupNotes;
	
	@JsonIgnore
	@Column(name = "created_date")
	private Date createdDate;
	
	@JsonIgnore
	@Column(name = "last_updated")
	private Date lastUpdated;
	
	@JsonIgnore
	@Column(name = "accepted_date")
	private Date acceptedDate;
	
	@Column(name = "accepted_by")
	private String acceptedBy;
	
	@Column(name = "product_type")
	private String productType;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "platform")
	private ApplicationPlatform platform;
	
	@Column(name = "is_long_sale")
	private Boolean isLongSale;
	
	@Column(name = "actual_section")
	private String actualSection;
	
	
	@Transient
	private String shippingMethod;
	
	@Column(name = "shipping_method_id")
	private Integer shippingMethodId;
	
	@JsonIgnore
	@Column(name = "shipping_date")
	private Date shippingDate;
	
	@Enumerated(EnumType.STRING)
	@Column(name="primary_payment_method")
	private PaymentMethod primaryPaymentMethod;
	
	@Enumerated(EnumType.STRING)
	@Column(name="secondary_payment_method")
	private PartialPaymentMethod secondaryPaymentMethod;
	
	@Enumerated(EnumType.STRING)
	@Column(name="third_payment_method")
	private PartialPaymentMethod thirdPaymentMethod;
	
	@Column(name="original_order_total")
	private Double originalOrderTotal;
	
	@Column(name="primary_transaction_id")
	private String primaryTransactionId;
	
	@Column(name="secondary_transaction_id")
	private String secondaryTransactionId;
	
	@Column(name="third_payment_transaction_id")
	private String thirdTransactionId;
	
	@Column(name="venue_city")
	private String venueCity;
	
	@Column(name="venue_state")
	private String venueState;
	
	@Column(name="venue_country")
	private String venueCountry;
	
	@Column(name="event_venue_category")
	private String venueCategory;
	
	@Column(name="category_ticket_group_id")
	private Integer categoryTicketGroupId;
	
	@Enumerated(EnumType.STRING)
	@Column(name="order_type")
	private OrderType orderType;
	
	@Column(name="secondary_order_id")
	private String secondaryOrderId;
	
	@Enumerated(EnumType.STRING)
	@Column(name="secondary_order_type")
	private SecondaryOrderType secondaryOrdertype;
	
	@Column(name="primary_payment_amount")
	private Double primaryPayAmt;

	@Column(name="secondary_payment_amount")
	private Double secondaryPayAmt;
	
	@Column(name="third_payment_amount")
	private Double thirdPayAmt;
	
	@Column(name="cje_package_enabled")
	private Boolean cjePackageEnabled;
	
	@Column(name="cje_package_notes")
	private String cjePackageNote;
	
	@Column(name="cje_ticket_price")
	private Double cjeTicketPrice;
	
	@Column(name="cje_ticket_points")
	private Double cjeTicketPoints;
	
	@Column(name="cje_package_cost")
	private Double cjePackagePrice;
	
	@Column(name="cje_package_points")
	private Double cjePackagePoints;
	
	@Column(name="cje_required_points")
	private Double cjeRequirePoints;
	
	@Column(name = "tax_amount")
	private Double taxes;
	
	@Column(name = "section_description")
	private String sectionDescription;
	
	@Column(name = "seat_details")
	private String actualSeat;
	
	@Column(name = "primary_refund_amount")
	private Double primaryRefundAmount;
	
	@Column(name = "secondary_refund_amount")
	private Double secondaryRefundAmount;
	
	@Column(name = "third_refund_amount")
	private Double thirdRefundAmount;
	
	@Column(name="broker_id")
	private Integer brokerId;
	
	@Transient
	private Double primaryAvailableAmt;
	@Transient
	private Double secondaryAvailableAmt;
	@Transient
	private Double thirdAvailableAmt;
	
	@Transient
	private String eventDateStr;
	@Transient
	private String eventTimeStr;
	@Transient
	private String createdDateStr;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getRow() {
		return row;
	}

	public void setRow(String row) {
		this.row = row;
	}

	public String getSeatLow() {
		return seatLow;
	}

	public void setSeatLow(String seatLow) {
		this.seatLow = seatLow;
	}

	public String getSeatHigh() {
		return seatHigh;
	}

	public void setSeatHigh(String seatHigh) {
		this.seatHigh = seatHigh;
	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public Date getEventTime() {
		return eventTime;
	}

	public void setEventTime(Time eventTime) {
		this.eventTime = eventTime;
	}

	public Integer getVenueId() {
		return venueId;
	}

	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}

	public String getVenueName() {
		return venueName;
	}

	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}

	public Double getTicketPrice() {
		try{
			if(ticketPrice != null){
				return Util.getRoundedValue(ticketPrice);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return ticketPrice;
	}

	public void setTicketPrice(Double ticketPrice) {
		this.ticketPrice = ticketPrice;
	}

	public Double getSoldPrice() {
		try{
			if(soldPrice != null){
				return Util.getRoundedValue(soldPrice);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return soldPrice;
	}

	public void setSoldPrice(Double soldPrice) {
		this.soldPrice = soldPrice;
	}

	public Double getOrderTotal() {
		try{
			if(orderTotal != null){
				return Util.getRoundedValue(orderTotal);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return orderTotal;
	}

	public void setOrderTotal(Double orderTotal) {
		this.orderTotal = orderTotal;
	}

	public Double getDiscountAmt() {
		try{
			if(discountAmt != null){
				return Util.getRoundedValue(discountAmt);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return discountAmt;
	}

	public void setDiscountAmt(Double discountAmt) {
		this.discountAmt = discountAmt;
	}

	public String getTixGroupNotes() {
		return tixGroupNotes;
	}

	public void setTixGroupNotes(String tixGroupNotes) {
		this.tixGroupNotes = tixGroupNotes;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Date getAcceptedDate() {
		return acceptedDate;
	}

	public void setAcceptedDate(Date acceptedDate) {
		this.acceptedDate = acceptedDate;
	}

	public String getAcceptedBy() {
		return acceptedBy;
	}

	public void setAcceptedBy(String acceptedBy) {
		this.acceptedBy = acceptedBy;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getShippingMethod() {
		return shippingMethod;
	}

	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	public Date getShippingDate() {
		return shippingDate;
	}

	public void setShippingDate(Date shippingDate) {
		this.shippingDate = shippingDate;
	}

	public com.rtw.tracker.enums.PaymentMethod getPrimaryPaymentMethod() {
		return primaryPaymentMethod;
	}

	public void setPrimaryPaymentMethod(PaymentMethod primaryPaymentMethod) {
		this.primaryPaymentMethod = primaryPaymentMethod;
	}

	public Double getOriginalOrderTotal() {
		try{
			if(originalOrderTotal != null){
				return Util.getRoundedValue(originalOrderTotal);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return originalOrderTotal;
	}

	public void setOriginalOrderTotal(Double originalOrderTotal) {
		this.originalOrderTotal = originalOrderTotal;
	}

	public String getPrimaryTransactionId() {
		return primaryTransactionId;
	}

	public void setPrimaryTransactionId(String primaryTransactionId) {
		this.primaryTransactionId = primaryTransactionId;
	}

	public String getVenueCity() {
		return venueCity;
	}

	public void setVenueCity(String venueCity) {
		this.venueCity = venueCity;
	}

	public String getVenueState() {
		return venueState;
	}

	public void setVenueState(String venueState) {
		this.venueState = venueState;
	}

	public String getVenueCountry() {
		return venueCountry;
	}

	public void setVenueCountry(String venueCountry) {
		this.venueCountry = venueCountry;
	}

	public Integer getCategoryTicketGroupId() {
		return categoryTicketGroupId;
	}

	public void setCategoryTicketGroupId(Integer categoryTicketGroupId) {
		this.categoryTicketGroupId = categoryTicketGroupId;
	}

	public String getVenueCategory() {
		return venueCategory;
	}

	public void setVenueCategory(String venueCategory) {
		this.venueCategory = venueCategory;
	}

	public ApplicationPlatform getPlatform() {
		return platform;
	}

	public void setPlatform(ApplicationPlatform platform) {
		this.platform = platform;
	}

	public Integer getShippingMethodId() {
		return shippingMethodId;
	}

	public void setShippingMethodId(Integer shippingMethodId) {
		this.shippingMethodId = shippingMethodId;
	}

	public OrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}

	public String getSecondaryOrderId() {
		return secondaryOrderId;
	}

	public void setSecondaryOrderId(String secondaryOrderId) {
		this.secondaryOrderId = secondaryOrderId;
	}

	public SecondaryOrderType getSecondaryOrdertype() {
		return secondaryOrdertype;
	}

	public void setSecondaryOrdertype(SecondaryOrderType secondaryOrdertype) {
		this.secondaryOrdertype = secondaryOrdertype;
	}

	@Transient
	public String getEventDateStr() {
		if(eventDateStr != null && !eventDateStr.isEmpty()){
			return eventDateStr;
		}else{
			return Util.formatDateToMonthDateYear(eventDate);
		}
	}
	
	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}
	
	@Transient
	public String getEventTimeStr() {
		if(eventTimeStr != null && !eventTimeStr.isEmpty()){
			return eventTimeStr;
		}else{
			return Util.formatTimeToHourMinutes(eventTime);
		}
	}

	public void setEventTimeStr(String eventTimeStr) {
		this.eventTimeStr = eventTimeStr;
	}

	public Double getPrimaryPayAmt() {
		try{
			if(primaryPayAmt != null){
				return Util.getRoundedValue(primaryPayAmt);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return primaryPayAmt;
	}

	public void setPrimaryPayAmt(Double primaryPayAmt) {
		this.primaryPayAmt = primaryPayAmt;
	}

	public Double getSecondaryPayAmt() {
		try{
			if(secondaryPayAmt != null){
				return Util.getRoundedValue(secondaryPayAmt);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return secondaryPayAmt;
	}

	public void setSecondaryPayAmt(Double secondaryPayAmt) {
		this.secondaryPayAmt = secondaryPayAmt;
	}

	public String getSecondaryTransactionId() {
		return secondaryTransactionId;
	}

	public void setSecondaryTransactionId(String secondaryTransactionId) {
		this.secondaryTransactionId = secondaryTransactionId;
	}

	public PartialPaymentMethod getSecondaryPaymentMethod() {
		return secondaryPaymentMethod;
	}

	public void setSecondaryPaymentMethod(PartialPaymentMethod secondaryPaymentMethod) {
		this.secondaryPaymentMethod = secondaryPaymentMethod;
	}

	public Boolean getCjePackageEnabled() {
		return cjePackageEnabled;
	}

	public void setCjePackageEnabled(Boolean cjePackageEnabled) {
		this.cjePackageEnabled = cjePackageEnabled;
	}

	public String getCjePackageNote() {
		return cjePackageNote;
	}

	public void setCjePackageNote(String cjePackageNote) {
		this.cjePackageNote = cjePackageNote;
	}

	public Double getCjeTicketPrice() {
		try{
			if(cjeTicketPrice != null){
				return Util.getRoundedValue(cjeTicketPrice);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return cjeTicketPrice;
	}

	public void setCjeTicketPrice(Double cjeTicketPrice) {
		this.cjeTicketPrice = cjeTicketPrice;
	}

	public Double getCjeTicketPoints() {
		return cjeTicketPoints;
	}

	public void setCjeTicketPoints(Double cjeTicketPoints) {
		this.cjeTicketPoints = cjeTicketPoints;
	}

	public Double getCjePackagePrice() {
		try{
			if(cjePackagePrice != null){
				return Util.getRoundedValue(cjePackagePrice);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return cjePackagePrice;
	}

	public void setCjePackagePrice(Double cjePackagePrice) {
		this.cjePackagePrice = cjePackagePrice;
	}

	public Double getCjePackagePoints() {
		return cjePackagePoints;
	}

	public void setCjePackagePoints(Double cjePackagePoints) {
		this.cjePackagePoints = cjePackagePoints;
	}

	public Double getCjeRequirePoints() {
		return cjeRequirePoints;
	}

	public void setCjeRequirePoints(Double cjeRequirePoints) {
		this.cjeRequirePoints = cjeRequirePoints;
	}
	
	
	public Double getTaxes() {
		try{
			if(taxes != null){
				return Util.getRoundedValue(taxes);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return taxes;
	}
	public void setTaxes(Double taxes) {
		this.taxes = taxes;
	}

	public String getCreatedDateStr() {
		if(createdDate!=null){
			createdDateStr = Util.formatDateTimeToMonthDateYearAndHourMinute(createdDate);
		}
		return createdDateStr;
	}

	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	
	public PartialPaymentMethod getThirdPaymentMethod() {
		return thirdPaymentMethod;
	}

	public void setThirdPaymentMethod(PartialPaymentMethod thirdPaymentMethod) {
		this.thirdPaymentMethod = thirdPaymentMethod;
	}

	public String getThirdTransactionId() {
		return thirdTransactionId;
	}

	public void setThirdTransactionId(String thirdTransactionId) {
		this.thirdTransactionId = thirdTransactionId;
	}

	public Double getThirdPayAmt() {
		return thirdPayAmt;
	}

	public void setThirdPayAmt(Double thirdPayAmt) {
		this.thirdPayAmt = thirdPayAmt;
	}

	public String getSectionDescription() {
		return sectionDescription;
	}

	public void setSectionDescription(String sectionDescription) {
		this.sectionDescription = sectionDescription;
	}

	public Boolean getIsLongSale() {
		return isLongSale;
	}

	public void setIsLongSale(Boolean isLongSale) {
		this.isLongSale = isLongSale;
	}

	public String getActualSection() {
		return actualSection;
	}

	public void setActualSection(String actualSection) {
		this.actualSection = actualSection;
	}

	public String getActualSeat() {
		return actualSeat;
	}

	public void setActualSeat(String actualSeat) {
		this.actualSeat = actualSeat;
	}

	public Double getPrimaryRefundAmount() {
		return primaryRefundAmount;
	}

	public void setPrimaryRefundAmount(Double primaryRefundAmount) {
		this.primaryRefundAmount = primaryRefundAmount;
	}

	public Double getSecondaryRefundAmount() {
		return secondaryRefundAmount;
	}

	public void setSecondaryRefundAmount(Double secondaryRefundAmount) {
		this.secondaryRefundAmount = secondaryRefundAmount;
	}

	public Double getThirdRefundAmount() {
		return thirdRefundAmount;
	}

	public void setThirdRefundAmount(Double thirdRefundAmount) {
		this.thirdRefundAmount = thirdRefundAmount;
	}

	public Double getPrimaryAvailableAmt() {
		if(primaryPayAmt==null){
			primaryPayAmt = 0.000;
		}
		if(primaryRefundAmount==null){
			primaryRefundAmount = 0.000;
		}
		return primaryPayAmt - primaryRefundAmount;
	}

	public void setPrimaryAvailableAmt(Double primaryAvailableAmt) {
		this.primaryAvailableAmt = primaryAvailableAmt;
	}

	public Double getSecondaryAvailableAmt() {
		if(secondaryPayAmt==null){
			secondaryPayAmt = 0.000;
		}
		if(secondaryRefundAmount==null){
			secondaryRefundAmount = 0.000;
		}
		return secondaryPayAmt - secondaryRefundAmount;
	}

	public void setSecondaryAvailableAmt(Double secondaryAvailableAmt) {
		this.secondaryAvailableAmt = secondaryAvailableAmt;
	}

	public Double getThirdAvailableAmt() {
		if(thirdPayAmt==null){
			thirdPayAmt = 0.000;
		}
		if(thirdRefundAmount==null){
			thirdRefundAmount = 0.000;
		}
		return thirdPayAmt - thirdRefundAmount;
	}

	public void setThirdAvailableAmt(Double thirdAvailableAmt) {
		this.thirdAvailableAmt = thirdAvailableAmt;
	}

	public Integer getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}
	
	
}
