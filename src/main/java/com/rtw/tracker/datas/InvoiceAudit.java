package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.enums.InvoiceAuditAction;

@Entity
@Table(name="invoice_audit")
public class InvoiceAudit implements Serializable {

	private Integer id;
	private Integer invoiceId;
	private String note;
	private InvoiceAuditAction action;
	private Date createdDate;
	private String createBy;
	private String createdDateStr;
	
	public InvoiceAudit(){
		
	}
	
	public InvoiceAudit(Invoice invoice){
		this.invoiceId = invoice.getId();
	}
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="invoice_id")
	public Integer getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}
	
	@Column(name="note")
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
	@Column(name="action")
	@Enumerated(EnumType.STRING)
	public InvoiceAuditAction getAction() {
		return action;
	}
	public void setAction(InvoiceAuditAction action) {
		this.action = action;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="created_by")
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	
	@Transient
	public String getCreatedDateStr() {
		return Util.formatDateTimeToMonthDateYearAndHourMinute(createdDate);
	}
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	
	
	
}
