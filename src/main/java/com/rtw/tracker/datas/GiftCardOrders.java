package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import com.rtw.tmat.utils.Util;

@Entity
@Table(name = "giftcard_order")
public class GiftCardOrders implements Serializable {

	
	private Integer id;
	private Integer custId;
	private Integer cardId;
	private String cardTitle;
	private String cardDescription;
	private Integer quantity;
	private Double redeemedRewards;
	private Double originalCost;
	private String orderType;
	private Integer shipAddrId;
	private Boolean isEmailSent;
	private Boolean isOrderFulfilled;
	private String cardFileUrl;
	private Date createdDate;
	private String createdBy;
	private String status;
	private String updatedBy;
	private Date updatedDate;
	private String remarks;
	private Boolean isRedeemed;
	
	private String createdDateStr;
	private String updatedDateStr;
	private String firstName;
	private String lastName;
	private String userId;
	private String fileName;
	private String barcode;
	private String phone;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "customer_id")
	public Integer getCustId() {
		return custId;
	}

	public void setCustId(Integer custId) {
		this.custId = custId;
	}

	@Column(name = "card_id")
	public Integer getCardId() {
		return cardId;
	}

	public void setCardId(Integer cardId) {
		this.cardId = cardId;
	}

	@Column(name = "card_title")
	public String getCardTitle() {
		return cardTitle;
	}

	public void setCardTitle(String cardTitle) {
		this.cardTitle = cardTitle;
	}

	@Column(name = "card_description")
	public String getCardDescription() {
		return cardDescription;
	}

	public void setCardDescription(String cardDescription) {
		this.cardDescription = cardDescription;
	}

	@Column(name = "quantity")
	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Column(name = "redeemed_rewards")
	public Double getRedeemedRewards() {
		return redeemedRewards;
	}

	public void setRedeemedRewards(Double redeemedRewards) {
		this.redeemedRewards = redeemedRewards;
	}

	@Column(name = "original_cost")
	public Double getOriginalCost() {
		return originalCost;
	}

	public void setOriginalCost(Double originalCost) {
		this.originalCost = originalCost;
	}

	@Column(name = "order_type")
	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	@Column(name = "shipping_address_id")
	public Integer getShipAddrId() {
		return shipAddrId;
	}

	public void setShipAddrId(Integer shipAddrId) {
		this.shipAddrId = shipAddrId;
	}

	@Column(name = "is_email_sent")
	public Boolean getIsEmailSent() {
		return isEmailSent;
	}

	public void setIsEmailSent(Boolean isEmailSent) {
		this.isEmailSent = isEmailSent;
	}

	@Column(name = "is_order_fulfilled")
	public Boolean getIsOrderFulfilled() {
		return isOrderFulfilled;
	}

	public void setIsOrderFulfilled(Boolean isOrderFulfilled) {
		this.isOrderFulfilled = isOrderFulfilled;
	}

	@Column(name = "card_file_url")
	public String getCardFileUrl() {
		return cardFileUrl;
	}

	public void setCardFileUrl(String cardFileUrl) {
		this.cardFileUrl = cardFileUrl;
	}

	@Column(name = "created_date")
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	@Column(name = "updated_by")
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column(name = "updated_date")
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@Column(name = "remarks")
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	@Column(name = "barcode")
	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	
	
	@Column(name = "is_redeemed")
	public Boolean getIsRedeemed() {
		return isRedeemed;
	}

	public void setIsRedeemed(Boolean isRedeemed) {
		this.isRedeemed = isRedeemed;
	}

	@Transient
	public String getCreatedDateStr() {
		this.createdDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(createdDate);
		return createdDateStr;
	}

	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}

	@Transient
	public String getUpdatedDateStr() {
		if(updatedDate!=null){
			this.updatedDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(updatedDate);
		}
		return updatedDateStr;
	}

	public void setUpdatedDateStr(String updatedDateStr) {
		this.updatedDateStr = updatedDateStr;
	}

	@Transient
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Transient
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Transient
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Transient
	public String getFileName() {
		if(cardFileUrl!=null && cardFileUrl.contains("/")){
			fileName = cardFileUrl.substring(cardFileUrl.lastIndexOf("/")+1);
		}
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Transient
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	
	
	

}