package com.rtw.tracker.datas;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("CrownJewelOrderConfirmation")
public class CrownJewelOrderConfirmation {

	private Integer status;
	private com.rtw.tmat.utils.Error error;
	private Integer orderNo;
	private String message;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public com.rtw.tmat.utils.Error getError() {
		return error;
	}
	public void setError(com.rtw.tmat.utils.Error error) {
		this.error = error;
	}
	public Integer getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(Integer orderNo) {
		this.orderNo = orderNo;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
