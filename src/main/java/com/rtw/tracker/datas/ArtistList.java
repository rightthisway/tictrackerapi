package com.rtw.tracker.datas;

import java.util.List;
import com.thoughtworks.xstream.annotations.XStreamAlias;


@XStreamAlias("ArtistList")
public class ArtistList {
	private Integer status;
	private com.rtw.tmat.utils.Error error; 
	private List<Artist> artists;
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public com.rtw.tmat.utils.Error getError() {
		return error;
	}
	public void setError(com.rtw.tmat.utils.Error error) {
		this.error = error;
	}
	public List<Artist> getArtists() {
		return artists;
	}
	public void setArtists(List<Artist> artists) {
		this.artists = artists;
	}


}
