package com.rtw.tracker.datas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "customer_order_details")
public class CustomerOrderDetails implements Serializable{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "bl_address1")
	private String blAddress1;
	
	@Column(name = "bl_address2")
	private String blAddress2;
	
	@Column(name = "bl_city")
	private String blCity;
	
	@Column(name = "bl_state")
	private Integer blState;
	
	@Column(name = "bl_state_name")
	private String blStateName;
	
	@Column(name = "bl_country")
	private Integer blCountry;
	
	@Column(name = "bl_country_name")
	private String blCountryName;
	
	@Column(name = "bl_zipcode")
	private String blZipCode;

	@Column(name = "bl_first_name")
	private String blFirstName;
	
	@Column(name = "bl_last_name")
	private String blLastName;
	
	@Column(name = "bl_phone1")
	private String blPhone1;
	
	@Column(name = "bl_phone2")
	private String blPhone2;
	
	@Column(name = "bl_email")
	private String blEmail;
	
    //shipping address
	@Column(name = "sh_address1")
	private String shAddress1;
	
	@Column(name = "sh_address2")
	private String shAddress2;
	
	@Column(name = "sh_city")
	private String shCity;
	
	@Column(name = "sh_state")
	private Integer shState;
	
	@Column(name = "sh_state_name")
	private String shStateName;
	
	@Column(name = "sh_country")
	private Integer shCountry;
	
	@Column(name = "sh_country_name")
	private String shCountryName;
	
	@Column(name = "sh_zipcode")
	private String shZipCode;

	@Column(name = "sh_first_name")
	private String shFirstName;
	
	@Column(name = "sh_last_name")
	private String shLastName;
	
	@Column(name = "sh_phone1")
	private String shPhone1;
	
	@Column(name = "sh_phone2")
	private String shPhone2;
	
	@Column(name = "sh_email")
	private String shEmail;

	@Column(name = "order_id")
	private Integer orderId;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBlAddress1() {
		return blAddress1;
	}

	public void setBlAddress1(String blAddress1) {
		this.blAddress1 = blAddress1;
	}

	public String getBlAddress2() {
		return blAddress2;
	}

	public void setBlAddress2(String blAddress2) {
		this.blAddress2 = blAddress2;
	}

	public String getBlCity() {
		return blCity;
	}

	public void setBlCity(String blCity) {
		this.blCity = blCity;
	}

	public Integer getBlState() {
		return blState;
	}

	public void setBlState(Integer blState) {
		this.blState = blState;
	}

	public Integer getBlCountry() {
		return blCountry;
	}

	public void setBlCountry(Integer blCountry) {
		this.blCountry = blCountry;
	}

	public String getBlZipCode() {
		return blZipCode;
	}

	public void setBlZipCode(String blZipCode) {
		this.blZipCode = blZipCode;
	}

	public String getBlFirstName() {
		return blFirstName;
	}

	public void setBlFirstName(String blFirstName) {
		this.blFirstName = blFirstName;
	}

	public String getBlLastName() {
		return blLastName;
	}

	public void setBlLastName(String blLastName) {
		this.blLastName = blLastName;
	}

	public String getBlPhone1() {
		return blPhone1;
	}

	public void setBlPhone1(String blPhone1) {
		this.blPhone1 = blPhone1;
	}

	public String getBlPhone2() {
		return blPhone2;
	}

	public void setBlPhone2(String blPhone2) {
		this.blPhone2 = blPhone2;
	}

	public String getShAddress1() {
		return shAddress1;
	}

	public void setShAddress1(String shAddress1) {
		this.shAddress1 = shAddress1;
	}

	public String getShAddress2() {
		return shAddress2;
	}

	public void setShAddress2(String shAddress2) {
		this.shAddress2 = shAddress2;
	}

	public String getShCity() {
		return shCity;
	}

	public void setShCity(String shCity) {
		this.shCity = shCity;
	}

	public Integer getShState() {
		return shState;
	}

	public void setShState(Integer shState) {
		this.shState = shState;
	}

	public Integer getShCountry() {
		return shCountry;
	}

	public void setShCountry(Integer shCountry) {
		this.shCountry = shCountry;
	}

	public String getShZipCode() {
		return shZipCode;
	}

	public void setShZipCode(String shZipCode) {
		this.shZipCode = shZipCode;
	}

	public String getShFirstName() {
		return shFirstName;
	}

	public void setShFirstName(String shFirstName) {
		this.shFirstName = shFirstName;
	}

	public String getShLastName() {
		return shLastName;
	}

	public void setShLastName(String shLastName) {
		this.shLastName = shLastName;
	}

	public String getShPhone1() {
		return shPhone1;
	}

	public void setShPhone1(String shPhone1) {
		this.shPhone1 = shPhone1;
	}

	public String getShPhone2() {
		return shPhone2;
	}

	public void setShPhone2(String shPhone2) {
		this.shPhone2 = shPhone2;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public String getBlStateName() {
		return blStateName;
	}

	public void setBlStateName(String blStateName) {
		this.blStateName = blStateName;
	}

	public String getBlCountryName() {
		return blCountryName;
	}

	public void setBlCountryName(String blCountryName) {
		this.blCountryName = blCountryName;
	}

	public String getShStateName() {
		return shStateName;
	}

	public void setShStateName(String shStateName) {
		this.shStateName = shStateName;
	}

	public String getShCountryName() {
		return shCountryName;
	}

	public void setShCountryName(String shCountryName) {
		this.shCountryName = shCountryName;
	}

	public String getBlEmail() {
		return blEmail;
	}

	public void setBlEmail(String blEmail) {
		this.blEmail = blEmail;
	}

	public String getShEmail() {
		return shEmail;
	}

	public void setShEmail(String shEmail) {
		this.shEmail = shEmail;
	}
	
	
}
