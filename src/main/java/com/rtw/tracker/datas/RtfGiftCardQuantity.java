package com.rtw.tracker.datas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="gift_card_value")
public class RtfGiftCardQuantity implements Serializable{
	
	private Integer id;
	private Integer cardId;
	private Double amount;
	private Integer maxQty;
	private Integer usedQty;
	private Integer freeQty;
	private Boolean status;
	private Integer maxQtyThreshold;
	
	@Id
	@GeneratedValue
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="card_id")
	public Integer getCardId() {
		return cardId;
	}
	public void setCardId(Integer cardId) {
		this.cardId = cardId;
	}
	
	@Column(name="card_amount")
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	@Column(name="max_quantity")
	public Integer getMaxQty() {
		return maxQty;
	}
	public void setMaxQty(Integer maxQty) {
		this.maxQty = maxQty;
	}
	
	@Column(name="used_quantity")
	public Integer getUsedQty() {
		return usedQty;
	}
	public void setUsedQty(Integer usedQty) {
		this.usedQty = usedQty;
	}
	
	@Column(name="remaining_quantity")
	public Integer getFreeQty() {
		return freeQty;
	}
	public void setFreeQty(Integer freeQty) {
		this.freeQty = freeQty;
	}
	
	@Column(name="status")
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Column(name="max_qty_threshold")
	public Integer getMaxQtyThreshold() {
		return maxQtyThreshold;
	}
	public void setMaxQtyThreshold(Integer maxQtyThreshold) {
		this.maxQtyThreshold = maxQtyThreshold;
	}
	
	

}
