package com.rtw.tracker.datas;

public enum POStatus {
	ACTIVE, EXPIRED, VOID, DISABLED
}
