package com.rtw.tracker.datas;

import java.io.Serializable;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;

@Entity
@Table(name="seatgeek_orders")
public class SeatGeekOrders  implements Serializable {
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String orderId;
	private Integer catTicketId;
	private Integer eventId;
	private String section;
	private String row;
	private Integer quantity;
	private Double totalSaleAmt;
	private Double totalPaymentAmt;
	private Integer instantDownload;
	private SeatGeekOrderStatus status;
	private String eventDate;
	private String eventTime;
	private String eventName;
	private String venueName;
	private String source;
	private String deliveryMethod;
	private Double ticketSoldPrice;
	
	private Date createdDate;
	private Date lastUpdatedDate;
	private Date orderDate;
	private Double totalAmt;
	private Double subtotalAmt;
	private Double feesAmt;
	
	private String acceptedBy;
	private String rejectedBy;
	private String lastUpdatedBy;
	private String seatGeekFileNames;
	
	
	private Integer ticTrackerOrderId;
	private Integer ticTrackerInvoiceId;
	
	private String orderDateStr;
	private String lastUpdatedDateStr;
	@Transient
	private String eventDateStr;
	@Transient
	private String eventTimeStr;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)  
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	public SeatGeekOrderStatus getStatus() {
		return status;
	}
	public void setStatus(SeatGeekOrderStatus status) {
		this.status = status;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="last_updated")
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	
	@Column(name="order_id")
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	
	@Column(name="cat_ticket_id")
	public Integer getCatTicketId() {
		return catTicketId;
	}
	public void setCatTicketId(Integer catTicketId) {
		this.catTicketId = catTicketId;
	}
	
	@Column(name="section")
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	
	@Column(name="row")
	public String getRow() {
		return row;
	}
	public void setRow(String row) {
		this.row = row;
	}
	
	@Column(name="quantity")
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	@Column(name="total_sale")
	public Double getTotalSaleAmt() {
		return totalSaleAmt;
	}
	public void setTotalSaleAmt(Double totalSaleAmt) {
		this.totalSaleAmt = totalSaleAmt;
	}
	
	@Column(name="total_payment")
	public Double getTotalPaymentAmt() {
		return totalPaymentAmt;
	}
	public void setTotalPaymentAmt(Double totalPaymentAmt) {
		this.totalPaymentAmt = totalPaymentAmt;
	}
	
	@Column(name="instant_download")
	public Integer getInstantDownload() {
		return instantDownload;
	}
	public void setInstantDownload(Integer instantDownload) {
		this.instantDownload = instantDownload;
	}
	
	/*@Column(name="event_date")
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	
	@Column(name="event_time")
	public Time getEventTime() {
		return eventTime;
	}
	public void setEventTime(Time eventTime) {
		this.eventTime = eventTime;
	}*/
	
	@Column(name="event_name")
	public String getEventName() {
		return eventName;
	}
	
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	@Column(name="event_date")
	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	
	@Column(name="event_time")
	public String getEventTime() {
		return eventTime;
	}
	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}
	
	@Column(name="venue_name")
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	
	@Column(name="source")
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	
	@Column(name="delivery_method")
	public String getDeliveryMethod() {
		return deliveryMethod;
	}
	public void setDeliveryMethod(String deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}
	@Column(name="ticket_sold_price")
	public Double getTicketSoldPrice() {
		return ticketSoldPrice;
	}
	public void setTicketSoldPrice(Double ticketSoldPrice) {
		this.ticketSoldPrice = ticketSoldPrice;
	}
	
	@Column(name="order_date")
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	
	@Column(name="total_amt")
	public Double getTotalAmt() {
		return totalAmt;
	}
	public void setTotalAmt(Double totalAmt) {
		this.totalAmt = totalAmt;
	}
	
	@Column(name="sub_total_amt")
	public Double getSubtotalAmt() {
		return subtotalAmt;
	}
	public void setSubtotalAmt(Double subtotalAmt) {
		this.subtotalAmt = subtotalAmt;
	}
	
	@Column(name="fees_amt")
	public Double getFeesAmt() {
		return feesAmt;
	}
	public void setFeesAmt(Double feesAmt) {
		this.feesAmt = feesAmt;
	}
	
	@Column(name="accepted_by")
	public String getAcceptedBy() {
		return acceptedBy;
	}
	public void setAcceptedBy(String acceptedBy) {
		this.acceptedBy = acceptedBy;
	}
	
	@Column(name="rejected_by")
	public String getRejectedBy() {
		return rejectedBy;
	}
	public void setRejectedBy(String rejectedBy) {
		this.rejectedBy = rejectedBy;
	}
	
	@Column(name="last_updated_by")
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	@Column(name="seat_geek_file_names")	
	public String getSeatGeekFileNames() {
		return seatGeekFileNames;
	}
	public void setSeatGeekFileNames(String seatGeekFileNames) {
		this.seatGeekFileNames = seatGeekFileNames;
	}
	@Transient
	public Integer getTicTrackerOrderId() {
		return ticTrackerOrderId;
	}
	@Transient
	public void setTicTrackerOrderId(Integer ticTrackerOrderId) {
		this.ticTrackerOrderId = ticTrackerOrderId;
	}
	@Transient
	public Integer getTicTrackerInvoiceId() {
		return ticTrackerInvoiceId;
	}
	@Transient
	public void setTicTrackerInvoiceId(Integer ticTrackerInvoiceId) {
		this.ticTrackerInvoiceId = ticTrackerInvoiceId;
	}
	
	@Transient
	public String getOrderDateStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(orderDate);
	}
	public void setOrderDateStr(String orderDateStr) {
		this.orderDateStr = orderDateStr;
	}
	
	@Transient
	public String getLastUpdatedDateStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(lastUpdatedDate);
	}
	public void setLastUpdatedDateStr(String lastUpdatedDateStr) {
		this.lastUpdatedDateStr = lastUpdatedDateStr;
	}
	
	@Transient
	public String getEventDateStr() {
		Date convertEventDate = null;		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		try {
			convertEventDate = formatter.parse(eventDate);
		} catch (ParseException e) {
			System.out.println(e);
		}		
		return Util.formatDateToMonthDateYear(convertEventDate);		
	} 
	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}
	
	@Transient
	public String getEventTimeStr() {
		Time convertEventTimes = null;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		try{
			Date convertEventTime = formatter.parse(eventTime);	
			convertEventTimes =  new java.sql.Time(convertEventTime.getTime());
		}catch(Exception ex){
			System.out.println("Exception in Event Time: "+ex);
		}
		return Util.formatTimeToHourMinutes(convertEventTimes);
	}
	public void setEventTimeStr(String eventTimeStr) {
		this.eventTimeStr = eventTimeStr;
	}
	
}

