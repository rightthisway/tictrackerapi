package com.rtw.tracker.datas;

import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.enums.OrderType;

public class RewardDetails {

	private OrderType orderType;
	private Integer customerId;
	private Integer orderNo;
	private Double orderTotal;
	private Double rewards;
	private Date orderDate;
	private Date eventDate;
	private Date activeDate;
	private Time eventTime;
	private String eventName;
	
	private String orderDateStr;
	private String eventDateStr;
	private String eventTimeStr;
	private String activeDateStr;	
	
	public OrderType getOrderType() {
		return orderType;
	}
	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}	
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public Integer getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(Integer orderNo) {
		this.orderNo = orderNo;
	}
	public Double getOrderTotal() {
		return orderTotal;
	}
	public void setOrderTotal(Double orderTotal) {
		this.orderTotal = orderTotal;
	}
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	public Time getEventTime() {
		return eventTime;
	}
	public void setEventTime(Time eventTime) {
		this.eventTime = eventTime;
	}	
	public Double getRewards() {
		return rewards;
	}
	public void setRewards(Double rewards) {
		this.rewards = rewards;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}	
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}	
	public Date getActiveDate() {
		return activeDate;
	}
	public void setActiveDate(Date activeDate) {
		this.activeDate = activeDate;
	}
	
	public String getOrderDateStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(getOrderDate());
	}
	public void setOrderDateStr(String orderDateStr) {
		this.orderDateStr = orderDateStr;
	}
	public String getEventDateStr() {
		if(getEventDate()==null){
			return "TBD";
		}
		return Util.formatDateToMonthDateYear(getEventDate());
	}
	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}
	public String getEventTimeStr() {
		if(getEventTime()==null){
			return "TBD";
		}
		return Util.formatTimeToHourMinutes(getEventTime());
	}
	public void setEventTimeStr(String eventTimeStr) {
		this.eventTimeStr = eventTimeStr;
	}
	public String getActiveDateStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(getActiveDate());
	}
	public void setActiveDateStr(String activeDateStr) {
		this.activeDateStr = activeDateStr;
	}
}
