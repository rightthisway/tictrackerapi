package com.rtw.tracker.datas;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

import com.rtw.tracker.enums.LockedTicketStatus;

@Entity
@Table(name="autocats_locked_tickets")
public class AutoCatsLockedTickets implements Serializable{
		

	private Integer id;
	private ProductType productType;
	private String sessionId;
	private Date creationDate;
	private Integer eventId;
	private Integer catgeoryTicketGroupId;
	private LockedTicketStatus lockStatus;
	private String ipAddress;
	private String platform;
	
	/**
	 * 
	 * @return id
	 */
	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id, 
	 */
	public void setId(Integer id) {
		this.id = id;
	}	
	
	@Column(name="lock_status", nullable=false)
	@Enumerated(EnumType.STRING)
	@Index(name="lockStatusIndex")
	public LockedTicketStatus getLockStatus() {
		return lockStatus;
	}
	public void setLockStatus(LockedTicketStatus lockStatus) {
		this.lockStatus = lockStatus;
	}
	
	@Column(name="product_type", nullable=false)
	@Enumerated(EnumType.STRING)
	@Index(name="productTypeIndex")	
	public ProductType getProductType() {
		return productType;
	}
	public void setProductType(ProductType productType) {
		this.productType = productType;
	}
	
	@Column(name="session_id")
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	@Column(name="creation_date")
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	@Column(name="eventId")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="category_ticket_group_id")
	public Integer getCatgeoryTicketGroupId() {
		return catgeoryTicketGroupId;
	}
	public void setCatgeoryTicketGroupId(Integer catgeoryTicketGroupId) {
		this.catgeoryTicketGroupId = catgeoryTicketGroupId;
	}
	
	@Column(name="ip_address")
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	@Column(name="platform")
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	
}
