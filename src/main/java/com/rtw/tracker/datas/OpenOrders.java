package com.rtw.tracker.datas;

import java.io.Serializable;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.rtw.tmat.utils.Util;
import javax.persistence.Transient;


public class OpenOrders implements Serializable{

	private Integer customerId;
	private String customerName;
	private String lastName;
	private String username;
	private String customerType;
	private String productType;
	private String addressLine1;
	private String city;
	private String status;
	private String zipCode;
	private String phone;
	private String country;
	private String platform;
	private String state;
	private Integer invoiceId;
	private String invoiceType;
	private Integer customerOrderId;
	private Integer ticketCount;
	private double invoiceTotal;
	private double ticketTotal;
	private Integer eventId;
	private String eventName;
	private Date eventDate;
	private Time eventTime;
	private String section;
	private String row;
	private Integer qty;
	private Double price;
	private Double soldPrice;
	private String rowRange;
	private String sectionRange;
	private String shippingMethod;
	private Integer shippingMethodId;
	private Date createdDate;
	private String createdBy;
	private String csr;
	private String purchaseOrderNo;
	private Date lastUpdatedDate;
	private String lastUpdatedBy;
	
	private String custCompanyName;
	private Integer invoiceAged;
	private Boolean isInvoiceEmailSent;
	private Date voidedDate;
	private String realTixShippingMethod;
	private String realTixMap;
	private Boolean realTixDelivered;
	private String trackingNo;
	private String voidedDateStr;
	private String createdDateStr;
	private String lastUpdatedDateStr;
	private String secondaryOrderType;
	private String secondaryOrderId;
	
	private String eventDateStr;
	private String eventTimeStr;
	
	private Boolean fedexLabelCreated;
	private Integer brokerId;
	private String orderType;
	
	private String primaryPaymentMethod;
	private String secondaryPaymentMethod;
	private String thirdPaymentMethod;
	private String isPoMapped;
	private Double pendingAmount;
	
	public Boolean getFedexLabelCreated() {
		return fedexLabelCreated;
	}

	public void setFedexLabelCreated(Boolean fedexLabelCreated) {
		this.fedexLabelCreated = fedexLabelCreated;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public Integer getTicketCount() {
		return ticketCount;
	}

	public void setTicketCount(Integer ticketCount) {
		this.ticketCount = ticketCount;
	}

	public double getInvoiceTotal() {
		return invoiceTotal;
	}

	public void setInvoiceTotal(double invoiceTotal) {
		this.invoiceTotal = invoiceTotal;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}

	public Integer getCustomerOrderId() {
		return customerOrderId;
	}

	public void setCustomerOrderId(Integer customerOrderId) {
		this.customerOrderId = customerOrderId;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public Time getEventTime() {
		return eventTime;
	}

	public void setEventTime(Time eventTime) {
		this.eventTime = eventTime;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getRow() {
		return row;
	}

	public void setRow(String row) {
		this.row = row;
	}

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getRowRange() {
		return rowRange;
	}

	public void setRowRange(String rowRange) {
		this.rowRange = rowRange;
	}

	public String getSectionRange() {
		return sectionRange;
	}

	public void setSectionRange(String sectionRange) {
		this.sectionRange = sectionRange;
	}

	public String getShippingMethod() {
		return shippingMethod;
	}

	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public String getCustCompanyName() {
		return custCompanyName;
	}

	public void setCustCompanyName(String custCompanyName) {
		this.custCompanyName = custCompanyName;
	}

	public Integer getInvoiceAged() {
		return invoiceAged;
	}

	public void setInvoiceAged(Integer invoiceAged) {
		this.invoiceAged = invoiceAged;
	}

	public Boolean getIsInvoiceEmailSent() {
		return isInvoiceEmailSent;
	}

	public void setIsInvoiceEmailSent(Boolean isInvoiceEmailSent) {
		this.isInvoiceEmailSent = isInvoiceEmailSent;
	}

	public Date getVoidedDate() {
		return voidedDate;
	}

	public void setVoidedDate(Date voidedDate) {
		this.voidedDate = voidedDate;
	}

	public String getRealTixShippingMethod() {
		return realTixShippingMethod;
	}

	public void setRealTixShippingMethod(String realTixShippingMethod) {
		this.realTixShippingMethod = realTixShippingMethod;
	}

	public String getRealTixMap() {
		return realTixMap;
	}

	public void setRealTixMap(String realTixMap) {
		this.realTixMap = realTixMap;
	}

	public Boolean getRealTixDelivered() {
		return realTixDelivered;
	}

	public void setRealTixDelivered(Boolean realTixDelivered) {
		this.realTixDelivered = realTixDelivered;
	}

	public String getVoidedDateStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(voidedDate);
	}

	public void setVoidedDateStr(String voidedDateStr) {
		this.voidedDateStr = voidedDateStr;
	}

	public String getCreatedDateStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(createdDate);
	}

	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}

	public String getLastUpdatedDateStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(lastUpdatedDate);
	}

	public void setLastUpdatedDateStr(String lastUpdatedDateStr) {
		this.lastUpdatedDateStr = lastUpdatedDateStr;
	}

	public String getTrackingNo() {
		return trackingNo;
	}

	public void setTrackingNo(String trackingNo) {
		this.trackingNo = trackingNo;
	}

	public Integer getShippingMethodId() {
		return shippingMethodId;
	}

	public void setShippingMethodId(Integer shippingMethodId) {
		this.shippingMethodId = shippingMethodId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPurchaseOrderNo() {
		return purchaseOrderNo;
	}

	public void setPurchaseOrderNo(String purchaseOrderNo) {
		this.purchaseOrderNo = purchaseOrderNo;
	}

	public String getCsr() {
		return csr;
	}

	public void setCsr(String csr) {
		this.csr = csr;
	}

	public String getSecondaryOrderType() {
		return secondaryOrderType;
	}

	public void setSecondaryOrderType(String secondaryOrderType) {
		this.secondaryOrderType = secondaryOrderType;
	}

	public String getSecondaryOrderId() {
		return secondaryOrderId;
	}

	public void setSecondaryOrderId(String secondaryOrderId) {
		this.secondaryOrderId = secondaryOrderId;
	}
	
	
	public String getEventDateStr() {
		return Util.formatDateToMonthDateYear(eventDate);
	}
	
	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}
	
	
	public String getEventTimeStr() {
		return Util.formatTimeToHourMinutes(eventTime); 
	}

	public void setEventTimeStr(String eventTimeStr) {
		this.eventTimeStr = eventTimeStr;
	}

	public double getTicketTotal() {
		return ticketTotal;
	}

	public void setTicketTotal(double ticketTotal) {
		this.ticketTotal = ticketTotal;
	}

	public Integer getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public Double getSoldPrice() {
		return soldPrice;
	}

	public void setSoldPrice(Double soldPrice) {
		this.soldPrice = soldPrice;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}
	
	public String getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}

	public String getPrimaryPaymentMethod() {
		return primaryPaymentMethod;
	}

	public void setPrimaryPaymentMethod(String primaryPaymentMethod) {
		this.primaryPaymentMethod = primaryPaymentMethod;
	}

	public String getSecondaryPaymentMethod() {
		return secondaryPaymentMethod;
	}

	public void setSecondaryPaymentMethod(String secondaryPaymentMethod) {
		this.secondaryPaymentMethod = secondaryPaymentMethod;
	}

	public String getThirdPaymentMethod() {
		return thirdPaymentMethod;
	}

	public void setThirdPaymentMethod(String thirdPaymentMethod) {
		this.thirdPaymentMethod = thirdPaymentMethod;
	}

	public String getIsPoMapped() {
		return isPoMapped;
	}

	public void setIsPoMapped(String isPoMapped) {
		this.isPoMapped = isPoMapped;
	}

	public Double getPendingAmount() {
		return pendingAmount;
	}

	public void setPendingAmount(Double pendingAmount) {
		this.pendingAmount = pendingAmount;
	}
	
	
	
	
}