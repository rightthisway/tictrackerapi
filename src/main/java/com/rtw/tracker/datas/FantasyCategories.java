package com.rtw.tracker.datas;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("FantasyCategories")
public class FantasyCategories {
	
	private Integer id;
	private String name;
	@JsonIgnore
	private String imageName;
	private String imageURL;
	private String packageInformation;
	private List<FantasyEventTeamTicket> fantasyTeamTickets;
	@JsonIgnore
	private Double packageCost;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public String getImageURL() {
		return imageURL;
	}
	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}
	public String getPackageInformation() {
		return packageInformation;
	}
	public void setPackageInformation(String packageInformation) {
		this.packageInformation = packageInformation;
	}
	public List<FantasyEventTeamTicket> getFantasyTeamTickets() {
		return fantasyTeamTickets;
	}
	public void setFantasyTeamTickets(
			List<FantasyEventTeamTicket> fantasyTeamTickets) {
		this.fantasyTeamTickets = fantasyTeamTickets;
	}
	public Double getPackageCost() {
		return packageCost;
	}
	public void setPackageCost(Double packageCost) {
		this.packageCost = packageCost;
	}
	public String getPkgInfoLine1() {
		return pkgInfoLine1;
	}
	public void setPkgInfoLine1(String pkgInfoLine1) {
		this.pkgInfoLine1 = pkgInfoLine1;
	}
	public String getPkgInfoLine2() {
		return pkgInfoLine2;
	}
	public void setPkgInfoLine2(String pkgInfoLine2) {
		this.pkgInfoLine2 = pkgInfoLine2;
	}
	public String getPkgInfoLine3() {
		return pkgInfoLine3;
	}
	public void setPkgInfoLine3(String pkgInfoLine3) {
		this.pkgInfoLine3 = pkgInfoLine3;
	}
	public String getPkgInfoLine4() {
		return pkgInfoLine4;
	}
	public void setPkgInfoLine4(String pkgInfoLine4) {
		this.pkgInfoLine4 = pkgInfoLine4;
	}
	public String getPkgInfoLine5() {
		return pkgInfoLine5;
	}
	public void setPkgInfoLine5(String pkgInfoLine5) {
		this.pkgInfoLine5 = pkgInfoLine5;
	}
	public String getPkgInfoLine6() {
		return pkgInfoLine6;
	}
	public void setPkgInfoLine6(String pkgInfoLine6) {
		this.pkgInfoLine6 = pkgInfoLine6;
	}
	@JsonIgnore
	private String pkgInfoLine1;
	@JsonIgnore
	private String pkgInfoLine2;
	@JsonIgnore
	private String pkgInfoLine3;
	@JsonIgnore
	private String pkgInfoLine4;
	@JsonIgnore
	private String pkgInfoLine5;
	@JsonIgnore
	private String pkgInfoLine6;
	
	

}
