package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;

@Entity
@Table(name="customer_ticket_downloads")
public class CustomerTicketDownloads implements Serializable{

	private Integer id;
	private Integer invoiceId;
	private Integer customerId;
	private String ticketType;
	private String ticketName;
	private String downloadPlatform;
	private Date downloadDateTime;
	private String ipAddress;
	
	private String email;
	
	@Transient
	private String downloadDateTimeStr;

	@Id
	@Column(name="id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="invoice_id")
	public Integer getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}
	
	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	@Column(name="ticket_type")
	public String getTicketType() {
		return ticketType;
	}

	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}

	@Column(name="ticket_name")
	public String getTicketName() {
		return ticketName;
	}

	public void setTicketName(String ticketName) {
		this.ticketName = ticketName;
	}

	@Column(name="donwload_platform")
	public String getDownloadPlatform() {
		return downloadPlatform;
	}

	public void setDownloadPlatform(String downloadPlatform) {
		this.downloadPlatform = downloadPlatform;
	}

	@Column(name="download_datetime")
	public Date getDownloadDateTime() {
		return downloadDateTime;
	}

	public void setDownloadDateTime(Date downloadDateTime) {
		this.downloadDateTime = downloadDateTime;
	}
	
	@Column(name="ip_address")
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Transient
	public String getDownloadDateTimeStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(downloadDateTime);
	}

	public void setDownloadDateTimeStr(String downloadDateTimeStr) {
		this.downloadDateTimeStr = downloadDateTimeStr;
	}

	@Transient
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}	
	
}
