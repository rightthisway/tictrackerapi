package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;

@Entity
@Table(name="referral_contest_winner")
public class ReferralContestWinner implements Serializable{

	private Integer id;
	private Integer contestId;
	private Integer participantId;
	private Date createdOn;
	private String createdBy;
	private Boolean isEmailed;
	private String city;
	
	private String winnerName;
	private String winnerEmail;
	private Integer winnerCustomerId;
	private String contestName;
	private Date startDate;
	private Date endDate;
	
	private String createdOnStr;
	private String startDateStr;
	private String endDateStr;
	
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="contest_id")
	public Integer getContestId() {
		return contestId;
	}
	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}
	
	@Column(name="participant_id")
	public Integer getParticipantId() {
		return participantId;
	}
	public void setParticipantId(Integer participantId) {
		this.participantId = participantId;
	}
	
	@Column(name="created_on")
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Column(name="created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	
	@Column(name="is_email_sent")
	public Boolean getIsEmailed() {
		return isEmailed;
	}
	public void setIsEmailed(Boolean isEmailed) {
		this.isEmailed = isEmailed;
	}
	
	
	@Column(name="city")
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	@Transient
	public String getWinnerName() {
		return winnerName;
	}
	public void setWinnerName(String winnerName) {
		this.winnerName = winnerName;
	}
	
	@Transient
	public String getWinnerEmail() {
		return winnerEmail;
	}
	public void setWinnerEmail(String winnerEmail) {
		this.winnerEmail = winnerEmail;
	}
	
	@Transient
	public Integer getWinnerCustomerId() {
		return winnerCustomerId;
	}
	public void setWinnerCustomerId(Integer winnerCustomerId) {
		this.winnerCustomerId = winnerCustomerId;
	}
	
	@Transient
	public String getCreatedOnStr() {
		this.createdOnStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(createdOn);
		return this.createdOnStr;
	}
	public void setCreatedOnStr(String createdOnStr) {
		this.createdOnStr = createdOnStr;
	}
	
	@Transient
	public String getContestName() {
		return contestName;
	}
	public void setContestName(String contestName) {
		this.contestName = contestName;
	}
	
	@Transient
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	@Transient
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	@Transient
	public String getStartDateStr() {
		this.startDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(startDate);
		return this.startDateStr;
	}
	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}
	
	@Transient
	public String getEndDateStr() {
		this.endDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(endDate);
		return this.endDateStr;
	}
	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}
	
	
}
