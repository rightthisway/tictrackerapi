package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.enums.FileType;

@Entity
@Table(name="invoice_ticket_attachment")
public class InvoiceTicketAttachment implements Serializable{

	private Integer id;
	private Integer invoiceId;
	private FileType type;
	private String filePath;
	private String extension;
	private Integer position;
	private String fileOriginalName;
	private Boolean isSent;
	private Boolean isEmailSent;
	private String fileName;
	private String fName;
	private String uploadedBy;
	private Date uploadedDateTime;	
	private String uploadedDateTimeStr;	
	
	@Id
	@GeneratedValue
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="invoice_id")
	public Integer getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}
	
	@Column(name="position")
	public Integer getPosition() {
		return position;
	}
	public void setPosition(Integer position) {
		this.position = position;
	}
	@Column(name="file_type")
	@Enumerated(EnumType.ORDINAL)
	public FileType getType() {
		return type;
	}
	
	public void setType(FileType type) {
		this.type = type;
	}
	
	
	@Column(name="file_path")
	public String getFilePath() {
		return filePath;
	}
	
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	@Column(name="file_extension")
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}

	@Column(name="uploaded_by")
	public String getUploadedBy() {
		return uploadedBy;
	}
	public void setUploadedBy(String uploadedBy) {
		this.uploadedBy = uploadedBy;
	}
	
	@Column(name="uploaded_datetime")
	public Date getUploadedDateTime() {
		return uploadedDateTime;
	}
	public void setUploadedDateTime(Date uploadedDateTime) {
		this.uploadedDateTime = uploadedDateTime;
	}
	
	
	@Column(name="file_name")
	public String getfName() {
		return fName;
	}
	public void setfName(String fName) {
		this.fName = fName;
	}
	
	@Column(name="file_original_name")
	public String getFileOriginalName() {
		return fileOriginalName;
	}
	public void setFileOriginalName(String fileOriginalName) {
		this.fileOriginalName = fileOriginalName;
	}
	
	@Column(name="is_sent")
	public Boolean getIsSent() {
		return isSent;
	}
	public void setIsSent(Boolean isSent) {
		this.isSent = isSent;
	}
	
	@Column(name="is_email_sent")
	public Boolean getIsEmailSent() {
		return isEmailSent;
	}
	public void setIsEmailSent(Boolean isEmailSent) {
		this.isEmailSent = isEmailSent;
	}
	@Transient
	public String getUploadedDateTimeStr() {
		if(uploadedDateTime != null){
			uploadedDateTimeStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(getUploadedDateTime());
		}
		return uploadedDateTimeStr;
	}
	public void setUploadedDateTimeStr(String uploadedDateTimeStr) {
		this.uploadedDateTimeStr = uploadedDateTimeStr;
	}
	
	@Transient
	public String getFileName() {
		this.fileName = this.fName;
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
