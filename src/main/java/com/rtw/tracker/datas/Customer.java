package com.rtw.tracker.datas;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.map.annotate.JsonDeserialize;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.rtw.tracker.datas.ApplicationPlatform;
import com.rtw.tracker.datas.CustomerLevel;
import com.rtw.tracker.datas.CustomerType;
import com.rtw.tracker.datas.ProductType;
import com.rtw.tracker.datas.SignupType;
import com.rtw.tracker.security.PasswordUtil;

//import com.rtw.tracker.security.PasswordUtil;


@Entity
@Table(name="customer")
public class Customer  implements Serializable{
		
	private Integer id;
	private String userName;
	private String customerName;
	private String lastName;
	private String email;
	private String secondaryEmail;
	private Boolean isRegistrationMailSent;
	private Boolean isMailSent;
	private String password;
	private String	phone;
	private String extension;
	private String	otherPhone;
	@JsonIgnore
	private Date signupDate;
	@JsonIgnore
	private Date joinDate;
	private String representativeName;
	private CustomerType customerType;
	private SignupType signupType; 
	private CustomerLevel customerLevel;
	private ProductType productType;
	private ApplicationPlatform	applicationPlatForm;
	private String	deviceId;
	private String	socialAccountId;
	private String companyName;
	private String notes;
	private String referrerCode;
	private Boolean isClient;
	private Boolean isBroker;
	private Integer brokerId;
	private Integer rtwCustomerId;
	private Integer rtw2CustomerId;
	private Integer tixcityCustomerId;
	private String userId;
	private Boolean isOptVerified;
	private Boolean isBlocked;
	private String signupDateStr;
	private String joinDateStr;
	
	/*@OneToMany
	@JoinColumn(name="customer_id", nullable=true)
	private CustomerAddress customerAddress;*/
	
	
	/**
	 * 
	 * @return id
	 */
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id ,Id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 
	 * @return userName
	 */
	@Column(name="username")
	public String getUserName() {
		if(null == userName || userName.isEmpty()){
			userName="";
		}
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
	@Column(name="is_mail_sent")
	public Boolean getIsMailSent() {
		return isMailSent;
	}
	public void setIsMailSent(Boolean isMailSent) {
		this.isMailSent = isMailSent;
	}
	
	@Column(name="is_emailed")
	public Boolean getIsRegistrationMailSent() {
		return isRegistrationMailSent;
	}
	public void setIsRegistrationMailSent(Boolean isRegistrationMailSent) {
		this.isRegistrationMailSent = isRegistrationMailSent;
	}
	/**
	 * 
	 * @return email
	 */
	@Column(name="email")
	public String getEmail() {
		if(null == email || email.isEmpty()){
			email="";
		}
		return email;
	}
	/**
	 * 
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * 
	 * @return password
	 */
	@Column(name="password")
	public String getPassword() {
		if(null == password || password.isEmpty()){
			password="";
		}
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Transient
	public void setEncryptPassword(String password) {
	    setPassword(PasswordUtil.generateEncrptedPassword(password));
	}
	
	/**
	 * 
	 * @return phone
	 */
	@Column(name="phone")
	public String getPhone() {
		if(null == phone ){
			phone = "";
		}
		return phone;
	}
	/**
	 * 
	 * @param phone , Phone no to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="product_type")
	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}
	
	@Column(name="cust_name")
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	@Column(name="ext")
	public String getExtension() {
		if(null == extension ){
			extension = "";
		}
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	
	@Column(name="other_phone")
	public String getOtherPhone() {
		if(null == otherPhone ){
			otherPhone = "";
		}
		return otherPhone;
	}
	public void setOtherPhone(String otherPhone) {
		this.otherPhone = otherPhone;
	}
	
	@Column(name="signup_date")
	public Date getSignupDate() {
		return signupDate;
	}
	public void setSignupDate(Date signupDate) {
		this.signupDate = signupDate;
	}
	
	@Column(name="representativeName")
	public String getRepresentativeName() {
		if(null == representativeName || representativeName.isEmpty()){
			representativeName="";
		}
		return representativeName;
	}
	public void setRepresentativeName(String representativeName) {
		this.representativeName = representativeName;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="customer_type")
	public CustomerType getCustomerType() {
		return customerType;
	}
	public void setCustomerType(CustomerType customerType) {
		this.customerType = customerType;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="signup_type")
	public SignupType getSignupType() {
		return signupType;
	}
	public void setSignupType(SignupType signupType) {
		this.signupType = signupType;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="application_platform")
	public ApplicationPlatform getApplicationPlatForm() {
		return applicationPlatForm;
	}
	public void setApplicationPlatForm(ApplicationPlatform applicationPlatForm) {
		this.applicationPlatForm = applicationPlatForm;
	}
	
	
	@Column(name="last_name")
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	
	@Column(name="device_id")
	public String getDeviceId() {
		if(null == deviceId || deviceId.isEmpty()){
			deviceId="";
		}
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	@Column(name="social_account_id")
	public String getSocialAccountId() {
		if(null == socialAccountId || socialAccountId.isEmpty()){
			socialAccountId="";
		}
		return socialAccountId;
	}
	public void setSocialAccountId(String socialAccountId) {
		this.socialAccountId = socialAccountId;
	}
	
	@Column(name="secondary_email")
	public String getSecondaryEmail() {
		if(null == secondaryEmail || secondaryEmail.isEmpty()){
			secondaryEmail="";
		}
		return secondaryEmail;
	}
	
	public void setSecondaryEmail(String secondaryEmail) {
		this.secondaryEmail = secondaryEmail;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="customer_level")
	public CustomerLevel getCustomerLevel() {
		return customerLevel;
	}
	
	public void setCustomerLevel(CustomerLevel customerLevel) {
		this.customerLevel = customerLevel;
	}
	/*public CustomerAddress getCustomerAddress() {
		return customerAddress;
	}
	
	public void setCustomerAddress(CustomerAddress customerAddress) {
		this.customerAddress = customerAddress;
	}*/
	@Column(name="company_name", nullable=true)
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	@Column(name="notes", nullable=true)
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	@Column(name = "referrer_code")
	public String getReferrerCode() {
		return referrerCode;
	}
	
	public void setReferrerCode(String referrerCode) {
		this.referrerCode = referrerCode;
	}
	
	@Column(name="is_client")
	public Boolean getIsClient() {
		return isClient;
	}
	public void setIsClient(Boolean isClient) {
		this.isClient = isClient;
	}
	
	@Column(name="is_broker")
	public Boolean getIsBroker() {
		return isBroker;
	}
	public void setIsBroker(Boolean isBroker) {
		this.isBroker = isBroker;
	}
	
	@Column(name="rtw_customer_id")
	public Integer getRtwCustomerId() {
		return rtwCustomerId;
	}
	public void setRtwCustomerId(Integer rtwCustomerId) {
		this.rtwCustomerId = rtwCustomerId;
	}
	
	@Column(name="rtw2_customer_id")
	public Integer getRtw2CustomerId() {
		return rtw2CustomerId;
	}
	public void setRtw2CustomerId(Integer rtw2CustomerId) {
		this.rtw2CustomerId = rtw2CustomerId;
	}
	
	@Column(name="tixcity_customer_id")
	public Integer getTixcityCustomerId() {
		return tixcityCustomerId;
	}
	public void setTixcityCustomerId(Integer tixcityCustomerId) {
		this.tixcityCustomerId = tixcityCustomerId;
	}
	
	@Column(name="broker_id")
	public Integer getBrokerId() {
		return brokerId;
	}
	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}
	
	@Column(name="user_id")
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	
	@Column(name="is_otp_verified")
	public Boolean getIsOptVerified() {
		return isOptVerified;
	}
	public void setIsOptVerified(Boolean isOptVerified) {
		this.isOptVerified = isOptVerified;
	}
	
	
	@Column(name="is_blocked")
	public Boolean getIsBlocked() {
		return isBlocked;
	}
	public void setIsBlocked(Boolean isBlocked) {
		this.isBlocked = isBlocked;
	}
	
	@Transient
	public String getSignupDateStr() {
		if(signupDate!=null){
			DateFormat formatDateTime = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
			signupDateStr = formatDateTime.format(signupDate);
		}
		return signupDateStr;
	}
	public void setSignupDateStr(String signupDateStr) {
		this.signupDateStr = signupDateStr;
	}
	
	@Transient
	public Date getJoinDate() {
		return joinDate;
	}
	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}
	
	@Transient
	public String getJoinDateStr() {
		if(joinDate!=null){
			DateFormat formatDateTime = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
			joinDateStr = formatDateTime.format(joinDate);
		}
		return joinDateStr;
	}
	public void setJoinDateStr(String joinDateStr) {
		this.joinDateStr = joinDateStr;
	}
	
	
	
	
	
}
