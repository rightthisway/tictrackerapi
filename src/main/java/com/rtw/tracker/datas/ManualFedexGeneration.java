package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;

@Entity
@Table(name="manual_fedex_generation")
public class ManualFedexGeneration implements Serializable{
	
	private Integer id;
	private String blFirstName;
	private String blLastName;
	private String blCompanyName;
	private String blAddress1;
	private String blAddress2;
	private String blCity;
	private Integer blState;
	private String blStateName;
	private Integer blCountry;
	private String blCountryName;
	private String blZipCode;
	private String blPhone;
	private String shCustomerName;
	private String shCompanyName;
	private String shAddress1;
	private String shAddress2;
	private String shCity;
	private Integer shState;
	private String shStateName;
	private Integer shCountry;
	private String shCountryName;
	private String shZipCode;
	private String shPhone;
	private String serviceType;
	private String signatureType;
	private String fedexLabelPath;
	private Boolean fedexLabelCreated;
	private String trackingNumber;
	private Date createdDate;
	private String createdBy;
	
	private String createdDateStr;
	
	public ManualFedexGeneration(){}
	
	
	@Id
	@GeneratedValue
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="bl_first_name")
	public String getBlFirstName() {
		return blFirstName;
	}
	public void setBlFirstName(String blFirstName) {
		this.blFirstName = blFirstName;
	}

	@Column(name="bl_last_name")
	public String getBlLastName() {
		return blLastName;
	}
	public void setBlLastName(String blLastName) {
		this.blLastName = blLastName;
	}
	
	@Column(name="bl_company_name")
	public String getBlCompanyName() {
		return blCompanyName;
	}
	public void setBlCompanyName(String blCompanyName) {
		this.blCompanyName = blCompanyName;
	}

	@Column(name="bl_address1")
	public String getBlAddress1() {
		return blAddress1;
	}
	public void setBlAddress1(String blAddress1) {
		this.blAddress1 = blAddress1;
	}

	@Column(name="bl_address2")
	public String getBlAddress2() {
		return blAddress2;
	}
	public void setBlAddress2(String blAddress2) {
		this.blAddress2 = blAddress2;
	}

	@Column(name="bl_city")
	public String getBlCity() {
		return blCity;
	}
	public void setBlCity(String blCity) {
		this.blCity = blCity;
	}

	@Column(name="bl_state")
	public Integer getBlState() {
		return blState;
	}
	public void setBlState(Integer blState) {
		this.blState = blState;
	}

	@Column(name="bl_state_name")
	public String getBlStateName() {
		return blStateName;
	}
	public void setBlStateName(String blStateName) {
		this.blStateName = blStateName;
	}

	@Column(name="bl_country")
	public Integer getBlCountry() {
		return blCountry;
	}
	public void setBlCountry(Integer blCountry) {
		this.blCountry = blCountry;
	}

	@Column(name="bl_country_name")
	public String getBlCountryName() {
		return blCountryName;
	}
	public void setBlCountryName(String blCountryName) {
		this.blCountryName = blCountryName;
	}

	@Column(name="bl_zipcode")
	public String getBlZipCode() {
		return blZipCode;
	}
	public void setBlZipCode(String blZipCode) {
		this.blZipCode = blZipCode;
	}

	@Column(name="bl_phone")
	public String getBlPhone() {
		return blPhone;
	}
	public void setBlPhone(String blPhone) {
		this.blPhone = blPhone;
	}

	@Column(name="sh_customer_name")
	public String getShCustomerName() {
		return shCustomerName;
	}
	public void setShCustomerName(String shCustomerName) {
		this.shCustomerName = shCustomerName;
	}

	@Column(name="sh_company_name")
	public String getShCompanyName() {
		return shCompanyName;
	}
	public void setShCompanyName(String shCompanyName) {
		this.shCompanyName = shCompanyName;
	}

	@Column(name="sh_address1")
	public String getShAddress1() {
		return shAddress1;
	}
	public void setShAddress1(String shAddress1) {
		this.shAddress1 = shAddress1;
	}

	@Column(name="sh_address2")
	public String getShAddress2() {
		return shAddress2;
	}
	public void setShAddress2(String shAddress2) {
		this.shAddress2 = shAddress2;
	}

	@Column(name="sh_city")
	public String getShCity() {
		return shCity;
	}
	public void setShCity(String shCity) {
		this.shCity = shCity;
	}

	@Column(name="sh_state")
	public Integer getShState() {
		return shState;
	}
	public void setShState(Integer shState) {
		this.shState = shState;
	}

	@Column(name="sh_state_name")
	public String getShStateName() {
		return shStateName;
	}
	public void setShStateName(String shStateName) {
		this.shStateName = shStateName;
	}

	@Column(name="sh_country")
	public Integer getShCountry() {
		return shCountry;
	}
	public void setShCountry(Integer shCountry) {
		this.shCountry = shCountry;
	}

	@Column(name="sh_country_name")
	public String getShCountryName() {
		return shCountryName;
	}
	public void setShCountryName(String shCountryName) {
		this.shCountryName = shCountryName;
	}

	@Column(name="sh_zipcode")
	public String getShZipCode() {
		return shZipCode;
	}
	public void setShZipCode(String shZipCode) {
		this.shZipCode = shZipCode;
	}

	@Column(name="sh_phone")
	public String getShPhone() {
		return shPhone;
	}
	public void setShPhone(String shPhone) {
		this.shPhone = shPhone;
	}

	@Column(name="service_type")
	public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	@Column(name="signature_type")
	public String getSignatureType() {
		return signatureType;
	}
	public void setSignatureType(String signatureType) {
		this.signatureType = signatureType;
	}

	@Column(name="fedex_label_path")
	public String getFedexLabelPath() {
		return fedexLabelPath;
	}
	public void setFedexLabelPath(String fedexLabelPath) {
		this.fedexLabelPath = fedexLabelPath;
	}

	@Column(name="fedex_label_created")
	public Boolean getFedexLabelCreated() {
		return fedexLabelCreated;
	}
	public void setFedexLabelCreated(Boolean fedexLabelCreated) {
		this.fedexLabelCreated = fedexLabelCreated;
	}

	@Column(name="tracking_no")
	public String getTrackingNumber() {
		return trackingNumber;
	}
	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}

	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name="created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Transient
	public String getCreatedDateStr() {
		return Util.formatDateToMonthDateYear(createdDate);
	}
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}


}
