package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;

@Entity
@Table(name="fanclub_members")
public class FanClubMember  implements Serializable{

	
	private Integer id;
	private Integer fanClubId;
	private Integer customerId;
	private Date joinedDate;
	private Date exitDate;
	private String status;
	
	private String joinedDateStr;
	private String exitDateStr;
	private String userId;
	private String email;
	private String phone;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="fanclub_mst_id")
	public Integer getFanClubId() {
		return fanClubId;
	}
	public void setFanClubId(Integer fanClubId) {
		this.fanClubId = fanClubId;
	}
	
	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Column(name="joined_date")
	public Date getJoinedDate() {
		return joinedDate;
	}
	public void setJoinedDate(Date joinedDate) {
		this.joinedDate = joinedDate;
	}
	
	@Column(name="exit_date")
	public Date getExitDate() {
		return exitDate;
	}
	public void setExitDate(Date exitDate) {
		this.exitDate = exitDate;
	}
	
	@Column(name="member_status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Transient
	public String getJoinedDateStr() {
		if(joinedDate!=null){
			this.joinedDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(joinedDate);
		}
		return joinedDateStr;
	}
	
	public void setJoinedDateStr(String joinedDateStr) {
		this.joinedDateStr = joinedDateStr;
	}
	
	@Transient
	public String getExitDateStr() {
		if(exitDate!=null){
			this.exitDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(exitDate);
		}
		return exitDateStr;
	}
	public void setExitDateStr(String exitDateStr) {
		this.exitDateStr = exitDateStr;
	}
	
	@Transient
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	@Transient
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Transient
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
}
