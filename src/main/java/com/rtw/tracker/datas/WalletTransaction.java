package com.rtw.tracker.datas;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("WalletTransaction")
public class WalletTransaction {
	private Integer status;
	private com.rtw.tmat.utils.Error error;
	private CustomerWallet wallet;
	private CustomerWalletHistory walletHistory;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public com.rtw.tmat.utils.Error getError() {
		return error;
	}
	public void setError(com.rtw.tmat.utils.Error error) {
		this.error = error;
	}
	public CustomerWallet getWallet() {
		return wallet;
	}
	public void setWallet(CustomerWallet wallet) {
		this.wallet = wallet;
	}
	public CustomerWalletHistory getWalletHistory() {
		return walletHistory;
	}
	public void setWalletHistory(CustomerWalletHistory walletHistory) {
		this.walletHistory = walletHistory;
	}
	
	
	
}
