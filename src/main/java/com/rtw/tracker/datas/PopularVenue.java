package com.rtw.tracker.datas;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.dao.implementation.DAORegistry;

@Entity
@Table(name="popular_venue")
public class PopularVenue implements Serializable{
	
	/**
	 * serialVersionUID is added only to remove warning
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private Integer venueId;
	private Venue venue;
	private Date createdDate;
	private String createdBy;
	private String status;
	private String createdDateStr;
	private Integer productId;
	private Product product;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="venue_id")
	public Integer getVenueId() {
		return venueId;
	}
	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Column(name="product_id")
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	@OneToOne
	@JoinColumn(name = "venue_id",insertable = false,updatable = false)
	public Venue getVenue() {
		return venue;
	}
	public void setVenue(Venue venue) {
		this.venue = venue;
	}	
	@Transient
	public Product getProduct() {
		if (productId == null) {
			return null;
		}
		if (product == null) {
			//DAOCall method - for fetching eventdetails by event id
			product = DAORegistry.getProductDAO().get(productId); 
		}
		return product;
	}	
	@Transient
	public void setProduct(Product product) {
		this.product = product;
	}
	/*@Transient
	public Venue getVenue() {
		if (venueId == null) {
			return null;
		}
		if (venue == null) {
			//DAOCall method - for fetching eventdetails by event id
			venue = DAORegistry.getVenueDAO().get(venueId); 
		}
		return venue;
	}
	@Transient
	public void setVenue(Venue venue) {
		this.venue = venue;
	}*/
	
	@Transient
	public String getCreatedDateStr() {
		if(getCreatedDate()==null){
			return "TBD";
		}
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(getCreatedDate());
	}
	
	@Transient
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	
}
