package com.rtw.tracker.datas;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.rtw.tracker.datas.Cards;

public class FileValidator implements Validator{

	@Override
	 public boolean supports(Class arg0) {
	  return false;
	 }

	 @Override
	 public void validate(Object uploadedFile, Errors errors) {

	  UploadedFile file = (UploadedFile) uploadedFile;

	  if (file.getFile().getSize() == 0) {
	   errors.rejectValue("file", "uploadForm.salectFile",
	     "Please select a file!");
	  }

	 }
	 
	 public void validateCards(Cards cards, Errors errors) {


	  if (cards.getFile()== null || cards.getFile().getSize() == 0) {
	   errors.rejectValue("file", "uploadForm.salectFile",
	     "Please select a file!");
	  }

	 }
	 
	 public void validateMobileImageCards(Cards cards, Errors errors) {


		  if (cards.getMobileFile()== null || cards.getMobileFile().getSize() == 0) {
		   errors.rejectValue("file", "uploadForm.selectFile",
		     "Please select a file!");
		  }

		 }
}
