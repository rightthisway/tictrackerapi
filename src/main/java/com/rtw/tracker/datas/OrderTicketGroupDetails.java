package com.rtw.tracker.datas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="order_ticket_group_details")
public class OrderTicketGroupDetails implements Serializable{

	private Integer id;
	private Integer orderId;
	private Integer ticketGroupId;
	private Integer quantity;
	private String zone;
	private String section;
	private String row;
	private String seat;
	private Double actualPrice;
	private Double soldPrice;
	
	
	@Id
	@GeneratedValue
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="order_id")
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	
	@Column(name="ticket_group_id")
	public Integer getTicketGroupId() {
		return ticketGroupId;
	}
	public void setTicketGroupId(Integer ticketGroupId) {
		this.ticketGroupId = ticketGroupId;
	}
	
	@Column(name="quantity")
	public Integer getQuantity() {
		return quantity;
	}
	
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	
	@Column(name="zone")
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	@Column(name="section")
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	
	
	@Column(name="row")
	public String getRow() {
		return row;
	}
	public void setRow(String row) {
		this.row = row;
	}
	
	@Column(name="seat")
	public String getSeat() {
		return seat;
	}
	public void setSeat(String seat) {
		this.seat = seat;
	}
	
	@Column(name="actual_price")
	public Double getActualPrice() {
		return actualPrice;
	}
	public void setActualPrice(Double actualPrice) {
		this.actualPrice = actualPrice;
	}
	
	@Column(name="sold_price")
	public Double getSoldPrice() {
		return soldPrice;
	}
	public void setSoldPrice(Double soldPrice) {
		this.soldPrice = soldPrice;
	}
	
	
	
	
}
