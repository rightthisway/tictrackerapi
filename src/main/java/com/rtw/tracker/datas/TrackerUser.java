package com.rtw.tracker.datas;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.codec.binary.Hex;

@Entity
@Table(name="tracker_users")
public class TrackerUser implements Serializable {

	private Integer id;
	private String userName;
	private String password;
	private String email;
	private String firstName;
	private String lastName;
	private String phone;
	private Set<Role> roles;
	private TrackerBrokers broker;
	private Boolean status;
	private String promotionalCode;
	private Date createDate;
	private String company;
	private String createdBy;
	
	
	private Integer sellerId;
	
	
	@Id
	@GeneratedValue
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="username")
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@Column(name="password")
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Column(name="email")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name="firstname")
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	@Column(name="lastname")
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@Column(name="phone")
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(name="tracker_user_roles",
        joinColumns=
            @JoinColumn(name="user_id", referencedColumnName="id"),
        inverseJoinColumns=
            @JoinColumn(name="role_id", referencedColumnName="id")
        )
	public Set<Role> getRoles() {
		return roles;
	}
		
	
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	
	public void setAndEncryptPassword(String password) {
	     MessageDigest algorithm;
		try {
			algorithm = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e) {
			
			throw new RuntimeException("SHA-1 algorithm not found!");
		}
	    byte [] digest = algorithm.digest(password.getBytes());
	    setPassword(new String(Hex.encodeHex(digest)));
	}
	
	@ManyToOne
	@JoinColumn(name="broker_id", nullable=true)
	public TrackerBrokers getBroker() {
		return broker;
	}
	public void setBroker(TrackerBrokers broker) {
		this.broker = broker;
	}
	
	@Column(name="status")
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Column(name="promotional_code")
	public String getPromotionalCode() {
		return promotionalCode;
	}
	public void setPromotionalCode(String promotionalCode) {
		this.promotionalCode = promotionalCode;
	}
	
	@Column(name="create_date")
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	@Column(name="created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Transient
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	
	@Transient
	public boolean hasRole(String roleName) {
		for (Role role:roles) {
			if (role.getName().equals(roleName)) {
				return true;
			}
		}
		return false;
	}
	
	@Transient
	public boolean hasRoles(String roleName, String roleName2) {
		for (Role role:roles) {
			if (role.getName().equals(roleName) || role.getName().equals(roleName2)) {
				return true;
			}
		}
		return false;
	}
	
	@Transient
	public Integer getSellerId() {
		return sellerId;
	}
	public void setSellerId(Integer sellerId) {
		this.sellerId = sellerId;
	}

	
	
	
}
