package com.rtw.tracker.datas;
import com.rtw.tmat.utils.Util;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import java.util.Date;

@Entity
@Table(name="contest_affiliate_settings")
public class ContestAffiliates implements Serializable{

	private Integer id;
	private Integer customerId;
	private String referralCode;
	private String status;
	private Date fromDate;
	private Date toDate;
	private String rewardType;
	private Double rewardValue;
	private Integer customerLives;
	private Integer affiliateLives;
	private Date createdDate;
	private String createdBy;
	private Date updatedDate;
	private String updatedBy;
	private Double activeCashReward;
	private Double debitedCashReward;
	private Double totalCashReward;
	private Double totalCreditedRewardDollar;
	private Double voidedCashReward;
	private Double customerRewardDollars;
	private Integer customerSuperFanStars;
	
	private String firstName;
	private String lastName;
	private String customerType;
	private String email;
	private String userId;
	private String phone;
	private String fromDateStr;
	private String toDateStr;
	private String createdDateStr;
	private String updatedDateStr;
	private String startDateStr;
	private String endDateStr;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Column(name="referral_code")
	public String getReferralCode() {
		return referralCode;
	}
	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="effective_from_date")
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	
	@Column(name="effective_to_date")
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	
	@Column(name="reward_type")
	public String getRewardType() {
		return rewardType;
	}
	public void setRewardType(String rewardType) {
		this.rewardType = rewardType;
	}
	
	@Column(name="reward_value")
	public Double getRewardValue() {
		return rewardValue;
	}
	public void setRewardValue(Double rewardValue) {
		this.rewardValue = rewardValue;
	}
	
	@Column(name="no_of_lives_to_tireone_customer")
	public Integer getCustomerLives() {
		return customerLives;
	}
	public void setCustomerLives(Integer customerLives) {
		this.customerLives = customerLives;
	}
	
	@Column(name="no_of_lives_to_affiliate_customer")
	public Integer getAffiliateLives() {
		return affiliateLives;
	}
	public void setAffiliateLives(Integer affiliateLives) {
		this.affiliateLives = affiliateLives;
	}
	
	@Column(name="create_Date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column(name="updated_date")
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@Column(name="updated_by")
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column(name="active_cash_reward")
	public Double getActiveCashReward() {
		return activeCashReward;
	}
	public void setActiveCashReward(Double activeCashReward) {
		this.activeCashReward = activeCashReward;
	}
	
	@Column(name="debited_cash_reward")
	public Double getDebitedCashReward() {
		return debitedCashReward;
	}
	public void setDebitedCashReward(Double debitedCashReward) {
		this.debitedCashReward = debitedCashReward;
	}
	
	@Column(name="total_cash_reward")
	public Double getTotalCashReward() {
		return totalCashReward;
	}
	public void setTotalCashReward(Double totalCashReward) {
		this.totalCashReward = totalCashReward;
	}
	
	@Column(name="total_credited_reward_dollars")
	public Double getTotalCreditedRewardDollar() {
		return totalCreditedRewardDollar;
	}
	public void setTotalCreditedRewardDollar(Double totalCreditedRewardDollar) {
		this.totalCreditedRewardDollar = totalCreditedRewardDollar;
	}
	
	@Column(name="voided_cash_reward")
	public Double getVoidedCashReward() {
		return voidedCashReward;
	}
	public void setVoidedCashReward(Double voidedCashReward) {
		this.voidedCashReward = voidedCashReward;
	}
	
	@Column(name="tireone_cust_reward_dollars")
	public Double getCustomerRewardDollars() {
		return customerRewardDollars;
	}
	public void setCustomerRewardDollars(Double customerRewardDollars) {
		this.customerRewardDollars = customerRewardDollars;
	}
	
	@Column(name="tireone_cust_super_fan_stars")
	public Integer getCustomerSuperFanStars() {
		return customerSuperFanStars;
	}
	public void setCustomerSuperFanStars(Integer customerSuperFanStars) {
		this.customerSuperFanStars = customerSuperFanStars;
	}
	@Transient
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	@Transient
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	@Transient
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	@Transient
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Transient
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	@Transient
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	@Transient
	public String getFromDateStr() {
		if(fromDate!=null){
			fromDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(fromDate);
		}
		return fromDateStr;
	}
	public void setFromDateStr(String fromDateStr) {
		this.fromDateStr = fromDateStr;
	}
	@Transient
	public String getToDateStr() {
		if(toDate!=null){
			toDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(toDate);
		}
		return toDateStr;
	}
	public void setToDateStr(String toDateStr) {
		this.toDateStr = toDateStr;
	}
	@Transient
	public String getCreatedDateStr() {
		if(createdDate!=null){
			createdDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(createdDate);
		}
		return createdDateStr;
	}
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	@Transient
	public String getUpdatedDateStr() {
		if(updatedDate!=null){
			updatedDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(updatedDate);
		}
		return updatedDateStr;
	}
	public void setUpdatedDateStr(String updatedDateStr) {
		this.updatedDateStr = updatedDateStr;
	}
	
	@Transient
	public String getStartDateStr() {
		if(fromDate!=null){
			this.startDateStr = Util.formatDateToMonthDateYear(fromDate);
		}
		return startDateStr;
	}
	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}
	
	@Transient
	public String getEndDateStr() {
		if(toDate!=null){
			this.endDateStr = Util.formatDateToMonthDateYear(toDate);
		}
		return endDateStr;
	}
	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}
	
	
	
}
