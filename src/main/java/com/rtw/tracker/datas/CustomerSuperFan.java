package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;

 
/**
 * class to represent CustomerSuperFan.
 * @author Ulaganathan
 *
 */
@Entity
@Table(name="customer_super_fan")
public class CustomerSuperFan implements Serializable{
	
	private Integer id;
	private Integer customerId;
	private Integer artistId;
	private String artistName;
	private String categoryName;
	private Date startDate;
	private Date endDate;
	private String status;
	private Boolean ticketPurchased;
	
	private String startDateStr;
	private String endDateStr;
	
	/**
	 * @return the id
	 */
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Column(name="artist_id")
	public Integer getArtistId() {
		return artistId;
	}
	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}
	
	@Column(name="artist_name")
	public String getArtistName() {
		return artistName;
	}
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
	
	@Column(name="parent_category_name")
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	
    @Column(name="start_date")
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
//	@Column(name="end_date")
	 @Column(name="end_date",insertable=false, updatable=false)
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="Ticket_purchased")
	public Boolean getTicketPurchased() {
		return ticketPurchased;
	}
	public void setTicketPurchased(Boolean ticketPurchased) {
		this.ticketPurchased = ticketPurchased;
	}
	
	@Transient
	public String getStartDateStr() {
		if(startDate != null){
			startDateStr = Util.formatDateTimeToMonthDateYearAndHourMinute(startDate);
		}
		return startDateStr;
	}
	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}
	
	@Transient
	public String getEndDateStr() {
		if(endDate != null){
			endDateStr = Util.formatDateTimeToMonthDateYearAndHourMinute(endDate);
		}
		return endDateStr;
	}
	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}
	
}
	
