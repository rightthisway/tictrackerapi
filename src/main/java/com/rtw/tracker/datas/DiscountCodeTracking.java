package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name = "track_discount_code_sharing")
public class DiscountCodeTracking implements Serializable{

	private Integer id;
	private Integer customerId;
	private String sharingType;
	private String sharingOption;
	private String sharedUrl;
	private ApplicationPlatform platform;
	private String sessionId;
	private Date createdDate;
	private Date lastUpdated;
	
	private String firstName;
	private String lastName;
	private String email;
	private String referrerCode;
	private Integer faceBookCnt;
	private Integer twitterCnt;
	private Integer linkedinCnt;
	private Integer whatsappCnt;
	private Integer androidCnt;
	private Integer imessageCnt;
	private Integer googleCnt;
	private Integer outlookCnt;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name = "platform")
	public ApplicationPlatform getPlatform() {
		return platform;
	}
	public void setPlatform(ApplicationPlatform platform) {
		this.platform = platform;
	}
	
	@Column(name = "last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Column(name = "session_id")
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	@Column(name = "sharing_type")
	public String getSharingType() {
		return sharingType;
	}
	public void setSharingType(String sharingType) {
		this.sharingType = sharingType;
	}
	
	@Column(name = "sharing_option")
	public String getSharingOption() {
		return sharingOption;
	}
	public void setSharingOption(String sharingOption) {
		this.sharingOption = sharingOption;
	}
	
	@Column(name = "shared_url")
	public String getSharedUrl() {
		return sharedUrl;
	}
	public void setSharedUrl(String sharedUrl) {
		this.sharedUrl = sharedUrl;
	}
	
	@Column(name = "created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Transient
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	@Transient
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@Transient
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Transient
	public String getReferrerCode() {
		return referrerCode;
	}
	public void setReferrerCode(String referrerCode) {
		this.referrerCode = referrerCode;
	}
	
	@Transient
	public Integer getFaceBookCnt() {
		return faceBookCnt;
	}
	public void setFaceBookCnt(Integer faceBookCnt) {
		this.faceBookCnt = faceBookCnt;
	}
	
	@Transient
	public Integer getTwitterCnt() {
		return twitterCnt;
	}
	public void setTwitterCnt(Integer twitterCnt) {
		this.twitterCnt = twitterCnt;
	}
	
	@Transient
	public Integer getLinkedinCnt() {
		return linkedinCnt;
	}
	public void setLinkedinCnt(Integer linkedinCnt) {
		this.linkedinCnt = linkedinCnt;
	}
	
	@Transient
	public Integer getWhatsappCnt() {
		return whatsappCnt;
	}
	public void setWhatsappCnt(Integer whatsappCnt) {
		this.whatsappCnt = whatsappCnt;
	}
	
	@Transient
	public Integer getAndroidCnt() {
		return androidCnt;
	}
	public void setAndroidCnt(Integer androidCnt) {
		this.androidCnt = androidCnt;
	}
	
	@Transient
	public Integer getImessageCnt() {
		return imessageCnt;
	}
	public void setImessageCnt(Integer imessageCnt) {
		this.imessageCnt = imessageCnt;
	}
	
	@Transient
	public Integer getGoogleCnt() {
		return googleCnt;
	}
	public void setGoogleCnt(Integer googleCnt) {
		this.googleCnt = googleCnt;
	}
	
	@Transient
	public Integer getOutlookCnt() {
		return outlookCnt;
	}
	public void setOutlookCnt(Integer outlookCnt) {
		this.outlookCnt = outlookCnt;
	}
		
	
}
