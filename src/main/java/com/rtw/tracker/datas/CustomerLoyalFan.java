package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;

 

@Entity
@Table(name="customer_loyal_fan")
public class CustomerLoyalFan implements Serializable{
	
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column(name="customer_id")
	private Integer customerId;
	
	@Column(name="loyal_fan_type")
	private String loyalFanType;
		
	@Column(name="artist_or_team_id")	
	private Integer artistId;
	
	@Column(name="loyal_name")
	private String loyalName;
	
	@Column(name="zip_code")
	private String zipCode;
	
	@Column(name="state")
	private String state;
	
	@Column(name="country")
	private String country;
	
	@Column(name="status")
	private String status;
	
	@Column(name="Ticket_purchased")
	private Boolean ticketPurchased;
	
	@Column(name="start_date")
	private Date startDate;
	
	@Column(name="end_date",insertable=false, updatable=false)
	private Date endDate;
	
	@Column(name="created_date")
	private Date createdDate;
	
	@Column(name="updated_date")
	private Date updatedDate;
	
	@Transient
	private String startDateStr;
	@Transient
	private String endDateStr;
	/*@Transient
	private String createdDateStr;
	@Transient
	private String updatedDateStr;*/
	
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
		
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
		
	public String getLoyalFanType() {
		return loyalFanType;
	}

	public void setLoyalFanType(String loyalFanType) {
		this.loyalFanType = loyalFanType;
	}

	public Integer getArtistId() {
		return artistId;
	}

	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}

	public String getLoyalName() {
		return loyalName;
	}

	public void setLoyalName(String loyalName) {
		this.loyalName = loyalName;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
		
	public Boolean getTicketPurchased() {
		return ticketPurchased;
	}
	public void setTicketPurchased(Boolean ticketPurchased) {
		this.ticketPurchased = ticketPurchased;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@Transient
	public String getStartDateStr() {
		if(startDate != null){
			startDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(startDate);
		}
		return startDateStr;
	}
	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}
	
	@Transient
	public String getEndDateStr() {
		if(endDate != null){
			endDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(endDate);
		}
		return endDateStr;
	}
	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}

	/*@Transient
	public String getCreatedDateStr() {
		if(createdDate != null){
			createdDateStr = Util.formatDateTimeToMonthDateYearAndHourMinute(createdDate);
		}
		return createdDateStr;
	}

	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}

	@Transient
	public String getUpdatedDateStr() {
		if(updatedDate != null){
			updatedDateStr = Util.formatDateTimeToMonthDateYearAndHourMinute(updatedDate);
		}
		return updatedDateStr;
	}

	public void setUpdatedDateStr(String updatedDateStr) {
		this.updatedDateStr = updatedDateStr;
	}*/
	
}
	
