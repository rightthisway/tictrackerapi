package com.rtw.tracker.datas;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("CrownJewelTicket")
public class CrownJewelTicket {
	
	private Integer ticketId;
	private Integer quantity;
	private String requiredPoints;
	private Double requiredPointsAsDouble;
	private String redeemAlertText;
	private String redeemAlertTextDesktop;
	private Boolean isReedemable;
	private Boolean isRealTicket;
	
	public CrownJewelTicket(){
		
	}
	/*public CrownJewelTicket(CrownJewelCategoryTicket ticket){
		setQuantity(ticket.getQuantity());
		setTicketId(ticket.getId());
		setIsRealTicket(ticket.getIsRealTicket());
	}*/

	public Integer getTicketId() {
		return ticketId;
	}

	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	public String getRedeemAlertText() {
		return redeemAlertText;
	}
	public void setRedeemAlertText(String redeemAlertText) {
		this.redeemAlertText = redeemAlertText;
	}
	public String getRedeemAlertTextDesktop() {
		return redeemAlertTextDesktop;
	}
	public void setRedeemAlertTextDesktop(String redeemAlertTextDesktop) {
		this.redeemAlertTextDesktop = redeemAlertTextDesktop;
	}
	public Boolean getIsReedemable() {
		return isReedemable;
	}
	public void setIsReedemable(Boolean isReedemable) {
		this.isReedemable = isReedemable;
	}
	public String getRequiredPoints() {
		return requiredPoints;
	}
	public void setRequiredPoints(String requiredPoints) {
		this.requiredPoints = requiredPoints;
	}
	public Double getRequiredPointsAsDouble() {
		return requiredPointsAsDouble;
	}
	public void setRequiredPointsAsDouble(Double requiredPointsAsDouble) {
		this.requiredPointsAsDouble = requiredPointsAsDouble;
	}
	public Boolean getIsRealTicket() {
		if(null == isRealTicket){
			isRealTicket = true;
		}
		return isRealTicket;
	}
	public void setIsRealTicket(Boolean isRealTicket) {
		this.isRealTicket = isRealTicket;
	}
	
	
}
