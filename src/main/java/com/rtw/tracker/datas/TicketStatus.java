package com.rtw.tracker.datas;

public enum TicketStatus {
	DISABLED, ACTIVE, EXPIRED, SOLD ,LOCKED, ONHOLD
}
