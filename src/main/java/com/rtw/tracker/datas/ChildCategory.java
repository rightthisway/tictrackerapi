package com.rtw.tracker.datas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

 
/**
 * class to represent Child Category of Tour.
 * @author hamin
 *
 */
@Entity
@Table(name="child_category")
public class ChildCategory implements Serializable{
	
	private String name;
	private Integer id;
	private Boolean isDisplay;
	private Integer tnId;
	//private ParentCategory parentCategory;
	
	/**
	 * @return the id
	 */
	
	@Id
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * @return the name
	 */
	@Column(name="name")
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
	@Column(name="display_on_search")
	public Boolean getIsDisplay() {
		return isDisplay;
	}

	public void setIsDisplay(Boolean isDisplay) {
		this.isDisplay = isDisplay;
	}
	
	
	@Column(name="tn_child_category_id")
	public Integer getTnId() {
		return tnId;
	}
	public void setTnId(Integer tnId) {
		this.tnId = tnId;
	}
	/**
	 * @return the parentCategory
	 */
	/*@OneToOne
	@JoinColumn(name="category_id")
	public ParentCategory getParentCategory() {
		return parentCategory;
	}
	*//**
	 * @param parentCategory the parentCategory to set
	 *//*
	public void setParentCategory(ParentCategory parentCategory) {
		this.parentCategory = parentCategory;
	}*/
	
	@Override
	public String toString() {		 
		return  "[ id:"+id+" name: "+name+"]";
	}
	
	@Override
	public boolean equals(Object obj) {
		ChildCategory temp = (ChildCategory) obj;
		return this.id.equals(temp.id);
	}
}
