package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.annotate.JsonDeserialize;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.rtw.tmat.utils.Util;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.enums.ZonesQuantityTypes;

@Entity
@Table(name="fantasy_event_team_zones")
public class CrownJewelTeamZones implements Serializable{
	
	/**
	 * serialVersionUID is added only to remove warning
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private Integer teamId;
	private Integer leagueId;
	private Integer eventId;
	private CrownJewelTeams team;
	private String zone;
	private Integer ticketsCount=0;
	private Integer soldTicketsCount=0;
	private String lastUpdateddBy;
	@JsonDeserialize(using = com.rtw.tracker.utils.CustomerDateAndTimeDeserialize.class)
	@JsonSerialize(using = com.rtw.tracker.utils.CustomerDateAndTimeSerializer.class)
	private Date lastUpdated;
	private String status;
	private String lastUpdatedStr;
	private EventDetails event;
	private Event dummyEvent;
	private Integer availableTixCount=0;
	private Integer pointsRedeemed=0;
	private Double price;
	private String eventString;
	private String venueString;
	private Boolean isAutoPrice;
	private ZonesQuantityTypes quantityType; 
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="team_id")
	public Integer getTeamId() {
		return teamId;
	}
	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	
	@Column(name="fantasy_event_id")
	public Integer getLeagueId() {
		return leagueId;
	}
	public void setLeagueId(Integer leagueId) {
		this.leagueId = leagueId;
	}
	@Column(name="zone")
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	@Transient
	public Integer getSoldTicketsCount() {
		return soldTicketsCount;
	}
	public void setSoldTicketsCount(Integer soldTicketsCount) {
		this.soldTicketsCount = soldTicketsCount;
	}
	@Column(name="last_updated_by")
	public String getLastUpdateddBy() {
		return lastUpdateddBy;
	}
	public void setLastUpdateddBy(String lastUpdateddBy) {
		this.lastUpdateddBy = lastUpdateddBy;
	}
	
	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Column(name="tickets_count")
	public Integer getTicketsCount() {
		return ticketsCount;
	}
	public void setTicketsCount(Integer ticketsCount) {
		this.ticketsCount = ticketsCount;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="price")
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	
	
	@Column(name="quantity_type")
	@Enumerated(EnumType.STRING)
	public ZonesQuantityTypes getQuantityType() {
		return quantityType;
	}
	public void setQuantityType(ZonesQuantityTypes quantityType) {
		this.quantityType = quantityType;
	}
	@Transient
	public String getLastUpdatedStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(getLastUpdated());
	}
	
	@Transient
	public void setLastUpdatedStr(String lastUpdatedStr) {
		this.lastUpdatedStr = lastUpdatedStr;
	}
	@Transient
	public EventDetails getEvent() {
		if (eventId == null) {
			return null;
		}
		if (event == null) {
			event = DAORegistry.getEventDetailsDAO().get(eventId); 
		}
		return event;
	}
	@Transient
	public void setEvent(EventDetails event) {
		this.event = event;
	}
	@Transient
	public CrownJewelTeams getTeam() {
		if (teamId == null) {
			return null;
		}
		if (team == null) {
			team = DAORegistry.getCrownJewelTeamsDAO().get(teamId); 
		}
		return team;
	}
	@Transient
	public void setTeam(CrownJewelTeams team) {
		this.team = team;
	}
	@Transient
	public Integer getAvailableTixCount() {
		return availableTixCount;
	}
	
	public void setAvailableTixCount(Integer availableTixCount) {
		this.availableTixCount = availableTixCount;
	}
	@Transient
	public Integer getPointsRedeemed() {
		return pointsRedeemed;
	}
	
	public void setPointsRedeemed(Integer pointsRedeemed) {
		this.pointsRedeemed = pointsRedeemed;
	}
	
	@Transient
	public String getEventString() {
		if(getDummyEvent()!=null){
			eventString = getDummyEvent().getEventName()+" "+getDummyEvent().getEventDateStr()+" "+getDummyEvent().getEventTimeStr();
		}
		return eventString;
	}
	public void setEventString(String eventString) {
		this.eventString = eventString;
	}
	
	@Transient
	public String getVenueString() {
		if(StringUtils.isNotEmpty(venueString)){
			return venueString;
		}
		if(getDummyEvent()!=null){
			Venue venu = DAORegistry.getVenueDAO().get(getDummyEvent().getVenueId());
			venueString = venu.getLocation();
		}
		return venueString;
	}
	public void setVenueString(String venueString) {
		this.venueString = venueString;
	}
	@Transient
	public Event getDummyEvent() {
		if(dummyEvent == null){
			dummyEvent = DAORegistry.getEventDAO().get(eventId);
		}
		return dummyEvent;
	}
	public void setDummyEvent(Event dummyEvent) {
		this.dummyEvent = dummyEvent;
	}
	
	@Transient
	public Boolean getIsAutoPrice() {
		return isAutoPrice;
	}
	public void setIsAutoPrice(Boolean isAutoPrice) {
		this.isAutoPrice = isAutoPrice;
	}
	
	
	
	
	
}
