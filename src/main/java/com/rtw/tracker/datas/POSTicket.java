package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.rtw.tmat.utils.Util;

@Entity
@Table(name="pos_ticket")
public class POSTicket implements Serializable{
	
	private Integer id;
	private String productType;
	private Integer productId;
	private Integer ticketId;
	private String section;
	private String row;
	private String seatNumber;
	private Double retailPrice;
	private Double facePrice;
	private Double cost;
	private Integer seatOrder;
	private Double wholesalePrice;
	private Double actualSoldPrice;
	private String generalDesc;
	private Boolean statusId;
	private Date createDate;
	private Date expectedArrivalDate;
	private Integer ticketGroupId;
	private Integer systemUserId;
	private Integer ticketOnHandStatusId;
	private Integer invoiceId;
	private Integer purchaseOrderId;
	private Integer modSystemUserId;
	private String originalSection;
	private Double wholesalePriceAtTimeOfSale;
		
	
	public POSTicket(){
		
	}
	
	@Column(name = "product_type")
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	
	@Column(name = "product_id")
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	
	
	@Id
	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "ticket_id")
	public Integer getTicketId() {
		return ticketId;
	}
	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}
	
	@Column(name = "section")
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	
	@Column(name = "row")
	public String getRow() {
		return row;
	}
	public void setRow(String row) {
		this.row = row;
	}
	
	@Column(name = "seat_number")
	public String getSeatNumber() {
		return seatNumber;
	}
	public void setSeatNumber(String seatNumber) {
		this.seatNumber = seatNumber;
	}
	
	@Column(name = "retail_price")
	public Double getRetailPrice() {
		try{
			if(retailPrice != null){
				return Util.getRoundedValue(retailPrice);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return retailPrice;
	}
	public void setRetailPrice(Double retailPrice) {
		this.retailPrice = retailPrice;
	}
	
	@Column(name = "face_price")
	public Double getFacePrice() {
		try{
			if(facePrice != null){
				return Util.getRoundedValue(facePrice);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return facePrice;
	}
	public void setFacePrice(Double facePrice) {
		this.facePrice = facePrice;
	}
	
	@Column(name = "cost")
	public Double getCost() {
		try{
			if(cost != null){
				return Util.getRoundedValue(cost);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	
	@Column(name = "seat_order")
	public Integer getSeatOrder() {
		return seatOrder;
	}
	public void setSeatOrder(Integer seatOrder) {
		this.seatOrder = seatOrder;
	}
	
	@Column(name = "wholesale_price")
	public Double getWholesalePrice() {
		try{
			if(wholesalePrice != null){
				return Util.getRoundedValue(wholesalePrice);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return wholesalePrice;
	}
	public void setWholesalePrice(Double wholesalePrice) {
		this.wholesalePrice = wholesalePrice;
	}
	
	@Column(name = "actual_sold_price")
	public Double getActualSoldPrice() {
		try{
			if(actualSoldPrice != null){
				return Util.getRoundedValue(actualSoldPrice);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return actualSoldPrice;
	}
	public void setActualSoldPrice(Double actualSoldPrice) {
		this.actualSoldPrice = actualSoldPrice;
	}
	
	@Column(name = "general_desc")
	public String getGeneralDesc() {
		return generalDesc;
	}
	public void setGeneralDesc(String generalDesc) {
		this.generalDesc = generalDesc;
	}
	
	@Column(name = "status_id")
	public Boolean getStatusId() {
		return statusId;
	}
	public void setStatusId(Boolean statusId) {
		this.statusId = statusId;
	}
	
	@Column(name = "create_date")
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	@Column(name = "expected_arrival_date")
	public Date getExpectedArrivalDate() {
		return expectedArrivalDate;
	}
	public void setExpectedArrivalDate(Date expectedArrivalDate) {
		this.expectedArrivalDate = expectedArrivalDate;
	}
	
	@Column(name = "ticket_group_id")
	public Integer getTicketGroupId() {
		return ticketGroupId;
	}
	public void setTicketGroupId(Integer ticketGroupId) {
		this.ticketGroupId = ticketGroupId;
	}
	
	@Column(name = "system_user_id")
	public Integer getSystemUserId() {
		return systemUserId;
	}
	public void setSystemUserId(Integer systemUserId) {
		this.systemUserId = systemUserId;
	}
	
	@Column(name = "ticket_on_hand_status_id")
	public Integer getTicketOnHandStatusId() {
		return ticketOnHandStatusId;
	}
	public void setTicketOnHandStatusId(Integer ticketOnHandStatusId) {
		this.ticketOnHandStatusId = ticketOnHandStatusId;
	}
	
	@Column(name = "invoice_id")
	public Integer getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}
	
	@Column(name = "purchase_order_id")
	public Integer getPurchaseOrderId() {
		return purchaseOrderId;
	}
	public void setPurchaseOrderId(Integer purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}
	
	@Column(name = "mod_system_user_id")
	public Integer getModSystemUserId() {
		return modSystemUserId;
	}
	public void setModSystemUserId(Integer modSystemUserId) {
		this.modSystemUserId = modSystemUserId;
	}
	
	@Column(name = "original_section")
	public String getOriginalSection() {
		return originalSection;
	}
	public void setOriginalSection(String originalSection) {
		this.originalSection = originalSection;
	}
	
	@Column(name = "wholesale_price_at_time_of_sale")
	public Double getWholesalePriceAtTimeOfSale() {
		try{
			if(wholesalePriceAtTimeOfSale != null){
				return Util.getRoundedValue(wholesalePriceAtTimeOfSale);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return wholesalePriceAtTimeOfSale;
	}
	public void setWholesalePriceAtTimeOfSale(Double wholesalePriceAtTimeOfSale) {
		this.wholesalePriceAtTimeOfSale = wholesalePriceAtTimeOfSale;
	}

}
