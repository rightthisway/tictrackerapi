package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;

@Entity
@Table(name = "pos_customer_order")
public class POSCustomerOrder implements Serializable{

	private String productType;
	private Integer productId;
	private Integer ticketRequestId;
	private Integer ticketGroupId;
	private Integer ticketQty;
	private Double retailPrice;
	private String section;
	private String row;
	private String ticketLowSeat;
	private String ticketHighSeat;
	private Integer invoiceId;
	private Integer eventId;
	private String eventName;
	private Date eventDateTime;
	private Integer venueId;
	private String venueName;
	
	private String eventDateTimeStr;
	
	public POSCustomerOrder(){
		
	}

	@Column(name = "product_type")
	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	@Column(name = "product_id")
	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	@Id
	@Column(name = "ticket_request_id")
	public Integer getTicketRequestId() {
		return ticketRequestId;
	}

	public void setTicketRequestId(Integer ticketRequestId) {
		this.ticketRequestId = ticketRequestId;
	}

	@Column(name = "ticket_group_id")
	public Integer getTicketGroupId() {
		return ticketGroupId;
	}

	public void setTicketGroupId(Integer ticketGroupId) {
		this.ticketGroupId = ticketGroupId;
	}

	@Column(name = "ticket_qty")
	public Integer getTicketQty() {
		return ticketQty;
	}

	public void setTicketQty(Integer ticketQty) {
		this.ticketQty = ticketQty;
	}

	@Column(name = "retail_price")
	public Double getRetailPrice() {
		return retailPrice;
	}

	public void setRetailPrice(Double retailPrice) {
		this.retailPrice = retailPrice;
	}

	@Column(name = "section")
	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	@Column(name = "row")
	public String getRow() {
		return row;
	}

	public void setRow(String row) {
		this.row = row;
	}

	@Column(name = "ticket_lowseat")
	public String getTicketLowSeat() {
		return ticketLowSeat;
	}

	public void setTicketLowSeat(String ticketLowSeat) {
		this.ticketLowSeat = ticketLowSeat;
	}

	@Column(name = "ticket_highseat")
	public String getTicketHighSeat() {
		return ticketHighSeat;
	}

	public void setTicketHighSeat(String ticketHighSeat) {
		this.ticketHighSeat = ticketHighSeat;
	}

	@Column(name = "invoice_id")
	public Integer getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}

	@Column(name = "event_id")
	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	@Column(name = "event_name")
	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	@Column(name = "event_datetime")
	public Date getEventDateTime() {
		return eventDateTime;
	}

	public void setEventDateTime(Date eventDateTime) {
		this.eventDateTime = eventDateTime;
	}
	
	@Column(name = "venue_id")
	public Integer getVenueId() {
		return venueId;
	}

	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}

	@Column(name = "venue_name")
	public String getVenueName() {
		return venueName;
	}

	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}

	@Transient
	public String getEventDateTimeStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(getEventDateTime());
	}

	public void setEventDateTimeStr(String eventDateTimeStr) {
		this.eventDateTimeStr = eventDateTimeStr;
	}
	
	
}
