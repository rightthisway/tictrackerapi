package com.rtw.tracker.datas;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;

@Entity
@Table(name="pos_purchase_order")
public class POSPurchaseOrder implements Serializable{
	
	private String productType;
	private Integer productId;
	private Integer purchaseOrderId;
	private String externalPONumber;
	
	private Double totalShipping;
	private Double totalExpenses;
	private Double totalDue;
	private Double totalTaxes;
	private Double poTotal;
	private Double balanceDue;
	private String notes;
	private Integer paymentTermsId;
	private Integer shippingTrackingId;
	private Integer clientBrokerId;
	private Integer systemUserId;
	private Integer clientBrokerEmployeeId;
	private Integer buyerBrokerId;
	private String createDate;
	private Integer dropShipped;
	private String displayedNotes;
	private Integer ticketRequestId;
	private Integer reprint;
	private Boolean isEmailed;
	private String emailInfo;
	private Integer currencyId;
	private Boolean dataCached;
	private Integer parentPOId;
	private String onHandNotes;
	private Integer tranOfficeId;
	private Integer userOfficeId;
	private Integer mercuryTransactionId;

	private String tmOrderId;
	private String systemUserName;
	
	//private Date listerOnHandDate;
	private Date billDueDate;
	private Date createdDate;
	
	private Integer totalQuantity;
	private Integer usedQunatity;
	private String customerName;
	private String createTimeStr;
		
	public POSPurchaseOrder(){
		
	}
	
	@Column(name = "product_type")
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	
	@Column(name = "product_id")
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	
	@Id
	@Column(name = "purchase_order_id")
	public Integer getPurchaseOrderId() {
		return purchaseOrderId;
	}
	public void setPurchaseOrderId(Integer purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}
	
	@Column(name = "external_po_number")
	public String getExternalPONumber() {
		return externalPONumber;
	}
	public void setExternalPONumber(String externalPONumber) {
		this.externalPONumber = externalPONumber;
	}
	
	@Column(name = "bill_due_date")
	public Date getBillDueDate() {
		return billDueDate;
	}
	public void setBillDueDate(Date billDueDate) {
		this.billDueDate = billDueDate;
	}
	
	@Column(name = "total_shipping")
	public Double getTotalShipping() {
		try{
			if(totalShipping != null){
				return Util.getRoundedValue(totalShipping);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return totalShipping;
	}
	public void setTotalShipping(Double totalShipping) {
		this.totalShipping = totalShipping;
	}
	
	@Column(name = "total_expenses")
	public Double getTotalExpenses() {
		try{
			if(totalExpenses != null){
				return Util.getRoundedValue(totalExpenses);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return totalExpenses;
	}
	public void setTotalExpenses(Double totalExpenses) {
		this.totalExpenses = totalExpenses;
	}
	
	@Column(name = "total_due")
	public Double getTotalDue() {
		try{
			if(totalDue != null){
				return Util.getRoundedValue(totalDue);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return totalDue;
	}
	public void setTotalDue(Double totalDue) {
		this.totalDue = totalDue;
	}
	
	@Column(name = "total_taxes")
	public Double getTotalTaxes() {
		try{
			if(totalTaxes != null){
				return Util.getRoundedValue(totalTaxes);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return totalTaxes;
	}
	public void setTotalTaxes(Double totalTaxes) {
		this.totalTaxes = totalTaxes;
	}
	
	@Column(name = "po_total")
	public Double getPoTotal() {
		try{
			if(poTotal != null){
				return Util.getRoundedValue(poTotal);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return poTotal;
	}
	public void setPoTotal(Double poTotal) {
		this.poTotal = poTotal;
	}
	
	@Column(name = "balance_due")
	public Double getBalanceDue() {
		try{
			if(balanceDue != null){
				return Util.getRoundedValue(balanceDue);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return balanceDue;
	}
	public void setBalanceDue(Double balanceDue) {
		this.balanceDue = balanceDue;
	}
	
	@Column(name = "notes")
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	@Column(name = "payment_terms_id")
	public Integer getPaymentTermsId() {
		return paymentTermsId;
	}
	public void setPaymentTermsId(Integer paymentTermsId) {
		this.paymentTermsId = paymentTermsId;
	}
	
	@Column(name = "shipping_tracking_id")
	public Integer getShippingTrackingId() {
		return shippingTrackingId;
	}
	public void setShippingTrackingId(Integer shippingTrackingId) {
		this.shippingTrackingId = shippingTrackingId;
	}
	
	@Column(name = "client_broker_id")
	public Integer getClientBrokerId() {
		return clientBrokerId;
	}
	public void setClientBrokerId(Integer clientBrokerId) {
		this.clientBrokerId = clientBrokerId;
	}
	
	@Column(name = "system_user_id")
	public Integer getSystemUserId() {
		return systemUserId;
	}
	public void setSystemUserId(Integer systemUserId) {
		this.systemUserId = systemUserId;
	}
	
	@Column(name = "client_broker_employee_id")
	public Integer getClientBrokerEmployeeId() {
		return clientBrokerEmployeeId;
	}
	public void setClientBrokerEmployeeId(Integer clientBrokerEmployeeId) {
		this.clientBrokerEmployeeId = clientBrokerEmployeeId;
	}
	
	@Column(name = "buyer_broker_id")
	public Integer getBuyerBrokerId() {
		return buyerBrokerId;
	}
	public void setBuyerBrokerId(Integer buyerBrokerId) {
		this.buyerBrokerId = buyerBrokerId;
	}
	
	@Column(name = "create_date")
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	
	@Column(name = "drop_shipped")
	public Integer getDropShipped() {
		return dropShipped;
	}
	public void setDropShipped(Integer dropShipped) {
		this.dropShipped = dropShipped;
	}
	
	@Column(name = "displayed_notes")
	public String getDisplayedNotes() {
		return displayedNotes;
	}
	public void setDisplayedNotes(String displayedNotes) {
		this.displayedNotes = displayedNotes;
	}
	
	@Column(name = "ticket_request_id")
	public Integer getTicketRequestId() {
		return ticketRequestId;
	}
	public void setTicketRequestId(Integer ticketRequestId) {
		this.ticketRequestId = ticketRequestId;
	}
	
	@Column(name = "reprint")
	public Integer getReprint() {
		return reprint;
	}
	public void setReprint(Integer reprint) {
		this.reprint = reprint;
	}
	
	@Column(name = "isemailed")
	public Boolean getIsEmailed() {
		return isEmailed;
	}
	public void setIsEmailed(Boolean isEmailed) {
		this.isEmailed = isEmailed;
	}
	
	@Column(name = "email_info")
	public String getEmailInfo() {
		return emailInfo;
	}
	public void setEmailInfo(String emailInfo) {
		this.emailInfo = emailInfo;
	}
	
	@Column(name = "currency_id")
	public Integer getCurrencyId() {
		return currencyId;
	}
	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}
	
	@Column(name = "data_cached")
	public Boolean getDataCached() {
		return dataCached;
	}
	public void setDataCached(Boolean dataCached) {
		this.dataCached = dataCached;
	}
	
	@Column(name = "parent_po_id")
	public Integer getParentPOId() {
		return parentPOId;
	}
	public void setParentPOId(Integer parentPOId) {
		this.parentPOId = parentPOId;
	}
	
	@Column(name = "on_hand_notes")
	public String getOnHandNotes() {
		return onHandNotes;
	}
	public void setOnHandNotes(String onHandNotes) {
		this.onHandNotes = onHandNotes;
	}
	
	@Column(name = "tran_office_id")
	public Integer getTranOfficeId() {
		return tranOfficeId;
	}
	public void setTranOfficeId(Integer tranOfficeId) {
		this.tranOfficeId = tranOfficeId;
	}
	
	@Column(name = "user_office_id")
	public Integer getUserOfficeId() {
		return userOfficeId;
	}
	public void setUserOfficeId(Integer userOfficeId) {
		this.userOfficeId = userOfficeId;
	}
	
	@Column(name = "mercury_transaction_id")
	public Integer getMercuryTransactionId() {
		return mercuryTransactionId;
	}
	public void setMercuryTransactionId(Integer mercuryTransactionId) {
		this.mercuryTransactionId = mercuryTransactionId;
	}
	/*
	@Column(name = "lister_on_hand_date")
	public Date getListerOnHandDate() {
		return listerOnHandDate;
	}
	public void setListerOnHandDate(Date listerOnHandDate) {
		this.listerOnHandDate = listerOnHandDate;
	}
	*/
	@Column(name = "tm_order_id")
	public String getTmOrderId() {
		return tmOrderId;
	}
	public void setTmOrderId(String tmOrderId) {
		this.tmOrderId = tmOrderId;
	}
	
	@Column(name = "system_user_name")
	public String getSystemUserName() {
		return systemUserName;
	}
	public void setSystemUserName(String systemUserName) {
		this.systemUserName = systemUserName;
	}
	
	@Column(name = "created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Transient
	public Integer getTotalQuantity() {
		return totalQuantity;
	}
	public void setTotalQuantity(Integer totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	@Transient
	public Integer getUsedQunatity() {
		return usedQunatity;
	}
	public void setUsedQunatity(Integer usedQunatity) {
		this.usedQunatity = usedQunatity;
	}

	@Transient
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	@Transient
	public String getCreateTimeStr() {
		Date convertCreateDate = null;	
		SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String convertCreateDateStr = dbDateFormat.format(createdDate);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		try {
			convertCreateDate = formatter.parse(dbDateFormat.format(createdDate));
		} catch (ParseException e) {
			System.out.println(e);
		}
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(createdDate);
	}
	public void setCreateTimeStr(String createTimeStr) {
		this.createTimeStr = createTimeStr;
	}
}
