package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name= "ZZZ_tmp_tm_refunds")
public class TicketMasterRefund implements Serializable {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column(name="InvoiceDate")
	private String invoiceDate;
	
	@Column(name="TicketCost")
	private Double ticketCost;
	
	@Column(name="POId")
	private String poId;
	
	@Column(name="NameOnCC")
	private String ccName;
	
	@Column(name="PODate")
	private String poDate;
	
	@Column(name="Vendor")
	private String vendor;
	
	@Column(name="Event")
	private String eventName;
	
	@Column(name="EventDate")
	private String eventDate;
	
	@Column(name="Venue")
	private String venue;
	
	@Column(name="TicketQty")
	private Integer ticketQty;
	
	@Column(name="RefundQty")
	private Integer refundQty;
	
	@Column(name="RefundCost")
	private Double refundCost;
	
	@Column(name="Section")
	private String section;
	
	@Column(name="Row")
	private String row;
	
	@Column(name="InternalTicketNotes")
	private String internalNotes;
	
	@Column(name="email")
	private String email;
	
	@Transient
	private String name;
	
	@Column(name="OLDemail")
	private String oldEmail;
	
	@Column(name="ErrorsFixedbyHand")
	private String error;
	
	@Column(name="created_Date")
	private Date createdDate;
	
	@Transient
	private Date processdDate;
	
	@Transient
	private Integer noOfDuplicates;
	
	@Transient
	private String verifiedEmail;
	
	@Transient
	private Integer mergRec;
	
	@Transient
	private Double refReceived;
	
	
	
	
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public Double getTicketCost() {
		return ticketCost;
	}
	public void setTicketCost(Double ticketCost) {
		this.ticketCost = ticketCost;
	}
	public String getPoId() {
		return poId;
	}
	public void setPoId(String poId) {
		this.poId = poId;
	}
	public String getCcName() {
		return ccName;
	}
	public void setCcName(String ccName) {
		this.ccName = ccName;
	}
	public String getPoDate() {
		return poDate;
	}
	public void setPoDate(String poDate) {
		this.poDate = poDate;
	}
	public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	public Integer getTicketQty() {
		return ticketQty;
	}
	public void setTicketQty(Integer ticketQty) {
		this.ticketQty = ticketQty;
	}
	public Integer getRefundQty() {
		return refundQty;
	}
	public void setRefundQty(Integer refundQty) {
		this.refundQty = refundQty;
	}
	public Double getRefundCost() {
		return refundCost;
	}
	public void setRefundCost(Double refundCost) {
		this.refundCost = refundCost;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getRow() {
		return row;
	}
	public void setRow(String row) {
		this.row = row;
	}
	public String getInternalNotes() {
		return internalNotes;
	}
	public void setInternalNotes(String internalNotes) {
		this.internalNotes = internalNotes;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getProcessdDate() {
		return processdDate;
	}
	public void setProcessdDate(Date processdDate) {
		this.processdDate = processdDate;
	}
	public String getOldEmail() {
		return oldEmail;
	}
	public void setOldEmail(String oldEmail) {
		this.oldEmail = oldEmail;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	public Integer getNoOfDuplicates() {
		return noOfDuplicates;
	}
	public void setNoOfDuplicates(Integer noOfDuplicates) {
		this.noOfDuplicates = noOfDuplicates;
	}
	public String getVerifiedEmail() {
		return verifiedEmail;
	}
	public void setVerifiedEmail(String verifiedEmail) {
		this.verifiedEmail = verifiedEmail;
	}
	public Integer getMergRec() {
		return mergRec;
	}
	public void setMergRec(Integer mergRec) {
		this.mergRec = mergRec;
	}
	public Double getRefReceived() {
		return refReceived;
	}
	public void setRefReceived(Double refReceived) {
		this.refReceived = refReceived;
	}
	
	

}
