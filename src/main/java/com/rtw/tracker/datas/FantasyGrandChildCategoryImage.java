package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;

@Entity
@Table(name="fantasy_grand_child_category_image")
public class FantasyGrandChildCategoryImage implements Serializable{

	private Integer id;
	private String imageFileUrl;
	private String mobileImageFileUrl;
	private String lastUpdatedBy;
	private Date lastUpdated;
	private FantasyGrandChildCategory grandChildCategory;
	private String status;
	private MultipartFile file;
	private String imageName;
	private Boolean showVenueInfo;
	private String venueInfo;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="image_url")
	public String getImageFileUrl() {
		return imageFileUrl;
	}
	public void setImageFileUrl(String imageFileUrl) {
		this.imageFileUrl = imageFileUrl;
	}
	
	@Column(name="mobile_image_url")
	public String getMobileImageFileUrl() {
		return mobileImageFileUrl;
	}
	public void setMobileImageFileUrl(String mobileImageFileUrl) {
		this.mobileImageFileUrl = mobileImageFileUrl;
	}
	
	@Column(name="last_updated_by")
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	
	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@ManyToOne
	@JoinColumn(name="fantasy_grand_child_category_id")
	public FantasyGrandChildCategory getGrandChildCategory() {
		return grandChildCategory;
	}
	public void setGrandChildCategory(FantasyGrandChildCategory grandChildCategory) {
		this.grandChildCategory = grandChildCategory;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Transient
	public MultipartFile getFile() {
		return file;
	}
	public void setFile(MultipartFile file) {
		this.file = file;
	}
	
	@Column(name="image_name")
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	
	@Column(name="visible_venue_info")
	public Boolean getShowVenueInfo() {
		return showVenueInfo;
	}
	public void setShowVenueInfo(Boolean showVenueInfo) {
		this.showVenueInfo = showVenueInfo;
	}
	
	@Column(name="venue_info")
	public String getVenueInfo() {
		return venueInfo;
	}
	public void setVenueInfo(String venueInfo) {
		this.venueInfo = venueInfo;
	}
	
	
}
