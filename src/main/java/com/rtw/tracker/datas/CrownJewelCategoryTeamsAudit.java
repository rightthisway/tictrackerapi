package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;

@Entity
@Table(name="crownjewel_category_teams_audit")
public class CrownJewelCategoryTeamsAudit  implements Serializable{

	private Integer id;
	private Integer grandChildId;
	private String name;
	private Double odds;
	private String lastUpdateddBy;
	private Date lastUpdated;
	private String status;
	private String lastUpdatedStr;
	private String action;
	private Integer markup;
	
	
	public CrownJewelCategoryTeamsAudit(CrownJewelCategoryTeams crownJewelCategoryTeam){
		this.name=crownJewelCategoryTeam.getName();
		this.odds=crownJewelCategoryTeam.getOdds();
		this.lastUpdated=crownJewelCategoryTeam.getLastUpdated();
		this.lastUpdateddBy=crownJewelCategoryTeam.getLastUpdatedBy();
		this.status=crownJewelCategoryTeam.getStatus();
		this.grandChildId = crownJewelCategoryTeam.getGrandChildId();
		this.markup = crownJewelCategoryTeam.getMarkup();
	}
	
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="grand_child_id")
	public Integer getGrandChildId() {
		return grandChildId;
	}
	public void setGrandChildId(Integer grandChildId) {
		this.grandChildId = grandChildId;
	}
	
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="odds")
	public Double getOdds() {
		return odds;
	}
	public void setOdds(Double odds) {
		this.odds = odds;
	}
	
	@Column(name="last_updated_by")
	public String getLastUpdateddBy() {
		return lastUpdateddBy;
	}
	public void setLastUpdateddBy(String lastUpdateddBy) {
		this.lastUpdateddBy = lastUpdateddBy;
	}
	
	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="action")
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	
	@Column(name="markup")
	public Integer getMarkup() {
		return markup;
	}
	public void setMarkup(Integer markup) {
		this.markup = markup;
	}


	@Transient
	public String getLastUpdatedStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(getLastUpdated());
	}
	public void setLastUpdatedStr(String lastUpdatedStr) {
		this.lastUpdatedStr = lastUpdatedStr;
	}
	
	
	
	
	
}
