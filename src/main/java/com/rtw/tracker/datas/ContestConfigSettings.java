package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * represents customer entity
 * @author Tamil
 *
 */
@Entity
@Table(name="config_settings")
public class ContestConfigSettings  implements Serializable{
	
	private Integer id;
	private String liveStreamUrl;
	private Integer liveThreshold;
	private String notification7;
	private String notification20;
	private Boolean showUpcomingContest;
	private Date updatedDateTime;
	private String updatedBy;
	
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
		
	@Column(name="video_source_url")
	public String getLiveStreamUrl() {
		return liveStreamUrl;
	}
	public void setLiveStreamUrl(String liveStreamUrl) {
		this.liveStreamUrl = liveStreamUrl;
	}
	
	@Column(name="lives_threshold")
	public Integer getLiveThreshold() {
		return liveThreshold;
	}
	public void setLiveThreshold(Integer liveThreshold) {
		this.liveThreshold = liveThreshold;
	}
	
	@Column(name="notifiction_7")
	public String getNotification7() {
		return notification7;
	}
	public void setNotification7(String notification7) {
		this.notification7 = notification7;
	}
	
	@Column(name="notifiction_20")
	public String getNotification20() {
		return notification20;
	}
	public void setNotification20(String notification20) {
		this.notification20 = notification20;
	}
	
	@Column(name="updated_datetime")
	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}
	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}
	
	@Column(name="updated_by")
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	
	@Column(name="show_upcoming_contest")
	public Boolean getShowUpcomingContest() {
		return showUpcomingContest;
	}
	public void setShowUpcomingContest(Boolean showUpcomingContest) {
		this.showUpcomingContest = showUpcomingContest;
	}
	
	
	
	
}
