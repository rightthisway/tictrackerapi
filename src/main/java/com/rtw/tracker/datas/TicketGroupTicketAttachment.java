package com.rtw.tracker.datas;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.enums.FileType;


@Entity
@Table(name="ticket_group_ticket_attachment")
public class TicketGroupTicketAttachment implements Serializable{

	private Integer id;
	private Integer ticketGroupId;
	private FileType type;
	private String filePath;
	private String extension;
	private Integer position;
	
	private String fileName;
	private String uploadedBy;
	private Date uploadedDateTime;	
	private String uploadedDateTimeStr;	
	
	@Id
	@GeneratedValue
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="ticket_group_id")
	public Integer getTicketGroupId() {
		return ticketGroupId;
	}
	public void setTicketGroupId(Integer ticketGroupId) {
		this.ticketGroupId = ticketGroupId;
	}
	
	@Column(name="position")
	public Integer getPosition() {
		return position;
	}	
	public void setPosition(Integer position) {
		this.position = position;
	}
	@Column(name="file_type")
	@Enumerated(EnumType.ORDINAL)
	public FileType getType() {
		return type;
	}
	
	public void setType(FileType type) {
		this.type = type;
	}
	
	
	@Column(name="file_path")
	public String getFilePath() {
		return filePath;
	}
	
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	@Column(name="file_extension")
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}

	@Column(name="uploaded_by")
	public String getUploadedBy() {
		return uploadedBy;
	}
	public void setUploadedBy(String uploadedBy) {
		this.uploadedBy = uploadedBy;
	}
	
	@Column(name="uploaded_datetime")
	public Date getUploadedDateTime() {
		return uploadedDateTime;
	}
	public void setUploadedDateTime(Date uploadedDateTime) {
		this.uploadedDateTime = uploadedDateTime;
	}
	
	@Transient
	public String getUploadedDateTimeStr() {
		if(uploadedDateTime != null){
			uploadedDateTimeStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(getUploadedDateTime());
		}
		return uploadedDateTimeStr;
	}
	public void setUploadedDateTimeStr(String uploadedDateTimeStr) {
		this.uploadedDateTimeStr = uploadedDateTimeStr;
	}
	
	@Transient
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}

