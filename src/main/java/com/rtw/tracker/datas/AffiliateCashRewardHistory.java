package com.rtw.tracker.datas;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonDeserialize;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.rtw.tracker.enums.RewardStatus;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * represents AffiliateCashRewardHistory entity
 * @author ulaganathan
 *
 */
@XStreamAlias("AffiliateCashRewardHistory")
@Entity
@Table(name="affiliate_cash_reward_history")
public class AffiliateCashRewardHistory  implements Serializable{
	
	private Integer id;
	private Integer userId;
	private Customer customer;
	private CustomerOrder order;
	private Double orderTotal;
	private Double creditedCash;
	private Double creditConv;
	@JsonDeserialize(using = com.rtw.tracker.utils.CustomerDateAndTimeDeserialize.class)
	@JsonSerialize(using = com.rtw.tracker.utils.CustomerDateAndTimeSerializer.class)
	private Date createDate;
	@JsonDeserialize(using = com.rtw.tracker.utils.CustomerDateAndTimeDeserialize.class)
	@JsonSerialize(using = com.rtw.tracker.utils.CustomerDateAndTimeSerializer.class)
	private Date updatedDate;
	private RewardStatus rewardStatus;
	
	private Boolean isRewardsEmailSent;
	private String emailDescription;
	private String promoCode;
    private String paymentNote;
    
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id ,Id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	@ManyToOne
	@JoinColumn(name="customer_id")
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	@ManyToOne
	@JoinColumn(name="customer_order_id")
	public CustomerOrder getOrder() {
		return order;
	}
	public void setOrder(CustomerOrder order) {
		this.order = order;
	}
	
	@Column(name="order_total")
	public Double getOrderTotal() {
		return orderTotal;
	}
	public void setOrderTotal(Double orderTotal) {
		this.orderTotal = orderTotal;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	public RewardStatus getRewardStatus() {
		return rewardStatus;
	}
	public void setRewardStatus(RewardStatus rewardStatus) {
		this.rewardStatus = rewardStatus;
	}
	
	@Column(name="updated_time")
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@Column(name="created_date")
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	@Column(name="is_email_sent")
	public Boolean getIsRewardsEmailSent() {
		return isRewardsEmailSent;
	}
	public void setIsRewardsEmailSent(Boolean isRewardsEmailSent) {
		this.isRewardsEmailSent = isRewardsEmailSent;
	}
	
	@Column(name="email_description")
	public String getEmailDescription() {
		return emailDescription;
	}
	public void setEmailDescription(String emailDescription) {
		this.emailDescription = emailDescription;
	}
	
	@Column(name="user_id")
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	@Column(name="credited_cash")
	public Double getCreditedCash() {
		return creditedCash;
	}
	public void setCreditedCash(Double creditedCash) {
		this.creditedCash = creditedCash;
	}
	
	@Column(name="credit_perc")
	public Double getCreditConv() {
		return creditConv;
	}
	public void setCreditConv(Double creditConv) {
		this.creditConv = creditConv;
	}
	
	@Column(name="promo_code")
	public String getPromoCode() {
		return promoCode;
	}
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}
	
	@Column(name="payment_note")
	public String getPaymentNote() {
		return paymentNote;
	}
	public void setPaymentNote(String paymentNote) {
		this.paymentNote = paymentNote;
	}	
	
}
