package com.rtw.tracker.datas;

public class RealTicketSectionDetails {

	private Integer ticketGroupId;
	private String section;
	private String row;
	private String seatLow;
	private String seatHigh;
	
	
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getRow() {
		return row;
	}
	public void setRow(String row) {
		this.row = row;
	}
	public String getSeatLow() {
		return seatLow;
	}
	public void setSeatLow(String seatLow) {
		this.seatLow = seatLow;
	}
	public String getSeatHigh() {
		return seatHigh;
	}
	public void setSeatHigh(String seatHigh) {
		this.seatHigh = seatHigh;
	}
	public Integer getTicketGroupId() {
		return ticketGroupId;
	}
	public void setTicketGroupId(Integer ticketGroupId) {
		this.ticketGroupId = ticketGroupId;
	}
	
	
	
	
}
