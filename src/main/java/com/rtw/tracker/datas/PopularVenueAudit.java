package com.rtw.tracker.datas;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.security.core.context.SecurityContextHolder;

import com.rtw.tmat.utils.Util;

@Entity
@Table(name="popular_venue_audit")
public class PopularVenueAudit implements Serializable{

	/**
	 * serialVersionUID is added only to remove warning
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String lastUpdateddBy;
	private Date lastUpdated;
	private String lastUpdatedStr;
	private String action;
	private Integer venueId;
	private Integer productId;
	
	
	public PopularVenueAudit(){
		
	}
	
	public PopularVenueAudit(Venue venue){
		this.lastUpdateddBy = SecurityContextHolder.getContext().getAuthentication().getName();
		this.lastUpdated= new Date();
		this.venueId=venue.getId();
	}
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="last_updated_by")
	public String getLastUpdateddBy() {
		return lastUpdateddBy;
	}
	public void setLastUpdateddBy(String lastUpdateddBy) {
		this.lastUpdateddBy = lastUpdateddBy;
	}
	
	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	

	@Column(name="action")
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}

	
	@Column(name="venue_id")
	public Integer getVenueId() {
		return venueId;
	}

	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}

	@Column(name="product_id")
	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	
	@Transient
	public String getLastUpdatedStr() {
		if(getLastUpdated()==null){
			return "TBD";
		}
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(getLastUpdated());
	}
	
	@Transient
	public void setLastUpdatedStr(String lastUpdatedStr) {
		this.lastUpdatedStr = lastUpdatedStr;
	}
}
