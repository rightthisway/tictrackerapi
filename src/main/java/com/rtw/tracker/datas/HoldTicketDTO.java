package com.rtw.tracker.datas;

public class HoldTicketDTO {

	private Integer id;
	private String cost;
	private String actualSoldPrice;
	private String facePrice;
	private String wholeSalePrice;
	private String retailPrice;
	private String section;
	private String row;
	private String seatNo;
	private String status;
	private String onHandStatus;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getActualSoldPrice() {
		return actualSoldPrice;
	}
	public void setActualSoldPrice(String actualSoldPrice) {
		this.actualSoldPrice = actualSoldPrice;
	}
	public String getFacePrice() {
		return facePrice;
	}
	public void setFacePrice(String facePrice) {
		this.facePrice = facePrice;
	}
	public String getWholeSalePrice() {
		return wholeSalePrice;
	}
	public void setWholeSalePrice(String wholeSalePrice) {
		this.wholeSalePrice = wholeSalePrice;
	}
	public String getRetailPrice() {
		return retailPrice;
	}
	public void setRetailPrice(String retailPrice) {
		this.retailPrice = retailPrice;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getRow() {
		return row;
	}
	public void setRow(String row) {
		this.row = row;
	}
	public String getSeatNo() {
		return seatNo;
	}
	public void setSeatNo(String seatNo) {
		this.seatNo = seatNo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getOnHandStatus() {
		return onHandStatus;
	}
	public void setOnHandStatus(String onHandStatus) {
		this.onHandStatus = onHandStatus;
	}
	
}
