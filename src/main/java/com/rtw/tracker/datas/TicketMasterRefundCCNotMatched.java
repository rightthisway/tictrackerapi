package com.rtw.tracker.datas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name= "ZZZ_tmp_tm_refunds_CC_Not_Matched")
public class TicketMasterRefundCCNotMatched implements Serializable  {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column(name="NotMatched")
	private String notMatched;
	
	@Column(name="CCRefundDate")
	private String ccRefundDate;
	
	@Column(name="CCName")
	private String ccName;
	
	@Column(name="CCHolderName")
	private String ccHolderName;
	
	//@Column(name="CCNumber")
	//private String ccNumber;
	
	@Column(name="CCAccount")
	private String ccAccount;
	
	@Column(name="CCRefundAmount")
	private Double ccRefundAmount;
	
	@Column(name="CCExtendedDetails")
	private String ccExtendedDetails;
	
	@Column(name="CCStatementAppearance")
	private String ccStatementAppearance;
	
	@Column(name="CCAddress")
	private String ccAddress;
	
	@Column(name="CCReference")
	private String ccReference;
	
	@Column(name="CCCategory")
	private String ccCategory;
	
	//@Column(name="CCDescription")
	//private String ccDescription;
	
	@Column(name="MappedCCHolderName")
	private String mappedCCHolderName;

	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNotMatched() {
		return notMatched;
	}

	public void setNotMatched(String notMatched) {
		this.notMatched = notMatched;
	}

	public String getCcRefundDate() {
		return ccRefundDate;
	}

	public void setCcRefundDate(String ccRefundDate) {
		this.ccRefundDate = ccRefundDate;
	}

	public String getCcName() {
		return ccName;
	}

	public void setCcName(String ccName) {
		this.ccName = ccName;
	}

	public String getCcHolderName() {
		return ccHolderName;
	}

	public void setCcHolderName(String ccHolderName) {
		this.ccHolderName = ccHolderName;
	}

	/*public String getCcNumber() {
		return ccNumber;
	}

	public void setCcNumber(String ccNumber) {
		this.ccNumber = ccNumber;
	}*/

	public String getCcAccount() {
		return ccAccount;
	}

	public void setCcAccount(String ccAccount) {
		this.ccAccount = ccAccount;
	}

	public Double getCcRefundAmount() {
		return ccRefundAmount;
	}

	public void setCcRefundAmount(Double ccRefundAmount) {
		this.ccRefundAmount = ccRefundAmount;
	}

	public String getCcExtendedDetails() {
		return ccExtendedDetails;
	}

	public void setCcExtendedDetails(String ccExtendedDetails) {
		this.ccExtendedDetails = ccExtendedDetails;
	}

	public String getCcStatementAppearance() {
		return ccStatementAppearance;
	}

	public void setCcStatementAppearance(String ccStatementAppearance) {
		this.ccStatementAppearance = ccStatementAppearance;
	}

	public String getCcAddress() {
		return ccAddress;
	}

	public void setCcAddress(String ccAddress) {
		this.ccAddress = ccAddress;
	}

	public String getCcReference() {
		return ccReference;
	}

	public void setCcReference(String ccReference) {
		this.ccReference = ccReference;
	}

	public String getCcCategory() {
		return ccCategory;
	}

	public void setCcCategory(String ccCategory) {
		this.ccCategory = ccCategory;
	}

	/*public String getCcDescription() {
		return ccDescription;
	}

	public void setCcDescription(String ccDescription) {
		this.ccDescription = ccDescription;
	}*/

	public String getMappedCCHolderName() {
		return mappedCCHolderName;
	}

	public void setMappedCCHolderName(String mappedCCHolderName) {
		this.mappedCCHolderName = mappedCCHolderName;
	}
	
}
