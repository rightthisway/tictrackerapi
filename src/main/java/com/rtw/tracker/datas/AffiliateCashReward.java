package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.rtw.tmat.utils.Util;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * represents AffiliateCashReward entity
 * @author Ulaganathan
 *
 */
@XStreamAlias("AffiliateCashReward")
@Entity
@Table(name="affiliate_cash_reward")
public class AffiliateCashReward  implements Serializable{
	
	private Integer id;
	private TrackerUser user;	
	private Double activeCash;
	private Double pendingCash;
	private Double lastCreditedCash;
	private Double lastDebitedCash;
	private Double lastVoidCash;
	private Double totalCreditedCash;
	private Double totalDebitedCash;
	private Double totalVoidedCash;	
	private Double lastPaidCash;
	private Double totalPaidCash;
	
	private Date lastUpdate;
	private String lastUpdatedDateStr;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id ,Id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	@ManyToOne
	@JoinColumn(name="user_id")
	public TrackerUser getUser() {
		return user;
	}
	public void setUser(TrackerUser user) {
		this.user = user;
	}
	
	@Column(name="active_cash")
	public Double getActiveCash() {
		return activeCash;
	}
	public void setActiveCash(Double activeCash) {
		this.activeCash = activeCash;
	}
	
	@Column(name="pending_cash")
	public Double getPendingCash() {
		return pendingCash;
	}
	public void setPendingCash(Double pendingCash) {
		this.pendingCash = pendingCash;
	}
	
	@Column(name="last_credited_cash")
	public Double getLastCreditedCash() {
		return lastCreditedCash;
	}
	public void setLastCreditedCash(Double lastCreditedCash) {
		this.lastCreditedCash = lastCreditedCash;
	}
	
	@Column(name="latest_debited_cash")
	public Double getLastDebitedCash() {
		return lastDebitedCash;
	}
	public void setLastDebitedCash(Double lastDebitedCash) {
		this.lastDebitedCash = lastDebitedCash;
	}
	
	@Column(name="last_voided_cash")
	public Double getLastVoidCash() {
		return lastVoidCash;
	}
	public void setLastVoidCash(Double lastVoidCash) {
		this.lastVoidCash = lastVoidCash;
	}
	
	@Column(name="total_credited_cash")
	public Double getTotalCreditedCash() {
		return totalCreditedCash;
	}
	public void setTotalCreditedCash(Double totalCreditedCash) {
		this.totalCreditedCash = totalCreditedCash;
	}
	
	@Column(name="total_debited_cash")
	public Double getTotalDebitedCash() {
		return totalDebitedCash;
	}
	public void setTotalDebitedCash(Double totalDebitedCash) {
		this.totalDebitedCash = totalDebitedCash;
	}
	
	@Column(name="total_voided_cash")
	public Double getTotalVoidedCash() {
		return totalVoidedCash;
	}
	public void setTotalVoidedCash(Double totalVoidedCash) {
		this.totalVoidedCash = totalVoidedCash;
	}
	
	@Column(name="last_paid_cash")
	public Double getLastPaidCash() {
		return lastPaidCash;
	}
	public void setLastPaidCash(Double lastPaidCash) {
		this.lastPaidCash = lastPaidCash;
	}
	
	@Column(name="total_paid_cash")
	public Double getTotalPaidCash() {
		return totalPaidCash;
	}
	public void setTotalPaidCash(Double totalPaidCash) {
		this.totalPaidCash = totalPaidCash;
	}
	
	@Column(name="updated_time")
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	@Transient
	public String getLastUpdatedDateStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(lastUpdate);
	}
	public void setLastUpdatedDateStr(String lastUpdatedDateStr) {
		this.lastUpdatedDateStr = lastUpdatedDateStr;
	}
}
