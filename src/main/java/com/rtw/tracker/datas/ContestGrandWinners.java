package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "contest_grand_winners")
public class ContestGrandWinners implements Serializable{
	
	private Integer id;
	private Integer customerId;
	private Integer contestId;
	private Integer rewardTickets;
	private Date createdDate;
	private Date expiryDate;
	private Integer eventId;
	private Integer orderId;
	private String status;
	private Boolean isNotified;
	private Date notifiedDate;
	private String jackpotType;
	private String winnerType;
	private String notes;
	private Integer questionId;
	
	
	@Id
	@GeneratedValue
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Column(name="contest_id")
	public Integer getContestId() {
		return contestId;
	}
	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}
	
	@Column(name="reward_tickets")
	public Integer getRewardTickets() {
		return rewardTickets;
	}
	public void setRewardTickets(Integer rewardTickets) {
		this.rewardTickets = rewardTickets;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="expiry_date")
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="order_id")
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="is_notified")
	public Boolean getIsNotified() {
		return isNotified;
	}
	public void setIsNotified(Boolean isNotified) {
		this.isNotified = isNotified;
	}
	
	@Column(name="notified_date")
	public Date getNotifiedDate() {
		return notifiedDate;
	}
	public void setNotifiedDate(Date notifiedDate) {
		this.notifiedDate = notifiedDate;
	}
	
	@Column(name="mini_jackpot_type")
	public String getJackpotType() {
		return jackpotType;
	}
	public void setJackpotType(String jackpotType) {
		this.jackpotType = jackpotType;
	}
	
	@Column(name="winner_type")
	public String getWinnerType() {
		return winnerType;
	}
	public void setWinnerType(String winnerType) {
		this.winnerType = winnerType;
	}
	
	@Column(name="notes")
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	@Column(name="question_id")
	public Integer getQuestionId() {
		return questionId;
	}
	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

}
