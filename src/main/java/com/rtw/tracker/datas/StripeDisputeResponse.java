package com.rtw.tracker.datas;

import java.util.List;


public class StripeDisputeResponse {

	private Integer status;
	private com.rtw.tmat.utils.Error error; 
	private String message;
	private List<StripeDispute> disputes;
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public com.rtw.tmat.utils.Error getError() {
		return error;
	}
	public void setError(com.rtw.tmat.utils.Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<StripeDispute> getDisputes() {
		return disputes;
	}
	public void setDisputes(List<StripeDispute> disputes) {
		this.disputes = disputes;
	}
	
	
}
