package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;

@Entity
@Table(name = "polling_customer_video_uploads")
//select id,video_url,cust_id,status,is_winner_or_general,upload_date from polling_customer_video_uploads;
public class PollingCustomerVideo implements Serializable {

	private static final long serialVersionUID = -5002012660129886861L;
	
	private Integer id;
	private Integer customerId;
	private String videoUrl; 
	private String status;
	private Boolean isWinnerOrGeneral;
	private Date uploadDate; 
	private String userId;
	private String customerName;
	private String phoneNo;
	private String emailId;
	private String uploadDateStr;
	private String dispFileName;
	private String updatedBy;
	private Date updatedDate;
	private Boolean isPublished;
	private Date blockDate;
	private String blockBy;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "cust_id")
	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	@Column(name = "video_url")
	public String getVideoUrl() {
		return videoUrl;
	}

	public void setVideoUrl(String videoUrl) {
		this.videoUrl = videoUrl;
	}

	
	@Column(name = "status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
	@Column(name = "is_winner_or_general")
	public Boolean getIsWinnerOrGeneral() {
		return isWinnerOrGeneral;
	}

	public void setIsWinnerOrGeneral(Boolean isWinnerOrGeneral) {
		this.isWinnerOrGeneral = isWinnerOrGeneral;
	}

	
	@Column(name = "upload_date")
	public Date getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}
	
	
	@Column(name = "updated_by")
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column(name = "updated_date")
	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	
	@Column(name = "is_published")
	public Boolean getIsPublished() {
		return isPublished;
	}

	public void setIsPublished(Boolean isPublished) {
		this.isPublished = isPublished;
	}
	
	
	@Column(name = "blocked_date")
	public Date getBlockDate() {
		return blockDate;
	}

	public void setBlockDate(Date blockDate) {
		this.blockDate = blockDate;
	}

	@Column(name = "blocked_by")
	public String getBlockBy() {
		return blockBy;
	}

	public void setBlockBy(String blockBy) {
		this.blockBy = blockBy;
	}

	@Transient
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Transient
	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	@Transient
	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	
	@Transient
	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	@Transient
	public String getUploadDateStr() {
		if(uploadDate!=null){
			this.uploadDateStr = Util.formatDateToMonthDateYear(uploadDate);
		}
		return uploadDateStr;
	}

	public void setUploadDateStr(String uploadDateStr) {
		this.uploadDateStr = uploadDateStr;
	}
	
	@Transient
	public String getDispFileName() {
		return userId + "_" + videoUrl ;
	}

	public void setDispFileName(String dispFileName) {
		this.dispFileName = dispFileName;
	}
 

}
