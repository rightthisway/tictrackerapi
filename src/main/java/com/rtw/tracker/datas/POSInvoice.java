package com.rtw.tracker.datas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="pos_invoice")
public class POSInvoice implements Serializable {
	
	private Integer id;
	private String productType;
	private Double invoiceTax;
	private Double invoiceExpense;
	private Double invoiceShippingCharges;
	private Double invoiceBalanceDue;
	
	@Id
	@Column(name="invoice_id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="product_type")
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	
	@Column(name="invoice_total_taxes")
	public Double getInvoiceTax() {
		return invoiceTax;
	}
	public void setInvoiceTax(Double invoiceTax) {
		this.invoiceTax = invoiceTax;
	}
	
	@Column(name="invoice_total_expense")
	public Double getInvoiceExpense() {
		return invoiceExpense;
	}
	public void setInvoiceExpense(Double invoiceExpense) {
		this.invoiceExpense = invoiceExpense;
	}
	
	@Column(name="invoice_total_shipping_cost")
	public Double getInvoiceShippingCharges() {
		return invoiceShippingCharges;
	}
	public void setInvoiceShippingCharges(Double invoiceShippingCharges) {
		this.invoiceShippingCharges = invoiceShippingCharges;
	}
	
	@Column(name="invoice_balance_due")
	public Double getInvoiceBalanceDue() {
		return invoiceBalanceDue;
	}
	public void setInvoiceBalanceDue(Double invoiceBalanceDue) {
		this.invoiceBalanceDue = invoiceBalanceDue;
	}
	
	
	

}
