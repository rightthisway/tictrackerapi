package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="pos_category_ticket_group")
public class POSCategoryTicketGroup implements Serializable{
	
	private String productType;	//product_type		
	private Integer productId; //product_id
	private Integer categoryTicketGroupId;	//category_ticket_group_id
	private Integer eventId; 	//event_id
	private Integer venueCategoryId;  //venue_category_id
	private Integer ticketCount;	//ticket_count
	private Double wholesalePrice;	//wholesale_price
	private Double retailPrice; 	//retail_price
	private String notes;	//notes
	private Date expectedArrivalDate; //expected_arrival_date
	private Double facePrice; //face_price
	private Double cost; //cost
	private String internalNotes;	//internal_notes
	//private Boolean taxExempt;	//tax_exempt
	private Date updateDateTime;	//update_datetime
	private Integer broadCast;	//broadcast
	private Date createDate;	//create_date
	private Integer officeId;	//office_id
	private Integer ticketGroupCodeId;	//ticket_group_code_id
	//private Date rowVersion;	//row_version
	private Integer unbroadcastDaysBeforeEvent;	//unbroadcast_days_before_event
	private Integer shippingMethodSpecialId; 	//shipping_method_special_id
	private Boolean showNearTermOptionId;	//show_near_term_option_id
	private Date priceUpdateDatetime;	//price_update_datetime
	//private Boolean tgNoteGrandFathered;	//tg_note_grandfathered
	//private Boolean autoProcessWebRequests;	//auto_process_web_requests
	private Integer venueConfiguratioinZoneId;	//venue_configuration_zone_id
	private Integer maxShowing;	//max_showing
	//private Boolean mergeRowVersion;	//merge_row_version
	private Integer splitRuleId;	//split_rule_id
	private Double lastWholesalePrice;	//last_wholesale_price
	private Integer wholesalePriceModSystemUserId; 	//wholesale_price_mod_system_user_id
	private Integer modSystemUserId;	//mod_system_user_id
	private String canonicalSection;	//canonical_section
	private Date canonicalSectionGenerateDate;	//canonical_section_generate_date
	private String standardSectionName;	//standard_section_name
	private Double actualPrice;
	
	
	public POSCategoryTicketGroup(){
		
	}
	
	@Column(name = "product_type")
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	
	@Column(name = "product_id")
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	
	@Id
	@Column(name = "category_ticket_group_id")
	public Integer getCategoryTicketGroupId() {
		return categoryTicketGroupId;
	}
	public void setCategoryTicketGroupId(Integer categoryTicketGroupId) {
		this.categoryTicketGroupId = categoryTicketGroupId;
	}
	
	@Column(name = "event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name = "venue_category_id")
	public Integer getVenueCategoryId() {
		return venueCategoryId;
	}
	public void setVenueCategoryId(Integer venueCategoryId) {
		this.venueCategoryId = venueCategoryId;
	}
	
	@Column(name = "ticket_count")
	public Integer getTicketCount() {
		return ticketCount;
	}
	public void setTicketCount(Integer ticketCount) {
		this.ticketCount = ticketCount;
	}
	
	@Column(name = "wholesale_price")
	public Double getWholesalePrice() {
		return wholesalePrice;
	}
	public void setWholesalePrice(Double wholesalePrice) {
		this.wholesalePrice = wholesalePrice;
	}
	
	@Column(name = "retail_price")
	public Double getRetailPrice() {
		return retailPrice;
	}
	public void setRetailPrice(Double retailPrice) {
		this.retailPrice = retailPrice;
	}
	
	@Column(name = "notes")
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	@Column(name = "expected_arrival_date")
	public Date getExpectedArrivalDate() {
		return expectedArrivalDate;
	}
	public void setExpectedArrivalDate(Date expectedArrivalDate) {
		this.expectedArrivalDate = expectedArrivalDate;
	}
	
	@Column(name = "face_price")
	public Double getFacePrice() {
		return facePrice;
	}
	public void setFacePrice(Double facePrice) {
		this.facePrice = facePrice;
	}
	
	@Column(name = "cost")
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	
	@Column(name = "internal_notes")
	public String getInternalNotes() {
		return internalNotes;
	}
	public void setInternalNotes(String internalNotes) {
		this.internalNotes = internalNotes;
	}
	/*
	@Column(name = "tax_exempt")
	public Boolean getTaxExempt() {
		return taxExempt;
	}
	public void setTaxExempt(Boolean taxExempt) {
		this.taxExempt = taxExempt;
	}
	*/
	@Column(name = "update_datetime")
	public Date getUpdateDateTime() {
		return updateDateTime;
	}
	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}
	
	@Column(name = "broadcast")
	public Integer getBroadCast() {
		return broadCast;
	}
	public void setBroadCast(Integer broadCast) {
		this.broadCast = broadCast;
	}
	
	@Column(name = "create_date")
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	@Column(name = "office_id")
	public Integer getOfficeId() {
		return officeId;
	}
	public void setOfficeId(Integer officeId) {
		this.officeId = officeId;
	}
	
	@Column(name = "ticket_group_code_id")
	public Integer getTicketGroupCodeId() {
		return ticketGroupCodeId;
	}
	public void setTicketGroupCodeId(Integer ticketGroupCodeId) {
		this.ticketGroupCodeId = ticketGroupCodeId;
	}
	/*
	@Column(name = "row_version")
	public Date getRowVersion() {
		return rowVersion;
	}
	public void setRowVersion(Date rowVersion) {
		this.rowVersion = rowVersion;
	}
	*/
	@Column(name = "unbroadcast_days_before_event")
	public Integer getUnbroadcastDaysBeforeEvent() {
		return unbroadcastDaysBeforeEvent;
	}
	public void setUnbroadcastDaysBeforeEvent(Integer unbroadcastDaysBeforeEvent) {
		this.unbroadcastDaysBeforeEvent = unbroadcastDaysBeforeEvent;
	}
	
	@Column(name = "shipping_method_special_id")
	public Integer getShippingMethodSpecialId() {
		return shippingMethodSpecialId;
	}
	public void setShippingMethodSpecialId(Integer shippingMethodSpecialId) {
		this.shippingMethodSpecialId = shippingMethodSpecialId;
	}
	
	@Column(name = "show_near_term_option_id")
	public Boolean getShowNearTermOptionId() {
		return showNearTermOptionId;
	}
	public void setShowNearTermOptionId(Boolean showNearTermOptionId) {
		this.showNearTermOptionId = showNearTermOptionId;
	}
	
	@Column(name = "price_update_datetime")
	public Date getPriceUpdateDatetime() {
		return priceUpdateDatetime;
	}
	public void setPriceUpdateDatetime(Date priceUpdateDatetime) {
		this.priceUpdateDatetime = priceUpdateDatetime;
	}
	/*
	@Column(name = "tg_note_grandfathered")
	public Boolean getTgNoteGrandFathered() {
		return tgNoteGrandFathered;
	}
	public void setTgNoteGrandFathered(Boolean tgNoteGrandFathered) {
		this.tgNoteGrandFathered = tgNoteGrandFathered;
	}
	
	@Column(name = "auto_process_web_requests")
	public Boolean getAutoProcessWebRequests() {
		return autoProcessWebRequests;
	}
	public void setAutoProcessWebRequests(Boolean autoProcessWebRequests) {
		this.autoProcessWebRequests = autoProcessWebRequests;
	}
	*/
	@Column(name = "venue_configuration_zone_id")
	public Integer getVenueConfiguratioinZoneId() {
		return venueConfiguratioinZoneId;
	}
	public void setVenueConfiguratioinZoneId(Integer venueConfiguratioinZoneId) {
		this.venueConfiguratioinZoneId = venueConfiguratioinZoneId;
	}
	
	@Column(name = "max_showing")
	public Integer getMaxShowing() {
		return maxShowing;
	}
	public void setMaxShowing(Integer maxShowing) {
		this.maxShowing = maxShowing;
	}
	/*
	@Column(name = "merge_row_version")
	public Boolean getMergeRowVersion() {
		return mergeRowVersion;
	}
	public void setMergeRowVersion(Boolean mergeRowVersion) {
		this.mergeRowVersion = mergeRowVersion;
	}
	*/
	@Column(name = "split_rule_id")
	public Integer getSplitRuleId() {
		return splitRuleId;
	}
	public void setSplitRuleId(Integer splitRuleId) {
		this.splitRuleId = splitRuleId;
	}
	
	@Column(name = "last_wholesale_price")
	public Double getLastWholesalePrice() {
		return lastWholesalePrice;
	}
	public void setLastWholesalePrice(Double lastWholesalePrice) {
		this.lastWholesalePrice = lastWholesalePrice;
	}
	
	@Column(name = "wholesale_price_mod_system_user_id")
	public Integer getWholesalePriceModSystemUserId() {
		return wholesalePriceModSystemUserId;
	}
	public void setWholesalePriceModSystemUserId(
			Integer wholesalePriceModSystemUserId) {
		this.wholesalePriceModSystemUserId = wholesalePriceModSystemUserId;
	}
	
	@Column(name = "mod_system_user_id")
	public Integer getModSystemUserId() {
		return modSystemUserId;
	}
	public void setModSystemUserId(Integer modSystemUserId) {
		this.modSystemUserId = modSystemUserId;
	}
	
	@Column(name = "canonical_section")
	public String getCanonicalSection() {
		return canonicalSection;
	}
	public void setCanonicalSection(String canonicalSection) {
		this.canonicalSection = canonicalSection;
	}
	
	@Column(name = "canonical_section_generate_date")
	public Date getCanonicalSectionGenerateDate() {
		return canonicalSectionGenerateDate;
	}
	public void setCanonicalSectionGenerateDate(Date canonicalSectionGenerateDate) {
		this.canonicalSectionGenerateDate = canonicalSectionGenerateDate;
	}
	
	@Column(name = "standard_section_name")
	public String getStandardSectionName() {
		return standardSectionName;
	}
	public void setStandardSectionName(String standardSectionName) {
		this.standardSectionName = standardSectionName;
	}

	@Transient
	public Double getActualPrice() {
		return actualPrice;
	}

	public void setActualPrice(Double actualPrice) {
		this.actualPrice = actualPrice;
	}
	
	
	
}
