package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.enums.PromotionalType;


@Entity
@Table(name = "rtf_promotional_order_tracking_new")
public class RTFPromotionalOfferTracking implements Serializable{	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name="promotional_offer_id")
	private Integer promotionalOfferId;
	
	@Column(name="customer_id")
	private Integer customerId;
	
	@Column(name="event_id")
	private Integer eventId;
	
	@Column(name="ticket_group_id")
	private Integer ticketGroupId;
	
	@Column(name="session_id")
	private String sessionId; 
	
	@Enumerated(EnumType.STRING)
	@Column(name="platform")
	private ApplicationPlatform platForm;
	
	@Enumerated(EnumType.STRING)
	@Column(name="offer_type")
	private PromotionalType offerType;
	
	@Column(name="order_id")
	private Integer orderId;
	
	@Column(name="additional_discount_conv")
	private Double additionalDiscountConv;
	
	@Column(name="additional_discount_amount") 
	private Double additionalDiscountAmt;
	
	@Column(name="total_price")
	private Double totalPrice;
	
	@Column(name="service_fees")
	private Double serviceFees;
	
	@Column(name="promo_code")
	private String promoCode;
	
	@Column(name="created_date")
	private Date createdDate;
	
	@Column(name="status")
	private String status;
	
	@Column(name="updated_date")
	private Date updatedDate;
	
	@Column(name="is_long_ticket")
	private Boolean isLongTicket;

	@Column(name="is_flat_discount")
	private Boolean isFlatDiscount;
	
	@Transient
	private String firstName;
	@Transient
	private String lastName;
	@Transient
	private String email;
	@Transient
	private String discount;
	@Transient
	private Double orderTotal;
	@Transient
	private Integer quantity;
	@Transient
	private String ipAddress;
	@Transient
	private String primaryPaymentMethod;
	@Transient
	private String secondaryPaymentMethod;
	@Transient
	private String thirdPaymentMethod;
	@Transient
	private String createdDateStr;
	@Transient
	private String updatedDateStr;
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPromotionalOfferId() {
		return promotionalOfferId;
	}

	public void setPromotionalOfferId(Integer promotionalOfferId) {
		this.promotionalOfferId = promotionalOfferId;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public Integer getTicketGroupId() {
		return ticketGroupId;
	}

	public void setTicketGroupId(Integer ticketGroupId) {
		this.ticketGroupId = ticketGroupId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public ApplicationPlatform getPlatForm() {
		return platForm;
	}

	public void setPlatForm(ApplicationPlatform platForm) {
		this.platForm = platForm;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Double getAdditionalDiscountConv() {
		return additionalDiscountConv;
	}

	public void setAdditionalDiscountConv(Double additionalDiscountConv) {
		this.additionalDiscountConv = additionalDiscountConv;
	}

	public Double getAdditionalDiscountAmt() {
		return additionalDiscountAmt;
	}

	public void setAdditionalDiscountAmt(Double additionalDiscountAmt) {
		this.additionalDiscountAmt = additionalDiscountAmt;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Double getServiceFees() {
		return serviceFees;
	}

	public void setServiceFees(Double serviceFees) {
		this.serviceFees = serviceFees;
	}

	public Boolean getIsLongTicket() {
		return isLongTicket;
	}

	public void setIsLongTicket(Boolean isLongTicket) {
		this.isLongTicket = isLongTicket;
	}

	public Boolean getIsFlatDiscount() {
		return isFlatDiscount;
	}

	public void setIsFlatDiscount(Boolean isFlatDiscount) {
		this.isFlatDiscount = isFlatDiscount;
	}

	public PromotionalType getOfferType() {
		return offerType;
	}

	public void setOfferType(PromotionalType offerType) {
		this.offerType = offerType;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Double getOrderTotal() {
		return orderTotal;
	}

	public void setOrderTotal(Double orderTotal) {
		this.orderTotal = orderTotal;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getPrimaryPaymentMethod() {
		return primaryPaymentMethod;
	}

	public void setPrimaryPaymentMethod(String primaryPaymentMethod) {
		this.primaryPaymentMethod = primaryPaymentMethod;
	}

	public String getSecondaryPaymentMethod() {
		return secondaryPaymentMethod;
	}

	public void setSecondaryPaymentMethod(String secondaryPaymentMethod) {
		this.secondaryPaymentMethod = secondaryPaymentMethod;
	}

	public String getThirdPaymentMethod() {
		return thirdPaymentMethod;
	}

	public void setThirdPaymentMethod(String thirdPaymentMethod) {
		this.thirdPaymentMethod = thirdPaymentMethod;
	}

	public String getCreatedDateStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(createdDate);
	}

	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}

	public String getUpdatedDateStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(updatedDate);
	}

	public void setUpdatedDateStr(String updatedDateStr) {
		this.updatedDateStr = updatedDateStr;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}
	
	
}

