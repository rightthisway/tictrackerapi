package com.rtw.tracker.datas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tracker.dao.implementation.DAORegistry;

 /**
  * class to represent Grand child category of a tour.
  * @author hamin
  *
  */
@Entity
@Table(name="grand_child_category")
public class GrandChildCategory implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private Integer id;
	private String tnName;
	private Integer tnId;
	//private Integer childCatId;
	//private ChildCategory  childCategory;
	private Boolean isDisplay;
	
	/**
	 * @return the id
	 */
	
	@Id
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * @return the name
	 */
	@Column(name="name")
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
	@Column(name="tn_name")
	public String getTnName() {
		return tnName;
	}
	public void setTnName(String tnName) {
		this.tnName = tnName;
	}
	
	
	@Column(name="display_on_search")
	public Boolean getIsDisplay() {
		return isDisplay;
	}

	public void setIsDisplay(Boolean isDisplay) {
		this.isDisplay = isDisplay;
	}
	
	@Column(name="tn_grand_child_category_id")
	public Integer getTnId() {
		return tnId;
	}
	public void setTnId(Integer tnId) {
		this.tnId = tnId;
	}
	
	
	
	/**
	 * @return the parentCategory
	 *//*
	@OneToOne
	@JoinColumn(name="child_category_id")
	public ChildCategory getChildCategory() {
		return childCategory;
	}
	*//**
	 * @param parentCategory the parentCategory to set
	 *//*
	public void setChildCategory(ChildCategory childCategory) {
		this.childCategory = childCategory;
	}
	
	@Column(name="tmat_grand_child_category_id")
	public Integer getTmatGrandChildCategoryid() {
		return tmatGrandChildCategoryid;
	}
	public void setTmatGrandChildCategoryid(Integer tmatGrandChildCategoryid) {
		this.tmatGrandChildCategoryid = tmatGrandChildCategoryid;
	}
	*//**
	 * String representation
	 *//*
	@Override
	public String toString() {
		 
		return  "[ id:"+id+" name: "+name+" childCategory: "+childCategory.getName()+"]";
	}*/
	
	
	/*@Column(name="child_category_id")
	public Integer getChildCatId() {
		return childCatId;
	}
	public void setChildCatId(Integer childCatId) {
		this.childCatId = childCatId;
	}
	
	@Transient
	public ChildCategory getChildCategory() {
		if(null !=  childCatId && childCatId> 0){
			childCategory = DAORegistry.getChildCategoryDAO().get(childCatId);
		}
		return childCategory;
	}
	public void setChildCategory(ChildCategory childCategory) {
		this.childCategory = childCategory;
	}*/
	
	
}
