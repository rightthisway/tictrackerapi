package com.rtw.tracker.datas;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("LoyalFanAutoSearch")
public class LoyalFanAutoSearch {
	private Integer status;
	private com.rtw.tmat.utils.Error error; 
	private List<LoyalFanStateSearch> searchList;
	
	public com.rtw.tmat.utils.Error getError() {
		return error;
	}
	public void setError(com.rtw.tmat.utils.Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public List<LoyalFanStateSearch> getSearchList() {
		return searchList;
	}
	public void setSearchList(List<LoyalFanStateSearch> searchList) {
		this.searchList = searchList;
	}

}
