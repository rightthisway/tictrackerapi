package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="customer_address")
public class CustomerAddress  implements Serializable{
	
	private Integer id;
	private Integer customerId;
	private String firstName;
	private String lastName;
	private String addressLine1;
	private String addressLine2;
	private AddressType addressType;
	private String city;
	private State state;
	private Country country;
	private String stateName;
	private String countryName;
	private String zipCode;
	private String phone1;
	private String phone2;
	private String phone3;
	private String representativeName;
	public Date lastUpdated;
	private String email;
	private EventStatus status;
	
	/*@OneToOne(mappedBy="billingAddress")
	private Customer billingAddressCust;*/
	
	/*@OneToOne(mappedBy="shippingAddress")
	private Customer shippingAddressCust;*/
	
	/*@OneToMany(mappedBy="customerAddress")
	private Customer customer;*/
	
	/**
	 *  
	 * @return id
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id, Id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	
	
	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	
	@Column(name="first_name")
	public String getFirstName() {
		if(null == firstName || firstName.isEmpty()){
			firstName="";
		}
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	@Column(name="last_name")
	public String getLastName() {
		if(null == lastName || lastName.isEmpty()){
			lastName="";
		}
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	/**
	 * 
	 * @return addressLine1
	 */
	@Column(name="address_line1")
	public  String  getAddressLine1() {
		/*if(null == addressLine1 || addressLine1.isEmpty()){
			addressLine1="";
		}*/
		return addressLine1;
	}
	/**
	 * 
	 * @param addressLine1,  address line 1 to set
	 */
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	/**
	 * 
	 * @return addressLine2
	 */
	@Column(name="address_line2")
	public  String  getAddressLine2() {
		/*if(null == addressLine2 ){
			addressLine2 = "";
		}*/
		return addressLine2;
	}
	/**
	 * 
	 * @param addressLine2, address line 2 to set
	 */
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	/**
	 * 
	 * @return addressType
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="address_type")
	public AddressType getAddressType() {
		return addressType;
	}
	public void setAddressType(AddressType addressType) {
		this.addressType = addressType;
	}
	
	/**
	 *  
	 * @return country
	 */
	@ManyToOne
	@JoinColumn(name="country")
	public  Country  getCountry() {
		return country;
	}
	
	/**
	 * 
	 * @param country, country to set
	 */
	public void setCountry(Country country) {
		this.country = country;
	}
	/**
	 * 
	 * @return city
	 */
	@Column(name="city")
	public  String  getCity() {
		if(null == city ){
			city = "";
		}
		return city;
	}
	/**
	 * 
	 * @param city, city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * 
	 * @return state
	 */
	@ManyToOne
	@JoinColumn(name="state")
	public  State  getState() {
		return state;
	}
	/**
	 * 
	 * @param state, state to set
	 */
	public void setState(State state) {
		this.state = state;
	}
	/**
	 * 
	 * @return zipCode
	 */
	@Column(name="zip_code")
	public  String  getZipCode() {
		if(null == zipCode ){
			zipCode = "";
		}
		return zipCode;
	}
	/**
	 * 
	 * @param zipCode, zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
	
	/**
	 * 
	 * @return phone1
	 */
	@Column(name="phone1")
	public  String  getPhone1() {
		if(null == phone1 ){
			phone1 = "";
		}
		return phone1;
	}
	/**
	 * 
	 * @param phone1, phone1 to set
	 */
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}
	
	@Column(name="phone2")
	public String getPhone2() {
		return phone2;
	}
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
	
	@Column(name="phone3")
	public String getPhone3() {
		return phone3;
	}
	public void setPhone3(String phone3) {
		this.phone3 = phone3;
	}
	
	@Column(name="representativeName")
	public String getRepresentativeName() {
		return representativeName;
	}
	public void setRepresentativeName(String representativeName) {
		this.representativeName = representativeName;
	}
	
	@Transient
	public String getStateName() {
		if(state != null && null != state.getId()){
			stateName = state.getName();
		}
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	
	@Transient
	public String getCountryName() {
		if(country != null && null != country.getId()){
			countryName = country.getName();
		}
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	/*public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}*/
	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Column(name="email")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	public EventStatus getStatus() {
		return status;
	}
	public void setStatus(EventStatus status) {
		this.status = status;
	}
	
	
	/*public Customer getBillingAddressCust() {
		return billingAddressCust;
	}
	
	public void setBillingAddressCust(Customer billingAddressCust) {
		this.billingAddressCust = billingAddressCust;
	}
	
	public Customer getShippingAddressCust() {
		return shippingAddressCust;
	}
	
	public void setShippingAddressCust(Customer shippingAddressCust) {
		this.shippingAddressCust = shippingAddressCust;
	}*/
	
	
}