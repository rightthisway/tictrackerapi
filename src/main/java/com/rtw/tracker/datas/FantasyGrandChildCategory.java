package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.map.annotate.JsonDeserialize;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.rtw.tmat.utils.Util;

@Entity
@Table(name="fantasy_grandchild_category")
public class FantasyGrandChildCategory implements Serializable{

	private static final long serialVersionUID = 1L;
	private String name;
	private Integer id;
	private Integer childId;
	private String childCategoryName;
	private Integer parentId;
	private String parentCategoryName;
	private String lastUpdatedBy;
	@JsonDeserialize(using = com.rtw.tracker.utils.CustomerDateAndTimeDeserialize.class)
	@JsonSerialize(using = com.rtw.tracker.utils.CustomerDateAndTimeSerializer.class)
	private Date lastUpdated;
	private String lastUpdatedStr;
	private Double packageCost;
	//private String packageDescription;
	private String packageLine1;
	private String packageLine2;
	private String packageLine3;
	private String packageLine4;
	private String packageLine5;
	private String packageLine6;
	
	public FantasyGrandChildCategory(){}
	
	/**
	 * @return the id
	 */
	
	@Id
	@GeneratedValue
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * @return the name
	 */
	@Column(name="name")
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Column(name="child_id")
	public Integer getChildId() {
		return childId;
	}

	public void setChildId(Integer childId) {
		this.childId = childId;
	}

	@Transient
	public String getChildCategoryName() {
		return childCategoryName;
	}

	public void setChildCategoryName(String childCategoryName) {
		this.childCategoryName = childCategoryName;
	}

	@Transient
	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	@Transient
	public String getParentCategoryName() {
		return parentCategoryName;
	}

	public void setParentCategoryName(String parentCategoryName) {
		this.parentCategoryName = parentCategoryName;
	}

	@Column(name="last_updated_by")
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Column(name="package_cost")
	public Double getPackageCost() {
		return packageCost;
	}

	public void setPackageCost(Double packageCost) {
		this.packageCost = packageCost;
	}

	/*@Column(name="package_description")
	public String getPackageDescription() {
		return packageDescription;
	}

	public void setPackageDescription(String packageDescription) {
		this.packageDescription = packageDescription;
	}*/

	@Column(name="package_line1")
	public String getPackageLine1() {
		return packageLine1;
	}

	public void setPackageLine1(String packageLine1) {
		this.packageLine1 = packageLine1;
	}

	@Column(name="package_line2")
	public String getPackageLine2() {
		return packageLine2;
	}

	public void setPackageLine2(String packageLine2) {
		this.packageLine2 = packageLine2;
	}

	@Column(name="package_line3")
	public String getPackageLine3() {
		return packageLine3;
	}

	public void setPackageLine3(String packageLine3) {
		this.packageLine3 = packageLine3;
	}

	@Column(name="package_line4")
	public String getPackageLine4() {
		return packageLine4;
	}

	public void setPackageLine4(String packageLine4) {
		this.packageLine4 = packageLine4;
	}

	@Column(name="package_line5")
	public String getPackageLine5() {
		return packageLine5;
	}

	public void setPackageLine5(String packageLine5) {
		this.packageLine5 = packageLine5;
	}

	@Column(name="package_line6")
	public String getPackageLine6() {
		return packageLine6;
	}

	public void setPackageLine6(String packageLine6) {
		this.packageLine6 = packageLine6;
	}

	@Transient
	public String getLastUpdatedStr() {
		if(lastUpdated != null){
			lastUpdatedStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(getLastUpdated());
		}
		return lastUpdatedStr;
	}

	public void setLastUpdatedStr(String lastUpdatedStr) {
		this.lastUpdatedStr = lastUpdatedStr;
	}
	
}
