package com.rtw.tracker.datas;

import java.sql.Time;
import java.util.Date;

import javax.persistence.Transient;

import org.hibernate.Hibernate;

import com.rtw.tmat.utils.Util;

/**
 * POJO class for PO's
 * @author dthiyagarajan
 *
 */
public class PurchaseOrders {

	private Integer id;
	private Integer customerId;
	private String customerName;
	private String customerType;
	private Date created;
	private Double poTotal;
	private Integer ticketQty;
	private String shippingType;
	private String status;
	private String csr;
	private Date lastUpdated;
	private Integer poAged;
	private String createdBy;
	private Boolean isEmailed;
	private String consignmentPoNo;
	private String transactionOffice;
	private String trackingNo;
	private String shippingNotes;
	private String externalNotes;
	private String internalNotes;
	private Integer eventTicketQty;
	private Double eventTicketCost;
	
	private Integer eventId;
	private String eventName;
	private Date eventDate;
	private Time eventTime;
	private String eventDateStr;
	private String eventTimeStr;
	private String createdDateStr;
	private String lastUpdatedStr;	
	private String productType;
	private String createDateStr; //In Customers Tab
	private String purchaseOrderType;
	private String posProductType;
	private Integer posPOId;
	
	private Boolean fedexLabelCreated;
	private Integer brokerId;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCustomerId(){
		return customerId;
	}
	
	public void setCustomerId(Integer customerId){
		this.customerId = customerId;
	}
	
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	
	public Double getPoTotal() {
		return poTotal;
	}
	
	public void setPoTotal(Double poTotal) {
		this.poTotal = poTotal;
	}
	public String getShippingType() {
		return shippingType;
	}
	public void setShippingType(String shippingType) {
		this.shippingType = shippingType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getTicketQty() {
		return ticketQty;
	}

	public void setTicketQty(Integer ticketQty) {
		this.ticketQty = ticketQty;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Integer getPoAged() {
		return poAged;
	}

	public void setPoAged(Integer poAged) {
		this.poAged = poAged;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Boolean getIsEmailed() {
		return isEmailed;
	}

	public void setIsEmailed(Boolean isEmailed) {
		this.isEmailed = isEmailed;
	}

	public String getTransactionOffice() {
		return transactionOffice;
	}

	public void setTransactionOffice(String transactionOffice) {
		this.transactionOffice = transactionOffice;
	}

	public String getTrackingNo() {
		return trackingNo;
	}

	public void setTrackingNo(String trackingNo) {
		this.trackingNo = trackingNo;
	}

	public String getShippingNotes() {
		return shippingNotes;
	}

	public void setShippingNotes(String shippingNotes) {
		this.shippingNotes = shippingNotes;
	}

	public String getExternalNotes() {
		return externalNotes;
	}

	public void setExternalNotes(String externalNotes) {
		this.externalNotes = externalNotes;
	}

	public String getInternalNotes() {
		return internalNotes;
	}

	public void setInternalNotes(String internalNotes) {
		this.internalNotes = internalNotes;
	}

	public String getConsignmentPoNo() {
		return consignmentPoNo;
	}

	public void setConsignmentPoNo(String consignmentPoNo) {
		this.consignmentPoNo = consignmentPoNo;
	}

	public String getCsr() {
		return csr;
	}

	public void setCsr(String csr) {
		this.csr = csr;
	}
	
	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public Time getEventTime() {
		return eventTime;
	}

	public void setEventTime(Time eventTime) {
		this.eventTime = eventTime;
	}

	public String getEventDateStr() {
		return Util.formatDateToMonthDateYear(eventDate);
	}

	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}

	public String getEventTimeStr() {
		return Util.formatTimeToHourMinutes(eventTime);
	}

	public void setEventTimeStr(String eventTimeStr) {
		this.eventTimeStr = eventTimeStr;
	}
	@Transient
	public String getCreatedDateStr() {
		return Util.formatDateToMonthDateYear(created);
	}

	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	
	public String getLastUpdatedStr() {
		return Util.formatDateToMonthDateYear(lastUpdated);
	}

	public void setLastUpdatedStr(String lastUpdatedStr) {
		this.lastUpdatedStr = lastUpdatedStr;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}
	
	public String getCreateDateStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(created);
	}
	public void setCreateDateStr(String createDateStr) {
		this.createDateStr = createDateStr;
	}

	public String getPurchaseOrderType() {
		return purchaseOrderType;
	}

	public void setPurchaseOrderType(String purchaseOrderType) {
		this.purchaseOrderType = purchaseOrderType;
	}

	public Boolean getFedexLabelCreated() {
		return fedexLabelCreated;
	}

	public void setFedexLabelCreated(Boolean fedexLabelCreated) {
		this.fedexLabelCreated = fedexLabelCreated;
	}

	public Integer getEventTicketQty() {
		return eventTicketQty;
	}

	public void setEventTicketQty(Integer eventTicketQty) {
		this.eventTicketQty = eventTicketQty;
	}

	public Double getEventTicketCost() {
		if(eventTicketCost!=null){
			try {
				eventTicketCost = Util.getRoundedValue(eventTicketCost);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return eventTicketCost;
	}

	public void setEventTicketCost(Double eventTicketCost) {
		this.eventTicketCost = eventTicketCost;
	}

	public String getPosProductType() {
		return posProductType;
	}

	public void setPosProductType(String posProductType) {
		this.posProductType = posProductType;
	}

	public Integer getPosPOId() {
		return posPOId;
	}

	public void setPosPOId(Integer posPOId) {
		this.posPOId = posPOId;
	}

	public Integer getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}
	
	
	
}