package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;

@Entity
@Table(name="polling_video_inventory")
public class PollingVideoInventory implements Serializable {

	
	private Integer id;
	private String title;
	private String description;
	private String category;
	private String videoUrl;
	private Boolean status;
	private String createdBy;
	private Date createdDate;
	private Boolean isDefault; 
	private String createdDateStr;
	private Integer categoryId;
	private Integer isDeleted;
	private Integer sponsorId;
	private String sponsorName;
	private Integer custVideoId;
	private String posterUrl;
	private Boolean hideMedia;
	private Date hideDate;
	private String hideBy;
	private String hideReason;
	private Boolean bolckStatus;
	private Date blockDate;
	private String blockBy;
	private String blockReason;
	private Integer fanclubVideoId;


	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}
	

	@Column(name="category_id")
	public Integer getCategoryId() {
		return categoryId;
	}


	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	@Column(name="title")
	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}
	
	
	@Column(name="video_description")
	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	@Column(name="category")
	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}

	@Column(name="video_url")
	public String getVideoUrl() {
		return videoUrl;
	}


	public void setVideoUrl(String videoUrl) {
		this.videoUrl = videoUrl;
	}
	
	@Column(name = "fanclub_video_id")
	public Integer getFanclubVideoId() {
		return fanclubVideoId;
	}

	public void setFanclubVideoId(Integer fanclubVideoId) {
		this.fanclubVideoId = fanclubVideoId;
	}

	@Column(name="status")
	public Boolean getStatus() {
		return status;
	}


	public void setStatus(Boolean status) {
		this.status = status;
	}

	@Column(name="created_by")
	public String getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}


	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Transient
	public String getCreatedDateStr() {
		if(createdDate!=null){
			this.createdDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(createdDate);
		}
		return createdDateStr;
	}


	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}

	@Column(name="is_default")
	public Boolean getIsDefault() {
		return isDefault;
	}


	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}


	@Column(name="is_deleted")
	public Integer getIsDeleted() {
		return isDeleted;
	}


	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}

	@Column(name="sponsor_id")
	public Integer getSponsorId() {
		return sponsorId;
	}

	
	public void setSponsorId(Integer sponsorId) {
		this.sponsorId = sponsorId;
	}
	
	
	@Column(name="cust_video_id")
	public Integer getCustVideoId() {
		return custVideoId;
	}


	public void setCustVideoId(Integer custVideoId) {
		this.custVideoId = custVideoId;
	}
	
	

	@Column(name="poster_url")
	public String getPosterUrl() {
		return posterUrl;
	}


	public void setPosterUrl(String posterUrl) {
		this.posterUrl = posterUrl;
	}
	
	
	
	@Column(name="hide_media")
	public Boolean getHideMedia() {
		return hideMedia;
	}


	public void setHideMedia(Boolean hideMedia) {
		this.hideMedia = hideMedia;
	}

	
	@Column(name="hide_date")
	public Date getHideDate() {
		return hideDate;
	}


	public void setHideDate(Date hideDate) {
		this.hideDate = hideDate;
	}


	@Column(name="hide_by")
	public String getHideBy() {
		return hideBy;
	}


	public void setHideBy(String hideBy) {
		this.hideBy = hideBy;
	}


	@Column(name="hide_reason")
	public String getHideReason() {
		return hideReason;
	}


	public void setHideReason(String hideReason) {
		this.hideReason = hideReason;
	}


	@Column(name="block_status")
	public Boolean getBolckStatus() {
		return bolckStatus;
	}


	public void setBolckStatus(Boolean bolckStatus) {
		this.bolckStatus = bolckStatus;
	}


	@Column(name="block_date")
	public Date getBlockDate() {
		return blockDate;
	}


	public void setBlockDate(Date blockDate) {
		this.blockDate = blockDate;
	}


	@Column(name="block_by")
	public String getBlockBy() {
		return blockBy;
	}


	public void setBlockBy(String blockBy) {
		this.blockBy = blockBy;
	}


	@Column(name="block_reason")
	public String getBlockReason() {
		return blockReason;
	}


	public void setBlockReason(String blockReason) {
		this.blockReason = blockReason;
	}


	@Transient
	public String getSponsorName() {
		return sponsorName;
	}


	public void setSponsorName(String sponsorName) {
		this.sponsorName = sponsorName;
	}
	
}
