package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "customer_order_paypal_refund")
public class CustomerOrderPaypalRefund implements Serializable{

	private Integer id;
	private Integer orderId;
	private Double amount;
	private String refundTransactionId;
	private String refundSaleId;
	private String status;
	private Double revertedUsedRewardPoints;
	private Double revertedEarnedRewardPoints;
	private Date createdTime;
	private Date updatedTime;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "order_id")
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	
	@Column(name = "amount")
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	@Column(name = "refund_transaction_id")
	public String getRefundTransactionId() {
		return refundTransactionId;
	}
	public void setRefundTransactionId(String refundTransactionId) {
		this.refundTransactionId = refundTransactionId;
	}
	
	@Column(name = "refund_sale_id")
	public String getRefundSaleId() {
		return refundSaleId;
	}
	public void setRefundSaleId(String refundSaleId) {
		this.refundSaleId = refundSaleId;
	}
	
	@Column(name = "status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name = "reverted_used_reward_points")
	public Double getRevertedUsedRewardPoints() {
		return revertedUsedRewardPoints;
	}
	public void setRevertedUsedRewardPoints(Double revertedUsedRewardPoints) {
		this.revertedUsedRewardPoints = revertedUsedRewardPoints;
	}
	
	@Column(name = "reverted_earned_reward_points")
	public Double getRevertedEarnedRewardPoints() {
		return revertedEarnedRewardPoints;
	}
	public void setRevertedEarnedRewardPoints(Double revertedEarnedRewardPoints) {
		this.revertedEarnedRewardPoints = revertedEarnedRewardPoints;
	}
	
	@Column(name = "create_time")
	public Date getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	
	@Column(name = "update_time")
	public Date getUpdatedTime() {
		return updatedTime;
	}
	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}

	
}
