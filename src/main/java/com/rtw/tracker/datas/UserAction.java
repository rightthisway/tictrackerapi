package com.rtw.tracker.datas;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;

@Entity
@Table(name="tracker_user_action")
public class UserAction implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;
	//private TrackerUser trackerUser;
	private Integer userId;
	private Date timeStamp;
	private String action;
	private String ipAddress;
	private String userName;
	
	private Integer dataId;
	private String message;
	
	private String timeStampStr;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	/*@ManyToOne
	@JoinColumn(name="user_id")
	public TrackerUser getTrackerUser() {
		return trackerUser;
	}
	public void setTrackerUser(TrackerUser trackerUser) {
		this.trackerUser = trackerUser;
	}*/
	
	@Column(name="user_id")
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	@Column(name="username")
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@Column(name="time_stamp")
	public Date getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}
	
	@Column(name="action")
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	
	@Column(name="ip_address")
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	@Column(name="data_id")	
	public Integer getDataId() {
		return dataId;
	}
	public void setDataId(Integer dataId) {
		this.dataId = dataId;
	}
	
	@Column(name="message")
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	@Transient
	public String getTimeStampStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(timeStamp);
	}
	
	public void setTimeStampStr(String timeStampStr) {
		this.timeStampStr = timeStampStr;
	}
}