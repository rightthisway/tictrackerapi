package com.rtw.tracker.datas;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("CrownJewelTicketMap")
public class CrownJewelTicketMap {
	
	private String svgKey;
	private String section;
	private Boolean peackageAllowed;
	//private String packageDetails;
	//private Double packageCostAsDouble;
	//private String packageCost;
	private String colorCode;
	private List<CrownJewelTicket> ticketsMap;
	
	
	public CrownJewelTicketMap(){
		
	}
	
	public CrownJewelTicketMap(CrownJewelCategoryTicket ticket , CrownJewelTeams teams ){
		//setPackageDetails(teams.getPackageNotes());
		//setPeackageAllowed(teams.getPackageApplicable());
		setSection(ticket.getSection());
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public Boolean getPeackageAllowed() {
		if(null == peackageAllowed){
			peackageAllowed=false;
		}
		return peackageAllowed;
	}

	public void setPeackageAllowed(Boolean peackageAllowed) {
		this.peackageAllowed = peackageAllowed;
	}

	/*public String getPackageDetails() {
		if(null == packageDetails ){
			packageDetails="";
		}
		return packageDetails;
	}

	public void setPackageDetails(String packageDetails) {
		this.packageDetails = packageDetails;
	}*/

	public List<CrownJewelTicket> getTicketsMap() {
		return ticketsMap;
	}

	public void setTicketsMap(List<CrownJewelTicket> ticketsMap) {
		this.ticketsMap = ticketsMap;
	}
	
	public String getColorCode() {
		return colorCode;
	}

	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}

	public String getSvgKey() {
		return svgKey;
	}

	public void setSvgKey(String svgKey) {
		this.svgKey = svgKey;
	}

	/*public Double getPackageCostAsDouble() {
		if(packageCostAsDouble == null){
			packageCostAsDouble = 0.00;
		}
		return packageCostAsDouble;
	}

	public void setPackageCostAsDouble(Double packageCostAsDouble) {
		this.packageCostAsDouble = packageCostAsDouble;
	}

	public String getPackageCost() {
		if(packageCost == null || packageCost.isEmpty()){
			packageCost = "0.00";
		}
		return packageCost;
	}

	public void setPackageCost(String packageCost) {
		this.packageCost = packageCost;
	}*/

}
