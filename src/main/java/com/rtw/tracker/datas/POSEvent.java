package com.rtw.tracker.datas;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;

@Entity
@Table(name="pos_event")
public class POSEvent implements Serializable{

	private Integer eventId;
	private String eventName;
	private Date eventDateTime;
	private Date createDate;
	private Integer venueId;
	private String productType;
	private Integer productId;
	
	private String eventDateStr;
	private String eventDateTimeStr;
	private String createDateStr;
	
	public POSEvent(){
		
	}

	@Id
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	@Column(name="event_name")
	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	@Column(name="event_datetime")
	public Date getEventDateTime() {
		return eventDateTime;
	}

	public void setEventDateTime(Date eventDateTime) {
		this.eventDateTime = eventDateTime;
	}

	@Column(name="create_date")
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Column(name="venue_id")
	public Integer getVenueId() {
		return venueId;
	}

	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}

	@Column(name="product_type")
	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	@Column(name="product_id")
	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	@Transient
	public String getEventDateStr() {
		if(getEventDateTime()==null){
			return "TBD";
		}
		return Util.formatDateToMonthDateYear(getEventDateTime());
	}

	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}

	@Transient
	public String getEventDateTimeStr() {
		if(getEventDateTime()==null){
			return "TBD";
		}
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(getEventDateTime());
	}
	
	public void setEventDateTimeStr(String eventDateTimeStr) {
		this.eventDateTimeStr = eventDateTimeStr;
	}
	
	@Transient
	public String getCreateDateStr() {
		return Util.formatDateToMonthDateYear(getCreateDate());
	}

	public void setCreateDateStr(String createDateStr) {
		this.createDateStr = createDateStr;
	}
	
}
