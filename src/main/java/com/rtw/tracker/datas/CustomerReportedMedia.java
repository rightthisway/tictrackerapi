package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;

@Entity
@Table(name="customer_reported_abuse_on_media")
public class CustomerReportedMedia implements Serializable{
	
	private Integer id;
	private Integer customerId;
	private Integer mediaId;
	private Integer commentId;
	private Integer abuseId;
	private Date createdDate;
	
	
	private  String createdDateStr;
	private String blockDateStr;
	private String hideDateStr;
	private String reportedByUserId;
	private String reportedByEmail;
	private String reportedByPhone;
	
	private String userId;
	private String email;
	private String phone;
	
	private String title;
	private String description;
	private String mediaUrl;
	private String imageUrl;
	private Boolean blockStatus;
	private String blockBy;
	private Date blockDate;
	private String hideBy;
	private Boolean hideStatus;
	private Date hideDate;
	
	
	private String abuseType;
	
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Column(name="media_id")
	public Integer getMediaId() {
		return mediaId;
	}
	public void setMediaId(Integer mediaId) {
		this.mediaId = mediaId;
	}
	
	@Column(name="comment_id")
	public Integer getCommentId() {
		return commentId;
	}
	public void setCommentId(Integer commentId) {
		this.commentId = commentId;
	}
	
	@Column(name="abuse_id")
	public Integer getAbuseId() {
		return abuseId;
	}
	public void setAbuseId(Integer abuseId) {
		this.abuseId = abuseId;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Transient
	public String getReportedByUserId() {
		return reportedByUserId;
	}
	public void setReportedByUserId(String reportedByUserId) {
		this.reportedByUserId = reportedByUserId;
	}
	
	@Transient
	public String getReportedByEmail() {
		return reportedByEmail;
	}
	public void setReportedByEmail(String reportedByEmail) {
		this.reportedByEmail = reportedByEmail;
	}
	
	@Transient
	public String getReportedByPhone() {
		return reportedByPhone;
	}
	public void setReportedByPhone(String reportedByPhone) {
		this.reportedByPhone = reportedByPhone;
	}
	
	@Transient
	public String getCreatedDateStr() {
		if(createdDate!=null){
			this.createdDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(createdDate);
		}
		return createdDateStr;
	}
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	
	@Transient
	public String getBlockDateStr() {
		if(blockDate!=null){
			blockDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(blockDate);
		}
		return blockDateStr;
	}
	public void setBlockDateStr(String blockDateStr) {
		this.blockDateStr = blockDateStr;
	}
	
	@Transient
	public String getHideDateStr() {
		if(hideDate!=null){
			hideDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(hideDate);
		}
		return hideDateStr;
	}
	public void setHideDateStr(String hideDateStr) {
		this.hideDateStr = hideDateStr;
	}
	
	@Transient
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	@Transient
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Transient
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@Transient
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	@Transient
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Transient
	public String getMediaUrl() {
		return mediaUrl;
	}
	public void setMediaUrl(String mediaUrl) {
		this.mediaUrl = mediaUrl;
	}
	
	@Transient
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	@Transient
	public Boolean getBlockStatus() {
		return blockStatus;
	}
	public void setBlockStatus(Boolean blockStatus) {
		this.blockStatus = blockStatus;
	}
	
	@Transient
	public String getBlockBy() {
		return blockBy;
	}
	public void setBlockBy(String blockBy) {
		this.blockBy = blockBy;
	}
	
	@Transient
	public Date getBlockDate() {
		return blockDate;
	}
	public void setBlockDate(Date blockDate) {
		this.blockDate = blockDate;
	}
	
	@Transient
	public String getHideBy() {
		return hideBy;
	}
	public void setHideBy(String hideBy) {
		this.hideBy = hideBy;
	}
	
	@Transient
	public Boolean getHideStatus() {
		return hideStatus;
	}
	public void setHideStatus(Boolean hideStatus) {
		this.hideStatus = hideStatus;
	}
	
	@Transient
	public Date getHideDate() {
		return hideDate;
	}
	public void setHideDate(Date hideDate) {
		this.hideDate = hideDate;
	}
	
	@Transient
	public String getAbuseType() {
		return abuseType;
	}
	public void setAbuseType(String abuseType) {
		this.abuseType = abuseType;
	}
	
}
