package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.enums.ContestJackpotType;

@Entity
@Table(name="contest")
public class Contests implements Serializable{

	private Integer id;
	private String contestName;
	private String extendedName;
	private Date startDateTime;
	//private Integer partipantCount;
	//private Integer winnerCount;
	//private Integer ticketWinnerCount;
	//private Integer pointWinnerCount;
	private Integer ticketWinnerThreshhold;
	private Integer freeTicketPerWinner;
	private String contestMode;
	private String contestPassword;
	private Integer questionSize;
	//private Double pointsPerWinner;
	private Double rewardPoints;
	private String status;
	private String type;
	private Date createdDate;
	private Date updatedDate;
	private Date actualStartTime;
	private Date actualEndTime;
	private String createdBy;
	private String updatedBy;
	private Boolean isCustomerStatUpdated;
	
	private String startDateTimeStr;
	private String createdDateTimeStr;
	private String updatedDateTimeStr;
	private String minutes;
	private String hours;
	private String startDateStr;
	private String startTimeStr;
	//private String rewardPerQuestionStr;
	private Integer preContestId;
	
	private Double singleTicketPrice;
	private String zone;		
	private String promotionalCode;
	private Double discountPercentage;
	private Integer rtfPromoOfferId;
	private Integer lastQuestionNo;
	private String lastAction;
	
	private Integer promoRefId; 
	private String promoRefType; 
	private String promoRefName;
	private Date promoExpiryDate;
	private String contestCategory;
	private Boolean isTestContest;
	private Integer participantStars;
	private Integer referralStars;
	private Double referralRewards;
	private Integer participantLives;
	private Double participantRewards;
	private Integer participantPoints;
	private Integer referralPoints;
	private String questionRewardType;
	
	private Integer mjpWinnerPerQuestion;
	private Integer giftCardId;
	private Integer giftCardPerWinner;
	private ContestJackpotType contestJackpotType;
	
	private Integer giftCardQty;
	private Double giftCardAmt;
	private String giftCardName;
	
	
	private String extName;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="contest_name")
	public String getContestName() {
		return contestName;
	}
	public void setContestName(String contestName) {
		this.contestName = contestName;
	}
	
	@Column(name="extended_name")
	public String getExtendedName() {
		return extendedName;
	}
	public void setExtendedName(String extendedName) {
		this.extendedName = extendedName;
	}

	@Column(name="contest_start_datetime")
	public Date getStartDateTime() {
		return startDateTime;
	}
	public void setStartDateTime(Date startDateTime) {
		this.startDateTime = startDateTime;
	}

	
	@Column(name="created_datetime")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	

	@Column(name="updated_datetime")
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@Column(name="actual_start_time")
	public Date getActualStartTime() {
		return actualStartTime;
	}
	public void setActualStartTime(Date actualStartTime) {
		this.actualStartTime = actualStartTime;
	}
	
	@Column(name="actual_end_time")
	public Date getActualEndTime() {
		return actualEndTime;
	}
	public void setActualEndTime(Date actualEndTime) {
		this.actualEndTime = actualEndTime;
	}
	
	@Column(name="eligible_free_ticket_winners")
	public Integer getTicketWinnerThreshhold() {
		return ticketWinnerThreshhold;
	}
	public void setTicketWinnerThreshhold(Integer ticketWinnerThreshhold) {
		this.ticketWinnerThreshhold = ticketWinnerThreshhold;
	}
	
	@Column(name="free_tickets_per_winner")
	public Integer getFreeTicketPerWinner() {
		return freeTicketPerWinner;
	}
	public void setFreeTicketPerWinner(Integer freeTicketPerWinner) {
		this.freeTicketPerWinner = freeTicketPerWinner;
	}
	
	
	@Column(name="contest_mode")
	public String getContestMode() {
		return contestMode;
	}
	public void setContestMode(String contestMode) {
		this.contestMode = contestMode;
	}
	
	@Column(name="no_of_questions")
	public Integer getQuestionSize() {
		return questionSize;
	}
	public void setQuestionSize(Integer questionSize) {
		this.questionSize = questionSize;
	}
	
	
	@Column(name="contest_password")
	public String getContestPassword() {
		return contestPassword;
	}
	public void setContestPassword(String contestPassword) {
		this.contestPassword = contestPassword;
	}
	
	@Column(name="total_rewards")
	public Double getRewardPoints() {
		return rewardPoints;
	}
	public void setRewardPoints(Double rewardPoints) {
		this.rewardPoints = rewardPoints;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name="updated_by")
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column(name="contest_type")
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	@Column(name="is_customer_stats_updated")
	public Boolean getIsCustomerStatUpdated() {
		return isCustomerStatUpdated;
	}
	public void setIsCustomerStatUpdated(Boolean isCustomerStatUpdated) {
		this.isCustomerStatUpdated = isCustomerStatUpdated;
	}
	
	@Column(name="zone")
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
		
	@Column(name="promotional_code")
	public String getPromotionalCode() {
		return promotionalCode;
	}
	public void setPromotionalCode(String promotionalCode) {
		this.promotionalCode = promotionalCode;
	}
	
	@Column(name="discount_percentage")
	public Double getDiscountPercentage() {
		return discountPercentage;
	}
	public void setDiscountPercentage(Double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}
	
	@Column(name="promo_offer_id")
	public Integer getRtfPromoOfferId() {
		return rtfPromoOfferId;
	}
	public void setRtfPromoOfferId(Integer rtfPromoOfferId) {
		this.rtfPromoOfferId = rtfPromoOfferId;
	}
	
	@Column(name="last_question_no")
	public Integer getLastQuestionNo() {
		return lastQuestionNo;
	}
	public void setLastQuestionNo(Integer lastQuestionNo) {
		this.lastQuestionNo = lastQuestionNo;
	}
	
	@Column(name="last_action")
	public String getLastAction() {
		return lastAction;
	}
	public void setLastAction(String lastAction) {
		this.lastAction = lastAction;
	}
	
	@Column(name="promo_ref_id")
	public Integer getPromoRefId() {
		return promoRefId;
	}
	public void setPromoRefId(Integer promoRefId) {
		this.promoRefId = promoRefId;
	}
	
	@Column(name="promo_ref_type")
	public String getPromoRefType() {
		return promoRefType;
	}
	public void setPromoRefType(String promoRefType) {
		this.promoRefType = promoRefType;
	}
	
	@Column(name="promo_ref_name")
	public String getPromoRefName() {
		return promoRefName;
	}
	public void setPromoRefName(String promoRefName) {
		this.promoRefName = promoRefName;
	}
	
	@Column(name="promo_expiry_date")
	public Date getPromoExpiryDate() {
		return promoExpiryDate;
	}
	public void setPromoExpiryDate(Date promoExpiryDate) {
		this.promoExpiryDate = promoExpiryDate;
	}
	
	@Column(name="contest_category")
	public String getContestCategory() {
		return contestCategory;
	}
	public void setContestCategory(String contestCategory) {
		this.contestCategory = contestCategory;
	}
	
	
	@Column(name="is_test_contest")
	public Boolean getIsTestContest() {
		return isTestContest;
	}
	public void setIsTestContest(Boolean isTestContest) {
		this.isTestContest = isTestContest;
	}
	
	@Column(name="participant_star")
	public Integer getParticipantStars() {
		return participantStars;
	}
	public void setParticipantStars(Integer participantStars) {
		this.participantStars = participantStars;
	}
	
	@Column(name="referral_star")
	public Integer getReferralStars() {
		return referralStars;
	}
	public void setReferralStars(Integer referralStars) {
		this.referralStars = referralStars;
	}
	
	@Column(name="referral_rewards")
	public Double getReferralRewards() {
		return referralRewards;
	}
	public void setReferralRewards(Double referralRewards) {
		this.referralRewards = referralRewards;
	}
	
	@Column(name="participant_lives")
	public Integer getParticipantLives() {
		return participantLives;
	}
	public void setParticipantLives(Integer participantLives) {
		this.participantLives = participantLives;
	}
	
	@Column(name="participant_rewards")
	public Double getParticipantRewards() {
		return participantRewards;
	}
	public void setParticipantRewards(Double participantRewards) {
		this.participantRewards = participantRewards;
	}
	
	
	@Column(name="question_reward_type")
	public String getQuestionRewardType() {
		return questionRewardType;
	}
	public void setQuestionRewardType(String questionRewardType) {
		this.questionRewardType = questionRewardType;
	}
	
	
	@Column(name="no_of_winner_per_question")
	public Integer getMjpWinnerPerQuestion() {
		return mjpWinnerPerQuestion;
	}
	public void setMjpWinnerPerQuestion(Integer mjpWinnerPerQuestion) {
		this.mjpWinnerPerQuestion = mjpWinnerPerQuestion;
	}
	
	@Column(name="gift_card_value_id")
	public Integer getGiftCardId() {
		return giftCardId;
	}
	public void setGiftCardId(Integer giftCardId) {
		this.giftCardId = giftCardId;
	}
	
	@Column(name="gift_card_per_winner")
	public Integer getGiftCardPerWinner() {
		return giftCardPerWinner;
	}
	public void setGiftCardPerWinner(Integer giftCardPerWinner) {
		this.giftCardPerWinner = giftCardPerWinner;
	}
	
	@Column(name="participants_rtf_points")
	public Integer getParticipantPoints() {
		return participantPoints;
	}
	public void setParticipantPoints(Integer participantPoints) {
		this.participantPoints = participantPoints;
	}
	
	@Column(name="referral_rtf_points")
	public Integer getReferralPoints() {
		return referralPoints;
	}
	public void setReferralPoints(Integer referralPoints) {
		this.referralPoints = referralPoints;
	}
	@Transient
	public Integer getGiftCardQty() {
		return giftCardQty;
	}
	public void setGiftCardQty(Integer giftCardQty) {
		this.giftCardQty = giftCardQty;
	}
	
	@Transient
	public Double getGiftCardAmt() {
		return giftCardAmt;
	}
	public void setGiftCardAmt(Double giftCardAmt) {
		this.giftCardAmt = giftCardAmt;
	}
	
	@Transient
	public String getGiftCardName() {
		return giftCardName;
	}
	public void setGiftCardName(String giftCardName) {
		this.giftCardName = giftCardName;
	}
	@Transient
	public String getStartDateTimeStr() {
		this.startDateTimeStr  = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(startDateTime);
		return startDateTimeStr;
	}
	
	public void setStartDateTimeStr(String startDateTimeStr) {
		this.startDateTimeStr = startDateTimeStr;
	}
	
	
	@Transient
	public String getStartDateStr() {
		this.startDateStr = Util.formatDateToMonthDateYear(startDateTime);
		return startDateStr;
	}
	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}
	
	@Transient
	public String getStartTimeStr() {
		if(startDateTime!=null){
			startTimeStr = Util.formatTimeToHourMinutes(startDateTime);
		}
		return startTimeStr;
	}
	public void setStartTimeStr(String startTimeStr) {
		this.startTimeStr = startTimeStr;
	}
	
	
	@Transient
	public String getCreatedDateTimeStr() {
		this.createdDateTimeStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(createdDate);
		return createdDateTimeStr;
	}
	public void setCreatedDateTimeStr(String createdDateTimeStr) {
		this.createdDateTimeStr = createdDateTimeStr;
	}

	@Transient
	public String getUpdatedDateTimeStr() {
		this.updatedDateTimeStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(updatedDate);
		return updatedDateTimeStr;
	}
	public void setUpdatedDateTimeStr(String updatedDateTimeStr) {
		this.updatedDateTimeStr = updatedDateTimeStr;
	}
	
	@Transient
	public String getMinutes() {
		return minutes;
	}
	public void setMinutes(String minutes) {
		this.minutes = minutes;
	}
	
	@Transient
	public String getHours() {
		return hours;
	}
	public void setHours(String hours) {
		this.hours = hours;
	}
	
	@Transient
	public String getExtName() {
		if(extendedName!=null && !extendedName.isEmpty()){
			extName = extendedName;
		}else{
			extName = contestName;
		}
		return extName;
	}
	public void setExtName(String extName) {
		this.extName = extName;
	}
	
	
	@Transient
	public Integer getPreContestId() {
		return preContestId;
	}
	public void setPreContestId(Integer preContestId) {
		this.preContestId = preContestId;
	}
	@Column(name="single_tix_price")
	public Double getSingleTicketPrice() {
		return singleTicketPrice;
	}
	public void setSingleTicketPrice(Double singleTicketPrice) {
		this.singleTicketPrice = singleTicketPrice;
	}
	
	@Column(name="contest_jackpot_type")
	@Enumerated(EnumType.STRING)
	public ContestJackpotType getContestJackpotType() {
		return contestJackpotType;
	}
	public void setContestJackpotType(ContestJackpotType contestJackpotType) {
		this.contestJackpotType = contestJackpotType;
	}
	
	
	
}