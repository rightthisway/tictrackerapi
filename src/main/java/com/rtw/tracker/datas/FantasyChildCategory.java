package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.map.annotate.JsonDeserialize;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.rtw.tmat.utils.Util;

@Entity
@Table(name="fantasy_child_category")
public class FantasyChildCategory implements Serializable{
	
	private String name;
	private Integer id;
	private Integer parentId;
	private String parentCategoryName;
	private String lastUpdatedBy;
	@JsonDeserialize(using = com.rtw.tracker.utils.CustomerDateAndTimeDeserialize.class)
	@JsonSerialize(using = com.rtw.tracker.utils.CustomerDateAndTimeSerializer.class)
	private Date lastUpdated;
	private String lastUpdatedStr;
	
	public FantasyChildCategory(){}
	
	/**
	 * @return the id
	 */
	
	@Id
	@GeneratedValue
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * @return the name
	 */
	@Column(name="name")
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Column(name="parent_id")
	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	@Transient
	public String getParentCategoryName() {
		return parentCategoryName;
	}

	public void setParentCategoryName(String parentCategoryName) {
		this.parentCategoryName = parentCategoryName;
	}
	
	@Column(name="last_updated_by")
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	@Transient
	public String getLastUpdatedStr() {
		if(lastUpdated != null){
			lastUpdatedStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(getLastUpdated());
		}
		return lastUpdatedStr;
	}

	public void setLastUpdatedStr(String lastUpdatedStr) {
		this.lastUpdatedStr = lastUpdatedStr;
	}
	
}
