package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;

@Entity
@Table(name="rtf_promotional_earn_life_offers")
public class PromotionalEarnLifeOffer implements Serializable{

	private Integer id;
	private String promoCode;
	private Date startDate;
	private Date endDate;
	private Integer maxLifePerCustomer;
	private Integer maxAccumulateThreshold;
	private Integer curAccumulatedCount;
	private String status; 
	private Date createdDate;
	private Date updatedDate;
	private String createdBy;
	private String updatedBy; 
	
	private String startDateStr;
	private String endDateStr;
	private String startDateEditStr;
	private String endDateEditStr;
	private String createdDateStr;
	private String updatedDateStr;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="promo_code")
	public String getPromoCode() {
		return promoCode;
	}
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}
	
	@Column(name="start_date")
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	@Column(name="end_date")
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	@Column(name="max_life_per_customer")
	public Integer getMaxLifePerCustomer() {
		return maxLifePerCustomer;
	}
	public void setMaxLifePerCustomer(Integer maxLifePerCustomer) {
		this.maxLifePerCustomer = maxLifePerCustomer;
	}
	
	@Column(name="maximum_accumulate_threshold")
	public Integer getMaxAccumulateThreshold() {
		return maxAccumulateThreshold;
	}
	public void setMaxAccumulateThreshold(Integer maxAccumulateThreshold) {
		this.maxAccumulateThreshold = maxAccumulateThreshold;
	}
	
	@Column(name="cur_accumulated_count")
	public Integer getCurAccumulatedCount() {
		return curAccumulatedCount;
	}
	public void setCurAccumulatedCount(Integer curAccumulatedCount) {
		this.curAccumulatedCount = curAccumulatedCount;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="modified_time")
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@Column(name="created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column(name="modified_by")
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Transient
	public String getStartDateStr() {
		if(startDate!=null){
			startDateStr = Util.formatDateTimeToMonthDateYearAndHourMinute(startDate);
		}
		return startDateStr;
	}
	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}
	
	@Transient
	public String getEndDateStr() {
		if(endDate!=null){
			endDateStr = Util.formatDateTimeToMonthDateYearAndHourMinute(endDate);
		}
		return endDateStr;
	}
	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}
	
	@Transient
	public String getCreatedDateStr() {
		if(createdDate!=null){
			createdDateStr = Util.formatDateTimeToMonthDateYearAndHourMinute(createdDate);
		}
		return createdDateStr;
	}
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	
	@Transient
	public String getUpdatedDateStr() {
		if(updatedDate!=null){
			updatedDateStr = Util.formatDateTimeToMonthDateYearAndHourMinute(updatedDate);
			
		}
		return updatedDateStr;
	}
	public void setUpdatedDateStr(String updatedDateStr) {
		this.updatedDateStr = updatedDateStr;
	}
	
	@Transient
	public String getStartDateEditStr() {
		if(startDate!=null){
			startDateEditStr = Util.formatDateToMonthDateYear(startDate);
		}
		return startDateEditStr;
	}
	public void setStartDateEditStr(String startDateEditStr) {
		this.startDateEditStr = startDateEditStr;
	}
	
	@Transient
	public String getEndDateEditStr() {
		if(endDate!=null){
			endDateEditStr = Util.formatDateToMonthDateYear(endDate);
		}
		return endDateEditStr;
	}
	public void setEndDateEditStr(String endDateEditStr) {
		this.endDateEditStr = endDateEditStr;
	}
	
	
	
	
	
	
	
	

}