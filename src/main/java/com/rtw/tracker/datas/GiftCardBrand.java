package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;
import com.rtw.tracker.aws.AWSFileService;

@Entity
@Table(name = "gift_card_brand")
public class GiftCardBrand  implements Serializable{

	private Integer id;
	private String name;
	private String description;
	//private Integer quantity;
	private String status;
	private String outOfStockFileName;
	private String fileName;
	private Boolean displayOutOfStock;
	private String createdBy;
	private String updatedBy;
	private Date  createdDate;
	private Date updatedDate;
	private String imageUrl;
	private String outOfStockImageUrl;
	
	private String createdDateStr;
	private String updatedDateStr;
	
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	/*@Column(name = "qty_limit")
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}*/
	
	@Column(name = "status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name = "out_of_sotck_image_url")
	public String getOutOfStockFileName() {
		return this.outOfStockFileName;
	}
	public void setOutOfStockFileName(String outOfStockFileName) {
		this.outOfStockFileName = outOfStockFileName;
	}
	
	@Column(name = "image_url")
	public String getFileName() {
		return this.fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column(name = "updated_by")
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column(name = "created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name = "updated_date")
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@Column(name = "diplay_out_of_stock")
	public Boolean getDisplayOutOfStock() {
		return displayOutOfStock;
	}
	public void setDisplayOutOfStock(Boolean displayOutOfStock) {
		this.displayOutOfStock = displayOutOfStock;
	}
	
	@Transient
	public String getImageUrl() {
		this.imageUrl = AWSFileService.AWS_BASE_URL+AWSFileService.GIFTCARD_BRAND_FOLDER+"/"+this.fileName;
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	@Transient
	public String getOutOfStockImageUrl() {
		this.outOfStockImageUrl = AWSFileService.AWS_BASE_URL+AWSFileService.GIFTCARD_BRAND_FOLDER+"/"+this.outOfStockFileName;
		return outOfStockImageUrl;
	}
	public void setOutOfStockImageUrl(String outOfStockImageUrl) {
		this.outOfStockImageUrl = outOfStockImageUrl;
	}
	@Transient
	public String getCreatedDateStr() {
		if(createdDate!=null){
			this.createdDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(createdDate);
		}
		return createdDateStr;
	}
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	
	@Transient
	public String getUpdatedDateStr() {
		if(updatedDate!=null){
			this.updatedDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(updatedDate);
		}
		return updatedDateStr;
	}
	
	public void setUpdatedDateStr(String updatedDateStr) {
		this.updatedDateStr = updatedDateStr;
	}
	
}
