package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="affiliates_settings")
public class AffiliateSetting implements Serializable{

	private Integer id;
	private Integer userId;
	private Double cashDiscount;
	private Double customerDiscount;
	private Double phoneCashDiscount;
	private Double phoneCustomerDiscount;
	private String status;
	private Boolean isRepeatBusiness;
	private Boolean isEarnRewardPoints;
	private Date lastUpdated;
	private String updatedBy;
	
	private String promoCode;
	private String fromDate;
	private String toDate;
	private String repeatBusiness;
	private String earnRewardPoints;
	
	@Id
	@GeneratedValue
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="user_id")
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	@Column(name="affiliates_discount")
	public Double getCashDiscount() {
		return cashDiscount;
	}
	public void setCashDiscount(Double cashDiscount) {
		this.cashDiscount = cashDiscount;
	}
	
	@Column(name="customer_discount")
	public Double getCustomerDiscount() {
		return customerDiscount;
	}
	public void setCustomerDiscount(Double customerDiscount) {
		this.customerDiscount = customerDiscount;
	}
	
	@Column(name="affiliates_status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="affiliates_repeat_business")
	public Boolean getIsRepeatBusiness() {
		return isRepeatBusiness;
	}
	public void setIsRepeatBusiness(Boolean isRepeatBusiness) {
		this.isRepeatBusiness = isRepeatBusiness;
	}
	
	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Column(name="updated_by")
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column(name="phone_order_cash_discount")
	public Double getPhoneCashDiscount() {
		return phoneCashDiscount;
	}
	public void setPhoneCashDiscount(Double phoneCashDiscount) {
		this.phoneCashDiscount = phoneCashDiscount;
	}
	
	@Column(name="phone_order_customer_discount")
	public Double getPhoneCustomerDiscount() {
		return phoneCustomerDiscount;
	}
	public void setPhoneCustomerDiscount(Double phoneCustomerDiscount) {
		this.phoneCustomerDiscount = phoneCustomerDiscount;
	}
	
	@Column(name="is_give_reward_point")
	public Boolean getIsEarnRewardPoints() {
		return isEarnRewardPoints;
	}
	public void setIsEarnRewardPoints(Boolean isEarnRewardPoints) {
		this.isEarnRewardPoints = isEarnRewardPoints;
	}
	
	@Transient
	public String getPromoCode() {
		return promoCode;
	}
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}
	
	@Transient
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	
	@Transient
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	
	@Transient
	public String getRepeatBusiness() {
		return repeatBusiness;
	}
	public void setRepeatBusiness(String repeatBusiness) {
		this.repeatBusiness = repeatBusiness;
	}
	
	@Transient
	public String getEarnRewardPoints() {
		return earnRewardPoints;
	}
	public void setEarnRewardPoints(String earnRewardPoints) {
		this.earnRewardPoints = earnRewardPoints;
	}
	
	
}
