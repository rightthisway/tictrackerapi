package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;


@Entity
@Table(name="stripe_disputes")
public class StripeDispute implements Serializable{

	@Id
	@GeneratedValue
	@Column(name="id")
	private String id;
	@Column(name="dispute_id")
	private String disputeId;
	@Column(name="amount")
	private Double amount;
	@Column(name="change_id")
	private String charge;
	@Column(name="created_date")
	private Date created;
	@Transient
	private String currency;
	@Column(name="is_refundable")
	private Boolean isChargeRefundable;
	@Column(name="reason")
	private String reason;
	@Column(name="status")
	private String status;
	@Column(name="customer_ip")
	private String customerPurchaseIp;
	@Transient
	private String customerEmailAddress;
	@Column(name="duplicate_charge_id")
	private String duplicateChargeId;
	@Column(name="order_id")
	private Integer orderId;
	
	@Transient
	private String createdDateStr;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCharge() {
		return charge;
	}
	public void setCharge(String charge) {
		this.charge = charge;
	}
	
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public Boolean getIsChargeRefundable() {
		return isChargeRefundable;
	}
	public void setIsChargeRefundable(Boolean isChargeRefundable) {
		this.isChargeRefundable = isChargeRefundable;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCustomerPurchaseIp() {
		return customerPurchaseIp;
	}
	public void setCustomerPurchaseIp(String customerPurchaseIp) {
		this.customerPurchaseIp = customerPurchaseIp;
	}
	public String getCustomerEmailAddress() {
		return customerEmailAddress;
	}
	public void setCustomerEmailAddress(String customerEmailAddress) {
		this.customerEmailAddress = customerEmailAddress;
	}
	public String getDuplicateChargeId() {
		return duplicateChargeId;
	}
	public void setDuplicateChargeId(String duplicateChargeId) {
		this.duplicateChargeId = duplicateChargeId;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public String getDisputeId() {
		return disputeId;
	}
	public void setDisputeId(String disputeId) {
		this.disputeId = disputeId;
	}
	public String getCreatedDateStr() {
		if(created!=null){
			createdDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(created);
		}
		return createdDateStr;
	}
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	
}
