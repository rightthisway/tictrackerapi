package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;

@Entity
@Table(name="question_bank")
public class QuestionBank implements Serializable{

	private Integer id;
	private String question;
	private String optionA;
	private String optionB;
	private String optionC;
	//private String optionD;
	private String answer;
	private Date createdDate;
	private Date updatedDate;
	private String createdBy;
	private String updatedBy;
	private Double questionReward;
	private String status;
	private String category;
	private String dificultyLevel;
	private Boolean factChecked;
	
	private String createdDateTimeStr;
	private String updatedDateTimeStr;
	private String rewardPerQuestionStr;
	
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="question")
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	
	@Column(name="option_a")
	public String getOptionA() {
		return optionA;
	}
	public void setOptionA(String optionA) {
		this.optionA = optionA;
	}
	
	@Column(name="option_b")
	public String getOptionB() {
		return optionB;
	}
	public void setOptionB(String optionB) {
		this.optionB = optionB;
	}
	
	@Column(name="option_c")
	public String getOptionC() {
		return optionC;
	}
	public void setOptionC(String optionC) {
		this.optionC = optionC;
	}
	
	/*@Column(name="option_d")
	public String getOptionD() {
		return optionD;
	}
	public void setOptionD(String optionD) {
		this.optionD = optionD;
	}*/
	
	@Column(name="answer")
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	@Column(name="created_datetime")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="updated_datetime")
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@Column(name="created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column(name="updated_by")
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column(name="question_reward")
	public Double getQuestionReward() {
		return questionReward;
	}
	public void setQuestionReward(Double questionReward) {
		this.questionReward = questionReward;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="category")
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
	@Column(name="dificulty_level")
	public String getDificultyLevel() {
		return dificultyLevel;
	}
	public void setDificultyLevel(String level) {
		this.dificultyLevel = level;
	}
	
	@Column(name="fact_checked")
	public Boolean getFactChecked() {
		return factChecked;
	}
	public void setFactChecked(Boolean factChecked) {
		this.factChecked = factChecked;
	}
	
	@Transient
	public String getRewardPerQuestionStr() {
		if(questionReward != null){
			rewardPerQuestionStr = String.valueOf(Util.roundOffDouble(questionReward));
		}
		return rewardPerQuestionStr;
	}
	public void setRewardPerQuestionStr(String rewardPerQuestionStr) {
		this.rewardPerQuestionStr = rewardPerQuestionStr;
	}
	
	@Transient
	public String getCreatedDateTimeStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(createdDate);
	}
	public void setCreatedDateTimeStr(String createdDateTimeStr) {
		this.createdDateTimeStr = createdDateTimeStr;
	}
	
	@Transient
	public String getUpdatedDateTimeStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(updatedDate);
	}
	public void setUpdatedDateTimeStr(String updatedDateTimeStr) {
		this.updatedDateTimeStr = updatedDateTimeStr;
	}
	
	
	
}
