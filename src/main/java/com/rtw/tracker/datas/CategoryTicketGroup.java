package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.map.annotate.JsonDeserialize;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.rtw.tmat.utils.Util;
import com.rtw.tracker.enums.LockedTicketStatus;

@Entity
@Table(name="category_ticket_group")
public class CategoryTicketGroup implements Serializable{
	
		private Integer id;
		private Integer eventId;
		private String section;
		private String row;
		private Integer quantity;
		private Double price;
		private String sectionRange;
		private String rowRange;
		private TicketStatus status;
		private String shippingMethod;
		private Integer shippingMethodId;
		@JsonDeserialize(using = com.rtw.tracker.utils.CustomerDateAndTimeDeserialize.class)
		@JsonSerialize(using = com.rtw.tracker.utils.CustomerDateAndTimeSerializer.class)
		private Date createdDate;
		@JsonDeserialize(using = com.rtw.tracker.utils.CustomerDateAndTimeDeserialize.class)
		@JsonSerialize(using = com.rtw.tracker.utils.CustomerDateAndTimeSerializer.class)
		private Date lastUpdatedDate;
		//private Date lastPriceUpdatedDate;
		private String createdBy;
		private Double soldPrice;
		private Integer soldQuantity;
		private ProductType producttype;
		private Integer invoiceId;
		private Double loyalFanPrice;
		private String actualSection;
		
		private String seatLow;
		private String seatHigh;
		private String externalNotes;
		private String internalNotes;
		private String nearTermDisplayOption;
		private Double facePrice;
		private Double cost;
		private Double wholeSalePrice;
		private Double retailPrice;
		private Boolean broadcast;
		private String marketPlaceNotes;
		private Integer maxShowing;
		private String ticketOnhandStatus;
		private Double taxAmount;
		
		private String svgKey;
		private String colorCode;
		private String rgbColor;
		
		private Integer catgeoryTicketGroupId;
		@JsonDeserialize(using = com.rtw.tracker.utils.CustomerDateAndTimeDeserialize.class)
		@JsonSerialize(using = com.rtw.tracker.utils.CustomerDateAndTimeSerializer.class)
		private Date creationDate;
		private String ipAddress;
		private String platform;
		private LockedTicketStatus lockStatus;
		private Integer brokerId;
		private String orderTotal;
		
		
		@Id
		@Column(name="id")
		@GeneratedValue(strategy=GenerationType.AUTO)
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		
		@Column(name = "event_id")
		public Integer getEventId() {
			return eventId;
		}
		public void setEventId(Integer eventId) {
			this.eventId = eventId;
		}
		
		@Column(name = "section")
		public String getSection() {
			return section;
		}
		public void setSection(String section) {
			this.section = section;
		}
		
		@Column(name = "row")
		public String getRow() {
			return row;
		}
		public void setRow(String row) {
			this.row = row;
		}
		
		@Column(name = "quantity")
		public Integer getQuantity() {
			return quantity;
		}
		public void setQuantity(Integer quantity) {
			this.quantity = quantity;
		}
		
		@Column(name = "price")
		public Double getPrice() {
			return price;
		}
		public void setPrice(Double price) {
			this.price = price;
		}
		
		@Column(name = "section_range")
		public String getSectionRange() {
			return sectionRange;
		}
		public void setSectionRange(String sectionRange) {
			this.sectionRange = sectionRange;
		}
		
		@Column(name = "row_range")
		public String getRowRange() {
			return rowRange;
		}
		public void setRowRange(String rowRange) {
			this.rowRange = rowRange;
		}
		
		@Enumerated(EnumType.STRING)
		@Column(name = "status") 
		public TicketStatus getStatus() {
			return status;
		}
		public void setStatus(TicketStatus status) {
			this.status = status;
		}
		
		@Column(name = "shipping_method")
		public String getShippingMethod() {
			return shippingMethod;
		}
		public void setShippingMethod(String shippingMethod) {
			this.shippingMethod = shippingMethod;
		}
		
		@Column(name = "created_date")
		public Date getCreatedDate() {
			return createdDate;
		}
		public void setCreatedDate(Date createdDate) {
			this.createdDate = createdDate;
		}
		
		@Column(name = "last_updated")
		public Date getLastUpdatedDate() {
			return lastUpdatedDate;
		}
		public void setLastUpdatedDate(Date lastUpdatedDate) {
			this.lastUpdatedDate = lastUpdatedDate;
		}
		
		@Column(name = "created_by")
		public String getCreatedBy() {
			return createdBy;
		}
		public void setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
		}
		
		@Column(name = "sold_price")
		public Double getSoldPrice() {
			return soldPrice;
		}
		public void setSoldPrice(Double soldPrice) {
			this.soldPrice = soldPrice;
		}
		
		@Column(name = "sold_qty")
		public Integer getSoldQuantity() {
			return soldQuantity;
		}
		public void setSoldQuantity(Integer soldQuantity) {
			this.soldQuantity = soldQuantity;
		}
		
		@Enumerated(EnumType.STRING)
		@Column(name = "product_type")
		public ProductType getProducttype() {
			return producttype;
		}
		public void setProducttype(ProductType producttype) {
			this.producttype = producttype;
		}
		
		@Column(name="invoice_id")
		public Integer getInvoiceId() {
			return invoiceId;
		}
		public void setInvoiceId(Integer invoiceId) {
			this.invoiceId = invoiceId;
		}
		
		@Column(name="seat_low")
		public String getSeatLow() {
			return seatLow;
		}
		public void setSeatLow(String seatLow) {
			this.seatLow = seatLow;
		}
		
		@Column(name="seat_high")
		public String getSeatHigh() {
			return seatHigh;
		}
		public void setSeatHigh(String seatHigh) {
			this.seatHigh = seatHigh;
		}
		
		@Column(name="external_notes")
		public String getExternalNotes() {
			return externalNotes;
		}
		public void setExternalNotes(String externalNotes) {
			this.externalNotes = externalNotes;
		}
		
		@Column(name="internal_notes")
		public String getInternalNotes() {
			return internalNotes;
		}
		public void setInternalNotes(String internalNotes) {
			this.internalNotes = internalNotes;
		}
		
		@Column(name="near_term_display_option")
		public String getNearTermDisplayOption() {
			return nearTermDisplayOption;
		}
		public void setNearTermDisplayOption(String nearTermDisplayOption) {
			this.nearTermDisplayOption = nearTermDisplayOption;
		}
		
		@Column(name="face_price")
		public Double getFacePrice() {
			return facePrice;
		}
		public void setFacePrice(Double facePrice) {
			this.facePrice = facePrice;
		}
		
		@Column(name="cost")
		public Double getCost() {
			return cost;
		}
		public void setCost(Double cost) {
			this.cost = cost;
		}
		
		@Column(name="wholesale_price")
		public Double getWholeSalePrice() {
			return wholeSalePrice;
		}
		public void setWholeSalePrice(Double wholeSalePrice) {
			this.wholeSalePrice = wholeSalePrice;
		}
		
		@Column(name="retail_price")
		public Double getRetailPrice() {
			return retailPrice;
		}
		public void setRetailPrice(Double retailPrice) {
			this.retailPrice = retailPrice;
		}
		
		@Column(name="broadcast")
		public Boolean getBroadcast() {
			return broadcast;
		}
		public void setBroadcast(Boolean broadcast) {
			this.broadcast = broadcast;
		}
		
		@Column(name="marketplace_notes")
		public String getMarketPlaceNotes() {
			return marketPlaceNotes;
		}
		public void setMarketPlaceNotes(String marketPlaceNotes) {
			this.marketPlaceNotes = marketPlaceNotes;
		}
		
		@Column(name="max_showing")
		public Integer getMaxShowing() {
			return maxShowing;
		}
		public void setMaxShowing(Integer maxShowing) {
			this.maxShowing = maxShowing;
		}
		
		@Column(name="on_hand")
		public String getTicketOnhandStatus() {
			return ticketOnhandStatus;
		}
		public void setTicketOnhandStatus(String ticketOnhandStatus) {
			this.ticketOnhandStatus = ticketOnhandStatus;
		}
		
		@Column(name="shipping_method_id")
		public Integer getShippingMethodId() {
			return shippingMethodId;
		}
		public void setShippingMethodId(Integer shippingMethodId) {
			this.shippingMethodId = shippingMethodId;
		}
		
		@Column(name="tax_amount")
		public Double getTaxAmount() {
			return taxAmount;
		}
		public void setTaxAmount(Double taxAmount) {
			this.taxAmount = taxAmount;
		}
		
		@Column(name="actual_section")
		public String getActualSection() {
			return actualSection;
		}
		public void setActualSection(String actualSection) {
			this.actualSection = actualSection;
		}
		@Transient
		public Double getLoyalFanPrice() {
			try {
				loyalFanPrice = Util.getLoyalFanPriceAsDouble(getPrice());
			} catch (Exception e) {
				e.printStackTrace();
			}
			return loyalFanPrice;
		}
		public void setLoyalFanPrice(Double loyalFanPrice) {
			this.loyalFanPrice = loyalFanPrice;
		}
		
		@Transient
		public String getSvgKey() {
			return svgKey;
		}
		public void setSvgKey(String svgKey) {
			this.svgKey = svgKey;
		}
		
		@Transient
		public String getColorCode() {
			return colorCode;
		}
		public void setColorCode(String colorCode) {
			this.colorCode = colorCode;
		}
		
		@Transient
		public String getRgbColor() {
			return rgbColor;
		}
		public void setRgbColor(String rgbColor) {
			this.rgbColor = rgbColor;
		}
		
		
		@Transient
		@Enumerated(EnumType.STRING)
		public LockedTicketStatus getLockStatus() {
			return lockStatus;
		}
		public void setLockStatus(LockedTicketStatus lockStatus) {
			this.lockStatus = lockStatus;
		}
		
		@Transient
		public Date getCreationDate() {
			return creationDate;
		}
		public void setCreationDate(Date creationDate) {
			this.creationDate = creationDate;
		}
		
		@Transient
		public Integer getCatgeoryTicketGroupId() {
			return catgeoryTicketGroupId;
		}
		public void setCatgeoryTicketGroupId(Integer catgeoryTicketGroupId) {
			this.catgeoryTicketGroupId = catgeoryTicketGroupId;
		}
		
		@Transient
		public String getIpAddress() {
			return ipAddress;
		}
		public void setIpAddress(String ipAddress) {
			this.ipAddress = ipAddress;
		}
		
		@Transient
		public String getPlatform() {
			return platform;
		}
		public void setPlatform(String platform) {
			this.platform = platform;
		}
		
		@Column(name="broker_id")
		public Integer getBrokerId() {
			return brokerId;
		}
		public void setBrokerId(Integer brokerId) {
			this.brokerId = brokerId;
		}
		
		@Transient
		public String getOrderTotal() {
			return orderTotal;
		}
		public void setOrderTotal(String orderTotal) {
			this.orderTotal = orderTotal;
		}	
		
		
		
		
			
	}