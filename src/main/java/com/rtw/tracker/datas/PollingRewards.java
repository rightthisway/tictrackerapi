package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "polling_rewards")
public class PollingRewards implements Serializable {

	private static final long serialVersionUID = -5002012680929886861L;

	private Integer id;
	private Integer contestId;
	private Integer lifePerQue;
	private Integer starsPerQue;
	private Integer rdPerQue;
	private Integer ticketsPerQue;
	private Integer gcPerQue;
	private Integer eraserPerQue;
	private Integer gbricksPerQue;
	private Integer hifivePerQue;
	private Integer crstballPerQue;
	private Integer rtfPointsPerQue;
	
	private Integer maxlifePerCustPerDay;
	private Integer maxstarsPerCustPerDay;
	private Integer maxrdPerCustPerDay;
	private Integer maxticketsPerCustPerDay;
	private Integer maxgcPerCustPerDay;
	private Integer maxeraserPerCustPerDay;
	private Integer maxgbricksPerCustPerDay;
	private Integer maxhifivePerCustPerDay;
	private Integer maxcrstballPerCustPerDay;

	private Integer maxlifePerContest;
	private Integer maxstarsPerContest;
	private Integer maxrdPerContest;
	private Integer maxticketsPerContest;
	private Integer maxgcPerContest;
	private Integer maxeraserPerContest;
	private Integer maxgbricksPerContest;
	private Integer maxhifivePerContest;
	private Integer maxcrstballPerContest;

	private String status;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "contest_id")
	public Integer getContestId() {
		return contestId;
	}

	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}

	@Column(name = "life_per_que")
	public Integer getLifePerQue() {
		return lifePerQue;
	}

	public void setLifePerQue(Integer lifePerQue) {
		this.lifePerQue = lifePerQue;
	}

	@Column(name = "stars_per_que")
	public Integer getStarsPerQue() {
		return starsPerQue;
	}

	public void setStarsPerQue(Integer starsPerQue) {
		this.starsPerQue = starsPerQue;
	}

	@Column(name = "rd_per_que")
	public Integer getRdPerQue() {
		return rdPerQue;
	}

	public void setRdPerQue(Integer rdPerQue) {
		this.rdPerQue = rdPerQue;
	}

	@Column(name = "tickets_per_que")
	public Integer getTicketsPerQue() {
		return ticketsPerQue;
	}

	public void setTicketsPerQue(Integer ticketsPerQue) {
		this.ticketsPerQue = ticketsPerQue;
	}

	@Column(name = "gc_per_que")
	public Integer getGcPerQue() {
		return gcPerQue;
	}

	public void setGcPerQue(Integer gcPerQue) {
		this.gcPerQue = gcPerQue;
	}

	@Column(name = "eraser_per_que")
	public Integer getEraserPerQue() {
		return eraserPerQue;
	}

	public void setEraserPerQue(Integer eraserPerQue) {
		this.eraserPerQue = eraserPerQue;
	}

	@Column(name = "gbricks_per_que")
	public Integer getGbricksPerQue() {
		return gbricksPerQue;
	}

	public void setGbricksPerQue(Integer gbricksPerQue) {
		this.gbricksPerQue = gbricksPerQue;
	}

	@Column(name = "hifive_per_que")
	public Integer getHifivePerQue() {
		return hifivePerQue;
	}

	public void setHifivePerQue(Integer hifivePerQue) {
		this.hifivePerQue = hifivePerQue;
	}

	@Column(name = "crstball_per_que")
	public Integer getCrstballPerQue() {
		return crstballPerQue;
	}

	public void setCrstballPerQue(Integer crstballPerQue) {
		this.crstballPerQue = crstballPerQue;
	}
	
	
	@Column(name = "rtfpoints_per_que")
	public Integer getRtfPointsPerQue() {
		return rtfPointsPerQue;
	}

	public void setRtfPointsPerQue(Integer rtfPointsPerQue) {
		this.rtfPointsPerQue = rtfPointsPerQue;
	}

	@Column(name = "maxlife_per_cust_per_day")
	public Integer getMaxlifePerCustPerDay() {
		return maxlifePerCustPerDay;
	}

	public void setMaxlifePerCustPerDay(Integer maxlifePerCustPerDay) {
		this.maxlifePerCustPerDay = maxlifePerCustPerDay;
	}

	@Column(name = "maxstars_per_cust_per_day")
	public Integer getMaxstarsPerCustPerDay() {
		return maxstarsPerCustPerDay;
	}

	public void setMaxstarsPerCustPerDay(Integer maxstarsPerCustPerDay) {
		this.maxstarsPerCustPerDay = maxstarsPerCustPerDay;
	}

	@Column(name = "maxrd_per_cust_per_day")
	public Integer getMaxrdPerCustPerDay() {
		return maxrdPerCustPerDay;
	}

	public void setMaxrdPerCustPerDay(Integer maxrdPerCustPerDay) {
		this.maxrdPerCustPerDay = maxrdPerCustPerDay;
	}

	@Column(name = "maxtickets_per_cust_per_day")
	public Integer getMaxticketsPerCustPerDay() {
		return maxticketsPerCustPerDay;
	}

	public void setMaxticketsPerCustPerDay(Integer maxticketsPerCustPerDay) {
		this.maxticketsPerCustPerDay = maxticketsPerCustPerDay;
	}

	@Column(name = "maxgc_per_cust_per_day")
	public Integer getMaxgcPerCustPerDay() {
		return maxgcPerCustPerDay;
	}

	public void setMaxgcPerCustPerDay(Integer maxgcPerCustPerDay) {
		this.maxgcPerCustPerDay = maxgcPerCustPerDay;
	}

	@Column(name = "maxeraser_per_cust_per_day")
	public Integer getMaxeraserPerCustPerDay() {
		return maxeraserPerCustPerDay;
	}

	public void setMaxeraserPerCustPerDay(Integer maxeraserPerCustPerDay) {
		this.maxeraserPerCustPerDay = maxeraserPerCustPerDay;
	}

	@Column(name = "maxgbricks_per_cust_per_day")
	public Integer getMaxgbricksPerCustPerDay() {
		return maxgbricksPerCustPerDay;
	}

	public void setMaxgbricksPerCustPerDay(Integer maxgbricksPerCustPerDay) {
		this.maxgbricksPerCustPerDay = maxgbricksPerCustPerDay;
	}

	@Column(name = "maxhifive_per_cust_per_day")
	public Integer getMaxhifivePerCustPerDay() {
		return maxhifivePerCustPerDay;
	}

	public void setMaxhifivePerCustPerDay(Integer maxhifivePerCustPerDay) {
		this.maxhifivePerCustPerDay = maxhifivePerCustPerDay;
	}

	@Column(name = "maxcrstball_per_cust_per_day")
	public Integer getMaxcrstballPerCustPerDay() {
		return maxcrstballPerCustPerDay;
	}

	public void setMaxcrstballPerCustPerDay(Integer maxcrstballPerCustPerDay) {
		this.maxcrstballPerCustPerDay = maxcrstballPerCustPerDay;
	}

	@Column(name = "maxlife_per_contest")
	public Integer getMaxlifePerContest() {
		return maxlifePerContest;
	}

	public void setMaxlifePerContest(Integer maxlifePerContest) {
		this.maxlifePerContest = maxlifePerContest;
	}

	@Column(name = "maxstars_per_contest")
	public Integer getMaxstarsPerContest() {
		return maxstarsPerContest;
	}

	public void setMaxstarsPerContest(Integer maxstarsPerContest) {
		this.maxstarsPerContest = maxstarsPerContest;
	}

	@Column(name = "maxrd_per_contest")
	public Integer getMaxrdPerContest() {
		return maxrdPerContest;
	}

	public void setMaxrdPerContest(Integer maxrdPerContest) {
		this.maxrdPerContest = maxrdPerContest;
	}

	@Column(name = "maxtickets_per_contest")
	public Integer getMaxticketsPerContest() {
		return maxticketsPerContest;
	}

	public void setMaxticketsPerContest(Integer maxticketsPerContest) {
		this.maxticketsPerContest = maxticketsPerContest;
	}

	@Column(name = "maxgc_per_contest")
	public Integer getMaxgcPerContest() {
		return maxgcPerContest;
	}

	public void setMaxgcPerContest(Integer maxgcPerContest) {
		this.maxgcPerContest = maxgcPerContest;
	}

	@Column(name = "maxeraser_per_contest")
	public Integer getMaxeraserPerContest() {
		return maxeraserPerContest;
	}

	public void setMaxeraserPerContest(Integer maxeraserPerContest) {
		this.maxeraserPerContest = maxeraserPerContest;
	}

	@Column(name = "maxgbricks_per_contest")
	public Integer getMaxgbricksPerContest() {
		return maxgbricksPerContest;
	}

	public void setMaxgbricksPerContest(Integer maxgbricksPerContest) {
		this.maxgbricksPerContest = maxgbricksPerContest;
	}

	@Column(name = "maxhifive_per_contest")
	public Integer getMaxhifivePerContest() {
		return maxhifivePerContest;
	}

	public void setMaxhifivePerContest(Integer maxhifivePerContest) {
		this.maxhifivePerContest = maxhifivePerContest;
	}

	@Column(name = "maxcrstball_per_contest")
	public Integer getMaxcrstballPerContest() {
		return maxcrstballPerContest;
	}

	public void setMaxcrstballPerContest(Integer maxcrstballPerContest) {
		this.maxcrstballPerContest = maxcrstballPerContest;
	}

	@Column(name = "status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "created_date")
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "updated_by")
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column(name = "updated_date")
	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}
