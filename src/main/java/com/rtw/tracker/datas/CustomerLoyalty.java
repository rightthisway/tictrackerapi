package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.rtw.tmat.utils.RewardConversionUtil;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * represents customer entity
 * @author Ulaganathan
 *
 */
//@XStreamAlias("CustomerLoyalty")
@Entity
@Table(name="cust_loyalty_reward_info")
public class CustomerLoyalty  implements Serializable{
	
	//@JsonIgnore
	private Integer id;
	private Integer customerId;
	private Double activePoints;
	private Double pendingPoints;
	private Double totalEarnedPoints;
	private Double totalSpentPoints;
	private Double latestEarnedPoints;
	private Double latestSpentPoints;
	private Double activeRewardDollers;
	private Double dollerConversion;
	private Double voidedRewardPoints;
	private Double revertedSpentPoints;
	
//	@JsonIgnore
	private Date lastUpdate;
	
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id ,Id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="cust_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Column(name="updated_time" )
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	
	@Column(name="active_reward_points" )
	public Double getActivePoints() {
		return activePoints;
	}
	public void setActivePoints(Double activePoints) {
		this.activePoints = activePoints;
	}
	
	@Column(name="total_earn_points" )
	public Double getTotalEarnedPoints() {
		return totalEarnedPoints;
	}
	public void setTotalEarnedPoints(Double totalEarnedPoints) {
		this.totalEarnedPoints = totalEarnedPoints;
	}
	
	@Column(name="total_spent_points" )
	public Double getTotalSpentPoints() {
		return totalSpentPoints;
	}
	public void setTotalSpentPoints(Double totalSpentPoints) {
		this.totalSpentPoints = totalSpentPoints;
	}
	
	@Column(name="latest_earn_points" )
	public Double getLatestEarnedPoints() {
		return latestEarnedPoints;
	}
	public void setLatestEarnedPoints(Double latestEarnedPoints) {
		this.latestEarnedPoints = latestEarnedPoints;
	}
	
	@Column(name="latest_spent_points" )
	public Double getLatestSpentPoints() {
		return latestSpentPoints;
	}
	public void setLatestSpentPoints(Double latestSpentPoints) {
		this.latestSpentPoints = latestSpentPoints;
	}
	
	@Transient
	public Double getActiveRewardDollers() {
		if(null != activePoints){
			activeRewardDollers =  RewardConversionUtil.getRewardDoller(activePoints);
		}else{
			activeRewardDollers=0.00;
		}
		return activeRewardDollers;
	}
	
	public void setActiveRewardDollers(Double activeRewardDollers) {
		this.activeRewardDollers = activeRewardDollers;
	}
	
	@Transient
	public Double getDollerConversion() {
		LoyaltySettings loyaltySettings = DAORegistry.getLoyaltySettingsDAO().getActivetLoyaltySettings();
		dollerConversion = loyaltySettings.getDollerConversion();
		return dollerConversion;
	}
	public void setDollerConversion(Double dollerConversion) {
		this.dollerConversion = dollerConversion;
	}
	
	
	@Column(name="pending_rewards")
	public Double getPendingPoints() {
		return pendingPoints;
	}
	public void setPendingPoints(Double pendingPoints) {
		this.pendingPoints = pendingPoints;
	}
	
	@Column(name="voided_reward_points")
	public Double getVoidedRewardPoints() {
		return voidedRewardPoints;
	}
	public void setVoidedRewardPoints(Double voidedRewardPoints) {
		this.voidedRewardPoints = voidedRewardPoints;
	}
	
	@Column(name="reverted_spent_points")
	public Double getRevertedSpentPoints() {
		return revertedSpentPoints;
	}
	public void setRevertedSpentPoints(Double revertedSpentPoints) {
		this.revertedSpentPoints = revertedSpentPoints;
	}
	
	
	
}
