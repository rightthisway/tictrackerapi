package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.map.annotate.JsonDeserialize;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.rtw.tmat.utils.Util;
import com.rtw.tracker.dao.implementation.DAORegistry;

@Entity
@Table(name="fantasy_events")
public class CrownJewelLeagues implements Serializable{
	
	/**
	 * serialVersionUID is added only to remove warning
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String name;
	private String lastUpdateddBy;
	@JsonDeserialize(using = com.rtw.tracker.utils.CustomerDateAndTimeDeserialize.class)
	@JsonSerialize(using = com.rtw.tracker.utils.CustomerDateAndTimeSerializer.class)
	private Date lastUpdated;
	private String status;
	private String lastUpdatedStr;
	//private String parentType;
	
	//private Artist artist;
	private Integer eventId;
	private Integer childId;
	private Integer grandChildId;
	private Boolean isRealEvent;
	private String venueString;
	private Boolean isPackageCost;
	private List<FantasyGrandChildCategory> grandChildCategory;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Column(name="last_updated_by")
	public String getLastUpdateddBy() {
		return lastUpdateddBy;
	}
	public void setLastUpdateddBy(String lastUpdateddBy) {
		this.lastUpdateddBy = lastUpdateddBy;
	}
	
	/*@Column(name="parent_type")
	public String getParentType() {
		return parentType;
	}
	public void setParentType(String parentType) {
		this.parentType = parentType;
	}*/
	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	/*@ManyToOne
	@JoinColumn(name="artist_id")
	public Artist getArtist() {
		return artist;
	}
	public void setArtist(Artist artist) {
		this.artist = artist;
	}*/
	
	@Column(name="only_package")
	public Boolean getIsPackageCost() {
		return isPackageCost;
	}
	public void setIsPackageCost(Boolean isPackageCost) {
		this.isPackageCost = isPackageCost;
	}
	@Transient
	public String getLastUpdatedStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(getLastUpdated());
	}
	public void setLastUpdatedStr(String lastUpdatedStr) {
		this.lastUpdatedStr = lastUpdatedStr;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	
	
	@Column(name="grandchild_id")
	public Integer getGrandChildId() {
		return grandChildId;
	}
	
	public void setGrandChildId(Integer grandChildId) {
		this.grandChildId = grandChildId;
	}
	
	@Column(name="is_real_event")
	public Boolean getIsRealEvent() {
		return isRealEvent;
	}
	public void setIsRealEvent(Boolean isRealEvent) {
		this.isRealEvent = isRealEvent;
	}
	
	@Transient
	public String getVenueString() {
		return venueString;
	}
	public void setVenueString(String venueString) {
		this.venueString = venueString;
	}
	
	@Transient
	public List<FantasyGrandChildCategory> getGrandChildCategory() {
		/*if(grandChildId!=null){
			FantasyGrandChildCategory gcc = DAORegistry.getFantasyGrandChildCategoryDAO().get(grandChildId);
			if(gcc != null){
				grandChildCategory = DAORegistry.getFantasyGrandChildCategoryDAO().getGrandChildsByChildCategoryId(gcc.getChildId());
			}
		}*/
		return grandChildCategory;
	}
	public void setGrandChildCategory(
			List<FantasyGrandChildCategory> grandChildCategory) {
		this.grandChildCategory = grandChildCategory;
	}
	
	@Transient
	public Integer getChildId() {
		/*if(grandChildId!=null){
			FantasyGrandChildCategory gcc = DAORegistry.getFantasyGrandChildCategoryDAO().get(grandChildId);
			if(gcc != null){
				childId = gcc.getChildId();
			}
		}*/
		return childId;
	}
	
	public void setChildId(Integer childId) {
		this.childId = childId;
	}

}
