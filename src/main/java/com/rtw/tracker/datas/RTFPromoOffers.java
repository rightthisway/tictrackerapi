package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tmat.utils.Util;

@Entity
@Table(name = "rtf_promotional_offer_hdr")
public class RTFPromoOffers implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name="promo_code")
	private String promoCode; 
	
	@Column(name="discount")
	private Double discountPer;
	
	@Column(name="mobile_discount")
	private Double mobileDiscountPerc;
	
	@Column(name="start_date")
	private Date startDate;
	
	@Column(name="end_date")
	private Date endDate;
	
	@Column(name="max_orders")
	private Integer maxOrders;
	
	@Column(name="no_of_orders")
	private Integer noOfOrders;
	
	@Column(name="is_flat_discount")
	private Boolean isFlatDiscount;
	
	@Column(name="status")
	private String status;
	
	@Column(name="created_by")
	private String createdBy;
	
	@Column(name="modified_by")
	private String modifiedBy;
	
	@Column(name="created_date")
	private Date createdDate;
	
	@Column(name="modified_time")
	private Date modifiedDate;
	
	@Column(name="flat_offer_order_threshold")
	private Double flatOfferOrderThreshold;
	
	@Transient
	private String startDateStr;
	@Transient
	private String endDateStr;
	@Transient
	private String createdDateStr;
	@Transient
	private String modifiedDateStr;
	@Transient
	private String promoType;	
	@Transient
	private Integer artistId;	
	@Transient
	private Integer venueId;	
	@Transient
	private Integer grandChildId;	
	@Transient
	private Integer childId;	
	@Transient
	private Integer parentId;
	@Transient
	private String artistName;
	@Transient
	private String venueName;
	@Transient
	private String parentCategory;
	@Transient
	private String childCategory;
	@Transient
	private String grandChildCategory;
	@Transient
	private Integer eventId;
	@Transient
	private String eventName;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getPromoCode() {
		return promoCode;
	}
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}
	
	public Double getDiscountPer() {
		return discountPer;
	}
	public void setDiscountPer(Double discountPer) {
		this.discountPer = discountPer;
	}
	
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public Integer getMaxOrders() {
		return maxOrders;
	}
	public void setMaxOrders(Integer maxOrders) {
		this.maxOrders = maxOrders;
	}
	
	public Integer getNoOfOrders() {
		return noOfOrders;
	}
	public void setNoOfOrders(Integer noOfOrders) {
		this.noOfOrders = noOfOrders;
	}
	
	public Boolean getIsFlatDiscount() {
		return isFlatDiscount;
	}
	public void setIsFlatDiscount(Boolean isFlatDiscount) {
		this.isFlatDiscount = isFlatDiscount;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	public Double getFlatOfferOrderThreshold() {
		return flatOfferOrderThreshold;
	}
	public void setFlatOfferOrderThreshold(Double flatOfferOrderThreshold) {
		this.flatOfferOrderThreshold = flatOfferOrderThreshold;
	}
	
	public String getStartDateStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(startDate);
	}
	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}
	
	public String getEndDateStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(endDate);
	}
	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}
	
	public String getCreatedDateStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(createdDate);
	}
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	
	public String getModifiedDateStr() {
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(modifiedDate);
	}
	public void setModifiedDateStr(String modifiedDateStr) {
		this.modifiedDateStr = modifiedDateStr;
	}
	
	public String getPromoType() {
		return promoType;
	}
	public void setPromoType(String promoType) {
		this.promoType = promoType;
	}
	
	public Integer getArtistId() {
		return artistId;
	}
	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}
	
	public Integer getVenueId() {
		return venueId;
	}
	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}
	
	public Integer getGrandChildId() {
		return grandChildId;
	}
	public void setGrandChildId(Integer grandChildId) {
		this.grandChildId = grandChildId;
	}
	
	public Integer getChildId() {
		return childId;
	}
	public void setChildId(Integer childId) {
		this.childId = childId;
	}
	
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	
	public String getArtistName() {
		return artistName;
	}
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
	
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	
	public String getParentCategory() {
		return parentCategory;
	}
	public void setParentCategory(String parentCategory) {
		this.parentCategory = parentCategory;
	}
	
	public String getChildCategory() {
		return childCategory;
	}
	public void setChildCategory(String childCategory) {
		this.childCategory = childCategory;
	}
	
	public String getGrandChildCategory() {
		return grandChildCategory;
	}
	public void setGrandChildCategory(String grandChildCategory) {
		this.grandChildCategory = grandChildCategory;
	}	
	
	public Double getMobileDiscountPerc() {
		return mobileDiscountPerc;
	}
	public void setMobileDiscountPerc(Double mobileDiscountPerc) {
		this.mobileDiscountPerc = mobileDiscountPerc;
	}
	
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
}
