package com.rtw.tracker.datas;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;


@XStreamAlias("InvoiceRefunds")
public class InvoiceRefundResponse {

	private Integer status;
	private com.rtw.tmat.utils.Error error; 
	private InvoiceRefund invoiceRefund;
	private List<InvoiceRefund> paypalRefunds;
	private List<InvoiceRefund> stripeRefunds;
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public com.rtw.tmat.utils.Error getError() {
		return error;
	}
	public void setError(com.rtw.tmat.utils.Error error) {
		this.error = error;
	}
	public InvoiceRefund getInvoiceRefund() {
		return invoiceRefund;
	}
	public void setInvoiceRefund(InvoiceRefund invoiceRefund) {
		this.invoiceRefund = invoiceRefund;
	}
	public List<InvoiceRefund> getPaypalRefunds() {
		return paypalRefunds;
	}
	public void setPaypalRefunds(List<InvoiceRefund> paypalRefunds) {
		this.paypalRefunds = paypalRefunds;
	}
	public List<InvoiceRefund> getStripeRefunds() {
		return stripeRefunds;
	}
	public void setStripeRefunds(List<InvoiceRefund> stripeRefunds) {
		this.stripeRefunds = stripeRefunds;
	}
	
}
