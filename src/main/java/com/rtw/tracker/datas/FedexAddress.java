package com.rtw.tracker.datas;

import java.io.Serializable;

import com.rtw.tracker.fedex.track.Address;

public class FedexAddress implements Serializable{

	private String street1;
	private String street2;
	private String city;
	private String state;
	private String country;
	private String countryCode;
	private String postalCode;
	private String addressType;
	
	
	public FedexAddress(Address address) {
		if(address.getStreetLines()!=null && address.getStreetLines().length>0){
			this.street1 = address.getStreetLines(0);
			if(address.getStreetLines().length>=2){
				this.street2 = address.getStreetLines(1);
			}
		}
		this.city = address.getCity();
		this.state = address.getStateOrProvinceCode();
		this.countryCode = address.getCountryCode();
		this.country = address.getCountryName();
		this.postalCode = address.getPostalCode();
		if(address.getResidential()){
			this.addressType="Residential";
		}else{
			this.addressType="Commercial";
		}
		 
	}
	
	public String getStreet1() {
		return street1;
	}
	public void setStreet1(String street1) {
		this.street1 = street1;
	}
	public String getStreet2() {
		return street2;
	}
	public void setStreet2(String street2) {
		this.street2 = street2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getAddressType() {
		return addressType;
	}
	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	
	
}
