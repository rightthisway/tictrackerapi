package com.rtw.tracker.datas;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.annotate.JsonDeserialize;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.rtw.tmat.utils.Util;

@Entity
@Table(name="event")
public class Event implements Serializable{
	
	private Integer eventId;
	private String eventName;
	@JsonIgnore
	private Date eventDate;
	private Time eventTime;
	private Integer venueId;
	private Integer status;
	private Integer venueCategoryId;
	private Integer artistId;
	@JsonIgnore
	private Date createDate;
	@JsonIgnore
	private Date lastUpdate;
	private String venueCategoryName;
	private String building;
	private String city;
	private String state;
	private String country;
	private String svgMapPath;
	private String svgText;
	
	private String eventDateStr;
	private String eventTimeStr;
	private String createDateStr;
	private String lastUpdateStr;
	
	public Event(){
		
	}
	
	@Id
	@Column(name="id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="name")
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	@Column(name="event_date")
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	
	@Column(name="event_time")
	public Time getEventTime() {
		return eventTime;
	}
	public void setEventTime(Time eventTime) {
		this.eventTime = eventTime;
	}
	
	@Column(name="status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Column(name="venue_category_id")
	public Integer getVenueCategoryId() {
		return venueCategoryId;
	}

	public void setVenueCategoryId(Integer venueCategoryId) {
		this.venueCategoryId = venueCategoryId;
	}

	@Column(name="artist_id")
	public Integer getArtistId() {
		return artistId;
	}

	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}

	@Column(name="create_date")
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Column(name="last_update")
	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	@Column(name="venue_category_name")
	public String getVenueCategoryName() {
		return venueCategoryName;
	}

	public void setVenueCategoryName(String venueCategoryName) {
		this.venueCategoryName = venueCategoryName;
	}

	@Transient
	public String getCreateDateStr() {
		if(StringUtils.isNotEmpty(createDateStr)){
			return createDateStr;
		}
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(getCreateDate());
	}

	public void setCreateDateStr(String createDateStr) {
		this.createDateStr = createDateStr;
	}

	@Transient
	public String getLastUpdateStr() {
		if(StringUtils.isNotEmpty(lastUpdateStr)){
			return lastUpdateStr;
		}
		return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(getLastUpdate());
	}

	public void setLastUpdateStr(String lastUpdateStr) {
		this.lastUpdateStr = lastUpdateStr;
	}

	@Column(name="venue_id")
	public Integer getVenueId() {
		return venueId;
	}
	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}
	
	
	@Transient
	public String getBuilding() {
		return building;
	}

	public void setBuilding(String building) {
		this.building = building;
	}

	@Transient
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Transient
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Transient
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Transient
	public String getEventDateStr() {
		if(StringUtils.isNotEmpty(eventDateStr)){
			return eventDateStr;
		}
		if(getEventDate()==null){
			return "TBD";
		}
		return Util.formatDateToMonthDateYear(getEventDate());
	}
	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}
	
	@Transient
	public String getEventTimeStr() {
		if(StringUtils.isNotEmpty(eventTimeStr)){
			return eventTimeStr;
		}
		if(getEventTime()==null){
			return "TBD";
		}
		return Util.formatTimeToHourMinutes(getEventTime());
	}
	public void setEventTimeStr(String eventTimeStr) {
		this.eventTimeStr = eventTimeStr;
	}
	
	@Transient
	public String getNameWithDateandVenue() {
		String fullName = eventName;
		if(eventDate != null) {
			fullName = fullName + " " + getEventDateStr() +" "+getEventTimeStr()+" "+getFormattedVenueDescription();
		} else {
			fullName = fullName + ",TBD";
		}
		return fullName;
	}
	
	@Transient
	public String getFormattedVenueDescription() {
		
		String[] tokens = {
			this.building,
			this.city,
			this.state,
			this.country				
		};
		
		String v = null;
		for (String token: tokens) {
			if (token == null || token.equals("null")) {
				continue;
			}
			
			if (v == null) {
				v = token;
			} else {
				v += ", " + token;
			}
		}
		return v;
	}

	@Transient
	public String getSvgMapPath() {
		return svgMapPath;
	}

	public void setSvgMapPath(String svgMapPath) {
		this.svgMapPath = svgMapPath;
	}

	@Transient
	public String getSvgText() {
		return svgText;
	}

	public void setSvgText(String svgText) {
		this.svgText = svgText;
	}
	
	
	
}
