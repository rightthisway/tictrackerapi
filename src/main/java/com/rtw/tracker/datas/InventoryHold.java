package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;

import com.rtw.tmat.utils.Util;

@Entity
@Table(name = "inventory_hold")
public class InventoryHold implements Serializable{

	private Integer id;
	private Integer customerId;
	private Double salePrice;
	private String cSR;
	private Date expirationDate;
	private Integer expirationMinutes;
	private Integer shippingMethodId;
	private Date holdDateTime;
	private String internalNote;
	private String externalNote;
	private String ticketIds;
	private Integer addressId;
	private String externalPo;
	private Integer ticketGroupId;
	private String status;
	
	private String customerName;
	private String expirationDateStr;
	private String holdDateTimeStr;
	private String shippingMethod;
	private Integer brokerId;
	private String section;
	private String row;
	private String seatLow;
	private String seatHigh;
	private Integer quantity;
	
	public InventoryHold(){		
	}

	@Id
	@GeneratedValue
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	@Column(name="sale_price")
	public Double getSalePrice() {
		return salePrice;
	}
	public void setSalePrice(Double salePrice) {
		this.salePrice = salePrice;
	}

	@Column(name="csr")
	public String getcSR() {
		return cSR;
	}
	public void setcSR(String cSR) {
		this.cSR = cSR;
	}

	@Column(name="expiration_date")
	public Date getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	@Column(name="expiration_minutes")
	public Integer getExpirationMinutes() {
		return expirationMinutes;
	}
	public void setExpirationMinutes(Integer expirationMinutes) {
		this.expirationMinutes = expirationMinutes;
	}
	
	@Column(name="shipping_method_id")
	public Integer getShippingMethodId() {
		return shippingMethodId;
	}
	public void setShippingMethodId(Integer shippingMethodId) {
		this.shippingMethodId = shippingMethodId;
	}

	@Column(name="hold_datetime")
	public Date getHoldDateTime() {
		return holdDateTime;
	}
	public void setHoldDateTime(Date holdDateTime) {
		this.holdDateTime = holdDateTime;
	}

	@Column(name="internal_note")
	public String getInternalNote() {
		return internalNote;
	}
	public void setInternalNote(String internalNote) {
		this.internalNote = internalNote;
	}

	@Column(name="external_note")
	public String getExternalNote() {
		return externalNote;
	}
	public void setExternalNote(String externalNote) {
		this.externalNote = externalNote;
	}

	@Column(name="ticket_ids")
	public String getTicketIds() {
		return ticketIds;
	}
	public void setTicketIds(String ticketIds) {
		this.ticketIds = ticketIds;
	}

	@Column(name="address_id")
	public Integer getAddressId() {
		return addressId;
	}
	public void setAddressId(Integer addressId) {
		this.addressId = addressId;
	}

	@Column(name="external_po")
	public String getExternalPo() {
		return externalPo;
	}
	public void setExternalPo(String externalPo) {
		this.externalPo = externalPo;
	}

	@Column(name="ticket_group_id")
	public Integer getTicketGroupId() {
		return ticketGroupId;
	}
	public void setTicketGroupId(Integer ticketGroupId) {
		this.ticketGroupId = ticketGroupId;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	@Transient
	public String getExpirationDateStr() {
		if(StringUtils.isEmpty(expirationDateStr))
			return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(getExpirationDate());
		return expirationDateStr;
	}

	public void setExpirationDateStr(String expirationDateStr) {
		this.expirationDateStr = expirationDateStr;
	}

	@Transient
	public String getHoldDateTimeStr() {
		if(StringUtils.isEmpty(holdDateTimeStr))
			return Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(getHoldDateTime());
		return holdDateTimeStr;
	}

	public void setHoldDateTimeStr(String holdDateTimeStr) {
		this.holdDateTimeStr = holdDateTimeStr;
	}

	@Transient
	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	@Transient
	public String getShippingMethod() {
		return shippingMethod;
	}

	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	@Column(name="broker_id")
	public Integer getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}
	
	@Transient
	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	@Transient
	public String getRow() {
		return row;
	}

	public void setRow(String row) {
		this.row = row;
	}

	@Transient
	public String getSeatLow() {
		return seatLow;
	}

	public void setSeatLow(String seatLow) {
		this.seatLow = seatLow;
	}

	@Transient
	public String getSeatHigh() {
		return seatHigh;
	}

	public void setSeatHigh(String seatHigh) {
		this.seatHigh = seatHigh;
	}

	@Transient
	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
}
