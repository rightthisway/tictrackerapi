package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.web.multipart.MultipartFile;

import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.enums.ArtistStatus;
import com.rtw.tracker.enums.CardType;

@Entity
@Table(name="child_category_image_audit")
public class ChildCategoryImageAudit implements Serializable  {
	
	private Integer id;
	private String imageFileUrl;
	private String lastUpdatedBy;
	private Date lastUpdated;
	private Integer childCategoryId;
	private String status;
	private String circleImageFileUrl;
	private String action;
	
	public ChildCategoryImageAudit() {
		//super();
	}
	
	public ChildCategoryImageAudit(ChildCategoryImage ccImage) {
		this.childCategoryId=ccImage.getChildCategory().getId();
		this.imageFileUrl=ccImage.getImageFileUrl();
		this.lastUpdated=ccImage.getLastUpdated();
		this.lastUpdatedBy=ccImage.getLastUpdatedBy();
		this.status=ccImage.getStatus();
		this.circleImageFileUrl=ccImage.getCircleImageFileUrl();
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="image_url")
	public String getImageFileUrl() {
		return imageFileUrl;
	}

	public void setImageFileUrl(String imageFileUrl) {
		this.imageFileUrl = imageFileUrl;
	}

	@Column(name="last_updated_by")
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Column(name="child_category_id")
	public Integer getChildCategoryId() {
		return childCategoryId;
	}

	public void setChildCategoryId(Integer childCategoryId) {
		this.childCategoryId = childCategoryId;
	}

	@Column(name="action")
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	@Column(name="status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="circle_image_url")
	public String getCircleImageFileUrl() {
		return circleImageFileUrl;
	}

	public void setCircleImageFileUrl(String circleImageFileUrl) {
		this.circleImageFileUrl = circleImageFileUrl;
	}

	
}

