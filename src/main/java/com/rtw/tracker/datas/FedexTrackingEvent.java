package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

public class FedexTrackingEvent implements Serializable{

	private Integer eventNo;
	private Date eventDate;
	private String eventType;
	private String stationId;
	private String arrivalLocationType;
	private FedexAddress eventAddress;
	
	public Integer getEventNo() {
		return eventNo;
	}
	public void setEventNo(Integer eventNo) {
		this.eventNo = eventNo;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public String getStationId() {
		return stationId;
	}
	public void setStationId(String stationId) {
		this.stationId = stationId;
	}
	public String getArrivalLocationType() {
		return arrivalLocationType;
	}
	public void setArrivalLocationType(String arrivalLocationType) {
		this.arrivalLocationType = arrivalLocationType;
	}
	public FedexAddress getEventAddress() {
		return eventAddress;
	}
	public void setEventAddress(FedexAddress eventAddress) {
		this.eventAddress = eventAddress;
	}
}
