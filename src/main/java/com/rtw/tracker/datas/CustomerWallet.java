package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="customer_wallet")
public class CustomerWallet implements Serializable{

	private Integer id;
	private Integer customerId;
	private Double activeCredit;
	private Double pendingDebit;
	private Double totalCredit;
	private Double totalUsedCredit;
	private Date lastUpdated;
	
	@Id
	@GeneratedValue
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Column(name="active_credit")
	public Double getActiveCredit() {
		return activeCredit;
	}
	public void setActiveCredit(Double activeCredit) {
		this.activeCredit = activeCredit;
	}
	
	@Column(name="pending_debit")
	public Double getPendingDebit() {
		return pendingDebit;
	}
	public void setPendingDebit(Double pendingDebit) {
		this.pendingDebit = pendingDebit;
	}
	
	@Column(name="total_credit")
	public Double getTotalCredit() {
		return totalCredit;
	}
	public void setTotalCredit(Double totalCredit) {
		this.totalCredit = totalCredit;
	}
	
	@Column(name="total_used_credit")
	public Double getTotalUsedCredit() {
		return totalUsedCredit;
	}
	public void setTotalUsedCredit(Double totalUsedCredit) {
		this.totalUsedCredit = totalUsedCredit;
	}
	
	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	
	
	
}
