package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "stripe_customer_cards")
public class CustomerStripeCreditCard implements Serializable{

	private Integer id;
	private Integer customerId;
	private String stripeCustomerId;
	private String stripeCardId;
	private String cardType;
	private String cardLastFourDigit;
	private Integer expiryMonth;
	private Integer expiryYear;
	private String status;
	private Date createdDate;
	private Date lastUpdatedDate;
	private Boolean isShowToCustomer;
	
	
	@Id
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Column(name="stripe_cust_id")
	public String getStripeCustomerId() {
		return stripeCustomerId;
	}
	public void setStripeCustomerId(String stripeCustomerId) {
		this.stripeCustomerId = stripeCustomerId;
	}
	
	@Column(name="stripe_card_id")
	public String getStripeCardId() {
		return stripeCardId;
	}
	public void setStripeCardId(String stripeCardId) {
		this.stripeCardId = stripeCardId;
	}
	
	@Column(name="card_type")
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	
	@Column(name="card_last_four_digit")
	public String getCardLastFourDigit() {
		return cardLastFourDigit;
	}
	public void setCardLastFourDigit(String cardLastFourDigit) {
		this.cardLastFourDigit = cardLastFourDigit;
	}
	
	@Column(name="expiry_month")
	public Integer getExpiryMonth() {
		return expiryMonth;
	}
	public void setExpiryMonth(Integer expiryMonth) {
		this.expiryMonth = expiryMonth;
	}
	
	@Column(name="expiry_year")
	public Integer getExpiryYear() {
		return expiryYear;
	}
	public void setExpiryYear(Integer expiryYear) {
		this.expiryYear = expiryYear;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="create_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="last_updated")
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	
	@Column(name="show_to_customer")
	public Boolean getIsShowToCustomer() {
		return isShowToCustomer;
	}
	public void setIsShowToCustomer(Boolean isShowToCustomer) {
		this.isShowToCustomer = isShowToCustomer;
	}
}
