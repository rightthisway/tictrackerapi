package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="user_preference")
public class UserPreference implements Serializable{

	private Integer id;
	private String userName;
	private String gridName;
	private String columnOrders;
	@JsonIgnore
	private List<String> orderedColumnList;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="user_name")
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@Column(name="grid_name")
	public String getGridName() {
		return gridName;
	}

	public void setGridName(String gridName) {
		this.gridName = gridName;
	}

	@Column(name="column_orders")
	public String getColumnOrders() {
		return columnOrders;
	}
	public void setColumnOrders(String columnOrders) {
		this.columnOrders = columnOrders;
	}
	
	@Transient
	public List<String> getOrderedColumnList() {

		if(columnOrders!=null && !columnOrders.isEmpty()){
			if(orderedColumnList==null && orderedColumnList.isEmpty()){
				orderedColumnList = new ArrayList<String>();
			}
			String columnArray[] = columnOrders.split(",");
			for(String column : columnArray){
				orderedColumnList.add(column);
			}
		}
		return orderedColumnList;
	
	}
	
	
	public void setOrderedColumnList(List<String> orderedColumnList) {
		this.orderedColumnList = orderedColumnList;
	}
	
	
	
	
	
}
