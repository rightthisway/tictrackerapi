package com.rtw.tracker.datas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtw.tracker.enums.ArtistStatus;

@Entity
@Table(name="artist")
public class Artist implements Serializable  {
	
	private Integer id;
	private String name;
	private String parentType;
	private Boolean custFavFlag;
	private Boolean custSuperFanFlag;
	private ArtistStatus artistStatus;
	private String grandChildCategoryName;
	private String childCategoryName;
	private Boolean isDisplay;
	//private GrandChildCategory grandChildCategory;
	
	public Artist() {
		//super();
	}
	
	@Id
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="name")
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	@Column(name="artist_status")
	@Enumerated(EnumType.ORDINAL)
	public ArtistStatus getArtistStatus() {
		return artistStatus;
	}

	public void setArtistStatus(ArtistStatus artistStatus) {
		this.artistStatus = artistStatus;
	}

	@Column(name="display_on_search")
	public Boolean getIsDisplay() {
		return isDisplay;
	}

	public void setIsDisplay(Boolean isDisplay) {
		this.isDisplay = isDisplay;
	}

	@Transient
	public Boolean getCustFavFlag() {
		if(null == custFavFlag ){
			custFavFlag = false;
		}
		return custFavFlag;
	}

	public void setCustFavFlag(Boolean custFavFlag) {
		this.custFavFlag = custFavFlag;
	}

	@Transient
	public Boolean getCustSuperFanFlag() {
		if(null == custSuperFanFlag ){
			custSuperFanFlag = false;
		}
		return custSuperFanFlag;
	}

	public void setCustSuperFanFlag(Boolean custSuperFanFlag) {
		this.custSuperFanFlag = custSuperFanFlag;
	}

	@Transient
	public String getParentType() {
		return parentType;
	}

	public void setParentType(String parentType) {
		this.parentType = parentType;
	}

	@Transient
	public String getGrandChildCategoryName() {
		return grandChildCategoryName;
	}

	public void setGrandChildCategoryName(String grandChildCategoryName) {
		this.grandChildCategoryName = grandChildCategoryName;
	}

	@Transient
	public String getChildCategoryName() {
		return childCategoryName;
	}

	public void setChildCategoryName(String childCategoryName) {
		this.childCategoryName = childCategoryName;
	}
	
	
	
	

}

