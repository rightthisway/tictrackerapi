package com.rtw.tracker.datas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name= "ZZZ_tmp_tm_refunds_KNOWN_emails_upload")
public class TicketMasterRefundKnownEmail implements Serializable {

	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="checkEmail")
	private String checkedEmail;
	
	@Column(name="nameCategory")
	private String nameCategory;
	
	@Column(name="InvoiceDate")
	private String invoiceDate;
	
	@Column(name="RefundReceived")
	private Double refundRec;
	
	@Column(name="RefundDue")
	private Double refundDue;
	
	@Column(name="POId")
	private String poId;
	
	@Column(name="NameOnCC")
	private String ccName;
	
	@Column(name="PODate")
	private String poDate;
	
	@Column(name="Vendor")
	private String vendor;
	
	@Column(name="Event")
	private String eventName;
	
	@Column(name="EventDate")
	private String eventDate;
	
	@Column(name="Venue")
	private String venue;
	
	@Column(name="TicketQty")
	private Integer ticketQty;
	
	@Column(name="RefundQty")
	private Integer refundQty;
	
	@Column(name="RefundCost")
	private Double refundCost;
	
	@Column(name="Section")
	private String section;
	
	@Column(name="Row")
	private String row;
	
	@Column(name="InternalTicketNotes")
	private String internalNotes;
	
	@Column(name="email")
	private String email;
	
	@Column(name="OLDemail")
	private String oldEmail;
	
	@Column(name="ErrorsFixedbyHand")
	private String error;
	
	@Column(name="MergedRecs")
	private Integer mergeRec;
	
	@Column(name="isMapped")
	private Boolean isMapped;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCheckedEmail() {
		return checkedEmail;
	}

	public void setCheckedEmail(String checkedEmail) {
		this.checkedEmail = checkedEmail;
	}

	public String getNameCategory() {
		return nameCategory;
	}

	public void setNameCategory(String nameCategory) {
		this.nameCategory = nameCategory;
	}

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public Double getRefundDue() {
		return refundDue;
	}

	public void setRefundDue(Double refundDue) {
		this.refundDue = refundDue;
	}

	public String getPoId() {
		return poId;
	}

	public void setPoId(String poId) {
		this.poId = poId;
	}

	public String getCcName() {
		return ccName;
	}

	public void setCcName(String ccName) {
		this.ccName = ccName;
	}

	public String getPoDate() {
		return poDate;
	}

	public void setPoDate(String poDate) {
		this.poDate = poDate;
	}

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getEventDate() {
		return eventDate;
	}

	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public Integer getTicketQty() {
		return ticketQty;
	}

	public void setTicketQty(Integer ticketQty) {
		this.ticketQty = ticketQty;
	}

	public Integer getRefundQty() {
		return refundQty;
	}

	public void setRefundQty(Integer refundQty) {
		this.refundQty = refundQty;
	}

	public Double getRefundCost() {
		return refundCost;
	}

	public void setRefundCost(Double refundCost) {
		this.refundCost = refundCost;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getRow() {
		return row;
	}

	public void setRow(String row) {
		this.row = row;
	}

	public String getInternalNotes() {
		return internalNotes;
	}

	public void setInternalNotes(String internalNotes) {
		this.internalNotes = internalNotes;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOldEmail() {
		return oldEmail;
	}

	public void setOldEmail(String oldEmail) {
		this.oldEmail = oldEmail;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}


	public Integer getMergeRec() {
		return mergeRec;
	}

	public void setMergeRec(Integer mergeRec) {
		this.mergeRec = mergeRec;
	}

	public Boolean getIsMapped() {
		return isMapped;
	}

	public void setIsMapped(Boolean isMapped) {
		this.isMapped = isMapped;
	}

	public Double getRefundRec() {
		return refundRec;
	}

	public void setRefundRec(Double refundRec) {
		this.refundRec = refundRec;
	}
	
}
