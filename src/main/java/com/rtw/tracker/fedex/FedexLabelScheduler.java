package com.rtw.tracker.fedex;

import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.InitializingBean;

import com.rtw.tracker.dao.implementation.DAORegistry;

//import ProcessShipmentRequest;

public class FedexLabelScheduler extends TimerTask implements InitializingBean {

	
	CreateFedexLabelUtil util = null;
	
	@Override
	public void afterPropertiesSet() throws Exception {
		TimerTask timerTask = new FedexLabelScheduler();
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE)+5);
		Timer timer = new Timer(true);
		timer.scheduleAtFixedRate(timerTask, cal.getTime(), 5*60*60*1000);
	}

	@Override
	public void run() { 
		if(util == null){
			util = new CreateFedexLabelUtil();
		}
		
		/*List<Integer> invoices = DAORegistry.getInvoiceDAO().getInvoiceTocreateFedexLabel();
		if(invoices!=null && !invoices.isEmpty()){
			for(Integer invoiceId : invoices){
				List<Recipient> recipients=DAORegistry.getInvoiceDAO().getRecipientListByInvoice(invoiceId,true);
				if(recipients!=null && !recipients.isEmpty()){
					 for(Recipient recipient:recipients){
				    	System.out.println("Processing Invoice Number :"+recipient.getInvoiceId());
				    	if(recipient.getCountryCode()!=null && recipient.getCountryCode().equalsIgnoreCase("US"))
				    		util.createFedExShipLabel(recipient,ServiceType.FEDEX_EXPRESS_SAVER,null);
				    	else
				    		util.createFedExShipLabel(recipient,ServiceType.INTERNATIONAL_PRIORITY,null);
					 }
				}
			}
			
		}*/
	}
	
}
