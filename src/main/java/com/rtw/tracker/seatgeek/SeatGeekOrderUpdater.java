package com.rtw.tracker.seatgeek;

import java.io.File;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.InitializingBean;

import com.rtw.tmat.dao.implementation.TMATDAORegistry;
import com.rtw.tmat.data.SeatGeekWSTracking;
import com.rtw.tmat.enums.SeatGeekWSTrackingStatus;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.CustomerOrder;
import com.rtw.tracker.datas.EventDetails;
import com.rtw.tracker.datas.InvoiceTicketAttachment;
import com.rtw.tracker.datas.SeatGeekOrderStatus;
import com.rtw.tracker.datas.SeatGeekOrders;
import com.rtw.tracker.enums.FileType;
import com.rtw.tracker.mail.MailManager;
import com.rtw.tracker.seatgeek.SeatGeekWSUtil;
import com.rtw.tracker.utils.Util;




public class SeatGeekOrderUpdater extends TimerTask implements InitializingBean {

	static MailManager mailManager = null;
	
	
	public static MailManager getMailManager() {
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		SeatGeekOrderUpdater.mailManager = mailManager;
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		TimerTask timerTask = new SeatGeekOrderUpdater();
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE)+1);
		Timer timer = new Timer(true);
		timer.scheduleAtFixedRate(timerTask, cal.getTime(), 5*60*1000);
	}
	
	@Override
	public void run() {
		try {
			if(Util.isContestRunning){
				System.out.println("RETURN");
				return;
			}
			System.out.println("RUNNNNN");
			System.out.println("SGORDER UPDATER Started : "+new Date());
			if(!SeatGeekWSUtil.isSeatGeekAPIEnabled()) {
				System.out.println("SGORDER UPDATER : SeatGeek API access is in disabled mode. : "+new Date());
			} else {
				autoConfirmOrdersUpdate();
				GetAllFailedOrdersFromSeatGeek();
				uploadTicketstoSeatGeekOrders();
			}
			System.out.println("SGORDER UPDATER End : "+new Date());
			
		} catch (Exception e) {
			System.out.println("SGORDER UPDATER ERROR : "+new Date());
			e.printStackTrace();
			
			try {
				 Map<String, Object> map = new HashMap<String, Object>();
				 map.put("error", ""+e.fillInStackTrace());

				 String subject = "SeatGeek Orders Updatr Job Failed : ";
				 String template = "mail-seat-geek-error-message.txt";
				 String fromName  = "tmatmanagers@rightthisway.com";
				 String mimeType = "text/html";
				 String toAddress ="tselvan@rightthisway.com";
				 String ccAddress = "AODev@rightthisway.com";
					
				 mailManager.sendMail(fromName, toAddress, ccAddress, null, subject, template, map, mimeType, null);
			} catch(Exception ex) {
				ex.printStackTrace();
				 System.out.println("SGORDER UPDATER :Error while sending error Email : "+new Date());
			}
		}
	}
	public static void autoConfirmOrdersUpdate() throws Exception {
		
		Date datebefore25Mins = new Date();
		datebefore25Mins.setMinutes(datebefore25Mins.getMinutes()-20);
		
		List<SeatGeekOrders> sgOrdersList = DAORegistry.getSeatGeekOrdersDAO().getAllOpenSeatGeekOrdersForAutoconfirm();
		
		for (SeatGeekOrders sgOrder : sgOrdersList) {
			if(sgOrder.getOrderDate().compareTo(datebefore25Mins) <= 0) {
				String message = SeatGeekWSUtil.updatingOrderStatus(sgOrder.getOrderId(), SeatGeekOrderStatus.confirmed);
				 if(!message.equals("200")) {

					 SeatGeekWSTracking sgWsTrackingOne = new SeatGeekWSTracking(); 
					 sgWsTrackingOne.setAction("Updating Order status");
					 sgWsTrackingOne.setOrderId(sgOrder.getOrderId());
					 sgWsTrackingOne.setCreatedDate(new Date());
					 sgWsTrackingOne.setStatus(SeatGeekWSTrackingStatus.FAILED);
					 sgWsTrackingOne.setMessage("Error While "+SeatGeekOrderStatus.confirmed+" order in Auto order confirmation");
					 sgWsTrackingOne.setError(message);
					 TMATDAORegistry.getSeatGeekWSTrackingDAO().save(sgWsTrackingOne);
				
					continue;
				}
				SeatGeekOrders sgOrdersTemp = new SeatGeekOrders();
				message = SeatGeekWSUtil.getOrderByOrderId(sgOrder.getOrderId(), sgOrdersTemp);
				if(message == null || !message.equals("200")) {
					SeatGeekWSTracking sgWsTrackingOne = new SeatGeekWSTracking(); 
					sgWsTrackingOne.setAction("Getting Orders by Order Id");
					sgWsTrackingOne.setOrderId(sgOrder.getOrderId());
					sgWsTrackingOne.setCreatedDate(new Date());
					sgWsTrackingOne.setStatus(SeatGeekWSTrackingStatus.FAILED);
					sgWsTrackingOne.setMessage("Error While Getting Orders by Order Id in Auto order confirmation");
					sgWsTrackingOne.setError(message);
					TMATDAORegistry.getSeatGeekWSTrackingDAO().save(sgWsTrackingOne);
				
					continue;
				}
				if(!sgOrdersTemp.getStatus().equals(SeatGeekOrderStatus.confirmed)) {
					SeatGeekWSTracking sgWsTrackingOne = new SeatGeekWSTracking(); 
					sgWsTrackingOne.setAction("Updating Order status");
					sgWsTrackingOne.setOrderId(sgOrder.getOrderId());
					sgWsTrackingOne.setCreatedDate(new Date());
					sgWsTrackingOne.setStatus(SeatGeekWSTrackingStatus.FAILED);
					sgWsTrackingOne.setMessage("Order status not updated in seatgeek While updating order to "+SeatGeekOrderStatus.confirmed+" in Auto order confirmation");
					sgWsTrackingOne.setError(message);
					TMATDAORegistry.getSeatGeekWSTrackingDAO().save(sgWsTrackingOne);
					
					//If order is mismatched with API then update in DB
					/*if(!sgOrder.getStatus().equals(sgOrdersTemp.getStatus())) {
						sgOrder.setStatus(sgOrdersTemp.getStatus());
						DAORegistry.getSeatGeekOrdersDAO().update(sgOrder);
					}*/
					continue;
				}
				SeatGeekWSUtil.createCustomerOrder(sgOrder);
				sgOrder.setStatus(SeatGeekOrderStatus.confirmed);
				sgOrder.setLastUpdatedDate(new Date());
				sgOrder.setAcceptedBy("AUTO");
					 
				DAORegistry.getSeatGeekOrdersDAO().update(sgOrder);
			}
		}
	}
	public static void GetAllFailedOrdersFromSeatGeek() throws Exception {
		
		List<SeatGeekOrders> sgOrdersList = new ArrayList<SeatGeekOrders>();
		String message = SeatGeekWSUtil.getOrdersBystatus(SeatGeekOrderStatus.failed,sgOrdersList);
		if(message == null || !message.equals("200")) {

			SeatGeekWSTracking sgWsTrackingOne = new SeatGeekWSTracking(); 
			sgWsTrackingOne.setAction("Getting Orders By Order status");
			//sgWsTrackingOne.setOrderId(sgOrderDB.getOrderId());
			sgWsTrackingOne.setCreatedDate(new Date());
			sgWsTrackingOne.setStatus(SeatGeekWSTrackingStatus.FAILED);
			sgWsTrackingOne.setMessage("Error While getting orders by failed order status in failed order scheduler");
			sgWsTrackingOne.setError(message);
			TMATDAORegistry.getSeatGeekWSTrackingDAO().save(sgWsTrackingOne);
			
		} else {
			for (SeatGeekOrders sgOrder : sgOrdersList) {
				System.out.println("SGORDER UPDATER Processed Order Id : "+sgOrder.getOrderId()+" : status : "+sgOrder.getStatus());
				
				SeatGeekOrders sgOrderDB = DAORegistry.getSeatGeekOrdersDAO().getSeatGeekOrderByOrderId(sgOrder.getOrderId());
				if(sgOrderDB != null) {
					/*if(sgOrderDB.getStatus().equals(SeatGeekOrderStatus.failed)) {
						message = SeatGeekWSUtil.updatingOrderStatus(sgOrderDB.getOrderId(), sgOrderDB.getStatus());
						if(message == null || !message.equals("200")) {
							SeatGeekWSTracking sgWsTrackingOne = new SeatGeekWSTracking(); 
							sgWsTrackingOne.setAction("Updating Order status");
							sgWsTrackingOne.setOrderId(sgOrderDB.getOrderId());
							sgWsTrackingOne.setCreatedDate(new Date());
							sgWsTrackingOne.setStatus(SeatGeekWSTrackingStatus.FAILED);
							sgWsTrackingOne.setMessage("Error While Updating mismatching orders status in failed order scheduler");
							sgWsTrackingOne.setError(message);
							TMATDAORegistry.getSeatGeekWSTrackingDAO().save(sgWsTrackingOne);
							continue;
						}
						SeatGeekOrders sgOrdersTemp = new SeatGeekOrders();
						message = SeatGeekWSUtil.getOrderByOrderId(sgOrder.getOrderId(), sgOrdersTemp);
						if(message == null || !message.equals("200")) {

							SeatGeekWSTracking sgWsTrackingOne = new SeatGeekWSTracking(); 
							sgWsTrackingOne.setAction("Getting Orders by Order Id");
							sgWsTrackingOne.setOrderId(sgOrder.getOrderId());
							sgWsTrackingOne.setCreatedDate(new Date());
							sgWsTrackingOne.setStatus(SeatGeekWSTrackingStatus.FAILED);
							sgWsTrackingOne.setMessage("Error While Getting Orders by Order Id in failed order scheduler");
							sgWsTrackingOne.setError(message);
							TMATDAORegistry.getSeatGeekWSTrackingDAO().save(sgWsTrackingOne);
						
							System.out.println("SGORDER Filed Order Get Message Error Order Id : "+sgOrder.getOrderId()+" : message : "+message);
							continue;
						}
						
						sgOrderDB.setStatus(SeatGeekOrderStatus.submitted);
						sgOrderDB.setLastUpdatedDate(new Date());
						DAORegistry.getSeatGeekOrdersDAO().update(sgOrderDB);
						
					}*/ /*else if(!sgOrderDB.getStatus().equals(sgOrder.getStatus())) {
						SeatGeekWSUtil.updatingOrderStatus(sgOrderDB.getOrderId(), sgOrderDB.getStatus());	
					} */
					
				} else {
					if(sgOrder.getEventId() == null) {
						Integer eventId= TMATDAORegistry.getQueryManagerDAO().getEventIdFromSeatGeekUploadCatsBySeatGeekTicketId(sgOrder.getCatTicketId());
						sgOrder.setEventId(eventId);
					}
					/*message = SeatGeekWSUtil.updatingOrderStatus(sgOrder.getOrderId(), SeatGeekOrderStatus.submitted);
					if(message == null || !message.equals("200")) {
						continue;
					}*/
					//sgOrder.setStatus(SeatGeekOrderStatus.failed);
					DAORegistry.getSeatGeekOrdersDAO().save(sgOrder);
				}
				
			}
		}
	}
	
	public static void uploadTicketstoSeatGeekOrders() throws Exception {
		
		List<Object[]> objArrList = DAORegistry.getSeatGeekOrdersDAO().getTicketUploadedOrders();
		for (Object[] objArray : objArrList) {
			try {
				String sgOrderId = (String)objArray[0];
				//Integer customerOrderId = (Integer)objArray[1];
				Integer invoiceId = (Integer)objArray[2];
				//String realTixFilePath = (String)objArray[3];
				
				SeatGeekOrders sgOrder = DAORegistry.getSeatGeekOrdersDAO().getSeatGeekOrderByOrderId(sgOrderId);
				System.out.println("SGORDER FILE UPLOAD Order Id : "+sgOrder.getOrderId()+" : status : "+sgOrder.getStatus());
				
				List<File> fileList = new ArrayList<File>();
				List<InvoiceTicketAttachment> ticketAttachments = DAORegistry.getInvoiceTicketAttachmentDAO().getAttachmentByInvoiceAndFileType(invoiceId, FileType.ETICKET);
				for (InvoiceTicketAttachment invAttachment : ticketAttachments) {
					File file = new File(invAttachment.getFilePath());
					if(file.exists()) {
						fileList.add(file);
					}
				}
				
				if(!fileList.isEmpty()) {
					String message = SeatGeekWSUtil.uploadTickets(sgOrder.getOrderId(),fileList);
					if(message == null || !message.equals("200")) {

						SeatGeekWSTracking sgWsTrackingOne = new SeatGeekWSTracking(); 
						sgWsTrackingOne.setAction("Uploading Tickets");
						sgWsTrackingOne.setOrderId(sgOrder.getOrderId());
						sgWsTrackingOne.setCreatedDate(new Date());
						sgWsTrackingOne.setStatus(SeatGeekWSTrackingStatus.FAILED);
						sgWsTrackingOne.setMessage("Error While Uploading Tickets in ticket upload scheduler");
						sgWsTrackingOne.setError(message);
						TMATDAORegistry.getSeatGeekWSTrackingDAO().save(sgWsTrackingOne);
					
						System.out.println("SGORDER FILE UPLOAD Message Error Order Id : "+sgOrder.getOrderId()+" : message : "+message);
						continue;
					}
					SeatGeekOrders sgOrdersTemp = new SeatGeekOrders();
					message = SeatGeekWSUtil.getOrderByOrderId(sgOrder.getOrderId(), sgOrdersTemp);
					if(message == null || !message.equals("200")) {

						SeatGeekWSTracking sgWsTrackingOne = new SeatGeekWSTracking(); 
						sgWsTrackingOne.setAction("Getting Orders by Order Id");
						sgWsTrackingOne.setOrderId(sgOrder.getOrderId());
						sgWsTrackingOne.setCreatedDate(new Date());
						sgWsTrackingOne.setStatus(SeatGeekWSTrackingStatus.FAILED);
						sgWsTrackingOne.setMessage("Error While Getting Orders by Order Id in ticket upload scheduler");
						sgWsTrackingOne.setError(message);
						TMATDAORegistry.getSeatGeekWSTrackingDAO().save(sgWsTrackingOne);
					
						System.out.println("SGORDER FILE UPLOAD Get Message Error Order Id : "+sgOrder.getOrderId()+" : message : "+message);
						continue;
					}
					if(!sgOrdersTemp.getStatus().equals(SeatGeekOrderStatus.fulfilled) ||
							sgOrdersTemp.getSeatGeekFileNames() == null) {
						
						SeatGeekWSTracking sgWsTrackingOne = new SeatGeekWSTracking(); 
						sgWsTrackingOne.setAction("Uploading Tickets");
						sgWsTrackingOne.setOrderId(sgOrder.getOrderId());
						sgWsTrackingOne.setCreatedDate(new Date());
						sgWsTrackingOne.setStatus(SeatGeekWSTrackingStatus.FAILED);
						sgWsTrackingOne.setMessage("Ticket not uploaded to Seatgeek in ticket upload scheduler");
						sgWsTrackingOne.setError(message);
						TMATDAORegistry.getSeatGeekWSTrackingDAO().save(sgWsTrackingOne);
					
						System.out.println("SGORDER FILE UPLOAD Get Message Error Order Id : "+sgOrder.getOrderId()+" : message : "+message);
						continue;
					}
					
					sgOrder.setStatus(SeatGeekOrderStatus.fulfilled);
					sgOrder.setSeatGeekFileNames(sgOrdersTemp.getSeatGeekFileNames());
					sgOrder.setLastUpdatedDate(new Date());
						
					DAORegistry.getSeatGeekOrdersDAO().update(sgOrder);
					System.out.println("SGORDER FILE UPLOAD Message Order Id : "+sgOrder.getOrderId()+" : message : "+message);
					
					try {
						int uploadedFileCount = 1;
						String fileNames = sgOrder.getSeatGeekFileNames();
						if(fileNames != null) {
							uploadedFileCount = fileNames.split(",").length;
						}
						Map<String, Object> map = new HashMap<String, Object>();
						map.put("sgOrder", sgOrder);
						map.put("uploadedFileCount", uploadedFileCount);
						EventDetails event =DAORegistry.getEventDetailsDAO().get(sgOrder.getEventId()); 
						map.put("event", event);
						String subject = " Tickets uploaded to Seatgeek for - "+event.getEventName()+", "+event.getEventDateTimeStr()+", "+event.getBuilding();
						String fromName  = "tmatmanagers@rightthisway.com";
						String mimeType = "text/html";
						String toAddress ="fulfillment@rightthisway.com";
						String bccAddress = "AODev@rightthisway.com";
						//String toAddress ="tselvan@rightthisway.com";
						//String bccAddress = null;
						String template = "mail-seat-geek-file-uploads.html";
						
						mailManager.sendMail(fromName, toAddress, null, bccAddress, subject, template, map, mimeType, null);
					} catch(Exception e) {
						e.printStackTrace();
					}
						
				}
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
}
