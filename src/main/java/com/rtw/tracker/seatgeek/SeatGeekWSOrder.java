package com.rtw.tracker.seatgeek;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("response")
public class SeatGeekWSOrder {
	private String status;
	private String error; 
	private String message;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
	
	
}
