package com.rtw.tracker.seatgeek;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.rtw.tmat.dao.implementation.TMATDAORegistry;
import com.rtw.tmat.data.SeatGeekWSTracking;
import com.rtw.tmat.enums.SeatGeekWSTrackingStatus;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.SeatGeekOrderStatus;
import com.rtw.tracker.datas.SeatGeekOrders;

@Controller
@RequestMapping({"/seatgeek"})
public class SeatGeekWSController {
	public static String SEAT_GEEK_USER_NAME="rtwsgk";
	public static String SEAT_GEEK_PASSWORD="rtw123$sgk!*@";
	public static String SEAT_GEEK_TOKEN="QWE4#4PO==+ABCDE";
	//public static String SEAT_GEEK_SOURCE="";
	
	@RequestMapping(value = "/order", method = RequestMethod.POST,produces = "application/json")
	public void seatGeekOrders(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		SeatGeekWSOrder sgWSOrder = new SeatGeekWSOrder();
		SeatGeekWSTracking sgWsTracking = new SeatGeekWSTracking(); 
		sgWsTracking.setAction("Rightthisway API invoked");
		sgWsTracking.setCreatedDate(new Date());
		try{
			System.out.println("SGK user : "+request.getParameter("username"));
			System.out.println("SGK password : "+request.getParameter("password"));
			System.out.println("SGK token : "+request.getParameter("token"));
			
			String userName = request.getParameter("username");
			String password = request.getParameter("password");
			String token = request.getParameter("token");
			String source = request.getParameter("source");
			String orderId = request.getParameter("orderid");
			
			sgWsTracking.setOrderId(orderId);
			
			if(userName == null || !userName.equals(SEAT_GEEK_USER_NAME)) {
				sgWsTracking.setStatus(SeatGeekWSTrackingStatus.FAILED);
				sgWsTracking.setMessage("Invalid UserName");
				sgWsTracking.setError("Invalid UserName");
				TMATDAORegistry.getSeatGeekWSTrackingDAO().save(sgWsTracking);
				
				sgWSOrder.setStatus("Failed");
				sgWSOrder.setError("Invalid UserName");
				Gson gson = new Gson();	
				response.getWriter().write(gson.toJson(sgWSOrder));
				return ;
			}
			if(password == null || !password.equals(SEAT_GEEK_PASSWORD)) {
				sgWsTracking.setStatus(SeatGeekWSTrackingStatus.FAILED);
				sgWsTracking.setMessage("Invalid Password");
				sgWsTracking.setError("Invalid Password");
				TMATDAORegistry.getSeatGeekWSTrackingDAO().save(sgWsTracking);
				
				sgWSOrder.setStatus("Failed");
				sgWSOrder.setError("Invalid Password");
				Gson gson = new Gson();	
				response.getWriter().write(gson.toJson(sgWSOrder));
				return ;
			}
			if(token==null || !token.equals(SEAT_GEEK_TOKEN)) {
				sgWsTracking.setStatus(SeatGeekWSTrackingStatus.FAILED);
				sgWsTracking.setMessage("Invalid Token");
				sgWsTracking.setError("Invalid Token");
				TMATDAORegistry.getSeatGeekWSTrackingDAO().save(sgWsTracking);
				
				sgWSOrder.setStatus("Failed");
				sgWSOrder.setError("Invalid Token");
				Gson gson = new Gson();	
				response.getWriter().write(gson.toJson(sgWSOrder));
				return ;
			}
			
			
			String quantity = request.getParameter("tixquantity");
			String ticketId = request.getParameter("posid");
			String section = request.getParameter("tixsection");
			String row = request.getParameter("tixrow");
			String eventTime = request.getParameter("eventtime");
			String eventDate = request.getParameter("date");
			String venue = request.getParameter("venue");
			String eventName = request.getParameter("event");
			String totalSale = request.getParameter("totalsale");
			String totalPayment = request.getParameter("totalpayment");
			String instantDownload = request.getParameter("instantdownload");
			
			System.out.println("SGORDER API Order Id : "+orderId+" : time : "+new Date()+" : eDate : "+eventDate+" : eTime : "+eventTime);
			SeatGeekOrders sgOrder = new SeatGeekOrders();
			
			if(orderId != null && orderId.trim().length()>0) {
				sgOrder.setOrderId(orderId);
			} else {
				sgWsTracking.setStatus(SeatGeekWSTrackingStatus.FAILED);
				sgWsTracking.setMessage("Invalid OrderId");
				sgWsTracking.setError("Invalid OrderId");
				TMATDAORegistry.getSeatGeekWSTrackingDAO().save(sgWsTracking);
				
				sgWSOrder.setStatus("Failed");
				sgWSOrder.setError("Invalid OrderId");
				Gson gson = new Gson();	
				response.getWriter().write(gson.toJson(sgWSOrder));
				return ;
			}
			if(quantity != null && quantity.trim().length()>0) {
				sgOrder.setQuantity(Integer.parseInt(quantity));
			}
			if(ticketId != null && ticketId.trim().length()>0) {
				sgOrder.setCatTicketId(Integer.parseInt(ticketId));
			}
			if(totalSale != null && totalSale.trim().length()>0) {
				sgOrder.setTotalSaleAmt(Double.parseDouble(totalSale));
			}
			if(totalPayment != null && totalPayment.trim().length()>0) {
				sgOrder.setTotalPaymentAmt(Double.parseDouble(totalPayment));
			}
			if(instantDownload != null && instantDownload.trim().length()>0) {
				sgOrder.setInstantDownload(Integer.parseInt(instantDownload));
			}
			SeatGeekOrders seatGeekOrderDB = DAORegistry.getSeatGeekOrdersDAO().getSeatGeekOrderByOrderId(orderId);
			if(seatGeekOrderDB!= null) {
				sgWsTracking.setStatus(SeatGeekWSTrackingStatus.FAILED);
				sgWsTracking.setMessage("Order Already Exist");
				sgWsTracking.setError("Order Already Exist");
				TMATDAORegistry.getSeatGeekWSTrackingDAO().save(sgWsTracking);
				
				sgWSOrder.setStatus("Failed");
				sgWSOrder.setError("Order already Exist.");
				Gson gson = new Gson();	
				response.getWriter().write(gson.toJson(sgWSOrder));
				return;
			}
			
			sgOrder.setEventName(eventName);
			sgOrder.setVenueName(venue);
			sgOrder.setSection(section);
			sgOrder.setRow(row);
			sgOrder.setEventDate(eventDate);
			sgOrder.setEventTime(eventTime);
			sgOrder.setCreatedDate(new Date());
			sgOrder.setLastUpdatedDate(new Date());
			sgOrder.setSource(source);
			sgOrder.setStatus(SeatGeekOrderStatus.submitted);
			sgOrder.setLastUpdatedBy("AUTO");

			try {
				String message = SeatGeekWSUtil.getOrderByOrderId(orderId, sgOrder);
				if(message == null || !message.equals("200")) {
					SeatGeekWSTracking sgWsTrackingOne = new SeatGeekWSTracking();
					sgWsTrackingOne.setOrderId(orderId);
					sgWsTrackingOne.setAction("Ordres By Id");
					sgWsTrackingOne.setCreatedDate(new Date());
					sgWsTrackingOne.setStatus(SeatGeekWSTrackingStatus.FAILED);
					sgWsTrackingOne.setMessage("Get Orders by Id API failed in Rightthisway Order Creation");
					sgWsTrackingOne.setError(message);
					TMATDAORegistry.getSeatGeekWSTrackingDAO().save(sgWsTrackingOne);
					
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
			Integer tmatEventId= TMATDAORegistry.getQueryManagerDAO().getEventIdFromSeatGeekUploadCatsBySeatGeekTicketId(sgOrder.getCatTicketId());
			sgOrder.setEventId(tmatEventId);
			DAORegistry.getSeatGeekOrdersDAO().save(sgOrder);
			
			sgWsTracking.setStatus(SeatGeekWSTrackingStatus.SUCCESS);
			sgWsTracking.setMessage("Order Created Successfully : "+eventDate+" : "+eventTime);
			TMATDAORegistry.getSeatGeekWSTrackingDAO().save(sgWsTracking);
			
			sgWSOrder.setStatus("Success");
			sgWSOrder.setMessage("Order Created Successfully.");
			Gson gson = new Gson();	
			response.getWriter().write(gson.toJson(sgWSOrder));
			//response.getWriter().write("ok");
			//return sgWSOrder;
		}catch (Exception e) {
			e.printStackTrace();
			
			try {
				sgWsTracking.setStatus(SeatGeekWSTrackingStatus.FAILED);
				sgWsTracking.setMessage("Exception Occured");
				sgWsTracking.setError(""+e.fillInStackTrace());
				TMATDAORegistry.getSeatGeekWSTrackingDAO().save(sgWsTracking);
			}catch(Exception ex) {
				ex.printStackTrace();
			}
			
			sgWSOrder.setStatus("Failed");
			sgWSOrder.setError("Something went wrong. please try again later.");
			Gson gson = new Gson();	
			response.getWriter().write(gson.toJson(sgWSOrder));
			//return sgWSOrder;
		}
		
	}
}
