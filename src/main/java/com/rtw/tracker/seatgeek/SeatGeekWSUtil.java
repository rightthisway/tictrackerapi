package com.rtw.tracker.seatgeek;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.util.FileCopyUtils;

import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.ApplicationPlatform;
import com.rtw.tracker.datas.CategoryTicket;
import com.rtw.tracker.datas.CategoryTicketGroup;
import com.rtw.tracker.datas.Customer;
import com.rtw.tracker.datas.CustomerAddress;
import com.rtw.tracker.datas.CustomerOrder;
import com.rtw.tracker.datas.CustomerOrderDetails;
import com.rtw.tracker.datas.EventDetails;
import com.rtw.tracker.datas.Invoice;
import com.rtw.tracker.datas.ProductType;
import com.rtw.tracker.datas.SeatGeekOrderStatus;
import com.rtw.tracker.datas.SeatGeekOrders;
import com.rtw.tracker.datas.TicketStatus;
import com.rtw.tracker.enums.InvoiceStatus;
import com.rtw.tracker.enums.OrderType;
import com.rtw.tracker.enums.PaymentMethod;
import com.rtw.tracker.enums.SecondaryOrderType;
import com.rtw.tracker.utils.Constants;


public class SeatGeekWSUtil {
	public static String seatGeekBaseURL = "https://sellerdirect-api.seatgeek.com/rightthisway/";
	public static String seatGeekApplicationKey = "application_key=<application_key>";
	public static String seatGeekToken="rPpHs2as1jqNChrp9G2g";
	public static Integer seatGeekApiAccessEnabled = 1;
	public static Integer seatGeekCustomerId = 2;
	
	
	public static String getSeatGeekBaseURL() {
		return seatGeekBaseURL;
	}

	public final void setSeatGeekBaseURL(String seatGeekBaseURL) {
		SeatGeekWSUtil.seatGeekBaseURL = seatGeekBaseURL;
	}

	public static String getSeatGeekApplicationKey() {
		return seatGeekApplicationKey;
	}

	public final void setSeatGeekApplicationKey(String seatGeekApplicationKey) {
		SeatGeekWSUtil.seatGeekApplicationKey = seatGeekApplicationKey;
	}

	public static String getSeatGeekToken() {
		return seatGeekToken;
	}

	public final void setSeatGeekToken(String seatGeekToken) {
		SeatGeekWSUtil.seatGeekToken = seatGeekToken;
	}

	public static Integer getSeatGeekCustomerId() {
		return seatGeekCustomerId;
	}

	public final void setSeatGeekCustomerId(Integer seatGeekCustomerId) {
		SeatGeekWSUtil.seatGeekCustomerId = seatGeekCustomerId;
	}
	
	public static Integer getSeatGeekApiAccessEnabled() {
		return seatGeekApiAccessEnabled;
	}

	public final void setSeatGeekApiAccessEnabled(Integer seatGeekApiAccessEnabled) {
		SeatGeekWSUtil.seatGeekApiAccessEnabled = seatGeekApiAccessEnabled;
	}

	public static boolean isSeatGeekAPIEnabled() throws Exception {
		if(seatGeekApiAccessEnabled != null && seatGeekApiAccessEnabled.equals(1)) {
			return true;
		}
		return false;
	}

	public static String deleteTicket(Integer ticketId) throws Exception {
		try{
			
			if(!isSeatGeekAPIEnabled()) {
				return "SeatGeek API access is in disabled mode.";
			}
			org.apache.http.ssl.SSLContextBuilder builder = new org.apache.http.ssl.SSLContextBuilder();
			SSLConnectionSocketFactory sslsf =null;
			try {
				builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
				sslsf = new SSLConnectionSocketFactory(builder.build());
			} catch (Exception e) {
				e.printStackTrace();
			}
			CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
			//System.out.println(url);
			String url=seatGeekBaseURL+"listings?token="+seatGeekToken+"&listing_id="+ticketId;
			HttpDelete delete = new HttpDelete(url);
			delete.addHeader("Content-Type", "application/json");
			System.out.println(url);
			HttpResponse response = httpClient.execute(delete);

			BufferedReader responseData = new BufferedReader(
			        new InputStreamReader(response.getEntity().getContent()));		
			String result = IOUtils.toString(responseData);
			System.out.println(result);
			String message = null;
			//{"meta":{"status":200}}
			JSONObject jsonObject = new JSONObject(result);
			if(jsonObject.has("error")){
				message = jsonObject.getJSONObject("error").getString("message");
			} else if(jsonObject.has("meta")) {
				JSONObject metaJSONObj = jsonObject.getJSONObject("meta");
				message = metaJSONObj.getString("status");
			}
			System.out.println("message : "+message);
			return message;

		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String updatingOrderStatus(String orderId,SeatGeekOrderStatus sgOrderStatus) throws Exception {
		try{
			
			if(!isSeatGeekAPIEnabled()) {
				return "SeatGeek API access is in disabled mode.";
			}
			
			org.apache.http.ssl.SSLContextBuilder builder = new org.apache.http.ssl.SSLContextBuilder();
			SSLConnectionSocketFactory sslsf =null;
			try {
				builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
				sslsf = new SSLConnectionSocketFactory(builder.build());
			} catch (Exception e) {
				e.printStackTrace();
			}
			CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
			
			String url=seatGeekBaseURL+"order?token="+seatGeekToken+"&status="+sgOrderStatus.toString()+"&order_id="+orderId;
			
			HttpPatch patch = new HttpPatch(url);
			//delete.addHeader("Accept", "application/vnd.ticketnetwork.posapi+json; version=1.0");
			patch.addHeader("Content-Type", "application/json");
			System.out.println(url);
			HttpResponse response = httpClient.execute(patch);

			BufferedReader responseData = new BufferedReader(
			        new InputStreamReader(response.getEntity().getContent()));		
			String result = IOUtils.toString(responseData);
			
			String message = null;
			JSONObject jsonObject = new JSONObject(result);
			if(jsonObject.has("error")){
				message = jsonObject.getJSONObject("error").getString("message");
			} else if(jsonObject.has("meta")) {
				JSONObject metaJSONObj = jsonObject.getJSONObject("meta");
				message = metaJSONObj.getString("status");
			}
			System.out.println(result+" : "+message);
			return message;

		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String getOrdersBystatus(SeatGeekOrderStatus sgOrderStatus,List<SeatGeekOrders> sgOrdersList) throws Exception {
		
		String message = null;
		try{
			if(!isSeatGeekAPIEnabled()) {
				return "SeatGeek API access is in disabled mode.";
			}
			
			org.apache.http.ssl.SSLContextBuilder builder = new org.apache.http.ssl.SSLContextBuilder();
			SSLConnectionSocketFactory sslsf =null;
			try {
				builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
				sslsf = new SSLConnectionSocketFactory(builder.build());
			} catch (Exception e) {
				e.printStackTrace();
			}
			CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
			String url=seatGeekBaseURL+"orders?token="+seatGeekToken+"&status="+sgOrderStatus.toString()+"&page_number=1&per_page=1000";
			HttpGet get = new HttpGet(url);
			get.addHeader("Content-Type", "application/json");
			System.out.println(url);
			HttpResponse response = httpClient.execute(get);

			BufferedReader responseData = new BufferedReader(
			        new InputStreamReader(response.getEntity().getContent()));		
			
			String result = IOUtils.toString(responseData);
			System.out.println(result);
			//{"meta":{"status":200}}
			DateFormat eventDateFmt = new SimpleDateFormat("yyyy-MM-dd");
			DateFormat eventTimeFmt = new SimpleDateFormat("HH:mm:ss");
			DateFormat orderDateFmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			
			JSONObject jsonObject = new JSONObject(result);
			if(jsonObject.has("error")){
				message = jsonObject.getJSONObject("error").getString("message");
			} else if(jsonObject.has("meta")) {
				JSONObject metaJSONObj = jsonObject.getJSONObject("meta");
				message = metaJSONObj.getString("status");
			//} 
				if(jsonObject.has("orders")){
				
				JSONArray ordersJsonArray = jsonObject.getJSONArray("orders");
				for (int i = 0 ; i < ordersJsonArray.length() ; i++) {
					SeatGeekOrders sgOrder = new SeatGeekOrders();
					JSONObject orderJSONObject = ordersJsonArray.getJSONObject(i);
					sgOrder.setOrderId(orderJSONObject.getString("id"));
					sgOrder.setStatus(SeatGeekOrderStatus.valueOf(orderJSONObject.getString("status")));
					sgOrder.setDeliveryMethod(orderJSONObject.getString("delivery"));
					String createdDateStr = orderJSONObject.getString("created");
					if(createdDateStr != null ) {
						//sgOrder.setOrderDate(orderDateFmt.parse(createdDateStr));
					}
					sgOrder.setOrderDate(new Date());
					
					String total = orderJSONObject.getString("total");
					if(total!= null && !total.isEmpty()) {
						sgOrder.setTotalAmt(Double.parseDouble(total));
						if(sgOrder.getTotalSaleAmt()== null) {
							sgOrder.setTotalSaleAmt(Double.parseDouble(total));
						}
					}
					String subTotal = orderJSONObject.getString("subtotal");
					if(subTotal!= null && !subTotal.isEmpty()) {
						sgOrder.setSubtotalAmt(Double.parseDouble(subTotal));
						if(sgOrder.getTotalPaymentAmt()== null) {
							sgOrder.setTotalPaymentAmt(Double.parseDouble(subTotal));
						}
					}
					String fees = orderJSONObject.getString("fees");
					if(fees!= null && !fees.isEmpty()) {
						sgOrder.setFeesAmt(Double.parseDouble(fees));
					}
					
					JSONObject jsonEvent = orderJSONObject.getJSONObject("event");
					sgOrder.setEventName(jsonEvent.getString("name"));
					sgOrder.setVenueName(jsonEvent.getString("venue"));
					String eventDateStr = jsonEvent.getString("date");
					if(eventDateStr != null && !eventDateStr.equals("TBD")) {
						Date eventDate = eventDateFmt.parse(eventDateStr);
						//sgOrder.setEventDate(eventDate);
						sgOrder.setEventDate(eventDateStr);
					}
					String eventTimeStr = jsonEvent.getString("time");
					if(eventTimeStr != null && !eventDateStr.equals("TBD")) {
						Date eventTime = eventTimeFmt.parse(eventTimeStr);
						//sgOrder.setEventTime(eventTime);
						sgOrder.setEventTime(eventTimeStr);
					}
					
					JSONObject jsonTicket = orderJSONObject.getJSONObject("listing");
					sgOrder.setSection(jsonTicket.getString("section"));
					sgOrder.setRow(jsonTicket.getString("row"));
					String price = jsonTicket.getString("price");
					if(price!= null && !price.isEmpty()) {
						sgOrder.setTicketSoldPrice(Double.parseDouble(price));	
					}
					String quantity = jsonTicket.getString("quantity");
					if(quantity!= null && !quantity.isEmpty()) {
						sgOrder.setQuantity(Integer.parseInt(quantity));	
					}
					String ticketId = jsonTicket.getString("id");
					if(ticketId!= null && !ticketId.isEmpty()) {
						sgOrder.setCatTicketId(Integer.parseInt(ticketId));	
					}
					if(jsonObject.has("files")) {
						String fileNames = "";
						JSONArray filesArr = jsonObject.getJSONArray("files");
						int length = filesArr.length();
						for (int j=0;j<length;j++) {
							fileNames = fileNames +","+filesArr.getString(j);
						}
						if(!fileNames.equals("")) {
							fileNames = fileNames.substring(1);
							sgOrder.setSeatGeekFileNames(fileNames);
						}
					}
					
					sgOrder.setCreatedDate(new Date());
					sgOrder.setLastUpdatedDate(new Date());
					sgOrder.setLastUpdatedBy("AUTO");
					sgOrdersList.add(sgOrder);
				}
				}
				
			}
			//System.out.println("message : "+message);

		}catch (Exception e) {
			e.printStackTrace();
			//return null;
		}
		return message;
	}
	
	public static String getOrderByOrderId(String orderId,SeatGeekOrders sgOrder) throws Exception {
		String message = null;
		try{
			if(!isSeatGeekAPIEnabled()) {
				return "SeatGeek API access is in disabled mode.";
			}
			if(sgOrder .getCreatedDate() == null) {
				sgOrder.setCreatedDate(new Date());
				sgOrder.setLastUpdatedDate(new Date());
				sgOrder.setLastUpdatedBy("AUTO");
			}
			
			org.apache.http.ssl.SSLContextBuilder builder = new org.apache.http.ssl.SSLContextBuilder();
			SSLConnectionSocketFactory sslsf =null;
			try {
				builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
				sslsf = new SSLConnectionSocketFactory(builder.build());
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			String url=seatGeekBaseURL+"order?token="+seatGeekToken+"&order_id="+orderId;
			HttpGet get = new HttpGet(url);
			get.addHeader("Content-Type", "application/json");
			System.out.println(url);
			CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
			HttpResponse response = httpClient.execute(get);

			BufferedReader responseData = new BufferedReader(
			        new InputStreamReader(response.getEntity().getContent()));		
			String result = IOUtils.toString(responseData);
			
			
			DateFormat eventDateFmt = new SimpleDateFormat("yyyy-MM-dd");
			DateFormat eventTimeFmt = new SimpleDateFormat("HH:mm:ss");
			DateFormat orderDateFmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			
			JSONObject jsonObject = new JSONObject(result);
			if(jsonObject.has("error")){
				message = jsonObject.getJSONObject("error").getString("message");
			} else if(jsonObject.has("meta")) {
				JSONObject metaJSONObj = jsonObject.getJSONObject("meta");
				message = metaJSONObj.getString("status");
			//}else if(jsonObject.has("id")){
				sgOrder.setOrderId(jsonObject.getString("id"));
				sgOrder.setStatus(SeatGeekOrderStatus.valueOf(jsonObject.getString("status")));
				sgOrder.setDeliveryMethod(jsonObject.getString("delivery"));
				String createdDateStr = jsonObject.getString("created");
				if(createdDateStr != null ) {
					//sgOrder.setOrderDate(orderDateFmt.parse(createdDateStr));
				}
				sgOrder.setOrderDate(new Date());
				
				String total = jsonObject.getString("total");
				if(total!= null && !total.isEmpty()) {
					sgOrder.setTotalAmt(Double.parseDouble(total));
					if(sgOrder.getTotalSaleAmt()== null) {
						sgOrder.setTotalSaleAmt(Double.parseDouble(total));
					}
				}
				String subTotal = jsonObject.getString("subtotal");
				if(subTotal!= null && !subTotal.isEmpty()) {
					sgOrder.setSubtotalAmt(Double.parseDouble(subTotal));
					if(sgOrder.getTotalPaymentAmt()== null) {
						sgOrder.setTotalPaymentAmt(Double.parseDouble(subTotal));
					}
				}
				String fees = jsonObject.getString("fees");
				if(fees!= null && !fees.isEmpty()) {
					sgOrder.setFeesAmt(Double.parseDouble(fees));
				}
				
				JSONObject jsonEvent = jsonObject.getJSONObject("event");
				sgOrder.setEventName(jsonEvent.getString("name"));
				sgOrder.setVenueName(jsonEvent.getString("venue"));
				String eventDateStr = jsonEvent.getString("date");
				if(eventDateStr != null && !eventDateStr.equals("TBD")) {
					Date eventDate = eventDateFmt.parse(eventDateStr);
					//sgOrder.setEventDate(eventDate);
					sgOrder.setEventDate(eventDateStr);
				}
				String eventTimeStr = jsonEvent.getString("time");
				if(eventTimeStr != null && !eventDateStr.equals("TBD")) {
					Date eventTime = eventTimeFmt.parse(eventTimeStr);
					//sgOrder.setEventTime(eventTime);
					sgOrder.setEventTime(eventTimeStr);
				}
				
				JSONObject jsonTicket = jsonObject.getJSONObject("listing");
				sgOrder.setSection(jsonTicket.getString("section"));
				sgOrder.setRow(jsonTicket.getString("row"));
				String price = jsonTicket.getString("price");
				if(price!= null && !price.isEmpty()) {
					sgOrder.setTicketSoldPrice(Double.parseDouble(price));	
				}
				String quantity = jsonTicket.getString("quantity");
				if(quantity!= null && !quantity.isEmpty()) {
					sgOrder.setQuantity(Integer.parseInt(quantity));	
				}
				String ticketId = jsonTicket.getString("id");
				if(ticketId!= null && !ticketId.isEmpty()) {
					sgOrder.setCatTicketId(Integer.parseInt(ticketId));	
				}
				if(jsonObject.has("files")) {
					String fileNames = "";
					JSONArray filesArr = jsonObject.getJSONArray("files");
					int length = filesArr.length();
					for (int i=0;i<length;i++) {
						fileNames = fileNames +","+filesArr.getString(i);
					}
					if(!fileNames.equals("")) {
						fileNames = fileNames.substring(1);
						sgOrder.setSeatGeekFileNames(fileNames);
					}
				}
				
			}
			System.out.println(result+" : "+message);

		}catch (Exception e) {
			e.printStackTrace();
		}
		return message;
	}
	
	public static String uploadTickets(String orderId,List<File> fileList) throws Exception {
		try{
			
			if(!isSeatGeekAPIEnabled()) {
				return "SeatGeek API access is in disabled mode.";
			}
			org.apache.http.ssl.SSLContextBuilder builder = new org.apache.http.ssl.SSLContextBuilder();
			SSLConnectionSocketFactory sslsf =null;
			try {
				builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
				sslsf = new SSLConnectionSocketFactory(builder.build());
			} catch (Exception e) {
				e.printStackTrace();
			}
			CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
			
			String url=seatGeekBaseURL+"order?token="+seatGeekToken;//+"&order_id="+orderId;
			
			HttpPut httpPut = new HttpPut(url);
			//delete.addHeader("Accept", "application/vnd.ticketnetwork.posapi+json; version=1.0");
			httpPut.addHeader("Host","sellerdirect-api.seatgeek.com");
			httpPut.addHeader("Content-Type", "application/x-www-form-urlencoded");
			
			String str = "{\"order_id\":\""+orderId+"\",\"files\":[" ;
			boolean firstFlag = true;
			String fileStr = "";
			for (File file : fileList) {
				if(!firstFlag) {
					fileStr = fileStr + ",";
				}
				//File file = new File("C:\\Users\\tselvan.AO09\\Desktop\\10161_0.pdf");
				String pdfBase64Format = new String(Base64.encodeBase64(FileUtils.readFileToByteArray((file))));
				fileStr = fileStr + "{\"file\":\""+pdfBase64Format+"\"}" ;
				firstFlag = false;
			}
						
			str = str+ fileStr + "]}";
			StringEntity params =new StringEntity(str);
			//httpPut.addHeader("content-type", "application/json");
			httpPut.setEntity(params);

			System.out.println(url);
			System.out.println(str);
			HttpResponse response = httpClient.execute(httpPut);

			BufferedReader responseData = new BufferedReader(
			        new InputStreamReader(response.getEntity().getContent()));		
			String result = IOUtils.toString(responseData);
			System.out.println(result);
			String message = null;
			JSONObject jsonObject = new JSONObject(result);
			if(jsonObject.has("error")){
				message = jsonObject.getJSONObject("error").getString("message");
			} else if(jsonObject.has("meta")) {
				JSONObject metaJSONObj = jsonObject.getJSONObject("meta");
				message = metaJSONObj.getString("status");
			}
			System.out.println("message : "+message);
			return message;

		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static void main(String[] args) throws Exception {
		//deleteTicket(14240103);
		//updatingOrderStatus("g66c72ww", SeatGeekOrderStatus.confirmed);
		SeatGeekOrders sgOrder = new SeatGeekOrders();
		getOrderByOrderId("g66c72ww",sgOrder);
		//getOrdersBystatus(SeatGeekOrderStatus.failed);
		
		/*List<File> fileList = new ArrayList<File>();
		File file = new File("C:\\trans-siberian-orchestra_220272-Sec_109_RJJ_S14.pdf");
		if(file.exists()) {
			fileList.add(file);
		}
		file = new File("C:\\trans-siberian-orchestra_220273-Sec_109_RJJ_S13.pdf");
		if(file.exists()) {
			fileList.add(file);
		}
		uploadTickets("g66cd5o",fileList);*/
		
		
		/*String pdfBase64Format = new String(Base64.encodeBase64(FileUtils.readFileToByteArray((file))));
		byte[] bytes = pdfBase64Format.getBytes();
		byte[] bytesNew = Base64.decodeBase64(bytes);
		//String fullFileName = Constants.RTW_cards_Path+fileName;
		File newFile = new File("C:\\Users\\tselvan.AO09\\Desktop\\10161_0_test.pdf");
		if (!newFile.exists()) {
			newFile.createNewFile();
		}
		FileUtils.writeByteArrayToFile(new File("C:\\Users\\tselvan.AO09\\Desktop\\10161_0_test.pdf"), bytesNew);
		System.out.println("done...");*/
	}
	
	public static String doBase64Encryption(String originalText){
	  byte[] encodedBytes = Base64.encodeBase64(originalText.getBytes());
	  String encryptedText = new String(encodedBytes);
	  return encryptedText;
	}
	
	public static void createCustomerOrder(SeatGeekOrders sgOrder) throws Exception {
		
		CustomerOrder orderDB = DAORegistry.getCustomerOrderDAO().getCustomerOrderBySeatGeekOrderId(sgOrder.getOrderId());
		if(orderDB != null) {
			System.err.println("Customer Orders Already created for SG order : "+sgOrder.getOrderId()+" : "+new Date());
			return;
		}
		
		//Customer customer = DAORegistry.getCustomerDAO().get(SEAT_GEEK_CUSTOMER_ID);
		EventDetails event = DAORegistry.getEventDetailsDAO().getEventById(sgOrder.getEventId());
		
		Date now = new Date();
		CategoryTicketGroup catTixGroup = new CategoryTicketGroup();
		catTixGroup.setBroadcast(false);
		catTixGroup.setCost(0.0);
		catTixGroup.setCreatedBy("AUTO");
        catTixGroup.setCreatedDate(now);
        catTixGroup.setEventId(sgOrder.getEventId());
        catTixGroup.setExternalNotes(null);
        catTixGroup.setFacePrice(0.0);
        catTixGroup.setInternalNotes("SEATGEEK");
        catTixGroup.setLastUpdatedDate(now);
        catTixGroup.setMarketPlaceNotes(null);
        catTixGroup.setMaxShowing(null);
        catTixGroup.setNearTermDisplayOption(null);
        catTixGroup.setPrice(sgOrder.getTicketSoldPrice());
        catTixGroup.setProducttype(ProductType.SEATGEEK); //confirm
        catTixGroup.setQuantity(sgOrder.getQuantity());
        catTixGroup.setRetailPrice(sgOrder.getTicketSoldPrice());
        catTixGroup.setRow(sgOrder.getRow());
        catTixGroup.setRowRange(null);
        catTixGroup.setSeatHigh(null);
        catTixGroup.setSeatLow(null);
        catTixGroup.setSection(sgOrder.getSection());
        catTixGroup.setSectionRange(null);
        catTixGroup.setShippingMethod(sgOrder.getDeliveryMethod());
        catTixGroup.setShippingMethodId(1);//Eticket
        
        catTixGroup.setSoldPrice(sgOrder.getTicketSoldPrice());
        catTixGroup.setSoldQuantity(sgOrder.getQuantity());
        catTixGroup.setStatus(TicketStatus.SOLD);
        catTixGroup.setTicketOnhandStatus(null);
        catTixGroup.setWholeSalePrice(sgOrder.getTicketSoldPrice());
        catTixGroup.setInvoiceId(null);
        catTixGroup.setTaxAmount(0.0);

        DAORegistry.getCategoryTicketGroupDAO().save(catTixGroup);

        List<CategoryTicket> catTicketList = new ArrayList<CategoryTicket>();
        if(catTixGroup.getQuantity()>0){
              CategoryTicket catTicket = null;
              for(int i=0;i<catTixGroup.getQuantity();i++){
                    catTicket = new CategoryTicket();
                    catTicket.setActualPrice(catTixGroup.getPrice());
                    catTicket.setCategoryTicketGroupId(catTixGroup.getId());
                    catTicket.setExchangeRequestId(null);
                    catTicket.setFillDate(now);
                    //catTicket.setInvoiceId(invoice.getId());
                    catTicket.setIsProcessed(true);
                    catTicket.setTicketId(null);
                    catTicket.setUserName("AUTO");
                    catTicket.setSoldPrice(catTixGroup.getSoldPrice());
                    catTicketList.add(catTicket);
              }
              DAORegistry.getCategoryTicketDAO().saveAll(catTicketList);
        }

		
		CustomerOrder customerOrder = new CustomerOrder();
		customerOrder.setCustomerId(seatGeekCustomerId);
		customerOrder.setQty(catTixGroup.getSoldQuantity());
		customerOrder.setStatus("ACTIVE");
		customerOrder.setEventId(catTixGroup.getEventId());
		customerOrder.setSection(catTixGroup.getSection());
		customerOrder.setRow(catTixGroup.getRow());
		customerOrder.setSeatHigh(null);
		customerOrder.setSeatLow(null);
		customerOrder.setTicketPrice(catTixGroup.getPrice());
		customerOrder.setSoldPrice(catTixGroup.getSoldPrice());
		customerOrder.setOrderTotal(catTixGroup.getSoldPrice()*catTixGroup.getSoldQuantity());
		customerOrder.setDiscountAmt(0.00);
		customerOrder.setTixGroupNotes(null);
		customerOrder.setCreatedDate(new Date());
		customerOrder.setLastUpdated(new Date());
		//customerOrder.setAcceptedDate(new Date());
		customerOrder.setAcceptedBy(null);
		customerOrder.setShippingMethodId(catTixGroup.getShippingMethodId());
		customerOrder.setAcceptedBy(null);
		customerOrder.setOrderType(OrderType.REGULAR);
		customerOrder.setSecondaryOrdertype(SecondaryOrderType.SEATGEEK);
		customerOrder.setSecondaryOrderId(sgOrder.getOrderId());
		
		customerOrder.setProductType(ProductType.SEATGEEK.toString());
		customerOrder.setPlatform(ApplicationPlatform.TICK_TRACKER);
		customerOrder.setCategoryTicketGroupId(catTixGroup.getId());
		customerOrder.setOriginalOrderTotal(catTixGroup.getPrice()*catTixGroup.getSoldQuantity());
		customerOrder.setPrimaryPaymentMethod(PaymentMethod.FULL_REWARDS);
		customerOrder.setPrimaryTransactionId("");
		
		if(event != null) {
			customerOrder.setVenueId(event.getVenueId());
			customerOrder.setEventName(event.getEventName());
			customerOrder.setEventDate(event.getEventDate());
			customerOrder.setEventTime(event.getEventTime());
			customerOrder.setVenueName(event.getBuilding());
			customerOrder.setVenueCategory(event.getVenueCategoryName());
			customerOrder.setVenueCity(event.getCity());
			customerOrder.setVenueState(event.getState());
			customerOrder.setVenueCountry(event.getCountry());
			Date shippingDate = new Date(event.getEventDate().getTime());
			shippingDate.setDate(shippingDate.getDate()-1);
			customerOrder.setShippingDate(shippingDate);
		}
		
		DAORegistry.getCustomerOrderDAO().save(customerOrder);
		
		Invoice invoice = null;
		if(customerOrder != null){
			invoice = new Invoice();
			invoice.setCustomerOrderId(customerOrder.getId());
			invoice.setInvoiceTotal(customerOrder.getOrderTotal());
			invoice.setTicketCount(catTixGroup.getSoldQuantity());
			invoice.setCustomerId(seatGeekCustomerId);
			invoice.setExtPONo(null);
			invoice.setStatus(InvoiceStatus.Outstanding);
			invoice.setInvoiceType("INVOICE");
			invoice.setCreatedDate(new Date());
			invoice.setCreatedBy("AUTO");
			invoice.setLastUpdated(new Date());
			invoice.setIsSent(false);
			invoice.setSentDate(null);
			invoice.setRealTixMap("No");
			invoice.setShippingMethodId(catTixGroup.getShippingMethodId());
			DAORegistry.getInvoiceDAO().save(invoice);
			
			//CategoryTicketGroup categoryTicketByEventId = DAORegistry.getCategoryTicketGroupDAO().getCategoryTicketByEventId(eventId);
			if(invoice.getId() != null) {
				DAORegistry.getCategoryTicketGroupDAO().updateCategoryTicket(catTixGroup.getId(),invoice.getId(),customerOrder.getSoldPrice());
			}
			CustomerOrderDetails customerOrderDetails = null;
			if(customerOrder != null){
				customerOrderDetails = new CustomerOrderDetails();
				customerOrderDetails.setOrderId(customerOrder.getId());
	
				CustomerAddress billingAddress = DAORegistry.getCustomerAddressDAO().getCustomerAddressInfo(seatGeekCustomerId);
				CustomerAddress shippingAddress = DAORegistry.getCustomerAddressDAO().getCustomerAddressInfo(seatGeekCustomerId);
				if(billingAddress != null) {
					customerOrderDetails.setBlAddress1(billingAddress.getAddressLine1());
					customerOrderDetails.setBlAddress2(billingAddress.getAddressLine2());
					customerOrderDetails.setBlFirstName(billingAddress.getFirstName());
					customerOrderDetails.setBlLastName(billingAddress.getLastName());
					customerOrderDetails.setBlCity(billingAddress.getCity());
					customerOrderDetails.setBlZipCode(billingAddress.getZipCode());
					customerOrderDetails.setBlPhone1(billingAddress.getPhone1());
					customerOrderDetails.setBlPhone2(billingAddress.getPhone2());
					if(billingAddress.getCountry()!= null){
						customerOrderDetails.setBlCountry(billingAddress.getCountry().getId());
						customerOrderDetails.setBlCountryName(billingAddress.getCountry().getName());
					}
					if(billingAddress.getState()!= null){
						customerOrderDetails.setShState(billingAddress.getState().getId());
						customerOrderDetails.setShStateName(billingAddress.getState().getName());
					}
				}
				if(shippingAddress != null) {
					customerOrderDetails.setShFirstName(shippingAddress.getFirstName());
					customerOrderDetails.setShLastName(shippingAddress.getLastName());
					customerOrderDetails.setShAddress1(shippingAddress.getAddressLine1());
					customerOrderDetails.setShAddress2(shippingAddress.getAddressLine2());
					customerOrderDetails.setShCity(shippingAddress.getCity());
					customerOrderDetails.setShPhone1(shippingAddress.getPhone1());
					customerOrderDetails.setShZipCode(shippingAddress.getZipCode());
					if(shippingAddress.getCountry()!= null){
						customerOrderDetails.setShCountry(shippingAddress.getCountry().getId());
						customerOrderDetails.setShCountryName(shippingAddress.getCountry().getName());
					}
					if(shippingAddress.getState()!= null){
						customerOrderDetails.setShState(shippingAddress.getState().getId());
						customerOrderDetails.setShStateName(shippingAddress.getState().getName());
					}
				}
					
				DAORegistry.getCustomerOrderDetailsDAO().save(customerOrderDetails);
			}
		}
	}
	
}
