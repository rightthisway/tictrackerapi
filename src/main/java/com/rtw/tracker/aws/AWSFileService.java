package com.rtw.tracker.aws;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.transfer.Copy;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.rtw.tracker.utils.URLUtil;

public class AWSFileService {

	public static void main(String[] args) throws IOException {
		//generaluploads/442485_1559837304432.mp4
		
		//downLoadFileFromS3(BUCKET_NAME_PUBLIC_UPLOAD, "lbernad_351111_1561414689408.3gp", "generaluploads");
		
		/*upLoadFileToS3("","3814_Carbonite_Optical_Mouse_AWM0804_150_thumb.png", "generaluploads",
				new File("C:\\TMPAWS\\3814_Carbonite_Optical_Mouse_AWM0804_150_thumb.png"));*/
		
//		deleteFilesFromS3("lbernad_351111_1561414689408.3gp", false);
	}

	public static  String  getAWSBaseURL () {
		return awsHttpURL;
	}
	
	private static String awsHttpURL = "http://d2vtn8rh18hkyv.cloudfront.net/";
	private static String AK = "AKIARJICTKFNN5SWY6VE";
	private static String ASA = "jF1WVZqR7n4wNkPJB3bLIqpg9BgcpxvbDX+JplOp";
	private static String BUCKET_NAME_PUBLIC_UPLOAD = "rtfpublicupload";
	
	private static String BUCKET_NAME_RTFMEDIA = "rtfmedia";
	public static String BUCKET_NAME_RTF_SERVER_MEDIA = "rtfservermedia";
	public static String BUCKET_RTF_ASSETS= "rtfassetz";
	
	
	//private static String CUSTOMER_VIDEO_WINNER_FOLDER = "winneruploads";
	//private static String CUSTOMER_VIDEO_GENERAL_FOLDER = "generaluploads";
	private static String MEDIA_CATEGORY_ICON_FOLDER = "mediatypeicons";
	public static String POLLING_SPONSOR_LOGO_FOLDER = "splogo";
	public static String TICKET_FOLDER = "tixfile";
	public static String GIFTCARD_BRAND_FOLDER = "gfc";
	public static String GIFTCARD_FOLDER = "gfcfile";
	public static String RTFPROD_FOLDER = "dpd";
	
	public static String POLLING_VIDEO_THUMBNAIL_FOLDER = "thumbnail";
	public static String AWS_BASE_URL = "https://rtfservermedia.s3.amazonaws.com/";
	public static String AWS_MEDIA_URL = "https://rtfmedia.s3.amazonaws.com/";
	public static String AWS_FANCLUB_MEDIA_URL = "https://customermediaupload.s3.amazonaws.com/";
	
	
	
	public static String CUSTOMER_MEDIA_BACKET = "customermediaupload";
	
	public static String FANCLUB_POSTER_FOLDER = "fanclub/poster";
	public static String FANCLUB_IMAGE_FOLDER = "fanclub/image";
	public static String FANCLUB_VIDEO_FOLDER = "fanclub/video";
	public static String FANCLUB_EVENT_IMAGE_FOLDER = "fanclub/events/image";
	
	public static String FANCLUB_POST_POSTER_FOLDER = "fanclub/posts/image";
	public static String FANCLUB_POST_IMAGE_FOLDER = "fanclub/posts/poster";
	public static String FANCLUB_POST_VIDEO_FOLDER = "fanclub/posts/video";
	
	public static String FANCLUB_COMMENT_POSTER_FOLDER = "fanclub/cmt/image";
	public static String FANCLUB_COMMENT_IMAGE_FOLDER = "fanclub/cmt/poster";
	public static String FANCLUB_COMMENT_VIDEO_FOLDER = "fanclub/cmt/video";
	
	
	
	
	
	//generaluploads/442485_1559837304432.mp4
	
	public static String getRTFTVMediaBucketName(){
		return BUCKET_NAME_RTFMEDIA;
	}
	
	public static String getPublicUploadMediaBucketName(){
		return BUCKET_NAME_PUBLIC_UPLOAD;
	}
	
	public static String getUploadMediaIconFolder(){
		return MEDIA_CATEGORY_ICON_FOLDER;
	}
	
	/*public static String getCustomerMediaFolderName(boolean isWinnerUpload){
		if(isWinnerUpload) {
			return CUSTOMER_VIDEO_WINNER_FOLDER;
		}else{
			return CUSTOMER_VIDEO_GENERAL_FOLDER;
		}	
	}*/
	

	public static AwsS3Response upLoadFileToS3(String bucketName , String fileName, String folderName, File fileObj) {
		AwsS3Response awsS3Response = new AwsS3Response();
		try {

			AWSCredentials credentials = new BasicAWSCredentials(AK, ASA);

			AmazonS3 s3client = AmazonS3ClientBuilder.standard()
					.withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.US_EAST_1)
					.build();
			String fileNameToUpload = folderName + "/" + fileName;
			s3client.putObject(new PutObjectRequest(bucketName, fileNameToUpload, fileObj));

			awsS3Response.setStatus(1);
			awsS3Response.setFullFilePath(fileNameToUpload);
			awsS3Response.setDesc("File has been Uploaded Successfully");

		} catch (AmazonServiceException ase) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc("Something went wrong while uploading file");

			System.out.println("Caught an AmazonServiceException, which means your request made it "
					+ "to Amazon S3, but was rejected with an error response for some reason.");
			System.out.println("Error Message:    " + ase.getMessage());
			System.out.println("HTTP Status Code: " + ase.getStatusCode());
			System.out.println("AWS Error Code:   " + ase.getErrorCode());
			System.out.println("Error Type:       " + ase.getErrorType());
			System.out.println("Request ID:       " + ase.getRequestId());
		} catch (AmazonClientException ace) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc("Something went wrong while uploading file");
			System.out.println("Caught an AmazonClientException, which means the client encountered "
					+ "a serious internal problem while trying to communicate with S3, "
					+ "such as not being able to access the network.");
			System.out.println("Error Message: " + ace.getMessage());
		} catch (Exception ex) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc("Something went wrong while uploading file");
			ex.printStackTrace();
		}
		return awsS3Response;

	}
	
	

		public static AwsS3Response transferCustomerMediaFileToRTFMedia(String from_bucket, String from_Path, String to_bucket, String to_Path) {	
		
			AwsS3Response awsS3Response = new AwsS3Response();	
			AWSCredentials credentials = new BasicAWSCredentials(AK, ASA);
			AmazonS3 s3client = AmazonS3ClientBuilder.standard()
			.withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.US_EAST_1)
			.build();
			
			TransferManager xfer_mgr = TransferManagerBuilder.standard().withS3Client(s3client).build();
			try {
				Copy xfer = xfer_mgr.copy(from_bucket, from_Path, to_bucket, to_Path);
				
				xfer.waitForCompletion();
				if (xfer.isDone()) {
				awsS3Response.setStatus(1);
				awsS3Response.setFullFilePath(to_Path);
				awsS3Response.setDesc("File has been Transferred Successfully");
				}
			
			} catch (AmazonServiceException ase) {
				awsS3Response.setStatus(0);
				awsS3Response.setDesc("Something went wrong while Transferring file");
				System.out.println("Caught an AmazonServiceException, which means your request made it "
				+ "to Amazon S3, but was rejected with an error response for some reason.");
				System.out.println("Error Message: " + ase.getMessage());
				System.out.println("HTTP Status Code: " + ase.getStatusCode());
				System.out.println("AWS Error Code: " + ase.getErrorCode());
				System.out.println("Error Type: " + ase.getErrorType());
				System.out.println("Request ID: " + ase.getRequestId());
				xfer_mgr.shutdownNow();
			} catch (AmazonClientException e) {
				awsS3Response.setStatus(0);
				awsS3Response.setDesc("Something went wrong while Transferring file");
				System.out.println("Caught an AmazonClientException while Transferring file " + e.getStackTrace() );
				xfer_mgr.shutdownNow();
			} catch (InterruptedException e) {
				awsS3Response.setStatus(0);
				awsS3Response.setDesc("Something went wrong while Transferring file");
				System.out.println("Caught an InterruptedException while Transferring file " + e.getStackTrace() );
				xfer_mgr.shutdownNow();
			}
			catch (Exception generalEx) {
				awsS3Response.setStatus(0);
				awsS3Response.setDesc("Something went wrong while Transferring file");	
				System.out.println("Caught an General Error while Transferring file " + generalEx.getStackTrace() );
				xfer_mgr.shutdownNow();
			}
			xfer_mgr.shutdownNow();
			return awsS3Response;
		}


	 

	public static AwsS3Response downLoadFileFromS3(String bucketName, String fileName, String folderName) {
		AwsS3Response awsS3Response = new AwsS3Response();
		try {

			AWSCredentials credentials = new BasicAWSCredentials(AK, ASA);

			AmazonS3 s3client = AmazonS3ClientBuilder.standard()
					.withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.US_EAST_1)
					.build();

			String downLoadFullPath = URLUtil.RTFCUSTMEDIA_DIRECTORY + fileName;
			S3Object fileObject = s3client.getObject(new GetObjectRequest(bucketName, folderName + "/" + fileName));
			FileUtils.copyInputStreamToFile(fileObject.getObjectContent(), new File(downLoadFullPath));

			awsS3Response.setStatus(1);
			awsS3Response.setFullFilePath(downLoadFullPath);

		} catch (AmazonServiceException ase) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc("Something went wrong while downloading file");

			System.out.println("Caught an AmazonServiceException, which means your request made it "
					+ "to Amazon S3, but was rejected with an error response for some reason.");
			System.out.println("Error Message:    " + ase.getMessage());
			System.out.println("HTTP Status Code: " + ase.getStatusCode());
			System.out.println("AWS Error Code:   " + ase.getErrorCode());
			System.out.println("Error Type:       " + ase.getErrorType());
			System.out.println("Request ID:       " + ase.getRequestId());
		} catch (AmazonClientException ace) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc("Something went wrong while downloading file");
			System.out.println("Caught an AmazonClientException, which means the client encountered "
					+ "a serious internal problem while trying to communicate with S3, "
					+ "such as not being able to access the network.");
			System.out.println("Error Message: " + ace.getMessage());
		} catch (Exception ex) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc("Something went wrong while downloading file");
			ex.printStackTrace();
		}
		return awsS3Response;

	}
	
	
	
	/*public static AwsS3Response downloadFilesToSharedDrive( String userName , String fileName, Boolean isWinnerUpload) {
		AwsS3Response awsS3Response = new AwsS3Response();
		try {
			
			String folderName = CUSTOMER_VIDEO_GENERAL_FOLDER;
			
			if(isWinnerUpload) {
				folderName = CUSTOMER_VIDEO_WINNER_FOLDER;
			} 

			AWSCredentials credentials = new BasicAWSCredentials(AK, ASA);

			AmazonS3 s3client = AmazonS3ClientBuilder.standard()
					.withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.US_EAST_1)
					.build();

			String downLoadFullPath = URLUtil.RTFCUSTMEDIA_DIRECTORY + userName+"-"+fileName;
			S3Object fileObject = s3client.getObject(new GetObjectRequest(BUCKET_NAME_PUBLIC_UPLOAD, folderName + "/" + fileName));
			FileUtils.copyInputStreamToFile(fileObject.getObjectContent(), new File(downLoadFullPath));

			awsS3Response.setStatus(1);
			awsS3Response.setFullFilePath(downLoadFullPath);
			
			
			AmazonS3 s3client = AmazonS3ClientBuilder.standard()
					.withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.US_EAST_1)
					.build();

			String downLoadFullPath = ShareDriveUtil.sharedDrivePath + fileName;
			
			S3Object fileObject = s3client.getObject(new GetObjectRequest(BUCKET_NAME_PUBLIC_UPLOAD, folderName + "/" + fileName));
			
			SmbFile newFile = ShareDriveUtil.getSmbFile(downLoadFullPath);
			  
			InputStream reader = new BufferedInputStream(fileObject.getObjectContent());
			OutputStream writer = new BufferedOutputStream(newFile.getOutputStream());

			int read = -1;

			while ( ( read = reader.read() ) != -1 ) {
			    writer.write(read);
			}

			writer.flush();
			writer.close();
			reader.close();
		} catch (AmazonServiceException ase) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc("Something went wrong while downloading file");

			System.out.println("Caught an AmazonServiceException, which means your request made it "
					+ "to Amazon S3, but was rejected with an error response for some reason.");
			System.out.println("Error Message:    " + ase.getMessage());
			System.out.println("HTTP Status Code: " + ase.getStatusCode());
			System.out.println("AWS Error Code:   " + ase.getErrorCode());
			System.out.println("Error Type:       " + ase.getErrorType());
			System.out.println("Request ID:       " + ase.getRequestId());
		} catch (AmazonClientException ace) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc("Something went wrong while downloading file");
			System.out.println("Caught an AmazonClientException, which means the client encountered "
					+ "a serious internal problem while trying to communicate with S3, "
					+ "such as not being able to access the network.");
			System.out.println("Error Message: " + ace.getMessage());
		} catch (Exception ex) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc("Something went wrong while downloading file");
			ex.printStackTrace();
		}
		return awsS3Response;

	}*/
	
	public static AwsS3Response deleteFilesFromS3(String bucketName, String fileName, String folderName) {
		AwsS3Response awsS3Response = new AwsS3Response();
		try {

			AWSCredentials credentials = new BasicAWSCredentials(AK, ASA);

			AmazonS3 s3client = AmazonS3ClientBuilder.standard()
					.withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.US_EAST_1)
					.build();

			
			s3client.deleteObject(new DeleteObjectRequest(bucketName, folderName + "/" + fileName));
			
			awsS3Response.setStatus(1);
			
		} catch (AmazonServiceException ase) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc("Something went wrong while downloading file");

			System.out.println("Caught an AmazonServiceException, which means your request made it "
					+ "to Amazon S3, but was rejected with an error response for some reason.");
			System.out.println("Error Message:    " + ase.getMessage());
			System.out.println("HTTP Status Code: " + ase.getStatusCode());
			System.out.println("AWS Error Code:   " + ase.getErrorCode());
			System.out.println("Error Type:       " + ase.getErrorType());
			System.out.println("Request ID:       " + ase.getRequestId());
		} catch (AmazonClientException ace) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc("Something went wrong while downloading file");
			System.out.println("Caught an AmazonClientException, which means the client encountered "
					+ "a serious internal problem while trying to communicate with S3, "
					+ "such as not being able to access the network.");
			System.out.println("Error Message: " + ace.getMessage());
		} catch (Exception ex) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc("Something went wrong while downloading file");
			ex.printStackTrace();
		}
		return awsS3Response;

	}


}