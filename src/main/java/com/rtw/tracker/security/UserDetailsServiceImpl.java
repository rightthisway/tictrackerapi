package com.rtw.tracker.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.rtw.tmat.utils.Constants;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.Role;
import com.rtw.tracker.datas.TrackerBrokers;
import com.rtw.tracker.datas.TrackerUser;
import com.rtw.tracker.datas.UserPreference;

public class UserDetailsServiceImpl implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String userName)
			throws UsernameNotFoundException {
		//System.out.println(userName);
		TrackerUser trackerUser = DAORegistry.getTrackerUserDAO().getTrackerUserByUsernameOrEmail(userName, userName);
		//System.out.println(trackerUser);
		if(trackerUser == null){
			return null;
		}
		HttpSession session = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
								.getRequest().getSession();
		session.setAttribute("trackerUser", trackerUser);
		//String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		List<UserPreference> userPreferences = DAORegistry.getUserPreferenceDAO().getPreferenceByUserName(userName);
		if(userPreferences!=null && !userPreferences.isEmpty()){
			for(UserPreference userPref : userPreferences){
				session.setAttribute(userPref.getGridName(), userPref.getColumnOrders());
			}
		}
		Boolean isAdmin =false;
		Boolean isBroker =false;
		Boolean isUser =false;
		Boolean isAffiliates =false;
		
		
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		if(trackerUser.getRoles() != null && !trackerUser.getRoles().isEmpty()){
			for(Role role : trackerUser.getRoles()){				
				authorities.add(new SimpleGrantedAuthority(role.getName()));
			    if(role.getName().equals("ROLE_SUPER_ADMIN")){
			    	isAdmin = true;
			     //session.setAttribute("isAdmin",true);
			    }else if(role.getName().equals("ROLE_BROKER")){
			    	isBroker = true;
			    // session.setAttribute("isBroker",true);
			    }else if(role.getName().equals("ROLE_USER")){
			    	isUser = true;
			     //session.setAttribute("isUser",true);
			    }else if(role.getName().equals("ROLE_AFFILIATES")){
			    	isAffiliates = true;
			    // session.setAttribute("isAffiliates",true);
			    }
			}
		}else{
			authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
			isUser = true;
		}
		
		session.setAttribute("isAdmin",isAdmin);
		session.setAttribute("isBroker",isBroker);
		session.setAttribute("isUser",isUser);
		session.setAttribute("isAffiliates",isAffiliates);
		try {
			Collection<TrackerBrokers> trackerBrokers = new ArrayList<TrackerBrokers>();
			if(isBroker && !isAdmin){
				trackerBrokers.add(trackerUser.getBroker());
				session.setAttribute("brokerId",trackerUser.getBroker().getId());
			}else if(isAdmin){
				trackerBrokers = DAORegistry.getTrackerBrokersDAO().getAll();
				session.setAttribute("brokerId",Constants.BROKERID);
			}else if(isUser){
				trackerBrokers.add(DAORegistry.getTrackerBrokersDAO().get(1001));
			}
			session.setAttribute("brokers",trackerBrokers);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
		UserDetails userDetails = new org.springframework.security.core.userdetails.
				User(trackerUser.getUserName(), trackerUser.getPassword(), true, true, true, true, authorities);
		
		return userDetails;
	}

}
