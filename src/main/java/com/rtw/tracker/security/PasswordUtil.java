package com.rtw.tracker.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PasswordUtil {
	private static Logger logger = LoggerFactory.getLogger(PasswordUtil.class);
	
	public static String generateEncrptedPassword(String clearPassword) {
	    MessageDigest algorithm;
		try {
			algorithm = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e) {
			logger.error("SHA-1 algorithm not found!", e);
			throw new RuntimeException("SHA-1 algorithm not found!");
		}
	    byte [] digest = algorithm.digest(clearPassword.getBytes());
	    String encrptedPw = new String(Hex.encodeHex(digest));
	    return encrptedPw;
	}
	
	public static void main(String[] args) {
		
		String  password = "c29222e98de6437a383e8d946a9b202298e0aa3a";
		Integer custId = 86;
		
		String token = generateEncrptedPassword(password+custId);
		
		System.out.println(token);
	}
}
