package com.rtw.tracker.pojos;

public class RelatedPODTO {

	private Integer id;
	private String poTotal;
	private Integer totalQuantity;
	private Integer usedQunatity;
	private String customerName;
	private String csr;
	private String createTimeStr;
	private String internalNotes;
	private String externalNotes;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPoTotal() {
		return poTotal;
	}
	public void setPoTotal(String poTotal) {
		this.poTotal = poTotal;
	}
	public Integer getTotalQuantity() {
		return totalQuantity;
	}
	public void setTotalQuantity(Integer totalQuantity) {
		this.totalQuantity = totalQuantity;
	}
	public Integer getUsedQunatity() {
		return usedQunatity;
	}
	public void setUsedQunatity(Integer usedQunatity) {
		this.usedQunatity = usedQunatity;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCsr() {
		return csr;
	}
	public void setCsr(String csr) {
		this.csr = csr;
	}
	public String getCreateTimeStr() {
		return createTimeStr;
	}
	public void setCreateTimeStr(String createTimeStr) {
		this.createTimeStr = createTimeStr;
	}
	public String getInternalNotes() {
		return internalNotes;
	}
	public void setInternalNotes(String internalNotes) {
		this.internalNotes = internalNotes;
	}
	public String getExternalNotes() {
		return externalNotes;
	}
	public void setExternalNotes(String externalNotes) {
		this.externalNotes = externalNotes;
	}
		
}
