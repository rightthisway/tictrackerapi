package com.rtw.tracker.pojos;

import java.util.Collection;

import com.rtw.tmat.utils.Error;

public class InvoiceDetailsDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message;
	private PaginationDTO invoicePaginationDTO;
	private Collection<OpenOrdersDTO> openOrdersDTO;
	private String invoiceStatus;
	private String invoiceTotal;
	private String layoutProductType;
	private String selectedProductType;
	private String fromDate;
	private String toDate;
	private String currentYear;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public PaginationDTO getInvoicePaginationDTO() {
		return invoicePaginationDTO;
	}
	public void setInvoicePaginationDTO(PaginationDTO invoicePaginationDTO) {
		this.invoicePaginationDTO = invoicePaginationDTO;
	}
	public Collection<OpenOrdersDTO> getOpenOrdersDTO() {
		return openOrdersDTO;
	}
	public void setOpenOrdersDTO(Collection<OpenOrdersDTO> openOrdersDTO) {
		this.openOrdersDTO = openOrdersDTO;
	}
	public String getInvoiceStatus() {
		return invoiceStatus;
	}
	public void setInvoiceStatus(String invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}
	public String getInvoiceTotal() {
		return invoiceTotal;
	}
	public void setInvoiceTotal(String invoiceTotal) {
		this.invoiceTotal = invoiceTotal;
	}
	public String getLayoutProductType() {
		return layoutProductType;
	}
	public void setLayoutProductType(String layoutProductType) {
		this.layoutProductType = layoutProductType;
	}
	public String getSelectedProductType() {
		return selectedProductType;
	}
	public void setSelectedProductType(String selectedProductType) {
		this.selectedProductType = selectedProductType;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getCurrentYear() {
		return currentYear;
	}
	public void setCurrentYear(String currentYear) {
		this.currentYear = currentYear;
	}
	

}
