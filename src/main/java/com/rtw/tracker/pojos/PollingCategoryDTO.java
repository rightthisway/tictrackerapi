package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.PollingCategory;

public class PollingCategoryDTO {

	private List<PollingCategory> pollingCategory;
	private PollingCategory pollingCategoryObj;
	private PaginationDTO pagination;
	private Error error = new Error();
	private String message = new String();
	private Integer status;

	public List<PollingCategory> getPollingCategory() {
		return pollingCategory;
	}

	public void setPollingCategory(List<PollingCategory> pollingCategory) {
		this.pollingCategory = pollingCategory;
	}

	public PaginationDTO getPagination() {
		return pagination;
	}

	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public PollingCategory getPollingCategoryObj() {
		return pollingCategoryObj;
	}

	public void setPollingCategoryObj(PollingCategory pollingCategoryObj) {
		this.pollingCategoryObj = pollingCategoryObj;
	}

}
