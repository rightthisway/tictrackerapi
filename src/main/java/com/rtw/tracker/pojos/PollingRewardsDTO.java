package com.rtw.tracker.pojos;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.PollingRewards;

public class PollingRewardsDTO {

	private PollingRewards pollingRewards;

	private PaginationDTO pagination;
	private Error error = new Error();
	private String message = new String();
	private Integer status;

	public PollingRewards getPollingRewards() {
		return pollingRewards;
	}

	public void setPollingRewards(PollingRewards pollingRewards) {
		this.pollingRewards = pollingRewards;
	}

	public PaginationDTO getPagination() {
		return pagination;
	}

	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}
