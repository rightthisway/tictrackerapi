package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.ContestQuestions;
import com.rtw.tracker.datas.Contests;

public class ContestQuestionDTO {

	private Integer status;
	private Error error; 
	private String message;
	private List<ContestQuestions> questionList;
	private ContestQuestions nextQuestion;
	private Contests contest;
	private PaginationDTO paginationDTO;
	private Integer lifeCount;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<ContestQuestions> getQuestionList() {
		return questionList;
	}
	public void setQuestionList(List<ContestQuestions> questionList) {
		this.questionList = questionList;
	}
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}
	public Contests getContest() {
		return contest;
	}
	public void setContest(Contests contest) {
		this.contest = contest;
	}
	public ContestQuestions getNextQuestion() {
		return nextQuestion;
	}
	public void setNextQuestion(ContestQuestions nextQuestion) {
		this.nextQuestion = nextQuestion;
	}
	public Integer getLifeCount() {
		return lifeCount;
	}
	public void setLifeCount(Integer lifeCount) {
		this.lifeCount = lifeCount;
	}
	
	
	
	
}
