package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.ContestEventRequest;
import com.rtw.tracker.datas.Contests;
import com.rtw.tracker.datas.PreContestChecklist;
import com.rtw.tracker.datas.QuestionBank;

public class ContestsDTO {

	private Integer status;
	private Error error; 
	private String message;
	private List<Contests> contestsList;
	private PaginationDTO paginationDTO;
	private List<ContestEventRequest> contestEventRequestList;
	private PaginationDTO contestEventRequestPaginationDTO;
	private List<QuestionBank> questionBankList;
	private List<PreContestChecklist> preContestList;
	private PaginationDTO questionBankPaginationDTO;
	private String liveStreamUrl;
	private Boolean contestStarted;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}
	public List<Contests> getContestsList() {
		return contestsList;
	}
	public void setContestsList(List<Contests> contestsList) {
		this.contestsList = contestsList;
	}
	public List<ContestEventRequest> getContestEventRequestList() {
		return contestEventRequestList;
	}
	public void setContestEventRequestList(
			List<ContestEventRequest> contestEventRequestList) {
		this.contestEventRequestList = contestEventRequestList;
	}
	public PaginationDTO getContestEventRequestPaginationDTO() {
		return contestEventRequestPaginationDTO;
	}
	public void setContestEventRequestPaginationDTO(
			PaginationDTO contestEventRequestPaginationDTO) {
		this.contestEventRequestPaginationDTO = contestEventRequestPaginationDTO;
	}
	public List<QuestionBank> getQuestionBankList() {
		return questionBankList;
	}
	public void setQuestionBankList(List<QuestionBank> questionBankList) {
		this.questionBankList = questionBankList;
	}
	public PaginationDTO getQuestionBankPaginationDTO() {
		return questionBankPaginationDTO;
	}
	public void setQuestionBankPaginationDTO(PaginationDTO questionBankPaginationDTO) {
		this.questionBankPaginationDTO = questionBankPaginationDTO;
	}
	public List<PreContestChecklist> getPreContestList() {
		return preContestList;
	}
	public void setPreContestList(List<PreContestChecklist> preContestList) {
		this.preContestList = preContestList;
	}
	public String getLiveStreamUrl() {
		return liveStreamUrl;
	}
	public void setLiveStreamUrl(String liveStreamUrl) {
		this.liveStreamUrl = liveStreamUrl;
	}
	public Boolean getContestStarted() {
		return contestStarted;
	}
	public void setContestStarted(Boolean contestStarted) {
		this.contestStarted = contestStarted;
	}	
	
	
	
}
