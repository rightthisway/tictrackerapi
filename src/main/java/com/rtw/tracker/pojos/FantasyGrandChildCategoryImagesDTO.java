package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.FantasyGrandChildCategory;
import com.rtw.tracker.datas.FantasyGrandChildCategoryImage;

public class FantasyGrandChildCategoryImagesDTO {

	private Integer status;
	private Error error; 
	private String message = new String();
	private String productName;
	private List<FantasyGrandChildCategoryImage> grandChildImageList;
	private List<FantasyGrandChildCategory> grandChilds;
	private List<FantasyGrandChildCategory> grandChildsList;
	private boolean grandChildsCheckAll;
	private String grandChildStr;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public List<FantasyGrandChildCategoryImage> getGrandChildImageList() {
		return grandChildImageList;
	}
	public void setGrandChildImageList(List<FantasyGrandChildCategoryImage> grandChildImageList) {
		this.grandChildImageList = grandChildImageList;
	}
	public List<FantasyGrandChildCategory> getGrandChilds() {
		return grandChilds;
	}
	public void setGrandChilds(List<FantasyGrandChildCategory> grandChilds) {
		this.grandChilds = grandChilds;
	}
	public List<FantasyGrandChildCategory> getGrandChildsList() {
		return grandChildsList;
	}
	public void setGrandChildsList(List<FantasyGrandChildCategory> grandChildsList) {
		this.grandChildsList = grandChildsList;
	}
	public boolean isGrandChildsCheckAll() {
		return grandChildsCheckAll;
	}
	public void setGrandChildsCheckAll(boolean grandChildsCheckAll) {
		this.grandChildsCheckAll = grandChildsCheckAll;
	}
	public String getGrandChildStr() {
		return grandChildStr;
	}
	public void setGrandChildStr(String grandChildStr) {
		this.grandChildStr = grandChildStr;
	}
	
}
