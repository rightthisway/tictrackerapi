package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.ChildCategory;
import com.rtw.tracker.datas.ChildCategoryImage;

public class ChildCategoryImagesDTO {

	private Integer status;
	private Error error; 
	private String message = new String();
	private List<ChildCategoryImage> childImageList;
	private List<ChildCategory> childs;
	private boolean childsCheckAll;
	private String childStr;
	private List<ChildCategory> childsList;
	private String productName;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<ChildCategoryImage> getChildImageList() {
		return childImageList;
	}
	public void setChildImageList(List<ChildCategoryImage> childImageList) {
		this.childImageList = childImageList;
	}
	public List<ChildCategory> getChilds() {
		return childs;
	}
	public void setChilds(List<ChildCategory> childs) {
		this.childs = childs;
	}
	public boolean isChildsCheckAll() {
		return childsCheckAll;
	}
	public void setChildsCheckAll(boolean childsCheckAll) {
		this.childsCheckAll = childsCheckAll;
	}
	public String getChildStr() {
		return childStr;
	}
	public void setChildStr(String childStr) {
		this.childStr = childStr;
	}
	public List<ChildCategory> getChildsList() {
		return childsList;
	}
	public void setChildsList(List<ChildCategory> childsList) {
		this.childsList = childsList;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}

}
