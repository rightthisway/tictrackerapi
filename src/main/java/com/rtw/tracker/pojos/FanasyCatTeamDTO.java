package com.rtw.tracker.pojos;

public class FanasyCatTeamDTO {

	private Integer id;
	private String name;
	private Double odds;
	private String fractionOdds;
	private Integer markup;
	private String cutoffDate;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getOdds() {
		return odds;
	}
	public void setOdds(Double odds) {
		this.odds = odds;
	}
	public String getFractionOdds() {
		return fractionOdds;
	}
	public void setFractionOdds(String fractionOdds) {
		this.fractionOdds = fractionOdds;
	}
	public Integer getMarkup() {
		return markup;
	}
	public void setMarkup(Integer markup) {
		this.markup = markup;
	}
	public String getCutoffDate() {
		return cutoffDate;
	}
	public void setCutoffDate(String cutoffDate) {
		this.cutoffDate = cutoffDate;
	}
		
}
