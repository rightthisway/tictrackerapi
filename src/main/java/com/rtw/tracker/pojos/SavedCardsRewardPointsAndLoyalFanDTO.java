package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class SavedCardsRewardPointsAndLoyalFanDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message;
	private List<CustomerCardInfoDTO> customerCardInfoDTO;
	private Double rewardPoints;
	private Double customerCredit;
	private Boolean isLoyalFanPrice;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<CustomerCardInfoDTO> getCustomerCardInfoDTO() {
		return customerCardInfoDTO;
	}
	public void setCustomerCardInfoDTO(List<CustomerCardInfoDTO> customerCardInfoDTO) {
		this.customerCardInfoDTO = customerCardInfoDTO;
	}
	public Double getRewardPoints() {
		return rewardPoints;
	}
	public void setRewardPoints(Double rewardPoints) {
		this.rewardPoints = rewardPoints;
	}
	public Double getCustomerCredit() {
		return customerCredit;
	}
	public void setCustomerCredit(Double customerCredit) {
		this.customerCredit = customerCredit;
	}
	public Boolean getIsLoyalFanPrice() {
		return isLoyalFanPrice;
	}
	public void setIsLoyalFanPrice(Boolean isLoyalFanPrice) {
		this.isLoyalFanPrice = isLoyalFanPrice;
	}
	
}
