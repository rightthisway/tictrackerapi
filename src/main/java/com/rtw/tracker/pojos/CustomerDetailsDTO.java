package com.rtw.tracker.pojos;

import java.util.Collection;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.Customers;

public class CustomerDetailsDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private Collection<Customers> customerList;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Collection<Customers> getCustomerList() {
		return customerList;
	}
	public void setCustomerList(Collection<Customers> customerList) {
		this.customerList = customerList;
	}
	
}
