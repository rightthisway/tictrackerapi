package com.rtw.tracker.pojos;

import java.io.Serializable;

public class SponsorAutoComplete implements Serializable{

	private Integer sponsorId;
	private String name;
	public Integer getSponsorId() {
		return sponsorId;
	}
	public void setSponsorId(Integer sponsorId) {
		this.sponsorId = sponsorId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	} 
	
	
	
	
}
