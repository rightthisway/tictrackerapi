package com.rtw.tracker.pojos;

public class CustomerLoyalFanDTO {

	private String artistName;
	private String cityName;
	private String categoryName;
	private String ticketsPurchased;
	private String startDate;
	private String endDate;
	
	public String getArtistName() {
		return artistName;
	}
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getTicketsPurchased() {
		return ticketsPurchased;
	}
	public void setTicketsPurchased(String ticketsPurchased) {
		this.ticketsPurchased = ticketsPurchased;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
}
