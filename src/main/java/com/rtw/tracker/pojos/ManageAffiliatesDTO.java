package com.rtw.tracker.pojos;

public class ManageAffiliatesDTO {

	private Integer userId;
	private String userName;
	private String email;
	private String firstName;
	private String lastName;
	private String phone;
	private String status;
	private String promotionalCode;
	private Double activeCash;
	private Double pendingCash;
	
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPromotionalCode() {
		return promotionalCode;
	}
	public void setPromotionalCode(String promotionalCode) {
		this.promotionalCode = promotionalCode;
	}
	public Double getActiveCash() {
		return activeCash;
	}
	public void setActiveCash(Double activeCash) {
		this.activeCash = activeCash;
	}
	public Double getPendingCash() {
		return pendingCash;
	}
	public void setPendingCash(Double pendingCash) {
		this.pendingCash = pendingCash;
	}
	
}
