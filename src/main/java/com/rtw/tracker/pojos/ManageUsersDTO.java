package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.HoldTicketDTO;

public class ManageUsersDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	List<TrackerUserCustomDTO> trackerUserCustomDTO;
	private PaginationDTO paginationDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<TrackerUserCustomDTO> getTrackerUserCustomDTO() {
		return trackerUserCustomDTO;
	}
	public void setTrackerUserCustomDTO(List<TrackerUserCustomDTO> trackerUserCustomDTO) {
		this.trackerUserCustomDTO = trackerUserCustomDTO;
	}
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}	
	
}
