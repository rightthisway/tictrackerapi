package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.GrandChildCategory;
import com.rtw.tracker.datas.GrandChildCategoryImage;

public class GrandChildCategoryImagesDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private List<GrandChildCategoryImage> grandChildImageList;
	private List<GrandChildCategory> grandChilds;
	private List<GrandChildCategory> grandChildsList;
	private boolean grandChildsCheckAll;
	private String grandChildStr;
	private String productName;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<GrandChildCategoryImage> getGrandChildImageList() {
		return grandChildImageList;
	}
	public void setGrandChildImageList(List<GrandChildCategoryImage> grandChildImageList) {
		this.grandChildImageList = grandChildImageList;
	}
	public List<GrandChildCategory> getGrandChilds() {
		return grandChilds;
	}
	public void setGrandChilds(List<GrandChildCategory> grandChilds) {
		this.grandChilds = grandChilds;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public List<GrandChildCategory> getGrandChildsList() {
		return grandChildsList;
	}
	public void setGrandChildsList(List<GrandChildCategory> grandChildsList) {
		this.grandChildsList = grandChildsList;
	}
	public boolean isGrandChildsCheckAll() {
		return grandChildsCheckAll;
	}
	public void setGrandChildsCheckAll(boolean grandChildsCheckAll) {
		this.grandChildsCheckAll = grandChildsCheckAll;
	}
	public String getGrandChildStr() {
		return grandChildStr;
	}
	public void setGrandChildStr(String grandChildStr) {
		this.grandChildStr = grandChildStr;
	}
	
}
