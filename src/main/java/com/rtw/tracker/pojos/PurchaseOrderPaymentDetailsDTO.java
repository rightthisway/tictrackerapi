package com.rtw.tracker.pojos;

public class PurchaseOrderPaymentDetailsDTO {

	private Integer id;
	private String name;
	private String paymentType;
	private String cardType;
	private String cardLastFourDigit;
	private String routingLastFourDigit;
	private String accountLastFourDigit;
	private String chequeNo;
	private String paymentStatus;
	private Double amount;
	private String paymentDate;
	private String paymentNote;
	
	private String paidName;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getCardLastFourDigit() {
		return cardLastFourDigit;
	}
	public void setCardLastFourDigit(String cardLastFourDigit) {
		this.cardLastFourDigit = cardLastFourDigit;
	}
	public String getRoutingLastFourDigit() {
		return routingLastFourDigit;
	}
	public void setRoutingLastFourDigit(String routingLastFourDigit) {
		this.routingLastFourDigit = routingLastFourDigit;
	}
	public String getAccountLastFourDigit() {
		return accountLastFourDigit;
	}
	public void setAccountLastFourDigit(String accountLastFourDigit) {
		this.accountLastFourDigit = accountLastFourDigit;
	}
	public String getChequeNo() {
		return chequeNo;
	}
	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	public String getPaymentNote() {
		return paymentNote;
	}
	public void setPaymentNote(String paymentNote) {
		this.paymentNote = paymentNote;
	}
	public String getPaidName() {
		return paidName;
	}
	public void setPaidName(String paidName) {
		this.paidName = paidName;
	}
	
}
