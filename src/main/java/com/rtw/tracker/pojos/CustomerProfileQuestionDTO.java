package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.CustomerProfileQuestion;

public class CustomerProfileQuestionDTO {

	private List<CustomerProfileQuestion> questions;
	private CustomerProfileQuestion question;
	private PaginationDTO pagination;
	private Error error = new Error(); 
	private String message = new String();
	private Integer status;
	
	
	public List<CustomerProfileQuestion> getQuestions() {
		return questions;
	}
	public void setQuestions(List<CustomerProfileQuestion> questions) {
		this.questions = questions;
	}
	public CustomerProfileQuestion getQuestion() {
		return question;
	}
	public void setQuestion(CustomerProfileQuestion question) {
		this.question = question;
	}
	public PaginationDTO getPagination() {
		return pagination;
	}
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
