package com.rtw.tracker.pojos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;


/**
 * represents customer entity
 * @author Tamil
 *
 */
@XStreamAlias("QuizContestWinners")
public class QuizContestWinners  implements Serializable{
	
	private Integer id;
	private Integer contestId;
	private Integer customerId;
	private Integer rewardTickets;
	private Double rewardPoints;
	private Integer rewardRank;
	
	private String customerName;
	private String userId;
	private String customerLastName;
	private String profilePicWV;
	
	
	@JsonIgnore
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="contest_id")
	public Integer getContestId() {
		return contestId;
	}
	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}
	
	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Column(name="reward_tickets")
	public Integer getRewardTickets() {
		return rewardTickets;
	}
	public void setRewardTickets(Integer rewardTickets) {
		this.rewardTickets = rewardTickets;
	}
	
	
	/*public Double getRewardPoints() {
		return rewardPoints;
	}
	public void setRewardPoints(Double rewardPoints) {
		this.rewardPoints = rewardPoints;
	}*/
	@Column(name="reward_points")
	public Double getRewardPoints() {
		return rewardPoints;
	}
	public void setRewardPoints(Double rewardPoints) {
		this.rewardPoints = rewardPoints;
	}
	
	@Column(name="reward_rank")
	public Integer getRewardRank() {
		return rewardRank;
	}
	
	public void setRewardRank(Integer rewardRank) {
		this.rewardRank = rewardRank;
	}
	
	@Transient
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	@Transient
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	@Transient
	public String getCustomerLastName() {
		return customerLastName;
	}
	public void setCustomerLastName(String customerLastName) {
		this.customerLastName = customerLastName;
	}
	
	@Transient
	public String getProfilePicWV() {
		return profilePicWV;
	}
	public void setProfilePicWV(String profilePicWV) {
		this.profilePicWV = profilePicWV;
	}
	

}
