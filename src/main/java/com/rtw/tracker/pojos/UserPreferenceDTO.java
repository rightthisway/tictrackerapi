package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.ArtistImage;
import com.rtw.tracker.datas.UserPreference;

public class UserPreferenceDTO {

	private Integer status;
	private Error error; 
	private String message;
	private String columnOrders;
	private List<UserPreference> userPreferences;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public com.rtw.tmat.utils.Error getError() {
		return error;
	}
	public void setError(com.rtw.tmat.utils.Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getColumnOrders() {
		return columnOrders;
	}
	public void setColumnOrders(String columnOrders) {
		this.columnOrders = columnOrders;
	}
	public List<UserPreference> getUserPreferences() {
		return userPreferences;
	}
	public void setUserPreferences(List<UserPreference> userPreferences) {
		this.userPreferences = userPreferences;
	}
	
	
}
