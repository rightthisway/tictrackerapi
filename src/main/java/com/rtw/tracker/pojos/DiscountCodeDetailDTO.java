package com.rtw.tracker.pojos;

import java.util.List;
import com.rtw.tmat.utils.Error;

public class DiscountCodeDetailDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private List<PromoOfferDTO> promoOfferDTO;
	private PaginationDTO promoOfferDetailPaginationDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<PromoOfferDTO> getPromoOfferDTO() {
		return promoOfferDTO;
	}
	public void setPromoOfferDTO(List<PromoOfferDTO> promoOfferDTO) {
		this.promoOfferDTO = promoOfferDTO;
	}
	public PaginationDTO getPromoOfferDetailPaginationDTO() {
		return promoOfferDetailPaginationDTO;
	}
	public void setPromoOfferDetailPaginationDTO(
			PaginationDTO promoOfferDetailPaginationDTO) {
		this.promoOfferDetailPaginationDTO = promoOfferDetailPaginationDTO;
	}	
		
}
