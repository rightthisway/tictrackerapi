package com.rtw.tracker.pojos;

import java.util.ArrayList;
import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.TrackerBrokers;

public class EditUserDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private EditTrackerUserCustomDTO editTrackerUserCustomDTO;
	private PaginationDTO paginationDTO;
	private List<String> rolesList;
	private TrackerBrokers trackerBrokers;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}
	public TrackerBrokers getTrackerBrokers() {
		return trackerBrokers;
	}
	public void setTrackerBrokers(TrackerBrokers trackerBrokers) {
		this.trackerBrokers = trackerBrokers;
	}
	public EditTrackerUserCustomDTO getEditTrackerUserCustomDTO() {
		return editTrackerUserCustomDTO;
	}
	public void setEditTrackerUserCustomDTO(EditTrackerUserCustomDTO editTrackerUserCustomDTO) {
		this.editTrackerUserCustomDTO = editTrackerUserCustomDTO;
	}
	public List<String> getRolesList() {
		return rolesList;
	}
	public void setRolesList(List<String> rolesList) {
		this.rolesList = rolesList;
	}
	

}
