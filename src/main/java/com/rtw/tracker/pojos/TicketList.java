package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tracker.datas.Event;



public class TicketList {
	private Integer status;
	private com.rtw.tmat.utils.Error error; 
	private Event event;
	//private DiscountDetails discountDetails;
	private List<TicketGroupQty> ticketGroupQtyList;
	private String aRefType;
	private String aRefValue;
	private PaginationDTO paginationDTO;
	private String message;
	
	public Event getEvent() {
		return event;
	}
	public void setEvent(Event event) {
		this.event = event;
	}
	
	public com.rtw.tmat.utils.Error getError() {
		return error;
	}
	public void setError(com.rtw.tmat.utils.Error error) {
		this.error = error;
	}
	/*public DiscountDetails getDiscountDetails() {
		return discountDetails;
	}
	public void setDiscountDetails(DiscountDetails discountDetails) {
		this.discountDetails = discountDetails;
	}*/
	/*public TicketGroupQtyMap getTicketGroupQtyMap() {
		return ticketGroupQtyMap;
	}
	public void setTicketGroupQtyMap(TicketGroupQtyMap ticketGroupQtyMap) {
		this.ticketGroupQtyMap = ticketGroupQtyMap;
	}*/
	public List<TicketGroupQty> getTicketGroupQtyList() {
		return ticketGroupQtyList;
	}
	public void setTicketGroupQtyList(List<TicketGroupQty> ticketGroupQtyList) {
		this.ticketGroupQtyList = ticketGroupQtyList;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public String getaRefType() {
		return aRefType;
	}
	public void setaRefType(String aRefType) {
		this.aRefType = aRefType;
	}
	public String getaRefValue() {
		return aRefValue;
	}
	public void setaRefValue(String aRefValue) {
		this.aRefValue = aRefValue;
	}
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}

