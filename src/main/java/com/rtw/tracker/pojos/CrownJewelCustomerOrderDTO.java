package com.rtw.tracker.pojos;

public class CrownJewelCustomerOrderDTO {

	private Integer id;
	private String eventName;
	private Integer teamId;
	private String teamName;
	private String isPackageSelected;
	private Double packageCost;
	private String packageNote;
	private Integer eventId;
	private String isRealTicket;
	private Integer ticketId;
	private String zone;
	private Integer ticketQty;
	private Double ticketPrice;
	private Double requirePoints;
	private Integer customerId;
	private String cjStatus;
	private String createdDateStr;
	private String lastUpdatedStr;
	private String acceptRejectReason;
	private Integer regularOrderId;
	private Integer teamZoneId;
	private String venue;
	private String city;
	private String state;
	private String country;
	private String platform;
	private String grandChildCategoryName;
	private String customerName;
	
	private String status;
	private String leagueName;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public Integer getTeamId() {
		return teamId;
	}
	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}
	public String getTeamName() {
		return teamName;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	public String getIsPackageSelected() {
		return isPackageSelected;
	}
	public void setIsPackageSelected(String isPackageSelected) {
		this.isPackageSelected = isPackageSelected;
	}
	public Double getPackageCost() {
		return packageCost;
	}
	public void setPackageCost(Double packageCost) {
		this.packageCost = packageCost;
	}
	public String getPackageNote() {
		return packageNote;
	}
	public void setPackageNote(String packageNote) {
		this.packageNote = packageNote;
	}
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public String getIsRealTicket() {
		return isRealTicket;
	}
	public void setIsRealTicket(String isRealTicket) {
		this.isRealTicket = isRealTicket;
	}
	public Integer getTicketId() {
		return ticketId;
	}
	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public Integer getTicketQty() {
		return ticketQty;
	}
	public void setTicketQty(Integer ticketQty) {
		this.ticketQty = ticketQty;
	}
	public Double getTicketPrice() {
		return ticketPrice;
	}
	public void setTicketPrice(Double ticketPrice) {
		this.ticketPrice = ticketPrice;
	}
	public Double getRequirePoints() {
		return requirePoints;
	}
	public void setRequirePoints(Double requirePoints) {
		this.requirePoints = requirePoints;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getCjStatus() {
		return cjStatus;
	}
	public void setCjStatus(String cjStatus) {
		this.cjStatus = cjStatus;
	}
	public String getCreatedDateStr() {
		return createdDateStr;
	}
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	public String getLastUpdatedStr() {
		return lastUpdatedStr;
	}
	public void setLastUpdatedStr(String lastUpdatedStr) {
		this.lastUpdatedStr = lastUpdatedStr;
	}
	public String getAcceptRejectReason() {
		return acceptRejectReason;
	}
	public void setAcceptRejectReason(String acceptRejectReason) {
		this.acceptRejectReason = acceptRejectReason;
	}
	public Integer getTeamZoneId() {
		return teamZoneId;
	}
	public void setTeamZoneId(Integer teamZoneId) {
		this.teamZoneId = teamZoneId;
	}
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	public String getGrandChildCategoryName() {
		return grandChildCategoryName;
	}
	public void setGrandChildCategoryName(String grandChildCategoryName) {
		this.grandChildCategoryName = grandChildCategoryName;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Integer getRegularOrderId() {
		return regularOrderId;
	}
	public void setRegularOrderId(Integer regularOrderId) {
		this.regularOrderId = regularOrderId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getLeagueName() {
		return leagueName;
	}
	public void setLeagueName(String leagueName) {
		this.leagueName = leagueName;
	}
	
}
