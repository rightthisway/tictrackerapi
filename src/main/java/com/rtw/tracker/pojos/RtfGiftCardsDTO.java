package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.GiftCardBrand;
import com.rtw.tracker.datas.RtfGiftCard;
import com.rtw.tracker.datas.RtfGiftCardQuantity;

public class RtfGiftCardsDTO {

	private List<GiftCardPojo> giftCards;
	private RtfGiftCard giftCard;
	private List<RtfGiftCardQuantity> qtyList;
	private List<GiftCardBrand> brands;
	private GiftCardBrand brand;
	private PaginationDTO pagination;
	private Error error = new Error(); 
	private String message = new String();
	private Integer status;
	
	public List<GiftCardPojo> getGiftCards() {
		return giftCards;
	}
	public void setGiftCards(List<GiftCardPojo> giftCards) {
		this.giftCards = giftCards;
	}
	
	public PaginationDTO getPagination() {
		return pagination;
	}
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public RtfGiftCard getGiftCard() {
		return giftCard;
	}
	public void setGiftCard(RtfGiftCard giftCard) {
		this.giftCard = giftCard;
	}
	public List<RtfGiftCardQuantity> getQtyList() {
		return qtyList;
	}
	public void setQtyList(List<RtfGiftCardQuantity> qtyList) {
		this.qtyList = qtyList;
	}
	public GiftCardBrand getBrand() {
		return brand;
	}
	public void setBrand(GiftCardBrand brand) {
		this.brand = brand;
	}
	public List<GiftCardBrand> getBrands() {
		return brands;
	}
	public void setBrands(List<GiftCardBrand> brands) {
		this.brands = brands;
	}
	
	
	
}
