package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.CategoryTicketGroup;


public class InvoiceCreationDTO {
	
	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private Integer shippingMethodId; 
	private String shippingMethod;
	private String sPubKey;
	private EventDetailsDTO eventDetailsDTO;
	private List<CategoryTicketGroupListDTO> categoryTicketGroupListDTOs;
	private Integer ticketId;
	private String invoiceId;
	private String type;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getShippingMethodId() {
		return shippingMethodId;
	}
	public void setShippingMethodId(Integer shippingMethodId) {
		this.shippingMethodId = shippingMethodId;
	}
	public String getShippingMethod() {
		return shippingMethod;
	}
	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}
	public String getsPubKey() {
		return sPubKey;
	}
	public void setsPubKey(String sPubKey) {
		this.sPubKey = sPubKey;
	}
	public EventDetailsDTO getEventDetailsDTO() {
		return eventDetailsDTO;
	}
	public void setEventDetailsDTO(EventDetailsDTO eventDetailsDTO) {
		this.eventDetailsDTO = eventDetailsDTO;
	}
	public List<CategoryTicketGroupListDTO> getCategoryTicketGroupListDTOs() {
		return categoryTicketGroupListDTOs;
	}
	public void setCategoryTicketGroupListDTOs(
			List<CategoryTicketGroupListDTO> categoryTicketGroupListDTOs) {
		this.categoryTicketGroupListDTOs = categoryTicketGroupListDTOs;
	}
	public Integer getTicketId() {
		return ticketId;
	}
	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}
	public String getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
