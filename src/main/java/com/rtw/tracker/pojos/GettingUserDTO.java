package com.rtw.tracker.pojos;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.TrackerUser;

public class GettingUserDTO {

	private Integer status;
	private Error error; 
	private String message;
	private TrackerUser trackerUser;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public com.rtw.tmat.utils.Error getError() {
		return error;
	}
	public void setError(com.rtw.tmat.utils.Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public TrackerUser getTrackerUser() {
		return trackerUser;
	}
	public void setTrackerUser(TrackerUser trackerUser) {
		this.trackerUser = trackerUser;
	}
	
	
}
