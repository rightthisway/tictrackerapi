package com.rtw.tracker.pojos;

import com.rtw.tracker.enums.RewardStatus;

public class AffiliatesOrderDTO {

	private Integer orderId;
	private String firstName;
	private String lastName;
	private String eventName;
	private String eventDate;
	private String eventTime;
	private Integer ticketQty;
	private Double orderTotal;
	private Double cashCredit;
	private RewardStatus status;
	private String promoCode;
/*	private String paymentNote;*/
	
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	public String getEventTime() {
		return eventTime;
	}
	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}
	public Integer getTicketQty() {
		return ticketQty;
	}
	public void setTicketQty(Integer ticketQty) {
		this.ticketQty = ticketQty;
	}
	public Double getOrderTotal() {
		return orderTotal;
	}
	public void setOrderTotal(Double orderTotal) {
		this.orderTotal = orderTotal;
	}
	public Double getCashCredit() {
		return cashCredit;
	}
	public void setCashCredit(Double cashCredit) {
		this.cashCredit = cashCredit;
	}
	public RewardStatus getStatus() {
		return status;
	}
	public void setStatus(RewardStatus status) {
		this.status = status;
	}
	public String getPromoCode() {
		return promoCode;
	}
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}
	/*public String getPaymentNote() {
		return paymentNote;
	}
	public void setPaymentNote(String paymentNote) {
		this.paymentNote = paymentNote;
	}*/
	
}
