package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class POCreateDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private List<POShippingMethodDTO> shippingMethodDTO;
	private Integer poId;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<POShippingMethodDTO> getShippingMethodDTO() {
		return shippingMethodDTO;
	}
	public void setShippingMethodDTO(List<POShippingMethodDTO> shippingMethodDTO) {
		this.shippingMethodDTO = shippingMethodDTO;
	}
	public Integer getPoId() {
		return poId;
	}
	public void setPoId(Integer poId) {
		this.poId = poId;
	}
		
}
