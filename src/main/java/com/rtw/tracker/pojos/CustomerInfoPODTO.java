package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class CustomerInfoPODTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private PaginationDTO poPaginationDTO;
	private List<CustomerPurchaseOrderDTO> customerPurchaseOrderDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public PaginationDTO getPoPaginationDTO() {
		return poPaginationDTO;
	}
	public void setPoPaginationDTO(PaginationDTO poPaginationDTO) {
		this.poPaginationDTO = poPaginationDTO;
	}
	public List<CustomerPurchaseOrderDTO> getCustomerPurchaseOrderDTO() {
		return customerPurchaseOrderDTO;
	}
	public void setCustomerPurchaseOrderDTO(
			List<CustomerPurchaseOrderDTO> customerPurchaseOrderDTO) {
		this.customerPurchaseOrderDTO = customerPurchaseOrderDTO;
	}
	
	
}
