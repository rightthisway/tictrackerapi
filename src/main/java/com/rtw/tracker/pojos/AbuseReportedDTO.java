package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.CustomerReportedMedia;

public class AbuseReportedDTO {
	
	private List<CustomerReportedMedia> reportedDatas;
	private Error error = new Error(); 
	private String message = new String();
	private Integer status;
	private PaginationDTO pagination;
	
	public List<CustomerReportedMedia> getReportedDatas() {
		return reportedDatas;
	}
	public void setReportedDatas(List<CustomerReportedMedia> reportedDatas) {
		this.reportedDatas = reportedDatas;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public PaginationDTO getPagination() {
		return pagination;
	}
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}

}
