package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.PollingCategory;
import com.rtw.tracker.datas.PollingCustomerVideo;
import com.rtw.tracker.datas.PollingVideoCategory;

public class PollingCustomerVideoDTO {

	private List<PollingCustomerVideo> customerVideos;
	private List<PollingVideoCategory> categories;
	private PaginationDTO pagination;
	private Error error = new Error();
	private String message = new String();
	private Integer status;


	public List<PollingCustomerVideo> getCustomerVideos() {
		return customerVideos;
	}

	public void setCustomerVideos(List<PollingCustomerVideo> customerVideos) {
		this.customerVideos = customerVideos;
	}

	public PaginationDTO getPagination() {
		return pagination;
	}

	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<PollingVideoCategory> getCategories() {
		return categories;
	}

	public void setCategories(List<PollingVideoCategory> categories) {
		this.categories = categories;
	}

}
