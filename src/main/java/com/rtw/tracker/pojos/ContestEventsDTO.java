package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.ContestEvents;

public class ContestEventsDTO {

	private Integer status;
	private Error error; 
	private String message;
	private List<ContestEvents> allEventList;
	private PaginationDTO paginationDTO;
	private List<ContestEvents> excludeEventList;
	private PaginationDTO excludeEventPaginationDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<ContestEvents> getAllEventList() {
		return allEventList;
	}
	public void setAllEventList(List<ContestEvents> allEventList) {
		this.allEventList = allEventList;
	}
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}
	public List<ContestEvents> getExcludeEventList() {
		return excludeEventList;
	}
	public void setExcludeEventList(List<ContestEvents> excludeEventList) {
		this.excludeEventList = excludeEventList;
	}
	public PaginationDTO getExcludeEventPaginationDTO() {
		return excludeEventPaginationDTO;
	}
	public void setExcludeEventPaginationDTO(PaginationDTO excludeEventPaginationDTO) {
		this.excludeEventPaginationDTO = excludeEventPaginationDTO;
	}
	
	
	
	
}
