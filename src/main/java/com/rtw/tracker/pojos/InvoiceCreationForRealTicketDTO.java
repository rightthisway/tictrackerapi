package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.CategoryTicketGroup;


public class InvoiceCreationForRealTicketDTO {
	
	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private String sPubKey;
	private EventDetailsDTO eventDetailsDTO;
	private List<TicketGroupDTO> ticketGroupDTOs;
	private String ticketId;
	private String invoiceId;
	private String type;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}	
	public String getsPubKey() {
		return sPubKey;
	}
	public void setsPubKey(String sPubKey) {
		this.sPubKey = sPubKey;
	}
	public EventDetailsDTO getEventDetailsDTO() {
		return eventDetailsDTO;
	}
	public void setEventDetailsDTO(EventDetailsDTO eventDetailsDTO) {
		this.eventDetailsDTO = eventDetailsDTO;
	}
	public List<TicketGroupDTO> getTicketGroupDTOs() {
		return ticketGroupDTOs;
	}
	public void setTicketGroupDTOs(List<TicketGroupDTO> ticketGroupDTOs) {
		this.ticketGroupDTOs = ticketGroupDTOs;
	}
	public String getTicketId() {
		return ticketId;
	}
	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}
	public String getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
