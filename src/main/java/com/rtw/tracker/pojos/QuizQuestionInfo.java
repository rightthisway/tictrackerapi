package com.rtw.tracker.pojos;

import com.rtw.tracker.datas.ContestQuestions;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("QuizQuestionInfo")
public class QuizQuestionInfo {
	
	private Integer status;
	private com.rtw.tmat.utils.Error error; 
	private ContestQuestions quizContestQuestion;
	private String message;
	private Integer noOfQuestions;
	private Integer pqLifeCount;
	
	
	
	public com.rtw.tmat.utils.Error getError() {
		return error;
	}
	public void setError(com.rtw.tmat.utils.Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public ContestQuestions getQuizContestQuestion() {
		return quizContestQuestion;
	}
	public void setQuizContestQuestion(ContestQuestions quizContestQuestion) {
		this.quizContestQuestion = quizContestQuestion;
	}
	public Integer getNoOfQuestions() {
		if(noOfQuestions == null) {
			noOfQuestions = 0;
		}
		return noOfQuestions;
	}
	public void setNoOfQuestions(Integer noOfQuestions) {
		this.noOfQuestions = noOfQuestions;
	}
	public Integer getPqLifeCount() {
		return pqLifeCount;
	}
	public void setPqLifeCount(Integer pqLifeCount) {
		this.pqLifeCount = pqLifeCount;
	}
	
	
	
}
