package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class CustomerInfoFavouriteEventDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private PaginationDTO favouriteEventPaginationDTO;
	private List<CustomerFavouriteEventDTO> customerFavouriteEventDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public PaginationDTO getFavouriteEventPaginationDTO() {
		return favouriteEventPaginationDTO;
	}
	public void setFavouriteEventPaginationDTO(
			PaginationDTO favouriteEventPaginationDTO) {
		this.favouriteEventPaginationDTO = favouriteEventPaginationDTO;
	}
	public List<CustomerFavouriteEventDTO> getCustomerFavouriteEventDTO() {
		return customerFavouriteEventDTO;
	}
	public void setCustomerFavouriteEventDTO(
			List<CustomerFavouriteEventDTO> customerFavouriteEventDTO) {
		this.customerFavouriteEventDTO = customerFavouriteEventDTO;
	}
	
	
}
