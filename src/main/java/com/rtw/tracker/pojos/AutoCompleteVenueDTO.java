package com.rtw.tracker.pojos;

import com.rtw.tmat.utils.Error;

public class AutoCompleteVenueDTO {

	private Integer status;
	private Error error; 
	private String message = new String();
	private String venue;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	
		
}
