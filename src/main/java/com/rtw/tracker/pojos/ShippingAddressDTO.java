package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.CustomerAddress;

public class ShippingAddressDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private List<CustomerAddressDTO> shippingAddressDTOs;
	private PaginationDTO paginationDTO;
	private Integer shippingAddressCount;
	private String customerId;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<CustomerAddressDTO> getShippingAddressDTOs() {
		return shippingAddressDTOs;
	}
	public void setShippingAddressDTOs(List<CustomerAddressDTO> shippingAddressDTOs) {
		this.shippingAddressDTOs = shippingAddressDTOs;
	}
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}
	public Integer getShippingAddressCount() {
		return shippingAddressCount;
	}
	public void setShippingAddressCount(Integer shippingAddressCount) {
		this.shippingAddressCount = shippingAddressCount;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
		
}
