package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class ManualFedexDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private PaginationDTO fedexGenerationPaginationDTO;
	private List<ManualFedexGenerationDTO> fedexGenerationDTO;
	private List<CountryDTO> countryDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public PaginationDTO getFedexGenerationPaginationDTO() {
		return fedexGenerationPaginationDTO;
	}
	public void setFedexGenerationPaginationDTO(
			PaginationDTO fedexGenerationPaginationDTO) {
		this.fedexGenerationPaginationDTO = fedexGenerationPaginationDTO;
	}
	public List<ManualFedexGenerationDTO> getFedexGenerationDTO() {
		return fedexGenerationDTO;
	}
	public void setFedexGenerationDTO(
			List<ManualFedexGenerationDTO> fedexGenerationDTO) {
		this.fedexGenerationDTO = fedexGenerationDTO;
	}
	public List<CountryDTO> getCountryDTO() {
		return countryDTO;
	}
	public void setCountryDTO(List<CountryDTO> countryDTO) {
		this.countryDTO = countryDTO;
	}	
		
}
