package com.rtw.tracker.pojos;



public class ContestsList{

	private Integer id;
	private String contestName;
	private Integer partipantCount;
	private Integer winnerCount;
	private Integer ticketWinnerCount;
	private Integer pointWinnerCount;
	private String status;
	private String createdBy;
	private String updatedBy;
	
	private String startDateTimeStr;
	private String createdDateTimeStr;
	private String updatedDateTimeStr;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getContestName() {
		return contestName;
	}
	public void setContestName(String contestName) {
		this.contestName = contestName;
	}
	public Integer getPartipantCount() {
		return partipantCount;
	}
	public void setPartipantCount(Integer partipantCount) {
		this.partipantCount = partipantCount;
	}
	public Integer getWinnerCount() {
		return winnerCount;
	}
	public void setWinnerCount(Integer winnerCount) {
		this.winnerCount = winnerCount;
	}
	public Integer getTicketWinnerCount() {
		return ticketWinnerCount;
	}
	public void setTicketWinnerCount(Integer ticketWinnerCount) {
		this.ticketWinnerCount = ticketWinnerCount;
	}
	public Integer getPointWinnerCount() {
		return pointWinnerCount;
	}
	public void setPointWinnerCount(Integer pointWinnerCount) {
		this.pointWinnerCount = pointWinnerCount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getStartDateTimeStr() {
		return startDateTimeStr;
	}
	public void setStartDateTimeStr(String startDateTimeStr) {
		this.startDateTimeStr = startDateTimeStr;
	}
	public String getCreatedDateTimeStr() {
		return createdDateTimeStr;
	}
	public void setCreatedDateTimeStr(String createdDateTimeStr) {
		this.createdDateTimeStr = createdDateTimeStr;
	}
	public String getUpdatedDateTimeStr() {
		return updatedDateTimeStr;
	}
	public void setUpdatedDateTimeStr(String updatedDateTimeStr) {
		this.updatedDateTimeStr = updatedDateTimeStr;
	}
	
	
	
	
}