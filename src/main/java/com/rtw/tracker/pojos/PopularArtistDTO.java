package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.PopularArtist;

public class PopularArtistDTO {

	private Integer status;
	private Error error; 
	private String message = new String();
	private PaginationDTO paginationDTO;
	private List<PopularArtistCustomDTO> popularArtistCustomDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}
	public List<PopularArtistCustomDTO> getPopularArtistCustomDTO() {
		return popularArtistCustomDTO;
	}
	public void setPopularArtistCustomDTO(List<PopularArtistCustomDTO> popularArtistCustomDTO) {
		this.popularArtistCustomDTO = popularArtistCustomDTO;
	}
	
}
