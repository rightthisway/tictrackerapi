package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.InvoiceRefund;

public class InvoiceRefundDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private PaginationDTO invoiceRefundPaginationDTO;
	private List<InvoiceRefundListDTO> invoiceRefundListDTO;
	private Integer orderId;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public PaginationDTO getInvoiceRefundPaginationDTO() {
		return invoiceRefundPaginationDTO;
	}
	public void setInvoiceRefundPaginationDTO(
			PaginationDTO invoiceRefundPaginationDTO) {
		this.invoiceRefundPaginationDTO = invoiceRefundPaginationDTO;
	}
	public List<InvoiceRefundListDTO> getInvoiceRefundListDTO() {
		return invoiceRefundListDTO;
	}
	public void setInvoiceRefundListDTO(
			List<InvoiceRefundListDTO> invoiceRefundListDTO) {
		this.invoiceRefundListDTO = invoiceRefundListDTO;
	}
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	
}
