package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class CreatePayInvoiceDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message;
	private CustomerOrderDTO customerOrderDTO;
	private InvoiceDTO invoiceDTO;
	private CustomersCustomDTO customerDTO;
	private List<CustomerCardInfoDTO> customerCardInfoDTO;
	private String sPubKey;
	private Double rewardPoints;
	private Double customerCredit;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public CustomerOrderDTO getCustomerOrderDTO() {
		return customerOrderDTO;
	}
	public void setCustomerOrderDTO(CustomerOrderDTO customerOrderDTO) {
		this.customerOrderDTO = customerOrderDTO;
	}
	public InvoiceDTO getInvoiceDTO() {
		return invoiceDTO;
	}
	public void setInvoiceDTO(InvoiceDTO invoiceDTO) {
		this.invoiceDTO = invoiceDTO;
	}
	public CustomersCustomDTO getCustomerDTO() {
		return customerDTO;
	}
	public void setCustomerDTO(CustomersCustomDTO customerDTO) {
		this.customerDTO = customerDTO;
	}
	public List<CustomerCardInfoDTO> getCustomerCardInfoDTO() {
		return customerCardInfoDTO;
	}
	public void setCustomerCardInfoDTO(List<CustomerCardInfoDTO> customerCardInfoDTO) {
		this.customerCardInfoDTO = customerCardInfoDTO;
	}
	public String getsPubKey() {
		return sPubKey;
	}
	public void setsPubKey(String sPubKey) {
		this.sPubKey = sPubKey;
	}
	public Double getRewardPoints() {
		return rewardPoints;
	}
	public void setRewardPoints(Double rewardPoints) {
		this.rewardPoints = rewardPoints;
	}
	public Double getCustomerCredit() {
		return customerCredit;
	}
	public void setCustomerCredit(Double customerCredit) {
		this.customerCredit = customerCredit;
	}
	
}
