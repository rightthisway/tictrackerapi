package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.CustomerAddress;

public class POGetShippingAddressForFedexDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private PaginationDTO shippingAddressPaginationDTO;
	private List<CustomerAddressDTO> shippingAddressDTO;
	private Integer shippingAddressCount;
	private String poId;
	private Integer customerId;
	private String companyName;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public PaginationDTO getShippingAddressPaginationDTO() {
		return shippingAddressPaginationDTO;
	}
	public void setShippingAddressPaginationDTO(
			PaginationDTO shippingAddressPaginationDTO) {
		this.shippingAddressPaginationDTO = shippingAddressPaginationDTO;
	}
	public List<CustomerAddressDTO> getShippingAddressDTO() {
		return shippingAddressDTO;
	}
	public void setShippingAddressDTO(List<CustomerAddressDTO> shippingAddressDTO) {
		this.shippingAddressDTO = shippingAddressDTO;
	}
	public Integer getShippingAddressCount() {
		return shippingAddressCount;
	}
	public void setShippingAddressCount(Integer shippingAddressCount) {
		this.shippingAddressCount = shippingAddressCount;
	}
	public String getPoId() {
		return poId;
	}
	public void setPoId(String poId) {
		this.poId = poId;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
}
