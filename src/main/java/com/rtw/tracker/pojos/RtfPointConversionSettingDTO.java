package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.RtfPointsConversionSetting;

public class RtfPointConversionSettingDTO {

	private RtfPointsConversionSetting setting;
	private List<RtfPointsConversionSetting> settings;
	private PaginationDTO pagination;
	private Error error = new Error(); 
	private String message = new String();
	private Integer status;
	
	
	public RtfPointsConversionSetting getSetting() {
		return setting;
	}
	public void setSetting(RtfPointsConversionSetting setting) {
		this.setting = setting;
	}
	public List<RtfPointsConversionSetting> getSettings() {
		return settings;
	}
	public void setSettings(List<RtfPointsConversionSetting> settings) {
		this.settings = settings;
	}
	public PaginationDTO getPagination() {
		return pagination;
	}
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
