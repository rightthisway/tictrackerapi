package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.PollingVideoCategory;

public class PollingVideoCategoryDTO {

	private List<PollingVideoCategory> pollingVideoCategories;
	private PaginationDTO pagination;
	private Error error = new Error();
	private String message = new String();
	private Integer status;
	private PollingVideoCategory pollingVideoCategory;

	public PollingVideoCategory getPollingVideoCategory() {
		return pollingVideoCategory;
	}

	public void setPollingVideoCategory(PollingVideoCategory pollingVideoCategory) {
		this.pollingVideoCategory = pollingVideoCategory;
	}

	public List<PollingVideoCategory> getPollingVideoCategories() {
		return pollingVideoCategories;
	}

	public void setPollingVideoCategories(List<PollingVideoCategory> pollingVideoCategories) {
		this.pollingVideoCategories = pollingVideoCategories;
	}

	public PaginationDTO getPagination() {
		return pagination;
	}

	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}
