package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.enums.ProfitLossSign;
import com.rtw.tmat.utils.Error;

public class OpenOrderStatusDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private List<OpenOrdersDTO> openOrders;
	private PaginationDTO openOrdersPaginationDTO;
	private List<EventDetailsDTO> eventDetails;
	private ProfitLossSign[] profitLossSign;
	private String selectedProfitLossSign;
	private String productType;
	private Integer noOfOrders;
	private Integer totalSectionQty;
	private String sectionTotalNetSoldPrice;
	private String sectionTotalActualSoldPrice;
	private String sectionTotalMarketPrice;
	private String sectionTotalProfitLoss;
	private Integer sectionConsideredOrders;
	private Integer sectionNotConsideredOrders;	
	private Integer totalZoneQty;
	private String zoneTotalNetSoldPrice;
	private String zoneTotalActualSoldPrice;
	private String zoneTotalPrice;
	private String zoneTotalProfitLoss;	
	private Integer zoneConsideredOrders;
	private Integer zoneNotConsideredOrders;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<OpenOrdersDTO> getOpenOrders() {
		return openOrders;
	}
	public void setOpenOrders(List<OpenOrdersDTO> openOrders) {
		this.openOrders = openOrders;
	}
	public PaginationDTO getOpenOrdersPaginationDTO() {
		return openOrdersPaginationDTO;
	}
	public void setOpenOrdersPaginationDTO(PaginationDTO openOrdersPaginationDTO) {
		this.openOrdersPaginationDTO = openOrdersPaginationDTO;
	}
	public List<EventDetailsDTO> getEventDetails() {
		return eventDetails;
	}
	public void setEventDetails(List<EventDetailsDTO> eventDetails) {
		this.eventDetails = eventDetails;
	}	
	public ProfitLossSign[] getProfitLossSign() {
		return profitLossSign;
	}
	public void setProfitLossSign(ProfitLossSign[] profitLossSign) {
		this.profitLossSign = profitLossSign;
	}
	public String getSelectedProfitLossSign() {
		return selectedProfitLossSign;
	}
	public void setSelectedProfitLossSign(String selectedProfitLossSign) {
		this.selectedProfitLossSign = selectedProfitLossSign;
	}	
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public Integer getNoOfOrders() {
		return noOfOrders;
	}
	public void setNoOfOrders(Integer noOfOrders) {
		this.noOfOrders = noOfOrders;
	}
	public Integer getTotalSectionQty() {
		return totalSectionQty;
	}
	public void setTotalSectionQty(Integer totalSectionQty) {
		this.totalSectionQty = totalSectionQty;
	}
	public String getSectionTotalNetSoldPrice() {
		return sectionTotalNetSoldPrice;
	}
	public void setSectionTotalNetSoldPrice(String sectionTotalNetSoldPrice) {
		this.sectionTotalNetSoldPrice = sectionTotalNetSoldPrice;
	}
	public String getSectionTotalActualSoldPrice() {
		return sectionTotalActualSoldPrice;
	}
	public void setSectionTotalActualSoldPrice(String sectionTotalActualSoldPrice) {
		this.sectionTotalActualSoldPrice = sectionTotalActualSoldPrice;
	}
	public String getSectionTotalMarketPrice() {
		return sectionTotalMarketPrice;
	}
	public void setSectionTotalMarketPrice(String sectionTotalMarketPrice) {
		this.sectionTotalMarketPrice = sectionTotalMarketPrice;
	}
	public String getSectionTotalProfitLoss() {
		return sectionTotalProfitLoss;
	}
	public void setSectionTotalProfitLoss(String sectionTotalProfitLoss) {
		this.sectionTotalProfitLoss = sectionTotalProfitLoss;
	}
	public Integer getSectionConsideredOrders() {
		return sectionConsideredOrders;
	}
	public void setSectionConsideredOrders(Integer sectionConsideredOrders) {
		this.sectionConsideredOrders = sectionConsideredOrders;
	}
	public Integer getSectionNotConsideredOrders() {
		return sectionNotConsideredOrders;
	}
	public void setSectionNotConsideredOrders(Integer sectionNotConsideredOrders) {
		this.sectionNotConsideredOrders = sectionNotConsideredOrders;
	}
	public Integer getTotalZoneQty() {
		return totalZoneQty;
	}
	public void setTotalZoneQty(Integer totalZoneQty) {
		this.totalZoneQty = totalZoneQty;
	}
	public String getZoneTotalNetSoldPrice() {
		return zoneTotalNetSoldPrice;
	}
	public void setZoneTotalNetSoldPrice(String zoneTotalNetSoldPrice) {
		this.zoneTotalNetSoldPrice = zoneTotalNetSoldPrice;
	}
	public String getZoneTotalActualSoldPrice() {
		return zoneTotalActualSoldPrice;
	}
	public void setZoneTotalActualSoldPrice(String zoneTotalActualSoldPrice) {
		this.zoneTotalActualSoldPrice = zoneTotalActualSoldPrice;
	}
	public String getZoneTotalPrice() {
		return zoneTotalPrice;
	}
	public void setZoneTotalPrice(String zoneTotalPrice) {
		this.zoneTotalPrice = zoneTotalPrice;
	}
	public String getZoneTotalProfitLoss() {
		return zoneTotalProfitLoss;
	}
	public void setZoneTotalProfitLoss(String zoneTotalProfitLoss) {
		this.zoneTotalProfitLoss = zoneTotalProfitLoss;
	}
	public Integer getZoneConsideredOrders() {
		return zoneConsideredOrders;
	}
	public void setZoneConsideredOrders(Integer zoneConsideredOrders) {
		this.zoneConsideredOrders = zoneConsideredOrders;
	}
	public Integer getZoneNotConsideredOrders() {
		return zoneNotConsideredOrders;
	}
	public void setZoneNotConsideredOrders(Integer zoneNotConsideredOrders) {
		this.zoneNotConsideredOrders = zoneNotConsideredOrders;
	}
		
}
