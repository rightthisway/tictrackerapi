package com.rtw.tracker.pojos;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.AffiliateCashRewardHistory;

public class AffiliateOrderSummaryDTO {

	private Integer status;
	private Error error; 
	private String message = new String();
	private AffiliateCashRewardHistory rewardHistory;
	private String activeDate;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public AffiliateCashRewardHistory getRewardHistory() {
		return rewardHistory;
	}
	public void setRewardHistory(AffiliateCashRewardHistory rewardHistory) {
		this.rewardHistory = rewardHistory;
	}
	public String getActiveDate() {
		return activeDate;
	}
	public void setActiveDate(String activeDate) {
		this.activeDate = activeDate;
	}
	
}
