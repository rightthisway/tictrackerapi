package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class ContestServerNodesDTO {

	private List<RtfConfigContestClusterNodes> nodes;
	private Error error = new Error(); 
	private String message = new String();
	private Integer status;
	
	
	public List<RtfConfigContestClusterNodes> getNodes() {
		return nodes;
	}
	public void setNodes(List<RtfConfigContestClusterNodes> nodes) {
		this.nodes = nodes;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	
	
	
}
