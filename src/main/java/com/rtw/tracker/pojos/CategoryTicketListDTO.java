package com.rtw.tracker.pojos;

import java.util.Collection;
import java.util.Set;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.CategoryTicketGroup;
import com.rtw.tracker.datas.EventDetails;

public class CategoryTicketListDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private Collection<CategoryTicketGroup> categoryTickets;
	private Set<Integer> categoryTicketQty;
	private EventDetails eventDetails;
	private PaginationDTO paginationDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Collection<CategoryTicketGroup> getCategoryTickets() {
		return categoryTickets;
	}
	public void setCategoryTickets(Collection<CategoryTicketGroup> categoryTickets) {
		this.categoryTickets = categoryTickets;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Set<Integer> getCategoryTicketQty() {
		return categoryTicketQty;
	}
	public void setCategoryTicketQty(Set<Integer> categoryTicketQty) {
		this.categoryTicketQty = categoryTicketQty;
	}
	public EventDetails getEventDetails() {
		return eventDetails;
	}
	public void setEventDetails(EventDetails eventDetails) {
		this.eventDetails = eventDetails;
	}
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}
	
	
}
