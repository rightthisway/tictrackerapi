package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class CustomerInfoLoyalFanDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private CustomerLoyalFanDTO loyalFanDTO;
	private List<PopularArtistCustomDTO> popularArtistCustomDTO;
	private PaginationDTO popularArtistPaginationDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public CustomerLoyalFanDTO getLoyalFanDTO() {
		return loyalFanDTO;
	}
	public void setLoyalFanDTO(CustomerLoyalFanDTO loyalFanDTO) {
		this.loyalFanDTO = loyalFanDTO;
	}
	public List<PopularArtistCustomDTO> getPopularArtistCustomDTO() {
		return popularArtistCustomDTO;
	}
	public void setPopularArtistCustomDTO(
			List<PopularArtistCustomDTO> popularArtistCustomDTO) {
		this.popularArtistCustomDTO = popularArtistCustomDTO;
	}
	public PaginationDTO getPopularArtistPaginationDTO() {
		return popularArtistPaginationDTO;
	}
	public void setPopularArtistPaginationDTO(
			PaginationDTO popularArtistPaginationDTO) {
		this.popularArtistPaginationDTO = popularArtistPaginationDTO;
	}
	
}
