package com.rtw.tracker.pojos;

import java.io.Serializable;
import java.util.Date;

import com.rtw.tmat.utils.Util;

public class GiftCardAutoComplete implements Serializable{

	private Integer cardId;
	private Integer cardValueId;
	private String title;
	private Integer qty;
	private Double cardAmount;
	private String cardAmountStr;
	
	public Integer getCardId() {
		return cardId;
	}
	public void setCardId(Integer cardId) {
		this.cardId = cardId;
	}
	public Integer getCardValueId() {
		return cardValueId;
	}
	public void setCardValueId(Integer cardValueId) {
		this.cardValueId = cardValueId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	public Double getCardAmount() {
		return cardAmount;
	}
	public void setCardAmount(Double cardAmount) {
		this.cardAmount = cardAmount;
	}
	public String getCardAmountStr() {
		if(cardAmount != null) {
			cardAmountStr = Util.roundOffStr(cardAmount);
		} else {
			cardAmountStr = "0.00";
		}
		return cardAmountStr;
	}
	public void setCardAmountStr(String cardAmountStr) {
		this.cardAmountStr = cardAmountStr;
	}
	
	
}
