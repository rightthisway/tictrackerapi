package com.rtw.tracker.pojos;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("ContWinnerRewardsInfo")
public class ContWinnerRewardsInfo {
	
	private Integer sts;
	private CassError err; 
	private String msg;
	private Double winnerRwds;
	
	
	public Integer getSts() {
		return sts;
	}
	public void setSts(Integer sts) {
		this.sts = sts;
	}
	public CassError getErr() {
		return err;
	}
	public void setErr(CassError err) {
		this.err = err;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Double getWinnerRwds() {
		if(winnerRwds == null) {
			winnerRwds=0.0;
		}
		return winnerRwds;
	}
	public void setWinnerRwds(Double winnerRwds) {
		this.winnerRwds = winnerRwds;
	}
	

	
}
