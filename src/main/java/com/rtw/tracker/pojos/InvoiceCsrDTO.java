package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class InvoiceCsrDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private List<TrackerUserDTO> trackerUserDTO;
	private String invoiceId;
	private String invoiceCsr;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<TrackerUserDTO> getTrackerUserDTO() {
		return trackerUserDTO;
	}
	public void setTrackerUserDTO(List<TrackerUserDTO> trackerUserDTO) {
		this.trackerUserDTO = trackerUserDTO;
	}
	public String getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}
	public String getInvoiceCsr() {
		return invoiceCsr;
	}
	public void setInvoiceCsr(String invoiceCsr) {
		this.invoiceCsr = invoiceCsr;
	}
		
}
