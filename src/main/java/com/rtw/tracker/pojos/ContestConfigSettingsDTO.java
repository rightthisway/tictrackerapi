package com.rtw.tracker.pojos;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.ContestConfigSettings;
import com.rtw.tracker.datas.LoyaltySettings;

public class ContestConfigSettingsDTO {

	private Integer status;
	private Error error; 
	private String message;

	private ContestConfigSettings contestConfigSettings; 
	private LoyaltySettings loyaltySettings; 
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public ContestConfigSettings getContestConfigSettings() {
		return contestConfigSettings;
	}
	public void setContestConfigSettings(ContestConfigSettings contestConfigSettings) {
		this.contestConfigSettings = contestConfigSettings;
	}
	public LoyaltySettings getLoyaltySettings() {
		return loyaltySettings;
	}
	public void setLoyaltySettings(LoyaltySettings loyaltySettings) {
		this.loyaltySettings = loyaltySettings;
	}
	
}
