package com.rtw.tracker.pojos;

import java.util.Collection;
import java.util.Set;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.CategoryTicketGroup;
import com.rtw.tracker.datas.EventDetails;
import com.rtw.tracker.datas.TicketGroup;

public class EventTicketDetailsDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private Collection<CategoryAndLongTickets> categoryAndLongTickets;
	private Set<Integer> ticketQty;
	private PaginationDTO paginationDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Collection<CategoryAndLongTickets> getCategoryAndLongTickets() {
		return categoryAndLongTickets;
	}
	public void setCategoryAndLongTickets(Collection<CategoryAndLongTickets> categoryAndLongTickets) {
		this.categoryAndLongTickets = categoryAndLongTickets;
	}
	public Set<Integer> getTicketQty() {
		return ticketQty;
	}
	public void setTicketQty(Set<Integer> ticketQty) {
		this.ticketQty = ticketQty;
	}
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}
	
}
