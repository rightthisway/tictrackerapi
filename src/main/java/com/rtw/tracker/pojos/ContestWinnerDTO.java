package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class ContestWinnerDTO {

	private String message;
	private Integer status;
	private Error error;
	private List<ContestWinner> summaryWinners;
	private List<ContestWinner> grandWinners;
	private PaginationDTO summaryPagination;
	private PaginationDTO grandWinnerPagination;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	
	public List<ContestWinner> getSummaryWinners() {
		return summaryWinners;
	}
	public void setSummaryWinners(List<ContestWinner> summaryWinners) {
		this.summaryWinners = summaryWinners;
	}
	public List<ContestWinner> getGrandWinners() {
		return grandWinners;
	}
	public void setGrandWinners(List<ContestWinner> grandWinners) {
		this.grandWinners = grandWinners;
	}
	public PaginationDTO getSummaryPagination() {
		return summaryPagination;
	}
	public void setSummaryPagination(PaginationDTO summaryPagination) {
		this.summaryPagination = summaryPagination;
	}
	public PaginationDTO getGrandWinnerPagination() {
		return grandWinnerPagination;
	}
	public void setGrandWinnerPagination(PaginationDTO grandWinnerPagination) {
		this.grandWinnerPagination = grandWinnerPagination;
	}
	
	
	
	
	
}
