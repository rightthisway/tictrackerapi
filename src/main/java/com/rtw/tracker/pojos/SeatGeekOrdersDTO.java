package com.rtw.tracker.pojos;

import com.rtw.tracker.datas.SeatGeekOrderStatus;

public class SeatGeekOrdersDTO {

	private Integer id;
	private String orderId;
	private String orderDateStr;
	private String eventName;
	private String eventDateStr;
	private String eventTimeStr;
	private String venueName;
	private Integer quantity;
	private String section;
	private String row;
	private String totalSaleAmt;
	private String totalPaymentAmt;
	private SeatGeekOrderStatus status;
	private String lastUpdatedDateStr;
	private Integer ticTrackerOrderId;
	private Integer ticTrackerInvoiceId;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getOrderDateStr() {
		return orderDateStr;
	}
	public void setOrderDateStr(String orderDateStr) {
		this.orderDateStr = orderDateStr;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getEventDateStr() {
		return eventDateStr;
	}
	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}
	public String getEventTimeStr() {
		return eventTimeStr;
	}
	public void setEventTimeStr(String eventTimeStr) {
		this.eventTimeStr = eventTimeStr;
	}
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getRow() {
		return row;
	}
	public void setRow(String row) {
		this.row = row;
	}
	public String getTotalSaleAmt() {
		return totalSaleAmt;
	}
	public void setTotalSaleAmt(String totalSaleAmt) {
		this.totalSaleAmt = totalSaleAmt;
	}
	public String getTotalPaymentAmt() {
		return totalPaymentAmt;
	}
	public void setTotalPaymentAmt(String totalPaymentAmt) {
		this.totalPaymentAmt = totalPaymentAmt;
	}
	public SeatGeekOrderStatus getStatus() {
		return status;
	}
	public void setStatus(SeatGeekOrderStatus status) {
		this.status = status;
	}
	public String getLastUpdatedDateStr() {
		return lastUpdatedDateStr;
	}
	public void setLastUpdatedDateStr(String lastUpdatedDateStr) {
		this.lastUpdatedDateStr = lastUpdatedDateStr;
	}
	public Integer getTicTrackerOrderId() {
		return ticTrackerOrderId;
	}
	public void setTicTrackerOrderId(Integer ticTrackerOrderId) {
		this.ticTrackerOrderId = ticTrackerOrderId;
	}
	public Integer getTicTrackerInvoiceId() {
		return ticTrackerInvoiceId;
	}
	public void setTicTrackerInvoiceId(Integer ticTrackerInvoiceId) {
		this.ticTrackerInvoiceId = ticTrackerInvoiceId;
	}
	
}
