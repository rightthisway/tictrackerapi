package com.rtw.tracker.pojos;

import com.rtw.tmat.utils.Error;

public class ReferralContestWinnerDTO {

	private Integer status;
	private Error error; 
	private String message;
	private ReferralContestWinnerListDTO contestWinnerDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public ReferralContestWinnerListDTO getContestWinnerDTO() {
		return contestWinnerDTO;
	}
	public void setContestWinnerDTO(ReferralContestWinnerListDTO contestWinnerDTO) {
		this.contestWinnerDTO = contestWinnerDTO;
	}
	
}
