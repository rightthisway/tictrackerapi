package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.PollingSponsor;
import com.rtw.tracker.datas.PollingVideoCategory;
import com.rtw.tracker.datas.PollingVideoInventory;

public class PollingVideoInventoryDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private List<PollingVideoInventory> videoList;
	private List<PollingVideoCategory> categoryList;	
	private List<PollingSponsor> sponsors;
	private PollingVideoInventory video;
	private PaginationDTO pagination;
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<PollingVideoInventory> getVideoList() {
		return videoList;
	}
	public void setVideoList(List<PollingVideoInventory> videoList) {
		this.videoList = videoList;
	}
	public List<PollingVideoCategory> getCategoryList() {
		return categoryList;
	}
	public void setCategoryList(List<PollingVideoCategory> categoryList) {
		this.categoryList = categoryList;
	}
	public PollingVideoInventory getVideo() {
		return video;
	}
	public void setVideo(PollingVideoInventory video) {
		this.video = video;
	}
	public PaginationDTO getPagination() {
		return pagination;
	}
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}
	public List<PollingSponsor> getSponsors() {
		return sponsors;
	}
	public void setSponsors(List<PollingSponsor> sponsors) {
		this.sponsors = sponsors;
	}
	
	
}
