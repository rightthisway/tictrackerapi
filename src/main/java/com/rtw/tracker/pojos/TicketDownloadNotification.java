package com.rtw.tracker.pojos;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("TicketDownloadNotification")
public class TicketDownloadNotification {

	private Integer status;
	private com.rtw.tmat.utils.Error error; 
	private String message;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public com.rtw.tmat.utils.Error getError() {
		return error;
	}
	public void setError(com.rtw.tmat.utils.Error error) {
		this.error = error;
	}
	
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
