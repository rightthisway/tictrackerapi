package com.rtw.tracker.pojos;

import java.util.List;
import com.rtw.tmat.utils.Error;

public class POPaymentDetailsDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private PaginationDTO poPaginationDTO;
	private List<PurchaseOrderPaymentDetailsDTO> poList;
	private List<PaymentHistoryDTO> rtwPOList;
	private String selectedProductType;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public PaginationDTO getPoPaginationDTO() {
		return poPaginationDTO;
	}
	public void setPoPaginationDTO(PaginationDTO poPaginationDTO) {
		this.poPaginationDTO = poPaginationDTO;
	}
	public List<PurchaseOrderPaymentDetailsDTO> getPoList() {
		return poList;
	}
	public void setPoList(List<PurchaseOrderPaymentDetailsDTO> poList) {
		this.poList = poList;
	}
	public List<PaymentHistoryDTO> getRtwPOList() {
		return rtwPOList;
	}
	public void setRtwPOList(List<PaymentHistoryDTO> rtwPOList) {
		this.rtwPOList = rtwPOList;
	}
	public String getSelectedProductType() {
		return selectedProductType;
	}
	public void setSelectedProductType(String selectedProductType) {
		this.selectedProductType = selectedProductType;
	}
	
}
