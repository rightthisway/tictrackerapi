package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class ContestAffiliateEarningDTO {

	private Integer status;
	private Error error; 
	private String message;
	private List<ContestAffiliateEarning> affiliateEarnings;
	private PaginationDTO paginationDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<ContestAffiliateEarning> getAffiliateEarnings() {
		return affiliateEarnings;
	}
	public void setAffiliateEarnings(List<ContestAffiliateEarning> affiliateEarnings) {
		this.affiliateEarnings = affiliateEarnings;
	}
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}
	
	
	
	
}
