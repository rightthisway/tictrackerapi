package com.rtw.tracker.pojos;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("QuizContestWinnerRewards")
public class QuizContestWinnerRewards {
	
	private Integer status;
	private com.rtw.tmat.utils.Error error; 
	private String message;
	private Double rewardsPerWinner;
	
	public com.rtw.tmat.utils.Error getError() {
		return error;
	}
	public void setError(com.rtw.tmat.utils.Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Double getRewardsPerWinner() {
		return rewardsPerWinner;
	}
	public void setRewardsPerWinner(Double rewardsPerWinner) {
		this.rewardsPerWinner = rewardsPerWinner;
	}
	
	
	
	
	
}
