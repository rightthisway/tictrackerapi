package com.rtw.tracker.pojos;

import java.util.List;
import java.util.Map;

import com.rtw.tmat.utils.Error;

public class GenericResponseDTO {

	private Integer status;
	private Error error; 
	private String message;
	private List<ReportPojo> reportList;
	private Map<String, Object> reportMap;
	
	public Map<String, Object> getReportMap() {
		return reportMap;
	}
	public void setReportMap(Map<String, Object> reportMap) {
		this.reportMap = reportMap;
	}
	public List<ReportPojo> getReportList() {
		return reportList;
	}
	public void setReportList(List<ReportPojo> reportList) {
		this.reportList = reportList;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public com.rtw.tmat.utils.Error getError() {
		return error;
	}
	public void setError(com.rtw.tmat.utils.Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
