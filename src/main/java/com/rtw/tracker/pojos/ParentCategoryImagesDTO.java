package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.ParentCategoryImage;

public class ParentCategoryImagesDTO {

	private Integer status;
	private Error error; 
	private String message = new String();
	private String productName;
	private List<ParentCategoryImage> parentImageList;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public List<ParentCategoryImage> getParentImageList() {
		return parentImageList;
	}
	public void setParentImageList(List<ParentCategoryImage> parentImageList) {
		this.parentImageList = parentImageList;
	}
	
}
