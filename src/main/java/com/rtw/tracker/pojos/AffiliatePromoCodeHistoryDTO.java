package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.AffiliatePromoCodeHistory;

public class AffiliatePromoCodeHistoryDTO {

	private Integer status;
	private Error error; 
	private String message = new String();
	private List<AffiliatePromoCodeHistory> affiliatePromoCodeHistories;
	private PaginationDTO paginationDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<AffiliatePromoCodeHistory> getAffiliatePromoCodeHistories() {
		return affiliatePromoCodeHistories;
	}
	public void setAffiliatePromoCodeHistories(List<AffiliatePromoCodeHistory> affiliatePromoCodeHistories) {
		this.affiliatePromoCodeHistories = affiliatePromoCodeHistories;
	}
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}
	
}
