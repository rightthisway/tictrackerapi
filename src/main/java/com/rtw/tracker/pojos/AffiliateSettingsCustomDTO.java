package com.rtw.tracker.pojos;

import java.util.Date;

public class AffiliateSettingsCustomDTO {

	private Integer userId;
	private Double cashDiscount;
	private Double customerDiscount;
	private Double phoneCashDiscount;
	private Double phoneCustomerDiscount;
	private String status;
	private Boolean isRepeatBusiness;
	private Boolean isEarnRewardPoints;
	private Date lastUpdated;
	private String updatedBy;
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Double getCashDiscount() {
		return cashDiscount;
	}
	public void setCashDiscount(Double cashDiscount) {
		this.cashDiscount = cashDiscount;
	}
	public Double getCustomerDiscount() {
		return customerDiscount;
	}
	public void setCustomerDiscount(Double customerDiscount) {
		this.customerDiscount = customerDiscount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Boolean getIsRepeatBusiness() {
		return isRepeatBusiness;
	}
	public void setIsRepeatBusiness(Boolean isRepeatBusiness) {
		this.isRepeatBusiness = isRepeatBusiness;
	}
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Double getPhoneCashDiscount() {
		return phoneCashDiscount;
	}
	public void setPhoneCashDiscount(Double phoneCashDiscount) {
		this.phoneCashDiscount = phoneCashDiscount;
	}
	public Double getPhoneCustomerDiscount() {
		return phoneCustomerDiscount;
	}
	public void setPhoneCustomerDiscount(Double phoneCustomerDiscount) {
		this.phoneCustomerDiscount = phoneCustomerDiscount;
	}
	public Boolean getIsEarnRewardPoints() {
		return isEarnRewardPoints;
	}
	public void setIsEarnRewardPoints(Boolean isEarnRewardPoints) {
		this.isEarnRewardPoints = isEarnRewardPoints;
	}

	
	
}
