package com.rtw.tracker.pojos;

import java.util.List;
import com.rtw.tmat.utils.Error;

public class CustomerPromotionalOfferDetailsDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private List<CustomerPromotionalOfferDTO> cusPromoOfferDTO;
	private PaginationDTO cusPromoOfferPaginationDTO;
	private List<CustomerPromotionalOfferTrackingDTO> cusPromoOfferTrackingDTO;	
	private PaginationDTO cusPromoOfferTrackingPaginationDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}	
	public List<CustomerPromotionalOfferDTO> getCusPromoOfferDTO() {
		return cusPromoOfferDTO;
	}
	public void setCusPromoOfferDTO(
			List<CustomerPromotionalOfferDTO> cusPromoOfferDTO) {
		this.cusPromoOfferDTO = cusPromoOfferDTO;
	}
	public PaginationDTO getCusPromoOfferPaginationDTO() {
		return cusPromoOfferPaginationDTO;
	}
	public void setCusPromoOfferPaginationDTO(
			PaginationDTO cusPromoOfferPaginationDTO) {
		this.cusPromoOfferPaginationDTO = cusPromoOfferPaginationDTO;
	}	
	public List<CustomerPromotionalOfferTrackingDTO> getCusPromoOfferTrackingDTO() {
		return cusPromoOfferTrackingDTO;
	}
	public void setCusPromoOfferTrackingDTO(
			List<CustomerPromotionalOfferTrackingDTO> cusPromoOfferTrackingDTO) {
		this.cusPromoOfferTrackingDTO = cusPromoOfferTrackingDTO;
	}
	public PaginationDTO getCusPromoOfferTrackingPaginationDTO() {
		return cusPromoOfferTrackingPaginationDTO;
	}
	public void setCusPromoOfferTrackingPaginationDTO(
			PaginationDTO cusPromoOfferTrackingPaginationDTO) {
		this.cusPromoOfferTrackingPaginationDTO = cusPromoOfferTrackingPaginationDTO;
	}
	
}
