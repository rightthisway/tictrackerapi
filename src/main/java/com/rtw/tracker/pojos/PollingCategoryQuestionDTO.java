package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.PollingCategory;
import com.rtw.tracker.datas.PollingCategoryQuestion;

public class PollingCategoryQuestionDTO {

	private Integer status;
	private Error error; 
	private String message;
	private List<PollingCategoryQuestion> questionList;
	private PollingCategory pollingCategory;
	private PaginationDTO paginationDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<PollingCategoryQuestion> getQuestionList() {
		return questionList;
	}
	public void setQuestionList(List<PollingCategoryQuestion> questionList) {
		this.questionList = questionList;
	}
	public PollingCategory getPollingCategory() {
		return pollingCategory;
	}
	public void setPollingCategory(PollingCategory pollingCategory) {
		this.pollingCategory = pollingCategory;
	}
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}
	
}
