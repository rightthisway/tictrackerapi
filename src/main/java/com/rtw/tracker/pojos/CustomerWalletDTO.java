package com.rtw.tracker.pojos;

public class CustomerWalletDTO {

	private String activeCredit;
	private String totalEarnedCredit;
	private String totalUsedCredit;
	
	public String getActiveCredit() {
		return activeCredit;
	}
	public void setActiveCredit(String activeCredit) {
		this.activeCredit = activeCredit;
	}
	public String getTotalEarnedCredit() {
		return totalEarnedCredit;
	}
	public void setTotalEarnedCredit(String totalEarnedCredit) {
		this.totalEarnedCredit = totalEarnedCredit;
	}
	public String getTotalUsedCredit() {
		return totalUsedCredit;
	}
	public void setTotalUsedCredit(String totalUsedCredit) {
		this.totalUsedCredit = totalUsedCredit;
	}	
	
}
