package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class CustomerInfoInvoiceTicketDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private PaginationDTO invoiceTicketPaginationDTO;
	private List<CustomerInvoiceTicketAttachmentDTO> customerInvoiceTicketAttachmentDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public PaginationDTO getInvoiceTicketPaginationDTO() {
		return invoiceTicketPaginationDTO;
	}
	public void setInvoiceTicketPaginationDTO(
			PaginationDTO invoiceTicketPaginationDTO) {
		this.invoiceTicketPaginationDTO = invoiceTicketPaginationDTO;
	}
	public List<CustomerInvoiceTicketAttachmentDTO> getCustomerInvoiceTicketAttachmentDTO() {
		return customerInvoiceTicketAttachmentDTO;
	}
	public void setCustomerInvoiceTicketAttachmentDTO(
			List<CustomerInvoiceTicketAttachmentDTO> customerInvoiceTicketAttachmentDTO) {
		this.customerInvoiceTicketAttachmentDTO = customerInvoiceTicketAttachmentDTO;
	}
	
}
