package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.Cards;
import com.rtw.tracker.datas.ChildCategory;
import com.rtw.tracker.datas.GrandChildCategory;
import com.rtw.tracker.datas.ParentCategory;
import com.rtw.tracker.datas.Product;

public class CardsDTO {

	private Integer status;
	private Error error; 
	private String message;
	private List<Cards> cardsList;				
	private List<ParentCategory> parentCategoryList;
	private List<ChildCategory> childCategoryList;
	private List<GrandChildCategory> grandChildCategoryLsit;
	private Cards card;
	private Product product;
	private String info;
	private String productName;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<Cards> getCardsList() {
		return cardsList;
	}
	public void setCardsList(List<Cards> cardsList) {
		this.cardsList = cardsList;
	}
	public List<ParentCategory> getParentCategoryList() {
		return parentCategoryList;
	}
	public void setParentCategoryList(List<ParentCategory> parentCategoryList) {
		this.parentCategoryList = parentCategoryList;
	}
	public List<ChildCategory> getChildCategoryList() {
		return childCategoryList;
	}
	public void setChildCategoryList(List<ChildCategory> childCategoryList) {
		this.childCategoryList = childCategoryList;
	}
	public List<GrandChildCategory> getGrandChildCategoryLsit() {
		return grandChildCategoryLsit;
	}
	public void setGrandChildCategoryLsit(List<GrandChildCategory> grandChildCategoryLsit) {
		this.grandChildCategoryLsit = grandChildCategoryLsit;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Cards getCard() {
		return card;
	}
	public void setCard(Cards card) {
		this.card = card;
	}
	
}
