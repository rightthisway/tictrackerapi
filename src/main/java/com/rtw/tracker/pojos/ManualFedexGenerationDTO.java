package com.rtw.tracker.pojos;

import java.util.Date;

public class ManualFedexGenerationDTO {

	private Integer id;
	private String blFirstName;
	private String blLastName;
	private String blCompanyName;
	private String blAddress1;
	private String blAddress2;
	private String blCity;
	private Integer blState;
	private String blStateName;
	private Integer blCountry;
	private String blCountryName;
	private String blZipCode;
	private String blPhone;
	private String shCustomerName;
	private String shCompanyName;
	private String shAddress1;
	private String shAddress2;
	private String shCity;
	private Integer shState;
	private String shStateName;
	private Integer shCountry;
	private String shCountryName;
	private String shZipCode;
	private String shPhone;
	private String serviceType;
	private String signatureType;
	private String fedexLabelPath;
	private String trackingNo;
	private String fedexLabelCreated;	
	private String createdDate;
	private String createdBy;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getBlFirstName() {
		return blFirstName;
	}
	public void setBlFirstName(String blFirstName) {
		this.blFirstName = blFirstName;
	}
	public String getBlLastName() {
		return blLastName;
	}
	public void setBlLastName(String blLastName) {
		this.blLastName = blLastName;
	}
	public String getBlCompanyName() {
		return blCompanyName;
	}
	public void setBlCompanyName(String blCompanyName) {
		this.blCompanyName = blCompanyName;
	}
	public String getBlAddress1() {
		return blAddress1;
	}
	public void setBlAddress1(String blAddress1) {
		this.blAddress1 = blAddress1;
	}
	public String getBlAddress2() {
		return blAddress2;
	}
	public void setBlAddress2(String blAddress2) {
		this.blAddress2 = blAddress2;
	}
	public String getBlCity() {
		return blCity;
	}
	public void setBlCity(String blCity) {
		this.blCity = blCity;
	}
	public Integer getBlState() {
		return blState;
	}
	public void setBlState(Integer blState) {
		this.blState = blState;
	}
	public String getBlStateName() {
		return blStateName;
	}
	public void setBlStateName(String blStateName) {
		this.blStateName = blStateName;
	}
	public Integer getBlCountry() {
		return blCountry;
	}
	public void setBlCountry(Integer blCountry) {
		this.blCountry = blCountry;
	}
	public String getBlCountryName() {
		return blCountryName;
	}
	public void setBlCountryName(String blCountryName) {
		this.blCountryName = blCountryName;
	}
	public String getBlZipCode() {
		return blZipCode;
	}
	public void setBlZipCode(String blZipCode) {
		this.blZipCode = blZipCode;
	}
	public String getBlPhone() {
		return blPhone;
	}
	public void setBlPhone(String blPhone) {
		this.blPhone = blPhone;
	}
	public String getShCustomerName() {
		return shCustomerName;
	}
	public void setShCustomerName(String shCustomerName) {
		this.shCustomerName = shCustomerName;
	}
	public String getShCompanyName() {
		return shCompanyName;
	}
	public void setShCompanyName(String shCompanyName) {
		this.shCompanyName = shCompanyName;
	}
	public String getShAddress1() {
		return shAddress1;
	}
	public void setShAddress1(String shAddress1) {
		this.shAddress1 = shAddress1;
	}
	public String getShAddress2() {
		return shAddress2;
	}
	public void setShAddress2(String shAddress2) {
		this.shAddress2 = shAddress2;
	}
	public String getShCity() {
		return shCity;
	}
	public void setShCity(String shCity) {
		this.shCity = shCity;
	}
	public Integer getShState() {
		return shState;
	}
	public void setShState(Integer shState) {
		this.shState = shState;
	}
	public String getShStateName() {
		return shStateName;
	}
	public void setShStateName(String shStateName) {
		this.shStateName = shStateName;
	}
	public Integer getShCountry() {
		return shCountry;
	}
	public void setShCountry(Integer shCountry) {
		this.shCountry = shCountry;
	}
	public String getShCountryName() {
		return shCountryName;
	}
	public void setShCountryName(String shCountryName) {
		this.shCountryName = shCountryName;
	}
	public String getShZipCode() {
		return shZipCode;
	}
	public void setShZipCode(String shZipCode) {
		this.shZipCode = shZipCode;
	}
	public String getShPhone() {
		return shPhone;
	}
	public void setShPhone(String shPhone) {
		this.shPhone = shPhone;
	}
	public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	public String getSignatureType() {
		return signatureType;
	}
	public void setSignatureType(String signatureType) {
		this.signatureType = signatureType;
	}
	public String getFedexLabelPath() {
		return fedexLabelPath;
	}
	public void setFedexLabelPath(String fedexLabelPath) {
		this.fedexLabelPath = fedexLabelPath;
	}
	public String getTrackingNo() {
		return trackingNo;
	}
	public void setTrackingNo(String trackingNo) {
		this.trackingNo = trackingNo;
	}
	public String getFedexLabelCreated() {
		return fedexLabelCreated;
	}
	public void setFedexLabelCreated(String fedexLabelCreated) {
		this.fedexLabelCreated = fedexLabelCreated;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
		
}
