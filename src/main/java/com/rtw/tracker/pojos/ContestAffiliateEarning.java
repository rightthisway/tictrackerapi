package com.rtw.tracker.pojos;

import java.util.Date;

import com.rtw.tmat.utils.Util;

public class ContestAffiliateEarning {
	
	private Integer id;
	private String affilateUserId;
	private String firstName;
	private String lastName;
	private String email;
	private String userId;
	private String phone;
	private String referralCode;
	private String rewardType;
	private Integer customerLives;
	private Integer affiliateLives;
	private Double cashReward;
	private Double rewardDollars;
	private Date createdDate;
	private Date updatedDate;
	private Date signupDate;
	private Date playedDate;
	private String signupDateStr;
	private String createdDateStr;
	private String updatedDateStr;
	private String playedDateStr;
	private String status;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAffilateUserId() {
		return affilateUserId;
	}
	public void setAffilateUserId(String affilateUserId) {
		this.affilateUserId = affilateUserId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getReferralCode() {
		return referralCode;
	}
	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}
	public String getRewardType() {
		return rewardType;
	}
	public void setRewardType(String rewardType) {
		this.rewardType = rewardType;
	}
	public Integer getCustomerLives() {
		return customerLives;
	}
	public void setCustomerLives(Integer customerLives) {
		this.customerLives = customerLives;
	}
	public Integer getAffiliateLives() {
		return affiliateLives;
	}
	public void setAffiliateLives(Integer affiliateLives) {
		this.affiliateLives = affiliateLives;
	}
	public Double getCashReward() {
		return cashReward;
	}
	public void setCashReward(Double cashReward) {
		this.cashReward = cashReward;
	}
	public Double getRewardDollars() {
		return rewardDollars;
	}
	public void setRewardDollars(Double rewardDollars) {
		this.rewardDollars = rewardDollars;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreatedDateStr() {
		if(createdDate!=null){
			createdDateStr = Util.formatDateTimeToMonthDateYearAndHourMinute(createdDate);
		}
		return createdDateStr;
	}
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	public String getUpdatedDateStr() {
		if(updatedDate!=null){
			updatedDateStr = Util.formatDateTimeToMonthDateYearAndHourMinute(updatedDate);
		}
		return updatedDateStr;
	}
	public void setUpdatedDateStr(String updatedDateStr) {
		this.updatedDateStr = updatedDateStr;
	}
	public Date getSignupDate() {
		return signupDate;
	}
	public void setSignupDate(Date signupDate) {
		this.signupDate = signupDate;
	}
	public String getSignupDateStr() {
		if(signupDate!=null){
			signupDateStr = Util.formatDateTimeToMonthDateYearAndHourMinute(signupDate);
		}
		return signupDateStr;
	}
	public void setSignupDateStr(String signupDateStr) {
		this.signupDateStr = signupDateStr;
	}
	
	public Date getPlayedDate() {
		return playedDate;
	}
	public void setPlayedDate(Date playedDate) {
		this.playedDate = playedDate;
	}
	public String getPlayedDateStr() {
		if(playedDate!=null){
			playedDateStr = Util.formatDateTimeToMonthDateYearAndHourMinute(playedDate);
		}
		return playedDateStr;
	}
	public void setPlayedDateStr(String playedDateStr) {
		this.playedDateStr = playedDateStr;
	}
	
	
	
	
	

}
