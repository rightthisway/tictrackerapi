package com.rtw.tracker.pojos;

public class ContestsUserCount {
	
	private Integer optionACount = 0;
	private Integer optionBCount = 0;
	private Integer optionCCount = 0;
	
	public Integer getOptionACount() {
		return optionACount;
	}
	public void setOptionACount(Integer optionACount) {
		this.optionACount = optionACount;
	}
	public Integer getOptionBCount() {
		return optionBCount;
	}
	public void setOptionBCount(Integer optionBCount) {
		this.optionBCount = optionBCount;
	}
	public Integer getOptionCCount() {
		return optionCCount;
	}
	public void setOptionCCount(Integer optionCCount) {
		this.optionCCount = optionCCount;
	}
	
	

}
