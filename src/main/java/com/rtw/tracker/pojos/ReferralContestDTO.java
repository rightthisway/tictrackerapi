package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class ReferralContestDTO {

	private Integer status;
	private Error error; 
	private String message;
	private List<ReferralContestListDTO> contestsListDTO;
	private PaginationDTO paginationDTO;
	private PaginationDTO participantsPaginationDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<ReferralContestListDTO> getContestsListDTO() {
		return contestsListDTO;
	}
	public void setContestsListDTO(List<ReferralContestListDTO> contestsListDTO) {
		this.contestsListDTO = contestsListDTO;
	}
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}	
	public PaginationDTO getParticipantsPaginationDTO() {
		return participantsPaginationDTO;
	}
	public void setParticipantsPaginationDTO(PaginationDTO participantsPaginationDTO) {
		this.participantsPaginationDTO = participantsPaginationDTO;
	}	
	
	
		
	
}
