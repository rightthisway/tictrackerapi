package com.rtw.tracker.pojos;


public class ZoneEvent {
	
	private Integer eventId;
	private String eventName;
	private String eventDate;
	private String eventTime;
	private String artistName;
	private String venueName;
	private String venueCity;
	private String venueState;
	private String venueCountry;
	private Boolean discountFlag;
	
	public Integer getEventId() {
		if(null == eventId ){
			eventId = 0;
		}
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public String getEventName() {
		if(null ==eventName || eventName.isEmpty()){
			eventName="";
		}
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getEventDate() {
		if(null ==eventDate || eventDate.isEmpty()){
			eventDate="";
		}
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	public String getEventTime() {
		if(null ==eventTime || eventTime.isEmpty()){
			eventTime="";
		}
		return eventTime;
	}
	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}
	public String getArtistName() {
		if(null ==artistName || artistName.isEmpty()){
			artistName="";
		}
		return artistName;
	}
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
	public String getVenueName() {
		if(null ==venueName || venueName.isEmpty()){
			venueName="";
		}
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	public String getVenueCity() {
		if(null ==venueCity || venueCity.isEmpty()){
			venueCity="";
		}
		return venueCity;
	}
	public void setVenueCity(String venueCity) {
		this.venueCity = venueCity;
	}
	public String getVenueState() {
		if(null ==venueState || venueState.isEmpty()){
			venueState="";
		}
		return venueState;
	}
	public void setVenueState(String venueState) {
		this.venueState = venueState;
	}
	public String getVenueCountry() {
		if(null ==venueCountry || venueCountry.isEmpty()){
			venueCountry="";
		}
		return venueCountry;
	}
	public void setVenueCountry(String venueCountry) {
		this.venueCountry = venueCountry;
	}
	
	
	public Boolean getDiscountFlag() {
		if(null == discountFlag ){
			discountFlag = false;
		}
		return discountFlag;
	}
	public void setDiscountFlag(Boolean discountFlag) {
		this.discountFlag = discountFlag;
	}
	
}

