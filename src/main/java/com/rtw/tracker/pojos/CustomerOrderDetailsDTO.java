package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class CustomerOrderDetailsDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private CustomerOrderDTO customerOrderDTO;
	private CustomerOrderDetailsInfoDTO customerOrderDetailsInfoDTO;
	private InvoiceDTO invoiceDTO;
	private PaginationDTO auditPaginationDTO;
	private List<InvoiceAuditDTO> invoiceAuditDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public CustomerOrderDTO getCustomerOrderDTO() {
		return customerOrderDTO;
	}
	public void setCustomerOrderDTO(CustomerOrderDTO customerOrderDTO) {
		this.customerOrderDTO = customerOrderDTO;
	}
	public CustomerOrderDetailsInfoDTO getCustomerOrderDetailsInfoDTO() {
		return customerOrderDetailsInfoDTO;
	}
	public void setCustomerOrderDetailsInfoDTO(
			CustomerOrderDetailsInfoDTO customerOrderDetailsInfoDTO) {
		this.customerOrderDetailsInfoDTO = customerOrderDetailsInfoDTO;
	}
	public InvoiceDTO getInvoiceDTO() {
		return invoiceDTO;
	}
	public void setInvoiceDTO(InvoiceDTO invoiceDTO) {
		this.invoiceDTO = invoiceDTO;
	}
	public PaginationDTO getAuditPaginationDTO() {
		return auditPaginationDTO;
	}
	public void setAuditPaginationDTO(PaginationDTO auditPaginationDTO) {
		this.auditPaginationDTO = auditPaginationDTO;
	}
	public List<InvoiceAuditDTO> getInvoiceAuditDTO() {
		return invoiceAuditDTO;
	}
	public void setInvoiceAuditDTO(List<InvoiceAuditDTO> invoiceAuditDTO) {
		this.invoiceAuditDTO = invoiceAuditDTO;
	}
			
}
