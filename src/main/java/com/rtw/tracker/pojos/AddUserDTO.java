package com.rtw.tracker.pojos;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.AffiliateSetting;
import com.rtw.tracker.datas.TrackerBrokers;
import com.rtw.tracker.datas.TrackerUser;

public class AddUserDTO {

	private Integer status;
	private Error error; 
	//private String message = new String();
	private String message;
	private TrackerUser trackerUser;
	private String roleAffiliateBroker;
	private String roleSuperAdmin;
	private String roleUser;
	private String roleContest;
	private String roleSeller;
	private TrackerBrokers trackerBrokers;
	private AffiliateSetting setting;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public TrackerUser getTrackerUser() {
		return trackerUser;
	}
	public void setTrackerUser(TrackerUser trackerUser) {
		this.trackerUser = trackerUser;
	}
	public String getRoleAffiliateBroker() {
		return roleAffiliateBroker;
	}
	public void setRoleAffiliateBroker(String roleAffiliateBroker) {
		this.roleAffiliateBroker = roleAffiliateBroker;
	}	
	public String getRoleSuperAdmin() {
		return roleSuperAdmin;
	}
	public void setRoleSuperAdmin(String roleSuperAdmin) {
		this.roleSuperAdmin = roleSuperAdmin;
	}
	public String getRoleUser() {
		return roleUser;
	}
	public void setRoleUser(String roleUser) {
		this.roleUser = roleUser;
	}	
	public String getRoleContest() {
		return roleContest;
	}
	public void setRoleContest(String roleContest) {
		this.roleContest = roleContest;
	}
	public TrackerBrokers getTrackerBrokers() {
		return trackerBrokers;
	}
	public void setTrackerBrokers(TrackerBrokers trackerBrokers) {
		this.trackerBrokers = trackerBrokers;
	}
	public AffiliateSetting getSetting() {
		return setting;
	}
	public void setSetting(AffiliateSetting setting) {
		this.setting = setting;
	}
	public String getRoleSeller() {
		return roleSeller;
	}
	public void setRoleSeller(String roleSeller) {
		this.roleSeller = roleSeller;
	}
	
}
