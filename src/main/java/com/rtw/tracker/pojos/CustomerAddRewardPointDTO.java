package com.rtw.tracker.pojos;

import com.rtw.tmat.utils.Error;

public class CustomerAddRewardPointDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private CustomerLoyaltyDTO rewardPointsDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public CustomerLoyaltyDTO getRewardPointsDTO() {
		return rewardPointsDTO;
	}
	public void setRewardPointsDTO(CustomerLoyaltyDTO rewardPointsDTO) {
		this.rewardPointsDTO = rewardPointsDTO;
	}
	
}
