package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class POUploadETicketDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private List<TicketGroupDTO> ticketgroupDTO;
	private CustomersCustomDTO customersDTO;
	private PurchaseOrderDTO  purchaseOrderDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<TicketGroupDTO> getTicketgroupDTO() {
		return ticketgroupDTO;
	}
	public void setTicketgroupDTO(List<TicketGroupDTO> ticketgroupDTO) {
		this.ticketgroupDTO = ticketgroupDTO;
	}
	public CustomersCustomDTO getCustomersDTO() {
		return customersDTO;
	}
	public void setCustomersDTO(CustomersCustomDTO customersDTO) {
		this.customersDTO = customersDTO;
	}
	public PurchaseOrderDTO getPurchaseOrderDTO() {
		return purchaseOrderDTO;
	}
	public void setPurchaseOrderDTO(PurchaseOrderDTO purchaseOrderDTO) {
		this.purchaseOrderDTO = purchaseOrderDTO;
	}	
		
}
