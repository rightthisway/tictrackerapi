package com.rtw.tracker.pojos;

import java.util.Collection;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.AffiliateCashReward;
import com.rtw.tracker.datas.AffiliatePromoCodeHistory;

public class AffiliateReportDTO {

	private Integer status;
	private Error error; 
	private String message;
	private Collection<AffiliatesOrderDTO> affiliatesOrderDTO;
	private AffiliateCashReward affiliateCashRewardDTO;
	private AffiliatePromoCodeHistory affiliatePromoCodeHistoryDTO;
	private PaginationDTO paginationDTO;
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public Collection<AffiliatesOrderDTO> getAffiliatesOrderDTO() {
		return affiliatesOrderDTO;
	}
	public void setAffiliatesOrderDTO(
			Collection<AffiliatesOrderDTO> affiliatesOrderDTO) {
		this.affiliatesOrderDTO = affiliatesOrderDTO;
	}
	
	public AffiliateCashReward getAffiliateCashRewardDTO() {
		return affiliateCashRewardDTO;
	}
	public void setAffiliateCashRewardDTO(AffiliateCashReward affiliateCashRewardDTO) {
		this.affiliateCashRewardDTO = affiliateCashRewardDTO;
	}
	
	public AffiliatePromoCodeHistory getAffiliatePromoCodeHistoryDTO() {
		return affiliatePromoCodeHistoryDTO;
	}
	public void setAffiliatePromoCodeHistoryDTO(
			AffiliatePromoCodeHistory affiliatePromoCodeHistoryDTO) {
		this.affiliatePromoCodeHistoryDTO = affiliatePromoCodeHistoryDTO;
	}
	
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}
	
	
}
