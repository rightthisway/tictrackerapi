package com.rtw.tracker.pojos;

import java.util.Collection;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.AffiliateCashRewardHistory;
import com.rtw.tracker.pojos.PaginationDTO;

public class AffiliateCashRewardHistoryDTO {

	private Integer status;
	private Error error; 
	private String message = new String();
	private Collection<AffiliatesOrderDTO> affiliatesOrderDTO;
	private PaginationDTO paginationDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}
	public Collection<AffiliatesOrderDTO> getAffiliatesOrderDTO() {
		return affiliatesOrderDTO;
	}
	public void setAffiliatesOrderDTO(Collection<AffiliatesOrderDTO> affiliatesOrderDTO) {
		this.affiliatesOrderDTO = affiliatesOrderDTO;
	}
	
}
