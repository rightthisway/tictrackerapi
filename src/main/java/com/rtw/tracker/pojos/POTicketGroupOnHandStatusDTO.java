package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class POTicketGroupOnHandStatusDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private PaginationDTO ticketGroupPaginationDTO;
	private List<TicketGroupDTO> ticketGroupDTO;
	private String poId;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public PaginationDTO getTicketGroupPaginationDTO() {
		return ticketGroupPaginationDTO;
	}
	public void setTicketGroupPaginationDTO(PaginationDTO ticketGroupPaginationDTO) {
		this.ticketGroupPaginationDTO = ticketGroupPaginationDTO;
	}
	public List<TicketGroupDTO> getTicketGroupDTO() {
		return ticketGroupDTO;
	}
	public void setTicketGroupDTO(List<TicketGroupDTO> ticketGroupDTO) {
		this.ticketGroupDTO = ticketGroupDTO;
	}
	public String getPoId() {
		return poId;
	}
	public void setPoId(String poId) {
		this.poId = poId;
	}	
	
}
