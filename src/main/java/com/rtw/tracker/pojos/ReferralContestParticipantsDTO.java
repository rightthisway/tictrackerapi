package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class ReferralContestParticipantsDTO {

	private Integer status;
	private Error error; 
	private String message;
	private List<ReferralContestParticipantsListDTO> contestParticipantsListDTO;	
	private PaginationDTO participantsPaginationDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}	
	public List<ReferralContestParticipantsListDTO> getContestParticipantsListDTO() {
		return contestParticipantsListDTO;
	}
	public void setContestParticipantsListDTO(
			List<ReferralContestParticipantsListDTO> contestParticipantsListDTO) {
		this.contestParticipantsListDTO = contestParticipantsListDTO;
	}
	public PaginationDTO getParticipantsPaginationDTO() {
		return participantsPaginationDTO;
	}
	public void setParticipantsPaginationDTO(PaginationDTO participantsPaginationDTO) {
		this.participantsPaginationDTO = participantsPaginationDTO;
	}	
	
	
		
	
}
