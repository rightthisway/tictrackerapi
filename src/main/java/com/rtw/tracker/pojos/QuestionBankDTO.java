package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.QuestionBank;

public class QuestionBankDTO {

	private Integer status;
	private Error error; 
	private String message;
	private List<QuestionBank> questionBankList;
	private PaginationDTO paginationDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<QuestionBank> getQuestionBankList() {
		return questionBankList;
	}
	public void setQuestionBankList(List<QuestionBank> questionBankList) {
		this.questionBankList = questionBankList;
	}
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}
		
}
