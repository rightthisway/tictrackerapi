package com.rtw.tracker.pojos;


public class OpenOrdersDTO {

	private Integer id;
	private Integer invoiceNo;
	private Integer orderId;
	private String invoiceDateStr;
	private String eventName;
	private String eventDateStr;
	private String eventTimeStr;
	private String venueName;
	private String venueCity;
	private String venueState;
	private String venueCountry;
	private String productType;
	private String internalNotes;	
	private Integer soldQty;
	private String customerName;
	private String zone;
	private Integer zoneTixQty;
	private String zoneCheapestPrice;
	private Integer zoneTixGroupCount;
	private String section;
	private String row;
	private String actualSoldPrice;
	private String marketPrice;
	private String lastUpdatedPrice;
	private Integer sectionTixQty;
	private Integer eventTixQty;
	private String totalActualSoldPrice;
	private String totalMarketPrice;
	private String profitAndLoss;
	private String netTotalSoldPrice;
	private String netSoldPrice;
	private String sectionMargin;
	private String zoneTotalPrice;
	private String zoneProfitAndLoss;
	private String zoneMargin;
	private Integer priceUpdateCount;
	private String price;
	private String discountCouponPrice;
	private String url;
	private String shippingMethod;
	private String trackingNo;
	private String secondaryOrderType;
	private Integer poId;
	private String poDateStr;
	private String secondaryOrderId;
	private String lastUpdateStr;
	private Integer tmatEventId;
	private String brokerId;
	private String companyName;
	
	private Integer invoiceId;
	private Integer customerId;
	private String username;
	private String customerType;
	private String custCompanyName;
	private Integer ticketCount;
	private String invoiceTotal;
	private String addressLine1;
	private String country;
	private String state;
	private String city;
	private String zipCode;
	private String phone;
	private Integer invoiceAged;
	private String createdDateStr;
	private String createdBy;
	private String csr;
	private String voidedDateStr;
	private String isInvoiceEmailSent;
	private String purchaseOrderNo;
	private String platform;
	private String fedexLabelCreated;
	private Integer customerOrderId;
	private String lastUpdatedDateStr;
	private String lastUpdatedBy;
	private String status;
	private Integer eventId;
	private String loyalFanOrder;
	private String primaryPaymentMethod;
	private String secondaryPaymentMethod;
	private String thirdPaymentMethod;
	private String orderType;
	
	private Integer openOrderInvoiceId;
	private String openOrderProductType;
	private String openOrderSection;
	private String openOrderRow;
	private Integer openOrderQuantity;
	private Double openOrderPrice;
	private Integer openOrderShippingMethodId;
	private String openOrderShippingMethod;
	private String openOrderCustomerName;
	private String openOrderUserName;
	private String openOrderPhone;
	private String openOrderAddressLine1;
	private String openOrderCountry;
	private String openOrderState;
	private String openOrderCity;
	private String openOrderZipcode;
	private Boolean openOrderRealTixDelivered;
	private String longSale;
	private String pendingAmount;
	
	private String isPoMapped;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(Integer invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	public String getInvoiceDateStr() {
		return invoiceDateStr;
	}
	public void setInvoiceDateStr(String invoiceDateStr) {
		this.invoiceDateStr = invoiceDateStr;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getEventDateStr() {
		return eventDateStr;
	}
	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}
	public String getEventTimeStr() {
		return eventTimeStr;
	}
	public void setEventTimeStr(String eventTimeStr) {
		this.eventTimeStr = eventTimeStr;
	}
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	public String getVenueCity() {
		return venueCity;
	}
	public void setVenueCity(String venueCity) {
		this.venueCity = venueCity;
	}
	public String getVenueState() {
		return venueState;
	}
	public void setVenueState(String venueState) {
		this.venueState = venueState;
	}
	public String getVenueCountry() {
		return venueCountry;
	}
	public void setVenueCountry(String venueCountry) {
		this.venueCountry = venueCountry;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getInternalNotes() {
		return internalNotes;
	}
	public void setInternalNotes(String internalNotes) {
		this.internalNotes = internalNotes;
	}
	public Integer getSoldQty() {
		return soldQty;
	}
	public void setSoldQty(Integer soldQty) {
		this.soldQty = soldQty;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public Integer getZoneTixQty() {
		return zoneTixQty;
	}
	public void setZoneTixQty(Integer zoneTixQty) {
		this.zoneTixQty = zoneTixQty;
	}
	public String getZoneCheapestPrice() {
		return zoneCheapestPrice;
	}
	public void setZoneCheapestPrice(String zoneCheapestPrice) {
		this.zoneCheapestPrice = zoneCheapestPrice;
	}
	public Integer getZoneTixGroupCount() {
		return zoneTixGroupCount;
	}
	public void setZoneTixGroupCount(Integer zoneTixGroupCount) {
		this.zoneTixGroupCount = zoneTixGroupCount;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getRow() {
		return row;
	}
	public void setRow(String row) {
		this.row = row;
	}
	public String getActualSoldPrice() {
		return actualSoldPrice;
	}
	public void setActualSoldPrice(String actualSoldPrice) {
		this.actualSoldPrice = actualSoldPrice;
	}
	public String getMarketPrice() {
		return marketPrice;
	}
	public void setMarketPrice(String marketPrice) {
		this.marketPrice = marketPrice;
	}
	public String getLastUpdatedPrice() {
		return lastUpdatedPrice;
	}
	public void setLastUpdatedPrice(String lastUpdatedPrice) {
		this.lastUpdatedPrice = lastUpdatedPrice;
	}
	public Integer getSectionTixQty() {
		return sectionTixQty;
	}
	public void setSectionTixQty(Integer sectionTixQty) {
		this.sectionTixQty = sectionTixQty;
	}
	public Integer getEventTixQty() {
		return eventTixQty;
	}
	public void setEventTixQty(Integer eventTixQty) {
		this.eventTixQty = eventTixQty;
	}
	public String getTotalActualSoldPrice() {
		return totalActualSoldPrice;
	}
	public void setTotalActualSoldPrice(String totalActualSoldPrice) {
		this.totalActualSoldPrice = totalActualSoldPrice;
	}
	public String getTotalMarketPrice() {
		return totalMarketPrice;
	}
	public void setTotalMarketPrice(String totalMarketPrice) {
		this.totalMarketPrice = totalMarketPrice;
	}
	public String getProfitAndLoss() {
		return profitAndLoss;
	}
	public void setProfitAndLoss(String profitAndLoss) {
		this.profitAndLoss = profitAndLoss;
	}
	public String getNetTotalSoldPrice() {
		return netTotalSoldPrice;
	}
	public void setNetTotalSoldPrice(String netTotalSoldPrice) {
		this.netTotalSoldPrice = netTotalSoldPrice;
	}
	public String getNetSoldPrice() {
		return netSoldPrice;
	}
	public void setNetSoldPrice(String netSoldPrice) {
		this.netSoldPrice = netSoldPrice;
	}
	public String getSectionMargin() {
		return sectionMargin;
	}
	public void setSectionMargin(String sectionMargin) {
		this.sectionMargin = sectionMargin;
	}
	public String getZoneTotalPrice() {
		return zoneTotalPrice;
	}
	public void setZoneTotalPrice(String zoneTotalPrice) {
		this.zoneTotalPrice = zoneTotalPrice;
	}
	public String getZoneProfitAndLoss() {
		return zoneProfitAndLoss;
	}
	public void setZoneProfitAndLoss(String zoneProfitAndLoss) {
		this.zoneProfitAndLoss = zoneProfitAndLoss;
	}
	public String getZoneMargin() {
		return zoneMargin;
	}
	public void setZoneMargin(String zoneMargin) {
		this.zoneMargin = zoneMargin;
	}
	public Integer getPriceUpdateCount() {
		return priceUpdateCount;
	}
	public void setPriceUpdateCount(Integer priceUpdateCount) {
		this.priceUpdateCount = priceUpdateCount;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getDiscountCouponPrice() {
		return discountCouponPrice;
	}
	public void setDiscountCouponPrice(String discountCouponPrice) {
		this.discountCouponPrice = discountCouponPrice;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getShippingMethod() {
		return shippingMethod;
	}
	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}
	public String getTrackingNo() {
		return trackingNo;
	}
	public void setTrackingNo(String trackingNo) {
		this.trackingNo = trackingNo;
	}
	public String getSecondaryOrderType() {
		return secondaryOrderType;
	}
	public void setSecondaryOrderType(String secondaryOrderType) {
		this.secondaryOrderType = secondaryOrderType;
	}
	public Integer getPoId() {
		return poId;
	}
	public void setPoId(Integer poId) {
		this.poId = poId;
	}
	public String getPoDateStr() {
		return poDateStr;
	}
	public void setPoDateStr(String poDateStr) {
		this.poDateStr = poDateStr;
	}
	public String getSecondaryOrderId() {
		return secondaryOrderId;
	}
	public void setSecondaryOrderId(String secondaryOrderId) {
		this.secondaryOrderId = secondaryOrderId;
	}
	public String getLastUpdateStr() {
		return lastUpdateStr;
	}
	public void setLastUpdateStr(String lastUpdateStr) {
		this.lastUpdateStr = lastUpdateStr;
	}
	public Integer getTmatEventId() {
		return tmatEventId;
	}
	public void setTmatEventId(Integer tmatEventId) {
		this.tmatEventId = tmatEventId;
	}
	public String getBrokerId() {
		return brokerId;
	}
	public void setBrokerId(String brokerId) {
		this.brokerId = brokerId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Integer getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public String getCustCompanyName() {
		return custCompanyName;
	}
	public void setCustCompanyName(String custCompanyName) {
		this.custCompanyName = custCompanyName;
	}
	public Integer getTicketCount() {
		return ticketCount;
	}
	public void setTicketCount(Integer ticketCount) {
		this.ticketCount = ticketCount;
	}
	public String getInvoiceTotal() {
		return invoiceTotal;
	}
	public void setInvoiceTotal(String invoiceTotal) {
		this.invoiceTotal = invoiceTotal;
	}
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Integer getInvoiceAged() {
		return invoiceAged;
	}
	public void setInvoiceAged(Integer invoiceAged) {
		this.invoiceAged = invoiceAged;
	}
	public String getCreatedDateStr() {
		return createdDateStr;
	}
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCsr() {
		return csr;
	}
	public void setCsr(String csr) {
		this.csr = csr;
	}
	public String getVoidedDateStr() {
		return voidedDateStr;
	}
	public void setVoidedDateStr(String voidedDateStr) {
		this.voidedDateStr = voidedDateStr;
	}
	public String getIsInvoiceEmailSent() {
		return isInvoiceEmailSent;
	}
	public void setIsInvoiceEmailSent(String isInvoiceEmailSent) {
		this.isInvoiceEmailSent = isInvoiceEmailSent;
	}
	public String getPurchaseOrderNo() {
		return purchaseOrderNo;
	}
	public void setPurchaseOrderNo(String purchaseOrderNo) {
		this.purchaseOrderNo = purchaseOrderNo;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	public String getFedexLabelCreated() {
		return fedexLabelCreated;
	}
	public void setFedexLabelCreated(String fedexLabelCreated) {
		this.fedexLabelCreated = fedexLabelCreated;
	}
	public Integer getCustomerOrderId() {
		return customerOrderId;
	}
	public void setCustomerOrderId(Integer customerOrderId) {
		this.customerOrderId = customerOrderId;
	}
	public String getLastUpdatedDateStr() {
		return lastUpdatedDateStr;
	}
	public void setLastUpdatedDateStr(String lastUpdatedDateStr) {
		this.lastUpdatedDateStr = lastUpdatedDateStr;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public String getLoyalFanOrder() {
		return loyalFanOrder;
	}
	public void setLoyalFanOrder(String loyalFanOrder) {
		this.loyalFanOrder = loyalFanOrder;
	}
	public String getPrimaryPaymentMethod() {
		return primaryPaymentMethod;
	}
	public void setPrimaryPaymentMethod(String primaryPaymentMethod) {
		this.primaryPaymentMethod = primaryPaymentMethod;
	}
	public String getSecondaryPaymentMethod() {
		return secondaryPaymentMethod;
	}
	public void setSecondaryPaymentMethod(String secondaryPaymentMethod) {
		this.secondaryPaymentMethod = secondaryPaymentMethod;
	}
	public String getThirdPaymentMethod() {
		return thirdPaymentMethod;
	}
	public void setThirdPaymentMethod(String thirdPaymentMethod) {
		this.thirdPaymentMethod = thirdPaymentMethod;
	}
	public Integer getOpenOrderInvoiceId() {
		return openOrderInvoiceId;
	}
	public void setOpenOrderInvoiceId(Integer openOrderInvoiceId) {
		this.openOrderInvoiceId = openOrderInvoiceId;
	}
	public String getOpenOrderProductType() {
		return openOrderProductType;
	}
	public void setOpenOrderProductType(String openOrderProductType) {
		this.openOrderProductType = openOrderProductType;
	}
	public String getOpenOrderSection() {
		return openOrderSection;
	}
	public void setOpenOrderSection(String openOrderSection) {
		this.openOrderSection = openOrderSection;
	}
	public String getOpenOrderRow() {
		return openOrderRow;
	}
	public void setOpenOrderRow(String openOrderRow) {
		this.openOrderRow = openOrderRow;
	}
	public Integer getOpenOrderQuantity() {
		return openOrderQuantity;
	}
	public void setOpenOrderQuantity(Integer openOrderQuantity) {
		this.openOrderQuantity = openOrderQuantity;
	}
	public Double getOpenOrderPrice() {
		return openOrderPrice;
	}
	public void setOpenOrderPrice(Double openOrderPrice) {
		this.openOrderPrice = openOrderPrice;
	}
	public Integer getOpenOrderShippingMethodId() {
		return openOrderShippingMethodId;
	}
	public void setOpenOrderShippingMethodId(Integer openOrderShippingMethodId) {
		this.openOrderShippingMethodId = openOrderShippingMethodId;
	}
	public String getOpenOrderShippingMethod() {
		return openOrderShippingMethod;
	}
	public void setOpenOrderShippingMethod(String openOrderShippingMethod) {
		this.openOrderShippingMethod = openOrderShippingMethod;
	}
	public String getOpenOrderCustomerName() {
		return openOrderCustomerName;
	}
	public void setOpenOrderCustomerName(String openOrderCustomerName) {
		this.openOrderCustomerName = openOrderCustomerName;
	}
	public String getOpenOrderUserName() {
		return openOrderUserName;
	}
	public void setOpenOrderUserName(String openOrderUserName) {
		this.openOrderUserName = openOrderUserName;
	}
	public String getOpenOrderPhone() {
		return openOrderPhone;
	}
	public void setOpenOrderPhone(String openOrderPhone) {
		this.openOrderPhone = openOrderPhone;
	}
	public String getOpenOrderAddressLine1() {
		return openOrderAddressLine1;
	}
	public void setOpenOrderAddressLine1(String openOrderAddressLine1) {
		this.openOrderAddressLine1 = openOrderAddressLine1;
	}
	public String getOpenOrderCountry() {
		return openOrderCountry;
	}
	public void setOpenOrderCountry(String openOrderCountry) {
		this.openOrderCountry = openOrderCountry;
	}
	public String getOpenOrderState() {
		return openOrderState;
	}
	public void setOpenOrderState(String openOrderState) {
		this.openOrderState = openOrderState;
	}
	public String getOpenOrderCity() {
		return openOrderCity;
	}
	public void setOpenOrderCity(String openOrderCity) {
		this.openOrderCity = openOrderCity;
	}
	public String getOpenOrderZipcode() {
		return openOrderZipcode;
	}
	public void setOpenOrderZipcode(String openOrderZipcode) {
		this.openOrderZipcode = openOrderZipcode;
	}
	public Boolean getOpenOrderRealTixDelivered() {
		return openOrderRealTixDelivered;
	}
	public void setOpenOrderRealTixDelivered(Boolean openOrderRealTixDelivered) {
		this.openOrderRealTixDelivered = openOrderRealTixDelivered;
	}
	public String getLongSale() {
		return longSale;
	}
	public void setLongSale(String longSale) {
		this.longSale = longSale;
	}
	public String getIsPoMapped() {
		return isPoMapped;
	}
	public void setIsPoMapped(String isPoMapped) {
		this.isPoMapped = isPoMapped;
	}
	public String getPendingAmount() {
		return pendingAmount;
	}
	public void setPendingAmount(String pendingAmount) {
		this.pendingAmount = pendingAmount;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	
	
	
		
}
