package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class FantasyEventsFromGrandChildDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message;
	private PaginationDTO eventTeamTicketPaginationDTO;
	private List<FantasyEventTeamTicketDTO> fantasyEventTeamTicketDTOs;
	private String packageInfo;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public PaginationDTO getEventTeamTicketPaginationDTO() {
		return eventTeamTicketPaginationDTO;
	}
	public void setEventTeamTicketPaginationDTO(
			PaginationDTO eventTeamTicketPaginationDTO) {
		this.eventTeamTicketPaginationDTO = eventTeamTicketPaginationDTO;
	}
	public List<FantasyEventTeamTicketDTO> getFantasyEventTeamTicketDTOs() {
		return fantasyEventTeamTicketDTOs;
	}
	public void setFantasyEventTeamTicketDTOs(
			List<FantasyEventTeamTicketDTO> fantasyEventTeamTicketDTOs) {
		this.fantasyEventTeamTicketDTOs = fantasyEventTeamTicketDTOs;
	}
	public String getPackageInfo() {
		return packageInfo;
	}
	public void setPackageInfo(String packageInfo) {
		this.packageInfo = packageInfo;
	}
	
}
