package com.rtw.tracker.pojos;

import java.util.Collection;
import java.util.List;
import com.rtw.tmat.utils.Error;

public class EmailBlastCustomersDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private List<CustomersCustomDTO> customersDTO;
	private PaginationDTO customersPaginationDTO;
	private List<CountryDTO> countryDTO;
	List<GrandChildCategoryDTO> grandChildDTO;
	List<ChildCategoryDTO> childDTO;
	Collection<EmailTemplateDTO> templateDTO; 
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<CustomersCustomDTO> getCustomersDTO() {
		return customersDTO;
	}
	public void setCustomersDTO(List<CustomersCustomDTO> customersDTO) {
		this.customersDTO = customersDTO;
	}
	public PaginationDTO getCustomersPaginationDTO() {
		return customersPaginationDTO;
	}
	public void setCustomersPaginationDTO(PaginationDTO customersPaginationDTO) {
		this.customersPaginationDTO = customersPaginationDTO;
	}
	public List<CountryDTO> getCountryDTO() {
		return countryDTO;
	}
	public void setCountryDTO(List<CountryDTO> countryDTO) {
		this.countryDTO = countryDTO;
	}
	public List<GrandChildCategoryDTO> getGrandChildDTO() {
		return grandChildDTO;
	}
	public void setGrandChildDTO(List<GrandChildCategoryDTO> grandChildDTO) {
		this.grandChildDTO = grandChildDTO;
	}
	public List<ChildCategoryDTO> getChildDTO() {
		return childDTO;
	}
	public void setChildDTO(List<ChildCategoryDTO> childDTO) {
		this.childDTO = childDTO;
	}
	public Collection<EmailTemplateDTO> getTemplateDTO() {
		return templateDTO;
	}
	public void setTemplateDTO(Collection<EmailTemplateDTO> templateDTO) {
		this.templateDTO = templateDTO;
	}
		
		
}
