package com.rtw.tracker.pojos;

import java.util.List;
import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.RtfRewardConfigInfo;

public class RTFRewardConfigDTO {
	
	private RtfRewardConfigInfo config;
	private List<RtfRewardConfigInfo> configs;
	private PaginationDTO pagination;
	private Error error = new Error(); 
	private String message = new String();
	private Integer status;
	
	
	public RtfRewardConfigInfo getConfig() {
		return config;
	}
	public void setConfig(RtfRewardConfigInfo config) {
		this.config = config;
	}
	public List<RtfRewardConfigInfo> getConfigs() {
		return configs;
	}
	public void setConfigs(List<RtfRewardConfigInfo> configs) {
		this.configs = configs;
	}
	public PaginationDTO getPagination() {
		return pagination;
	}
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}

	
	
}
