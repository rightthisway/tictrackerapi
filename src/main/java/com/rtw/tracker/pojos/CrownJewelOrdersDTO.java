package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.CrownJewelCustomerOrder;
import com.rtw.tracker.datas.FantasyGrandChildCategory;
import com.rtw.tracker.datas.FantasyParentCategory;

public class CrownJewelOrdersDTO {

	private Integer status;
	private String cjStatus;
	private Error error; 
	private String message = new String();
	private String fromDate;
	private String toDate;
	private String teamName;
	private String leagueName;
	private List<CrownJewelCustomerOrderDTO> orders;
	private String parentCategory;
	private String grandChildCategory;
	private List<FantasyParentCategory> parentCategories;
	private List<FantasyGrandChildCategory> grandChildCategories;
	private PaginationDTO paginationDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getTeamName() {
		return teamName;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	public String getLeagueName() {
		return leagueName;
	}
	public void setLeagueName(String leagueName) {
		this.leagueName = leagueName;
	}
	public String getParentCategory() {
		return parentCategory;
	}
	public void setParentCategory(String parentCategory) {
		this.parentCategory = parentCategory;
	}
	public String getGrandChildCategory() {
		return grandChildCategory;
	}
	public void setGrandChildCategory(String grandChildCategory) {
		this.grandChildCategory = grandChildCategory;
	}
	public List<FantasyParentCategory> getParentCategories() {
		return parentCategories;
	}
	public void setParentCategories(List<FantasyParentCategory> parentCategories) {
		this.parentCategories = parentCategories;
	}
	public List<FantasyGrandChildCategory> getGrandChildCategories() {
		return grandChildCategories;
	}
	public void setGrandChildCategories(List<FantasyGrandChildCategory> grandChildCategories) {
		this.grandChildCategories = grandChildCategories;
	}
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}
	public String getCjStatus() {
		return cjStatus;
	}
	public void setCjStatus(String cjStatus) {
		this.cjStatus = cjStatus;
	}
	public List<CrownJewelCustomerOrderDTO> getOrders() {
		return orders;
	}
	public void setOrders(List<CrownJewelCustomerOrderDTO> orders) {
		this.orders = orders;
	}
	
}
