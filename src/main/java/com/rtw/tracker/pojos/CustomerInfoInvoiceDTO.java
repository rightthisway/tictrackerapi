package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class CustomerInfoInvoiceDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private PaginationDTO invoicePaginationDTO;
	private List<CustomerInvoiceDTO> customerInvoiceDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public PaginationDTO getInvoicePaginationDTO() {
		return invoicePaginationDTO;
	}
	public void setInvoicePaginationDTO(PaginationDTO invoicePaginationDTO) {
		this.invoicePaginationDTO = invoicePaginationDTO;
	}
	public List<CustomerInvoiceDTO> getCustomerInvoiceDTO() {
		return customerInvoiceDTO;
	}
	public void setCustomerInvoiceDTO(List<CustomerInvoiceDTO> customerInvoiceDTO) {
		this.customerInvoiceDTO = customerInvoiceDTO;
	}
	
}
