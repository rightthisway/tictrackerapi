package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class POCsrDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private List<TrackerUserDTO> trackerUserDTO;
	private String poId;
	private String poCsr;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<TrackerUserDTO> getTrackerUserDTO() {
		return trackerUserDTO;
	}
	public void setTrackerUserDTO(List<TrackerUserDTO> trackerUserDTO) {
		this.trackerUserDTO = trackerUserDTO;
	}
	public String getPoId() {
		return poId;
	}
	public void setPoId(String poId) {
		this.poId = poId;
	}
	public String getPoCsr() {
		return poCsr;
	}
	public void setPoCsr(String poCsr) {
		this.poCsr = poCsr;
	}	
		
}
