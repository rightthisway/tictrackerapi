package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class PurchaseOrderDetailsDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private PaginationDTO poPaginationDTO;
	private List<PurchaseOrderDTO> purchaseOrderDTO;
	private String poTotal;
	private String layoutProductType;
	private String selectedProductType;
	private String fromDate;
	private String toDate;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public PaginationDTO getPoPaginationDTO() {
		return poPaginationDTO;
	}
	public void setPoPaginationDTO(PaginationDTO poPaginationDTO) {
		this.poPaginationDTO = poPaginationDTO;
	}
	public List<PurchaseOrderDTO> getPurchaseOrderDTO() {
		return purchaseOrderDTO;
	}
	public void setPurchaseOrderDTO(List<PurchaseOrderDTO> purchaseOrderDTO) {
		this.purchaseOrderDTO = purchaseOrderDTO;
	}
	public String getPoTotal() {
		return poTotal;
	}
	public void setPoTotal(String poTotal) {
		this.poTotal = poTotal;
	}
	public String getLayoutProductType() {
		return layoutProductType;
	}
	public void setLayoutProductType(String layoutProductType) {
		this.layoutProductType = layoutProductType;
	}
	public String getSelectedProductType() {
		return selectedProductType;
	}
	public void setSelectedProductType(String selectedProductType) {
		this.selectedProductType = selectedProductType;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
		
}
