package com.rtw.tracker.pojos;

public class GrandChildCategoryDTO {

	private Integer id;
	private String name;
	private Integer tnId;
	private String tnName;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getTnId() {
		return tnId;
	}
	public void setTnId(Integer tnId) {
		this.tnId = tnId;
	}
	public String getTnName() {
		return tnName;
	}
	public void setTnName(String tnName) {
		this.tnName = tnName;
	}
	
}
