package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.Customer;
import com.rtw.tracker.datas.CustomerAddress;
import com.rtw.tracker.datas.GiftCardOrderAttachment;
import com.rtw.tracker.datas.GiftCardOrders;

public class GiftCardOrdersEditDTO {
	
	private Integer status;
	private Error error = new Error(); 
	private String message;
	
	
	private GiftCardOrders giftCardOrders;	
	private CustomerAddress customerAddress;	
	private Customer customer;
	private List<GiftCardOrderAttachment> eGiftAttachments;
	private List<GiftCardOrderAttachment> barcodeAttachments;
	
	public CustomerAddress getCustomerAddress() {
		return customerAddress;
	}	
	public void setCustomerAddress(CustomerAddress customerAddress) {
		this.customerAddress = customerAddress;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}	
	
	public GiftCardOrders getGiftCardOrders() {
		return giftCardOrders;
	}
	
	public void setGiftCardOrders(GiftCardOrders giftCardOrders) {
		this.giftCardOrders = giftCardOrders;
	}	
	
	public CustomerAddress getCustomerShippingAddress() {
		return customerAddress;
	}
	
	public void setCustomerShippingAddress(CustomerAddress customerAddress) {
		this.customerAddress = customerAddress;
	}
	
	public List<GiftCardOrderAttachment> geteGiftAttachments() {
		return eGiftAttachments;
	}
	public void seteGiftAttachments(List<GiftCardOrderAttachment> eGiftAttachments) {
		this.eGiftAttachments = eGiftAttachments;
	}
	
	public List<GiftCardOrderAttachment> getBarcodeAttachments() {
		return barcodeAttachments;
	}
	public void setBarcodeAttachments(List<GiftCardOrderAttachment> barcodeAttachments) {
		this.barcodeAttachments = barcodeAttachments;
	}
	
}
