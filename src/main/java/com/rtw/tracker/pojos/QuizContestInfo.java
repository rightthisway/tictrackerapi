package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tracker.datas.Contests;
import com.rtw.tracker.datas.Customer;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("QuizContestInfo")
public class QuizContestInfo {
	
	private Integer status;
	private com.rtw.tmat.utils.Error error; 
	private Integer ContestId;
	private String contestName;
	private String contestStartDate;
	private String contestStartTime;
	private String contestPriceText;
	private Integer noOfQuestions;
	private Integer maxFreeTixWinners;
	private String message;
	private String videoSourceUrl;
	private String appSyncUrl;
	private String appSyncToken;
	private String jwPlayerLicenceKey;
	private String startingCountdownVideoUrl;
	private Boolean isContestStarted;
	private Integer freeTicketsPerWinner=0;
	private Double contestTotalRewards=0.0;
	private Double contestAnswerRewards=0.0;
	private Double totalActiveRewards=0.0;
	
	
	private String partnerId;
	private String sourceId;
	private String entryId;
	private String liveStreamUrl;
	private String uiConfId;
	
	private Integer pendingRequests=0;
	
	private Customer customer;
	private String customerProfilePicWebView;
	
	private Boolean showJoinButton = false;
	private String joinButtonLabel;
	
	List<Contests> contestList;
	
	public com.rtw.tmat.utils.Error getError() {
		return error;
	}
	public void setError(com.rtw.tmat.utils.Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getContestName() {
		if(contestName == null) {
			contestName = "";
		}
		return contestName;
	}
	public void setContestName(String contestName) {
		this.contestName = contestName;
	}
	public String getContestStartDate() {
		if(contestStartDate == null) {
			contestStartDate = "";
		}
		return contestStartDate;
	}
	public void setContestStartDate(String contestStartDate) {
		this.contestStartDate = contestStartDate;
	}
	public String getContestStartTime() {
		if(contestStartTime == null) {
			contestStartTime = "";
		}
		return contestStartTime;
	}
	public void setContestStartTime(String contestStartTime) {
		this.contestStartTime = contestStartTime;
	}
	public String getContestPriceText() {
		if(contestPriceText == null) {
			contestPriceText = "";
		}
		return contestPriceText;
	}
	public void setContestPriceText(String contestPriceText) {
		this.contestPriceText = contestPriceText;
	}
	public Integer getContestId() {
		if(ContestId == null) {
			ContestId = 0;
		}
		return ContestId;
	}
	public void setContestId(Integer contestId) {
		ContestId = contestId;
	}
	public Integer getMaxFreeTixWinners() {
		if(maxFreeTixWinners == null) {
			maxFreeTixWinners = 0;
		}
		return maxFreeTixWinners;
	}
	public void setMaxFreeTixWinners(Integer maxFreeTixWinners) {
		this.maxFreeTixWinners = maxFreeTixWinners;
	}
	public Integer getNoOfQuestions() {
		if(noOfQuestions == null) {
			noOfQuestions = 0;
		}
		return noOfQuestions;
	}
	public void setNoOfQuestions(Integer noOfQuestions) {
		this.noOfQuestions = noOfQuestions;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public String getCustomerProfilePicWebView() {
		if(customerProfilePicWebView == null) {
			customerProfilePicWebView = "";
		}
		return customerProfilePicWebView;
	}
	public void setCustomerProfilePicWebView(String customerProfilePicWebView) {
		this.customerProfilePicWebView = customerProfilePicWebView;
	}
	public String getVideoSourceUrl() {
		if(videoSourceUrl == null) {
			videoSourceUrl = "";
		}
		return videoSourceUrl;
	}
	public void setVideoSourceUrl(String videoSourceUrl) {
		this.videoSourceUrl = videoSourceUrl;
	}
	public String getAppSyncUrl() {
		if(appSyncUrl == null) {
			appSyncUrl = "";
		}
		return appSyncUrl;
	}
	public void setAppSyncUrl(String appSyncUrl) {
		this.appSyncUrl = appSyncUrl;
	}
	public String getAppSyncToken() {
		if(appSyncToken == null) {
			appSyncToken = "";
		}
		return appSyncToken;
	}
	public void setAppSyncToken(String appSyncToken) {
		this.appSyncToken = appSyncToken;
	}
	public String getJwPlayerLicenceKey() {
		if(jwPlayerLicenceKey == null) {
			jwPlayerLicenceKey = "";
		}
		return jwPlayerLicenceKey;
	}
	public void setJwPlayerLicenceKey(String jwPlayerLicenceKey) {
		this.jwPlayerLicenceKey = jwPlayerLicenceKey;
	}
	public String getStartingCountdownVideoUrl() {
		if(startingCountdownVideoUrl == null) {
			startingCountdownVideoUrl = "";
		}
		return startingCountdownVideoUrl;
	}
	public void setStartingCountdownVideoUrl(String startingCountdownVideoUrl) {
		this.startingCountdownVideoUrl = startingCountdownVideoUrl;
	}
	public Boolean getIsContestStarted() {
		if(isContestStarted == null) {
			isContestStarted = false;
		}
		return isContestStarted;
	}
	public void setIsContestStarted(Boolean isContestStarted) {
		this.isContestStarted = isContestStarted;
	}
	
	public String getSourceId() {
		if(sourceId == null) {
			sourceId = "";
		}
		return sourceId;
	}
	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}
	
	public String getLiveStreamUrl() {
		if(liveStreamUrl == null) {
			liveStreamUrl = "";
		}
		return liveStreamUrl;
	}
	public void setLiveStreamUrl(String liveStreamUrl) {
		this.liveStreamUrl = liveStreamUrl;
	}
	public String getPartnerId() {
		if(partnerId == null) {
			partnerId = "";
		}
		return partnerId;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	public String getEntryId() {
		if(entryId == null) {
			entryId = "";
		}
		return entryId;
	}
	public void setEntryId(String entryId) {
		this.entryId = entryId;
	}
	public String getUiConfId() {
		if(uiConfId == null) {
			uiConfId = "";
		}
		return uiConfId;
	}
	public void setUiConfId(String uiConfId) {
		this.uiConfId = uiConfId;
	}
	public Integer getPendingRequests() {
		if(pendingRequests == null) {
			pendingRequests = 0;
		}
		return pendingRequests;
	}
	public void setPendingRequests(Integer pendingRequests) {
		this.pendingRequests = pendingRequests;
	}
	public Double getContestTotalRewards() {
		if(contestTotalRewards == null) {
			contestTotalRewards = 0.0;
		}
		return contestTotalRewards;
	}
	public void setContestTotalRewards(Double contestTotalRewards) {
		this.contestTotalRewards = contestTotalRewards;
	}
	public List<Contests> getContestList() {
		return contestList;
	}
	public void setContestList(List<Contests> contestList) {
		this.contestList = contestList;
	}
	public Integer getFreeTicketsPerWinner() {
		if(freeTicketsPerWinner == null) {
			freeTicketsPerWinner =0;
		}
		return freeTicketsPerWinner;
	}
	public void setFreeTicketsPerWinner(Integer freeTicketsPerWinner) {
		this.freeTicketsPerWinner = freeTicketsPerWinner;
	}
	public Double getContestAnswerRewards() {
		if(contestAnswerRewards == null) {
			contestAnswerRewards = 0.0;
		}
		return contestAnswerRewards;
	}
	public void setContestAnswerRewards(Double contestAnswerRewards) {
		this.contestAnswerRewards = contestAnswerRewards;
	}
	public Double getTotalActiveRewards() {
		if(totalActiveRewards == null) {
			totalActiveRewards = 0.0;
		}
		return totalActiveRewards;
	}
	public void setTotalActiveRewards(Double totalActiveRewards) {
		this.totalActiveRewards = totalActiveRewards;
	}
	public Boolean getShowJoinButton() {
		if(showJoinButton == null) {
			showJoinButton = false;
		}
		return showJoinButton;
	}
	public void setShowJoinButton(Boolean showJoinButton) {
		this.showJoinButton = showJoinButton;
	}
	public String getJoinButtonLabel() {
		if(joinButtonLabel == null) {
			joinButtonLabel = "";
		}
		return joinButtonLabel;
	}
	public void setJoinButtonLabel(String joinButtonLabel) {
		this.joinButtonLabel = joinButtonLabel;
	}
	
	
	
	
}
