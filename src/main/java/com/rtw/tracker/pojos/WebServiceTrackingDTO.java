package com.rtw.tracker.pojos;

import java.util.Collection;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.WebServiceTracking;

public class WebServiceTrackingDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	Collection<WebServiceTracking> webServiceTrackingList;
	Integer websiteIPCount;
	private PaginationDTO paginationDTO;
	private String fromDate;
	private String toDate;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Collection<WebServiceTracking> getWebServiceTrackingList() {
		return webServiceTrackingList;
	}
	public void setWebServiceTrackingList(Collection<WebServiceTracking> webServiceTrackingList) {
		this.webServiceTrackingList = webServiceTrackingList;
	}
	public Integer getWebsiteIPCount() {
		return websiteIPCount;
	}
	public void setWebsiteIPCount(Integer websiteIPCount) {
		this.websiteIPCount = websiteIPCount;
	}
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	
}
