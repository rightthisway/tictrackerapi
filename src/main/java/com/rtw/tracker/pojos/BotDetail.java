package com.rtw.tracker.pojos;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("BotDetail")
public class BotDetail {
	
	private Integer customerId;
	private String userId; 
	private String email;
	private String phoneNo;	
	private String signupDate;	
	private String signUpPlatform;
	private String signUpDeviceId;
	private String referrerCode;
	private String otpVerified;
	private String allLoggedInDevices;
	private String allIPAddress;
	
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getSignupDate() {
		return signupDate;
	}
	public void setSignupDate(String signupDate) {
		this.signupDate = signupDate;
	}
	public String getSignUpPlatform() {
		return signUpPlatform;
	}
	public void setSignUpPlatform(String signUpPlatform) {
		this.signUpPlatform = signUpPlatform;
	}
	public String getSignUpDeviceId() {
		return signUpDeviceId;
	}
	public void setSignUpDeviceId(String signUpDeviceId) {
		this.signUpDeviceId = signUpDeviceId;
	}
	public String getReferrerCode() {
		return referrerCode;
	}
	public void setReferrerCode(String referrerCode) {
		this.referrerCode = referrerCode;
	}
	public String getOtpVerified() {
		return otpVerified;
	}
	public void setOtpVerified(String otpVerified) {
		this.otpVerified = otpVerified;
	}
	public String getAllLoggedInDevices() {
		return allLoggedInDevices;
	}
	public void setAllLoggedInDevices(String allLoggedInDevices) {
		this.allLoggedInDevices = allLoggedInDevices;
	}
	public String getAllIPAddress() {
		return allIPAddress;
	}
	public void setAllIPAddress(String allIPAddress) {
		this.allIPAddress = allIPAddress;
	}
	
	 
	 
}
