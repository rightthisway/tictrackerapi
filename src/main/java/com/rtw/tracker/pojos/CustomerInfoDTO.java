package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class CustomerInfoDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private List<CustomersCustomDTO> customersDTO;
	private CustomerTopLevelInfoDTO customerTopLevelInfoDTO;
	private CustomerAddressDTO billingAddressDTO;
	private List<CustomerAddressDTO> shippingAddressDTO;
	private PaginationDTO shippingPaginationDTO;
	private Integer shippingCount;
	private List<CustomerStripeCreditCardDTO> creditCardDTO;
	private PaginationDTO creditCardPaginationDTO;
	private CustomerWalletDTO customerWalletDTO;
	private CustomerLoyaltyDTO rewardPointsDTO;
	private CustomerLoyalFanDTO loyalFanDTO;
	private List<CountryDTO> countryDTO;
	private List<StateDTO> stateDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}	
	public List<CustomersCustomDTO> getCustomersDTO() {
		return customersDTO;
	}
	public void setCustomersDTO(List<CustomersCustomDTO> customersDTO) {
		this.customersDTO = customersDTO;
	}
	public CustomerTopLevelInfoDTO getCustomerTopLevelInfoDTO() {
		return customerTopLevelInfoDTO;
	}
	public void setCustomerTopLevelInfoDTO(
			CustomerTopLevelInfoDTO customerTopLevelInfoDTO) {
		this.customerTopLevelInfoDTO = customerTopLevelInfoDTO;
	}
	public CustomerAddressDTO getBillingAddressDTO() {
		return billingAddressDTO;
	}
	public void setBillingAddressDTO(CustomerAddressDTO billingAddressDTO) {
		this.billingAddressDTO = billingAddressDTO;
	}
	public List<CustomerAddressDTO> getShippingAddressDTO() {
		return shippingAddressDTO;
	}
	public void setShippingAddressDTO(List<CustomerAddressDTO> shippingAddressDTO) {
		this.shippingAddressDTO = shippingAddressDTO;
	}	
	public PaginationDTO getShippingPaginationDTO() {
		return shippingPaginationDTO;
	}
	public void setShippingPaginationDTO(PaginationDTO shippingPaginationDTO) {
		this.shippingPaginationDTO = shippingPaginationDTO;
	}
	public Integer getShippingCount() {
		return shippingCount;
	}
	public void setShippingCount(Integer shippingCount) {
		this.shippingCount = shippingCount;
	}	
	public List<CustomerStripeCreditCardDTO> getCreditCardDTO() {
		return creditCardDTO;
	}
	public void setCreditCardDTO(List<CustomerStripeCreditCardDTO> creditCardDTO) {
		this.creditCardDTO = creditCardDTO;
	}
	public PaginationDTO getCreditCardPaginationDTO() {
		return creditCardPaginationDTO;
	}
	public void setCreditCardPaginationDTO(PaginationDTO creditCardPaginationDTO) {
		this.creditCardPaginationDTO = creditCardPaginationDTO;
	}
	public CustomerWalletDTO getCustomerWalletDTO() {
		return customerWalletDTO;
	}
	public void setCustomerWalletDTO(CustomerWalletDTO customerWalletDTO) {
		this.customerWalletDTO = customerWalletDTO;
	}
	public CustomerLoyaltyDTO getRewardPointsDTO() {
		return rewardPointsDTO;
	}
	public void setRewardPointsDTO(CustomerLoyaltyDTO rewardPointsDTO) {
		this.rewardPointsDTO = rewardPointsDTO;
	}
	public CustomerLoyalFanDTO getLoyalFanDTO() {
		return loyalFanDTO;
	}
	public void setLoyalFanDTO(CustomerLoyalFanDTO loyalFanDTO) {
		this.loyalFanDTO = loyalFanDTO;
	}
	public List<CountryDTO> getCountryDTO() {
		return countryDTO;
	}
	public void setCountryDTO(List<CountryDTO> countryDTO) {
		this.countryDTO = countryDTO;
	}
	public List<StateDTO> getStateDTO() {
		return stateDTO;
	}
	public void setStateDTO(List<StateDTO> stateDTO) {
		this.stateDTO = stateDTO;
	}
	
}
