package com.rtw.tracker.pojos;

public class VideoLikeInfo {
	
	 	private Integer sts;
	 	private String nxtCTxt;
		private String msg;
		private CassError err;
		private Integer cuId;
		private Integer vidId;
		private String isFavAdd;
		private String sTo;
		private String smPfm;
		private String mId;
		private Integer rtfPoints;
		
		public Integer getRtfPoints() {
			return rtfPoints;
		}
		public void setRtfPoints(Integer rtfPoints) {
			this.rtfPoints = rtfPoints;
		}
		public String getsTo() {
			return sTo;
		}
		public void setsTo(String sTo) {
			this.sTo = sTo;
		}
		public String getSmPfm() {
			return smPfm;
		}
		public void setSmPfm(String smPfm) {
			this.smPfm = smPfm;
		}
		public String getmId() {
			return mId;
		}
		public void setmId(String mId) {
			this.mId = mId;
		}
		
		
		
	 	public Integer getSts() {
			return sts;
		}
		public void setSts(Integer sts) {
			this.sts = sts;
		}
		public String getNxtCTxt() {
			return nxtCTxt;
		}
		public void setNxtCTxt(String nxtCTxt) {
			this.nxtCTxt = nxtCTxt;
		}
		public String getMsg() {
			return msg;
		}
		public void setMsg(String msg) {
			this.msg = msg;
		}
		public CassError getErr() {
			return err;
		}
		public void setErr(CassError err) {
			this.err = err;
		}
		public Integer getCuId() {
			return cuId;
		}
		public void setCuId(Integer cuId) {
			this.cuId = cuId;
		}
		public Integer getVidId() {
			return vidId;
		}
		public void setVidId(Integer vidId) {
			this.vidId = vidId;
		}
		public String getIsFavAdd() {
			return isFavAdd;
		}
		public void setIsFavAdd(String isFavAdd) {
			this.isFavAdd = isFavAdd;
		}
		
		
		
	
	
}
