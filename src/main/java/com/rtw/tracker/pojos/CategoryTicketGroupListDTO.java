package com.rtw.tracker.pojos;

import com.rtw.tracker.datas.ProductType;

public class CategoryTicketGroupListDTO {

	private Integer id;
	private Integer eventId;
	private String eventName;
	private String eventDate;
	private String eventTime;
	private String venue;
	private String section;
	private String row;
	private Integer quantity;
	private String price;
	private String taxAmount;
	private String sectionRange;
	private String rowRange;
	private String shippingMethod;
	private ProductType productType;
	private String retailPrice;
	private String wholeSalePrice;
	private String facePrice;
	private String loyalFanPrice;
	private String cost;
	private Boolean broadcast;
	private String internalNotes;
	private String externalNotes;
	private String marketPlaceNotes;
	private Integer maxShowing;
	private String seatLow;
	private String seatHigh;
	private String nearTermDisplayOption;
	private Integer brokerId;
	private String ticketType;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	public String getEventTime() {
		return eventTime;
	}
	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getRow() {
		return row;
	}
	public void setRow(String row) {
		this.row = row;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(String taxAmount) {
		this.taxAmount = taxAmount;
	}
	public String getSectionRange() {
		return sectionRange;
	}
	public void setSectionRange(String sectionRange) {
		this.sectionRange = sectionRange;
	}
	public String getRowRange() {
		return rowRange;
	}
	public void setRowRange(String rowRange) {
		this.rowRange = rowRange;
	}
	public String getShippingMethod() {
		return shippingMethod;
	}
	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}
	public ProductType getProductType() {
		return productType;
	}
	public void setProductType(ProductType productType) {
		this.productType = productType;
	}
	public String getRetailPrice() {
		return retailPrice;
	}
	public void setRetailPrice(String retailPrice) {
		this.retailPrice = retailPrice;
	}
	public String getWholeSalePrice() {
		return wholeSalePrice;
	}
	public void setWholeSalePrice(String wholeSalePrice) {
		this.wholeSalePrice = wholeSalePrice;
	}
	public String getFacePrice() {
		return facePrice;
	}
	public void setFacePrice(String facePrice) {
		this.facePrice = facePrice;
	}
	public String getLoyalFanPrice() {
		return loyalFanPrice;
	}
	public void setLoyalFanPrice(String loyalFanPrice) {
		this.loyalFanPrice = loyalFanPrice;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public Boolean getBroadcast() {
		return broadcast;
	}
	public void setBroadcast(Boolean broadcast) {
		this.broadcast = broadcast;
	}
	public String getInternalNotes() {
		return internalNotes;
	}
	public void setInternalNotes(String internalNotes) {
		this.internalNotes = internalNotes;
	}
	public String getExternalNotes() {
		return externalNotes;
	}
	public void setExternalNotes(String externalNotes) {
		this.externalNotes = externalNotes;
	}
	public String getMarketPlaceNotes() {
		return marketPlaceNotes;
	}
	public void setMarketPlaceNotes(String marketPlaceNotes) {
		this.marketPlaceNotes = marketPlaceNotes;
	}
	public Integer getMaxShowing() {
		return maxShowing;
	}
	public void setMaxShowing(Integer maxShowing) {
		this.maxShowing = maxShowing;
	}
	public String getSeatLow() {
		return seatLow;
	}
	public void setSeatLow(String seatLow) {
		this.seatLow = seatLow;
	}
	public String getSeatHigh() {
		return seatHigh;
	}
	public void setSeatHigh(String seatHigh) {
		this.seatHigh = seatHigh;
	}
	public String getNearTermDisplayOption() {
		return nearTermDisplayOption;
	}
	public void setNearTermDisplayOption(String nearTermDisplayOption) {
		this.nearTermDisplayOption = nearTermDisplayOption;
	}
	public Integer getBrokerId() {
		return brokerId;
	}
	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}
	public String getTicketType() {
		return ticketType;
	}
	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}
	
}
