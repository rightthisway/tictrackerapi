package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.LoyalFanParentCategoryImage;
import com.rtw.tracker.datas.ParentCategory;

public class LoyalFanParentCategoryImagesDTO {

	private Integer status;
	private Error error; 
	private String message = new String();
	private String productName;
	private String parentStr;
	private boolean parentCheckAll;
	private List<ParentCategory> parentList;
	private List<ParentCategory> parents;
	private List<LoyalFanParentCategoryImage> parentImageList;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getParentStr() {
		return parentStr;
	}
	public void setParentStr(String parentStr) {
		this.parentStr = parentStr;
	}
	public boolean isParentCheckAll() {
		return parentCheckAll;
	}
	public void setParentCheckAll(boolean parentCheckAll) {
		this.parentCheckAll = parentCheckAll;
	}
	public List<ParentCategory> getParentList() {
		return parentList;
	}
	public void setParentList(List<ParentCategory> parentList) {
		this.parentList = parentList;
	}
	public List<ParentCategory> getParents() {
		return parents;
	}
	public void setParents(List<ParentCategory> parents) {
		this.parents = parents;
	}
	public List<LoyalFanParentCategoryImage> getParentImageList() {
		return parentImageList;
	}
	public void setParentImageList(List<LoyalFanParentCategoryImage> parentImageList) {
		this.parentImageList = parentImageList;
	}

}
