package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class InvoiceDownloadTicketDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private PaginationDTO ticketDownloadPaginationDTO;
	private List<CustomerTicketsDownloadDTO> ticketsDownloadDTO;
	private Integer invoiceId;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public PaginationDTO getTicketDownloadPaginationDTO() {
		return ticketDownloadPaginationDTO;
	}
	public void setTicketDownloadPaginationDTO(
			PaginationDTO ticketDownloadPaginationDTO) {
		this.ticketDownloadPaginationDTO = ticketDownloadPaginationDTO;
	}
	public List<CustomerTicketsDownloadDTO> getTicketsDownloadDTO() {
		return ticketsDownloadDTO;
	}
	public void setTicketsDownloadDTO(
			List<CustomerTicketsDownloadDTO> ticketsDownloadDTO) {
		this.ticketsDownloadDTO = ticketsDownloadDTO;
	}
	public Integer getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}
		
}
