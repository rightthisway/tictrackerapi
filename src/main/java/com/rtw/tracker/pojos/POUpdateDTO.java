package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class POUpdateDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private List<TicketGroupDTO> ticketGroupDTO;
	private PurchaseOrderPaymentDetailsDTO paymentDetailsDTO;
	private PurchaseOrderDTO purchaseOrderDTO;
	private CustomersCustomDTO customerInfoDTO;
	private CustomersCustomDTO customerAddressDTO;
	private List<POShippingMethodDTO> shippingMethodDTO;
	private String shippingMethodName;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<POShippingMethodDTO> getShippingMethodDTO() {
		return shippingMethodDTO;
	}
	public void setShippingMethodDTO(List<POShippingMethodDTO> shippingMethodDTO) {
		this.shippingMethodDTO = shippingMethodDTO;
	}
	public List<TicketGroupDTO> getTicketGroupDTO() {
		return ticketGroupDTO;
	}
	public void setTicketGroupDTO(List<TicketGroupDTO> ticketGroupDTO) {
		this.ticketGroupDTO = ticketGroupDTO;
	}
	public PurchaseOrderPaymentDetailsDTO getPaymentDetailsDTO() {
		return paymentDetailsDTO;
	}
	public void setPaymentDetailsDTO(
			PurchaseOrderPaymentDetailsDTO paymentDetailsDTO) {
		this.paymentDetailsDTO = paymentDetailsDTO;
	}
	public PurchaseOrderDTO getPurchaseOrderDTO() {
		return purchaseOrderDTO;
	}
	public void setPurchaseOrderDTO(PurchaseOrderDTO purchaseOrderDTO) {
		this.purchaseOrderDTO = purchaseOrderDTO;
	}
	public CustomersCustomDTO getCustomerInfoDTO() {
		return customerInfoDTO;
	}
	public void setCustomerInfoDTO(CustomersCustomDTO customerInfoDTO) {
		this.customerInfoDTO = customerInfoDTO;
	}
	public CustomersCustomDTO getCustomerAddressDTO() {
		return customerAddressDTO;
	}
	public void setCustomerAddressDTO(CustomersCustomDTO customerAddressDTO) {
		this.customerAddressDTO = customerAddressDTO;
	}
	public String getShippingMethodName() {
		return shippingMethodName;
	}
	public void setShippingMethodName(String shippingMethodName) {
		this.shippingMethodName = shippingMethodName;
	}
			
}
