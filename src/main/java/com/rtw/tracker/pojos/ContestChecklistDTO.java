package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.Contests;
import com.rtw.tracker.datas.PreContestChecklist;
import com.rtw.tracker.datas.TrackerUser;


public class ContestChecklistDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private PreContestChecklist preContestChecklist;
	List<PreContestChecklist> preContestChecklists;
	private Contests contest;
	private List<TrackerUser> users;
	private PaginationDTO preContestPaginationDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public Contests getContest() {
		return contest;
	}
	public void setContest(Contests contest) {
		this.contest = contest;
	}
	public List<TrackerUser> getUsers() {
		return users;
	}
	public void setUsers(List<TrackerUser> users) {
		this.users = users;
	}
	
	public PreContestChecklist getPreContestChecklist() {
		return preContestChecklist;
	}
	public void setPreContestChecklist(PreContestChecklist preContestChecklist) {
		this.preContestChecklist = preContestChecklist;
	}
	
	public List<PreContestChecklist> getPreContestChecklists() {
		return preContestChecklists;
	}
	public void setPreContestChecklists(List<PreContestChecklist> preContestChecklists) {
		this.preContestChecklists = preContestChecklists;
	}
	public PaginationDTO getPreContestPaginationDTO() {
		return preContestPaginationDTO;
	}
	public void setPreContestPaginationDTO(PaginationDTO preContestPaginationDTO) {
		this.preContestPaginationDTO = preContestPaginationDTO;
	}
	
	
	
	
}
