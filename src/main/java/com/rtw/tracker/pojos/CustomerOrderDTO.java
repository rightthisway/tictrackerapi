package com.rtw.tracker.pojos;

import com.rtw.tracker.enums.PartialPaymentMethod;
import com.rtw.tracker.enums.PaymentMethod;

public class CustomerOrderDTO {

	private Integer id;
	private Double orderTotal;
	private String orderCreateDate;
	private String section;
	private Integer qty;
	private String seatLow;
	private String seatHigh;
	private Double ticketPrice;
	private Double soldPrice;
	private String shippingMethod;
	private Integer eventId;
	private String eventName;
	private String eventDateStr;
	private String eventTimeStr;
	private String venueName;
	private PaymentMethod primaryPaymentMethod;
	private Double primaryPayAmt;
	private PartialPaymentMethod secondaryPaymentMethod;
	private Double secondaryPayAmt;
	private PartialPaymentMethod thirdPaymentMethod;
	private Double thirdPayAmt;
	
	private Integer orderId;
	private Double primaryAvailableAmt;
	private Double secondaryAvailableAmt;
	private Double thirdAvailableAmt;
	
	private Integer ticketGroupId;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Double getOrderTotal() {
		return orderTotal;
	}
	public void setOrderTotal(Double orderTotal) {
		this.orderTotal = orderTotal;
	}
	public String getOrderCreateDate() {
		return orderCreateDate;
	}
	public void setOrderCreateDate(String orderCreateDate) {
		this.orderCreateDate = orderCreateDate;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	public String getSeatLow() {
		return seatLow;
	}
	public void setSeatLow(String seatLow) {
		this.seatLow = seatLow;
	}
	public String getSeatHigh() {
		return seatHigh;
	}
	public void setSeatHigh(String seatHigh) {
		this.seatHigh = seatHigh;
	}
	public Double getTicketPrice() {
		return ticketPrice;
	}
	public void setTicketPrice(Double ticketPrice) {
		this.ticketPrice = ticketPrice;
	}
	public Double getSoldPrice() {
		return soldPrice;
	}
	public void setSoldPrice(Double soldPrice) {
		this.soldPrice = soldPrice;
	}
	public String getShippingMethod() {
		return shippingMethod;
	}
	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getEventDateStr() {
		return eventDateStr;
	}
	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}
	public String getEventTimeStr() {
		return eventTimeStr;
	}
	public void setEventTimeStr(String eventTimeStr) {
		this.eventTimeStr = eventTimeStr;
	}
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	public PaymentMethod getPrimaryPaymentMethod() {
		return primaryPaymentMethod;
	}
	public void setPrimaryPaymentMethod(PaymentMethod primaryPaymentMethod) {
		this.primaryPaymentMethod = primaryPaymentMethod;
	}
	public Double getPrimaryPayAmt() {
		return primaryPayAmt;
	}
	public void setPrimaryPayAmt(Double primaryPayAmt) {
		this.primaryPayAmt = primaryPayAmt;
	}
	public PartialPaymentMethod getSecondaryPaymentMethod() {
		return secondaryPaymentMethod;
	}
	public void setSecondaryPaymentMethod(
			PartialPaymentMethod secondaryPaymentMethod) {
		this.secondaryPaymentMethod = secondaryPaymentMethod;
	}
	public Double getSecondaryPayAmt() {
		return secondaryPayAmt;
	}
	public void setSecondaryPayAmt(Double secondaryPayAmt) {
		this.secondaryPayAmt = secondaryPayAmt;
	}
	public PartialPaymentMethod getThirdPaymentMethod() {
		return thirdPaymentMethod;
	}
	public void setThirdPaymentMethod(PartialPaymentMethod thirdPaymentMethod) {
		this.thirdPaymentMethod = thirdPaymentMethod;
	}
	public Double getThirdPayAmt() {
		return thirdPayAmt;
	}
	public void setThirdPayAmt(Double thirdPayAmt) {
		this.thirdPayAmt = thirdPayAmt;
	}
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	public Double getPrimaryAvailableAmt() {
		return primaryAvailableAmt;
	}
	public void setPrimaryAvailableAmt(Double primaryAvailableAmt) {
		this.primaryAvailableAmt = primaryAvailableAmt;
	}
	public Double getSecondaryAvailableAmt() {
		return secondaryAvailableAmt;
	}
	public void setSecondaryAvailableAmt(Double secondaryAvailableAmt) {
		this.secondaryAvailableAmt = secondaryAvailableAmt;
	}
	public Double getThirdAvailableAmt() {
		return thirdAvailableAmt;
	}
	public void setThirdAvailableAmt(Double thirdAvailableAmt) {
		this.thirdAvailableAmt = thirdAvailableAmt;
	}
	public Integer getTicketGroupId() {
		return ticketGroupId;
	}
	public void setTicketGroupId(Integer ticketGroupId) {
		this.ticketGroupId = ticketGroupId;
	}
	
}
