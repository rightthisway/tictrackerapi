package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class AffiliatesDTO {

	private Integer status;
	private Error error; 
	private String message = new String();
	private List<ManageAffiliatesDTO> manageAffiliatesDTOs;
	private PaginationDTO paginationDTO;
	private PaginationDTO affiliatesPaginationDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}
	public PaginationDTO getAffiliatesPaginationDTO() {
		return affiliatesPaginationDTO;
	}
	public void setAffiliatesPaginationDTO(PaginationDTO affiliatesPaginationDTO) {
		this.affiliatesPaginationDTO = affiliatesPaginationDTO;
	}
	public List<ManageAffiliatesDTO> getManageAffiliatesDTOs() {
		return manageAffiliatesDTOs;
	}
	public void setManageAffiliatesDTOs(List<ManageAffiliatesDTO> manageAffiliatesDTOs) {
		this.manageAffiliatesDTOs = manageAffiliatesDTOs;
	}	
	
}
