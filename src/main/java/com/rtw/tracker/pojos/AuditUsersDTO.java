package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class AuditUsersDTO {

	private Integer status;
	private Error error; 
	private String message = new String();
	private List<UserActionDTO> userActionDTOs;
	private PaginationDTO auditPaginationDTO;
	private String userEmail;
	private String fromDate;
	private String toDate;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<UserActionDTO> getUserActionDTOs() {
		return userActionDTOs;
	}
	public void setUserActionDTOs(List<UserActionDTO> userActionDTOs) {
		this.userActionDTOs = userActionDTOs;
	}
	public PaginationDTO getAuditPaginationDTO() {
		return auditPaginationDTO;
	}
	public void setAuditPaginationDTO(PaginationDTO auditPaginationDTO) {
		this.auditPaginationDTO = auditPaginationDTO;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	
}
