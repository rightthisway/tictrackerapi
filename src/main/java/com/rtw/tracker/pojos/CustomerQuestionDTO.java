package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.CustomerContestQuestion;

public class CustomerQuestionDTO {


	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private List<CustomerContestQuestion> customerQuestions;
	private PaginationDTO questionPagination;
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<CustomerContestQuestion> getCustomerQuestions() {
		return customerQuestions;
	}
	public void setCustomerQuestions(List<CustomerContestQuestion> customerQuestions) {
		this.customerQuestions = customerQuestions;
	}
	public PaginationDTO getQuestionPagination() {
		return questionPagination;
	}
	public void setQuestionPagination(PaginationDTO questionPagination) {
		this.questionPagination = questionPagination;
	}
	
	
	
	
}
