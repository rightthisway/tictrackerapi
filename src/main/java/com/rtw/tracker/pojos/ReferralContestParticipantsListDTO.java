package com.rtw.tracker.pojos;


public class ReferralContestParticipantsListDTO {

	private Integer id;
	private Integer contestId;
	private Integer customerId;
	private String customerName;
	private String customerEmail;
	private Integer purchaseCustomerId;
	private String purchaseCustomerName;
	private String purchaseCustomerEmail;
	private String referralCode;
	private String createdDate;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getContestId() {
		return contestId;
	}
	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerEmail() {
		return customerEmail;
	}
	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}
	public Integer getPurchaseCustomerId() {
		return purchaseCustomerId;
	}
	public void setPurchaseCustomerId(Integer purchaseCustomerId) {
		this.purchaseCustomerId = purchaseCustomerId;
	}
	public String getPurchaseCustomerName() {
		return purchaseCustomerName;
	}
	public void setPurchaseCustomerName(String purchaseCustomerName) {
		this.purchaseCustomerName = purchaseCustomerName;
	}
	public String getPurchaseCustomerEmail() {
		return purchaseCustomerEmail;
	}
	public void setPurchaseCustomerEmail(String purchaseCustomerEmail) {
		this.purchaseCustomerEmail = purchaseCustomerEmail;
	}
	public String getReferralCode() {
		return referralCode;
	}
	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	
}
