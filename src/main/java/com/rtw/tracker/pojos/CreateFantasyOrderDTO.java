package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class CreateFantasyOrderDTO {

	private Integer status;
	private Error error; 
	private String message = new String();
	private List<FantasyParentCategoryDTO> fantasyParentCategoryDTOs;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<FantasyParentCategoryDTO> getFantasyParentCategoryDTOs() {
		return fantasyParentCategoryDTOs;
	}
	public void setFantasyParentCategoryDTOs(
			List<FantasyParentCategoryDTO> fantasyParentCategoryDTOs) {
		this.fantasyParentCategoryDTOs = fantasyParentCategoryDTOs;
	}
	
}
