package com.rtw.tracker.pojos;

import com.rtw.tmat.utils.Error;

public class CrownJewelOrderSummaryDTO {

	private Integer status;
	private Error error; 
	private String message = new String();
	private CategoryTicketGroupListDTO categoryTicketGroupDTO;
	private CrownJewelCustomerOrderDTO crownJewelCustomerOrderDTO;
	private CustomersCustomDTO customersCustomDTO;
	private EventDetailsDTO eventDetailsDTO;
	private CustomerLoyaltyDTO customerLoyaltyDTO;
	private Integer invoiceId;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public CategoryTicketGroupListDTO getCategoryTicketGroupDTO() {
		return categoryTicketGroupDTO;
	}
	public void setCategoryTicketGroupDTO(
			CategoryTicketGroupListDTO categoryTicketGroupDTO) {
		this.categoryTicketGroupDTO = categoryTicketGroupDTO;
	}
	public CrownJewelCustomerOrderDTO getCrownJewelCustomerOrderDTO() {
		return crownJewelCustomerOrderDTO;
	}
	public void setCrownJewelCustomerOrderDTO(
			CrownJewelCustomerOrderDTO crownJewelCustomerOrderDTO) {
		this.crownJewelCustomerOrderDTO = crownJewelCustomerOrderDTO;
	}
	public CustomersCustomDTO getCustomersCustomDTO() {
		return customersCustomDTO;
	}
	public void setCustomersCustomDTO(CustomersCustomDTO customersCustomDTO) {
		this.customersCustomDTO = customersCustomDTO;
	}
	public EventDetailsDTO getEventDetailsDTO() {
		return eventDetailsDTO;
	}
	public void setEventDetailsDTO(EventDetailsDTO eventDetailsDTO) {
		this.eventDetailsDTO = eventDetailsDTO;
	}
	public CustomerLoyaltyDTO getCustomerLoyaltyDTO() {
		return customerLoyaltyDTO;
	}
	public void setCustomerLoyaltyDTO(CustomerLoyaltyDTO customerLoyaltyDTO) {
		this.customerLoyaltyDTO = customerLoyaltyDTO;
	}
	public Integer getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}
	
}
