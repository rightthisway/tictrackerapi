package com.rtw.tracker.pojos;

public class DiscountCodeTrackingDTO {

	private Integer customerId;
	private String firstName;
	private String lastName;	
	private String email;
	private String referrerCode;
	private Integer faceBookCnt;
	private Integer twitterCnt;
	private Integer linkedinCnt;
	private Integer whatsappCnt;
	private Integer androidCnt;
	private Integer imessageCnt;
	private Integer googleCnt;
	private Integer outlookCnt;
	
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getReferrerCode() {
		return referrerCode;
	}
	public void setReferrerCode(String referrerCode) {
		this.referrerCode = referrerCode;
	}
	public Integer getFaceBookCnt() {
		return faceBookCnt;
	}
	public void setFaceBookCnt(Integer faceBookCnt) {
		this.faceBookCnt = faceBookCnt;
	}
	public Integer getTwitterCnt() {
		return twitterCnt;
	}
	public void setTwitterCnt(Integer twitterCnt) {
		this.twitterCnt = twitterCnt;
	}
	public Integer getLinkedinCnt() {
		return linkedinCnt;
	}
	public void setLinkedinCnt(Integer linkedinCnt) {
		this.linkedinCnt = linkedinCnt;
	}
	public Integer getWhatsappCnt() {
		return whatsappCnt;
	}
	public void setWhatsappCnt(Integer whatsappCnt) {
		this.whatsappCnt = whatsappCnt;
	}
	public Integer getAndroidCnt() {
		return androidCnt;
	}
	public void setAndroidCnt(Integer androidCnt) {
		this.androidCnt = androidCnt;
	}
	public Integer getImessageCnt() {
		return imessageCnt;
	}
	public void setImessageCnt(Integer imessageCnt) {
		this.imessageCnt = imessageCnt;
	}
	public Integer getGoogleCnt() {
		return googleCnt;
	}
	public void setGoogleCnt(Integer googleCnt) {
		this.googleCnt = googleCnt;
	}
	public Integer getOutlookCnt() {
		return outlookCnt;
	}
	public void setOutlookCnt(Integer outlookCnt) {
		this.outlookCnt = outlookCnt;
	}	
	
}
