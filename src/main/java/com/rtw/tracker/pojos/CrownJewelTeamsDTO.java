package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.CrownJewelCategoryTeams;
import com.rtw.tracker.datas.CrownJewelLeagues;
import com.rtw.tracker.datas.CrownJewelTeams;
import com.rtw.tracker.datas.FantasyChildCategory;
import com.rtw.tracker.datas.FantasyGrandChildCategory;
import com.rtw.tracker.datas.FantasyParentCategory;

public class CrownJewelTeamsDTO {

	private Integer status;
	private Error error; 
	private String message = new String();
	private String childId;
	private String grandChildId;
	private FantasyParentCategory fantasyParentCategory;
	private FantasyChildCategory fantasyChildCategory;
	private FantasyGrandChildCategory fantasyGrandChildCategory;
	private List<FantasyChildCategory> childCategories;
	private List<FantasyGrandChildCategory> grandChildCategories;
	private List<FantasyParentCategory> parentCategoryList;
	private List<CrownJewelLeagues> leaguesList;
	private List<CrownJewelTeams> teamsList;
	private List<CrownJewelCategoryTeams> crownJewelCategoryTeams;
	private List<FantasyChildCategory> childCategory;
	private List<FantasyGrandChildCategory> grandChildCategory;
	private String copyFromLeagueId;
	private String leagueId;
	private String statusId;
	private PaginationDTO paginationDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getChildId() {
		return childId;
	}
	public void setChildId(String childId) {
		this.childId = childId;
	}
	public String getGrandChildId() {
		return grandChildId;
	}
	public void setGrandChildId(String grandChildId) {
		this.grandChildId = grandChildId;
	}
	public List<FantasyChildCategory> getChildCategories() {
		return childCategories;
	}
	public void setChildCategories(List<FantasyChildCategory> childCategories) {
		this.childCategories = childCategories;
	}
	public List<FantasyGrandChildCategory> getGrandChildCategories() {
		return grandChildCategories;
	}
	public void setGrandChildCategories(List<FantasyGrandChildCategory> grandChildCategories) {
		this.grandChildCategories = grandChildCategories;
	}
	public List<FantasyParentCategory> getParentCategoryList() {
		return parentCategoryList;
	}
	public void setParentCategoryList(List<FantasyParentCategory> parentCategoryList) {
		this.parentCategoryList = parentCategoryList;
	}
	public List<CrownJewelLeagues> getLeaguesList() {
		return leaguesList;
	}
	public void setLeaguesList(List<CrownJewelLeagues> leaguesList) {
		this.leaguesList = leaguesList;
	}
	public List<CrownJewelTeams> getTeamsList() {
		return teamsList;
	}
	public void setTeamsList(List<CrownJewelTeams> teamsList) {
		this.teamsList = teamsList;
	}
	public String getCopyFromLeagueId() {
		return copyFromLeagueId;
	}
	public void setCopyFromLeagueId(String copyFromLeagueId) {
		this.copyFromLeagueId = copyFromLeagueId;
	}
	public String getLeagueId() {
		return leagueId;
	}
	public void setLeagueId(String leagueId) {
		this.leagueId = leagueId;
	}
	public String getStatusId() {
		return statusId;
	}
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}
	public List<FantasyChildCategory> getChildCategory() {
		return childCategory;
	}
	public void setChildCategory(List<FantasyChildCategory> childCategory) {
		this.childCategory = childCategory;
	}	
	public List<FantasyGrandChildCategory> getGrandChildCategory() {
		return grandChildCategory;
	}
	public void setGrandChildCategory(
			List<FantasyGrandChildCategory> grandChildCategory) {
		this.grandChildCategory = grandChildCategory;
	}
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}
	public List<CrownJewelCategoryTeams> getCrownJewelCategoryTeams() {
		return crownJewelCategoryTeams;
	}
	public void setCrownJewelCategoryTeams(List<CrownJewelCategoryTeams> crownJewelCategoryTeams) {
		this.crownJewelCategoryTeams = crownJewelCategoryTeams;
	}
	public FantasyParentCategory getFantasyParentCategory() {
		return fantasyParentCategory;
	}
	public void setFantasyParentCategory(FantasyParentCategory fantasyParentCategory) {
		this.fantasyParentCategory = fantasyParentCategory;
	}
	public FantasyChildCategory getFantasyChildCategory() {
		return fantasyChildCategory;
	}
	public void setFantasyChildCategory(FantasyChildCategory fantasyChildCategory) {
		this.fantasyChildCategory = fantasyChildCategory;
	}
	public FantasyGrandChildCategory getFantasyGrandChildCategory() {
		return fantasyGrandChildCategory;
	}
	public void setFantasyGrandChildCategory(
			FantasyGrandChildCategory fantasyGrandChildCategory) {
		this.fantasyGrandChildCategory = fantasyGrandChildCategory;
	}
	
}
