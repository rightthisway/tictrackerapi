package com.rtw.tracker.pojos;

import com.rtw.tmat.utils.Error;

public class PONotesDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private String poId;
	private String internalNote;
	private String externalNote;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}	
	public String getPoId() {
		return poId;
	}
	public void setPoId(String poId) {
		this.poId = poId;
	}
	public String getInternalNote() {
		return internalNote;
	}
	public void setInternalNote(String internalNote) {
		this.internalNote = internalNote;
	}
	public String getExternalNote() {
		return externalNote;
	}
	public void setExternalNote(String externalNote) {
		this.externalNote = externalNote;
	}
	
}
