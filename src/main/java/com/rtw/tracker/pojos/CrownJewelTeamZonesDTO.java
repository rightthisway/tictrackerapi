package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.CrownJewelLeagues;
import com.rtw.tracker.datas.CrownJewelTeamZones;
import com.rtw.tracker.datas.CrownJewelTeams;
import com.rtw.tracker.datas.FantasyChildCategory;
import com.rtw.tracker.datas.FantasyGrandChildCategory;
import com.rtw.tracker.datas.FantasyParentCategory;
import com.rtw.tracker.enums.ZonesQuantityTypes;

public class CrownJewelTeamZonesDTO {

	private Integer status;
	private Error error; 
	private String message = new String();
	private String childId;
	private String grandChildId;
	private List<FantasyChildCategory> childCategories;
	private List<FantasyGrandChildCategory> grandChildCategories;
	private List<CrownJewelLeagues> leaguesList;
	private List<CrownJewelTeams> teamsList;
	private List<CrownJewelTeamZones> cjTeamZone;
	private String copyFromLeagueId;
	private String leagueId;
	private String teamId;
	private String copyFromLeagueIdStr;
	private String savedZoneStr;
	private ZonesQuantityTypes[] quantityTypes;
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getChildId() {
		return childId;
	}
	public void setChildId(String childId) {
		this.childId = childId;
	}
	public String getGrandChildId() {
		return grandChildId;
	}
	public void setGrandChildId(String grandChildId) {
		this.grandChildId = grandChildId;
	}
	public List<FantasyChildCategory> getChildCategories() {
		return childCategories;
	}
	public void setChildCategories(List<FantasyChildCategory> childCategories) {
		this.childCategories = childCategories;
	}
	public List<FantasyGrandChildCategory> getGrandChildCategories() {
		return grandChildCategories;
	}
	public void setGrandChildCategories(List<FantasyGrandChildCategory> grandChildCategories) {
		this.grandChildCategories = grandChildCategories;
	}
	public List<CrownJewelLeagues> getLeaguesList() {
		return leaguesList;
	}
	public void setLeaguesList(List<CrownJewelLeagues> leaguesList) {
		this.leaguesList = leaguesList;
	}
	public List<CrownJewelTeams> getTeamsList() {
		return teamsList;
	}
	public void setTeamsList(List<CrownJewelTeams> teamsList) {
		this.teamsList = teamsList;
	}
	public List<CrownJewelTeamZones> getCjTeamZone() {
		return cjTeamZone;
	}
	public void setCjTeamZone(List<CrownJewelTeamZones> cjTeamZone) {
		this.cjTeamZone = cjTeamZone;
	}
	public String getCopyFromLeagueId() {
		return copyFromLeagueId;
	}
	public void setCopyFromLeagueId(String copyFromLeagueId) {
		this.copyFromLeagueId = copyFromLeagueId;
	}
	public String getLeagueId() {
		return leagueId;
	}
	public void setLeagueId(String leagueId) {
		this.leagueId = leagueId;
	}
	public String getTeamId() {
		return teamId;
	}
	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}
	public String getCopyFromLeagueIdStr() {
		return copyFromLeagueIdStr;
	}
	public void setCopyFromLeagueIdStr(String copyFromLeagueIdStr) {
		this.copyFromLeagueIdStr = copyFromLeagueIdStr;
	}
	public String getSavedZoneStr() {
		return savedZoneStr;
	}
	public void setSavedZoneStr(String savedZoneStr) {
		this.savedZoneStr = savedZoneStr;
	}
	public ZonesQuantityTypes[] getQuantityTypes() {
		return quantityTypes;
	}
	public void setQuantityTypes(ZonesQuantityTypes[] quantityTypes) {
		this.quantityTypes = quantityTypes;
	}
		
}
