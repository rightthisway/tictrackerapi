package com.rtw.tracker.pojos;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.rtw.tmat.utils.Error;

@XStreamAlias("QuizJoinContestInfo")
public class QuizJoinContestInfo {
	
	private Integer status;
	private Error error; 
	private Integer totalUsersCount=0;
	private String message;
	
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getTotalUsersCount() {
		return totalUsersCount;
	}
	public void setTotalUsersCount(Integer totalUsersCount) {
		this.totalUsersCount = totalUsersCount;
	}
	
	
}
