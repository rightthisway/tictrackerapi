package com.rtw.tracker.pojos;

import com.rtw.tracker.enums.InvoiceStatus;

public class InvoiceDTO {

	private Integer invoiceId;
	private String invoiceInternalNote;
	private String invoiceTrackingNo;
	private InvoiceStatus invoiceStatus;
	private String invoiceRealTixMap;
	private String expDeliveryDate;
		
	public Integer getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}
	public String getInvoiceInternalNote() {
		return invoiceInternalNote;
	}
	public void setInvoiceInternalNote(String invoiceInternalNote) {
		this.invoiceInternalNote = invoiceInternalNote;
	}
	public String getInvoiceTrackingNo() {
		return invoiceTrackingNo;
	}
	public void setInvoiceTrackingNo(String invoiceTrackingNo) {
		this.invoiceTrackingNo = invoiceTrackingNo;
	}
	public InvoiceStatus getInvoiceStatus() {
		return invoiceStatus;
	}
	public void setInvoiceStatus(InvoiceStatus invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}
	public String getInvoiceRealTixMap() {
		return invoiceRealTixMap;
	}
	public void setInvoiceRealTixMap(String invoiceRealTixMap) {
		this.invoiceRealTixMap = invoiceRealTixMap;
	}
	public String getExpDeliveryDate() {
		return expDeliveryDate;
	}
	public void setExpDeliveryDate(String expDeliveryDate) {
		this.expDeliveryDate = expDeliveryDate;
	}
	
}
