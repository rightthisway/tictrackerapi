package com.rtw.tracker.pojos;

public class ReportPojo {
	
	String name;
	String viewName;
	Integer viewCount;
	Integer viewCount1;
	Integer viewCount2;
	Double viewCountDouble;
	Double viewCountDouble1;
	String viewName2;
	public String getViewName2() {
		return viewName2;
	}
	public void setViewName2(String viewName2) {
		this.viewName2 = viewName2;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getViewName() {
		return viewName;
	}
	public void setViewName(String viewName) {
		this.viewName = viewName;
	}
	public Integer getViewCount() {
		return viewCount;
	}
	public void setViewCount(Integer viewCount) {
		this.viewCount = viewCount;
	}
	public Integer getViewCount1() {
		return viewCount1;
	}
	public void setViewCount1(Integer viewCount1) {
		this.viewCount1 = viewCount1;
	}

	public Integer getViewCount2() {
		return viewCount2;
	}
	public void setViewCount2(Integer viewCount2) {
		this.viewCount2 = viewCount2;
	}
	public Double getViewCountDouble() {
		return viewCountDouble;
	}
	public void setViewCountDouble(Double viewCountDouble) {
		this.viewCountDouble = viewCountDouble;
	}
	public Double getViewCountDouble1() {
		return viewCountDouble1;
	}
	public void setViewCountDouble1(Double viewCountDouble1) {
		this.viewCountDouble1 = viewCountDouble1;
	}
	
	

}
