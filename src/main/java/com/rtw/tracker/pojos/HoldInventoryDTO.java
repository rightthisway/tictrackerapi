package com.rtw.tracker.pojos;

import java.util.List;
import java.util.Set;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.InventoryHold;

public class HoldInventoryDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private List<InventoryHold> inventoryHoldList;
	private Set<Integer> inventoryHoldQty;
	private PaginationDTO paginationDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<InventoryHold> getInventoryHoldList() {
		return inventoryHoldList;
	}
	public void setInventoryHoldList(List<InventoryHold> inventoryHoldList) {
		this.inventoryHoldList = inventoryHoldList;
	}
	public Set<Integer> getInventoryHoldQty() {
		return inventoryHoldQty;
	}
	public void setInventoryHoldQty(Set<Integer> inventoryHoldQty) {
		this.inventoryHoldQty = inventoryHoldQty;
	}
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}
	
	
}
