package com.rtw.tracker.pojos;

public class InvoiceRefundListDTO {

	private Integer invoiceRefundId;
	private String refundType;
	private Integer refundOrderId;
	private String refundTransactionId;
	private String refundId;
	private String refundAmount;
	private String refundStatus;
	private String refundReason;
	private String revertedUsedRewardPoints;
	private String revertedEarnedRewardPoints;
	private String refundCreatedTime;
	private String refundedBy;
	
	public Integer getInvoiceRefundId() {
		return invoiceRefundId;
	}
	public void setInvoiceRefundId(Integer invoiceRefundId) {
		this.invoiceRefundId = invoiceRefundId;
	}
	public String getRefundType() {
		return refundType;
	}
	public void setRefundType(String refundType) {
		this.refundType = refundType;
	}
	public Integer getRefundOrderId() {
		return refundOrderId;
	}
	public void setRefundOrderId(Integer refundOrderId) {
		this.refundOrderId = refundOrderId;
	}
	public String getRefundTransactionId() {
		return refundTransactionId;
	}
	public void setRefundTransactionId(String refundTransactionId) {
		this.refundTransactionId = refundTransactionId;
	}
	public String getRefundId() {
		return refundId;
	}
	public void setRefundId(String refundId) {
		this.refundId = refundId;
	}
	public String getRefundAmount() {
		return refundAmount;
	}
	public void setRefundAmount(String refundAmount) {
		this.refundAmount = refundAmount;
	}
	public String getRefundStatus() {
		return refundStatus;
	}
	public void setRefundStatus(String refundStatus) {
		this.refundStatus = refundStatus;
	}
	public String getRefundReason() {
		return refundReason;
	}
	public void setRefundReason(String refundReason) {
		this.refundReason = refundReason;
	}
	public String getRevertedUsedRewardPoints() {
		return revertedUsedRewardPoints;
	}
	public void setRevertedUsedRewardPoints(String revertedUsedRewardPoints) {
		this.revertedUsedRewardPoints = revertedUsedRewardPoints;
	}
	public String getRevertedEarnedRewardPoints() {
		return revertedEarnedRewardPoints;
	}
	public void setRevertedEarnedRewardPoints(String revertedEarnedRewardPoints) {
		this.revertedEarnedRewardPoints = revertedEarnedRewardPoints;
	}
	public String getRefundCreatedTime() {
		return refundCreatedTime;
	}
	public void setRefundCreatedTime(String refundCreatedTime) {
		this.refundCreatedTime = refundCreatedTime;
	}
	public String getRefundedBy() {
		return refundedBy;
	}
	public void setRefundedBy(String refundedBy) {
		this.refundedBy = refundedBy;
	}

}
