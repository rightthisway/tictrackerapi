package com.rtw.tracker.pojos;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;



@XStreamAlias("QuizContestSummary")
public class QuizContestSummaryInfo {
	
	private Integer status;
	private com.rtw.tmat.utils.Error error; 
	private String message;
	private Integer contestWinnersCount;
	List<QuizContestWinners> quizContestWinners;
	QuizContestWinners quizContestWinner;
	
	
	public com.rtw.tmat.utils.Error getError() {
		return error;
	}
	public void setError(com.rtw.tmat.utils.Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<QuizContestWinners> getQuizContestWinners() {
		return quizContestWinners;
	}
	public void setQuizContestWinners(List<QuizContestWinners> quizContestWinners) {
		this.quizContestWinners = quizContestWinners;
	}
	public Integer getContestWinnersCount() {
		return contestWinnersCount;
	}
	public void setContestWinnersCount(Integer contestWinnersCount) {
		this.contestWinnersCount = contestWinnersCount;
	}
	public QuizContestWinners getQuizContestWinner() {
		return quizContestWinner;
	}
	public void setQuizContestWinner(QuizContestWinners quizContestWinner) {
		this.quizContestWinner = quizContestWinner;
	}
	
	
	
	
}
