package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.FanClub;
import com.rtw.tracker.datas.FanClubEvent;
import com.rtw.tracker.datas.FanClubMember;
import com.rtw.tracker.datas.FanClubPost;
import com.rtw.tracker.datas.FanClubPostComments;
import com.rtw.tracker.datas.FanClubVideo;
import com.rtw.tracker.datas.FanclubAbuseReported;
import com.rtw.tracker.datas.PollingVideoCategory;

public class FanClubDTO {

	private List<FanClub> clubs;
	private List<FanClubMember> members;
	private List<FanClubPost> posts;
	private List<FanClubEvent> events;
	private List<FanClubVideo> videos;
	private List<PollingVideoCategory> categories;
	private List<FanclubAbuseReported> abuseData;
	private FanClubVideo video;
	private FanClub club;
	private FanClubPost post;
	private FanClubEvent event;
	private Error error = new Error(); 
	private String message = new String();
	private Integer status;
	private PaginationDTO pagination;
	private PaginationDTO dummyPagination;
	
	private List<FanClubPostComments> postComments;
	
	
	public List<FanClub> getClubs() {
		return clubs;
	}
	public void setClubs(List<FanClub> clubs) {
		this.clubs = clubs;
	}
	public List<FanClubMember> getMembers() {
		return members;
	}
	public void setMembers(List<FanClubMember> members) {
		this.members = members;
	}
	public List<FanClubPost> getPosts() {
		return posts;
	}
	public void setPosts(List<FanClubPost> posts) {
		this.posts = posts;
	}
	public List<FanClubEvent> getEvents() {
		return events;
	}
	public void setEvents(List<FanClubEvent> events) {
		this.events = events;
	}
	public FanClub getClub() {
		return club;
	}
	public void setClub(FanClub club) {
		this.club = club;
	}
	public FanClubPost getPost() {
		return post;
	}
	public void setPost(FanClubPost post) {
		this.post = post;
	}
	public FanClubEvent getEvent() {
		return event;
	}
	public void setEvent(FanClubEvent event) {
		this.event = event;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public PaginationDTO getPagination() {
		return pagination;
	}
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}
	public List<FanClubVideo> getVideos() {
		return videos;
	}
	public void setVideos(List<FanClubVideo> videos) {
		this.videos = videos;
	}
	public FanClubVideo getVideo() {
		return video;
	}
	public void setVideo(FanClubVideo video) {
		this.video = video;
	}
	public PaginationDTO getDummyPagination() {
		return dummyPagination;
	}
	public void setDummyPagination(PaginationDTO dummyPagination) {
		this.dummyPagination = dummyPagination;
	}
	public List<PollingVideoCategory> getCategories() {
		return categories;
	}
	public void setCategories(List<PollingVideoCategory> categories) {
		this.categories = categories;
	}
	public List<FanClubPostComments> getPostComments() {
		return postComments;
	}
	public void setPostComments(List<FanClubPostComments> postComments) {
		this.postComments = postComments;
	}
	public List<FanclubAbuseReported> getAbuseData() {
		return abuseData;
	}
	public void setAbuseData(List<FanclubAbuseReported> abuseData) {
		this.abuseData = abuseData;
	}
	
}
