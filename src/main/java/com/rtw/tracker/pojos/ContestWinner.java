package com.rtw.tracker.pojos;

import java.util.Date;

import com.rtw.tmat.utils.Util;

public class ContestWinner {
	
	private Integer id;
	private Integer customerId;
	private String custName;
	private String lastName;
	private String userId;
	private String email;
	private String phone;
	private Double rewardPoints;
	
	private Integer rewardTickets;
	private Integer orderId;
	private String status;
	private String jackpotType;
	private String winnerType;
	private String notes;
	
	
	private Date expiryDate;
	private Date createdDate;
	private String expiryDateStr;
	private String createdDateStr;
	
	
	
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Double getRewardPoints() {
		return rewardPoints;
	}
	public void setRewardPoints(Double rewardPoints) {
		this.rewardPoints = rewardPoints;
	}
	public Integer getRewardTickets() {
		return rewardTickets;
	}
	public void setRewardTickets(Integer rewardTickets) {
		this.rewardTickets = rewardTickets;
	}
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getJackpotType() {
		return jackpotType;
	}
	public void setJackpotType(String jackpotType) {
		this.jackpotType = jackpotType;
	}
	public String getWinnerType() {
		return winnerType;
	}
	public void setWinnerType(String winnerType) {
		this.winnerType = winnerType;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getExpiryDateStr() {
		if(expiryDate!=null){
			expiryDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(expiryDate);
		}
		return expiryDateStr;
	}
	public void setExpiryDateStr(String expiryDateStr) {
		this.expiryDateStr = expiryDateStr;
	}
	public String getCreatedDateStr() {
		if(createdDate!=null){
			createdDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(createdDate);
		}
		return createdDateStr;
	}
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	
	
}
