package com.rtw.tracker.pojos;

import java.util.List;
import com.rtw.tmat.utils.Error;

public class RelatedPOInvoicesDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private List<RelatedPODTO> relatedPODTO;
	private List<RelatedInvoiceDTO> relatedInvoiceDTO;
	private PaginationDTO poPaginationDTO;
	private PaginationDTO invoicePaginationDTO; 
	private String selectedProductType;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<RelatedPODTO> getRelatedPODTO() {
		return relatedPODTO;
	}
	public void setRelatedPODTO(List<RelatedPODTO> relatedPODTO) {
		this.relatedPODTO = relatedPODTO;
	}
	public List<RelatedInvoiceDTO> getRelatedInvoiceDTO() {
		return relatedInvoiceDTO;
	}
	public void setRelatedInvoiceDTO(List<RelatedInvoiceDTO> relatedInvoiceDTO) {
		this.relatedInvoiceDTO = relatedInvoiceDTO;
	}
	public PaginationDTO getPoPaginationDTO() {
		return poPaginationDTO;
	}
	public void setPoPaginationDTO(PaginationDTO poPaginationDTO) {
		this.poPaginationDTO = poPaginationDTO;
	}
	public PaginationDTO getInvoicePaginationDTO() {
		return invoicePaginationDTO;
	}
	public void setInvoicePaginationDTO(PaginationDTO invoicePaginationDTO) {
		this.invoicePaginationDTO = invoicePaginationDTO;
	}
	public String getSelectedProductType() {
		return selectedProductType;
	}
	public void setSelectedProductType(String selectedProductType) {
		this.selectedProductType = selectedProductType;
	}
		
}
