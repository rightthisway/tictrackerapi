package com.rtw.tracker.pojos;

import com.rtw.tracker.enums.FileType;

public class InvoiceTicketAttachmentDTO {

	private Integer ticketAttachmentInvoiceId;	
	private FileType ticketAttachmentType;
	private Integer ticketAttachmentPosition;
	private String ticketAttachmentFileName;
	private Integer qrcodeAttachmentInvoiceId;
	private FileType qrcodeAttachmentType;
	private Integer qrcodeAttachmentPosition;
	private String qrcodeAttachmentFileName;
	private Integer barcodeAttachmentInvoiceId;
	private FileType barcodeAttachmentType;
	private Integer barcodeAttachmentPosition;
	private String barcodeAttachmentFileName;
	
	public Integer getTicketAttachmentInvoiceId() {
		return ticketAttachmentInvoiceId;
	}
	public void setTicketAttachmentInvoiceId(Integer ticketAttachmentInvoiceId) {
		this.ticketAttachmentInvoiceId = ticketAttachmentInvoiceId;
	}
	public FileType getTicketAttachmentType() {
		return ticketAttachmentType;
	}
	public void setTicketAttachmentType(FileType ticketAttachmentType) {
		this.ticketAttachmentType = ticketAttachmentType;
	}
	public Integer getTicketAttachmentPosition() {
		return ticketAttachmentPosition;
	}
	public void setTicketAttachmentPosition(Integer ticketAttachmentPosition) {
		this.ticketAttachmentPosition = ticketAttachmentPosition;
	}
	public String getTicketAttachmentFileName() {
		return ticketAttachmentFileName;
	}
	public void setTicketAttachmentFileName(String ticketAttachmentFileName) {
		this.ticketAttachmentFileName = ticketAttachmentFileName;
	}
	public Integer getQrcodeAttachmentInvoiceId() {
		return qrcodeAttachmentInvoiceId;
	}
	public void setQrcodeAttachmentInvoiceId(Integer qrcodeAttachmentInvoiceId) {
		this.qrcodeAttachmentInvoiceId = qrcodeAttachmentInvoiceId;
	}
	public FileType getQrcodeAttachmentType() {
		return qrcodeAttachmentType;
	}
	public void setQrcodeAttachmentType(FileType qrcodeAttachmentType) {
		this.qrcodeAttachmentType = qrcodeAttachmentType;
	}
	public Integer getQrcodeAttachmentPosition() {
		return qrcodeAttachmentPosition;
	}
	public void setQrcodeAttachmentPosition(Integer qrcodeAttachmentPosition) {
		this.qrcodeAttachmentPosition = qrcodeAttachmentPosition;
	}
	public String getQrcodeAttachmentFileName() {
		return qrcodeAttachmentFileName;
	}
	public void setQrcodeAttachmentFileName(String qrcodeAttachmentFileName) {
		this.qrcodeAttachmentFileName = qrcodeAttachmentFileName;
	}
	public Integer getBarcodeAttachmentInvoiceId() {
		return barcodeAttachmentInvoiceId;
	}
	public void setBarcodeAttachmentInvoiceId(Integer barcodeAttachmentInvoiceId) {
		this.barcodeAttachmentInvoiceId = barcodeAttachmentInvoiceId;
	}
	public FileType getBarcodeAttachmentType() {
		return barcodeAttachmentType;
	}
	public void setBarcodeAttachmentType(FileType barcodeAttachmentType) {
		this.barcodeAttachmentType = barcodeAttachmentType;
	}
	public Integer getBarcodeAttachmentPosition() {
		return barcodeAttachmentPosition;
	}
	public void setBarcodeAttachmentPosition(Integer barcodeAttachmentPosition) {
		this.barcodeAttachmentPosition = barcodeAttachmentPosition;
	}
	public String getBarcodeAttachmentFileName() {
		return barcodeAttachmentFileName;
	}
	public void setBarcodeAttachmentFileName(String barcodeAttachmentFileName) {
		this.barcodeAttachmentFileName = barcodeAttachmentFileName;
	}

}
