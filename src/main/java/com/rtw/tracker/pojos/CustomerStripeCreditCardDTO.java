package com.rtw.tracker.pojos;

import java.util.Date;

public class CustomerStripeCreditCardDTO {

	private Integer id;
	private String stripeCustomerId;
	private String stripeCardId;
	private String cardType;
	private String cardLastFourDigit;
	private Integer expiryMonth;
	private Integer expiryYear;
	private String status;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getStripeCustomerId() {
		return stripeCustomerId;
	}
	public void setStripeCustomerId(String stripeCustomerId) {
		this.stripeCustomerId = stripeCustomerId;
	}
	public String getStripeCardId() {
		return stripeCardId;
	}
	public void setStripeCardId(String stripeCardId) {
		this.stripeCardId = stripeCardId;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getCardLastFourDigit() {
		return cardLastFourDigit;
	}
	public void setCardLastFourDigit(String cardLastFourDigit) {
		this.cardLastFourDigit = cardLastFourDigit;
	}
	public Integer getExpiryMonth() {
		return expiryMonth;
	}
	public void setExpiryMonth(Integer expiryMonth) {
		this.expiryMonth = expiryMonth;
	}
	public Integer getExpiryYear() {
		return expiryYear;
	}
	public void setExpiryYear(Integer expiryYear) {
		this.expiryYear = expiryYear;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}	
	
}
