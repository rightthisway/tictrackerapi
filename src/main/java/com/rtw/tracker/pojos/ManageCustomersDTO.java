package com.rtw.tracker.pojos;

import java.util.List;
import com.rtw.tmat.utils.Error;

public class ManageCustomersDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private PaginationDTO paginationDTO;
	private List<CustomersCustomDTO> customersDTO;
	private List<CountryDTO> countryDTO;
	private String productType;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}	
	public List<CustomersCustomDTO> getCustomersDTO() {
		return customersDTO;
	}
	public void setCustomersDTO(List<CustomersCustomDTO> customersDTO) {
		this.customersDTO = customersDTO;
	}
	public List<CountryDTO> getCountryDTO() {
		return countryDTO;
	}
	public void setCountryDTO(List<CountryDTO> countryDTO) {
		this.countryDTO = countryDTO;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}	
	
}
