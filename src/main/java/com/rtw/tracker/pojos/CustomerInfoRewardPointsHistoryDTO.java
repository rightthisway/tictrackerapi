package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class CustomerInfoRewardPointsHistoryDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private PaginationDTO rewardPointsPaginationDTO;
	private List<CustomerRewardPointsDTO> customerRewardPointsDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public PaginationDTO getRewardPointsPaginationDTO() {
		return rewardPointsPaginationDTO;
	}
	public void setRewardPointsPaginationDTO(PaginationDTO rewardPointsPaginationDTO) {
		this.rewardPointsPaginationDTO = rewardPointsPaginationDTO;
	}
	public List<CustomerRewardPointsDTO> getCustomerRewardPointsDTO() {
		return customerRewardPointsDTO;
	}
	public void setCustomerRewardPointsDTO(
			List<CustomerRewardPointsDTO> customerRewardPointsDTO) {
		this.customerRewardPointsDTO = customerRewardPointsDTO;
	}
	
}
