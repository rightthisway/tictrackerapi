package com.rtw.tracker.pojos;

import java.util.Collection;
import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.Product;

public class PopularArtistScreenDTO {

	private Integer status;
	private Error error; 
	private String message = new String();
	private PaginationDTO paginationDTO;
	private PaginationDTO popularArtistPaginationDTO;
	private List<PopularArtistCustomDTO> artistList;
	private List<PopularArtistCustomDTO> popularArtistList;
	private Product product;
	private String productName;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}
	public List<PopularArtistCustomDTO> getArtistList() {
		return artistList;
	}
	public void setArtistList(List<PopularArtistCustomDTO> artistList) {
		this.artistList = artistList;
	}
	public List<PopularArtistCustomDTO> getPopularArtistList() {
		return popularArtistList;
	}
	public void setPopularArtistList(List<PopularArtistCustomDTO> popularArtistList) {
		this.popularArtistList = popularArtistList;
	}
	public PaginationDTO getPopularArtistPaginationDTO() {
		return popularArtistPaginationDTO;
	}
	public void setPopularArtistPaginationDTO(PaginationDTO popularArtistPaginationDTO) {
		this.popularArtistPaginationDTO = popularArtistPaginationDTO;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
}
