package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.PreContestChecklistTracking;

public class ContestChecklistTrackingDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private List<PreContestChecklistTracking> trackings;
	private PaginationDTO trackingPagination;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<PreContestChecklistTracking> getTrackings() {
		return trackings;
	}
	public void setTrackings(List<PreContestChecklistTracking> trackings) {
		this.trackings = trackings;
	}
	public PaginationDTO getTrackingPagination() {
		return trackingPagination;
	}
	public void setTrackingPagination(PaginationDTO trackingPagination) {
		this.trackingPagination = trackingPagination;
	}
	
}
