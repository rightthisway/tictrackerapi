package com.rtw.tracker.pojos;

public class CustomerFavouriteEventDTO {

	private Integer eventId;
	private String eventName;
	private String eventDateStr;
	private String eventTimeStr;
	private String dayOfWeek;
	private String building;
	private Integer venueId;
	private Integer noOfTixCount;
	private Integer noOfTixSoldCount;
	private String city;
	private String state;
	private String country;
	private String grandChildCategoryName;
	private String childCategoryName;
	private String parentCategoryName;
	private String eventCreationStr;
	private String eventUpdatedStr;
	private String notes;
	
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getEventDateStr() {
		return eventDateStr;
	}
	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}
	public String getEventTimeStr() {
		return eventTimeStr;
	}
	public void setEventTimeStr(String eventTimeStr) {
		this.eventTimeStr = eventTimeStr;
	}
	public String getDayOfWeek() {
		return dayOfWeek;
	}
	public void setDayOfWeek(String dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}
	public String getBuilding() {
		return building;
	}
	public void setBuilding(String building) {
		this.building = building;
	}
	public Integer getVenueId() {
		return venueId;
	}
	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}
	public Integer getNoOfTixCount() {
		return noOfTixCount;
	}
	public void setNoOfTixCount(Integer noOfTixCount) {
		this.noOfTixCount = noOfTixCount;
	}
	public Integer getNoOfTixSoldCount() {
		return noOfTixSoldCount;
	}
	public void setNoOfTixSoldCount(Integer noOfTixSoldCount) {
		this.noOfTixSoldCount = noOfTixSoldCount;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getGrandChildCategoryName() {
		return grandChildCategoryName;
	}
	public void setGrandChildCategoryName(String grandChildCategoryName) {
		this.grandChildCategoryName = grandChildCategoryName;
	}
	public String getChildCategoryName() {
		return childCategoryName;
	}
	public void setChildCategoryName(String childCategoryName) {
		this.childCategoryName = childCategoryName;
	}
	public String getParentCategoryName() {
		return parentCategoryName;
	}
	public void setParentCategoryName(String parentCategoryName) {
		this.parentCategoryName = parentCategoryName;
	}
	public String getEventCreationStr() {
		return eventCreationStr;
	}
	public void setEventCreationStr(String eventCreationStr) {
		this.eventCreationStr = eventCreationStr;
	}
	public String getEventUpdatedStr() {
		return eventUpdatedStr;
	}
	public void setEventUpdatedStr(String eventUpdatedStr) {
		this.eventUpdatedStr = eventUpdatedStr;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
}
