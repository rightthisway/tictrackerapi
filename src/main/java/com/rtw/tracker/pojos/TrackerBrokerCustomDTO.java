package com.rtw.tracker.pojos;

public class TrackerBrokerCustomDTO {

	private Integer brokerId;
	private String brokerCompany;
	private Double brokerServiceFees;
	
	public Integer getBrokerId() {
		return brokerId;
	}
	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}
	public String getBrokerCompany() {
		return brokerCompany;
	}
	public void setBrokerCompany(String brokerCompany) {
		this.brokerCompany = brokerCompany;
	}
	public Double getBrokerServiceFees() {
		return brokerServiceFees;
	}
	public void setBrokerServiceFees(Double brokerServiceFees) {
		this.brokerServiceFees = brokerServiceFees;
	}
		
}
