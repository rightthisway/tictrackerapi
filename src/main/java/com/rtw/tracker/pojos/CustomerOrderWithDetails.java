package com.rtw.tracker.pojos;

import java.util.ArrayList;
import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.CustomerOrder;
import com.rtw.tracker.datas.CustomerOrderDetails;
import com.rtw.tracker.datas.CustomerTicketDownloads;
import com.rtw.tracker.datas.InvoiceAudit;

public class CustomerOrderWithDetails {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private CustomerOrderDetails orderDetails;
	private CustomerOrder customerOrder;
	List<CustomerTicketDownloads> customerTicketDownloads;
	List<InvoiceAudit> auditList;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public CustomerOrderDetails getOrderDetails() {
		return orderDetails;
	}
	public void setOrderDetails(CustomerOrderDetails orderDetails) {
		this.orderDetails = orderDetails;
	}
	public CustomerOrder getCustomerOrder() {
		return customerOrder;
	}
	public void setCustomerOrder(CustomerOrder customerOrder) {
		this.customerOrder = customerOrder;
	}
	public List<CustomerTicketDownloads> getCustomerTicketDownloads() {
		return customerTicketDownloads;
	}
	public void setCustomerTicketDownloads(List<CustomerTicketDownloads> customerTicketDownloads) {
		this.customerTicketDownloads = customerTicketDownloads;
	}
	public List<InvoiceAudit> getAuditList() {
		return auditList;
	}
	public void setAuditList(List<InvoiceAudit> auditList) {
		this.auditList = auditList;
	}
	
}
