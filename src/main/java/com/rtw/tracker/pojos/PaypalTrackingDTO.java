package com.rtw.tracker.pojos;

import java.util.Collection;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.PayPalTracking;

public class PaypalTrackingDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	Collection<PayPalTracking> trackingList;
	private PaginationDTO paginationDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Collection<PayPalTracking> getTrackingList() {
		return trackingList;
	}
	public void setTrackingList(Collection<PayPalTracking> trackingList) {
		this.trackingList = trackingList;
	}
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}
	
}
