package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class GetEventsByVenueArtistDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private List<EventDetailsDTO> eventDetails;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<EventDetailsDTO> getEventDetails() {
		return eventDetails;
	}
	public void setEventDetails(List<EventDetailsDTO> eventDetails) {
		this.eventDetails = eventDetails;
	}	
		
}
