package com.rtw.tracker.pojos;

import java.util.List;
import com.rtw.tmat.utils.Error;

public class InvoicePaymentDetailsDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private PaginationDTO invoicePaginationDTO;
	private List<PaymentHistoryDTO> invoiceList;
	private List<PaymentHistoryDTO> rtwInvoiceList;
	private String selectedProductType;
		
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public PaginationDTO getInvoicePaginationDTO() {
		return invoicePaginationDTO;
	}
	public void setInvoicePaginationDTO(PaginationDTO invoicePaginationDTO) {
		this.invoicePaginationDTO = invoicePaginationDTO;
	}
	public List<PaymentHistoryDTO> getInvoiceList() {
		return invoiceList;
	}
	public void setInvoiceList(List<PaymentHistoryDTO> invoiceList) {
		this.invoiceList = invoiceList;
	}
	public List<PaymentHistoryDTO> getRtwInvoiceList() {
		return rtwInvoiceList;
	}
	public void setRtwInvoiceList(List<PaymentHistoryDTO> rtwInvoiceList) {
		this.rtwInvoiceList = rtwInvoiceList;
	}
	public String getSelectedProductType() {
		return selectedProductType;
	}
	public void setSelectedProductType(String selectedProductType) {
		this.selectedProductType = selectedProductType;
	}	
		
}
