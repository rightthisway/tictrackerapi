package com.rtw.tracker.pojos;

public class FantasyEventTeamTicketDTO {

	private Integer categoryId;
	private Integer teamId;
	private String teamName;
	private Integer eventId;
	private Integer teamZoneId;
	private String zone;
	private Integer quantity;
	private Double requiredPoints;
	private Integer ticketId;
	private Boolean isRealTicket;
	private Integer tmatEventId;
	private String eventDateStr;
	private String eventTimeStr;
	private String venueName;
	private String city;
	private String state;
	private String country;
	
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	public Integer getTeamId() {
		return teamId;
	}
	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}
	public String getTeamName() {
		return teamName;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public Integer getTeamZoneId() {
		return teamZoneId;
	}
	public void setTeamZoneId(Integer teamZoneId) {
		this.teamZoneId = teamZoneId;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Double getRequiredPoints() {
		return requiredPoints;
	}
	public void setRequiredPoints(Double requiredPoints) {
		this.requiredPoints = requiredPoints;
	}
	public Integer getTicketId() {
		return ticketId;
	}
	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}
	public Boolean getIsRealTicket() {
		return isRealTicket;
	}
	public void setIsRealTicket(Boolean isRealTicket) {
		this.isRealTicket = isRealTicket;
	}
	public Integer getTmatEventId() {
		return tmatEventId;
	}
	public void setTmatEventId(Integer tmatEventId) {
		this.tmatEventId = tmatEventId;
	}
	public String getEventDateStr() {
		return eventDateStr;
	}
	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}
	public String getEventTimeStr() {
		return eventTimeStr;
	}
	public void setEventTimeStr(String eventTimeStr) {
		this.eventTimeStr = eventTimeStr;
	}
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
}
