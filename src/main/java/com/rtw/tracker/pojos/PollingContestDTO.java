package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.PollingContCategoryMapper;
import com.rtw.tracker.datas.PollingContest;

public class PollingContestDTO {

	private List<PollingContest> pollingContest;
	
	private List<PollingContCategoryMapper>  pollingContCategoriesList;

	private PaginationDTO pagination;
	private Error error = new Error();
	private String message = new String();
	private Integer status;
	private PollingContest pollContest;
	
	public PollingContest getPollContest() {
		return pollContest;
	}

	public void setPollContest(PollingContest pollContest) {
		this.pollContest = pollContest;
	}

	public List<PollingContest> getPollingContest() {
		return pollingContest;
	}

	public void setPollingContest(List<PollingContest> pollingContest) {
		this.pollingContest = pollingContest;
	}

	public PaginationDTO getPagination() {
		return pagination;
	}

	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<PollingContCategoryMapper> getPollingContCategoriesList() {
		return pollingContCategoriesList;
	}

	public void setPollingContCategoriesList(List<PollingContCategoryMapper> pollingContCategoriesList) {
		this.pollingContCategoriesList = pollingContCategoriesList;
	}

}
