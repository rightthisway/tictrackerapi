package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class ValidateReferralCodeDTO {

	private Integer status;
	private Error error; 
	private String message;
	private Double rewardPoints;
	private Integer promoTrackingId;
	private Integer affPromoTrackingId;
	private List<String> longSalePriceList;
	private String price;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public com.rtw.tmat.utils.Error getError() {
		return error;
	}
	public void setError(com.rtw.tmat.utils.Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Double getRewardPoints() {
		return rewardPoints;
	}
	public void setRewardPoints(Double rewardPoints) {
		this.rewardPoints = rewardPoints;
	}
	public Integer getPromoTrackingId() {
		return promoTrackingId;
	}
	public void setPromoTrackingId(Integer promoTrackingId) {
		this.promoTrackingId = promoTrackingId;
	}
	public Integer getAffPromoTrackingId() {
		return affPromoTrackingId;
	}
	public void setAffPromoTrackingId(Integer affPromoTrackingId) {
		this.affPromoTrackingId = affPromoTrackingId;
	}
	public List<String> getLongSalePriceList() {
		return longSalePriceList;
	}
	public void setLongSalePriceList(List<String> longSalePriceList) {
		this.longSalePriceList = longSalePriceList;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	
}
