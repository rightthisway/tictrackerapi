package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.enums.ProfitLossSign;
import com.rtw.tmat.utils.Error;

public class PastOrderStatusDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private List<OpenOrdersDTO> pastOrders;
	private PaginationDTO pastOrdersPaginationDTO;
	private List<EventDetailsDTO> eventDetails;
	private ProfitLossSign[] profitLossSign;
	private String selectedProfitLossSign;
	private String productType;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<OpenOrdersDTO> getPastOrders() {
		return pastOrders;
	}
	public void setPastOrders(List<OpenOrdersDTO> pastOrders) {
		this.pastOrders = pastOrders;
	}
	public PaginationDTO getPastOrdersPaginationDTO() {
		return pastOrdersPaginationDTO;
	}
	public void setPastOrdersPaginationDTO(PaginationDTO pastOrdersPaginationDTO) {
		this.pastOrdersPaginationDTO = pastOrdersPaginationDTO;
	}
	public List<EventDetailsDTO> getEventDetails() {
		return eventDetails;
	}
	public void setEventDetails(List<EventDetailsDTO> eventDetails) {
		this.eventDetails = eventDetails;
	}	
	public ProfitLossSign[] getProfitLossSign() {
		return profitLossSign;
	}
	public void setProfitLossSign(ProfitLossSign[] profitLossSign) {
		this.profitLossSign = profitLossSign;
	}
	public String getSelectedProfitLossSign() {
		return selectedProfitLossSign;
	}
	public void setSelectedProfitLossSign(String selectedProfitLossSign) {
		this.selectedProfitLossSign = selectedProfitLossSign;
	}	
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
		
}
