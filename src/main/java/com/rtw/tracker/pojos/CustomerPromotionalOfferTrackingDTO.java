package com.rtw.tracker.pojos;

public class CustomerPromotionalOfferTrackingDTO {

	private Integer id;
	private Integer customerId;
	private String firstName;
	private String lastName;
	private String email;
	private String promoCode;
	private String discount;
	private Integer orderId;
	private String orderTotal;
	private Integer quantity;
	private String ipAddress;
	private String primaryPaymentMethod;
	private String secondaryPaymentMethod;
	private String thirdPaymentMethod;
	private String platform;
	private String status;
	private String createdDate;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPromoCode() {
		return promoCode;
	}
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}
	public String getDiscount() {
		return discount;
	}
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	public String getOrderTotal() {
		return orderTotal;
	}
	public void setOrderTotal(String orderTotal) {
		this.orderTotal = orderTotal;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getPrimaryPaymentMethod() {
		return primaryPaymentMethod;
	}
	public void setPrimaryPaymentMethod(String primaryPaymentMethod) {
		this.primaryPaymentMethod = primaryPaymentMethod;
	}
	public String getSecondaryPaymentMethod() {
		return secondaryPaymentMethod;
	}
	public void setSecondaryPaymentMethod(String secondaryPaymentMethod) {
		this.secondaryPaymentMethod = secondaryPaymentMethod;
	}
	public String getThirdPaymentMethod() {
		return thirdPaymentMethod;
	}
	public void setThirdPaymentMethod(String thirdPaymentMethod) {
		this.thirdPaymentMethod = thirdPaymentMethod;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	
}
