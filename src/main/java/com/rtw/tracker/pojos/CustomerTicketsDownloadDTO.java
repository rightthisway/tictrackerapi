package com.rtw.tracker.pojos;

public class CustomerTicketsDownloadDTO {

	private Integer id;
	private Integer invoiceId;
	private Integer customerId;
	private String ticketType;
	private String ticketName;
	private String email;
	private String ipAddress;
	private String downloadPlatform;
	private String downloadDateTime;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getTicketType() {
		return ticketType;
	}
	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}
	public String getTicketName() {
		return ticketName;
	}
	public void setTicketName(String ticketName) {
		this.ticketName = ticketName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getDownloadPlatform() {
		return downloadPlatform;
	}
	public void setDownloadPlatform(String downloadPlatform) {
		this.downloadPlatform = downloadPlatform;
	}
	public String getDownloadDateTime() {
		return downloadDateTime;
	}
	public void setDownloadDateTime(String downloadDateTime) {
		this.downloadDateTime = downloadDateTime;
	}
		
}
