package com.rtw.tracker.pojos;

import com.rtw.tmat.utils.Error;

public class AutoCompleteArtistAndCategoryDTO {

	private Integer status;
	private Error error; 
	private String message = new String();
	private String artistAndCategory;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getArtistAndCategory() {
		return artistAndCategory;
	}
	public void setArtistAndCategory(String artistAndCategory) {
		this.artistAndCategory = artistAndCategory;
	}
		
}
