package com.rtw.tracker.pojos;


import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.PollingSponsor;
import com.rtw.tracker.datas.RtfGiftCard;
import com.rtw.tracker.datas.RtfGiftCardQuantity;


public class PollingSponsorDTO {

	private List<PollingSponsor> pollingSponsor;

	private PaginationDTO pagination;
	private Error error = new Error(); 
	private String message = new String();
	private Integer status;
	private RtfGiftCard giftCard;
	private List<RtfGiftCardQuantity> qtyList;
	private PollingSponsor pollSponsor;
	
	public PollingSponsor getPollSponsor() {
		return pollSponsor;
	}
	public void setPollSponsor(PollingSponsor pollSponsor) {
		this.pollSponsor = pollSponsor;
	}
	public List<PollingSponsor> getPollingSponsor() {
		return pollingSponsor;
	}
	public void setPollingSponsor(List<PollingSponsor> pollingSponsor) {
		this.pollingSponsor = pollingSponsor;
	}
	
	public PaginationDTO getPagination() {
		return pagination;
	}
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	
}
