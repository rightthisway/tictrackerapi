package com.rtw.tracker.pojos;

import com.rtw.tmat.utils.Error;
import java.util.List;
import com.rtw.tracker.datas.ContestAffiliates;

public class ContestAffiliatesDTO {

	private Integer status;
	private Error error; 
	private String message;
	private ContestAffiliates contestAffiliates;
	private List<ContestAffiliates> contestAffiliatesList;
	private PaginationDTO paginationDTO;
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public ContestAffiliates getContestAffiliates() {
		return contestAffiliates;
	}
	public void setContestAffiliates(ContestAffiliates contestAffiliates) {
		this.contestAffiliates = contestAffiliates;
	}
	public List<ContestAffiliates> getContestAffiliatesList() {
		return contestAffiliatesList;
	}
	public void setContestAffiliatesList(
			List<ContestAffiliates> contestAffiliatesList) {
		this.contestAffiliatesList = contestAffiliatesList;
	}
	
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}
	
	
	
}
