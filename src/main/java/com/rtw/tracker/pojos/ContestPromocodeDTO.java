package com.rtw.tracker.pojos;

import java.util.List;
import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.ContestPromocode;
import com.rtw.tracker.datas.PromotionalEarnLifeOffer;

public class ContestPromocodeDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private List<ContestPromocode> contestPromocodes;
	private ContestPromocode contestPromocode;
	private PaginationDTO discountCodePaginationDTO;
	
	private PromotionalEarnLifeOffer promoOffer;
	private List<PromotionalEarnLifeOffer> promoOffers;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public List<ContestPromocode> getContestPromocodes() {
		return contestPromocodes;
	}
	public void setContestPromocodes(List<ContestPromocode> contestPromocodes) {
		this.contestPromocodes = contestPromocodes;
	}
	public PaginationDTO getDiscountCodePaginationDTO() {
		return discountCodePaginationDTO;
	}
	public void setDiscountCodePaginationDTO(PaginationDTO discountCodePaginationDTO) {
		this.discountCodePaginationDTO = discountCodePaginationDTO;
	}
	public ContestPromocode getContestPromocode() {
		return contestPromocode;
	}
	public void setContestPromocode(ContestPromocode contestPromocode) {
		this.contestPromocode = contestPromocode;
	}
	public PromotionalEarnLifeOffer getPromoOffer() {
		return promoOffer;
	}
	public void setPromoOffer(PromotionalEarnLifeOffer promoOffer) {
		this.promoOffer = promoOffer;
	}
	public List<PromotionalEarnLifeOffer> getPromoOffers() {
		return promoOffers;
	}
	public void setPromoOffers(List<PromotionalEarnLifeOffer> promoOffers) {
		this.promoOffers = promoOffers;
	}
	
	
	
		
}
