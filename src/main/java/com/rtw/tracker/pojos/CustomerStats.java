package com.rtw.tracker.pojos;

import java.util.List;
import java.util.Set;
 
public class CustomerStats {
	
	private Integer status;
	private com.rtw.tmat.utils.Error error; 
	private String message; 
	private List<CustomerStatistics> customerStatistics;
	private List<BotDetail> botList;
	private Set<String> referrlCodeList;
	
	public com.rtw.tmat.utils.Error getError() {
		return error;
	}
	public void setError(com.rtw.tmat.utils.Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<CustomerStatistics> getCustomerStatistics() {
		return customerStatistics;
	}
	public void setCustomerStatistics(List<CustomerStatistics> customerStatistics) {
		this.customerStatistics = customerStatistics;
	}
	public List<BotDetail> getBotList() {
		return botList;
	}
	public void setBotList(List<BotDetail> botList) {
		this.botList = botList;
	}
	public Set<String> getReferrlCodeList() {
		return referrlCodeList;
	}
	public void setReferrlCodeList(Set<String> referrlCodeList) {
		this.referrlCodeList = referrlCodeList;
	}
	
}
