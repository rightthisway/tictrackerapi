package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class AddCustomerDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private List<CountryDTO> countryDTO;
	private List<StateDTO> stateDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<CountryDTO> getCountryDTO() {
		return countryDTO;
	}
	public void setCountryDTO(List<CountryDTO> countryDTO) {
		this.countryDTO = countryDTO;
	}
	public List<StateDTO> getStateDTO() {
		return stateDTO;
	}
	public void setStateDTO(List<StateDTO> stateDTO) {
		this.stateDTO = stateDTO;
	}
	
}
