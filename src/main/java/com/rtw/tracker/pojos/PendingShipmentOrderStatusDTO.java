package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.enums.ProfitLossSign;
import com.rtw.tmat.utils.Error;

public class PendingShipmentOrderStatusDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private List<OpenOrdersDTO> pendingShipmentOrders;
	private PaginationDTO pendingShipmentOrdersPaginationDTO;
	private List<EventDetailsDTO> eventDetails;
	private ProfitLossSign[] profitLossSign;
	private String selectedProfitLossSign;
	private String productType;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}	
	public List<OpenOrdersDTO> getPendingShipmentOrders() {
		return pendingShipmentOrders;
	}
	public void setPendingShipmentOrders(List<OpenOrdersDTO> pendingShipmentOrders) {
		this.pendingShipmentOrders = pendingShipmentOrders;
	}
	public PaginationDTO getPendingShipmentOrdersPaginationDTO() {
		return pendingShipmentOrdersPaginationDTO;
	}
	public void setPendingShipmentOrdersPaginationDTO(
			PaginationDTO pendingShipmentOrdersPaginationDTO) {
		this.pendingShipmentOrdersPaginationDTO = pendingShipmentOrdersPaginationDTO;
	}
	public List<EventDetailsDTO> getEventDetails() {
		return eventDetails;
	}
	public void setEventDetails(List<EventDetailsDTO> eventDetails) {
		this.eventDetails = eventDetails;
	}	
	public ProfitLossSign[] getProfitLossSign() {
		return profitLossSign;
	}
	public void setProfitLossSign(ProfitLossSign[] profitLossSign) {
		this.profitLossSign = profitLossSign;
	}
	public String getSelectedProfitLossSign() {
		return selectedProfitLossSign;
	}
	public void setSelectedProfitLossSign(String selectedProfitLossSign) {
		this.selectedProfitLossSign = selectedProfitLossSign;
	}	
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
		
}
