package com.rtw.tracker.pojos;

import com.rtw.tmat.utils.Error;

public class CustomerPromotionalOrderSummaryDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private CustomerPromotionalOfferTrackingDetailsDTO cusPromoOfferTrackingDetailsDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public CustomerPromotionalOfferTrackingDetailsDTO getCusPromoOfferTrackingDetailsDTO() {
		return cusPromoOfferTrackingDetailsDTO;
	}
	public void setCusPromoOfferTrackingDetailsDTO(
			CustomerPromotionalOfferTrackingDetailsDTO cusPromoOfferTrackingDetailsDTO) {
		this.cusPromoOfferTrackingDetailsDTO = cusPromoOfferTrackingDetailsDTO;
	}	
		
}
