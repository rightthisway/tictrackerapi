package com.rtw.tracker.pojos;

import com.rtw.tmat.utils.Error;

public class FantasyCategoryTeamDTO {

	private Integer status;
	private Error error; 
	private String message = new String();
	private FanasyCatTeamDTO fanasyCatTeamDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public FanasyCatTeamDTO getFanasyCatTeamDTO() {
		return fanasyCatTeamDTO;
	}
	public void setFanasyCatTeamDTO(FanasyCatTeamDTO fanasyCatTeamDTO) {
		this.fanasyCatTeamDTO = fanasyCatTeamDTO;
	}
			
}
