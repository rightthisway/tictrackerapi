package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class InvoiceEditDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message;
	private InvoiceDTO invoiceDTO;
	private InvoiceDTO invoiceExpDeliveryDateDTO;
	private CustomerOrderDTO customerOrderDTO;
	private OpenOrdersDTO openOrdersDTO;
	private CustomerAddressDTO customerShippingAddressDTO;
	private List<TicketGroupDTO> ticketGroupDTO;
	private List<InvoiceTicketAttachmentDTO> eticketAttachmentDTO;
	private List<InvoiceTicketAttachmentDTO> qrcodeAttachmentDTO;
	private List<InvoiceTicketAttachmentDTO> barcodeAttachmentDTO;
	private Integer eticketCount;
	private Integer qrcodeCount;
	private Integer barcodeCount;
	private List<POShippingMethodDTO> shippingMethodDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public InvoiceDTO getInvoiceDTO() {
		return invoiceDTO;
	}
	public void setInvoiceDTO(InvoiceDTO invoiceDTO) {
		this.invoiceDTO = invoiceDTO;
	}
	public InvoiceDTO getInvoiceExpDeliveryDateDTO() {
		return invoiceExpDeliveryDateDTO;
	}
	public void setInvoiceExpDeliveryDateDTO(InvoiceDTO invoiceExpDeliveryDateDTO) {
		this.invoiceExpDeliveryDateDTO = invoiceExpDeliveryDateDTO;
	}
	public CustomerOrderDTO getCustomerOrderDTO() {
		return customerOrderDTO;
	}
	public void setCustomerOrderDTO(CustomerOrderDTO customerOrderDTO) {
		this.customerOrderDTO = customerOrderDTO;
	}
	public OpenOrdersDTO getOpenOrdersDTO() {
		return openOrdersDTO;
	}
	public void setOpenOrdersDTO(OpenOrdersDTO openOrdersDTO) {
		this.openOrdersDTO = openOrdersDTO;
	}
	public CustomerAddressDTO getCustomerShippingAddressDTO() {
		return customerShippingAddressDTO;
	}
	public void setCustomerShippingAddressDTO(
			CustomerAddressDTO customerShippingAddressDTO) {
		this.customerShippingAddressDTO = customerShippingAddressDTO;
	}
	public List<TicketGroupDTO> getTicketGroupDTO() {
		return ticketGroupDTO;
	}
	public void setTicketGroupDTO(List<TicketGroupDTO> ticketGroupDTO) {
		this.ticketGroupDTO = ticketGroupDTO;
	}
	public List<InvoiceTicketAttachmentDTO> getEticketAttachmentDTO() {
		return eticketAttachmentDTO;
	}
	public void setEticketAttachmentDTO(
			List<InvoiceTicketAttachmentDTO> eticketAttachmentDTO) {
		this.eticketAttachmentDTO = eticketAttachmentDTO;
	}
	public List<InvoiceTicketAttachmentDTO> getQrcodeAttachmentDTO() {
		return qrcodeAttachmentDTO;
	}
	public void setQrcodeAttachmentDTO(
			List<InvoiceTicketAttachmentDTO> qrcodeAttachmentDTO) {
		this.qrcodeAttachmentDTO = qrcodeAttachmentDTO;
	}
	public List<InvoiceTicketAttachmentDTO> getBarcodeAttachmentDTO() {
		return barcodeAttachmentDTO;
	}
	public void setBarcodeAttachmentDTO(
			List<InvoiceTicketAttachmentDTO> barcodeAttachmentDTO) {
		this.barcodeAttachmentDTO = barcodeAttachmentDTO;
	}
	public Integer getEticketCount() {
		return eticketCount;
	}
	public void setEticketCount(Integer eticketCount) {
		this.eticketCount = eticketCount;
	}
	public Integer getQrcodeCount() {
		return qrcodeCount;
	}
	public void setQrcodeCount(Integer qrcodeCount) {
		this.qrcodeCount = qrcodeCount;
	}
	public Integer getBarcodeCount() {
		return barcodeCount;
	}
	public void setBarcodeCount(Integer barcodeCount) {
		this.barcodeCount = barcodeCount;
	}
	public List<POShippingMethodDTO> getShippingMethodDTO() {
		return shippingMethodDTO;
	}
	public void setShippingMethodDTO(List<POShippingMethodDTO> shippingMethodDTO) {
		this.shippingMethodDTO = shippingMethodDTO;
	}
	
}
