package com.rtw.tracker.pojos;

import java.util.Collection;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.ShippingMethod;

public class ShippingMethodDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private Collection<ShippingMethod> shippingMethodList;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Collection<ShippingMethod> getShippingMethodList() {
		return shippingMethodList;
	}
	public void setShippingMethodList(Collection<ShippingMethod> shippingMethodList) {
		this.shippingMethodList = shippingMethodList;
	}
	
}
