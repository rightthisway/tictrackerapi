package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.SuperFanLevels;

public class SuperFanLevelDTO {

	private List<SuperFanLevels> levels;
	private SuperFanLevels level;
	private Error error = new Error(); 
	private String message = new String();
	private Integer status;
	private PaginationDTO pagination;
	
	
	public List<SuperFanLevels> getLevels() {
		return levels;
	}
	public void setLevels(List<SuperFanLevels> levels) {
		this.levels = levels;
	}
	public SuperFanLevels getLevel() {
		return level;
	}
	public void setLevel(SuperFanLevels level) {
		this.level = level;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public PaginationDTO getPagination() {
		return pagination;
	}
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}
	
	
}
