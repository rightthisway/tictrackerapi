package com.rtw.tracker.pojos;

import java.sql.Time;
import java.util.Date;

public class PurchaseOrderDTO {

	
	private Integer id;
	private Integer customerId;
	private String customerName;
	private String customerType;	
	private String poTotal;
	private Integer ticketQty;
	private Integer eventTicketQty;
	private String eventTicketCost;	
	private String createdDateStr;
	private String createdBy;
	private String shippingType;
	private String status;
	private String productType;
	private String lastUpdatedStr;	
	private Integer poAged;
	private String csr;	
	private String consignmentPoNo;
	private String purchaseOrderType;
	private String transactionOffice;
	private String trackingNo;
	private String shippingNotes;
	private String externalNotes;
	private String internalNotes;
	private Integer eventId;
	private String eventName;
	private String eventDateStr;
	private String eventTimeStr;
	private String posProductType;
	private Integer posPOId;		
	private String isEmailed;
	private String fedexLabelCreated;
	private String brokerId;
	
	private Integer poId;
	private String poCsr;
	private Integer poTicketCount;
	private String poCreateTime;
	private Integer poCustomerId;
	private String poShippingMethod;
	
	private String poTrackingNo;
	private String poExternalPONo;
	private String poConsignmentPONo;
	private String poTransactionOffice;
	private String poPurchaseOrderType;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public String getPoTotal() {
		return poTotal;
	}
	public void setPoTotal(String poTotal) {
		this.poTotal = poTotal;
	}
	public Integer getTicketQty() {
		return ticketQty;
	}
	public void setTicketQty(Integer ticketQty) {
		this.ticketQty = ticketQty;
	}
	public Integer getEventTicketQty() {
		return eventTicketQty;
	}
	public void setEventTicketQty(Integer eventTicketQty) {
		this.eventTicketQty = eventTicketQty;
	}
	public String getEventTicketCost() {
		return eventTicketCost;
	}
	public void setEventTicketCost(String eventTicketCost) {
		this.eventTicketCost = eventTicketCost;
	}
	public String getCreatedDateStr() {
		return createdDateStr;
	}
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getShippingType() {
		return shippingType;
	}
	public void setShippingType(String shippingType) {
		this.shippingType = shippingType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getLastUpdatedStr() {
		return lastUpdatedStr;
	}
	public void setLastUpdatedStr(String lastUpdatedStr) {
		this.lastUpdatedStr = lastUpdatedStr;
	}
	public Integer getPoAged() {
		return poAged;
	}
	public void setPoAged(Integer poAged) {
		this.poAged = poAged;
	}
	public String getCsr() {
		return csr;
	}
	public void setCsr(String csr) {
		this.csr = csr;
	}
	public String getConsignmentPoNo() {
		return consignmentPoNo;
	}
	public void setConsignmentPoNo(String consignmentPoNo) {
		this.consignmentPoNo = consignmentPoNo;
	}
	public String getPurchaseOrderType() {
		return purchaseOrderType;
	}
	public void setPurchaseOrderType(String purchaseOrderType) {
		this.purchaseOrderType = purchaseOrderType;
	}
	public String getTransactionOffice() {
		return transactionOffice;
	}
	public void setTransactionOffice(String transactionOffice) {
		this.transactionOffice = transactionOffice;
	}
	public String getTrackingNo() {
		return trackingNo;
	}
	public void setTrackingNo(String trackingNo) {
		this.trackingNo = trackingNo;
	}
	public String getShippingNotes() {
		return shippingNotes;
	}
	public void setShippingNotes(String shippingNotes) {
		this.shippingNotes = shippingNotes;
	}
	public String getExternalNotes() {
		return externalNotes;
	}
	public void setExternalNotes(String externalNotes) {
		this.externalNotes = externalNotes;
	}
	public String getInternalNotes() {
		return internalNotes;
	}
	public void setInternalNotes(String internalNotes) {
		this.internalNotes = internalNotes;
	}
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getEventDateStr() {
		return eventDateStr;
	}
	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}
	public String getEventTimeStr() {
		return eventTimeStr;
	}
	public void setEventTimeStr(String eventTimeStr) {
		this.eventTimeStr = eventTimeStr;
	}
	public String getPosProductType() {
		return posProductType;
	}
	public void setPosProductType(String posProductType) {
		this.posProductType = posProductType;
	}
	public Integer getPosPOId() {
		return posPOId;
	}
	public void setPosPOId(Integer posPOId) {
		this.posPOId = posPOId;
	}
	public String getIsEmailed() {
		return isEmailed;
	}
	public void setIsEmailed(String isEmailed) {
		this.isEmailed = isEmailed;
	}
	public String getFedexLabelCreated() {
		return fedexLabelCreated;
	}
	public void setFedexLabelCreated(String fedexLabelCreated) {
		this.fedexLabelCreated = fedexLabelCreated;
	}
	public String getBrokerId() {
		return brokerId;
	}
	public void setBrokerId(String brokerId) {
		this.brokerId = brokerId;
	}
	public Integer getPoId() {
		return poId;
	}
	public void setPoId(Integer poId) {
		this.poId = poId;
	}
	public String getPoCsr() {
		return poCsr;
	}
	public void setPoCsr(String poCsr) {
		this.poCsr = poCsr;
	}
	public Integer getPoTicketCount() {
		return poTicketCount;
	}
	public void setPoTicketCount(Integer poTicketCount) {
		this.poTicketCount = poTicketCount;
	}
	public String getPoCreateTime() {
		return poCreateTime;
	}
	public void setPoCreateTime(String poCreateTime) {
		this.poCreateTime = poCreateTime;
	}
	public Integer getPoCustomerId() {
		return poCustomerId;
	}
	public void setPoCustomerId(Integer poCustomerId) {
		this.poCustomerId = poCustomerId;
	}
	public String getPoShippingMethod() {
		return poShippingMethod;
	}
	public void setPoShippingMethod(String poShippingMethod) {
		this.poShippingMethod = poShippingMethod;
	}
	public String getPoTrackingNo() {
		return poTrackingNo;
	}
	public void setPoTrackingNo(String poTrackingNo) {
		this.poTrackingNo = poTrackingNo;
	}
	public String getPoExternalPONo() {
		return poExternalPONo;
	}
	public void setPoExternalPONo(String poExternalPONo) {
		this.poExternalPONo = poExternalPONo;
	}
	public String getPoConsignmentPONo() {
		return poConsignmentPONo;
	}
	public void setPoConsignmentPONo(String poConsignmentPONo) {
		this.poConsignmentPONo = poConsignmentPONo;
	}
	public String getPoTransactionOffice() {
		return poTransactionOffice;
	}
	public void setPoTransactionOffice(String poTransactionOffice) {
		this.poTransactionOffice = poTransactionOffice;
	}
	public String getPoPurchaseOrderType() {
		return poPurchaseOrderType;
	}
	public void setPoPurchaseOrderType(String poPurchaseOrderType) {
		this.poPurchaseOrderType = poPurchaseOrderType;
	}
		
}
