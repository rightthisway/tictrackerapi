package com.rtw.tracker.pojos;

import com.rtw.tracker.datas.TicketGroupTicketAttachment;

public class TicketGroupDTO {

	private Integer id;
	private Integer eventId;
	private String eventName;
	private String eventDate;
	private String eventTime;
	private String venue;
	private String section;
	private String row;
	private String seatLow;
	private String seatHigh;
	private Integer quantity;
	private String price;
	private String onHand;
	private Integer invoiceId;
	
	private Integer tgId;
	private String tgSection;
	private String tgRow;
	private String tgSeatLow;
	private String tgSeatHigh;
	private Integer tgQuantity;
	private String tgPrice;
	private Integer tgEventId;
	private String tgEventName;
	private String tgEventDate;
	private String tgEventTime;
	private String tgInstantDownload;
	private TicketGroupTicketAttachment tgTicketAttachment;
	private Integer tgrpId;
	private String tgrpType;
	private String tgrpPosition;
	private String tgrpFilePath;
	private String tgrpFileName;
	
	private Integer purchaseOrderId;
	private String availableSeatLow;
	private String availableSeatHigh;
	private Integer availableQty;
	private String priceStr;
	private String createdBy;
	
	private Integer ticketGroupId;
	private String ticketGroupSection;
	private String ticketGroupRow;
	private String ticketGroupMappedSeatLow;
	private String ticketGroupMappedSeatHigh;
	private Integer ticketGroupMappedQty;
	private String ticketGroupPrice;
	
	private String taxAmount;
	private String sectionRange;
	private String rowRange;
	private String shippingMethod;
	private String productType;
	private String retailPrice;
	private String wholeSalePrice;
	private String facePrice;
	private String loyalFanPrice;
	private String cost;
	private Boolean broadcast;
	private String internalNotes;
	private String externalNotes;
	private String marketPlaceNotes;
	private String maxShowing;
	private String nearTermDisplayOption;
	private Integer brokerId;
	private String ticketType;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getRow() {
		return row;
	}
	public void setRow(String row) {
		this.row = row;
	}
	public String getSeatLow() {
		return seatLow;
	}
	public void setSeatLow(String seatLow) {
		this.seatLow = seatLow;
	}
	public String getSeatHigh() {
		return seatHigh;
	}
	public void setSeatHigh(String seatHigh) {
		this.seatHigh = seatHigh;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getOnHand() {
		return onHand;
	}
	public void setOnHand(String onHand) {
		this.onHand = onHand;
	}
	public Integer getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}
	public Integer getTgId() {
		return tgId;
	}
	public void setTgId(Integer tgId) {
		this.tgId = tgId;
	}
	public String getTgSection() {
		return tgSection;
	}
	public void setTgSection(String tgSection) {
		this.tgSection = tgSection;
	}
	public String getTgRow() {
		return tgRow;
	}
	public void setTgRow(String tgRow) {
		this.tgRow = tgRow;
	}
	public String getTgSeatLow() {
		return tgSeatLow;
	}
	public void setTgSeatLow(String tgSeatLow) {
		this.tgSeatLow = tgSeatLow;
	}
	public String getTgSeatHigh() {
		return tgSeatHigh;
	}
	public void setTgSeatHigh(String tgSeatHigh) {
		this.tgSeatHigh = tgSeatHigh;
	}
	public Integer getTgQuantity() {
		return tgQuantity;
	}
	public void setTgQuantity(Integer tgQuantity) {
		this.tgQuantity = tgQuantity;
	}
	public String getTgPrice() {
		return tgPrice;
	}
	public void setTgPrice(String tgPrice) {
		this.tgPrice = tgPrice;
	}
	public Integer getTgEventId() {
		return tgEventId;
	}
	public void setTgEventId(Integer tgEventId) {
		this.tgEventId = tgEventId;
	}
	public String getTgEventName() {
		return tgEventName;
	}
	public void setTgEventName(String tgEventName) {
		this.tgEventName = tgEventName;
	}
	public String getTgEventDate() {
		return tgEventDate;
	}
	public void setTgEventDate(String tgEventDate) {
		this.tgEventDate = tgEventDate;
	}
	public String getTgEventTime() {
		return tgEventTime;
	}
	public void setTgEventTime(String tgEventTime) {
		this.tgEventTime = tgEventTime;
	}
	public String getTgInstantDownload() {
		return tgInstantDownload;
	}
	public void setTgInstantDownload(String tgInstantDownload) {
		this.tgInstantDownload = tgInstantDownload;
	}
	public TicketGroupTicketAttachment getTgTicketAttachment() {
		return tgTicketAttachment;
	}
	public void setTgTicketAttachment(TicketGroupTicketAttachment tgTicketAttachment) {
		this.tgTicketAttachment = tgTicketAttachment;
	}
	public Integer getTgrpId() {
		return tgrpId;
	}
	public void setTgrpId(Integer tgrpId) {
		this.tgrpId = tgrpId;
	}
	public String getTgrpType() {
		return tgrpType;
	}
	public void setTgrpType(String tgrpType) {
		this.tgrpType = tgrpType;
	}
	public String getTgrpPosition() {
		return tgrpPosition;
	}
	public void setTgrpPosition(String tgrpPosition) {
		this.tgrpPosition = tgrpPosition;
	}
	public String getTgrpFilePath() {
		return tgrpFilePath;
	}
	public void setTgrpFilePath(String tgrpFilePath) {
		this.tgrpFilePath = tgrpFilePath;
	}
	public String getTgrpFileName() {
		return tgrpFileName;
	}
	public void setTgrpFileName(String tgrpFileName) {
		this.tgrpFileName = tgrpFileName;
	}
	public Integer getPurchaseOrderId() {
		return purchaseOrderId;
	}
	public void setPurchaseOrderId(Integer purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}
	public String getAvailableSeatLow() {
		return availableSeatLow;
	}
	public void setAvailableSeatLow(String availableSeatLow) {
		this.availableSeatLow = availableSeatLow;
	}
	public String getAvailableSeatHigh() {
		return availableSeatHigh;
	}
	public void setAvailableSeatHigh(String availableSeatHigh) {
		this.availableSeatHigh = availableSeatHigh;
	}
	public Integer getAvailableQty() {
		return availableQty;
	}
	public void setAvailableQty(Integer availableQty) {
		this.availableQty = availableQty;
	}
	public String getPriceStr() {
		return priceStr;
	}
	public void setPriceStr(String priceStr) {
		this.priceStr = priceStr;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Integer getTicketGroupId() {
		return ticketGroupId;
	}
	public void setTicketGroupId(Integer ticketGroupId) {
		this.ticketGroupId = ticketGroupId;
	}
	public String getTicketGroupSection() {
		return ticketGroupSection;
	}
	public void setTicketGroupSection(String ticketGroupSection) {
		this.ticketGroupSection = ticketGroupSection;
	}
	public String getTicketGroupRow() {
		return ticketGroupRow;
	}
	public void setTicketGroupRow(String ticketGroupRow) {
		this.ticketGroupRow = ticketGroupRow;
	}
	public String getTicketGroupMappedSeatLow() {
		return ticketGroupMappedSeatLow;
	}
	public void setTicketGroupMappedSeatLow(String ticketGroupMappedSeatLow) {
		this.ticketGroupMappedSeatLow = ticketGroupMappedSeatLow;
	}
	public String getTicketGroupMappedSeatHigh() {
		return ticketGroupMappedSeatHigh;
	}
	public void setTicketGroupMappedSeatHigh(String ticketGroupMappedSeatHigh) {
		this.ticketGroupMappedSeatHigh = ticketGroupMappedSeatHigh;
	}
	public Integer getTicketGroupMappedQty() {
		return ticketGroupMappedQty;
	}
	public void setTicketGroupMappedQty(Integer ticketGroupMappedQty) {
		this.ticketGroupMappedQty = ticketGroupMappedQty;
	}
	public String getTicketGroupPrice() {
		return ticketGroupPrice;
	}
	public void setTicketGroupPrice(String ticketGroupPrice) {
		this.ticketGroupPrice = ticketGroupPrice;
	}
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public String getEventTime() {
		return eventTime;
	}
	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}
	public String getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(String taxAmount) {
		this.taxAmount = taxAmount;
	}
	public String getSectionRange() {
		return sectionRange;
	}
	public void setSectionRange(String sectionRange) {
		this.sectionRange = sectionRange;
	}
	public String getRowRange() {
		return rowRange;
	}
	public void setRowRange(String rowRange) {
		this.rowRange = rowRange;
	}
	public String getShippingMethod() {
		return shippingMethod;
	}
	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getRetailPrice() {
		return retailPrice;
	}
	public void setRetailPrice(String retailPrice) {
		this.retailPrice = retailPrice;
	}
	public String getWholeSalePrice() {
		return wholeSalePrice;
	}
	public void setWholeSalePrice(String wholeSalePrice) {
		this.wholeSalePrice = wholeSalePrice;
	}
	public String getFacePrice() {
		return facePrice;
	}
	public void setFacePrice(String facePrice) {
		this.facePrice = facePrice;
	}
	public String getLoyalFanPrice() {
		return loyalFanPrice;
	}
	public void setLoyalFanPrice(String loyalFanPrice) {
		this.loyalFanPrice = loyalFanPrice;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public Boolean getBroadcast() {
		return broadcast;
	}
	public void setBroadcast(Boolean broadcast) {
		this.broadcast = broadcast;
	}
	public String getInternalNotes() {
		return internalNotes;
	}
	public void setInternalNotes(String internalNotes) {
		this.internalNotes = internalNotes;
	}
	public String getExternalNotes() {
		return externalNotes;
	}
	public void setExternalNotes(String externalNotes) {
		this.externalNotes = externalNotes;
	}
	public String getMarketPlaceNotes() {
		return marketPlaceNotes;
	}
	public void setMarketPlaceNotes(String marketPlaceNotes) {
		this.marketPlaceNotes = marketPlaceNotes;
	}
	public String getMaxShowing() {
		return maxShowing;
	}
	public void setMaxShowing(String maxShowing) {
		this.maxShowing = maxShowing;
	}
	public String getNearTermDisplayOption() {
		return nearTermDisplayOption;
	}
	public void setNearTermDisplayOption(String nearTermDisplayOption) {
		this.nearTermDisplayOption = nearTermDisplayOption;
	}
	public Integer getBrokerId() {
		return brokerId;
	}
	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}
	public String getTicketType() {
		return ticketType;
	}
	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}
		
}
