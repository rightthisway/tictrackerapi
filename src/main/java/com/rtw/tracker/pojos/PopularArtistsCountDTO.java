package com.rtw.tracker.pojos;

import com.rtw.tmat.utils.Error;

public class PopularArtistsCountDTO {

	private Integer status;
	private Error error; 
	private String message;
	private String count;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public com.rtw.tmat.utils.Error getError() {
		return error;
	}
	public void setError(com.rtw.tmat.utils.Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCount() {
		return count;
	}
	public void setCount(String count) {
		this.count = count;
	}
	
}
