package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class EditUsersDTO {

	private Integer status;
	private Error error; 
	private String message = new String();
	private PaginationDTO paginationDTO;
	private EditTrackerUserCustomDTO editTrackerUserCustomDTO;
	private List<String> rolesList;
	private TrackerBrokerCustomDTO trackerBrokerCustomDTO;
	private AffiliateSettingsCustomDTO affiliateSettingCustomDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}
	public EditTrackerUserCustomDTO getEditTrackerUserCustomDTO() {
		return editTrackerUserCustomDTO;
	}
	public void setEditTrackerUserCustomDTO(EditTrackerUserCustomDTO editTrackerUserCustomDTO) {
		this.editTrackerUserCustomDTO = editTrackerUserCustomDTO;
	}
	public List<String> getRolesList() {
		return rolesList;
	}
	public void setRolesList(List<String> rolesList) {
		this.rolesList = rolesList;
	}
	public TrackerBrokerCustomDTO getTrackerBrokerCustomDTO() {
		return trackerBrokerCustomDTO;
	}
	public void setTrackerBrokerCustomDTO(TrackerBrokerCustomDTO trackerBrokerCustomDTO) {
		this.trackerBrokerCustomDTO = trackerBrokerCustomDTO;
	}
	public AffiliateSettingsCustomDTO getAffiliateSettingCustomDTO() {
		return affiliateSettingCustomDTO;
	}
	public void setAffiliateSettingCustomDTO(
			AffiliateSettingsCustomDTO affiliateSettingCustomDTO) {
		this.affiliateSettingCustomDTO = affiliateSettingCustomDTO;
	}
	
}
