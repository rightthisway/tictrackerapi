package com.rtw.tracker.pojos;

import java.util.Collection;
import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.HoldTicketDTO;
import com.rtw.tracker.datas.Ticket;

public class TicketListDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	List<HoldTicketDTO> tickets;
	private PaginationDTO paginationDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}
	public List<HoldTicketDTO> getTickets() {
		return tickets;
	}
	public void setTickets(List<HoldTicketDTO> tickets) {
		this.tickets = tickets;
	}
	
}
