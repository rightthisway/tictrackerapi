package com.rtw.tracker.pojos;

import java.util.Date;


public class CustomerAddressDTO {

	private Integer id;
	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private String street1;
	private String street2;
	private String city;
	private String state;
	private Integer stateId;
	private String country;
	private Integer countryId;
	private String zip;
	private String addressType;
	
	private String addressLine1;
	private String addressLine2;
	private String zipCode;
	
	private Integer shId;
	
	private Integer shipAddrOrderId;
	private String shipAddrFirstName;
	private String shipAddrLastName;
	private String shipAddrAddressLine1;
	private String shipAddrAddressLine2;
	private String shipAddrEmail;
	private String shipAddrPhone1;
	private String shipAddrCity;
	private Integer shipAddrState;
	private String shipAddrStateName;
	private Integer shipAddrCountry;
	private String shipAddrCountryName;
	private String shipAddrZipCode;
	
	private Integer billAddrOrderId;
	private String billAddrFirstName;
	private String billAddrLastName;
	private String billAddrAddressLine1;
	private String billAddrAddressLine2;
	private String billAddrEmail;
	private String billAddrPhone1;
	private String billAddrCity;
	private Integer billAddrState;
	private String billAddrStateName;
	private Integer billAddrCountry;
	private String billAddrCountryName;
	private String billAddrZipCode;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getStreet1() {
		return street1;
	}
	public void setStreet1(String street1) {
		this.street1 = street1;
	}
	public String getStreet2() {
		return street2;
	}
	public void setStreet2(String street2) {
		this.street2 = street2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Integer getStateId() {
		return stateId;
	}
	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Integer getCountryId() {
		return countryId;
	}
	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getAddressType() {
		return addressType;
	}
	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public Integer getShipAddrOrderId() {
		return shipAddrOrderId;
	}
	public void setShipAddrOrderId(Integer shipAddrOrderId) {
		this.shipAddrOrderId = shipAddrOrderId;
	}
	public String getShipAddrFirstName() {
		return shipAddrFirstName;
	}
	public void setShipAddrFirstName(String shipAddrFirstName) {
		this.shipAddrFirstName = shipAddrFirstName;
	}
	public String getShipAddrLastName() {
		return shipAddrLastName;
	}
	public void setShipAddrLastName(String shipAddrLastName) {
		this.shipAddrLastName = shipAddrLastName;
	}
	public String getShipAddrAddressLine1() {
		return shipAddrAddressLine1;
	}
	public void setShipAddrAddressLine1(String shipAddrAddressLine1) {
		this.shipAddrAddressLine1 = shipAddrAddressLine1;
	}
	public String getShipAddrAddressLine2() {
		return shipAddrAddressLine2;
	}
	public void setShipAddrAddressLine2(String shipAddrAddressLine2) {
		this.shipAddrAddressLine2 = shipAddrAddressLine2;
	}
	public String getShipAddrEmail() {
		return shipAddrEmail;
	}
	public void setShipAddrEmail(String shipAddrEmail) {
		this.shipAddrEmail = shipAddrEmail;
	}
	public String getShipAddrPhone1() {
		return shipAddrPhone1;
	}
	public void setShipAddrPhone1(String shipAddrPhone1) {
		this.shipAddrPhone1 = shipAddrPhone1;
	}
	public String getShipAddrCity() {
		return shipAddrCity;
	}
	public void setShipAddrCity(String shipAddrCity) {
		this.shipAddrCity = shipAddrCity;
	}
	public Integer getShipAddrState() {
		return shipAddrState;
	}
	public void setShipAddrState(Integer shipAddrState) {
		this.shipAddrState = shipAddrState;
	}
	public String getShipAddrStateName() {
		return shipAddrStateName;
	}
	public void setShipAddrStateName(String shipAddrStateName) {
		this.shipAddrStateName = shipAddrStateName;
	}
	public Integer getShipAddrCountry() {
		return shipAddrCountry;
	}
	public void setShipAddrCountry(Integer shipAddrCountry) {
		this.shipAddrCountry = shipAddrCountry;
	}
	public String getShipAddrCountryName() {
		return shipAddrCountryName;
	}
	public void setShipAddrCountryName(String shipAddrCountryName) {
		this.shipAddrCountryName = shipAddrCountryName;
	}
	public String getShipAddrZipCode() {
		return shipAddrZipCode;
	}
	public void setShipAddrZipCode(String shipAddrZipCode) {
		this.shipAddrZipCode = shipAddrZipCode;
	}
	public Integer getBillAddrOrderId() {
		return billAddrOrderId;
	}
	public void setBillAddrOrderId(Integer billAddrOrderId) {
		this.billAddrOrderId = billAddrOrderId;
	}
	public String getBillAddrFirstName() {
		return billAddrFirstName;
	}
	public void setBillAddrFirstName(String billAddrFirstName) {
		this.billAddrFirstName = billAddrFirstName;
	}
	public String getBillAddrLastName() {
		return billAddrLastName;
	}
	public void setBillAddrLastName(String billAddrLastName) {
		this.billAddrLastName = billAddrLastName;
	}
	public String getBillAddrAddressLine1() {
		return billAddrAddressLine1;
	}
	public void setBillAddrAddressLine1(String billAddrAddressLine1) {
		this.billAddrAddressLine1 = billAddrAddressLine1;
	}
	public String getBillAddrAddressLine2() {
		return billAddrAddressLine2;
	}
	public void setBillAddrAddressLine2(String billAddrAddressLine2) {
		this.billAddrAddressLine2 = billAddrAddressLine2;
	}
	public String getBillAddrEmail() {
		return billAddrEmail;
	}
	public void setBillAddrEmail(String billAddrEmail) {
		this.billAddrEmail = billAddrEmail;
	}
	public String getBillAddrPhone1() {
		return billAddrPhone1;
	}
	public void setBillAddrPhone1(String billAddrPhone1) {
		this.billAddrPhone1 = billAddrPhone1;
	}
	public String getBillAddrCity() {
		return billAddrCity;
	}
	public void setBillAddrCity(String billAddrCity) {
		this.billAddrCity = billAddrCity;
	}
	public Integer getBillAddrState() {
		return billAddrState;
	}
	public void setBillAddrState(Integer billAddrState) {
		this.billAddrState = billAddrState;
	}
	public String getBillAddrStateName() {
		return billAddrStateName;
	}
	public void setBillAddrStateName(String billAddrStateName) {
		this.billAddrStateName = billAddrStateName;
	}
	public Integer getBillAddrCountry() {
		return billAddrCountry;
	}
	public void setBillAddrCountry(Integer billAddrCountry) {
		this.billAddrCountry = billAddrCountry;
	}
	public String getBillAddrCountryName() {
		return billAddrCountryName;
	}
	public void setBillAddrCountryName(String billAddrCountryName) {
		this.billAddrCountryName = billAddrCountryName;
	}
	public String getBillAddrZipCode() {
		return billAddrZipCode;
	}
	public void setBillAddrZipCode(String billAddrZipCode) {
		this.billAddrZipCode = billAddrZipCode;
	}
	public Integer getShId() {
		return shId;
	}
	public void setShId(Integer shId) {
		this.shId = shId;
	}
	
}
