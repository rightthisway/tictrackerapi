package com.rtw.tracker.pojos;

import java.util.Collection;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.TrackerBrokers;

public class GettingBrokerDTO {

	private Integer status;
	private Error error; 
	private String message;
	private String brokerId;
	private String companyName;
	private Collection<TrackerBrokers> trackerBrokers;
	private TrackerBrokers trackerBroker;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public com.rtw.tmat.utils.Error getError() {
		return error;
	}
	public void setError(com.rtw.tmat.utils.Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getBrokerId() {
		return brokerId;
	}
	public void setBrokerId(String brokerId) {
		this.brokerId = brokerId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Collection<TrackerBrokers> getTrackerBrokers() {
		return trackerBrokers;
	}
	public void setTrackerBrokers(Collection<TrackerBrokers> trackerBrokers) {
		this.trackerBrokers = trackerBrokers;
	}
	public TrackerBrokers getTrackerBroker() {
		return trackerBroker;
	}
	public void setTrackerBroker(TrackerBrokers trackerBroker) {
		this.trackerBroker = trackerBroker;
	}
	
}
