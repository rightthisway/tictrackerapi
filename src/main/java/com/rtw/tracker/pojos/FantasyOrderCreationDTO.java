package com.rtw.tracker.pojos;

import com.rtw.tmat.utils.Error;

public class FantasyOrderCreationDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message;
	private Integer orderNo;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(Integer orderNo) {
		this.orderNo = orderNo;
	}
	
}
