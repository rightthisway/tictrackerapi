package com.rtw.tracker.pojos;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.Customer;
import com.rtw.tracker.datas.CustomerOrder;
import com.rtw.tracker.datas.Invoice;
import com.rtw.tracker.datas.InvoiceRefund;

public class InvoiceRefundDetailsDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private CustomerOrderDTO customerOrderDTO;
	private InvoiceDTO invoiceDTO;
	private CustomersCustomDTO customerDTO;
	private Boolean isValid;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public CustomerOrderDTO getCustomerOrderDTO() {
		return customerOrderDTO;
	}
	public void setCustomerOrderDTO(CustomerOrderDTO customerOrderDTO) {
		this.customerOrderDTO = customerOrderDTO;
	}
	public InvoiceDTO getInvoiceDTO() {
		return invoiceDTO;
	}
	public void setInvoiceDTO(InvoiceDTO invoiceDTO) {
		this.invoiceDTO = invoiceDTO;
	}
	public CustomersCustomDTO getCustomerDTO() {
		return customerDTO;
	}
	public void setCustomerDTO(CustomersCustomDTO customerDTO) {
		this.customerDTO = customerDTO;
	}
	public Boolean getIsValid() {
		return isValid;
	}
	public void setIsValid(Boolean isValid) {
		this.isValid = isValid;
	}
		
}
