package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.CrownJewelLeagues;
import com.rtw.tracker.datas.FantasyChildCategory;
import com.rtw.tracker.datas.FantasyGrandChildCategory;

public class CrownJewelLeaguesDTO {

	private Integer status;
	private Error error; 
	private String message = new String();
	private String childId;
	private String grandChildId;
	private List<CrownJewelLeagues> leaguesList;
	private List<FantasyChildCategory> childCategories;
	private List<FantasyGrandChildCategory> grandChildCategories;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getChildId() {
		return childId;
	}
	public void setChildId(String childId) {
		this.childId = childId;
	}
	public String getGrandChildId() {
		return grandChildId;
	}
	public void setGrandChildId(String grandChildId) {
		this.grandChildId = grandChildId;
	}
	public List<FantasyChildCategory> getChildCategories() {
		return childCategories;
	}
	public void setChildCategories(List<FantasyChildCategory> childCategories) {
		this.childCategories = childCategories;
	}
	public List<FantasyGrandChildCategory> getGrandChildCategories() {
		return grandChildCategories;
	}
	public void setGrandChildCategories(List<FantasyGrandChildCategory> grandChildCategories) {
		this.grandChildCategories = grandChildCategories;
	}
	public List<CrownJewelLeagues> getLeaguesList() {
		return leaguesList;
	}
	public void setLeaguesList(List<CrownJewelLeagues> leaguesList) {
		this.leaguesList = leaguesList;
	}
	
	
}
