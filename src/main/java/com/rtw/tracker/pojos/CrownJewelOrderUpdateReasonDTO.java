package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class CrownJewelOrderUpdateReasonDTO {

	private Integer status;
	private Error error; 
	private String message = new String();
	private List<CrownJewelCustomerOrderDTO> crownJewelCustomerOrderDTOs;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<CrownJewelCustomerOrderDTO> getCrownJewelCustomerOrderDTOs() {
		return crownJewelCustomerOrderDTOs;
	}
	public void setCrownJewelCustomerOrderDTOs(
			List<CrownJewelCustomerOrderDTO> crownJewelCustomerOrderDTOs) {
		this.crownJewelCustomerOrderDTOs = crownJewelCustomerOrderDTOs;
	}
		
}
