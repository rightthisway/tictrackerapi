package com.rtw.tracker.pojos;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnore;
public class LiveCustDetails implements Serializable{
		
	
		private Integer cuId;
		private String uId;
		@JsonIgnore
		private String imgP;
		private String imgU;

		public Integer getCuId() {
			if(cuId == null) {
				cuId=0;
			}
			return cuId;
		}

		public void setCuId(Integer cuId) {
			this.cuId = cuId;
		}

		public String getuId() {
			if(uId == null) {
				uId="";
			}
			return uId;
		}

		public void setuId(String uId) {
			this.uId = uId;
		}

		public String getImgP() {
			return imgP;
		}

		public void setImgP(String imgP) {
			this.imgP = imgP;
		}

		public String getImgU() {
			if(imgU == null) {
				imgU="";
			} else {
				//imgU = URLUtil.profilePicWebURByImageName(this.imgP);
				//imgU = URLUtil.profilePicForSummary(this.imgP);
			}
			return imgU;
		}

		public void setImgU(String imgU) {
			this.imgU = imgU;
		}
	
		
	}