package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;

public class SeatGeekOrderDetailsDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message;
	private PaginationDTO seatGeekPaginationDTO;
	private List<SeatGeekOrdersDTO> seatGeekOrdersDTO;	
	private String fromDate;
	private String toDate;
	private String orderNo;
	private String selectedTab;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public PaginationDTO getSeatGeekPaginationDTO() {
		return seatGeekPaginationDTO;
	}
	public void setSeatGeekPaginationDTO(PaginationDTO seatGeekPaginationDTO) {
		this.seatGeekPaginationDTO = seatGeekPaginationDTO;
	}
	public List<SeatGeekOrdersDTO> getSeatGeekOrdersDTO() {
		return seatGeekOrdersDTO;
	}
	public void setSeatGeekOrdersDTO(List<SeatGeekOrdersDTO> seatGeekOrdersDTO) {
		this.seatGeekOrdersDTO = seatGeekOrdersDTO;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getSelectedTab() {
		return selectedTab;
	}
	public void setSelectedTab(String selectedTab) {
		this.selectedTab = selectedTab;
	}
	
}
