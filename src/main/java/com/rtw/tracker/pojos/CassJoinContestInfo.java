package com.rtw.tracker.pojos;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("CassJoinContestInfo")
public class CassJoinContestInfo {
	
	private Integer sts;
	private CassError err; 
	private String msg;
	private Integer lqNo;
	private Boolean isLstCrt;
	private Boolean isLifeUsed=false;
	
	private Double caRwds;
	private Integer tCount=0;
	
	public Integer getSts() {
		return sts;
	}

	public void setSts(Integer sts) {
		this.sts = sts;
	}

	public CassError getErr() {
		return err;
	}

	public void setErr(CassError err) {
		this.err = err;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Integer getLqNo() {
		return lqNo;
	}

	public void setLqNo(Integer lqNo) {
		this.lqNo = lqNo;
	}


	public Boolean getIsLstCrt() {
		return isLstCrt;
	}

	public void setIsLstCrt(Boolean isLstCrt) {
		this.isLstCrt = isLstCrt;
	}

	public Boolean getIsLifeUsed() {
		return isLifeUsed;
	}

	public void setIsLifeUsed(Boolean isLifeUsed) {
		this.isLifeUsed = isLifeUsed;
	}

	public Double getCaRwds() {
		return caRwds;
	}

	public void setCaRwds(Double caRwds) {
		this.caRwds = caRwds;
	}

	public Integer gettCount() {
		if(tCount == null) {
			tCount = 0;
		}
		return tCount;
	}

	public void settCount(Integer tCount) {
		this.tCount = tCount;
	}

	@Override
	public String toString() {
		return "CassJoinContestInfo [sts=" + sts + ", err=" + err + ", msg=" + msg + ", lqNo=" + lqNo + ", idLstCrt="
				+ isLstCrt + ", isLifeUsed=" + isLifeUsed + ", caRwds=" + caRwds + "]";
	}
	
		
}
