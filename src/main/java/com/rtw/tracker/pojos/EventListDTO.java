package com.rtw.tracker.pojos;

import java.util.Collection;
import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.EventDetails;
import com.rtw.tracker.datas.ShippingMethod;

public class EventListDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private Collection<EventDetails> events;
	private PaginationDTO paginationDTO;
	private PaginationDTO ticketPagingInfo;
	private String fromDate;
	private String toDate;
	private Collection<ShippingMethod> shippingMethods;
	private int currentYear;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Collection<EventDetails> getEvents() {
		return events;
	}
	public void setEvents(Collection<EventDetails> events) {
		this.events = events;
	}
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}
	public PaginationDTO getTicketPagingInfo() {
		return ticketPagingInfo;
	}
	public void setTicketPagingInfo(PaginationDTO ticketPagingInfo) {
		this.ticketPagingInfo = ticketPagingInfo;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public Collection<ShippingMethod> getShippingMethods() {
		return shippingMethods;
	}
	public void setShippingMethods(Collection<ShippingMethod> shippingMethods) {
		this.shippingMethods = shippingMethods;
	}
	public int getCurrentYear() {
		return currentYear;
	}
	public void setCurrentYear(int currentYear) {
		this.currentYear = currentYear;
	}
	
}
