package com.rtw.tracker.pojos;

import java.util.List;
import com.rtw.tmat.utils.Error;

public class EventStatisticsReportDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private List<String> eventsStatisticsHeaderDTO;
	private List<Integer> eventsStatisticsDTO; 
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<String> getEventsStatisticsHeaderDTO() {
		return eventsStatisticsHeaderDTO;
	}
	public void setEventsStatisticsHeaderDTO(List<String> eventsStatisticsHeaderDTO) {
		this.eventsStatisticsHeaderDTO = eventsStatisticsHeaderDTO;
	}
	public List<Integer> getEventsStatisticsDTO() {
		return eventsStatisticsDTO;
	}
	public void setEventsStatisticsDTO(List<Integer> eventsStatisticsDTO) {
		this.eventsStatisticsDTO = eventsStatisticsDTO;
	}
			
}
