package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.FanClub;
import com.rtw.tracker.datas.FanClubEvent;
import com.rtw.tracker.datas.FanClubMember;
import com.rtw.tracker.datas.FanClubPost;
import com.rtw.tracker.datas.FanClubPostComments;
import com.rtw.tracker.datas.FanClubVideo;
import com.rtw.tracker.datas.PollingVideoCategory;

public class FanClubPostCommentDTO {

	private List<FanClubPostComments> postcommentss;
	private Error error = new Error(); 
	private String message = new String();
	private Integer status;
	private PaginationDTO pagination;
	
	}
