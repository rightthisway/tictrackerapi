package com.rtw.tracker.pojos;

public class CustomerInvoiceTicketAttachmentDTO {

	private Integer id;
	private Integer invoiceId;
	private String fileName;
	private String fileType;
	private String position;
	private String downloadTicket;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getDownloadTicket() {
		return downloadTicket;
	}
	public void setDownloadTicket(String downloadTicket) {
		this.downloadTicket = downloadTicket;
	}
	
}
