package com.rtw.tracker.pojos;

public class RelatedInvoiceDTO {

	private Integer id;
	private Integer invoiceId;
	private String eventName;
	private String section;
	private String row;
	private String seat;
	private Double retailPrice;
	private Double facePrice;
	private Double cost;
	private Double wholesalePrice;
	private Double actualSoldPrice;
	private String purchasedBy;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getRow() {
		return row;
	}
	public void setRow(String row) {
		this.row = row;
	}
	public String getSeat() {
		return seat;
	}
	public void setSeat(String seat) {
		this.seat = seat;
	}
	public Double getRetailPrice() {
		return retailPrice;
	}
	public void setRetailPrice(Double retailPrice) {
		this.retailPrice = retailPrice;
	}
	public Double getFacePrice() {
		return facePrice;
	}
	public void setFacePrice(Double facePrice) {
		this.facePrice = facePrice;
	}
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	public Double getWholesalePrice() {
		return wholesalePrice;
	}
	public void setWholesalePrice(Double wholesalePrice) {
		this.wholesalePrice = wholesalePrice;
	}
	public Double getActualSoldPrice() {
		return actualSoldPrice;
	}
	public void setActualSoldPrice(Double actualSoldPrice) {
		this.actualSoldPrice = actualSoldPrice;
	}
	public String getPurchasedBy() {
		return purchasedBy;
	}
	public void setPurchasedBy(String purchasedBy) {
		this.purchasedBy = purchasedBy;
	}
		
}
