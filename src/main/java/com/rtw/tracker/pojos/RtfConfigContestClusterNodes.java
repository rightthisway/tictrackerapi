package com.rtw.tracker.pojos;

import java.io.Serializable;

public class RtfConfigContestClusterNodes implements Serializable{

	private Integer urlId;
	private String url;
	private Integer status;

	public RtfConfigContestClusterNodes() {
	}
	public RtfConfigContestClusterNodes(Integer urlId,String url,Integer status) {
		this.urlId=urlId;
		this.url=url;
		this.status=status;
	}
	
	public Integer getUrlId() {
		return urlId;
	}
	public void setUrlId(Integer urlId) {
		this.urlId = urlId;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	
}