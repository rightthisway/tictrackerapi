package com.rtw.tracker.pojos;

import java.util.Collection;
import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.EventDetails;

public class ArtistDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private PaginationDTO paginationDTO;
	private Collection<EventDetails> artists;
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}
	
	public Collection<EventDetails> getArtists() {
		return artists;
	}
	public void setArtists(Collection<EventDetails> artists) {
		this.artists = artists;
	}
		
		
}
