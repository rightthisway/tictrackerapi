package com.rtw.tracker.pojos;

public class CustomerLoyaltyDTO {

	private Integer id;
	private Integer rewardCustomerId;
	private String activePoints;
	private String pendingPoints;
	private String totalEarnedPoints;
	private String totalSpentPoints;
	private String latestEarnedPoints;
	private String latestSpentPoints;
	private String voidedPoints;
	
	private Double pointsSpent;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getRewardCustomerId() {
		return rewardCustomerId;
	}
	public void setRewardCustomerId(Integer rewardCustomerId) {
		this.rewardCustomerId = rewardCustomerId;
	}
	public String getActivePoints() {
		return activePoints;
	}
	public void setActivePoints(String activePoints) {
		this.activePoints = activePoints;
	}
	public String getPendingPoints() {
		return pendingPoints;
	}
	public void setPendingPoints(String pendingPoints) {
		this.pendingPoints = pendingPoints;
	}
	public String getTotalEarnedPoints() {
		return totalEarnedPoints;
	}
	public void setTotalEarnedPoints(String totalEarnedPoints) {
		this.totalEarnedPoints = totalEarnedPoints;
	}
	public String getTotalSpentPoints() {
		return totalSpentPoints;
	}
	public void setTotalSpentPoints(String totalSpentPoints) {
		this.totalSpentPoints = totalSpentPoints;
	}
	public String getLatestEarnedPoints() {
		return latestEarnedPoints;
	}
	public void setLatestEarnedPoints(String latestEarnedPoints) {
		this.latestEarnedPoints = latestEarnedPoints;
	}
	public String getLatestSpentPoints() {
		return latestSpentPoints;
	}
	public void setLatestSpentPoints(String latestSpentPoints) {
		this.latestSpentPoints = latestSpentPoints;
	}
	public String getVoidedPoints() {
		return voidedPoints;
	}
	public void setVoidedPoints(String voidedPoints) {
		this.voidedPoints = voidedPoints;
	}
	public Double getPointsSpent() {
		return pointsSpent;
	}
	public void setPointsSpent(Double pointsSpent) {
		this.pointsSpent = pointsSpent;
	}
	
}
