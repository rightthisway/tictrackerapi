package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.GiftCardOrders;

public class GiftCardOrdersDTO {
	

	private Integer status;
	private Error error; 
	private String message;
	private List<GiftCardOrders> giftCardOrdersList;
	private PaginationDTO paginationDTO;	
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public PaginationDTO getPaginationDTO() {
		return paginationDTO;
	}
	public void setPaginationDTO(PaginationDTO paginationDTO) {
		this.paginationDTO = paginationDTO;
	}
	public List<GiftCardOrders> getGiftCardOrdersList() {
		return giftCardOrdersList;
	}
	public void setGiftCardOrdersList(List<GiftCardOrders> giftCardOrdersList) {
		this.giftCardOrdersList = giftCardOrdersList;
	}
	
	
	
}
