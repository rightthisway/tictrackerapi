package com.rtw.tracker.pojos;

public class ReferralContestWinnerListDTO {

	private Integer id;
	private String winnerName;
	private String winnerEmail;
	private Integer winnerCustomerId;
	private String createdBy;
	private String createdDate;
	private String startDate;
	private String endDate;
	private String contestName;
	private String winnerCity;
	private String isEmailed;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getWinnerName() {
		return winnerName;
	}
	public void setWinnerName(String winnerName) {
		this.winnerName = winnerName;
	}
	public String getWinnerEmail() {
		return winnerEmail;
	}
	public void setWinnerEmail(String winnerEmail) {
		this.winnerEmail = winnerEmail;
	}
	public Integer getWinnerCustomerId() {
		return winnerCustomerId;
	}
	public void setWinnerCustomerId(Integer winnerCustomerId) {
		this.winnerCustomerId = winnerCustomerId;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getContestName() {
		return contestName;
	}
	public void setContestName(String contestName) {
		this.contestName = contestName;
	}
	public String getWinnerCity() {
		return winnerCity;
	}
	public void setWinnerCity(String winnerCity) {
		this.winnerCity = winnerCity;
	}
	public String getIsEmailed() {
		return isEmailed;
	}
	public void setIsEmailed(String isEmailed) {
		this.isEmailed = isEmailed;
	}
	
}
