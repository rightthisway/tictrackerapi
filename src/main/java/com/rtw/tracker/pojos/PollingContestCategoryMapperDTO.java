package com.rtw.tracker.pojos;

import java.util.List;

import com.rtw.tmat.utils.Error;
import com.rtw.tracker.datas.PollingCategory;
import com.rtw.tracker.datas.PollingContCategoryMapper;

public class PollingContestCategoryMapperDTO {

	private PaginationDTO pagination;
	private Error error = new Error();
	private String message = new String();
	private Integer status;
	private List<PollingContCategoryMapper> pollingContCategoryMapper;

	public List<PollingContCategoryMapper> getPollingContCategoryMapper() {
		return pollingContCategoryMapper;
	}

	public void setPollingContCategoryMapper(
			List<PollingContCategoryMapper> pollingContCategoryMapper) {
		this.pollingContCategoryMapper = pollingContCategoryMapper;
	}
	
	public PaginationDTO getPagination() {
		return pagination;
	}

	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}
