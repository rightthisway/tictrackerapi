package com.rtw.tracker.pojos;

import java.util.Date;

public class PaymentHistoryDTO {

	private String primaryPaymentMethod;	
	private String secondaryPaymentMethod;
	private String thirdPaymentMethod;
	private Double primaryAmount;
	private Double secondaryAmount;
	private Double thirdPayAmt;
	private String paidOnStr;
	private String primaryTransaction;
	private String secondaryTransaction;
	private String thirdTransactionId;
	private String csr;
	private String cardType;
	
	private String creditCardNumber;
	private String checkNumber;
	private Double amount;
	private String note;
	private String systemUser;
	private String paymentType;
	private String transOffice;
	
	public String getPrimaryPaymentMethod() {
		return primaryPaymentMethod;
	}
	public void setPrimaryPaymentMethod(String primaryPaymentMethod) {
		this.primaryPaymentMethod = primaryPaymentMethod;
	}
	public String getSecondaryPaymentMethod() {
		return secondaryPaymentMethod;
	}
	public void setSecondaryPaymentMethod(String secondaryPaymentMethod) {
		this.secondaryPaymentMethod = secondaryPaymentMethod;
	}
	public String getThirdPaymentMethod() {
		return thirdPaymentMethod;
	}
	public void setThirdPaymentMethod(String thirdPaymentMethod) {
		this.thirdPaymentMethod = thirdPaymentMethod;
	}
	public Double getPrimaryAmount() {
		return primaryAmount;
	}
	public void setPrimaryAmount(Double primaryAmount) {
		this.primaryAmount = primaryAmount;
	}
	public Double getSecondaryAmount() {
		return secondaryAmount;
	}
	public void setSecondaryAmount(Double secondaryAmount) {
		this.secondaryAmount = secondaryAmount;
	}
	public Double getThirdPayAmt() {
		return thirdPayAmt;
	}
	public void setThirdPayAmt(Double thirdPayAmt) {
		this.thirdPayAmt = thirdPayAmt;
	}
	public String getPaidOnStr() {
		return paidOnStr;
	}
	public void setPaidOnStr(String paidOnStr) {
		this.paidOnStr = paidOnStr;
	}
	public String getPrimaryTransaction() {
		return primaryTransaction;
	}
	public void setPrimaryTransaction(String primaryTransaction) {
		this.primaryTransaction = primaryTransaction;
	}
	public String getSecondaryTransaction() {
		return secondaryTransaction;
	}
	public void setSecondaryTransaction(String secondaryTransaction) {
		this.secondaryTransaction = secondaryTransaction;
	}
	public String getThirdTransactionId() {
		return thirdTransactionId;
	}
	public void setThirdTransactionId(String thirdTransactionId) {
		this.thirdTransactionId = thirdTransactionId;
	}
	public String getCsr() {
		return csr;
	}
	public void setCsr(String csr) {
		this.csr = csr;
	}
	public String getCreditCardNumber() {
		return creditCardNumber;
	}
	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}
	public String getCheckNumber() {
		return checkNumber;
	}
	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getSystemUser() {
		return systemUser;
	}
	public void setSystemUser(String systemUser) {
		this.systemUser = systemUser;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getTransOffice() {
		return transOffice;
	}
	public void setTransOffice(String transOffice) {
		this.transOffice = transOffice;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	
}
