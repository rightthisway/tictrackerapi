package com.rtw.tracker.pojos;

import java.util.List;
import com.rtw.tmat.utils.Error;

public class SharingDiscountCodeDTO {

	private Integer status;
	private Error error = new Error(); 
	private String message = new String();
	private PaginationDTO shDiscountCodePaginationDTO;
	private List<DiscountCodeTrackingDTO> discountCodeTrackingDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public PaginationDTO getShDiscountCodePaginationDTO() {
		return shDiscountCodePaginationDTO;
	}
	public void setShDiscountCodePaginationDTO(
			PaginationDTO shDiscountCodePaginationDTO) {
		this.shDiscountCodePaginationDTO = shDiscountCodePaginationDTO;
	}
	public List<DiscountCodeTrackingDTO> getDiscountCodeTrackingDTO() {
		return discountCodeTrackingDTO;
	}
	public void setDiscountCodeTrackingDTO(
			List<DiscountCodeTrackingDTO> discountCodeTrackingDTO) {
		this.discountCodeTrackingDTO = discountCodeTrackingDTO;
	}	
		
}
