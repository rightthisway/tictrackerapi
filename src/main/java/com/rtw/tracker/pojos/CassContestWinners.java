package com.rtw.tracker.pojos;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * represents customer entity
 * @author Tamil
 *
 */
@XStreamAlias("ContestWinners")
public class CassContestWinners  implements Serializable{
	
	private UUID id;
	private Integer coId;
	private Integer cuId;
	private Integer rTix;
	private Double rPoints;
	private Date crDated;
	
	@JsonIgnore
	private String imgP;//profile image path
	private String imgU;// profile image URL
	
	private String uId;
	private String rPointsSt;
	private String jCreditSt;
	
	public CassContestWinners( ) { }
	
	public CassContestWinners(UUID id, Integer coId, Integer cuId, Integer rTix, Double rPoints, Date crDated) {
		this.id = id;
		this.coId = coId;
		this.cuId = cuId;
		this.rTix = rTix;
		this.rPoints = rPoints;
		this.crDated = crDated;
	}
	
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}

	public Integer getCoId() {
		return coId;
	}
	public void setCoId(Integer coId) {
		this.coId = coId;
	}

	public Integer getCuId() {
		return cuId;
	}
	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}

	public Integer getrTix() {
		return rTix;
	}
	public void setrTix(Integer rTix) {
		this.rTix = rTix;
	}

	public Double getrPoints() {
		return rPoints;
	}
	public void setrPoints(Double rPoints) {
		this.rPoints = rPoints;
	}

	public Date getCrDated() {
		return crDated;
	}
	public void setCrDated(Date crDated) {
		this.crDated = crDated;
	}

	public String getuId() {
		return uId;
	}
	public void setuId(String uId) {
		this.uId = uId;
	}

	public String getImgP() {
		return imgP;
	}
	public void setImgP(String imgP) {
		this.imgP = imgP;
	}

	public String getImgU() {
		return imgU;
	}
	public void setImgU(String imgU) {
		this.imgU = imgU;
	}

	public String getrPointsSt() {
		return rPointsSt;
	}
	public void setrPointsSt(String rPointsSt) {
		this.rPointsSt = rPointsSt;
	}

	public String getjCreditSt() {
		return jCreditSt;
	}

	public void setjCreditSt(String jCreditSt) {
		this.jCreditSt = jCreditSt;
	}

}
