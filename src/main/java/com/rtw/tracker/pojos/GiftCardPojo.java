package com.rtw.tracker.pojos;

import java.io.Serializable;
import java.util.Date;

import com.rtw.tmat.utils.Util;

public class GiftCardPojo implements Serializable{

	private Integer id;
	private String title;
	private String description;
	private String imageUrl;
	private String conversionType;
	private Double conversionRate;
	private Double pointsConversionRate;
	private Boolean status;
	private Date createdDate;
	private Date updatedDate;
	private String updatedBy;
	private String createdBy;
	private Double amount;
	private Integer maxQuantity;
	private Integer usedQuantity;
	private Integer remainingQty;
	private Integer maxQtyThreshold;
	private String fileName;
	private String cardStatus;
	private String cardType;
	private Integer brandId;
	private String name;
	
	private String createdDateStr;
	private String updatedDateStr;
	
	private Date startDate;
	private Date endDate;
	
	private String startDateStr;
	private String endDateStr;
	
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getConversionType() {
		return conversionType;
	}
	public void setConversionType(String conversionType) {
		this.conversionType = conversionType;
	}
	public Double getConversionRate() {
		return conversionRate;
	}
	public void setConversionRate(Double conversionRate) {
		this.conversionRate = conversionRate;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	public Integer getMaxQuantity() {
		return maxQuantity;
	}
	public void setMaxQuantity(Integer maxQuantity) {
		this.maxQuantity = maxQuantity;
	}
	public Integer getUsedQuantity() {
		return usedQuantity;
	}
	public void setUsedQuantity(Integer usedQuantity) {
		this.usedQuantity = usedQuantity;
	}
	public Integer getRemainingQty() {
		return remainingQty;
	}
	public void setRemainingQty(Integer remainingQty) {
		this.remainingQty = remainingQty;
	}
	public Integer getMaxQtyThreshold() {
		return maxQtyThreshold;
	}
	public void setMaxQtyThreshold(Integer maxQtyThreshold) {
		this.maxQtyThreshold = maxQtyThreshold;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedDateStr() {
		if(createdDate!=null){
			this.createdDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(createdDate);
		}
		return createdDateStr;
	}
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	public String getUpdatedDateStr() {
		if(updatedDate!=null){
			this.updatedDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(updatedDate);
		}
		return updatedDateStr;
	}
	public void setUpdatedDateStr(String updatedDateStr) {
		this.updatedDateStr = updatedDateStr;
	}
	public String getFileName() {
		if(imageUrl!=null && imageUrl.contains("/")){
			fileName = imageUrl.substring(imageUrl.lastIndexOf("/")+1);
		}
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getCardStatus() {
		return cardStatus;
	}
	public void setCardStatus(String cardStatus) {
		this.cardStatus = cardStatus;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getStartDateStr() {		
		if(startDate!=null){
			this.startDateStr = Util.formatDateToMonthDateYear(startDate);
		}
		return startDateStr;
	}
	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}
	public String getEndDateStr() {
		if(endDate!=null){
			this.endDateStr = Util.formatDateToMonthDateYear(endDate);
		}		
		return endDateStr;
	}
	public void setEndDateStr(String endDateStr) {	
		
		this.endDateStr = endDateStr;
	}
	public Integer getBrandId() {
		return brandId;
	}
	public void setBrandId(Integer brandId) {
		this.brandId = brandId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getPointsConversionRate() {
		return pointsConversionRate;
	}
	public void setPointsConversionRate(Double pointsConversionRate) {
		this.pointsConversionRate = pointsConversionRate;
	}
	
	
}
