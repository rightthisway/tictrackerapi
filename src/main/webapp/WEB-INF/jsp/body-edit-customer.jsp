
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
	<link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
    <link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../resources/css/font-awesome.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>
	<link href="../resources/css/jquery.alerts.css" rel=" stylesheet">
  
	 <script src="../resources/js/jquery-1.8.3.min.js"></script>
    <script src="../resources/js/bootstrap.min.js"></script>
	<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
	<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
	<script src="../resources/js/slick/slick.core.js"></script>
	<script src="../resources/js/slick/slick.formatters.js"></script>
	<script src="../resources/js/slick/slick.editors.js"></script>
	<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
	<script src="../resources/js/slick/slick.grid.js"></script>
	<script src="../resources/js/slick/slick.dataview.js"></script>
	<script src="../resources/js/slick/controls/slick.pager.js"></script>
	<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
	<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
	<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>
	<script src="../resources/js/jquery.alerts.js"></script>
  
<style>
input {
	color: black !important;
}
.slick-headerrow-column {
     background: #87ceeb;
     text-overflow: clip;
     -moz-box-sizing: border-box;
     box-sizing: border-box;
}
.slick-headerrow-column input {
     margin: 0;
     padding: 0;
     width: 100%;
     height: 100%;
     -moz-box-sizing: border-box;
     box-sizing: border-box;
}
.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}
#addCustomer {
  font-family: arial;
  font-size: 8pt;
  font-weight: bold;
  color: #2b2b2b;
  background-color: rgba(0, 122, 255, 0.32);
  border: 1px solid gray;
}

input{
		color : black !important;
	}
.panel .panel-heading {
  line-height: 34px;
  padding: 0 15px;
  min-height: 34px;
  border-left: 1px solid #ccc;
  border-right: 1px solid #ccc;
  -webkit-border-radius: 0px;
  -moz-border-radius: 0px;
  border-radius: 0px;  
  position: relative;
  box-shadow: inset 0 -2px 0 rgba(0,0,0,.05);
  -moz-box-shadow: inset 0 -2px 0 rgba(0,0,0,.05);
  -webkit-box-shadow: inset 0 -2px 0 rgba(0,0,0,.05);  
}
.panel-heading,.modal-header{
    background: #F7F7F7;
    color: black;
}
	
</style>
<script>
var shippingInfo=[];
var invoiceInfo=[];
var cardInfo=[];
var poInfo=[];
var customerId='${customerId}';
$(document).ready(function(){
	 $("div#divLoading").addClass('show');
	 $("#sameBillingEmail").change(function(){
		 if($("#sameBillingEmail").is(':checked')){
		 	var email = $('#email').val();
		 	if(email=='' || email==undefined){
		 		jAlert("Please add Primary email address from top level info tab.");
		 		$("#sameBillingEmail").attr('checked',false);
		 		return false;
		 	}
		 	$("#blEmail").val(email);
	 	 }else{
	 		$("#blEmail").val('');
	 		
	 	 }
	 });
	 $("#sameBillingPhone").change(function(){
		 if($("#sameBillingPhone").is(':checked')){
		 	var phone = $('#phone').val();
		 	if(phone=='' || phone==undefined){
		 		jAlert("Please add Primary Phone from top level info tab.");
		 		$("#sameBillingPhone").attr('checked',false);
		 		return false;
		 	}
		 	$("#blPhone").val(phone);
	 	 }else{
	 		$("#blPhone").val('');
	 		
	 	 }
	 });
	 $("#sameShippingEmail").change(function(){
		 if($("#sameShippingEmail").is(':checked')){
		 	var email = $('#email').val();
		 	if(email=='' || email==undefined){
		 		jAlert("Please add Primary email address from top level info tab.");
		 		$("#sameShippingEmail").attr('checked',false);
		 		return false;
		 	}
		 	$("#shEmail").val(email);
	 	 }else{
	 		$("#shEmail").val('');
	 		
	 	 }
	 });
	 $("#sameShippingPhone").change(function(){
		 if($("#sameShippingPhone").is(':checked')){
		 	var phone = $('#phone').val();
		 	if(phone=='' || phone==undefined){
		 		jAlert("Please add Primary Phone from top level info tab.");
		 		$("#sameShippingPhone").attr('checked',false);
		 		return false;
		 	}
		 	$("#shPhone").val(phone);
	 	 }else{
	 		$("#shPhone").val('');
	 		
	 	 }
	 });
	 getCustomerInfoForEdit(customerId);
	$('#editCustomerDiv').hide();
	$('#shippingTab').click(function(){
		setTimeout(function(){ 
		createShippingGrid(shippingInfo);},10);
	});
	$('#invoiceTab').click(function(){
		setTimeout(function(){ //createInvoiceGrid(invoiceInfo);
		getCustomerInfoForInvoice();},10);
	});
	$('#cardTab').click(function(){
		setTimeout(function(){ 
		createCreditCardGrid(cardInfo);},10);
	});
	$('#poTab').click(function(){
		setTimeout(function(){ //createPOGrid(poInfo);
		getCustomerInfoForPO();},10);
	});
	$('#loyalFanTab').click(function(){
		setTimeout(function(){
			getArtistGridData(0);},10);
	});
	$('#favouriteEventTab').click(function(){
		setTimeout(function(){
			getEventsGridData(0);},10);
	});
	$('#menuContainer').click(function(){
	  if($('.ABCD').length >0){
		   $('#menuContainer').removeClass('ABCD');
	  }else{
		   $('#menuContainer').addClass('ABCD');
	  }
	  customerGrid.resizeCanvas();
	  if(shippingGrid!=undefined){
		  shippingGrid.resizeCanvas();
	  }
	  if(invoiceGrid!=undefined){
		  invoiceGrid.resizeCanvas();
	  }
	  if(poGrid!=undefined){
		  poGrid.resizeCanvas();
	  }
	  if(creditCardGrid!=undefined){
		  creditCardGrid.resizeCanvas();
	  }
	  if(artistGrid!=undefined){
		  artistGrid.resizeCanvas();
	  }
	  if(eventGrid!=undefined){
		  eventGrid.resizeCanvas();
	  }
	});
});

function cancelAction(){
	$('#myModal-2').modal('hide');	
}

function invoiceResetFilters(){
	invoiceGridSearchString='';
	invoiceGridColumnFilters = {};
	getCustomerInfoForInvoice();
}

function poResetFilters(){
	poGridSearchString='';
	poGridColumnFilters = {};
	getCustomerInfoForPO();
}

function loyalFanResetFilters(){
	artistSearchString='';
	artistColumnFilters = {};
	getArtistGridData(0);
}

function eventResetFilters(){
	eventGridSearchString='';
	eventColumnFilters = {};
	getEventsGridData(0);
}
</script>

<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i>Accounting
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Invoice</a>
			</li>
			<li><i class="fa fa-laptop"></i>View Customer</li>
		</ol>
	</div>
</div>
<br/>
<div id="editCustomerDiv">
<div class="row">
			<section class="panel">
				<ul class="nav nav-tabs" style="margin-left:10px;">
					<li id ="defaultTab" class="active"><a style="font-size: 13px;font-family: arial;" data-toggle="tab" href="#topLevelInfo">Top-Level
							Info</a></li>
					<li class=""><a style="font-size: 13px;font-family: arial;" data-toggle="tab" href="#address">Billing Address</a></li>
					<li class=""><a style="font-size: 13px;font-family: arial;" data-toggle="tab" href="#shippingAddress" id="shippingTab">Shipping/Other Addresses</a>
					<li class=""><a style="font-size: 13px;font-family: arial;" data-toggle="tab" href="#creditCard" id="cardTab">Credit
							Card</a></li>
					<li class=""><a style="font-size: 13px;font-family: arial;" data-toggle="tab" href="#invoice" id="invoiceTab">Invoice</a></li>
					<li class=""><a style="font-size: 13px;font-family: arial;" data-toggle="tab" href="#po" id="poTab">Purchase Order</a>
					</li>
					<li class=""><a style="font-size: 13px;font-family: arial;" data-toggle="tab" href="#credit">Wallet Credit</a></li>
					<li class=""><a style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#rewardPoints">Reward Points</a></li>
					<li class=""><a style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#loyalFan" id="loyalFanTab">Loyal Fan</a></li>
					<li class=""><a style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#favouriteEvent" id="favouriteEventTab">Favourite Event</a></li>
					<li class=""><a style="font-size: 13px;font-family: arial;" data-toggle="tab" href="#notes">Notes</a></li>
				</ul>
			</section>
	</div>
	<div class="panel-body">
			<div class="tab-content">
				<div id="topLevelInfo" class="tab-pane active">
						<input id="action" value="action" name="action" type="hidden" />
						<input id="productType" value="" name="productType" type="hidden" />
						<input class="form-control" id="custId" value="${customerId}"
							name="custId" type="hidden" />
						<input type="hidden" name="billingAddressId"
							value="${billingAddress.id}" />
						<div class="form-group">
							<div class="col-sm-3">
								<label>Type <span class="required">*</span> </label>
								<select name="customerType"
									class="form-control input-sm m-bot15" id="customerType">
									<option value="PHONE_CUSTOMER">Phone Customer</option>
									<option value="WEB_CUSTOMER">Web Customer</option>
									<option value="RETAIL_CUSTOMER">Retail Customer</option>
									<option value="BROKER">Broker</option>
									<option value="VENDOR">Vendor</option>
									<option value="CORPORATE">Corporate</option>
									<option value="CONSUMER">Consumer</option>
									<option value="EMPLOYEE">Employee</option>
									<option value="INTERNAL_USE_ONLY">Internal Use Only</option>
									<option value="EXCHANGES">Exchanges</option>
									<option value="EBAY_CUSTOMER">Ebay Customer</option>
									<option value="DIVISIONS">Divisions</option>
									<option value="EBAY_ESL">Ebay ESL</option>
									<option value="EBAY_CAT">Ebay CAT</option>
									<option value="CORPORATION">Corporation</option>
									<option value="AFFILIATE">Affiliate</option>
									<option value="EBAY_ZONES">Ebay Zones</option>
									<option value="AO_WEB_CUSTOMER">AO Web Customer</option>
									<option value="EBAY_RATA">Ebay RATA</option>
									<option value="TND_ZONES">TND Zones</option>
								</select>

							</div>
							<div class="col-sm-2">
								<label>Customer status <span class="required">*</span> </label>
								<select name="customerLevel"
									class="form-control input-sm m-bot15" id="customerLevel">
									<option value="">--select--</option>
									<option value="GOLD">Gold</option>
									<option value="SILVER">Silver</option>
									<option value="PLATINUM">Platinum</option>
								</select>

							</div>

							<div class="col-sm-3">
								<label> Signup Type <span class="required">*</span> </label>
								<select name="signupType"
									class="form-control input-sm m-bot15" id="signupType">
									<option value="">--select--</option>
									<option value="REWARDTHEFAN">REWARD THE FAN</option>
									<option value="FACEBOOK">Facebook</option>
									<option value="GOOGLE">Google</option>
								</select>
							</div>
							<div class="col-sm-3">
								<label>Client/Broker <span class="required">*</span> </label>
								<select name="clientBrokerType"
									class="form-control input-sm m-bot15" id="clientBrokerType">
									<option value="client">Client</option>
									<option value="broker">Broker</option>
									<option value="both">Both</option>
								</select>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="form-group">
							<div class="col-sm-5">
								<label>First Name<span class="required">*</span> </label>
								<input class="form-control" id="name"
									name="customerName" />
							</div>
							<div class="col-sm-5">
								<label>Last Name<span class="required">*</span> </label>
								<input class="form-control" id="lastName"
									name="lastName" />
							</div>
							<div class="col-sm-5">
								<label>E-Mail <span class="required">*</span> </label>
								<input class="form-control" id="email"
									type="email" name="email" />
							</div>
							<div class="col-sm-5">
								<label>Phone <span class="required">*</span> </label>
								<input class="form-control" id="phone"
									name="phone" type="text" />
							</div>
							<div class="col-sm-5">
								<label>Ext</label>
								<input class="form-control"  id="fedexNo"
									name="extension" />
							</div>
							<div class="col-sm-5">
								<label>Other Phone </label>
								<input class="form-control"  id="otherPhone"
									name="Otherphone" type="text" />
							</div>
							<div class="col-sm-5">
								<label>Representative </label>
								<input class="form-control"
									id="representativeName" name="representativeName" type="text" />
							</div>
						</div>
						<div class="row"></div>
						<div class="row" align="center">
						<div class="col-lg-12">
							<div id="editCustomerAction" class="form-group">							
								<button class="btn btn-primary" style="margin-top:25px;" type="button" onclick="saveCustomerInfo('customerInfo');">Update</button>
								<button class="btn btn-default" style="margin-top:25px;" onclick="cancelAction();" type="button">Cancel</button>
							</div>
						</div>
						</div>
				</div>
				<div id="address" class="tab-pane">
				<header class="panel-heading"> Billing Address </header>
				<div class="form-group">
				<input id="updateBillingAddressAction" value="action" name="action" type="hidden" />
				<input class="form-control" id="custId" value="${customerId}" name="custId" type="hidden" />
				<input type="hidden" name="billingAddressId" value="${billingAddress.id}"/>
					<div class="col-sm-5">
						<label>First Name <span class="required">*</span></label> <input class="form-control"
							id="blFirstName" name="blFirstName" type="text" />
					</div>
					<div class="col-sm-5">
						<label>Last Name <span class="required">*</span><span class="required">*</span></label>
					    <input class="form-control" id="blLastName" name="blLastName" type="text" />
					</div>
					<div class="col-sm-5">
						<label>Email <span class="required">*</span></label>
					    <input class="form-control" id="blEmail" name="blEmail" type="email" />
					    <input type="checkbox" id="sameBillingEmail">Same as Primary Email
					</div>
					<div class="col-sm-5">
						<label>Phone <span class="required">*</span></label>
					    <input class="form-control" id="blPhone" name="blPhone" type="text" />
					    <input type="checkbox" id="sameBillingPhone">Same as Primary Phone
					</div>

					<div class="col-sm-5">
						<label>Street1 <span class="required">*</span></label>
					    <input class="form-control" id="street1" type="text" name="addressLine1" />
					</div>
					<div class="col-sm-5">
						<label>Street2</label>
						<input class="form-control" id="street2" type="text" name="addressLine2" />
					</div> 
					<div class="col-sm-5">
						<label>Country <span class="required">*</span></label>		
						<select id="countryName" name="countryName" class="form-control input-sm m-bot15" onchange="loadState('stateName','')">
							<option value="-1">--select--</option>
							<c:forEach var="country" items="${countries}">
         							<option value="${country.id}" 
         							<c:if test="${billingAddress.country.id == country.id}"> selected </c:if>>
         								<c:out value="${country.name}"/>
         							</option>
         						
     						 </c:forEach>
						</select>
					</div>
					<div class="col-sm-5">
						<label>State <span class="required">*</span></label>
						<select id="stateName" name="stateName" class="form-control input-sm m-bot15" onchange="">
							 <option value="-1">--select--</option>
							 <%-- <c:forEach var="state" items="${states}">
         							<option value="${state.id}" 
         								<c:if test="${billingAddress.state.id == state.id}"> selected </c:if>>
										<c:out value="${state.name}"/>		
									</option>
     						 </c:forEach> --%>
						</select>
					</div>	
					<div class="col-sm-5">
						<label>City <span class="required">*</span></label>
						<input class="form-control" id="city" type="text" name="city" />
					</div>
					<div class="col-sm-5">
						<label>Zip Code <span class="required">*</span></label>		
						<input class="form-control" id="zipCode" type="text" name="zipCode"/>
					</div>
					
				</div>
				<div class="row"></div>
				<div class="col-lg-12">&nbsp;</div>
					<div class="row" align="center">
						<div class="col-lg-12">
							<div class="form-group">
								<!--<button type="submit" class="btn btn-info pull-right">Submit</button>-->
								<button class="btn btn-primary" type="button" onclick="saveCustomerInfo('billingInfo');">Update</button>
								<button class="btn btn-default" onclick="cancelAction();" type="button">Cancel</button>
							</div>
						</div>
					</div>
				</div>
				<div id="shippingAddress" class="tab-pane">
				<header class="panel-heading"> 
					Shipping/Other Address
					<a data-toggle="modal" class="btn btn-primary" id="addShippingAddress" onclick="addModal();">Add</i></a>
				</header>
				<!--
				<div class="form-group">
			      <form>
					<div class="col-lg-12">
						<table class="table table-striped table-advance table-hover" id="shippingTable">
							
						</table>
					</div>
				</div>
			</form>
			-->
			<div style="position: relative">
						<div style="width: 100%;">
							<br />
							<br />
							<div class="grid-header" style="width: 100%">
								<label>Customer Shipping/Other Addresses</label> <!-- <span id="shipping_grid_toogle_search" style="float: right"
									class="ui-icon ui-icon-search" title="Toggle search panel"
									onclick="shippingToggleFilterRow()"></span> -->
							</div>
							<div id="custShippingGrid" style="width: 100%;height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
							<div id="shipping_pager" style="width: 100%; height: 20px;"></div>
						</div>
			</div>
			<div id="customer_inlineFilterPanel" style="display:none;background:#dddddd;padding:3px;color:black;">
				Show records with invoice including <input type="text" id="txtSearch2">
			</div>
					
				<!-- Toggle add shipping address form-->
			<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog"
				tabindex="-1" id="myModal-2" class="modal fade">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button aria-hidden="true" data-dismiss="modal" class="close"
								type="button">Close</button>
							<h4 class="modal-title">Add Shipping/Other Address</h4>
						</div>
						<div class="panel-body">
							<div class="modal-body">
							 
							 	<input id="action" value="addShippingAddress" name="action" type="hidden" />
							 	<input type="hidden" name="shippingAddrId"  id="shippingAddrId" value=""/>
							 	<input class="form-control" id="custId" value="${customerId}" name="custId" type="hidden" />
								<input type="hidden" name="count" id="count" value="${count}" />
								<div class="form-group">
									<div class="col-sm-5">
										<label>First Name <span class="required">*</span>
										</label> <input class="form-control" id="shFirstName" name="shFirstName"
											type="text" value="" />
									</div>
									<div class="col-sm-5">
										<label>Last Name <span class="required">*</span>
										</label> <input class="form-control" id="shLastName" name="shLastName"
											type="text" value="" />
									</div>
									<div class="col-sm-5">
											<label>Email <span class="required">*</span></label> <input
												class="form-control" id="shEmail" name="shEmail"
												type="text" value="" />
												<input type="checkbox" id="sameShippingEmail">Same as Primary Email
									</div>
									<div class="col-sm-5">
										<label>Phone <span class="required">*</span></label> <input
											class="form-control" id="shPhone" name="shPhone"
											type="text" value="" />
											<input type="checkbox" id="sameShippingPhone">Same as Primary Phone
									</div>
									<div class="row"></div>
									<div class="col-lg-12">&nbsp;</div>
									<div class="col-sm-5">
										<label>Street1 <span class="required">*</span>
										</label> <input class="form-control" id="shAddressLine1" type="text"
											name="shAddressLine1" value="" />
									</div>
									<div class="col-sm-5">
										<label>Street2</label> <input class="form-control"
											id="shAddressLine2" type="text" name="shAddressLine2"
											value="" />
									</div>
										<div class="row"></div>
										<div class="col-lg-12">&nbsp;</div>
									<div class="col-sm-5" style="float: left;">
										<label>Country <span class="required">*</span>
										</label> <select id="shCountryName" name="shCountryName"
											class="form-control input-sm m-bot15" onchange="loadState('shStateName','')">
											<option value="-1">--select--</option>
											<c:forEach var="country" items="${countries}">
												<option value="${country.id}">
													<c:out value="${country.name}" />
												</option>

											</c:forEach>
										</select>
									</div>
									<div class="col-sm-5">
										<label>State <span class="required">*</span>
										</label> <select id="shStateName" name="shStateName"
											class="form-control input-sm m-bot15" onchange="">
											<option value="-1">--select--</option>
											<%-- <c:forEach var="state" items="${states}">
												<option value="${state.id}">
													<c:out value="${state.name}" />
												</option>
											</c:forEach> --%>
										</select>
									</div>
										<div class="row"></div>
										<div class="col-lg-12">&nbsp;</div>
									<div class="col-sm-5" style="float: left;">
										<label>City <span class="required">*</span>
										</label> <input class="form-control" id="shCity" type="text" name="shCity"
											value="" />
									</div>
									<div class="col-sm-5">
										<label>Zip Code <span class="required">*</span>
										</label> <input class="form-control" id="shZipCode" type="text"
											name="shZipCode" value="" />
									</div>
								</div>
									<div class="row"></div>
									<div class="col-lg-12">&nbsp;</div>
								<div class="form-group">
									<div class="col-sm-6">
										<!--<button type="submit" class="btn btn-info pull-right">Submit</button>-->
										<button class="btn btn-primary" type="button" onclick="saveCustomerInfo('shippingInfo')">Save</button>
										<button class="btn btn-default" onclick="cancelAction();"
											type="button">Cancel</button>
									</div>
								</div>
							</div>
							
						</div>
						
					</div>
				</div>
			</div>
				</div>
				<div id="invoice" class="tab-pane">
				<!-- Grid view for customers -->
					<div style="position: relative">
						<div style="width: 100%;">
							<br />
							<br />
							<div class="grid-header" style="width: 100%">
								<label>Customer Invoices</label>
								<a href="javascript:invoiceResetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters</a>
							</div>
							<div id="custInvoiceGrid" style="width: 100%;height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
							<div id="pager" style="width: 100%; height: 20px;"></div>
						</div>
					</div>					
				</div>
				
				<div id="po" class="tab-pane">
				<!-- Grid view for customers -->
					<div style="position: relative">
						<div style="width: 100%;">
							<br />
							<br />
							<div class="grid-header" style="width: 100%">
								<label>Customer Purchase Order</label>
								<a href="javascript:poResetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters</a>
							</div>
							<div id="poGrid" style="width: 100%;height:200px;border-right:1px solid gray;border-left:1px solid gray"></div>
							<div id="poPager" style="width: 100%; height: 20px;"></div>
						</div>
					</div>
				</div>
				
					<div id="rewardPoints" class="tab-pane">
						<div style="position: relative">
						<div style="width: 100%;">
							<div class="row" align="center">						
								<div class="form-group">	
									<div class="col-sm-2"> &nbsp; </div>
									<div class="col-sm-1">
										<label for="name" class="control-label">Reward Points</label>
									</div>
									<div class="col-sm-2">
										<input class="form-control" id="newRewardPoints" name="newRewardPoints" />
									</div>
									<div class="col-sm-2">					
										<button class="btn btn-primary" type="button" onclick="updateRewardPoints()">Save Points</button>
									</div>
									<div class="col-sm-5">
									&nbsp;</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-12">&nbsp;</div>
					<br/>
				<div style="position: relative">
					<div style="width: 100%;">
						<section class="panel"> <header style="font-size: 13px;font-family: arial;" class="panel-heading">
		Customer Reward Points </header>
		<div class="panel-body">
			<div class="form-group">
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Current Reward Point Balance: </label>
					<span style="font-size: 12px;" id=rewardPointBalance></span>
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Available Reward Points: </label>
					<span id=actPoints></span>
				</div>
				<div class="col-sm-3"> 
					<label style="font-size: 13px;font-weight:bold;">Pending Reward Points: </label>
					<span id=pendPoints></span>
				</div>
				<!-- <div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Latest Earned Points: </label>
					<span id=latEarnPoints></span>
				</div>-->
				<div class="col-sm-1">
					<label style="font-size: 13px;font-weight:bold;">&nbsp; </label>
					<span></span>
                </div>
			</div>
			
			<div class="form-group">
				<!-- <div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Latest Spent Points: </label>
					<span id=latSpentPoints></span>
				</div> -->
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Total Lifetime Earned Points: </label>
					<span style="font-size: 12px;" id="totEarnPoints"></span> &nbsp; 
						<img src="../resources/images/icons/info.png" onclick="openPointsHistory('Earned Points');" height="15" width="15" />					
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Total Points Redeemed to Date: </label>
					<span id=totSpentPoints></span> &nbsp; 
						<img src="../resources/images/icons/info.png" onclick="openPointsHistory('Redeemed Points');" height="15" width="15" />
				</div>				
				<div class="col-sm-3"> 
					<label style="font-size: 13px;font-weight:bold;">Voided Points To Date: </label>
					<span id=voidPoints></span>
				</div>
				<div class="col-sm-1">
					<label style="font-size: 13px;font-weight:bold;">&nbsp; </label>
					<span></span>
                </div>
			</div>
			
		</div>
		</section>			
					</div>
				</div>
			</div>
			<div id="credit" class="tab-pane">
				<div style="position: relative">
					<div style="width: 100%;">
						<section class="panel"> <header style="font-size: 13px;font-family: arial;" class="panel-heading">
		Wallet Credit Details </header>
		<div class="panel-body">
			<div class="form-group">
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Active Credit : </label>
					<span style="font-size: 12px;" id="activeCredit"></span>
					
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Total Earned Credit : </label>
					<span id="totalEarnedCredit"></span>
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Total Used Credit : </label>
					<span id="totalUsedCredit"></span>
				</div>
			</div>
		</div>
		</section>			
	</div>
</div>
</div>
				<div id="notes" class="tab-pane">
					<input type="hidden" name="action" value="action" id="saveNote"/>
					<input class="form-control" id="custId" value="${customerId}" name="custId" type="hidden" />
					<span style="font-size: 13px;font-family: arial; float:left;">Add a new note</span>
					<br/><br/>
					<div class="container">
					  <textarea name="notes" id="notesTxt" class="form-control noresize" rows="8"></textarea>
					</div>
					<div class="col-lg-12">&nbsp;</div>
					<div class="row" align="center">
						<div class="col-lg-12">
							<div class="form-group">
								<button class="btn btn-primary" type="button" onclick="saveCustomerInfo('notes');">Save Note</button>
							</div>
						</div>
					</div>
				</div>
				<div id="creditCard" class="tab-pane">
					<div style="position: relative">
						<div style="width: 100%;">
							<br />
							<br />
							<div class="grid-header" style="width: 100%">
								<label>Customer Credit Cards</label> <span style="float: right"
									class="ui-icon ui-icon-search" title="Toggle search panel"
									onclick="creditCardToggleFilterRow()"></span>
							</div>
							<div id="custCreditCardGrid" style="width: 100%;height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
							<div id="creditCard_pager" style="width: 100%; height: 20px;"></div>
						</div>
					</div>
					<div id="customer_inlineFilterPanel" style="display:none;background:#dddddd;padding:3px;color:black;">
  					Show records with Credit Card including <input type="text" id="txtSearch2">
  					</div>
				</div>
				<div id="loyalFan" class="tab-pane">
					<div id="loyalFanDiv">
						<div style="position: relative">
							<div style="width: 100%;">
								<section class="panel"> 
								<header style="font-size: 13px;font-family: arial;" class="panel-heading">Artist Details </header>
								<div class="panel-body">
									<div class="form-group">
										<div class="col-sm-4">
											<label style="font-size: 13px;font-weight:bold;">Artist Name : </label>
											<span style="font-size: 12px;" id="loyalArtistName"></span>											
										</div>
										<!-- <div class="col-sm-4">
											<label style="font-size: 13px;font-weight:bold;">Category Name : </label>
											<span id="loyalCategoryName"></span>
										</div> -->
										<div class="col-sm-4">
											<label style="font-size: 13px;font-weight:bold;">Tickets Purchased : </label>
											<span id="loyalTicketsPurchased"></span>
										</div>
										<div class="col-sm-4">
											<label style="font-size: 13px;font-weight:bold;">Start Date : </label>
											<span style="font-size: 12px;" id="loyalStartDate"></span>											
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-4">
											<label style="font-size: 13px;font-weight:bold;">End Date : </label>
											<span id="loyalEndDate"></span>
										</div>
										<div class="col-sm-4">
											&nbsp;
										</div>
										<div class="col-sm-4">
											&nbsp;
										</div>
									</div>
								</div>
								</section>			
							</div>
						</div>
					</div><br/>	
					<div style="position: relative">
						<div style="width: 100%;">
							<div class="grid-header" style="width: 100%">
								<label>Artist Details</label>
								<a href="javascript:loyalFanResetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters</a>
							</div>
							<div id="artist_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
							<div id="artist_pager" style="width: 100%; height: 10px;"></div>
						</div>
					</div>
					<br/>
					<div class="col-lg-12">&nbsp;</div>
					<div style="position: relative">
						<div class="col-lg-12">
							<div class="form-group">						
								<button class="btn btn-primary" type="button" onclick="saveCustomerInfo('loyalFan');">Save Artist</button>
							</div>
						</div>
					</div>
				</div>
				
				<div id="favouriteEvent" class="tab-pane">
					<div style="position: relative">
						<div style="width: 100%;">
							<div class="grid-header" style="width: 100%">
								<label>Event Details</label>
								<a href="javascript:eventResetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters</a>
							</div>
							<div id="event_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
							<div id="event_pager" style="width: 100%; height: 10px;"></div>
						</div>
					</div>
					<br/>
					<div class="col-lg-12">&nbsp;</div>
					<div style="position: relative">
						<div class="col-lg-12">
							<div class="form-group">						
								<button class="btn btn-primary" type="button" onclick="saveCustomerInfo('favouriteEvent');">Save Events</button>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
		</section>
	</div>
</div>
</div>		
<script>
//function for deleting shipping address functionality
function deleteFormatter(row,cell,value,columnDef,dataContext){  
   /*  var button = "<input class='del' value='Delete' type='button' id='"+ dataContext.id +"' />"; */
    var button = "<img class='deleteClickableImage' src='../resources/images/ico-delete.gif' id='"+ dataContext.shippingId +"'/>";
    //the id is so that you can identify the row when the particular button is clicked
    return button;
    //Now the row will display your button
}

//function for edit functionality
function editFormatter(row,cell,value,columnDef,dataContext){
	//the id is so that you can identify the row when the particular button is clicked
	var button = "<img class='editClickableImage' src='../resources/js/slick/images/pencil.gif' id='"+ dataContext.shippingId +"'/>";
	return button;
}

//Function to hook up the edit button event for Shipping Address
$('.editClickableImage').live('click', function(){
	var me = $(this), id = me.attr('id');
	editModal(id);
});

//Now you can use jquery to hook up your delete button event for Shipping Address
$('.deleteClickableImage').live('click', function(){
    var me = $(this), id = me.attr('id');
    var delFlag = deleteShippingAddress(id);//confirm("Are you sure,Do you want to Delete it?");
});

function getCustomerInfoForEdit(customerId){
	$.ajax({
			url : "${pageContext.request.contextPath}/Client/GetCustomerInfo",
			type : "post",
			data:"custId="+customerId,
			success : function(response){
				var data = JSON.parse(JSON.stringify(response));
				showCustomerInfo(data);
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
	});
}

var invoiceGridSearchString='';
var invoiceGridColumnFilters = {};
var invoicePagingInfo;
var poPagingInfo;
var pagingInfo;
var cardPagingInfo;
function getCustomerInfoForInvoice(){
	var customrId = '${customerId}';
	$.ajax({
		url : "${pageContext.request.contextPath}/Client/GetCustomerInfoForInvoice",
		type : "post",
		data:"custId="+customrId+"&headerFilter="+invoiceGridSearchString,
		success : function(response){
			var data = JSON.parse(JSON.stringify(response));
			if(data.invoiceInfo !=null && data.invoiceInfo != '' && data.invoiceInfo!=undefined){
				invoiceInfo = data.invoiceInfo;	
			}else{
				invoiceInfo =[];
			}
			invoicePagingInfo = data.invoicePagingInfo;
			createInvoiceGrid(invoiceInfo);
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}
function getCustomerInfoForPO(){
	var customrId = '${customerId}';
	$.ajax({
		url : "${pageContext.request.contextPath}/Client/GetCustomerInfoForPO",
		type : "post",
		data:"custId="+customrId+"&headerFilter="+poGridSearchString,
		success : function(response){
			var data = JSON.parse(JSON.stringify(response));
			if(data.poInfo !=null && data.poInfo != '' && data.poInfo!=undefined){
				poInfo = data.poInfo;
			}else{
				poInfo=[];
			}
			poPagingInfo = data.poPagingInfo;
			createPOGrid(poInfo);
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function showCustomerInfo(data){
	$('#editCustomerDiv').show();
	$(".nav-tabs li").removeClass("active");
	$(".tab-content div").removeClass("active");
	$(window).scrollTop($('#topLevelInfo').offset().top);
	$('#defaultTab').addClass('active');
	$('#topLevelInfo').addClass('active');

	
	//fill basic info
	fillBasicInfo(data.customerInfo);

	if(data.states!=null && data.states != '' && data.states!=undefined){
		updateStateCombo(data.states,'stateName',data.billingInfo.state);
	}
	
	//billing info
	fillBillingInfo(data.billingInfo);

	if(data.shippingInfo !=null && data.shippingInfo != '' && data.shippingInfo!=undefined){
		shippingInfo = data.shippingInfo;
	}else{
		shippingInfo =[];
	}
	
	//shipping info
	if(data.shippingPagingInfo!=null && data.shippingPagingInfo!=undefined){
		pagingInfo = data.shippingPagingInfo;
	}else{
		pagingInfo = {};
	}
	fillShippingInfo(data.shippingInfo,data.shippingCount);
	
	
	if(data.cardInfo !=null && data.cardInfo != '' && data.cardInfo!=undefined){
		cardInfo = data.cardInfo;
	}else{
		cardInfo =[];
	}
	if(data.cardPagingInfo !=null && data.cardPagingInfo != '' && data.cardPagingInfo!=undefined){
		cardPagingInfo = data.cardPagingInfo;
	}else{
		cardPagingInfo =[];
	}
	
	fillCreditInfo(data.creditInfo);
	
	//reward points info
	fillRewardPointsInfo(data.rewardPointsInfo);
	
	fillLoyalFanInfo(data.loyalFanInfo);
}

function fillBasicInfo(customerInfo){
	$('#productType').val(customerInfo.productType);
	$('#custId').val(customerInfo.id);
	$('#customerType').val(customerInfo.type);
	$('#customerLevel').val(customerInfo.level);
	$('#clientBrokerType').val(customerInfo.clientBrokerType);
	$('#signupType').val(customerInfo.signupType);
	$('#name').val(customerInfo.name);
	$('#lastName').val(customerInfo.lastName);
	$('#email').val(customerInfo.email);
	$('#phone').val(customerInfo.phone);
	$('#fedexNo').val(customerInfo.ext);
	$('#otherPhone').val(customerInfo.otherPhone);
	$('#representativeName').val(customerInfo.representative);
	$('#notesTxt').val(customerInfo.notes);
}

function fillBillingInfo(billingInfo){
	$('#blFirstName').val(billingInfo.firstName);
	$('#blLastName').val(billingInfo.lastName);
	$('#blEmail').val(billingInfo.email);
	$('#blPhone').val(billingInfo.phone);
	$('#street1').val(billingInfo.street1);
	$('#street2').val(billingInfo.street2);
	//$('#countryName').val(billingInfo.country);
	$("#countryName option:contains(" + billingInfo.country + ")").attr('selected', 'selected');
	//$('#stateName').val(billingInfo.state);
	$("#stateName option:contains(" + billingInfo.state + ")").attr('selected', 'selected');
	$('#city').val(billingInfo.city);
	$('#zipCode').val(billingInfo.zip);
}	

function fillShippingInfo(shippingInfo,shippingCount){
		//$('#shippingTable').empty();
		$('#count').val(0);
		/*var template='<tbody>'+
			'<tr>'+
				'<th><i class=""></i> First Name</th>'+
				'<th><i class=""></i> Last Name</th>'+
				'<th><i class=""></i> Street1</th>'+
				'<th><i class=""></i> Street2</th>'+
				'<th><i class=""></i> Country</th>'+
				'<th><i class=""></i> State</th>'+
				'<th><i class=""></i> City</th>'+
				'<th><i class=""></i> Zip Code</th>'+
				'<th><i class=""></i> Address Type</th>'+
				'<th><i class="icon_cogs"></i> Action</th>'+
			'</tr>'+
							
		'</tbody>';*/
		if(shippingInfo!=null && shippingInfo!='' && shippingInfo.length>0){
		if(shippingCount!=null && shippingCount!=''){
			$('#count').val(shippingCount);
		}
		}
		/*
		for(var i=0;i<shippingInfo.length;i++){
			template += '<tr id="'+shippingInfo[i].id+'">'+
			'<input type="hidden" name="shippingAddressId" value="'+shippingInfo[i].id+'"/>'+
			'<td id="firstName_'+shippingInfo[i].id+'">'+shippingInfo[i].firstName+'</td>'+
			'<td id="lastName_'+shippingInfo[i].id+'">'+shippingInfo[i].lastName+'</td>'+
			'<td id="street1_'+shippingInfo[i].id+'">'+shippingInfo[i].street1+'</td>'+
			'<td id="street2_'+shippingInfo[i].id+'">'+shippingInfo[i].street2+'</td>'+
			'<td id="country_'+shippingInfo[i].id+'">'+shippingInfo[i].country+'</td>'+
			'<td id="state_'+shippingInfo[i].id+'">'+shippingInfo[i].state+'</td>'+
			'<td id="city_'+shippingInfo[i].id+'">'+shippingInfo[i].city+'</td>'+
			'<td id="zipCode_'+shippingInfo[i].id+'">'+shippingInfo[i].zip+'</td>'+
			'<td id="addressType_'+shippingInfo[i].id+'">'+shippingInfo[i].addressType+'</td>'+
			'<td>'+
				'<div class="btn-group">'+
					'<a class="btn btn-success" id="edit" onclick="editModal('+shippingInfo[i].id+');"><i class="icon_check_alt2"></i> </a> '+ 
					'<a class="btn btn-danger" id="delete" onclick="deleteShippingAddress('+shippingInfo[i].id+');"><i class="icon_close_alt2"></i> </a>'+
				'</div>'+
			'</td>'+
			'</tr>';
		}
	$('#shippingTable').append(template);*/
}
function fillCreditInfo(customerCredit){
	$('#activeCredit').html(customerCredit.activeCredit);
	$('#totalEarnedCredit').html(customerCredit.totalEarnedCredit);
	$('#totalUsedCredit').html(customerCredit.totalUsedCredit);
}

function fillRewardPointsInfo(rewardPointsInfo) {		
	$('#totEarnPoints').html(rewardPointsInfo.totalEarnedPoints);
	$('#totSpentPoints').html(rewardPointsInfo.totalSpentPoints);
	$('#latEarnPoints').html(rewardPointsInfo.latestEarnedPoints);
	$('#latSpentPoints').html(rewardPointsInfo.latestSpentPoints);
	$('#rewardPointBalance').html((parseFloat(rewardPointsInfo.activePoints) + parseFloat(rewardPointsInfo.pendingPoints)).toFixed(2));
	$('#actPoints').html(rewardPointsInfo.activePoints);
	$('#pendPoints').html(rewardPointsInfo.pendingPoints);
	$('#voidPoints').html(rewardPointsInfo.voidedPoints);
}

function fillLoyalFanInfo(loyalFanInfo){	
	if(loyalFanInfo != null && loyalFanInfo != ''){
		if(loyalFanInfo.artistName != null){
			$('#loyalArtistName').html(loyalFanInfo.artistName);
			$('#loyalTicketsPurchased').html(loyalFanInfo.ticketsPurchased);
			$('#loyalStartDate').html(loyalFanInfo.startDate);
			$('#loyalEndDate').html(loyalFanInfo.endDate);
			$('#loyalFanDiv').show();
		}else{
			$('#loyalFanDiv').hide();
		}
	}
}

/* function deleteCustomer(userId){
	if(userId == ''){
		jAlert("Userid is not found please refresh page and try again.");
		return false;
	}
	jConfirm("Are you sure to delete an customer ?","Confrim",function(r){
		if(r){
			$.ajax({
				url : "${pageContext.request.contextPath}/DeleteCustomer",
				type : "post",
				data : "userName="+ $("#userName").val() + "&userId=" + userId,
				success : function(response){
					if(response != "" && response!=null){
						jAlert("Customer deleted successfully.");
						refreshCustomerGridValues(null,false,true,response);
						return true;
					}else{
						jAlert(response);
						return false;
					}
				},
				error : function(error){
					jAlert("There is something wrong. Please try again"+error,"Error");
					return false;
				}
			});
		} else {
			return false;
		}
	});
	return false;
} */

//show the pop window center
function popupCenter(url, title, w, h) {  
    // Fixes dual-screen position                         Most browsers      Firefox  
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;  
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;  
              
    width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;  
    height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;  
              
    var left = ((width / 2) - (w / 2)) + dualScreenLeft;  
    var top = ((height / 2) - (h / 2)) + dualScreenTop;  
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);  
  
    // Puts focus on the newWindow  
    if (window.focus) {  
        newWindow.focus();  
    }  
}  

var shippingGrid;
var shippingDataView;
var shippingData=[];
var shippingColumn = [
              /*  {id:"id", name:"ID", field: "id", sortable: true}, */
			   {id:"shippingId", name:"Shipping ID", field: "shippingId", sortable: true},
               {id:"firstname", name:"First Name", field: "firstname", sortable: true},
               {id:"lastname", name:"Last Name", field: "lastname", sortable: true},
               {id:"email", name:"Email", field: "email", sortable: true},
               {id:"phone", name:"Phone", field: "phone", sortable: true},
               {id:"street1", name:"Street1", field: "street1", sortable: true},
               {id:"street2", name:"Street2", field: "street2", sortable: true},
               {id:"country", name:"Country", field: "country", sortable: true},
               {id:"state", name:"State", field: "state", sortable: true},
               {id:"city", name:"City", field: "city", sortable: true},
               {id:"zipcode", name:"Zip Code", field: "zipcode", sortable: true},
               {id:"address", name:"Address Type", field: "address", sortable: true},
			   {id: "editCol", field:"editCol", name:"Edit", width:20, formatter:editFormatter},
               {id:"delCol", name:"Delete", field: "delCol", width:10, formatter:deleteFormatter}
              ];
              
var shippingOptions = {		  
	//editable: true,
	//enableAddRow: true,
	enableCellNavigation : true,
	//asyncEditorLoading: true,
	forceFitColumns : true,
	topPanelHeight : 25,
	//autoHeight: true
};

var sortcol = "shippingId";
var sortdir = 1;
var percentCompleteThreshold = 0;
var searchString = "";

//filter option
function myFilter(item, args) {
	var x= item["shippingId"];
	if (args.searchString  != ""
			&& x.indexOf(args.searchString ) == -1) {
		
		if (typeof x === 'string' || x instanceof String) {
			if(x.toLowerCase().indexOf(args.searchString.toLowerCase()) == -1) {
				return false;
			}
		} else {
			return false;
		}
	}
	return true;
}


function comparer(a, b) {
	  var x = a[sortcol], y = b[sortcol];
	  if(!isNaN(x)){
	  	return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	  }
	  if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
function shippingToggleFilterRow() {
	shippingGrid.setTopPanelVisibility(!shippingGrid.getOptions().showTopPanel);
}
	
//$(".grid-header .ui-icon-sg").addClass("ui-state-default-sg ui-corner-all").mouseover(function(e) {
	$("#shipping_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all").mouseover(function(e) {
	$(e.target).addClass("ui-state-hover")}).mouseout(function(e) {
		$(e.target).removeClass("ui-state-hover")
});
 
function createShippingGrid(jsonData) {
	var shippingData=[];
	for (var i = 0; i < jsonData.length; i++) {
		var  data= jsonData[i];
		var d = (shippingData[i] = {});
		d["id"] = data.id;
		d["shippingId"] = data.id;
		d["firstname"] = data.firstName;
		d["lastname"] = data.lastName;
		d["email"] = data.email;
		d["phone"] = data.phone;
		d["street1"] = data.street1;
		d["street2"] = data.street2;
		d["country"] = data.country;
		d["state"] = data.state;
		d["city"] = data.city;
		d["zipcode"] = data.zip;
		d["address"] = data.addressType;
	}
	
	shippingDataView = new Slick.Data.DataView({inlineFilters: true });
	shippingGrid = new Slick.Grid("#custShippingGrid", shippingDataView, shippingColumn, shippingOptions);
	shippingGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	shippingGrid.setSelectionModel(new Slick.RowSelectionModel());
	if(pagingInfo!=null){
		var shippingGridPager = new Slick.Controls.Pager(shippingDataView, shippingGrid, $("#shipping_pager"),pagingInfo);
	}
	var columnpicker = new Slick.Controls.ColumnPicker(shippingColumn, shippingGrid, shippingOptions);

	  
	  // move the filter panel defined in a hidden div into shippingGrid top panel
	  $("#inlineFilterPanel")
	      .appendTo(shippingGrid.getTopPanel())
	      .show();
	  
	  shippingGrid.onKeyDown.subscribe(function (e) {
	    // select all rows on ctrl-a
	    if (e.which != 65 || !e.ctrlKey) {
	      return false;
	    }
	    var rows = [];
	    for (var i = 0; i < shippingDataView.getLength(); i++) {
	      rows.push(i);
	    }
	    shippingGrid.setSelectedRows(rows);
	    e.preventDefault();
	  });
	  shippingGrid.onSort.subscribe(function (e, args) {
	    sortdir = args.sortAsc ? 1 : -1;
	    sortcol = args.sortCol.field;
	    if ($.browser.msie && $.browser.version <= 8) {
	      // using temporary Object.prototype.toString override
	      // more limited and does lexicographic sort only by default, but can be much faster
	      // use numeric sort of % and lexicographic for everything else
	      shippingDataView.fastSort(sortcol, args.sortAsc);
	    } else {
	      // using native sort with comparer
	      // preferred method but can be very slow in IE with huge datasets
	      shippingDataView.sort(comparer, args.sortAsc);
	    }
	  });
	  // wire up model events to drive the shippingGrid
	  shippingDataView.onRowCountChanged.subscribe(function (e, args) {
	    shippingGrid.updateRowCount();
	    shippingGrid.render();
	  });
	  shippingDataView.onRowsChanged.subscribe(function (e, args) {
	    shippingGrid.invalidateRows(args.rows);
	    shippingGrid.render();
	  });
	  
	  // wire up the search textbox to apply the filter to the model
	  $("#txtSearch,#txtSearch2").keyup(function (e) {
	    Slick.GlobalEditorLock.cancelCurrentEdit();
	    // clear on Esc
	    if (e.which == 27) {
	      this.value = "";
	    }
	    searchString = this.value;
	    updateFilter();
	  });
	  function updateFilter() {
	    shippingDataView.setFilterArgs({
	      searchString: searchString
	    });
	    shippingDataView.refresh();
	  }
	  
	  shippingDataView.beginUpdate();
	  shippingDataView.setItems(shippingData);
	  shippingDataView.setFilterArgs({
	    percentCompleteThreshold: percentCompleteThreshold,
	    searchString: searchString
	  });
	  shippingDataView.setFilter(myFilter);
	  shippingDataView.endUpdate();
	  // if you don't want the items that are not visible (due to being filtered out
	  // or being on a different page) to stay selected, pass 'false' to the second arg
	  shippingDataView.syncGridSelection(shippingGrid, true);
	  shippingDataView.refresh();
	  $("#gridContainer").resizable();
}	
/*-----------*/

	var poGridSearchString = "";
	var poGridColumnFilters = {};
	var poDataView;
	var poGrid;
	var poData = [];
	var poColumns = [ {
		id : "id",
		name : "PO ID",
		field : "id",
		sortable : true
	}, {
		id : "customerName",
		name : "Customer",
		field : "customerName"
	}, {
		id : "customerType",
		name : "Customer Type",
		field : "customerType",
		sortable : true
	}, {
		id : "poTotal",
		name : "Grand Total",
		field : "poTotal",
		sortable : true
	},{
		id : "ticketQty",
		name : "Ticket Qty",
		field : "ticketQty",
		sortable : true
	},{
		id : "created",
		name : "Created",
		field : "created",
		sortable : true
	}, {
		id : "shippingType",
		name : "Shipping Type",
		field : "shippingType",
		sortable : true
	}, {
		id : "productType",
		name : "Product Type",
		field : "productType",
		sortable : true
	}, {
		id: "status",
		name: "Status", 
		field: "status", 
		sortable: true
	} ];

	var poOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var poGridSortcol = "customerName";
	var poGridSortdir = 1;
	var percentCompleteThreshold = 0;
	
	function deleteRecordFrompoGrid(id) {
		poDataView.deleteItem(id);
		poGrid.invalidate();
	}
	/*
	function poGridFilter(item, args) {
		var x= item["customerName"];
		if (args.poGridSearchString != ""
				&& x.indexOf(args.poGridSearchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.poGridSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}
	*/
	function poGridComparer(a, b) {
		var x = a[poGridSortcol], y = b[poGridSortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	/*
	function poToggleFilterRow() {
		poGrid.setTopPanelVisibility(!poGrid.getOptions().showTopPanel);
	}
	
	//$(".grid-header .ui-icon").addClass("ui-state-default-sg ui-corner-all")
	$("#event_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all")
			.mouseover(function(e) {
				$(e.target).addClass("ui-state-hover")
			}).mouseout(function(e) {
				$(e.target).removeClass("ui-state-hover")
			});
	*/
function createPOGrid(jsonData) {
		var i = 0;
		poData=[];
		for(var i=0;i<jsonData.length;i++){
			var  data= jsonData[i];
			var d = (poData[i] = {});
			d["id"] = data.poId;	  		
			d["customerName"] = data.customerName;
			d["customerType"] = data.customerType;
			d["poTotal"] = data.total;
			d["ticketQty"] = data.ticketQty;
			d["created"] = data.created;
			d["shippingType"] = data.shippingType;
			d["productType"] = data.productType;
			d["status"] = data.status;
		}

		poDataView = new Slick.Data.DataView();
		poGrid = new Slick.Grid("#poGrid", poDataView, poColumns, poOptions);
		poGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		poGrid.setSelectionModel(new Slick.RowSelectionModel());
		if(poPagingInfo!=null){
			var poGridPager = new Slick.Controls.Pager(poDataView, poGrid, $("#poPager"),poPagingInfo);
		}
		var poGridColumnpicker = new Slick.Controls.ColumnPicker(poColumns, poGrid,
				poOptions);
		var cols = poGrid.getColumns();
	
		var colPO = [];
		for(var c=0;c<cols.length;c++) {
		  //if(cols[c].name!='Title' && cols[c].name!='Start') {
		 	 colPO.push(cols[c]);
		 // }  
		  poGrid.setColumns(colPO);
		}
		poGrid.invalidate();

		// move the filter panel defined in a hidden div into poGrid top panel
		//$("#event_inlineFilterPanel").appendTo(poGrid.getTopPanel()).show();

		poGrid.onSort.subscribe(function(e, args) {
			poGridSortdir = args.sortAsc ? 1 : -1;
			poGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				poDataView.fastSort(poGridSortcol, args.sortAsc);
			} else {
				poDataView.sort(poGridComparer, args.sortAsc);
			}
		});
		// wire up model events to drive the poGrid
		poDataView.onRowCountChanged.subscribe(function(e, args) {
			poGrid.updateRowCount();
			poGrid.render();
		});
		poDataView.onRowsChanged.subscribe(function(e, args) {
			poGrid.invalidateRows(args.rows);
			poGrid.render();
		});

		$(poGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	poGridSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				  poGridColumnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in poGridColumnFilters) {
					  if (columnId !== undefined && poGridColumnFilters[columnId] !== "") {
						 poGridSearchString += columnId + ":" +poGridColumnFilters[columnId]+",";
					  }
					}
					getCustomerInfoForPO();
				}
			  }		 
		});
		poGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id != 'customerName' && args.column.id != 'customerType'){
					if(args.column.id == 'created'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(poGridColumnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else{
						$("<input type='text'>")
					   .data("columnId", args.column.id)
					   .val(poGridColumnFilters[args.column.id])
					   .appendTo(args.node);
					}
				}
			}			
		});
		poGrid.init();
		/*
		// wire up the search textbox to apply the filter to the model
		$("#poGridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			poGridSearchString = this.value;
			updatepoGridFilter();
		});
		function updatepoGridFilter() {
			poDataView.setFilterArgs({
				poGridSearchString : poGridSearchString
			});
			poDataView.refresh();
		}
		*/
		// initialize the model after all the events have been hooked up
		poDataView.beginUpdate();
		poDataView.setItems(poData);
		/*poDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			poGridSearchString : poGridSearchString
		});
		poDataView.setFilter(poGridFilter);*/
		poDataView.endUpdate();	
		poDataView.syncGridSelection(poGrid, true);
		$("#gridContainer").resizable();
		
}
</script>
<script>
var creditCardGrid;
var creditCardDataView;
var creditCardData=[];
var creditCardColumn = [
              /*  {id:"id", name:"ID", field: "id", sortable: true}, */
               {id:"stripeCustomerId", name:"Stripe Customer Id", field: "stripeCustomerId", sortable: true},
               {id:"stripeCardId", name:"Stripe Card Id", field: "stripeCardId", sortable: true},
               {id:"cardType", name:"CardType", field: "cardType", sortable: true},
               {id:"cardLastFourDigit", name:"Card Last Four Digit", field: "cardLastFourDigit", sortable: true},
               {id:"expiryMonth", name:"Expiry Month", field: "expiryMonth", sortable: true},
               {id:"expiryYear", name:"Expiry Year", field: "expiryYear", sortable: true},
               {id:"status", name:"Status", field: "status", sortable: true}
              ];
              
var creditCardOptions = {		  
	//editable: true,
	//enableAddRow: true,
	enableCellNavigation : true,
	//asyncEditorLoading: true,
	forceFitColumns : true,
	topPanelHeight : 25,
	//autoHeight: true
};

var sortcol = "id";
var sortdir = 1;
var percentCompleteThreshold = 0;
var searchString = "";

//filter option
function myFilter(item, args) {
	var x= item["id"];
	if (args.searchString  != ""
			&& x.indexOf(args.searchString ) == -1) {
		
		if (typeof x === 'string' || x instanceof String) {
			if(x.toLowerCase().indexOf(args.searchString.toLowerCase()) == -1) {
				return false;
			}
		} else {
			return false;
		}
	}
	return true;
}


function comparer(a, b) {
	  var x = a[sortcol], y = b[sortcol];
	  if(!isNaN(x)){
	  	return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	  }
	  if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
function creditCardToggleFilterRow() {
	creditCardGrid.setTopPanelVisibility(!creditCardGrid.getOptions().showTopPanel);
}
	
$(".grid-header .ui-icon-sg").addClass("ui-state-default-sg ui-corner-all").mouseover(function(e) {
	$(e.target).addClass("ui-state-hover")}).mouseout(function(e) {
		$(e.target).removeClass("ui-state-hover")
});
 
function createCreditCardGrid(jsonData) {
	var creditCardData=[];
	for (var i = 0; i < jsonData.length; i++) {
		var  data= jsonData[i];
		var d = (creditCardData[i] = {});
		d["id"] = data.id;
		d["stripeCustomerId"] = data.stripeCustomerId;
		d["stripeCardId"] = data.stripeCardId;
		d["cardType"] = data.cardType;
		d["cardLastFourDigit"] = data.cardLastFourDigit;
		d["expiryMonth"] = data.expiryMonth;
		d["expiryYear"] = data.expiryYear;
		d["status"] = data.status;
	}
	
	creditCardDataView = new Slick.Data.DataView({inlineFilters: true });
	creditCardGrid = new Slick.Grid("#custCreditCardGrid", creditCardDataView, creditCardColumn, creditCardOptions);
	creditCardGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	creditCardGrid.setSelectionModel(new Slick.RowSelectionModel());
	if(cardPagingInfo!=null){
		var creditCardGridPager = new Slick.Controls.Pager(creditCardDataView, creditCardGrid, $("#creditCard_pager"),cardPagingInfo);
	}
	var columnpicker = new Slick.Controls.ColumnPicker(creditCardColumn, creditCardGrid, creditCardOptions);

	  
	  // move the filter panel defined in a hidden div into creditCardGrid top panel
	  $("#inlineFilterPanel")
	      .appendTo(creditCardGrid.getTopPanel())
	      .show();
	  
	  creditCardGrid.onKeyDown.subscribe(function (e) {
	    // select all rows on ctrl-a
	    if (e.which != 65 || !e.ctrlKey) {
	      return false;
	    }
	    var rows = [];
	    for (var i = 0; i < creditCardDataView.getLength(); i++) {
	      rows.push(i);
	    }
	    creditCardGrid.setSelectedRows(rows);
	    e.preventDefault();
	  });
	  creditCardGrid.onSort.subscribe(function (e, args) {
	    sortdir = args.sortAsc ? 1 : -1;
	    sortcol = args.sortCol.field;
	    if ($.browser.msie && $.browser.version <= 8) {
	      // using temporary Object.prototype.toString override
	      // more limited and does lexicographic sort only by default, but can be much faster
	      // use numeric sort of % and lexicographic for everything else
	      creditCardDataView.fastSort(sortcol, args.sortAsc);
	    } else {
	      // using native sort with comparer
	      // preferred method but can be very slow in IE with huge datasets
	      creditCardDataView.sort(comparer, args.sortAsc);
	    }
	  });
	  // wire up model events to drive the creditCardGrid
	  creditCardDataView.onRowCountChanged.subscribe(function (e, args) {
	    creditCardGrid.updateRowCount();
	    creditCardGrid.render();
	  });
	  creditCardDataView.onRowsChanged.subscribe(function (e, args) {
	    creditCardGrid.invalidateRows(args.rows);
	    creditCardGrid.render();
	  });
	  
	  // wire up the search textbox to apply the filter to the model
	  $("#txtSearch,#txtSearch2").keyup(function (e) {
	    Slick.GlobalEditorLock.cancelCurrentEdit();
	    // clear on Esc
	    if (e.which == 27) {
	      this.value = "";
	    }
	    searchString = this.value;
	    updateFilter();
	  });
	  function updateFilter() {
	    creditCardDataView.setFilterArgs({
	      searchString: searchString
	    });
	    creditCardDataView.refresh();
	  }
	  
	  creditCardDataView.beginUpdate();
	  creditCardDataView.setItems(creditCardData);
	  creditCardDataView.setFilterArgs({
	    percentCompleteThreshold: percentCompleteThreshold,
	    searchString: searchString
	  });
	  creditCardDataView.setFilter(myFilter);
	  creditCardDataView.endUpdate();
	  // if you don't want the items that are not visible (due to being filtered out
	  // or being on a different page) to stay selected, pass 'false' to the second arg
	  creditCardDataView.syncGridSelection(creditCardGrid, true);
	  creditCardDataView.refresh();
	  $("#gridContainer").resizable();
}


var invoiceGrid;
var invoiceDataView;
var invoiceData=[];
var invoiceColumn = [
              /*  {id:"id", name:"ID", field: "id", sortable: true}, */
               {id:"invoiceId", name:"Invoice Id", field: "invoiceId", sortable: true},
               {id:"invoiceTotal", name:"Invoice Total", field: "invoiceTotal", sortable: true},
               {id:"createdDate", name:"Created Date", field: "createdDate", sortable: true},
               {id:"createdBy", name:"Created By", field: "createdBy", sortable: true},
               {id:"ticketCount", name:"Ticket Count", field: "ticketCount", sortable: true},
               {id:"productType", name:"Product Type", field: "productType", sortable: true},               
               /* {id:"extNo", name:"Ext PO", field: "extNo", sortable: true} */
               {id:"orderType", name:"Order Type", field: "orderType", sortable: true},
               {id:"viewTickets", name:"View Tickets", field: "viewTickets", formatter : viewLinkFormatter}
              ];
              
var invoiceOptions = {
	enableCellNavigation : true,
	forceFitColumns : true,
	topPanelHeight : 25,
	showHeaderRow: true,
	headerRowHeight: 30,
	explicitInitialization: true
};

var sortcol = "invoiceId";
var sortdir = 1;
var percentCompleteThreshold = 0;

/*
function myFilter(item, args) {
	var x= item["invoiceId"];
	if (args.searchString  != ""
			&& x.indexOf(args.searchString ) == -1) {
		
		if (typeof x === 'string' || x instanceof String) {
			if(x.toLowerCase().indexOf(args.searchString.toLowerCase()) == -1) {
				return false;
			}
		} else {
			return false;
		}
	}
	return true;
}
*/

function comparer(a, b) {
	  var x = a[sortcol], y = b[sortcol];
	  if(!isNaN(x)){
	  	return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	  }
	  if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	/*
function invoiceToggleFilterRow() {
	invoiceGrid.setTopPanelVisibility(!invoiceGrid.getOptions().showTopPanel);
}
	
$(".grid-header .ui-icon-sg").addClass("ui-state-default-sg ui-corner-all").mouseover(function(e) {
	$(e.target).addClass("ui-state-hover");
	}).mouseout(function(e) {
	$(e.target).removeClass("ui-state-hover");
});
 */
function createInvoiceGrid(jsonData) {
	var invoiceData=[];
	for (var i = 0; i < jsonData.length; i++) {
		var  data= jsonData[i];
		var d = (invoiceData[i] = {});
		d["id"] = i;
		d["invoiceId"] = data.invoiceId;
		d["invoiceTotal"] = data.invoiceTotal;
		d["createdDate"] = data.createdDate;
		d["createdBy"] = data.createdBy;
		d["ticketCount"] = data.ticketCount;
		d["productType"] = data.productType;
		d["orderType"] = data.orderType;
		//d["extNo"] = data.ext;
		d["viewTickets"] = data.status;
	}
	
	invoiceDataView = new Slick.Data.DataView();
	invoiceGrid = new Slick.Grid("#custInvoiceGrid", invoiceDataView, invoiceColumn, invoiceOptions);
	invoiceGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	invoiceGrid.setSelectionModel(new Slick.RowSelectionModel());
	if(invoicePagingInfo!=null){
		var invoiceGridPager = new Slick.Controls.Pager(invoiceDataView, invoiceGrid, $("#pager"),invoicePagingInfo);
	}
	var columnpicker = new Slick.Controls.ColumnPicker(invoiceColumn, invoiceGrid, invoiceOptions);

	  
	  // move the filter panel defined in a hidden div into invoiceGrid top panel
	  //$("#inlineFilterPanel").appendTo(invoiceGrid.getTopPanel()).show();
	  
	  invoiceGrid.onKeyDown.subscribe(function (e) {
	    // select all rows on ctrl-a
	    if (e.which != 65 || !e.ctrlKey) {
	      return false;
	    }
	    var rows = [];
	    for (var i = 0; i < invoiceDataView.getLength(); i++) {
	      rows.push(i);
	    }
	    invoiceGrid.setSelectedRows(rows);
	    e.preventDefault();
	  });
	  invoiceGrid.onSort.subscribe(function (e, args) {
	    sortdir = args.sortAsc ? 1 : -1;
	    sortcol = args.sortCol.field;
	    if ($.browser.msie && $.browser.version <= 8) {
	      invoiceDataView.fastSort(sortcol, args.sortAsc);
	    } else {
	      invoiceDataView.sort(comparer, args.sortAsc);
	    }
	  });
	  // wire up model events to drive the invoiceGrid
	  invoiceDataView.onRowCountChanged.subscribe(function (e, args) {
	    invoiceGrid.updateRowCount();
	    invoiceGrid.render();
	  });
	  invoiceDataView.onRowsChanged.subscribe(function (e, args) {
	    invoiceGrid.invalidateRows(args.rows);
	    invoiceGrid.render();
	  });
	  
	  $(invoiceGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	invoiceGridSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				  invoiceGridColumnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in invoiceGridColumnFilters) {
					  if (columnId !== undefined && invoiceGridColumnFilters[columnId] !== "") {
						 invoiceGridSearchString += columnId + ":" +invoiceGridColumnFilters[columnId]+",";
					  }
					}
					getCustomerInfoForInvoice();
				}
			  }
		 
		});
	    invoiceGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){			
				if(args.column.id == 'createdDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(invoiceGridColumnFilters[args.column.id])
				   .appendTo(args.node);
				} 
				else{
					$("<input type='text'>")
				   .data("columnId", args.column.id)
				   .val(invoiceGridColumnFilters[args.column.id])
				   .appendTo(args.node);
				}			
			}
		});
	    invoiceGrid.init();
	  /*
	  // wire up the search textbox to apply the filter to the model
	  $("#txtSearch,#txtSearch2").keyup(function (e) {
	    Slick.GlobalEditorLock.cancelCurrentEdit();
	    // clear on Esc
	    if (e.which == 27) {
	      this.value = "";
	    }
	    searchString = this.value;
	    updateFilter();
	  });
	  function updateFilter() {
	    invoiceDataView.setFilterArgs({
	      searchString: searchString
	    });
	    invoiceDataView.refresh();
	  }
	  */
	  invoiceDataView.beginUpdate();
	  invoiceDataView.setItems(invoiceData);
	  /*invoiceDataView.setFilterArgs({
	    percentCompleteThreshold: percentCompleteThreshold,
	    searchString: searchString
	  });
	  invoiceDataView.setFilter(myFilter);*/
	  invoiceDataView.endUpdate();
	  // if you don't want the items that are not visible (due to being filtered out
	  // or being on a different page) to stay selected, pass 'false' to the second arg
	  invoiceDataView.syncGridSelection(invoiceGrid, true);
	  invoiceDataView.refresh();
	  $("#gridContainer").resizable();
}	

function viewLinkFormatter(row, cell, value, columnDef, dataContext) {
	var downloadTicket = invoiceGrid.getDataItem(row).viewTickets;
	var link = '';
	if(downloadTicket == 'Yes'){
		link = "<img class='editClickImage' style='height:17px;' src='../resources/images/viewPdf.png' onclick='openInvoiceTicketPopup("+ invoiceGrid.getDataItem(row).invoiceId + ")'/>";
	}
	return link;
}

 function openInvoiceTicketPopup(invId){
		var url = "${pageContext.request.contextPath}/Client/GetCustomerInfoForInvoiceTickets?invoiceId="+ invId;
		popupCenter(url, "Invoice Tickets", "800", "500");
	}
 
	function deleteShippingAddress(shippingId) {		
		/* var productType = $('#productType').val();
		if(productType!='REWARDTHEFAN'){
			jAlert("Not Allowed to update Customer that belongs to other than REWARD THE FAN Product.");
			return;
		} */
		
		if (shippingId == '') {
			jAlert("Shippingid is not found please refresh page and try again.");
			return false;
		}
		jConfirm("Are you sure to delete an shipping address ?","Confirm",function(r){
			if (r) {
				$.ajax({
					url : "${pageContext.request.contextPath}/DeleteShippingAddress",
					type : "post",
					data : "userName=" + $("#email").val() + "&userId="+ shippingId,
					success : function(response) {
						if (response == "true") {
							getCustomerInfoForEdit(customerId);
							jAlert("Shipping address deleted successfully.");
						} else {
							jAlert(response);
							return false;
						}
					},
					error : function(error) {
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
				});
			} else {
				return false;
			}
		});
		return false;
	}

	//toggle the add shipping address modal to add
	function addModal(){
		var shippingAddressCount = $('#count').val();
		/* var productType = $('#productType').val();
		if(productType!='REWARDTHEFAN'){
			jAlert("Not Allowed to update Customer that belongs to other than REWARD THE FAN Product.");
			return;
		} */
		if(shippingAddressCount == 5){
			jAlert("You have reached the limit of adding shipping address");
		}else{
			$('#myModal-2').modal('show');
			$('#shFirstName').val('');
			$('#shLastName').val('');
			$('#shEmail').val('');
			$('#shPhone').val('');
			$('#shAddressLine1').val('');
			$('#shAddressLine2').val('');
			$('#shCountryName').val('');
			$('#shStateName').val('');
			$('#shCity').val('');
			$('#shZipCode').val('');
			$('#shippingAddrId').val('');
		}
	}
	
	//toggle the edit shipping address modal to edit
	function editModal(addrId){
		/* var productType = $('#productType').val();
		if(productType!='REWARDTHEFAN'){
			jAlert("Not Allowed to update Customer that belongs to other than REWARD THE FAN Product.");
			return;
		} */
		$('#myModal-2').modal('show');
		for (var i = 0; i < shippingInfo.length; i++) {
		var  data= shippingInfo[i];
		var d = (shippingData[i] = {});
		if(addrId == data.id){		
		$('#shFirstName').val(data.firstName);
		$('#shLastName').val(data.lastName);
		$('#shEmail').val(data.email);
		$('#shPhone').val(data.phone);
		$('#shAddressLine1').val(data.street1);
		$('#shAddressLine2').val(data.street2);
		$("#shCountryName option:contains(" + data.country + ")").attr('selected', 'selected');
		$('#shCity').val(data.city);
		$('#shZipCode').val(data.zip);
		$('#shippingAddrId').val(data.id);
		loadState('shStateName',data.state);
		}
	}
		/*
		$('#shFirstName').val($('#firstName_'+addrId).text());
		$('#shLastName').val($('#lastName_'+addrId).text());
		$('#shAddressLine1').val($('#street1_'+addrId).text());
		$('#shAddressLine2').val($('#street2_'+addrId).text());
		//$('#shCountryName').val($('#country_'+addrId).text());
		$("#shCountryName option:contains(" + $('#country_'+addrId).text() + ")").attr('selected', 'selected');
		//$('#shStateName').val($('#state_'+addrId).text());
		//$("#shStateName option:contains(" + $('#country_'+addrId).text() + ")").attr('selected', 'selected');
		$('#shCity').val($('#city_'+addrId).text());
		$('#shZipCode').val($('#zipCode_'+addrId).text());
		$('#shippingAddrId').val(addrId);
		loadState('shStateName',$('#state_'+addrId).text());
		*/
	}	

function saveCustomerInfo(actionType){
	/* var productType = $('#productType').val();
	if(productType!='REWARDTHEFAN'){
		jAlert("Not Allowed to update Customer that belongs to other than REWARD THE FAN Product.");
		return;
	} */
	var paramString = '';
	if(actionType == 'customerInfo'){
		if(validateCustomerInfo()){
			paramString = 'action=customerInfo&customerType='+$('#customerType').val()+'&customerLevel='+$('#customerLevel').val()+
						'&signupType='+$('#signupType').val()+'&name='+$('#name').val()+'&lastName='+$('#lastName').val()+'&email='+$('#email').val()+
						'&phone='+$('#phone').val()+'&extension='+$('#fedexNo').val()+'&otherPhone='+$('#otherPhone').val()+
						'&representativeName='+$('#representativeName').val()+'&id='+customerId+'&clientBrokerType='+$('#clientBrokerType').val();
		}
		
	}else if(actionType == 'billingInfo'){
		if(validateBillingInfo()){
			paramString = 'action=billingInfo&blFirstName='+$('#blFirstName').val()+'&blLastName='+$('#blLastName').val()+
						  '&addressLine1='+$('#street1').val()+'&addressLine2='+$('#street2').val()+'&countryName='+$('#countryName').val()+
						  '&stateName='+$('#stateName').val()+'&city='+$('#city').val()+'&zipCode='+$('#zipCode').val()+'&id='+customerId+
						  '&blEmail='+$('#blEmail').val()+'&blPhone='+$('#blPhone').val();
						  
		}
	}else if(actionType == 'shippingInfo'){
		if(validateShippingInfo()){
			paramString = 'action=shippingInfo&shFirstName='+$('#shFirstName').val()+'&shLastName='+$('#shLastName').val()+
						  '&shAddressLine1='+$('#shAddressLine1').val()+'&shAddressLine2='+$('#shAddressLine2').val()+
						  '&shCountryName='+$('#shCountryName').val()+'&id='+customerId+'&shippingAddrId='+$('#shippingAddrId').val()+
						'&shStateName='+$('#shStateName').val()+'&shCity='+$('#shCity').val()+'&shZipCode='+$('#shZipCode').val()+
						'&shEmail='+$('#shEmail').val()+'&shPhone='+$('#shPhone').val();
						  
		}
	}else if(actionType == 'invoiceInfo'){

	}else if(actionType == 'poInfo'){

	}else if(actionType == 'notes'){
		if($('#notesTxt').val()=='' || $('#notesTxt').val()==null){
			jAlert("Please Add notes to save.");
			return;
		}
		paramString = 'action=notes&notes='+$('#notesTxt').val()+'&id='+customerId;
	}else if(actionType == 'loyalFan'){
		var loyalSelectedRow = artistGrid.getSelectedRows([0])[0];	
		var artistId = artistGrid.getDataItem(loyalSelectedRow).artistId;
		var categoryName = artistGrid.getDataItem(loyalSelectedRow).parentCategory;
		if(artistId != null && artistId != ''){
			paramString = 'action=loyalFan&artistId='+artistId+'&categoryName='+categoryName+'&id='+customerId;
		}else{
			jAlert("Please select Artist to save.");
			return;
		}
	}else if(actionType == 'favouriteEvent'){
		var eventIds  = getSelectedEventGridId();
		if(eventIds=='' || eventIds.length==0){
			jAlert("Please Select Event to add in favourites.");
			return;
		}else{
			paramString = 'action=favouriteEvent&eventIds='+eventIds+'&id='+customerId;
		}
	}

	if(paramString!=''){
		updateCustomerDetails(paramString);
	}
}

function updateCustomerDetails(paramString){
	$.ajax({
		url : "${pageContext.request.contextPath}/Client/UpdateCustomerDetails",
		type : "post",
		dataType:"json",
		data : paramString,
		success : function(res){
			var jsonData = JSON.parse(JSON.stringify(res));
			if(jsonData.shippingInfo != undefined){
				$('#myModal-2').modal('hide');
				shippingInfo = jsonData.shippingInfo;
				createShippingGrid(jsonData.shippingInfo);
			}
			if(jsonData.billingInfo != undefined){
				fillBillingInfo(jsonData.billingInfo);
				jAlert("Billing Info Updated successfully.");
			}
			if(jsonData.Message != undefined){
				jAlert(jsonData.Message);
				fillLoyalFanInfo(jsonData.loyalFanInfo);
			}
			if(jsonData.FavEventMessage != undefined){
				jAlert(jsonData.FavEventMessage);
				getEventsGridData(0);
			}
			jAlert("Record updated successfully.");
		}, error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function updateRewardPoints(){
	var rewardPointsStr = $('#newRewardPoints').val();
	if(rewardPointsStr < 0){
		jAlert("Please enter valid reward points.");
		return;
	}
		
	$.ajax({
		url : "${pageContext.request.contextPath}/Client/AddRewardPoints",
		type : "post",
		dataType:"json",
		data : "customerId="+customerId+"&points="+rewardPointsStr,
		success : function(res){
			var jsonData = JSON.parse(JSON.stringify(res));
			if(jsonData.msg != undefined){
				jAlert(jsonData.msg);
				fillRewardPointsInfo(jsonData.rewardPointsInfo);
			}			
		}, error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function loadState(id,name){
	var countryId =0;
	if(id=='shStateName'){
		countryId = $('#shCountryName').val();
	}else if(id=='stateName'){
		countryId = $('#countryName').val();
	}
	
	if(countryId>0){
		$.ajax({
			url : "${pageContext.request.contextPath}/GetStates",
			type : "post",
			dataType:"json",
			data : "countryId="+countryId,
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				updateStateCombo(jsonData,id,name);
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}else{
		$('#'+id).empty();
		$('#'+id).append("<option value='-1'>--select--</option>");
	}
}

function updateStateCombo(jsonData,id,stateName){
	$('#'+id).empty();
	$('#'+id).append("<option value='-1'>--select--</option>");
	if(jsonData!='' && jsonData!=null){
		for(var i=0;i<jsonData.length;i++){
			$('#'+id).append("<option value="+jsonData[i].id+">"+jsonData[i].name+"</option>");
		}
		if(stateName!=''){
			$("#"+id+" option:contains(" + stateName + ")").attr('selected', 'selected');
		}
	}
}

function validateShippingInfo(){
	if($('#shFirstName').val()=='' || $('#shFirstName').val()==null){
		jAlert("Shipping Address - First name is mendatory.");
		return false;
	}
	if($('#shLastName').val()=='' || $('#shLastName').val()==null){
		jAlert("Shipping Address - Last name is mendatory.");
		return false;
	}
	if($('#shEmail').val()=='' || $('#shEmail').val()==null){
		jAlert("Shipping Address - Email is Mendatory.");
		return false;
	}
	if($('#shPhone').val()=='' || $('#shPhone').val()==null){
		jAlert("Shipping Address - Phone is Mendatory.");
		return false;
	}
	if($('#shAddressLine1').val()=='' || $('#shAddressLine1').val()==null){
		jAlert("Shipping Address - Street1 is mendatory.");
		return false;
	}
	if($('#shCity').val()=='' || $('#shCity').val()==null){
		jAlert("Shipping Address - City is mendatory.");
		return false;
	}
	if($('#shCountryName').val() <= 0){
		jAlert("Shipping Address - Country is mendatory.");
		return false;
	}
	if($('#shStateName').val() <= 0){
		jAlert("Shipping Address - State is mendatory.");
		return false;
	}
	if($('#shZipCode').val()=='' || $('#shZipCode').val()==null){
		jAlert("Shipping Address - Zipcode is mendatory.");
		return false;
	}
	return true;
}

function validateBillingInfo(){
	if($('#blFirstName').val()=='' || $('#blFirstName').val()==null){
		jAlert("Billing Address - First name is mendatory.");
		return false;
	}
	if($('#blLastName').val()=='' || $('#blLastName').val()==null){
		jAlert("Billing Address - Last name is mendatory.");
		return false;
	}
	if($('#blEmail').val()=='' || $('#blEmail').val()==null){
		jAlert("Billing address - Email is mendatory.");
		return false;
	}
	if($('#blPhone').val()=='' || $('#blPhone').val()==null){
		jAlert("Billing address - Phone is mendatory.");
		return false;
	}
	if($('#street1').val()=='' || $('#street1').val()==null){
		jAlert("Billing Address - Street1 is mendatory.");
		return false;
	}
	if($('#city').val()=='' || $('#city').val()==null){
		jAlert("Billing Address - City is mendatory.");
		return false;
	}
	if($('#countryName').val() <= 0){
		jAlert("Billing Address - Country is mendatory.");
		return false;
	}
	if($('#stateName').val() <= 0){
		jAlert("Billing Address - State is mendatory.");
		return false;
	}
	if($('#zipCode').val()=='' || $('#zipCode').val()==null){
		jAlert("Billing Address - Zip Code is mendatory.");
		return false;
	}
	return true;
}

function validateCustomerInfo(){
	if($('#name').val()=='' || $('#name').val()==null){
		jAlert("First Name is mendatory.");
		return false;
	}
	if($('#lastName').val()=='' || $('#lastName').val()==null){
		jAlert("Last Name is mendatory.");
		return false;
	}
	if($('#email').val()=='' || $('#email').val()==null){
		jAlert("Email is mendatory.");
		return false;
	}
	if($('#phone').val()=='' || $('#phone').val()==null){
		jAlert("Phone is mendatory.");
		return false;
	}
	if($('#customerLevel').val()=='' || $('#customerLevel').val()==null){
		jAlert("Customer Status is mendatory.");
		return false;
	}
	if($('#signupType').val()=='' || $('#signupType').val()==null){
		jAlert("Signup Type is mendatory.");
		return false;
	}
	if($('#customerLevel').val()=='' || $('#customerLevel').val()==null){
		jAlert("Customer Status is mendatory.");
		return false;
	}
	if($('#signupType').val()=='' || $('#signupType').val()==null){
		jAlert("Signup Type is mendatory.");
		return false;
	}
	
	return true;
}

/*
var artistCheckboxSelector = new Slick.CheckboxSelectColumn({
  cssClass: "slick-cell-checkboxsel"
});*/
	var artistPagingInfo;
	var artistDataView;
	var artistGrid;
	var artistData = [];
	var artistGridPager;
	var artistSearchString='';
	var artistColumnFilters = {};
	var userArtistColumnsStr = '<%=session.getAttribute("artistsearchgrid")%>';
	var userArtistColumns =[];
	var allArtistColumns = [ /*artistCheckboxSelector.getColumnDefinition(),*/
	{id: "artist", 
		name: "Artist", 
		field: "artist",
		width:80,		
		sortable: true
	}, {id: "grandChildCategory", 
		name: "Grand Child Category", 
		field: "grandChildCategory", 
		width:80,
		sortable: true
	},{id: "childCategory", 
		name: "Child Category", 
		field: "childCategory",
		width:80,			
		sortable: true
	}, {
		id : "parentCategory",
		field : "parentCategory",
		name : "Parent Category",
		width:80,
		sortable: true
	}/*, {
		id : "visibleInSearch",
		field : "visibleInSearch",
		name : "Visible / In Visible",
		width:80,
		sortable: true
	}*/ ];

	if(userArtistColumnsStr!='null' && userArtistColumnsStr!=''){
		columnOrder = userArtistColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<allArtistColumns.length;j++){
				if(columnWidth[0] == allArtistColumns[j].id){
					userArtistColumns[i] =  allArtistColumns[j];
					userArtistColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}
			
		}
	}else{
		userArtistColumns = allArtistColumns;
	}
	
	var artistOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var artistGridSortcol = "artistName";
	var artistGridSortdir = 1;
	var percentCompleteThreshold = 0;
	var artistGridSearchString = "";
	
	function artistGridComparer(a, b) {
		var x = a[artistGridSortcol], y = b[artistGridSortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
		
	function getArtistGridData(pageNo) {
		var cusId = '${customerId}';
		$.ajax({
			url : "${pageContext.request.contextPath}/GetArtistsForCustomer",
			type : "post",
			dataType: "json",
			data : "customerId="+cusId+"&pageNo="+pageNo+"&headerFilter="+artistSearchString,
			success : function(res){
				var jsonData = res;
				if(jsonData==null || jsonData=='') {
					jAlert("No Artist found.");
				} 
				artistPagingInfo = jsonData.artistPagingInfo;
				refreshartistGridValues(jsonData);
				fillLoyalFanInfo(jsonData.loyalFanInfo);
				clearAllSelections();
				//$('#artist_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserArtistPreference()'>");
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	function refreshartistGridValues(jsonData) {
	 $("div#divLoading").addClass('show');
		artistData = [];
		if(jsonData!=null && jsonData.artistList !=null ){
			for (var i = 0; i < jsonData.artistList.length; i++) {
				var  data= jsonData.artistList[i]; 
				var d = (artistData[i] = {});
				d["artistId"] = data.artistId;
				d["id"] =  i;
				d["grandChildCategory"] = data.grandChildCategory;
				d["childCategory"] = data.childCategory;
				d["parentCategory"] = data.parentCategory;
				d["artist"] = data.artistName;
				/*d["visibleInSearch"] = data.displayOnSearch;*/
			}
		}

		artistDataView = new Slick.Data.DataView();
		artistGrid = new Slick.Grid("#artist_grid", artistDataView, userArtistColumns, artistOptions);
		artistGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		artistGrid.setSelectionModel(new Slick.RowSelectionModel());
		//artistGrid.registerPlugin(artistCheckboxSelector);
		if(jsonData!=null){
			artistGridPager = new Slick.Controls.Pager(artistDataView, artistGrid, $("#artist_pager"),jsonData.artistPagingInfo);
		}
		var artistGridColumnpicker = new Slick.Controls.ColumnPicker(allArtistColumns, artistGrid,
				artistOptions);
		
		artistGrid.onSort.subscribe(function(e, args) {
			artistGridSortdir = args.sortAsc ? 1 : -1;
			artistGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				artistDataView.fastSort(artistGridSortcol, args.sortAsc);
			} else {
				artistDataView.sort(artistGridComparer, args.sortAsc);
			}
		});

		// wire up model artists to drive the artistGrid
		artistDataView.onRowCountChanged.subscribe(function(e, args) {
			artistGrid.updateRowCount();
			artistGrid.render();
		});
		artistDataView.onRowsChanged.subscribe(function(e, args) {
			artistGrid.invalidateRows(args.rows);
			artistGrid.render();
		});

		$(artistGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
			artistSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				artistColumnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in artistColumnFilters) {
					  if (columnId !== undefined && artistColumnFilters[columnId] !== "") {
						  artistSearchString += columnId + ":" +artistColumnFilters[columnId]+",";
					  }
					}
					getArtistGridData(0);
				}
			  }
		 
		});
		artistGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				$("<input type='text'>")
			   .data("columnId", args.column.id)
			   .val(artistColumnFilters[args.column.id])
			   .appendTo(args.node);
			}
			
		});
		artistGrid.init();

		// initialize the model after all the artists have been hooked up
		artistDataView.beginUpdate();
		artistDataView.setItems(artistData);
		artistDataView.endUpdate();
		artistDataView.syncGridSelection(artistGrid, true);
		artistGrid.resizeCanvas();
		 $("div#divLoading").removeClass('show');
	}
		
	function saveUserArtistPreference(){
		var cols = visibleColumns;
		if(cols==null || cols =='' || cols.length==0){
			cols = artistGrid.getColumns();
		}
		var colStr = '';
		for(var i=0;i<cols.length;i++){
			colStr += cols[i].id+":"+cols[i].width+",";
		}
		saveUserPreference('artistsearchgrid',colStr);
	}	

	function pagingControl(move,id){
		var pageNo = 0;
		if(id=='artist_pager'){
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(artistPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(artistPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(artistPagingInfo.pageNum)-1;
			}
			getArtistGridData(pageNo);
		}else if(id=='event_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(eventPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(eventPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(eventPagingInfo.pageNum)-1;
			}
			getEventsGridData(pageNo);
		}
	}
	
	function openPointsHistory(msg){
		var custmrId = '${customerId}';
		var url = "${pageContext.request.contextPath}/Client/EditRewardPoints?customerId="
			+ custmrId+"&Message="+msg;
		popupCenter(url, "Edit Reward Points", "800", "500");
	}

	/* Favourite Events Grid */
		var eventCheckboxSelector = new Slick.CheckboxSelectColumn({
		  cssClass: "slick-cell-checkboxsel"
		});
		var eventPagingInfo;
		var eventDataView;
		var eventGrid;
		var eventData = [];
		var eventGridSearchString='';
		var eventColumnFilters = {};
		var userEventColumnsStr = '<%=session.getAttribute("eventsgrid")%>';
		var userEventColumns =[];	
		var loadEventColumns = [eventCheckboxSelector.getColumnDefinition(), "eventName", "eventDate", "eventTime", "dayOfTheWeek", "venue", "noOfTix", "city", "state", "country", "notes"];
		var allEventColumns = [ eventCheckboxSelector.getColumnDefinition(),
		{	id : "eventMarketId",
			name : "Event Market Id",
			field : "eventMarketId",
			width:80,
			sortable : true
		},{
			id : "eventName",
			name : "Event Name",
			field : "eventName",
			width:80,
			sortable : true
		},{
			id : "eventDate",
			name : "Event Date",
			field : "eventDate",
			width:80,
			sortable : true
		},{
			id : "eventTime",
			name : "Event Time",
			field : "eventTime",
			width:80,
			sortable : true
		},{
			id : "dayOfTheWeek",
			name : "Day of Week",
			field : "dayOfTheWeek",
			width:80,
			sortable : true
		},{
			id : "venue",
			name : "Venue",
			field : "venue",
			width:80,
			sortable : true
		},{
			id : "venueId",
			name : "Venue Id",
			field : "venueId",
			width:80,
			sortable : true
		},{
			id : "noOfTix",
			name : "No of Tix",
			field : "noOfTix",
			width:80,
			sortable : true
		},{
			id : "noOfTixSold",
			name : "No of Tix Sold",
			field : "noOfTixSold",
			width:80,
			sortable : true
		},{
			id : "city",
			name : "City",
			field : "city",
			width:80,
			sortable : true
		},{
			id : "state",
			name : "State",
			field : "state",
			width:80,
			sortable : true
		},{
			id: "country", 
			name: "Country", 
			field: "country",
			width:80,	
			sortable: true
		},{
			id: "grandChildCategory", 
			name: "Grand Child Category", 
			field: "grandChildCategory", 
			width:80,
			sortable: true
		},{
			id: "childCategory", 
			name: "Child Category", 
			field: "childCategory", 
			width:80,
			sortable: true
		},{
			id : "parentCategory",
			field : "parentCategory",
			name : "Parent Category",
			width:80,
			sortable: true
		},{
			id : "notes",
			field : "notes",
			name : "Notes",
			width:80,
			sortable: true,
			editor:Slick.Editors.LongText
		},{
			id : "eventCreated",
			field : "eventCreated",
			name : "Event Created",
			width:80,
			sortable: true
		},{
			id : "eventLastUpdated",
			field : "eventLastUpdated",
			name : "Event Last Updated",
			width:80,
			sortable: true
		} ];
		
		if(userEventColumnsStr!='null' && userEventColumnsStr!=''){
			columnOrder = userEventColumnsStr.split(',');
			var columnWidth = [];
			for(var i=0;i<columnOrder.length;i++){
				columnWidth = columnOrder[i].split(":");
				for(var j=0;j<allEventColumns.length;j++){
					if(columnWidth[0] == allEventColumns[j].id){
						userEventColumns[i] = allEventColumns[j];
						userEventColumns[i].width=(columnWidth[1]-5);
						break;
					}
				}
				
			}
		}else{
			columnOrder = loadEventColumns;
			var columnWidth;
			for(var i=0;i<columnOrder.length;i++){
				columnWidth = columnOrder[i];
				for(var j=0;j<allEventColumns.length;j++){
					if(columnWidth == allEventColumns[j].id){
						userEventColumns[i] = allEventColumns[j];
						userEventColumns[i].width=80;
						break;
					}
				}			
			}
			//userEventColumns = allEventColumns;
		}
		
		var eventOptions = {
			editable: true,
			enableCellNavigation : true,
			asyncEditorLoading: false,
			forceFitColumns : true,
			multiSelect: false,
			topPanelHeight : 25,
			showHeaderRow: true,
			headerRowHeight: 30,
			explicitInitialization: true
		};
		var eventGridSortcol = "eventName";
		var eventGridSortdir = 1;
		var percentCompleteThreshold = 0;
			
		function eventGridComparer(a, b) {
			var x = a[eventGridSortcol], y = b[eventGridSortcol];
			if(!isNaN(x)){
			   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
			}
			if(x == '' || x == null) {
				return 1;
			} else if(y == '' || y == null) {
				return -1;
			}
			if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
				return x.toLowerCase().localeCompare(y.toLowerCase());	
			} else {
				return (x == y ? 0 : (x > y ? 1 : -1));	
			}
		}
			
		function getEventsGridData(pageNo) {		
			$.ajax({
				url : "${pageContext.request.contextPath}/GetEventsForCustomer",
				type : "post",
				dataType: "json",
				data : "pageNo="+pageNo+"&headerFilter="+eventGridSearchString,
				success : function(res){
					var jsonData = res;
					if(jsonData==null || jsonData=='') {
						jAlert("No Data Found.");
					} 
					eventPagingInfo = jsonData.pagingInfo;
					refreshEventGridValues(jsonData.events);
					eventGrid.setSelectedRows([]);
					$('#event_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserEventPreference()'>");
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
			
		function refreshEventGridValues(jsonData) {
			eventData=[];
			if(jsonData!=null && jsonData.length > 0){
				for (var i = 0; i < jsonData.length; i++) {
					var  data= jsonData[i];
					var d = (eventData[i] = {});
					d["id"] = i;
					d["eventMarketId"] = data.eventId;
					d["eventName"] = data.eventName;
					d["eventDate"] = data.eventDateStr;
					d["eventTime"] = data.eventTimeStr;
					d["dayOfTheWeek"] = data.dayOfWeek;
					d["venue"] = data.building;
					d["venueId"] = data.venueId;
					d["noOfTix"] = data.noOfTixCount;
					d["noOfTixSold"] = data.noOfTixSoldCount;
					d["city"] = data.city;
					d["state"] = data.state;
					d["country"] = data.country;
					d["grandChildCategory"] = data.grandChildCategoryName;
					d["childCategory"] = data.childCategoryName;
					d["parentCategory"] = data.parentCategoryName;
					d["eventCreated"] = data.eventCreationStr;
					d["eventLastUpdated"] = data.eventUpdatedStr;
					d["notes"] = data.notes;
				}
			}

			eventDataView = new Slick.Data.DataView();
			eventGrid = new Slick.Grid("#event_grid", eventDataView, userEventColumns, eventOptions);
			eventGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
			eventGrid.setSelectionModel(new Slick.RowSelectionModel());
			eventGrid.registerPlugin(eventCheckboxSelector);
			if(eventPagingInfo!=null){
				var eventGridPager = new Slick.Controls.Pager(eventDataView, eventGrid, $("#event_pager"),eventPagingInfo);
			}
			var eventGridColumnpicker = new Slick.Controls.ColumnPicker(allEventColumns, eventGrid,
					eventOptions);
			
			eventGrid.onSort.subscribe(function(e, args) {
				eventGridSortdir = args.sortAsc ? 1 : -1;
				eventGridSortcol = args.sortCol.field;
				if ($.browser.msie && $.browser.version <= 8) {
					eventDataView.fastSort(eventGridSortcol, args.sortAsc);
				} else {
					eventDataView.sort(eventGridComparer, args.sortAsc);
				}
			});
			// wire up model events to drive the eventGrid
			eventDataView.onRowCountChanged.subscribe(function(e, args) {
				eventGrid.updateRowCount();
				eventGrid.render();
			});
			eventDataView.onRowsChanged.subscribe(function(e, args) {
				eventGrid.invalidateRows(args.rows);
				eventGrid.render();
			});
			
			$(eventGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
			 var keyCode = (e.keyCode ? e.keyCode : e.which);
				eventGridSearchString='';
				 var columnId = $(this).data("columnId");
				  if (columnId != null) {
					eventColumnFilters[columnId] = $.trim($(this).val());
					if(keyCode == 13) {
						for (var columnId in eventColumnFilters) {
						  if (columnId !== undefined && eventColumnFilters[columnId] !== "") {
							  eventGridSearchString += columnId + ":" +eventColumnFilters[columnId]+",";
						  }
						}
						getEventsGridData(0);
					}
				  }
			 
			});
			eventGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
				$(args.node).empty();
				if(args.column.id.indexOf('checkbox') == -1){
					if(args.column.id != 'dayOfTheWeek'){
						if(args.column.id == 'eventTime'){
							$("<input type='text' placeholder='hh:mm a'>")
						   .data("columnId", args.column.id)
						   .val(eventColumnFilters[args.column.id])
						   .appendTo(args.node);
						}
						else if(args.column.id == 'eventDate' || args.column.id == 'eventCreated' || args.column.id == 'eventLastUpdated'){
							$("<input type='text' placeholder='mm/dd/yyyy'>")
						   .data("columnId", args.column.id)
						   .val(eventColumnFilters[args.column.id])
						   .appendTo(args.node);
						}else{
							$("<input type='text'>")
						   .data("columnId", args.column.id)
						   .val(eventColumnFilters[args.column.id])
						   .appendTo(args.node);
						}
					}
				}
			});
			eventGrid.init();
					
			// initialize the model after all the events have been hooked up
			eventDataView.beginUpdate();
			eventDataView.setItems(eventData);
			
			eventDataView.endUpdate();
			eventDataView.syncGridSelection(eventGrid, true);
			$("#gridContainer").resizable();
			eventGrid.resizeCanvas();
			
			var eventrowIndex;
			eventGrid.onSelectedRowsChanged.subscribe(function() { 
				var temprEventRowIndex = eventGrid.getSelectedRows([0])[0];
				if (temprEventRowIndex != eventrowIndex) {
					eventrowIndex = temprEventRowIndex;
					/* var eventId =eventGrid.getDataItem(temprEventRowIndex).eventMarketId;
					 var eventdetailStr = eventGrid.getDataItem(temprEventRowIndex).eventName+', '+eventGrid.getDataItem(temprEventRowIndex).eventDate+', '+
					eventGrid.getDataItem(temprEventRowIndex).eventTime+', '+eventGrid.getDataItem(temprEventRowIndex).venue;
					 
					 $('#AddTicketGroupSuccessMsg').hide();
					 
					getCategoryTicketGroupsforEvent(eventId,eventdetailStr); */
				}
			});
		}
				
		function getSelectedEventGridId() {
			var tempEventRowIndex = eventGrid.getSelectedRows();
			
			var eventIdStr='';
			$.each(tempEventRowIndex, function (index, value) {
				eventIdStr += ','+eventGrid.getDataItem(value).eventMarketId;
			});
			
			if(eventIdStr != null && eventIdStr!='') {
				eventIdStr = eventIdStr.substring(1, eventIdStr.length);
				 return eventIdStr;
			}
		}
				
		function saveUserEventPreference(){
			var cols = visibleColumns;
			if(cols==null || cols =='' || cols.length==0){
				cols = eventGrid.getColumns();
			}
			var colStr = '';
			for(var i=0;i<cols.length;i++){
				colStr += cols[i].id+":"+cols[i].width+","
			}
			saveUserPreference('eventsgrid',colStr);
		}
	 
</script>