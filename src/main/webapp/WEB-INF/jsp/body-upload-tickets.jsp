<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<!-- Bootstrap CSS -->
<link href="../resources/css/bootstrap.min.css" rel="stylesheet">
<link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
<link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
<link href="../resources/css/font-awesome.min.css" rel="stylesheet" />
<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
<link href="../resources/css/jquery.alerts.css" rel=" stylesheet">
<!-- Custom styles -->
<link href="../resources/css/widgets.css" rel="stylesheet">
<link href="../resources/css/style.css" rel="stylesheet">
<link href="../resources/css/style-responsive.css" rel="stylesheet" />
<script src="../resources/js/jquery-1.8.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/bootstrap.min.js"></script>
<script src="../resources/js/jquery.alerts.js"></script>
<script>
	$(document).ready(function() {
		
	});
	
	window.onunload = function () {
	    var win = window.opener;
	    if (!win.closed) {
	    	window.opener.getInvoiceGridData(0);
	    }
	};
	
	function uploadTickets() {
		if(!$('#uploadToExchange').is(':checked')){
			jConfirm("Are you Sure you dont want to upload tickets to Exchange?","Confirm",function(r) {
				if (r) {
					$("#uploadTicketToExchange").val('No');
					$("#action").val('uploadFiles');
					$("#uploadTickets").submit();
				}
			});
		}else{
			$("#uploadTicketToExchange").val('Yes');
			$("#action").val('uploadFiles');
			$("#uploadTickets").submit();
		}
		
	}
	function downloadTicketFile(invoiceId, fileType, position) {
		var url = "${pageContext.request.contextPath}/Accounting/DownloadRealTix?invoiceId="
				+ invoiceId + "&fileType=" + fileType+"&position="+position;
		$('#download-frame').attr('src', url);
	}
	function addTicketUploadRow(tableName) {
		var j = $('#'+tableName+' tr').length-1;
		j++;
		$('#'+tableName).append('<tr id="'+tableName+'_'+j+'"></tr>');
		var rowString = '';
		if(tableName=='eticketTable'){
			rowString = '<td>'+ (j) +'</td>'+
				//'<td><input type="checkbox" id="eticketCheck_'+j+'" class="eticketClass" name="eticketRow_'+j+'"></td>'+
				'<td> <input type="file" id="eticket_'+j+'" name="eticket_'+j+'"/></td>';
				
		}else if(tableName=='qrcodeTable'){
			rowString = '<td>'+ (j) +'</td>'+
				//'<td><input type="checkbox" id="barcodeCheck_'+j+'" class="qrcodeClass" name="qrcodeRow_'+j+'"></td>'+
				'<td> <input type="file" id="qrCode_'+j+'" name="qrcode_'+j+'"/></td>';
		}else if(tableName=='barcodeTable'){
			rowString = '<td>'+ (j) +'</td>'+
				//'<td><input type="checkbox" id="qrcodeCheck_'+j+'" class="barcodeClass" name="barcodeRow_'+j+'"></td>'+
				'<td> <input type="file" id="barcode_'+j+'" name="barcode_'+j+'"/></td>';
		}
		$('#'+tableName+'_count').val(j);
		$('#'+tableName+'_'+j).html(rowString);
	}
	function deleteTicketUploadRow(tableName) {
		var count = $('#'+tableName+' tr').length-1;
		if($('#'+tableName+'_'+count).hasClass("savedClass")){
			var fileType = '';
			if(tableName=='eticketTable'){
				fileType = 'ETICKET';
			}else if(tableName=='barcodeTable'){
				fileType = 'BARCODE';
			}else if(tableName=='qrcodeTable'){
				fileType = 'QRCODE';
			} 
			deleteFile(tableName,fileType,count);
		}else{
			$('#'+tableName+'_'+count).remove();
			$('#'+tableName+'_count').val(count-1);
		}
		
		
	}
	function deleteFile(tableName,fileType,position){
		jConfirm("Are you Sure you want delete attach file?","Confirm",function(r) {
			if (r) {
				$.ajax({
					url : "${pageContext.request.contextPath}/Accounting/DeleteRealTixAttachment",
					type : "post",
					dataType:"text",
					data : "invoiceId="+$('#invoiceId').val()+"&fileType="+fileType+"&position="+position,
					success : function(res){
						jAlert(res);
						$('#'+tableName+'_'+position).remove();
						$('#'+tableName+'_count').val(position-1);
					}, error : function(error){
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
				});
			}
		});
		 
	}

</script>
<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i>Accounting
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Invoice</a>
			</li>
			<li><i class="fa fa-laptop"></i>Upload Tickets</li>
		</ol>
	</div>
</div>
<br />
<c:if test="${successMessage != null}">
	<div class="alert alert-success fade in">
		<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${successMessage}</strong>
	</div>
</c:if>
<c:if test="${errorMessage != null}">
	<div class="alert alert-block alert-danger fade in">
		<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${errorMessage}</strong>
	</div>
</c:if>
<c:if test="${realTicketMapped == true}">
<div id="row">
	<div class="col-lg-12">
		<form class="form-validate form-horizontal" id="uploadTickets" method="post" enctype="multipart/form-data" action="${pageContext.request.contextPath}/Accounting/UploadTickets">
			<div class="form-group">
				<input type="hidden" id="action" name="action" value="" /> 
				<input type="hidden" id="invoiceId" name="invoiceId" value="${invoiceId}" />
				<input type="hidden" name="eventId" value="${customerOrders.eventId}" />
				<input type="hidden" name="eventName" value="${customerOrders.eventName}"/>
				<input type="hidden" name="eventDate" value="${customerOrders.eventDateStr}"/>
				<input type="hidden" name="eventTime" value="${customerOrders.eventTimeStr}"/>
				<input type="hidden" name="section" value="${customerOrders.section}"/>
				<input type="hidden" name="row" value="${customerOrders.row}"/>
				<div class="col-sm-4">
					<a id="eticket_add_row" class="btn btn-default btn-sm pull-left" style="margin-left:16px;"  onclick="addTicketUploadRow('eticketTable');">Add Row</a>
				<a id='eticket_delete_row' class='btn btn-default btn-sm pull-left' onclick="deleteTicketUploadRow('eticketTable');">Delete Row</a>
				</div>
				<div class="col-sm-4">
					<a id="qrcode_add_row" class="btn btn-default btn-sm pull-left" style="margin-left:16px;"  onclick="addTicketUploadRow('qrcodeTable');">Add Row</a>
				<a id='qrcode_delete_row' class='btn btn-default btn-sm pull-left' onclick="deleteTicketUploadRow('qrcodeTable');">Delete Row</a>
				</div>
				<div class="col-sm-4">
					<a id="barcode_add_row" class="btn btn-default btn-sm pull-left" style="margin-left:16px;"  onclick="addTicketUploadRow('barcodeTable');">Add Row</a>
				<a id='barcode_delete_row' class='btn btn-default btn-sm pull-left' onclick="deleteTicketUploadRow('barcodeTable');">Delete Row</a>
				</div>
			</div>
			
			<div class="form-group">
			<div class="col-sm-4">
				<table class="table table-bordered table-hover" id="eticketTable">
				<input type="hidden" name="eticketTable_count" id="eticketTable_count" value="${eticketCount}">
					<thead>
						<tr >
							<th class="text-center">
								#
							</th>
							<th style="font-size: 13px;font-family: arial;" class="text-center">
								E-ticket
							</th>
						</tr>
					</thead>
					<tbody>
					<c:if test="${not empty eticketAttachments}">
						 <c:forEach var="eticket" varStatus="vStatus" items="${eticketAttachments}">
                      		<tr id="eticketTable_${eticket.position}" class="savedClass">
	                      		<td>${vStatus.count}</td>
	                      		<td> 
	                      			<input type="file" id="eticket_${eticket.position}" name="eticket_${eticket.position}"/><br/>
	                      			<a href="javascript:downloadTicketFile('${eticket.invoiceId}','${eticket.type}','${eticket.position}')" >Download file</a>
	                      			<%-- <img class="editClickableImage" style="height:14px;" src="../resources/images/ico-delete.gif" onclick="deleteFile('${eticket.invoiceId}','${eticket.type}','${eticket.position}')"/> --%>
	                      		</td>
                      		</tr>
                      	</c:forEach>
                    </c:if>
                    </tbody>
				</table>
				</div>
				<div class="col-sm-4">
					<table class="table table-bordered table-hover" id="qrcodeTable">
					<input type="hidden" name="qrcodeTable_count" id="qrcodeTable_count" value="${qrcodeCount}">
					<thead>
						<tr >
							<th class="text-center">
								#
							</th>
							<th style="font-size: 13px;font-family: arial;" class="text-center">
								Mobile Entry
							</th>
							
						</tr>
					</thead>
					<tbody>
					<c:if test="${not empty qrcodeAttachments}">
						 <c:forEach var="qrcode" varStatus="vStatus" items="${qrcodeAttachments}">
                      		<tr id="qrcodeTable_${qrcode.position}" class="savedClass">
	                      		<td>${vStatus.count}</td>
	                      		<td> 
	                      			<input type="file" id="qrcode_${qrcode.position}" name="qrcode_${qrcode.position}"/><br/>
	                      			<a href="javascript:downloadTicketFile('${qrcode.invoiceId}','${qrcode.type}','${qrcode.position}')" >Download file</a>
	                      			<%-- <img class="editClickableImage" style="height:14px;" src="../resources/images/ico-delete.gif" onclick="deleteFile('${qrcode.invoiceId}','${qrcode.type}','${qrcode.position}')"/> --%>
	                      		</td>
                      		</tr>
                      	</c:forEach>
                    </c:if>
                    </tbody>
				</table>
				</div>
				<div class="col-sm-4">
					<table class="table table-bordered table-hover" id="barcodeTable">
					<input type="hidden" name="barcodeTable_count" id="barcodeTable_count" value="${barcodeCount}">
					<thead>
						<tr >
							<th class="text-center">
								#
							</th>
							<th style="font-size: 13px;font-family: arial;" class="text-center">
								Electronic Transfer
							</th>
						</tr>
					</thead>
					<tbody>
					<c:if test="${not empty barcodeAttachments}">
						 <c:forEach var="barcode" varStatus="vStatus" items="${barcodeAttachments}">
                      		<tr id="barcodeTable_${barcode.position}" class="savedClass">
	                      		<td>${vStatus.count}</td>
	                      		<td> 
	                      			<input type="file" id="barcode_${barcode.position}" name="barcode_${barcode.position}"/><br/>
	                      			<a href="javascript:downloadTicketFile('${barcode.invoiceId}','${barcode.type}','${barcode.position}')" >Download file</a>
	                      			<%-- <img class="editClickableImage" style="height:14px;" src="../resources/images/ico-delete.gif" onclick="deleteFile('${barcode.invoiceId}','${barcode.type}','${barcode.position}')"/> --%>
	                      		</td>
                      		</tr>
                      	</c:forEach>
                    </c:if>
                    </tbody>
				</table>
				</div>
			</div>
			<div align="center">
				 <input type="checkbox" id="uploadToExchange" name="uploadToExchange" <c:if test="${invoice.uploadToExchnge == 'true'}">checked</c:if>>
				<input type="hidden" id="uploadTicketToExchange" name="uploadTicketToExchange" value="">
				 <label >Upload ticket to Exchnage</label>
			</div>
			<br/><br/>
		</form>
	</div>
	<br />
	<div class="col-lg-12">
		<div class="form-group" align="center">
			<button type="button" onclick="uploadTickets();" style="width:100px" class="btn btn-primary btn-sm">Save</button>
		</div>
	</div>
</div>
</c:if>
