<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap CSS -->    
    <link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../resources/css/font-awesome.min.css" rel="stylesheet" />    
    <!-- full calendar css-->
    <link href="../resources/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="../resources/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
    <!-- easy pie chart-->
    <link href="../resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <!-- owl carousel -->
    <link rel="stylesheet" href="../resources/css/owl.carousel.css" type="text/css">
	<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
	<link rel="stylesheet" href="../resources/css/fullcalendar.css">
	<link href="../resources/css/widgets.css" rel="stylesheet">
    
    <link href="../resources/css/style-responsive.css" rel="stylesheet" />
	<link href="../resources/css/xcharts.min.css" rel=" stylesheet">	
	
	<link href="../resources/css/jquery.alerts.css" rel=" stylesheet">
	
	<!--<link href="../resources/css/jquery-ui-1.10.4.min.css" rel="stylesheet">--> 
	<link href="../resources/css/style.css" rel="stylesheet">
	
	 <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->
    
    <script src="../resources/js/jquery-1.8.3.min.js"></script>
    <%-- <script type="text/javascript" src="../resources/js/jquery-ui-1.9.2.custom.min.js"></script> --%>
    
    <!-- bootstrap -->
    <script src="../resources/js/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script src="../resources/js/jquery.scrollTo.min.js"></script>
    <script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../resources/js/jquery.alerts.js"></script>
    
    <!-- slickgrid related scripts -->
	<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
	<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
	<script src="../resources/js/slick/slick.core.js"></script>
	<script src="../resources/js/slick/slick.formatters.js"></script>
	<script src="../resources/js/slick/slick.editors.js"></script>
	<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
	<script src="../resources/js/slick/slick.grid.js"></script>
	<script src="../resources/js/slick/slick.dataview.js"></script>
	<script src="../resources/js/slick/controls/slick.pager.js"></script>
	<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
	<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

	<link href="../resources/css/datepicker.css" rel="stylesheet">
	<script src="../resources/js/bootstrap-datepicker.js"></script>

	<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>
	<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>

<style>
input {
	color: black !important;
}

.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}
.slick-headerrow-column {
      background: #87ceeb;
      text-overflow: clip;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }
    .slick-headerrow-column input {
      margin: 0;
      padding: 0;
      width: 100%;
      height: 100%;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }
.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}
.slick-viewport {
    overflow-x: hidden !important;
}
/*

input{
		color : black !important;
	}
*/	
#totalPrice{
  font-family: arial;
  font-size: 10pt;
  font-weight: bold;
}

#totalQty{
  font-family: arial;
  font-size: 10pt;
  font-weight: bold;
}
</style>

<script type="text/javascript">

$(document).ready(function(){
		$('#cardInfo').hide();
		$('#accountInfo').hide();
		$('#chequeInfo').hide();
		$("div#divLoading").addClass('show');
		
		/* $('#fromDate').datepicker({
			format: "mm/dd/yyyy",
			autoclose : true,
			orientation: "top",
			todayHighlight: true
		});
	
		$('#toDate').datepicker({
			format: "mm/dd/yyyy",
			autoclose : true,
			orientation: "top",
			todayHighlight: true
		}); */
		var poType = '${purchaseOrder.purchaseOrderType}';
		if(poType == 'CONSIGNMENT'){
			$('#poTypeCheck').attr('checked',true);
			$('#consignmentPoDiv').show();
		}else{
			$('#consignmentPoDiv').hide();
		}
		
		$('#poTypeCheck').click(function(){
			if($('#poTypeCheck').is(':checked')){
				$('#poType').val("CONSIGNMENT");
				$('#consignmentPoDiv').show();
			}else{
				$('#poType').val("REGULAR");
				$('#consignmentPoDiv').hide();
			}
		});
		
		
		$('#paymentDate').datepicker({
	        format: "mm/dd/yyyy",
			autoclose : true,
			orientation: "top",
			todayHighlight: true
	    });
		
		$('#addTicket').click(function(){
			$('#myTable').show();
			getEventInfoForPO();
		});
		showHideFedexInfo();
		$('#shippingMethod').change(function(){
			showHideFedexInfo();
		});
				
		$('input[type=radio][name=paymentType]').change(function(){
	        if (this.value == 'card') {
	            $('#cardInfo').show();
				$('#accountInfo').hide();
				$('#chequeInfo').hide();
	        }else if (this.value == 'account') {
				$('#chequeInfo').hide();           
			    $('#cardInfo').hide();
				$('#accountInfo').show();
	        }else if (this.value == 'cheque') {
				$('#cardInfo').hide();
			    $('#accountInfo').hide();
	            $('#chequeInfo').show();
	        }
		});
		
		function showHideFedexInfo(){
			if($('#shippingMethod option:selected').html().toLowerCase().indexOf("fedex") >= 0){
				$('#trackingInfo').show();
			}else{
				$('#trackingInfo').hide();
			}
		}
		
		$(window).resize(function() {
			eventGrid.resizeCanvas();
		});
		
		$('#eventSearchValue').keypress(function (event) {
			var keyCode = (event.keyCode ? event.keyCode : event.which);
			 if(keyCode == 13)  // the enter key code
			  {
				 getEventGridData(0); 
			    //$('#searchCustomerBtn').click();
			    //return false;  
			  }
		});
		
		$('.searchcontrol').keypress(function (event) {
		var keyCode = (event.keyCode ? event.keyCode : event.which);
		 if(keyCode == 13)  // the enter key code
		  {
			 getEventGridData(0);
			//$('#searchEventBtn').click();
			//return false;  
		  }
		});
		
		var paymentType = "${payment.paymentType}";
		if(paymentType == 'card'){
			$('#radioCard').prop('checked',true);
		  	$('#cardInfo').show();
		}
		if(paymentType == 'account'){
			$('#radioAccount').prop('checked',true);
		  	$('#accountInfo').show();
		}
		if(paymentType == 'cheque'){
			$('#radioCheque').prop('checked',true);
		  	$('#chequeInfo').show();
		}
		/*
		<c:if test="${payment.paymentType=='card'}">
		    $('#radioCard').prop('checked',true);
		  	$('#cardInfo').show();
		</c:if>
		<c:if test="${payment.paymentType=='account'}">
		 	$('#radioAccount').prop('checked',true);
		  	$('#accountInfo').show();
		</c:if>
		<c:if test="${payment.paymentType=='cheque'}">
		 	$('#radioCheque').prop('checked',true);
		  	$('#chequeInfo').show();
		</c:if>
		*/
		
	});
	
	window.onunload = function () {
	    var win = window.opener;
	    if (!win.closed) {
	    	window.opener.getPOGridData(0);
	    }
	};
	
	function doEditPOValidation(){
		var isValid = true;
		if($('#shippingMethod option:selected').html().toLowerCase().indexOf("fedex") < 0){
			$('#trackingNo').val("");
		}
		$('.section').each(function() {
			if($(this).val() == '' || $(this).val() == null){
				isValid =false;
			}
		}); 
		if(!isValid){
			 alert("Please add Section.");
			 return;
		}
		$('.rowValue').each(function() {
			if($(this).val() == '' || $(this).val() == null){
				isValid =false;
			}
		}); 
		if(!isValid){
			 alert("Please add Row.");
			 return;
		}
		$('.seatLow').each(function() {
			if($(this).val() == '' || $(this).val() == null){
				isValid =false;
			}
		});
		if(!isValid){
			 alert("Please add Seat Low.");
			 return;
		}
		$('.seatHigh').each(function() {
			if($(this).val() == '' || $(this).val() == null){
				isValid =false;
			}
		}); 
		if(!isValid){
			 alert("Please add Seat High.");
			 return;
		}
		$('.qty').each(function() {
			if($(this).val() == '' || $(this).val() == null){
				isValid =false;				
			}
		});
		if(!isValid){
			alert("Please add Quantity.");
			return;
		}		
		$('.price').each(function() {
			if($(this).val() == '' || $(this).val() == null){
				isValid =false;
			}
			if(isNaN($(this).val())){
				isValid =false;
			}
		});
		if(!isValid){
			 alert("Please add Price.");
			 return;
		} 
		if($('#shippingMethod').val() < 0){
			jAlert("Please select Shipping method.");
			return;
		}
		if($('#cardNo').val()!='' && $('#cardNo').val().length !=4){
			jAlert("Please enter Card last 4 digit only");
			return;
		}
		var myTable = document.getElementById("myTable");
	    var currentIndex = myTable.rows.length;
		if(currentIndex <= 1){
			isValid = false;
		}
		if(!isValid){
			 alert("Please add atleast 1 ticket to update.");
			 return;
		}
		if($('#totalQty').val() <= 0){
			jAlert("Total quantity not less than 0.");
			return;
		}
		if($('#totalPrice').val() <= 0){
			jAlert("Total price not less than 0.");
			return;
		}
		$("#action").val('editPOInfo');
		$("#purchaseOrderForm").submit();
		
	}
	
	function doVoidPO(){
		var myTable = document.getElementById("myTable");
	    var currentIndex = myTable.rows.length;
		if(currentIndex <= 1){			
			 jAlert("Please add atleast 1 ticket to void.");
			 return;
		}
		if($('#totalQty').val() <= 0){
			jAlert("Total quantity not less than 0.");
			return;
		}else{
			jConfirm("Do you want to void this PO ?","Confirm",function(r){
				if(r){					
					if($('#shippingMethod option:selected').html().toLowerCase().indexOf("fedex") < 0){
						$('#trackingNo').val("");
					}
					$("#action").val('voidPO');
					$("#purchaseOrderForm").submit();
					//window.close();
					//window.opener.onChildClosed();
				}else{
					return false;
				}
			});
		}
	}

	//Edit PO cancel action
	function cancelAction(){
		window.close();
		window.opener.location.reload(true);
		window.location.href = "${pageContext.request.contextPath}/Accounting/ManagePO";
	}
	
	function removeField(id){
		//jAlert('remove');
		var p=id.parentNode.parentNode;
   	 	p.parentNode.removeChild(p);
	}
	
	//function to sum the entered quantity
	function sumQty(qty){
		var sum = 0;
		$('.qty').each(function() {
			if($(this).val() != '' && !isNaN($(this).val())){
				sum += parseInt($(this).val());
			}
		});

		$('#totalQty').val(sum);
	}
	
	//function to sum the entered price
	function sumPrice(price){
		var sum = 0;
		$('.price').each(function() {
			if($(this).val() != '' && !isNaN($(this).val())){
				sum += parseFloat($(this).val());
			}
		});

		$('#totalPrice').val(sum);
	}
	
	function resetFilters(){
		eventGridSearchString='';
		columnFilters = {};
		getEventGridData(0);
	}
	
	function searchEventData(){
		eventGridSearchString='';
		columnFilters = {};
		getEventGridData(0);
	}
</script>
<div class="container">
<div class="row">
	<div class="col-lg-12">
		<h3 style="font-size: 13px;font-family: arial;" class="page-header">
			<i class="fa fa-laptop"></i> ACCOUNTING
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#"
				onclick="window.close(); window.opener.location.reload(true);">Accounting</a>
			</li>
			<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Edit Purchase Order</li>
		</ol>

	</div>
</div>

<div id="poInfo">
<div class="row">
	<div class="col-lg-12">
		<section class="panel"> <header style="font-size: 13px;font-family: arial;" class="panel-heading">
		Customer Info </header>
		<div class="panel-body">
			<c:if test="${successMessage != null}">
				<div class="alert alert-success fade in">
					<strong
						style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${successMessage}</strong>
				</div>
			</c:if>
			<c:if test="${errorMessage != null}">
				<div class="alert alert-block alert-danger fade in">
					<strong
						style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${errorMessage}</strong>
				</div>
			</c:if>
			
				<div class="form-group">
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Customer Name:&nbsp;</label>
					<span style="font-size: 15px;">${customerInfo.customerName}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Email:&nbsp; </label>
					<span style="font-size: 15px;">${customerInfo.userName}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Phone:&nbsp;</label>
					<span style="font-size: 15px;">${customerInfo.phone}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Address Line1:&nbsp;</label>
					<span style="font-size: 15px;">${customerAddress.addressLine1}</span>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Address Line2:&nbsp;</label>
					<span style="font-size: 15px;">${customerAddress.addressLine2}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">City:&nbsp;</label>
					<span style="font-size: 15px;">${customerAddress.city}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">State:&nbsp;</label>
					<span style="font-size: 15px;">${customerAddress.state.name}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Country:&nbsp;</label>
					<span style="font-size: 15px;">${customerAddress.country.name}</span>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">ZipCode:&nbsp;</label>
					<span style="font-size: 15px;">${customerAddress.zipCode}</span>
				</div>
			</div>
		</div>
	</section>
</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<section class="panel"> <header style="font-size: 13px;font-family: arial;" class="panel-heading">
		Ticket Info </header>
		<div class="panel-body">				
			<div class="form-group">
				<form id="purchaseOrderForm" name="purchaseOrderForm"
				method="post"
				action="${pageContext.request.contextPath}/Accounting/EditPurchaseOrder">
				<input class="form-control" id="action" value="action" name="action" type="hidden" /> 
				<input class="form-control" id="purchaseOrderId" value="${purchaseOrder.id}" name="poId" type="hidden" /> 
				<input class="form-control" id="customerId" value="${purchaseOrder.customerId}" name="customerId" type="hidden" />
					<input type="hidden" name="action" value="action" id="action" /> <input
						type="hidden" name="customerId" value="${customer.id}"
						id="customerId" />
						<div class="table-responsive">
						<table id="myTable"
							class="table table-striped table-advance table-hover">
							<tbody>
								<tr>
									<th>
										<!-- S.No -->
									</th>
									 <th style="font-size: 13px;font-family: arial;">Event Name</th>
									<th style="font-size: 13px;font-family: arial;">Section</th>
									<th style="font-size: 13px;font-family: arial;">Row</th>
									<th style="font-size: 13px;font-family: arial;">Seat Low</th>
									<th style="font-size: 13px;font-family: arial;">Seat High</th>
									<th style="font-size: 13px;font-family: arial;">Qty</th>
									<th style="font-size: 13px;font-family: arial;">Price</th>
									<th style="font-size: 13px;font-family: arial;">Action</th>
								</tr>
								<!-- dynamic rows -->
								<!--<h4 id="totalQty" style="float:right;">Total qty</h4>-->
								<c:forEach var="ticketGroup" items="${ticketGroup}" varStatus="theCount">
									<input type="hidden" id="rowId_${theCount.count}" name="rowId_${theCount.count}" value="${ticketGroup.eventId}">
									<input type="hidden" name="ticketGroup_${theCount.count}_${ticketGroup.id}" />
									<%-- <input type="text" name="eventId_${ticketGroup.eventId}" value=""/> --%>
									<tr>
										<td>
											<%-- ${theCount.count} --%>
										</td>
										<td><label name="event_${ticketGroup.id}">
											${ticketGroup.events.eventName} ${ticketGroup.events.eventDateStr} ${ticketGroup.events.eventTimeStr}</label>
										</td>
										<td><input type="text" class="section" name="section_${theCount.count}_${ticketGroup.id}"
											value="${ticketGroup.section}" />
										</td>
										<td><input type="text" class="rowValue" name="row_${theCount.count}_${ticketGroup.id}"
											size="10" value="${ticketGroup.row}" />
										</td>
										<td><input type="text" class="seatLow" name="seatLow_${theCount.count}_${ticketGroup.id}"
											value="${ticketGroup.seatLow}" />
										</td>
										<td><input type="text" class="seatHigh" name="seatHigh_${theCount.count}_${ticketGroup.id}"
											value="${ticketGroup.seatHigh}" />
										</td>
										<td><input type="text" name="qty_${theCount.count}_${ticketGroup.id}"
											id="qty_${theCount.count}_${ticketGroup.id}" class="qty"
											onkeyup="sumQty(this);" value="${ticketGroup.quantity}" />
										</td>
										<td><input type="text" name="price_${theCount.count}_${ticketGroup.id}"
											id="price_${theCount.count}_${ticketGroup.id}" class="price"
											onkeyup="sumPrice(this);" value="${ticketGroup.price}" />
										</td>
										<td><input type="button" value="Remove"
											onclick="removeField(this); sumQty(this); sumPrice(this);" class="button" />
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						</div>
					</div>
				
		
<div class="row">
	<div class="col-lg-12">
		<div class="form-group">
		 <div class="col-lg-3 col-sm-3">
			Shipping Method :
			<select id="shippingMethod" name="shippingMethod" class="form-control input-sm m-bot15">
			  <option value="-1">--select--</option>
			  <c:forEach items="${shippingMethods}" var="shippingMethod">
				<option <c:if test="${shippingMethod.id==purchaseOrder.shippingMethodId}"> Selected </c:if>
					value="${shippingMethod.id}"> ${shippingMethod.name}
				</option>
			</c:forEach>
			</select>
		 </div>
		 
		 <div class="col-lg-3 col-sm-3" id="trackingInfo">
			Tracking No :
			<input type="text" name="trackingNo" id="trackingNo" value="${purchaseOrder.trackingNo}" class="form-control"/>
		 </div>
		 
		 <div class="col-lg-3 col-sm-3" style="float:right">
			Total Price : $<input type="text" id="totalPrice" name="totalPrice" readOnly value="${purchaseOrder.poTotal}" style="border: none;">
		 </div>
		 
		 <div class="col-lg-3 col-sm-3 " style="float:right">
			Total Qty :
			<input type="text" id="totalQty" name="totalQty" readOnly value="${purchaseOrder.ticketCount}" style="border: none;">
		 </div>
		 </div>		 
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="form-group">
			<div class="col-sm-3 ">
			<label>External PO No :</label>
			<input type="text" name="externalPONo" value="${purchaseOrder.externalPONo}" class="form-control"/>
		 </div>
		 <div class="col-sm-3 ">
			<label>Consignment PO :</label>
			<input type="checkbox" id="poTypeCheck" />
			<input type="hidden" id="poType" name="poType" />
		 </div>
		 <div class="col-sm-3" id="consignmentPoDiv">
			<label>Consignment PO No :</label>
			<input type="text" name="consignmentPo" id="consignmentPo" value="${purchaseOrder.consignmentPoNo}" class="form-control"/>
		 </div>
		 <div class="col-sm-3 ">
			<label>Transaction Office :</label>
			<select name="transactionOffice" class="form-control">
		  		<option value="Main" <c:if test="${purchaseOrder.transactionOffice == 'Main'}" >selected</c:if>>Main</option>
		  		<option value="Spec" <c:if test="${purchaseOrder.transactionOffice == 'Spec'}" >selected</c:if>>Spec</option>
		  	</select>  
		 </div>
		</div>
	</div>
</div>
</div>	
</section>
</div>
</div>
<div class="row">
	<div class="col-lg-12">		
		 <section class="panel">
			<header style="font-size: 13px;font-family: arial;" class="panel-heading">Payment Details</header>
			<div class="panel-body">
				<div class="row">
				<div class="col-lg-12">
				<div class="form-group">
					<div class="col-sm-4">
						  <input type="radio" name="paymentType" id="radioCard" value="card">Credit Card
					</div>
					<div class="col-sm-4">
						  <input type="radio" name="paymentType" id="radioAccount" value="account">In Account
					</div>
					<div class="col-sm-4">
						  <input type="radio" name="paymentType" id="radioCheque" value="cheque">Cheque
					</div>
				</div>
				<hr/>
				<div class="form-group" id="cardInfo">
					<div class="col-sm-4">
						<label>Card Holder Name</label>
					  	<input type="text" name="name" id="name" <c:if test="${payment.paymentType=='card'}">value="${payment.name}"</c:if> class="form-control"/> 
					</div>
					<div class="col-sm-4">
						<label>Card Type</label>
					  	<select name="cardType" class="form-control">
					  		<option value="American Express" <c:if test="${payment.cardType=='American Express'}">selected</c:if> >American Express</option>
							<option value="Visa" <c:if test="${payment.cardType=='Visa'}">selected</c:if> >Visa</option>
							<option value="MasterCard" <c:if test="${payment.cardType=='MasterCard'}">selected</c:if> >MasterCard</option>
							<option value="JCB" <c:if test="${payment.cardType=='JCB'}">selected</c:if> >JCB</option>
							<option value="Discover" <c:if test="${payment.cardType=='Discover'}">selected</c:if> >Discover</option>
							<option value="Diners Club" <c:if test="${payment.cardType=='Diners Club'}">selected</c:if> >Diners Club</option>
					  	</select> 
					</div>
					<div class="col-sm-4">
						<label>Card Last 4 Digit</label>
					  	<input type="text" name="cardNo" id="cardNo" value="${payment.cardLastFourDigit}" class="form-control"/> 
					</div>
					<hr/>
				</div>
				<div class="form-group" id="accountInfo">
					<div class="col-sm-4">
						<label>Account Name</label>
					  	<input type="text" name="accountName" id="accountName" <c:if test="${payment.paymentType=='account'}">value="${payment.name}"</c:if> class="form-control"/> 
					</div>
					<div class="col-sm-4">
						<label>Routing no. Last 4 Digit</label>
					  	<input type="text" name="routingNo" id="routingNo" <c:if test="${payment.paymentType=='account'}">value="${payment.routingLastFourDigit}"</c:if> class="form-control"/>
					</div>
					<div class="col-sm-4">
						<label>Account no. Last 4 Digit</label>
					  	<input type="text" name="accountNo" id="accountNo"  <c:if test="${payment.paymentType=='account'}">value="${payment.accountLastFourDigit}"</c:if>value="${payment.accountLastFourDigit}" class="form-control"/> 
					</div>
					<hr/>
				</div>
				<div class="form-group" id="chequeInfo">
					<div class="col-sm-3">
						<label>Name</label>
					  	<input type="text" name="chequeName" id="chequeName" <c:if test="${payment.paymentType=='cheque'}">value="${payment.name}"</c:if> class="form-control"/> 
					</div>
					<div class="col-sm-3">
						<label>Routing no. Last 4 Digit</label>
						
					  	<input type="text" name="routingChequeNo" id="routingChequeNo" <c:if test="${payment.paymentType=='cheque'}">value="${payment.routingLastFourDigit}"</c:if> class="form-control"/>
					</div>
					<div class="col-sm-3">
						<label>Account no. Last 4 Digit</label>
					  	<input type="text" name="accountChequeNo" id="accountChequeNo" <c:if test="${payment.paymentType=='cheque'}">value="${payment.accountLastFourDigit}"</c:if>  class="form-control"/>
					</div>
					<div class="col-sm-3">
						<label>Cheque No.</label>
					  	<input type="text" name="checkNo" id="checkNo" value="${payment.chequeNo}" class="form-control"/> 
					</div>
				</div>
				</div>
				</div>
				<div class="row">
				<div class="col-lg-12">
				<div class="form-group">
					<div class="col-sm-4">
						<label>Payment Date</label>
					  	<input type="text" name="paymentDate" id="paymentDate" value="${payment.paymentDateStr}" class="form-control"/> 
					</div>
					<div class="col-sm-4">
						<label>payment Status</label>
					  	<select name="paymentStatus" class="form-control">
					  		<option value="Paid" <c:if test="${payment.paymentStatus=='Paid'}">selected</c:if>>Paid</option>
					  		<option value="Pending"  <c:if test="${payment.paymentStatus=='Pending'}">selected</c:if>>Pending</option>
					  	</select> 
					</div>
					<div class="col-sm-4">
						<label>Payment Note</label>
					  	<textarea name="paymentNote" id="paymentNote" rows="4" cols="50" class="form-control">${payment.paymentNote}</textarea>
					</div>
					<div class="col-sm-3"></div>
				</div>
				</div>
				</div>
			</div>
		</section>
	</div>

</div>

<div class="form-group" style="margin-top:10px;">
	<div class="col-sm-6">
		<button class="btn btn-primary" type="button"
			onclick="doEditPOValidation();">Update PO</button>
		<button class="btn btn-danger" type="button"
			onclick="doVoidPO();">Void PO</button>
		<button class="btn btn-default" onclick="cancelAction();"
			type="button">Cancel</button>
	</div>
</div>
</form>
</div>
<br/><br/>

<div id="eventInfo">
<div class="row">
	<header style="font-size: 13px;font-family: arial;" class="panel-heading">Select an event to add ticket </header>
	<div  class="col-lg-12">
                <!--  search form start -->
		<form:form class="form-inline" role="form" onsubmit="return false" id="eventSearch" method="post" action="${pageContext.request.contextPath}/Events">
		<input type="hidden" id="eventID" name="eventID">
			<%-- <div class="form-group col-xs-3 col-md-3">
				<label for="name" class="control-label">From Date</label> 
				<input class="form-control searchcontrol" type="text" id="fromDate" name="fromDate" value="${fromDate}">
			</div>
			<div class="form-group col-xs-3 col-md-3">
				<label for="name" class="control-label">To Date</label> 
				<input class="form-control searchcontrol" type="text" id="toDate" name="toDate" value="${toDate}">
			</div> --%>			
			<div class="form-group col-xs-2 col-md-2">			
				<label for="name" class="control-label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>			
				<!--<select id="searchType" name="searchType" class="form-control input-sm m-bot15">-->
				<select id="searchType" name="searchType" class="form-control">
					<option value="event_name">Event</option>
					<option value="artist_name" <c:if test="${searchType eq 'artist_name'}">selected</c:if> >Artist</option>
					<%-- <option value="grand_child_category_name" <c:if test="${searchType eq 'grand_child_category_name'}">selected</c:if> >GrandChildCategory</option>
					<option value="building" <c:if test="${searchType eq 'building'}">selected</c:if> >Venue</option>
					<option value="city" <c:if test="${searchType eq 'city'}">selected</c:if> >City</option>
					<option value="state" <c:if test="${searchType eq 'state'}">selected</c:if> >State</option>	 --%>			  
				</select>
			</div>
			<div class="form-group col-xs-3 col-md-3">				
				<div class="navbar-form">
					<input class="form-control searchcontrol" placeholder="Search" type="text" id="eventSearchValue" name="eventSearchValue" value="${eventSearchValue}">
				</div>
			</div>
            <div class="form-group col-xs-1 col-md-1">				
				<button type="button" id="searchEventBtn" class="btn btn-primary" onclick="searchEventData();">Search</button>
			</div>
			
              </form:form>
               <!--  search form end -->                
          </div>
</div>
<br/>
<!-- event Grid Code Starts -->
<div style="position: relative; display: none;" id="eventGridDiv">
	<div class="table-responsive grid-table">
		<div class="grid-header full-width">
			<label>Events Details</label>
			<div class="pull-right">
				<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters</a>
			</div>
		</div>
		<div id="event_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
		<div id="event_pager" style="width: 100%; height: 10px;"></div>
	</div>
</div>
<br/>

<br/>
<div class="form-group" id="addTicketDiv" style="display: none">
	<div class="col-sm-6">
		<button class="btn btn-primary" type="submit" id="addTicket" onclick="">Add Ticket</button>
	</div>
</div>
<br/>
</div>
</div>

<script>
	var pagingInfo;
	var eventDataView;
	var eventGrid;
	var eventData = [];
	var eventGridSearchString='';
	var columnFilters = {};
	var eventColumns = [ {
		id : "eventName",
		name : "Event Name",
		field : "eventName",
		sortable : true
	}, {
		id : "eventDate",
		name : "Event Date",
		field : "eventDate",
		sortable : true
	}, {
		id : "eventTime",
		name : "Event Time",
		field : "eventTime",
		sortable : true
	}, {
		id : "venue",
		name : "Venue",
		field : "venue",
		sortable : true
	}, {
		id : "city",
		name : "City",
		field : "city",
		sortable : true
	}, {
		id : "state",
		name : "State",
		field : "state",
		sortable : true
	},  {id: "country", 
		name: "Country", 
		field: "country", 
		sortable: true
	},/* {id: "artist", 
		name: "Artist", 
		field: "artist", 
		sortable: true
	},*/{id: "grandChildCategory", 
		name: "Grand Child Category", 
		field: "grandChildCategory", 
		sortable: true
	},{id: "childCategory", 
		name: "Child Category", 
		field: "childCategory", 
		sortable: true
	}, {
		id : "parentCategory",
		field : "parentCategory",
		name : "Parent Category",
		sortable: true
	} ];

	var eventOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var eventGridSortcol = "eventName";
	var eventGridSortdir = 1;
	var percentCompleteThreshold = 0;
	
	function deleteRecordFromEventGrid(id) {
		eventDataView.deleteItem(id);
		eventGrid.invalidate();
	}
	/*
	function eventGridFilter(item, args) {
		var x= item["customerName"];
		if (args.eventGridSearchString  != ""
				&& x.indexOf(args.eventGridSearchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.eventGridSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}
	*/
	function eventGridComparer(a, b) {
		var x = a[eventGridSortcol], y = b[eventGridSortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	/*
	function eventGridToggleFilterRow() {
		eventGrid.setTopPanelVisibility(!eventGrid.getOptions().showTopPanel);
	}
	
	//$(".grid-header .ui-icon-sg").addClass("ui-state-default-sg ui-corner-all")
	$("#event_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all")
			.mouseover(function(e) {
				$(e.target).addClass("ui-state-hover")
			}).mouseout(function(e) {
				$(e.target).removeClass("ui-state-hover")
			});
	*/
	function pagingControl(move,id){		
		var pageNo = 0;
		if(move == 'FIRST'){
			pageNo = 0;
		}else if(move == 'LAST'){
			pageNo = parseInt(pagingInfo.totalPages)-1;
		}else if(move == 'NEXT'){
			pageNo = parseInt(pagingInfo.pageNum) +1;
		}else if(move == 'PREV'){
			pageNo = parseInt(pagingInfo.pageNum)-1;
		}
		getEventGridData(pageNo);
	}
	
	function getEventGridData(pageNo) {
		$('#eventGridDiv').show();
		/*$('#addTicketDiv').show();*/
		var searchValue = $("#eventSearchValue").val();		
		$.ajax({
			url : "${pageContext.request.contextPath}/GetEventDetails",
			type : "post",
			data : $("#eventSearch").serialize()+"&pageNo="+pageNo+"&headerFilter="+eventGridSearchString,
			dataType:"json",
			/* async : false, */
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				pagingInfo = jsonData.eventPagingInfo;
				refreshEventGridValues(jsonData.events);
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function refreshEventGridValues(jsonData) {
		eventData = [];
		for (var i = 0; i < jsonData.length; i++) {
			var  data= jsonData[i]; 
			var d = (eventData[i] = {});
			d["id"] = i;
			d["eventID"] = data.eventId;
			d["eventName"] = data.eventName;
			d["eventDate"] = data.eventDateStr;
			d["eventTime"] = data.eventTimeStr;
			d["venue"] = data.building;
			d["city"] = data.city;
			d["state"] = data.state;
			d["country"] = data.country;
			d["grandChildCategory"] = data.grandChildCategoryName;
			d["childCategory"] = data.childCategoryName;
			d["parentCategory"] = data.parentCategoryName;
		}
		
		eventDataView = new Slick.Data.DataView();
		eventGrid = new Slick.Grid("#event_grid", eventDataView, eventColumns, eventOptions);
		eventGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		eventGrid.setSelectionModel(new Slick.RowSelectionModel());
		if(pagingInfo != null){
			var eventGridPager = new Slick.Controls.Pager(eventDataView, eventGrid, $("#event_pager"),pagingInfo);
		}
		var eventGridColumnpicker = new Slick.Controls.ColumnPicker(eventColumns, eventGrid,
				eventOptions);

		// move the filter panel defined in a hidden div into eventGrid top panel
		//$("#event_inlineFilterPanel").appendTo(eventGrid.getTopPanel()).show();

		/*eventGrid.onKeyDown.subscribe(function(e) {
			// select all rows on ctrl-a
			if (e.which != 65 || !e.ctrlKey) {
				return false;
			}
			var rows = [];
			for ( var i = 0; i < eventDataView.getLength(); i++) {
				rows.push(i);
			}
			eventGrid.setSelectedRows(rows);
			e.preventDefault();
		});*/
		
		eventGrid.onSort.subscribe(function(e, args) {
			eventGridSortdir = args.sortAsc ? 1 : -1;
			eventGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				eventDataView.fastSort(eventGridSortcol, args.sortAsc);
			} else {
				eventDataView.sort(eventGridComparer, args.sortAsc);
			}
		});
		// wire up model events to drive the eventGrid
		eventDataView.onRowCountChanged.subscribe(function(e, args) {
			eventGrid.updateRowCount();
			eventGrid.render();
		});
		eventDataView.onRowsChanged.subscribe(function(e, args) {
			eventGrid.invalidateRows(args.rows);
			eventGrid.render();
		});

		$(eventGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
			eventGridSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				  columnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in columnFilters) {
					  if (columnId !== undefined && columnFilters[columnId] !== "") {
						  eventGridSearchString += columnId + ":" +columnFilters[columnId]+",";
					  }
					}
					getEventGridData(0);
				}
			  }		 
		});
		eventGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id == 'eventTime'){
					$("<input type='text' placeholder='hh:mm a'>")
				   .data("columnId", args.column.id)
				   .val(columnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else if(args.column.id == 'eventDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(columnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else{
					$("<input type='text'>")
				   .data("columnId", args.column.id)
				   .val(columnFilters[args.column.id])
				   .appendTo(args.node);
				}
			}				
		});
		eventGrid.init();
		/*
		// wire up the search textbox to apply the filter to the model
		$("#eventGridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			eventGridSearchString = this.value;
			updateEventGridFilter();
		});
		function updateEventGridFilter() {
			eventDataView.setFilterArgs({
				eventGridSearchString : eventGridSearchString
			});
			eventDataView.refresh();
		}
		*/
		// initialize the model after all the events have been hooked up
		eventDataView.beginUpdate();
		eventDataView.setItems(eventData);
		/*eventDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			eventGridSearchString : eventGridSearchString
		});
		eventDataView.setFilter(eventGridFilter);*/
		eventDataView.endUpdate();
		eventDataView.syncGridSelection(eventGrid, true);
		$("#gridContainer").resizable();
		$("div#divLoading").removeClass('show');
		eventGrid.resizeCanvas();
		/* var eventrowIndex;
		eventGrid.onSelectedRowsChanged.subscribe(function() { 
			var temprEventRowIndex = eventGrid.getSelectedRows([0])[0];
			if (temprEventRowIndex != eventrowIndex) {
				eventrowIndex = temprEventRowIndex;
				 var eventId =eventGrid.getDataItem(temprEventRowIndex).id;
				//jAlert(eventId);
				//getCategoryTicketGroupsforEvent(eventId);
			}
		}); */
		eventGrid.onSelectedRowsChanged.subscribe(function() { 
			$('#addTicketDiv').show();
		});
	}
	
	
	//Get selected event on button click
	function getEventInfoForPO(){		
		var temprEventRowIndex = eventGrid.getSelectedRows([0])[0];
		if(temprEventRowIndex == null){
			jAlert("Please select event");
		}{			
		var eventId = eventGrid.getDataItem(temprEventRowIndex).eventID;
		var eventName = eventGrid.getDataItem(temprEventRowIndex).eventName;
		var eventDate = eventGrid.getDataItem(temprEventRowIndex).eventDate;
		var venue = eventGrid.getDataItem(temprEventRowIndex).eventDate;
		var eventTime = eventGrid.getDataItem(temprEventRowIndex).eventTime;
		//jAlert(eventId+","+eventName+","+eventDate+","+venue);	
		//$('#eventId').val(eventId);
		//$('#eventName').text(eventName);
		//$('#eventDate').text(eventDate);
		//$('#venue').text(venue);
		
		//Add dynamic rows to table for selected event
		var myTable = document.getElementById("myTable");
        var currentIndex = myTable.rows.length;
        var currentRow = myTable.insertRow(-1);
        
        var input1 = document.createElement("input");
		input1.setAttribute("type", "hidden");
		input1.setAttribute("name", "rowId_" + currentIndex);
		input1.setAttribute("value", currentIndex);
		//input1.setAttribute("id","display");

		var input = document.createElement("input");
		input.setAttribute("type", "hidden");
		input.setAttribute("name", "ticketGroup_"+currentIndex+"_"+ eventId);
		input.setAttribute("value", eventId);
		input.setAttribute("id","display");

	    var eventNameLabel = document.createElement("Label");
		eventNameLabel.setAttribute("for","event_"+eventId);
		eventNameLabel.innerHTML = eventName+" "+eventDate+" "+eventTime;
		
    	/* var eventDateLabel = document.createElement("Label");
    	eventDateLabel.setAttribute("for","event_"+eventId);
    	eventDateLabel.innerHTML = eventDate;

    	var venueLabel = document.createElement("Label");
    	venueLabel.setAttribute("for","event_"+eventId);
    	venueLabel.innerHTML = venue; */
    	
    	var sectionBox = document.createElement("input");
    	sectionBox.setAttribute("name", "section_" +currentIndex+"_"+ eventId);
    	sectionBox.setAttribute("id","section_"+currentIndex+"_"+eventId);
    	sectionBox.setAttribute("class","section");
    	
    	var row = document.createElement("input");
    	row.setAttribute("name", "row_" +currentIndex+"_"+ eventId);
    	row.setAttribute("id","row_"+currentIndex+"_"+eventId);
    	row.setAttribute("class","rowValue");
    	
    	var seatLow = document.createElement("input");
    	seatLow.setAttribute("name", "seatLow_" +currentIndex+"_"+ eventId);
    	seatLow.setAttribute("id","seatLow_"+currentIndex+"_"+eventId);
    	seatLow.setAttribute("class","seatLow"); 
    	
    	var seatHigh = document.createElement("input");
    	seatHigh.setAttribute("name", "seatHigh_" +currentIndex+"_"+ eventId);
    	seatHigh.setAttribute("id","seatHigh_"+currentIndex+"_"+eventId);
    	seatHigh.setAttribute("class","seatHigh");
    	
    	var qty = document.createElement("input");
    	qty.setAttribute("type", "number");
    	qty.setAttribute("name", "qty_" +currentIndex+"_"+ eventId);
    	qty.setAttribute("id","qty_"+currentIndex+"_"+eventId);
		qty.setAttribute("class","qty");
		qty.setAttribute("onkeyup","sumQty(this);");
    	
    	var price = document.createElement("input");
    	//price.setAttribute("type", "number");
    	price.setAttribute("name", "price_" +currentIndex+"_"+ eventId);
    	price.setAttribute("id","price_"+currentIndex+"_"+eventId);
		price.setAttribute("class","price");
		price.setAttribute("onkeyup","sumPrice(this);");
    	
        var addRowBox = document.createElement("input");
        addRowBox.setAttribute("type", "button");
        addRowBox.setAttribute("value", "Remove");
        addRowBox.setAttribute("onclick", "removeField(this); sumQty(this); sumPrice(this);");
        addRowBox.setAttribute("class", "button");

		currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(input1);
		currentCell.appendChild(input);

        var currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(eventNameLabel);

		/* currentCell = currentRow.insertCell(-1);
        currentCell.appendChild(eventDateLabel);

		currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(venueLabel); */

		currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(sectionBox);
		
		currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(row);
		
		currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(seatLow);
		
		currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(seatHigh);
		
		currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(qty);
		
		currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(price);
        
		currentCell = currentRow.insertCell(-1);
        currentCell.appendChild(addRowBox);
		
        $(window).scrollTop(300);
	}			
			/*
			var footer = myTable.createTFoot();
			var row = footer.insertRow();
			var cell = row.insertCell();
			cell.innerHTML = "Total Qty";*/			
		
	}

</script>