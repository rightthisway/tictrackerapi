<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
    <link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../resources/css/font-awesome.min.css" rel="stylesheet" />    
    <!-- Custom styles -->
	<link href="../resources/css/widgets.css" rel="stylesheet">
    <link href="../resources/css/style.css" rel="stylesheet">
    <link href="../resources/css/style-responsive.css" rel="stylesheet" />
    <link href="../resources/css/jquery.alerts.css" rel=" stylesheet">
    
	<script src="../resources/js/jquery-1.8.3.min.js"></script>
	<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
	<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
    <script src="../resources/js/bootstrap.min.js"></script>
    <script src="../resources/js/jquery.scrollTo.min.js"></script>
    <script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    
<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>
  <link href="../resources/css/datepicker.css" rel="stylesheet">
  
  <script src="../resources/js/slick/lib/firebugx.js"></script>
<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>
<script src="../resources/js/jquery.alerts.js"></script>

<script>

window.onresize = function(){
	promoCodeGrid.resizeCanvas();
}
</script>

<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i> Affiliates
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Affiliates</a>
			</li>
			<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Promotional Code</li>
		</ol>
	</div>
</div>
<br />

<c:if test="${successMessage != null}">
	<div class="alert alert-success fade in">
		<strong
			style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${successMessage}</strong>
	</div>
</c:if>
<c:if test="${errorMessage != null}">
	<div class="alert alert-block alert-danger fade in">
		<strong
			style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${errorMessage}</strong>
	</div>
</c:if>

<div style="position: relative" id="ctd">
	<div style="width: 100%;">
		<div class="grid-header" style="width: 100%">
			<label>Promotional Code History</label> <!--<span id="promoCode_grid_toogle_search" style="float: right"
				class="ui-icon ui-icon-search" title="Toggle search panel" onclick="promoCodeGridToggleFilterRow()"></span>-->
		</div>
		<div id="promocode_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
		<div id="promocode_pager" style="width: 100%; height: 10px;"></div>
	</div>
</div>
<br/><br/>
<!--
<div id="promocode_inlineFilterPanel" style="display: none; background: #dddddd; padding: 3px; color: black;">
				Show records with ID <input type="text" id="promoCodeGridSearch">-->
</div>
<script>
var selectedRow='';
var id='';
var promoCodeDataView;
var pagingInfo=JSON.parse(JSON.stringify(${pagingInfo}));
var promoCodeGrid;
var promoCodeData = [];
var promoCodeColumns = [ {
	id : "id",
	name : "ID",
	field : "id",
	sortable : true
},{
	id : "promoCode",
	name : "Promotional Code",
	field : "promoCode",
	sortable : true
},/* {
	id : "createdDate",
	name : "Created Date",
	field : "createdDate",
	sortable : true
},{
	id : "lastUpdate",
	name : "Last Updated",
	field : "lastUpdate",
	sortable : true
}, */ {
	id : "status",
	name : "Status",
	field : "status",
	width:80,
	sortable : true
}, {
	id : "fromDate",
	name : "From Date",
	field : "fromDate",
	width:80,
	sortable : true
}, {
	id : "toDate",
	name : "To Date",
	field : "toDate",
	width:80,
	sortable : true
}];

var promoCodeOptions = {
	enableCellNavigation : true,
	forceFitColumns : true,
	multiSelect : false,
	topPanelHeight : 25,
};
	
var promoCodeSortCol = "id";
var promoCodeSortdir = 1;
var promoCodeSearchString = "";
var percentCompleteThreshold = 0;

function promoCodeFilter(item, args) {
	var x = item["id"];
	if (args.promoCodeSearchString  != ""
			&& x.indexOf(args.promoCodeSearchString) == -1) {
		
		if (typeof x === 'string' || x instanceof String) {
			if(x.toLowerCase().indexOf(args.promoCodeSearchString.toLowerCase()) == -1) {
				return false;
			}
		} else {
			return false;
		}
	}
	return true;
}

function promoCodeComparer(a, b) {
	var x = a[promoCodeSortCol], y = b[promoCodeSortCol];
	if(!isNaN(x)){
	   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	}
	if(x == '' || x == null) {
		return 1;
	} else if(y == '' || y == null) {
		return -1;
	}
	if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
		return x.toLowerCase().localeCompare(y.toLowerCase());	
	} else {
		return (x == y ? 0 : (x > y ? 1 : -1));	
	}
}

function promoCodeGridToggleFilterRow() {
		promoCodeGrid.setTopPanelVisibility(!promoCodeGrid.getOptions().showTopPanel);
	}

//$(".grid-header .ui-icon").addClass("ui-state-default-sg ui-corner-all")
	$("#promoCode_grid_toogle_search").addClass(
			"ui-state-default-sg ui-corner-all").mouseover(function(e) {
		$(e.target).addClass("ui-state-hover");
	}).mouseout(function(e) {
		$(e.target).removeClass("ui-state-hover");
	});
	
function refreshPromoCodeGridValues(jsonData) {
		promoCodeData=[];
		if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i];			
				var d = (promoCodeData[i] = {});
				d["id"] = data.id;	
				d["promoCode"] = data.promoCode;
				/* d["createdDate"] = data.createDate;
				d["lastUpdate"] = data.lastUpdated; */
				d["status"] = data.status;
				d["fromDate"] = data.fromDate;
				d["toDate"] = data.toDate;
			}
		}
		
		promoCodeDataView = new Slick.Data.DataView({
			inlineFilters : true
		});
		promoCodeGrid = new Slick.Grid("#promocode_grid", promoCodeDataView, promoCodeColumns, promoCodeOptions);
		promoCodeGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		promoCodeGrid.setSelectionModel(new Slick.RowSelectionModel());		
		var promoCodePager = new Slick.Controls.Pager(promoCodeDataView, promoCodeGrid, $("#promocode_pager"),pagingInfo);
		var promoCodeColumnPicker = new Slick.Controls.ColumnPicker(promoCodeColumns, promoCodeGrid,
			promoCodeOptions);
		
		// move the filter panel defined in a hidden div into eventGrid top panel
		$("#promocode_inlineFilterPanel").appendTo(promoCodeGrid.getTopPanel()).show();
		
		promoCodeGrid.onSort.subscribe(function(e, args) {
		promoCodeSortdir = args.sortAsc ? 1 : -1;
		promoCodeSortCol = args.sortCol.field;
		if ($.browser.msie && $.browser.version <= 8) {
			promoCodeDataView.fastSort(promoCodeSortCol, args.sortAsc);
		} else {
			promoCodeDataView.sort(promoCodeComparer, args.sortAsc);
		}
	});
	// wire up model events to drive the eventGrid
	promoCodeDataView.onRowCountChanged.subscribe(function(e, args) {
		promoCodeGrid.updateRowCount();
		promoCodeGrid.render();
	});
	promoCodeDataView.onRowsChanged.subscribe(function(e, args) {
		promoCodeGrid.invalidateRows(args.rows);
		promoCodeGrid.render();
	});
	
	// wire up the search textbox to apply the filter to the model
		$("#promoCodeGridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			promoCodeSearchString = this.value;
			updatePromoCodeGridFilter();
		});
		
	function updatePromoCodeGridFilter() {
		promoCodeDataView.setFilterArgs({
			promoCodeSearchString : promoCodeSearchString
		});
		promoCodeDataView.refresh();
	}
	var promoCodeRowIndex;
	promoCodeGrid.onSelectedRowsChanged.subscribe(function() {
		var temprEventRowIndex = promoCodeGrid.getSelectedRows([ 0 ])[0];
			if (temprEventRowIndex != promoCodeRowIndex) {
				promoCodeRowIndex = temprEventRowIndex;
			}
			
			selectedRow = promoCodeGrid.getSelectedRows([0])[0];
			if (selectedRow >=0) {
				id = promoCodeGrid.getDataItem(temprEventRowIndex).id;
			}else{
				id='';
			}			
		});

	// initialize the model after all the events have been hooked up
	promoCodeDataView.beginUpdate();
	promoCodeDataView.setItems(promoCodeData);
	
	promoCodeDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			promoCodeSearchString : promoCodeSearchString
		});
	promoCodeDataView.setFilter(promoCodeFilter);
		
	promoCodeDataView.endUpdate();
	promoCodeDataView.syncGridSelection(promoCodeGrid, true);
	$("#gridContainer").resizable();
	promoCodeGrid.resizeCanvas();
	$("div#divLoading").removeClass('show');	
}

window.onload = function() {
	refreshPromoCodeGridValues(${promoCodeHistory});
};
</script>