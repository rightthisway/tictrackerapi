<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
    <link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../resources/css/font-awesome.min.css" rel="stylesheet" />    
    <!-- Custom styles -->
	<link href="../resources/css/widgets.css" rel="stylesheet">
    <link href="../resources/css/style.css" rel="stylesheet">
    <link href="../resources/css/style-responsive.css" rel="stylesheet" />
    <link href="../resources/css/jquery.alerts.css" rel=" stylesheet">
    
	<script src="../resources/js/jquery-1.8.3.min.js"></script>
	<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
	<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
    <script src="../resources/js/bootstrap.min.js"></script>
    <script src="../resources/js/jquery.scrollTo.min.js"></script>
    <script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    
<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>
  <link href="../resources/css/datepicker.css" rel="stylesheet">
  
  <script src="../resources/js/slick/lib/firebugx.js"></script>
<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>
<script src="../resources/js/jquery.alerts.js"></script>

<script>

window.onresize = function(){
	cstrGrid.resizeCanvas();
}
</script>

<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i> Accounting
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Invoice</a>
			</li>
			<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Stripe Refund Detail</li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<c:if test="${successMessage != null}">
			<div class="alert alert-success fade in">
				<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${successMessage}</strong>
			</div>
		</c:if>
		<c:if test="${errorMessage != null}">
			<div class="alert alert-block alert-danger fade in">
				<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${errorMessage}</strong>
			</div>
		</c:if>
	</div>
</div>
<br />

<div style="position: relative" id="cstr">
	<div style="width: 100%;">
		<div class="grid-header" style="width: 100%">
			<label>Stripe Refund Informations</label> <span id="openOrder_grid_toogle_search" style="float: right"
				class="ui-icon ui-icon-search" title="Toggle search panel" onclick="openOrderGridToggleFilterRow()"></span>
		</div>
		<div id="cstr_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
		<div id="cstr_pager" style="width: 100%; height: 10px;"></div>
	</div>
</div>
<br/><br/>

<div id="refund_inlineFilterPanel" style="display: none; background: #dddddd; padding: 3px; color: black;">
				Show records with Stripe Refund <input type="text" id="openOrderGridSearch">
</div>
<script>
var selectedRow='';
var id='';
var cstrDataView;
var cstrGrid;
var pagingInfo=JSON.parse(JSON.stringify(${pagingInfo}));
var refundGroupId="${orderId}";
var cstrData = [];
var cstrColumns = [ /*{id : "id",name : "ID",field : "id",sortable : true},*/
	{
	id : "orderId",
	name : "Order Id",
	field : "orderId",
	sortable : true
},{
	id : "refundType",
	name : "Refund Type",
	field : "refundType",
	sortable : true
},{
	id : "transactionId",
	name : "Transaction Id",
	field : "transactionId",
	sortable : true
},{
	id : "refundId",
	name : "Refund Id",
	field : "refundId",
	sortable : true
}, {
	id : "amount",
	name : "Amount",
	field : "amount",
	width:80,
	sortable : true
},{
	id : "status",
	name : "Status",
	field : "status",
	sortable : true
},{
	id : "reason",
	name : "Reason",
	field : "reason",
	sortable : true
},{
	id : "revertedUsedRewardPoints",
	name : "Used Reward Points",
	field : "revertedUsedRewardPoints",
	sortable : true
},{
	id : "revertedEarnedRewardPoints",
	name : "Earned Reward Points",
	field : "revertedEarnedRewardPoints",
	sortable : true
},{
	id : "createdTime",
	name : "Refund Date",
	field : "createdTime",
	sortable : true
},{
	id : "refundedBy",
	name : "Refunded By",
	field : "refundedBy",
	sortable : true
}];

var cstrOptions = {
	enableCellNavigation : true,
	forceFitColumns : true,
	multiSelect : false,
	topPanelHeight : 25,
};
	
var cstrSortcol = "id";
var cstrSortdir = 1;
var cstrSearchString = "";
var percentCompleteThreshold = 0;

function cstrFilter(item, args) {
	var x= item["id"];
	if (args.cstrSearchString  != ""
			&& x.indexOf(args.cstrSearchString) == -1) {
		
		if (typeof x === 'string' || x instanceof String) {
			if(x.toLowerCase().indexOf(args.cstrSearchString.toLowerCase()) == -1) {
				return false;
			}
		} else {
			return false;
		}
	}
	return true;
}

function cstrComparer(a, b) {
	var x = a[cstrSortcol], y = b[cstrSortcol];
	if(!isNaN(x)){
	   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	}
	if(x == '' || x == null) {
		return 1;
	} else if(y == '' || y == null) {
		return -1;
	}
	if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
		return x.toLowerCase().localeCompare(y.toLowerCase());	
	} else {
		return (x == y ? 0 : (x > y ? 1 : -1));	
	}
}

function openOrderGridToggleFilterRow() {
		cstrGrid.setTopPanelVisibility(!cstrGrid.getOptions().showTopPanel);
	}

//$(".grid-header .ui-icon").addClass("ui-state-default-sg ui-corner-all")
	$("#openOrder_grid_toogle_search").addClass(
			"ui-state-default-sg ui-corner-all").mouseover(function(e) {
		$(e.target).addClass("ui-state-hover");
	}).mouseout(function(e) {
		$(e.target).removeClass("ui-state-hover");
	});
	
function refreshcstrGridValues() {
		var i = 0;
		<c:forEach var="cstr" items="${invoiceRefund}">
			var d = (cstrData[i] = {});
			d["id"] = '${cstr.id}';	
			d["refundType"] = '${cstr.refundType}';
			d["orderId"] = '${cstr.orderId}';
			d["transactionId"] = '${cstr.transactionId}';
			d["refundId"] = '${cstr.refundId}';
			d["amount"] = '${cstr.amount}';
			d["status"] = '${cstr.status}';
			d["reason"] = '${cstr.reason}';
			d["revertedUsedRewardPoints"] = '${cstr.revertedUsedRewardPoints}';
			d["revertedEarnedRewardPoints"] = '${cstr.revertedEarnedRewardPoints}';
			d["createdTime"] = '${cstr.createdTime}';
			d["refundedBy"] = '${cstr.refundedBy}';
			i++;
		</c:forEach>
	
			
		cstrDataView = new Slick.Data.DataView({
			inlineFilters : true
		});
		cstrGrid = new Slick.Grid("#cstr_grid", cstrDataView, cstrColumns, cstrOptions);
		cstrGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		cstrGrid.setSelectionModel(new Slick.RowSelectionModel());		
		var cstrPager = new Slick.Controls.Pager(cstrDataView, cstrGrid, $("#cstr_pager"),pagingInfo);
		var cstrColumnpicker = new Slick.Controls.ColumnPicker(cstrColumns, cstrGrid,
			cstrOptions);
		
		// move the filter panel defined in a hidden div into eventGrid top panel
		$("#refund_inlineFilterPanel").appendTo(cstrGrid.getTopPanel()).show();
		
		cstrGrid.onSort.subscribe(function(e, args) {
		cstrSortdir = args.sortAsc ? 1 : -1;
		cstrSortcol = args.sortCol.field;
		if ($.browser.msie && $.browser.version <= 8) {
			cstrDataView.fastSort(cstrSortcol, args.sortAsc);
		} else {
			cstrDataView.sort(cstrComparer, args.sortAsc);
		}
	});
	// wire up model events to drive the eventGrid
	cstrDataView.onRowCountChanged.subscribe(function(e, args) {
		cstrGrid.updateRowCount();
		cstrGrid.render();
	});
	cstrDataView.onRowsChanged.subscribe(function(e, args) {
		cstrGrid.invalidateRows(args.rows);
		cstrGrid.render();
	});
	
	// wire up the search textbox to apply the filter to the model
		$("#openOrderGridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			cstrSearchString = this.value;
			updateCSTRGridFilter();
		});
		
	function updateCSTRGridFilter() {
		cstrDataView.setFilterArgs({
			cstrSearchString : cstrSearchString
		});
		cstrDataView.refresh();
	}
	var eventrowIndex;
	cstrGrid.onSelectedRowsChanged.subscribe(function() {
		var temprEventRowIndex = cstrGrid.getSelectedRows([ 0 ])[0];
			if (temprEventRowIndex != eventrowIndex) {
				eventrowIndex = temprEventRowIndex;
				//getCustomerInfoForInvoice(temprEventRowIndex);
			}
			
			selectedRow = cstrGrid.getSelectedRows([0])[0];
			if (selectedRow >=0) {
				id = cstrGrid.getDataItem(temprEventRowIndex).id;
			}else{
				id='';
			}			
		});

	// initialize the model after all the events have been hooked up
	cstrDataView.beginUpdate();
	cstrDataView.setItems(cstrData);
	
	cstrDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			cstrSearchString : cstrSearchString
		});
	cstrDataView.setFilter(cstrFilter);
		
	cstrDataView.endUpdate();
	cstrDataView.syncGridSelection(cstrGrid, true);
	$("#gridContainer").resizable();
	cstrGrid.resizeCanvas();
	$("div#divLoading").removeClass('show');
	if(refundGroupId>0){
		for(var i=0;i<cstrGrid.getDataLength();i++){
			if(cstrGrid.getDataItem(i).id==refundGroupId){
				cstrGrid.setSelectedRows([i]);
				break;
			}
		}
	}
	
}
window.onload = function() {
	refreshcstrGridValues();
	
};
</script>