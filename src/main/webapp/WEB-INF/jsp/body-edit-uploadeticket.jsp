<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<!-- Bootstrap CSS -->    
    <link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../resources/css/font-awesome.min.css" rel="stylesheet" />    
    <!-- full calendar css-->
    <link href="../resources/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="../resources/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
    <!-- easy pie chart-->
    <link href="../resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <!-- owl carousel -->
    <link rel="stylesheet" href="../resources/css/owl.carousel.css" type="text/css">
	<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
	<link rel="stylesheet" href="../resources/css/fullcalendar.css">
	<link href="../resources/css/widgets.css" rel="stylesheet">
    
    <link href="../resources/css/style-responsive.css" rel="stylesheet" />
	<link href="../resources/css/xcharts.min.css" rel=" stylesheet">	
	
	<link href="../resources/css/jquery.alerts.css" rel=" stylesheet">
	
	<!--<link href="../resources/css/jquery-ui-1.10.4.min.css" rel="stylesheet">--> 
	<link href="../resources/css/style.css" rel="stylesheet">
	
	 <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->
    
    <script src="../resources/js/jquery-1.8.3.min.js"></script>
    <%-- <script type="text/javascript" src="../resources/js/jquery-ui-1.9.2.custom.min.js"></script> --%>
    
    <!-- bootstrap -->
    <script src="../resources/js/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script src="../resources/js/jquery.scrollTo.min.js"></script>
    <script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../resources/js/jquery.alerts.js"></script>
    
    <!-- slickgrid related scripts -->
	<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
	<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
	<script src="../resources/js/slick/slick.core.js"></script>
	<script src="../resources/js/slick/slick.formatters.js"></script>
	<script src="../resources/js/slick/slick.editors.js"></script>
	<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
	<script src="../resources/js/slick/slick.grid.js"></script>
	<script src="../resources/js/slick/slick.dataview.js"></script>
	<script src="../resources/js/slick/controls/slick.pager.js"></script>
	<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
	<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

	<link href="../resources/css/datepicker.css" rel="stylesheet">
	<script src="../resources/js/bootstrap-datepicker.js"></script>

	<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>
	<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>

<style>
input {
	color: black !important;
}

.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}
.slick-headerrow-column {
      background: #87ceeb;
      text-overflow: clip;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }
    .slick-headerrow-column input {
      margin: 0;
      padding: 0;
      width: 100%;
      height: 100%;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }
.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}
.slick-viewport {
    overflow-x: hidden !important;
}
/*

input{
		color : black !important;
	}
*/	
#totalPrice{
  font-family: arial;
  font-size: 10pt;
  font-weight: bold;
}

#totalQty{
  font-family: arial;
  font-size: 10pt;
  font-weight: bold;
}


p.textWrap {
    width: 11em; 
    border: 1px;
    word-wrap: break-word;
}
</style>

<script type="text/javascript">

$(document).ready(function(){
		$("div#divLoading").addClass('show');
		
		$('#addTicket').click(function(){
			$('#myTable').show();
			getEventInfoForPO();
		});
		
		$(window).resize(function() {
			eventGrid.resizeCanvas();
		});
		
		$('.searchcontrol').keypress(function (event) {
		var keyCode = (event.keyCode ? event.keyCode : event.which);
		 if(keyCode == 13)  // the enter key code
		  {
			 getEventGridData(0);
			//$('#searchEventBtn').click();
			//return false;  
		  }
		});
				
	});
	
	window.onunload = function () {
	    var win = window.opener;
	    if (!win.closed) {
	    	window.opener.getPOGridData(0);
	    }
	};
	
	function doEditPOValidation(){
		$("#action").val('uploadPOInfo');
		$("#purchaseOrderForm").submit();		
	}
	
	//Edit PO cancel action
	function cancelAction(){
		window.close();
		window.opener.location.reload(true);
		window.location.href = "${pageContext.request.contextPath}/Accounting/ManagePO";
	}
	
	function removeField(id){
		//jAlert('remove');
		var p=id.parentNode.parentNode;
   	 	p.parentNode.removeChild(p);
	}
	
	function downloadTicketFile(ticketGroupId,fileType,position){
		 var url = "${pageContext.request.contextPath}/Accounting/DownloadETicket?ticketGroupId="+ticketGroupId+"&fileType="+fileType+"&position="+position;
		 $('#download-frame').attr('src', url);
	}
</script>
<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
<div class="row">
	<div class="col-lg-12">
		<h3 style="font-size: 13px;font-family: arial;" class="page-header">
			<i class="fa fa-laptop"></i> ACCOUNTING
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#"
				onclick="window.close(); window.opener.location.reload(true);">Accounting</a>
			</li>
			<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Edit Purchase Order</li>
		</ol>

	</div>
</div>

<div id="poInfo">
<div class="row">
	<div class="col-lg-12">
		<section class="panel"> <header style="font-size: 13px;font-family: arial;" class="panel-heading">
		Customer Info </header>
		<div class="panel-body">
			<c:if test="${successMessage != null}">
				<div class="alert alert-success fade in">
					<strong
						style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${successMessage}</strong>
				</div>
			</c:if>
			<c:if test="${errorMessage != null}">
				<div class="alert alert-block alert-danger fade in">
					<strong
						style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${errorMessage}</strong>
				</div>
			</c:if>
			
				<div class="form-group">
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">PO ID:&nbsp;</label>
					<span style="font-size: 15px;">${purchaseOrder.id}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">PO Total:&nbsp;</label>
					<span style="font-size: 15px;">${purchaseOrder.poTotal}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Customer Name:&nbsp;</label>
					<span style="font-size: 15px;">${customerInfo.customerName}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Created Date:&nbsp; </label>
					<span style="font-size: 15px;">${purchaseOrder.createTimeStr}</span>
				</div>				
			</div>
			<div class="form-group">
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">CSR:&nbsp;</label>
					<span style="font-size: 15px;">${purchaseOrder.csr}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Quantity:&nbsp;</label>
					<span style="font-size: 15px;">${purchaseOrder.ticketCount}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Shipping Method:&nbsp;</label>
					<span style="font-size: 15px;">
					<c:forEach items="${shippingMethods}" var="shippingMethod">
						 <c:if test="${shippingMethod.id==purchaseOrder.shippingMethodId}">  ${shippingMethod.name} </c:if>					
					</c:forEach>
					</span>
				</div>
			</div>
		</div>
	</section>
</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<section class="panel"> <header style="font-size: 13px;font-family: arial;" class="panel-heading">
		Ticket Info </header>
		<div class="panel-body">				
			<div class="form-group">
				<form id="purchaseOrderForm" name="purchaseOrderForm"
				method="post" enctype="multipart/form-data"
				action="${pageContext.request.contextPath}/Accounting/UploadEticket">
				<input class="form-control" id="action" value="action" name="action" type="hidden" /> 
				<input class="form-control" id="purchaseOrderId" value="${purchaseOrder.id}" name="poId" type="hidden" /> 
				<input class="form-control" id="customerId" value="${purchaseOrder.customerId}" name="customerId" type="hidden" />
					<input type="hidden" name="action" value="action" id="action" /> <input
						type="hidden" name="customerId" value="${customer.id}"
						id="customerId" />
						<table id="myTable"
							class="table table-striped table-advance table-hover">
							<tbody>
								<tr>
									<th>
										<!-- S.No -->
									</th>
									 <th style="font-size: 13px;font-family: arial;">Event Name</th>
									<th style="font-size: 13px;font-family: arial;">Section</th>
									<th style="font-size: 13px;font-family: arial;">Row</th>
									<th style="font-size: 13px;font-family: arial;">Seat Low</th>
									<th style="font-size: 13px;font-family: arial;">Seat High</th>
									<th style="font-size: 13px;font-family: arial;">Qty</th>
									<th style="font-size: 13px;font-family: arial;">Price</th>
									<th style="font-size: 13px;font-family: arial;">Instant Download</th>
									<th style="font-size: 13px;font-family: arial;">Upload</th>
									<!--<th style="font-size: 13px;font-family: arial;">Action</th>-->
								</tr>
								<!-- dynamic rows -->
								<!--<h4 id="totalQty" style="float:right;">Total qty</h4>-->
								<c:forEach var="ticketGroup" items="${ticketGroup}" varStatus="theCount">
									<input type="hidden" id="rowId_${theCount.count}" name="rowId_${theCount.count}" value="${ticketGroup.eventId}">
									
									<%-- <input type="text" name="eventId_${ticketGroup.eventId}" value=""/> --%>
									<tr>
										<td>
											<%-- ${theCount.count} --%>
										</td>
										<td><label name="event_${ticketGroup.id}">
											${ticketGroup.events.eventName} ${ticketGroup.events.eventDateStr} ${ticketGroup.events.eventTimeStr}</label>
										</td>
										<td><input type="text" disabled="true" class="section" name="section_${theCount.count}_${ticketGroup.id}"
											value="${ticketGroup.section}" />
										</td>
										<td><input type="text" disabled="true" class="rowValue" name="row_${theCount.count}_${ticketGroup.id}"
											size="10" value="${ticketGroup.row}" />
										</td>
										<td><input type="text" disabled="true" class="seatLow" name="seatLow_${theCount.count}_${ticketGroup.id}"
											value="${ticketGroup.seatLow}" />
										</td>
										<td><input type="text" disabled="true" class="seatHigh" name="seatHigh_${theCount.count}_${ticketGroup.id}"
											value="${ticketGroup.seatHigh}" />
										</td>
										<td><input type="text" disabled="true" name="qty_${theCount.count}_${ticketGroup.id}"
											id="qty_${theCount.count}_${ticketGroup.id}" class="qty"
											onkeyup="sumQty(this);" value="${ticketGroup.quantity}" />
										</td>
										<td><input type="text" disabled="true" name="price_${theCount.count}_${ticketGroup.id}"
											id="price_${theCount.count}_${ticketGroup.id}" class="price"
											onkeyup="sumPrice(this);" value="${ticketGroup.price}" />
										</td>
										<td><input type="checkbox" name="instantdownload_${theCount.count}_${ticketGroup.id}"
											id="instantdownload_${theCount.count}_${ticketGroup.id}" class="instantdownload"
											value="True" />True
										</td>
										<td>
										
										<c:choose>
											<c:when test="${null ne ticketGroup.ticketGroupTicketAttachment }">
												<c:set value="${ticketGroup.ticketGroupTicketAttachment}" var="ticketAttachment" ></c:set>
											
												<input type="file" name="file_${theCount.count}_${ticketGroup.id}"
																id="file_${theCount.count}_${ticketGroup.id}" />
												<p class="textWrap">
													<a href="javascript:downloadTicketFile('${ticketAttachment.ticketGroupId}','${ticketAttachment.type}','${ticketAttachment.position}')">${ticketAttachment.filePath}</a>
												</p>
											</c:when>
											<c:otherwise>
												<input type="file" name="file_${theCount.count}_${ticketGroup.id}"
														id="file_${theCount.count}_${ticketGroup.id}" />
											</c:otherwise>
										
										
										</c:choose>											
										</td>
										
									</tr>
									<input type="hidden" name="ticketGroup_${theCount.count}_${ticketGroup.id}" />
								</c:forEach>
							</tbody>
						</table>
						</form>
					</div>
	</div>	
</section>
</div>
</div>

<div class="form-group" style="margin-top:10px;">
	<div class="col-sm-6">
		<button class="btn btn-primary" type="button"
			onclick="doEditPOValidation();">Update PO</button>		
		<button class="btn btn-default" onclick="cancelAction();"
			type="button">Cancel</button>
	</div>
</div>
</div>
<br/><br/>

<script>
	//Get selected event on button click
	function getEventInfoForPO(){		
		var temprEventRowIndex = eventGrid.getSelectedRows([0])[0];
		if(temprEventRowIndex == null){
			jAlert("Please select event");
		}{			
		var eventId = eventGrid.getDataItem(temprEventRowIndex).eventID;
		var eventName = eventGrid.getDataItem(temprEventRowIndex).eventName;
		var eventDate = eventGrid.getDataItem(temprEventRowIndex).eventDate;
		var venue = eventGrid.getDataItem(temprEventRowIndex).eventDate;
		var eventTime = eventGrid.getDataItem(temprEventRowIndex).eventTime;
		//jAlert(eventId+","+eventName+","+eventDate+","+venue);	
		//$('#eventId').val(eventId);
		//$('#eventName').text(eventName);
		//$('#eventDate').text(eventDate);
		//$('#venue').text(venue);
		
		//Add dynamic rows to table for selected event
		var myTable = document.getElementById("myTable");
        var currentIndex = myTable.rows.length;
        var currentRow = myTable.insertRow(-1);
        
        var input1 = document.createElement("input");
		input1.setAttribute("type", "hidden");
		input1.setAttribute("name", "rowId_" + currentIndex);
		input1.setAttribute("value", currentIndex);
		//input1.setAttribute("id","display");

		var input = document.createElement("input");
		input.setAttribute("type", "hidden");
		input.setAttribute("name", "ticketGroup_"+currentIndex+"_"+ eventId);
		input.setAttribute("value", eventId);
		input.setAttribute("id","display");

	    var eventNameLabel = document.createElement("Label");
		eventNameLabel.setAttribute("for","event_"+eventId);
		eventNameLabel.innerHTML = eventName+" "+eventDate+" "+eventTime;
		
    	/* var eventDateLabel = document.createElement("Label");
    	eventDateLabel.setAttribute("for","event_"+eventId);
    	eventDateLabel.innerHTML = eventDate;

    	var venueLabel = document.createElement("Label");
    	venueLabel.setAttribute("for","event_"+eventId);
    	venueLabel.innerHTML = venue; */
    	
    	var sectionBox = document.createElement("input");
    	sectionBox.setAttribute("name", "section_" +currentIndex+"_"+ eventId);
    	sectionBox.setAttribute("id","section_"+currentIndex+"_"+eventId);
    	sectionBox.setAttribute("class","section");
    	
    	var row = document.createElement("input");
    	row.setAttribute("name", "row_" +currentIndex+"_"+ eventId);
    	row.setAttribute("id","row_"+currentIndex+"_"+eventId);
    	row.setAttribute("class","rowValue");
    	
    	var seatLow = document.createElement("input");
    	seatLow.setAttribute("name", "seatLow_" +currentIndex+"_"+ eventId);
    	seatLow.setAttribute("id","seatLow_"+currentIndex+"_"+eventId);
    	seatLow.setAttribute("class","seatLow"); 
    	
    	var seatHigh = document.createElement("input");
    	seatHigh.setAttribute("name", "seatHigh_" +currentIndex+"_"+ eventId);
    	seatHigh.setAttribute("id","seatHigh_"+currentIndex+"_"+eventId);
    	seatHigh.setAttribute("class","seatHigh");
    	
    	var qty = document.createElement("input");
    	qty.setAttribute("type", "number");
    	qty.setAttribute("name", "qty_" +currentIndex+"_"+ eventId);
    	qty.setAttribute("id","qty_"+currentIndex+"_"+eventId);
		qty.setAttribute("class","qty");
		qty.setAttribute("onkeyup","sumQty(this);");
    	
    	var price = document.createElement("input");
    	//price.setAttribute("type", "number");
    	price.setAttribute("name", "price_" +currentIndex+"_"+ eventId);
    	price.setAttribute("id","price_"+currentIndex+"_"+eventId);
		price.setAttribute("class","price");
		price.setAttribute("onkeyup","sumPrice(this);");
    	
        var addRowBox = document.createElement("input");
        addRowBox.setAttribute("type", "button");
        addRowBox.setAttribute("value", "Remove");
        addRowBox.setAttribute("onclick", "removeField(this); sumQty(this); sumPrice(this);");
        addRowBox.setAttribute("class", "button");

		currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(input1);
		currentCell.appendChild(input);

        var currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(eventNameLabel);

		/* currentCell = currentRow.insertCell(-1);
        currentCell.appendChild(eventDateLabel);

		currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(venueLabel); */

		currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(sectionBox);
		
		currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(row);
		
		currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(seatLow);
		
		currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(seatHigh);
		
		currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(qty);
		
		currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(price);
        
		currentCell = currentRow.insertCell(-1);
        currentCell.appendChild(addRowBox);
		
        $(window).scrollTop(300);
	}			
			/*
			var footer = myTable.createTFoot();
			var row = footer.insertRow();
			var cell = row.insertCell();
			cell.innerHTML = "Total Qty";*/			
		
	}

</script>