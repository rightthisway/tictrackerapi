<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<!-- Bootstrap CSS -->    
    <link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../resources/css/font-awesome.min.css" rel="stylesheet" />    
    <!-- full calendar css-->
    <link href="../resources/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="../resources/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
    <!-- easy pie chart-->
    <link href="../resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <!-- owl carousel -->
    <link rel="stylesheet" href="../resources/css/owl.carousel.css" type="text/css">
	<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
	<link rel="stylesheet" href="../resources/css/fullcalendar.css">
	<link href="../resources/css/widgets.css" rel="stylesheet">
    <link href="../resources/css/style.css" rel="stylesheet">
    <link href="../resources/css/style-responsive.css" rel="stylesheet" />
	<link href="../resources/css/xcharts.min.css" rel=" stylesheet">	
	<link href="../resources/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
	<link href="../resources/css/jquery.alerts.css" rel=" stylesheet">
	<link href="../resources/css/datepicker.css" rel="stylesheet">
	
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->
    
    <!-- javascripts -->
    <%-- <script src="../resources/js/jquery.js"></script>
	<script src="../resources/js/jquery-ui-1.10.4.min.js"></script> --%>
    <script src="../resources/js/jquery-1.8.3.min.js"></script>
    <%-- <script type="text/javascript" src="../resources/js/jquery-ui-1.9.2.custom.min.js"></script> --%>
    <!-- bootstrap -->
    <script src="../resources/js/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script src="../resources/js/jquery.scrollTo.min.js"></script>
    <script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../resources/js/jquery.alerts.js"></script>
    <script src="../resources/js/bootstrap-datepicker.js"></script>
    
  <link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>


<style>
.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: #f5f5f5;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}
.slick-viewport {
    overflow-x: hidden !important;
}
/* .light_box {
	display: none;
	position: absolute;
	top: 10%;
	left: 25%;
	width: 45%;
	height: 100%;
	padding: 20px;
	border: 1px solid #888;
	background-color: #fefefe;
	z-index: 1;
	overflow: auto;
} */

#addCustomer {
  font-family: arial;
  font-size: 8pt;
  font-weight: bold;
  color: #2b2b2b;
  background-color: rgba(0, 122, 255, 0.32);
  border: 1px solid gray;
}

input{
		color : black !important;
	}
	
#totalPrice{
  font-family: arial;
  font-size: 10pt;
  font-weight: bold;
}

#totalQty{
  font-family: arial;
  font-size: 10pt;
  font-weight: bold;
}
</style>
    
<script>
	
	//function to sum the entered quantity
	function sumQty(qty){
		var sum = 0;
		$('.qty').each(function() {
			if($(this).val() != '' && !isNaN($(this).val())){
				sum += parseInt($(this).val());
			}
		});

		$('#totalQty').val(sum);
	}
	
	//function to sum the entered price
	function sumPrice(price){
		var sum = 0;
		$('.price').each(function() {
			if($(this).val() != '' && !isNaN($(this).val())){
				sum += parseInt($(this).val());
			}
		});

		$('#totalPrice').val(sum);
	}

	$(document).ready(function(){
		 $("div#divLoading").addClass('show');
		$('#myTable').hide();
		$('#poInfo').hide();
		
		$('#cardInfo').hide();
		$('#accountInfo').hide();
		$('#chequeInfo').hide();
		$('input[type=radio][name=paymentType]').change(function() {
        if (this.value == 'card') {
            $('#cardInfo').show();
			$('#accountInfo').hide();
			$('#chequeInfo').hide();
        }
        else if (this.value == 'account') {
			$('#chequeInfo').hide();           
		    $('#cardInfo').hide();
			$('#accountInfo').show();
        }else if (this.value == 'cheque') {
			$('#cardInfo').hide();
		    $('#accountInfo').hide();
            $('#chequeInfo').show();
        }
    });
		$('#addTicket').click(function(){
			$('#myTable').show();
			$('#poInfo').show();
			getEventInfoForPO();
		});
		$('#paymentDate').datepicker({
	        format: "mm/dd/yyyy",
			autoclose : true,
			orientation: "top",
			todayHighlight: true
	    });
		
		
		showHideFedexInfo();
		$('#shippingMethod').change(function(){
			showHideFedexInfo();
		});
		
		function showHideFedexInfo(){
			if($('#shippingMethod option:selected').html().toLowerCase().indexOf("fedex") >= 0){
				$('#trackingInfo').show();
			}else{
				$('#trackingInfo').hide();
			}
		}
	
		$('#searchValue').keypress(function (event) {
			var keyCode = (event.keyCode ? event.keyCode : event.which);
			 if(keyCode == 13)  // the enter key code
			  {
				 getEventGridData();
			    //$('#searchCustomerBtn').click();
			    //return false;  
			  }
		});
		
	});

	//Save purchase order
	function savePurchaseOrder(){
		if($('#shippingMethod option:selected').html().toLowerCase().indexOf("fedex") < 0){
			$('#trackingNo').val("");
		}
		/* $('.qty').each(function() {
			if($(this).val() =='' || $(this).val() ==null){
				jAlert("Please add Quantity.");
				return;
			}
		});
		
		$('.seatLow').each(function() {
			if($(this).val() =='' || $(this).val() ==null){
				jAlert("Please add Seat Low.");
				return;
			}
		});
		$('.seatHigh').each(function() {
			if($(this).val() =='' || $(this).val() ==null){
				jAlert("Please add Seat High.");
				return;
			}
		}); */
		if($('#shippingMethod').val() < 0){
			jAlert("Please Select Shipping method.");
			return;
		}
		if($('#cardNo').val()!='' && $('#cardNo').val().length !=4){
			jAlert("Please enter Card last 4 digit only");
			return;
		}
		$('#action').val('savePO');
		$('#purchaseOrderForm').submit();
	}
	
	//PO cancel action
	function cancelAction(){
		window.close();
	}
</script>

 
<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i> Purchase Order
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a  style="font-size: 13px;font-family: arial;" href="#">Purchase Order</a>
			</li>
			<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Create PO</li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<section class="panel"> 
		<c:if test="${successMessage != null}">
				<div class="alert alert-success fade in">
					<strong
						style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${successMessage}</strong>
				</div>
			</c:if>
			<c:if test="${errorMessage != null}">
				<div class="alert alert-block alert-danger fade in">
					<strong
						style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${errorMessage}</strong>
				</div>
			</c:if>
		<header style="font-size: 13px;font-family: arial;" class="panel-heading">
		Customer Details </header>
		<div class="panel-body">

			<div class="form-group">
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Customer Name:&nbsp;</label>
					<span style="font-size: 15px;">${customer.customerName}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Email:&nbsp;</label><span style="font-size: 15px;">${customer.userName}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Phone:</label><span style="font-size: 15px;">${customer.phone}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Address Line1:</label><span style="font-size: 15px;">${customerAddress.addressLine1}</span>
				</div>
			</div>
			<br/>
			<div class="form-group">
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Address Line2:</label><span style="font-size: 15px;">${customerAddress.addressLine2}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">City:</label><span style="font-size: 15px;">${customerAddress.city}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">State:</label><span style="font-size: 15px;">${customerAddress.state.name}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Country:</label><span style="font-size: 15px;">${customerAddress.country.name}</span>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">ZipCode:</label><span style="font-size: 15px;">${customerAddress.zipCode}</span>
				</div>
			</div>
		</div>
		</section>
	</div>
</div>

<%-- <div class="row">
	<div class="col-lg-12">
		<!--  search form start -->
		<form:form class="form-inline" role="form" id="eventSearch"
			method="post" action="${pageContext.request.contextPath}/Events">
			<div class="col-sm-1"></div>
			<!--<label class="control-label col-sm-2" >Search Type</label>-->
			<div class="col-sm-2">
				<select id="searchType" name="searchType"
					class="form-control input-sm m-bot15">
					<option value="eventName">Event</option>
					<option value="artistName"
						<c:if test="${searchType eq 'artistName'}">selected</c:if>>Artist</option>
					<option value="grandChildCategoryName"
						<c:if test="${grandChildCategoryName eq 'artistName'}">selected</c:if>>GrandChildCategory</option>
					<option value="venueName"
						<c:if test="${searchType eq 'venueName'}">selected</c:if>>Venue</option>
					<option value="city"
						<c:if test="${searchType eq 'city'}">selected</c:if>>City</option>
					<option value="state"
						<c:if test="${searchType eq 'state'}">selected</c:if>>State</option>

				</select>
			</div>
			<div class="col-sm-3">
				<div class="navbar-form">
					<input class="form-control" placeholder="Search" type="text"
						id="searchValue" name="searchValue" value="${searchValue}">
				</div>
			</div>
			<div class="col-sm-3">
				<button type="submit" class="btn btn-primary">search</button>
			</div>
			<div class="col-sm-1"></div>
		</form:form>
		<!--  search form end -->
	</div>

</div> --%>
<div class="row">
<header style="font-size: 13px;font-family: arial;" class="panel-heading">
		Select an event to add ticket </header>
<div class="col-lg-12">
		<form:form class="form-inline" role="form" id="eventSearch" method="post" action="${pageContext.request.contextPath}/Events">
			<div class="col-sm-1"> </div>
			<!--<label class="control-label col-sm-2" >Search Type</label>-->
			<div class="col-sm-2">
				<select id="searchType" name="searchType" class="form-control input-sm m-bot15">
					<option value="event_name">Event</option>
				   <option value="artist_name" <c:if test="${searchType eq 'artist_name'}">selected</c:if> >Artist</option>
				  <option value="grand_child_category_name" <c:if test="${searchType eq 'grand_child_category_name'}">selected</c:if> >GrandChildCategory</option>
				  <option value="building" <c:if test="${searchType eq 'building'}">selected</c:if> >Venue</option>
				  <option value="city" <c:if test="${searchType eq 'city'}">selected</c:if> >City</option>
				  <option value="state" <c:if test="${searchType eq 'state'}">selected</c:if> >State</option>
				  
				</select>
			</div>
              <div class="col-sm-3">
			<div class="navbar-form">
				<input class="form-control" placeholder="Search" type="text" id="searchValue" name="searchValue" value="${searchValue}">
			</div>
			</div>
			<div class="col-sm-3">
			 <button type="button" class="btn btn-primary" onclick="getEventGridData();">Search</button>
			 </div>
			 <div class="col-sm-1"> </div>
              </form:form>
               <!--  search form end -->                
          </div>
</div>
<!-- event Grid Code Starts -->
<div style="position: relative; display: none;" id="eventGridDiv">
	<div style="width: 100%;">
		<div class="grid-header" style="width: 100%">
			<label>Events Details</label> <span id="event_grid_toogle_search" style="float: right"
				class="ui-icon-sg ui-icon-sg-search" title="Toggle search panel"
				onclick="eventGridToggleFilterRow()"></span>
		</div>
		<div id="event_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
		<div id="event_pager" style="width: 100%; height: 10px;"></div>
	</div>
</div>


<div id="event_inlineFilterPanel" style="display:none;background:#dddddd;padding:3px;color:black;">
  Show records with EventName including <input type="text" id="eventGridSearch">
</div>


<!-- Show the selected event info -->
<br/><br/><br/>
<div class="form-group" id="addTicketDiv" style="display: none">
	<div class="col-sm-6">
		<button class="btn btn-primary" type="submit" id="addTicket" onclick="">Add Ticket</button>
	</div>
</div>
<br/><br/><br/>

<div id="poInfo">
<div class="col-lg-12">		
		 <section class="panel">
			<header style="font-size: 13px;font-family: arial;" class="panel-heading">Ticket Details</header>
			<div class="panel-body">
<div class="form-group">
	<form name="purchaseOrderForm" id="purchaseOrderForm" action="${pageContext.request.contextPath}/PurchaseOrder/SavePO" method="post">
	<input type="hidden" name="action" value="action" id="action"/>
	<input type="hidden" name="customerId" value="${customer.id}" id="customerId"/>
		<div class="col-lg-12">
			<table id="myTable"
				class="table table-striped table-advance table-hover">
				<tbody>
					<tr >
						<th></th>
						<th style="font-size: 13px;font-family: arial;"><i class=""></i> Event Name</th>
						<!-- <th><i class=""></i> Event Date</th>
					<th><i class=""></i> Venue</th> -->
						<th style="font-size: 13px;font-family: arial;"><i class=""></i> Section</th>
						<th style="font-size: 13px;font-family: arial;"><i class=""></i> Row</th>
						<th style="font-size: 13px;font-family: arial;"><i class=""></i> Seat Low</th>
						<th style="font-size: 13px;font-family: arial;"><i class=""></i> Seat High</th>
						<th style="font-size: 13px;font-family: arial;"><i class=""></i> Qty</th>
						<th style="font-size: 13px;font-family: arial;"><i class=""></i> Price</th>
						<th style="font-size: 13px;font-family: arial;"><i class=""></i> Action</th>
					</tr> 
					<!-- dynamic rows -->
					<!--<h4 id="totalQty" style="float:right;">Total qty</h4>-->
				</tbody>
			</table>
		</div>
</div>

<div class="row">
	<div class="col-lg-12">
		 <div class="col-lg-3 col-sm-3 ">
			<label >Shipping Method</label>
			<select id="shippingMethod" name="shippingMethod" class="form-control input-sm m-bot15">
			  <option value="-1">--select--</option>
			  <c:forEach items="${shippingMethods}" var="shippingMethod">
				<option value="${shippingMethod.id}"> ${shippingMethod.name}
				</option>
			</c:forEach>
			</select>
		 </div>
		 <div class="col-lg-3 col-sm-3" id="trackingInfo">
			<label >Tracking No </label>
			<input type="text" name="trackingNo" id="trackingNo" class="form-control"/>
		 </div>
		 <div class="col-lg-3 col-sm-3"  style="float:right">
			Total Price : $<input type="text" id="totalPrice" name="totalPrice" readOnly value="" style="border: none;">
		 </div>
		 <div class="col-lg-3 col-sm-3"  style="float:right">
			Total Qty :
			<input type="text" id="totalQty" name="totalQty" readOnly value="" style="border: none;">
		 </div>
		 
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="col-lg-3 col-sm-3 ">
			<label>External PO No :</label>
			<input type="text" name="externalPONo" value=""  class="form-control"/>
		 </div>
		 <div class="col-lg-3 col-sm-3 ">
			<label>Consignment PO No :</label>
			<input type="text" name="consignmentPo" value=""  class="form-control"/>
		 </div>
		 <div class="col-lg-3 col-sm-3 ">
			<label>Transaction Office :</label>
			<select name="transactionOffice" class="form-control">
		  		<option value="Main">Main</option>
		  		<option value="Spec">Spec</option>
		  	</select> 
		 </div>
		</div>
	</div>
	</div>
</section>
</div>
<div class="row">
	<div class="col-lg-12">		
		 <section class="panel">
			<header style="font-size: 13px;font-family: arial;" class="panel-heading">Payment Details</header>
			<div class="panel-body">
				<div class="form-group">
					<div class="col-sm-4">
						  <input type="radio" name="paymentType" value="card">Credit Card
					</div>
					<div class="col-sm-4">
						  <input type="radio" name="paymentType" value="account">In Account
					</div>
					<div class="col-sm-4">
						  <input type="radio" name="paymentType" value="cheque">Cheque
					</div>
					<hr/>
				</div>
				<div class="form-group" id="cardInfo">
					<div class="col-sm-4">
						<label>Card Holder Name</label>
					  	<input type="text" name="name" id="name" class="form-control"/> 
					</div>
					<div class="col-sm-4">
						<label>Card Type</label>
					  	<select name="cardType" class="form-control">
					  		<option value="American Express">American Express</option>
							<option value="Visa">Visa</option>
							<option value="MasterCard">MasterCard</option>
							<option value="JCB">JCB</option>
							<option value="Discover">Discover</option>
							<option value="Diners Club">Diners Club</option>
					  	</select> 
					</div>
					<div class="col-sm-4">
						<label>Card Last 4 Digit</label>
					  	<input type="text" name="cardNo" id="cardNo" class="form-control"/> 
					</div>
					<hr/>
				</div>
				<div class="form-group" id="accountInfo">
					<div class="col-sm-4">
						<label>Account Name</label>
					  	<input type="text" name="name" id="name" class="form-control"/> 
					</div>
					<div class="col-sm-4">
						<label>Routing no. Last 4 Digit</label>
					  	<input type="text" name="routingNo" id="routingNo" class="form-control"/>
					</div>
					<div class="col-sm-4">
						<label>Account no. Last 4 Digit</label>
					  	<input type="text" name="accountNo" id="accountNo" class="form-control"/> 
					</div>
					<hr/>
				</div>
				<div class="form-group" id="chequeInfo">
					<div class="col-sm-3">
						<label>Name</label>
					  	<input type="text" name="name" id="name" class="form-control"/> 
					</div>
					<div class="col-sm-3">
						<label>Routing no. Last 4 Digit</label>
					  	<input type="text" name="routingNo" id="routingNo" class="form-control"/>
					</div>
					<div class="col-sm-3">
						<label>Account no. Last 4 Digit</label>
					  	<input type="text" name="accountNo" id="accountNo" class="form-control"/>
					</div>
					<div class="col-sm-3">
						<label>Cheque No.</label>
					  	<input type="text" name="checkNo" id="checkNo" class="form-control"/> 
					</div>
					<hr/>
				</div>
				<div class="form-group">
					<div class="col-sm-4">
						<label>Payment Date</label>
					  	<input type="text" name="paymentDate" id="paymentDate" class="form-control"/> 
					</div>
					<div class="col-sm-4">
						<label>payment Status</label>
					  	<select name="paymentStatus" class="form-control">
					  		<option value="Paid">Paid</option>
					  		<option value="Pending">Pending</option>
					  	</select> 
					</div>
					<div class="col-sm-4">
						<label>Payment Note</label>
					  	<textarea name="paymentNote" id="paymentNote" rows="4" cols="50" class="form-control"> </textarea>
					</div>
					<div class="col-sm-3"></div>
				</div>
			</div>
		</section>
	</div>

</div>

<div class="form-group" style="margin-top:10px;">
	<div id="POAction" class="col-sm-6">
		<button class="btn btn-primary" type="button" onclick="savePurchaseOrder();">Create PO</button>
		<button class="btn btn-default" onclick="cancelAction();"
			type="button">Cancel</button>
	</div>
</div>
</form>
</div>


<!-- slickgrid related scripts -->
<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<script>
	var eventDataView;
	var eventGrid;
	var eventData = [];
	var eventColumns = [ {
		id : "eventName",
		name : "Event Name",
		field : "eventName",
		sortable : true
	}, {
		id : "eventDate",
		name : "Event Date",
		field : "eventDate",
		sortable : true
	}, {
		id : "eventTime",
		name : "Event Time",
		field : "eventTime",
		sortable : true
	}, {
		id : "venue",
		name : "Venue",
		field : "venue",
		sortable : true
	}, {
		id : "city",
		name : "City",
		field : "city",
		sortable : true
	}, {
		id : "state",
		name : "State",
		field : "state",
		sortable : true
	},  {id: "country", 
		name: "Country", 
		field: "country", 
		sortable: true
	}, {id: "artist", 
		name: "Artist", 
		field: "artist", 
		sortable: true
	}, {id: "grandChildCategory", 
		name: "Grand Child Category", 
		field: "grandChildCategory", 
		sortable: true
	},{id: "childCategory", 
		name: "Child Category", 
		field: "childCategory", 
		sortable: true
	}, {
		id : "parentCategory",
		field : "parentCategory",
		name : "Parent Category",
		sortable: true
	} ];

	var eventOptions = {
		//editable: true,
		//enableAddRow: true,
		enableCellNavigation : true,
		//asyncEditorLoading: true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25
	};
	var eventGridSortcol = "eventName";
	var eventGridSortdir = 1;
	var percentCompleteThreshold = 0;
	var eventGridSearchString = "";

	
	function deleteRecordFromEventGrid(id) {
		eventDataView.deleteItem(id);
		eventGrid.invalidate();
	}
	function eventGridFilter(item, args) {
		var x= item["eventName"];
		if (args.eventGridSearchString  != ""
				&& x.indexOf(args.eventGridSearchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.eventGridSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}

	function eventGridComparer(a, b) {
		var x = a[eventGridSortcol], y = b[eventGridSortcol];
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	function eventGridToggleFilterRow() {
		eventGrid.setTopPanelVisibility(!eventGrid.getOptions().showTopPanel);
	}
	
	//$(".grid-header .ui-icon-sg").addClass("ui-state-default-sg ui-corner-all")
	$("#event_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all")
			.mouseover(function(e) {
				$(e.target).addClass("ui-state-hover")
			}).mouseout(function(e) {
				$(e.target).removeClass("ui-state-hover")
			});

	function getEventGridData() {
		$('#eventGridDiv').show();
		$('#addTicketDiv').show();
		var searchValue = $("#searchValue").val();
		if(searchValue== null || searchValue == '') {
			jAlert('Enter valid key to searh events');
			return false;
		}
		
		$.ajax({
			  
			url : "${pageContext.request.contextPath}/GetEventDetails",
			type : "post",
			data : $("#eventSearch").serialize(),
			dataType:"json",
			/* async : false, */
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				//jAlert(jsonData);
				refreshEventGridValues(jsonData);
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	function refreshEventGridValues(jsonData) {
		
		eventData = [];
		for (var i = 0; i < jsonData.length; i++) {
			var  data= jsonData[i]; 
			var d = (eventData[i] = {});
			d["id"] = data.eventId;
			d["eventName"] = data.eventName;
			d["eventDate"] = data.eventDate;
			d["eventTime"] = data.eventTime;
			d["venue"] = data.venue;
			d["city"] = data.city;
			d["state"] = data.state;
			d["country"] = data.country;
			d["grandChildCategory"] = data.grandChildCategory;
			d["childCategory"] = data.childCategory;
			d["parentCategory"] = data.parentCategory;
			d["artist"] = data.artist;
		}

		eventDataView = new Slick.Data.DataView({
			inlineFilters : true
		});
		eventGrid = new Slick.Grid("#event_grid", eventDataView, eventColumns, eventOptions);
		eventGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		eventGrid.setSelectionModel(new Slick.RowSelectionModel());
		var eventGridPager = new Slick.Controls.Pager(eventDataView, eventGrid, $("#event_pager"));
		var eventGridColumnpicker = new Slick.Controls.ColumnPicker(eventColumns, eventGrid,
				eventOptions);

		// move the filter panel defined in a hidden div into eventGrid top panel
		$("#event_inlineFilterPanel").appendTo(eventGrid.getTopPanel()).show();

		/*eventGrid.onKeyDown.subscribe(function(e) {
			// select all rows on ctrl-a
			if (e.which != 65 || !e.ctrlKey) {
				return false;
			}
			var rows = [];
			for ( var i = 0; i < eventDataView.getLength(); i++) {
				rows.push(i);
			}
			eventGrid.setSelectedRows(rows);
			e.preventDefault();
		});*/
		
		eventGrid.onSort.subscribe(function(e, args) {
			eventGridSortdir = args.sortAsc ? 1 : -1;
			eventGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				// using temporary Object.prototype.toString override
				// more limited and does lexicographic sort only by default, but can be much faster
				// use numeric sort of % and lexicographic for everything else
				eventDataView.fastSort(eventGridSortcol, args.sortAsc);
			} else {
				// using native sort with comparer
				// preferred method but can be very slow in IE with huge datasets
				eventDataView.sort(eventGridComparer, args.sortAsc);
			}
		});
		// wire up model events to drive the eventGrid
		eventDataView.onRowCountChanged.subscribe(function(e, args) {
			eventGrid.updateRowCount();
			eventGrid.render();
		});
		eventDataView.onRowsChanged.subscribe(function(e, args) {
			eventGrid.invalidateRows(args.rows);
			eventGrid.render();
		});

		// wire up the search textbox to apply the filter to the model
		$("#eventGridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			eventGridSearchString = this.value;
			updateEventGridFilter();
		});
		function updateEventGridFilter() {
			eventDataView.setFilterArgs({
				eventGridSearchString : eventGridSearchString
			});
			eventDataView.refresh();
		}
		
		// initialize the model after all the events have been hooked up
		eventDataView.beginUpdate();
		eventDataView.setItems(eventData);
		eventDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			eventGridSearchString : eventGridSearchString
		});
		eventDataView.setFilter(eventGridFilter);
		eventDataView.endUpdate();
		eventDataView.syncGridSelection(eventGrid, true);
		$("#gridContainer").resizable();
		$("div#divLoading").removeClass('show');
		
		/* var eventrowIndex;
		eventGrid.onSelectedRowsChanged.subscribe(function() { 
			var temprEventRowIndex = eventGrid.getSelectedRows([0])[0];
			if (temprEventRowIndex != eventrowIndex) {
				eventrowIndex = temprEventRowIndex;
				 var eventId =eventGrid.getDataItem(temprEventRowIndex).id;
				//jAlert(eventId);
				//getCategoryTicketGroupsforEvent(eventId);
			}
		}); */
	}
	
	
	//Get selected event on button click
	function getEventInfoForPO(){		
			var temprEventRowIndex = eventGrid.getSelectedRows([0])[0];
			if(temprEventRowIndex == null){
				jAlert("Please select event");
			}{
			var eventId = eventGrid.getDataItem(temprEventRowIndex).id;
			var eventName = eventGrid.getDataItem(temprEventRowIndex).eventName;
			var eventDate = eventGrid.getDataItem(temprEventRowIndex).eventDate;
			var venue = eventGrid.getDataItem(temprEventRowIndex).eventDate;
			//jAlert(eventId+","+eventName+","+eventDate+","+venue);	
			//$('#eventId').val(eventId);
			//$('#eventName').text(eventName);
			//$('#eventDate').text(eventDate);
			//$('#venue').text(venue);

			//Add dynamic rows to table for selected event
			var myTable = document.getElementById("myTable");
	        var currentIndex = myTable.rows.length;
	        var currentRow = myTable.insertRow(-1);

			var input = document.createElement("input");
			input.setAttribute("type", "hidden");
			input.setAttribute("name", "eventId_" + eventId);
			input.setAttribute("value", eventId);
			input.setAttribute("id","display");

			var eventNameLabel = document.createElement("Label");
			eventNameLabel.setAttribute("for","event_"+eventId);
			eventNameLabel.innerHTML = eventName;
			
	    	/* var eventDateLabel = document.createElement("Label");
	    	eventDateLabel.setAttribute("for","event_"+eventId);
	    	eventDateLabel.innerHTML = eventDate;

	    	var venueLabel = document.createElement("Label");
	    	venueLabel.setAttribute("for","event_"+eventId);
	    	venueLabel.innerHTML = venue; */
	    	
	    	var sectionBox = document.createElement("input");
	    	sectionBox.setAttribute("name", "section_" + eventId);
	    	sectionBox.setAttribute("id","section_"+eventId);
	    	
	    	var row = document.createElement("input");
	    	row.setAttribute("name", "row_" + eventId);
	    	row.setAttribute("id","row_"+eventId);
	    	
	    	var seatLow = document.createElement("input");
	    	seatLow.setAttribute("name", "seatLow_" + eventId);
	    	seatLow.setAttribute("id","seatLow_"+eventId);
	    	seatLow.setAttribute("class","seatLow");
	    	
	    	var seatHigh = document.createElement("input");
	    	seatHigh.setAttribute("name", "seatHigh_" + eventId);
	    	seatHigh.setAttribute("id","seatHigh_"+eventId);
	    	seatHigh.setAttribute("class","seatHigh");
	    	
	    	var qty = document.createElement("input");
	    	qty.setAttribute("type", "number");
	    	qty.setAttribute("name", "qty_" + eventId);
	    	qty.setAttribute("id","qty_"+eventId);
			qty.setAttribute("class","qty");
			qty.setAttribute("onkeyup","sumQty(this);");
	    	
	    	var price = document.createElement("input");
	    	price.setAttribute("type", "number");
	    	price.setAttribute("name", "price_" + eventId);
	    	price.setAttribute("id","price_"+eventId);
			price.setAttribute("class","price");
			price.setAttribute("onkeyup","sumPrice(this);");
	    	
	        var addRowBox = document.createElement("input");
	        addRowBox.setAttribute("type", "button");
	        addRowBox.setAttribute("value", "Remove");
	        addRowBox.setAttribute("onclick", "removeField(this);");
	        addRowBox.setAttribute("class", "button");
	
			currentCell = currentRow.insertCell(-1);
			currentCell.appendChild(input);

	        var currentCell = currentRow.insertCell(-1);
			currentCell.appendChild(eventNameLabel);

			/* currentCell = currentRow.insertCell(-1);
	        currentCell.appendChild(eventDateLabel);

			currentCell = currentRow.insertCell(-1);
			currentCell.appendChild(venueLabel); */

			currentCell = currentRow.insertCell(-1);
			currentCell.appendChild(sectionBox);
			
			currentCell = currentRow.insertCell(-1);
			currentCell.appendChild(row);
			
			currentCell = currentRow.insertCell(-1);
			currentCell.appendChild(seatLow);
			
			currentCell = currentRow.insertCell(-1);
			currentCell.appendChild(seatHigh);
			
			currentCell = currentRow.insertCell(-1);
			currentCell.appendChild(qty);
			
			currentCell = currentRow.insertCell(-1);
			currentCell.appendChild(price);
	        
			currentCell = currentRow.insertCell(-1);
	        currentCell.appendChild(addRowBox);
			
			}
			
			
			/*
			var footer = myTable.createTFoot();
			var row = footer.insertRow();
			var cell = row.insertCell();
			cell.innerHTML = "Total Qty";*/
			
		
	}

	function removeField(id){
	//jAlert('remove');
		var p=id.parentNode.parentNode;
   	 	p.parentNode.removeChild(p);
	}

</script>
