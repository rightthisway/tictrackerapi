<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<script type="text/javascript">

var roleAffiliateBroker = "${roleAffiliateBroker}";

$(document).ready(function(){	
	
	if(roleAffiliateBroker != null && roleAffiliateBroker == 'ROLE_BROKER'){
		$('#companyName').removeAttr('readonly');
	}else{
		$('#companyName').attr('readonly','readonly');
	}
});

	function doAddUserValidations(){
		var roles = $("input[name=role]");		
		var flag = false;
		
		if(roleAffiliateBroker != null && roleAffiliateBroker == 'ROLE_BROKER'){
			if($("#companyName").val() == ''){
				jAlert("Invalid Company.","Info");
				return false;
			}
			if($("#serviceFees").val() == ''){
				jAlert("Service Fees can't be blank","Info");
				return false;
			}
		}
		else if(roleAffiliateBroker != null && roleAffiliateBroker == 'ROLE_AFFILIATES'){
		}
		else{			
			$.each(roles, function(index, obj){
				if($(obj).attr('checked') == 'checked'){				
					flag = true;
				}
			});
			if(!flag){
				jAlert('Please choose at least one Role.',"Info");
				return false;
			}
		}
		
		if($("#userName").val() == ''){
			jAlert("Username can't be blank","Info");
			return false;
		}else if($("#firstName").val() == ''){
			jAlert("Firstrname can't be blank","Info");
			return false;
		}else if($("#lastName").val() == ''){
			jAlert("Lastname can't be blank","Info");
			return false;
		}else if($("#email").val() == ''){
			jAlert("Email can't be blank","Info");
			return false;
		}else if($("#password").val() == ''){
			jAlert("Password can't be blank","Info");
			return false;
		}else if($("#repassword").val() == ''){
			jAlert("Re-Password can't be blank","Info");
			return false;
		}else if($("#password").val() != $("#repassword").val()){
			jAlert("Password and Re-Password must match","Info");
			return false;
		}else if(validateEmail($('#email').val()) == false){
			jAlert("Invalid Email.","Info");
			return false;
		}
		else{
			$.ajax({
					url : "${pageContext.request.contextPath}/CheckUser",
					type : "get",
					data : "userName="+ $("#userName").val() + "&email=" + $("#email").val(),
					/* async : false, */
					
					success : function(response){
						if(response == "true"){
							if(roleAffiliateBroker != null && (roleAffiliateBroker == 'ROLE_BROKER' || roleAffiliateBroker == 'ROLE_AFFILIATES')){
								$('#loginForm').attr('action','${pageContext.request.contextPath}/Client/AddUsers');
								$("#loginForm").submit();
							}else{
								$('#loginForm').attr('action','${pageContext.request.contextPath}/Admin/AddUser');
								$("#loginForm").submit();
							}
						}else{
							jAlert(response,"Info");
							return false;
						}
					},
					error : function(error){
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
			});
		}
		
	}
	
	function validateEmail(email) {
	    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	    //alert(re.test(email));
	    return re.test(email);
	}
	
	function cancelAction(){
		if(roleAffiliateBroker != null && roleAffiliateBroker == 'ROLE_BROKER'){
			window.location.href = "${pageContext.request.contextPath}/Client/ManageBrokers";
		}
		else if(roleAffiliateBroker != null && roleAffiliateBroker == 'ROLE_AFFILIATES'){
			window.location.href = "${pageContext.request.contextPath}/Affiliates";
		}
		else{
			window.location.href = "${pageContext.request.contextPath}/Admin/ManageUsers";
		}
	}
	
</script>

<style>
	input{
		color : black !important;
	}
	.required{
		color: red !important;
    }
</style>


<div class="row">
				<div class="col-lg-12">
					<h3 class="page-header"><i class="fa fa-laptop"></i> Admin</h3>
					<ol class="breadcrumb">
						<c:choose>
							<c:when test="${empty roleAffiliateBroker or roleAffiliateBroker eq 'ROLE_SUPER_ADMIN' or roleAffiliateBroker eq 'ROLE_USER'}">
								<li><i class="fa fa-home"></i><a href="#">Admin</a></li>
								<li><i class="fa fa-laptop"></i>Add User</li>
							</c:when>
							<c:when test="${roleAffiliateBroker ne null and roleAffiliateBroker eq 'ROLE_BROKER'}">
								<li><i class="fa fa-home"></i><a href="#">Broker</a></li>
								<li><i class="fa fa-laptop"></i>Add Broker</li>
							</c:when>
							<c:when test="${roleAffiliateBroker ne null and roleAffiliateBroker eq 'ROLE_AFFILIATES'}">
								<li><i class="fa fa-home"></i><a href="#">Affiliates</a></li>
								<li><i class="fa fa-laptop"></i>Add Affiliates</li>
							</c:when>
						</c:choose>						  	
					</ol>
				</div>
</div>

<div class="row">
<div class="col-lg-12">
<section class="panel">
  <header class="panel-heading">
						<c:choose>
							<c:when test="${empty roleAffiliateBroker or roleAffiliateBroker eq 'ROLE_SUPER_ADMIN' or roleAffiliateBroker eq 'ROLE_USER'}">
								Fill User Details
							</c:when>
							<c:when test="${roleAffiliateBroker ne null and roleAffiliateBroker eq 'ROLE_BROKER'}">
								Fill Broker Details
							</c:when>
							<c:when test="${roleAffiliateBroker ne null and roleAffiliateBroker eq 'ROLE_AFFILIATES'}">
								Fill Affiliates Details
							</c:when>
						</c:choose>	
						
                    </header>
 <div class="panel-body">
							<c:if test="${successMessage != null}">
                              	<div class="alert alert-success fade in">
                              		<strong style="font-family:arial, helvetica;font-size:17px;display:block;text-align:center;">${successMessage}</strong>
                              	</div>
                              </c:if>
                              <c:if test="${errorMessage != null}">
                              	<div class="alert alert-block alert-danger fade in">
                              		<strong style="font-family:arial, helvetica;font-size:17px;display:block;text-align:center;">${errorMessage}</strong>
                              	</div>
                              </c:if>
  <form:form role="form" class="form-horizontal" commandName="trackerUser" modelAttribute="trackerUser" id="loginForm" method="post" action="">
<input class="form-control" id="action" value="action" name="action" type="hidden" />
    <div class="form-group">
      <div class="col-sm-5"><label>Username <span class="required">*</span></label><form:input class="form-control" path="userName" id="userName" name="userName" type="text" /></div>
      <div class="col-sm-5"><label>Email <span class="required">*</span></label><form:input class="form-control " path="email" id="email" type="email" name="email"/></div>
    </div>

	<div class="form-group">
      <div class="col-sm-5"><label>First Name <span class="required">*</span></label><form:input class="form-control" path="firstName" id="firstName" name="firstName" type="text"/></div>
      <div class="col-sm-3"><label>Last Name <span class="required">*</span></label><form:input class="form-control" path="lastName" id="lastName" name="lastName" type="text"/></div>
    </div>
    
	<div class="form-group">
      <div class="col-sm-5"><label>Password <span class="required">*</span></label><form:input class="form-control " path="password" id="password" name="password" type="password" /></div>
      <div class="col-sm-5"><label>Re-Password<span class="required">*</span></label><input class="form-control" id="repassword" name="repassword" type="password" /></div>
    </div>

	<div class="form-group">
      <div class="col-sm-5"><label>Phone</label><form:input class="form-control " path="phone" id="phone" name="phone" maxlength="10" /></div>	  
	  <c:if test="${roleAffiliateBroker ne null and roleAffiliateBroker eq 'ROLE_BROKER'}">
		<div class="col-sm-5"><label>Company<span class="required">*</span></label><input class="form-control" id="companyName" name="companyName" type="text" readonly="readonly"/></div>
	  </c:if>
	</div>
	<c:if test="${roleAffiliateBroker ne null and roleAffiliateBroker eq 'ROLE_BROKER'}">
	<div class="form-group">	  
		<div class="col-sm-5"><label>Service Fees<span class="required">*</span></label><input class="form-control" id="serviceFees" name="serviceFees" type="text"/></div>
    </div>
	</c:if>
	
	<div class="form-group">
		<label class="control-label col-lg-2" for="inputSuccess">Roles <span class="required">*</span></label>
        <div class="col-lg-10">
		<c:choose>
			<c:when test="${empty roleAffiliateBroker or roleAffiliateBroker eq 'ROLE_SUPER_ADMIN' or roleAffiliateBroker eq 'ROLE_USER'}">
			    <div id="roleDiv">
				 	<label class="">
						<input type="checkbox" name="role" value="role_super_admin"> Super Admin
					</label>
					<label class="">
						<input type="checkbox" name="role" value="role_user"> User
					</label>
				</div>
			</c:when>
			<c:when test="${roleAffiliateBroker ne null and roleAffiliateBroker eq 'ROLE_BROKER'}">
				<div id="roleBrokerDiv">
				 	<label>
						Broker <input type="hidden" name="role" value="role_broker" id="brokerRole">
					</label>
				</div>
			</c:when>
			<c:when test="${roleAffiliateBroker ne null and roleAffiliateBroker eq 'ROLE_AFFILIATES'}">
				<div id="roleAffiliateDiv">
				 	<label>
						Affiliates <input type="hidden" name="role" value="role_affiliates">
					</label>
				</div>
			</c:when>
		</c:choose>
		</div>
    </div>

    <div class="form-group">
      <div class="col-sm-6">
        <!--<button type="submit" class="btn btn-info pull-right">Submit</button>-->
		<button class="btn btn-primary" type="button" onclick="doAddUserValidations()">Save</button>
		<button class="btn btn-default" onclick="cancelAction()" type="button">Cancel</button>
      </div>
    </div>
  </form:form>
</section>
</div>
</div>