<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<script type="text/javascript">
var isUser = '<%=session.getAttribute("isUser")%>';
$(document).ready(function(){
	if(isUser != 'false'){
		$("#adminRpt").hide();
	}
});
</script>

<style>
	input{
		color : black !important;
	}
</style>


<div class="row">
				<div class="col-lg-12">
					<h3 class="page-header"><i class="fa fa-laptop"></i> Reports</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="#">Reports</a></li>
						<li><i class="fa fa-laptop"></i>Reports Home</li>						  	
					</ol>
					<!-- <a style="margin-top:-0.8%;margin-left:5%;" class="btn btn-primary" href="#" title="Sales Report">Sales Report</a> -->
				</div>
</div>

<div class="row">
           <div class="col-lg-4">
               <section class="panel">
                   <header class="panel-heading">
                      <strong>Sales Report</strong> 
                   </header>
                   <div class="list-group">
                  	   <a class="list-group-item " href="DownloadTNDEventSalesReport">
                           Events Last 3 Days Sales
                       </a>
                      <a class="list-group-item " href="DownloadEventsNotInRTFReport">
                           Events in tmat but not in RTF
                       </a>
                       <a class="list-group-item " href="EventsStatisticsReport">
                           RTF Events Statistics
                       </a>
                       <a class="list-group-item " href="DownloadRTFFilledReport">
                           RTF Filled Reports
                       </a>
                        <a class="list-group-item " href="DownloadRTFUnFilledReport">
                           RTF UnFilled Reports
                       </a>
                       <a class="list-group-item " href="DownloadRTFUnFilledShortsWithPossibleLongInventoryReport">
                           RTF Filled Shorts With Possible Long Inventory Reports
                       </a>
                       <a class="list-group-item " href="DownloadRTFLast15DaysUnsentReport">
                           RTF 15 Days Unsent Report
                       </a>
                       <a class="list-group-item " href="DownloadRTFZonesNotHavingColorsReport">
                           RTF Map Not Having Zone Colors
                       </a>
                       <a class="list-group-item " href="UnsoldTickets">
                           RTF UnSold Tickets
                       </a>
                       <a class="list-group-item " href="PoInvoiceReport">
                           RTF PO/Invoice Report
                       </a>
                       <a class="list-group-item " href="DownloadRTFProfitAndLossReport">
                           RTF Profit/Loss Report
                       </a>
                       <a class="list-group-item " href="DownloadRTFSalesReport">
                           Reward The Fan Sales Report
                       </a>
                       <a class="list-group-item " href="DownloadPointsSummeryReport">
                           Points Summary Report
                       </a>
                   </div>
               </section>
           </div>
           <!-- <div id="adminRpt">
            <div class="col-lg-4">
            	<section class="panel">
                   <header class="panel-heading">
                      <strong>Admin Report</strong> 
                   </header>
                   <div class="list-group">
                       
                       
                       <a class="list-group-item " href="DownloadRTFUnsoldInventory">
                           RTF Unsold Inventory
                       </a>
                      
                   </div>
               </section>
            </div>
            </div>
           <div class="col-lg-4"></div> -->
</div>