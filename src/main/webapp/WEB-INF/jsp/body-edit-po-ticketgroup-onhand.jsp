<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
    <link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../resources/css/font-awesome.min.css" rel="stylesheet" />    
	<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>
    <link rel="stylesheet" href="../resources/css/owl.carousel.css" type="text/css">
	<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
	<link href="../resources/css/widgets.css" rel="stylesheet">
    <link href="../resources/css/style.css" rel="stylesheet">
    <link href="../resources/css/style-responsive.css" rel="stylesheet" />
    <link href="../resources/css/jquery.alerts.css" rel=" stylesheet">
	<script src="../resources/js/jquery-1.8.3.min.js"></script>
	<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
	<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
    <script src="../resources/js/bootstrap.min.js"></script>
    <script src="../resources/js/jquery.scrollTo.min.js"></script>
    <script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>
<script src="../resources/js/jquery.alerts.js"></script>
<script>
$(document).ready(function(){
	
});

window.onunload = function () {
    var win = window.opener;
    if (!win.closed) {
    	window.opener.getPOGridData(0);
    }
};

function updateTicketOnHand(onHand){
	var ids = getSelectedTicketGroupGridId();
	if(ids==null || ids==''){
		jAlert("Please select atleast one record to update");
		return;
	}else{
		window.location="${pageContext.request.contextPath}/Accounting/UpdatePOTicketGroupOnhandStatus?onHand="+onHand+"&ticketGroupIds="+ids+"&poId=${poId}";
	}
}
window.onresize = function(){
	ticketGroupGrid.resizeCanvas();
};
</script>
<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i> Accounting
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Purchase Order</a>
			</li>
			<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Edit Ticket ON-HAND/NOT ON-HAND</li>
		</ol>
	</div>
</div>

<br />
<c:if test="${successMessage != null}">
	<div class="alert alert-success fade in">
		<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${successMessage}</strong>
	</div>
</c:if>
<c:if test="${errorMessage != null}">
	<div class="alert alert-block alert-danger fade in">
		<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${errorMessage}</strong>
	</div>
</c:if>
<div style="position: relative" id="ticketGroup">
	<div style="width: 100%;">
		<div class="grid-header" style="width: 100%">
			<label>Ticket Groups</label> <span id="openOrder_grid_toogle_search" style="float: right"
				class="ui-icon ui-icon-search" title="Toggle search panel"></span>
		</div>
		<div id="ticketGroup_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
		<div id="ticketGroup_pager" style="width: 100%; height: 10px;"></div>
		<br/><br/>
		<div class="col-lg-12">
			<button type="button" class="btn btn-primary" onclick="updateTicketOnHand('Yes')">Mark as ON-HAND</button>
			<button type="button" class="btn btn-primary" onclick="updateTicketOnHand('No')">Mark as NOT ON-HAND</button>
		</div>
	</div>
</div>
<script>
$("div#divLoading").addClass('show');
var ticketGroupCheckboxSelector = new Slick.CheckboxSelectColumn({
	 cssClass: "slick-cell-checkboxsel"
});
		var pagingInfo;
		var ticketGroupDataView;
		var ticketGroupGrid;
		var ticketGroupData = [];
		var ticketGroupColumns = [ ticketGroupCheckboxSelector.getColumnDefinition(),
		{
			id : "id",
			name : "Ticket Group Id",
			field : "id",
			width:80,
			sortable : true
		},{
			id : "eventName",
			name : "Event Name",
			field : "eventName",
			width:80,
			sortable : true
		}, {
			id : "eventDate",
			name : "Event Date",
			field : "eventDate",
			width:80,
			sortable : true
		}, {
			id : "venue",
			name : "Venue",
			field : "venue",
			width:80,
			sortable : true
		},  {
			id: "section", 
			name: "Section", 
			field: "section",
			width:80,		
			sortable: true
		},{
			id: "row", 
			name: "Row", 
			field: "row", 
			width:80,
			sortable: true,
		},{
			id: "seatLow", 
			name: "Seat Low", 
			field: "seatLow", 
			width:80,
			sortable: true
		} , {
			id: "seatHigh", 
			name: "Seat High", 
			field: "seatHigh",
			width:80,
			sortable: true
		} , {
			id: "quantity", 
			name: "Quantity", 
			field: "quantity",
			width:80,
			sortable: true
		} , {
			id: "price", 
			name: "Price", 
			field: "price",
			width:80,
			sortable: true
		} , {
			id: "onhand", 
			name: "On Hand", 
			field: "onhand",
			width:80,
			sortable: true
		}, {
			id: "invoiceId", 
			name: "Invoice Id", 
			field: "invoiceId",
			width:80,
			sortable: true
		}];
		
		var ticketGroupOptions = {
			editable: true,
			//enableAddRow: true,
			enableCellNavigation : true,
			//asyncEditorLoading: true,
			forceFitColumns : true,
			multiSelect: false,
			topPanelHeight : 25
		};
		var ticketGroupGridSortcol = "id";
		var ticketGroupGridSortdir = 1;
		var percentCompleteThreshold = 0;
		var ticketGroupGridSearchString = "";

		
		function deleteRecordFromTicketGroupGrid(id) {
			ticketGroupDataView.deleteItem(id);
			ticketGroupGrid.invalidate();
		}
		function ticketGroupGridFilter(item, args) {
			var x= item["id"];
			if (args.ticketGroupGridSearchString  != ""
					&& x.indexOf(args.ticketGroupGridSearchString) == -1) {
				
				if (typeof x === 'string' || x instanceof String) {
					if(x.toLowerCase().indexOf(args.ticketGroupGridSearchString.toLowerCase()) == -1) {
						return false;
					}
				} else {
					return false;
				}
			}
			return true;
		}

		function ticketGroupGridComparer(a, b) {
			var x = a[ticketGroupGridSortcol], y = b[ticketGroupGridSortcol];
			if(!isNaN(x)){
			   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
			}
			if(x == '' || x == null) {
				return 1;
			} else if(y == '' || y == null) {
				return -1;
			}
			if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
				return x.toLowerCase().localeCompare(y.toLowerCase());	
			} else {
				return (x == y ? 0 : (x > y ? 1 : -1));	
			}
		}
		function ticketGroupGridToggleFilterRow() {
			ticketGroupGrid.setTopPanelVisibility(!ticketGroupGrid.getOptions().showTopPanel);
		}
		
		//$(".grid-header .ui-icon").addClass("ui-state-default-sg ui-corner-all")
		$("#ticketGroup_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all")
			.mouseover(function(e) {
				$(e.target).addClass("ui-state-hover")
			}).mouseout(function(e) {
				$(e.target).removeClass("ui-state-hover")
			});

		
		function getTicketGroupGridData() {
			var searchValue = $("#searchValue").val();
			if(searchValue== null || searchValue == '') {
				jAlert('Enter valid key to searh ticketGroup');
				return false;
			}
		}
	function refreshTicketGroupGridValues() {
		
		ticketGroupData = [];
			var i=0;
			<c:forEach var="ticketGroup" items="${ticketGroups}">
			var d = (ticketGroupData[i] = {});
			d["id"] = "${ticketGroup.id}";
			//d["id"] = "id_" + i;
			d["eventName"] = "${ticketGroup.id}";
			d["eventDate"] = "${ticketGroup.eventDateStr}";
			d["venue"] = "${ticketGroup.venue}";
			d["section"] = "${ticketGroup.section}";
			d["row"] = "${ticketGroup.row}";
			d["seatLow"] = "${ticketGroup.seatLow}";
			d["seatHigh"] = "${ticketGroup.seatHigh}";
			d["quantity"] = "${ticketGroup.quantity}";
			d["price"] = "${ticketGroup.price}";
			d["onhand"] = "${ticketGroup.onHand}";
			d["invoiceId"] = "${ticketGroup.invoiceId}";
			i++;
			</c:forEach>

			ticketGroupDataView = new Slick.Data.DataView({
				inlineFilters : true
			});
			ticketGroupGrid = new Slick.Grid("#ticketGroup_grid", ticketGroupDataView, ticketGroupColumns, ticketGroupOptions);
			ticketGroupGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
			ticketGroupGrid.setSelectionModel(new Slick.RowSelectionModel());
			ticketGroupGrid.registerPlugin(ticketGroupCheckboxSelector);
			if(pagingInfo != null){
				var ticketGroupGridPager = new Slick.Controls.Pager(ticketGroupDataView, ticketGroupGrid, $("#ticketGroup_pager"),pagingInfo);
			}
			var ticketGroupGridColumnpicker = new Slick.Controls.ColumnPicker(ticketGroupColumns, ticketGroupGrid,
					ticketGroupOptions);

			// move the filter panel defined in a hidden div into ticketGroupGrid top panel
			$("#ticketGroup_inlineFilterPanel").appendTo(ticketGroupGrid.getTopPanel()).show();
			ticketGroupGrid.onSort.subscribe(function(e, args) {
				ticketGroupGridSortdir = args.sortAsc ? 1 : -1;
				ticketGroupGridSortcol = args.sortCol.field;
				if ($.browser.msie && $.browser.version <= 8) {
					ticketGroupDataView.fastSort(ticketGroupGridSortcol, args.sortAsc);
				} else {
					ticketGroupDataView.sort(ticketGroupGridComparer, args.sortAsc);
				}
			});

			ticketGroupGrid.onCellChange.subscribe(function (e,args) { 
					var temprEventRowIndex = ticketGroupGrid.getSelectedRows();
					var ticketGroupId;
					var eventNotes; 
					$.each(temprEventRowIndex, function (index, value) {
						ticketGroupId = ticketGroupGrid.getDataItem(value).id;
						ticketGroupNotes = ticketGroupGrid.getDataItem(value).notes;
					});
					saveNotes(ticketGroupId,ticketGroupNotes);
	         });
			// wire up model ticketGroups to drive the ticketGroupGrid
			ticketGroupDataView.onRowCountChanged.subscribe(function(e, args) {
				ticketGroupGrid.updateRowCount();
				ticketGroupGrid.render();
			});
			ticketGroupDataView.onRowsChanged.subscribe(function(e, args) {
				ticketGroupGrid.invalidateRows(args.rows);
				ticketGroupGrid.render();
			});

			// wire up the search textbox to apply the filter to the model
			$("#ticketGroupGridSearch").keyup(function(e) {
				Slick.GlobalEditorLock.cancelCurrentEdit();
				// clear on Esc
				if (e.which == 27) {
					this.value = "";
				}
				ticketGroupGridSearchString = this.value;
				updateTicketGroupGridFilter();
			});
			function updateTicketGroupGridFilter() {
				ticketGroupDataView.setFilterArgs({
					ticketGroupGridSearchString : ticketGroupGridSearchString
				});
				ticketGroupDataView.refresh();
			}
			
			// initialize the model after all the ticketGroups have been hooked up
			ticketGroupDataView.beginUpdate();
			ticketGroupDataView.setItems(ticketGroupData);
			ticketGroupDataView.setFilterArgs({
				percentCompleteThreshold : percentCompleteThreshold,
				ticketGroupGridSearchString : ticketGroupGridSearchString
			});
			ticketGroupDataView.setFilter(ticketGroupGridFilter);
			ticketGroupDataView.endUpdate();
			ticketGroupDataView.syncGridSelection(ticketGroupGrid, true);
			$("#gridContainer").resizable();
			ticketGroupGrid.resizeCanvas();
			 $("div#divLoading").removeClass('show');
		}
		
		function getSelectedTicketGroupGridId() {
			var temprTicketGroupRowIndex = ticketGroupGrid.getSelectedRows();
			
			var ticketGroupIdStr='';
			$.each(temprTicketGroupRowIndex, function (index, value) {
				ticketGroupIdStr += ','+ticketGroupGrid.getDataItem(value).id;
			});
			
			if(ticketGroupIdStr != null && ticketGroupIdStr!='') {
				ticketGroupIdStr = ticketGroupIdStr.substring(1, ticketGroupIdStr.length);
				 return ticketGroupIdStr;
			}
		}

window.onload = function() {
	pagingInfo=JSON.parse(JSON.stringify(${pagingInfo}));
	refreshTicketGroupGridValues();	
};
</script>


