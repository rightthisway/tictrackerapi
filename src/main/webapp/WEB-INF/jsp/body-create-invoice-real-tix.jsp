<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<!-- Bootstrap CSS -->    
    <link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
    <link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../resources/css/font-awesome.min.css" rel="stylesheet" />    
	<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>
    <link rel="stylesheet" href="../resources/css/owl.carousel.css" type="text/css">
	<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
	<link rel="stylesheet" href="../resources/css/fullcalendar.css">
	<link href="../resources/css/widgets.css" rel="stylesheet">
    <link href="../resources/css/style.css" rel="stylesheet">
    <link href="../resources/css/style-responsive.css" rel="stylesheet" />
	<link href="../resources/css/xcharts.min.css" rel=" stylesheet">	
	<link href="../resources/css/datepicker.css" rel="stylesheet">
	<link href="../resources/css/jquery.alerts.css" rel="stylesheet">
	<script src="../resources/js/jquery-1.8.3.min.js"></script>
	<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
	<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
    <script src="../resources/js/bootstrap.min.js"></script>
    <script src="../resources/js/jquery.scrollTo.min.js"></script>
    <script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../resources/js/bootstrap-datepicker.js"></script>
    <script src="../resources/js/jquery.alerts.js"></script>
 


<style>
.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: #f5f5f5;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#ui-datepicker-div{
	top:580px !important;
}

input{
		color : black !important;
	}

</style>

<script>
$(document).ready(function(){
	showHideFedexInfo();
	$('#shippingMethod').change(function(){
		showHideFedexInfo();
	});
	$('#expDeliveryDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		todayHighlight: true
    });
});

window.onunload = function () {
    var win = window.opener;
    if (!win.closed) {
    	window.opener.getInvoiceGridData(0);
    }
};

function showHideFedexInfo(){
	if($('#shippingMethod option:selected').html().toLowerCase().indexOf("fedex") >=0){
		$('#trackingInfo').show();
	}else{
		$('#trackingInfo').hide();
	}
	
}

function saveOpenOrder(){
	var rowCount = $('#rowCount').val();
	var totalQty;
	var invoiceQuntity= "${openOrder.qty}";
	if(rowCount>0){
		for(var i=1;i<=rowCount;i++){
			var qty = $('#qty_'+i).val();
			if($('#ticketGroup_'+i).val() >0){
				
			}else{
				jAlert("PO is not mapped, Please Map PO for row No."+i+1);
			}
			if(i==1){
				totalQty= qty;
			}else{
				totalQty += qty;
			}
			var seatLow = $('#seatLow_'+i).val();
			var seatHigh = $('#seatHigh_'+i).val();
			if(qty>0 && seatLow>0 && seatHigh>0){
				if(seatHigh < seatLow){
					jAlert("Seat High cannot be less than Seat Low, Please enter correct values.");
				}
				/* if((seatHigh-seatLow+1)!=qty){
					jAlert("SeatLow SeatHigh range is not matching entered Quantity,Please enter range same as Quantity.");
					return;
				} */
			}else{
				jAlert("SeatLow, SeatHigh and Quntity should be greater then 0");
				return;
			}
		}
	}else{
		jConfirm("Are you Sure you want to clear mapped PO, All uploaded files also removed?","Confirm",function(r) {
			if (r) {
				$("#sendTicketToCustomer").val('No');
				$("#action").val('clearMapping');
				if($('#shippingMethod option:selected').html().toLowerCase().indexOf("fedex") < 0){
					$('#trackingNo').val("");
				}
				$("#openOrderForm").submit();
			}
		});
		return;
	}
	if(totalQty>invoiceQuntity){
		jAlert("Mapped total Quantity cannot be greater then invoice quantity.");
		return;
	}
	if($('#shippingMethod').val()=='-1'){
		jAlert("Please select shipping method.");
		return;
	}
	
	if($('#sendTicket').is(':checked')){
		jConfirm("Are you Sure you want to email ticket attachment to customer?","Confirm",function(r) {
			if (r) {
				$("#sendTicketToCustomer").val('Yes');
				$("#action").val('openOrderSave');
				if($('#shippingMethod option:selected').html().toLowerCase().indexOf("fedex") < 0){
					$('#trackingNo').val("");
				}
				$("#openOrderForm").submit();
			}
		});
	}else{
		$("#sendTicketToCustomer").val('No');
		$("#action").val('openOrderSave');
		if($('#shippingMethod option:selected').html().toLowerCase().indexOf("fedex") < 0){
			$('#trackingNo').val("");
		}
		$("#openOrderForm").submit();
	}
	
	
}

function doVoidInvoice(){
	$('#myModal-2').modal('show');
	/*jConfirm("Do you want to void this Invoice?","Confirm",function(r){		
		if(r){
			
			
			$("#action").val('voidOpenOrder');
			if($('#shippingMethod option:selected').html().toLowerCase().indexOf("fedex") < 0){
				$('#trackingNo').val("");
			}
			$("#openOrderForm").submit();
		}else{
			return false;
		}
	});*/
}

function cancelAction(){
	$('#myModal-2').modal('hide');
	$("#refundType").val("");
	$("#refundAmountDiv").hide();
	$("#walletCCDiv").hide();
	$("#walletDiv").hide();
	$("#rewardPointDiv").hide();
	$('#refundAmount').val('');
	$('#walletCCAmount').val('');
	$('#walletAmount').val('');
	$('#rewardPoint').val('');
	$("#msgDiv").hide();
	$('#msgBox').text('');
}

function downloadTicketFile(invoiceId,fileType,position){
	 var url = "${pageContext.request.contextPath}/Accounting/DownloadRealTix?invoiceId="+invoiceId+"&fileType="+fileType+"&position="+position;
	 $('#download-frame').attr('src', url);
}

function callCloseBtnOnClick(){
	window.close();
}
</script>
<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
<div class="row">
	<div class="col-lg-12">
		<!-- <h3 class="page-header">
			<i class="fa fa-laptop"></i> Deliveries
		</h3> -->
		<ol class="breadcrumb">
			<!-- <li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Deliveries</a>
			</li> -->
			<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Open Order</li>
		</ol>
	</div>
</div>

<c:if test="${successMessage != null}">
	<div class="alert alert-success fade in">
		<strong
			style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${successMessage}</strong>
	</div>
</c:if>
<c:if test="${errorMessage != null}">
	<div class="alert alert-block alert-danger fade in">
		<strong
			style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${errorMessage}</strong>
	</div>
</c:if>

<div class="row">
	<div class="col-lg-12">
		<section class="panel"> <header style="font-size: 13px;font-family: arial;" class="panel-heading">
		Customer Details </header>
		<div class="panel-body">
		<%-- <c:forEach var="openOrder" items="${openOrders}"> --%>
			<div class="form-group">
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Customer Name:</label>
				  	<span style="font-size: 13px;" id="customerNameDiv">${openOrder.customerName}</span> 
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Email:</label>
					<span style="font-size: 15px;">${openOrder.username}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Phone:</label>
					<span style="font-size: 15px;">${openOrder.phone}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Address Line:</label>
					<span style="font-size: 15px;">${openOrder.addressLine1}</span>
                </div>
			</div>
			<div class="form-group"></div>
			<div class="form-group">
				<div class="col-sm-3"> 
					<label style="font-size: 13px;font-weight:bold;">Country:</label>
					<span style="font-size: 15px;">${openOrder.country}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">State:</label>
					<span style="font-size: 15px;">${openOrder.state}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">City:</label>
					<span style="font-size: 15px;">${openOrder.city}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">ZipCode:</label>
					<span style="font-size: 15px;">${openOrder.zipCode}</span>
				</div>
			</div>
			<%-- </c:forEach> --%>
		</div>
		</section>
	</div>
</div>
<br/>
<br/>

<!-- Shipping Address section -->
<div class="row">
	<div class="col-lg-12">
		<section class="panel">
		 <header style="font-size: 13px;font-family: arial;" class="panel-heading">Shipping Address &nbsp;&nbsp;&nbsp;&nbsp;
		 <!-- <input type="checkbox" data-toggle="modal" class="btn btn-primary" name="showShAddr" onclick="showShippingAddress();" id="showShAddr" style="font-size: 13px;font-family: arial;"/>Show Shipping Addres -->
		 </header>
			<div class="panel-body">
			<c:forEach var="shippingAddress" items="${shippingAddress}">
			<div class="form-group">
				<div class="col-sm-3">
					<label style="font-size: 13px; font-weight: bold;">Address Line 1:</label>
					<span style="font-size: 13px;">${shippingAddress.addressLine1}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px; font-weight: bold;">Address Line 2:</label>
					<span style="font-size: 15px;">${shippingAddress.addressLine2}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px; font-weight: bold;">Phone:</label>
					<span style="font-size: 15px;">${shippingAddress.phone1}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px; font-weight: bold;">Country:</label>
					<span style="font-size: 15px;">${shippingAddress.countryName}</span>
				</div>
			</div>
			<div class="form-group">
				
				<div class="col-sm-3">
					<label style="font-size: 13px; font-weight: bold;">State:</label>
					<span style="font-size: 15px;">${shippingAddress.stateName}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px; font-weight: bold;">City:</label>
					<span style="font-size: 15px;">${shippingAddress.city}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px; font-weight: bold;">ZipCOde:</label>
					<span style="font-size: 15px;">${shippingAddress.zipCode}</span>
				</div>
			</div>
			</c:forEach>
		</div>
		</section>
	</div>
</div>


<div class="row">
	<div class="col-lg-12 col-sm-12">
		<section class="panel"> <header style="font-size: 13px;font-family: arial;" class="panel-heading" id="eventDetailDiv">
			${customerOrder.eventName}, ${customerOrder.eventDateStr},${customerOrder.eventTimeStr},${customerOrder.venueName}
		 </header>
		 <%-- <c:forEach var="openOrder" items="${openOrders}" > --%>
		<div class="panel-body">
			<div class="row">
				<div >
					<label style="font-size: 13px;font-weight:bold;" class="col-sm-2">Section:</label>
					<span style="font-size: 12px;" class="col-sm-2">${openOrder.section}</span>
				</div>
				<div>
					<label style="font-size: 13px;font-weight:bold;" class="col-sm-2">Row:</label>
					<span class="col-sm-2">${openOrder.row}</span>
				</div>
				<div >
					<label style="font-size: 13px;font-weight:bold;" class="col-sm-2">Product Type :</label>
					<span class="col-sm-2">${openOrder.productType}</span>
				</div>
			</div>
			
			<div class="row">
				<div >
					<label style="font-size: 13px;font-weight:bold;" class="col-sm-2">Shipping Method : </label><span class="col-sm-2">${openOrder.shippingMethod}</span>
				</div>
				<div >
					<label style="font-size: 13px;font-weight:bold;" class="col-sm-2">Quantity :</label>
					<span class="col-sm-2">${openOrder.qty}</span>
				</div>
				<div>
					<label style="font-size: 13px;font-weight:bold;" class="col-sm-2">Price:</label><span class="col-sm-2">${openOrder.price}</span>
				</div>
				
			</div>
			
		</div>
		<%-- </c:forEach> --%>
		</section>
	</div>
</div>

<!-- Add Real ticket details -->
<div class="row">
	<section class="panel">
		<ul class="nav nav-tabs" style="margin-left:10px;background:#f7f7f7;">
			<li id ="defaultTab" class="active"><a style="font-size: 13px;font-family: arial;" data-toggle="tab" href="#addTickets">Add Tickets</a></li>
			<li class=""><a style="font-size: 13px;font-family: arial;" data-toggle="tab" href="#uploadTickets">Upload Tickets</a></li>
		</ul>
	</section>
</div>
<div class="panel-body">
	<form class="form-validate form-horizontal" id="openOrderForm" method="post" enctype="multipart/form-data"
				action="${pageContext.request.contextPath}/Accounting/SaveOpenOrders">
    <div class="tab-content">
		<div id="addTickets" class="tab-pane active">
			<input type="hidden" name="action" id="action" value="action"/>
			<input type="hidden" name="rowCount" id="rowCount"/>
			<input type="hidden" name="invoiceId" id="invoiceId" value="${openOrder.invoiceId}"/>			
			<input type="hidden" name="eventId" value="${customerOrder.eventId}" />
			<input type="hidden" name="eventName" value="${customerOrder.eventName}"/>
			<input type="hidden" name="eventDate" value="${customerOrder.eventDateStr}"/>
			<input type="hidden" name="eventTime" value="${customerOrder.eventTimeStr}"/>
			<!-- <input type="hidden" name="productType" value="${openOrder.productType}"/> -->
			<input type="hidden" name="section" value="${openOrder.section}"/>
			<input type="hidden" name="row" value="${openOrder.row}"/>
			<input type="hidden" id="primaryPayment" name="primaryPayment" value="${customerOrder.primaryPaymentMethod}" />
			<input type="hidden" id="primaryPaymentAmt" name="primaryPaymentAmt" value="${customerOrder.primaryAvailableAmt}" />
			<input type="hidden" id="secondaryPayment" name="secondaryPayment" value="${customerOrder.secondaryPaymentMethod}" />
			<input type="hidden" id="secondaryPaymentAmt" name="secondaryPaymentAmt" value="${customerOrder.secondaryAvailableAmt}" />
			<input type="hidden" id="thirdPayment" name="thirdPayment" value="${customerOrder.thirdPaymentMethod}" />
			<input type="hidden" id="thirdPaymentAmt" name="thirdPaymentAmt" value="${customerOrder.thirdAvailableAmt}" />
			
			<table class="table table-bordered table-hover" id="tab_logic">
				<thead>
					<tr >
						<th class="text-center">
							#
						</th>
						<th style="font-size: 13px;font-family: arial;" class="text-center">
							Map PO
						</th>
						<th style="font-size: 13px;font-family: arial;" class="text-center">
							Section
						</th>
						<th style="font-size: 13px;font-family: arial;" class="text-center">
							Row
						</th>
						<th style="font-size: 13px;font-family: arial;" class="text-center">
							Seat Low
						</th>
						<th style="font-size: 13px;font-family: arial;" class="text-center">
							Seat High
						</th>
						
						<th style="font-size: 13px;font-family: arial;" class="text-center">
							Qty
						</th>
						<th style="font-size: 13px;font-family: arial;" class="text-center">
							Price
						</th>
						<!-- <th style="font-size: 13px;font-family: arial;" class="text-center">
							Shipping Method
						</th> -->
					</tr>
				</thead>
				<tbody>
					<c:if test="${not empty ticketGroups}">
				 <c:forEach var="ticketGroup" varStatus="vStatus" items="${ticketGroups}">
                      <tr id="addr_${vStatus.count}">
                      <td>${vStatus.count}
                      	  <input type="hidden" name="rowNumber" id="rowNumber_${vStatus.count}" value="${vStatus.count}" />
						  <input type="hidden" name="id_${vStatus.count}" id="id_${vStatus.count}" value="${ticketGroup.id}" />
                      </td>
                      <td style="font-size: 13px;" align="center">
                     		<a href="javascript:loadPOByEvent(${customerOrder.eventId},${vStatus.count},${ticketGroup.id})">Use Long Inventory</a>
                     		 <input type="hidden" name="ticketGroup_${vStatus.count}" id="ticketGroup_${vStatus.count}" value="${ticketGroup.id}" />
						</td>
						<td style="font-size: 13px;" align="center">
                     		<input name="section_${vStatus.count}" id="section_${vStatus.count}" readonly="readonly" type='text' value="${ticketGroup.section}" placeholder='Section' class='form-control input-md' />
						</td>
						<td style="font-size: 13px;" align="center">
                     		<input name="row_${vStatus.count}" id="row_${vStatus.count}" type='text' readonly="readonly" value="${ticketGroup.row}" placeholder='Row' class='form-control input-md' />
						</td>
						<td style="font-size: 13px;" align="center">
                     		<input name="seatLow_${vStatus.count}" id="seatLow_${vStatus.count}" type='text' value="${ticketGroup.mappedSeatLow}" placeholder='Seat Low' class='form-control input-md' />
						</td>
						<td style="font-size: 13px;" align="center">
                     		<input name="seatHigh_${vStatus.count}" id="seatHigh_${vStatus.count}" type='text' value="${ticketGroup.mappedSeatHigh}" placeholder='Seat High' class='form-control input-md' />
						</td>
						<td style="font-size: 13px;" align="center">
                     		<input name="qty_${vStatus.count}" id="qty_${vStatus.count}" type='text' placeholder='Qty' value="${ticketGroup.mappedQty}" class='form-control input-md' />
						</td>
						<td style="font-size: 13px;" align="center">
                     		<input name="price_${vStatus.count}" id="price_${vStatus.count}" type='text' readonly="readonly" placeholder='Price' value="${ticketGroup.priceStr}" class='form-control input-md' />
						</td>
					</tr>
					</c:forEach>
					</c:if>
				</tbody>
			</table>
			<div class="row">
				<a id="add_row" onclick="callAddRealTicketRow();" class="btn btn-default btn-sm pull-left" style="margin-left:16px;"  onclick="callAddRealTicketRow();">Add Row</a>
				<a id='delete_row' class='btn btn-default btn-sm pull-left' onclick="callDeleteRealTicketRow();">Delete Row</a>
			</div>
			<br/>
		<div class="row" style="margin-left:35px;">
			<div class="col-lg-12">
		          
		          <div class="col-lg-3 col-sm-3 ">
					<label>Shipping Method</label>
		          	<select id="shippingMethod" name="shippingMethod" class="form-control input-sm m-bot15">
					  <option value="-1">--select--</option>
					  <c:forEach items="${shippingMethods}" var="shippingMethod">
						<option <c:if test="${shippingMethod.id==openOrder.shippingMethodId}"> Selected </c:if>
							value="${shippingMethod.id}"> ${shippingMethod.name}
						</option>
					</c:forEach>
					</select>
		          </div>
				 
				<div id="trackingInfo" class="col-lg-3 col-sm-3 ">
					<label>Tracking No</label>
		          	<input type="text" id="trackingNo" name="trackingNo" class="form-control input-sm m-bot15" value="${invoice.trackingNo}">
		         </div>
		          
		          <div class="col-lg-3 col-sm-3 ">
					<label>Shipment Completed</label>
		          	<select id="realTixDelivered" name="realTixDelivered" class="form-control input-sm m-bot15">
						<option value='0'<c:if test="${openOrder ne null and !openOrder.realTixDelivered}">selected</c:if>>No</option>
						<option value='1' <c:if test="${openOrder ne null and openOrder.realTixDelivered}">selected</c:if>>Yes</option> 
					</select>
		          </div>
				  <div class="col-lg-3 col-sm-3 ">
					<label>Expected Delivery Date</label>
		          	<input type="text" id="expDeliveryDate" name="expDeliveryDate" class="form-control input-sm m-bot15" value="${expDeliveryDate}">
		         </div>
				 </div>
		     </div>
		     <%-- <div class="row" style="margin-left:35px;">
				<div class="col-lg-12">
		          <div class="col-lg-3 col-sm-3 ">
		         	 <label>Purchase Order No</label>
						<input type="text" id="poNo" class="form-control input-sm m-bot15" name="poNo" value="${invoice.poNo}">
		          </div>
		       </div>
		     </div> --%>
		</div>
		
		<div id="uploadTickets" class="tab-pane" style="margin-left:20px;">
			<div class="form-group">
				<div class="col-sm-4">
					<a id="eticket_add_row" class="btn btn-default btn-sm pull-left" style="margin-left:16px;"  onclick="addTicketUploadRow('eticketTable');">Add Row</a>
				<a id='eticket_delete_row' class='btn btn-default btn-sm pull-left' onclick="deleteTicketUploadRow('eticketTable');">Delete Row</a>
				</div>
				<div class="col-sm-4">
					<a id="qrcode_add_row" class="btn btn-default btn-sm pull-left" style="margin-left:16px;"  onclick="addTicketUploadRow('qrcodeTable');">Add Row</a>
				<a id='qrcode_delete_row' class='btn btn-default btn-sm pull-left' onclick="deleteTicketUploadRow('qrcodeTable');">Delete Row</a>
				</div>
				<div class="col-sm-4">
					<a id="barcode_add_row" class="btn btn-default btn-sm pull-left" style="margin-left:16px;"  onclick="addTicketUploadRow('barcodeTable');">Add Row</a>
				<a id='barcode_delete_row' class='btn btn-default btn-sm pull-left' onclick="deleteTicketUploadRow('barcodeTable');">Delete Row</a>
				</div>
			</div>
			
			<div class="form-group">
			<div class="col-sm-4">
				<table class="table table-bordered table-hover" id="eticketTable">
				<input type="hidden" name="eticketTable_count" id="eticketTable_count" value="${eticketCount}">
					<thead>
						<tr >
							<th class="text-center">
								#
							</th>
							<th style="font-size: 13px;font-family: arial;" class="text-center">
								E-ticket
							</th>
						</tr>
					</thead>
					<tbody>
					<c:if test="${not empty eticketAttachments}">
						 <c:forEach var="eticket" varStatus="vStatus" items="${eticketAttachments}">
                      		<tr id="eticketTable_${eticket.position}" class="savedClass">
	                      		<td>${vStatus.count}</td>
	                      		<td> 
	                      			<input type="file" id="eticket_${eticket.position}" name="eticket_${eticket.position}"/><br/>
	                      			<a href="javascript:downloadTicketFile('${eticket.invoiceId}','${eticket.type}','${eticket.position}')" >${eticket.fileName}</a>
	                      			<%-- <img class="editClickableImage" style="height:14px;" src="../resources/images/ico-delete.gif" onclick="deleteFile('${eticket.invoiceId}','${eticket.type}','${eticket.position}')"/> --%>
	                      		</td>
                      		</tr>
                      	</c:forEach>
                    </c:if>
                    </tbody>
				</table>
				</div>
				<div class="col-sm-4">
					<table class="table table-bordered table-hover" id="qrcodeTable">
					<input type="hidden" name="qrcodeTable_count" id="qrcodeTable_count" value="${qrcodeCount}">
					<thead>
						<tr >
							<th class="text-center">
								#
							</th>
							<th style="font-size: 13px;font-family: arial;" class="text-center">
								Mobile Entry
							</th>
							
						</tr>
					</thead>
					<tbody>
					<c:if test="${not empty qrcodeAttachments}">
						 <c:forEach var="qrcode" varStatus="vStatus" items="${qrcodeAttachments}">
                      		<tr id="qrcodeTable_${qrcode.position}" class="savedClass">
	                      		<td>${vStatus.count}</td>
	                      		<td> 
	                      			<input type="file" id="qrcode_${qrcode.position}" name="qrcode_${qrcode.position}"/><br/>
	                      			<a href="javascript:downloadTicketFile('${qrcode.invoiceId}','${qrcode.type}','${qrcode.position}')" >${qrcode.fileName}</a>
	                      			<%-- <img class="editClickableImage" style="height:14px;" src="../resources/images/ico-delete.gif" onclick="deleteFile('${qrcode.invoiceId}','${qrcode.type}','${qrcode.position}')"/> --%>
	                      		</td>
                      		</tr>
                      	</c:forEach>
                    </c:if>
                    </tbody>
				</table>
				</div>
				<div class="col-sm-4">
					<table class="table table-bordered table-hover" id="barcodeTable">
					<input type="hidden" name="barcodeTable_count" id="barcodeTable_count" value="${barcodeCount}">
					<thead>
						<tr >
							<th class="text-center">
								#
							</th>
							<th style="font-size: 13px;font-family: arial;" class="text-center">
								Electronic Transfer 
							</th>
						</tr>
					</thead>
					<tbody>
					<c:if test="${not empty barcodeAttachments}">
						 <c:forEach var="barcode" varStatus="vStatus" items="${barcodeAttachments}">
                      		<tr id="barcodeTable_${barcode.position}" class="savedClass">
	                      		<td>${vStatus.count}</td>
	                      		<td> 
	                      			<input type="file" id="barcode_${barcode.position}" name="barcode_${barcode.position}"/><br/>
	                      			<a href="javascript:downloadTicketFile('${barcode.invoiceId}','${barcode.type}','${barcode.position}')" >${barcode.fileName}</a>
	                      			<%-- <img class="editClickableImage" style="height:14px;" src="../resources/images/ico-delete.gif" onclick="deleteFile('${barcode.invoiceId}','${barcode.type}','${barcode.position}')"/> --%>
	                      		</td>
                      		</tr>
                      	</c:forEach>
                    </c:if>
                    </tbody>
				</table>
				</div>
			</div>
			<div align="center">
				 <input type="checkbox" id="sendTicket" name="sendTicket"<%--  <c:if test="${invoice.uploadToExchnge == 'true'}">checked</c:if> --%>>
				<input type="hidden" id="sendTicketToCustomer" name="sendTicketToCustomer" value="">
				 <label >Email Ticket Attachment to Customer</label>
			</div>
			<br/><br/>
		</div>
	</div>
</div>

			<!-- Toggle add shipping address form-->
				<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog"
					tabindex="-1" id="myModal-2" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button aria-hidden="true" data-dismiss="modal" class="close"
									type="button">Close</button>
								<h4 class="modal-title">Void Invoice</h4>
							</div>
							<div class="panel-body">
								<div class="modal-body">

									<input id="action" value="addShippingAddress" name="action"
										type="hidden" /> <input type="hidden" name="shippingAddrId"
										id="shippingAddrId" value="" /> <input class="form-control"
										id="custId" value="${customerId}" name="custId" type="hidden" />
									<input type="hidden" name="count" id="count" value="${count}" />
									<div class="form-group">										
										<div class="col-sm-4"> &nbsp; </div>
										<div class="col-sm-4">
											<label>Refund Type <span class="required">*</span> </label> 
											<select id="refundType" name="refundType" class="form-control input-sm m-bot15" onchange="loadRefundType(this)">
												<option value="">--select--</option>
												<option value="NOREFUND">No Refund</option>
												<option value="FULLREFUND">Full Refund</option>
												<option value="PARTIALREFUND">Partial Refund</option>
											</select>
										</div>
										<div class="col-sm-4"> &nbsp; </div>
									</div>
									<div class="row"></div>
									<div class="form-group">
										<div class="col-sm-4" style="float: left; display:none;" id="refundAmountDiv">
											<label>Refund Amount <span class="required">*</span> </label> 
											<input class="form-control" id="refundAmount" type="text" name="refundAmount" value="" />
										</div>
										<div class="col-sm-4" id="walletCCDiv" style="display:none;">
											<label>Credit To Customer Wallet From Credit Card/PayPal<span class="required">*</span> </label> 
											<input class="form-control" id="walletCCAmount" type="text" name="walletCCAmount" value="" />
										</div>
										<div class="col-sm-4" id="walletDiv" style="display:none;">
											<label>Credit To Customer Wallet From Wallet<span class="required">*</span> </label> 
											<input class="form-control" id="walletAmount" type="text" name="walletAmount" value="" />
										</div>
										<div class="col-sm-2" id="rewardPointDiv" style="display:none;">
											<label>Reward Points <span class="required">*</span> </label> 
											<input class="form-control" id="rewardPoint" type="text" name="rewardPoint" value="" />
										</div>
										<div class="col-sm-12" id="msgDiv" style="display:none;">
											<div class="alert alert-block alert-danger fade in">
												<span id="msgBox" style="display: block; text-align: center;"></span>
											</div>
										</div>
									</div>
									<div class="row"></div>
									<div class="col-lg-12">&nbsp;</div>
									<div class="form-group">
										<div class="col-sm-6">
											<button class="btn btn-primary" type="button"
												onclick="saveRefund()">Void Invoice</button>
											<button class="btn btn-default" onclick="cancelAction();"
												type="button">Cancel</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
 			
			<div align="center">
				<c:if test="${invoice.status != 'Voided'}">
					<button type="button" onclick="saveOpenOrder();" class="btn btn-primary btn-sm">Save</button>
					<c:if test="${invoice.realTixMap != 'YES'}">
						<button class="btn btn-danger" type="button" onclick="doVoidInvoice();">Void Invoice</button>
					</c:if>
				</c:if>
				<button type="button" onclick="callCloseBtnOnClick();" class="btn btn-default btn-sm">Close</button>
			</div>
		</form>
		 
<script>
var i = $('#tab_logic tr').length-1;
$('#rowCount').val(i);
function callAddRealTicketRow() {
	i++;
	$('#tab_logic').append('<tr id="addr_'+i+'"></tr>');
	
	 $('#addr_'+i).html("<td>"+ (i) +"</td>"+
			 "<td><a href='javascript:loadPOByEvent(${customerOrder.eventId},"+i+",0)'>Check Long Inventory</a>"+
			 "<input id ='ticketGroup_"+i+"' name='ticketGroup_"+i+"' type='hidden' value='' /> </td> " +
	 		"<td><input id='section_"+i+"' readonly='readonly' name='section_"+i+"' type='text' placeholder='Section' class='form-control input-md' /> </td> " +
		   "<td><input id='row_"+i+"' readonly='readonly' name='row_"+i+"' type='text' placeholder='Row'  class='form-control input-md'></td> "+
		   "<td><input id='seatLow_"+i+"'  name='seatLow_"+i+"' type='text' placeholder='Seat Low'  class='form-control input-md'></td> " +
		   "<td><input id='seatHigh_"+i+"' name='seatHigh_"+i+"' type='text' placeholder='Seat High'  class='form-control input-md'></td> " +
			"<td><input id='qty_"+i+"' name='qty_"+i+"' type='text' placeholder='Quantity'  class='form-control input-md'></td> " +
			"<td><input id='price_"+i+"' readonly='readonly' name='price_"+i+"' type='text' placeholder='Price'  class='form-control input-md'></td>" +
			"");
	$('#rowCount').val(i);
}
function callDeleteRealTicketRow() {
   	 if($('#tab_logic tr').length > 1){
   		 $("#addr_"+i).remove();
		 i--;
	 }
   	$('#rowCount').val(i);
}


function addTicketUploadRow(tableName) {
	var j = $('#'+tableName+' tr').length-1;
	j++;
	$('#'+tableName).append('<tr id="'+tableName+'_'+j+'"></tr>');
	var rowString = '';
	if(tableName=='eticketTable'){
		rowString = '<td>'+ (j) +'</td>'+
			//'<td><input type="checkbox" id="eticketCheck_'+j+'" class="eticketClass" name="eticketRow_'+j+'"></td>'+
			'<td> <input type="file" id="eticket_'+j+'" name="eticket_'+j+'"/></td>';
			
	}else if(tableName=='qrcodeTable'){
		rowString = '<td>'+ (j) +'</td>'+
			//'<td><input type="checkbox" id="barcodeCheck_'+j+'" class="qrcodeClass" name="qrcodeRow_'+j+'"></td>'+
			'<td> <input type="file" id="qrCode_'+j+'" name="qrcode_'+j+'"/></td>';
	}else if(tableName=='barcodeTable'){
		rowString = '<td>'+ (j) +'</td>'+
			//'<td><input type="checkbox" id="qrcodeCheck_'+j+'" class="barcodeClass" name="barcodeRow_'+j+'"></td>'+
			'<td> <input type="file" id="barcode_'+j+'" name="barcode_'+j+'"/></td>';
	}
	$('#'+tableName+'_count').val(j);
	$('#'+tableName+'_'+j).html(rowString);
}
function deleteTicketUploadRow(tableName) {
	var count = $('#'+tableName+' tr').length-1;
	if($('#'+tableName+'_'+count).hasClass("savedClass")){
		var fileType = '';
		if(tableName=='eticketTable'){
			fileType = 'ETICKET';
		}else if(tableName=='barcodeTable'){
			fileType = 'BARCODE';
		}else if(tableName=='qrcodeTable'){
			fileType = 'QRCODE';
		} 
		deleteFile(tableName,fileType,count);
	}else{
		$('#'+tableName+'_'+count).remove();
		$('#'+tableName+'_count').val(count-1);
	}
}
function deleteFile(tableName,fileType,position){
	jConfirm("Are you Sure you want delete attach file?","Confirm",function(r) {
		if (r) {
			$.ajax({
				url : "${pageContext.request.contextPath}/Accounting/DeleteRealTixAttachment",
				type : "post",
				dataType:"text",
				data : "invoiceId="+$('#invoiceId').val()+"&fileType="+fileType+"&position="+position,
				success : function(res){
					jAlert(res);
					$('#'+tableName+'_'+position).remove();
					$('#'+tableName+'_count').val(position-1);
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
	});
	 
}

function loadRefundType(obj){
	var primaryPayMethod = $('#primaryPayment').val();
	var secondaryPayMethod = $('#secondaryPayment').val();
	var thirdPayMethod = $('#thirdPayment').val();
	
	if(obj.value == 'PARTIALREFUND'){
		if(primaryPayMethod == 'CREDITCARD' || primaryPayMethod == 'PAYPAL' 
				|| primaryPayMethod == 'GOOGLEPAY' || primaryPayMethod == 'IPAY'){			
			$("#refundAmountDiv").show();
			$("#walletCCDiv").show();
			//$("#walletDiv").show();
		}
		else if(primaryPayMethod == 'CUSTOMER_WALLET'){
			if(secondaryPayMethod == 'CREDITCARD' || secondaryPayMethod == 'IPAY' || 
					secondaryPayMethod == 'GOOGLEPAY'){
				$("#refundAmountDiv").show();
				$("#walletCCDiv").show();
				$("#walletDiv").show();	
			}else{
				$("#walletDiv").show();
			}
		}
		else if(primaryPayMethod == 'FULL_REWARDS'){
			//$("#rewardPointDiv").show();
		}
		else if(primaryPayMethod == 'PARTIAL_REWARDS'){
			//$('#rewardPoint').val($('#primaryPaymentAmt').val());
			if(secondaryPayMethod == 'CREDITCARD' || secondaryPayMethod == 'PAYPAL'
				|| secondaryPayMethod == 'IPAY' || secondaryPayMethod == 'GOOGLEPAY'){
				$("#refundAmountDiv").show();
				$("#walletCCDiv").show();
				//$("#walletDiv").show();
				//$("#rewardPointDiv").show();
			}
			else if(secondaryPayMethod == 'CUSTOMER_WALLET' && thirdPayMethod == 'CREDITCARD'){
				$("#refundAmountDiv").show();
				$("#walletCCDiv").show();
				$("#walletDiv").show();
				//$("#rewardPointDiv").show();
			}
			else if(secondaryPayMethod == 'CUSTOMER_WALLET'){
				$("#walletDiv").show();
				//$("#rewardPointDiv").show();
			}
			/* else if(secondaryPayMethod == 'IPAY' || secondaryPayMethod == 'GOOGLEPAY'){
				$("#msgDiv").show();
				$('#msgBox').text('Cannot refund as selected invoice payment is made using '+secondaryPayMethod);
			} */
		}
		/* else if(primaryPayMethod == 'IPAY' || primaryPayMethod == 'GOOGLEPAY'){
			$("#msgDiv").show();
			$('#msgBox').text('Cannot refund as selected invoice payment is made using '+primaryPayMethod);
		} */
	}
	else if(obj.value == 'FULLREFUND'){
		/*  if(primaryPayMethod == 'IPAY' || primaryPayMethod == 'GOOGLEPAY'){
			allDivHide();
			$("#msgDiv").show();
			$('#msgBox').text('Cannot refund as selected invoice payment is made using '+primaryPayMethod);
		} 
		else if(primaryPayMethod == 'PARTIAL_REWARDS'){
			if(secondaryPayMethod == 'IPAY' || secondaryPayMethod == 'GOOGLEPAY'){
				allDivHide();
				$("#msgDiv").show();
				$('#msgBox').text('Cannot refund as selected invoice payment is made using '+secondaryPayMethod);
			}else{
				allDivHide();
				$("#msgDiv").hide();
				$('#msgBox').text('');
			}
		}
		else{
			allDivHide();
			$("#msgDiv").hide();
			$('#msgBox').text('');
		} */
	}else{
		allDivHide();
		$("#msgDiv").hide();
		$('#msgBox').text('');
	}
}

function allDivHide(){
	$("#refundAmountDiv").hide();
	$("#walletCCDiv").hide();
	$("#walletDiv").hide();
	$("#rewardPointDiv").hide();
	$('#refundAmount').val('');
	$('#walletCCAmount').val('');
	$('#walletAmount').val('');
	$('#rewardPoint').val('');
}

function saveRefund(){
	var refundType = $('#refundType').val();
	var primaryPayMethod = $('#primaryPayment').val();
	var secondaryPayMethod = $('#secondaryPayment').val();	
	var thirdPayMethod = $('#thirdPayment').val();
	var primaryPayAmt = parseFloat($('#primaryPaymentAmt').val());
	var secondaryPayAmt = parseFloat($('#secondaryPaymentAmt').val());
	var thirdPayAmt = parseFloat($('#thirdPaymentAmt').val());
	var refundAmount = parseFloat($('#refundAmount').val());
	var walletAmt = parseFloat($('#walletAmount').val());
	var walletCCAmt = parseFloat($('#walletCCAmount').val());
	var rewardPoints = parseFloat($('#rewardPoint').val());
	var isValid = false;
	if(refundType=='PARTIALREFUND'){
		if(primaryPayMethod == 'CREDITCARD' || primaryPayMethod == 'PAYPAL' || primaryPayMethod == 'GOOGLEPAY' || primaryPayMethod == 'IPAY'){
			if(($('#refundAmount').val() != null && $('#refundAmount').val() != '') || ($('#walletCCAmount').val() != null && $('#walletCCAmount').val() != '')){
				if($('#refundAmount').val() > 0 || $('#walletCCAmount').val() > 0){
					if(refundAmount > primaryPayAmt){
						jAlert("Refund amount should not be greater than "+primaryPayAmt);
						return false;
					}
					else if(walletCCAmt > primaryPayAmt){
						jAlert("Credit to Wallet from paypal/cc amount should not be greater than "+primaryPayAmt);
						return false;
					}
					else if((refundAmount+walletCCAmt) > primaryPayAmt){
						jAlert("Total of (Refund amount + Credit to wallet from paypal/cc) amount should not be greater than "+primaryPayAmt);
						return false;
					}else{
						isValid = true;
					}
				}else{
					jAlert("Refund amount/Credit to wallet from paypal/cc amount should be greater than 0");
					return false;
				}
			}
			else{
				jAlert("Refund amount/Credit to wallet from paypal/cc amount should not be empty");
				return false;
			}
		}
		else if(primaryPayMethod == 'CUSTOMER_WALLET'){
			if(secondaryPayMethod == 'CREDITCARD' || secondaryPayMethod == 'GOOGLEPAY' || secondaryPayMethod == 'IPAY'){
				if(($('#refundAmount').val() != null && $('#refundAmount').val() != '') || ($('#walletCCAmount').val() != null && $('#walletCCAmount').val() != '') || ($('#walletAmount').val() != null && $('#walletAmount').val() != '')){
					if($('#refundAmount').val() > 0 || $('#walletCCAmount').val() > 0 || $('#walletAmount').val() > 0){
						if(refundAmount > secondaryPayAmt){
							jAlert("Refund amount should not be greater than "+secondaryPayAmt);
							return false;
						}
						else if(walletAmt > primaryPayAmt){
							jAlert("Credit to wallet from Wallet amount should not be greater than "+primaryPayAmt);
							return false;
						}
						else if((refundAmount+walletCCAmt) > secondaryPayAmt){
							jAlert("Total of (Refund amount + Credit to wallet from paypal/cc) amount should not be greater than "+secondaryPayAmt);
							return false;
						}else{
							isValid = true;
						}
					}else{
						jAlert("Refund amount/Credit to wallet from (paypal/cc/wallet) amount should be greater than 0");
						return false;
					}
				}
				else{
					jAlert("Refund/Credit to wallet from paypal/cc amount should not be empty.");
					return false;
				}
			}
			else{
				if($('#walletAmount').val() != null && $('#walletAmount').val() != ''){
					if($('#walletAmount').val() > 0){
						if(walletAmt > primaryPayAmt){
							jAlert("Credit to wallet from Wallet amount should not be greater than "+primaryPayAmt);
							return false;			
						}else{
							isValid = true;
						}
					}else{
						jAlert("Credit to wallet from Wallet amount should be greater than 0");
						return false;
					}
				}
				else{
					jAlert("Credit to wallet from Wallet amount should not be empty.");
					return false;			
				}
			}		
		}
		else if(primaryPayMethod == 'FULL_REWARDS'){
			isValid = true;
			/*if($('#rewardPoint').val() != null && $('#rewardPoint').val() != ''){
				if(rewardPoints > primaryPayAmt){
					jAlert("Reward points should be less than or equal to "+primaryPayAmt);
					return false;
				}else{
					isValid = true;
				}
			}
			else{
				jAlert("Reward points should not be empty.");
				return false;
			}*/
		}
		else if(primaryPayMethod == 'PARTIAL_REWARDS'){
			if(secondaryPayMethod == 'CREDITCARD' || secondaryPayMethod == 'PAYPAL' || secondaryPayMethod == 'GOOGLEPAY' || secondaryPayMethod == 'IPAY'){
				if(($('#refundAmount').val() != null && $('#refundAmount').val() != '') || ($('#walletCCAmount').val() != null && $('#walletCCAmount').val() != '')){ // && ($('#rewardPoint').val() != null && $('#rewardPoint').val() != '')){
					if($('#refundAmount').val() > 0 || $('#walletCCAmount').val() > 0){
						/*if(rewardPoints > primaryPayAmt){
							jAlert("Reward points should be less than or equal to "+primaryPayAmt);
							return false;
						}
						else */
						if(refundAmount > secondaryPayAmt){
							jAlert("Refund amount should not be greater than "+secondaryPayAmt);
							return false;
						}
						else if((refundAmount+walletCCAmt) > secondaryPayAmt){
							jAlert("Total of (Refund amount + Credit to wallet from paypal/cc) amount should not be greater than "+secondaryPayAmt);
							return false;
						}else{
							isValid = true;
						}
					}else{
						jAlert("Refund amount/Credit to wallet from paypal/cc amount should be greater than 0");
						return false;
					}
				}
				else{
					//jAlert("Refund/Wallet amount/Reward Points should not be empty.");
					jAlert("Refund amount/Credit to wallet from paypal/cc amount should not be empty.");
					return false;
				}
			}
			else if(secondaryPayMethod == 'CUSTOMER_WALLET' && (thirdPayMethod == 'CREDITCARD' || secondaryPayMethod == 'GOOGLEPAY' || secondaryPayMethod == 'IPAY')){
				if(($('#refundAmount').val() != null && $('#refundAmount').val() !='') || ($('#walletCCAmount').val() != null && $('#walletCCAmount').val() != '')
						|| ($('#walletAmount').val() != null && $('#walletAmount').val() != '')){ //&& ($('#rewardPoint').val() != null && $('#rewardPoint').val() != '')){
					if($('#refundAmount').val() > 0 || $('#walletCCAmount').val() > 0 || $('#walletAmount').val() > 0){
						/*if(rewardPoints > primaryPayAmt){
							jAlert("Reward points should be less than or equal to "+primaryPayAmt);
							return false;
						}
						else */
						if(walletAmt > secondaryPayAmt){
							jAlert("Wallet amount should not be greater than "+secondaryPayAmt);
							return false;
						}
						else if(refundAmount > thirdPayAmt){
							jAlert("Refund amount should not be greater than "+thirdPayAmt);
							return false;
						}
						else if((refundAmount+walletCCAmt) > thirdPayAmt){
							jAlert("Total of (Refund amount + Credit to wallet from paypal/cc) amount should not be greater than "+thirdPayAmt);
							return false;
						}else{
							isValid = true;
						}
					}else{
						jAlert("Refund amount/Credit to wallet from (paypal/cc/wallet) amount should be greater than 0");
						return false;
					}
				}
				else{
					//jAlert("Refund/Wallet Amount/Reward Points should not be empty.");
					jAlert("Refund amount/Credit to wallet from paypal/cc Amount should not be empty.");
					return false;
				}
			}
			else if(secondaryPayMethod == 'CUSTOMER_WALLET'){
				if(($('#walletAmount').val() != null && $('#walletAmount').val() != '')){	//($('#rewardPoint').val() != null && $('#rewardPoint').val() != '') && 
					if($('#walletAmount').val() > 0){
						/*if(rewardPoints > primaryPayAmt){
							jAlert("Reward points should be less than or equal to "+primaryPayAmt);
							return false;
						}
						else */
						if(walletAmt > secondaryPayAmt){
							jAlert("Credit to wallet from Wallet amount should not be greater than "+secondaryPayAmt);
							return false;
						}else{
							isValid = true;
						}
					}else{
						jAlert("Credit to wallet from Wallet amount should be greater than 0");
						return false;
					}
				}
				else{
					//jAlert("Reward Points/Wallet amount should not be empty.");
					jAlert("Credit to wallet from Wallet amount should not be empty.");
					return false;
				}
			}
		}
	}else if(refundType=='NOREFUND' || refundType=='FULLREFUND'){
		isValid = true;
	}else{
		isValid = false;
		jAlert("Please select valid refund option.");
	}
	if(isValid){
		//jAlert("Pass value to Controller..");
		$("#action").val('voidOpenOrder');
		if($('#shippingMethod option:selected').html().toLowerCase().indexOf("fedex") < 0){
			$('#trackingNo').val("");
		}
		$("#openOrderForm").submit();
	}
	
}

function loadPOByEvent(eventId,i,ticketId){
	if(ticketId==0){
		ticketId = $('#ticketGroup_'+i).val();
	}
	popupCenter("${pageContext.request.contextPath}/Invoice/GetLongInventoryForInvoice?eventId="+eventId+"&rowId="+i+"&ticketId="+ticketId, "Choose PO", "650", "500");
}

//show the pop window center
function popupCenter(url, title, w, h) {  
    // Fixes dual-screen position                         Most browsers      Firefox  
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;  
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;  
              
    width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;  
    height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;  
              
    var left = ((width / 2) - (w / 2)) + dualScreenLeft;  
    var top = ((height / 2) - (h / 2)) + dualScreenTop;  
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);  
  
    // Puts focus on the newWindow  
    if (window.focus) {  
        newWindow.focus();  
    }  
}

var teamTable = document.getElementById("tab_logic");
if(teamTable != null && teamTable != '' && $('#tab_logic tr').length <= 1) {
	callAddRealTicketRow();	
}
</script>