<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap CSS -->    
    <link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../resources/css/font-awesome.min.css" rel="stylesheet" />    
    <!-- full calendar css-->
    <link href="../resources/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="../resources/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
    <!-- easy pie chart-->
    <link href="../resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <!-- owl carousel -->
    <link rel="stylesheet" href="../resources/css/owl.carousel.css" type="text/css">
	<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
	<link rel="stylesheet" href="../resources/css/fullcalendar.css">
	<link href="../resources/css/widgets.css" rel="stylesheet">
    
    <link href="../resources/css/style-responsive.css" rel="stylesheet" />
	<link href="../resources/css/xcharts.min.css" rel=" stylesheet">	
	
	<link href="../resources/css/jquery.alerts.css" rel=" stylesheet">
	
	<!--<link href="../resources/css/jquery-ui-1.10.4.min.css" rel="stylesheet">--> 
	<link href="../resources/css/style.css" rel="stylesheet">

    <script src="../resources/js/jquery-1.8.3.min.js"></script>
    <%-- <script type="text/javascript" src="../resources/js/jquery-ui-1.9.2.custom.min.js"></script> --%>
    
    <!-- bootstrap -->
    <script src="../resources/js/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script src="../resources/js/jquery.scrollTo.min.js"></script>
    <script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../resources/js/jquery.alerts.js"></script>
    
    <!-- slickgrid related scripts -->
	<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
	<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
	<script src="../resources/js/slick/slick.core.js"></script>
	<script src="../resources/js/slick/slick.formatters.js"></script>
	<script src="../resources/js/slick/slick.editors.js"></script>
	<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
	<script src="../resources/js/slick/slick.grid.js"></script>
	<script src="../resources/js/slick/slick.dataview.js"></script>
	<script src="../resources/js/slick/controls/slick.pager.js"></script>
	<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
	<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

	<link href="../resources/css/datepicker.css" rel="stylesheet">
	<script src="../resources/js/bootstrap-datepicker.js"></script>

	<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>
	<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>


<style>
.cell-title { 
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}
.slick-headerrow-column {
      background: #87ceeb;
      text-overflow: clip;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }
    .slick-headerrow-column input {
      margin: 0;
      padding: 0;
      width: 100%;
      height: 100%;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}
.slick-viewport {
    overflow-x: hidden !important;
}
/* .light_box {
	display: none;
	position: absolute;
	top: 10%;
	left: 25%;
	width: 45%;
	height: 100%;
	padding: 20px;
	border: 1px solid #888;
	background-color: #fefefe;
	z-index: 1;
	overflow: auto;
} */

#addCustomer {
  font-family: arial;
  font-size: 8pt;
  font-weight: bold;
  color: #2b2b2b;
  background-color: rgba(0, 122, 255, 0.32);
  border: 1px solid gray;
}

input{
		color : black !important;
	}
	
#totalPrice{
  font-family: arial;
  font-size: 10pt;
  font-weight: bold;
}

#totalQty{
  font-family: arial;
  font-size: 10pt;
  font-weight: bold;
}
</style>
    
<script>
	
	//function to sum the entered quantity
	function sumQty(qty){
		var sum = 0;
		$('.qty').each(function() {
			if($(this).val() != '' && !isNaN($(this).val())){
				sum += parseInt($(this).val());
			}
		});

		$('#totalQty').val(sum);
	}
	
	//function to sum the entered price
	function sumPrice(price){
		var sum = 0;
		$('.price').each(function() {
			if($(this).val() != '' && !isNaN($(this).val())){
				sum += parseFloat($(this).val());
			}
		});

		$('#totalPrice').val(sum);
	}
	var paymentType='';
	$(document).ready(function(){
		 $("div#divLoading").addClass('show');
		 getCustomerGridData(0);
		$('#myTable').hide();
		$('#poInfo').hide();
		
		$('#cardInfo').hide();
		$('#accountInfo').hide();
		$('#chequeInfo').hide();
		
		/*  $('#fromDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation: "top",
		todayHighlight: true
		});
	
		$('#toDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation: "top",
		todayHighlight: true
		}); */
		$('#consignmentPoDiv').hide();
		$('#poTypeCheck').click(function(){
			if($('#poTypeCheck').is(':checked')){
				$('#poType').val("CONSIGNMENT");
				$('#consignmentPoDiv').show();
			}else{
				$('#poType').val("REGULAR");
				$('#consignmentPoDiv').hide();
			}
		});
	
		$('input[type=radio][name=paymentType]').change(function() {
			paymentType=this.value;
		if (this.value == 'card') {
            $('#cardInfo').show();
			$('#accountInfo').hide();
			$('#chequeInfo').hide();
			
        }
        else if (this.value == 'account') {
			$('#chequeInfo').hide();           
		    $('#cardInfo').hide();
			$('#accountInfo').show();
        }else if (this.value == 'cheque') {
			$('#cardInfo').hide();
		    $('#accountInfo').hide();
            $('#chequeInfo').show();
        }
    });
		$('#addTicket').click(function(){
			$('#myTable').show();
			$('#poInfo').show();
			getEventInfoForPO();
		});
		$('#paymentDate').datepicker({
	        format: "mm/dd/yyyy",
			autoclose : true,
			orientation: "top",
			todayHighlight: true
	    });
		
		
		showHideFedexInfo();
		$('#shippingMethod').change(function(){
			showHideFedexInfo();
		});
		
		function showHideFedexInfo(){
			if($('#shippingMethod option:selected').html().toLowerCase().indexOf("fedex") >= 0){
				$('#trackingInfo').show();
			}else{
				$('#trackingInfo').hide();
			}
		}
	
		$(window).resize(function() {
			customerGrid.resizeCanvas();
			eventGrid.resizeCanvas();
		});
		
		$('.searchcontrol').keypress(function (event) {
		var keyCode = (event.keyCode ? event.keyCode : event.which);
		 if(keyCode == 13)  // the enter key code
		  {
			 getEventGridData(0);
			 return false;
		  }
		});
	
	});

	function addCustomerMsg(){
		jAlert("Customer Added successfully.");
	}
	
	//Save purchase order
	function savePurchaseOrder(){
		var isValid = true;
		if($('#shippingMethod option:selected').html().toLowerCase().indexOf("fedex") < 0){
			$('#trackingNo').val("");
		}
		$('.section').each(function() {
			if($(this).val() == '' || $(this).val() == null){
				isValid =false;
			}
		}); 
		if(!isValid){
			 alert("Please add Section.");
			 return;
		}
		$('.rowValue').each(function() {
			if($(this).val() == '' || $(this).val() == null){
				isValid =false;
			}
		}); 
		if(!isValid){
			 alert("Please add Row.");
			 return;
		}
		$('.seatLow').each(function() {
			if($(this).val() == '' || $(this).val() == null){
				isValid =false;
			}
		});
		if(!isValid){
			 alert("Please add Seat Low.");
			 return;
		}
		$('.seatHigh').each(function() {
			if($(this).val() == '' || $(this).val() == null){
				isValid =false;
			}
		}); 
		if(!isValid){
			 alert("Please add Seat High.");
			 return;
		}
		$('.qty').each(function() {
			if($(this).val() == '' || $(this).val() == null){
				isValid =false;				
			}
		});
		if(!isValid){
			alert("Please add Quantity.");
			return;
		}		
		$('.price').each(function() {
			if($(this).val() == '' || $(this).val() == null){
				isValid =false;
			}
			if(isNaN($(this).val())){
				isValid =false;
			}
		});
		if(!isValid){
			 alert("Please add Price.");
			 return;
		}
		if($('#shippingMethod').val() < 0){
			alert("Please select Shipping method.");
			return;
		}
		if(paymentType=='card'){
			if(($('#cardNo').val()!='' && $('#cardNo').val().length !=4) ){
				alert("Please enter Card last 4 digit only");
				return;
			}
		}else if(paymentType=='account'){
			if(($('#accountNo').val()!='' && $('#accountNo').val().length !=4) ){
				alert("Please enter Account No. last 4 digit only");
				return;
			}
			if(($('#routingNo').val()!='' && $('#routingNo').val().length !=4)){
				alert("Please enter Routing No. last 4 digit only");
				return;
			}
		}
		else if(paymentType=='cheque'){
			if(($('#accountChequeNo').val()!='' && $('#accountChequeNo').val().length !=4) ){
				alert("Please enter Account No. last 4 digit only");
				return;
			}
			if(($('#routingChequeNo').val()!='' && $('#routingChequeNo').val().length !=4)){
				alert("Please enter Routing No. last 4 digit only");
				return;
			}
		}
		var myTable = document.getElementById("myTable");
	    var currentIndex = myTable.rows.length;
		if(currentIndex <= 1){
			isValid = false;
		}
		if(!isValid){
			 alert("Please add atleast 1 ticket.");
			 return;
		}
		if($('#totalQty').val() <= 0){
			alert("Total quantity not less than 0.");
			return;
		}
		if($('#totalPrice').val() <= 0){
			alert("Total price not less than 0.");
			return;
		}
		$('#action').val('savePO');
		$('#purchaseOrderForm').submit();
	}
	
	//PO cancel action
	function cancelAction(){
		window.close();
	}
	$(document).ready(function() {
		$('#searchValue').keypress(function (event) {
			var keyCode = (event.keyCode ? event.keyCode : event.which);
			 if(keyCode == 13)  // the enter key code
			  {
				 getCustomerGridData(0);
				 return false;  
			  }
		});
		
		$('#eventSearchValue').keypress(function (event) {
			var keyCode = (event.keyCode ? event.keyCode : event.which);
			 if(keyCode == 13)  // the enter key code
			  {
				 getEventGridData(0);
				 return false;  
			  }
		});
	});
	
	function addNewCustomer(){
		popupCenter("${pageContext.request.contextPath}/Client/AddCustomerPopup","Add Customer","1000","1000");		
	}
	
	function resetFilters(){
		customerGridSearchString='';
		columnFilters = {};
		getCustomerGridData(0);
	}
	
	function eventResetFilters(){
		eventGridSearchString='';
		eventGridcolumnFilters = {};
		getEventGridData(0);
	}
	
	function searchEventData(){
		eventGridSearchString='';
		eventGridcolumnFilters = {};
		getEventGridData(0);
	}
</script>


<div class="full-width">
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i> Purchase Order
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a  style="font-size: 13px;font-family: arial;" href="#">Purchase Order</a>
			</li>
			<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Create PO</li>
		</ol>
	</div>
</div>

<div class="full-width">
<div class="col-lg-12">
<h3 style="font-size: 13px;font-family: arial;" class="page-header">
			Search Customer
		</h3>		<c:if test="${successMessage != null}">
			<div class="alert alert-success fade in">
				<strong
					style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${successMessage}</strong>
			</div>
		</c:if>
		<c:if test="${errorMessage != null}">
			<div class="alert alert-block alert-danger fade in">
				<strong 
					style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${errorMessage}</strong>
			</div>
		</c:if>
		<!--  search form start -->
		<form:form class="form-inline" role="form" onsubmit="return false" id="customerSearch" method="post" >
			<!-- 
			<div class="form-group col-xs-2 col-md-2">
			<label for="name" class="control-label">Search By</label> 
				<select id="searchType" name="searchBy" class="form-control input-sm m-bot15">
				 <option value="customerName" <c:if test="${searchType eq 'customerName'}">selected</c:if> >Customer Name</option>
				  <option value="email">Email</option>
				  <option value="customerType" <c:if test="${searchType eq 'customerType'}">selected</c:if> >CustomerType</option>
				</select>
			</div>
			
			<div class="form-group col-xs-2 col-md-2">
				<label for="name" class="control-label">Search Value</label>
				<input class="form-control" placeholder="Search" type="text" id="searchValue" name="searchValue" value="${searchValue}">
			</div>
			
			<div class="form-group col-xs-2 col-md-2">
				<label for="name" class="control-label">Product Type</label> <select id="productType" name="productType" class="form-control ">
					<option <c:if test="${productType=='ALL'}"> Selected </c:if> value="ALL">All</option>
					<option <c:if test="${productType=='REWARDTHEFAN'}"> Selected </c:if> value="REWARDTHEFAN">Reward The Fan</option>
					<option <c:if test="${productType=='RTW'}"> Selected </c:if> value="RTW">RTW</option>
					<option <c:if test="${productType=='RTW2'}"> Selected </c:if> value="RTW2">RTW2</option>
				</select>
			</div>
			
			<div class="form-group col-xs-2 col-md-2">
				<label>Client/Broker</label>
				<select name="customerTypeSearchValues"
					class="form-control input-sm m-bot15" id="customerTypeSearch">
					<option value="both" <c:if test="${customerType=='both'}"> Selected </c:if>>Both</option>
					<option value="client" <c:if test="${customerType=='client'}"> Selected </c:if>>Client</option>
					<option value="broker" <c:if test="${customerType=='broker'}"> Selected </c:if>>Broker</option>
				</select>
			</div>
            
			<div class="form-group col-xs-2 col-md-2">
			 	<button type="button" id="searchCustomerBtn" class="btn btn-primary" style="margin-top:18px;"  onclick="getCustomerGridData(0);">Search</button>
			 </div>
			 -->
			 <div class="form-group">
			 	<button type="button" class="btn btn-primary" style="float:right; margin-top:18px;"  onclick="addNewCustomer();">Add New Customer</button>
			 </div>
			 
              </form:form>
          </div>
</div>
<br/>
<div class="full-width" id="customerGridDiv">
	<div class="table-responsive grid-table">
		<div class="grid-header full-width">
			<label style="font-size: 10px;">Customers Details</label>
			<div class="pull-right">
			<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters</a>
			</div>
		</div>
		<div id="customer_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
		<div id="customer_pager" style="width: 100%; height: 20px;"></div>
	</div>
</div>

<br />
<br />
<div class="full-width">
	<div class="full-width">
		<section class="panel full-width"> <header style="font-size: 13px;font-family: arial;" class="panel-heading">
		Customer Details </header>
		<div class="panel-body full-width">
			<div class="form-group">
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Customer Name:</label>
					<span style="font-size: 12px;" id="customerNameDiv"></span>
					
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Email:</label>
					<span id=emailDiv></span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Phone:</label><span id=phoneDiv></span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Address Line:</label><span id=addressLineDvi></span>
                </div>
			</div>
			
			<div class="form-group">
			<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">City:</label><span id=cityDiv></span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">State:</label><span id=stateDiv></span>
				</div>
				<div class="col-sm-3"> 
					<label style="font-size: 13px;font-weight:bold;">Country:</label><span id=countryDiv></span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">ZipCode:</label><span id=zipCodeDiv></span>
				</div>
			</div>
			
		</div>
		</section>
	</div>
</div>
<br><br>

<div id="eventInfo" style="display:none">
<div class="full-width col-xs-12">
	<header style="font-size: 13px;font-family: arial;" class="panel-heading">Select an event to add ticket </header>
	<div class="row">
		<form:form class="form-inline" onsubmit="return false" role="form" id="eventSearch" method="post" action="${pageContext.request.contextPath}/Events">
			<input type="hidden" id="eventID" name="eventID">
			<%-- <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-4">
				<label for="name" class="control-label">From Date </label> 
				<input class="form-control searchcontrol" type="text" id="fromDate" name="fromDate" value="${fromDate}">
			</div>
			<div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-4">
				<label for="name" class="control-label">To Date&nbsp;&nbsp;&nbsp;&nbsp; </label> 
				<input class="form-control searchcontrol" type="text" id="toDate" name="toDate" value="${toDate}">
			</div> --%>
			<div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-4"> 
				<select id="searchType" name="searchType" class="form-control" style="margin-top:18px;">
					<option value="event_name">Event</option>
					<option value="artist_name" <c:if test="${searchType eq 'artist_name'}">selected</c:if> >Artist</option>
					<!-- <option value="grand_child_category_name" <c:if test="${searchType eq 'grand_child_category_name'}">selected</c:if> >GrandChildCategory</option>
					<option value="building" <c:if test="${searchType eq 'building'}">selected</c:if> >Venue</option>
					<option value="city" <c:if test="${searchType eq 'city'}">selected</c:if> >City</option>
					<option value="state" <c:if test="${searchType eq 'state'}">selected</c:if> >State</option> -->
				</select>
			</div>
			<div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-4">
				
					<input class="form-control searchcontrol" placeholder="Search" type="text" style="margin-top:18px;" id="eventSearchValue" name="eventSearchValue" value="${eventSearchValue}">
				
			</div>            
			<div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-4">
				<button type="button" id="searchEventBtn" class="btn btn-primary" style="margin-top:18px;" onclick="searchEventData();">Search</button>
			</div>
			<div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-4">
				&nbsp;
			</div>			
            </form:form>
               <!--  search form end -->                
    </div>
</div>
<br/>
<!-- event Grid Code Starts -->
<div class="full-width mt-20 mb-20" style="position: relative; display: none;" id="eventGridDiv">
	<div class="table-responsive grid-table">
		<div class="grid-header full-width">
			<label>Events Details</label>
			<div class="pull-right">
			<a href="javascript:eventResetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters</a>
			</div>
		</div>
		<div id="event_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
		<div id="event_pager" style="width: 100%; height: 20px;"></div>
	</div>
</div>

<!-- Show the selected event info -->
<br/><br/><br/>
<div class="form-group" id="addTicketDiv" style="display: none">
	<div class="col-sm-6">
		<button class="btn btn-primary" type="submit" id="addTicket" onclick="">Add Ticket</button>
	</div>
</div>
<br/><br/><br/>
</div>
<div id="poInfo" class="full-width">
<div class="col-xs-12">		
		 <section class="panel">
			<header style="font-size: 13px;font-family: arial;" class="panel-heading">Ticket Details</header>
			<div class="panel-body">
<div class="form-group full-width">
	<form name="purchaseOrderForm" id="purchaseOrderForm" action="${pageContext.request.contextPath}/PurchaseOrder/SavePO" method="post">
	<input type="hidden" name="action" value="action" id="action"/>
	<input type="hidden" name="poType" id="poType">
	<input type="hidden" name="customerId" value="" id="customerId"/>
		<div class="col-xs-12">
			<table id="myTable"
				class="table table-striped table-advance table-hover">
				<tbody>
					<tr >
						<th></th>
						<th style="font-size: 13px;font-family: arial;"><i class=""></i> Event Name</th>
						<!-- <th><i class=""></i> Event Date</th>
					<th><i class=""></i> Venue</th> -->
						<th style="font-size: 13px;font-family: arial;"><i class=""></i> Section</th>
						<th style="font-size: 13px;font-family: arial;"><i class=""></i> Row</th>
						<th style="font-size: 13px;font-family: arial;"><i class=""></i> Seat Low</th>
						<th style="font-size: 13px;font-family: arial;"><i class=""></i> Seat High</th>
						<th style="font-size: 13px;font-family: arial;"><i class=""></i> Qty</th>
						<th style="font-size: 13px;font-family: arial;"><i class=""></i> Price</th>
						<th style="font-size: 13px;font-family: arial;"><i class=""></i> Action</th>
					</tr> 
					<!-- dynamic rows -->
					<!--<h4 id="totalQty" style="float:right;">Total qty</h4>-->
				</tbody>
			</table>
		</div>
</div>

<div class="full-width">
	<div class="col-lg-12">
		 <div class="col-lg-3 col-sm-3 ">
			<label >Shipping Method</label>
			<select id="shippingMethod" name="shippingMethod" class="form-control input-sm m-bot15">
			  <option value="-1">--select--</option>
			  <c:forEach items="${shippingMethods}" var="shippingMethod">
				<option value="${shippingMethod.id}"> ${shippingMethod.name}
				</option>
			</c:forEach>
			</select>
		 </div>
		 <div class="col-lg-3 col-sm-3" id="trackingInfo">
			<label >Tracking No </label>
			<input type="text" name="trackingNo" id="trackingNo" class="form-control"/>
		 </div>
		 <div class="col-lg-3 col-sm-3"  style="float:right">
			Total Price : $<input type="text" id="totalPrice" name="totalPrice" readOnly value="" style="border: none;">
		 </div>
		 <div class="col-lg-3 col-sm-3"  style="float:right">
			Total Qty :
			<input type="text" id="totalQty" name="totalQty" readOnly value="" style="border: none;">
		 </div>
		 
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="col-lg-3 col-sm-3 ">
			<label>External PO No :</label>
			<input type="text" name="externalPONo" value=""  class="form-control"/>
		 </div>
		 <div class="col-lg-3 col-sm-3 ">
			<label>Consignment PO :</label>
			 <input type="checkbox" id="poTypeCheck" >
		 </div>
		
		 <div class="col-lg-3 col-sm-3" id="consignmentPoDiv">
			<label>Consignment PO No :</label>
			<input type="text" name="consignmentPo" id="consignmentPo" value=""  class="form-control"/>
		 </div>
		 <div class="col-lg-3 col-sm-3 ">
			<label>Transaction Office :</label>
			<select name="transactionOffice" class="form-control">
		  		<option value="Main">Main</option>
		  		<option value="Spec">Spec</option>
		  	</select> 
		 </div>
		</div>
	</div>
	</div>
</section>
</div>
<div class="row">
	<div class="col-lg-12">		
		 <section class="panel full-width">
			<header style="font-size: 13px;font-family: arial;" class="panel-heading">Payment Details</header>
			<div class="panel-body">
				<div class="row">
				<div class="col-lg-12">
				<div class="form-group">
					<div class="col-sm-4">
						  <input type="radio" name="paymentType" value="card">Credit Card
					</div>
					<div class="col-sm-4">
						  <input type="radio" name="paymentType" value="account">In Account
					</div>
					<div class="col-sm-4">
						  <input type="radio" name="paymentType" value="cheque">Cheque
					</div>
					<hr/>
				</div>
				<div class="form-group" id="cardInfo">
					<div class="col-sm-4">
						<label>Card Holder Name</label>
					  	<input type="text" name="name" id="name" class="form-control"/> 
					</div>
					<div class="col-sm-4">
						<label>Card Type</label>
					  	<select name="cardType" class="form-control">
					  		<option value="American Express">American Express</option>
							<option value="Visa">Visa</option>
							<option value="MasterCard">MasterCard</option>
							<option value="JCB">JCB</option>
							<option value="Discover">Discover</option>
							<option value="Diners Club">Diners Club</option>
					  	</select> 
					</div>
					<div class="col-sm-4">
						<label>Card Last 4 Digit</label>
					  	<input type="text" name="cardNo" id="cardNo" class="form-control"/> 
					</div>
					<hr/>
				</div>
				<div class="form-group" id="accountInfo">
					<div class="col-sm-4">
						<label>Account Name</label>
					  	<input type="text" name="accountName" id="accountName" class="form-control"/> 
					</div>
					<div class="col-sm-4">
						<label>Routing no. Last 4 Digit</label>
					  	<input type="text" name="routingNo" id="routingNo" class="form-control"/>
					</div>
					<div class="col-sm-4">
						<label>Account no. Last 4 Digit</label>
					  	<input type="text" name="accountNo" id="accountNo" class="form-control"/> 
					</div>
					<hr/>
				</div>
				<div class="form-group" id="chequeInfo">
					<div class="col-sm-3">
						<label>Name</label>
					  	<input type="text" name="chequeName" id="chequeName" class="form-control"/> 
					</div>
					<div class="col-sm-3">
						<label>Routing no. Last 4 Digit</label>
					  	<input type="text" name="routingChequeNo" id="routingChequeNo" class="form-control"/>
					</div>
					<div class="col-sm-3">
						<label>Account no. Last 4 Digit</label>
					  	<input type="text" name="accountChequeNo" id="accountChequeNo" class="form-control"/>
					</div>
					<div class="col-sm-3">
						<label>Cheque No.</label>
					  	<input type="text" name="checkNo" id="checkNo" class="form-control"/> 
					</div>
					<hr/>
				</div>
				</div>
				</div>
				<div class="row">
				<div class="col-lg-12">
				<div class="form-group">
					<div class="col-sm-4">
						<label>Payment Date</label>
					  	<input type="text" name="paymentDate" id="paymentDate" class="form-control"/> 
					</div>
					<div class="col-sm-4">
						<label>payment Status</label>
					  	<select name="paymentStatus" class="form-control">
					  		<option value="Paid">Paid</option>
					  		<option value="Pending">Pending</option>
					  	</select> 
					</div>
					<div class="col-sm-4">
						<label>Payment Note</label>
					  	<textarea name="paymentNote" id="paymentNote" rows="4" cols="50" class="form-control"> </textarea>
					</div>
					<div class="col-sm-3"></div>
				</div>
				</div>
				</div>
			</div>
		</section>
	</div>

</div>

<div class="form-group" style="margin-top:10px;">
	<div id="POAction" class="col-sm-12" align="center">
		<button class="btn btn-primary" type="button" onclick="savePurchaseOrder();">Create PO</button>
		<button class="btn btn-default" onclick="cancelAction();"
			type="button">Cancel</button>
	</div>
</div>
</form>
</div>




<script>
	var eventPagingInfo;
	var eventDataView;
	var eventGrid;
	var eventData = [];
	var eventGridSearchString='';
	var eventGridcolumnFilters = {};
	var eventColumns = [ {
		id : "eventName",
		name : "Event Name",
		field : "eventName",
		sortable : true
	}, {
		id : "eventDate",
		name : "Event Date",
		field : "eventDate",
		sortable : true
	}, {
		id : "eventTime",
		name : "Event Time",
		field : "eventTime",
		sortable : true
	}, {
		id : "venue",
		name : "Venue",
		field : "venue",
		sortable : true
	}, {
		id : "city",
		name : "City",
		field : "city",
		sortable : true
	}, {
		id : "state",
		name : "State",
		field : "state",
		sortable : true
	},  {id: "country", 
		name: "Country", 
		field: "country", 
		sortable: true
	}, {id: "grandChildCategory", 
		name: "Grand Child Category", 
		field: "grandChildCategory", 
		sortable: true
	},{id: "childCategory", 
		name: "Child Category", 
		field: "childCategory", 
		sortable: true
	}, {
		id : "parentCategory",
		field : "parentCategory",
		name : "Parent Category",
		sortable: true
	} ];

	var eventOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var eventGridSortcol = "eventName";
	var eventGridSortdir = 1;
	var percentCompleteThreshold = 0;
	
	function deleteRecordFromEventGrid(id) {
		eventDataView.deleteItem(id);
		eventGrid.invalidate();
	}
	/*
	function eventGridFilter(item, args) {
		var x= item["eventName"];
		if (args.eventGridSearchString  != ""
				&& x.indexOf(args.eventGridSearchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.eventGridSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}
	*/
	function eventGridComparer(a, b) {
		var x = a[eventGridSortcol], y = b[eventGridSortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	/*
	function eventGridToggleFilterRow() {
		eventGrid.setTopPanelVisibility(!eventGrid.getOptions().showTopPanel);
	}
	
	//$(".grid-header .ui-icon-sg").addClass("ui-state-default-sg ui-corner-all")
	$("#event_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all")
			.mouseover(function(e) {
				$(e.target).addClass("ui-state-hover");
			}).mouseout(function(e) {
				$(e.target).removeClass("ui-state-hover");
			});
	*/
	function pagingControl(move,id){
		if(id=='customer_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(pagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(pagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(pagingInfo.pageNum)-1;
			}
			getCustomerGridData(pageNo);
		}else if(id=='event_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(eventPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(eventPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(eventPagingInfo.pageNum)-1;
			}
			getEventGridData(pageNo);
		}
		
	}
	
	function getCustomerGridData(pageNo) {
		var searchValue = $("#searchValue").val();
		$.ajax({
			url : "${pageContext.request.contextPath}/Client/GetCustomers",
			type : "post",
			data : $("#customerSearch").serialize()+"&pageNo="+pageNo+"&headerFilter="+customerGridSearchString,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				pagingInfo = jsonData.pagingInfo;
				refreshcustomerGridValues(jsonData.customers);
			}, error : function(error){
				jAlert("There is something wrong. Please try again"+error,"Error");
				return false;
			}
		});
	}
	function getEventGridData(pageNo) {
		$('#eventGridDiv').show();
		var searchValue = $("#eventSearchValue").val();
		$.ajax({			  
			url : "${pageContext.request.contextPath}/GetEventDetails",
			type : "post",
			data : $("#eventSearch").serialize()+"&pageNo="+pageNo+"&headerFilter="+eventGridSearchString,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				eventPagingInfo = jsonData.eventPagingInfo;
				refreshEventGridValues(jsonData.events);
			}, error : function(error){
				jAlert("There is something wrong. Please try again"+error,"Error");
				return false;
			}
		});
	}
	function refreshEventGridValues(jsonData) {
		eventData = [];
		for (var i = 0; i < jsonData.length; i++) {
			var  data= jsonData[i]; 
			var d = (eventData[i] = {});
			d["id"] = i;
			d["eventID"] = data.eventId;
			d["eventName"] = data.eventName;
			d["eventDate"] = data.eventDateStr;
			d["eventTime"] = data.eventTimeStr;
			d["venue"] = data.building;
			d["city"] = data.city;
			d["state"] = data.state;
			d["country"] = data.country;
			d["grandChildCategory"] = data.grandChildCategoryName;
			d["childCategory"] = data.childCategoryName;
			d["parentCategory"] = data.parentCategoryName;
		}

		eventDataView = new Slick.Data.DataView();
		eventGrid = new Slick.Grid("#event_grid", eventDataView, eventColumns, eventOptions);
		eventGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		eventGrid.setSelectionModel(new Slick.RowSelectionModel());
		if(eventPagingInfo!=null){
			var eventGridPager = new Slick.Controls.Pager(eventDataView, eventGrid, $("#event_pager"),eventPagingInfo);
		}
		
		var eventGridColumnpicker = new Slick.Controls.ColumnPicker(eventColumns, eventGrid,
				eventOptions);

		// move the filter panel defined in a hidden div into eventGrid top panel
		//$("#event_inlineFilterPanel").appendTo(eventGrid.getTopPanel()).show();

		/*eventGrid.onKeyDown.subscribe(function(e) {
			// select all rows on ctrl-a
			if (e.which != 65 || !e.ctrlKey) {
				return false;
			}
			var rows = [];
			for ( var i = 0; i < eventDataView.getLength(); i++) {
				rows.push(i);
			}
			eventGrid.setSelectedRows(rows);
			e.preventDefault();
		});*/
		
		eventGrid.onSort.subscribe(function(e, args) {
			eventGridSortdir = args.sortAsc ? 1 : -1;
			eventGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				eventDataView.fastSort(eventGridSortcol, args.sortAsc);
			} else {
				eventDataView.sort(eventGridComparer, args.sortAsc);
			}
		});
		// wire up model events to drive the eventGrid
		eventDataView.onRowCountChanged.subscribe(function(e, args) {
			eventGrid.updateRowCount();
			eventGrid.render();
		});
		eventDataView.onRowsChanged.subscribe(function(e, args) {
			eventGrid.invalidateRows(args.rows);
			eventGrid.render();
		});
		$(eventGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
			eventGridSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				  eventGridcolumnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in eventGridcolumnFilters) {
					  if (columnId !== undefined && eventGridcolumnFilters[columnId] !== "") {
						  eventGridSearchString += columnId + ":" +eventGridcolumnFilters[columnId]+",";
					  }
					}
					getEventGridData(0);
				}
			  }
		 
		});
		eventGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id == 'eventTime'){
					$("<input type='text' placeholder='hh:mm a'>")
				   .data("columnId", args.column.id)
				   .val(eventGridcolumnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else if(args.column.id == 'eventDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(eventGridcolumnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else{
					$("<input type='text'>")
				   .data("columnId", args.column.id)
				   .val(eventGridcolumnFilters[args.column.id])
				   .appendTo(args.node);
				}
			}				
		});
		eventGrid.init();
			
		/*
		// wire up the search textbox to apply the filter to the model
		$("#eventGridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			eventGridSearchString = this.value;
			updateEventGridFilter();
		});
		function updateEventGridFilter() {
			eventDataView.setFilterArgs({
				eventGridSearchString : eventGridSearchString
			});
			eventDataView.refresh();
		}
		*/
		// initialize the model after all the events have been hooked up
		eventDataView.beginUpdate();
		eventDataView.setItems(eventData);
		/*eventDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			eventGridSearchString : eventGridSearchString
		});
		eventDataView.setFilter(eventGridFilter);*/
		eventDataView.endUpdate();
		eventDataView.syncGridSelection(eventGrid, true);
		$("#gridContainer").resizable();
		$("div#divLoading").removeClass('show');
		
		/* var eventrowIndex;
		eventGrid.onSelectedRowsChanged.subscribe(function() { 
			var temprEventRowIndex = eventGrid.getSelectedRows([0])[0];
			if (temprEventRowIndex != eventrowIndex) {
				eventrowIndex = temprEventRowIndex;
				 var eventId =eventGrid.getDataItem(temprEventRowIndex).id;
				//jAlert(eventId);
				//getCategoryTicketGroupsforEvent(eventId);
			}
		}); */
		
		eventGrid.onSelectedRowsChanged.subscribe(function() { 
			$('#addTicketDiv').show();
		});
	}
	
	
	var pagingInfo;
	var customerGrid;
	var customerDataView;
	var customerData=[];
	var customerGridSearchString='';
	var columnFilters = {};
	var customerColumns = [
	               {id:"customerType", name:"Customer Type", field: "customerType", sortable: true, width: 50},
	               {id:"firstName", name:"First Name", field: "firstName", sortable: true, width: 100},
	               {id:"lastName", name:"Last Name", field: "lastName", sortable: true, width: 100},
	               {id:"email", name:"Email", field: "email", sortable: true, width: 140},
	               {id:"productType", name:"Product Type", field:"productType", sortable: true, width: 120},
	               {id:"client", name:"Client", field: "client", sortable: true},
	               {id:"broker", name:"Broker", field: "broker", sortable: true},
	               {id:"street1", name:"Street1", field: "street1", sortable: true},
	               {id:"street2", name:"Street2", field: "street2", sortable: true},
	               {id:"city", name:"City", field: "city", sortable: true},
	               {id:"state", name:"State", field: "state", sortable: true},
				   {id:"country", name:"Country", field: "country", sortable: true},
	               {id:"zip", name:"Zip", field: "zip", sortable: true},
	               {id:"phone", name:"Phone", field: "phone", width:100, sortable: true},
	               {id: "editCol", field:"editCol", name:"Edit Customer", width:10, formatter:editFormatter}
	              ];
	              
	var customerOptions = {
			enableCellNavigation : true,
			forceFitColumns : true,
			multiSelect: false,
			topPanelHeight : 25,
			showHeaderRow: true,
			headerRowHeight: 30,
			explicitInitialization: true
		};
		var customerGridSortcol = "customerName";
		var customerGridSortdir = 1;
		var percentCompleteThreshold = 0;

		/*	
		function customerGridFilter(item, args) {
			var x= item["customerName"];
			if (args.customerGridSearchString  != ""
					&& x.indexOf(args.customerGridSearchString) == -1) {
				
				if (typeof x === 'string' || x instanceof String) {
					if(x.toLowerCase().indexOf(args.customerGridSearchString.toLowerCase()) == -1) {
						return false;
					}
				} else {
					return false;
				}
			}
			return true;
		}
		*/
		function editFormatter(row,cell,value,columnDef,dataContext){  
		    var button = "<img class='editClickableImage' src='../resources/images/ico-edit.gif' id='"+ dataContext.customerId +"'/>";
		    return button;
		}
		
		$('.editClickableImage').live('click', function(){
		    var me = $(this), id = me.attr('id');
		    var delFlag = editCustomer(id);
		});

		function customerGridComparer(a, b) {
			var x = a[customerGridSortcol], y = b[customerGridSortcol];
			if(!isNaN(x)){
			   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
			}
			if(x == '' || x == null) {
				return 1;
			} else if(y == '' || y == null) {
				return -1;
			}
			if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
				return x.toLowerCase().localeCompare(y.toLowerCase());	
			} else {
				return (x == y ? 0 : (x > y ? 1 : -1));	
			}
		}
		/*
		function customerGridToggleFilterRow() {
			customerGrid.setTopPanelVisibility(!customerGrid.getOptions().showTopPanel);
		}
		
		//$(".grid-header .ui-icon").addClass("ui-state-default-sg ui-corner-all")
		$("#customer_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all")
				.mouseover(function(e) {
					$(e.target).addClass("ui-state-hover")
				}).mouseout(function(e) {
					$(e.target).removeClass("ui-state-hover")
				});
		*/
		
		function refreshcustomerGridValues(jsonData) {
			 $("div#divLoading").addClass('show');
			customerData = [];
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i]; 
				var d = (customerData[i] = {});
				d["id"] = i;
				d["customerId"] = data.customerId;
				d["bAddressId"] = data.billingAddressId;
				d["firstName"] = data.customerName;
				d["lastName"] = data.lastName;
				d["customerType"] = data.customerType;
				d["client"] = data.client;
				d["broker"] = data.broker;
				d["email"] = data.customerEmail;
				d["productType"] = data.productType;
				d["street1"] = data.addressLine1;	
				d["street2"] = data.addressLine2;
				d["city"] = data.city;
				d["state"] = data.state;
				d["country"] = data.country;
				d["zip"] = data.zipCode;
				d["phone"] = data.phone;
			}

			customerDataView = new Slick.Data.DataView();
			customerGrid = new Slick.Grid("#customer_grid", customerDataView, customerColumns, customerOptions);
			customerGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
			customerGrid.setSelectionModel(new Slick.RowSelectionModel());
			if(pagingInfo!=null){
				var customerGridPager = new Slick.Controls.Pager(customerDataView, customerGrid, $("#customer_pager"),pagingInfo);
			}
			
			var customerGridColumnpicker = new Slick.Controls.ColumnPicker(customerColumns, customerGrid,
					customerOptions);

			// move the filter panel defined in a hidden div into customerGrid top panel
			//$("#customer_inlineFilterPanel").appendTo(customerGrid.getTopPanel()).show();

			customerGrid.onSort.subscribe(function(e, args) {
				customerGridSortdir = args.sortAsc ? 1 : -1;
				customerGridSortcol = args.sortCol.field;
				if ($.browser.msie && $.browser.version <= 8) {
					customerDataView.fastSort(customerGridSortcol, args.sortAsc);
				} else {
					customerDataView.sort(customerGridComparer, args.sortAsc);
				}
			});
			// wire up model customers to drive the customerGrid
			customerDataView.onRowCountChanged.subscribe(function(e, args) {
				customerGrid.updateRowCount();
				customerGrid.render();
			});
			customerDataView.onRowsChanged.subscribe(function(e, args) {
				customerGrid.invalidateRows(args.rows);
				customerGrid.render();
			});
			$(customerGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
			 var keyCode = (e.keyCode ? e.keyCode : e.which);
				customerGridSearchString='';
				 var columnId = $(this).data("columnId");
				  if (columnId != null) {
					columnFilters[columnId] = $.trim($(this).val());
					if(keyCode == 13) {
						for (var columnId in columnFilters) {
						  if (columnId !== undefined && columnFilters[columnId] !== "") {
							  customerGridSearchString += columnId + ":" +columnFilters[columnId]+",";
						  }
						}
						getCustomerGridData(0);
					}
				  }
			 
			});
			customerGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
				$(args.node).empty();
				if(args.column.id.indexOf('checkbox') == -1){
					if(args.column.id != 'editCol'){
						$("<input type='text'>")
					   .data("columnId", args.column.id)
					   .val(columnFilters[args.column.id])
					   .appendTo(args.node);
					}
				}
			});
			customerGrid.init();
				
			/*
			// wire up the search textbox to apply the filter to the model
			$("#customerGridSearch").keyup(function(e) {
				Slick.GlobalEditorLock.cancelCurrentEdit();
				// clear on Esc
				if (e.which == 27) {
					this.value = "";
				}
				customerGridSearchString = this.value;
				updatecustomerGridFilter();
			});
			function updatecustomerGridFilter() {
				customerDataView.setFilterArgs({
					customerGridSearchString : customerGridSearchString
				});
				customerDataView.refresh();
			}
			*/
			// initialize the model after all the customers have been hooked up
			customerDataView.beginUpdate();
			customerDataView.setItems(customerData);
			/*customerDataView.setFilterArgs({
				percentCompleteThreshold : percentCompleteThreshold,
				customerGridSearchString : customerGridSearchString
			});
			customerDataView.setFilter(customerGridFilter);*/
			customerDataView.endUpdate();
			customerDataView.syncGridSelection(customerGrid, true);
			$("#gridContainer").resizable();
			
			var customerRowIndex;
			customerGrid.onSelectedRowsChanged.subscribe(function() { 
				var tempcustomerRowIndex = customerGrid.getSelectedRows([0])[0];
				if (tempcustomerRowIndex != customerRowIndex) {
					customerRowIndex = tempcustomerRowIndex;
					resetCustomerInfo();
					setSelectedCustomerInfo(customerRowIndex);
					
				}
			});
			 $("div#divLoading").removeClass('show');
		}
		
		function editCustomer(custId){
			if(custId == null){
				jAlert("There is something wrong.Please try again.");
			}else{
				var url = "${pageContext.request.contextPath}/Client/ViewCustomer?custId="+ custId;
				popupCenter(url, "Edit Customer Details", "1000", "800");
			}
		}
		
		
		function resetCustomerInfo(){
			$("#customerNameDiv").empty();
			$("#emailDiv").empty();
			$("#phoneDiv").empty();
			$("#addressLineDvi").empty();
			$("#countryDiv").empty();
			$("#stateDiv").empty();
			$("#cityDiv").empty();
			$("#zipCodeDiv").empty();
			
			$('#shAddressLine1Div').empty();
			$('#shAddressLine2Div').empty();
			$('#shPhoneDiv').empty();
			$('#shCountryDiv').empty();
			$('#shStateDiv').empty();
			$('#shCityDiv').empty();
			$('#shZipCodeDiv').empty();
		}
		
		
		//Get selected event on button click
		function setSelectedCustomerInfo(customerGridIndex) {
			var address=customerGrid.getDataItem(customerGridIndex).street1;
			var strret2 = customerGrid.getDataItem(customerGridIndex).street2;
			if(address==undefined){
				address='';
			}
			if(strret2==undefined){
				strret2='';
			}
			$("#eventInfo").show();
			$("#customerId").val(customerGrid.getDataItem(customerGridIndex).customerId);
				$("#customerNameDiv").text(customerGrid.getDataItem(customerGridIndex).customerName);
				$("#emailDiv").text(customerGrid.getDataItem(customerGridIndex).email);
				$("#phoneDiv").text(customerGrid.getDataItem(customerGridIndex).phone);
				if(strret2!=null && strret2 != '') {
					address = address +','+strret2;
				}
				$("#addressLineDvi").text(address);
				$("#countryDiv").text(customerGrid.getDataItem(customerGridIndex).country);
				$("#stateDiv").text(customerGrid.getDataItem(customerGridIndex).state);
				$("#cityDiv").text(customerGrid.getDataItem(customerGridIndex).city);
				$("#zipCodeDiv").text(customerGrid.getDataItem(customerGridIndex).zip);
			}
	
	
	//Get selected event on button click
	function getEventInfoForPO(){		
			var temprEventRowIndex = eventGrid.getSelectedRows([0])[0];
			if(temprEventRowIndex == null){
				jAlert("Please select event");
			}{
			var eventId = eventGrid.getDataItem(temprEventRowIndex).eventID;
			var eventName = eventGrid.getDataItem(temprEventRowIndex).eventName;
			var eventDate = eventGrid.getDataItem(temprEventRowIndex).eventDate;
			var venue = eventGrid.getDataItem(temprEventRowIndex).eventDate;
			var eventTime = eventGrid.getDataItem(temprEventRowIndex).eventTime;
			//jAlert(eventId+","+eventName+","+eventDate+","+venue);	
			//$('#eventId').val(eventId);
			//$('#eventName').text(eventName);
			//$('#eventDate').text(eventDate);
			//$('#venue').text(venue);

			//Add dynamic rows to table for selected event
			var myTable = document.getElementById("myTable");
	        var currentIndex = myTable.rows.length;
	        var currentRow = myTable.insertRow(-1);

	        var input1 = document.createElement("input");
			input1.setAttribute("type", "hidden");
			input1.setAttribute("name", "rowId_" + currentIndex);
			input1.setAttribute("value", currentIndex);
			input1.setAttribute("class","rowId");
			//input1.setAttribute("id","display");

			var input = document.createElement("input");
			input.setAttribute("type", "hidden");
			input.setAttribute("name", "eventId_" +currentIndex+"_"+ eventId);
			input.setAttribute("value", eventId);
			input.setAttribute("id","display");

			var eventNameLabel = document.createElement("Label");
			eventNameLabel.setAttribute("for","event_"+eventId);
			eventNameLabel.innerHTML = eventName+" "+eventDate+" "+eventTime;
			
	    	/* var eventDateLabel = document.createElement("Label");
	    	eventDateLabel.setAttribute("for","event_"+eventId);
	    	eventDateLabel.innerHTML = eventDate;

	    	var venueLabel = document.createElement("Label");
	    	venueLabel.setAttribute("for","event_"+eventId);
	    	venueLabel.innerHTML = venue; */
	    	
	    	var sectionBox = document.createElement("input");
	    	sectionBox.setAttribute("name", "section_" +currentIndex+"_"+ eventId);
	    	sectionBox.setAttribute("id","section_"+currentIndex+"_"+eventId);
	    	sectionBox.setAttribute("class","section");
	    	
	    	var row = document.createElement("input");
	    	row.setAttribute("name", "row_" +currentIndex+"_"+ eventId);
	    	row.setAttribute("id","row_"+currentIndex+"_"+eventId);
	    	row.setAttribute("class","rowValue");
	    	
	    	var seatLow = document.createElement("input");
	    	seatLow.setAttribute("name", "seatLow_" +currentIndex+"_"+ eventId);
	    	seatLow.setAttribute("id","seatLow_"+currentIndex+"_"+eventId);
	    	seatLow.setAttribute("class","seatLow");
	    	
	    	var seatHigh = document.createElement("input");
	    	seatHigh.setAttribute("name", "seatHigh_" +currentIndex+"_"+ eventId);
	    	seatHigh.setAttribute("id","seatHigh_"+currentIndex+"_"+eventId);
	    	seatHigh.setAttribute("class","seatHigh");
	    	
	    	var qty = document.createElement("input");
	    	qty.setAttribute("type", "number");
	    	qty.setAttribute("name", "qty_" +currentIndex+"_"+ eventId);
	    	qty.setAttribute("id","qty_"+currentIndex+"_"+eventId);
			qty.setAttribute("class","qty");
			qty.setAttribute("onkeyup","sumQty(this);");
	    	
	    	var price = document.createElement("input");
	    	//price.setAttribute("type", "number");
	    	price.setAttribute("name", "price_" +currentIndex+"_"+ eventId);
	    	price.setAttribute("id","price_"+currentIndex+"_"+eventId);
			price.setAttribute("class","price");
			price.setAttribute("onkeyup","sumPrice(this);");
	    	
	        var addRowBox = document.createElement("input");
	        addRowBox.setAttribute("type", "button");
	        addRowBox.setAttribute("value", "Remove");
	        addRowBox.setAttribute("onclick", "removeField(this); sumQty(this); sumPrice(this);");
	        addRowBox.setAttribute("class", "button");
	
	        //currentCell = currentRow.insertCell(-1);
			//currentCell.appendChild(input1);
			
			currentCell = currentRow.insertCell(-1);
			currentCell.appendChild(input1);
			currentCell.appendChild(input);

	        var currentCell = currentRow.insertCell(-1);
			currentCell.appendChild(eventNameLabel);

			/* currentCell = currentRow.insertCell(-1);
	        currentCell.appendChild(eventDateLabel);

			currentCell = currentRow.insertCell(-1);
			currentCell.appendChild(venueLabel); */

			currentCell = currentRow.insertCell(-1);
			currentCell.appendChild(sectionBox);
			
			currentCell = currentRow.insertCell(-1);
			currentCell.appendChild(row);
			
			currentCell = currentRow.insertCell(-1);
			currentCell.appendChild(seatLow);
			
			currentCell = currentRow.insertCell(-1);
			currentCell.appendChild(seatHigh);
			
			currentCell = currentRow.insertCell(-1);
			currentCell.appendChild(qty);
			
			currentCell = currentRow.insertCell(-1);
			currentCell.appendChild(price);
	        
			currentCell = currentRow.insertCell(-1);
	        currentCell.appendChild(addRowBox);
			
			$(window).scrollTop(1100);
			}
			
			
			/*
			var footer = myTable.createTFoot();
			var row = footer.insertRow();
			var cell = row.insertCell();
			cell.innerHTML = "Total Qty";*/
			
		
	}
	
	//show the pop window center
	function popupCenter(url, title, w, h) {  
	    // Fixes dual-screen position                         Most browsers      Firefox  
	    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;  
	    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;  
	              
	    width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;  
	    height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;  
	              
	    var left = ((width / 2) - (w / 2)) + dualScreenLeft;  
	    var top = ((height / 2) - (h / 2)) + dualScreenTop;  
	    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);  
	  
	    // Puts focus on the newWindow  
	    if (window.focus) {  
	        newWindow.focus();  
	    }
	} 
	

	function removeField(id){
	//jAlert('remove');
		var p=id.parentNode.parentNode;
   	 	p.parentNode.removeChild(p);
	}

</script>
