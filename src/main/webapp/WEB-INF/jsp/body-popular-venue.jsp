<!DOCTYPE HTML>
<html>
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <title>Manage Popular Venue</title>
  
  <link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>
	
	<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
	<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
	<script src="../resources/js/slick/slick.core.js"></script>
	<script src="../resources/js/slick/slick.formatters.js"></script>
	<script src="../resources/js/slick/slick.editors.js"></script>
	<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
	<script src="../resources/js/slick/slick.grid.js"></script>
	<script src="../resources/js/slick/slick.dataview.js"></script>
	<script src="../resources/js/slick/controls/slick.pager.js"></script>
	<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
	<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
	<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>
	
<style>
.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: #f5f5f5;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

img.editClickableImage{
   cursor: pointer; 
   vertical-align: middle;
}

img.deleteClickableImage{
   cursor: pointer; 
   vertical-align: middle;
}

img.auditClickableImage{
   cursor: pointer; 
   vertical-align: middle;
}

</style>
</head>
<body>

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Venue</a>
			</li>
			<li><i class="fa fa-laptop"></i>Manage Popular Venue</li>
		</ol>

	</div>
	
	<div class="col-lg-12">
                <!--  search form start -->
			<form:form class="form-inline" role="form" id="venueSearch" onsubmit="return false" method="post" action="${pageContext.request.contextPath}/RewardTheFan/PopularVenue">
			<input type="hidden" id="productId" name="productId" value="${product.id}" />
				<div class="col-sm-1"> </div>
				<!--<label class="control-label col-sm-2" >Search Type</label>-->
				<div class="col-sm-2">
					<select id="searchType" name="searchType" class="form-control input-sm m-bot15" style = "width:100%">
					  <option value="venueName">Venue</option>
					  <option value="city" <c:if test="${searchType eq 'city'}">selected</c:if> >City</option>
					  <option value="state" <c:if test="${searchType eq 'state'}">selected</c:if> >State</option>
					  <option value="country" <c:if test="${searchType eq 'country'}">selected</c:if> >Country</option>
					  
					</select>
				</div>
               <div class="col-sm-3">
				<div class="navbar-form">
					<input class="form-control" placeholder="Search" type="text" id="searchValue" name="searchValue" value="${searchValue}">
				</div>
				</div>
				</form:form>
				<div class="col-sm-3">
				 <!-- <button type="submit" class="btn btn-primary">search</button> -->
				 <button type="button" id="searchPopularVenueBtn" class="btn btn-primary" onclick="getVenueGridData();">search</button>
				 </div>
				 <div class="col-sm-1"> </div>
               
                <!--  search form end -->                
           </div>
           
</div>

<!-- venue Grid Code Starts -->
<div style="position: relative" id="venueGridDiv">
	<div style="width: 100%;">
		<div class="grid-header" style="width: 100%">
			<label>Venue Details</label> <span id="venue_grid_toogle_search" style="float: right"
				class="ui-icon ui-icon-search" title="Toggle search panel"
				onclick="venueGridToggleFilterRow()"></span>
		</div>
		<div id="venue_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
		<div id="venue_pager" style="width: 100%; height: 10px;"></div>
	</div>
</div>


<div id="venue_inlineFilterPanel" style="display:none;background:#dddddd;padding:3px;color:black;">
  Show records with VenueName including <input type="text" id="venueGridSearch">
</div>


<script>

$(document).ready(function() {
	$('#venueSearch').submit(function(){
		e.preventDefault();
	});
	
	$('#searchValue').keypress(function (event) {
		
		var keyCode = (event.keyCode ? event.keyCode : event.which);
		 if(keyCode == 13)  // the enter key code
		  {
			 getVenueGridData();
		    //$('#searchPopularArtistBtn').click();
		    //return false;  
		  }
	});
});


var venueCheckboxSelector = new Slick.CheckboxSelectColumn({
  cssClass: "slick-cell-checkboxsel"
});

	var venueDataView;
	var venueGrid;
	var venueData = [];
	var userVenueColumnsStr = '<%=session.getAttribute("venuegrid")%>';
	var userVenueColumns =[];
	var allVenueColumns = [ venueCheckboxSelector.getColumnDefinition(),
	
	{
		id : "venueId",
		name : "Venue Id",
		field : "venueId",
		width:80,
		sortable : true
	},{
		id : "venueName",
		name : "Venue Name",
		field : "venueName",
		width:80,
		sortable : true
	}, {
		id : "city",
		name : "City",
		field : "city",
		width:80,
		sortable : true
	}, {
		id : "state",
		name : "State",
		field : "state",
		width:80,
		sortable : true
	},  {id: "country", 
		name: "Country", 
		field: "country",
		width:80,		
		sortable: true
	},{id: "notes", 
		name: "Notes", 
		field: "notes", 
		width:80,
		sortable: true,
		editor:Slick.Editors.LongText
	},{id: "postalCode", 
		name: "postal Code", 
		field: "postalCode", 
		width:80,
		sortable: true
	} , {id: "catTicketCount", 
		name: "Cat Ticket Count", 
		field: "catTicketCount",
		width:80,
		sortable: true
	}];

	if(userVenueColumnsStr!='null' && userVenueColumnsStr!=''){
		columnOrder = userVenueColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<allVenueColumns.length;j++){
				if(columnWidth[0] == allVenueColumns[j].id){
					userVenueColumns[i] =  allVenueColumns[j];
					userVenueColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}
			
		}
	}else{
		userVenueColumns = allVenueColumns;
	}
	
	var venueOptions = {
		editable: true,
		//enableAddRow: true,
		enableCellNavigation : true,
		//asyncEditorLoading: true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25
	};
	var venueGridSortcol = "venueName";
	var venueGridSortdir = 1;
	var percentCompleteThreshold = 0;
	var venueGridSearchString = "";

	
	function deleteRecordFromVenueGrid(id) {
		venueDataView.deleteItem(id);
		venueGrid.invalidate();
	}
	function venueGridFilter(item, args) {
		var x= item["venueName"];
		if (args.venueGridSearchString  != ""
				&& x.indexOf(args.venueGridSearchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.venueGridSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}

	function venueGridComparer(a, b) {
		var x = a[venueGridSortcol], y = b[venueGridSortcol];
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	function venueGridToggleFilterRow() {
		venueGrid.setTopPanelVisibility(!venueGrid.getOptions().showTopPanel);
	}
	
	//$(".grid-header .ui-icon").addClass("ui-state-default-sg ui-corner-all")
	$("#venue_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all")
		.mouseover(function(e) {
			$(e.target).addClass("ui-state-hover")
		}).mouseout(function(e) {
			$(e.target).removeClass("ui-state-hover")
		});

	
	function getVenueGridData() {
		var searchValue = $("#searchValue").val();
		if(searchValue== null || searchValue == '') {
			jAlert('Enter valid key to searh venues');
			return false;
		}
		
		$.ajax({
			  
			url : "${pageContext.request.contextPath}/GetVenueDetails",
			type : "post",
			data : $("#venueSearch").serialize(),
			dataType:"json",
			/* async : false, */
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				//jAlert(jsonData);
				if(jsonData==null || jsonData=='') {
					jAlert("No Venue found.");
				}
				refreshVenueGridValues(jsonData,true);
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
function refreshVenueGridValues(jsonData,isAjaxCall) {
	 $("div#divLoading").addClass('show');
		venueData = [];
		
		if(isAjaxCall) {
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i]; 
				var d = (venueData[i] = {});
				d["id"] = data.id;
				d["venueId"] = data.id;
				d["venueName"] = data.building;
				d["city"] = data.city;
				d["state"] = data.state;
				d["country"] = data.country;
				d["notes"] = data.notes;
				d["postalCode"] = data.postalCode;
				d["catTicketCount"] = data.catTicketCount;
				
			}
		} else {
			var i=0;
			<c:forEach var="venue" items="${venueList}">
			var d = (venueData[i] = {});
			d["id"] = "${venue.id}";
			//d["id"] = "id_" + i;
			d["venueId"] = "${venue.id}";
			d["venueName"] = "${venue.building}";
			d["venue"] = "${venue.building}";
			d["city"] = "${venue.city}";
			d["state"] = "${venue.state}";
			d["country"] = "${venue.country}";
			d["notes"] = "${venue.notes}";
			d["postalCode"] = "${venue.postalCode}";
			d["catTicketCount"] = "${venue.catTicketCount}";
			
			i++;
			</c:forEach>
		}

		venueDataView = new Slick.Data.DataView({
			inlineFilters : true
		});
		venueGrid = new Slick.Grid("#venue_grid", venueDataView, userVenueColumns, venueOptions);
		venueGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		venueGrid.setSelectionModel(new Slick.RowSelectionModel());
		venueGrid.registerPlugin(venueCheckboxSelector);
		var venueGridPager = new Slick.Controls.Pager(venueDataView, venueGrid, $("#venue_pager"));
		var venueGridColumnpicker = new Slick.Controls.ColumnPicker(allVenueColumns, venueGrid,
				venueOptions);

		// move the filter panel defined in a hidden div into venueGrid top panel
		$("#venue_inlineFilterPanel").appendTo(venueGrid.getTopPanel()).show();

		/*venueGrid.onKeyDown.subscribe(function(e) {
			// select all rows on ctrl-a
			if (e.which != 65 || !e.ctrlKey) {
				return false;
			}
			var rows = [];
			for ( var i = 0; i < venueDataView.getLength(); i++) {
				rows.push(i);
			}
			venueGrid.setSelectedRows(rows);
			e.prvenueDefault();
		});*/
		
		venueGrid.onSort.subscribe(function(e, args) {
			venueGridSortdir = args.sortAsc ? 1 : -1;
			venueGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				// using temporary Object.prototype.toString override
				// more limited and does lexicographic sort only by default, but can be much faster
				// use numeric sort of % and lexicographic for everything else
				venueDataView.fastSort(venueGridSortcol, args.sortAsc);
			} else {
				// using native sort with comparer
				// preferred method but can be very slow in IE with huge datasets
				venueDataView.sort(venueGridComparer, args.sortAsc);
			}
		});

		venueGrid.onCellChange.subscribe(function (e,args) { 
				var temprEventRowIndex = venueGrid.getSelectedRows();
				var venueId;
				var eventNotes; 
				$.each(temprEventRowIndex, function (index, value) {
					venueId = venueGrid.getDataItem(value).id;
					venueNotes = venueGrid.getDataItem(value).notes;
				});
				saveNotes(venueId,venueNotes);
         });
		// wire up model venues to drive the venueGrid
		venueDataView.onRowCountChanged.subscribe(function(e, args) {
			venueGrid.updateRowCount();
			venueGrid.render();
		});
		venueDataView.onRowsChanged.subscribe(function(e, args) {
			venueGrid.invalidateRows(args.rows);
			venueGrid.render();
		});

		// wire up the search textbox to apply the filter to the model
		$("#venueGridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			venueGridSearchString = this.value;
			updateVenueGridFilter();
		});
		function updateVenueGridFilter() {
			venueDataView.setFilterArgs({
				venueGridSearchString : venueGridSearchString
			});
			venueDataView.refresh();
		}
		
		// initialize the model after all the venues have been hooked up
		venueDataView.beginUpdate();
		venueDataView.setItems(venueData);
		venueDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			venueGridSearchString : venueGridSearchString
		});
		venueDataView.setFilter(venueGridFilter);
		venueDataView.endUpdate();
		venueDataView.syncGridSelection(venueGrid, true);
		$("#gridContainer").resizable();
		venueGrid.resizeCanvas();
		 $("div#divLoading").removeClass('show');
		/* var venuerowIndex;
		venueGrid.onSelectedRowsChanged.subscribe(function() { 
			var temprVenueRowIndex = venueGrid.getSelectedRows([0])[0];
			if (temprVenueRowIndex != venuerowIndex) {
				venuerowIndex = temprVenueRowIndex;
				 var venueId =venueGrid.getDataItem(temprVenueRowIndex).id;
				//jAlert(venueId);
				//getCategoryTicketGroupsforVenue(venueId);
			}
		}); */
	}
	
	function getSelectedVenueGridId() {
		var temprVenueRowIndex = venueGrid.getSelectedRows();
		
		var venueIdStr='';
		$.each(temprVenueRowIndex, function (index, value) {
			venueIdStr += ','+venueGrid.getDataItem(value).id;
		});
		
		if(venueIdStr != null && venueIdStr!='') {
			venueIdStr = venueIdStr.substring(1, venueIdStr.length);
			 return venueIdStr;
		}
	}
	function getSelectedVenueGridRowCount() {
		var temprVenueRowIndex = venueGrid.getSelectedRows();
		
		var rowCount=0;
		$.each(temprVenueRowIndex, function (index, value) {
			rowCount++;
		});
		
		return rowCount;
	}
	
	function updatePopularVenues(venueIds,productId) {
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdatePopularVenue",
			type : "post",
			data : "action=create&productId="+productId+"&venueIds="+ venueIds,
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.length > 0){
					jAlert("Selected Venue added to popular Venues.");
				}
				refreshpopVenueGridValues(jsonData,true);
				venueGrid.setSelectedRows([]);
				clearAllSelections();
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	function addPopularVenues() {
		var venueIds = getSelectedVenueGridId();
		var productId=$("#productId").val();
		var flag = true;
		if(venueIds == null || venueIds=='') {
			flag= false;
			jAlert("Select a Venue to add as Popular Venue.");
			return false;
		}
		
		var selectedVenuesCount = getSelectedVenueGridRowCount();
		if(flag) {
			$.ajax({
				url : "${pageContext.request.contextPath}/GetPopularVenuesCount",
				type : "post",
				data : "productId="+productId,
				success : function(res){
					//var jsonData = JSON.parse(JSON.stringify(res));
					if((selectedVenuesCount+ parseInt(res)) > 20){
						jAlert("Maximum Eligible popluar venues are 20.\n"
								+"Remove some existing poluar venues(Count : "+parseInt(res)+") to add selected venues as popular venues.");
						return false;
					} else {
						updatePopularVenues(venueIds,productId);
					}				
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		return false;	
		} 
	}

	function saveNotes(venueId,notes){
		$.ajax({
			url : "${pageContext.request.contextPath}/saveVenueNotes",
			type : "post",
			data : "venueId="+venueId+"&notes="+notes,
			success : function(res){
				jAlert(res);				
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
</script>

<!-- venue Grid Code Starts -->
<br />
<!-- <header class="panel-heading"> 
	<a data-toggle="modal" class="btn btn-primary" id="addCategoryTickets" onclick="addModal();">Add</a>
</header> -->

<!-- popVenue Grid Code Starts -->
<br />
<div style="position: relative;" id="popVenueGridDiv">
<a data-toggle="modal" class="btn btn-primary" id="addPopularVenues" onclick="addPopularVenues();">Add Popular Venue</a>
<div id="AddpopVenueGroupSuccessMsg" style="display: none;" class="alert alert-success fade in">
 	Popular Venue Added Successfully.
  </div>
  
	<div style="width: 100%;">
		<div class="grid-header" style="width: 100%">
			<label id="popVenueGridHeaderLabel">Popular Venue</label> <span id="popVenue_grid_toogle_search" style="float: right"
				class="ui-icon ui-icon-search" title="Toggle search panel"
				onclick="popVenueGridToggleFilterRow()"></span>
		</div>
		<div id="popVenue_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
		<div id="popVenue_pager" style="width: 100%; height: 10px;"></div>
	</div>
</div>


<div id="popVenue_inlineFilterPanel" style="display:none;background:#dddddd;padding:3px;color:black;">
  Show records with VenueName including <input type="text" id="popVenueGridSearch">
</div>
<br />
<br />
<a data-toggle="modal" class="btn btn-primary" id="deletePopularVenues" onclick="deletePopularVenues();">Delete Popular Venue</a>
<script>
var popVenueCheckboxSelector = new Slick.CheckboxSelectColumn({
	  cssClass: "slick-cell-checkboxsel"
	});
	
	var popVenueDataView;
	var popVenueGrid;
	var popVenueData = [];
	var userPopVenueColumnsStr = '<%=session.getAttribute("popvenuegrid")%>';
	var userPopVenueColumns =[];
	var allPopVenueColumns =  [ popVenueCheckboxSelector.getColumnDefinition(),
		{id : "venueName",
		name : "Venue Name",
		field : "venueName",
		width:80,
		sortable : true
	}, {
		id : "city",
		name : "City",
		field : "city",
		width:80,
		sortable : true
	}, {
		id : "state",
		name : "State",
		field : "state",
		width:80,
		sortable : true
	},  {id: "country", 
		name: "Country", 
		field: "country",
		width:80,		
		sortable: true
	}, {id: "postalCode", 
		name: "Postal Code", 
		field: "postalCode",
		width:80,
		sortable: true
	},{id: "createdBy", 
		name: "Created By", 
		field: "createdBy",
		width:80,
		sortable: true
	}, {
		id : "createdDate",
		field : "createdDate",
		name : "Created Date",
		width:80,
		sortable: true
	} ];

	
	if(userPopVenueColumnsStr!='null' && userPopVenueColumnsStr!=''){
		var columnOrder = userPopVenueColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<allPopVenueColumns.length;j++){
				if(columnWidth[0] == allPopVenueColumns[j].id){
					userPopVenueColumns[i] =  allPopVenueColumns[j];
					userPopVenueColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}
			
		}
	}else{
		userPopVenueColumns = allPopVenueColumns;
	}
	
	var popVenueOptions = {
		//editable: true,
		//enableAddRow: true,
		enableCellNavigation : true,
		//asyncEditorLoading: true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25
	};
	var popVenueGridSortcol = "venueName";
	var popVenueGridSortdir = 1;
	var percentCompleteThreshold = 0;
	var popVenueGridSearchString = "";

	function deleteRecordFromVenueGrid(id) {
		popVenueDataView.deleteItem(id);
		popVenueGrid.invalidate();
	}
	function popVenueGridFilter(item, args) {
		var x= item["venueName"];
		if (args.popVenueGridSearchString  != ""
				&& x.indexOf(args.popVenueGridSearchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.popVenueGridSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}

	function popVenueGridComparer(a, b) {
		var x = a[popVenueGridSortcol], y = b[popVenueGridSortcol];
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	function popVenueGridToggleFilterRow() {
		popVenueGrid.setTopPanelVisibility(!popVenueGrid.getOptions().showTopPanel);
	}
	
	//$(".grid-header .ui-icon").addClass("ui-state-default-sg ui-corner-all")
	$("#popVenue_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all")
			.mouseover(function(e) {
				$(e.target).addClass("ui-state-hover")
			}).mouseout(function(e) {
				$(e.target).removeClass("ui-state-hover")
			});

	function refreshpopVenueGridValues(jsonData,isAjaxCall) {
		
		/*if(venuedetailStr != '0') {
			$('#popVenueGridDiv').show();
			//$('#popVenueGridHeaderLabel').text(venuedetailStr);
		}*/
		popVenueData = [];
		if(isAjaxCall) {
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i]; 
				var d = (popVenueData[i] = {});
				d["id"] = data.venueId;
				d["venueName"] = data.venue;
				d["city"] = data.city;
				d["state"] = data.state;
				d["country"] = data.country;
				d["postalCode"] = data.postalCode;
				d["createdBy"] = data.createdBy;
				d["createdDate"] = data.createdDate;
			}
		} else {
			var i=0;
			<c:forEach var="popVenue" items="${popVenues}">
			var d = (popVenueData[i] = {});
			d["id"] = "${popVenue.venue.id}";
			//d["id"] = "id_" + i;
			d["venueName"] = "${popVenue.venue.building}";
			d["city"] = "${popVenue.venue.city}";
			d["state"] = "${popVenue.venue.state}";
			d["country"] = "${popVenue.venue.country}";
			d["createdBy"] = "${popVenue.createdBy}";
			d["createdDate"] = "${popVenue.createdDateStr}";
			i++;
			</c:forEach>
		}
		
		popVenueDataView = new Slick.Data.DataView({
			inlineFilters : true
		});
		popVenueGrid = new Slick.Grid("#popVenue_grid", popVenueDataView, userPopVenueColumns, popVenueOptions);
		popVenueGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		popVenueGrid.setSelectionModel(new Slick.RowSelectionModel());
		popVenueGrid.registerPlugin(popVenueCheckboxSelector);
		var popVenueGridPager = new Slick.Controls.Pager(popVenueDataView, popVenueGrid, $("#popVenue_pager"));
		var popVenueGridColumnpicker = new Slick.Controls.ColumnPicker(allPopVenueColumns, popVenueGrid,
				popVenueOptions);

		// move the filter panel defined in a hidden div into popVenueGrid top panel
		$("#popVenue_inlineFilterPanel").appendTo(popVenueGrid.getTopPanel()).show();

		/* popVenueGrid.onKeyDown.subscribe(function(e) {
			// select all rows on ctrl-a
			if (e.which != 65 || !e.ctrlKey) {
				return false;
			}
			var rows = [];
			for ( var i = 0; i < popVenueDataView.getLength(); i++) {
				rows.push(i);
			}
			popVenueGrid.setSelectedRows(rows);
			e.preventDefault();
		}); */
		
		popVenueGrid.onSort.subscribe(function(e, args) {
			popVenueGridSortdir = args.sortAsc ? 1 : -1;
			popVenueGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				// using temporary Object.prototype.toString override
				// more limited and does lexicographic sort only by default, but can be much faster
				// use numeric sort of % and lexicographic for everything else
				popVenueDataView.fastSort(popVenueGridSortcol, args.sortAsc);
			} else {
				// using native sort with comparer
				// preferred method but can be very slow in IE with huge datasets
				popVenueDataView.sort(popVenueGridComparer, args.sortAsc);
			}
		});
		// wire up model popVenues to drive the popVenueGrid
		popVenueDataView.onRowCountChanged.subscribe(function(e, args) {
			popVenueGrid.updateRowCount();
			popVenueGrid.render();
		});
		popVenueDataView.onRowsChanged.subscribe(function(e, args) {
			popVenueGrid.invalidateRows(args.rows);
			popVenueGrid.render();
		});

		// wire up the search textbox to apply the filter to the model
		$("#popVenueGridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			popVenueGridSearchString = this.value;
			updatePopVenueGridFilter();
		});
		function updatePopVenueGridFilter() {
			popVenueDataView.setFilterArgs({
				popVenueGridSearchString : popVenueGridSearchString
			});
			popVenueDataView.refresh();
		}
		
		// initialize the model after all the popVenues have been hooked up
		popVenueDataView.beginUpdate();
		popVenueDataView.setItems(popVenueData);
		popVenueDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			popVenueGridSearchString : popVenueGridSearchString
		});
		popVenueDataView.setFilter(popVenueGridFilter);
		popVenueDataView.endUpdate();
		popVenueDataView.syncGridSelection(popVenueGrid, true);
		popVenueGrid.setSelectedRows([]);
		popVenueGrid.resizeCanvas();
	}
	
	<!-- popVenue Grid Code Ends -->
	
	function getSelectedPopVenueGridId() {
		var temprVenueRowIndex = popVenueGrid.getSelectedRows();
		
		var venueIdStr='';
		$.each(temprVenueRowIndex, function (index, value) {
			venueIdStr += ','+popVenueGrid.getDataItem(value).id;
		});
		
		if(venueIdStr != null && venueIdStr!='') {
			venueIdStr = venueIdStr.substring(1, venueIdStr.length);
			 return venueIdStr;
		}
	}
	
	function deletePopularVenues() {
		var venueIds = getSelectedPopVenueGridId();
		var productId=$("#productId").val();
		if(venueIds != null && venueIds!='') {
			jConfirm("Are you sure you want to remove selected venues from popular venues?","Confirm",function(r){
				if(r){
					$.ajax({
						url : "${pageContext.request.contextPath}/UpdatePopularVenue",
						type : "post",
						data : "action=delete&productId="+productId+"&venueIds="+ venueIds,
						success : function(res){
							var jsonData = JSON.parse(JSON.stringify(res));
							jAlert("Selected Venue removed from popular Venues.");
							refreshpopVenueGridValues(jsonData,true);
							clearAllSelections();
						}, error : function(error){
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
					});
				}
			});
		}else {
			jAlert("Select an Venue to remove from Popular Venue's.");
		} 
	}
	
	$('#menuContainer').click(function(){
		  if($('.ABCD').length >0){
			   $('#menuContainer').removeClass('ABCD');
		  }else{
			   $('#menuContainer').addClass('ABCD');
		  }
		  venueGrid.resizeCanvas();
		  popVenueGrid.resizeCanvas();
	});
	
	function clearAllSelections(){
		var eventTable = document.getElementsByClassName('slick-cell-checkboxsel');
		for(var j=0;j<eventTable.length;j++){
			var eventCheckbox = eventTable[j].getElementsByTagName('input');
			for ( var i = 0; i < eventCheckbox.length; i++) {
				if(eventCheckbox[i].type =='checkbox'){
					eventCheckbox[i].checked = false;
				}
			}
		}
		
	}
	
	function saveUserVenuePreference(){
		var cols = visibleColumns;
		if(cols==null || cols =='' || cols.length==0){
			cols = venueGrid.getColumns();
		}
		var colStr = '';
		for(var i=0;i<cols.length;i++){
			colStr += cols[i].id+":"+cols[i].width+","
		}
		saveUserPreference('venuegrid',colStr);
	}
	function saveUserPopVenuePreference(){
		var cols = visibleColumns;
		if(cols==null || cols =='' || cols.length==0){
			cols = popVenueGrid.getColumns();
		}
		var colStr = '';
		for(var i=0;i<cols.length;i++){
			colStr += cols[i].id+":"+cols[i].width+","
		}
		saveUserPreference('popvenuegrid',colStr);
	}
	
	
	//call functions once page loaded
	window.onload = function() {
		refreshVenueGridValues(null,false);
		refreshpopVenueGridValues(null,false) ;
		$('#venue_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserVenuePreference()'>");
		$('#popVenue_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserPopVenuePreference()'>");
		enableMenu();
	};
</script>
	
</body>
</html>