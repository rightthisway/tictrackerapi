<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<link href="../resources/css/bootstrap.min.css" rel="stylesheet">
<link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
<link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
<link href="../resources/css/font-awesome.min.css" rel="stylesheet" />
<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />
<link rel="stylesheet" href="../resources/css/owl.carousel.css" type="text/css">
<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
<link href="../resources/css/widgets.css" rel="stylesheet">
<link href="../resources/css/style.css" rel="stylesheet">
<link href="../resources/css/style-responsive.css" rel="stylesheet" />
<script src="../resources/js/jquery-1.8.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/bootstrap.min.js"></script>
<script src="../resources/js/jquery.scrollTo.min.js"></script>
<script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type="text/javascript" src="../resources/js/jquery.panzoom.min.js"></script>
<script type="text/javascript" src="../resources/js/jquery.svg.pan.zoom.js"></script>
<script type="text/javascript" src="../resources/js/svg-pan-zoom.js"></script>
<script type="text/javascript" src="../resources/js/hammer.js"></script>
<script type="text/javascript" src="../resources/js/TicketController.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>
<style>
.selectedSVG{
  
  stroke-width: 15;
  stroke:black !important;
  
}
.slick-headerrow-column {
    background: #87ceeb;
    text-overflow: clip;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
.slick-headerrow-column input {
     margin: 0;
     padding: 0;
     width: 100%;
     height: 100%;
     -moz-box-sizing: border-box;
     box-sizing: border-box;
}
</style>
<div class="full-width">
	<div class="col-lg-12">
		<div class="svgContainer" style="z-index: 10">
			<div id="svgMap" class="col-xs-12 col-sm-6 col-md-8" style="">
				<svg id="svg1" height="710" version="1.1" width="900" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="overflow: hidden; position: relative;"
					viewBox="0 0 2650 2048" preserveAspectRatio="xMinYMin" align="left"> <desc style='-webkit-tap-highlight-color: rgba(0, 0, 0, 0);'>Created with Rapha�l 2.1.4</desc> <defs
					style='-webkit-tap-highlight-color: rgba(0, 0, 0, 0);'></defs> <image id='mapImg' x='0' y='0' width='2560' height='2048' preserveAspectRatio='none' xmlns:xlink='http://www.w3.org/1999/xlink'
					xlink:href='${ticketList.event.svgMapPath}' style='-webkit-tap-highlight-color: rgba(0, 0, 0, 0);' stroke-width='1'></image> ${ticketList.event.svgText} <img id="svgZoomIn"
					style="cursor: pointer; float: right; bottom: 90%; position: absolute; right: 95%; width: 30px !important; height: 30px !important;" src="../resources/images/zoomIn.jpg" /> &nbsp <img
					style="cursor: pointer; float: right; bottom: 83%; position: absolute; right: 95%; width: 30px !important; height: 30px !important;" id="svgZoomOut" src="../resources/images/zoomOut.jpg" /> </svg>
			</div>
		</div>
	</div>
	<div class="ticket-price" style="display: none">
		<ul id="tickets">
			<c:forEach var="ticket1" items="${ticketList.ticketGroupQtyList}">
				<c:if test="${ticket1.qty == 0 }">
					<c:forEach var="ticket" items="${ticket1.categoryTicketGroups}" varStatus="loopStatus">
						<li data-zone-qty="${ticket.quantity}" data-zone="${ticket.svgKey}" data-color="${ticket.colorCode}" id="tr_${ticket.svgKey}-:-${ticket.colorCode}"></li>
					</c:forEach>
				</c:if>
			</c:forEach>
		</ul>
	</div>
</div>
<div class="full-width">
	<div class="table-responsive grid-table mt-20">
		<div class="grid-header full-width">
			<label>Tickets</label>
		</div>
		<div id="ticket_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
		<div id="ticket_pager" style="width: 100%; height: 10px;"></div>
	</div>
</div>
<script>
var pagingInfo=JSON.parse(JSON.stringify(${pagingInfo}));
var ticketDataView;
var ticketGrid;
var ticketData = [];
var ticketColumns = [ {
	id : "ticId",
	name : "Id",
	field : "ticId",
	sortable : true
},{
	id : "section",
	name : "Section",
	field : "section",
	sortable : true
},{
	id : "actualSection",
	name : "Actual Section",
	field : "actualSection",
	sortable : true
},{
	id : "row",
	name : "Row",
	field : "row",
	sortable : true
},{
	id : "quantity",
	name : "Quantity",
	field : "quantity",
	sortable : true
},{
	id : "price",
	name : "Price",
	field : "price",
	sortable : true
}, {
	id : "sectionRange",
	name : "Section Range",
	field : "sectionRange",
	width:80,
	sortable : true
}, {
	id : "rowRange",
	name : "Row Range",
	field : "rowRange",
	sortable : true
}, {
	id : "shippingMethod",
	name : "Shipping Method",
	field : "shippingMethod",
	sortable : true
}];

var ticketOptions = {
	enableCellNavigation : true,
	forceFitColumns : true,
	topPanelHeight : 25,
	showHeaderRow: true,
	headerRowHeight: 30,
	explicitInitialization: true
};
var ticketSortcol = "id";
var ticketSortdir = 1;
var ticketSearchString = "";
var columnFilters = {};

function ticketFilter(item, args) {
	var x= item["invoiceId"];
	if (args.eventGridSearchString  != ""
			&& x.indexOf(args.eventGridSearchString) == -1) {
		
		if (typeof x === 'string' || x instanceof String) {
			if(x.toLowerCase().indexOf(args.eventGridSearchString.toLowerCase()) == -1) {
				return false;
			}
		} else {
			return false;
		}
	}
	return true;
}

function ticketComparer(a, b) {
	var x = a[ticketSortcol], y = b[ticketSortcol];
	if(!isNaN(x)){
	   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	}
	if(x == '' || x == null) {
		return 1;
	} else if(y == '' || y == null) {
		return -1;
	}
	if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
		return x.toLowerCase().localeCompare(y.toLowerCase());	
	} else {
		return (x == y ? 0 : (x > y ? 1 : -1));	
	}
}

function filter(item) {
    for (var columnId in columnFilters) {
      if (columnId !== undefined && columnFilters[columnId] !== "") {
        var c = ticketGrid.getColumns()[ticketGrid.getColumnIndex(columnId)];
        if (item[c.field].toLowerCase() != columnFilters[columnId].toLowerCase()) {
          return false;
        }
      }
    }
    return true;
  }

function refreshTicketGridValues() {
	var i=0;
	<c:forEach var="tickets" items="${ticketList.ticketGroupQtyList}">
		<c:forEach var="ticket" items="${tickets.categoryTicketGroups}">
			var d = (ticketData[i] = {});
			d["id"] = i;
			d["ticId"] = '${ticket.id}';
			d["key"] = '${ticket.svgKey}';
			d["section"] = '${ticket.section}';
			d["actualSection"] = '${ticket.actualSection}';
			d["row"] = '${ticket.row}';
			d["quantity"] = '${ticket.quantity}';
			d["price"] = '${ticket.price}';
			d["sectionRange"] = '${ticket.sectionRange}';
			d["rowRange"] = '${ticket.rowRange}';
			d["shippingMethod"] = '${ticket.shippingMethod}';
			i++;
		</c:forEach>
	</c:forEach>
	
			
		ticketDataView = new Slick.Data.DataView({
			inlineFilters : true
		});
		ticketGrid = new Slick.Grid("#ticket_grid", ticketDataView, ticketColumns, ticketOptions);
		ticketGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		ticketGrid.setSelectionModel(new Slick.RowSelectionModel());
		var ticketPager = new Slick.Controls.Pager(ticketDataView, ticketGrid, $("#ticket_pager"),pagingInfo);
		var ticketColumnpicker = new Slick.Controls.ColumnPicker(ticketColumns, ticketGrid,
			ticketOptions);

	ticketGrid.onSort.subscribe(function(e, args) {
		ticketSortdir = args.sortAsc ? 1 : -1;
		ticketSortcol = args.sortCol.field;
		if ($.browser.msie && $.browser.version <= 8) {
			ticketDataView.fastSort(ticketSortcol, args.sortAsc);
		} else {
			ticketDataView.sort(ticketComparer, args.sortAsc);
		}
	});
	// wire up model events to drive the eventGrid
	ticketDataView.onRowCountChanged.subscribe(function(e, args) {
		ticketGrid.updateRowCount();
		ticketGrid.render();
	});
	ticketDataView.onRowsChanged.subscribe(function(e, args) {
		ticketGrid.invalidateRows(args.rows);
		ticketGrid.render();
	});
	$(ticketGrid.getHeaderRow()).delegate(":input", "change keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
	 	 var columnId = $(this).data("columnId");
	      if (columnId != null) {
	    		columnFilters[columnId] = $.trim($(this).val());
	  	        ticketDataView.refresh();
	      }
	});
	ticketGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
		$(args.node).empty();
			if(args.column.id == 'section'){
				$("<input type='text' id='sectionHeader' class='headerInput'>")
			   .data("columnId", args.column.id)
			   .val(columnFilters[args.column.id])
			   .appendTo(args.node);
			}else{
				$("<input type='text' class='headerInput'>")
				   .data("columnId", args.column.id)
				   .val(columnFilters[args.column.id])
				   .appendTo(args.node);
			}
	});
	ticketGrid.init();
	// initialize the model after all the events have been hooked up
	ticketDataView.beginUpdate();
	ticketDataView.setItems(ticketData);
	ticketDataView.setFilter(filter);
	ticketDataView.endUpdate();
	ticketDataView.syncGridSelection(ticketGrid, true);
	$("#gridContainer").resizable();
	ticketGrid.resizeCanvas();
	$("div#divLoading").removeClass('show');
}

window.onload = function() {
	refreshTicketGridValues();
};

</script>