<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<script type="text/javascript">
var isUser = '<%=session.getAttribute("isUser")%>';
$(document).ready(function(){
	if(isUser != 'false'){
		$("#adminRpt").hide();
	}
});
</script>

<style>
	input{
		color : black !important;
	}
</style>


<div class="row">
				<div class="col-xs-12">
					<h3 class="page-header"><i class="fa fa-laptop"></i> Reports</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="/Reports/Home">Reports</a></li>
						<li><i class="fa fa-laptop"></i>Events Statistics Report</li>						  	
					</ol>
					<!-- <a style="margin-top:-0.8%;margin-left:5%;" class="btn btn-primary" href="#" title="Sales Report">Sales Report</a> -->
				</div>
</div>

<div class="row">
           <div class="col-xs-12">
				<div class="table-responsive report-table">	
					<table class="table-striped" border="1">
						<tr>
							<c:forEach var="eventsStatistics" items="${eventsStatisticsReportHeader }">
								<th>
									${eventsStatistics }
								</th>
							</c:forEach>
						</tr>
						<tr>
							<c:forEach var="eventsStatistics" items="${eventsStatisticsCsv }">
								<td>
									${eventsStatistics}
								</td>
							</c:forEach>
						</tr>
					</table>
				</div>
           </div>  
</div>