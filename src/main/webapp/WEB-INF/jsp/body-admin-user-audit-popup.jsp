<!DOCTYPE HTML>
<html>
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <title>User audit</title>
  <link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.8.16.custom.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker.css" type="text/css"/>
  <script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>
  <link href="../resources/css/jquery.alerts.css" rel=" stylesheet">
  <script src="../resources/js/jquery.alerts.js"></script>
  <style>
    .cell-title {
      font-weight: bold;
    }
    .cell-effort-driven {
      text-align: center;
    }
    .cell-selection {
      border-right-color: silver;
      border-right-style: solid;
      background: #f5f5f5;
      color: gray;
      text-align: right;
      font-size: 10px;
    }
    .slick-row.selected .cell-selection {
      background-color: transparent; /* show default selected row background */
    }
    .slick-viewport {
    	overflow-x: hidden !important;
	}
  </style>
</head>
<body>
<script type="text/javascript">
$(document).ready(function(){
	 $("div#divLoading").addClass('show');
	$('#fromDate').datepicker({
        format: "mm/dd/yyyy",
        startDate : "01-01-2016",
        endDate: "${endDate}"
    });
	
	$('#toDate').datepicker({
        format: "mm/dd/yyyy",
        startDate : "01-01-2016",
        endDate: "${endDate}"
    });
});

function doAuditUserValidations(){
	if($("#fromDate").val() == ''){
		jAlert("From date can't be blank.");
		return false;
	}else if($("#toDate").val() == ''){
		jAlert("From date can't be blank.");
		return false;
	}else{
		$("#auditUserForm").attr('action','${pageContext.request.contextPath}/Admin/AuditUser?userId='+$("#userId").val());
		$("#auditUserForm").submit();
	}
}
</script>
	<form class="form-validate form-horizontal" id="auditUserForm" method="post">
                       <input class="form-control" id="action" value="action" name="action" type="hidden" />
                       
                       		<table class="table table-striped table-advance table-hover" align="center">
                       		<tbody>
                       		<tr>
                       		<td>
                            	Date *
                            </td>
                            <td>
                                	<input style="width:180px;" id="fromDate" name="fromDate" value="${fromDate}" type="text"/>
									<input style="width:180px;" id="toDate" name="toDate" value="${toDate}" type="text"/>
									<input type="hidden" name="userId" id="userId" value="${userId}">
                             </td>
                             </tr>
                            <%--  <tr>
                             	<td>
                             		User *
                             	</td>
                             	<td>
                             		<select style="width:18%;" id="userId">
                                       <c:forEach items="${allTrackerUserList}" var="trackerUser">
                                       		<option name="userId" value="${trackerUser.id}"
                                       		<c:if test="${userId == trackerUser.id}">selected</c:if>>
                                       		${trackerUser.firstName} ${trackerUser.lastName}</option>
                                       </c:forEach>
                                    </select>
                             	</td>
                             </tr> --%>
                             <tr>
                             <td></td>
                             	<td align="center">
                             		<button class="btn btn-primary" onclick="doAuditUserValidations()" type="button">Submit</button>
                             	</td>
                             </tr>
                             </tbody>
                             </table>
				</form>
				
<div style="position:relative">
  <div style="width:100%;">
    <div class="grid-header" style="width:100%">
      <label>User Audit</label>
      <span style="float:right" class="ui-icon-sg ui-icon-sg-search" title="Toggle search panel"
            onclick="toggleFilterRow()"></span>
    </div>
    <div id="myGrid" style="width:100%;height:250px;border-right:1px solid gray;border-left:1px solid gray"></div>
    <div id="pager" style="width:100%;height:20px;"></div>
  </div>
</div>

<div id="inlineFilterPanel" style="display:none;background:#dddddd;padding:3px;color:black;">
  Show records with IP Address including <input type="text" id="txtSearch2">
  <!--and % at least &nbsp;
  <div style="width:100px;display:inline-block;" id="pcSlider2"></div>
--></div>

<script src="../resources/js/slick/lib/firebugx.js"></script>

<script src="../resources/js/slick/lib/jquery-1.7.min.js"></script>
<script src="../resources/js/slick/lib/jquery-ui-1.8.16.custom.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>

<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>

<script>
var dataView;
var grid;
var data = [];
var columns = [
               {id: "ipAddress", name: "IP Address", field: "ipAddress"},
               {id: "firstName", name: "First Name", field: "firstName", sortable: true},
               {id: "lastName", name: "Last Name", field: "lastName", sortable: true},
               {id: "action", name: "Action", field: "action", sortable: true},
               {id: "time", name: "Time", field: "time", sortable: true}
			   
             ];
             
var options = {
  //editable: true,
  //enableAddRow: true,
  enableCellNavigation: true,
  //asyncEditorLoading: true,
  forceFitColumns: true,
  topPanelHeight: 25
};
var sortcol = "ipAddress";
var sortdir = 1;
var percentCompleteThreshold = 0;
var searchString = "";


//Now define your buttonFormatter function
function buttonFormatter(row,cell,value,columnDef,dataContext){  
    var button = "<input class='del' value='Delete' type='button' id='"+ dataContext.id +"' />";
    //the id is so that you can identify the row when the particular button is clicked
    return button;
    //Now the row will display your button
}
function auditFormatter(row,cell,value,columnDef,dataContext){  
    var button = "<input class='audit' value='Audit' type='button' id='"+ dataContext.id +"' />";
    //the id is so that you can identify the row when the particular button is clicked
    return button;
    //Now the row will display your button
}

//Now you can use jquery to hook up your delete button event
$('.del').live('click', function(){
    var me = $(this), id = me.attr('id');
   jConfirm("Are you sure,Do you want to Delete it?","Confirm",function(r){
	   if (r) {
	    	dataView.deleteItem(id);
	    	grid.invalidate();   
	    }
   });
    
    //assuming you have used a dataView to create your grid
    //also assuming that its variable name is called 'dataView'
    //use the following code to get the item to be deleted from it
    //dataView.deleteItem(id);
    //This is possible because in the formatter we have assigned the row id itself as the button id;
    //now assuming your grid is called 'grid'
   // grid.invalidate();        
});

//Now you can use jquery to hook up your delete button event
$('.audit').live('click', function(){
    var me = $(this), id = me.attr('id');
    jAlert(id);
    //dataView.deleteItem(id);
    //grid.invalidate();        
});

function myFilter(item, args) {
  var x= item["ipAddress"];
	if (args.searchString != ""
			&& x.indexOf(args.searchString) == -1) {
		
		if (typeof x === 'string' || x instanceof String) {
			if(x.toLowerCase().indexOf(args.searchString.toLowerCase()) == -1) {
				return false;
			}
		} else {
			return false;
		}
	}
  return true;
}

function comparer(a, b) {
  var x = a[sortcol], y = b[sortcol];
  if(x == '' || x == null) {
		return 1;
	} else if(y == '' || y == null) {
		return -1;
	}
	if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
		return x.toLowerCase().localeCompare(y.toLowerCase());	
	} else {
		return (x == y ? 0 : (x > y ? 1 : -1));	
	}
}
function toggleFilterRow() {
  grid.setTopPanelVisibility(!grid.getOptions().showTopPanel);
}
$(".grid-header .ui-icon-sg")
        .addClass("ui-state-default-sg ui-corner-all")
        .mouseover(function (e) {
          $(e.target).addClass("ui-state-hover")
        })
        .mouseout(function (e) {
          $(e.target).removeClass("ui-state-hover")
        });

$(function () {
	var i =0;
	<c:forEach var="userAction" items="${userActionList}">
	var d = (data[i] = {});
	d["id"] = '${userAction.id}';
	//d["id"] = "id_" + i;
	//jAlert('ip..'+'${userAction.ipAddress}');
    d["ipAddress"] = '${userAction.ipAddress}';
    d["firstName"] = '${userAction.trackerUser.firstName}';
    d["lastName"] = '${userAction.trackerUser.lastName}';
    d["action"] = '${userAction.action}';
    d["time"] = '${userAction.timeStampStr}';
	i++;
	</c:forEach>
  // prepare the data
  /*for (var i = 0; i < 50000; i++) {
    var d = (data[i] = {});
    d["id"] = "id_" + i;
    d["num"] = i;
    d["title"] = "Task " + i;
    d["duration"] = "5 days";
    d["start"] = "01/01/2009";
    d["finish"] = "01/05/2009";
  }*/
  dataView = new Slick.Data.DataView({ inlineFilters: true });
  grid = new Slick.Grid("#myGrid", dataView, columns, options);
  grid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
  var cols = grid.getColumns();
  var colTest = [];
  for(var c=0;c<cols.length;c++) {
	  //if(cols[c].name!='Title' && cols[c].name!='Start') {
	 	 colTest.push(cols[c]);
	 // }  
	  grid.setColumns(colTest);
  }
  grid.invalidate();
  grid.setSelectionModel(new Slick.RowSelectionModel());
  var pager = new Slick.Controls.Pager(dataView, grid, $("#pager"));
  var columnpicker = new Slick.Controls.ColumnPicker(columns, grid, options);

  
  // move the filter panel defined in a hidden div into grid top panel
  $("#inlineFilterPanel")
      .appendTo(grid.getTopPanel())
      .show();
  
  grid.onKeyDown.subscribe(function (e) {
    // select all rows on ctrl-a
    if (e.which != 65 || !e.ctrlKey) {
      return false;
    }
    var rows = [];
    for (var i = 0; i < dataView.getLength(); i++) {
      rows.push(i);
    }
    grid.setSelectedRows(rows);
    e.preventDefault();
  });
  grid.onSort.subscribe(function (e, args) {
    sortdir = args.sortAsc ? 1 : -1;
    sortcol = args.sortCol.field;
    if ($.browser.msie && $.browser.version <= 8) {
      // using temporary Object.prototype.toString override
      // more limited and does lexicographic sort only by default, but can be much faster
      // use numeric sort of % and lexicographic for everything else
      dataView.fastSort(sortcol, args.sortAsc);
    } else {
      // using native sort with comparer
      // preferred method but can be very slow in IE with huge datasets
      dataView.sort(comparer, args.sortAsc);
    }
  });
  // wire up model events to drive the grid
  dataView.onRowCountChanged.subscribe(function (e, args) {
    grid.updateRowCount();
    grid.render();
  });
  dataView.onRowsChanged.subscribe(function (e, args) {
    grid.invalidateRows(args.rows);
    grid.render();
  });
 
  //var h_runfilters = null;
  // wire up the slider to apply the filter to the model
  /*$("#pcSlider,#pcSlider2").slider({
    "range": "min",
    "slide": function (event, ui) {
      Slick.GlobalEditorLock.cancelCurrentEdit();
      if (percentCompleteThreshold != ui.value) {
        window.clearTimeout(h_runfilters);
        h_runfilters = window.setTimeout(updateFilter, 10);
        percentCompleteThreshold = ui.value;
      }
    }
  });*/
  // wire up the search textbox to apply the filter to the model
  $("#txtSearch,#txtSearch2").keyup(function (e) {
    Slick.GlobalEditorLock.cancelCurrentEdit();
    // clear on Esc
    if (e.which == 27) {
      this.value = "";
    }
    searchString = this.value;
    updateFilter();
  });
  function updateFilter() {
    dataView.setFilterArgs({
      searchString: searchString
    });
    dataView.refresh();
  }

  /*$("#btnSelectRows").click(function () {
    if (!Slick.GlobalEditorLock.commitCurrentEdit()) {
      return;
    }
    var rows = [];
    for (var i = 0; i < 10 && i < dataView.getLength(); i++) {
      rows.push(i);
    }
    grid.setSelectedRows(rows);
  });*/
  // initialize the model after all the events have been hooked up
  dataView.beginUpdate();
  dataView.setItems(data);
  dataView.setFilterArgs({
    percentCompleteThreshold: percentCompleteThreshold,
    searchString: searchString
  });
  dataView.setFilter(myFilter);
  dataView.endUpdate();
  // if you don't want the items that are not visible (due to being filtered out
  // or being on a different page) to stay selected, pass 'false' to the second arg
  dataView.syncGridSelection(grid, true);
  $("#gridContainer").resizable();

  $(window).resize(function(){
	    //var h=$(window).height();
	   // $('#myGrid').css({'height':(h-65)+'px'});
	    grid.resizeCanvas();
	});
  $("div#divLoading").removeClass('show');
  //var width = $(window).width() - 25; 
  //$("#mydiv").width(width);
  
	  
});
</script>
</body>
</html>