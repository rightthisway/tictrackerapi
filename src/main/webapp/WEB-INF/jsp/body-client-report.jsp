<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>

<style>
input {
	color: black !important;
}
.tagSpan1{
	margin-right:5px;
	background: #87ceeb;
	font-size: 12pt;
	height:40px;
	
}
.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#addCustomer {
	font-family: arial;
	font-size: 8pt;
	font-weight: bold;
	color: #2b2b2b;
	background-color: rgba(0, 122, 255, 0.32);
	border: 1px solid gray;
}

input {
	color: black !important;
}

</style>
<script>
var venueArray = [];
var artistArray = [];
$(document).ready(function(){
	//$('#resetLink').hide();
	$('#searchValue').keypress(function (event) {
	var keyCode = (event.keyCode ? event.keyCode : event.which);
	 if(keyCode == 13)  // the enter key code
	  {
		 getCustomerGridData(0);
	    return false;  
	  }
	});
	
	$('#venue').autocomplete("AutoCompleteVenues", {
		width: 650,
		max: 1000,
		minChars: 2,
		formatItem: function(row, i, max) {
			if(row[0]=='VENUE'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2]+", "+row[3] ;
			}
		}
	}).result(function (event,row,formatted){
		var htmlString ='';
		htmlString = '<span class="tagSpan1" id="venue_'+venueArray.length+'"><span>'+row[2]+'&nbsp;</span>';
		htmlString += '<a href="javascript:removeVenueLabel('+venueArray.length+');">x</a></span>';
		$('#tagSpanVenue').append(htmlString);
		venueArray[venueArray.length] = "'"+row[2]+"'";
		$('#venue').val('');
	});
	
	
	$('#artist').autocomplete("AutoCompleteArtist", {
		width: 650,
		max: 1000,
		minChars: 2,
		formatItem: function(row, i, max) {
			if(row[0]=='ARTIST'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2];
			}
		}
	}).result(function (event,row,formatted){
		var htmlString ='';
		htmlString = '<span class="tagSpan1" id="artist_'+artistArray.length+'"><span>'+row[2]+'&nbsp;</span>';
		htmlString += '<a href="javascript:removeArtistLabel('+artistArray.length+');">x</a></span>';
		$('#tagSpanArtist').append(htmlString);
		artistArray[artistArray.length] = "'"+row[2]+"'";
		$('#artist').val('');
	});
	
	
});

/* function resetItem(){
	$('#resetLink').hide();
	$('#venueId').val("");
	$('#selectedItem').text("");
	$('#venueName').val("");
} */

function loadState(){
	//var countryId = $('#country').val();
	var countries =[];
	$('#country :selected').each(function(i, selected) {
		if (!$(selected).val() == '') {
			countries[i] = $(selected).val();
		}
	});
	var countryIds='';
	for ( var i = 0; i < countries.length; i++) {
		countryIds = countryIds + countries[i] + ',';
	}
	
	if(countryIds!=''){
		$.ajax({
			url : "${pageContext.request.contextPath}/GetStates",
			type : "post",
			dataType:"json",
			data : "countryId="+countryIds,
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				updateStateCombo(jsonData);
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}else{
		$('#state').empty();
		$('#state').append("<option value=''>--select--</option>");
	}
}

function updateStateCombo(jsonData){
	$('#state').empty();
	/* $('#state').append("<option value=''>--select--</option>"); */
	if(jsonData!='' && jsonData!=null){
		for(var i=0;i<jsonData.length;i++){
			$('#state').append("<option value="+jsonData[i].id+">"+jsonData[i].name+"</option>");
		}
	}
}

/* function loadGrandChildCategory(){
	var childIds =[];
	$('#childCategory :selected').each(function(i, selected) {
		if (!$(selected).val() == '') {
			childIds[i] = $(selected).val();
		}
	});
	var childIdStr=childIds.toString();
	if(childIdStr!=''){
		$.ajax({
			url : "${pageContext.request.contextPath}/GetGrandChildCateory",
			type : "post",
			dataType:"json",
			data : "childIds="+childIdStr,
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				updateGrandChildCombo(jsonData);
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}else{
		$('#grandChildCategory').empty();
	}
} 

function updateGrandChildCombo(jsonData){
	$('#grandChildCategory').empty();
	if(jsonData!='' && jsonData!=null){
		for(var i=0;i<jsonData.length;i++){
			$('#grandChildCategory').append("<option value="+jsonData[i].id+">"+jsonData[i].name+"</option>");
		}
	}
}
 */
function searchCustomer(){
	getCustomerGridData(0);
}

function exportToExcel(){	
	var selectedCountry = [];
	var selectedState = [];
	var selectedProduct = [];
	var grandChilds=[];
	var childs = [];
	$('#country :selected').each(function(i, selected) {
		if (!$(selected).val() == '') {
			selectedCountry[i] = $(selected).val();
		}
	});
	$('#state :selected').each(function(i, selected) {
		if (!$(selected).val() == '') {
			selectedState[i] = $(selected).val();
		}
	});
	$('#productType :selected').each(function(i, selected) {
		if (!$(selected).val() == '') {
			selectedProduct[i] = $(selected).val();
		}
	});
	$('#grandchildCategory :selected').each(function(i, selected) {
		if (!$(selected).val() == '') {
			grandChilds[i] = $(selected).val();
		}
	});
	$('#childCategory :selected').each(function(i, selected) {
		if (!$(selected).val() == '') {
			childs[i] = $(selected).val();
		}
	});
	
	var venues = '';
	var cities = '';
	var countryIds='';
	var stateIds ='';
	var products='';
	var artists = '';
	var grandChildIds='';
	var childIds = '';
	if(grandChilds.length > 0){
		grandChildIds = grandChilds.toString();
	}
	if(childs.length > 0){
		childIds = childs.toString();
	}
	if(venueArray.length > 0){
		venues = venueArray.toString();
	}
	if(cityArray.length > 0){
		cities = cityArray.toString();
	}
	if(artistArray.length > 0){
		artists = artistArray.toString();
	}
	/* if(selectedCountry.length > 0){
		countryIds =selectedCountry.toString();
	}
	if(selectedState.length  >0){
		stateIds = selectedState.toString();
	} */
	
	for ( var i = 0; i < selectedCountry.length; i++) {
		countryIds = countryIds + selectedCountry[i] + ',';
	}
	for ( var i = 0; i < selectedState.length; i++) {
		stateIds = stateIds + selectedState[i] + ',';
	}
	for ( var i = 0; i < selectedProduct.length; i++) {
		products = products + selectedProduct[i] + ',';
	}
	
    var url = "${pageContext.request.contextPath}/Admin/ExportClientReport?products="+products+"&stateIds="+stateIds+"&countryIds="+countryIds+"&grandChildIds="+grandChildIds;
    url += "&cities="+cities+"&venue="+venues+"&headerFilter="+customerGridSearchString+"&artists="+artists+"&customerType="+$('#customerType').val()+"&childIds="+childIds;
    $('#download-frame').attr('src', url);
}
var cityArray = [];
function handleCity(e){
	var keyCode = (e.keyCode ? e.keyCode : e.which);
	var city = $('#city').val();
	if(city==null || city==''){
		return;
	}
	if(keyCode==13){
		var htmlString ='';
		htmlString = '<span class="tagSpan1" id="city_'+cityArray.length+'"><span>'+$('#city').val()+'&nbsp;</span>';
		htmlString += '<a href="javascript:removeLabel('+cityArray.length+');">x</a></span>';
		$('#tagSpanCity').append(htmlString);
		cityArray[cityArray.length] = "'"+city+"'";
		$('#city').val('');
	}
	
}
function removeVenueLabel(id){
	$('#venue_'+id).empty();
	if(venueArray.length==1 || venueArray.length==0){
		venueArray = [];
		$('#tagSpanVenue').empty();
	}else{
		venueArray.splice(id,1);
	}
	
}
function removeArtistLabel(id){
	$('#artist_'+id).empty();
	if(artistArray.length==1 || artistArray.length==0){
		artistArray = [];
		$('#tagSpanArtist').empty();
	}else{
		artistArray.splice(id,1);
	}
	
}
function removeLabel(id){
	$('#city_'+id).empty();
	if(cityArray.length==1 || cityArray.length==0){
		cityArray = [];
		$('#tagSpanCity').empty();
	}else{
		cityArray.splice(id,1);
	}
	
}

function resetFilters(){
	customerGridSearchString='';
	columnFilters = {};
	getCustomerGridData(0);
}
</script>

<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i>Manage Customer
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Manage Customer</a></li>
			<li><i class="fa fa-laptop"></i>Email Blast Management/GEOTARGETED</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-lg-12 filters-div multiple-select">
		<form role="form" id="customerSearch" onsubmit="return false">
			<div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-3">
				<label>Country</label> <select multiple id="country" name="country" style="" class="form-control" onchange="loadState()">
					<!-- <option value=''>--select--</option> -->
					<c:forEach var="country" items="${countries}">
						<option value="${country.id}">
							<c:out value="${country.name}" />
						</option>
					</c:forEach>
				</select>
			</div>
			<div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-3">
				<label>State</label> <select multiple id="state" name="state" class="form-control" style="" onchange="">
					<!-- <option value=''>--select--</option> -->
					<c:forEach var="state" items="${stateList}">
						<option value="${state.id}">
							<c:out value="${state.name}" />
						</option>
					</c:forEach>
				</select>
			</div>
			 <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-3">
				<label>Child Category</label> <select multiple id="childCategory" name="childCategory" class="form-control" onchange="loadGrandChildCategory();">
					<!-- <option value=''>--select--</option> -->
					<c:forEach var="child" items="${childs}">
						<option value="${child.tnId}">
							<c:out value="${child.name}" />
						</option>
					</c:forEach>
				</select>
			</div>
			<div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-3">
				<label>Grand Child Category</label> <select multiple id="grandchildCategory" name="grandchildCategory" class="form-control" style="" onchange="">
					<c:forEach var="grandChild" items="${grandChilds}">
						<option value="${grandChild.tnId}">
							<c:out value="${grandChild.name}" />
						</option>
					</c:forEach> 
				</select>
			</div>
			<div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-3">
				<label for="name" class="control-label">Product Type</label> 
				<select multiple id="productType" name="productType" class="form-control" style="">
					<option value="REWARDTHEFAN">Reward The Fan</option>
					<option value="RTW">RTW</option>
					<option value="RTW2">RTW2</option>
				</select>
			</div>
			<div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-4">
				<label>City</label>
				<input class="form-control searchcontrol" type="text" name="city" id="city" onkeypress="handleCity(event)" placeholder="city"> 
				<div id="tagSpanCity"></div>
			</div>
			<div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-3">
				<label for="name" class="control-label">Venue</label> 
				<input class="form-control searchcontrol" type="text" id="venue" placeholder="Venue">
				<div id="tagSpanVenue"></div> 
			</div>
			<div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-3">
				<label for="name" class="control-label">Artist</label> 
				<input class="form-control searchcontrol" type="text" id="artist" placeholder="Artist">
				<div id="tagSpanArtist"></div> 
			</div>
			<div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-3">
				<label>Customer Type</label>
				<select class="form-control" name="customerType" id="customerType"> 
				<option value="">Both</option>
					<option value="CLIENT">Client</option>
					<option value="BROKER">Broker</option>
				</select>
			</div>
			<div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-4" style="float:right">
				<button type="button" id="searchCustomerBtn" class="btn btn-primary" style="margin-top: 18px;" onclick="searchCustomer();">search</button>
			</div>
	</div>
	</form>
</div>
</div>

<br/><br/>

<!-- Grid view for customers -->
<div style="position: relative">
	<div class="table-responsive grid-table">
		<div class="grid-header full-width">
			<label>Manage Customers</label>
			<div class="pull-right">
				<a href="javascript:exportToExcel()" name='Export to Excel' style='float: right; margin-right: 10px;'>Export to Excel</a> 
				<a href="javascript:resetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters &nbsp; |</a>
			</div>
		</div>
		<div id="customerGrid" style="width: 100%; height: 200px; overflow: auto; border-right: 1px solid gray; border-left: 1px solid gray"></div>
		<div id="customer_pager" style="width: 100%; height: 20px;"></div>
	</div>
</div>
<br />

</div>
<div id="inlineFilterPanel" style="display: none; background: #dddddd; padding: 3px; color: black;">
	Show records with customer including <input type="text" id="txtSearch2">
	<!--and % at least &nbsp;
  <div style="width:100px;display:inline-block;" id="pcSlider2"></div>
-->
</div>
<script>
var pagingInfo;
var customerId='';
var customerInfo='';
var customerGrid;
var eventrowIndex;
var customerDataView;
var customerData=[];
var customerGridSearchString='';
var columnFilters = {};
var userCustomerColumnsStr = '<%=session.getAttribute("customergrid")%>';
var userCustomerColumns =[];
var loadCustomerColumns = ["customerName", "lastName", "companyName", "email", "street1", 
			"city", "state", "country", "zip", "phone"];
var allCustomerColumns = [
				{id:"customerId", name:"Customer ID", field: "customerId",width:80, sortable: true},         
               /* {id:"customerType", name:"Customer Type", field: "customerType",width:80, sortable: true}, */
               {id:"customerName", name:"First Name", field: "customerName",width:80, sortable: true},
               {id:"lastName", name:"Last Name", field: "lastName",width:80, sortable: true},
               {id:"productType", name:"Product Type", field: "productType",width:80, sortable: true},
               /* {id:"signupType", name:"Signup Type", field: "signupType",width:80, sortable: true}, */ 
               {id:"client", name:"Client", field: "client",width:80, sortable: true},
               {id:"broker", name:"Broker", field: "broker",width:80, sortable: true},
                /* {id:"customerStatus", name:"Customer Status", field: "customerStatus",width:80, sortable: true}, */
               {id:"companyName", name:"Company Name", field: "companyName",width:80, sortable: true},
               {id:"email", name:"Email", field: "email",width:80, sortable: true},
               {id:"street1", name:"Street1", field: "street1",width:80, sortable: true},
               /* {id:"street2", name:"Street2", field: "street2", sortable: true}, */
               {id:"city", name:"City", field: "city",width:80, sortable: true},
               {id:"state", name:"State", field: "state",width:80, sortable: true},
			   {id:"country", name:"Country", field: "country",width:80, sortable: true},
               {id:"zip", name:"Zip", field: "zip",width:80, sortable: true},
               {id:"phone", name:"Phone", field: "phone",width:80,sortable: true},
               /*{id:"referrerCode", name:"Referral Code", field: "referrerCode",width:80,sortable: true},
               {id: "delCol", field:"delCol", name:"Delete", width:10, formatter:buttonFormatter}
               {id: "editCol", field:"editCol", name:"Edit", width:20, formatter:editFormatter},
			   {id: "createPOCol", field:"createPOCol", name:"", width:100, formatter:createPOFormatter} ,
			   {id: "audit", field:"delCol", name:"", width:0, formatter:auditFormatter} */
              ];
  
	if(userCustomerColumnsStr!='null' && userCustomerColumnsStr!=''){
		var columnOrder = userCustomerColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<allCustomerColumns.length;j++){
				if(columnWidth[0] == allCustomerColumns[j].id){
					userCustomerColumns[i] =  allCustomerColumns[j];
					userCustomerColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}
			
		}
	}else{
		var columnOrder = loadCustomerColumns;
		var columnWidth;
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i];
			for(var j=0;j<allCustomerColumns.length;j++){
				if(columnWidth == allCustomerColumns[j].id){
					userCustomerColumns[i] = allCustomerColumns[j];
					userCustomerColumns[i].width=80;
					break;
				}
			}			
		}
		//userCustomerColumns = allCustomerColumns;
	}
  
var customerOptions = {
	enableCellNavigation : true,
	forceFitColumns : true,
	multiSelect: false,
	topPanelHeight : 25,
	showHeaderRow: true,
	headerRowHeight: 30,
	explicitInitialization: true
};

var customerSortcol = "customerName";
var customerSortdir = 1;
var percentCompleteThreshold = 0;
var customerSearchString = "";

//Now define your buttonFormatter function
function buttonFormatter(row,cell,value,columnDef,dataContext){  
   /*  var button = "<input class='del' value='Delete' type='button' id='"+ dataContext.id +"' />"; */
    var button = "<img class='delClickableImage' src='../resources/images/ico-delete.gif' id='"+ dataContext.customerId +"'/>";
    //the id is so that you can identify the row when the particular button is clicked
    return button;
    //Now the row will display your button
}

//function for deleting shipping address functionality
function deleteFormatter(row,cell,value,columnDef,dataContext){  
   /*  var button = "<input class='del' value='Delete' type='button' id='"+ dataContext.id +"' />"; */
    var button = "<img class='deleteClickableImage' src='../resources/images/ico-delete.gif' id='"+ dataContext.shippingId +"'/>";
    //the id is so that you can identify the row when the particular button is clicked
    return button;
    //Now the row will display your button
}

//function for edit functionality
function editFormatter(row,cell,value,columnDef,dataContext){
	//the id is so that you can identify the row when the particular button is clicked
	var button = "<img class='editClickableImage' src='../resources/js/slick/images/pencil.gif' id='"+ dataContext.shippingId +"'/>";
	return button;
}

//function for creating the purchase order functionality
function createPOFormatter(row,cell,value,columnDef,dataContext){
	//var createPOButton = "<img class='createPOClickableImage' src='../resources/images/PO-icon.jpg' id='"+ dataContext.customerId +"'/>";
	var createPOButton = "<input class='createPOClickableImage' value='Create PO' type='button' id='"+ dataContext.customerId +"' />";
	return createPOButton;
}

//Now you can use jquery to hook up your delete button event
$('.delClickableImage').live('click', function(){
    var me = $(this), id = me.attr('id');
    var delFlag = deleteCustomer(id);//confirm("Are you sure,Do you want to Delete it?");
});

//Function to hook up the edit button event for Shipping Address
$('.editClickableImage').live('click', function(){
	var me = $(this), id = me.attr('id');
	editModal(id);
});

//Now you can use jquery to hook up your delete button event for Shipping Address
$('.deleteClickableImage').live('click', function(){
    var me = $(this), id = me.attr('id');
    var delFlag = deleteShippingAddress(id);//confirm("Are you sure,Do you want to Delete it?");
});

//function to hoo up the create po button
$('.createPOClickableImage').live('click', function(){
	var me = $(this), id = me.attr('id');
	popupCreatePO(id);
});

function customerComparer(a, b) {
	  var x = a[customerSortcol], y = b[customerSortcol];
	  if(!isNaN(x)){
	  	return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	  }
	  if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}

function pagingControl(move,id){
	var pageNo = 0;
	if(move == 'FIRST'){
		pageNo = 0;
	}else if(move == 'LAST'){
		pageNo = parseInt(pagingInfo.totalPages)-1;
	}else if(move == 'NEXT'){
		pageNo = parseInt(pagingInfo.pageNum) +1;
	}else if(move == 'PREV'){
		pageNo = parseInt(pagingInfo.pageNum)-1;
	}
	getCustomerGridData(pageNo);
}

function getCustomerGridData(pageNo) {
	var selectedCountry = [];
	var selectedState = [];
	var selectedProduct = [];
	var grandChild=[];
	var childs=[];
	$('#country :selected').each(function(i, selected) {
		if (!$(selected).val() == '') {
			selectedCountry[i] = $(selected).val();
		}
	});
	$('#state :selected').each(function(i, selected) {
		if (!$(selected).val() == '') {
			selectedState[i] = $(selected).val();
		}
	});
	$('#productType :selected').each(function(i, selected) {
		if (!$(selected).val() == '') {
			selectedProduct[i] = $(selected).val();
		}
	});
	$('#grandchildCategory :selected').each(function(i, selected) {
		if (!$(selected).val() == '') {
			grandChild[i] = $(selected).val();
		}
	});
	$('#childCategory :selected').each(function(i, selected) {
		if (!$(selected).val() == '') {
			childs[i] = $(selected).val();
		}
	});
	
	var venues='';
	var cities ='';
	var countryIds='';
	var stateIds ='';
	var products='';
	var grandChildIds='';
	var childIds = '';
	var artists ='';
	for ( var i = 0; i < selectedCountry.length; i++) {
		countryIds = countryIds + selectedCountry[i] + ',';
	}
	for ( var i = 0; i < selectedState.length; i++) {
		stateIds = stateIds + selectedState[i] + ',';
	}
	for ( var i = 0; i < selectedProduct.length; i++) {
		products = products + selectedProduct[i] + ',';
	}
	
	
	if(grandChild.length > 0){
		grandChildIds = grandChild.toString();
	}
	
	if(childs.length > 0){
		childIds = childs.toString();
	}
	
	if(venueArray.length > 0){
		venues = venueArray.toString();
	}
	if(cityArray.length > 0){
		cities = cityArray.toString();
	}
	if(artistArray.length > 0){
		artists = artistArray.toString();
	}
	
	
	
	
	eventrowIndex=-1;
	$.ajax({
		url : "${pageContext.request.contextPath}/Admin/GetCleanedClient",
		type : "post",
		dataType: "json",
		data : $("#customerSearch").serialize()+"&pageNo="+pageNo+"&headerFilter="+customerGridSearchString+"&products="+products+"&stateIds="+stateIds+"&countryIds="+countryIds+"&grandChildIds="+grandChildIds+"&cities="+cities+"&venue="+venues+"&artists="+artists+"&customerType="+$('#customerType').val()+"&childIds="+childIds,
		success : function(res){
			var jsonData = res;
			if(jsonData==null || jsonData=='') {
				jAlert("No Data Found.");
			} 
			pagingInfo = jsonData.pagingInfo;
			refreshCustomerGridValues(jsonData.customers);
			clearAllSelections();
			$('#customer_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserCustomerPreference()'>");
		}, error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}
function refreshCustomerGridValues(jsonData) {
	customerData=[];
	if(jsonData!=null && jsonData.length > 0){
		for (var i = 0; i < jsonData.length; i++) {
			var  data= jsonData[i];			
			var d = (customerData[i] = {});			
			d["id"] = i;
			d["customerId"] = data.customerId;
			d["customerName"] = data.customerName;
			d["lastName"] = data.lastName;
			//d["customerType"] = data.customerType;
			d["productType"] = data.productType;
			//d["signupType"] = data.signupType;
			d["client"] = data.client;
			d["broker"] = data.broker;
			//d["customerStatus"] = data.customerStatus;
			d["companyName"] = data.companyName;
			d["email"] = data.customerEmail;
			d["street1"] = data.addressLine1;	
			d["street2"] = data.addressLine2;
			d["city"] = data.city;
			d["state"] = data.state;
			d["country"] = data.country;
			d["zip"] = data.zipCode;
			d["phone"] =data.phone;
		}
	}
	customerDataView = new Slick.Data.DataView();
	customerGrid = new Slick.Grid("#customerGrid", customerDataView, userCustomerColumns, customerOptions);
	customerGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	var cols = customerGrid.getColumns();
	
	 var colTest = [];
	  for(var c=0;c<cols.length;c++) {
		  //if(cols[c].name!='Title' && cols[c].name!='Start') {
		 	 colTest.push(cols[c]);
		 // }  
		  customerGrid.setColumns(colTest);
	  }
	  customerGrid.invalidate();
	  customerGrid.setSelectionModel(new Slick.RowSelectionModel());
	  if(pagingInfo!=null){
		  var pager = new Slick.Controls.Pager(customerDataView, customerGrid, $("#customer_pager"),pagingInfo);
	  }
	  var columnpicker = new Slick.Controls.ColumnPicker(allCustomerColumns,customerGrid, customerOptions);
	 
	  customerGrid.onSort.subscribe(function (e, args) {
	    customerSortdir = args.sortAsc ? 1 : -1;
	    customerSortcol = args.sortcol.field;
	    if ($.browser.msie && $.browser.version <= 8) {
	      customerDataView.fastSort(customerSortcol, args.sortAsc);
	    } else {
	      customerDataView.sort(customerComparer, args.sortAsc);
	    }
	  });
	  // wire up model events to drive the customerGrid
	  customerDataView.onRowCountChanged.subscribe(function (e, args) {
	    customerGrid.updateRowCount();
	    customerGrid.render();
	  });
	  customerDataView.onRowsChanged.subscribe(function (e, args) {
	    customerGrid.invalidateRows(args.rows);
	    customerGrid.render();
	  });
	  
	  $(customerGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	customerGridSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				columnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in columnFilters) {
					  if (columnId !== undefined && columnFilters[columnId] !== "") {
						  customerGridSearchString += columnId + ":" +columnFilters[columnId]+",";
					  }
					}
					getCustomerGridData(0);
				}
			  }
		 
		});
	  	customerGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id != 'delCol'){
					$("<input type='text'>")
				   .data("columnId", args.column.id)
				   .val(columnFilters[args.column.id])
				   .appendTo(args.node);
				}
			}
		});
		customerGrid.init();
		
		customerGrid.onSelectedRowsChanged.subscribe(function() { 
			var temprEventRowIndex = customerGrid.getSelectedRows([0])[0];
			if (temprEventRowIndex != eventrowIndex) {
				eventrowIndex = temprEventRowIndex;
				customerId =customerGrid.getDataItem(temprEventRowIndex).customerId;
				var productType = customerGrid.getDataItem(temprEventRowIndex).productType;
			}
		});
	  	  
	  customerDataView.beginUpdate();
	  customerDataView.setItems(customerData);
	  
	  customerDataView.endUpdate();
	  // if you don't want the items that are not visible (due to being filtered out
	  // or being on a different page) to stay selected, pass 'false' to the second arg
	  customerDataView.syncGridSelection(customerGrid, true);
	  $("#gridContainer").resizable();
	  $("div#divLoading").removeClass('show');
	  customerGrid.resizeCanvas();
}	
//call functions once page loaded
	window.onload = function() {
		pagingInfo = JSON.parse(JSON.stringify(${pagingInfo}));
		refreshCustomerGridValues(JSON.parse(JSON.stringify(${customers})));
		$('#customer_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserCustomerPreference()'>");
	};
</script>