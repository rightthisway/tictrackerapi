<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<!-- Bootstrap CSS -->    
    <link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../resources/css/font-awesome.min.css" rel="stylesheet" />    
    <!-- full calendar css-->
    <link href="../resources/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="../resources/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
    <!-- easy pie chart-->
    <link href="../resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <!-- owl carousel -->
    <link rel="stylesheet" href="../resources/css/owl.carousel.css" type="text/css">
	<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
	<link rel="stylesheet" href="../resources/css/fullcalendar.css">
	<link href="../resources/css/widgets.css" rel="stylesheet">
    <link href="../resources/css/style.css" rel="stylesheet">
    <link href="../resources/css/style-responsive.css" rel="stylesheet" />
	<link href="../resources/css/xcharts.min.css" rel=" stylesheet">	
	<link href="../resources/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
	
	<link href="../resources/css/jquery.alerts.css" rel=" stylesheet">
    
	<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>
	
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->
    
    <!-- javascripts -->
    <%-- <script src="../resources/js/jquery.js"></script>
	<script src="../resources/js/jquery-ui-1.10.4.min.js"></script> --%>
    <script src="../resources/js/jquery-1.8.3.min.js"></script>
    <%-- <script type="text/javascript" src="../resources/js/jquery-ui-1.9.2.custom.min.js"></script> --%>
    <!-- bootstrap -->
    <script src="../resources/js/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script src="../resources/js/jquery.scrollTo.min.js"></script>
    <script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../resources/js/jquery.alerts.js"></script>

	<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
	
	<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
	<script src="../resources/js/slick/slick.core.js"></script>
	<script src="../resources/js/slick/slick.formatters.js"></script>
	<script src="../resources/js/slick/slick.editors.js"></script>
	<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
	<script src="../resources/js/slick/slick.grid.js"></script>
	<script src="../resources/js/slick/slick.dataview.js"></script>
	<script src="../resources/js/slick/controls/slick.pager.js"></script>
	<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
	<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
	<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>
	<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<style>
.cell-title {
      font-weight: bold;
    }
    .cell-effort-driven {
      text-align: center;
    }
    .slick-headerrow-column {
     background: #87ceeb;
     text-overflow: clip;
     -moz-box-sizing: border-box;
     box-sizing: border-box;
	}
	.slick-headerrow-column input {
	     margin: 0;
	     padding: 0;
	     width: 100%;
	     height: 100%;
	     -moz-box-sizing: border-box;
	     box-sizing: border-box;
	}
    .cell-selection {
      border-right-color: silver;
      border-right-style: solid;
      background: silver;
      color: gray;
      text-align: right;
      font-size: 10px;
    }
    .slick-row.selected .cell-selection {
      background-color: transparent; /* show default selected row background */
    }
</style>
<script>

	var ticketDataView;
	var pagingInfo;
	var ticketGrid;
	var ticketData = [];
	var ticketColumnStr = '<%=session.getAttribute("ticketsgrid")%>';
	var ticketColumns = [ {id: "invoiceId", 
		name: "Invoice No", 
		field: "invoiceId", 
		sortable: true
	},{
		id : "section",
		name : "Section",
		field : "section",
		sortable : true
	}, {
		id : "row",
		name : "Row",
		field : "row",
		sortable : true
	}, {
		id : "seatLow",
		name : "Seat Low",
		field : "seatLow",
		sortable : true
	}, {
		id : "seatHigh",
		name : "Seat High",
		field : "seatHigh",
		sortable : true
	}, {
		id : "quantity",
		name : "Quantity",
		field : "quantity",
		sortable : true
	}, {
		id : "retailPrice",
		name : "Retail Price",
		field : "retailPrice",
		sortable : true
	}, {
		id : "wholeSalePrice",
		name : "WholeSale Price",
		field : "wholeSalePrice",
		sortable : true
	}, {
		id : "price",
		name : "Price",
		field : "price",
		sortable : true
	},{
		id : "facePrice",
		name : "Face Price",
		field : "facePrice",
		sortable : true
	},{
		id : "cost",
		name : "cost",
		field : "cost",
		sortable : true
	},{
		id : "shippingMethod",
		name : "Shipping Method",
		field : "shippingMethod",
		sortable : true
	},{id: "nearTermDisplayOption", 
		name: "NearTerm Display Option", 
		field: "nearTermDisplayOption", 
		sortable: true
	},{id: "broadcast", 
		name: "BroadCast", 
		field: "broadcast", 
		sortable: true
	}, {
		id : "sectionRange",
		name : "Section Range",
		field : "sectionRange",
		sortable : true
	}, {id: "rowRange", 
		name: "Row Range", 
		field: "rowRange", 
		sortable: true
	}, {id: "internalNotes", 
		name: "Internal Notes", 
		field: "internalNotes",
		editor:Slick.Editors.LongText,
		sortable: true
	},{id: "externalNotes", 
		name: "External Notes", 
		field: "externalNotes",
		sortable: true
	}, {id: "marketPlaceNotes", 
		name: "Market Place Notes", 
		field: "marketPlaceNotes", 
		sortable: true
	}];
	
	
	var ticketOptions = {
		//editable: true,
		//enableAddRow: true,
		enableCellNavigation : true,
		//asyncEditorLoading: true,
		forceFitColumns : true,
		topPanelHeight : 25,
		//autoHeight: true
	};
	var ticketGridSortcol = "invoiceId";
	var ticketGridSortdir = 1;
	var percentCompleteThreshold = 0;
	var ticketGridSearchString = "";
	
	/*
	function deleteRecordFromEventGrid(id) {
		ticketDataView.deleteItem(id);
		ticketGrid.invalidate();
	}
	function ticketGridFilter(item, args) {
		var x= item["section"];
		if (args.ticketGridSearchString != ""
				&& x.indexOf(args.ticketGridSearchString) == -1) {
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.eventGridSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}
	*/
	function ticketGridComparer(a, b) {
		var x = a[ticketGridSortcol], y = b[ticketGridSortcol];
		if(!isNaN(x)){
		  	return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
		
		
	}
	/*
	function ticketGridToggleFilterRow() {
		ticketGrid.setTopPanelVisibility(!ticketGrid.getOptions().showTopPanel);
	}
	
	//$(".grid-header .ui-icon").addClass("ui-state-default-sg ui-corner-all")
	$("#ticket_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all")
			.mouseover(function(e) {
				$(e.target).addClass("ui-state-hover")
			}).mouseout(function(e) {
				$(e.target).removeClass("ui-state-hover")
			});
	*/
	
	function pagingControl(move,id){		
		var pageNo = 0;
		if(move == 'FIRST'){
			pageNo = 0;
		}else if(move == 'LAST'){
			pageNo = parseInt(pagingInfo.totalPages)-1;
		}else if(move == 'NEXT'){
			pageNo = parseInt(pagingInfo.pageNum) +1;
		}else if(move == 'PREV'){
			pageNo = parseInt(pagingInfo.pageNum)-1;
		}	
	}
	
	function refreshSoldTicketDetils(jsonData){
		ticketData = [];
		if(jsonData!=null){			
			for (var i = 0; i < jsonData.length; i++) {				
				var data = jsonData[i];
				var d = (ticketData[i] = {});
				d["id"] = i;
				d["invoiceId"] = data.invoiceId;
				d["section"] = data.section;
				d["row"] = data.row;
				d["seatLow"] = data.seatLow;
				d["seatHigh"] = data.seatHigh;
				d["quantity"] = data.quantity;
				d["retailPrice"] = data.retailPrice;
				d["wholeSalePrice"] = data.wholeSalePrice;
				d["price"] = data.price;
				d["facePrice"] = data.facePrice;
				d["cost"] = data.cost;
				d["shippingMethod"] = data.shippingMethod;
				d["nearTermDisplayOption"] = data.nearTermDisplayOption;
				d["broadcast"] = data.broadcast;
				d["sectionRange"] = data.sectionRange;
				d["rowRange"] = data.rowRange;
				d["internalNotes"] = data.internalNotes;
				d["externalNotes"] = data.externalNotes;
				d["marketPlaceNotes"] = data.marketPlaceNotes;
			}
		}				
	
	ticketDataView = new Slick.Data.DataView();
	ticketGrid = new Slick.Grid("#ticket_grid", ticketDataView, ticketColumns, ticketOptions);
	ticketGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	ticketGrid.setSelectionModel(new Slick.RowSelectionModel());
	if(pagingInfo!=null){
		var ticketGridPager = new Slick.Controls.Pager(ticketDataView, ticketGrid, $("#ticket_pager"),pagingInfo);
	}
	
	var ticketGridColumnpicker = new Slick.Controls.ColumnPicker(ticketColumns, ticketGrid,
			ticketOptions);
	
	// move the filter panel defined in a hidden div into ticketGrid top panel
	//$("#ticket_inlineFilterPanel").appendTo(ticketGrid.getTopPanel()).show();
	 ticketGrid.onKeyDown.subscribe(function (e) {
	    // select all rows on ctrl-a
	    if (e.which != 65 || !e.ctrlKey) {
	      return false;
	    }
	    var rows = [];
	    for (var i = 0; i < ticketDataView.getLength(); i++) {
	      rows.push(i);
	    }
	    ticketGrid.setSelectedRows(rows);
	    e.preventDefault();
	  });
	 
	ticketGrid.onSort.subscribe(function(e, args) {
		ticketGridSortdir = args.sortAsc ? 1 : -1;
		ticketGridSortcol = args.sortCol.field;
		if ($.browser.msie && $.browser.version <= 8) {
			ticketDataView.fastSort(ticketGridSortcol, args.sortAsc);
		} else {
			ticketDataView.sort(ticketGridComparer, args.sortAsc);
		}
	});
	// wire up model tickets to drive the ticketGrid
	ticketDataView.onRowCountChanged.subscribe(function(e, args) {
		ticketGrid.updateRowCount();
		ticketGrid.render();
	});
	ticketDataView.onRowsChanged.subscribe(function(e, args) {
		ticketGrid.invalidateRows(args.rows);
		ticketGrid.render();
	});
	/* wire up the search textbox to apply the filter to the model
	$("#ticketGridSearch").keyup(function(e) {
		Slick.GlobalEditorLock.cancelCurrentEdit();
		// clear on Esc
		if (e.which == 27) {
			this.value = "";
		}
		ticketGridSearchString = this.value;
		updateEventGridFilter();
	});
	function updateEventGridFilter() {
		ticketDataView.setFilterArgs({
			ticketGridSearchString : ticketGridSearchString
		});
		ticketDataView.refresh();
	}
	*/
	// initialize the model after all the tickets have been hooked up
	ticketDataView.beginUpdate();
	ticketDataView.setItems(ticketData);
	/*ticketDataView.setFilterArgs({
		percentCompleteThreshold : percentCompleteThreshold,
		ticketGridSearchString : ticketGridSearchString
	});
	ticketDataView.setFilter(ticketGridFilter);*/
	ticketDataView.endUpdate();
	ticketDataView.syncGridSelection(ticketGrid, true);
	ticketDataView.refresh();
	$("#gridContainer").resizable();
	ticketGrid.resizeCanvas();

}

	function saveUserTicketPreference(){
		var cols = visibleColumns;
		if(cols==null || cols =='' || cols.length==0){
			cols = ticketGrid.getColumns();
		}
		var colStr = '';
		for(var i=0;i<cols.length;i++){
			colStr += cols[i].id+":"+cols[i].width+",";
		}
		saveUserPreference('ticketsgrid',colStr);
	}

</script>

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Events</a>
			</li>
			<li><i class="fa fa-laptop"></i>Sold Tickets</li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
	<label>Event Name &nbsp;&nbsp;&nbsp;: ${eventDetails.eventName}</label><br/>
	<label>Event Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: ${eventDetails.eventDateStr}</label><br/>
	<label>Event Time &nbsp;&nbsp;&nbsp;&nbsp;: ${eventDetails.eventTimeStr}</label><br/>
	<label>Venue      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: ${eventDetails.building},  ${eventDetails.city},  ${eventDetails.state},  ${eventDetails.country}</label><br/><br/>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div style="position: relative">
			<div style="width: 100%;">
				<br />
				<br />
				<div class="grid-header" style="width: 100%">
					<label id="ticketGridHeaderLabel">Sold Ticket</label>
				</div>
				<div id="ticket_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
				<div id="ticket_pager" style="width: 100%; height: 10px;"></div>
			</div>
		</div>
	</div>
</div>

<script>	
	$(window).load(function() {
		pagingInfo = JSON.parse(JSON.stringify(${pagingInfo}));
		var sldTickets = JSON.parse(JSON.stringify(${soldTickets}));
		refreshSoldTicketDetils(sldTickets);
		$('#ticket_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserTicketPreference()'>");
	});
</script>
