<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<%@ page import="com.rtw.tracker.data.TrackerUser" %>
<%@ page import="com.rtw.tracker.data.Role" %>
<html lang="en">
  <head>
    <meta charset="utf-8">
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1"> -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Events, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="../resources/images/favicon.png">

    <title><tiles:insertAttribute name="title"></tiles:insertAttribute></title>
	<tiles:importAttribute name="menu" scope="request"/>
	<tiles:importAttribute name="subMenu" scope="request"/>
	
    <!-- Bootstrap CSS -->    
    <link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../resources/css/font-awesome.min.css" rel="stylesheet" />    
    <!-- full calendar css-->
    <link href="../resources/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="../resources/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
    <!-- easy pie chart-->
    <link href="../resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <!-- owl carousel -->
    <link rel="stylesheet" href="../resources/css/owl.carousel.css" type="text/css">
	<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
	<link rel="stylesheet" href="../resources/css/fullcalendar.css">
	<link href="../resources/css/widgets.css" rel="stylesheet">
    
	<link href="../resources/css/xcharts.min.css" rel=" stylesheet">	
	<link href="../resources/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
	<link href="../resources/css/jquery.alerts.css" rel=" stylesheet">
	<link href="../resources/css/style.css" rel="stylesheet">
    <link href="../resources/css/style-responsive.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->
    
	
    <!-- javascripts -->
    <%-- <script src="../resources/js/jquery.js"></script>
	<script src="../resources/js/jquery-ui-1.10.4.min.js"></script> --%>
    <script src="../resources/js/jquery-1.8.3.min.js"></script>
    <%-- <script type="text/javascript" src="../resources/js/jquery-ui-1.9.2.custom.min.js"></script> --%>
    <!-- bootstrap -->
    <script src="../resources/js/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script src="../resources/js/jquery.scrollTo.min.js"></script>
    <script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>
	<script src="../resources/js/jquery.mobile.custom.min.js"></script>
   
    <!-- jQuery full calendar -->
    <%-- <script src="../resources/js/fullcalendar.min.js"></script>
	<script src="../resources/assets/fullcalendar/fullcalendar/fullcalendar.js"></script> --%>
    <!--script for this page only-->
    <%-- <script src="../resources/js/calendar-custom.js"></script>
	<script src="../resources/js/jquery.rateit.min.js"></script> --%>
    <!-- custom select -->
    <script src="../resources/js/jquery.customSelect.min.js" ></script>
	<!-- <script src="../resources/js/jquery.longclick-min.js" ></script> -->
	<%-- <script src="../resources/assets/chart-master/Chart.js"></script> --%>
    <script src="../resources/js/owl.carousel.js" ></script>
    <!--custome script for all page-->
    <script src="../resources/js/scripts.js"></script>
    <script src="../resources/js/jquery.alerts.js"></script>
		
	
	
    <!-- custom script for this page-->
    <!-- <script src="../js/sparkline-chart.js"></script> -->
    <%-- <script src="../resources/js/easy-pie-chart.js"></script>
	<script src="../resources/js/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="../resources/js/jquery-jvectormap-world-mill-en.js"></script>
	<script src="../resources/js/xcharts.min.js"></script>
	<script src="../resources/js/jquery.autosize.min.js"></script>
	<script src="../resources/js/jquery.placeholder.min.js"></script>
	<script src="../resources/js/gdp-data.js"></script>	
	<script src="../resources/js/morris.min.js"></script>
	<script src="../resources/js/sparklines.js"></script>	
	<script src="../resources/js/charts.js"></script> --%>
	<script src="../resources/js/jquery.slimscroll.min.js"></script>	
	<style>
	.searchText{
		  position: relative;
		  min-height: 1px;
		  max-height : 20px;
		  padding-left: 15px;
		  float: left;
		  width: 13.6666667%;
	}
	</style>
  <script>
	
      //knob
     /*  $(function() {
        $(".knob").knob({
          'draw' : function () { 
            $(this.i).val(this.cv + '%')
          }
        })
      }); */
	
      
      $(document).ready(function() {
		//carousel  
    	  setTimeout(function(){$("div#divLoading").removeClass('show');},3000);
          $("#owl-slider").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true

          });
          
          $(document).ajaxStart(function () {
              $("div#divLoading").addClass('show');
          });

          $(document).ajaxStop(function () {
              $("div#divLoading").removeClass('show');
          });
		  
		  $('.globalSearchControl').keypress(function (event) {
			var keyCode = (event.keyCode ? event.keyCode : event.which);
			 if(keyCode == 13)  // the enter key code
			  {
				 getSearchDataByProductType();
				return false;  
			  }
			});
		  
		    var ctrlDown = false;
	       	var cDown = false; 

		    $(document).keydown(function(e) {
		        if (e.keyCode == 17 || e.keyCode == 91){
		        	ctrlDown = true;
		        }else if(e.keyCode == 67){
		        	cDown = true;
		        }
		        if(ctrlDown && cDown){
		        	copyTextToClipboard();
		        }
		    }).keyup(function(e) {
		        if (e.keyCode == 17 || e.keyCode == 91){
		        	ctrlDown = false;
		        }else if(e.keyCode == 67){
		        	cDown = false;
		        }
		    });
			
          // for toggling check script.js - line number 56
          /* $("#toggleMenu").toggle(function(){
  				$("#container").attr('class','sidebar-closed');
  				$("#main-content").css('margin-left','0px');
  		  },function(){
  				$("#container").attr('class','');
  				$("#main-content").css('margin-left','285px');
  		  }); */
		  <c:if test="${menu == 'events'}">
		$('#events').addClass('active');
		$('#eventsA').addClass('focus');
	</c:if> 
	<c:if test="${menu == 'EventCategoryImage'}">
		$('#EventCategoryImage').find("ul").show();
		$('#EventCategoryImage').addClass('active');
		<c:choose>
			<c:when test="${subMenu == 'artistImages'}">
				$('#artistImagesLi').addClass('active');
				$('#artistImages').addClass('focus');
			</c:when>
			<c:when test="${subMenu == 'grandChildImages'}">
				$('#grandChildImagesLi').addClass('active');
				$('#grandChildImages').addClass('focus');
			</c:when>
			<c:when test="${subMenu == 'fantasyGrandChildImages'}">
				$('#fantasyGrandChildImagesLi').addClass('active');
				$('#fantasyGrandChildImages').addClass('focus');
			</c:when>
			<c:when test="${subMenu == 'loyalFanImages'}">
				$('#loyalFanImagesLi').addClass('active');
				$('#loyalFanImages').addClass('focus');
			</c:when>
			<c:when test="${subMenu == 'childImages'}">
				$('#childImagesLi').addClass('active');
				$('#childImages').addClass('focus');
			</c:when>
			<c:when test="${subMenu == 'parentImages'}">
				$('#parentImagesLi').addClass('active');
				$('#parentImages').addClass('focus');
			</c:when>
		</c:choose>
	</c:if> 
	<c:if test="${menu == 'RewardTheFan'}">
		$('#RewardTheFan').find("ul").show();
		$('#RewardTheFan').addClass('active');
		<c:choose>
			<c:when test="${subMenu == 'popularEvents'}">
				$('#popularEventsLi').addClass('active');
				$('#popularEvents').addClass('focus');
			</c:when>
			<c:when test="${subMenu == 'popularArtist'}">
				$('#popularArtistLi').addClass('active');
				$('#popularArtist').addClass('focus');
			</c:when>
			<c:when test="${subMenu == 'artistSearch'}">
				$('#artistSearchLi').addClass('active');
				$('#artistSearch').addClass('focus');
			</c:when>
			
			<c:when test="${subMenu == 'popularGrandChild'}">
				$('#popularGrandChildLi').addClass('active');
				$('#popularGrandChild').addClass('focus');
			</c:when>
			<c:when test="${subMenu == 'popularVenue'}">
				$('#popularVenueLi').addClass('active');
				$('#popularVenue').addClass('focus');
			</c:when>
			<c:when test="${subMenu == 'cards'}">
				$('#cardsLi').addClass('active');
				$('#cards').addClass('focus');
			</c:when>
			
		</c:choose>
	</c:if>
	<c:if test="${menu == 'CrownJewelEvents'}">
		$('#CrownJewelEvents').find("ul").show();
		$('#CrownJewelEvents').addClass('active');
		<c:choose>
			<c:when test="${subMenu == 'crownJewelEvents'}">
				$('#crownJewelEventsLi').addClass('active');
				$('#crownJewelEvents').addClass('focus');
			</c:when>
			<c:when test="${subMenu == 'crownJewelTeam'}">
				$('#crownJewelTeamLi').addClass('active');
				$('#crownJewelTeam').addClass('focus');
			</c:when>
			<c:when test="${subMenu == 'crownJewelTeamZone'}">
				$('#crownJewelTeamZoneLi').addClass('active');
				$('#crownJewelTeamZone').addClass('focus');
			</c:when>
			<c:when test="${subMenu == 'crownJewelOrder'}">
				$('#crownJewelOrderLi').addClass('active');
				$('#crownJewelOrder').addClass('focus');
			</c:when>
			<c:when test="${subMenu == 'crownJewelFinalTeam'}">
				$('#crownJewelFinaTeamLi').addClass('active');
				$('#crownJewelFinalTeam').addClass('focus');
			</c:when>
			<c:when test="${subMenu == 'crownJewelChild'}">
				$('#crownJewelChildLi').addClass('active');
				$('#crownJewelChild').addClass('focus');
			</c:when>
			<c:when test="${subMenu == 'crownJewelCategoryTeam'}">
			$('#crownJewelCategoryTeamLi').addClass('active');
			$('#crownJewelCategoryTeam').addClass('focus');
		</c:when>
			
		</c:choose>
	</c:if>
	<c:if test="${menu == 'SeatGeek'}">
		$('#SeatGeek').addClass('active');
		$('#SeatGeekA').addClass('focus');
	</c:if> 
	<c:if test="${menu == 'Accounting'}">
		$('#Accounting').find("ul").show();
		$('#Accounting').addClass('active');
		<c:choose>
			<c:when test="${subMenu == 'invoices'}">
				$('#invoicesLi').addClass('active');
				$('#invoices').addClass('focus');
			</c:when>
			<c:when test="${subMenu == 'purchageOrder'}">
				$('#purchageOrderLi').addClass('active');
				$('#purchageOrder').addClass('focus');
			</c:when>
			<c:when test="${subMenu == 'openOrder'}">
				$('#orderLi').addClass('active');
				$('#openOrder').addClass('focus');
			</c:when>
			<c:when test="${subMenu == 'closedPassed'}">
				$('#closedPassedLi').addClass('active');
				$('#closedPassed').addClass('focus');
			</c:when>
		</c:choose>
	</c:if>
	/* <c:if test="${menu == 'deliveries'}">
		$('#deliveries').addClass('active');
		$('#deliveriesA').addClass('focus');
	</c:if> */
	<c:if test="${menu == 'customers'}">
		$('#customers').find("ul").show();
		$('#customers').addClass('active');
		<c:choose>
			<c:when test="${subMenu == 'customer'}">
				$('#customersLi').addClass('active');
				$('#customersA').addClass('focus');
			</c:when>
			<c:when test="${subMenu == 'clientReport'}">
				$('#clientLi').addClass('active');
				$('#clientA').addClass('focus');
			</c:when>
			<c:when test="${subMenu == 'discountCode'}">
				$('#discountCodeLi').addClass('active');
				$('#discountCode').addClass('focus');
			</c:when>
			<c:when test="${subMenu == 'customerPromoOffer'}">
				$('#customerPromoOfferLi').addClass('active');
				$('#customerPromoOffer').addClass('focus');
			</c:when>
		</c:choose>
	</c:if> 
	<c:if test="${menu == 'affiliatesandbrokers'}">
		$('#affiliatesandbrokers').find("ul").show();
		$('#affiliatesandbrokers').addClass('active');
		<c:choose>
			<c:when test="${subMenu == 'affiliateDetails'}">
				$('#affiliateUserLi').addClass('active');
				$('#affiliateDetails').addClass('focus');
			</c:when>
			<c:when test="${subMenu == 'manageBrokers'}">
				$('#brokersLi').addClass('active');
				$('#manageBrokers').addClass('focus');
			</c:when>
		</c:choose>
	</c:if>
	<c:if test="${menu == 'webTracking'}">
		$('#webTracking').addClass('active');
		$('#webTrackingA').addClass('focus');
	</c:if> 
	<c:if test="${menu == 'reports'}">
		$('#reports').addClass('active');
		$('#reportsA').addClass('focus');
	</c:if> 
	<c:if test="${menu == 'admin'}">
		$('#admin').find("ul").show();
		$('#admin').addClass('active');
		<c:choose>
			<c:when test="${subMenu == 'manageUsers'}">
				$('#usersLi').addClass('active');
				$('#users').addClass('focus');
			</c:when>
			<c:when test="${subMenu == 'paypalTransactions'}">
				$('#paypalLi').addClass('active');
				$('#paypal').addClass('focus');
			</c:when>
		</c:choose>
	</c:if>
	<c:if test="${menu == 'affiliate'}">
		$('#affiliate').find("ul").show();
		$('#affiliate').addClass('active');
		<c:choose>
			<c:when test="${subMenu == 'affiliateReport'}">
				$('#affiliateLi').addClass('active');
				$('#affiliateReport').addClass('focus');
			</c:when>
		</c:choose>
	</c:if>
          
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });
	  
	  /* ---------- Map ---------- */
	/* $(function(){
	  $('#map').vectorMap({
	    map: 'world_mill_en',
	    series: {
	      regions: [{
	        values: gdpData,
	        scale: ['#000', '#000'],
	        normalizeFunction: 'polynomial'
	      }]
	    },
		backgroundColor: '#eef3f7',
	    onLabelShow: function(e, el, code){
	      el.html(el.html()+' (GDP - '+gdpData[code]+')');
	    }
	  });
	}); */
	
	//Function to highlight the selected menu
	//when page reloads
	function saveUserPreference(gridName,colStr){
		$.ajax({
			url : "${pageContext.request.contextPath}/SaveUserPreference",
			type : "post",
			data : "columns="+colStr+"&gridName="+gridName,
			dataType:"text",
			success : function(res){
				if(res=='OK'){
					jAlert("Grid preference updated successfully.");
				}else{
					jAlert("There is something wrong. Please try again");
				}
			}, error : function(error){
				jAlert("There is something wrong. Please try again"+error,"Error");
				return false;
			}
		});
	}
	function clearAllSelections(){
		var eventTable = document.getElementsByClassName('slick-cell-checkboxsel');
		for(var j=0;j<eventTable.length;j++){
			var eventCheckbox = eventTable[j].getElementsByTagName('input');
			for ( var i = 0; i < eventCheckbox.length; i++) {
				if(eventCheckbox[i].type =='checkbox'){
					eventCheckbox[i].checked = false;
				}
			}
		}
		
	}
	
	function copyTextToClipboard(){
		var text = $('.slick-cell.active').html()
		var textArea = document.createElement("textarea");

	    // Place in top-left corner of screen regardless of scroll position.
	    textArea.style.position = 'fixed';
	    textArea.style.top = 0;
	    textArea.style.left = 0;

	    // Ensure it has a small width and height. Setting to 1px / 1em
	    // doesn't work as this gives a negative w/h on some browsers.
	    textArea.style.width = '2em';
	    textArea.style.height = '2em';

	    // We don't need padding, reducing the size if it does flash render.
	    textArea.style.padding = 0;

	    // Clean up any borders.
	    textArea.style.border = 'none';
	    textArea.style.outline = 'none';
	    textArea.style.boxShadow = 'none';

	    // Avoid flash of white box if rendered for any reason.
	    textArea.style.background = 'transparent';

	    textArea.value = text;

	    document.body.appendChild(textArea);

	    textArea.select();

	    try {
	        var successful = document.execCommand('copy');
	        var msg = successful ? 'successful' : 'unsuccessful';
	        console.log('Copying text command was ' + msg);
	    } catch (err) {
	        console.log('Oops, unable to copy');
	    }

	    document.body.removeChild(textArea);
	}
	
	function commonDateComparator(x,y){
		 var aa = x.split('/');
		 var a = aa[2];
		 a += aa[0];
		 a += aa[1];
		 
		 var bb = y.split('/');
		 var b = bb[2];
		 b += bb[0];
		 b += bb[1];
	    return a < b ? -1 : (a > b ? 1 : 0);
	}
	
	function enableMenu(){
		
	}
	
	function changeProductType(){
		$.ajax({
			url : "${pageContext.request.contextPath}/SetGlobalProduct",
			type : "post",
			data : "productType="+$('#globalProductType').val(),
			dataType:"text",
			success : function(res){
				if(res=='OK'){
					location.reload();
				}
			}, error : function(error){
				jAlert("There is something wrong. Please try again"+error,"Error");
				return false;
			}
		});
	}
	
	function changeBroker(){
		$.ajax({
			url : "${pageContext.request.contextPath}/SetGlobalBroker",
			type : "post",
			data : "brokerId="+$('#broker').val(),
			dataType:"text",
			success : function(res){
				if(res=='OK'){
					location.reload();
				}
			}, error : function(error){
				jAlert("There is something wrong. Please try again"+error,"Error");
				return false;
			}
		});
	}
	
	
	function getSearchDataByProductType(){
		var invoiceNo = $('#invoiceNo').val();
		var orderNo = $('#orderNo').val();
		var poNo = $('#poNo').val();
		var customerName = $('#customerName').val();
		
		if(invoiceNo != null && invoiceNo != ''){
			window.location = "${pageContext.request.contextPath}/Accounting/Invoices?action=search&invoiceNo="+ invoiceNo;
		}
		if(orderNo != null && orderNo != ''){
			window.location = "${pageContext.request.contextPath}/Accounting/Invoices?action=search&orderNo="+ orderNo;
		}
		if(poNo != null && poNo != ''){
			window.location = "${pageContext.request.contextPath}/Accounting/ManagePO?poNo="+poNo;
		}
		if(customerName != null && customerName != ''){
			window.location = "${pageContext.request.contextPath}/Client/ManageDetails?customerName="+customerName;
		}
	}
	
  </script>
 
    
  </head>
  <body >
  <!-- container section start -->
  <section id="menuContainer" class="ABCD1">
           
      <header class="header blue-bg">
      <!-- check script.js line no - 56 to change toggle event -->
			<div class="toggle-logo">
				<div class="toggle-nav">
					<div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i id="toggleMenu" class="icon_menu"></i></div>
				</div>

				<!--logo start-->
				<a href="${pageContext.request.contextPath}/Dashboard" class="logo">
					<!-- Ticket <span class="MyLogolite">Fulfillment</span> -->
					<img alt="" src="../resources/images/ticket-fulfillment-logo.png">
				</a>
				<!--logo end-->
			</div>
			<div class="form-group account-div">
				<!-- <div class="top-nav notification-row"> -->                
				<!-- notificatoin dropdown start-->
				<ul class="nav pull-right top-menu">
					
					
					<!-- user login dropdown start-->
					<li class="dropdown">
						<a data-toggle="dropdown" class="dropdown-toggle" href="#">
							<span class="profile-ava">
								<img width = "25px" height = "25px" alt="" src="../resources/images/userIcon.png">
							</span>
							<span class="username" style="font-size:8pt;">
							<% TrackerUser trackerUser = (TrackerUser)request.getSession().getAttribute("trackerUser");
								String userDisplayName = "";
								//boolean isUser = true;
								if(trackerUser != null){
									userDisplayName = trackerUser.getFirstName().substring(0, 1).toUpperCase() + trackerUser.getFirstName().substring(1).toLowerCase() + " " +
										trackerUser.getLastName().substring(0, 1).toUpperCase() + trackerUser.getLastName().substring(1).toLowerCase();
									
								}%>
								<%= userDisplayName %>
							</span>
							<b class="caret"></b>
						</a>
						<ul class="dropdown-menu logout">
							<div class="log-arrow-up"></div>
							<!--<li class="eborder-top">
								<a href="#"><i class="icon_profile"></i> My Profile</a>
							</li>
							<li>
								<a href="#"><i class="icon_mail_alt"></i> My Inbox</a>
							</li>
							<li>
								<a href="#"><i class="icon_clock_alt"></i> Timeline</a>
							</li>
							<li>
								<a href="#"><i class="icon_chat_alt"></i> Chats</a>
							</li>
							<li>
								<a href="login.html"><i class="icon_key_alt"></i> Log Out</a>
							</li>
							<li>
								<a href="documentation.html"><i class="icon_key_alt"></i> Documentation</a>
							</li>-->
							<li>
								<a href="${pageContext.request.contextPath}/Logout"><i class="icon_key_alt"></i> Logout</a>
							</li>
						</ul>
					</li>
					<!-- user login dropdown end -->
				</ul>
				<!-- notificatoin dropdown end-->
			</div>
			<c:if test="${sessionScope.isAdmin == true || sessionScope.isBroker == true}">
            <div class="filter-form">
				<form name="searchForm" id="searchForm">
					<div class="form-group">
						<!--<label for="name" class="control-label">Order No</label> -->
						<input class="form-control globalSearchControl" placeholder="Order" type="text" id="orderNo" name="orderNo">
					</div>
					<div class="form-group">
						<!--<label for="name" class="control-label">Invoice No</label> -->
						<input class="form-control globalSearchControl" placeholder="Invoice" type="text" id="invoiceNo" name="invoiceNo">
					</div>
					
					<div class="form-group">
						<!--<label for="name" class="control-label">PurchaseOrder No</label> -->
						<input class="form-control globalSearchControl" placeholder="Po. No" type="text" id="poNo" name="poNo">
					</div>
					<div class="form-group">
						<!--<label for="name" class="control-label">Customer ID</label> -->
						<input class="form-control globalSearchControl" placeholder="Customer Name" type="text" id="customerName" name="customerName">
					</div>
					<c:if test="${sessionScope.isAdmin == true}">
						<div class="form-group">
							<select id="globalProductType" name="globalProductType" class="form-control" onchange="changeProductType();">	
								<option value="">--Product Type--</option>			
								<option value="REWARDTHEFAN" <c:if test="${sessionScope.productType ne null and sessionScope.productType eq 'REWARDTHEFAN'}">Selected</c:if>>Reward The Fan</option>
								<option value="RTW" <c:if test="${sessionScope.productType ne null and sessionScope.productType eq 'RTW'}">Selected</c:if>>RTW</option>
								<option value="RTW2" <c:if test="${sessionScope.productType ne null and sessionScope.productType eq 'RTW2'}">Selected</c:if>>RTW2</option>
								<option value="SEATGEEK" <c:if test="${sessionScope.productType ne null and sessionScope.productType eq 'SEATGEEK'}">Selected</c:if>>SEATGEEK</option>
							</select>
						</div>
					
						<div class="form-group">
							<!--<label for="name" class="control-label">Company Product</label> -->
							<select id="broker" name="broker" class="form-control" onchange="changeBroker();">	
								<!-- <option value="-1">--Broker--</option>	 -->		
								<c:forEach var="broker" items="${brokers}">
									<option value="${broker.id}" <c:if test="${sessionScope.brokerId ne null and sessionScope.brokerId eq broker.id}">Selected</c:if>>
										<c:out value="${broker.companyName}" /> - <c:out value="${broker.id}" />
									</option>
								</c:forEach>
							</select>
						</div>
					</c:if>
					<div class="button-search">
						<button id="searchInvoiceBtn" class="btn btn-primary" onClick="getSearchDataByProductType();" type="button">Search</button>
					</div>
				</form>
				
			</div>
			</c:if>
			<!-- </div> -->
			
            
      </header> 
      <!--header end-->

      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" >  
              	  <c:if test="${sessionScope.isUser == true || sessionScope.isBroker == true || sessionScope.isAdmin == true}">              
	                  <li id="events">
	                      <a href="${pageContext.request.contextPath}/Events" >
	                          <i class="icon_house_alt"></i>
	                          <span>Events</span>
	                      </a>
	                  </li>
                  </c:if>
                  <c:if test="${sessionScope.isUser == true || sessionScope.isAdmin == true}">
	                   <li id="EventCategoryImage" class="sub-menu" >
	                      <a href="javascript:;">
	                          <i class="icon_document_alt"></i>
	                          <span><b>Images</b></span>
	                          <span class="menu-arrow arrow_carrot-right"></span>
	                      </a>
	                      <ul class="sub" <c:if test="${menu == 'EventCategoryImage'}">style="display:block"</c:if> >
	                          <li id="artistImagesLi"><a id="artistImages" href="${pageContext.request.contextPath}/ArtistImages" >Artist/Team</a></li>
	                         
							 <li id="grandChildImagesLi"><a id="grandChildImages" href="${pageContext.request.contextPath}/GrandChildCategoryImages">Grand Child</a></li>
	                          
							  <li id="childImagesLi"><a id="childImages" href="${pageContext.request.contextPath}/ChildCategoryImages">Child</a></li>
	                          
							  <li id="parentImagesLi"><a id="parentImages" href="${pageContext.request.contextPath}/ParentCategoryImages">Parent</a></li>
							  <li id="fantasyGrandChildImagesLi"><a id="fantasyGrandChildImages" href="${pageContext.request.contextPath}/FantasyGrandChildCategoryImages">Fantasy Grand Child</a></li>
							  <li id="loyalFanImagesLi"><a id="loyalFanImages" href="${pageContext.request.contextPath}/LoyalFanParentCategoryImages">Loyal Fan Image</a></li>
	                      </ul>
	                  </li>
                  </c:if>
                  <c:if test="${sessionScope.isUser == true || sessionScope.isAdmin == true}">
					  <li id="RewardTheFan" class="sub-menu" >
	                      <a href="javascript:;">
	                          <i class="icon_document_alt"></i>
	                          <span><b>Reward The Fan</b></span>
	                          <span class="menu-arrow arrow_carrot-right"></span>
	                      </a>
	                      <ul class="sub" <c:if test="${menu == 'RewardTheFan'}">style="display:block"</c:if> >
	                          <%-- <li id="popularEventsLi"><a  id="popularEvents" href="${pageContext.request.contextPath}/RewardTheFan/PopularEvents" >Popular Events</a></li> --%>
	                          
							  <li id="popularArtistLi"><a  id="popularArtist" href="${pageContext.request.contextPath}/RewardTheFan/PopularArtist" >Popular Artist</a></li>
	                          <li id="artistSearchLi"><a  id="artistSearch" href="${pageContext.request.contextPath}/RewardTheFan/ManageArtistForSearch" >Manage Artist Search</a></li>
							 
							 <%-- <li id="popularGrandChildLi"><a  id="popularGrandChild" href="${pageContext.request.contextPath}/RewardTheFan/PopularGrandChildCategory" >Popular GrandChild</a></li>	                          
							  <li id="popularVenueLi"><a  id="popularVenue" href="${pageContext.request.contextPath}/RewardTheFan/PopularVenue">Popular Venue</a></li> --%>
	                          
							  <li <c:if test="${subMenu == 'cards'}">class="active"</c:if>><a  id="cards" href="${pageContext.request.contextPath}/RewardTheFan/Cards" <c:if test="${subMenu == 'cards'}">class="focus"</c:if>>Cards</a></li>
	                		</ul>
	                  </li>   
                  </c:if>  
                   
                  <%-- <li id="Presale" class="sub-menu">
                      <a href="javascript:;" class="">
                          <i class="icon_table"></i>
                          <span><b>Presale Zone Tickets</b></span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
                      <ul class="sub">
                      	  <li><a class="${cssClass}" id="pztPopularEvents" href="${pageContext.request.contextPath}/Presale/PopularEvents">Popular Events</a></li>
                          <li><a class="${cssClass}" id="pztPopularArtist" href="${pageContext.request.contextPath}/Presale/PopularArtist">Popular Artist</a></li>
                          <li><a class="${cssClass}" id="pztPopularGrandChild" href="${pageContext.request.contextPath}/Presale/PopularGrandChildCategory">Popular GrandChild</a></li>
                          <li><a class="${cssClass}" id="pztPopularVenue" href="${pageContext.request.contextPath}/Presale/PopularVenue">Popular Venue</a></li>
                           <li><a class="${cssClass}" id="pztPopularCards" href="${pageContext.request.contextPath}/Presale/Cards">Cards</a></li>
                      </ul>
                  </li> --%>
                  
                 <%--  <li id="CategoryTickets" class="sub-menu">
                      <a class="" href="javascript:;">
                          <i class="icon_genius"></i>
                          <span><b>Category Tickets</b></span>
						  <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
					  <ul class="sub">
					   <li><a class="${cssClass}" id="ctPopularEvents" href="${pageContext.request.contextPath}/CategoryTickets/PopularEvents">Popular Events</a></li>
                          <li><a class="${cssClass}" id="ctPopularArtist" href="${pageContext.request.contextPath}/CategoryTickets/PopularArtist">Popular Artist</a></li>
                          <li><a class="${cssClass}" id="ctPopularGrandChild" href="${pageContext.request.contextPath}/CategoryTickets/PopularGrandChildCategory">Popular GrandChild</a></li>
                          <li><a class="${cssClass}" id="ctPopularVenue" href="${pageContext.request.contextPath}/CategoryTickets/PopularVenue">Popular Venue</a></li>
                          <li><a class="${cssClass}" id="ctPopularCards" href="${pageContext.request.contextPath}/CategoryTickets/Cards">Cards</a></li>
                      </ul>
                  </li> --%>
                    
                  <%-- <li id="FirstTenRows" class="sub-menu">
                      <a href="javascript:;" class="">
                          <i class="icon_desktop"></i>
                          <span><b>First Ten Rows</b></span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
                      <ul class="sub">
                    	  <li><a class="${cssClass}" id="ftrPopularEvents" href="${pageContext.request.contextPath}/FirstTenRows/PopularEvents">Popular Events</a></li>
                          <li><a class="${cssClass}" id="ftrPopularArtist" href="${pageContext.request.contextPath}/FirstTenRows/PopularArtist">Popular Artist</a></li>
                          <li><a class="${cssClass}" id="ftrPopularGrandChild" href="${pageContext.request.contextPath}/FirstTenRows/PopularGrandChildCategory">Popular GrandChild</a></li>
                          <li><a class="${cssClass}" id="ftrPopularVenue" href="${pageContext.request.contextPath}/FirstTenRows/PopularVenue">Popular Venue</a></li>
                          <li><a class="${cssClass}" id="ftrPopularCards" href="${pageContext.request.contextPath}/FirstTenRows/Cards">Cards</a></li>
                          
                      </ul>
                  </li> --%>
                  
                 <%--  <li id="LastRowTickets" class="sub-menu">                     
                      <a class="" href="javascript:;">
                          <i class="icon_piechart"></i>
                          <span><b>Last Row Tickets</b></span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
                        <ul class="sub">
                         <li><a class="${cssClass}" id="ftrPopularEvents" href="${pageContext.request.contextPath}/LastRowTickets/PopularEvents">Popular Events</a></li>
                          <li><a class="${cssClass}" id="ftrPopularArtist" href="${pageContext.request.contextPath}/LastRowTickets/PopularArtist">Popular Artist</a></li>
                          <li><a class="${cssClass}" id="ftrPopularGrandChild" href="${pageContext.request.contextPath}/LastRowTickets/PopularGrandChildCategory">Popular GrandChild</a></li>
                          <li><a class="${cssClass}" id="ftrPopularVenue" href="${pageContext.request.contextPath}/LastRowTickets/PopularVenue">Popular Venue</a></li>
                          <li><a class="${cssClass}" id="ftrPopularCards" href="${pageContext.request.contextPath}/LastRowTickets/Cards">Cards</a></li>
                      </ul>             
                  </li> --%>
                  <c:if test="${sessionScope.isAdmin == true || sessionScope.isUser == true}">
	                  <li id="CrownJewelEvents" class="sub-menu">                     
	                      <a href="javascript:;">
	                          <i class="icon_piechart"></i>
	                          <span><b>Fantasy Sports Tickets</b></span>
	                          <span class="menu-arrow arrow_carrot-right"></span>
	                      </a>
	                        <ul class="sub" <c:if test="${menu == 'CrownJewelEvents'}">style="display:block"</c:if>>
	                         <li id="crownJewelEventsLi"><a id="crownJewelEvents" href="${pageContext.request.contextPath}/CrownJewelEvents/CrownJewelEvents" >Fantasy Sports Tickets</a></li>
							 <li id="crownJewelTeamLi"><a id="crownJewelTeam" href="${pageContext.request.contextPath}/CrownJewelEvents/CrownJewelTeams" >Fantasy Sports Tickets Team</a></li>
							 <li id="crownJewelTeamZoneLi" style="width:102%;"><a id="crownJewelTeamZone" href="${pageContext.request.contextPath}/CrownJewelEvents/CrownJewelTeamZones" >Fantasy Sports Tickets Zones</a></li>
							  <li id="crownJewelFinaTeamLi"><a id="crownJewelFinalTeam" href="${pageContext.request.contextPath}/CrownJewelEvents/CrownJewelFinalTeam" >Fantasy Sports Tickets Finals</a></li>
							  <li id="crownJewelChildLi"><a id="crownJewelChild" href="${pageContext.request.contextPath}/CrownJewelEvents/CrownJewelChild" >Fantasy Categories</a></li>
							  <li id="crownJewelCategoryTeamLi"><a id="crownJewelCategoryTeam" href="${pageContext.request.contextPath}/CrownJewelEvents/CrownJewelCategoryTeams" >Fantasy Teams</a></li>
							  <li id="crownJewelOrderLi"><a id="crownJewelOrder" href="${pageContext.request.contextPath}/CrownJewelEvents/CrownJewelOrders?status=Outstanding" >Fantasy Orders</a></li>
	                      </ul> 
	                  </li>
	                </c:if>
	                <c:if test="${sessionScope.isAdmin == true}">
		                 <li id="SeatGeek">
		                      <a id="SeatGeekA" href="${pageContext.request.contextPath}/SeatGeek/SgOpenOrders">
		                          <i class="icon_documents_alt"></i>
		                          <span><b>SeatGeek</b></span>
		                      </a>
		                  </li>
                  	</c:if>
                  	<c:if test="${sessionScope.isBroker == true || sessionScope.isAdmin == true}">
	                  <li id="Accounting" class="sub-menu">                     
	                      <a  href="javascript:;">
	                          <i class="icon_currency"></i>
	                          <span><b>Order Management</b></span>
	                          <span class="menu-arrow arrow_carrot-right"></span>
	                      </a>
	                        <ul class="sub" <c:if test="${menu == 'Accounting'}">style="display:block"</c:if>>
	                        <li id="invoicesLi"><a  id="invoices" href="${pageContext.request.contextPath}/Accounting/Invoices?status=Outstanding" >Invoices</a></li>
							<li id="purchageOrderLi"><a  id="purchageOrder" href="${pageContext.request.contextPath}/Accounting/ManagePO" >Purchase Order</a></li>
	                      	<li id="orderLi"> <a id="openOrder" href="${pageContext.request.contextPath}/Deliveries/OpenOrders">Open Order & Status</a></li>
	                      	<li id="closedPassedLi"> <a id="closedPassed" href="${pageContext.request.contextPath}/Deliveries/ClosedOrders">Closed & Past Orders</a></li>
	                      </ul> 
	                  </li>
                  	</c:if>
                  <%-- <li id="Deliveries" class="sub-menu">                     
                      <a href="javascript:;">
                          <i class="icon_bag"></i>
                          <span><b>Deliveries</b></span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
                        <ul class="sub" <c:if test="${menu == 'Deliveries'}">style="display:block"</c:if>>
                          <li id="ordersLi"><a  id="orders" href="${pageContext.request.contextPath}/Deliveries/OpenOrders">Orders</a></li>
						  <li id="shipmentOrdersLi" ><a id="shipmentOrders" href="${pageContext.request.contextPath}/Deliveries/PendingShipmentOrders" >Shipment Pendings</a></li>
						  <li id="closedOrdersLi"><a id="closedOrders" href="${pageContext.request.contextPath}/Deliveries/ClosedOrders">Closed Orders</a></li>
                      </ul> 
                  </li> --%>
                  
                  <%-- <li id="deliveries" >
                      <a id="deliveriesA" href="${pageContext.request.contextPath}/Deliveries/OpenOrders">
                          <i class="icon_bag"></i>
                          <span><b>Orders</b></span>
                      </a>
                  </li> --%>
                                      
                        <%--   <a href="${pageContext.request.contextPath}/Client/ManageDetails" class=""></a>
                          <i class="icon_documents_alt"></i>
                          <span><b>Customers</b></span> --%>
                         <!--  <span class="menu-arrow arrow_carrot-right"></span> -->
                      <%--   <ul class="sub">
                          <li><a class="" href="${pageContext.request.contextPath}/Client/AddClient">Client</a></li>                          
                          <li><a class="" href="${pageContext.request.contextPath}/Client/AddClientBroker">Client Broker</a></li>
                          <li><a class="" href="${pageContext.request.contextPath}/Client/AddCustomer">Customer</a></li>
                          <li><a class="" href="${pageContext.request.contextPath}/Client/ManageDetails">Manage Details</a></li>
                      </ul>  --%>  
                  <c:if test="${sessionScope.isAdmin == true || sessionScope.isUser == true || sessionScope.isBroker == true}">
	                  <li id="customers" class="sub-menu">
	                  	  <a  href="javascript:;">
	                          <i class="icon_documents_alt"></i>
	                          <span><b>Manage Customers</b></span>
	                          <span class="menu-arrow arrow_carrot-right"></span>
	                      </a>
	                      <ul class="sub" <c:if test="${menu == 'customers'}">style="display:block"</c:if>>
	                        <li id="customersLi"><a id="customersA" href="${pageContext.request.contextPath}/Client/ManageDetails">Customers</a></li>	                        
	                        <c:if test="${sessionScope.isAdmin == true || sessionScope.isUser == true}">
	                        	<li id="clientLi"><a id="clientA" href="${pageContext.request.contextPath}/Admin/CleanedClient">Email Blast Mangement</a></li>
	                        	<li id="discountCodeLi"><a id="discountCode" href="${pageContext.request.contextPath}/Client/GenerateDiscountCode" >Generate Discount Code</a></li>
	                        	<li id="customerPromoOfferLi"><a id="customerPromoOffer" href="${pageContext.request.contextPath}/Client/CustomerPromotionalOffer" >Customer Promotional Offer</a></li>
	                      	</c:if>
	                      </ul>
	                  </li>          
                  </c:if>
                  <c:if test="${sessionScope.isAdmin == true || sessionScope.isUser == true}">
					  <li id="affiliatesandbrokers" class="sub-menu">
	                      <a  href="javascript:;">
	                          <i class="icon_currency"></i>
	                          <span><b>Affiliates And Brokers</b></span>
	                          <span class="menu-arrow arrow_carrot-right"></span>
	                      </a>
	                      <ul class="sub" <c:if test="${menu == 'affiliatesandbrokers'}">style="display:block"</c:if>>
	                        <li id="affiliateUserLi"><a id="affiliateDetails" href="${pageContext.request.contextPath}/Affiliates?status=Active">Affiliates</a></li>
	                        <li id="brokersLi"><a id="manageBrokers" href="${pageContext.request.contextPath}/Client/ManageBrokers?status=Active">Brokers</a></li>
	                      </ul> 
	                  </li>                  
                  </c:if>
                  <c:if test="${sessionScope.isAdmin == true}">
	                  <li id="webTracking">                     
	                      <a id="webTrackingA" href="${pageContext.request.contextPath}/Tracking/Home" >
	                          <i class="icon_calculator_alt"></i>
	                          <span><b>Website Tracking</b></span>
	                      </a>
	                        <!-- <ul class="sub">
	                          <li><a class="" href="form_component.html">IP Tracking</a></li>
	                      	</ul>  -->            
	                  </li>
                  </c:if>
                  <c:if test="${sessionScope.isAdmin == true}">
	                  <li id="reports">
	                      <a id="reportsA" href="${pageContext.request.contextPath}/Reports/Home">
	                          <i class="icon_documents_alt"></i>
	                          <span><b>Reports</b></span>
	                      </a>
	                  </li>
                  </c:if>
				  <c:if test="${sessionScope.isAdmin == true}">
					  <li id="admin" class="sub-menu">
	                      <a  href="javascript:;">
	                          <i class="icon_currency"></i>
	                          <span><b>Admin</b></span>
	                          <span class="menu-arrow arrow_carrot-right"></span>
	                      </a>
	                      <ul class="sub" <c:if test="${menu == 'admin'}">style="display:block"</c:if>>
	                        <li id="usersLi"><a id="users" href="${pageContext.request.contextPath}/Admin/ManageUsers?status=Active">Manage Users</a></li>
	                        <li id="paypalLi"><a id="paypal" href="${pageContext.request.contextPath}/Admin/PaypalTracking" >Paypal Transactions</a></li>
	                      </ul> 
	                  </li>                  
                  </c:if>
                  <c:if test="${sessionScope.isAffiliates == true}">
					  <li id="affiliate" class="sub-menu">
	                      <a  href="javascript:;">
	                          <i class="icon_currency"></i>
	                          <span><b>Affiliate</b></span>
	                          <span class="menu-arrow arrow_carrot-right"></span>
	                      </a>
	                      <ul class="sub" <c:if test="${menu == 'affiliate'}">style="display:block"</c:if>>
	                        <li id="affiliateLi"><a id="affiliateReport" href="${pageContext.request.contextPath}/AffiliateReport">Affiliate Report</a></li>
	                      </ul> 
	                  </li>                  
                  </c:if>
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!--main content start-->
      
      <section id="main-content">
          <section class="wrapper">
          	<tiles:insertAttribute name="body"></tiles:insertAttribute>
          </section>
          <div id="divLoading"></div>
      </section>
      
      <!--main content end-->
  </section>
  <!-- container section start -->
  </body>
</html>
