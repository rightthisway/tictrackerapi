<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
    <link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../resources/css/font-awesome.min.css" rel="stylesheet" />    
    <!-- Custom styles -->
	<link href="../resources/css/widgets.css" rel="stylesheet">
    <link href="../resources/css/style.css" rel="stylesheet">
    <link href="../resources/css/style-responsive.css" rel="stylesheet" />
    <link href="../resources/css/jquery.alerts.css" rel=" stylesheet">
    
	<script src="../resources/js/jquery-1.8.3.min.js"></script>
	<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
	<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
    <script src="../resources/js/bootstrap.min.js"></script>
    <script src="../resources/js/jquery.scrollTo.min.js"></script>
    <script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    
<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>
  <link href="../resources/css/datepicker.css" rel="stylesheet">
  
  <script src="../resources/js/slick/lib/firebugx.js"></script>
<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>
<script src="../resources/js/jquery.alerts.js"></script>

<script>

window.onresize = function(){
	ctdGrid.resizeCanvas();
}
</script>

<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i> Accounting
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Invoice</a>
			</li>
			<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Download Ticket Detail</li>
		</ol>
	</div>
</div>
<br />

<c:if test="${successMessage != null}">
	<div class="alert alert-success fade in">
		<strong
			style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${successMessage}</strong>
	</div>
</c:if>
<c:if test="${errorMessage != null}">
	<div class="alert alert-block alert-danger fade in">
		<strong
			style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${errorMessage}</strong>
	</div>
</c:if>

<div style="position: relative" id="ctd">
	<div style="width: 100%;">
		<div class="grid-header" style="width: 100%">
			<label>Ticket Download Informations</label> <span id="openOrder_grid_toogle_search" style="float: right"
				class="ui-icon ui-icon-search" title="Toggle search panel" onclick="openOrderGridToggleFilterRow()"></span>
		</div>
		<div id="ctd_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
		<div id="ctd_pager" style="width: 100%; height: 10px;"></div>
	</div>
</div>
<br/><br/>

<div id="event_inlineFilterPanel" style="display: none; background: #dddddd; padding: 3px; color: black;">
				Show records with CTD <input type="text" id="openOrderGridSearch">
</div>
<script>
var selectedRow='';
var id='';
var ctdDataView;
var pagingInfo=JSON.parse(JSON.stringify(${pagingInfo}));
var ctdGrid;
var ticketGroupId="${invoiceId}";
var ctdData = [];
var ctdColumns = [ {
	id : "id",
	name : "ID",
	field : "id",
	sortable : true
},{
	id : "invoiceId",
	name : "Invoice Id",
	field : "invoiceId",
	sortable : true
},{
	id : "customerId",
	name : "Customer Id",
	field : "customerId",
	sortable : true
},{
	id : "ticketType",
	name : "Ticket Type",
	field : "ticketType",
	sortable : true
}, {
	id : "ticketName",
	name : "Ticket Name",
	field : "ticketName",
	width:80,
	sortable : true
},{
	id : "email",
	name : "E-Mail",
	field : "email",
	width:80,
	sortable : true
},{
	id : "ipAddress",
	name : "IP Address",
	field : "ipAddress",
	width:80,
	sortable : true
},{
	id : "downloadPlatform",
	name : "Download Platform",
	field : "downloadPlatform",
	sortable : true
},{
	id : "downloadDateTime",
	name : "Download Date",
	field : "downloadDateTime",
	sortable : true
}];

var ctdOptions = {
	enableCellNavigation : true,
	forceFitColumns : true,
	multiSelect : false,
	topPanelHeight : 25,
};
	
var ctdSortcol = "id";
var ctdSortdir = 1;
var ctdSearchString = "";
var percentCompleteThreshold = 0;

function ctdFilter(item, args) {
	var x= item["id"];
	if (args.ctdSearchString  != ""
			&& x.indexOf(args.ctdSearchString) == -1) {
		
		if (typeof x === 'string' || x instanceof String) {
			if(x.toLowerCase().indexOf(args.ctdSearchString.toLowerCase()) == -1) {
				return false;
			}
		} else {
			return false;
		}
	}
	return true;
}

function ctdComparer(a, b) {
	var x = a[ctdSortcol], y = b[ctdSortcol];
	if(!isNaN(x)){
	   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	}
	if(x == '' || x == null) {
		return 1;
	} else if(y == '' || y == null) {
		return -1;
	}
	if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
		return x.toLowerCase().localeCompare(y.toLowerCase());	
	} else {
		return (x == y ? 0 : (x > y ? 1 : -1));	
	}
}

function openOrderGridToggleFilterRow() {
		ctdGrid.setTopPanelVisibility(!ctdGrid.getOptions().showTopPanel);
	}

//$(".grid-header .ui-icon").addClass("ui-state-default-sg ui-corner-all")
	$("#openOrder_grid_toogle_search").addClass(
			"ui-state-default-sg ui-corner-all").mouseover(function(e) {
		$(e.target).addClass("ui-state-hover");
	}).mouseout(function(e) {
		$(e.target).removeClass("ui-state-hover");
	});
	
function refreshctdGridValues() {
		var i = 0;
		<c:forEach var="ctd" items="${ticketDownload}">
			var d = (ctdData[i] = {});
			d["id"] = '${ctd.id}';			
			d["invoiceId"] = '${ctd.invoiceId}';
			d["customerId"] = '${ctd.customerId}';
			d["ticketType"] = '${ctd.ticketType}';
			d["ticketName"] = '${ctd.ticketName}';
			d["email"] = '${ctd.email}';
			d["ipAddress"] = '${ctd.ipAddress}';
			d["downloadPlatform"] = '${ctd.downloadPlatform}';
			d["downloadDateTime"] = '${ctd.downloadDateTime}';
			i++;
		</c:forEach>
	
			
		ctdDataView = new Slick.Data.DataView({
			inlineFilters : true
		});
		ctdGrid = new Slick.Grid("#ctd_grid", ctdDataView, ctdColumns, ctdOptions);
		ctdGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		ctdGrid.setSelectionModel(new Slick.RowSelectionModel());		
		var ctdPager = new Slick.Controls.Pager(ctdDataView, ctdGrid, $("#ctd_pager"),pagingInfo);
		var ctdColumnpicker = new Slick.Controls.ColumnPicker(ctdColumns, ctdGrid,
			ctdOptions);
		
		// move the filter panel defined in a hidden div into eventGrid top panel
		$("#event_inlineFilterPanel").appendTo(ctdGrid.getTopPanel()).show();
		
		ctdGrid.onSort.subscribe(function(e, args) {
		ctdSortdir = args.sortAsc ? 1 : -1;
		ctdSortcol = args.sortCol.field;
		if ($.browser.msie && $.browser.version <= 8) {
			ctdDataView.fastSort(ctdSortcol, args.sortAsc);
		} else {
			ctdDataView.sort(ctdComparer, args.sortAsc);
		}
	});
	// wire up model events to drive the eventGrid
	ctdDataView.onRowCountChanged.subscribe(function(e, args) {
		ctdGrid.updateRowCount();
		ctdGrid.render();
	});
	ctdDataView.onRowsChanged.subscribe(function(e, args) {
		ctdGrid.invalidateRows(args.rows);
		ctdGrid.render();
	});
	
	// wire up the search textbox to apply the filter to the model
		$("#openOrderGridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			ctdSearchString = this.value;
			updateCTDGridFilter();
		});
		
	function updateCTDGridFilter() {
		ctdDataView.setFilterArgs({
			ctdSearchString : ctdSearchString
		});
		ctdDataView.refresh();
	}
	var eventrowIndex;
	ctdGrid.onSelectedRowsChanged.subscribe(function() {
		var temprEventRowIndex = ctdGrid.getSelectedRows([ 0 ])[0];
			if (temprEventRowIndex != eventrowIndex) {
				eventrowIndex = temprEventRowIndex;
				//getCustomerInfoForInvoice(temprEventRowIndex);
			}
			
			selectedRow = ctdGrid.getSelectedRows([0])[0];
			if (selectedRow >=0) {
				id = ctdGrid.getDataItem(temprEventRowIndex).id;
			}else{
				id='';
			}			
		});

	// initialize the model after all the events have been hooked up
	ctdDataView.beginUpdate();
	ctdDataView.setItems(ctdData);
	
	ctdDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			ctdSearchString : ctdSearchString
		});
	ctdDataView.setFilter(ctdFilter);
		
	ctdDataView.endUpdate();
	ctdDataView.syncGridSelection(ctdGrid, true);
	$("#gridContainer").resizable();
	ctdGrid.resizeCanvas();
	$("div#divLoading").removeClass('show');
	if(ticketGroupId>0){
		for(var i=0;i<ctdGrid.getDataLength();i++){
			if(ctdGrid.getDataItem(i).id==ticketGroupId){
				ctdGrid.setSelectedRows([i]);
				break;
			}
		}
	}
	
}
window.onload = function() {
	refreshctdGridValues();
	
};
</script>