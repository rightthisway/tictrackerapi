<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<!-- Bootstrap CSS -->    
    <link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../resources/css/font-awesome.min.css" rel="stylesheet" />    
    <!-- full calendar css-->
    <link href="../resources/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="../resources/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
    <!-- easy pie chart-->
    <link href="../resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <!-- owl carousel -->
    <link rel="stylesheet" href="../resources/css/owl.carousel.css" type="text/css">
	<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
	<link rel="stylesheet" href="../resources/css/fullcalendar.css">
	<link href="../resources/css/widgets.css" rel="stylesheet">
    <link href="../resources/css/style.css" rel="stylesheet">
    <link href="../resources/css/style-responsive.css" rel="stylesheet" />
	<link href="../resources/css/xcharts.min.css" rel=" stylesheet">	
	<link href="../resources/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
	
	<link href="../resources/css/jquery.alerts.css" rel=" stylesheet">
    
	<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>
	
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->
    
    <!-- javascripts -->
    <%-- <script src="../resources/js/jquery.js"></script>
	<script src="../resources/js/jquery-ui-1.10.4.min.js"></script> --%>
    <script src="../resources/js/jquery-1.8.3.min.js"></script>
    <%-- <script type="text/javascript" src="../resources/js/jquery-ui-1.9.2.custom.min.js"></script> --%>
    <!-- bootstrap -->
    <script src="../resources/js/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script src="../resources/js/jquery.scrollTo.min.js"></script>
    <script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../resources/js/jquery.alerts.js"></script>

	<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
	
	<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
	<script src="../resources/js/slick/slick.core.js"></script>
	<script src="../resources/js/slick/slick.formatters.js"></script>
	<script src="../resources/js/slick/slick.editors.js"></script>
	<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
	<script src="../resources/js/slick/slick.grid.js"></script>
	<script src="../resources/js/slick/slick.dataview.js"></script>
	<script src="../resources/js/slick/controls/slick.pager.js"></script>
	<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
	<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
	<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>
	<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<style>
.cell-title {
      font-weight: bold;
    }
    .cell-effort-driven {
      text-align: center;
    }
    .slick-headerrow-column {
     background: #87ceeb;
     text-overflow: clip;
     -moz-box-sizing: border-box;
     box-sizing: border-box;
	}
	.slick-headerrow-column input {
	     margin: 0;
	     padding: 0;
	     width: 100%;
	     height: 100%;
	     -moz-box-sizing: border-box;
	     box-sizing: border-box;
	}
    .cell-selection {
      border-right-color: silver;
      border-right-style: solid;
      background: silver;
      color: gray;
      text-align: right;
      font-size: 10px;
    }
    .slick-row.selected .cell-selection {
      background-color: transparent; /* show default selected row background */
    }
	
</style>
<script>

var pagingInfo;
var rewardPointsGrid;
var rewardPointsDataView;
var rewardPointsData=[];
var rewardPointsSearchString = "";
var columnFilters = {};
var rewardPointsColumn = [ {id:"orderNo", name:"Order No", field: "orderNo", sortable: true},
               {id:"orderDate", name:"Order Date", field: "orderDate", sortable: true},
               {id:"orderType", name:"Order Type", field: "orderType", sortable: true},
               /*{id:"availableDate", name:"Available Date", field: "availableDate", sortable: true},*/
               {id:"rewardPoints", name:"Reward Points", field: "rewardPoints", sortable: true},
               {id:"eventName", name:"Event Name", field: "eventName", sortable: true},
               {id:"eventDate", name:"Event Date", field: "eventDate", sortable: true},
				{id:"eventTime", name:"Event Time", field: "eventTime", sortable: true}			   
              ];
              
var rewardPointsOptions = {	
    enableCellNavigation : true,
	forceFitColumns : true,
	multiSelect: false,
	topPanelHeight : 25,
	showHeaderRow: true,
	headerRowHeight: 30,
	explicitInitialization: true
};

var sortcol = "orderNo";
var sortdir = 1;
var percentCompleteThreshold = 0;

function comparer(a, b) {
	  var x = a[sortcol], y = b[sortcol];
	  if(!isNaN(x)){
	  	return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	  }
	  if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
 
function pagingControl(move,id){		
	var pageNo = 0;
	if(move == 'FIRST'){
		pageNo = 0;
	}else if(move == 'LAST'){
		pageNo = parseInt(pagingInfo.totalPages)-1;
	}else if(move == 'NEXT'){
		pageNo = parseInt(pagingInfo.pageNum) +1;
	}else if(move == 'PREV'){
		pageNo = parseInt(pagingInfo.pageNum)-1;
	}	
	getRewardPointsGridData(pageNo);
}

function getRewardPointsGridData(pageNo) {
		var Message = $('#breakUpMsg').val();
		var customerId = $('#customerId').val();
		$.ajax({
			url : "${pageContext.request.contextPath}/Client/GetRewardPoints",
			type : "post",
			dataType: "json",
			data : "pageNo="+pageNo+"&headerFilter="+rewardPointsSearchString+"&Message="+Message+"&customerId="+customerId,
			success : function(res){
				var jsonData = res;
				if(jsonData==null || jsonData=='') {
					jAlert("No Data Found.");
				}
				pagingInfo = jsonData.pagingInfo;
				createRewardPointsGrid(jsonData.rewardPointsList);
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
function createRewardPointsGrid(jsonData) {	
	rewardPointsData=[];
	var i=0;
	if(jsonData!=null && jsonData.length > 0){
		for (var i = 0; i < jsonData.length; i++) {
			var  data = jsonData[i];
			var d = (rewardPointsData[i] = {});
			d["id"] = i;
			d["orderNo"] = data.orderNo;
			d["orderDate"] = data.orderDate;
			d["orderType"] = data.orderType;
			d["rewardPoints"] = data.rewardPoint;
			d["eventName"] = data.eventName;
			d["eventDate"] = data.eventDate;
			d["eventTime"] = data.eventTime;
		}
	}
					
	rewardPointsDataView = new Slick.Data.DataView();
	rewardPointsGrid = new Slick.Grid("#custRewardPointsGrid", rewardPointsDataView, rewardPointsColumn, rewardPointsOptions);
	rewardPointsGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	rewardPointsGrid.setSelectionModel(new Slick.RowSelectionModel());
	if(pagingInfo != null){
		var shippingGridPager = new Slick.Controls.Pager(rewardPointsDataView, rewardPointsGrid, $("#rewardPoints_pager"),pagingInfo);
	}
	var columnpicker = new Slick.Controls.ColumnPicker(rewardPointsColumn, rewardPointsGrid, rewardPointsOptions);

	  /*
	  rewardPointsGrid.onKeyDown.subscribe(function (e) {
	    // select all rows on ctrl-a
	    if (e.which != 65 || !e.ctrlKey) {
	      return false;
	    }
	    var rows = [];
	    for (var i = 0; i < rewardPointsDataView.getLength(); i++) {
	      rows.push(i);
	    }
	    rewardPointsGrid.setSelectedRows(rows);
	    e.preventDefault();
	  }); */
	  rewardPointsGrid.onSort.subscribe(function (e, args) {
	    sortdir = args.sortAsc ? 1 : -1;
	    sortcol = args.sortCol.field;
	    if ($.browser.msie && $.browser.version <= 8) {
	      rewardPointsDataView.fastSort(sortcol, args.sortAsc);
	    } else {
	      rewardPointsDataView.sort(comparer, args.sortAsc);
	    }
	  });
	  // wire up model events to drive the rewardPointsGrid
	  rewardPointsDataView.onRowCountChanged.subscribe(function (e, args) {
	    rewardPointsGrid.updateRowCount();
	    rewardPointsGrid.render();
	  });
	  rewardPointsDataView.onRowsChanged.subscribe(function (e, args) {
	    rewardPointsGrid.invalidateRows(args.rows);
	    rewardPointsGrid.render();
	  });

		$(rewardPointsGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	rewardPointsSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				columnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in columnFilters) {
					  if (columnId !== undefined && columnFilters[columnId] !== "") {
						 rewardPointsSearchString += columnId + ":" +columnFilters[columnId]+",";
					  }
					}
					getRewardPointsGridData(0);
				}
			  }
		 
		});
		rewardPointsGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){			
				$("<input type='text'>")
			   .data("columnId", args.column.id)
			   .val(columnFilters[args.column.id])
			   .appendTo(args.node);
			}			
		});
		rewardPointsGrid.init();	  	 
		 
	  rewardPointsDataView.beginUpdate();
	  rewardPointsDataView.setItems(rewardPointsData);
	  
	  rewardPointsDataView.endUpdate();
	  rewardPointsDataView.syncGridSelection(rewardPointsGrid, true);
	  rewardPointsDataView.refresh();
	  $("#gridContainer").resizable();
	  rewardPointsGrid.resizeCanvas();
}	

window.onunload = function () {
    var win = window.opener;
    if (!win.closed) {
    	//window.opener.getPOGridData(0);
    }
};

window.onresize = function(){
	rewardPointsGrid.resizeCanvas();
}

function resetFilters(){	
	rewardPointsSearchString='';
	columnFilters = {};
	getRewardPointsGridData(0);
}
</script>

<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i> Customers
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Customers</a>
			</li>
			<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Reward Points</li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<c:if test="${successMessage != null}">
			<div class="alert alert-success fade in">
				<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${successMessage}</strong>
			</div>
		</c:if>
		<c:if test="${errorMessage != null}">
			<div class="alert alert-block alert-danger fade in">
				<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${errorMessage}</strong>
			</div>
		</c:if>
	</div>
</div>
<br />

<div class="row">
	<div class="col-lg-12">
		<input type="hidden" id="breakUpMsg" name="breakUpMsg" value="${rewardPointBreakUp}">
		<input id="customerId" value="${customerId}" name="customerId" type="hidden" />
		<div style="position: relative">
			<div style="width: 100%;">
				<br />
				<br />
				<div class="grid-header" style="width: 100%">
					<label>Reward Points</label>
					<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters</a>
				</div>
				<div id="custRewardPointsGrid" style="width: 100%;height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
				<div id="rewardPoints_pager" style="width: 100%; height: 20px;"></div>
			</div>
		</div>
	</div>
</div>
<br/><br/>
	

	<c:if test="${empty rewardPointsList}">
		<div class="form-group" align="center">
			<div class="col-sm-12">
				<label style="font-size: 15px;font-weight:bold;">No reward points found, Please purchase any ticket.</label>
			</div>
		</div>
	</c:if>
	
<!--</div>-->


<script>
window.onload = function() {
	pagingInfo = JSON.parse(JSON.stringify(${pagingInfo}));
	createRewardPointsGrid(JSON.parse(JSON.stringify(${rewardPointsList})));
};
</script>