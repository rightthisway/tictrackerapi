<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<!-- Bootstrap CSS -->    
    <link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../resources/css/font-awesome.min.css" rel="stylesheet" />    
    <!-- full calendar css-->
    <link href="../resources/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="../resources/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
    <!-- easy pie chart-->
    <link href="../resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <!-- owl carousel -->
    <link rel="stylesheet" href="../resources/css/owl.carousel.css" type="text/css">
	<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
	<link rel="stylesheet" href="../resources/css/fullcalendar.css">
	<link href="../resources/css/widgets.css" rel="stylesheet">
    <link href="../resources/css/style.css" rel="stylesheet">
    <link href="../resources/css/style-responsive.css" rel="stylesheet" />
	<link href="../resources/css/xcharts.min.css" rel=" stylesheet">	
	<link href="../resources/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
	
	<link href="../resources/css/jquery.alerts.css" rel=" stylesheet">
    
	<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>
	
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->
    
    <!-- javascripts -->
    <%-- <script src="../resources/js/jquery.js"></script>
	<script src="../resources/js/jquery-ui-1.10.4.min.js"></script> --%>
    <script src="../resources/js/jquery-1.8.3.min.js"></script>
    <%-- <script type="text/javascript" src="../resources/js/jquery-ui-1.9.2.custom.min.js"></script> --%>
    <!-- bootstrap -->
    <script src="../resources/js/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script src="../resources/js/jquery.scrollTo.min.js"></script>
    <script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../resources/js/jquery.alerts.js"></script>

	<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
	
	<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
	<script src="../resources/js/slick/slick.core.js"></script>
	<script src="../resources/js/slick/slick.formatters.js"></script>
	<script src="../resources/js/slick/slick.editors.js"></script>
	<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
	<script src="../resources/js/slick/slick.grid.js"></script>
	<script src="../resources/js/slick/slick.dataview.js"></script>
	<script src="../resources/js/slick/controls/slick.pager.js"></script>
	<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
	<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
	<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>
	<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>

<script>
window.onresize = function(){
	ticketDownloadGrid.resizeCanvas();
}
/*
var shippingCheckboxSelector = new Slick.CheckboxSelectColumn({
  cssClass: "slick-cell-checkboxsel"
});*/

var pagingInfo;
var ticketDownloadGrid;
var ticketDownloadDataView;
var ticketDownloadData=[];
var ticketDownloadColumn = [ /*shippingCheckboxSelector.getColumnDefinition(), 
                {id:"id", name:"ID", field: "id", sortable: true}, */
			   {id:"invoiceId", name:"Invoice ID", field: "invoiceId", sortable: true},
               {id:"fileName", name:"File Name", field: "fileName", sortable: true},
               {id:"fileType", name:"File Type", field: "fileType", sortable: true},
			   /*{id:"position", name:"Position", field: "position", sortable: true},*/
               {id:"view", name : "Download", field : "view", formatter : viewLinkFormatter}
              ];
              
var ticketDownloadOptions = {		  
	//editable: true,
	//enableAddRow: true,
	enableCellNavigation : true,
	//asyncEditorLoading: true,
	forceFitColumns : true,
	topPanelHeight : 25,
	//autoHeight: true
};

var sortcol = "invoiceId";
var sortdir = 1;
var percentCompleteThreshold = 0;
var searchString = "";

/* //filter option
function myFilter(item, args) {
	var x= item["shippingId"];
	if (args.searchString  != ""
			&& x.indexOf(args.searchString ) == -1) {
		
		if (typeof x === 'string' || x instanceof String) {
			if(x.toLowerCase().indexOf(args.searchString.toLowerCase()) == -1) {
				return false;
			}
		} else {
			return false;
		}
	}
	return true;
}
*/

function comparer(a, b) {
	  var x = a[sortcol], y = b[sortcol];
	  if(!isNaN(x)){
	  	return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	  }
	  if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
/*	
function shippingToggleFilterRow() {
	ticketDownloadGrid.setTopPanelVisibility(!ticketDownloadGrid.getOptions().showTopPanel);
}
	
//$(".grid-header .ui-icon-sg").addClass("ui-state-default-sg ui-corner-all").mouseover(function(e) {
	$("#shipping_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all").mouseover(function(e) {
	$(e.target).addClass("ui-state-hover")}).mouseout(function(e) {
		$(e.target).removeClass("ui-state-hover")
});
*/

function pagingControl(move,id){		
	var pageNo = 0;
	if(move == 'FIRST'){
		pageNo = 0;
	}else if(move == 'LAST'){
		pageNo = parseInt(pagingInfo.totalPages)-1;
	}else if(move == 'NEXT'){
		pageNo = parseInt(pagingInfo.pageNum) +1;
	}else if(move == 'PREV'){
		pageNo = parseInt(pagingInfo.pageNum)-1;
	}	
}

function createTicketDownloadGrid(jsonData) {
	ticketDownloadData=[];
	var i=0;
	if(jsonData!=null && jsonData.length > 0){
		for (var i = 0; i < jsonData.length; i++) {
			var  data = jsonData[i];
			var d = (ticketDownloadData[i] = {});
			d["id"] = data.id;
			d["invoiceId"] = data.invoiceId;
			d["fileName"] = data.fileName;
			d["fileType"] = data.fileType;
			d["position"] = data.position;
			d["view"] = data.downloadTicket;
		}
	}
	
	ticketDownloadDataView = new Slick.Data.DataView();
	ticketDownloadGrid = new Slick.Grid("#custTicketDownloadGrid", ticketDownloadDataView, ticketDownloadColumn, ticketDownloadOptions);
	ticketDownloadGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	ticketDownloadGrid.setSelectionModel(new Slick.RowSelectionModel());
	if(pagingInfo != null){
		var ticketDownloadGridPager = new Slick.Controls.Pager(ticketDownloadDataView, ticketDownloadGrid, $("#ticketDownload_pager"),pagingInfo);
	}
	var columnpicker = new Slick.Controls.ColumnPicker(ticketDownloadColumn, ticketDownloadGrid, ticketDownloadOptions);
	  
	  ticketDownloadGrid.onKeyDown.subscribe(function (e) {
	    // select all rows on ctrl-a
	    if (e.which != 65 || !e.ctrlKey) {
	      return false;
	    }
	    var rows = [];
	    for (var i = 0; i < ticketDownloadDataView.getLength(); i++) {
	      rows.push(i);
	    }
	    ticketDownloadGrid.setSelectedRows(rows);
	    e.preventDefault();
	  });
	  
	  ticketDownloadGrid.onSort.subscribe(function (e, args) {
	    sortdir = args.sortAsc ? 1 : -1;
	    sortcol = args.sortCol.field;
	    if ($.browser.msie && $.browser.version <= 8) {
	      ticketDownloadDataView.fastSort(sortcol, args.sortAsc);
	    } else {
	      ticketDownloadDataView.sort(comparer, args.sortAsc);
	    }
	  });
	  
	  // wire up model events to drive the ticketDownloadGrid
	  ticketDownloadDataView.onRowCountChanged.subscribe(function (e, args) {
	    ticketDownloadGrid.updateRowCount();
	    ticketDownloadGrid.render();
	  });
	  ticketDownloadDataView.onRowsChanged.subscribe(function (e, args) {
	    ticketDownloadGrid.invalidateRows(args.rows);
	    ticketDownloadGrid.render();
	  });
	  
	  ticketDownloadDataView.beginUpdate();
	  ticketDownloadDataView.setItems(ticketDownloadData);
	  
	  ticketDownloadDataView.endUpdate();
	  ticketDownloadDataView.syncGridSelection(ticketDownloadGrid, true);
	  ticketDownloadDataView.refresh();
	  $("#gridContainer").resizable();
	  ticketDownloadGrid.resizeCanvas();
}	

function viewLinkFormatter(row, cell, value, columnDef, dataContext) {
	var downloadTicket = ticketDownloadGrid.getDataItem(row).view;
	var invId = ticketDownloadGrid.getDataItem(row).invoiceId;
	var fileTyp = ticketDownloadGrid.getDataItem(row).fileType;
	var position = ticketDownloadGrid.getDataItem(row).position;
	var link = '';
	if(downloadTicket == 'Yes'){
		link = "<img class='editClickableImage' style='height:17px;' src='../resources/images/viewPdf.png' onclick=downloadTicketFile("+invId+",'"+fileTyp+"',"+position+"); />";
	}
	return link;
}
	
function downloadTicketFile(invoiceId,fileType,position){
	 var url = "${pageContext.request.contextPath}/Accounting/DownloadRealTix?invoiceId="+invoiceId+"&fileType="+fileType+"&position="+position;	 
	 $('#download-frame').attr('src', url);
}

window.onunload = function () {
    var win = window.opener;
    if (!win.closed) {
    	window.opener.getCustomerInfoForInvoice();
    }
};
</script>

<div class="row">
<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i> Client
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Manage Details</a>
			</li>
			<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Invoice</li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<c:if test="${successMessage != null}">
			<div class="alert alert-success fade in">
				<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${successMessage}</strong>
			</div>
		</c:if>
		<c:if test="${errorMessage != null}">
			<div class="alert alert-block alert-danger fade in">
				<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${errorMessage}</strong>
			</div>
		</c:if>
	</div>
</div>
<br />

<div class="row">
	<div class="col-lg-12">

		<div style="position: relative">
			<div style="width: 100%;">
				<br />
				<br />
				<div class="grid-header" style="width: 100%">
					<label>Invoice Details</label>
				</div>
				<div id="custTicketDownloadGrid" style="width: 100%;height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
				<div id="ticketDownload_pager" style="width: 100%; height: 20px;"></div>
			</div>
		</div>
		<br/>
		
	</div>
</div>
<br/>

	<c:if test="${empty invoiceInfo}">
		<div class="form-group" align="center">
			<div class="col-sm-12">
				<label style="font-size: 15px;font-weight:bold;">No Download Tickets found, Please add ticket to download.</label>
			</div>
		</div>
	</c:if>


<script>
window.onload = function() {
	pagingInfo = JSON.parse(JSON.stringify(${pagingInfo}));
	createTicketDownloadGrid(JSON.parse(JSON.stringify(${invoiceInfo})));
};
</script>