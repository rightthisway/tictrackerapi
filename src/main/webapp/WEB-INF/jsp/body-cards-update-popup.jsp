<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<script type='text/javascript' src='../resources/js/jquery-all.js'></script>
 <!-- Bootstrap CSS -->    
    <link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../resources/css/font-awesome.min.css" rel="stylesheet" />    
    <!-- full calendar css-->
    <link href="../resources/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="../resources/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
    <!-- easy pie chart-->
    <link href="../resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <!-- owl carousel -->
    <link rel="stylesheet" href="../resources/css/owl.carousel.css" type="text/css">
	<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
	<link rel="stylesheet" href="../resources/css/fullcalendar.css">
	<link href="../resources/css/widgets.css" rel="stylesheet">
    <link href="../resources/css/style.css" rel="stylesheet">
    <link href="../resources/css/style-responsive.css" rel="stylesheet" />
	<link href="../resources/css/xcharts.min.css" rel=" stylesheet">	
	<link href="../resources/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
    <link href="../resources/css/jquery.alerts.css" rel=" stylesheet">
    
    <script src="../resources/js/jquery-1.8.3.min.js"></script>
    <script src="../resources/js/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script src="../resources/js/jquery.scrollTo.min.js"></script>
    <script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>
   
    <!-- custom select -->
    <script src="../resources/js/jquery.customSelect.min.js" ></script>
	<%-- <script src="../resources/assets/chart-master/Chart.js"></script> --%>
    <script src="../resources/js/owl.carousel.js" ></script>
    <!--custome script for all page-->
    <script src="../resources/js/scripts.js"></script>
	<script src="../resources/js/jquery.slimscroll.min.js"></script>
	
	<link href="../resources/css/datepicker.css" rel="stylesheet">
	<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
	<script src="../resources/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
	<script src="../resources/js/jquery.alerts.js"></script>
	

<style>
input {
	color: black !important;
}

.form-horizontal .form-group {
    border-bottom: 1px solid #FFFFFF;
    padding-bottom: 5px;
    margin-bottom: 5px;
    margin-left: 0px;
    margin-right: 0px;
  }
 .form-group-top {
    padding-top: 5px;
    margin-top: 5px;
    margin-left: 0px;
    margin-right: 0px;
  }
 
 .td {
    text-align: center;
    vertical-align: middle;
}
  
</style>

<script type="text/javascript">
function callParent(info) {
	if(info != null && info!='') {
		window.close();
		window.opener.callChildWindowClose(info);
		
	}
		
}
callParent('${info}');

$(document).ready(function(){
	
	 $('.autoArtist').autocomplete("AutoCompleteGrandChildAndArtistAndVenue", {
		width: 650,
		max: 1000,
		minChars: 2,		
		dataType: "text",
		
		formatItem: function(row, i, max) {
			if(row[0]=='ARTIST'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='VENUE'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			} else if(row[0]=='CHILD'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			} else {
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}
		}
	}).result(function (event,row,formatted){
		  //var rowId = $(this).attr('id').split("_")[1];
		  
			$('#tmatArtist').val('');
			$('#tmatArtistText').text(row[2]);
			$('#artistId').val(row[1]);
			
	});
});


var validFilesTypes = ["jpg", "jpeg"];

    function CheckExtension(file) {
        /*global document: false */
        var filePath = file.value;
        var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
        var isValidFile = false;

        for (var i = 0; i < validFilesTypes.length; i++) {
            if (ext == validFilesTypes[i]) {
                isValidFile = true;
                break;
            }
        }
        if (!isValidFile) {
            file.value = null;
            jAlert("Invalid File. Valid extensions are:\n\n" + validFilesTypes.join(", "));
        }

        return isValidFile;
    }
    
    var validFileSize = 1 * 1024 * 1024;
    function CheckFileSize(file) {
        /*global document: false */
        var fileSize = file.files[0].size;
        var isValidFile = false;
        if (fileSize !== 0 && fileSize <= validFileSize) {
            isValidFile = true;
        }
        else {
            file.value = null;
            jAlert("File Size Should be Greater than 0 and less than 1 MB.");
        }
        return isValidFile;
    }
    
    function CheckFile(file) {
    	
    	if(file == null || file.value == null) {
    		jAlert("Please select valid file to update card.");
    		isValidFile = false;
    		return false;
    	}
        var isValidFile = CheckExtension(file);

        if (isValidFile)
            isValidFile = CheckFileSize(file);

        return isValidFile;
    }

function callCardTypeChange(obj) {
	if(obj.value=='PROMOTION') {
		$('#imageFileDiv').show();
		$('#cardImageDiv').hide();
		$('#fileRequired').val('Y');
		
		$('#cardDetailDiv').show();
		$('#tmatArtistDiv').show();
		
	} else if(obj.value=='YESNO') {
		$('#imageFileDiv').show();
		$('#cardImageDiv').hide();
		$('#fileRequired').val('Y');
		
		$('#cardDetailDiv').show();
		$('#tmatArtistDiv').hide();
		
	} else {
		$('#cardDetailDiv').hide();
		$('#tmatArtistDiv').hide();
		$('#fileRequired').val('N');
	}
}

function validateForm() {
var flag = true;
	
	var cardType = document.getElementById("cardType");
	if(cardType == null || cardType.value=='') {
		jAlert('Please select Cardtype to update card.');
		flag = false;
		return false;
	}
	
	var fileRequired = $('#fileRequired').val();
	var fileObj = document.getElementById("file");
	if(cardType.value == 'PROMOTION') {
		if(fileRequired == '' || fileRequired == 'Y') {
			flag = CheckFile(fileObj);
		}
		
		if(flag) {
			
			var imageTextObj = document.getElementById("imageText");
			if(imageTextObj == null || imageTextObj.value=='') {
				jAlert('Please Enter valid image text to update promotion card.');
				flag = false;
				return false;
			}
			var tmatArtistIdObj = document.getElementById("artistId");
			if(tmatArtistIdObj == null || tmatArtistIdObj.value=='') {
				jAlert('Please select Valid TMAT artist to update promotion card.');
				flag = false;
				return false;
			}
		}
		
	} else if(cardType.value == 'YESNO') {
		if(fileRequired == '' || fileRequired == 'Y') {
			flag = CheckFile(fileObj);
		}
		
		if(flag) {
			var imageTextObj = document.getElementById("imageText");
			if(imageTextObj == null || imageTextObj.value=='') {
				jAlert('Please Enter valid image text to update promotion card.');
				flag = false;
				return false;
			}
		}
	}
	if(flag) {
		var regex = /[0-9]|\./;
		var desktopPosition = document.getElementById("desktopPosition").value;
		if(desktopPosition=='' || !regex.test(desktopPosition)) {
			jAlert('Please Enter valid desktop position.');
			flag = false;
			return false;
		} 
		
		var mobilePosition = document.getElementById("mobilePosition").value;
		if(mobilePosition=='' || !regex.test(mobilePosition)) {
			jAlert('Please Enter valid mobile position.');
			flag = false;
			return false;
		} 
	}
	return flag;
}


function callSaveBtnOnClick() {
	
	var flag = validateForm();
	
	if(flag) {
		var cardId = $("#id").val();
		var mobilePosition = document.getElementById("mobilePosition").value;
		var desktopPosition = document.getElementById("desktopPosition").value;
		var isExistingCard = checkExistingCard(cardId,desktopPosition,mobilePosition);
		if(isExistingCard) {
			flag = false;
			return false;
		}	
	}

	
}
function callSubmitForm(action) {
	$("#action").val(action);
	$("#fileForm").attr('action', '${pageContext.request.contextPath}/RewardTheFan/updateCards');
	$("#fileForm").submit();
}

	function checkExistingCard(cardId,desktopPosition,mobilePosition) {
		var productId=$("#productId").val();
		$.ajax({
			url : "${pageContext.request.contextPath}/CheckCardPosition",
			type : "post",
			data : "productId="+productId+"&cardId="+ cardId+"&dPosition="+desktopPosition+"&mPosition="+mobilePosition,
			success : function(res){
				if(res=="No Card Exist for this Positions.") {
					callSubmitForm('update');
					return false;					
				} else {
					jAlert(res);
					return true;
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return true;
			}
		});
		return true;
	}

	function callDeleteBtnOnClick() {
		jConfirm("Are you sure you want to delete this card ?","Confirm",function(r){
		if(r){
			callSubmitForm('delete');
		}else{}
		});
	}
	
function callModifyBtnOnClick() {
	$('#imageRow').show();
	$('#cardImageRow').hide();
	var cardType = document.getElementById("cardType");
	callCardTypeChange(cardType,rowNumber);
}

function callChangeImage(){
	$('#imageFileDiv').show();
	$('#cardImageDiv').hide();
	$('#fileRequired').val('Y');
}
</script>


<div class="container">
    <div class="row clearfix">
		<div class="full-width column">
		<form role="form" name="fileForm" enctype="multipart/form-data" id="fileForm" method="post" modelAttribute="cards" action="${pageContext.request.contextPath}/Client/AddCategoryTickets">
			<input type="hidden" name="action" id="action" value="action"/>
			<input type="hidden" name="productId" id="productId" value="${product.id}"/>
			<div class="table-responsive card-table">
			<table class="table table-bordered table-hover" id="tab_logic">
				<thead>
					<tr >
						<th class="col-lg-3">
							Card Type
						</th>
						<th class="col-lg-4">
							Card Details
						</th>
						<th class="col-lg-3">
							Category Details
						</th>
						<th class="col-lg-1">
							Desktop Position
						</th>
						<th class="col-lg-1">
							Mobile Position
						</th>
					</tr>
				</thead>
				<tbody>
                      <tr id="imageRow">
						<td style="vertical-align: middle;  ">
						<input type="hidden" name="id" id="id" value="${card.id}" />
						<input type="hidden" name="fileRequired" id="fileRequired" value="N" />
						<input type="hidden" name="imageFileUrl" id="imageFileUrl" value="${card.imageFileUrl}"/>
						<select name="cardType" id="cardType" class="form-control input-sm m-bot15" onchange="callCardTypeChange(this);">
                        	<option value="">--- Select ---</option>
							<option value="UPCOMMINGEVENT" <c:if test="${card.cardType eq 'UPCOMMINGEVENT'}">selected</c:if> >UpComming Event Card</option>
							<option value="PROMOTION" <c:if test="${card.cardType eq 'PROMOTION'}">selected</c:if>>Promotion Card</option>
							<option value="YESNO" <c:if test="${card.cardType eq 'YESNO'}">selected</c:if> >Yes/No Card</option>
                       </select>
						</td>
						<td style="font-size: 13px;" align="center">
							<div class="text-left" id="cardDetailDiv" 
							<c:if test="${(card.cardType eq null or (card.cardType ne 'PROMOTION' and card.cardType ne 'YESNO'))}">style="display: none;" </c:if>>
                                  <div class="form-group form-group-top full-width" id="imageFileDiv" 
                                  <c:if test="${(card.id ne null and (card.cardType eq 'PROMOTION' or card.cardType eq 'YESNO'))}">style="display: none;" </c:if>>
                                      <label for="imageFile" class="col-md-3 col-sm-12 col-xs-12 control-label">Image File</label>
                                      <div class="col-md-9 col-sm-12 col-xs-12">
                                          <input type="file" id="file" name="file">
                                      </div>
                                  </div>
                                   <div class="form-group form-group-top full-width" id="cardImageDiv" 
                                   <c:if test="${(card.id eq null or (card.cardType ne 'PROMOTION' and card.cardType ne 'YESNO'))}">style="display: none" </c:if>>
                                      <div class="col-lg-12" >
                                       <c:if test="${card.cardType ne null and (card.cardType eq 'PROMOTION' or card.cardType eq 'YESNO')}">
                                          <img src='${pageContext.request.contextPath}/RewardTheFan/GetImageFile?type=rtwCards&filePath=${card.imageFileUrl}' height="150" width="400" />
                                          <a style="color:blue " href="javascript:callChangeImage();">
											Change Image</a>
                                         </c:if>
                                      </div>
                                  </div>
                                  <div class="form-group full-width" id="imageTextDiv">
                                      <label for="imageText" class="col-md-3 col-sm-12 col-xs-12 control-label">Image Text </label>
                                      <div class="col-md-9 col-sm-12 col-xs-12">
                                      	<input class="form-control input-sm m-bot15" type="text" name="imageText" id="imageText" value="${card.imageText}" size="40" >
                                      </div>
                                  </div>
                                  <div class="form-group full-width" id="tmatArtistDiv"
                                  	<c:if test="${card.cardType eq null or card.cardType ne 'PROMOTION'}">style="display: none;" </c:if>>
                                      <label for="tmatArtist" class="col-md-3 col-sm-12 col-xs-12 control-label">TMAT Artist</label>
                                      <div class="col-md-9 col-sm-12 col-xs-12">
                                          <input class="form-control input-sm m-bot15 autoArtist" type="text" name="tmatArtist" id="tmatArtist" placeholder="Search TMAT artist">
                                          <input type="hidden" id="artistId" name="artistId" value ="${card.artistId}">
                                          <b><label for="imageText" class="text-left pull-left selected-artist" id="tmatArtistText"><c:if test="${card.artist ne null}">${card.artist.name}</c:if></label></b>
                                      </div>
                                  </div>
                              </div>
						</td>
						<td style="vertical-align: middle;  ">
						<select name="categoryType" id="categoryType" class="form-control input-sm m-bot15">
                        	<option value="">--- Select ---</option>
                        	<option value ="HOMEPAGE" selected> Home Page </option>
                        	<c:forEach var="parentCategory" items="${parentCategoryList}">
         						<option value="pc_${parentCategory.id}" <c:if test="${card.parentCategoryId eq parentCategory.id}">selected</c:if> ><c:out value="${parentCategory.name}"/></option>
     						</c:forEach>
     						<c:forEach var="childCategory" items="${childCategoryList}">
         						<option value="cc_${childCategory.id}" <c:if test="${card.childCategoryId eq childCategory.id}">selected</c:if> ><c:out value="${childCategory.name}"/></option>
     						</c:forEach>
     						<c:forEach var="grandChildCategory" items="${grandChildCategoryLsit}">
         						<option value="gc_${grandChildCategory.id}" <c:if test="${card.grandChildCategoryId eq grandChildCategory.id}">selected</c:if> ><c:out value="${grandChildCategory.name}"/></option>
     						</c:forEach>							
	                   </select>
						</td>
						<td style="vertical-align: middle;">
                            	<input class="form-control input-sm m-bot15" type="text" name="desktopPosition" size="4" id="desktopPosition" value="${card.desktopPosition}">
                            	<div id="imgContainer"></div>
						</td>
						<td style="vertical-align: middle;">
                            	<input class="form-control input-sm m-bot15" type="text" name="mobilePosition" size="4" id="mobilePosition" value="${card.mobilePosition}">
                            	<div id="imgContainer"></div>
						</td>
						
						</tr>
				</tbody>
			</table>
			</div>
			<div class="full-width form-group">
                <div class="col-lg-offset-5 col-lg-7">
                	<button type="button" class="btn btn-primary" onclick="callSaveBtnOnClick();">Save</button>
                	<c:if test="${card.id ne null }">
                    	<button type="button" class="btn btn-danger" onclick="callDeleteBtnOnClick();">Delete</button>
                    </c:if>
                    <button type="button" class="btn btn-default" onclick="window.close();">Close</button>
                    
                </div>
            </div>
           </form>
		</div>
		</div>
		</div>



