<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<!-- Bootstrap CSS -->    
    <link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../resources/css/font-awesome.min.css" rel="stylesheet" />    
    <!-- full calendar css-->
    <link href="../resources/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="../resources/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
    <!-- easy pie chart-->
    <link href="../resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <!-- owl carousel -->
    <link rel="stylesheet" href="../resources/css/owl.carousel.css" type="text/css">
	<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
	<link rel="stylesheet" href="../resources/css/fullcalendar.css">
	<link href="../resources/css/widgets.css" rel="stylesheet">
    <link href="../resources/css/style.css" rel="stylesheet">
    <link href="../resources/css/style-responsive.css" rel="stylesheet" />
	<link href="../resources/css/xcharts.min.css" rel=" stylesheet">	
	<link href="../resources/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
	
	<link href="../resources/css/jquery.alerts.css" rel=" stylesheet">
    
	<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>
	
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->
    
    <!-- javascripts -->
    <%-- <script src="../resources/js/jquery.js"></script>
	<script src="../resources/js/jquery-ui-1.10.4.min.js"></script> --%>
    <script src="../resources/js/jquery-1.8.3.min.js"></script>
    <%-- <script type="text/javascript" src="../resources/js/jquery-ui-1.9.2.custom.min.js"></script> --%>
    <!-- bootstrap -->
    <script src="../resources/js/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script src="../resources/js/jquery.scrollTo.min.js"></script>
    <script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../resources/js/jquery.alerts.js"></script>

	<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
	
	<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
	<script src="../resources/js/slick/slick.core.js"></script>
	<script src="../resources/js/slick/slick.formatters.js"></script>
	<script src="../resources/js/slick/slick.editors.js"></script>
	<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
	<script src="../resources/js/slick/slick.grid.js"></script>
	<script src="../resources/js/slick/slick.dataview.js"></script>
	<script src="../resources/js/slick/controls/slick.pager.js"></script>
	<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
	<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
	<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>
	<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<style>
.cell-title {
      font-weight: bold;
    }
    .cell-effort-driven {
      text-align: center;
    }
    .slick-headerrow-column {
     background: #87ceeb;
     text-overflow: clip;
     -moz-box-sizing: border-box;
     box-sizing: border-box;
	}
	.slick-headerrow-column input {
	     margin: 0;
	     padding: 0;
	     width: 100%;
	     height: 100%;
	     -moz-box-sizing: border-box;
	     box-sizing: border-box;
	}
    .cell-selection {
      border-right-color: silver;
      border-right-style: solid;
      background: silver;
      color: gray;
      text-align: right;
      font-size: 10px;
    }
    .slick-row.selected .cell-selection {
      background-color: transparent; /* show default selected row background */
    }
	
</style>
<script>

var pagingInfo;
var holdTicketsGrid;
var holdTicketsDataView;
var holdTicketsData=[];
var holdTicketsSearchString = "";
var columnFilters = {};
var holdTicketsColumn = [ {id:"customerName", name:"Customer Name", field: "customerName", sortable: true},
               {id:"salePrice", name:"Sale Price", field: "salePrice", sortable: true},
               {id:"createdBy", name:"Created By", field: "createdBy", sortable: true},
               {id:"expirationDate", name:"Expiration Date", field: "expirationDate", sortable: true},
               {id:"expirationMinutes", name:"Expiration Minutes", field: "expirationMinutes", sortable: true},
               {id:"shippingMethodId", name:"Shipping Method ID", field: "shippingMethodId", sortable: true},               
               {id:"holdDate", name:"Hold Date", field: "holdDate", sortable: true},
               {id:"internalNote", name:"Internal Notes", field: "internalNote", sortable: true},
               {id:"externalNote", name:"External Notes", field: "externalNote", sortable: true},
               {id:"ticketIds", name:"Ticket ID's", field: "ticketIds", sortable: true},
               {id:"ticketGroupId", name:"Ticket Group ID", field: "ticketGroupId", sortable: true},
               {id:"externalPo", name:"External PO", field: "externalPo", sortable: true},
               {id:"status", name:"Status", field: "status", sortable: true}		   
              ];
              
var holdTicketsOptions = {	
    enableCellNavigation : true,
	forceFitColumns : true,
	multiSelect: false,
	topPanelHeight : 25,
	showHeaderRow: true,
	headerRowHeight: 30,
	explicitInitialization: true
};

var sortcol = "customerName";
var sortdir = 1;
var percentCompleteThreshold = 0;

function comparer(a, b) {
	  var x = a[sortcol], y = b[sortcol];
	  if(!isNaN(x)){
	  	return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	  }
	  if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
 
function pagingControl(move,id){		
	var pageNo = 0;
	if(move == 'FIRST'){
		pageNo = 0;
	}else if(move == 'LAST'){
		pageNo = parseInt(pagingInfo.totalPages)-1;
	}else if(move == 'NEXT'){
		pageNo = parseInt(pagingInfo.pageNum) +1;
	}else if(move == 'PREV'){
		pageNo = parseInt(pagingInfo.pageNum)-1;
	}	
	getHoldTicketsGridData(pageNo);
}

function getHoldTicketsGridData(pageNo) {
		$.ajax({
			url : "${pageContext.request.contextPath}/GetHoldTickets",
			type : "post",
			dataType: "json",
			data : "pageNo="+pageNo+"&headerFilter="+holdTicketsSearchString,
			success : function(res){
				var jsonData = res;
				if(jsonData==null || jsonData=='') {
					jAlert("No Data Found.");
				}
				pagingInfo = jsonData.pagingInfo;
				createHoldTicketsGrid(jsonData.holdTicketsList);
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
function createHoldTicketsGrid(jsonData) {	
	holdTicketsData=[];
	var i=0;
	if(jsonData!=null && jsonData.length > 0){
		for (var i = 0; i < jsonData.length; i++) {
			var  data = jsonData[i];
			var d = (holdTicketsData[i] = {});
			d["id"] = i;
			d["customerName"] = data.customerName;
			d["salePrice"] = data.salePrice;
			d["createdBy"] = data.createdBy;
			d["expirationDate"] = data.expirationDate;
			d["expirationMinutes"] = data.expirationMinutes;
			d["shippingMethodId"] = data.shippingMethodId;
			d["holdDate"] = data.holdDate;
			d["internalNote"] = data.internalNote;
			d["externalNote"] = data.externalNote;
			d["ticketIds"] = data.ticketIds;
			d["ticketGroupId"] = data.ticketGroupId;
			d["externalPo"] = data.externalPo;
			d["status"] = data.status;
		}
	}
					
	holdTicketsDataView = new Slick.Data.DataView();
	holdTicketsGrid = new Slick.Grid("#eventHoldTicketsGrid", holdTicketsDataView, holdTicketsColumn, holdTicketsOptions);
	holdTicketsGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	holdTicketsGrid.setSelectionModel(new Slick.RowSelectionModel());
	if(pagingInfo != null){
		var shippingGridPager = new Slick.Controls.Pager(holdTicketsDataView, holdTicketsGrid, $("#holdTickets_pager"),pagingInfo);
	}
	var columnpicker = new Slick.Controls.ColumnPicker(holdTicketsColumn, holdTicketsGrid, holdTicketsOptions);

	  /*
	  holdTicketsGrid.onKeyDown.subscribe(function (e) {
	    // select all rows on ctrl-a
	    if (e.which != 65 || !e.ctrlKey) {
	      return false;
	    }
	    var rows = [];
	    for (var i = 0; i < holdTicketsDataView.getLength(); i++) {
	      rows.push(i);
	    }
	    holdTicketsGrid.setSelectedRows(rows);
	    e.preventDefault();
	  }); */
	  holdTicketsGrid.onSort.subscribe(function (e, args) {
	    sortdir = args.sortAsc ? 1 : -1;
	    sortcol = args.sortCol.field;
	    if ($.browser.msie && $.browser.version <= 8) {
	      holdTicketsDataView.fastSort(sortcol, args.sortAsc);
	    } else {
	      holdTicketsDataView.sort(comparer, args.sortAsc);
	    }
	  });
	  // wire up model events to drive the holdTicketsGrid
	  holdTicketsDataView.onRowCountChanged.subscribe(function (e, args) {
	    holdTicketsGrid.updateRowCount();
	    holdTicketsGrid.render();
	  });
	  holdTicketsDataView.onRowsChanged.subscribe(function (e, args) {
	    holdTicketsGrid.invalidateRows(args.rows);
	    holdTicketsGrid.render();
	  });

		$(holdTicketsGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	holdTicketsSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				columnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in columnFilters) {
					  if (columnId !== undefined && columnFilters[columnId] !== "") {
						 holdTicketsSearchString += columnId + ":" +columnFilters[columnId]+",";
					  }
					}
					getHoldTicketsGridData(0);
				}
			  }		 
		});
		holdTicketsGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){			
				$("<input type='text'>")
			   .data("columnId", args.column.id)
			   .val(columnFilters[args.column.id])
			   .appendTo(args.node);
			}			
		});
		holdTicketsGrid.init();	  	 
		 
	  holdTicketsDataView.beginUpdate();
	  holdTicketsDataView.setItems(holdTicketsData);
	  
	  holdTicketsDataView.endUpdate();
	  holdTicketsDataView.syncGridSelection(holdTicketsGrid, true);
	  holdTicketsDataView.refresh();
	  $("#gridContainer").resizable();
	  holdTicketsGrid.resizeCanvas();
}	

window.onresize = function(){
	holdTicketsGrid.resizeCanvas();
}

function resetFilters(){	
	holdTicketsSearchString='';
	columnFilters = {};
	getHoldTicketsGridData(0);
}
</script>

<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i> Events
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Events</a>
			</li>
			<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Holded Tickets</li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<c:if test="${successMessage != null}">
			<div class="alert alert-success fade in">
				<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${successMessage}</strong>
			</div>
		</c:if>
		<c:if test="${errorMessage != null}">
			<div class="alert alert-block alert-danger fade in">
				<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${errorMessage}</strong>
			</div>
		</c:if>
	</div>
</div>
<br />

<div class="row">
	<div class="col-lg-12">
		<div style="position: relative">
			<div style="width: 100%;">
				<br />
				<br />
				<div class="grid-header" style="width: 100%">
					<label>Hold/UnHold Tickets</label>
					<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters</a>
				</div>
				<div id="eventHoldTicketsGrid" style="width: 100%;height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
				<div id="holdTickets_pager" style="width: 100%; height: 20px;"></div>
			</div>
		</div>
	</div>
</div>
<br/><br/>
	

<c:if test="${empty holdTicketsList}">
	<div class="form-group" align="center">
		<div class="col-sm-12">
			<label style="font-size: 15px;font-weight:bold;">No hold tickets found, Please purchase any ticket.</label>
		</div>
	</div>
</c:if>
	

<script>
window.onload = function() {
	pagingInfo = JSON.parse(JSON.stringify(${pagingInfo}));
	createHoldTicketsGrid(JSON.parse(JSON.stringify(${holdTicketsList})));
};
</script>